/***************************************************************************************
    JavaScript Calendar - Digital Christian Design
    //Script featured on and available at JavaScript Kit: http://www.javascriptkit.com
    // Functions
        changedate(): Moves to next or previous month or year, or current month depending on the button clicked.
        createCalendar(): Renders the calander into the page with links for each to fill the date form filds above.
            
***************************************************************************************/
//*************************************************************************************
//*Modificación realizada por Lcdo. Maikol Isava
//*************************************************************************************
//Diarios es sin letra.
//Anual: Y
/*var events = new Array(
    ["Y","5", "18","2016", "", "10:00 AM", "10:59 AM", "", "Aniversario Contraloria Del Estado Barinas.", "Contraloria Del Estado Barinas", "<img src=\"{$_Parametros.ruta_Img}modIN/eventos/68.jpg\" width=\"auto\" height=\"140\">"],
    ["","5", "18","2016", "", "10:00 AM", "10:59 AM", "", "Aniversario Contraloria Del Estado Barinas.", "Contraloria Del Estado Barinas", "<img src=\"{$_Parametros.ruta_Img}modIN/eventos/68.jpg\" width=\"auto\" height=\"140\">"],
    ["","5", "23","2016", "", "11:30 AM", "02:00 PM", "", "Aniversario Contraloria De Monagas.", "Contraloria Del Estado Monagas", "<img src=\"{$_Parametros.ruta_Img}modIN/eventos/CONTRALORIAMONAGAS.jpeg\" width=\"auto\" height=\"140\">"]
);  */

var thisDate = 1;       

var wordMonth = new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
var today = new Date();                         // Date object to store the current date
var todaysDay = today.getDay() + 1;                 // Stores the current day number 1-7
var todaysDate = today.getDate();                   // Stores the current numeric date within the month
var todaysMonth = today.getUTCMonth() + 1;              // Stores the current month 1-12
var todaysYear = today.getFullYear();                   // Stores the current year
var monthNum = todaysMonth;                     // Tracks the current month being displayed
var yearNum = todaysYear;                       // Tracks the current year being displayed
var firstDate = new Date(String(monthNum)+"/1/"+String(yearNum));   // Object Storing the first day of the current month
var firstDay = firstDate.getUTCDay();                   // Tracks the day number 1-7 of the first day of the current month
var lastDate = new Date(String(monthNum+1)+"/0/"+String(yearNum));  // Tracks the last date of the current month
var numbDays = 0;
var calendarString = "";
var eastermonth = 0;
var easterday = 0;

//*****************************
function changedate(buttonpressed) {
    if (buttonpressed == "prevyr") yearNum--;
    else if (buttonpressed == "nextyr") yearNum++;
    else if (buttonpressed == "prevmo") monthNum--;
    else if (buttonpressed == "nextmo") monthNum++;
    else  if (buttonpressed == "return") { 
        monthNum = todaysMonth;
        yearNum = todaysYear;
    }

    if (monthNum == 0) {
        monthNum = 12;
        yearNum--;
    }
    else if (monthNum == 13) {
        monthNum = 1;
        yearNum++
    }

    lastDate = new Date(yearNum, monthNum, 0);//new Date(String(monthNum+1)+"/0/"+String(yearNum));
    numbDays = lastDate.getDate();
    firstDate = new Date(String(monthNum)+"/1/"+String(yearNum));
    firstDay = firstDate.getDay() + 1;
    createCalendar();
    return;
}

//******************************
function easter(year) {
// alimentar en el año que devuelve el mes y el día de Pascua usando dos variables globales : eastermonth y Easterday
var a = year % 19;
var b = Math.floor(year/100);
var c = year % 100;
var d = Math.floor(b/4);
var e = b % 4;
var f = Math.floor((b+8) / 25);
var g = Math.floor((b-f+1) / 3);
var h = (19*a + b - d - g + 15) % 30;
var i = Math.floor(c/4);
var j = c % 4;
var k = (32 + 2*e + 2*i - h - j) % 7;
var m = Math.floor((a + 11*h + 22*k) / 451);
var month = Math.floor((h + k - 7*m + 114) / 31);
var day = ((h + k - 7*m +114) % 31) + 1;
eastermonth = month;
easterday = day;
}

//*****************************
//showevents(' + numero de dia + ',' + numero mes + ',' + numero año + ',' + i + ',' + x + ') //i -> 1 a 6 ; x -> 1 a 7

function createCalendar() {
    calendarString = '';
    var daycounter = 0;
    calendarString += '<table width="100%"  border="0" class="tabla-calendario" cellpadding="0" cellspacing="1">';
    calendarString += '<tbody><tr class="calendario_fila_mes ">';
    calendarString += '<td class="calendario_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"40\" onMouseOver=\"\" onMouseOut=\"\" onClick=\"changedate(\'prevyr\')\" ><<<<\/td>';
    calendarString += '<td class="calendario_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"40\" onMouseOver=\"\" onMouseOut=\"\" onClick=\"changedate(\'prevmo\')\" ><<<\/td>';
    calendarString += '<td class="calendario_dias" bgcolor=\"\" align=\"center\" valign=\"center\" width=\"128\" height=\"40\" colspan=\"3\"><b>' + wordMonth[monthNum-1] + '&nbsp;&nbsp;' + yearNum + '<\/b><\/td>';
    calendarString += '<td  class="calendario_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"40\" onMouseOver=\"\" onMouseOut=\"" onClick=\"changedate(\'nextmo\')\" >>><\/td>';
    calendarString += '<td class="calendario_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"40\" onMouseOver=\"" onMouseOut=\"" onClick=\"changedate(\'nextyr\')\" >>>><\/td>';
    calendarString += '<\/tr>';
    calendarString += '<tr > ';
    calendarString += '<td class="calendario_fila_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"22\">DOM<\/td>';
    calendarString += '<td class="calendario_fila_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"22\">LUN<\/td>';
    calendarString += '<td class="calendario_fila_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"22\">MAR<\/td>';
    calendarString += '<td class="calendario_fila_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"22\">MIÉ<\/td>';
    calendarString += '<td class="calendario_fila_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"22\">JUE<\/td>';
    calendarString += '<td class="calendario_fila_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"22\">VIE<\/td>';
    calendarString += '<td class="calendario_fila_dias" align=\"center\" valign=\"center\" width=\"40\" height=\"22\">SÁB<\/td>';
    calendarString += '<\/tr>';

    thisDate == 1;

    for (var i = 1; i <= 6; i++) {
        calendarString += '<tr>';
        for (var x = 1; x <= 7; x++) {
            daycounter = (thisDate - firstDay)+1;
            thisDate++;
            if ((daycounter > numbDays) || (daycounter < 1)) {
                calendarString += '<td align=\"center\" class="" bgcolor=\"\" height=\"30\" width=\"40\">&nbsp;<\/td>';
            } else {
                if (checkevents(daycounter,monthNum,yearNum,i,x) || ((todaysDay == x) && (todaysDate == daycounter) && (todaysMonth == monthNum))){
                    if ((todaysDay == x) && (todaysDate == daycounter) && (todaysMonth == monthNum))
                    {
                        DIA_EVENTO = daycounter;
                        MES_EVENTO = monthNum;
                        ANIO_EVENTO = yearNum;
                        DIA_NUM = i;
                        SEMANA_NUM = x;
                        calendarString += '<td class="calendario_hoy calendario_dias" align=\"center\" bgcolor=\"\" height=\"30\" width=\"40\"><a href=\"javascript:showevents(' + daycounter + ',' + monthNum + ',' + yearNum + ',' + i + ',' + x + ')\">' + daycounter + '<\/a><\/td>';

                    }
                    else    calendarString += '<td class="calendario_evento calendario_dias" align=\"center\" bgcolor=\"\" height=\"30\" width=\"40\" title="Evento" ><a href=\"javascript:showevents(' + daycounter + ',' + monthNum + ',' + yearNum + ',' + i + ',' + x + ')\">' + daycounter + '<\/a><\/td>';
                } else {
                    calendarString += '<td align=\"center\" class="calendario_dias" bgcolor=\"#ffffff\" height=\"30\" width=\"40\">' + daycounter + '<\/td>';
                }
            }
        }
        calendarString += '<\/tr>';
    }

    calendarString += '<tr><td colspan=\"7\" nowrap align=\"center\" valign=\"center\" bgcolor=\"\" width=\"280\" height=\"\"><\/td><\/tr><\/tbody><\/table>';

    var object=document.getElementById('calendar');
    object.innerHTML= calendarString;
    thisDate = 1;
}

//***************************************
function checkevents(day,month,year,week,dayofweek) {
var numevents = 0;
var floater = 0;

    for (var i = 0; i < events.length; i++) {
        if (events[i][0] == "W") {
            if ((events[i][2] == dayofweek)) numevents++;
        }
        else if (events[i][0] == "Y") {
            if ((events[i][2] == day) && (events[i][1] == month)) numevents++;
        }
        else if (events[i][0] == "F") {
            if ((events[i][1] == 3) && (events[i][2] == 0) && (events[i][3] == 0) ) {
                easter(year);
                if (easterday == day && eastermonth == month) numevents++;
            } else {
                floater = floatingholiday(year,events[i][1],events[i][2],events[i][3]);
                if ((month == 5) && (events[i][1] == 5) && (events[i][2] == 4) && (events[i][3] == 2)) {
                    if ((floater + 7 <= 31) && (day == floater + 7)) {
                        numevents++;
                    } else if ((floater + 7 > 31) && (day == floater)) numevents++;
                } else if ((events[i][1] == month) && (floater == day)) numevents++;
            }
        }
        else if ((events[i][2] == day) && (events[i][1] == month) && (events[i][3] == year)) {
            numevents++;
        }
    }

    if (numevents == 0) {
        return false;
    } else {
        return true;
    }
}

//*******************************************************
function showevents(day,month,year,week,dayofweek) {
var theevent = "";
var floater = 0;

    for (var i = 0; i < events.length; i++) {
        // En primer lugar vamos a procesar los eventos recurrentes (si los hay ) :
        if (events[i][0] != "") {
            if (events[i][0] == "D") {
            }
            if (events[i][0] == "W") {
                if ((events[i][2] == dayofweek)) {
                theevent += "<p>Events of: \n" + month +'/'+ day +'/'+ year + '\n';
                theevent += events[i][6] + '\n';
                theevent += 'Start Time: ' + events[i][4] + '\n';
                theevent += 'Ending Time: ' + events[i][5] + '\n';
                theevent += 'Description: ' + events[i][7] + '\n';
                theevent += '\n -------------- \n\n</p><br>';
                document.getElementById('eventlist').innerHTML = theevent;
                }
            }
            if (events[i][0] == "M") {
            }
            if (events[i][0] == "Y") {
                if ((events[i][2] == day) && (events[i][1] == month)) {
                //theevent += "<p>Events of: \n" + month +'/'+ day +'/'+ year + '\n';
                theevent += ' <table width="100%" border="0"  style="/*border-radius:6px; box-shadow:0 1px 3px rgba(0,0,0,.35); border-bottom:1px solid #333; border-top:1px solid #333; border-left:1px solid #333; border-right:1px solid #333; margin-bottom:10px; padding:0px;*/ "> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td width="400" style="text-align:left; padding-left:10px"><span style="color:#333; font-weight:bold;" >Fecha: </span>' + day +'/'+ month +'/'+ year + ' </td>';
                                    theevent += ' <td width="163" id="imagen_evento" rowspan="7"  style="text-align:center">' + events[i][10] + '</td> ';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px"><span style="color:#333; font-weight:bold;" >Inicio: </span>' + events[i][5] + '</td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px"><span style="color:#333; font-weight:bold;" >Finalizacion: </span> ' + events[i][6] + '</td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px"><span style="color:#333; font-weight:bold;">Evento:</span></td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px">' + events[i][8] + '</td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px"><span style="color:#333; font-weight:bold;" >Lugar: </span></td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px">' + events[i][9] + '</td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td height="20" style="text-align:left; padding-left:10px"></td>';
                                    theevent += ' </tr> ';
                                    theevent += ' </table> ';

                document.getElementById('eventlist').innerHTML = "";
                document.getElementById('eventlist').innerHTML = theevent;
                }
            }
            if (events[i][0] == "F") {
                if ((events[i][1] == 3) && (events[i][2] == 0) && (events[i][3] == 0) ) {
                    if (easterday == day && eastermonth == month) {
                        theevent += "Events of: \n" + month +'/'+ day +'/'+ year + '\n';
                        theevent += events[i][6] + '\n';
                        theevent += 'Start Time: ' + events[i][4] + '\n';
                        theevent += 'Ending Time: ' + events[i][5] + '\n';
                        theevent += 'Description: ' + events[i][7] + '\n';
                        theevent += '\n -------------- \n\n';
                        document.getElementById('eventlist').innerHTML = theevent;
                    } 
                } else {
                    floater = floatingholiday(year,events[i][1],events[i][2],events[i][3]);

                    if ((month == 5) && (events[i][1] == 5) && (events[i][2] == 4) && (events[i][3] == 2)) {
                        if ((floater + 7 <= 31) && (day == floater + 7)) {
                            theevent += "Events of: \n" + month +'/'+ day +'/'+ year + '\n';
                            theevent += events[i][6] + '\n';
                            theevent += 'Start Time: ' + events[i][4] + '\n';
                            theevent += 'Ending Time: ' + events[i][5] + '\n';
                            theevent += 'Description: ' + events[i][7] + '\n';
                            theevent += '\n -------------- \n\n';
                            document.getElementById('eventlist').innerHTML = theevent;
                        } else if ((floater + 7 > 31) && (day == floater)) {
                            theevent += "Events of: \n" + month +'/'+ day +'/'+ year + '\n';
                            theevent += events[i][6] + '\n';
                            theevent += 'Start Time: ' + events[i][4] + '\n';
                            theevent += 'Ending Time: ' + events[i][5] + '\n';
                            theevent += 'Description: ' + events[i][7] + '\n';
                            theevent += '\n -------------- \n\n';
                            document.getElementById('eventlist').innerHTML = theevent;
                        }
                    } else if ((events[i][1] == month) && (floater == day)) {
                        theevent += "Events of: \n" + month +'/'+ day +'/'+ year + '\n';
                        theevent += events[i][6] + '\n';
                        theevent += 'Start Time: ' + events[i][4] + '\n';
                        theevent += 'Ending Time: ' + events[i][5] + '\n';
                        theevent += 'Description: ' + events[i][7] + '\n';
                        theevent += '\n -------------- \n\n';
                        document.getElementById('eventlist').innerHTML = theevent;
                    }
                }
        }
        }
        // Ahora vamos a procesar los eventos que suceden en un tiempo del mes correspondiente , día, año :
        else if ((events[i][2] == day) && (events[i][1] == month) && (events[i][3] == year)) {
            //theevent += "Events of: \n" + month +'/'+ day +'/'+ year + '\n';
            theevent += ' <table width="100%" border="0"  style="/*border-radius:6px; box-shadow:0 1px 3px rgba(0,0,0,.35); border-bottom:1px solid #333; border-top:1px solid #333; border-left:1px solid #333; border-right:1px solid #333; margin-bottom:10px; padding:0px;*/ "> ';

                                    theevent += ' <tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td width="400" style="text-align:left; padding-left:10px"><span style="color:#333; font-weight:bold;" >Fecha: </span>' + day +'/'+ month +'/'+ year + ' </td>';
                                    theevent += ' <td id="imagen_evento" width="163" rowspan="7"  style="text-align:center">' + events[i][10] + '</td> ';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px"><span style="color:#333; font-weight:bold;" >Inicio: </span>' + events[i][5] + '</td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px"><span style="color:#333; font-weight:bold;" >Finalizacion: </span> ' + events[i][6] + '</td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px"><span style="color:#333; font-weight:bold;">Evento:</span></td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px">' + events[i][8] + '</td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px"><span style="color:#333; font-weight:bold;" >Lugar: </span></td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td style="text-align:left; padding-left:10px">' + events[i][9] + '</td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' <td height="20" style="text-align:left; padding-left:10px"></td>';
                                    theevent += ' </tr> ';
                                    theevent += ' <tr> ';
                                    theevent += ' </table> ';
            document.getElementById('eventlist').innerHTML = "";
            document.getElementById('eventlist').innerHTML = theevent;
        }
    }
    if (theevent == "")
    {
        theevent += '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td width="2%" >&nbsp;</td><td width="78%" >No hay eventos para el d&iacute;a de hoy ('+ day +' - '+ month +' - '+ year+')</td><td width="20%" height="200" rowspan="3">&nbsp;</td></tr><tr><td height="">&nbsp;</td><td>&nbsp;</td></tr></table>';
        document.getElementById('eventlist').innerHTML = theevent;
    }
}

//********************************************
function floatingholiday(targetyr,targetmo,cardinaloccurrence,targetday){

var firstdate = new Date(String(targetmo)+"/1/"+String(targetyr));  // Object Storing the first day of the current month.
var firstday = firstdate.getUTCDay();   // The first day (0-6) of the target month.
var dayofmonth = 0; // zero out our calendar day variable.

    targetday = targetday - 1;

    if (targetday >= firstday) {
        cardinaloccurrence--;   // Subtract 1 from cardinal day.
        dayofmonth = (cardinaloccurrence * 7) + ((targetday - firstday)+1);
    } else {
        dayofmonth = (cardinaloccurrence * 7) + ((targetday - firstday)+1);
    }
return dayofmonth;

}
