/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        14-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

var AppFunciones = function(){
    var f = this;

    /**********
     * Descripcion: Funcion para Validar los errores del Formulario enviado desde el Controlador
     *
     * @param arrayDatos
     * @param mensaje
     **********/
    f.metValidarError = function(arrayDatos,mensaje){
        for (var item in arrayDatos) {
            if($.isArray(arrayDatos[item])){
                var array=arrayDatos[item];
                for (var itemArray in array){
                    if(array[itemArray]=='error'){
                        $(document.getElementById(item+itemArray+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                        $(document.getElementById(item+itemArray+'Error')).removeClass('has-success has-feedback');
                        $(document.getElementById(item+itemArray+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(item+itemArray+'Error')).addClass('has-error has-feedback');
                        $(document.getElementById(item+itemArray+'Error')).append('<span id="'+item+itemArray+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                        swal("Error!", mensaje, "error");
                    }else{
                        $(document.getElementById(item+itemArray+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(item+itemArray+'Error')).addClass('has-success has-feedback');
                        $(document.getElementById(item+itemArray+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                        $(document.getElementById(item+itemArray+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');

                    }
                }
            }else{
                if(arrayDatos[item]=='error'){
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
    };

    /*****
     * Descripcion: Funcion para actualizar los Datos de Una tabla al Modificar Un registro
     *
     * @param arrayDatos
     * @param id
     * @param nombreId
     * @param arrayCheck
     * @param arrayMostrarOrden
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metActualizarRegistroTabla = function(arrayDatos,id,nombreId,arrayCheck,arrayMostrarOrden,mensaje,idModalCerrar,idModalContenido){
        var html='';
        for (i = 0; i < arrayMostrarOrden.length; i++){
            if($.inArray(arrayMostrarOrden[i], arrayCheck)>=0){
                if(arrayDatos[arrayMostrarOrden[i]] ==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                html= html+'<td><i class="'+iconoFlag+'"></i></td>';
            }else{
                html= html+'<td> '+arrayDatos[arrayMostrarOrden[i]]+' </td>';
            }
        };
        html= html+'<td></td>';
        $(document.getElementById(nombreId+id)).html(html);
        swal("Registro Modificado!", mensaje, "success");
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    }

    /*****
     * Descripcion: Funcion para actualizar los Datos de Una tabla al Crear un nuevo Registro
     *
     * @param arrayDatos
     * @param id
     * @param nombreId
     * @param arrayCheck
     * @param arrayMostrarOrden
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metNuevoRegistroTabla = function(arrayDatos,id,nombreId,arrayCheck,arrayMostrarOrden,mensaje,idModalCerrar,idModalContenido,idTabla){
        var html='<tr id="'+nombreId+id+'">';
        for (i = 0; i < arrayMostrarOrden.length; i++){
            if($.inArray(arrayMostrarOrden[i], arrayCheck)>=0){
                if(arrayDatos[arrayMostrarOrden[i]] ==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                html= html+'<td><i class="'+iconoFlag+'"></i></td>';
            }else{
                html= html+'<td> '+arrayDatos[arrayMostrarOrden[i]]+' </td>';
            }
        };
        if(!idTabla){
            idTabla='datatable1';
        }
        html= html+'<td></td></tr>';
        $(document.getElementById(idTabla)).append(html);
        swal("Registro Guardado!", mensaje, "success");
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    }


    f.metAprobarRegistro = function(arrayDatos,id,idTabla,mensaje,idModalCerrar,idModalContenido){
        $(document.getElementById(idTabla+id)).remove();
        swal("Aprobado!", mensaje, "success");
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    };

    f._handleTabShow = function(tab, navigation, index, wizard){
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        navigation.find('li').removeClass('done');
        navigation.find('li.active').prevAll().addClass('done');

        wizard.find('.progress-bar').css({width: percent + '%'});
        $('.form-wizard-horizontal').find('.progress').css({'width': percentWidth});
    };


    f.metWizard = function(){
        $.validator.setDefaults({highlight:function(element){$(element).closest('.form-group').addClass('has-error');},unhighlight:function(element){$(element).closest('.form-group').removeClass('has-error');},errorElement:'span',errorClass:'help-block',errorPlacement:function(error,element){if(element.parent('.input-group').length){error.insertAfter(element.parent());}
        else if(element.parent('label').length){error.insertAfter(element.parent());}
        else{error.insertAfter(element);}}});

        $('#rootWizard').bootstrapWizard({
            onTabShow: function(tab, navigation, index) {
                f._handleTabShow(tab, navigation, index, $('#rootWizard'));

            },
            onTabClick: function(tab, navigation, index) {

                var form = $('#rootWizard').find('.form-validation');
                var valid = form.valid();
                if(!valid) {
                    form.data('validator').focusInvalid();
                    return false;
                }
            },
            onNext: function(tab, navigation, index) {
                var form = $('#rootWizard').find('.form-validation');
                var valid = form.valid();
                if(!valid) {
                    form.data('validator').focusInvalid();
                    return false;
                }
            },
            onLast: function(tab, navigation, index) {
                var form = $('#rootWizard').find('.form-validation');
                var valid = form.valid();
                if(!valid) {
                    form.data('validator').focusInvalid();
                    return false;
                }
            }
        });
    }


    f.metCodeMirror = function(idTextArea){
        var cm = CodeMirror.fromTextArea(document.getElementById(idTextArea), {
            lineNumbers: true,
            matchBrackets: true,
            indentUnit: 4,
            indentWithTabs: true,
            extraKeys: {
                "Ctrl-Space": "autocomplete",
                "Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); },
                "Alt-F": "findPersistent",
                "F11": function(cm) {
                    cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                },
                "Esc": function(cm) {
                    if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                }
            },
            mode: {name: "application/x-httpd-php", globalVars: true},
            lineWrapping: true,
            foldGutter: true,
            gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
        });
        return cm;
    }

    /***
     * Descripcion: Json de Estados
     * @param url
     * @param idPais
     * @param idEstado
     */
    f.metJsonEstado = function(url,idPais,idEstado){
        function json(id){
            $('#fk_a009_num_estado').html('');
            $('#fk_a009_num_estado').append('<option value="">Seleccione el Estado</option>');
            $('#s2id_fk_a009_num_estado .select2-chosen').html('Seleccione el Estado');
            if(!id){
                id = $('#fk_a008_num_pais').val();
            }else{
                id = idPais;
            }
            $.post(url,{ idPais: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_estado']==idEstado){
                        $('#fk_a009_num_estado').append('<option selected value="'+dato[i]['pk_num_estado']+'">'+dato[i]['ind_estado']+'</option>');
                        $('#s2id_fk_a009_num_estado .select2-chosen').html(dato[i]['ind_estado']);
                    }else{
                        $('#fk_a009_num_estado').append('<option value="'+dato[i]['pk_num_estado']+'">'+dato[i]['ind_estado']+'</option>');
                    }
                }
            },'json');
        }
        if(idPais){
            json(idPais);
            $('#fk_a008_num_pais').change(function(){
                json(false);
                $('#fk_a011_num_municipio').html('');
                $('#fk_a010_num_ciudad').html('');
            });
        }else{
            $('#fk_a008_num_pais').change(function(){
                json(false);
                $('#fk_a011_num_municipio').html('');
                $('#fk_a010_num_ciudad').html('');
            });
        }
    }

    /***
     * Descripcion: Json de Ciudad
     * @param url
     * @param idEstado
     * @param idCiudad
     */
    f.metJsonCiudad = function(url,idEstado,idCiudad){
        function json(id){
            $('#fk_a010_num_ciudad').html('');
            $('#fk_a010_num_ciudad').append('<option value="">Seleccione la Ciudad</option>');
            $('#s2id_fk_a010_num_ciudad .select2-chosen').html('Seleccione la Ciudad');
            if(!id){
                id = $('#fk_a009_num_estado').val();
            }else{
                id = idEstado;
            }
            $.post(url,{ idEstado: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_ciudad']==idCiudad){
                        $('#fk_a010_num_ciudad').append('<option selected value="'+dato[i]['pk_num_ciudad']+'">'+dato[i]['ind_ciudad']+'</option>');
                        $('#s2id_fk_a010_num_ciudad .select2-chosen').html(dato[i]['ind_ciudad']);
                    }else{
                        $('#fk_a010_num_ciudad').append('<option value="'+dato[i]['pk_num_ciudad']+'">'+dato[i]['ind_ciudad']+'</option>');
                    }
                }
            },'json');
        }
        if(idEstado){
            json(idEstado);
            $('#fk_a009_num_estado').change(function(){
                json(false);
            });
        }else{
            $('#fk_a009_num_estado').change(function(){
                json(false);
            });
        }
    }

    f.metJsonProcesoNomina = function(url,idNomina,idProceso){
        function json(id){
            if($('#fk_nmb001_num_tipo_nomina').val()!='') {
                $('#fk_nmb003_num_tipo_proceso').html('');
                $('#fk_nmb003_num_tipo_proceso').append('<option value="">Seleccione el Proceso</option>');
                $('#s2id_fk_nmb003_num_tipo_proceso .select2-chosen').html('Seleccione el Proceso');
                if (!id) {
                    id = $('#fk_nmb001_num_tipo_nomina').val();
                } else {
                    id = idNomina;
                }
                $.post(url, {idNomina: id}, function (dato) {
                    for (var i = 0; i < dato.length; i++) {
                        if (dato[i]['pk_num_tipo_proceso'] == idProceso) {
                            $('#fk_nmb003_num_tipo_proceso').append('<option selected value="' + dato[i]['pk_num_tipo_proceso'] + '">' + dato[i]['ind_nombre_proceso'] + '</option>');
                            $('#s2id_fk_nmb003_num_tipo_proceso .select2-chosen').html(dato[i]['ind_nombre_proceso']);
                        } else {
                            $('#fk_nmb003_num_tipo_proceso').append('<option value="' + dato[i]['pk_num_tipo_proceso'] + '">' + dato[i]['ind_nombre_proceso'] + '</option>');
                        }
                    }
                }, 'json');
            }else{
                $('#fk_nmb003_num_tipo_proceso').html('');
                $('#fk_nmb003_num_tipo_proceso').append('<option value="">Seleccione el Proceso</option>');
                $('#s2id_fk_nmb003_num_tipo_proceso .select2-chosen').html('Seleccione el Proceso');
            }
        }
        if(idNomina){
            json(idNomina);
            $('#fk_nmb001_num_tipo_nomina').change(function(){
                json(false);
            });
        }else{
            $('#fk_nmb001_num_tipo_nomina').change(function(){
                json(false);
            });
        }
    }

    f.metJsonPeriodoNomina = function(url,idNomina,idPeriodo){
        function json(id){
            if($('#fk_nmb001_num_tipo_nomina').val()!='') {
                $('#fk_nmc001_num_tipo_nomina_periodo').html('');
                $('#fk_nmc001_num_tipo_nomina_periodo').append('<option value="">Seleccione el Periodo</option>');
                $('#s2id_fk_nmc001_num_tipo_nomina_periodo .select2-chosen').html('Seleccione el Periodo');
                if (!id) {
                    id = $('#fk_nmb001_num_tipo_nomina').val();
                } else {
                    id = idNomina;
                }
                $.post(url, {idNomina: id}, function (dato) {
                    for (var i = 0; i < dato.length; i++) {
                        if (dato[i]['pk_num_tipo_nomina_periodo'] == idPeriodo) {
                            $('#fk_nmc001_num_tipo_nomina_periodo').append('<option selected value="' + dato[i]['pk_num_tipo_nomina_periodo'] + '">' + dato[i]['fec_anio'] + '-' + dato[i]['fec_mes'] + '</option>');
                            $('#s2id_fk_nmc001_num_tipo_nomina_periodo .select2-chosen').html(dato[i]['fec_anio'] + '-' + dato[i]['fec_mes']);
                        } else {
                            $('#fk_nmc001_num_tipo_nomina_periodo').append('<option value="' + dato[i]['pk_num_tipo_nomina_periodo'] + '">' + dato[i]['fec_anio'] + '-' + dato[i]['fec_mes'] + '</option>');
                        }
                    }
                    if(idNomina!="undefined"){
                        $('#fk_nmc001_num_tipo_nomina_periodo').append('<option value="otros">Otros Periodos</option>');
                    }
                }, 'json');
            }else{
                $('#fk_nmc001_num_tipo_nomina_periodo').html('');
                $('#fk_nmc001_num_tipo_nomina_periodo').append('<option value="">Seleccione el Periodo</option>');
                $('#s2id_fk_nmc001_num_tipo_nomina_periodo .select2-chosen').html('Seleccione el Periodo');
            }
        }
        if(idNomina){
            json(idNomina);
            $('#fk_nmb001_num_tipo_nomina').change(function(){
                json(false);
            });
        }else{
            $('#fk_nmb001_num_tipo_nomina').change(function(){
                json(false);
            });
        }
    }

    f.metJsonPeriodoProcesosDisponibles = function(url,accion,recepcion,padre,idJson,nombreJson,mensaje,todos,tipo){
        $('#'+accion).change(function(){
            if($('#'+accion).val()!='') {
                $('#'+recepcion).html('');
                $('#'+recepcion).append('<option value="">'+mensaje+'</option>');
                $('#s2id_'+recepcion+' .select2-chosen').html(mensaje);
                $.post(url, {idPeriodoProceso: $('#'+accion).val(),todos:todos, tipo:tipo }, function (dato) {
                    for (var i = 0; i < dato.length; i++) {
                        $('#'+recepcion).append('<option value="' + dato[i][idJson] + '">' + dato[i][nombreJson]+'</option>');
                    }
                }, 'json');
            }else{
                $('#'+recepcion).html('');
                $('#'+recepcion).append('<option value="">'+mensaje+'</option>');
                $('#s2id_'+recepcion+' .select2-chosen').html(mensaje);
                $('#listadoPersonas').html('');
            }
        });
        $('#'+padre).change(function(){
            if($('#'+padre).val()=='') {
                $('#'+recepcion).html('');
                $('#'+recepcion).append('<option value="">'+mensaje+'</option>');
                $('#s2id_'+recepcion+' .select2-chosen').html(mensaje);
                $('#listadoPersonas').html('');
            };
        });
    }

    /*****
     * Descripcion: Funcion para la datatabla del registro
     *
     * @param idDt
     * @param url
     * @param idioma
     * @param columnas
     * #Arreglo que contiene Filtro del sistema
     * @param filtrado
     */
    f.dataTable = function (idDt, url, idioma, columnas,filtrado) {
        var dt = $(idDt).dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": url,
                "type": "POST",
                "data": function ( d ) {
                    d.filtro = filtrado;
                }
            }, "language": {
                "processing":     "Procesando...",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "loadingRecords": "Cargando...",
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "columns": columnas
        });
        return dt;
    };
	
	
	 f.dataTable2 = function (idDt, url, idioma, columnas,filtrado) {
        var dt = $(idDt).dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": url,
                "type": "POST",
                "data": function ( d ) {
                    d.filtro = filtrado;
                }
            }, "language": {
                "processing":     "Procesando...",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "loadingRecords": "Cargando...",
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "columns": columnas
        });
        return dt;
    };

    /*****
     * Descripcion: Funcion para Aprobar los Datos de Una tabla al Crear un nuevo Registro
     *
     * @param idDataTabla
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metAprobarRegistroJson = function(idDataTabla,mensaje,idModalCerrar,idModalContenido){
        swal("Aprobado!", mensaje, "success");
        $('#'+idDataTabla).dataTable().api().row().remove().draw(false);
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    };


    /*****
     * Descripcion: Funcion para actualizar los Datos de Una tabla al Modificar Un registro
     *
     * @param idDataTabla
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metActualizarRegistroTablaJson = function(idDataTabla,mensaje,idModalCerrar,idModalContenido){
        swal("Registro Modificado!", mensaje, "success");
        $('#'+idDataTabla).dataTable().api().row().remove().draw(false);
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    };

    /*****
     * Descripcion: Funcion para actualizar los Datos de Una tabla al Crear un nuevo Registro
     *
     * @param idDataTabla
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metNuevoRegistroTablaJson = function(idDataTabla,mensaje,idModalCerrar,idModalContenido){
        swal("Registro Guardado!", mensaje, "success");
        $('#'+idDataTabla).dataTable().api().row().remove().draw(false);
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    };

    /*****
     * Descripcion: Funcion para Eliminar los Datos de Una tabla al Crear un nuevo Registro
     *
     * @param idDataTabla
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metEliminarRegistroJson = function(idDataTabla,mensaje,idModalCerrar,idModalContenido){
        swal("Eliminado!", mensaje, "success");
        $('#'+idDataTabla).dataTable().api().row().remove().draw(false);
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    };

}