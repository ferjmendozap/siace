/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Planificación Fiscal
 * Descripción: Funciones generales
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Alexis Ontiveros     |ontiveros.alexis@cmldc.gob.ve         | 0426-514.43.82
 * | 2 |
 * |_____________________________________________________________________________________
 *
 *  * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        26-12-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/
var AppfFunciones = function() {
    var f = this;
    /**
     * Prepara un objeto Json para ser pasado por referencia por una función.
     */
    f.metPreparaObj=function(obj){
        var a = '';
        if (obj.constructor == Array)
            a += '[';
        if (obj.constructor == Object)
            a += '{ ';
        for (var i in obj) {
            if (obj[i] == null) {
                obj[i] = ""
            }
            if (obj.constructor != Array)
                a += i + ':';
            if (obj[i].constructor == Object) {
                metPreparaObj(obj[i]);
            } else if (obj[i].constructor == Array) {
                metPreparaObj(obj[i]);
            } else if (obj[i].constructor == String) {
                obj[i] = obj[i].replace(/'|&#039;/g, "\\'");
                obj[i] = obj[i].replace(/"/g, '&#34;');
                6
                a += "'" + obj[i] + "',";
            } else {
                a += obj[i] + ',';
            }
        }
        if (obj.constructor == Object)
            a += '},';
        if (obj.constructor == Array)
            a += '],';
        return a.substr(0, a.length - 1).split(',}').join('}').split(',]').join(']');
    }
    /**
     * Permite el ingreso de sólo números enteros en un campo de formulario.
     */
    f.metValidaCampNum=function (evt) {
        var nav4 = window.Event ? true : false;
        var key = nav4 ? evt.which : evt.keyCode;
        return (key <= 13 || (key >= 48 && key <= 57));
    }
    /**
     * Selecciona el valor al posicionarse en campo de formulario
     */
    f.metSeleccionaCampo=function(nombre_selector) {
        $('#' + nombre_selector).select();
    }

    /**Habilita o desabilita uno o un conjunto objetos de formulario
     *opcion:True=habilita y false=deshabilita.
     */
    f.metEstadoObjForm=function(nombre_campos, opcion) {
        var nombres_obj = nombre_campos.split(',');
        if (opcion == true) {
            opcion = false;
        } else {
            opcion = true;
        }
        if (nombres_obj.length) {
            var i = 0;
            for (i; i < nombres_obj.length; i++) {
                $('#' + nombres_obj[i]).attr({'disabled': opcion})
            }
        }
    }
    f.metActivaError=function(data,limpiar){
        for (var item in data) {
            if(data[item]=='error'){
                if(limpiar){
                    $('#'+item+'Error').removeClass('has-success has-feedback');
                    $('#'+item+'Error').removeClass('has-error has-feedback');
                    $('#'+item+'Icono').remove();
                }else {
                    $('#' + item + 'Error').removeClass('has-success has-feedback');
                    $('#' + item + 'Error').removeClass('has-error has-feedback');
                    $('#' + item + 'Error').addClass('has-error has-feedback');
                    $('#' + item + 'Error').append('<span id="' + item + 'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                }
            }else{
                if(limpiar){
                    $('#'+item+'Error').removeClass('has-success has-feedback');
                    $('#'+item+'Error').removeClass('has-error has-feedback');
                    $('#'+item+'Icono').remove();
                }else{
                    $('#'+item+'Error').removeClass('has-error has-feedback');
                    $('#'+item+'Error').addClass('has-success has-feedback');
                    $('#'+item+'Icono').removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $('#'+item+'Icono').addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
    }
    /**
     * Quita caracteres especiales, espacios demás y enter de la cadena de los campos texto campos de un form.
     * Caracteres admitidos: #,', "
     * Ejemplo: mDepuraCampo('nombre_campo1,nombre_campo2,.....'); n campos separadors por coma
     */
    f.metDepuraText=function($nomb_campos){
        var $obj_form=$nomb_campos.split(',');
        if($obj_form.length){
            var $i=0
            for($i; $i < $obj_form.length; $i++){
                if($('#'+$obj_form[$i]).val()){
                    var $cadena=$('#'+$obj_form[$i]).val();
                    $cadena=$cadena.replace(/[{}*&%$@_\?\>\<\:\+\)\(!/¡¿=^\\|\[\]]/g,'');
                    $cadena=$cadena.replace(/([\n])/g, ' ');//Saltos de linea
                    $cadena=$cadena.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');//Quita los blancos demás
                    $('#'+$obj_form[$i]).val($cadena);
                }
            }
        }
    }
    /**
     * Quita caracteres especiales, espacios demás y enter de la cadena de los campos texto de un form.
     * Caracteres admitidos: (, ), %, #,', "
     * Ejemplo: mDepuraCampo('nombre_campo1,nombre_campo2,.....'); n campos separadors por coma
     */
    f.metDepuraText2=function($nomb_campos){
        var $obj_form=$nomb_campos.split(',');
        if($obj_form.length){
            var $i=0
            for($i; $i < $obj_form.length; $i++){
                if($('#'+$obj_form[$i]).val()){
                    var $cadena=$('#'+$obj_form[$i]).val();
                    $cadena=$cadena.replace(/[{}*&$@_\?\>\<\+!/¡¿=^\\|\[\]]/g,'');//Caracteres especiales.
                    $cadena=$cadena.replace(/([\n])/g, ' ');//Saltos de linea
                    $cadena=$cadena.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');//Quita los blancos demás
                    $('#'+$obj_form[$i]).val($cadena);
                }
            }
        }
    }
    /**
     * devuelve el nombre de una opción de un combobox.
     * @param nombre del combobox
     * @param id de la opción
     * @returns nombre de la opción.
     */
    f.metCapturaNombOpcion=function(nombre_combo,valor){
        return $("#"+nombre_combo).find("option[value='"+valor+"']").text();
    }
    /**
     * Busca el id en un combobox
     * @param nombreCombo, Nombre del combobox
     * @param valor, id a buscar
     * @returns el id de la opción
     */
    f.metBuscaIdValorComboBox=function(nombreCombo,valor){
        var retornar='';
        $("#"+nombreCombo+" option").each(function(){
            if ($(this).val() != "" ){
                if($(this).val()==valor){
                    retornar=$(this).val(); return false;
                }
            }
        });
        return retornar;
    }
    /**
     * Valida la entrada de datos de un campo para el R.I.F.
     * @param nombre_campo
     * Uso en js:  appf.metValidaDocFiscal($('#nombreCampo'));
     */
    f.metValidaDocFiscal=function (nombre_campo){
        $(nombre_campo).keyup(function () {
            var valor=this.value.charAt(0);
            if(valor == 'G' || valor == 'V' || valor == 'J' || valor == 'E' || valor== 'g'|| valor == 'v' || valor == 'j' || valor == 'e' || valor=='-') {
                var cadena = this.value;
                if(cadena.substring(1) != '') {
                    tempcadena=cadena.slice(-2);
                    if(tempcadena=='--'){
                        cadena=cadena.slice(0,-1);
                    }
                    var numeros = cadena.substring(1).replace(/[^0-9-]/ig, '');
                    $(this).val((valor+numeros).toUpperCase());
                }else{
                    if(cadena=='-') {
                        this.value='';
                    }else{
                        $(this).val($(this).val().toUpperCase() + '-');
                    }
                }
            }else{
                this.value='';
            }
        });
    }
    /**
     * Permite la entrara de solo letras, retroceso, suprimír, apóstrofo y comilla simple en un campo texto a través de una clase CSS como identificador del campo
     * @param letras
     * Ejemplo:
     * En el Html: <input type="text" name="nombre campo" id="id del campo" class="letras">
     * En el js; appf.metValidaCampoTextAlfab($('.letras'));
     */
    f.metValidaTextAlfabetico=function(letras){
        $(".lMayuscula").keypress(function (key) {
            if ((key.charCode < 97 || key.charCode > 122)//letras mayusculas
                && (key.charCode < 65 || key.charCode > 90) //letras minusculas
                && (key.charCode != 0) //retroceso
                && (key.charCode != 241) //ñ
                && (key.charCode != 209) //Ñ
                && (key.charCode != 32) //espacio
                && (key.charCode != 225) //á
                && (key.charCode != 233) //é
                && (key.charCode != 237) //í
                && (key.charCode != 243) //ó
                && (key.charCode != 250) //ú
                && (key.charCode != 193) //Á
                && (key.charCode != 201) //É
                && (key.charCode != 205) //Í
                && (key.charCode != 211) //Ó
                && (key.charCode != 218) //Ú
                && (key.charCode != 39) //'
            )
                return false;
        });
    }
    f.metConvertMayuscula=function(campo){
        $(campo).keyup(function() {
            $(this).val($(this).val().toUpperCase());
        });
    }
    f.metValidaEmail=function(nombre_campo){//valida Email.
        var retornar=true;
        if($("#"+nombre_campo).val().length > 1) {
            if($("#"+nombre_campo).val().indexOf('@', 0) == -1 || $("#"+nombre_campo).val().indexOf('.', 0) == -1) {
                retornar = false;
            }
        }
        return retornar;
    }
    f.metValidaUrl=function(campo, tipo){
        $(campo).keyup(function () {
            if (this.value.charAt(0) != 'w' && this.value.charAt(0) != '') {
                this.value = this.value.charAt(0).replace(/[^w]/i, '');
            }
            if (this.value.charAt(1) != 'w' && this.value.charAt(1) != '') {
                var valor = this.value.charAt(0);
                var valor2 = this.value.charAt(1).replace(/[^w]/i, '');
                this.value = valor+valor2;
            }
            if (this.value.charAt(2) != 'w' && this.value.charAt(2) != '') {
                var valor = this.value.charAt(0);
                var valor2 = this.value.charAt(1).replace(/[^w]/i, '');
                var valor3 = this.value.charAt(2).replace(/[^w]/i, '');
                this.value = valor+valor2+valor3;
            }
            var valor = this.value;
            if(valor=='www'){
                this.value = valor+'.';
            }
            if(tipo=='web') {
                this.value = this.value.replace(/[^A-Za-z0-9ñÑáéíóúÁÉÍÓÚ.]/i, '');
            }else if(tipo=='logo'){
                this.value = this.value.replace(/[^A-Za-z0-9ñÑáéíóúÁÉÍÓÚ.\/]/i, '');
            }
        });
    }
}