/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *
 *  * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        15-09-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/
var ModCdFunciones = function(){
    var f = this;


    /***
     * Descripcion: Json Select Centros de Costo
     * @param url
     * @param idDependencia
     */
    f.metJsonCentroCosto = function(url,idDependencia,idCentroCosto){
        function json(id){
            $('#num_depend_ext').html('');
            $('#num_depend_ext').append('<option value="">Seleccione Dependencia</option>');
            if(!id){
                id = $('#num_org_ext').val();
            }else{
                id = idDependencia;
            }
            $.post(url,{ idDependencia: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_ente']==idCentroCosto){
                        $('#num_depend_ext').append('<option selected value="'+dato[i]['pk_num_ente']+'">'+dato[i]['ind_nombre_ente']+'</option>');
                    }else{
                        $('#num_depend_ext').append('<option value="'+dato[i]['pk_num_ente']+'">'+dato[i]['ind_nombre_ente']+'</option>');
                    }
                }
            },'json');
        }
        if(idDependencia){
            json(idDependencia);
            $('#num_org_ext').change(function(){
                json(false);

            });
        }else{
            $('#num_org_ext').change(function(){
                json(false);

            });
        }
    }


    /***
     * Descripcion: Json Select
     * @param url
     * @param idPais
     * @param idEstado
     */
 
    f.metJsonEstadoD = function(url,idPais,idEstado){
        function json(id){
            $('#fk_a009_num_estado').html('');
            $('#fk_a009_num_estado').append('<option value="">Seleccione el Estado</option>');
            $('#s2id_fk_a009_num_estado .select2-chosen').html('Seleccione el Estado');
            if(!id){
                id = $('#fk_a008_num_pais').val();
            }else{
                id = idPais;

            }
            $.post(url,{ idPais: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_estado']==idEstado){
                        $('#fk_a009_num_estado').append('<option selected value="'+dato[i]['pk_num_estado']+'">'+dato[i]['ind_estado']+'</option>');
                        $('#s2id_fk_a009_num_estado .select2-chosen').html(dato[i]['ind_estado']);
                    }else{
                        $('#fk_a009_num_estado').append('<option value="'+dato[i]['pk_num_estado']+'">'+dato[i]['ind_estado']+'</option>');
                    }
                }
            },'json');
        }
        if(idPais){
            json(idPais);
            $('#fk_a008_num_pais').change(function(){
                json(false);
                $('#fk_a011_num_municipio').html('');
                $('#fk_a010_num_ciudad').html('');
            });
        }else{
            $('#fk_a008_num_pais').change(function(){
                json(false);
                $('#fk_a011_num_municipio').html('');
                $('#fk_a010_num_ciudad').html('');
            });
        }
    }




    /***
     * Descripcion: Json de Ciudad
     * @param url
     * @param idEstado
     * @param idCiudad
     */
    f.metJsonCiudadN = function(url,idEstado,idCiudad){
        function json(id){
            $('#fk_a010_num_ciudad').html('');
            $('#fk_a010_num_ciudad').append('<option value="">Seleccione la Ciudad</option>');
            $('#s2id_fk_a010_num_ciudad .select2-chosen').html('Seleccione la Ciudad');
            if(!id){
                id = $('#fk_a009_num_estado').val();
            }else{
                id = idEstado;
            }
            $.post(url,{ idEstado: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_ciudad']==idCiudad){
                        $('#fk_a010_num_ciudad').append('<option selected value="'+dato[i]['pk_num_ciudad']+'">'+dato[i]['ind_ciudad']+'</option>');
                        $('#s2id_fk_a010_num_ciudad .select2-chosen').html(dato[i]['ind_ciudad']);
                    }else{
                        $('#fk_a010_num_ciudad').append('<option value="'+dato[i]['pk_num_ciudad']+'">'+dato[i]['ind_ciudad']+'</option>');
                    }
                }
            },'json');
        }
        if(idEstado){
            json(idEstado);
            $('#fk_a009_num_estado').change(function(){
                json(false);
            });
        }else{
            $('#fk_a009_num_estado').change(function(){
                json(false);
            });
        }
    }

    /***
     * Descripcion: Json de Municipio
     * @param url
     * @param idEstado
     * @param idMunicipio
     */
    f.metJsonMunicipioN = function(url,idEstado,idMunicipio){
        function json(id){
            $('#fk_a011_num_municipio').html('');
            $('#fk_a011_num_municipio').append('<option value="">Seleccione el Municipio</option>');
            $('#s2id_fk_a011_num_municipio .select2-chosen').html('Seleccione el Municipio');
            if(!id){
                id = $('#fk_a009_num_estado').val();
            }else{
                id = idEstado;
            }

            $.post(url,{ idEstado: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['pk_num_municipio']==idMunicipio){
                        $('#fk_a011_num_municipio').append('<option selected value="'+dato[i]['pk_num_municipio']+'">'+dato[i]['ind_municipio']+'</option>');
                        $('#s2id_fk_a011_num_municipio2 .select2-chosen').html(dato[i]['ind_municipio']);
                    }else{
                        $('#fk_a011_num_municipio').append('<option value="'+dato[i]['pk_num_municipio']+'">'+dato[i]['ind_municipio']+'</option>');
                    }
                }
            },'json');
        }
        if(idEstado){
            json(idEstado);
            $('#fk_a009_num_estado').change(function(){
                json(false);
            });
        }else{
            $('#fk_a009_num_estado').change(function(){
                json(false);
            });
        }
    }

   


 

    /***
     * Descripcion: Burcar Persona
     * @param url
     * @param cedula
     */
	
    f.metBuscarPersona= function(url,cedula,id_persona){

        /*VERIFICO CUAL ES EL PARAMETRO ENVIADO PARA DETERMINAR QUE TIPO DE BUSQUEDA HACER*/

        if(!cedula){
            //BUSQUEDA POR CODIGO PERSONA
            $.post(url,{ parametro: id_persona, busqueda: 'PorIdPersona' } ,function(dato){

            },'json');

        }else{
            //BUSQUEDA POR CEDULA
            //URL: modRH/gestion/empleadosCONTROL/BuscarPersonaMET
            $.post(url,{ parametro: cedula, busqueda: 'PorCedula' } ,function(dato){

                if(dato==false){

                    $("#NombCompleto").val('');
                    $("#formAjax")[0].reset();
                    $("#apellido1").focus();
                    $("#cedula").val(cedula);
                    $("#accion").show();
                    $(".form-control").removeAttr("readonly");

                    var DocFiscal = new String();
                    DocFiscal = $("#cedula").val().trim();
                    $("#doc_fiscal").val(DocFiscal+"-");

                    //swal("Información!", "La Persona no Existe... Registre a la Persona", "info");

                }else{

                    if(dato['pk_num_empleado']>0){
                        //existe empleado
                        $(".form-control").attr("readonly","readonly");//desabilito el formulario
                        $("#cedula").removeAttr("readonly");//habilito nada mas que el campo cedula
                        $("#accion").hide();//oculto el boton de guardar
                        $("#siguiente").addClass('disabled');
                        $("#ultimo").addClass('disabled');

                    }else{
                        //existe la persona pero no es empleado
                        $("#accion").show();//muestro el boton de guardar
                        $(".form-control").removeAttr("readonly");//habilito todo el formulario si esta disabled

                    }

                    if(dato['fec_nacimiento']==null){
                        var fn = '00-00-0000';
                    }
                    else{
                        var fn = dato['fec_nacimiento'].split("-");
                    }

                    $("#empleado").val(dato['pk_num_empleado']);
                    $("#persona").val(dato['pk_num_persona']);
                    $("#nombre1").val(dato['ind_nombre1']);
                    $("#nombre2").val(dato['ind_nombre2']);
                    $("#apellido1").val(dato['ind_apellido1']);
                    $("#apellido2").val(dato['ind_apellido2']);
                    $("#NombCompleto").val(dato['ind_nombre1']+" "+dato['ind_nombre2']+" "+dato['ind_apellido1']+" "+dato['ind_apellido2']);
                    $("#fk_a006_num_miscelaneo_detalle_sexo").val(dato['fk_a006_num_miscelaneo_detalle_sexo']);
                    $("#ind_estatus_persona").val(dato['ind_estatus_persona']);
                    $("#ind_estatus").val(dato['ind_estatus']);
                    $("#fk_a006_num_miscelaneo_detalle_nacionalidad").val(dato['fk_a006_num_miscelaneo_detalle_nacionalidad']);
                    $("#doc_fiscal").val(dato['ind_documento_fiscal']);
                    $("#email").val(dato['ind_email']);
                    $("#fechaNacimiento").val(fn[2]+"-"+fn[1]+"-"+fn[0]);
                    $("#lugarNacimiento").val(dato['ind_lugar_nacimiento']);
                    $("#fk_a010_num_ciudad").val(dato['fk_a010_num_ciudad']);
                    $("#fk_a013_num_sector").val(dato['fk_a013_num_sector']);


                }



            },'json');

        }


    }

}