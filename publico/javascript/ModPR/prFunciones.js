/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Presupuesto
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Liduvica Bastardo                |        liduvica@hotmail.com        |         0424-9080200         |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        14-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

var PrFunciones = function(){
    var f = this;

    /**********
     * Descripcion: Funcion para Validar los errores del Formulario enviado desde el Controlador
     *
     * @param arrayDatos
     * @param mensaje
     **********/
	 
	 f.metJsonPresupuesto = function(url,anio,idPresupuesto){
        function json(id){
            $('#fk_prb004_num_presupuesto').html('');
            if(!id){
                id = $('#fec_anio').val();
            }else{
                id = anio;
            }
			
            $.post(url,{ anio: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
					
                    if(dato[i]['fk_prb004_num_presupuesto']==idPresupuesto){
                        $('#fk_prb004_num_presupuesto').append('<option selected value="'+dato[i]['pk_num_presupuesto']+'">'+dato[i]['ind_cod_presupuesto']+'</option>');
                    }else{
					    $('#fk_prb004_num_presupuesto').append('<option  value="'+dato[i]['pk_num_presupuesto']+'">'+dato[i]['ind_cod_presupuesto']+'</option>');
                    }
                }
            },'json');
        }
        if(anio){
            json(anio);
            $('#fec_anio').change(function(){
                json(false);

            });
        }else{
            $('#fec_anio').change(function(){
                json(false);

            });
        }
    }


   	 f.metJsonProyecto = function(url,sector,proyecto){
        function json(id){
            $('#fk_prb005_num_proyecto').html('');
            if(!id){
                id = $('#fk_prb008_num_sector').val();
            }else{
                id = sector;
            }
          $.post(url,{ sector: id } ,function(dato){
																	
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['fk_prb005_num_proyecto']==proyecto){
                        $('#fk_prb005_num_proyecto').append('<option selected value="'+dato[i]['pk_num_proyecto']+'">'+dato[i]['ind_cod_proyecto']+'-'+dato[i]['ind_descripcion']+'</option>');
                    }else{
					    $('#fk_prb005_num_proyecto').append('<option  value="'+dato[i]['pk_num_proyecto']+'">'+dato[i]['ind_cod_proyecto']+'-'+dato[i]['ind_descripcion']+'</option>');
                    }
                }
            },'json');
        }
        if(sector){
            json(sector);
            $('#fk_prb008_num_sector').change(function(){
                json(false);

            });
        }else{
            $('#fk_prb008_num_sector').change(function(){
                json(false);

            });
        }
    }


     f.metJsonSubPrograma = function(url,programa,subprograma){
        function json(id){
               $('#fk_prb010_num_subprograma').html('');
            if(!id){
                id = $('#fk_prb009_num_programa').val();
            }else{
                id = programa;
            }
          $.post(url,{ programa: id } ,function(dato){
																	
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['fk_prb010_num_subprograma']==programa){
                        $('#fk_prb010_num_subprograma').append('<option selected value="'+dato[i]['pk_num_subprograma']+'">'+dato[i]['ind_cod_subprograma']+'-'+dato[i]['ind_descripcion']+'</option>');
                    }else{
					    $('#fk_prb010_num_subprograma').append('<option  value="'+dato[i]['pk_num_subprograma']+'">'+dato[i]['ind_cod_subprograma']+'-'+dato[i]['ind_descripcion']+'</option>');
                    }
                }
            },'json');
        }
				
        if(programa){
            json(programa);
            $('#fk_prb009_num_programa').change(function(){
                json(false);

            });
        }else{
            $('#fk_prb009_num_programa').change(function(){
                json(false);

            });
			
        }
    }


      f.metJsonActividad = function(url,proyecto,actividad){
        function json(id){
               $('#fk_prb011_num_actividad').html('');
            if(!id){
                id = $('#fk_prb005_num_proyecto').val();
            }else{
                id = proyecto;
            }
          $.post(url,{ proyecto: id } ,function(dato){
																	
                for(var i=0;i<dato.length;i++){
                    if(dato[i]['fk_prb011_num_actividad']==actividad){
                        $('#fk_prb011_num_actividad').append('<option selected value="'+dato[i]['pk_num_actividad']+'">'+dato[i]['ind_cod_actividad']+'-'+dato[i]['ind_descripcion']+'</option>');
                    }else{
					    $('#fk_prb011_num_actividad').append('<option  value="'+dato[i]['pk_num_actividad']+'">'+dato[i]['ind_cod_actividad']+'-'+dato[i]['ind_descripcion']+'</option>');
                    }
                }
            },'json');
        }
        if(proyecto){
            json(proyecto);
            $('#fk_prb005_num_proyecto').change(function(){
                json(false);

            });
        }else{
            $('#fk_prb005_num_proyecto').change(function(){
                json(false);

            });
        }
    }


     f.metJsonIndicePresupuestario = function(url,tipo_presupuesto, tipo_categoria, indice_presupuestario){
        function json(id){
               $('#fk_prb016_indice_presupuestario').html('');
            if(!id){
                id = $('#ind_tipo_presupuesto').val();
				id_cat=  $('#ind_tipo_categoria').val();
            }else{
                id = tipo_presupuesto;
				id_cat= tipo_categoria;
            }
			

          $.post(url,{tipo_presupuesto: id, tipo_categoria: id_cat } ,function(dato){
																		  
				if 	(dato.length==0)	
				    $('#fk_prb016_indice_presupuestario').append('<option value="">'+" "+'</option>').change();
				  
                for(var i=0;i<dato.length;i++){
					
                    if(dato[i]['pk_num_indice_presupuestario']==indice_presupuestario){
                        $('#fk_prb016_indice_presupuestario').append('<option selected value="'+dato[i]['pk_num_indice_presupuestario']+'">'+dato[i]['ind_cod_indice_presupuestario']+'</option>');
                    }else{
					    $('#fk_prb016_indice_presupuestario').append('<option  value="'+dato[i]['pk_num_indice_presupuestario']+'">'+dato[i]['ind_cod_indice_presupuestario']+'</option>').change();
                    }
                }
            },'json');
        }
		
        if(tipo_presupuesto){
            json(tipo_presupuesto);
            $('#ind_tipo_presupuesto').change(function(){
                json(false);
            });
			
        }else{
            $('#ind_tipo_presupuesto').change(function(){
                json(false);

            });
        }
		
		 if(tipo_categoria){
            json(tipo_categoria);
            $('#ind_tipo_categoria').change(function(){
                json(false);
            });
        }else{
            $('#ind_tipo_categoria').change(function(){
                json(false);

            });
        }
		
    }

    
	 f.metJsonIndiceValores = function(url,indice){
        function json(id){
               $('#fk_prb008_num_sector').html('');
			   $('#fk_prb009_num_programa').html('');
			   $('#fk_prb010_num_subprograma').html('');
			   $('#fk_prb005_num_proyecto').html('');
			   $('#fk_prb011_num_actividad').html('');
			   $('#fk_prb012_unidad_ejecutora').html('');
            if(!id){
                id = $('#fk_prb016_indice_presupuestario').val();
            }else{
                id = indice;
            }
          $.post(url,{ indice: id } ,function(dato){
						
																	
                for(var i=0;i<dato.length;i++){
             
                        $('#fk_prb008_num_sector').append('<option selected value="'+dato[i]['pk_num_sector']+'">'+dato[i]['ind_cod_sector']+'-'+dato[i]['sector_descripcion']+'</option>');
						$('#fk_prb009_num_programa').append('<option selected value="'+dato[i]['pk_num_programa']+'">'+dato[i]['ind_cod_programa']+'-'+dato[i]['programa_descripcion']+'</option>');
						$('#fk_prb010_num_subprograma').append('<option selected value="'+dato[i]['pk_num_subprograma']+'">'+dato[i]['ind_cod_subprograma']+'-'+dato[i]['subprograma_descripcion']+'</option>');
						
						$('#fk_prb005_num_proyecto').append('<option selected value="'+dato[i]['pk_num_proyecto']+'">'+dato[i]['ind_cod_proyecto']+'-'+dato[i]['proyecto_descripcion']+'</option>');
						$('#fk_prb011_num_actividad').append('<option selected value="'+dato[i]['pk_num_actividad']+'">'+dato[i]['ind_cod_actividad']+'-'+dato[i]['actividad_descripcion']+'</option>');
						
						$('#fk_prb012_unidad_ejecutora').append('<option selected value="'+dato[i]['pk_num_unidad_ejecutora']+'">'+dato[i]['cod_unidad_ejecutora']+'-'+dato[i]['unidad_descripcion']+'</option>');
						
                  
                }
            },'json');
        }
        if(indice){
            json(indice);
            $('#fk_prb016_indice_presupuestario').change(function(){
                json(false);
            });
        }else{
            $('#fk_prb016_indice_presupuestario').change(function(){
                json(false);

            });
        }
    }



      f.metJsonPresupuestoReporte = function(url,anio){
        function json(id){
            $('#idpresupuesto').html('');
            if(!id){
                id = $('#fec_anio').val();
            }else{
                id = anio;
            }
			
            $.post(url,{ anio: id } ,function(dato){
                for(var i=0;i<dato.length;i++){
					
                  //  if(dato[i]['fk_prb004_num_presupuesto']==idPresupuesto){
                        $('#idpresupuesto').append('<option selected value="'+dato[i]['pk_num_presupuesto']+'">'+dato[i]['ind_cod_presupuesto']+'</option>');
                  /* }else{
					    $('#fk_prb004_num_presupuesto').append('<option  value="'+dato[i]['pk_num_presupuesto']+'">'+dato[i]['ind_cod_presupuesto']+'</option>');
                    }*/
                }
            },'json');
        }
        if(anio){
            json(anio);
            $('#fec_anio').change(function(){
                json(false);

            });
        }else{
            $('#fec_anio').change(function(){
                json(false);

            });
        }
    }


   
   f.metJsonCategoria = function(url,tipo_presupuesto){
        function json(id){
            $('#ind_tipo_categoria').html('');
            if(!id){
                id = $('#ind_tipo_presupuesto').val();
            }else{
                id = tipo_presupuesto;
            }
			
            $.post(url,{ tipo_presupuesto: id } ,function(dato){								  
              for(var i=0;i<dato.length;i++){
					
                     if(dato[i]['cod_detalle']=='O')
                        $('#ind_tipo_categoria').append('<option  value="'+dato[i]['cod_detalle']+'">'+dato[i]['ind_nombre_detalle']+'</option>');
                     else
					    $('#ind_tipo_categoria').append('<option value="'+dato[i]['cod_detalle']+'">'+dato[i]['ind_nombre_detalle']+'</option>').change();
                }  
            },'json');
        }
		
        if(tipo_presupuesto){
            json(tipo_presupuesto);
            $('#ind_tipo_presupuesto').change(function(){
                json(false);
            });
        }else{
            $('#ind_tipo_presupuesto').change(function(){
                json(false);

            });
        }
    }


    f.metJsonReformulacionReporte = function(url,presupuesto){
        function json(id){
            $('#idreformulacion').html('');
            if(!id){
                id = $('#idpresupuesto').val();
            }else{
                id = presupuesto;
            }
			
            $.post(url,{ presupuesto: id } ,function(dato){
													 					 
                for(var i=0;i<dato.length;i++){
					  
					  if (i==0)	
				           $('#idreformulacion').append('<option value="">'+" "+'</option>');
							  
                        $('#idreformulacion').append('<option selected value="'+dato[i]['pk_num_reformulacion_presupuesto']+'">'+dato[i]['cod_reformulacion']+'</option>');
                }
            },'json');
        }
        if(presupuesto){
            json(presupuesto);
            $('#idpresupuesto').change(function(){
                json(false);

            });
        }else{
            $('#idpresupuesto').change(function(){
                json(false);

            });
        }
    }


     f.metJsonAjusteReporte = function(url,presupuesto){
        function json(id){
            $('#idajuste').html('');
            if(!id){
                id = $('#idpresupuesto').val();
            }else{
                id = presupuesto;
            }
			
            $.post(url,{ presupuesto: id } ,function(dato){
													 					 
                for(var i=0;i<dato.length;i++){
					  
					  if (i==0)	
				           $('#idajuste').append('<option value="">'+" "+'</option>');
							  
                        $('#idajuste').append('<option selected value="'+dato[i]['pk_num_ajuste_presupuestario']+'">'+dato[i]['ind_cod_ajuste_presupuestario']+'</option>');
                }
            },'json');
        }
        if(presupuesto){
            json(presupuesto);
            $('#idpresupuesto').change(function(){
                json(false);

            });
        }else{
            $('#idpresupuesto').change(function(){
                json(false);

            });
        }
    }



     f.metValidarError = function(arrayDatos,mensaje){
        for (var item in arrayDatos) {
            if($.isArray(arrayDatos[item])){
                var array=arrayDatos[item];
                for (var itemArray in array){
                    if(array[itemArray]=='error'){
                        $(document.getElementById(item+itemArray+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                        $(document.getElementById(item+itemArray+'Error')).removeClass('has-success has-feedback');
                        $(document.getElementById(item+itemArray+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(item+itemArray+'Error')).addClass('has-error has-feedback');
                        $(document.getElementById(item+itemArray+'Error')).append('<span id="'+item+itemArray+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                        swal("Error!", mensaje, "error");
                    }else{
                        $(document.getElementById(item+itemArray+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(item+itemArray+'Error')).addClass('has-success has-feedback');
                        $(document.getElementById(item+itemArray+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                        $(document.getElementById(item+itemArray+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');

                    }
                }
            }else{
                if(arrayDatos[item]=='error'){
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
    };

    /*****
     * Descripcion: Funcion para actualizar los Datos de Una tabla al Modificar Un registro
     *
     * @param arrayDatos
     * @param id
     * @param nombreId
     * @param arrayCheck
     * @param arrayMostrarOrden
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metActualizarRegistroTabla = function(arrayDatos,id,nombreId,arrayCheck,arrayMostrarOrden,mensaje,idModalCerrar,idModalContenido){
        var html='';
        for (i = 0; i < arrayMostrarOrden.length; i++){
            if($.inArray(arrayMostrarOrden[i], arrayCheck)>=0){
                if(arrayDatos[arrayMostrarOrden[i]] ==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                html= html+'<td><i class="'+iconoFlag+'"></i></td>';
            }else{
                html= html+'<td> '+arrayDatos[arrayMostrarOrden[i]]+' </td>';
            }
        };
        html= html+'<td></td>';
        $(document.getElementById(nombreId+id)).html(html);
        swal("Registro Modificado!", mensaje, "success");
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    }

    /*****
     * Descripcion: Funcion para actualizar los Datos de Una tabla al Crear un nuevo Registro
     *
     * @param arrayDatos
     * @param id
     * @param nombreId
     * @param arrayCheck
     * @param arrayMostrarOrden
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metNuevoRegistroTabla = function(arrayDatos,id,nombreId,arrayCheck,arrayMostrarOrden,mensaje,idModalCerrar,idModalContenido,idTabla){
        var html='<tr id="'+nombreId+id+'">';
        for (i = 0; i < arrayMostrarOrden.length; i++){
            if($.inArray(arrayMostrarOrden[i], arrayCheck)>=0){
                if(arrayDatos[arrayMostrarOrden[i]] ==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                html= html+'<td><i class="'+iconoFlag+'"></i></td>';
            }else{
                html= html+'<td> '+arrayDatos[arrayMostrarOrden[i]]+' </td>';
            }
        };
        if(!idTabla){
            idTabla='datatable1';
        }
        html= html+'<td></td></tr>';
        $(document.getElementById(idTabla)).append(html);
        swal("Registro Guardado!", mensaje, "success");
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    }


    f.metAprobarRegistro = function(arrayDatos,id,idTabla,mensaje,idModalCerrar,idModalContenido){
        $(document.getElementById(idTabla+id)).remove();
        swal("Aprobado!", mensaje, "success");
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    };

    f._handleTabShow = function(tab, navigation, index, wizard){
        var total = navigation.find('li').length;
        var current = index + 0;
        var percent = (current / (total - 1)) * 100;
        var percentWidth = 100 - (100 / total) + '%';

        navigation.find('li').removeClass('done');
        navigation.find('li.active').prevAll().addClass('done');

        wizard.find('.progress-bar').css({width: percent + '%'});
        $('.form-wizard-horizontal').find('.progress').css({'width': percentWidth});
    };


    f.metWizard = function(){
        $.validator.setDefaults({highlight:function(element){$(element).closest('.form-group').addClass('has-error');},unhighlight:function(element){$(element).closest('.form-group').removeClass('has-error');},errorElement:'span',errorClass:'help-block',errorPlacement:function(error,element){if(element.parent('.input-group').length){error.insertAfter(element.parent());}
        else if(element.parent('label').length){error.insertAfter(element.parent());}
        else{error.insertAfter(element);}}});

        $('#rootWizard').bootstrapWizard({
            onTabShow: function(tab, navigation, index) {
                f._handleTabShow(tab, navigation, index, $('#rootWizard'));

            },
            onTabClick: function(tab, navigation, index) {

                var form = $('#rootWizard').find('.form-validation');
                var valid = form.valid();
                if(!valid) {
                    form.data('validator').focusInvalid();
                    return false;
                }
            },
            onNext: function(tab, navigation, index) {
                var form = $('#rootWizard').find('.form-validation');
                var valid = form.valid();
                if(!valid) {
                    form.data('validator').focusInvalid();
                    return false;
                }
            },
            onLast: function(tab, navigation, index) {
                var form = $('#rootWizard').find('.form-validation');
                var valid = form.valid();
                if(!valid) {
                    form.data('validator').focusInvalid();
                    return false;
                }
            }
        });
    }


    f.metCodeMirror = function(idTextArea){
        var cm = CodeMirror.fromTextArea(document.getElementById(idTextArea), {
            lineNumbers: true,
            matchBrackets: true,
            indentUnit: 4,
            indentWithTabs: true,
            extraKeys: {
                "Ctrl-Space": "autocomplete",
                "Ctrl-Q": function(cm){ cm.foldCode(cm.getCursor()); },
                "Alt-F": "findPersistent",
                "F11": function(cm) {
                    cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                },
                "Esc": function(cm) {
                    if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                }
            },
            mode: {name: "application/x-httpd-php", globalVars: true},
            lineWrapping: true,
            foldGutter: true,
            gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
        });
        return cm;
    }

    /***
     * Descripcion: Json de Estados
     * @param url
     * @param idPais
     * @param idEstado
     */
   
	
	 f.dataTable = function (idDt, url, idioma, columnas,filtrado) {
        var dt = $(idDt).dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": url,
                "type": "POST",
                "data": function ( d ) {
                    d.filtro = filtrado;
                }
            }, "language": {
                "processing":     "Procesando...",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "loadingRecords": "Cargando...",
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "columns": columnas
        });
        return dt;
    };

    /*****
     * Descripcion: Funcion para Aprobar los Datos de Una tabla al Crear un nuevo Registro
     *
     * @param idDataTabla
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metAprobarRegistroJson = function(idDataTabla,mensaje,idModalCerrar,idModalContenido){
        swal("Aprobado!", mensaje, "success");
        $('#'+idDataTabla).dataTable().api().row().remove().draw(false);
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    };


    /*****
     * Descripcion: Funcion para actualizar los Datos de Una tabla al Modificar Un registro
     *
     * @param idDataTabla
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metActualizarRegistroTablaJson = function(idDataTabla,mensaje,idModalCerrar,idModalContenido){
        swal("Registro Modificado!", mensaje, "success");
        $('#'+idDataTabla).dataTable().api().row().remove().draw(false);
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    };

    /*****
     * Descripcion: Funcion para actualizar los Datos de Una tabla al Crear un nuevo Registro
     *
     * @param idDataTabla
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metNuevoRegistroTablaJson = function(idDataTabla,mensaje,idModalCerrar,idModalContenido){
        swal("Registro Guardado!", mensaje, "success");
        $('#'+idDataTabla).dataTable().api().row().remove().draw(false);
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    };

    /*****
     * Descripcion: Funcion para Eliminar los Datos de Una tabla al Crear un nuevo Registro
     *
     * @param idDataTabla
     * @param mensaje
     * @param idModalCerrar
     * @param idModalContenido
     */
    f.metEliminarRegistroJson = function(idDataTabla,mensaje,idModalCerrar,idModalContenido){
        swal("Eliminado!", mensaje, "success");
        $('#'+idDataTabla).dataTable().api().row().remove().draw(false);
        $(document.getElementById(idModalCerrar)).click();
        $(document.getElementById(idModalContenido)).html('');
    };

}