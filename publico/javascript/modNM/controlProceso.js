/**
 * Created by munoz on 13/7/2016.
 */
$(document).ready(function() {
    var url=$('#datatable1').attr('url');
    $('#nuevo').click(function(){
        $('#formModalLabel').html($(this).attr('titulo'));
        $.post(url,{ idControlProceso:0, status:'pr' },function($dato){
            $('#ContenidoModal').html($dato);
        });
    });
    $('#datatable1 tbody').on( 'click', '.modificar', function () {
        $('#formModalLabel').html($(this).attr('titulo'));
        $.post(url,{ idControlProceso:$(this).attr('idControlProceso'), status:'pr' },function($dato){
            $('#ContenidoModal').html($dato);
        });
    });
    $('#datatable1 tbody').on( 'click', '.ver', function () {
        $('#formModalLabel').html($(this).attr('titulo'));
        $.post(url,{ idControlProceso:$(this).attr('idControlProceso'), ver:1, status:'pr' },function($dato){
            $('#ContenidoModal').html($dato);
        });
    });
    $('#datatable1 tbody').on( 'click', '.aprobar', function () {
        console.log('prueba');
        $('#formModalLabel').html($(this).attr('titulo'));
        $.post(url,{ idControlProceso:$(this).attr('idControlProceso'), ver:1, status:'AP' },function($dato){
            $('#ContenidoModal').html($dato);
        });
    });
    $('.cerrar').click(function(){
        $('#formModalLabel').html($(this).attr('titulo'));
        $.post(url,{ idControlProceso:$(this).attr('idControlProceso'), ver:1, status:'CR' },function($dato){
            $('#ContenidoModal').html($dato);
        });
    });
});