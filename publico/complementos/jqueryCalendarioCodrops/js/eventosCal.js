/*
* EVENTOS CALENDARIO
*/

if(!document.all) 
	{
		document.captureEvents(Event.MOUSEMOVE);
	}
document.onmousemove = leerCoordRaton;

var xCoord;//registramos la ultima coordenada leida en el centinela de coordenadasx
var yCoord;//registramos la ultima coordenada leida en el centinela de coordenadasx*/
var ventanaNuevo = null;

function ventanaNuevoEvento() 
{//..............................................
	
	if(ventanaNuevo != null)
	{
		ventanaNuevo.esconder();
	}
	
	//alert(xCoord+"-"+yCoord); 
	ventanaNuevo = new vEmergente
		(
		'contactoDenuncia',
		'Contacto directo con el Contralor del estado Sucre',
		xCoord, yCoord,
		600, 450,
		true, 
		true, 
		false, 
		true, 
		null, null, null, null
		);	
		
	capaContenidoVent = xGetElementById('contactoDenuncia');
	capaContenidoVent.style.overflow = "scroll";
	
	capaContenidoVent.className = 'ces_fuente';
	
	var contenido; 
	
	
	AjaxRequest.post
			(
				{
					'url':'nuevoEvento.php',
					
					'onSuccess': function(req)
									{
										
										capaContenidoVent.innerHTML = req.responseText;
										ventanaNuevo.mostrar();
										
									},
					'onError': function(req)
							{ 
								alert('Error!\nStatusText='+req.statusText+'\nContents='+req.responseText);
							}
				}
			);
			
	
	
	
	
		
		//ventanaEvento.esconder();
}//................................................


//permite listar todos los eventos de un dia especifico..............................
function listarEventos(fecha){

	var type = 'zoomin';

	$('.overlay-container').fadeIn(function() {
		window.setTimeout(function(){
		$('.window-container.'+type).addClass('window-container-visible');
		}, 100);
	});

	$("#capaFormulario").hide();
	$("#capaRegistros").show();

    	$("#registros").load("registros.php",{data:fecha}, function(response, status, xhr) {

        /*if (status == "error") {
            var msg = "Error!, algo ha sucedido: ";
            $("#capa").html(msg + xhr.status + " " + xhr.statusText);
        }*/

    	});	

	$(document).ready(function() {
    		$('#listadoEventos').dataTable();
	} );
	


	

}
//fin listarEventos()................................................................


function modificarEventos(id){

	$("#capaRegistros").hide();
	$("#capaFormulario").show();
	$("#operacion").val('editar');

    var url = "../controlador/evento.php";

    var opcion = "cargarEventosId";

    var parametros = "opcion="+opcion+"&id="+id;

    $.ajax({
        method: 'POST',
        url: url,
        data: parametros,
		dataType: 'json',
        async: true,
        statusCode: {
            404: function() {

                alert("El destino no se encuentra");
                return false;
            }
        },
        beforeSend: function() {



        },
        success: function(Request) {

	    var data = Request;

		$("#id_evento").val(data[0]['id_evento']);
		$("#txtFechaEvento").val(data[0]['fecha_evento']);
	    $("#txtHoraIni").val(data[0]['hora_inicio']);
	    $("#txtHoraFin").val(data[0]['hora_final']);
	    $("#horario1").val(data[0]['hora_ini_hor']);
	    $("#horario2").val(data[0]['hora_fin_hor']);
	    $("#txtDescripcion").val(data[0]['descripcion']);
	    $("#estado").val(data[0]['id_estado']);

            var elegido = $("#estado").val();

            $.post("dataselect.php", { elegido: elegido, tipo:'municipio' }, function(data){
            $("#municipio").html(data);
	    });
            $.post("dataselect.php", { elegido: elegido, muni:data[0]['id_municipio'], tipo:'municipioId' }, function(data){
            $("#municipio").html(data);
	    });

            $.post("dataselect.php", { elegido: data[0]['id_municipio'], parroq:data[0]['id_parroquia'], tipo:'parroquiaId' }, function(data){
            $("#parroquia").html(data);
            }); 
	   
	    $("#img").html('<img src="../eventos/'+data[0]['id_evento']+'" width="150" height="70">');

            $("#btnGuardarEvento").html('Modificar');

			$("#opcion").val('modificarEvento');


        },
    });


}

