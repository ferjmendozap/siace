<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">

            <div id="rootwizard2" class="form-wizard form-wizard-horizontal">

                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" autocomplete="off">

                    <div class="form-wizard-nav">
                        <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                        <ul class="nav nav-justified">
                            <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">DATOS GENERALES RETENCIÓN</span></a></li>
                            <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">CONCEPTOS DE NOMINA</span></a></li>
                        </ul>
                    </div><!--end .form-wizard-nav -->

                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idRetencion}" name="idRetencion" id="idRetencion"/>
                    <input type="hidden" value="{$proceso}" name="proceso" id="proceso"/>

                    <div class="tab-content clearfix">

                        <!-- datos genereales-->
                        <div class="tab-pane active" id="step1">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" tabindex="1" required data-msg-required="Seleccione Organismo">
                                        <option value="">Seleccione...</option>
                                        {foreach item=dat from=$listadoOrganismos}
                                            {if isset($formDB.fk_a001_num_organismo)}
                                                {if $dat.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                                    <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fk_a006_num_miscelaneo_detalle">Organismo</label>
                                    <p class="help-block"><span class="text-sm text-bold" style="color: red">*</span></p>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group control-width-normal">
                                    <div class="input-group date" id="fecha_ini">
                                        <div class="input-group-content">
                                            <input type="text" class="form-control input-sm" id="fec_fecha_resolucion" name="form[txt][fec_fecha_resolucion]" value="{if isset($formDB.fec_fecha_resolucion)}{$formDB.fec_fecha_resolucion|date_format:"d-m-Y"}{else}{$fecha_resolucion}{/if}" required data-msg-required="Indique Fecha Resolución">
                                            <label>Fecha Resolución</label>
                                            <p class="help-block"><span class="text-sm text-bold" style="color: red">*</span></p>
                                        </div>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div><!--end .form-group -->
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-content">
                                            <input type="text" class="form-control input-sm" id="nombreTrabajador" value="{if isset($formDB.nombre_empleado)}{$formDB.nombre_empleado}{/if}" disabled>
                                            <input type="hidden" name="form[int][fk_rhb001_num_empleado]" id="fk_rhb001_num_empleado" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{/if}"  disabled>
                                            <label for="nombreTrabajador">Empleado</label>
                                            <p class="help-block"><span class="text-sm text-bold" style="color: red">*</span></p>
                                        </div>
                                        <div class="input-group-btn">
                                            <button class="btn btn-sm btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleados" >Buscar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-content">
                                            <input type="text" class="form-control input-sm" id="nombrePersona" value="{if isset($formDB.nombre_demandante)}{$formDB.nombre_demandante}{/if}" disabled>
                                            <input type="hidden" name="form[int][fk_a003_num_persona_demandante]" id="fk_a003_num_persona_demandante" value="{if isset($formDB.fk_a003_num_persona_demandante)}{$formDB.fk_a003_num_persona_demandante}{/if}"  disabled>
                                            <label for="nombrePersona">Demandante</label>
                                            <p class="help-block"><span class="text-sm text-bold" style="color: red">*</span></p>
                                        </div>
                                        <div class="input-group-btn">
                                            <button class="btn btn-sm btn-default" type="button" id="buscarPersona" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Personas" >Buscar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <select id="fk_a006_num_miscelaneo_detalle_tiporetencion" name="form[int][fk_a006_num_miscelaneo_detalle_tiporetencion]" class="form-control input-sm" required>
                                        <option value="">Seleccione...</option>
                                        {foreach item=dat from=$miscelaneoTipoRetencion}
                                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_tiporetencion)}
                                                {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tiporetencion}
                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fk_a006_num_miscelaneo_detalle_tiporetencion">Tipo de Retención</label>
                                    <p class="help-block"><span class="text-sm text-bold" style="color: red">*</span></p>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <select id="fk_a006_num_miscelaneo_detalle_tipopago" name="form[int][fk_a006_num_miscelaneo_detalle_tipopago]" class="form-control input-sm" required>
                                        <option value="">Seleccione...</option>
                                        {foreach item=dat from=$miscelaneoTipoPago}
                                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipopago)}
                                                {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipopago}
                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fk_a006_num_miscelaneo_detalle_tipopago">Tipo de Pago</label>
                                    <p class="help-block"><span class="text-sm text-bold" style="color: red">*</span></p>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_expediente)}{$formDB.ind_expediente}{/if}" name="form[txt][ind_expediente]" id="ind_expediente" required data-msg-required="Ingrese Numero de Expediente" style="text-transform: uppercase"  autocomplete="off">
                                    <label for="ind_expediente">Expediente</label>
                                    <p class="help-block"><span class="text-sm text-bold" style="color: red">*</span></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_juzgado)}{$formDB.ind_juzgado}{/if}" name="form[txt][ind_juzgado]" id="ind_juzgado" required data-msg-required="Introduzca Juzgado" style="text-transform: uppercase" autocomplete="off">
                                    <label for="ind_juzgado">Juzgado</label>
                                    <p class="help-block"><span class="text-sm text-bold" style="color: red">*</span></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="form[txt][txt_observaciones]" id="txt_observaciones" class="form-control input-sm" rows="3" placeholder="" style="text-transform: uppercase">{if isset($formDB.txt_observaciones)}{$formDB.txt_observaciones}{/if}</textarea>
                                    <label for="txt_observaciones">Observaciones Generales</label>
                                </div>
                            </div>


                            <div class="col-xs-3">
                                <div class="checkbox checkbox-styled">
                                    <label>
                                        <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" name="form[int][num_estatus]">
                                        <span>Estatus</span>
                                    </label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p class="text-xs text-bold text-right">LOS CAMPOS MARCADOS CON ASTERISCOS <span class="text-lg text-bold" style="color: red"> ( * ) </span>SON OBLIGATORIOS</p>
                                    </div>
                                </div>
                            </div>



                        </div><!-- fin datos generales -->

                        <!-- nacimiento -->
                        <div class="tab-pane" id="step2">


                            <!-- CONCEPTOS DE NOMINA -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p class="text-xs text-bold text-left">INDIQUE LOS CONCEPTOS DE NOMINA QUE AFECTAN LA RETENCIÓN JUDICIAL</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body no-padding">
                                            <div class="table-responsive no-margin">
                                                <table class="table table-striped no-margin" id="contenidoTablaConceptos">
                                                    <thead>
                                                    <tr>
                                                        <th class="text-center">#</th>
                                                        <th class="col-sm-2">Concepto</th>
                                                        <th class="col-sm-6">Descripción</th>
                                                        <th class="col-sm-2">Tipo</th>
                                                        <th class="col-sm-2">Descuento</th>
                                                        <th>...</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {if isset($formDBconceptos)}
                                                        {foreach item=datos from=$formDBconceptos}
                                                            <tr id="con{$numero}" idCon="{$datos.pk_num_retencion_conceptos}">
                                                                <input type="hidden" value="{$datos.pk_num_retencion_conceptos}" name="form[int][pk_num_retencion_conceptos][{$n}]" id="pk_num_retencion_conceptos{$numero}">
                                                                <td class="text-right" style="vertical-align: middle;">
                                                                    {$numero}
                                                                </td>
                                                                <td style="vertical-align: middle;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <div class="input-group-content">
                                                                                    <input type="text" class="form-control input-sm" value="{$datos.fk_nmb002_num_concepto}" name="form[int][fk_nmb002_num_concepto][{$n}]" id="fk_nmb002_num_concepto{$n}" required autocomplete="off">
                                                                                    <label for="fk_nmb002_num_concepto{$n}">Concepto {$n}</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td style="vertical-align: middle;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" value="{$datos.ind_concepto}" name="form[txt][ind_concepto][{$n}]" id="ind_concepto{$n}" required>
                                                                            <label for="ind_concepto{$n}"><i class="md md-insert-comment"></i> Descripción {$n}</label>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td style="vertical-align: middle;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_miscelaneo_detalle_tipodescuento{$n}" name="form[int][fk_a006_num_miscelaneo_detalle_tipodescuento][{$n}]" class="form-control input-sm" required data-msg-required="Seleccione Tipo Descuento">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$miscelaneoTipoDescuento}
                                                                                    {if isset($datos.fk_a006_num_miscelaneo_detalle_tipodescuento)}
                                                                                         {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_tipodescuento}
                                                                                             <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                          {else}
                                                                                             <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                          {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td style="vertical-align: middle;">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" value="{$datos.num_descuento|number_format:2:",":"."}" name="form[int][num_descuento][{$n}]" id="num_descuento{$n}" required autocomplete="off">
                                                                            <label for="num_descuento{$n}">Descuento {$n}</label>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td style="vertical-align: middle;">
                                                                    <div class="col-sm-12">
                                                                        <button type="button" class="eliminarBD btn ink-reaction btn-raised btn-xs btn-danger" id="con{$numero}" idCon="{$datos.pk_num_retencion_conceptos}" boton="si, Eliminar" descripcion="Ha Eliminado el Concepto" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Concepto de Nómina Seleccionado!!"><i class="md md-delete" style="color: #ffffff;"></i></button>
                                                                    </div>
                                                                </td>
                                                                <input type="hidden" value="{$n++}">
                                                                <input type="hidden" value="{$numero++}">

                                                            </tr>
                                                        {/foreach}
                                                    {/if}
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="6">
                                                            <div class="col-sm-12 text-center">
                                                                <button type="button" id="nuevoConcepto" class="btn btn-sm btn-info ink-reaction btn-raised">
                                                                    <i class="md md-add"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div>

                            <!-- FIN CONCEPTOS DE NOMINA -->

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div align="right">
                                    <button type="button" id="btnCerrar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                    <button type="submit" id="accionNuevaRetencion" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                    <ul class="pager wizard">
                        <li class="previous first" id="primero"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                        <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                        <li class="next last" id="ultimo"><a class="ink-reaction btn-raised" href="javascript:void(0);">Ultimo</a></li>
                        <li class="next" id="siguiente"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                    </ul>

                </form>
            </div>
        </div>
    </div>
</div>


<!-- </form> -->

<script type="text/javascript">

    //inicializar el datepicker
    $("#fec_fecha_resolucion").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();

            $.post('{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/NuevaRetencionMET', datos ,function(dato){

                if(dato['status']=='error'){
                    /*Error para usuarios normales del sistema*/
                    swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    //swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");
                }else if(dato['status']=='modificar'){

                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $("#_contenido").load('{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL');
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                }else if(dato['status']=='creacion'){

                    swal("Registro Guardado!", 'Registro Guardado Satisfactoriamente', "success");
                    $("#_contenido").load('{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL');
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');


                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho').css( "width", "75%" );

        //Script para forzar el avance del wizard por el botón suigiente.
        $(".form-wizard-nav").on('click', function(){
            var form = $('#rootwizard2').find('.form-validation');
            var valid = form.valid();
            if(!valid) {
                form.data('validator').focusInvalid();
                return false;
            }
        });

        //verifico el proceso que estoy ejecutando
        //NUEVO - MODIFICAR - VER
        var proceso = $("#proceso").val();
        if(proceso=='VER'){
            //desabilito todo el formulario
            $(".form-control").attr("disabled","disabled");
            //oculto los botones GUARDAR - BUSCAR - ELIMINAR
            $("#accionNuevaRetencion").hide();
            $('#buscarEmpleado').hide();
            $('#buscarPersona').hide();
            $('.eliminarBD').hide();
            $('#btnCerrar').html('<span class="glyphicon glyphicon-floppy-remove"></span> Cerrar');
        }

        $("#accionNuevaRetencion").click(function(){
            var num_filas = $('#contenidoTablaConceptos >tbody >tr').length;
            var filas = $('#contenidoTablaConceptos >tbody >tr'); {*trae las filas del tbody*}
            if(num_filas==0) {
                swal({  {*Mensaje personalizado*}
                    title: 'Error',
                    text: 'Por lo menos debe registrar un Concepto',
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Aceptar',
                    closeOnConfirm: true
                });
                return false;
            }

            if($("#fk_rhb001_num_empleado").val()=='' || $("#fk_a003_num_persona_demandante").val()==''){
                swal({  {*Mensaje personalizado*}
                    title: 'Error',
                    text: 'Asegurese de haber seleccionado el  Empleado y  Demandante',
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Aceptar',
                    closeOnConfirm: true
                });
                return false;
            }

        });


        var $url = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/ConsultarEmpleadoMET';
        $('#buscarEmpleado').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', tipo_busqueda:'ACTIVOS', accion:'RETENCION_JUDICIAL' },function($dato){
                $('#ContenidoModal2').html($dato);
                $('#modalAncho2').css( "width", "70%" );
            });
        });

        var $urle = '{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/ConsultarPersonaMET';
        $('#buscarPersona').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($urle, { proceso: 'Nuevo', accion:'RETENCION_JUDICIAL' },function($dato){
                $('#ContenidoModal2').html($dato);

            });
        });

        $('#contenidoTablaConceptos  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });

        $("#nuevoConcepto").click(function() {

            var idtabla= $("#contenidoTablaConceptos");
            var tr= $("#contenidoTablaConceptos > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;

            var eliminar = '<button class="eliminarTMP btn ink-reaction btn-raised btn-xs btn-danger" idCon="con'+numero+'"  boton="si, Eliminar" descripcion="Ha Eliminado la linea" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Linea!!"><i class="md md-delete" style="color: #ffffff;"></i></button>';

            var concepto='<div class="form-group">'+
                    '<div class="input-group">'+
                    '<div class="input-group-content">'+
                    '<input type="text" class="form-control input-sm" name="form[int][fk_nmb002_num_concepto]['+nuevoTr+']" id="fk_nmb002_num_concepto'+nuevoTr+'" linea="'+nuevoTr+'" required style="text-transform: uppercase"  autocomplete="off">'+
                    '<label for="fk_nmb002_num_concepto'+nuevoTr+'">Concepto '+numero+'</label>'+
                    '</div>'+
                    '<div class="input-group-btn">'+
                    '<button class="buscarConcepto btn btn-xs btn-info" type="button" id="buscarConcepto" linea="'+nuevoTr+'"  data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Conceptos" title="Listar Conceptos"><i class="md md-search" style="color: #ffffff;"></button>'+
                    '</div>'+
                    '</div>'+
                    '</div>';
            var descripcion='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" value="" name="form[txt][ind_concepto]['+nuevoTr+']" id="ind_concepto'+nuevoTr+'" required>'+
                    '<label for="ind_concepto'+nuevoTr+'"><i class="md md-insert-comment"></i> Descripción '+numero+'</label>'+
                    '</div>';
            var tipo='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_tipodescuento'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_tipodescuento]['+nuevoTr+']" class="form-control input-sm" required data-msg-required="Seleccione Tipo Descuento">'+
                    '<option value="">Seleccione...</option>'+
                    '{foreach item=dat from=$miscelaneoTipoDescuento}'+
                    '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                    '{/foreach}'+
                    '</select>'+
                    '</div>';
            var descuento='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" name="form[int][num_descuento]['+nuevoTr+']" id="num_descuento'+nuevoTr+'" required style="text-transform: uppercase"  autocomplete="off">'+
                    '<label for="num_descuento'+nuevoTr+'">Descuento '+numero+'</label>'+
                    '</div>';
            idtabla.append('<tr id=con'+numero+'>'+
                    '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+concepto+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+descripcion+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+tipo+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+descuento+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+eliminar+'</div></td>'+
                    '</tr>');

            $("#num_descuento"+nuevoTr).maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });


        });

        $('#contenidoTablaConceptos tbody').on( 'click', '.eliminarTMP', function () {
            var id=$(this).attr('idCon');
            $(document.getElementById(id)).remove();
        });

        $('#contenidoTablaConceptos tbody').on( 'click', '.buscarConcepto', function () {
            var $url = '{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/ListarConceptosMET';
             $('#formModalLabel2').html($(this).attr('titulo'));
             $.post($url, { proceso: 'Nuevo' , linea: $(this).attr('linea') },function($dato){
                $('#ContenidoModal2').html($dato);
             });
        });

        //ELIMINAR CONCEPTOS DE LA RETENCION DE LA BASE DE DATOS
        $('#contenidoTablaConceptos tbody').on( 'click', '.eliminarBD', function () {

            var id = $(this).attr('idCon');
            var linea = $(this).attr('id');

            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/EliminarConceptoDetalleMET';
                $.post($url, { id: id },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById(linea)).remove();
                        $("#pk_num_retencion_conceptos"+linea).val("");
                        swal("Eliminado!", "Concepto Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });



    });






</script>