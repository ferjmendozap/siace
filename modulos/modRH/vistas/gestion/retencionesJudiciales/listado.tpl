
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Retenciones Judiciales</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card">

                        <div class="card-head">
                            <div class="tools">
                                <div class="btn-group">
                                    <a data-toggle="offcanvas" id="filtro" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Expediente</th>
                                    <th>Fecha Resolución</th>
                                    <th>Empleado</th>
                                    <th>Juzgado</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=ret from=$listadoRetenciones}

                                    <tr id="idRetencion{$ret.pk_num_retencion}">
                                        <td>{$ret.pk_num_retencion}</td>
                                        <td>{$ret.ind_expediente}</td>
                                        <td>{$ret.fec_fecha_resolucion|date_format:"d-m-Y"}</td>
                                        <td>{$ret.nombre_empleado}</td>
                                        <td>{$ret.ind_juzgado}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-01-08-03-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idRetencion="{$ret.pk_num_retencion}"
                                                        descripcion="El Usuario a Modificado una Retención Judicial" titulo="Modificar Retención Judicial">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-01-08-04-V',$_Parametros.perfil)}
                                                <button class="ver btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Ver" idRetencion="{$ret.pk_num_retencion}" descripcion="El Usuario a Visualizado una Retención Judicial" titulo="Ver Retención Judicial">
                                                    <i class="md md-search" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="6">
                                        {if in_array('RH-01-01-08-02-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado una Retención Judicial" title="Nueva Retención Judicial" titulo="Nueva Retención Judicial" id="nuevo" >
                                                    <i class="md md-create"></i> Nueva Retención &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- filtro de busqueda de las retenciones-->
<div class="offcanvas">

    <div id="offcanvas-filtro" class="offcanvas-pane width-10">

        <div class="offcanvas-head">
            <header>Filtro de Busqueda</header>
            <div class="offcanvas-tools">
                <a class="cerrar btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form id="filtroRetenciones" class="form" role="form" method="post">
                <input type="hidden" name="filtro" value="1">

                <!--ORGANISMO-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" required data-msg-required="Seleccione Organismo">
                                {foreach item=dat from=$listadoOrganismos}
                                    {if isset($formDB.fk_a001_num_organismo)}
                                        {if $dat.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                            <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                        {else}
                                            <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                        {/if}
                                    {else}
                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_a006_num_miscelaneo_detalle">Organismo</label>
                        </div>
                    </div>
                </div>
                <!--BUSCAR-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm" name="form[txt][buscar]" style="text-transform: uppercase" id="buscar" autocomplete="off">
                            <label for="buscar">Buscar</label>
                        </div>
                    </div>
                </div>
                <!--ESTADO-->
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <select id="Estado" name="Estado" class="form-control input-sm">
                                {foreach key=key item=item from=$EstadoRegistro}

                                    {if $key==$EstadoPorDefecto}
                                        <option selected value="{$key}">{$item}</option>
                                    {else}
                                        <option value="{$key}">{$item}</option>
                                    {/if}

                                {/foreach}
                            </select>
                            <label for="Estado">Estado</label>
                        </div>
                    </div>
                </div>
                <!--FECHA INGRESO-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-daterange input-group" id="rango-fechas">
                                <div class="input-group-content">
                                    <input type="text" class="form-control" name="fecha_inicio" id="fecha_inicio" />
                                </div>
                                <span class="input-group-addon">a</span>
                                <div class="input-group-content">
                                    <input type="text" class="form-control" name="fecha_fin" id="fecha_fin"/>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                            <label for="">Fecha de Resolcuión</label>
                        </div>
                    </div>
                </div>
                <!--BOTON-->
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltroRetenciones">
                            Filtrar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        $("#fecha_inicio").datepicker({
            todayHighlight: true,
            format:'dd-mm-yyyy',
            autoclose: true,
            language:'es'
        });
        $("#fecha_fin").datepicker({
            todayHighlight: true,
            format:'dd-mm-yyyy',
            autoclose: true,
            language:'es'
        });

        var $url='{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/NuevaRetencionMET';

        //EVENTO PARA REGISTRAR UNA NUEVA RETENCION
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ proceso: 'NUEVO' },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //EVENTO PARA MODIFICAR UNA NUEVA RETENCION
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idRetencion: $(this).attr('idRetencion'), proceso: 'MODIFICAR' },function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });

        //EVENTO PARA VISUALIZAR UNA NUEVA RETENCION
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idRetencion: $(this).attr('idRetencion'), proceso: 'VER' },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#filtro').click(function(){
            $(".form-control").removeAttr("disabled");
        });

        //ACCION DEL FILTRO
        $("#filtroRetenciones").validate({
            submitHandler: function(form) {
                var pk_num_organismo = $("#fk_a001_num_organismo").val();
                var buscar = $("#buscar").val();
                var estado_registro = $("#Estado").val();
                var fecha_inicio = $("#fecha_inicio").val();
                var fecha_fin = $("#fecha_fin").val();

                var url_listar='{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/BuscarRetencionesMET';
                $.post(url_listar,{ pk_num_organismo: pk_num_organismo, buscar: buscar,  estado_registro: estado_registro, fecha_inicio: fecha_inicio, fecha_fin: fecha_fin },function(respuesta_post) {
                    var tabla_listado = $('#datatable1').DataTable();
                    tabla_listado.clear().draw();
                    if(respuesta_post != -1) {
                        for(var i=0; i<respuesta_post.length; i++) {


                            var aux = respuesta_post[i].fec_fecha_resolucion;

                            var fecha = aux.split("-");

                            var modificar ='<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" title="Editar" idRetencion="'+respuesta_post[i].pk_num_retencion+'" descripcion="El Usuario a Modificado una Retención Judicial" titulo="Modificar Retención Judicial"><i class="fa fa-edit" style="color: #ffffff;"></i></button>';

                            var ver  = '<button class="ver btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" title="Ver" idRetencion="'+respuesta_post[i].pk_num_retencion+'" descripcion="El Usuario a Visualizado una Retención Judicial" titulo="Ver Retención Judicial"><i class="md md-search" style="color: #ffffff;"></i></button>';


                            var fila = tabla_listado.row.add([
                                respuesta_post[i].pk_num_retencion,
                                respuesta_post[i].ind_expediente,
                                fecha[2]+"-"+fecha[1]+"-"+fecha[0],
                                respuesta_post[i].nombre_empleado,
                                respuesta_post[i].ind_juzgado,
                                modificar+" "+ver
                            ]).draw()
                                    .nodes()
                                    .to$()
                            $(fila)
                                    .attr('id','idRetencion'+respuesta_post[i].pk_num_retencion+'')

                        }
                        $('#filtroRetenciones').each(function () { {*resetear los campos del formulario*}
                            this.reset();
                        });
                        $(".cerrar").click();
                    }

                },'json');
            }
        });



    });
</script>