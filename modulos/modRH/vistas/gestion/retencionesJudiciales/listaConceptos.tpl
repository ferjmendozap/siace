<div class="modal-body">


    <table id="datatableConceptos" class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="col-sm-1">Id</th>
            <th class="col-sm-10">Concepto</th>
            <th class="col-sm-1">Tipo</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        {foreach item=f from=$conceptos}



            <tr id="idConcepto{$f.pk_num_concepto}">
                <td>{$f.pk_num_concepto}</td>
                <td>{$f.ind_descripcion}</td>
                <td>{$f.ind_nombre_detalle}</td>
                <td>
                    <button type="button" idConcepto="{$f.pk_num_concepto}" nombreConcepto="{$f.ind_descripcion}" class="cargarConcepto btn ink-reaction btn-raised btn-xs btn-primary"  title="Cargar Concepto"><i class="glyphicon glyphicon-download"></i></button>
                </td>
            </tr>




        {/foreach}
        </tbody>

    </table>

    <div class="row">
        <div align="center">
            <button type="button" class="btn btn-default ink-reaction btn-raised" descripcionModal="Aceptar" data-dismiss="modal">Aceptar</button>
        </div>
    </div>

</div>


<script type="text/javascript">

    $(document).ready(function() {

        $('#modalAncho2').css( "width", "50%" );

        $('#datatableConceptos').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatableConceptos tbody').on( 'click', '.cargarConcepto', function () {

            var idConcepto = ($(this).attr('idConcepto'));
            var nombreConcepto   = ($(this).attr('nombreConcepto'));


            $("#fk_nmb002_num_concepto"+{$linea}).val(idConcepto);
            $("#ind_concepto"+{$linea}).val(nombreConcepto);

            /***************************************************************************************/
            $(document.getElementById('cerrarModal2')).click();
            $(document.getElementById('ContenidoModal2')).html('');
            /***************************************************************************************/


        });

    });


</script>