<div class="modal-body">


    <table id="datatablePersonas" class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="col-sm-1">Cedula</th>
            <th class="col-sm-5">Nombres</th>
            <th class="col-sm-6">Apellidos</th>
        </tr>
        </thead>
        <tbody>
        {foreach item=f from=$personas}



            <tr id="idConcepto{$f.pk_num_persona}" idPersona="{$f.pk_num_persona}" nombrePersona="{$f.ind_nombre1} {$f.ind_nombre2} {$f.ind_apellido1} {$f.ind_apellido2}">
                <td>{$f.ind_cedula_documento}</td>
                <td>{$f.ind_nombre1} {$f.ind_nombre2}</td>
                <td>{$f.ind_apellido1} {$f.ind_apellido2}</td>
            </tr>




        {/foreach}
        </tbody>

    </table>

    <div class="row">
        <div align="center">
            <button type="button" class="btn btn-default ink-reaction btn-raised" descripcionModal="Aceptar" data-dismiss="modal">Aceptar</button>
        </div>
    </div>

</div>

<style type="text/css">
    tr{
        cursor: pointer;
    }
</style>
<script type="text/javascript">

    $(document).ready(function() {

        $('#modalAncho2').css( "width", "50%" );

        $('#datatablePersonas').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatablePersonas tbody').on( 'click', 'tr', function () {

            var idPersona = ($(this).attr('idPersona'));
            var nombrePersona   = ($(this).attr('nombrePersona'));


            $("#fk_a003_num_persona_demandante").val(idPersona);
            $("#nombrePersona").val(nombrePersona);

            /***************************************************************************************/
            $(document.getElementById('cerrarModal2')).click();
            $(document.getElementById('ContenidoModal2')).html('');
            /***************************************************************************************/


        });

    });


</script>