<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idMeritoDemerito}" name="idMeritoDemerito"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>


                                <div class="col-xs-10" style="margin-top: -10px">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select id="fk_a006_num_miscelaneo_detalle_tipo" name="form[int][fk_a006_num_miscelaneo_detalle_tipo]" class="form-control input-sm" required data-msg-required="Seleccione Tipo">
                                                    <option value="">Seleccione...</option>
                                                    {foreach item=dat from=$TipoMerDem}
                                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipo)}
                                                            {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipo}
                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {else}
                                                                <option value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {else}
                                                            <option value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {/foreach}
                                                </select>
                                                <label for="fk_a006_num_miscelaneo_detalle_tipo">Tipo</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div>
                                        <input type="hidden" value="{if isset($formDB.ind_tipo)}{$formDB.ind_tipo}{/if}" id="ind_tipo" name="form[txt][ind_tipo]"/>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select id="fk_a006_num_miscelaneo_detalle_meritodemerito" name="form[int][fk_a006_num_miscelaneo_detalle_meritodemerito]" class="form-control input-sm" required data-msg-required="Seleccione el Merito o Demerito">
                                                    <option value="">...</option>
                                                    {foreach item=dat from=$MeritoDemerito}
                                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_meritodemerito)}
                                                            {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_meritodemerito}
                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {else}
                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {else}
                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {/foreach}
                                                </select>
                                                <label for="fk_a006_num_miscelaneo_detalle_tipocta">Meritos/Demeritos</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div>
                                    </div><!--end .row -->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="ind_documento" name="form[txt][ind_documento]" value="{if isset($formDB.ind_documento)}{$formDB.ind_documento}{/if}" required data-msg-required="Indique Documento" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                <label for="ind_documento">Documento</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div><!--end .col -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <div class="input-group date" id="fecha">
                                                    <div class="input-group-content">
                                                        <input type="text" class="form-control input-sm" name="form[txt][fec_documento]" id="fec_documento" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento|date_format:"d-m-Y"}{/if}" required data-msg-required="Indique Fecha de Documento" readonly>
                                                        <label for="fec_documento">Fecha Documento</label>
                                                    </div>
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-content">
                                                        <input type="text" class="form-control input-sm" id="nombreTrabajador" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1} {$formDB.ind_apellido1}{/if}">
                                                        <input type="hidden" name="form[int][fk_rhb001_num_empleado_responsable]" id="fk_rhb001_num_empleado_responsable" value="{if isset($formDB.fk_rhb001_num_empleado_responsable)}{$formDB.fk_rhb001_num_empleado_responsable}{/if}">
                                                        <label for="nombreTrabajador">Responsable</label>
                                                    </div>
                                                    <div class="input-group-btn">
                                                        <button class="btn btn-sm btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal3" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleados" >Buscar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="checkbox checkbox-styled">
                                                <label>
                                                    <input type="checkbox"  id="num_flag_responsable" checked>
                                                </label>
                                            </div>
                                        </div><!--end .col -->
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="ind_persona_externa" name="form[txt][ind_persona_externa]" value="{if isset($formDB.ind_persona_externa)}{$formDB.ind_persona_externa}{/if}" disabled>
                                                <label for="ind_persona_externa">Persona Externa</label>
                                            </div>
                                        </div><!--end .col -->
                                        <div class="col-md-1">
                                            <div class="checkbox checkbox-styled">
                                                <label>
                                                    <input type="checkbox" {if isset($formDB.num_flag_externo) and $formDB.num_flag_externo==1} checked{/if} value="1" id="num_flag_externo" name="form[int][num_flag_externo]">
                                                </label>
                                            </div>
                                        </div><!--end .col -->
                                    </div><!--end .row -->

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea name="form[txt][txt_observaciones]" id="txt_observaciones" class="form-control input-sm" rows="3" placeholder="">{if isset($formDB.txt_observaciones)}{$formDB.txt_observaciones}{/if}</textarea>
                                                <label for="txt_observaciones">Observaciones</label>
                                            </div>
                                        </div><!--end .col -->
                                    </div>

                                    <div class="row">
                                        <br><br>
                                        <div align="right">
                                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> Cancelar</button>
                                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="guardar"><span class="glyphicon glyphicon-floppy-disk"> Guardar</button>
                                        </div>
                                    </div>

                                </div>
                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/meritosDemeritosCONTROL/MeritosDemeritosMET', datos ,function(dato){

                if(dato['status']=='modificar'){

                    $(document.getElementById('idMeritoDemerito'+dato['idMeritoDemerito'])).html('<td width="20%">'+dato['tipo_merdem']+'</td>' +
                            '<td width="20%">'+dato['merdem']+'</td>' +
                            '<td width="10%">'+dato['fec_documento']+'</td>' +
                            '<td width="15%">'+dato['ind_documento']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idMeritoDemerito="'+dato['idMeritoDemerito']+'"' +
                            'descipcion="El Usuario a Modificado Meritos Demeritos" titulo="Modificar Meritos Demeritos">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMeritoDemerito="'+dato['idMeritoDemerito']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Meritos Demeritos" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Merito Demerito!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){

                        $(document.getElementById('datatable2')).append('<tr id="idMeritoDemerito'+dato['idMeritoDemerito']+'" style="font-size: 11px">' +
                                '<td width="35%">'+dato['tipo_merdem']+'</td>' +
                                '<td width="20%">'+dato['merdem']+'</td>' +
                                '<td width="10%">'+dato['fec_documento']+'</td>' +
                                '<td width="15%">'+dato['ind_documento']+'</td>' +
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                                'data-keyboard="false" data-backdrop="static" idMeritoDemerito="'+dato['idMeritoDemerito']+'"' +
                                'descipcion="El Usuario a Modificado Meritos Demeritos" titulo="Modificar Meritos Demeritos">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMeritoDemerito="'+dato['idMeritoDemerito']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado Meritos Demeritos" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Merito Demerito!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');

                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "72%" );

        //inicializar el datepicker
        $("#fecha").datepicker({
            format:'dd-mm-yyyy',
            language:'es',
            autoclose: true
        });

        //AL SELECIONAR SELECT TIPO
        $("#fk_a006_num_miscelaneo_detalle_tipo").change(function () {
            var cod_detalle = $('#fk_a006_num_miscelaneo_detalle_tipo').val().split("-");
            $("#ind_tipo").val(cod_detalle[1]);
            $("#fk_a006_num_miscelaneo_detalle_tipo option:selected").each(function () {
                $.post("{$_Parametros.url}modRH/gestion/meritosDemeritosCONTROL/SelectMeritosDemeritosMET", { tipo: $('#fk_a006_num_miscelaneo_detalle_tipo').val() }, function(data){
                    var json = data,
                            obj = JSON.parse(json);
                    $("#fk_a006_num_miscelaneo_detalle_meritodemerito").html(obj);
                });
            });
        });

        //chekboxes para habilitar campo responsable al seleccionar el checkbox
        $('#num_flag_responsable').click(function() {
            if( $('#num_flag_responsable').is(':checked') ) {
                $("#fk_rhb001_num_empleado_responsable").prop("disabled", false);
                $("#ind_persona_externa").prop("disabled", true);
                $("#buscarEmpleado").prop("disabled", false);
                $("#num_flag_externo").prop("checked", false);
            }else{
                $("#fk_rhb001_num_empleado_responsable").prop("disabled", true);
                $("#buscarEmpleado").prop("disabled", true);
                $("#ind_persona_externa").prop("disabled", false);
                $("#num_flag_externo").prop("checked", true);
            }
        });

        //chekboxes para habilitar campo persona externa al seleccionar el checkbox
        $('#num_flag_externo').click(function() {
            if( $('#num_flag_externo').is(':checked') ) {
                $("#ind_persona_externa").prop("disabled", false);
                $("#fk_rhb001_num_empleado_responsable").prop("disabled", true);
                $("#buscarEmpleado").prop("disabled", true);
                $("#num_flag_responsable").prop("checked", false);
            }else{
                $("#ind_persona_externa").prop("disabled", true);
                $("#buscarEmpleado").prop("disabled", false);
                $("#fk_rhb001_num_empleado_responsable").prop("disabled", false);
                $("#num_flag_responsable").prop("checked", true);
            }
        });

        var $url = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/ConsultarEmpleadoMET';
        $('#buscarEmpleado').click(function(){
            $('#formModalLabel3').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', tipo_busqueda:'ACTIVOS', accion:'MERITOS-DEMERITOS' },function($dato){
                $('#ContenidoModal3').html($dato);
                $('#modalAncho3').css( "width", "70%" );
            });
        });


    });






</script>