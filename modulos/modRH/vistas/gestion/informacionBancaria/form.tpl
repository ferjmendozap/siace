<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idInformacionBancaria}" name="idInformacionBancaria"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>


                                <div class="col-xs-10" style="margin-top: -10px">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select id="fk_a006_num_miscelaneo_detalle_banco" name="form[int][fk_a006_num_miscelaneo_detalle_banco]" class="form-control input-sm" required data-msg-required="Seleccione Banco">
                                                    <option value="">Seleccione...</option>
                                                    {foreach item=sex from=$Banco}
                                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_banco)}
                                                            {if $sex.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_banco}
                                                                <option selected value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                            {else}
                                                                <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {else}
                                                            <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {/foreach}
                                                </select>
                                                <label for="fk_a006_num_miscelaneo_detalle_banco">Banco</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select id="fk_a006_num_miscelaneo_detalle_tipocta" name="form[int][fk_a006_num_miscelaneo_detalle_tipocta]" class="form-control input-sm" required data-msg-required="Seleccione Tipo de Cuenta">
                                                    <option value="">Seleccione...</option>
                                                    {foreach item=dat from=$TipoCta}
                                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipocta)}
                                                            {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipocta}
                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {else}
                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {else}
                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {/foreach}
                                                </select>
                                                <label for="fk_a006_num_miscelaneo_detalle_tipocta">Tipo de Cuenta</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div>
                                    </div><!--end .row -->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select id="fk_a006_num_miscelaneo_detalle_tipoapo" name="form[int][fk_a006_num_miscelaneo_detalle_tipoapo]" class="form-control input-sm" required data-msg-required="Seleccione Tipo de Aporte">
                                                    <option value="">Seleccione...</option>
                                                    {foreach item=dat from=$TipoApo}
                                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipoapo)}
                                                            {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipoapo}
                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {else}
                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {else}
                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {/foreach}
                                                </select>
                                                <label for="fk_a006_num_miscelaneo_detalle_tipoapo">Tipo de Aporte</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="ind_cuenta" maxlength="20" name="form[int][ind_cuenta]" value="{if isset($formDB.ind_cuenta)}{$formDB.ind_cuenta}{/if}" data-rule-number="true" required data-msg-required="Introduzca Numero de Cuenta" data-msg-number="Por favor Introduzca solo numeros">
                                                <label for="ind_cuenta">Numero de Cuenta</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div><!--end .col -->
                                    </div>
                                    <div class="row">
                                        <!--
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="num_sueldo_mensual" name="form[int][num_sueldo_mensual]" value="{if isset($formDB.num_sueldo_mensual)}{$formDB.num_sueldo_mensual|number_format:2:",":"."}{/if}">
                                                <label for="num_sueldo_mensual">Sueldo Mensual</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div><!--end .col -->
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-styled">
                                                <label>
                                                    <input type="checkbox" {if isset($formDB.num_flag_principal) and $formDB.num_flag_principal==1} checked{/if} value="1" name="form[int][num_flag_principal]">
                                                    <span>Cuenta Principal</span>
                                                </label>
                                            </div>
                                        </div><!--end .col -->
                                    </div><!--end .row -->

                                    <div class="row">
                                        <br><br>
                                        <div align="right">
                                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                                        </div>
                                    </div>

                                </div>
                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/informacionBancariaCONTROL/InformacionBancariaMET', datos ,function(dato){

                if(dato['status']=='modificar'){

                    $(document.getElementById('idInformacionBancaria'+dato['idInformacionBancaria'])).html('<td width="35%">'+dato['banco']+'</td>' +
                            '<td width="15%">'+dato['tipocta']+'</td>' +
                            '<td width="20%">'+dato['ind_cuenta']+'</td>' +
                            '<td width="15%">'+dato['tipoapo']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idInformacionBancaria="'+dato['idInformacionBancaria']+'"' +
                            'descipcion="El Usuario a Modificado Informacion Bancaria" titulo="Modificar Información Bancaria">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idInformacionBancaria="'+dato['idInformacionBancaria']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Información Bancaria" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Información Bancaria!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){

                    if(dato['idInformacionBancaria']=='rep'){
                        $("#ind_cuenta").focus();
                        swal("Error!", "Verifique el numero de cuenta, ya existe un registro con ese numero de cuenta.", "error");
                    }else{
                        $(document.getElementById('datatable2')).append('<tr id="idInformacionBancaria'+dato['idInformacionBancaria']+'" style="font-size: 11px">' +
                                '<td width="35%">'+dato['banco']+'</td>' +
                                '<td width="15%">'+dato['tipocta']+'</td>' +
                                '<td width="20%">'+dato['ind_cuenta']+'</td>' +
                                '<td width="15%">'+dato['tipoapo']+'</td>' +
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                                'data-keyboard="false" data-backdrop="static" idInformacionBancaria="'+dato['idInformacionBancaria']+'"' +
                                'descipcion="El Usuario a Modificado Informacion Bancaria" titulo="Modificar Información Bancaria">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idInformacionBancaria="'+dato['idInformacionBancaria']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado Información Bancaria" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Información Bancaria!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');
                    }
                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "72%" );

        //PARA DAR FORMATO AL CAMPO MONTO
        $("#num_sueldo_mensual").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });


    });






</script>