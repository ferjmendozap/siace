<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                        <div id="rootwizard2" class="form-wizard form-wizard-horizontal">

                            <form enctype="multipart/form-data" id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <div class="form-wizard-nav">
                                    <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                                    <ul class="nav nav-justified">
                                        <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">CAPACITACIÓN</span></a></li>
                                        <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">FUNDAMENTACIÓN</span></a></li>
                                        <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">PARTICIPANTES</span></a></li>
                                        <li><a href="#step4" data-toggle="tab"><span class="step">4</span> <span class="title">HORARIO</span></a></li>
                                        <li><a href="#step5" data-toggle="tab"><span class="step">5</span> <span class="title">GASTOS</span></a></li>
                                        <li><a href="#step6" data-toggle="tab"><span class="step">6</span> <span class="title">FINALIZAR</span></a></li>
                                    </ul>
                                </div><!--end .form-wizard-nav -->

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idCapacitacion}" name="idCapacitacion" id="idCapacitacion"/>
                                <input type="hidden" value="{$proceso}" name="proceso" id="proceso"/>
                                <input type="hidden" name="form[txt][id_empleados]"  id="id_empleados">
                                <input type="hidden" value="{$tipo}" name="tipo"  id="tipo">

                                <div class="tab-content clearfix">

                                    <!-- datos genereales-->
                                    <div class="tab-pane active" id="step1">

                                        <br/><br/>

                                        <div class="col-xs-12" style="margin-top: -10px">


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" tabindex="1" required data-msg-required="Seleccione Organismo">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$listadoOrganismos}
                                                                {if isset($formDBpr.fk_a001_num_organismo)}
                                                                    {if $dat.pk_num_organismo==$formDBpr.fk_a001_num_organismo}
                                                                        <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle">Organismo</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" id="nombreTrabajador" value="{if isset($formDBpr.nombre_persona_solicitante)}{$formDBpr.nombre_persona_solicitante}{/if}" disabled>
                                                                <input type="hidden" name="form[int][fk_rhb001_empleado_solicitante]" id="fk_rhb001_empleado_solicitante" value="{if isset($formDBpr.fk_rhb001_empleado_solicitante)}{$formDBpr.fk_rhb001_empleado_solicitante}{/if}"  disabled>
                                                                <label for="nombreTrabajador">Empleado</label>
                                                            </div>
                                                            <div class="input-group-btn">
                                                                <button class="btn btn-sm btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado" tabindex="2" disabled>Buscar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" name="form[txt][ind_periodo]" id="ind_periodo" value="{if isset($formDBpr.ind_periodo)}{$formDBpr.ind_periodo}{else}{$periodo}{/if}" disabled>
                                                                <label for="ind_periodo">Periodo</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fk_rhc048_num_curso">Curso</label>
                                                        <select id="fk_rhc048_num_curso" name="form[int][fk_rhc048_num_curso]" class="form-control select2-list select2 input-sm" tabindex="3" disabled>
                                                            <option value="">Seleccione Curso</option>
                                                            {foreach item=i from=$listadoCursos}
                                                                {if isset($formDBpr.fk_rhc048_num_curso)}
                                                                    {if $i.pk_num_curso==$formDBpr.fk_rhc048_num_curso}
                                                                        <option selected value="{$i.pk_num_curso}">{$i.ind_nombre_curso}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_curso}">{$i.ind_nombre_curso}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$i.pk_num_curso}">{$i.ind_nombre_curso}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fk_rhc040_centro_estudio">Centro de Estudio</label>
                                                        <select id="fk_rhc040_centro_estudio" name="form[int][fk_rhc040_centro_estudio]" class="form-control select2-list select2 input-sm" tabindex="4" disabled>
                                                            <option value="">Seleccione Centro de Estudio</option>
                                                            {foreach item=i from=$listadoCentros}
                                                                {if isset($formDBpr.fk_rhc040_centro_estudio)}
                                                                    {if $i.pk_num_institucion==$formDBpr.fk_rhc040_centro_estudio}
                                                                        <option selected value="{$i.pk_num_institucion}">{$i.ind_nombre_institucion}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_institucion}">{$i.ind_nombre_institucion}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$i.pk_num_institucion}">{$i.ind_nombre_institucion}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a008_num_pais" name="form[int][fk_a008_num_pais]" class="form-control input-sm" required data-msg-required="Seleccione País" tabindex="5" disabled>
                                                            <option value="">Seleccione el Pais</option>
                                                            {foreach item=i from=$listadoPais}
                                                                {if isset($formDBpr.pk_num_pais)}
                                                                    {if $i.pk_num_pais==$formDBpr.pk_num_pais}
                                                                        <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a008_num_pais"><i class="md md-map"></i> Pais</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelnaeo_detalle_tipocap" name="form[int][fk_a006_num_miscelnaeo_detalle_tipocap]" class="form-control input-sm" tabindex="9" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$miscelaneoTipoCap}
                                                                {if isset($formDBpr.fk_a006_num_miscelnaeo_detalle_tipocap)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBpr.fk_a006_num_miscelnaeo_detalle_tipocap}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelnaeo_detalle_tipocap">Tipo de Capacitación</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelnaeo_detalle_modalidad" name="form[int][fk_a006_num_miscelnaeo_detalle_modalidad]" class="form-control input-sm" tabindex="10" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$miscelaneoModalidad}
                                                                {if isset($formDBpr.fk_a006_num_miscelnaeo_detalle_modalidad)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBpr.fk_a006_num_miscelnaeo_detalle_modalidad}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelnaeo_detalle_modalidad">Modalidad</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelnaeo_detalle_origen" name="form[int][fk_a006_num_miscelnaeo_detalle_origen]" class="form-control input-sm" tabindex="11" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$miscelaneoOrigen}
                                                                {if isset($formDBpr.fk_a006_num_miscelnaeo_detalle_origen)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBpr.fk_a006_num_miscelnaeo_detalle_origen}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelnaeo_detalle_origen">Origen</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a009_num_estado" name="form[int][fk_a009_num_estado]"  class="form-control input-sm" required data-msg-required="Seleccione Estado" tabindex="6" disabled>
                                                            <option value="">Seleccione el Estado</option>
                                                        </select>
                                                        <label for="fk_a009_num_estado"><i class="md md-map"></i> Estado</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="num_vacantes" name="form[int][num_vacantes]" value="{if isset($formDBpr.num_vacantes)}{$formDBpr.num_vacantes}{/if}" tabindex="12" disabled>
                                                        <label for="num_vacantes">Vacantes</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="num_participantes" name="form[txt][num_participantes]" value="{if isset($formDBpr.num_participantes)}{$formDBpr.num_participantes}{else}0{/if}" tabindex="13" disabled>
                                                        <label for="num_vacantes">Participantes</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="input-daterange input-group" id="rango-fechas_">
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" name="form[txt][fec_fecha_inicio]" id="fec_fecha_inicio" value="{if isset($formDBpr.fec_fecha_inicio)}{$formDBpr.fec_fecha_inicio|date_format:"d-m-Y"}{/if}" tabindex="14" disabled/>
                                                                <label for="">Fecha de Inicio</label>
                                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                            </div>
                                                            <span class="input-group-addon">a</span>
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" name="form[txt][fec_fecha_termino]" id="fec_fecha_termino" value="{if isset($formDBpr.fec_fecha_termino)}{$formDBpr.fec_fecha_termino|date_format:"d-m-Y"}{/if}" tabindex="15" disabled/>
                                                                <label for="">Fecha de Fin</label>
                                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                                <div class="form-control-line"></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a011_num_municipio" name="form[int][fk_a011_num_municipio]"  class="form-control input-sm" required data-msg-required="Seleccione Municipio" tabindex="7" disabled>
                                                            <option value="">Seleccione el Municipio</option>
                                                        </select>
                                                        <label for="fk_a011_num_municipio"><i class="md md-map"></i> Municipio</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="ind_expositor" name="form[txt][ind_expositor]" value="{if isset($formDBpr.ind_expositor)}{$formDBpr.ind_expositor}{/if}" tabindex="16" disabled>
                                                        <label for="ind_expositor">Expositor</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="ind_telefonos" name="form[txt][ind_telefonos]" value="{if isset($formDBpr.ind_telefonos)}{$formDBpr.ind_telefonos}{/if}" tabindex="17" disabled>
                                                        <label for="ind_telefonos">Telefonos</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a010_num_ciudad" name="form[int][fk_a010_num_ciudad]"  class="form-control input-sm" required data-msg-required="Seleccione Ciudad" tabindex="8" disabled>
                                                            <option value="">Seleccione la Ciudad</option>
                                                        </select>
                                                        <label for="fk_a010_num_ciudad"><i class="md md-map"></i> Ciudad</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="dias" name="form[int][num_total_dias]" value="{if isset($formDBpr.num_total_dias)}{$formDBpr.num_total_dias}{/if}"disabled>
                                                        <label for="num_total_dias">Dias</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="horas" name="form[txt][num_total_horas]" value="{if isset($formDBpr.ind_total_horas)}{$formDBpr.ind_total_horas}{/if}" disabled>
                                                        <label for="horas">Horas</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="checkbox checkbox-styled">
                                                        <label>
                                                            <input type="checkbox" {if isset($formDBpr.flag_costos) and $formDBpr.flag_costos==1} checked {/if} value="1" name="form[int][flag_costos]" id="flag_costos" disabled>
                                                            <span>Capacitación Sin Costo</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_observaciones]" id="txt_observaciones" class="form-control input-sm" rows="2" placeholder="" disabled>{if isset($formDBpr.txt_observaciones)}{$formDBpr.txt_observaciones}{/if}</textarea>
                                                        <label for="txt_observaciones">Observaciones Generales</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="card">
                                                        <div class="card-head card-head-xs style-primary">
                                                            <header>Costo Estimado Total</header>
                                                            <div class="tools">
                                                                <a class="btn btn-xs btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                                            </div>
                                                          </div><!--end .card-head -->
                                                        <div class="card-body">

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="num_monto_estimado" name="form[txt][num_monto_estimado]" value="{if isset($formDBpr.num_monto_estimado)}{$formDBpr.num_monto_estimado|number_format:2:",":"."}{/if}" disabled>
                                                                    <label for="num_monto_estimado">Costo Estimado</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="num_monto_asumido" name="form[txt][num_monto_asumido]" value="{if isset($formDBpr.num_monto_asumido)}{$formDBpr.num_monto_asumido|number_format:2:",":"."}{/if}" disabled>
                                                                    <label for="num_monto_asumido">Monto Asumido</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="saldo" name="form[txt][saldo]" value="" disabled>
                                                                    <label for="horas">Saldo</label>
                                                                </div>
                                                            </div>

                                                        </div><!--end .card-body -->
                                                    </div><!--end .card -->
                                                </div><!--end .col -->

                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="card">
                                                        <div class="card-head card-head-xs style-primary">
                                                            <header>Costo Real</header>
                                                            <div class="tools">
                                                                <a class="btn btn-xs btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                                            </div>
                                                        </div><!--end .card-head -->
                                                        <div class="card-body">

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="sub_total" name="form[txt][sub_total]" value="{if isset($formDBGastos.num_sub_total)}{$formDBGastos.num_sub_total|number_format:2:",":"."}{/if}" disabled>
                                                                    <label for="sub_total">Sub. Total</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="impuestos" name="form[txt][impuestos]" value="{if isset($formDBGastos.num_impuestos)}{$formDBGastos.num_impuestos|number_format:2:",":"."}{/if}" disabled>
                                                                    <label for="impuestos">Impuestos</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="total" name="form[txt][total]" value="{if isset($formDBGastos.num_total)}{$formDBGastos.num_total|number_format:2:",":"."}{/if}" disabled>
                                                                    <label for="total">Total</label>
                                                                </div>
                                                            </div>

                                                        </div><!--end .card-body -->
                                                    </div><!--end .card -->
                                                </div><!--end .col -->

                                             </div>




                                        </div><!--end .col -->


                                    </div><!-- fin datos generales -->

                                    <!-- FUNDAMENTACION-->
                                    <div class="tab-pane" id="step2">

                                        <br><br>
                                        <div class="col-xs-12">

                                            <!-- 1 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_1]" id="txt_fundamentacion_1" class="form-control input-sm" rows="2" placeholder="" disabled>{if isset($formDBfu.txt_fundamentacion_1)}{$formDBfu.txt_fundamentacion_1}{/if}</textarea>
                                                        <label for="txt_fundamentacion_1">1. ¿Cual es el Objetivo de la Capacitación?</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- 2 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_2]" id="txt_fundamentacion_2" class="form-control input-sm" rows="2" placeholder="" disabled>{if isset($formDBfu.txt_fundamentacion_2)}{$formDBfu.txt_fundamentacion_2}{/if}</textarea>
                                                        <label for="txt_fundamentacion_2">2. ¿En qué medida la Capacitación va en relación a los objetivos del área y de la organización?</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- 3 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_3]" id="txt_fundamentacion_3" class="form-control input-sm" rows="2" placeholder="" disabled>{if isset($formDBfu.txt_fundamentacion_3)}{$formDBfu.txt_fundamentacion_3}{/if}</textarea>
                                                        <label for="txt_fundamentacion_3">3. Días y horarios mas factibles para la capacitación</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- 4 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_4]" id="txt_fundamentacion_4" class="form-control input-sm" rows="2" placeholder="" disabled>{if isset($formDBfu.txt_fundamentacion_4)}{$formDBfu.txt_fundamentacion_4}{/if}</textarea>
                                                        <label for="txt_fundamentacion_4">4. ¿Qué se espera después de la capacitación?</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- 5 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_5]" id="txt_fundamentacion_5" class="form-control input-sm" rows="2" placeholder="" disabled>{if isset($formDBfu.txt_fundamentacion_5)}{$formDBfu.txt_fundamentacion_5}{/if}</textarea>
                                                        <label for="txt_fundamentacion_5">5. ¿Como hace hoy si Trabajo?</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- 6 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_6]" id="txt_fundamentacion_6" class="form-control input-sm" rows="2" placeholder="" disabled>{if isset($formDBfu.txt_fundamentacion_6)}{$formDBfu.txt_fundamentacion_6}{/if}</textarea>
                                                        <label for="txt_fundamentacion_6">6. ¿Hay colaboradores dentro de la Institución que pueden dictar este curso?</label>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>

                                    </div><!-- fin fundamentación -->

                                    <!-- PARTICIPANTES -->
                                    <div class="tab-pane" id="step3">

                                        <br><br>
                                        <div class="col-xs-12">




                                            <div class="col-lg-12">

                                                <table id="empleadosBeneficiarios" class="table table-striped no-margin table-condensed" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th class="col-sm-1">Emp.</th>
                                                        <th class="col-sm-4">Nombre Completo</th>
                                                        <th class="col-sm-1">Total Dias</th>
                                                        <th class="col-sm-1">Total Horas</th>
                                                        <th class="col-sm-1">Dias Asis.</th>
                                                        <th class="col-sm-1">Apro.</th>
                                                        <th class="col-sm-1">Nota</th>
                                                        <th class="col-sm-1">Importe Gastos</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {foreach item=dat from=$formDBem}


                                                        {if $tipo == 'TE'}

                                                            <tr id="" style="font-size: 11px">
                                                                <input type="hidden" size="2"  id="empleado_cap{$n}" name="form[int][empleado_cap][]" value="{$dat.fk_rhb001_num_empleado}">
                                                                <td>{$dat.fk_rhb001_num_empleado}</td>
                                                                <td>{$dat.ind_nombre1} {$dat.ind_nombre2} {$dat.ind_apellido1} {$dat.ind_apellido2}</td>
                                                                <td>{$dat.num_asistencias}</td>
                                                                <td>{$dat.ind_horas_asistencia}</td>
                                                                <td><input type="text" size="2"  id="num_dias_asistidos{$n}" name="form[int][num_dias_asistidos][]"> </td>
                                                                <td><input type="checkbox" id="flag_aprobado{$n}" name="form[int][flag_aprobado][]"> </td>
                                                                <td><input type="text" size="2"  id="ind_calificacion{$n}" name="form[int][ind_calificacion][]"></td>
                                                                <td>{$dat.num_importe_gastos|number_format:2:",":"."}</td>

                                                            </tr>
                                                            <input type="hidden" value="{$n++}">

                                                        {else}

                                                            <tr id="" style="font-size: 11px">

                                                                <td>{$dat.fk_rhb001_num_empleado}</td>
                                                                <td>{$dat.ind_nombre1} {$dat.ind_nombre2} {$dat.ind_apellido1} {$dat.ind_apellido2}</td>
                                                                <td>{$dat.num_asistencias}</td>
                                                                <td>{$dat.ind_horas_asistencia}</td>
                                                                <td>{$dat.num_dias_asistidos}</td>
                                                                <td>
                                                                    <i class="{if $dat.flag_aprobado==1}md md-check{else}md md-not-interested{/if}"></i>
                                                                </td>
                                                                <td>{$dat.ind_calificacion}</td>
                                                                <td>{$dat.num_importe_gastos|number_format:2:",":"."}</td>

                                                            </tr>

                                                        {/if}



                                                    {/foreach}

                                                    </tbody>
                                                </table>

                                            </div>


                                        </div>

                                    </div><!-- fin PARTICIPANTES -->

                                    <!--
                                        HORARIOS
                                    -->
                                    <div class="tab-pane" id="step4">

                                        <br><br>
                                        <div class="col-xs-12">

                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="card-head card-head-xs style-primary">
                                                        <header>Horarios Segun Rangos de Fechas Asignadas</header>
                                                        <div class="tools">
                                                            <a class="btn btn-xs btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                                        </div>
                                                    </div><!--end .card-head -->
                                                    <div class="card-body">

                                                        <div class="col-md-7">

                                                            {foreach item=dat from=$formDBho}


                                                            <div class="row contain-lg">
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control input-sm" id="fecha" name="form[txt][fecha][]" value="{$dat.fec_fecha|date_format:"d-m-Y"}" disabled>
                                                                        <label for="fecha">Fecha</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control input-sm desde" id="desde" name="form[txt][desde][]" value="{$dat.fec_hora_ini}" disabled>
                                                                        <label for="desde">Desde</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control input-sm desde" id="hasta" name="form[txt][hasta][]" value="{$dat.fec_hora_fin}" disabled>
                                                                        <label for="hasta">Hasta</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <input type="text" class="form-control input-sm total" id="totalH" name="form[txt][totalH][]" value="{$dat.ind_total_horas}" disabled>
                                                                        <label for="totalH">Total Horas</label>
                                                                    </div>
                                                                </div>
                                                             </div>

                                                            {/foreach}

                                                        </div>

                                                        <div class="col-md-5">

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" id="diasT" name="form[int][diasT]" value="{if isset($formDBpr.num_total_dias)}{$formDBpr.num_total_dias}{/if}" disabled>
                                                                    <label for="diasT">Total Dias</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-5">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" id="horasT" name="form[txt][horasT]" value="{if isset($formDBpr.ind_total_horas)}{$formDBpr.ind_total_horas}{/if}" disabled>
                                                                    <label for="horasT">Total Horas</label>
                                                                </div>
                                                            </div>

                                                        </div>


                                                    </div><!--end .card-body -->
                                                </div><!--end .card -->
                                            </div><!--end .col -->




                                        </div>

                                    </div><!-- fin otros datos personales -->


                                    <div class="tab-pane" id="step5">

                                        <br><br>

                                        {if $tipo=='IN'}
                                        <div class="col-xs-12">
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="ind_detalle" name="form[txt][ind_detalle]" value="" style="text-transform: uppercase">
                                                    <label for="ind_detalle">Comprobante</label>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="fec_fecha" name="form[txt][fec_fecha]" value="">
                                                    <label for="fec_fecha">Fecha</label>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="num_sub_total" name="form[txt][num_sub_total]" value="">
                                                    <label for="num_sub_total">Sub Total</label>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="num_impuestos" name="form[txt][num_impuestos]" value="">
                                                    <label for="num_impuestos">Impuestos</label>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="num_total" name="form[txt][num_total]" value="" disabled>
                                                    <label for="num_total">Total</label>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <button type="button" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" id="cargar"><span class="glyphicon glyphicon-floppy-disk"></span></button>
                                            </div>

                                        </div>
                                        {/if}

                                        <div class="col-xs-12">

                                            <table id="listadoGastos" class="table table-striped no-margin table-condensed" width="100%">
                                                <thead>
                                                <tr>
                                                    <th class="col-sm-5">Comprobante</th>
                                                    <th class="col-sm-1">Fecha</th>
                                                    <th class="col-sm-1">Sub Total</th>
                                                    <th class="col-sm-1">Impuestos</th>
                                                    <th class="col-sm-1">Total</th>
                                                    <th class="col-sm-1">Accion</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {if isset($listadoGastos)}
                                                    {foreach item=dat from=$listadoGastos}

                                                    <tr id="" style="font-size: 11px">

                                                        <td>{$dat.ind_detalle}</td>
                                                        <td>{$dat.fec_fecha|date_format:"d-m-Y"} </td>
                                                        <td>{$dat.num_sub_total|number_format:2:",":"."}</td>
                                                        <td>{$dat.num_impuestos|number_format:2:",":"."}</td>
                                                        <td>{$dat.num_total|number_format:2:",":"."}</td>
                                                        <td>...</td>

                                                    </tr>

                                                    {/foreach}
                                                {/if}

                                                </tbody>
                                            </table>

                                            <br>

                                        </div>

                                    </div>


                                    <div class="tab-pane" id="step6">

                                        <br><br>


                                        <div class="col-md-6 col-md-offset-3">

                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary">
                                                    <header>Cerrar Ventana!</header>
                                                    <div class="tools">
                                                        <a class="btn btn-xs btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                                    </div>
                                                </div><!--end .card-head -->
                                                <div class="card-body">

                                                    <br>
                                                    <div class="row text-center">
                                                        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal">Cerrar Ventana</button>


                                                    </div>
                                                    <br>

                                                </div><!--end .card-body -->
                                            </div><!--end .card -->

                                        </div>

                                    </div>




                                </div>

                                <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                                <ul class="pager wizard">
                                    <li class="previous first" id="primero"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <li class="next last" id="ultimo"><a class="ink-reaction btn-raised" href="javascript:void(0);">Ultimo</a></li>
                                    <li class="next" id="siguiente"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                                </ul>

                            </form>
                        </div>

            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //inicializar el datepicker
    $("#rango-fechas").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");

            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();

            var TIPO = $("#tipo").val();

            if(TIPO=='IN'){
                $.post('{$_Parametros.url}modRH/gestion/capacitacionCONTROL/IniciarCapacitacionFormMET', datos ,function(dato){

                    if(dato['status']=='error'){


                        //Error para usuarios normales del sistema
                        swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        //Error para los programadores para visualizar en detalle el ERROR
                        //swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");


                    }
                    else if(dato['status']=='iniciado'){

                        swal("Capacitación Iniciada!", "Capacitación Iniciada satisfactoriamente...", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                        $("#_contenido").load('{$_Parametros.url}modRH/gestion/capacitacionCONTROL');

                    }

                },'json');
            }
            if(TIPO=='TE'){
                $.post('{$_Parametros.url}modRH/gestion/capacitacionCONTROL/TerminarCapacitacionFormMET', datos ,function(dato){

                    if(dato['status']=='error'){


                        //Error para usuarios normales del sistema
                        swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        //Error para los programadores para visualizar en detalle el ERROR
                        //swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");


                    }
                    else if(dato['status']=='terminado'){

                        swal("Capacitación Iniciada!", "Capacitación Iniciada satisfactoriamente...", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                        $("#_contenido").load('{$_Parametros.url}modRH/gestion/capacitacionCONTROL');

                    }

                },'json');
            }





        }
    });


    $(document).ready(function() {

        var rh  = new  AppFunciones();
        var rh2 = new  ModRhFunciones();

        var idPais   = '{if isset($formDBub.pk_num_pais)}{$formDBub.pk_num_pais}{/if}';
        var idEstado = '{if isset($formDBub.pk_num_estado)}{$formDBub.pk_num_estado}{/if}';
        var idCiudad = '{if isset($formDBub.fk_a010_num_ciudad)}{$formDBub.fk_a010_num_ciudad}{/if}';
        var idMunicipio = '{if isset($formDBub.pk_num_municipio)}{$formDBub.pk_num_municipio}{/if}';

        var idPais2   = '{if isset($formDBdomicilio.pais_domicilio)}{$formDBdomicilio.pais_domicilio}{/if}';
        var idEstado2 = '{if isset($formDBdomicilio.estado_domicilio)}{$formDBdomicilio.estado_domicilio}{/if}';
        var idCiudad2 = '{if isset($formDBdomicilio.ciudad_domicilio)}{$formDBdomicilio.ciudad_domicilio}{/if}';
        var idMunicipio2 = '{if isset($formDBdomicilio.municipio_domicilio)}{$formDBdomicilio.municipio_domicilio}{/if}';
        var idParroquia  = '{if isset($formDBdomicilio.parroquia_domicilio)}{$formDBdomicilio.parroquia_domicilio}{/if}';
        var idSector     = '{if isset($formDBdomicilio.sector_domicilio)}{$formDBdomicilio.sector_domicilio}{/if}';

        var idDependencia = '{if isset($formDBlaborales.fk_a004_num_dependencia)}{$formDBlaborales.fk_a004_num_dependencia}{/if}';
        var idCentroCosto = '{if isset($formDBlaborales.fk_a023_num_centro_costo)}{$formDBlaborales.fk_a023_num_centro_costo}{/if}';
        var idGrupoOcupacional = '{if isset($formDBlaborales.fk_a006_num_miscelaneo_detalle_grupoocupacional)}{$formDBlaborales.fk_a006_num_miscelaneo_detalle_grupoocupacional}{/if}';
        var idSerieOcupacional = '{if isset($formDBlaborales.fk_rhc010_num_serie)}{$formDBlaborales.fk_rhc010_num_serie}{/if}';
        var idCargo = '{if isset($formDBlaborales.fk_rhc063_num_puestos_cargo)}{$formDBlaborales.fk_rhc063_num_puestos_cargo}{/if}';

        rh.metJsonEstado('{$_Parametros.url}estado/jsonEstado',idPais,idEstado);
        rh2.metJsonMunicipioN('{$_Parametros.url}municipio/JsonMunicipio',idEstado,idMunicipio);
        rh2.metJsonCiudadN('{$_Parametros.url}ciudad/JsonCiudad',idEstado,idCiudad);

        rh2.metJsonEstadoD('{$_Parametros.url}estado/jsonEstado',idPais2,idEstado2);
        rh2.metJsonCiudadD('{$_Parametros.url}ciudad/JsonCiudad',idMunicipio2,idCiudad2);
        rh2.metJsonMunicipioD('{$_Parametros.url}municipio/JsonMunicipio',idEstado2,idMunicipio2);
        rh2.metJsonParroquiaD('{$_Parametros.url}modRH/gestion/empleadosCONTROL/JsonParroquiaMET',idMunicipio2,idParroquia);
        rh2.metJsonSectorD('{$_Parametros.url}modRH/gestion/empleadosCONTROL/JsonSectorMET',idParroquia,idSector);

        rh2.metJsonCentroCosto('{$_Parametros.url}modRH/gestion/empleadosCONTROL/JsonCentroCostoMET',idDependencia,idCentroCosto);
        rh2.metJsonSerieOcupacional('{$_Parametros.url}modRH/maestros/serieOcupacionalCONTROL/JsonSerieOcupacionalMET',idGrupoOcupacional,idSerieOcupacional);
        rh2.metJsonCargos('{$_Parametros.url}modRH/maestros/cargosCONTROL/JsonCargosMET',idGrupoOcupacional,idSerieOcupacional,idCargo);

        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho').css( "width", "80%" );

        $("#num_monto_estimado").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        $("#num_monto_asumido").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });


        /*************************************gastos****************************************************/
        $("#num_sub_total").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        $("#num_impuestos").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        $("#num_total").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        /*************************************gastos****************************************************/

        $('#fec_fecha_inicio').mask('99-99-9999');
        $('#fec_fecha_termino').mask('99-99-9999');
        $('#fec_fecha').mask('99-99-9999');


        $("#num_impuestos").keyup(function( event ) {


            var imp = $("#num_impuestos").val();
            var sub = $("#num_sub_total").val();

            imp = quitarMiles(imp);
            imp = imp.replace(',','.');

            sub = quitarMiles(sub);
            sub = sub.replace(',','.');

            var total = parseFloat(sub) + parseFloat(imp);

            $("#num_total").val(toCurrency(total));

        });

        $("#cargar").click(function() {

            var detalle = $("#ind_detalle").val();
            var fecha   = $("#fec_fecha").val();
            var num_sub_total = $("#num_sub_total").val();
            var num_impuestos = $("#num_impuestos").val();
            var num_total     = $("#num_total").val();
            var capacitacion = $("#idCapacitacion").val();

            num_sub_total = quitarMiles(num_sub_total);
            num_sub_total = num_sub_total.replace(',','.');
            num_impuestos = quitarMiles(num_impuestos);
            num_impuestos = num_impuestos.replace(',','.');
            num_total = quitarMiles(num_total);
            num_total = num_total.replace(',','.');


            if(detalle==''){
                swal("Informacion!", "Indique detalle del Gasto", "warning");
                return false;
            }
            else if(fecha==''){
                swal("Informacion!", "Indique fecha de Comprobante", "warning");
                return false;
            }
            else if(num_sub_total==''){
                swal("Informacion!", "Indique Sub Total", "warning");
                return false;
            }
            else if(num_impuestos==''){
                swal("Informacion!", "Indique Impuestos", "warning");
                return false;
            }
            else{
                var tabla_agregar = $('#listadoGastos').DataTable();
                var $url_gastos = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/GuardarGastosMET';
                $.post($url_gastos, { capacitacion:capacitacion, detalle:detalle, fecha:fecha, num_sub_total:num_sub_total, num_impuestos:num_impuestos, num_total:num_total  } ,function(dato){

                    if(dato['status']=='error'){

                        /*Error para usuarios normales del sistema*/
                        swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        /*Error para los programadores para visualizar en detalle el ERROR*/
                        //swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");

                    }
                    else if(dato['status']=='gasto_registrado'){

                        $("#ind_detalle").val('');
                        $("#fec_fecha").val('');
                        $("#num_sub_total").val('');
                        $("#num_impuestos").val('');
                        $("#num_total").val('');



                        var fila = tabla_agregar.row.add([ {*añade las filas al tbody permitiendo aplicar la paginación automaticamente*}
                            detalle,
                            fecha,
                            toCurrency(num_sub_total),
                            toCurrency(num_impuestos),
                            num_total,
                            '<button type="button" idGasto="'+dato['idGasto']+'" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'

                        ]).draw()
                                .nodes(){*Agregar una clase a cada fila añadida*}
                                .to$()
                                .addClass( 'fila_agregar' );

                        $(fila)
                                .attr('id','idGasto'+dato['idGasto']+'');


                        /*para mostrar el total de los gastos registrados en la capacitacion*/
                        var $url_gastos_mostrar = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/MostrarGastosMET';
                        $.post($url_gastos_mostrar, { capacitacion:capacitacion } ,function(gastos){

                            $("#sub_total").val(gastos['sub_total']);
                            $("#impuestos").val(gastos['impuestos']);
                            $("#total").val(gastos['total']);


                        },'json');




                    }

                },'json');


            }

        });


        $('#listadoGastos tbody').on( 'click', '.eliminar', function () {

            var obj = this;

            var capacitacion = $("#idCapacitacion").val();
            var gasto = $(this).attr('idGasto');

            swal({
                title:'Estas Seguro?',
                text: 'Estas seguro que desea descartar el Gasto!!!',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Descartar",
                closeOnConfirm: false
            }, function(){

                var tabla_agregar = $('#listadoGastos').DataTable();

                var $url_gastos_eliminar = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/EliminarGastosMET';

                $.post($url_gastos_eliminar, { capacitacion:capacitacion, gasto:gasto } ,function(dato){


                    if(dato['status']=='OK'){

                        tabla_agregar.row( $(obj).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}
                        swal("Descartado!", "Descartado de la lista de Gastos", "success");

                    }
                    else if(dato['error']){
                        swal("Error!", dato['mensaje'], "error");
                    }


                },'json');

            });

        });


        var $url = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/ConsultarEmpleadoMET';
        $('#buscarEmpleado').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', accion:'CAPACITACION' },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#insertarEmpleado').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', accion:'INSERTAR_EMPLEADO_CAPACITACION' },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#flag_costos').click(function() {
            if( $('#flag_costos').is(':checked') ) {

                $("#num_monto_estimado").prop("disabled","disabled");
                $("#num_monto_asumido").prop("disabled","disabled");
                $("#num_monto_estimado").val('');
                $("#num_monto_asumido").val('');


            }else{
                $("#num_monto_estimado").removeAttr("disabled");
                $("#num_monto_asumido").removeAttr("disabled");
                $("#num_monto_estimado").focus();
            }
        });

        /*al selecionar fecha final / termino para mostrar los horario en la ultima pestaña*/
        $("#fec_fecha_termino").change(function() {

            var fecha_ini = $("#fec_fecha_inicio").val();
            var fecha_fin = $("#fec_fecha_termino").val();

            $("#grafica").html('');
            $("#horasT").val('');

            var $url_verificar_fechas = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/GraficarHorarioMET';

            $.post($url_verificar_fechas, { fecha_ini:fecha_ini, fecha_fin:fecha_fin },function($dato){


                var cantDias = 0;

                for (var i = 0; i < $dato.length; i++) {




                    var objeto = '<div class="row contain-lg">'+
                                 '<div class="col-md-2">'+
                                 '<div class="form-group">'+
                                 '<input type="text" class="form-control input-sm" id="fecha'+i+'" name="form[txt][fecha]['+i+']" value="'+$dato[i]+'">'+
                                 '<label for="fecha'+i+'">Fecha</label>'+
                                 '</div>'+
                                 '</div>'+
                                 '<div class="col-md-2">'+
                                 '<div class="form-group">'+
                                 '<input type="text" class="form-control input-sm desde'+i+'" id="desde'+i+'" name="form[txt][desde]['+i+']" value="">'+
                                 '<label for="desde'+i+'">Desde</label>'+
                                 '</div>'+
                                 '</div>'+
                                 '<div class="col-md-2">'+
                                 '<div class="form-group">'+
                                 '<input type="text" class="form-control input-sm desde'+i+'" id="hasta'+i+'" name="form[txt][hasta]['+i+']" onchange="totalizarHoras('+i+')" value="">'+
                                 '<label for="hasta'+i+'">Hasta</label>'+
                                 '</div>'+
                                 '</div>'+
                                 '<div class="col-md-2">'+
                                 '<div class="form-group">'+
                                 '<input type="text" class="form-control input-sm total'+i+'" id="totalH'+i+'" name="form[txt][totalH]['+i+']" value="">'+
                                 '<label for="totalH'+i+'">Total Horas</label>'+
                                 '</div>'+
                                 '</div>';

                    $("#grafica").append(objeto);

                    $(".desde"+i).inputmask('hh:mm t', { placeholder: 'hh:mm xm', alias: 'time12', hourFormat: '12' });

                    cantDias++;

                }

                $("#dias").val(cantDias);
                $("#diasT").val(cantDias);

            },'json');


        });



        var tabla_agregar = $('#empleadosBeneficiarios').DataTable();

        {* ************************************************ *}
        $('#empleadosBeneficiarios tbody').on('click', '.eliminar', function () {

            var obj = this;

            swal({
                title: 'Estas Seguro?',
                text: 'Estas seguro que desea descartar al Empleado(a)',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Descartar",
                closeOnConfirm: false
            }, function () {

                tabla_agregar.row($(obj).parents('tr')).remove().draw();

                {*Elimina la fila en cuestion*}
                swal("Descartado!", "Descartado de la lista el Empleado", "success");


                {*verifico la tablas donde estan los empleados para volver armar el arreglo de empleados*}
                var num_filas = $('#empleadosBeneficiarios >tbody >tr').length;
                var filas = $('#empleadosBeneficiarios >tbody >tr'); {*trae las filas del tbody*}


                var cad_id_empleados = '';
                $('#id_empleados').val('');


                for(var i=0; i<num_filas; i++) {

                    {*voy armando arreglo de empleados*}
                    cad_id_empleados = cad_id_empleados + filas[i].cells[0].innerHTML + '#';

                }
                {*asigno arreglo de empleados*}
                $('#id_empleados').val(cad_id_empleados);







            });


        });
        {* **************************************************** *}




    });

    /*permite totalizar la horas de la linea*/
    function totalizarHoras(i)
    {
        var hora_ini = $("#desde"+i).val();
        var hora_fin = $("#hasta"+i).val();

        var $url_totalizar = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/RestarHorasMET';
        var $url_actualizar = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/SumarHorasMET';

        $.post($url_totalizar, { hora_ini:hora_ini, hora_fin:hora_fin, operacion:'json' },function($dato){

            $("#totalH"+i).val($dato);

            fnTotalizar();

        },'json');


    }

    /*permite totalizar las horas de todas la lineas*/
    function fnTotalizar(){

        var fecha_ini = $("#fec_fecha_inicio").val();
        var fecha_fin = $("#fec_fecha_termino").val();

        var $url_verificar_fechas = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/GraficarHorarioMET';

        $.post($url_verificar_fechas, { fecha_ini:fecha_ini, fecha_fin:fecha_fin },function($dato){

            var AUX = 0;

            for (var i = 0; i < $dato.length; i++) {




                if(i==0){

                    var TL = $("#totalH0").val();
                    AUX = TL;
                    $("#horasT").val(AUX);

                }
                else{

                    var band = $("#totalH"+i).val();
                    if(band==''){
                        TL='00:00:00'
                    }else{
                        TL=band;
                    }

                    var tl = TL.split(":");//total de la linea descompuesto en un arreglo
                    var au = AUX.split(":");//total de horas descompuesto en un arreglo

                    var auxH = parseFloat(tl[0]) + parseFloat(au[0]);
                    var auxM = parseFloat(tl[1]) + parseFloat(au[1]);

                    if(auxM >= 60){

                        auxH = auxH + 1;
                        auxM = auxM - 60;

                    }

                    auxH = ('0'+auxH).slice(-2);
                    auxM = ('0'+auxM).slice(-2);

                    AUX = auxH+":"+auxM+":00";

                }

            }

            $("#horasT").val(AUX);
            $("#horas").val(AUX);


        },'json');

    }

    function toCurrency(cnt){
        cnt = cnt.toString().replace(/\$|\,/g,'');
        if (isNaN(cnt))
            return 0;
        var sgn = (cnt == (cnt = Math.abs(cnt)));
        cnt = Math.floor(cnt * 100 + 0.5);
        cvs = cnt % 100;
        cnt = Math.floor(cnt / 100).toString();
        if (cvs < 10)
            cvs = '0' + cvs;
        for (var i = 0; i < Math.floor((cnt.length - (1 + i)) / 3); i++)
            cnt = cnt.substring(0, cnt.length - (4 * i + 3)) + '.' + cnt.substring(cnt.length - (4 * i + 3));
        return (((sgn) ? '' : '-') + cnt + ',' + cvs);
    }

    var formatNumber = {
        separador: ".", // separador para los miles
        sepDecimal: ',', // separador para los decimales
        formatear:function (num){
            num +=' ';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
            }
            return this.simbol + splitLeft  +splitRight;
        },
        new:function(num, simbol){
            this.simbol = simbol ||'';
            return this.formatear(num);
        }
    }

    function quitarMiles(valor)
    {
        while (valor.indexOf('.')>-1) {
            pos= valor.indexOf('.');
            valor = "" + (valor.substring(0, pos) + '' +
                    valor.substring((pos + '.'.length), valor.length));
        }
        return valor;
    }



</script>