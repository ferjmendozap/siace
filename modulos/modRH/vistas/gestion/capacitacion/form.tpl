<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                        <div id="rootwizard2" class="form-wizard form-wizard-horizontal">

                            <form enctype="multipart/form-data" id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <div class="form-wizard-nav">
                                    <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                                    <ul class="nav nav-justified">
                                        <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">CAPACITACIÓN</span></a></li>
                                        <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">FUNDAMENTACIÓN</span></a></li>
                                        <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">PARTICIPANTES</span></a></li>
                                        <li><a href="#step4" data-toggle="tab"><span class="step">4</span> <span class="title">HORARIO</span></a></li>
                                        <li><a href="#step5" data-toggle="tab"><span class="step">5</span> <span class="title">FINALIZAR REGISTRO</span></a></li>
                                    </ul>
                                </div><!--end .form-wizard-nav -->

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idCapacitacion}" name="idCapacitacion" id="idCapacitacion"/>
                                <input type="hidden" name="form[txt][id_empleados]"  id="id_empleados">
                                <input type="hidden" name="form[txt][id_dependencias]"  id="id_dependencias">

                                <div class="tab-content clearfix">

                                    <!-- datos genereales-->
                                    <div class="tab-pane active" id="step1">

                                        <br/><br/>

                                        <div class="col-xs-12" style="margin-top: -10px">


                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" tabindex="1" required data-msg-required="Seleccione Organismo">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$listadoOrganismos}
                                                                {if isset($formDBlaborales.fk_a001_num_organismo)}
                                                                    {if $dat.pk_num_organismo==$formDBlaborales.fk_a001_num_organismo}
                                                                        <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle">Organismo</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" id="nombreTrabajador" value="{if isset($formDB.nombre_completo)}{$formDB.nombre_completo}{/if}" disabled>
                                                                <input type="hidden" name="form[int][fk_rhb001_empleado_solicitante]" id="fk_rhb001_empleado_solicitante" value="{if isset($formDB.fk_rhb001_empleado_solicitante)}{$formDB.fk_rhb001_empleado_solicitante}{/if}"  disabled>
                                                                <label for="nombreTrabajador">Solicitante</label>
                                                            </div>
                                                            <div class="input-group-btn">
                                                                <button class="btn btn-sm btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado" tabindex="2">Buscar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" name="form[txt][ind_periodo]" id="ind_periodo" value="{if isset($formDB.ind_periodo)}{$formDB.ind_periodo}{else}{$periodo}{/if}" disabled>
                                                                <label for="ind_periodo">Periodo</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fk_rhc048_num_curso">Curso</label>
                                                        <select id="fk_rhc048_num_curso" name="form[int][fk_rhc048_num_curso]" class="form-control select2-list select2 input-sm" tabindex="3">
                                                            <option value="">Seleccione Curso</option>
                                                            {foreach item=i from=$listadoCursos}
                                                                {if isset($formDB.fk_rhc048_num_curso)}
                                                                    {if $i.pk_num_curso==$formDB.fk_rhc048_num_curso}
                                                                        <option selected value="{$i.pk_num_curso}">{$i.ind_nombre_curso}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_curso}">{$i.ind_nombre_curso}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$i.pk_num_curso}">{$i.ind_nombre_curso}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="fk_rhc040_centro_estudio">Centro de Estudio</label>
                                                        <select id="fk_rhc040_centro_estudio" name="form[int][fk_rhc040_centro_estudio]" class="form-control select2-list select2 input-sm" tabindex="4">
                                                            <option value="">Seleccione Centro de Estudio</option>
                                                            {foreach item=i from=$listadoCentros}
                                                                {if isset($formDB.fk_rhc040_centro_estudio)}
                                                                    {if $i.pk_num_institucion==$formDB.fk_rhc040_centro_estudio}
                                                                        <option selected value="{$i.pk_num_institucion}">{$i.ind_nombre_institucion}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_institucion}">{$i.ind_nombre_institucion}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$i.pk_num_institucion}">{$i.ind_nombre_institucion}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a008_num_pais" name="form[int][fk_a008_num_pais]" class="form-control input-sm" required data-msg-required="Seleccione País" tabindex="5">
                                                            <option value="">Seleccione el Pais</option>
                                                            {foreach item=i from=$listadoPais}
                                                                {if isset($formDBnacimiento.pais_nacimiento)}
                                                                    {if $i.pk_num_pais==$formDBnacimiento.pais_nacimiento}
                                                                        <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a008_num_pais"><i class="md md-map"></i> Pais</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelnaeo_detalle_tipocap" name="form[int][fk_a006_num_miscelnaeo_detalle_tipocap]" class="form-control input-sm" tabindex="9" required>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$miscelaneoTipoCap}
                                                                {if isset($formDB.fk_a006_num_miscelnaeo_detalle_tipocap)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelnaeo_detalle_tipocap}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelnaeo_detalle_tipocap">Tipo de Capacitación</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelnaeo_detalle_modalidad" name="form[int][fk_a006_num_miscelnaeo_detalle_modalidad]" class="form-control input-sm" tabindex="10" required>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$miscelaneoModalidad}
                                                                {if isset($formDB.fk_a006_num_miscelnaeo_detalle_modalidad)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelnaeo_detalle_modalidad}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelnaeo_detalle_modalidad">Modalidad</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelnaeo_detalle_origen" name="form[int][fk_a006_num_miscelnaeo_detalle_origen]" class="form-control input-sm" tabindex="11" required>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$miscelaneoOrigen}
                                                                {if isset($formDB.fk_a006_num_miscelnaeo_detalle_origen)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelnaeo_detalle_origen}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelnaeo_detalle_origen">Origen</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a009_num_estado" name="form[int][fk_a009_num_estado]"  class="form-control input-sm" required data-msg-required="Seleccione Estado" tabindex="6">
                                                            <option value="">Seleccione el Estado</option>
                                                        </select>
                                                        <label for="fk_a009_num_estado"><i class="md md-map"></i> Estado</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="num_vacantes" name="form[int][num_vacantes]" value="{if isset($formDB.num_vacantes)}{$formDB.num_vacantes}{/if}" tabindex="12" required>
                                                        <label for="num_vacantes">Vacantes</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="num_participantes" name="form[txt][num_participantes]" value="{if isset($formDB.num_participantes)}{$formDB.num_participantes}{else}0{/if}" tabindex="13" disabled>
                                                        <label for="num_vacantes">Participantes</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="input-daterange input-group" id="rango-fechas_">
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" name="form[txt][fec_fecha_inicio]" id="fec_fecha_inicio" tabindex="14" required/>
                                                                <label for="">Fecha de Inicio</label>
                                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                            </div>
                                                            <span class="input-group-addon">a</span>
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" name="form[txt][fec_fecha_termino]" id="fec_fecha_termino" tabindex="15" required/>
                                                                <label for="">Fecha de Fin</label>
                                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                                <div class="form-control-line"></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a011_num_municipio" name="form[int][fk_a011_num_municipio]"  class="form-control input-sm" required data-msg-required="Seleccione Municipio" tabindex="7">
                                                            <option value="">Seleccione el Municipio</option>
                                                        </select>
                                                        <label for="fk_a011_num_municipio"><i class="md md-map"></i> Municipio</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="ind_expositor" name="form[txt][ind_expositor]" value="{if isset($formDB.ind_expositor)}{$formDB.ind_expositor}{/if}" tabindex="16">
                                                        <label for="ind_expositor">Expositor</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="ind_telefonos" name="form[txt][ind_telefonos]" value="{if isset($formDB.ind_telefonos)}{$formDB.ind_telefonos}{/if}" tabindex="17">
                                                        <label for="ind_telefonos">Telefonos</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a010_num_ciudad" name="form[int][fk_a010_num_ciudad]"  class="form-control input-sm" required data-msg-required="Seleccione Ciudad" tabindex="8">
                                                            <option value="">Seleccione la Ciudad</option>
                                                        </select>
                                                        <label for="fk_a010_num_ciudad"><i class="md md-map"></i> Ciudad</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="dias" name="form[int][num_total_dias]" value="" disabled>
                                                        <label for="num_total_dias">Dias</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="horas" name="form[txt][num_total_horas]" value="" disabled>
                                                        <label for="horas">Horas</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="checkbox checkbox-styled">
                                                        <label>
                                                            <input type="checkbox" {if isset($formDB.flag_costos) and $formDB.flag_costos==1} checked {/if} value="1" name="form[int][flag_costos]" id="flag_costos">
                                                            <span>Capacitación Sin Costo</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_observaciones]" id="txt_observaciones" class="form-control input-sm" rows="2" placeholder="">{if isset($formDB.txt_observaciones)}{$formDB.txt_observaciones}{/if}</textarea>
                                                        <label for="txt_observaciones">Observaciones Generales</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="card">
                                                        <div class="card-head card-head-xs style-primary">
                                                            <header>Costo Estimado Total</header>
                                                            <div class="tools">
                                                                <a class="btn btn-xs btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                                            </div>
                                                          </div><!--end .card-head -->
                                                        <div class="card-body">

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="num_monto_estimado" name="form[txt][num_monto_estimado]" value="">
                                                                    <label for="num_monto_estimado">Costo Estimado</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="num_monto_asumido" name="form[txt][num_monto_asumido]" value="">
                                                                    <label for="num_monto_asumido">Monto Asumido</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="saldo" name="form[txt][saldo]" value="" disabled>
                                                                    <label for="horas">saldo</label>
                                                                </div>
                                                            </div>

                                                        </div><!--end .card-body -->
                                                    </div><!--end .card -->
                                                </div><!--end .col -->

                                                <div class="col-lg-6 col-sm-6">
                                                    <div class="card">
                                                        <div class="card-head card-head-xs style-primary">
                                                            <header>Costo Real</header>
                                                            <div class="tools">
                                                                <a class="btn btn-xs btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                                            </div>
                                                        </div><!--end .card-head -->
                                                        <div class="card-body">

                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="sub_total" name="form[txt][sub_total]" value="" disabled>
                                                                    <label for="sub_total">Sub. Total</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="impuestos" name="form[txt][impuestos]" value="" disabled>
                                                                    <label for="impuestos">Impuestos</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control input-sm" id="total" name="form[txt][total]" value="" disabled>
                                                                    <label for="total">Total</label>
                                                                </div>
                                                            </div>

                                                        </div><!--end .card-body -->
                                                    </div><!--end .card -->
                                                </div><!--end .col -->

                                             </div>




                                        </div><!--end .col -->


                                    </div><!-- fin datos generales -->

                                    <!-- FUNDAMENTACION-->
                                    <div class="tab-pane" id="step2">

                                        <br><br>
                                        <div class="col-xs-12">

                                            <!-- 1 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_1]" id="txt_fundamentacion_1" class="form-control input-sm" rows="2" placeholder="">{if isset($formDB.txt_fundamentacion_1)}{$formDB.txt_fundamentacion_1}{/if}</textarea>
                                                        <label for="txt_fundamentacion_1">1. ¿Cual es el Objetivo de la Capacitación?</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- 2 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_2]" id="txt_fundamentacion_2" class="form-control input-sm" rows="2" placeholder="">{if isset($formDB.txt_fundamentacion_2)}{$formDB.txt_fundamentacion_2}{/if}</textarea>
                                                        <label for="txt_fundamentacion_2">2. ¿En qué medida la Capacitación va en relación a los objetivos del área y de la organización?</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- 3 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_3]" id="txt_fundamentacion_3" class="form-control input-sm" rows="2" placeholder="">{if isset($formDB.txt_fundamentacion_3)}{$formDB.txt_fundamentacion_3}{/if}</textarea>
                                                        <label for="txt_fundamentacion_3">3. Días y horarios mas factibles para la capacitación</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- 4 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_4]" id="txt_fundamentacion_4" class="form-control input-sm" rows="2" placeholder="">{if isset($formDB.txt_fundamentacion_4)}{$formDB.txt_fundamentacion_4}{/if}</textarea>
                                                        <label for="txt_fundamentacion_4">4. ¿Qué se espera después de la capacitación?</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- 5 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_5]" id="txt_fundamentacion_5" class="form-control input-sm" rows="2" placeholder="">{if isset($formDB.txt_fundamentacion_5)}{$formDB.txt_fundamentacion_5}{/if}</textarea>
                                                        <label for="txt_fundamentacion_5">5. ¿Como hace hoy si Trabajo?</label>
                                                    </div>
                                                </div>

                                            </div>
                                            <!-- 6 -->
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <textarea name="form[alphaNum][txt_fundamentacion_6]" id="txt_fundamentacion_6" class="form-control input-sm" rows="2" placeholder="">{if isset($formDB.txt_fundamentacion_6)}{$formDB.txt_fundamentacion_6}{/if}</textarea>
                                                        <label for="txt_fundamentacion_6">6. ¿Hay colaboradores dentro de la Institución que pueden dictar este curso?</label>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>

                                    </div><!-- fin fundamentación -->

                                    <!-- PARTICIPANTES -->
                                    <div class="tab-pane" id="step3">

                                        <br><br>
                                        <div class="col-xs-12">


                                            <div class="row text-center">

                                                <button type="button" class="btn btn-primary ink-reaction btn-raised" id="insertarEmpleado" descripcion="Agregar Empleado" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#formModal2"
                                                        titulo="Agregar Empleado">Insertar Empleado</button>


                                            </div>

                                            <br>


                                            <div class="col-lg-12">

                                                <table id="empleadosBeneficiarios" class="table table-striped no-margin table-condensed" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th class="col-sm-1">Emp.</th>
                                                        <th class="col-sm-4">Nombre Completo</th>
                                                        <th class="col-sm-1">Total Dias</th>
                                                        <th class="col-sm-1">Total Horas</th>
                                                        <th class="col-sm-1">Dias Asis.</th>
                                                        <th class="col-sm-1">Apro.</th>
                                                        <th class="col-sm-1">Nota</th>
                                                        <th class="col-sm-1">Importe Gastos</th>
                                                        <th class="col-sm-1">Eliminar</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>

                                            </div>


                                        </div>

                                    </div><!-- fin PARTICIPANTES -->

                                    <!--
                                        HORARIOS
                                    -->
                                    <div class="tab-pane" id="step4">

                                        <br><br>
                                        <div class="col-xs-12">

                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="card-head card-head-xs style-primary">
                                                        <header>Horarios Segun Rangos de Fechas Asignadas</header>
                                                        <div class="tools">
                                                            <a class="btn btn-xs btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                                        </div>
                                                    </div><!--end .card-head -->
                                                    <div class="card-body">

                                                        <div class="col-md-7">

                                                            <div id="grafica">

                                                            </div>

                                                        </div>

                                                        <div class="col-md-5">

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" id="diasT" name="form[int][diasT]" value="" disabled>
                                                                    <label for="diasT">Total Dias</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" id="horasT" name="form[txt][horasT]" value="" disabled>
                                                                    <label for="horasT">Total Horas</label>
                                                                </div>
                                                            </div>

                                                        </div>


                                                    </div><!--end .card-body -->
                                                </div><!--end .card -->
                                            </div><!--end .col -->




                                        </div>

                                    </div><!-- fin  -->


                                    <div class="tab-pane" id="step5">

                                        <br><br>


                                        <div class="col-md-6 col-md-offset-3">

                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary">
                                                    <header>Realiza esta acción si estas completamente seguro del Registro!</header>
                                                    <div class="tools">
                                                        <a class="btn btn-xs btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
                                                    </div>
                                                </div><!--end .card-head -->
                                                <div class="card-body">

                                                    <br>
                                                    <div class="row text-center">
                                                        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                                        <button type="submit" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" id="guardarCapacitacion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>

                                                    </div>
                                                    <br>

                                                </div><!--end .card-body -->
                                            </div><!--end .card -->

                                        </div>




                                    </div>




                                </div>

                                <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                                <ul class="pager wizard">
                                    <li class="previous first" id="primero"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <li class="next last" id="ultimo"><a class="ink-reaction btn-raised" href="javascript:void(0);">Ultimo</a></li>
                                    <li class="next" id="siguiente"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                                </ul>

                            </form>
                        </div>

            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //inicializar el datepicker
    $("#rango-fechas").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");

            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();


            $.post('{$_Parametros.url}modRH/gestion/capacitacionCONTROL/RegistrarCapacitacionMET', datos ,function(dato){

                if(dato['status']=='error'){
                    /*Error para usuarios normales del sistema*/
                    swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    //swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");
                }
                else if(dato['status']=='registrado'){

                    swal("Registro Guardado!", "Capacitación Guardada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $("#_contenido").load('{$_Parametros.url}modRH/gestion/capacitacionCONTROL');

                }

            },'json');


        }
    });


    $(document).ready(function() {

        var rh  = new  AppFunciones();
        var rh2 = new  ModRhFunciones();

        var idPais   = '{if isset($formDBnacimiento.pais_nacimiento)}{$formDBnacimiento.pais_nacimiento}{/if}';
        var idEstado = '{if isset($formDBnacimiento.estado_nacimiento)}{$formDBnacimiento.estado_nacimiento}{/if}';
        var idCiudad = '{if isset($formDBnacimiento.ciudad_nacimiento)}{$formDBnacimiento.ciudad_nacimiento}{/if}';
        var idMunicipio = '{if isset($formDBnacimiento.municipio_nacimiento)}{$formDBnacimiento.municipio_nacimiento}{/if}';

        var idPais2   = '{if isset($formDBdomicilio.pais_domicilio)}{$formDBdomicilio.pais_domicilio}{/if}';
        var idEstado2 = '{if isset($formDBdomicilio.estado_domicilio)}{$formDBdomicilio.estado_domicilio}{/if}';
        var idCiudad2 = '{if isset($formDBdomicilio.ciudad_domicilio)}{$formDBdomicilio.ciudad_domicilio}{/if}';
        var idMunicipio2 = '{if isset($formDBdomicilio.municipio_domicilio)}{$formDBdomicilio.municipio_domicilio}{/if}';
        var idParroquia  = '{if isset($formDBdomicilio.parroquia_domicilio)}{$formDBdomicilio.parroquia_domicilio}{/if}';
        var idSector     = '{if isset($formDBdomicilio.sector_domicilio)}{$formDBdomicilio.sector_domicilio}{/if}';

        var idDependencia = '{if isset($formDBlaborales.fk_a004_num_dependencia)}{$formDBlaborales.fk_a004_num_dependencia}{/if}';
        var idCentroCosto = '{if isset($formDBlaborales.fk_a023_num_centro_costo)}{$formDBlaborales.fk_a023_num_centro_costo}{/if}';
        var idGrupoOcupacional = '{if isset($formDBlaborales.fk_a006_num_miscelaneo_detalle_grupoocupacional)}{$formDBlaborales.fk_a006_num_miscelaneo_detalle_grupoocupacional}{/if}';
        var idSerieOcupacional = '{if isset($formDBlaborales.fk_rhc010_num_serie)}{$formDBlaborales.fk_rhc010_num_serie}{/if}';
        var idCargo = '{if isset($formDBlaborales.fk_rhc063_num_puestos_cargo)}{$formDBlaborales.fk_rhc063_num_puestos_cargo}{/if}';

        rh.metJsonEstado('{$_Parametros.url}estado/jsonEstado',idPais,idEstado);
        rh2.metJsonMunicipioN('{$_Parametros.url}municipio/JsonMunicipio',idEstado,idMunicipio);
        rh2.metJsonCiudadN('{$_Parametros.url}ciudad/JsonCiudad',idMunicipio,idCiudad);


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho').css( "width", "80%" );

        $("#num_monto_estimado").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        $("#num_monto_asumido").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });

        $('#fec_fecha_inicio').mask('99-99-9999');
        $('#fec_fecha_termino').mask('99-99-9999');

        //Script para forzar el avance del wizard por el botón suigiente.
        $(".form-wizard-nav").on('click', function(){
            var form = $('#rootwizard2').find('.form-validation');
            var valid = form.valid();
            if(!valid) {
                form.data('validator').focusInvalid();
                return false;
            }
        });

        var $url = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/ConsultarEmpleadoMET';
        $('#buscarEmpleado').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', accion:'CAPACITACION' },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#insertarEmpleado').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', accion:'INSERTAR_EMPLEADO_CAPACITACION' },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#flag_costos').click(function() {
            if( $('#flag_costos').is(':checked') ) {

                $("#num_monto_estimado").prop("disabled","disabled");
                $("#num_monto_asumido").prop("disabled","disabled");
                $("#num_monto_estimado").val('');
                $("#num_monto_asumido").val('');


            }else{
                $("#num_monto_estimado").removeAttr("disabled");
                $("#num_monto_asumido").removeAttr("disabled");
                $("#num_monto_estimado").focus();
            }
        });

        /*al selecionar fecha final / termino para mostrar los horario en la ultima pestaña*/
        $("#fec_fecha_termino").change(function() {

            var fecha_ini = $("#fec_fecha_inicio").val();
            var fecha_fin = $("#fec_fecha_termino").val();

            $("#grafica").html('');
            $("#horasT").val('');

            var $url_verificar_fechas = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/GraficarHorarioMET';

            $.post($url_verificar_fechas, { fecha_ini:fecha_ini, fecha_fin:fecha_fin },function($dato){


                var cantDias = 0;

                for (var i = 0; i < $dato.length; i++) {




                    var objeto = '<div class="row contain-lg">'+
                                 '<div class="col-md-2">'+
                                 '<div class="form-group">'+
                                 '<input type="text" class="form-control input-sm" id="fecha'+i+'" name="form[txt][fecha]['+i+']" value="'+$dato[i]+'">'+
                                 '<label for="fecha'+i+'">Fecha</label>'+
                                 '</div>'+
                                 '</div>'+
                                 '<div class="col-md-2">'+
                                 '<div class="form-group">'+
                                 '<input type="text" class="form-control input-sm desde'+i+'" id="desde'+i+'" name="form[txt][desde]['+i+']" value="" required>'+
                                 '<label for="desde'+i+'">Desde</label>'+
                                 '</div>'+
                                 '</div>'+
                                 '<div class="col-md-2">'+
                                 '<div class="form-group">'+
                                 '<input type="text" class="form-control input-sm desde'+i+'" id="hasta'+i+'" name="form[txt][hasta]['+i+']" onchange="totalizarHoras('+i+')" value="" required>'+
                                 '<label for="hasta'+i+'">Hasta</label>'+
                                 '</div>'+
                                 '</div>'+
                                 '<div class="col-md-2">'+
                                 '<div class="form-group">'+
                                 '<input type="text" class="form-control input-sm total'+i+'" id="totalH'+i+'" name="form[txt][totalH]['+i+']" value="">'+
                                 '<label for="totalH'+i+'">Total Horas</label>'+
                                 '</div>'+
                                 '</div>';

                    $("#grafica").append(objeto);

                    $(".desde"+i).inputmask('hh:mm t', { placeholder: 'hh:mm xm', alias: 'time12', hourFormat: '12' });

                    cantDias++;

                }

                $("#dias").val(cantDias);
                $("#diasT").val(cantDias);

            },'json');


        });



        var tabla_agregar = $('#empleadosBeneficiarios').DataTable();

        {* ************************************************ *}
        $('#empleadosBeneficiarios tbody').on('click', '.eliminar', function () {

            var obj = this;

            swal({
                title: 'Estas Seguro?',
                text: 'Estas seguro que desea descartar al Empleado(a)',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Descartar",
                closeOnConfirm: false
            }, function () {

                tabla_agregar.row($(obj).parents('tr')).remove().draw();

                {*Elimina la fila en cuestion*}
                swal("Descartado!", "Descartado de la lista el Empleado", "success");


                {*verifico la tablas donde estan los empleados para volver armar el arreglo de empleados*}
                var num_filas = $('#empleadosBeneficiarios >tbody >tr').length;
                var filas = $('#empleadosBeneficiarios >tbody >tr'); {*trae las filas del tbody*}


                var cad_id_empleados = '';
                $('#id_empleados').val('');



                for(var i=0; i<num_filas; i++) {

                    if(filas[i].cells[0].innerHTML=='No Hay Registros Disponibles'){
                        $('#id_empleados').val('')
                    }else{
                        {*voy armando arreglo de empleados*}
                        cad_id_empleados = cad_id_empleados + filas[i].cells[0].innerHTML + '#';
                    }




                }
                {*asigno arreglo de empleados*}
                $('#id_empleados').val(cad_id_empleados);
                var par =  $('#num_participantes').val();
                $('#num_participantes').val(par-1);







            });


        });
        {* **************************************************** *}




    });

    /*permite totalizar la horas de la linea*/
    function totalizarHoras(i)
    {
        var hora_ini = $("#desde"+i).val();
        var hora_fin = $("#hasta"+i).val();

        var $url_totalizar = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/RestarHorasMET';
        var $url_actualizar = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/SumarHorasMET';

        $.post($url_totalizar, { hora_ini:hora_ini, hora_fin:hora_fin, operacion:'json' },function($dato){

            $("#totalH"+i).val($dato);

            fnTotalizar();



        },'json');


    }

    /*permite totalizar las horas de todas la lineas*/
    function fnTotalizar(){

        var fecha_ini = $("#fec_fecha_inicio").val();
        var fecha_fin = $("#fec_fecha_termino").val();

        var $url_verificar_fechas = '{$_Parametros.url}modRH/gestion/capacitacionCONTROL/GraficarHorarioMET';

        $.post($url_verificar_fechas, { fecha_ini:fecha_ini, fecha_fin:fecha_fin },function($dato){

            var AUX = 0;

            for (var i = 0; i < $dato.length; i++) {




                if(i==0){

                    var TL = $("#totalH0").val();
                    AUX = TL;
                    $("#horasT").val(AUX);

                }
                else{

                    var band = $("#totalH"+i).val();
                    if(band==''){
                        TL='00:00:00'
                    }else{
                        TL=band;
                    }

                    var tl = TL.split(":");//total de la linea descompuesto en un arreglo
                    var au = AUX.split(":");//total de horas descompuesto en un arreglo

                    var auxH = parseFloat(tl[0]) + parseFloat(au[0]);
                    var auxM = parseFloat(tl[1]) + parseFloat(au[1]);

                    if(auxM >= 60){

                        auxH = auxH + 1;
                        auxM = auxM - 60;

                    }

                    auxH = ('0'+auxH).slice(-2);
                    auxM = ('0'+auxM).slice(-2);

                    AUX = auxH+":"+auxM+":00";

                }

            }

            $("#horasT").val(AUX);
            $("#horas").val(AUX);


        },'json');

    }



</script>