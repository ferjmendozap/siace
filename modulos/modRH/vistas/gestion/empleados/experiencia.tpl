{if $Empleado==0}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->
{else}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->


    <div class="well clearfix">
        <div class="col-sm-12">
            <h4 class="text-primary text-center">Datos del Empleado</h4>
        </div>
        <div class="col-sm-12">
            Empleado: {$Empleado}
        </div>
        <div class="col-sm-12">
            Nombres y Apellidos: {$Nombres}
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <input type="hidden" name="idEmpleado" id="idEmpleado" value="{$Empleado}">
            <table id="datatable2" class="table table-striped no-margin table-condensed">
                <thead>
                <tr>
                    <th width="30%">Empresa</th>
                    <th width="30%">Cargo</th>
                    <th width="15%">Desde</th>
                    <th width="15%">Hasta</th>
                    <th width="5%">Mod</th>
                    <th width="5%">Eli</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=dat from=$listadoExperienciaLaboral}
                    <tr id="idExperienciaLaboral{$dat.pk_num_experiencia}" valor="{$dat.pk_num_experiencia}" style="font-size: 11px">
                        <td width="30%">{$dat.ind_empresa}</td>
                        <td width="30%">{$dat.ind_cargo}</td>
                        <td width="15%">{$dat.fec_ingreso|date_format:"d-m-Y"}</td>
                        <td width="15%">{$dat.fec_egreso|date_format:"d-m-Y"}</td>
                        <td width="5%">
                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                    data-keyboard="false" data-backdrop="static" title="Editar Experiencia" idExperienciaLaboral="{$dat.pk_num_experiencia}"
                                    descipcion="El Usuario a Modificado Experiencia Laboral" titulo="Modificar Experiencia Laboral">
                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                            </button>
                        </td>
                        <td width="5%">
                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idExperienciaLaboral="{$dat.pk_num_experiencia}"
                                    descipcion="El usuario a eliminado Experiencia Laboral" title="Eliminar Experiencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Experiencia Laboral!!">
                                <i class="md md-delete" style="color: #ffffff;"></i>
                            </button>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                    <th colspan="6">
                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                descripcion="el Usuario a creado una Experiencia Laboral" title="Nueva Experiencia Laboral"  titulo="Nueva Experiencia Laboral" id="nuevaExperienciaLaboral" >
                            <i class="md md-create"></i> Nueva Experiencia &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                    </th>
                </tfoot>
            </table>
        </div>
    </div>
{/if}




<script type="text/javascript">

    $(document).ready(function() {

        //ancho de la ventana modal
        $('#modalAncho').css( "width", "70%" );

        //inicializo el datatbales
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        var $url='{$_Parametros.url}modRH/gestion/experienciaLaboralCONTROL/ExperienciaLaboralMET';

        //al dar click en nueva carga
        $('#nuevaExperienciaLaboral').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });

        //al dar click boton modificar del listado de carga familiar
        $('#datatable2 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idExperienciaLaboral: $(this).attr('idExperienciaLaboral')},function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        //al dar click boton eliminar del listado de carga familiar
        $('#datatable2 tbody').on( 'click', '.eliminar', function () {

            var idExperienciaLaboral = $(this).attr('idExperienciaLaboral');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/experienciaLaboralCONTROL/EliminarExperienciaLaboralMET';
                $.post($url, { idExperienciaLaboral: idExperienciaLaboral},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idExperienciaLaboral'+$dato['idExperienciaLaboral'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();
                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

    });

</script>