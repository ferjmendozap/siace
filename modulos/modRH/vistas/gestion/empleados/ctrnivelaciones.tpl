{if $Empleado==0}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->
{else}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->


    <div class="well clearfix">
        <div class="col-sm-12">
            <h4 class="text-primary text-center">Datos del Empleado</h4>
        </div>
        <div class="col-sm-12">
            Empleado: {$Empleado}
        </div>
        <div class="col-sm-12">
            Nombres y Apellidos: {$Nombres}
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <input type="hidden" name="idEmpleado" id="idEmpleado" value="{$Empleado}">
            <table id="datatable2" class="table table-striped no-margin table-condensed">
                <thead>
                <tr>
                    <th width="5%">Fecha</th>
                    <th width="20%">Tipo</th>
                    <th width="20%">Dependencia</th>
                    <th width="20%">Cargo</th>
                    <th width="15%">Categoria</th>
                    <th width="15%">Nomina</th>

                </tr>
                </thead>
                <tbody>
                {foreach item=dat from=$listadoHistorialNivelaciones}
                    <tr id="idNivelacion{$dat.pk_num_empleado_nivelacion_historial}" valor="{$dat.pk_num_empleado_nivelacion_historials}" style="font-size: 11px">
                        <td width="5%">{$dat.fec_fecha_registro|date_format:"d-m-Y"}</td>
                        <td width="20%">{$dat.ind_tipo_accion}</td>
                        <td width="20%">{$dat.ind_dependencia}</td>
                        <td width="20%">{$dat.ind_cargo}</td>
                        <td width="15%">{$dat.ind_categoria_cargo}</td>
                        <td width="15%">{$dat.ind_tipo_nomina}</td>

                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                    <th colspan="6">
                        <button class="btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                descripcion="" title="Reporte Historial de Nivelaciones"  titulo="Reporte Historial Nivelaciones del Empleado" id="historialNivelaciones" >
                            <i class="md-file-download"></i> Generar Reporte &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                descripcion="el Usuario a realizado Nivelación del Empleado" title="Nueva Nivelación" titulo="Nueva Nivelación" id="nuevaNivelacion" >
                            <i class="md md-create"></i> Nueva Nivelación &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                    </th>
                </tfoot>
            </table>
        </div>
    </div>


{/if}


<script type="text/javascript">

    $(document).ready(function() {

        //ancho de la ventana modal
        $('#modalAncho').css( "width", "80%" );

        //inicializo el datatbales
        var tabla = $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        var $url='{$_Parametros.url}modRH/gestion/nivelacionesCONTROL/NivelacionesMET/';


        //al dar click en nueva carga
        $('#nuevaNivelacion').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });

        var $url_reporte='{$_Parametros.url}modRH/gestion/nivelacionesCONTROL/ReporteHistorialNivelacionMET/{$Empleado}';

        //al dar click en generar reporte
        $('#historialNivelaciones').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('<iframe src="'+$url_reporte+'" width="100%" height="950"></iframe>');
        });





    });


</script>