{if $Empleado==0}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->
{else}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->


    <div class="well clearfix">
        <div class="col-sm-12">
            <h4 class="text-primary text-center">Datos del Empleado</h4>
        </div>
        <div class="col-sm-12">
            Empleado: {$Empleado}
        </div>
        <div class="col-sm-12">
            Nombres y Apellidos: {$Nombres}
        </div>
    </div>

    <div class="card">
        <div class="card-head">
            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active"><a href="#carreras">Carreras</a></li>
                <li><a href="#cursos">Otros Estudios</a></li>
                <li><a href="#idiomas">Idiomas</a></li>
            </ul>
        </div><!--end .card-head -->
        <div class="card-body tab-content">

            <!--INMUEBLES-->
            <div class="tab-pane active" id="carreras">

                <table id="datatableCarreras" class="table table-striped no-margin table-condensed">
                    <thead>
                    <tr>
                        <th width="25%">Grado de Instrucción</th>
                        <th width="25%">Profesión</th>
                        <th width="25%">Centro de Estudio</th>
                        <th width="15%">Fec. Graduación</th>
                        <th width="5%">Mod</th>
                        <th width="5%">Eli</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=dat from=$listadoCarreras}

                        <tr id="idInstruccionCarreras{$dat.pk_num_empleado_instruccion}" valor="{$dat.pk_num_empleado_instruccion}" style="font-size: 11px">
                            <td width="25%">{$dat.grado_instruccion}</td>
                            <td width="25%">{$dat.ind_nombre_profesion}</td>
                            <td width="25%">{$dat.ind_nombre_institucion}</td>
                            <th width="15%">{$dat.fec_graduacion|date_format:"d-m-Y"}</th>
                            <td width="5%">
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                        data-keyboard="false" data-backdrop="static" title="Editar" idInstruccionCarreras="{$dat.pk_num_empleado_instruccion}"
                                        descipcion="El Usuario a Modificado Carrera" titulo="Modificar Carrera del Empleado">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            </td>
                            <td width="5%">
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idInstruccionCarreras="{$dat.pk_num_empleado_instruccion}"
                                        descipcion="El usuario a eliminado Carrera" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Carrera del Empleado!!">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <th colspan="6">
                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                    descripcion="el Usuario a creado un Nueva Carrera" title="Nueva carrera" titulo="Nueva carrera" id="nuevaCarrera" >
                                <i class="md md-create"></i> Nueva Carrera &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </button>
                        </th>
                    </tfoot>
                </table>
            </div>

            <!--INVERSIÓN-->
            <div class="tab-pane" id="cursos">

                <table id="datatableOtrosEstudios" class="table table-striped no-margin table-condensed">
                    <thead>
                    <tr>
                        <th width="25%">Curso</th>
                        <th width="25%">Centro Formación</th>
                        <th width="15%">Horas</th>
                        <th width="15%">Desde</th>
                        <th width="15%">Hasta</th>
                        <th width="5%">Mod</th>
                        <th width="5%">Eli</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=dat from=$listadoOtros}

                        <tr id="idInstruccionOtrosEstudios{$dat.pk_num_empleado_curso}" valor="{$dat.pk_num_empleado_curso}" style="font-size: 11px">
                            <td width="25%">{$dat.ind_nombre_curso}</td>
                            <td width="25%">{$dat.ind_nombre_institucion}</td>
                            <td width="15%">{$dat.num_horas}</td>
                            <td width="15%">{$dat.fec_desde|date_format:"d-m-Y"}</td>
                            <td width="15%">{$dat.fec_hasta|date_format:"d-m-Y"}</td>
                            <td width="5%">
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                        data-keyboard="false" data-backdrop="static" title="Editar" idInstruccionOtrosEstudios="{$dat.pk_num_empleado_curso}"
                                        descipcion="El Usuario a Modificado Otros Estudios" titulo="Modificar Otros Estudios">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            </td>
                            <td width="5%">
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idInstruccionOtrosEstudios="{$dat.pk_num_empleado_curso}"
                                        descipcion="El usuario a eliminado Otros Estudios" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Otros Estudios!!">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <th colspan="7">
                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                    descripcion="el Usuario a creado un Curso del empleado" title="Nuevo Registro Otros Estudios"  titulo="Nuevo Registro Otros Estudios" id="nuevoOtrosEstudios" >
                                <i class="md md-create"></i> Nuevo Registro &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </button>
                        </th>
                    </tfoot>
                </table>
            </div>

            <!--VEHICULO-->
            <div class="tab-pane" id="idiomas">

                <table id="datatableIdiomas" class="table table-striped no-margin table-condensed">
                    <thead>
                    <tr>
                        <th width="25%">Idioma</th>
                        <th width="15%">Lectura</th>
                        <th width="15%">Oral</th>
                        <th width="15%">Escritura</th>
                        <th width="15%">General</th>
                        <th width="5%">Mod</th>
                        <th width="5%">Eli</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=dat from=$listadoIdiomas}

                        <tr id="idInstruccionIdiomas{$dat.pk_num_empleado_idioma}" valor="{$dat.pk_num_empleado_idioma}" style="font-size: 11px">
                            <td width="25%">{$dat.ind_nombre_idioma}</td>
                            <td width="15%">{$dat.lectura}</td>
                            <td width="15%">{$dat.oral}</td>
                            <td width="15%">{$dat.escritura}</td>
                            <td width="15%">{$dat.general}</td>
                            <td width="5%">
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                        data-keyboard="false" data-backdrop="static" title="Editar" idInstruccionIdiomas="{$dat.pk_num_empleado_idioma}"
                                        descipcion="El Usuario a Modificado Idiomas Empleado" titulo="Modificar Idiomas del Empleado">
                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>
                            </td>
                            <td width="5%">
                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idInstruccionIdiomas="{$dat.pk_num_empleado_idioma}"
                                        descipcion="El usuario a eliminado Idiomas Empleado" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Idiomas del Empleado!!">
                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                        <th colspan="7">
                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                    descripcion="el Usuario a creado un Idioma del Empleado" title="Nuevo Registro Idioma del Empleado"  titulo="Nuevo Registro Idioma del Empleado" id="nuevoIdioma" >
                                <i class="md md-create"></i> Nuevo Idioma &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </button>
                        </th>
                    </tfoot>
                </table>
            </div>


        </div><!--end .card-body -->
    </div><!--end .card -->

{/if}


<script type="text/javascript">

    $(document).ready(function() {

        //ancho de la ventana modal
        $('#modalAncho').css( "width", "70%" );


        //inicializo el datatbales
        $('#datatableCarreras').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatableOtrosEstudios').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatableIdiomas').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        //url para cada caso
        var $urlCarreras='{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/CarrerasMET';
        var $urlOtrosEstudios='{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/OtrosEstudiosMET';
        var $urlIdiomas ='{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/IdiomasMET';

        var tabla_carreras = $('#datatableCarreras').DataTable();
        var tabla_idiomas  = $('#datatableIdiomas').DataTable();
        var tabla_otros    = $('#datatableOtrosEstudios').DataTable();
        /*******************************INSTRUCCION CARRERAS***********************************************************************/
            //al dar click en nuevo patrimonio
        $('#nuevaCarrera').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlCarreras,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#datatableCarreras tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlCarreras,{ idInstruccionCarreras: $(this).attr('idInstruccionCarreras'), idEmpleado:{$Empleado} },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#datatableCarreras tbody').on( 'click', '.eliminar', function () {

            var idInstruccionCarreras = $(this).attr('idInstruccionCarreras');
            var obj = this;

            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/EliminarInstruccionMET';
                $.post($url, { idInstruccionCarreras: idInstruccionCarreras, tipo:'CARRERAS' },function($dato){
                    if($dato['status']=='OK'){
                        //$(document.getElementById('idInstruccionCarreras'+$dato['idInstruccionCarreras'])).html('');
                        tabla_carreras.row( $(obj).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();
                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        /*******************************INSTRUCCION CARRERAS***********************************************************************/

        /*******************************INSTRUCCION OTROS ESTUDIOS***********************************************************************/
            //al dar click en nuevo patrimonio
        $('#nuevoOtrosEstudios').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlOtrosEstudios,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#datatableOtrosEstudios tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlOtrosEstudios,{ idInstruccionOtrosEstudios: $(this).attr('idInstruccionOtrosEstudios'), idEmpleado:{$Empleado}},function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#datatableOtrosEstudios tbody').on( 'click', '.eliminar', function () {

            var idInstruccionOtrosEstudios = $(this).attr('idInstruccionOtrosEstudios');
            var obj = this;
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/EliminarInstruccionMET';
                $.post($url, { idInstruccionOtrosEstudios: idInstruccionOtrosEstudios, tipo:'OTROSESTUDIOS' },function($dato){
                    if($dato['status']=='OK'){
                        //$(document.getElementById('idInstruccionOtrosEstudios'+$dato['idInstruccionOtrosEstudios'])).html('');
                        tabla_otros.row( $(obj).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();
                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        /*******************************INSTRUCCION OTROS ESTUDIOS***********************************************************************/


        /*******************************IDIOMAS***********************************************************************/
            //al dar click en nuevo
        $('#nuevoIdioma').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlIdiomas,{ idEmpleado:{$Empleado}},function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#datatableIdiomas tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($urlIdiomas,{ idInstruccionIdiomas: $(this).attr('idInstruccionIdiomas'), idEmpleado:{$Empleado} },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#datatableIdiomas tbody').on( 'click', '.eliminar', function () {

            var idInstruccionIdiomas = $(this).attr('idInstruccionIdiomas');
            var obj = this;
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/instruccionEmpleadoCONTROL/EliminarInstruccionMET';
                $.post($url, { idInstruccionIdioma: idInstruccionIdiomas, tipo:'IDIOMAS' },function($dato){
                    if($dato['status']=='OK'){
                        //$(document.getElementById('idInstruccionIdioma'+$dato['idInstruccionIdioma'])).html('');
                        tabla_idiomas.row( $(obj).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();
                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        /*******************************IDIOMAS***********************************************************************/

    });

</script>