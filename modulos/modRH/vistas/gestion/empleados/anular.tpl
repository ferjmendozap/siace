<div class="col-md-12">
<form id="formAjax" class="form form-validation" role="form" novalidate="novalidate">

    <input type="hidden" name="idEmpleado" id="idEmpleado" value="{$idEmpleado}">
    <input type="hidden" name="estado_nuevo" id="estado_nuevo" value="{$estado_nuevo}">
    <input type="hidden" name="estado_actual" id="estado_actual" value="{$estado_actual}">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <textarea name="form[alphaNum][ind_motivo_anulacion]" id="ind_motivo_anulacion" class="form-control" rows="3" placeholder="" required data-msg-required="Indique Motivo de Anulación" style="text-transform: uppercase"></textarea>
                <label for="ind_motivo_anulacion">Indique Motivo de Anulación</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>
    </div>


    <div class="row">
        <div align="center">
            <button type="button" class="btn btn-default ink-reaction btn-raised" descipcionModal="Aceptar" id="guardar">Aceptar</button>
        </div>
    </div>
</form>
</div>



<script type="text/javascript">


    $(document).ready(function() {

        //ancho de la ventana
        $('#modalAncho2').css( "width", "45%" );

        var app = new  AppFunciones();

        $("#guardar").click(function(){

            if($("#ind_motivo_anulacion").val()==''){
                swal("Advertencia!","Debe Indicar Motivo de Anulación", "warning");
                return false;
            }else{

                if( $("#estado_actual").val()== 'PR'){
                    var mensaje = 'Esta Realmente seguro de Anular el registro? Despues de anularlo en estado Preparado no podrá volver a utlizar el registro';
                    var estado  = 'AN';
                }else{
                    var mensaje = 'Esta Realmente seguro de Anular el registro? El Registro volvera a Estado Preparado';
                    var estado  = 'PR';
                }
                swal({
                    title: 'Aviso',
                    text: mensaje,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){
                    var $url='{$_Parametros.url}modRH/gestion/empleadosCONTROL/AnularOperacionMET';
                    $.post($url, { idEmpleado: {$idEmpleado}, estado_nuevo:estado, motivo: $("#ind_motivo_anulacion").val() },function($dato){
                        if($dato['status']=='anular'){
                            swal("Registro Anulado!", 'Operación Realizada Satisfactoriamente.', "success");
                            $('#listadoE').dataTable().api().row().remove().draw(false);
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                            $(document.getElementById('cerrarModal2')).click();
                            $(document.getElementById('ContenidoModal2')).html('');
                            $('#cerrar').click();
                        }
                    },'json');
                });

            }

        });




    });

</script>