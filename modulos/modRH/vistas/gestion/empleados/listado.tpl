<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Empleados {if $estatus == 'PR'} (Revisar Registro) {else} (Aprobar Registro) {/if}</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">

                        <div class="card-body">
                            <table id="listadoE" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>...</th>
                                    <th class="col-sm-1">Cedula</th>
                                    <th class="col-sm-3">Nombre Completo</th>
                                    <th class="col-sm-3">Dependencia</th>
                                    <th class="col-sm-3">Cargo</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>


                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/gestion/empleadosCONTROL/EmpleadoMET';
        var $url_info='{$_Parametros.url}modRH/gestion/empleadosCONTROL/InformacionRegistroMET'


        /*cargo el listado de registros*/
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#listadoE',
                "{$_Parametros.url}modRH/gestion/empleadosCONTROL/JsonDataTablaEmpleadosMET/{$estatus}",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_foto" },
                    { "data": "ind_cedula_documento" },
                    { "data": "nombre_empleado" },
                    { "data": "ind_dependencia" },
                    { "data": "ind_descripcion_cargo" },
                    { "data": "ind_estado_aprobacion" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );

        /*REVISAR EMPLEADO*/
        $('#listadoE tbody').on( 'click', '.revisar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idEmpleado: $(this).attr('idEmpleado'), operacion:'revisar'},function($dato){
                    $('#ContenidoModal').html($dato);
                    $(".form-control").attr("readonly","readonly");
                    $("#nuevaDireccion").hide();
                    $("#nuevoTelefono").hide();
                    $("#nuevoTelefono1").hide();
                    $("#nuevoTelefono2").hide();
                    $("#nuevaLic").hide();
                });
        });
        /*APROBAR EMPLEADO*/
        $('#listadoE tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idEmpleado: $(this).attr('idEmpleado'), operacion:'aprobar'},function($dato){
                $('#ContenidoModal').html($dato);
                $(".form-control").attr("readonly","readonly");
                $("#nuevaDireccion").hide();
                $("#nuevoTelefono").hide();
                $("#nuevoTelefono1").hide();
                $("#nuevoTelefono2").hide();
                $("#nuevaLic").hide();
            });
        });


        /*VER INFORMACION DEL EMPLEADO*/
        $('#listadoE tbody').on( 'click', '.info', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_info,{ idEmpleado: $(this).attr('idEmpleado')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });


        /*MODIFICAR DATOS DEL DEL EMPLEADO*/
        $('#listadoE tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idEmpleado: $(this).attr('idEmpleado'), operacion:'modificarp'},function($dato){
                $('#ContenidoModal').html($dato);

            });
        });

    });
</script>