{if $Empleado==0}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->
{else}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->


    <div class="well clearfix">
        <div class="col-sm-12">
            <h4 class="text-primary text-center">Datos del Empleado</h4>
        </div>
        <div class="col-sm-12">
            Empleado: {$Empleado}
        </div>
        <div class="col-sm-12">
            Nombres y Apellidos: {$Nombres}
        </div>
    </div>

    <div class="card">

        <div class="card-body">
            <input type="hidden" name="idEmpleado" id="idEmpleado" value="{$Empleado}">
            <table id="datatable2" class="table table-striped no-margin table-condensed">
                <thead>
                <tr>
                    <th>Fecha Ing.</th>
                    <th>Dependencia</th>
                    <th>Cargo</th>
                    <th>Categoria</th>
                    <th>Sueldo</th>
                    <th>Nomina</th>
                    <th>Tipo Pago</th>
                    <th>Tipo Trab.</th>

                </tr>
                </thead>
                <tbody>
                {foreach item=dat from=$listadoHistorial}
                    <tr style="font-size: 11px">
                        <td>{$dat.fec_ingreso|date_format:"d-m-Y"}</td>
                        <td>{$dat.ind_dependencia}</td>
                        <td>{$dat.ind_cargo}</td>
                        <td>{$dat.ind_categoria_cargo}</td>
                        <td>{$dat.num_nivel_salarial|number_format:2:",":"."}</td>
                        <td>{$dat.ind_tipo_nomina}</td>
                        <td>{$dat.ind_tipo_pago}</td>
                        <td>{$dat.ind_tipo_trabajador}</td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                    <th colspan="8">
                        <button class="btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                descripcion="" title="Reporte Historial del Empleado"  titulo="Reporte Historial del Empleado" id="historialEmpleado" >
                            <i class="md-file-download"></i> Generar Reporte &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                    </th>
                </tfoot>
            </table>
        </div>
    </div>


{/if}


<script type="text/javascript">


    $(document).ready(function() {

        //ancho de la ventana modal
        $('#modalAncho').css("width", "80%");

        //inicializo el datatbales
        var tabla = $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });


        var $url='{$_Parametros.url}modRH/gestion/historialCONTROL/ReporteHistorialMET/{$Empleado}';

        //al dar click en generar reporte
        $('#historialEmpleado').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('<iframe src="'+$url+'" width="100%" height="950"></iframe>');
        });

    });//fin document.ready

</script>