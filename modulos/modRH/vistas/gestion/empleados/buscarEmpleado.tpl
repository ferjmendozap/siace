<input type="hidden" id="accion" name="accion" value="{$accion}">
<table id="empleadoListado" class="table table-striped table-hover">
	<thead>
	<tr align="center">
		<th class="col-sm-1">Cedula</th>
		<th class="col-sm-5">Nombre Completo</th>
		<th class="col-sm-4">Dependencia</th>
		<th class="col-sm-1">Est. Registro</th>
		<th class="col-sm-1">Sit. Trabajo</th>
	</tr>
	</thead>
	<tbody>
	{assign var="numero" value="1"}
	{foreach item=datos from=$datosTrabajador}
		<tr pk_num_empleado="{$datos.pk_num_empleado}">
			<td>{$datos.ind_cedula_documento}</td>
			<td>{$datos.ind_nombre1} {$datos.ind_nombre2} {$datos.ind_apellido1} {$datos.ind_apellido2} </td>
			<td>{$datos.ind_dependencia}</td>
			<td>{if $datos.estado_registro==1}Activo{else}No Activo{/if}</td>
			<td>{if $datos.situacion_trabajo==1}Activo{else}No Activo{/if}</td>
		</tr>
	{/foreach}
	</tbody>
</table>
</section>
<style type="text/css">
	tr{
		cursor: pointer;
	}
</style>
<script type="text/javascript">

	// Inicializador de DataTables
	$(".fecha").datepicker({
		format: 'dd/mm/yyyy',
	});

    $(document).ready(function() {

        //inicializo el datatbales empleados
        var tabla = $('#empleadoListado').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "paging":true
        });


        $('#empleadoListado tbody').on( 'click', 'tr', function () {

            var ACCION = $("#accion").val();
            var empleado = $(this).attr('pk_num_empleado');


            if (ACCION=='RETENCION_JUDICIAL') {

                var $urlCargar='{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/ObtenerDatosEmpleadoMET';
                $.post($urlCargar,{ idEmpleado: empleado},function(dato) {
                    var $nombre = dato['nombreTrabajador'];
                    $("#fk_rhb001_num_empleado").val(dato['id_empleado']);
                    $("#nombreTrabajador").val($nombre);

                },'json');

            }

            else if (ACCION=='FIRMA_ELECTRONICA') {

                var $urlCargar='{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/ObtenerDatosEmpleadoMET';
                $.post($urlCargar,{ idEmpleado: empleado},function(dato) {
                    $("#fk_rhb001_empleado_receptor").val(dato['id_empleado']);
                    $("#nombreTrabajador").val(dato['nombreTrabajador']);
                    $("#fk_a004_num_dependencia_receptor").val(dato['id_dependencia']);

                },'json');

                $(document.getElementById('cerrarModal3')).click();
                $(document.getElementById('ContenidoModal3')).html('');

            }

            else if (ACCION=='MERITOS-DEMERITOS'){

                var $urlCargar='{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/ObtenerDatosEmpleadoMET';
                $.post($urlCargar,{ idEmpleado: empleado },function(dato) {
                    var $nombre = dato['nombreTrabajador'];
                    $("#fk_rhb001_num_empleado_responsable").val(dato['id_empleado']);
                    $("#nombreTrabajador").val($nombre);
                },'json');

            }

            else if (ACCION=='CAPACITACION'){

                var $urlCargar='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/ObtenerDatosEmpleadoMET';
                $.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato) {
                    var $nombre = dato['nombreTrabajador'];
                    $("#fk_rhb001_empleado_solicitante").val(dato['id_empleado']);
                    $("#nombreTrabajador").val($nombre);

                },'json');

            }

            else if (ACCION=='INSERTAR_EMPLEADO_CAPACITACION'){

                var tabla_beneficiarios = $('#empleadosBeneficiarios').DataTable();

                var $urlCargar='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/ObtenerDatosEmpleadoMET';
                $.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato) {



                    var pk_num_empleado = dato['id_empleado'];{*celda 0 que contiene el codigo de la fila*}

                    var num_filas = $('#empleadosBeneficiarios >tbody >tr').length;
                    var filas = $('#empleadosBeneficiarios >tbody >tr'); {*trae las filas del tbody*}
                    var band = 0;
                    for(var i=0; i<num_filas; i++){
                        if(filas[i].cells[0].innerHTML == pk_num_empleado){

                            swal({  {*Mensaje personalizado*}
                                title: '',
                                text: 'Ya está agregado',
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: 'Aceptar',
                                closeOnConfirm: true
                            });

                            band = 1;
                            break;
                        }


                    }

                    var vacantes = $('#num_vacantes').val();

                    var cad_id_empleados = $('#id_empleados').val();
                    var cad_id_dependencia = $('#id_dependencias').val();

                    if(band == 0){

                        band = 0;

                        //alert(num_filas+"-"+vacantes);

                        for(var i=0; i<num_filas; i++) {

                            if (filas[i].cells[0].innerHTML == 'No Hay Registros Disponibles') {

                                tabla_beneficiarios.row.add([
                                    dato['id_empleado'], {*this representa el array (rows) *}
                                    dato['nombreTrabajador'],
                                    '',
                                    '',
                                    '',
                                    '',
                                    '0,00',
                                    '0,00',
                                    '<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'
                                ]).draw(false);


                                cad_id_empleados = cad_id_empleados + dato['id_empleado'] + '#';
                                cad_id_dependencia = cad_id_dependencia + dato['id_dependencia'] + '#';

                            } else if (num_filas < vacantes) {

                                tabla_beneficiarios.row.add([
                                    dato['id_empleado'], {*this representa el array (rows) *}
                                    dato['nombreTrabajador'],
                                    '',
                                    '',
                                    '',
                                    '',
                                    '0,00',
                                    '0,00',
                                    '<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'
                                ]).draw(false);


                                cad_id_empleados = cad_id_empleados + dato['id_empleado'] + '#';
                                cad_id_dependencia = cad_id_dependencia + dato['id_dependencia'] + '#';

                            } else {

                                swal("Advertencia!", "El número de participantes no puede ser mayor al número de vacantes.", "error");
                                return false;
                            }

                        }


                    }

                    $('#id_empleados').val(cad_id_empleados);
                    $('#id_dependencias').val(cad_id_dependencia);

                    var par = cad_id_empleados.split("#");
                    $('#num_participantes').val(par.length-1);


                },'json');

            }
            else if (ACCION=='PENSION_INVALIDEZ'){

                var $urlCargar='{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/ObtenerDatosEmpleadoMET';
                $.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato){
                    var $nombre = dato['nombreTrabajador'];
                    var edad = calcular_edad(dato['fec_nacimiento']);
                    $("#fk_rhb001_num_empleado").val(dato['id_empleado']);
                    $("#ind_organismo").val(dato['organismo']);
                    $("#fk_a001_num_organismo").val(dato['fk_a001_num_organismo']);
                    $("#ind_dependencia").val(dato['ind_dependencia']);
                    $("#fk_a004_num_dependencia").val(dato['fk_a004_num_dependencia']);
                    $("#ind_cargo").val(dato['cargo']);
                    $("#fk_rhc063_num_puestos").val(dato['fk_rhc063_num_puestos_cargo']);
                    $("#nombreTrabajador").val($nombre);
                    $("#pkNumEmpleado").val(dato['pk_num_empleado']);
                    $("#cedula").val(dato['ind_cedula_documento']);
                    $("#ind_sexo").val(dato['sexo']);
                    $("#sexo").val(dato['fk_a006_num_miscelaneo_detalle_sexo']);
                    $("#nacimiento").val(dato['fec_nacimiento']);
                    $("#num_edad").val(edad);
                    $("#fec_fecha_ingreso").val(dato['fec_ingreso']);
                    $("#num_ultimo_sueldo").val(dato['num_sueldo_basico']);
                    $("#num_anio_servicio").val(dato['anio_serv']);
                    $("#fk_nmb001_num_tipo_nomina").val(dato['nomina']);
                    $("#fk_a006_num_miscelaneo_detalle_tipotrab").val(dato['tipo_trabajador']);
                    $("#fk_a006_num_miscelaneo_detalle_motivopension").val(dato['fk_a006_num_miscelaneo_detalle_motivopension']);
                    $("#fk_a006_num_miscelaneo_detalle_tipopension").val(dato['fk_a006_num_miscelaneo_detalle_tipopension']);
                    $("#ind_detalle_tipopension").val(dato['tipo_pension']);
                    $("#ind_detalle_motivopension").val(dato['motivo_pension']);

                    {*verifico si el empleado cumple con los requisitos para optar por la pension*}
                    var $url_verificar = '{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/VerificarRequisitosMET';
                    $.post($url_verificar, { idEmpleado: $('#fk_rhb001_num_empleado').val(), tipo:'REQUISITOS', fecha_nac: dato['fec_nacimiento'] , fecha_ing: dato['fec_ingreso'] }, function(resultado){

                        $requisito = resultado['requisito'];
                        if($requisito==1){
                            $('#requisitoVer').html('<div class="alert alert-success"><strong><i class="glyphicon glyphicon-ok"></i></strong> El empleado cumple con los requisitos para optar a la Pensión.</div>');
                            $('#botonGuardar').show();
                            $("#num_monto_pension").removeAttr('disabled');
                        } else {
                            $('#botonGuardar').hide();
                            $('#requisitoVer').html('<div class="alert alert-danger"><strong><i class="glyphicon glyphicon-alert"></i></strong> El Empleado NO cumple con los requisitos para optar a la Pensión.</div>');
                        }


                    },'json');

                    var hoy = new Date();
                    var fecha = hoy.getDate()+"-"+(hoy.getMonth()+1)+"-"+hoy.getFullYear();

                    if($("#proceso").val()=='Nuevo'){
                        $("#fk_nmb001_num_tipo_nomina").attr('disabled','disabled');
                        $("#fk_a006_num_miscelaneo_detalle_tipotrab").attr('disabled','disabled');
                        $("#ind_resolucion").attr('disabled','disabled');
                        $("#num_monto_pension").attr('disabled','disabled');
                        $("#fec_fecha_egreso").attr('disabled','disabled');
                        $("#fk_rhc032_num_motivo_cese").attr('disabled','disabled');
                        $("#txt_observacion_egreso").attr('disabled','disabled');
                        $("#txt_observacion_pre").removeAttr('disabled');
                        $("#fecha_preparado").val(fecha);
                        $("#preparado_por").val('{$nombreUsuario}');
                        $("#usuario_preparado_por").val({$idUsuario});
                    }


                },'json');

            }
            else if (ACCION=='PENSIONADO_SOBREVIVIENTE'){

                // **********************************************PASO 1*************************************************
                var $urlCargar='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ObtenerDatosEmpleadoMET';
                $.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato){
                    var $nombre = dato['nombreTrabajador'];
                    var edad = calcular_edad(dato['fec_nacimiento']);
                    $("#fk_rhb001_num_empleado").val(dato['id_empleado']);
                    $("#ind_organismo").val(dato['organismo']);
                    $("#fk_a001_num_organismo").val(dato['fk_a001_num_organismo']);
                    $("#ind_dependencia").val(dato['ind_dependencia']);
                    $("#fk_a004_num_dependencia").val(dato['fk_a004_num_dependencia']);
                    $("#ind_cargo").val(dato['cargo']);
                    $("#fk_rhc063_num_puestos").val(dato['fk_rhc063_num_puestos_cargo']);
                    $("#nombreTrabajador").val($nombre);
                    $("#pkNumEmpleado").val(dato['pk_num_empleado']);
                    $("#cedula").val(dato['ind_cedula_documento']);
                    $("#ind_sexo").val(dato['sexo']);
                    $("#sexo").val(dato['fk_a006_num_miscelaneo_detalle_sexo']);
                    $("#nacimiento").val(dato['fec_nacimiento']);
                    $("#num_edad").val(edad);
                    $("#fec_fecha_ingreso").val(dato['fec_ingreso']);
                    $("#num_ultimo_sueldo").val(dato['num_sueldo_basico']);
                    $("#num_anio_servicio").val(dato['anio_serv']);
                    $("#fk_nmb001_num_tipo_nomina").val(dato['nomina']);
                    $("#fk_a006_num_miscelaneo_detalle_tipotrab").val(dato['tipo_trabajador']);
                    $("#fk_a006_num_miscelaneo_detalle_motivopension").val(dato['fk_a006_num_miscelaneo_detalle_motivopension']);
                    $("#fk_a006_num_miscelaneo_detalle_tipopension").val(dato['fk_a006_num_miscelaneo_detalle_tipopension']);
                    $("#ind_detalle_tipopension").val(dato['tipo_pension']);
                    $("#ind_detalle_motivopension").val(dato['motivo_pension']);


                    // **********************************************PASO 2*************************************************
                    var $antecedentes='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ObtenerAntecedentesMET';
                    $.post($antecedentes,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val() },function(datoAntecedentes){
                        var tabla_listado = $('#datatable3').DataTable();
                        tabla_listado.clear().draw();
                        if(datoAntecedentes != -1) {
                            for(var i=0; i<datoAntecedentes.length; i++) {
                                tabla_listado.row.add([
                                    i+1,
                                    datoAntecedentes[i].empresa,
                                    datoAntecedentes[i].fechaIngreso,
                                    datoAntecedentes[i].fechaEgreso,
                                    datoAntecedentes[i].anio,
                                    datoAntecedentes[i].mes,
                                    datoAntecedentes[i].dia
                                ]).draw()
                                    .nodes()
                                    .to$()
                            }
                        }
                    },'json');

                    // **********************************************PASO 3*************************************************
                    // Antecedentes totales
                    var $totalServicio='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/CalcularAntecedentesTotalMET';
                    var tabla_listado = $('#datatable3').DataTable();
                    tabla_listado.clear().draw();
                    $.post($totalServicio,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val()},function(servicioTotal){

                        $("#antAnio").html(servicioTotal['anio']);
                        $("#antMes").html(servicioTotal['mes']);
                        $("#antDia").html(servicioTotal['dia']);

                        $("#orgAnio").html(servicioTotal['anioInst']);
                        $("#orgMes").html(servicioTotal['mesInst']);
                        $("#orgDia").html(servicioTotal['diaInst']);

                        $("#tAnio").html(servicioTotal['anioTotal']);
                        $("#tMes").html(servicioTotal['mesTotal']);
                        $("#tDia").html(servicioTotal['diaTotal']);

                    },'json');
					//SUELDO
                    var $urlSueldoBase='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ConsultarRelacionSueldosMET';
                    $.post($urlSueldoBase,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val(), concepto: $("#concepto90").val()},function(datoSueldo){
                        var tablaSueldoBase = $('#datatable90').DataTable();
                        tablaSueldoBase.clear().draw();
                        var sumaBase = 0;
                        if(datoSueldo != -1) {
                            for(var i=0; i<datoSueldo.length; i++) {
                                var numBase = parseFloat(datoSueldo[i].monto);
                                sumaBase = sumaBase + numBase;
                                tablaSueldoBase.row.add([
                                    i+1,
                                    datoSueldo[i].periodo,
                                    numBase.toFixed(2)
                                ]).draw()
                                    .nodes()
                                    .to$()
                            }
                        }
                        $("#sueldo").val(sumaBase);
                    },'json');
					//PRIMAS
                    var $urlSueldoPrima='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ConsultarRelacionSueldosMET';
                    $.post($urlSueldoPrima,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val(), concepto: $("#concepto91").val()},function(datoPrima){
                        var tablaSueldoBase = $('#datatable91').DataTable();
                        tablaSueldoBase.clear().draw();
                        var sumaBase = 0;
                        if(datoPrima != -1) {
                            for(var i=0; i<datoPrima.length; i++) {
                                var numBase = parseFloat(datoPrima[i].monto);
                                sumaBase = sumaBase + numBase;
                                tablaSueldoBase.row.add([
                                    i+1,
                                    datoPrima[i].periodo,
                                    numBase.toFixed(2)
                                ]).draw()
                                    .nodes()
                                    .to$()
                            }
                        }
                        var sueldo =  $("#sueldo").val() + sumaBase;
                        $("#prima").val(sumaBase);
                        $("#total").val(sueldo);
                    },'json');

					//BENEFICIARIOS
                    var tablaBeneficiarios = $('#datatableBeneficiarios').DataTable();
                    var $urlBeneficiarios = '{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ConsultarBeneficiariosMET';
                    $.post($urlBeneficiarios,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val()},function(datos){

                        //tablaBeneficiarios.clear().draw();
                        //alert(datos.length);
                        for(var i=0; i < datos.length; i++) {

                            tablaBeneficiarios.row.add([
                                datos[i].cedula,
                                datos[i].nombre,
                                datos[i].parentesco,
                                datos[i].fecha_nacimiento,
                                datos[i].sexo,
                                datos[i].edad,
                                '<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'
                            ]).draw()
                                .nodes()
                                .to$()
                        }

                    },'json');

                    {*verifico si el empleado cumple con los requisitos para optar por la pension*}
                    var $url_verificar = '{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/VerificarRequisitosMET';
                    $.post($url_verificar, { idEmpleado: $('#fk_rhb001_num_empleado').val(), fecha_nac: $("#nacimiento").val() , fecha_ing: $("#fec_fecha_ingreso").val(), sexo: $("#ind_sexo").val(), edad: $("#num_edad").val() }, function(resultado){

                        $requisito = resultado['requisito'];
                        if($requisito==1){
                            $('#requisitoVer').html('<div class="alert alert-success"><strong><i class="glyphicon glyphicon-ok"></i></strong> El empleado cumple con los requisitos para optar a la Jubilación.</div>');
                            $('#botonGuardar').show();
                            $("#sueldo").val(resultado['sueldo']);
                            $("#prima").val(resultado['primas']);
                            $("#total").val(resultado['total']);
                            $("#porcentaje").val(resultado['porcentaje']);
                            $("#s_base").val(resultado['base']);
                            $("#jubilacion").val(resultado['jubilacion']);
                            $("#num_monto_pension").val(resultado['pension']);
                            $("#num_anios_servicio_exceso").val(resultado['anio_servicio_exceso']);
                        } else {
                            $('#botonGuardar').show();
                            $('#requisitoVer').html('<div class="alert alert-danger"><strong><i class="glyphicon glyphicon-alert"></i></strong> El Empleado NO cumple con los requisitos para optar a la Jubilación.</div>');
                            $("#sueldo").val(resultado['sueldo']);
                            $("#prima").val(resultado['primas']);
                            $("#total").val(resultado['total']);
                            $("#porcentaje").val(resultado['porcentaje']);
                            $("#s_base").val(resultado['base']);
                            $("#jubilacion").val(resultado['jubilacion']);
                            $("#num_monto_pension").val(resultado['pension']);
                            $("#num_monto_pension").val(resultado['pension']);
                            $("#num_anios_servicio_exceso").val(resultado['anio_servicio_exceso']);
                        }


                    },'json');

                    {* ************************************************ *}
                    $('#datatableBeneficiarios tbody').on( 'click', '.eliminar', function () {

                        var obj = this;

                        swal({
                            title:'Estas Seguro?',
                            text: 'Estas seguro que desea descartar al Beneficiario(a)',
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "si, Descartar",
                            closeOnConfirm: false
                        }, function(){

                            tablaBeneficiarios.row( $(obj).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}
                            swal("Descartado!", "Descartado de la lista de Beneficiarios", "success");
                        });

                    });
                    {* **************************************************** *}

                    var hoy = new Date();
                    var fecha = hoy.getDate()+"-"+(hoy.getMonth()+1)+"-"+hoy.getFullYear();

                    if($("#proceso").val()=='Nuevo'){
                        $("#fk_nmb001_num_tipo_nomina").attr('required');
                        $("#fk_a006_num_miscelaneo_detalle_tipotrab").attr('required');
                        $("#ind_resolucion").attr('required');
                        $("#num_monto_pension").attr('disabled','disabled');
                        $("#fec_fecha_egreso").attr('required');
                        $("#fk_rhc032_num_motivo_cese").attr('required');
                        $("#txt_observacion_egreso").attr('required');
                        $("#txt_observacion_pre").removeAttr('disabled');
                        $("#fecha_preparado").val(fecha);
                        $("#preparado_por").val('{$nombreUsuario}');
                        $("#usuario_preparado_por").val({$idUsuario});
                    }

                },'json');

            }
            else if(ACCION=='REQUISITOS_UTILES'){


                var $urlCargar='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ObtenerDatosEmpleadoMET';
                $.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato) {
                    var $nombre = dato['nombreTrabajador'];
                    $("#fk_rhb001_num_empleado").val(dato['id_empleado']);
                    $("#nombreTrabajador").val($nombre);

                    //consulto la carga familiar que puede optar al beneficio
                    var $carga='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ConsultarCargaFamiliarMET';
                    $.post($carga,{ pk_num_empleado: dato['id_empleado'] },function(datos){
                        var tabla_listado = $('#cargaFamiliarHijos').DataTable();
                        tabla_listado.clear().draw();
                        var cad_id_beneficiarios = $('#d_beneficiarios').val();
                        if(datos != -1) {
                            for(var i=0; i<datos.length; i++) {

                                var req = '<button type="button" class="requisitosBeneficiario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal3" data-keyboard="false" data-backdrop="static" tituloReq="'+datos[i].nombre+'" pk_num_carga="'+datos[i].pk_num_carga+'" idEmpleado="'+datos[i].pk_num_empleado+'" ><i class="md md-input" style="color: #ffffff;"></i></button>';

                                var fila = tabla_listado.row.add([
                                    datos[i].ind_cedula_documento,
                                    datos[i].nombre,
                                    datos[i].fecha_nacimiento,
                                    datos[i].parentesco,
                                    datos[i].sexo,
                                    req
                                ]).draw()
                                    .nodes()
                                    .to$();

                                $(fila)
                                    .attr('id','requisitosBeneficiario');

                                cad_id_beneficiarios = cad_id_beneficiarios + datos[i].pk_num_carga + '#';
                            }

                            $('#d_beneficiarios').val(cad_id_beneficiarios);

                        }
                    },'json');

                },'json');

            }
            else if (ACCION=='BENEFICIOS_UTILES_HIJOS'){

                var $urlCargar='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ObtenerDatosEmpleadoMET';
                $.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato) {
                    var $nombre = dato['nombreTrabajador'];
                    $("#fk_rhb001_num_empleado").val(dato['id_empleado']);
                    $("#nombreTrabajador").val($nombre);

                    //consulto los hijos disponibles para el beneficio
                    var $carga='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ConsultarCargaFamiliarHijosMET';
                    $.post($carga,{ pk_num_empleado: dato['id_empleado'] },function(datos){
                        var json = datos,
                            obj = JSON.parse(json);
                        $("#hijos").html(obj);
                    });
                },'json');

            }
            else if(ACCION=='CESE_REINGRESO'){

                var $url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ObtenerDatosEmpleadoMET';
                var $url_verificar = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/VerificarSolicitudesMET';

                //verifico primeramente si tiene una solicitud de cese o reingreso ya registrada

                $.post($url_verificar, { idEmpleado: $(this).attr('pk_num_empleado')}, function(datos){


                    if((datos['status']=='NO-C') || (datos['status']=='NO-R')){

                        $.post($url, { idEmpleado: datos['id'] }, function(dat){

                            swal("Información","El Empleado "+dat['nombreTrabajador']+" posee un registro activo de cese o reingreso. Por favor verifique los datos!!!", "info");

                        },'json');


                    }
                    else if(datos['status']=='SI'){


                        $.post($url, { idEmpleado: datos['id'] }, function(datos){

                            $("#fk_rhb001_num_empleado").val(datos['id_empleado']);
                            $("#nombreTrabajador").val(datos['nombreTrabajador']);
                            $("#cedula").val(datos['ind_cedula_documento']);
                            $("#fk_a001_num_organismo").val(datos['fk_a001_num_organismo']);
                            $("#fk_a004_num_dependencia").val(datos['fk_a004_num_dependencia']);
                            $("#ind_dependencia").val(datos['ind_dependencia']);
                            $("#sexo").val(datos['fk_a006_num_miscelaneo_detalle_sexo']);
                            $("#fecha_nacimiento").val(datos['fec_nacimiento']);
                            $("#fec_fecha_ingreso").val(datos['fec_ingreso']);
                            $("#fk_rhc063_num_puestos").val(datos['fk_rhc063_num_puestos_cargo']);
                            $('#fk_rhc063_num_puestos').append('<option value="'+datos['fk_rhc063_num_puestos_cargo']+'">'+datos['cargo']+'</option>');
                            $('#s2id_fk_rhc063_num_puestos .select2-chosen').html(datos['cargo']);
                            $("#ind_cargo").val(datos['cargo']);
                            $("#num_sueldo_actual").val(datos['num_sueldo_basico']);
                            $("#num_edad").val(datos['edad']);
                            $("#num_anio_servicio").val(datos['anio_serv']);
                            $("#fk_a006_num_miscelaneo_detalle_tipocr").val(datos['fk_a006_num_miscelaneo_detalle_tipocr']);
                            $("#fk_nmb001_num_tipo_nomina").val(datos['nomina']);
                            $("#fk_a006_num_miscelaneo_detalle_tipotrab").val(datos['tipo_trabajador']);

                            if(datos['tipo']=='C'){
                                $("#num_situacion_trabajo").val(0);
                                $("#ind_resolucion").attr('required','required');
                                $("#fec_fecha_egreso").attr('required','required');
                                $("#fk_rhc032_num_motivo_cese").attr('required','required');
                                $("#txt_observacion_egreso").attr('required','required');
                            }else{
                                $("#num_situacion_trabajo").val(1);
                                $("#fk_a001_num_organismo").removeAttr('disabled');
                                $("#fk_a004_num_dependencia").removeAttr('disabled');
                                $("#fec_fecha_ingreso").removeAttr('disabled');
                                $("#fec_fecha_egreso").attr('disabled','disabled');
                                $("#fk_rhc032_num_motivo_cese").attr('disabled','disabled');
                                $("#txt_observacion_egreso").attr('disabled','disabled');

                            }

                            var hoy = new Date();
                            var fecha = hoy.getDate()+"-"+(hoy.getMonth()+1)+"-"+hoy.getFullYear();


                            $("#txt_observacion_pre").removeAttr('disabled');
                            $("#fecha_preparado").val(fecha);
                            $("#preparado_por").val('{$nombreUsuario}');
                            $("#usuario_preparado_por").val({$idUsuario});


                        },'json');


                    }

                },'json');

            }


            if (ACCION=='MERITOS-DEMERITOS'){
                $(document.getElementById('cerrarModal3')).click();
                $(document.getElementById('ContenidoModal3')).html('');
            }else{

                if(ACCION == 'FIRMA_ELECTRONICA'){
                    $(document.getElementById('cerrarModal3')).click();
                    $(document.getElementById('ContenidoModal3')).html('');
                }else{
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');
                }
            }

        });



    });




	// Buscador
	function buscarTrabajador()
	{
		var pk_num_organismo = $("#pk_num_organismo").val();
		var pk_num_dependencia = $("#pk_num_dependencia").val();
		var centro_costo = $("#centro_costo").val();
		var tipo_nomina = $("#tipo_nomina").val();
		var tipo_trabajador = $("#tipo_trabajador").val();
		var estado_registro = $("#edo_reg").val();
		var situacion_trabajo = $("#sit_trab").val();
		var fecha_inicio = $("#fecha_inicio").val();
		var fecha_fin = $("#fecha_fin").val();



		var url_listar='{$_Parametros.url}modRH/gestion/empleadosCONTROL/BuscarTrabajadorMET';
		$.post(url_listar,{ pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, centro_costo: centro_costo, tipo_nomina: tipo_nomina, tipo_trabajador: tipo_trabajador, estado_registro: estado_registro, situacion_trabajo: situacion_trabajo, fecha_inicio: fecha_inicio, fecha_fin: fecha_fin },function(respuesta_post) {
			var tabla_listado = $('#empleadoListado').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					var botonCargar = '<button type="button" pk_num_empleado = "' + respuesta_post[i].pk_num_empleado + '" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Cargar empleado"><i class="glyphicon glyphicon-download"></i></button>';
					tabla_listado.row.add([
						i+1,
						respuesta_post[i].pk_num_empleado,
						respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_nombre2+' '+ respuesta_post[i].ind_apellido1+' '+ respuesta_post[i].ind_apellido2,
						respuesta_post[i].ind_cedula_documento,
						botonCargar
					]).draw()
							.nodes()
							.to$()
				}
			}
		},'json');
	}

	function calcular_edad(fecha)
	{
		//calculo la fecha de hoy
		hoy=new Date()
		//alert(hoy)
		//calculo la fecha que recibo
		//La descompongo en un array
		var array_fecha = fecha.split("-")
		//si el array no tiene tres partes, la fecha es incorrecta
		if (array_fecha.length!=3)
			return false
		//compruebo que los anio, mes, dia son correctos
		var anio
		anio = parseInt(array_fecha[2]);
		if (isNaN(anio))
			return false
		var mes
		mes = parseInt(array_fecha[1]);
		if (isNaN(mes))return false
		var dia
		dia = parseInt(array_fecha[0]);
		if (isNaN(dia))
			return false
		//resto los años de las dos fechas
		 var edad=hoy.getFullYear()- anio - 1; //-1 porque no se si ha cumplido años ya este año

		//si resto los meses y me da menor que 0 entonces no ha cumplido años. Si da mayor si ha cumplido
		if (hoy.getMonth() + 1 - mes < 0) //+ 1 porque los meses empiezan en 0
			return edad
		if (hoy.getMonth() + 1 - mes > 0)
			return edad+1
		//entonces es que eran iguales. miro los dias
		//si resto los dias y me da menor que 0 entonces no ha cumplido años. Si da mayor o igual si ha cumplido
		if (hoy.getDate() - dia >= 0)
			return edad + 1
		return edad
	}


	// Seleccion de empleados
	$('#empleadoListado tbody').on( 'click', '.cargar', function (respuesta_post) {

		var ACCION = $("#accion").val();


		/*************************************************************************************************************************/
		/*DEPENDIENDO DE LA ACCION EJECUTO EL PROCESO A EJECUTAR.                                                                                                                      */
		/*************************************************************************************************************************/

		if(ACCION=='PENSIONADO_SOBREVIVIENTE'){

			// **********************************************PASO 1*************************************************
			var $urlCargar='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ObtenerDatosEmpleadoMET';
			$.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato){
				var $nombre = dato['nombreTrabajador'];
				var edad = calcular_edad(dato['fec_nacimiento']);
				$("#fk_rhb001_num_empleado").val(dato['id_empleado']);
				$("#ind_organismo").val(dato['organismo']);
				$("#fk_a001_num_organismo").val(dato['fk_a001_num_organismo']);
				$("#ind_dependencia").val(dato['ind_dependencia']);
				$("#fk_a004_num_dependencia").val(dato['fk_a004_num_dependencia']);
				$("#ind_cargo").val(dato['cargo']);
				$("#fk_rhc063_num_puestos").val(dato['fk_rhc063_num_puestos_cargo']);
				$("#nombreTrabajador").val($nombre);
				$("#pkNumEmpleado").val(dato['pk_num_empleado']);
				$("#cedula").val(dato['ind_cedula_documento']);
				$("#ind_sexo").val(dato['sexo']);
				$("#sexo").val(dato['fk_a006_num_miscelaneo_detalle_sexo']);
				$("#nacimiento").val(dato['fec_nacimiento']);
				$("#num_edad").val(edad);
				$("#fec_fecha_ingreso").val(dato['fec_ingreso']);
				$("#num_ultimo_sueldo").val(dato['num_sueldo_basico']);
				$("#num_anio_servicio").val(dato['anio_serv']);
				$("#fk_nmb001_num_tipo_nomina").val(dato['nomina']);
				$("#fk_a006_num_miscelaneo_detalle_tipotrab").val(dato['tipo_trabajador']);
				$("#fk_a006_num_miscelaneo_detalle_motivopension").val(dato['fk_a006_num_miscelaneo_detalle_motivopension']);
				$("#fk_a006_num_miscelaneo_detalle_tipopension").val(dato['fk_a006_num_miscelaneo_detalle_tipopension']);
				$("#ind_detalle_tipopension").val(dato['tipo_pension']);
				$("#ind_detalle_motivopension").val(dato['motivo_pension']);


				// **********************************************PASO 2*************************************************
				var $antecedentes='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ObtenerAntecedentesMET';
				$.post($antecedentes,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val() },function(datoAntecedentes){
					var tabla_listado = $('#datatable3').DataTable();
					tabla_listado.clear().draw();
					if(datoAntecedentes != -1) {
						for(var i=0; i<datoAntecedentes.length; i++) {
							tabla_listado.row.add([
								i+1,
								datoAntecedentes[i].empresa,
								datoAntecedentes[i].fechaIngreso,
								datoAntecedentes[i].fechaEgreso,
								datoAntecedentes[i].anio,
								datoAntecedentes[i].mes,
								datoAntecedentes[i].dia
							]).draw()
									.nodes()
									.to$()
						}
					}
				},'json');

				// **********************************************PASO 3*************************************************
				// Antecedentes totales
				var $totalServicio='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/CalcularAntecedentesTotalMET';
				var tabla_listado = $('#datatable3').DataTable();
				tabla_listado.clear().draw();
				$.post($totalServicio,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val()},function(servicioTotal){

					$("#antAnio").html(servicioTotal['anio']);
					$("#antMes").html(servicioTotal['mes']);
					$("#antDia").html(servicioTotal['dia']);

					$("#orgAnio").html(servicioTotal['anioInst']);
					$("#orgMes").html(servicioTotal['mesInst']);
					$("#orgDia").html(servicioTotal['diaInst']);

					$("#tAnio").html(servicioTotal['anioTotal']);
					$("#tMes").html(servicioTotal['mesTotal']);
					$("#tDia").html(servicioTotal['diaTotal']);

				},'json');
				/*SUELDO*/
				var $urlSueldoBase='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ConsultarRelacionSueldosMET';
				$.post($urlSueldoBase,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val(), concepto: $("#concepto90").val()},function(datoSueldo){
					var tablaSueldoBase = $('#datatable90').DataTable();
					tablaSueldoBase.clear().draw();
					var sumaBase = 0;
					if(datoSueldo != -1) {
						for(var i=0; i<datoSueldo.length; i++) {
							var numBase = parseFloat(datoSueldo[i].monto);
							sumaBase = sumaBase + numBase;
							tablaSueldoBase.row.add([
								i+1,
								datoSueldo[i].periodo,
								numBase.toFixed(2)
							]).draw()
									.nodes()
									.to$()
						}
					}
					$("#sueldo").val(sumaBase);
				},'json');
				/*PRIMAS*/
				var $urlSueldoPrima='{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ConsultarRelacionSueldosMET';
				$.post($urlSueldoPrima,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val(), concepto: $("#concepto91").val()},function(datoPrima){
					var tablaSueldoBase = $('#datatable91').DataTable();
					tablaSueldoBase.clear().draw();
					var sumaBase = 0;
					if(datoPrima != -1) {
						for(var i=0; i<datoPrima.length; i++) {
							var numBase = parseFloat(datoPrima[i].monto);
							sumaBase = sumaBase + numBase;
							tablaSueldoBase.row.add([
								i+1,
								datoPrima[i].periodo,
								numBase.toFixed(2)
							]).draw()
									.nodes()
									.to$()
						}
					}
					var sueldo =  $("#sueldo").val() + sumaBase;
					$("#prima").val(sumaBase);
					$("#total").val(sueldo);
				},'json');

				/*BENEFICIARIOS*/
				var tablaBeneficiarios = $('#datatableBeneficiarios').DataTable();
				var $urlBeneficiarios = '{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ConsultarBeneficiariosMET';
				$.post($urlBeneficiarios,{ pk_num_empleado: $('#fk_rhb001_num_empleado').val()},function(datos){

					//tablaBeneficiarios.clear().draw();
					//alert(datos.length);
					for(var i=0; i < datos.length; i++) {

						tablaBeneficiarios.row.add([
							datos[i].cedula,
							datos[i].nombre,
							datos[i].parentesco,
							datos[i].fecha_nacimiento,
							datos[i].sexo,
							datos[i].edad,
							'<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'
						]).draw()
								.nodes()
								.to$()
					}

				},'json');

				{*verifico si el empleado cumple con los requisitos para optar por la pension*}
				var $url_verificar = '{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/VerificarRequisitosMET';
				$.post($url_verificar, { idEmpleado: $('#fk_rhb001_num_empleado').val(), fecha_nac: $("#nacimiento").val() , fecha_ing: $("#fec_fecha_ingreso").val(), sexo: $("#ind_sexo").val(), edad: $("#num_edad").val() }, function(resultado){

					$requisito = resultado['requisito'];
					if($requisito==1){
						$('#requisitoVer').html('<div class="alert alert-success"><strong><i class="glyphicon glyphicon-ok"></i></strong> El empleado cumple con los requisitos para optar a la Jubilación.</div>');
						$('#botonGuardar').show();
						$("#sueldo").val(resultado['sueldo']);
						$("#prima").val(resultado['primas']);
						$("#total").val(resultado['total']);
						$("#porcentaje").val(resultado['porcentaje']);
						$("#s_base").val(resultado['base']);
						$("#jubilacion").val(resultado['jubilacion']);
						$("#num_monto_pension").val(resultado['pension']);
						$("#num_anios_servicio_exceso").val(resultado['anio_servicio_exceso']);
					} else {
						$('#botonGuardar').show();
						$('#requisitoVer').html('<div class="alert alert-danger"><strong><i class="glyphicon glyphicon-alert"></i></strong> El Empleado NO cumple con los requisitos para optar a la Jubilación.</div>');
						$("#sueldo").val(resultado['sueldo']);
						$("#prima").val(resultado['primas']);
						$("#total").val(resultado['total']);
						$("#porcentaje").val(resultado['porcentaje']);
						$("#s_base").val(resultado['base']);
						$("#jubilacion").val(resultado['jubilacion']);
						$("#num_monto_pension").val(resultado['pension']);
						$("#num_monto_pension").val(resultado['pension']);
						$("#num_anios_servicio_exceso").val(resultado['anio_servicio_exceso']);
					}


				},'json');

				{* ************************************************ *}
				$('#datatableBeneficiarios tbody').on( 'click', '.eliminar', function () {

					var obj = this;

					swal({
						title:'Estas Seguro?',
						text: 'Estas seguro que desea descartar al Beneficiario(a)',
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "si, Descartar",
						closeOnConfirm: false
					}, function(){

						tablaBeneficiarios.row( $(obj).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}
						swal("Descartado!", "Descartado de la lista de Beneficiarios", "success");
					});

				});
				{* **************************************************** *}

				var hoy = new Date();
				var fecha = hoy.getDate()+"-"+(hoy.getMonth()+1)+"-"+hoy.getFullYear();

				if($("#proceso").val()=='Nuevo'){
					$("#fk_nmb001_num_tipo_nomina").attr('required');
					$("#fk_a006_num_miscelaneo_detalle_tipotrab").attr('required');
					$("#ind_resolucion").attr('required');
					$("#num_monto_pension").attr('disabled','disabled');
					$("#fec_fecha_egreso").attr('required');
					$("#fk_rhc032_num_motivo_cese").attr('required');
					$("#txt_observacion_egreso").attr('required');
					$("#txt_observacion_pre").removeAttr('disabled');
					$("#fecha_preparado").val(fecha);
					$("#preparado_por").val('{$nombreUsuario}');
					$("#usuario_preparado_por").val({$idUsuario});
				}

			},'json');


		}
		else if(ACCION=='CESE_REINGRESO'){

			var $url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ObtenerDatosEmpleadoMET';
            var $url_verificar = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/VerificarSolicitudesMET';

			//verifico primeramente si tiene una solicitud de cese o reingreso ya registrada

			$.post($url_verificar, { idEmpleado: $(this).attr('pk_num_empleado')}, function(datos){


					if((datos['status']=='NO-C') || (datos['status']=='NO-R')){

						$.post($url, { idEmpleado: datos['id'] }, function(dat){

							swal("Información","El Empleado "+dat['nombreTrabajador']+" posee un registro activo de cese o reingreso. Por favor verifique los datos!!!", "info");

						},'json');


					}
					else if(datos['status']=='SI'){


						$.post($url, { idEmpleado: datos['id'] }, function(datos){

							$("#fk_rhb001_num_empleado").val(datos['id_empleado']);
							$("#nombreTrabajador").val(datos['nombreTrabajador']);
							$("#cedula").val(datos['ind_cedula_documento']);
							$("#fk_a001_num_organismo").val(datos['fk_a001_num_organismo']);
							$("#fk_a004_num_dependencia").val(datos['fk_a004_num_dependencia']);
							$("#ind_dependencia").val(datos['ind_dependencia']);
							$("#sexo").val(datos['fk_a006_num_miscelaneo_detalle_sexo']);
							$("#fecha_nacimiento").val(datos['fec_nacimiento']);
							$("#fec_fecha_ingreso").val(datos['fec_ingreso']);
							$("#fk_rhc063_num_puestos").val(datos['fk_rhc063_num_puestos_cargo']);
							$('#fk_rhc063_num_puestos').append('<option value="'+datos['fk_rhc063_num_puestos_cargo']+'">'+datos['cargo']+'</option>');
							$('#s2id_fk_rhc063_num_puestos .select2-chosen').html(datos['cargo']);
							$("#ind_cargo").val(datos['cargo']);
							$("#num_sueldo_actual").val(datos['num_sueldo_basico']);
							$("#num_edad").val(datos['edad']);
							$("#num_anio_servicio").val(datos['anio_serv']);
							$("#fk_a006_num_miscelaneo_detalle_tipocr").val(datos['fk_a006_num_miscelaneo_detalle_tipocr']);
							$("#fk_nmb001_num_tipo_nomina").val(datos['nomina']);
							$("#fk_a006_num_miscelaneo_detalle_tipotrab").val(datos['tipo_trabajador']);

							if(datos['tipo']=='C'){
								$("#num_situacion_trabajo").val(0);
								$("#ind_resolucion").attr('required','required');
								$("#fec_fecha_egreso").attr('required','required');
								$("#fk_rhc032_num_motivo_cese").attr('required','required');
								$("#txt_observacion_egreso").attr('required','required');
							}else{
								$("#num_situacion_trabajo").val(1);
								$("#fk_a001_num_organismo").removeAttr('disabled');
								$("#fk_a004_num_dependencia").removeAttr('disabled');
								$("#fec_fecha_ingreso").removeAttr('disabled');
								$("#fec_fecha_egreso").attr('disabled','disabled');
								$("#fk_rhc032_num_motivo_cese").attr('disabled','disabled');
								$("#txt_observacion_egreso").attr('disabled','disabled');

							}

							var hoy = new Date();
							var fecha = hoy.getDate()+"-"+(hoy.getMonth()+1)+"-"+hoy.getFullYear();


							$("#txt_observacion_pre").removeAttr('disabled');
							$("#fecha_preparado").val(fecha);
							$("#preparado_por").val('{$nombreUsuario}');
							$("#usuario_preparado_por").val({$idUsuario});


						},'json');


					}

			},'json');




		}
		else if(ACCION=='REQUISITOS_UTILES'){


			var $urlCargar='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ObtenerDatosEmpleadoMET';
			$.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato) {
				var $nombre = dato['nombreTrabajador'];
				$("#fk_rhb001_num_empleado").val(dato['id_empleado']);
				$("#nombreTrabajador").val($nombre);

				//consulto la carga familiar que puede optar al beneficio
				var $carga='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ConsultarCargaFamiliarMET';
				$.post($carga,{ pk_num_empleado: dato['id_empleado'] },function(datos){
					var tabla_listado = $('#cargaFamiliarHijos').DataTable();
					tabla_listado.clear().draw();
					var cad_id_beneficiarios = $('#d_beneficiarios').val();
					if(datos != -1) {
						for(var i=0; i<datos.length; i++) {

							var req = '<button type="button" class="requisitosBeneficiario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal3" data-keyboard="false" data-backdrop="static" tituloReq="'+datos[i].nombre+'" pk_num_carga="'+datos[i].pk_num_carga+'" idEmpleado="'+datos[i].pk_num_empleado+'" ><i class="md md-input" style="color: #ffffff;"></i></button>';

							var fila = tabla_listado.row.add([
								datos[i].ind_cedula_documento,
								datos[i].nombre,
								datos[i].fecha_nacimiento,
								datos[i].parentesco,
								datos[i].sexo,
								req
							]).draw()
									.nodes()
									.to$();

							$(fila)
									.attr('id','requisitosBeneficiario');

							cad_id_beneficiarios = cad_id_beneficiarios + datos[i].pk_num_carga + '#';
						}

						$('#d_beneficiarios').val(cad_id_beneficiarios);

					}
				},'json');


			},'json');

		}
		else if (ACCION=='BENEFICIOS_UTILES_HIJOS'){

			var $urlCargar='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ObtenerDatosEmpleadoMET';
			$.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato) {
				var $nombre = dato['nombreTrabajador'];
				$("#fk_rhb001_num_empleado").val(dato['id_empleado']);
				$("#nombreTrabajador").val($nombre);

				//consulto los hijos disponibles para el beneficio
				var $carga='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ConsultarCargaFamiliarHijosMET';
				$.post($carga,{ pk_num_empleado: dato['id_empleado'] },function(datos){
					var json = datos,
							obj = JSON.parse(json);
					$("#hijos").html(obj);
				});
			},'json');

		}

		else if (ACCION=='CAPACITACION'){

			var $urlCargar='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/ObtenerDatosEmpleadoMET';
			$.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato) {
				var $nombre = dato['nombreTrabajador'];
				$("#fk_rhb001_empleado_solicitante").val(dato['id_empleado']);
				$("#nombreTrabajador").val($nombre);

			},'json');

		}

		else if (ACCION=='INSERTAR_EMPLEADO_CAPACITACION'){

			var tabla_beneficiarios = $('#empleadosBeneficiarios').DataTable();

			var $urlCargar='{$_Parametros.url}modRH/gestion/capacitacionCONTROL/ObtenerDatosEmpleadoMET';
			$.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato) {

				/*var $nombre = dato['nombreTrabajador'];
				$("#fk_rhb001_num_empleado").val(dato['id_empleado']);
				$("#nombreTrabajador").val($nombre);*/

				var pk_num_empleado = dato['id_empleado'];{*celda 0 que contiene el codigo de la fila*}

				var num_filas = $('#empleadosBeneficiarios >tbody >tr').length;
				var filas = $('#empleadosBeneficiarios >tbody >tr'); {*trae las filas del tbody*}
				var band = 0;
				for(var i=0; i<num_filas; i++){
					if(filas[i].cells[0].innerHTML == pk_num_empleado){

						swal({  {*Mensaje personalizado*}
							title: '',
							text: 'Ya está agregado',
							type: "warning",
							showCancelButton: false,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: 'Aceptar',
							closeOnConfirm: true
						});

						band = 1;
						break;
					}


				}

				var vacantes = $('#num_vacantes').val();

				var cad_id_empleados = $('#id_empleados').val();
				var cad_id_dependencia = $('#id_dependencias').val();

				if(band == 0){

					band = 0;

					//alert(num_filas+"-"+vacantes);

					for(var i=0; i<num_filas; i++) {

						if (filas[i].cells[0].innerHTML == 'No Hay Registros Disponibles') {

							tabla_beneficiarios.row.add([
								dato['id_empleado'], {*this representa el array (rows) *}
								dato['nombreTrabajador'],
								'',
								'',
								'',
								'',
								'0,00',
								'0,00',
								'<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'
							]).draw(false);


							cad_id_empleados = cad_id_empleados + dato['id_empleado'] + '#';
							cad_id_dependencia = cad_id_dependencia + dato['id_dependencia'] + '#';

						} else if (num_filas < vacantes) {

							tabla_beneficiarios.row.add([
								dato['id_empleado'], {*this representa el array (rows) *}
								dato['nombreTrabajador'],
								'',
								'',
								'',
								'',
								'0,00',
								'0,00',
								'<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'
							]).draw(false);


							cad_id_empleados = cad_id_empleados + dato['id_empleado'] + '#';
							cad_id_dependencia = cad_id_dependencia + dato['id_dependencia'] + '#';

						} else {

							swal("Advertencia!", "El número de participantes no puede ser mayor al número de vacantes.", "error");
							return false;
						}

					}


				}

				$('#id_empleados').val(cad_id_empleados);
				$('#id_dependencias').val(cad_id_dependencia);

				var par = cad_id_empleados.split("#");
				$('#num_participantes').val(par.length-1);


			},'json');

		}

		else if (ACCION=='RETENCION_JUDICIAL') {

			var $urlCargar='{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/ObtenerDatosEmpleadoMET';
			$.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado')},function(dato) {
				var $nombre = dato['nombreTrabajador'];
				$("#fk_rhb001_num_empleado").val(dato['id_empleado']);
				$("#nombreTrabajador").val($nombre);

			},'json');

		}

        else if (ACCION=='MERITOS-DEMERITOS'){

            var $urlCargar='{$_Parametros.url}modRH/gestion/retencionesJudicialesCONTROL/ObtenerDatosEmpleadoMET';
            $.post($urlCargar,{ idEmpleado: $(this).attr('pk_num_empleado') },function(dato) {
                var $nombre = dato['nombreTrabajador'];
                $("#fk_rhb001_num_empleado_responsable").val(dato['id_empleado']);
                $("#nombreTrabajador").val($nombre);
            },'json');

        }


        if (ACCION=='MERITOS-DEMERITOS'){
            $(document.getElementById('cerrarModal3')).click();
            $(document.getElementById('ContenidoModal3')).html('');
        }
        else{
			/***************************************************************************************/
            $(document.getElementById('cerrarModal2')).click();
            $(document.getElementById('ContenidoModal2')).html('');
			/***************************************************************************************/
        }




	});
</script>