{if $Empleado==0}
<!-- no se ha seleccionado ningun registro se muestra advertencia -->
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->
{else}
<!-- no se ha seleccionado ningun registro se muestra advertencia -->

    <div class="well clearfix">
        <div class="margin-bottom-xxl">
                <div class="pull-left width-3 clearfix hidden-xs">
                        <img class="img-circle size-2" src="{if isset($Foto)}{$_Parametros.ruta_Img}modRH/fotos/{$Foto}{else}{$_Parametros.ruta_Img}avatar.jpg{/if}" alt="">
                </div>
                <h4 class="no-margin">
                    <span class="text-bold">NOMBRES Y APELLIDOS:</span> <span class="text-light">{$Nombres}</span>
                </h4>
                <h6>
                    <span class="text-bold">CEDULA:</span> <span class="text-light">{$Cedula}</span>
                </h6>
                <h6 class="text-bold">
                        <span class="text-bold">CARGO:</span> <span class="text-light">{$Cargo}</span>
                </h6>
        </div>
    </div>




    <div class="card">

        <div class="card-body">

            <table id="datatable2" class="table table-striped no-margin table-condensed">
                <thead>
                <tr>
                    <th width="15%">Nro. Documento</th>
                    <th width="40%">Nombre Completo</th>
                    <th width="10%">Parentesco</th>
                    <th width="15%">Fecha Nacimiento</th>
                    <th width="10%">Sexo</th>
                    <th width="5%">Mod</th>
                    <th width="5%">Eli</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=dat from=$listadoCargaFamiliar}

                    <tr id="idCargaFamiliar{$dat.pk_num_carga}" valor="{$dat.pk_num_carga}" style="font-size: 11px">
                        <td width="15%">{$dat.ind_cedula_documento}</td>
                        <td width="40%">{$dat.nombre}</td>
                        <td width="10%">{$dat.parentesco}</td>
                        <td width="15%">{$dat.fec_nacimiento|date_format:"d-m-Y"}</td>
                        <td width="10%">{$dat.sexo}</td>
                        <td width="5%">
                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                                    data-keyboard="false" data-backdrop="static" title="Editar Carga Familiar" idCargaFamiliar="{$dat.pk_num_carga}"
                                    descipcion="El Usuario a Modificado Carga Familiar" titulo="Modificar Carga Familiar">
                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                            </button>
                        </td>
                        <td width="5%">
                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Eliminar" idCargaFamiliar="{$dat.pk_num_carga}"
                                    descipcion="El usuario a eliminado Carga Familiar" title="Eliminar Carga Familiar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Carga Familiar!!">
                                <i class="md md-delete" style="color: #ffffff;"></i>
                            </button>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                    <th colspan="7">
                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                                descripcion="el Usuario a creado una Carga Familiar" title="Nuevo Registro Carga Familiar"  titulo="Nuevo Registro Carga Familiar" id="nuevaCarga" >
                            <i class="md md-create"></i> Nueva carga &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </button>
                    </th>
                </tfoot>
            </table>

        </div>

    </div>

{/if}




<script type="text/javascript">

    $(document).ready(function() {

        //ancho de la ventana modal
        $('#modalAncho').css( "width", "70%" );

        //inicializo el datatbales
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        var $url='{$_Parametros.url}modRH/gestion/cargaFamiliarCONTROL/CargaFamiliarMET';

        //al dar click en nueva carga
        $('#nuevaCarga').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idEmpleado:{$Empleado}, tipo:'NU' },function(dato){
                $('#ContenidoModal2').html(dato);
            });
        });

        //al dar click boton modificar del listado de carga familiar
        $('#datatable2 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html('');
            $.post($url,{ idCargaFamiliar: $(this).attr('idCargaFamiliar'), tipo:'MO'},function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        //al dar click boton eliminar del listado de carga familiar
        $('#datatable2 tbody').on( 'click', '.eliminar', function () {

            var idCargaFamiliar=$(this).attr('idCargaFamiliar');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/cargaFamiliarCONTROL/EliminarCargaFamiliarMET';
                $.post($url, { idCargaFamiliar: idCargaFamiliar},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idCargaFamiliar'+$dato['idCargaFamiliar'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();
                    }else{
                        swal("Error",$dato['mensaje'], "error");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

    });

</script>