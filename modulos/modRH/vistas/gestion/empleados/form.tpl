<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                        <div id="rootwizard2" class="form-wizard form-wizard-horizontal">

                            <form enctype="multipart/form-data" id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <div class="form-wizard-nav">
                                    <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                                    <ul class="nav nav-justified">
                                        <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">DATOS GENERALES</span></a></li>
                                        <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">NACIMIENTO</span></a></li>
                                        <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">DOMICILIO</span></a></li>
                                        <li><a href="#step4" data-toggle="tab"><span class="step">4</span> <span class="title">OTROS DATOS PERSONALES</span></a></li>
                                        <li><a href="#step5" data-toggle="tab"><span class="step">5</span> <span class="title">ORGANIZACIÓN</span></a></li>
                                        <li><a href="#step6" data-toggle="tab"><span class="step">6</span> <span class="title">DATOS LABORALES</span></a></li>
                                    </ul>
                                </div><!--end .form-wizard-nav -->

                                <div class="well well-sm clearfix">
                                    <div class="col-md-1">
                                        <img class="img-circle width-0" id="img" src="{if isset($formDBprincipales.ind_foto)}{$_Parametros.ruta_Img}modRH/fotos/{$formDBprincipales.ind_foto}{else}{$_Parametros.ruta_Img}foto.png{/if}" alt="" />
                                    </div>
                                    <div class="col-md-2">
                                        <div class="text-bold">C.I: <span id="ci" class="text-bold text-lg">{if isset($formDBprincipales.ind_cedula_documento)}{$formDBprincipales.ind_cedula_documento}{/if}</span></div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="text-bold">Nombres y Apellidos: <span id="nom" class="text-bold text-lg">{if isset($formDBprincipales.ind_nombre1) && isset($formDBprincipales.ind_nombre2) && isset($formDBprincipales.ind_apellido1) && isset($formDBprincipales.ind_apellido2)}{$formDBprincipales.ind_nombre1} {$formDBprincipales.ind_nombre2} {$formDBprincipales.ind_apellido1} {$formDBprincipales.ind_apellido2}{/if}</span></div>
                                    </div>
                                </div>

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{$formDBprincipales.ind_estado_aprobacion}" name="estado_aprobacion" id="estado_aprobacion"/>

                                <div class="tab-content clearfix">

                                    <!-- datos genereales-->
                                    <div class="tab-pane active" id="step1">

                                        <br/><br/>

                                        <div class="col-xs-10" style="margin-top: -10px">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="empleado" name="form[int][empleado]" value="{if isset($formDBprincipales.pk_num_empleado)}{$formDBprincipales.pk_num_empleado}{/if}" readonly>
                                                        <label for="empleado">Interno</label>
                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="cod_empleado" name="form[txt][cod_empleado]" value="{if isset($formDBprincipales.cod_empleado)}{$formDBprincipales.cod_empleado}{/if}" readonly>
                                                        <label for="empleado">Empleado</label>
                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="persona" name="form[int][persona]" value="{if isset($formDBprincipales.pk_num_persona)}{$formDBprincipales.pk_num_persona}{/if}" readonly>
                                                        <label for="persona">Codigo Persona</label>
                                                    </div>
                                                </div><!--end .col -->

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="cedula" maxlength="8" name="form[int][cedula]" value="{if isset($formDBprincipales.ind_cedula_documento)}{$formDBprincipales.ind_cedula_documento}{/if}" required data-msg-required="Indique Cedula de Identidad">
                                                        <label for="lastname">Introduzca Cedula de Identidad</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>


                                            </div><!--end .row -->

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="apellido1" name="form[txt][apellido1]" value="{if isset($formDBprincipales.ind_apellido1)}{$formDBprincipales.ind_apellido1}{/if}" required data-msg-required="Indique Primer Apellido" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="firstname">Primer Apellido</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="apellido2" name="form[txt][apellido2]" value="{if isset($formDBprincipales.ind_apellido2)}{$formDBprincipales.ind_apellido2}{/if}"  onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="lastname">Segundo Apellido</label>

                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="nombre1" name="form[txt][nombre1]" value="{if isset($formDBprincipales.ind_nombre1)}{$formDBprincipales.ind_nombre1}{/if}" required data-msg-required="Indique Primer Nombre" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="lastname">Primer Nombre</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="nombre2" name="form[txt][nombre2]" value="{if isset($formDBprincipales.ind_nombre2)}{$formDBprincipales.ind_nombre2}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="lastname">Segundo Nombre</label>
                                                    </div>
                                                </div><!--end .col -->
                                            </div><!--end .row -->


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="NombCompleto" name="form[txt][NombCompleto]" value="{if isset($formDBprincipales.ind_nombre1) && isset($formDBprincipales.ind_nombre2) && isset($formDBprincipales.ind_apellido1) && isset($formDBprincipales.ind_apellido2)}{$formDBprincipales.ind_nombre1} {$formDBprincipales.ind_nombre2} {$formDBprincipales.ind_apellido1} {$formDBprincipales.ind_apellido2}{/if}">
                                                        <label for="company">Nombre Completo</label>
                                                    </div>
                                                </div><!--end .col -->

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_sexo" name="form[int][fk_a006_num_miscelaneo_detalle_sexo]" class="form-control input-sm" required data-msg-required="Seleccione el Sexo">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=sex from=$Sexo}
                                                                {if isset($formDBprincipales.fk_a006_num_miscelaneo_detalle_sexo)}
                                                                    {if $sex.pk_num_miscelaneo_detalle==$formDBprincipales.fk_a006_num_miscelaneo_detalle_sexo}
                                                                        <option selected value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_sexo">Sexo</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="checkbox-inline checkbox-styled checkbox-primary">
                                                            <label>
                                                                <input type="checkbox" {if isset($formDBprincipales.ind_estatus_persona) and $formDBprincipales.ind_estatus_persona==1} checked{/if}  value="1" name="form[int][ind_estatus_persona]" id="ind_estatus_persona" >
                                                                <span>Estado de Registro</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <!--<div class="form-group">
                                                        <select id="ind_estatus_persona" name="form[int][ind_estatus_persona]" class="form-control input-sm">
                                                            {foreach key=key item=item from=$EstadoRegistro}
                                                                {if isset($formDBprincipales.ind_estatus_persona)}
                                                                    {if $key==$formDBprincipales.ind_estatus_persona}
                                                                        <option selected value="{$key}">{$item}</option>
                                                                    {else}
                                                                        <option value="{$key}">{$item}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$key}">{$item}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="ind_estatus_persona">Estado Registro</label>
                                                    </div>-->
                                                </div>

                                            </div><!--end .row -->
                                        </div><!--end .col -->
                                        <div class="col-xs-2" style="margin-top: -10px;">
                                            <div class="row" align="center">
                                                <div class="col-md-12">
                                                    <div id="fotoCargada">
                                                        <img class="img-thumbnail border-gray border-lg img-responsive auto-width" src="{if isset($formDBprincipales.ind_foto)}{$_Parametros.ruta_Img}modRH/fotos/{$formDBprincipales.ind_foto}{else}{$_Parametros.ruta_Img}foto.png{/if}" alt="Cargar o Actualizar Foto del Empleado" title="Cargar o Actualizar Foto del Empleado" id="cargarImagen" style="cursor: pointer"/>
                                                    </div>
                                                    <input type="file" name="Foto" id="Foto" style="display: none" />
                                                    <input type="hidden" name="form[txt][aleatorio]" id="aleatorio" value="{if isset($aleatorio)}{$aleatorio}{/if}">
                                                    <!--<small id="ampliarFoto" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" style="cursor: pointer">Ampliar Foto</small>-->
                                                </div><!--end .col -->
                                            </div>
                                        </div>

                                        <div class="col-xs-8">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_nacionalidad" name="form[int][fk_a006_num_miscelaneo_detalle_nacionalidad]" class="form-control input-sm" required data-msg-required="Seleccione Nacionalidad">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=nac from=$Nacion}
                                                                {if isset($formDBprincipales.fk_a006_num_miscelaneo_detalle_nacionalidad)}
                                                                    {if $nac.pk_num_miscelaneo_detalle==$formDBprincipales.fk_a006_num_miscelaneo_detalle_nacionalidad}
                                                                        <option selected value="{$nac.pk_num_miscelaneo_detalle}">{$nac.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$nac.pk_num_miscelaneo_detalle}">{$nac.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $nac.pk_num_miscelaneo_detalle==$DefaultNacionalidad}
                                                                        <option selected value="{$nac.pk_num_miscelaneo_detalle}">{$nac.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$nac.pk_num_miscelaneo_detalle}">{$nac.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {/if}

                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_nacionalidad">Nacionalidad</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="doc_fiscal" name="form[txt][doc_fiscal]" value="{if isset($formDBprincipales.ind_documento_fiscal)}{$formDBprincipales.ind_documento_fiscal}{/if}" required data-msg-required="Indique Documento Fiscal">
                                                        <label for="doc_fiscal">Documento Fiscal/Rif</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <input type="email" class="form-control input-sm" id="email" name="form[formula][email]" value="{if isset($formDBprincipales.ind_email)}{$formDBprincipales.ind_email}{/if}">
                                                        <label for="email">Email</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p class="text-xs text-bold text-right">LOS CAMPOS MARCADOS CON ASTERISCOS <span class="text-lg text-bold" style="color: red"> ( * ) </span>SON OBLIGATORIOS</p>
                                                </div>
                                            </div>
                                        </div>


                                    </div><!-- fin datos generales -->

                                    <!-- nacimiento -->
                                    <div class="tab-pane" id="step2">

                                        <br><br>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a008_num_pais" name="form[int][fk_a008_num_pais]" class="form-control input-sm" required data-msg-required="Seleccione País">
                                                            <option value="">Seleccione el Pais</option>
                                                            {foreach item=i from=$listadoPais}
                                                                {if isset($formDBnacimiento.pais_nacimiento)}
                                                                    {if $i.pk_num_pais==$formDBnacimiento.pais_nacimiento}
                                                                        <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_pais==$DefaultPais}
                                                                        <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a008_num_pais"><i class="md md-map"></i> Pais</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a009_num_estado" name="form[int][fk_a009_num_estado]"  class="form-control input-sm" required data-msg-required="Seleccione Estado">
                                                            <option value="">Seleccione el Estado</option>
                                                            {foreach item=i from=$listadoEstado}
                                                                {if isset($formDBnacimiento.estado_nacimiento)}
                                                                    {if $i.pk_num_estado==$formDBnacimiento.estado_nacimiento}
                                                                        <option selected value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_estado==$DefaultEstado}
                                                                        <option selected value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a009_num_estado"><i class="md md-map"></i> Estado</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a011_num_municipio" name="form[int][fk_a011_num_municipio]"  class="form-control input-sm" required data-msg-required="Seleccione Municipio">
                                                            <option value="">Seleccione el Municipio</option>
                                                            {foreach item=i from=$listadoMunicipio}
                                                                {if isset($formDBnacimiento.municipio_nacimiento)}
                                                                    {if $i.pk_num_municipio==$formDBnacimiento.municipio_nacimiento}
                                                                        <option selected value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_municipio==$DefaultMunicipio}
                                                                        <option selected value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a011_num_municipio"><i class="md md-map"></i> Municipio</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a010_num_ciudad" name="form[int][fk_a010_num_ciudad]"  class="form-control input-sm" required data-msg-required="Seleccione Ciudad">
                                                            <option value="">Seleccione la Ciudad</option>
                                                            {foreach item=i from=$listadoCiudad}
                                                                {if isset($formDBnacimiento.ciudad_nacimiento)}
                                                                    {if $i.pk_num_ciudad==$formDBnacimiento.ciudad_nacimiento}
                                                                        <option selected value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_ciudad==$DefaultCiudad}
                                                                        <option selected value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a010_num_ciudad"><i class="md md-map"></i> Ciudad</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group" id="fechaNacimientoError">
                                                        <div class="input-group date" id="fecha">
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" name="form[txt][fechaNacimiento]" id="fechaNacimiento" value="{if isset($formDBnacimiento.fec_nacimiento)}{$formDBnacimiento.fec_nacimiento|date_format:"d-m-Y"}{/if}" required data-msg-required="Indique Fecha de Nacimiento">
                                                                <label for="fechaNacimiento">Fecha de Nacimiento</label>
                                                            </div>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="lugarNacimiento" name="form[txt][lugarNacimiento]" value="{if isset($formDBnacimiento.ind_lugar_nacimiento)}{$formDBnacimiento.ind_lugar_nacimiento}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="lugarNacimiento">Lugar de Nacimiento</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p class="text-xs text-bold text-right">LOS CAMPOS MARCADOS CON ASTERISCOS <span class="text-lg text-bold" style="color: red"> ( * ) </span>SON OBLIGATORIOS</p>
                                                </div>
                                            </div>
                                        </div>



                                    </div><!-- fin nacimiento -->

                                    <!-- domicilio -->
                                    <div class="tab-pane" id="step3">

                                        <br><br>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a008_num_pais2" name="form[int][fk_a008_num_pais2]" class="form-control input-sm" required data-msg-required="Seleccione País">
                                                            <option value="">Seleccione el Pais</option>
                                                            {foreach item=i from=$listadoPais}
                                                                {if isset($formDBdomicilio.pais_domicilio)}
                                                                    {if $i.pk_num_pais==$formDBdomicilio.pais_domicilio}
                                                                        <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_pais==$DefaultPais}
                                                                        <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a008_num_pais2"><i class="md md-map"></i> Pais</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a009_num_estado2" name="form[int][fk_a009_num_estado2]"  class="form-control input-sm" required data-msg-required="Seleccione Estado">
                                                            <option value="">Seleccione el Estado</option>
                                                            {foreach item=i from=$listadoEstado}
                                                                {if isset($formDBdomicilio.estado_domicilio)}
                                                                    {if $i.pk_num_estado==$formDBdomicilio.estado_domicilio}
                                                                        <option selected value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_estado==$DefaultEstado}
                                                                        <option selected value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a009_num_estado2"><i class="md md-map"></i> Estado</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a011_num_municipio2" name="form[int][fk_a011_num_municipio2]"  class="form-control input-sm" required data-msg-required="Seleccione Municipio">
                                                            <option value="">Seleccione el Municipio</option>
                                                            {foreach item=i from=$listadoMunicipio}
                                                                {if isset($formDBdomicilio.municipio_domicilio)}
                                                                    {if $i.pk_num_municipio==$formDBdomicilio.municipio_domicilio}
                                                                        <option selected value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_municipio==$DefaultMunicipio}
                                                                        <option selected value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a011_num_municipio2"><i class="md md-map"></i> Municipio</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a010_num_ciudad2" name="form[int][fk_a010_num_ciudad2]"  class="form-control input-sm" required data-msg-required="Seleccione Ciudad">
                                                            <option value="">Seleccione la Ciudad</option>
                                                            {foreach item=i from=$listadoCiudad}
                                                                {if isset($formDBdomicilio.ciudad_domicilio)}
                                                                    {if $i.pk_num_ciudad==$formDBdomicilio.ciudad_domicilio}
                                                                        <option selected value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_ciudad==$DefaultCiudad}
                                                                        <option selected value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a010_num_ciudad2"><i class="md md-map"></i> Ciudad</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="parroquia" name="form[int][parroquia]"  class="form-control input-sm" required>
                                                            <option value="">Seleccione la Parroquia</option>
                                                            {foreach item=i from=$listadoParroquia}
                                                                {if isset($formDBdomicilio.parroquia_domicilio)}
                                                                    {if $i.pk_num_parroquia==$formDBdomicilio.parroquia_domicilio}
                                                                        <option selected value="{$i.pk_num_parroquia}">{$i.ind_parroquia}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_parroquia}">{$i.ind_parroquia}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_parroquia==$DefaultParroquia}
                                                                        <option selected value="{$i.pk_num_parroquia}">{$i.ind_parroquia}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_parroquia}">{$i.ind_parroquia}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="parroquia"><i class="md md-map"></i> Parroquia</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a013_num_sector" name="form[int][fk_a013_num_sector]"  class="form-control input-sm" required>
                                                            <option value="">Seleccione el Sector</option>
                                                            {foreach item=i from=$listadoSector}
                                                                {if isset($formDBdomicilio.sector_domicilio)}
                                                                    {if $i.pk_num_sector==$formDBdomicilio.sector_domicilio}
                                                                        <option selected value="{$i.pk_num_sector}">{$i.ind_sector}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_sector}">{$i.ind_sector}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_sector==$DefaultSector}
                                                                        <option selected value="{$i.pk_num_sector}">{$i.ind_sector}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_sector}">{$i.ind_sector}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a013_num_sector"><i class="md md-map"></i> Sector</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-10">
                                                    <div class="col-lg-12">
                                                        <div class="card">
                                                            <div class="card-body no-padding">
                                                                <div class="table-responsive no-margin">
                                                                    <table class="table table-striped no-margin" id="contenidoTablaDir">
                                                                        <thead>
                                                                        <tr>
                                                                            <th class="text-center">#</th>
                                                                            <th colspan="3">Direcci&oacute;n</th>
                                                                            <th>...</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                        {if isset($formDBdireccion)}
                                                                            {foreach item=datos from=$formDBdireccion}
                                                                                <tr id="dir{$numeroD}" idDir="{$datos.pk_num_direccion_persona}">
                                                                                    <input type="hidden" value="{$datos.pk_num_direccion_persona}" name="form[int][pk_num_persona_direccion][{$n}]" id="pk_num_persona_direccion{$numeroD}">
                                                                                    <td class="text-right" style="vertical-align: middle;">
                                                                                        {$numeroD}
                                                                                    </td>
                                                                                    <td colspan="3" style="vertical-align: middle;">
                                                                                        <div class="col-sm-4">
                                                                                            <div class="form-group">
                                                                                                <select id="fk_a006_num_miscelaneo_detalle_tipodir{$n}" name="form[int][fk_a006_num_miscelaneo_detalle_tipodir][{$n}]" class="form-control input-sm" required data-msg-required="Seleccione Tipo Direccion">
                                                                                                    <option value="">Seleccione...</option>
                                                                                                    {foreach item=dat from=$TipoDir}
                                                                                                        {if isset($datos.fk_a006_num_miscelaneo_detalle_tipodir)}
                                                                                                            {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_tipodir}
                                                                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {else}
                                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                        {/if}
                                                                                                    {/foreach}
                                                                                                </select>
                                                                                                <label for="fk_a006_num_miscelaneo_detalle_tipodir{$n}">Tipo Direccion</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-4">
                                                                                            <div class="form-group">
                                                                                                <select id="fk_a006_num_miscelaneo_detalle_domicilio{$n}" name="form[int][fk_a006_num_miscelaneo_detalle_domicilio][{$n}]" class="form-control input-sm" required data-msg-required="Seleccione Domicilio">
                                                                                                    <option value="">Seleccione...</option>
                                                                                                    {foreach item=dat from=$SituacionDomicilio}
                                                                                                        {if isset($datos.fk_a006_num_miscelaneo_detalle_domicilio)}
                                                                                                            {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_domicilio}
                                                                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {else}
                                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                        {/if}
                                                                                                    {/foreach}
                                                                                                </select>
                                                                                                <label for="fk_a006_num_miscelaneo_detalle_domicilio{$n}">Situacion Domicilio</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-4">
                                                                                            <div class="form-group">
                                                                                                <select id="fk_a006_num_tipo_zona{$n}" name="form[int][fk_a006_num_tipo_zona][{$n}]" class="form-control input-sm" required>
                                                                                                    <option value="">Seleccione...</option>
                                                                                                    {foreach item=dat from=$TipoZona}
                                                                                                        {if isset($datos.fk_a006_num_tipo_zona)}
                                                                                                            {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_tipo_zona}
                                                                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {else}
                                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                        {/if}
                                                                                                    {/foreach}
                                                                                                </select>
                                                                                                <label for="fk_a006_num_tipo_zona{$n}">Tipo Zona</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                            <div class="form-group">
                                                                                                <select id="fk_a006_num_ubicacion_inmueble{$n}" name="form[int][fk_a006_num_ubicacion_inmueble][{$n}]" class="form-control input-sm" required>
                                                                                                    <option value="">Seleccione...</option>
                                                                                                    {foreach item=dat from=$UbicacionInmueble}
                                                                                                        {if isset($datos.fk_a006_num_ubicacion_inmueble)}
                                                                                                            {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_ubicacion_inmueble}
                                                                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {else}
                                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                        {/if}
                                                                                                    {/foreach}
                                                                                                </select>
                                                                                                <label for="fk_a006_num_ubicacion_inmueble{$n}">Ubicacion Inmueble</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-9">
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control input-sm" id="ind_ubicacion_inmueble{$n}" name="form[txt][ind_ubicacion_inmueble][{$n}]" value="{if isset($datos.ind_ubicacion_inmueble)}{$datos.ind_ubicacion_inmueble}{/if}" required>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                            <div class="form-group">
                                                                                                <select id="fk_a006_num_tipo_inmueble{$n}" name="form[int][fk_a006_num_tipo_inmueble][{$n}]" class="form-control input-sm" required>
                                                                                                    <option value="">Seleccione...</option>
                                                                                                    {foreach item=dat from=$TipoInmueble}
                                                                                                        {if isset($datos.fk_a006_num_tipo_inmueble)}
                                                                                                            {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_tipo_inmueble}
                                                                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {else}
                                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                        {/if}
                                                                                                    {/foreach}
                                                                                                </select>
                                                                                                <label for="fk_a006_num_tipo_inmueble{$n}">Tipo Inmueble</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-9">
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control input-sm" id="ind_tipo_inmueble{$n}" name="form[txt][ind_tipo_inmueble][{$n}]" value="{if isset($datos.ind_tipo_inmueble)}{$datos.ind_tipo_inmueble}{/if}" required>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-3">
                                                                                            <div class="form-group">
                                                                                                <select id="fk_a006_num_zona_residencial{$n}" name="form[int][fk_a006_num_zona_residencial][{$n}]" class="form-control input-sm" required>
                                                                                                    <option value="">Seleccione...</option>
                                                                                                    {foreach item=dat from=$ZonaResidencial}
                                                                                                        {if isset($datos.fk_a006_num_zona_residencial)}
                                                                                                            {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_zona_residencial}
                                                                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {else}
                                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                        {/if}
                                                                                                    {/foreach}
                                                                                                </select>
                                                                                                <label for="fk_a006_num_tipo_inmueble{$n}">Zona Residencial</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-9">
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control input-sm" id="ind_zona_residencial{$n}" name="form[txt][ind_zona_residencial][{$n}]" value="{if isset($datos.ind_zona_residencial)}{$datos.ind_zona_residencial}{/if}" required>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-5">
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control input-sm" id="ind_inmueble{$n}" name="form[txt][ind_inmueble][{$n}]" value="{if isset($datos.ind_inmueble)}{$datos.ind_inmueble}{/if}">
                                                                                                <label for="ind_inmueble{$n}">Inmueble</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-5">
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control input-sm" id="ind_punto_referencia{$n}" name="form[txt][ind_punto_referencia][{$n}]" value="{if isset($datos.ind_punto_referencia)}{$datos.ind_punto_referencia}{/if}" required>
                                                                                                <label for="ind_punto_referencia{$n}">Punto de Refrencia</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-2">
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control input-sm" id="ind_zona_postal{$n}" name="form[txt][ind_zona_postal][{$n}]" value="{if isset($datos.ind_zona_postal)}{$datos.ind_zona_postal}{/if}">
                                                                                                <label for="ind_zona_postal{$n}">Zona Postal</label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-sm-12">
                                                                                            <div class="form-group">
                                                                                                <input type="text" class="form-control input-sm" id="ind_direccion{$n}" name="form[txt][ind_direccion][{$n}]" value="{if isset($datos.ind_direccion)}{$datos.ind_direccion}{/if}" required>
                                                                                                <label for="ind_direccion{$n}">Dirección</label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td style="vertical-align: middle;">
                                                                                        <div class="col-sm-12">
                                                                                            {if $operacion == 'revisar' || $operacion == 'aprobar'}

                                                                                            {else}
                                                                                                <button type="button" class="eliminarBD btn ink-reaction btn-raised btn-xs btn-danger" id="dir{$numeroD}" idDir="{$datos.pk_num_direccion_persona}" boton="si, Eliminar" descripcion="Ha Eliminado la Dirección" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Dirección!!"><i class="md md-delete" style="color: #ffffff;"></i></button>
                                                                                            {/if}
                                                                                        </div>
                                                                                    </td>
                                                                                    <input type="hidden" value="{$n++}">
                                                                                    <input type="hidden" value="{$numeroD++}">
                                                                                </tr>
                                                                            {/foreach}
                                                                        {/if}
                                                                        </tbody>
                                                                        <tfoot>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                <div class="col-sm-12 text-center">
                                                                                    <button type="button" id="nuevaDireccion" class="btn btn-info ink-reaction btn-raised">
                                                                                        <i class="md md-add"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="col-lg-12">
                                                        <div class="card">
                                                            <div class="card-body no-padding">
                                                                <div class="table-responsive no-margin">
                                                                    <table class="table table-striped no-margin" id="contenidoTabla">
                                                                        <thead>
                                                                        <tr>
                                                                            <th class="text-center">#</th>
                                                                            <th>Tipo Telefono</th>
                                                                            <th>Telefono</th>
                                                                            <th></th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        {if isset($formDBtelefono)}
                                                                            {foreach item=datos from=$formDBtelefono}
                                                                                <tr id="tlf{$numeroT}" idTlf="{$datos.pk_num_telefono}">
                                                                                    <input type="hidden" value="{$datos.pk_num_telefono}" name="form[int][pk_num_telefono][{$nt}]" id="pk_num_telefono{$numeroT}">
                                                                                    <td class="text-right" style="vertical-align: middle;">
                                                                                        {$numeroT}
                                                                                    </td>
                                                                                    <td style="vertical-align: middle;">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="form-group">
                                                                                                <select id="fk_a006_num_miscelaneo_detalle_tipo_telefono{$nt}" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_telefono][{$nt}]" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">
                                                                                                    <option value="">Seleccione...</option>
                                                                                                    {foreach item=dat from=$TipoTelf}
                                                                                                        {if isset($datos.fk_a006_num_miscelaneo_detalle_tipo_telefono)}
                                                                                                            {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_tipo_telefono}
                                                                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {else}
                                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                        {/if}
                                                                                                    {/foreach}
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td style="vertical-align: middle;">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="form-group floating-label" id="ind_telefono{$nt}Error">
                                                                                                <input type="text" class="form-control input-sm" maxlength="11" value="{$datos.ind_telefono}" name="form[txt][ind_telefono][{$nt}]" id="ind_telefono{$nt}" data-rule-number="true" data-msg-number="Por favor Introduzca solo numeros">
                                                                                                <label for="ind_telefono{$nt}"><i class="md md-insert-comment"></i> Telefono </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td style="vertical-align: middle;">
                                                                                        <div class="col-sm-12">
                                                                                            {if $operacion == 'revisar' || $operacion == 'aprobar'}

                                                                                            {else}
                                                                                            <button type="button" class="eliminarBD btn ink-reaction btn-raised btn-xs btn-danger" id="tlf{$numeroT}" idTlf="{$datos.pk_num_telefono}"  boton="si, Eliminar" descipcion="Ha Eliminado el Telefono" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Telefono!!"><i class="md md-delete" style="color: #ffffff;"></i></button>
                                                                                            {/if}
                                                                                        </div>
                                                                                    </td>
                                                                                    <input type="hidden" value="{$nt++}">
                                                                                    <input type="hidden" value="{$numeroT++}">
                                                                                </tr>
                                                                            {/foreach}
                                                                        {/if}
                                                                        </tbody>
                                                                        <tfoot>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <div class="col-sm-12 text-center">
                                                                                    <button type="button" id="nuevoTelefono" class="btn btn-info ink-reaction btn-raised">
                                                                                        <i class="md md-add"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p class="text-xs text-bold text-right">LOS CAMPOS MARCADOS CON ASTERISCOS <span class="text-lg text-bold" style="color: red"> ( * ) </span>SON OBLIGATORIOS</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- fin domicilio -->
                                    <!--
                                        FICHAS DATOS GENERALES
                                    -->
                                    <div class="tab-pane" id="step4">
                                        <br><br>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_gsanguineo" name="form[int][fk_a006_num_miscelaneo_detalle_gsanguineo]" class="form-control input-sm">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$GrupoSanguineo}
                                                                {if isset($formDBprincipales.fk_a006_num_miscelaneo_det_gruposangre)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBprincipales.fk_a006_num_miscelaneo_det_gruposangre}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle">Grupo Sanguineo</label>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group" id="fk_a006_num_miscelaneo_detalle_edocivilError">
                                                        <select id="fk_a006_num_miscelaneo_detalle_edocivil" name="form[int][fk_a006_num_miscelaneo_detalle_edocivil]" class="form-control input-sm" required data-msg-required="Seleccione Estado Civil">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$EdoCivil}
                                                                {if isset($formDBprincipales.fk_a006_num_miscelaneo_detalle_edocivil)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBprincipales.fk_a006_num_miscelaneo_detalle_edocivil}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle">Estado Civil</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                            </div>


                                            <h4 class="text-primary text-center">Otros Datos (Referencias)</h4>
                                            <hr class="ruler-lg">


                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="well clearfix">
                                                        <input type="hidden" value="{if isset($formDBemergencia[0]['pk_num_empleado_emergencia'])}{$formDBemergencia[0]['pk_num_empleado_emergencia']}{/if}" name="form[int][pk_num_empleado_emergencia1]" id="pk_num_empleado_emergencia1">
                                                        <input type="hidden" value="{if isset($formDBemergencia[0]['pk_num_persona'])}{$formDBemergencia[0]['pk_num_persona']}{/if}" name="form[int][pk_num_persona1]">
                                                        <input type="hidden" value="{if isset($formDBemergencia[0]['pk_num_direccion_persona'])}{$formDBemergencia[0]['pk_num_direccion_persona']}{/if}" name="form[int][pk_num_direccion_persona1]">
                                                        <div class="form-group floating-label">
                                                            <input type="text" class="form-control input-sm" id="cedRef1" name="form[txt][cedRef1]" value="{if isset($formDBemergencia[0]['ind_cedula_documento'])}{$formDBemergencia[0]['ind_cedula_documento']}{/if}">
                                                            <label for="cedRef1">Cedula</label>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <div class="form-group floating-label">
                                                                    <input type="text" class="form-control input-sm" id="nombRef1" name="form[txt][nombRef1]" value="{if isset($formDBemergencia[0]['ind_nombre1'])}{$formDBemergencia[0]['ind_nombre1']}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                                    <label for="nombRef1">Primer Nombre</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <div class="form-group floating-label">
                                                                    <input type="text" class="form-control input-sm" id="apeRef1" name="form[txt][apeRef1]" value="{if isset($formDBemergencia[0]['ind_apellido1'])}{$formDBemergencia[0]['ind_apellido1']}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                                    <label for="apeRef1">Primer Apellido</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">

                                                            <div class="card">
                                                                <div class="card-head">
                                                                    <header>1. Direcci&oacute;n<span class="text-xs"> - Click para visualizar Datos de Direcci&oacute;n</span></header>
                                                                    <div class="tools">
                                                                        <div class="btn-group">
                                                                            <a class="btn btn-icon-toggle btn-collapse boton_ocultar1"><i class="fa fa-angle-down"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div><!--end .card-head -->
                                                                <div class="card-body">
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_miscelaneo_detalle_tipodirRef1" name="form[int][fk_a006_num_miscelaneo_detalle_tipodirRef1]" class="form-control input-sm">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$TipoDir}
                                                                                    {if isset($formDBemergencia[0]['fk_a006_num_miscelaneo_detalle_tipodir'])}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[0]['fk_a006_num_miscelaneo_detalle_tipodir']}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_miscelaneo_detalle_tipodirRef1">Tipo. Dir</label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_miscelaneo_detalle_domicilio1" name="form[int][fk_a006_num_miscelaneo_detalle_domicilio1]" class="form-control input-sm">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$SituacionDomicilio}
                                                                                    {if isset($formDBemergencia[0].fk_a006_num_miscelaneo_detalle_domicilio)}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[0].fk_a006_num_miscelaneo_detalle_domicilio}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_miscelaneo_detalle_domicilio1">Sit. Dom</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_tipo_zona1" name="form[int][fk_a006_num_tipo_zona1]" class="form-control input-sm" required data-msg-required="Requerido">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$TipoZona}
                                                                                    {if isset($formDBemergencia[0].fk_a006_num_tipo_zona)}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[0].fk_a006_num_tipo_zona}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_tipo_zona1">Tipo Zona</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_ubicacion_inmueble1" name="form[int][fk_a006_num_ubicacion_inmueble1]" class="form-control input-sm" required data-msg-required="Requerido">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$UbicacionInmueble}
                                                                                    {if isset($formDBemergencia[0].fk_a006_num_ubicacion_inmueble)}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[0].fk_a006_num_ubicacion_inmueble}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_ubicacion_inmueble1">Ubicacion Inmueble</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-9">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_ubicacion_inmueble1" name="form[txt][ind_ubicacion_inmueble1]" value="{if isset($formDBemergencia[0].ind_ubicacion_inmueble)}{$formDBemergencia[0].ind_ubicacion_inmueble}{/if}" required data-msg-required="Requerido">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_tipo_inmueble1" name="form[int][fk_a006_num_tipo_inmueble1]" class="form-control input-sm" required data-msg-required="Requerido">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$TipoInmueble}
                                                                                    {if isset($formDBemergencia[0].fk_a006_num_tipo_inmueble)}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[0].fk_a006_num_tipo_inmueble}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_tipo_inmueble1">Tipo Inmueble</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-9">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_tipo_inmueble1" name="form[txt][ind_tipo_inmueble1]" value="{if isset($formDBemergencia[0].ind_tipo_inmueble)}{$formDBemergencia[0].ind_tipo_inmueble}{/if}" required data-msg-required="Requerido">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_zona_residencial1" name="form[int][fk_a006_num_zona_residencial1]" class="form-control input-sm" required data-msg-required="Requerido">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$ZonaResidencial}
                                                                                    {if isset($formDBemergencia[0].fk_a006_num_zona_residencial)}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[0].fk_a006_num_zona_residencial}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_tipo_inmueble1">Zona Residencial</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-9">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_zona_residencial1" name="form[txt][ind_zona_residencial1]" value="{if isset($formDBemergencia[0].ind_zona_residencial)}{$formDBemergencia[0].ind_zona_residencial}{/if}" required data-msg-required="Requerido">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_inmueble1" name="form[txt][ind_inmueble1]" value="{if isset($formDBemergencia[0].ind_inmueble)}{$formDBemergencia[0].ind_inmueble}{/if}">
                                                                            <label for="ind_inmueble1">Inmueble</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_punto_referencia1" name="form[txt][ind_punto_referencia1]" value="{if isset($formDBemergencia[0].ind_punto_referencia)}{$formDBemergencia[0].ind_punto_referencia}{/if}" required data-msg-required="Requerido">
                                                                            <label for="ind_punto_referencia1">Punto de Refrencia</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_zona_postal1" name="form[txt][ind_zona_postal1]" value="{if isset($formDBemergencia[0].ind_zona_postal)}{$formDBemergencia[0].ind_zona_postal}{/if}">
                                                                            <label for="ind_zona_postal1">Zona Postal</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="dirRef1" name="form[txt][dirRef1]" value="{if isset($formDBemergencia[0]['ind_direccion'])}{$formDBemergencia[0]['ind_direccion']}{/if}" required data-msg-required="Requerido">
                                                                            <label for="dirRef1">Dirección</label>
                                                                        </div>
                                                                    </div>

                                                                </div><!--end .card-body -->
                                                            </div><!--end .card -->
                                                        </div>

                                                        <div class="row">
                                                            <div class="card">
                                                                <div class="card-body no-padding">
                                                                    <div class="table-responsive no-margin">
                                                                        <table class="table table-striped no-margin" id="contenidoTabla1">
                                                                            <thead>
                                                                            <tr>
                                                                                <th class="text-center">#</th>
                                                                                <th>Tipo Telefono</th>
                                                                                <th>Telefono</th>
                                                                                <th>...</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            {if isset($formDBtlfemerg1)}
                                                                                {foreach item=datos from=$formDBtlfemerg1}
                                                                                    <tr id="tlf1{$numero1}" idTlf1="{$datos.pk_num_telefono}">
                                                                                        <input type="hidden" value="{$datos.pk_num_telefono}" name="form[int][pk_num_telefono1][{$nt1}]">
                                                                                        <td class="text-right" style="vertical-align: middle;">
                                                                                            {$numero1}
                                                                                        </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <div class="col-sm-12">
                                                                                                <div class="form-group">
                                                                                                    <select id="fk_a006_num_miscelaneo_detalle_tipo_telefono1{$nt1}" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_telefono1][{$nt1}]" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">
                                                                                                        <option value="">Seleccione...</option>
                                                                                                        {foreach item=dat from=$TipoTelf}
                                                                                                            {if isset($datos.fk_a006_num_miscelaneo_detalle_tipo_telefono)}
                                                                                                                {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_tipo_telefono}
                                                                                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                                {else}
                                                                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                                {/if}
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {/foreach}
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <div class="col-sm-12">
                                                                                                <div class="form-group floating-label" id="ind_telefono1{$nt1}Error">
                                                                                                    <input type="text" class="form-control input-sm" maxlength="11" value="{$datos.ind_telefono}" name="form[txt][ind_telefono1][{$nt1}]" id="ind_telefono1{$nt1}" data-rule-number="true" data-msg-number="Por favor Introduzca solo numeros">
                                                                                                    <label for="ind_telefono1{$nt1}"><i class="md md-insert-comment"></i> Telefono </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <div class="col-sm-12">
                                                                                                {if $operacion == 'revisar' || $operacion == 'aprobar'}

                                                                                                {else}
                                                                                                <button type="button" class="eliminarBD btn ink-reaction btn-raised btn-xs btn-danger" id="tlf1{$numero1}" idTlf1="{$datos.pk_num_telefono}"  boton="si, Eliminar" descipcion="Ha Eliminado el Telefono" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Telefono!!"><i class="md md-delete" style="color: #ffffff;"></i></button>
                                                                                                {/if}
                                                                                            </div>
                                                                                        </td>
                                                                                        <input type="hidden" value="{$nt1++}">
                                                                                        <input type="hidden" value="{$numero1++}">
                                                                                    </tr>
                                                                                {/foreach}
                                                                            {/if}
                                                                            </tbody>
                                                                            <tfoot>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <div class="col-sm-12 text-center">
                                                                                        <button type="button" id="nuevoTelefono1" class="btn btn-info ink-reaction btn-raised">
                                                                                            <i class="md md-add"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <select id="fk_a006_num_miscelaneo_detalle_parentesco" name="form[int][fk_a006_num_miscelaneo_detalle_parentesco]" class="form-control input-sm" >
                                                                <option value="">Seleccione...</option>
                                                                {foreach item=dat from=$Parentesco}
                                                                    {if isset($formDBemergencia[0]['fk_a006_num_miscelaneo_detalle_parentesco'])}
                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[0]['fk_a006_num_miscelaneo_detalle_parentesco']}
                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="fk_a006_num_miscelaneo_detalle">Parentesco</label>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="well clearfix">
                                                        <input type="hidden" value="{if isset($formDBemergencia[1]['pk_num_empleado_emergencia'])}{$formDBemergencia[1]['pk_num_empleado_emergencia']}{/if}" name="form[int][pk_num_empleado_emergencia2]" id="pk_num_empleado_emergencia2">
                                                        <input type="hidden" value="{if isset($formDBemergencia[1]['pk_num_persona'])}{$formDBemergencia[1]['pk_num_persona']}{/if}" name="form[int][pk_num_persona2]">
                                                        <input type="hidden" value="{if isset($formDBemergencia[1]['pk_num_direccion_persona'])}{$formDBemergencia[1]['pk_num_direccion_persona']}{/if}" name="form[int][pk_num_direccion_persona2]">
                                                        <div class="form-group floating-label">
                                                            <input type="text" class="form-control input-sm" id="cedRef2" name="form[txt][cedRef2]" value="{if isset($formDBemergencia[1]['ind_cedula_documento'])}{$formDBemergencia[1]['ind_cedula_documento']}{/if}">
                                                            <label for="cedRef2">Cedula</label>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <div class="form-group floating-label">
                                                                    <input type="text" class="form-control input-sm" id="nombRef2" name="form[txt][nombRef2]" value="{if isset($formDBemergencia[1]['ind_nombre1'])}{$formDBemergencia[1]['ind_nombre1']}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                                    <label for="nombRef2">Primer Nombre</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <div class="form-group floating-label">
                                                                    <input type="text" class="form-control input-sm" id="apeRef2" name="form[txt][apeRef2]" value="{if isset($formDBemergencia[1]['ind_apellido1'])}{$formDBemergencia[1]['ind_apellido1']}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                                    <label for="apeRef2">Primer Apellido</label>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="row">

                                                            <div class="card">
                                                                <div class="card-head">
                                                                    <header>2. Direcci&oacute;n<span class="text-xs"> - Click para visualizar Datos de Direcci&oacute;n</span></header>
                                                                    <div class="tools">
                                                                        <div class="btn-group">
                                                                            <a class="btn btn-icon-toggle btn-collapse boton_ocultar2"><i class="fa fa-angle-down"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div><!--end .card-head -->
                                                                <div class="card-body">
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_miscelaneo_detalle_tipodirRef2" name="form[int][fk_a006_num_miscelaneo_detalle_tipodirRef2]" class="form-control input-sm">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$TipoDir}
                                                                                    {if isset($formDBemergencia[1]['fk_a006_num_miscelaneo_detalle_tipodir'])}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[1]['fk_a006_num_miscelaneo_detalle_tipodir']}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_miscelaneo_detalle_tipodirRef2">Tipo. Dir</label>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_miscelaneo_detalle_domicilio2" name="form[int][fk_a006_num_miscelaneo_detalle_domicilio2]" class="form-control input-sm">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$SituacionDomicilio}
                                                                                    {if isset($formDBemergencia[1].fk_a006_num_miscelaneo_detalle_domicilio)}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[1].fk_a006_num_miscelaneo_detalle_domicilio}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_miscelaneo_detalle_domicilio2">Sit. Dom</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_tipo_zona2" name="form[int][fk_a006_num_tipo_zona2]" class="form-control input-sm" required data-msg-required="Requerido">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$TipoZona}
                                                                                    {if isset($formDBemergencia[1].fk_a006_num_tipo_zona)}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[1].fk_a006_num_tipo_zona}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_tipo_zona2">Tipo Zona</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_ubicacion_inmueble2" name="form[int][fk_a006_num_ubicacion_inmueble2]" class="form-control input-sm" required data-msg-required="Requerido">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$UbicacionInmueble}
                                                                                    {if isset($formDBemergencia[1].fk_a006_num_ubicacion_inmueble)}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[1].fk_a006_num_ubicacion_inmueble}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_ubicacion_inmueble2">Ubicacion Inmueble</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-9">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_ubicacion_inmueble2" name="form[txt][ind_ubicacion_inmueble2]" value="{if isset($formDBemergencia[1].ind_ubicacion_inmueble)}{$formDBemergencia[1].ind_ubicacion_inmueble}{/if}" required data-msg-required="Requerido">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_tipo_inmueble2" name="form[int][fk_a006_num_tipo_inmueble2]" class="form-control input-sm" required data-msg-required="Requerido">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$TipoInmueble}
                                                                                    {if isset($formDBemergencia[1].fk_a006_num_tipo_inmueble)}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[1].fk_a006_num_tipo_inmueble}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_tipo_inmueble2">Tipo Inmueble</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-9">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_tipo_inmueble2" name="form[txt][ind_tipo_inmueble2]" value="{if isset($formDBemergencia[1].ind_tipo_inmueble)}{$formDBemergencia[1].ind_tipo_inmueble}{/if}" required data-msg-required="Requerido">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <div class="form-group">
                                                                            <select id="fk_a006_num_zona_residencial2" name="form[int][fk_a006_num_zona_residencial2]" class="form-control input-sm" required data-msg-required="Requerido">
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=dat from=$ZonaResidencial}
                                                                                    {if isset($formDBemergencia[1].fk_a006_num_zona_residencial)}
                                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[1].fk_a006_num_zona_residencial}
                                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {else}
                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                        {/if}
                                                                                    {else}
                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                            </select>
                                                                            <label for="fk_a006_num_tipo_inmueble2">Zona Residencial</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-9">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_zona_residencial2" name="form[txt][ind_zona_residencial2]" value="{if isset($formDBemergencia[1].ind_zona_residencial)}{$formDBemergencia[1].ind_zona_residencial}{/if}" required data-msg-required="Requerido">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_inmueble2" name="form[txt][ind_inmueble2]" value="{if isset($formDBemergencia[1].ind_inmueble)}{$formDBemergencia[1].ind_inmueble}{/if}">
                                                                            <label for="ind_inmueble2">Inmueble</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-5">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_punto_referencia2" name="form[txt][ind_punto_referencia2]" value="{if isset($formDBemergencia[1].ind_punto_referencia)}{$formDBemergencia[1].ind_punto_referencia}{/if}" required data-msg-required="Requerido">
                                                                            <label for="ind_punto_referencia2">Punto de Refrencia</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-2">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="ind_zona_postal2" name="form[txt][ind_zona_postal2]" value="{if isset($formDBemergencia[1].ind_zona_postal)}{$formDBemergencia[1].ind_zona_postal}{/if}">
                                                                            <label for="ind_zona_postal1">Zona Postal</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <input type="text" class="form-control input-sm" id="dirRef2" name="form[txt][dirRef2]" value="{if isset($formDBemergencia[1]['ind_direccion'])}{$formDBemergencia[1]['ind_direccion']}{/if}" required data-msg-required="Requerido">
                                                                            <label for="dirRef2">Dirección</label>
                                                                        </div>
                                                                    </div>

                                                                </div><!--end .card-body -->
                                                            </div><!--end .card -->
                                                        </div>
                                                         <div class="row">
                                                            <div class="card">
                                                                <div class="card-body no-padding">
                                                                    <div class="table-responsive no-margin">
                                                                        <table class="table table-striped no-margin" id="contenidoTabla2">
                                                                            <thead>
                                                                            <tr>
                                                                                <th class="text-center">#</th>
                                                                                <th>Tipo Telefono</th>
                                                                                <th>Telefono</th>
                                                                                <th>...</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            {if isset($formDBtlfemerg2)}
                                                                                {foreach item=datos from=$formDBtlfemerg2}
                                                                                    <tr id="tlf2{$numero2}" idTlf2="{$datos.pk_num_telefono}">
                                                                                        <input type="hidden" value="{$datos.pk_num_telefono}" name="form[int][pk_num_telefono2][{$nt2}]">
                                                                                        <td class="text-right" style="vertical-align: middle;">
                                                                                            {$numero2}
                                                                                        </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <div class="col-sm-12">
                                                                                                <div class="form-group">
                                                                                                    <select id="fk_a006_num_miscelaneo_detalle_tipo_telefono2{$nt2}" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_telefono2][{$nt2}]" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">
                                                                                                        <option value="">Seleccione...</option>
                                                                                                        {foreach item=dat from=$TipoTelf}
                                                                                                            {if isset($datos.fk_a006_num_miscelaneo_detalle_tipo_telefono)}
                                                                                                                {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_tipo_telefono}
                                                                                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                                {else}
                                                                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                                {/if}
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {/foreach}
                                                                                                    </select>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <div class="col-sm-12">
                                                                                                <div class="form-group floating-label" id="ind_telefono2{$nt2}Error">
                                                                                                    <input type="text" class="form-control input-sm" maxlength="11" value="{$datos.ind_telefono}" name="form[txt][ind_telefono2][{$nt2}]" id="ind_telefono2{$nt2}" data-rule-number="true" data-msg-number="Por favor Introduzca solo numeros">
                                                                                                    <label for="ind_telefono2{$nt2}"><i class="md md-insert-comment"></i> Telefono </label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td style="vertical-align: middle;">
                                                                                            <div class="col-sm-12">
                                                                                                {if $operacion == 'revisar' || $operacion == 'aprobar'}

                                                                                                {else}
                                                                                                <button type="button" class="eliminarBD btn ink-reaction btn-raised btn-xs btn-danger" id="tlf2{$numero2}" idTlf2="{$datos.pk_num_telefono}"  boton="si, Eliminar" descipcion="Ha Eliminado el Telefono" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Telefono!!"><i class="md md-delete" style="color: #ffffff;"></i></button>
                                                                                                {/if}
                                                                                            </div>
                                                                                        </td>
                                                                                        <input type="hidden" value="{$nt2++}">
                                                                                        <input type="hidden" value="{$numero2++}">
                                                                                    </tr>
                                                                                {/foreach}
                                                                            {/if}
                                                                            </tbody>
                                                                            <tfoot>
                                                                            <tr>
                                                                                <td colspan="3">
                                                                                    <div class="col-sm-12 text-center">
                                                                                        <button type="button" id="nuevoTelefono2" class="btn btn-info ink-reaction btn-raised">
                                                                                            <i class="md md-add"></i>
                                                                                        </button>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tfoot>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <select id="fk_a006_num_miscelaneo_detalle_parentesco" name="form[int][fk_a006_num_miscelaneo_detalle_parentesco2]" class="form-control input-sm" >
                                                                <option value="">Seleccione...</option>
                                                                {foreach item=dat from=$Parentesco}
                                                                    {if isset($formDBemergencia[1]['fk_a006_num_miscelaneo_detalle_parentesco'])}
                                                                        {if $dat.pk_num_miscelaneo_detalle==$formDBemergencia[1]['fk_a006_num_miscelaneo_detalle_parentesco']}
                                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="fk_a006_num_miscelaneo_detalle">Parentesco</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="text-primary text-center">Licencia de Conducir</h4>
                                            <hr class="ruler-lg">
                                            <div class="row">

                                                <div class="card">
                                                    <input type="hidden" value="{if isset($formDBlicencia[0]['pk_num_persona'])}{$formDBlicencia[1]['pk_num_persona']}{/if}" name="form[int][pk_num_personalic]">
                                                    <div class="card-body no-padding">
                                                        <div class="table-responsive no-margin">
                                                            <table class="table table-striped no-margin" id="contenidoTablaLic">
                                                                <thead>
                                                                <tr>
                                                                    <th class="text-center">#</th>
                                                                    <th>Tipo Lic</th>
                                                                    <th>Clase Lic</th>
                                                                    <th>Fecha. Expiración</th>
                                                                    <th>...</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                {if isset($formDBlicencia)}
                                                                    {foreach item=datos from=$formDBlicencia}
                                                                        <tr id="lic{$numerol}" idLic="{$datos.pk_num_licencia}">
                                                                            <input type="hidden" value="{$datos.pk_num_licencia}" name="form[int][pk_num_licencia][{$nl}]" id="pk_num_licencia{$nl}">
                                                                            <td class="text-right" style="vertical-align: middle;">
                                                                                {$numerol}
                                                                            </td>
                                                                            <td style="vertical-align: middle;">
                                                                                <div class="col-sm-12">
                                                                                    <div class="form-group">
                                                                                        <select id="fk_a006_num_miscelaneo_detalle_tipolic{$nl}" name="form[int][fk_a006_num_miscelaneo_detalle_tipolic][{$nl}]" class="form-control input-sm" required data-msg-required="Seleccione Tipo Licencia">
                                                                                            <option value="">Seleccione...</option>
                                                                                            {foreach item=dat from=$TipoLic}
                                                                                                {if isset($datos.fk_a006_num_miscelaneo_detalle_tipolic)}
                                                                                                    {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_tipolic}
                                                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                    {else}
                                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                    {/if}
                                                                                                {else}
                                                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                {/if}
                                                                                            {/foreach}
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td style="vertical-align: middle;">
                                                                                <div class="col-sm-12">
                                                                                    <div class="form-group">
                                                                                        <select id="fk_a006_num_miscelaneo_detalle_claselic{$nl}" name="form[int][fk_a006_num_miscelaneo_detalle_claselic][{$nl}]" class="form-control input-sm" required data-msg-required="Seleccione Clase Licencia">
                                                                                            <option value="">Seleccione...</option>
                                                                                            {foreach item=dat from=$ClaseLic}
                                                                                                {if isset($datos.fk_a006_num_miscelaneo_detalle_claselic)}
                                                                                                    {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_claselic}
                                                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                    {else}
                                                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                    {/if}
                                                                                                {else}
                                                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                {/if}
                                                                                            {/foreach}
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td style="vertical-align: middle;">
                                                                                <div class="col-sm-12">
                                                                                    <div class="form-group floating-label" id="fec_expiracion_licencia{$nl}Error">
                                                                                        <input type="text" class="form-control input-sm" value="{$datos.fec_expiracion_licencia|date_format:"d-m-Y"}" name="form[txt][fec_expiracion_licencia][{$nl}]" id="fec_expiracion_licencia{$nl}">
                                                                                        <label for="fec_expiracion_licencia{$nl}"><i class="md md-insert-comment"></i> Fecha Expiración </label>
                                                                                        <p class="help-block"><span class="text-xs" style="color: red">(dd-mm-YYYY)</span></p>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td style="vertical-align: middle;">
                                                                                <div class="col-sm-12">
                                                                                    {if $operacion == 'revisar' || $operacion == 'aprobar'}

                                                                                    {else}
                                                                                    <button type="button" class="eliminarBD btn ink-reaction btn-raised btn-xs btn-danger" id="lic{$numerol}" idLic="{$datos.pk_num_licencia}"  boton="si, Eliminar" descipcion="Ha Eliminado La Licencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Licencia!!"><i class="md md-delete" style="color: #ffffff;"></i></button>
                                                                                    {/if}
                                                                                </div>
                                                                            </td>
                                                                            <input type="hidden" value="{$nl++}">
                                                                            <input type="hidden" value="{$numerol++}">
                                                                        </tr>
                                                                    {/foreach}
                                                                {/if}
                                                                </tbody>
                                                                <tfoot>
                                                                <tr>
                                                                    <td colspan="5">
                                                                        <div class="col-sm-12 text-center">
                                                                            <button type="button" id="nuevaLic" class="btn btn-info ink-reaction btn-raised">
                                                                                <i class="md md-add"></i>
                                                                            </button>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p class="text-xs text-bold text-right">LOS CAMPOS MARCADOS CON ASTERISCOS <span class="text-lg text-bold" style="color: red"> ( * ) </span>SON OBLIGATORIOS</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div><!-- fin otros datos personales -->

                                    <!-- organizacion -->
                                    <div class="tab-pane" id="step5">

                                        <br><br>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" required data-msg-required="Seleccione Organismo">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$listadoOrganismos}
                                                                {if isset($formDBlaborales.fk_a001_num_organismo)}
                                                                    {if $dat.pk_num_organismo==$formDBlaborales.fk_a001_num_organismo}
                                                                        <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $dat.pk_num_organismo==$DefaultOrg}
                                                                        <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle">Organismo</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group" id="fk_a004_num_dependenciaError">
                                                        <select id="fk_a004_num_dependencia" name="form[int][fk_a004_num_dependencia]" class="form-control input-sm" required data-msg-required="Seleccione Dependencia">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$listadoDependencias}
                                                                {if isset($formDBlaborales.fk_a004_num_dependencia)}
                                                                    {if $dat.pk_num_dependencia==$formDBlaborales.fk_a004_num_dependencia}
                                                                        <option selected value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_dependencia}">{$dat.ind_dependencia}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle">Dependencia</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group" id="fk_a023_num_centro_costoError">
                                                        <select id="fk_a023_num_centro_costo" name="form[int][fk_a023_num_centro_costo]"  class="form-control input-sm" required data-msg-required="Seleccione Centro de Costo">
                                                            <option value="">Seleccione Centro de Costo</option>
                                                        </select>
                                                        <label for="fk_a023_num_centro_costo"><i class="md md-map"></i> Centro de Costo</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="text-primary text-center">Datos Nómina</h4>
                                            <hr class="ruler-xl">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control input-sm" required data-msg-required="Seleccione Tipo Nomina">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$listadoTipoNomina}
                                                                {if isset($formDBlaborales.fk_nmb001_num_tipo_nomina)}
                                                                    {if $dat.pk_num_tipo_nomina==$formDBlaborales.fk_nmb001_num_tipo_nomina}
                                                                        <option selected value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $dat.pk_num_tipo_nomina==$DefaultTipNom}
                                                                        <option selected value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_nmb001_num_tipo_nomina">Tipo Nomina</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_tipopago" name="form[int][fk_a006_num_miscelaneo_detalle_tipopago]" class="form-control input-sm" required data-msg-required="Seleccione Tipo de Pago">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$TipoPago}
                                                                {if isset($formDBlaborales.fk_a006_num_miscelaneo_detalle_tipopago)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBlaborales.fk_a006_num_miscelaneo_detalle_tipopago}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$DefaultTipPag}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_tipopago">Tipo de Pago</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="num_tipo_trabajador" name="form[int][num_tipo_trabajador]" class="form-control input-sm" required data-msg-required="Seleccione Tipo de Trabajador">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$listadoTipoTrabajador}
                                                                {if isset($formDBlaborales.fk_a006_num_miscelaneo_detalle_tipotrabajador)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBlaborales.fk_a006_num_miscelaneo_detalle_tipotrabajador}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$DefaultTipTrab}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="num_tipo_trabajadors">Tipo Trabajador</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="text-primary text-center">Horario Laboral</h4>
                                            <hr class="ruler-xl">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_rhb007_num_horario" name="form[int][fk_rhb007_num_horario]" class="form-control input-sm" required data-msg-required="Seleccione Horario Laboral">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$listadoHorario}
                                                                {if isset($formDBlaborales.fk_rhb007_num_horario)}
                                                                    {if $dat.pk_num_horario==$formDBlaborales.fk_rhb007_num_horario}
                                                                        <option selected value="{$dat.pk_num_horario}">{$dat.ind_descripcion}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_horario}">{$dat.ind_descripcion}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $dat.pk_num_horario==$DefaultHor}
                                                                        <option selected value="{$dat.pk_num_horario}">{$dat.ind_descripcion}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_horario}">{$dat.ind_descripcion}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_rhb007_num_horario">Horario Laboral</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <p class="text-xs text-bold text-right">LOS CAMPOS MARCADOS CON ASTERISCOS <span class="text-lg text-bold" style="color: red"> ( * ) </span>SON OBLIGATORIOS</p>
                                                </div>
                                            </div>
                                        </div>

                                    </div><!-- fin organizacion -->


                                    <!-- datos laborales -->
                                    <div class="tab-pane" id="step6">

                                        <br><br>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group" id="fechaIngresoError">
                                                        <div class="input-group date" id="fechaIng">
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" name="form[txt][fechaIngreso]" id="fechaIngreso" value="{if isset($formDBlaborales.fec_ingreso)}{$formDBlaborales.fec_ingreso|date_format:"d-m-Y"}{/if}" required data-msg-required="Indique Fecha de Ingreso">
                                                                <label for="fechaIngreso">Fecha de Ingreso</label>
                                                            </div>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="nroResolucion" name="form[txt][nroResolucion]"  value="{if isset($formDBlaborales.ind_resolucion_ingreso)}{$formDBlaborales.ind_resolucion_ingreso}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                        <label for="nroResolucion">Nro. Resolución</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="text-primary text-center">Cese</h4>
                                            <hr class="ruler-lg">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="fechaCese" name="fechaCese" disabled>
                                                        <label for="fechaCese">Fecha de Cese</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="motivoCese" name="motivoCese" disabled>
                                                        <label for="motivoCese">Motivo Cese</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="explicacionCese" name="explicacionCese" disabled>
                                                        <label for="explicacionCese">Explicación</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">

                                                    <div class="form-group">
                                                        <div class="checkbox-inline checkbox-styled checkbox-primary">
                                                            <label>
                                                                <input type="checkbox" {if isset($formDBprincipales.ind_estatus_empleado) and $formDBprincipales.ind_estatus_empleado==1} checked{/if}  value="1" name="form[int][ind_estatus]" id="ind_estatus" >
                                                                <span>Situación de Trabajo</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <!--<div class="form-group">
                                                        <select id="ind_estatus" name="form[int][ind_estatus]" class="form-control input-sm">
                                                            {foreach key=key item=item from=$EstadoRegistro}
                                                                {if isset($formDBprincipales.ind_estatus_empleado)}
                                                                    {if $key==$formDBprincipales.ind_estatus_empleado}
                                                                        <option selected value="{$key}">{$item}</option>
                                                                    {else}
                                                                        <option value="{$key}">{$item}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$key}">{$item}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="ind_estatus">Situacion de Trabajo</label>
                                                    </div>-->
                                                </div>
                                            </div>

                                            <h4 class="text-primary text-center">Estructura del Cargo</h4>
                                            <hr class="ruler-lg">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group" id="fk_a006_num_miscelaneo_detalle_grupoocupacionalError">
                                                        <select id="fk_a006_num_miscelaneo_detalle_grupoocupacional" name="form[int][fk_a006_num_miscelaneo_detalle_grupoocupacional]" class="form-control input-sm" required data-msg-required="Seleccione Grupo Ocupacional">
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$listadoGrupoOcupacional}
                                                                {if isset($formDBlaborales.fk_a006_num_miscelaneo_detalle_grupoocupacional)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBlaborales.fk_a006_num_miscelaneo_detalle_grupoocupacional}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_grupoocupacional">Grupo Ocupacional</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group" id="fk_rhc010_num_serieError">
                                                        <select id="fk_rhc010_num_serie" name="form[int][fk_rhc010_num_serie]"  class="form-control input-sm" required data-msg-required="Seleccione Serie Ocupacional">
                                                            <option value="">Seleccione Serie Ocupacional</option>
                                                        </select>
                                                        <label for="fk_rhc010_num_serie">Serie Ocupacional</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">

                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="num_monto_jub_pen" name="form[txt][num_monto_jub_pen]" value="{if isset($formDBlaboralesSueldo.num_sueldo)}{$formDBlaboralesSueldo.num_sueldo|number_format:2:",":"."}{/if}" disabled>
                                                        <label for="num_monto_jub_pen">Monto Jubilaci&oacute;n o Pensi&oacute;n</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group" id="fk_rhc063_num_puestos_cargoError">
                                                        <select id="fk_rhc063_num_puestos_cargo" name="form[int][fk_rhc063_num_puestos_cargo]"  class="form-control input-sm" required data-msg-required="Seleccione Cargo">
                                                            <option value="">Seleccione Cargo</option>
                                                        </select>
                                                        <label for="fk_rhc063_num_puestos_cargo">Cargo</label>
                                                        <p class="help-block"><span class="text-xs text-bold" style="color: red; font-size: 11px">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_paso" name="form[int][fk_a006_num_miscelaneo_detalle_paso]" class="form-control input-sm" disabled>
                                                            {foreach item=i from=$Pasos}
                                                                {if isset($formDBlaborales.fk_a006_num_miscelaneo_detalle_paso)}
                                                                    {if $i.pk_num_miscelaneo_detalle==$formDBlaborales.fk_a006_num_miscelaneo_detalle_paso}
                                                                        <option selected value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_paso">Paso</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="categoria" name="form[txt][categoria]" value="{if isset($formDBlaborales.categoria_cargo)}{$formDBlaborales.categoria_cargo}{/if}" disabled>
                                                        <label for="Categoria">Categoria</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="num_sueldo_basico" name="form[txt][num_sueldo_basico]" value="{if isset($formDBlaboralesSueldo.num_sueldo_promedio)}{$formDBlaboralesSueldo.num_sueldo_promedio|number_format:2:",":"."}{/if}" disabled>
                                                        <label for="num_sueldo_basico">Sueldo</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <p class="text-xs text-bold text-right">LOS CAMPOS MARCADOS CON ASTERISCOS <span class="text-lg text-bold" style="color: red"> ( * ) </span>SON OBLIGATORIOS</p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>



                                    </div><!-- fin datos laborales -->


                                    <!-- area de botones -->
                                    <div class="row">
                                        <div class="col-md-12" align="right">
                                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                            {if $operacion=='registrar' || $operacion=='modificar'}
                                                <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                                            {/if}
                                            {if $operacion=='modificarp'}
                                                <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Modificar</button>
                                            {/if}
                                            {if $operacion=='revisar'}
                                                <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="revisar">Revisar</button>
                                                <button type="button" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" id="anular" data-toggle="modal" data-target="#formModal2"
                                                        data-keyboard="false" data-backdrop="static" titulo="Indicar Motivo de Anulación"><span class="icm icm-blocked"></span> Anular</button>
                                            {/if}
                                            {if $operacion=='aprobar'}
                                                <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="aprobar">Aprobar</button>
                                                <button type="button" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" id="anular" data-toggle="modal" data-target="#formModal2"
                                                        data-keyboard="false" data-backdrop="static" titulo="Indicar Motivo de Anulación"><span class="icm icm-blocked"></span> Anular</button>
                                            {/if}
                                            <input type="hidden" name="operacion" id="operacion" value="{$operacion}">
                                        </div>
                                    </div>


                                </div>

                                <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                                <ul class="pager wizard">
                                    <li class="previous first" id="primero"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <!-- <li class="next last" id="ultimo"><a class="ink-reaction btn-raised" href="javascript:void(0);">Ultimo</a></li>-->
                                    <li class="next" id="siguiente"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                                </ul>

                            </form>
                        </div>

            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //inicializar el datepicker
    $("#fecha").datepicker({
        format:'dd-mm-yyyy',
        language:'es',
        autoclose: true
    });

    $("#fechaIng").datepicker({
        format:'dd-mm-yyyy',
        language:'es',
        autoclose: true
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();

            var app = new  AppFunciones();

            //verifico que los campos requeridos no esten vacios
            var FECHANACIMIENTO = $("#fechaNacimiento").val();
            var EDOCIVIL = $("#fk_a006_num_miscelaneo_detalle_edocivil").val();
            var FECHAINGRESO = $("#fechaIngreso").val();
            var GRUPOOCUPACIONAL = $("#fk_a006_num_miscelaneo_detalle_grupoocupacional").val();
            var SERIEOCUPACIONAL = $("#fk_rhc010_num_serie").val();
            var CARGO = $("#fk_rhc063_num_puestos_cargo").val();
            var DEPENDENCIA = $("#fk_a004_num_dependencia").val();
            var CENTROCOSTO = $("#fk_a023_num_centro_costo").val();

            if(FECHANACIMIENTO==''){
                swal("Advertencia!", "Hay campos vacios en el Formulario. Por Favor revise los Datos", "error");
                $(document.getElementById('fechaNacimientoError')).removeClass('has-success has-feedback');
                $(document.getElementById('fechaNacimientoError')).removeClass('has-error has-feedback');
                $(document.getElementById('fechaNacimientoError')).addClass('has-error has-feedback');
                return false;
            }
            else if(EDOCIVIL==''){
                swal("Advertencia!", "Hay campos vacios en el Formulario. Por Favor revise los Datos", "error");
                $(document.getElementById('fk_a006_num_miscelaneo_detalle_edocivilError')).removeClass('has-success has-feedback');
                $(document.getElementById('fk_a006_num_miscelaneo_detalle_edocivilError')).removeClass('has-error has-feedback');
                $(document.getElementById('fk_a006_num_miscelaneo_detalle_edocivilError')).addClass('has-error has-feedback');
                return false;
            }
            else if(DEPENDENCIA==''){
                swal("Advertencia!", "Hay campos vacios en el Formulario. Por Favor revise los Datos", "error");
                $(document.getElementById('fk_a004_num_dependenciaError')).removeClass('has-success has-feedback');
                $(document.getElementById('fk_a004_num_dependenciaError')).removeClass('has-error has-feedback');
                $(document.getElementById('fk_a004_num_dependenciaError')).addClass('has-error has-feedback');
                return false;
            }
            else if(CENTROCOSTO==''){
                swal("Advertencia!", "Hay campos vacios en el Formulario. Por Favor revise los Datos", "error");
                $(document.getElementById('fk_a023_num_centro_costoError')).removeClass('has-success has-feedback');
                $(document.getElementById('fk_a023_num_centro_costoError')).removeClass('has-error has-feedback');
                $(document.getElementById('fk_a023_num_centro_costoError')).addClass('has-error has-feedback');
                return false;
            }
            else if(FECHAINGRESO==''){
                swal("Advertencia!", "Hay campos vacios en el Formulario. Por Favor revise los Datos", "error");
                $(document.getElementById('fechaIngresoError')).removeClass('has-success has-feedback');
                $(document.getElementById('fechaIngresoError')).removeClass('has-error has-feedback');
                $(document.getElementById('fechaIngresoError')).addClass('has-error has-feedback');
                return false;
            }
            else if(GRUPOOCUPACIONAL==''){
                swal("Advertencia!", "Hay campos vacios en el Formulario. Por Favor revise los Datos", "error");
                $(document.getElementById('fk_a006_num_miscelaneo_detalle_grupoocupacionalError')).removeClass('has-success has-feedback');
                $(document.getElementById('fk_a006_num_miscelaneo_detalle_grupoocupacionalError')).removeClass('has-error has-feedback');
                $(document.getElementById('fk_a006_num_miscelaneo_detalle_grupoocupacionalError')).addClass('has-error has-feedback');
                return false;
            }
            else if(SERIEOCUPACIONAL=='') {
                swal("Advertencia!", "Hay campos vacios en el Formulario. Por Favor revise los Datos", "error");
                $(document.getElementById('fk_rhc010_num_serieError')).removeClass('has-success has-feedback');
                $(document.getElementById('fk_rhc010_num_serieError')).removeClass('has-error has-feedback');
                $(document.getElementById('fk_rhc010_num_serieError')).addClass('has-error has-feedback');
                return false;
            }
            else if(CARGO==''){
                swal("Advertencia!", "Hay campos vacios en el Formulario. Por Favor revise los Datos", "error");
                $(document.getElementById('fk_rhc063_num_puestos_cargoError')).removeClass('has-success has-feedback');
                $(document.getElementById('fk_rhc063_num_puestos_cargoError')).removeClass('has-error has-feedback');
                $(document.getElementById('fk_rhc063_num_puestos_cargoError')).addClass('has-error has-feedback');
                return false;
            }
            else {

                $.post('{$_Parametros.url}modRH/gestion/empleadosCONTROL/EmpleadoMET', datos, function (dato) {

                    if (dato['status'] == 'error') {
                        /*Error para usuarios normales del sistema*/
                        swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        /*Error para los programadores para visualizar en detalle el ERROR*/
                        //swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");
                    }
                    if (dato['status'] == 'modificacion') {
                        swal("Registro Modificado!", "El Empleado se modifico satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }
                    if (dato['status'] == 'modificacionp') {
                        app.metActualizarRegistroTablaJson('listadoE', 'Operación Realizada Satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                    }
                    else if (dato['status'] == 'creacion') {
                        swal("Registro Guardado!", "EL Empleado fue guardado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    } else if (dato['status'] == 'revisar') {
                        swal("Registro Revisado!", 'Operación Realizada Satisfactoriamente.', "success");
                        $('#listadoE').dataTable().api().row().remove().draw(false);
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    } else if (dato['status'] == 'aprobar') {
                        swal("Registro Aprobado!", 'Operación Realizada Satisfactoriamente.', "success");
                        $('#listadoE').dataTable().api().row().remove().draw(false);
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }

                }, 'json');
            }


        }
    });


    $(document).ready(function() {



        var rh  = new  AppFunciones();
        var rh2 = new  ModRhFunciones();

        var idPais   = '{if isset($formDBnacimiento.pais_nacimiento)}{$formDBnacimiento.pais_nacimiento}{/if}';
        var idEstado = '{if isset($formDBnacimiento.estado_nacimiento)}{$formDBnacimiento.estado_nacimiento}{/if}';
        var idCiudad = '{if isset($formDBnacimiento.ciudad_nacimiento)}{$formDBnacimiento.ciudad_nacimiento}{/if}';
        var idMunicipio = '{if isset($formDBnacimiento.municipio_nacimiento)}{$formDBnacimiento.municipio_nacimiento}{/if}';

        var idPais2   = '{if isset($formDBdomicilio.pais_domicilio)}{$formDBdomicilio.pais_domicilio}{/if}';
        var idEstado2 = '{if isset($formDBdomicilio.estado_domicilio)}{$formDBdomicilio.estado_domicilio}{/if}';
        var idCiudad2 = '{if isset($formDBdomicilio.ciudad_domicilio)}{$formDBdomicilio.ciudad_domicilio}{/if}';
        var idMunicipio2 = '{if isset($formDBdomicilio.municipio_domicilio)}{$formDBdomicilio.municipio_domicilio}{/if}';
        var idParroquia  = '{if isset($formDBdomicilio.parroquia_domicilio)}{$formDBdomicilio.parroquia_domicilio}{/if}';
        var idSector     = '{if isset($formDBdomicilio.sector_domicilio)}{$formDBdomicilio.sector_domicilio}{/if}';

        var idDependencia = '{if isset($formDBlaborales.fk_a004_num_dependencia)}{$formDBlaborales.fk_a004_num_dependencia}{/if}';
        var idCentroCosto = '{if isset($formDBlaborales.fk_a023_num_centro_costo)}{$formDBlaborales.fk_a023_num_centro_costo}{/if}';
        var idGrupoOcupacional = '{if isset($formDBlaborales.fk_a006_num_miscelaneo_detalle_grupoocupacional)}{$formDBlaborales.fk_a006_num_miscelaneo_detalle_grupoocupacional}{/if}';
        var idSerieOcupacional = '{if isset($formDBlaborales.fk_rhc010_num_serie)}{$formDBlaborales.fk_rhc010_num_serie}{/if}';
        var idCargo = '{if isset($formDBlaborales.fk_rhc063_num_puestos_cargo)}{$formDBlaborales.fk_rhc063_num_puestos_cargo}{/if}';

        rh.metJsonEstado('{$_Parametros.url}estado/jsonEstado',idPais,idEstado);
        rh2.metJsonMunicipioN('{$_Parametros.url}municipio/JsonMunicipio',idEstado,idMunicipio);
        rh2.metJsonCiudadN('{$_Parametros.url}ciudad/JsonCiudad',idEstado,idCiudad);

        rh2.metJsonEstadoD('{$_Parametros.url}estado/jsonEstado',idPais2,idEstado2);
        rh2.metJsonCiudadD('{$_Parametros.url}ciudad/JsonCiudad',idEstado2,idCiudad2);
        rh2.metJsonMunicipioD('{$_Parametros.url}municipio/JsonMunicipio',idEstado2,idMunicipio2);
        rh2.metJsonParroquiaD('{$_Parametros.url}modRH/gestion/empleadosCONTROL/JsonParroquiaMET',idMunicipio2,idParroquia);
        rh2.metJsonSectorD('{$_Parametros.url}modRH/gestion/empleadosCONTROL/JsonSectorMET',idParroquia,idSector);

        rh2.metJsonCentroCosto('{$_Parametros.url}modRH/gestion/empleadosCONTROL/JsonCentroCostoMET',idDependencia,idCentroCosto);
        rh2.metJsonSerieOcupacional('{$_Parametros.url}modRH/maestros/serieOcupacionalCONTROL/JsonSerieOcupacionalMET',idGrupoOcupacional,idSerieOcupacional);
        rh2.metJsonCargos('{$_Parametros.url}modRH/maestros/cargosCONTROL/JsonCargosMET',idGrupoOcupacional,idSerieOcupacional,idCargo);

        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho').css( "width", "85%" );

        $("#cargarImagen").css({ 'width':'140px', 'height':'150px' });

        //Script para forzar el avance del wizard por el botón suigiente.
        $(".form-wizard-nav").on('click', function(){

            var form = $('#rootwizard2').find('.form-validation');
            var valid = form.valid();
            if(!valid) {
                form.data('validator').focusInvalid();
                return false;
            }

        });


        if( $("#operacion").val()== 'revisar' || $("#operacion").val()== 'aprobar'){
            $(".form-control").attr("disabled","disabled");//desabilito el formulario
        }


        //para ocultar direccion en datos de referencias
        $(".boton_ocultar1").click();
        $(".boton_ocultar2").click();

        if($("#pk_num_empleado_emergencia1").val()!=''){
            $(".boton_ocultar1").click();
        }
        if($("#pk_num_empleado_emergencia2").val()!=''){
            $(".boton_ocultar2").click();
        }

         /*A CCION DEL BOTON ANULAR EMPLEADO EMPLEADO
         * SI EL REGISTRO ESTA EN ESTADO PREPARADO SE ANULA Y NO PUEDE SER YA UTILIZADO
         * SI EL REGISTRO ESTA EN ESTADO REVISADO PASA A PREPARADO
         */
        $("#anular").click(function(){

            if( $("#operacion").val()== 'revisar'){
                var estado  = 'AN';
            }else {
                var estado  = 'PR';
            }

            var estado_actual = $("#estado_aprobacion").val();

            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post("{$_Parametros.url}modRH/gestion/empleadosCONTROL/IndicarMotivoAnulacionMET",{ idEmpleado: {$idEmpleado}, estado_nuevo:estado, estado_actual:estado_actual },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        //FUNCIONAMIENTO TELEFONOS EMPLEADOS
        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTabla1  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTabla2  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTablaDir tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTablaLic tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });





        $("#nuevoTelefono").click(function() {
            var idtabla= $("#contenidoTabla");
            var tr= $("#contenidoTabla > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;

            var eliminar = '<button class="eliminarTMP btn ink-reaction btn-raised btn-xs btn-danger" idTlf=tlf'+numero+'  boton="si, Eliminar" descipcion="Ha Eliminado la linea" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Linea!!"><i class="md md-delete" style="color: #ffffff;"></i></button>';


            var tipoTelefono='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_tipo_telefono'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_telefono]['+nuevoTr+']" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">'+
                    '<option value="">Seleccione...</option>'+
                    '{foreach item=dat from=$TipoTelf}'+
                            '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                    '{/foreach}'+
                    '</select>'+
                    '</div>';
            var telefono='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" maxlength="11" value="" name="form[txt][ind_telefono]['+nuevoTr+']" id="ind_telefono'+nuevoTr+'" data-rule-number="true" data-msg-number="Por favor Introduzca solo numeros" required>'+
                    '<label for="ind_telefono'+nuevoTr+'"><i class="md md-insert-comment"></i> Telefono '+numero+'</label>'+
                    '</div>';
            idtabla.append('<tr id=tlf'+numero+'>'+
                    '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+tipoTelefono+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+telefono+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+eliminar+'</div></td>'+
                    '</tr>');
        });

        $('#contenidoTabla tbody').on( 'click', '.eliminarTMP', function () {

            var id=$(this).attr('idTlf');
            $(document.getElementById(id)).remove();

        });



        $("#nuevoTelefono1").click(function() {
            var idtabla= $("#contenidoTabla1");
            var tr= $("#contenidoTabla1 > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;

            var eliminar = '<button class="eliminarTMP btn ink-reaction btn-raised btn-xs btn-danger" idTlf1=tlf1'+numero+'  boton="si, Eliminar" descipcion="Ha Eliminado la linea" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Linea!!"><i class="md md-delete" style="color: #ffffff;"></i></button>';

            var tipoTelefono='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_tipo_telefono1'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_telefono1]['+nuevoTr+']" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">'+
                    '<option value="">Seleccione...</option>'+
                    '{foreach item=dat from=$TipoTelf}'+
                    '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                    '{/foreach}'+
                    '</select>'+
                    '</div>';
            var telefono='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" maxlength="11" value="" name="form[txt][ind_telefono1]['+nuevoTr+']" id="ind_telefono1'+nuevoTr+'" data-rule-number="true" data-msg-number="Por favor Introduzca solo numeros" required>'+
                    '<label for="ind_telefono1'+nuevoTr+'"><i class="md md-insert-comment"></i> Telefono '+numero+'</label>'+
                    '</div>';
            idtabla.append('<tr id=tlf1'+numero+'>'+
                    '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+tipoTelefono+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+telefono+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+eliminar+'</div></td>'+
                    '</tr>');
        });
        $('#contenidoTabla1 tbody').on( 'click', '.eliminarTMP', function () {

            var id=$(this).attr('idTlf1');
            $(document.getElementById(id)).remove();
            /*swal("Eliminado!", "Linea Eliminada.", "success");
            $('#cerrar').click();*/
        });

        $("#nuevoTelefono2").click(function() {
            var idtabla= $("#contenidoTabla2");
            var tr= $("#contenidoTabla2 > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;

            var eliminar = '<button class="eliminarTMP btn ink-reaction btn-raised btn-xs btn-danger" idTlf2=tlf2'+numero+'  boton="si, Eliminar" descipcion="Ha Eliminado la linea" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Linea!!"><i class="md md-delete" style="color: #ffffff;"></i></button>';

            var tipoTelefono='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_tipo_telefono2'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_telefono2]['+nuevoTr+']" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">'+
                    '<option value="">Seleccione...</option>'+
                    '{foreach item=dat from=$TipoTelf}'+
                    '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                    '{/foreach}'+
                    '</select>'+
                    '</div>';
            var telefono='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" maxlength="11" value="" name="form[txt][ind_telefono2]['+nuevoTr+']" id="ind_telefono2'+nuevoTr+'" data-rule-number="true" data-msg-number="Por favor Introduzca solo numeros" required>'+
                    '<label for="ind_telefono2'+nuevoTr+'"><i class="md md-insert-comment"></i> Telefono '+numero+'</label>'+
                    '</div>';
            idtabla.append('<tr id=tlf2'+numero+'>'+
                    '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+tipoTelefono+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+telefono+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+eliminar+'</div></td>'+
                    '</tr>');
        });
        $('#contenidoTabla2 tbody').on( 'click', '.eliminarTMP', function () {

            var id=$(this).attr('idTlf2');
            $(document.getElementById(id)).remove();
        });

        $("#nuevaDireccion").click(function() {
            var idtabla= $("#contenidoTablaDir");
            var tr= $("#contenidoTablaDir > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;

            var eliminar = '<button class="eliminarTMP btn ink-reaction btn-raised btn-xs btn-danger" iddir=dir'+numero+'  boton="si, Eliminar" descipcion="Ha Eliminado la linea" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Linea!!"><i class="md md-delete" style="color: #ffffff;"></i></button>';

            var tipoDireccion='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_tipodir'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_tipodir]['+nuevoTr+']" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">'+
                    '<option value="">Seleccione...</option>'+
                    '{foreach item=dat from=$TipoDir}'+
                    '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                    '{/foreach}'+
                    '</select>'+
                    '<label for="fk_a006_num_miscelaneo_detalle_tipodir'+nuevoTr+'">Tipo Direccion '+numero+'</label>'+
                    '</div>';

            var direccion='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" value="" name="form[txt][ind_direccion]['+nuevoTr+']" id="ind_direccion'+nuevoTr+'" required>'+
                    '<label for="ind_direccion'+nuevoTr+'"><i class="md md-insert-comment"></i> Direccion '+numero+'</label>'+
                    '</div>';

            var situDomicilio='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_domicilio'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_domicilio]['+nuevoTr+']" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">'+
                    '<option value="">Seleccione...</option>'+
                    '{foreach item=dat from=$SituacionDomicilio}'+
                    '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                    '{/foreach}'+
                    '</select>'+
                    '<label for="fk_a006_num_miscelaneo_detalle_domicilio'+nuevoTr+'">Situacion Domicilio '+numero+'</label>'+
                    '</div>';

            var tipoZona='<div class="form-group">'+
                '<select id="fk_a006_num_tipo_zona'+nuevoTr+'" name="form[int][fk_a006_num_tipo_zona]['+nuevoTr+']" class="form-control input-sm" required>'+
                '<option value="">Seleccione...</option>'+
                '{foreach item=dat from=$TipoZona}'+
                '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                '{/foreach}'+
                '</select>'+
                '<label for="fk_a006_num_tipo_zona'+nuevoTr+'">Tipo Zona '+numero+'</label> '+
                '</div>';

            var ubicacionInmueble='<div class="form-group">'+
                '<select id="fk_a006_num_ubicacion_inmueble'+nuevoTr+'" name="form[int][fk_a006_num_ubicacion_inmueble]['+nuevoTr+']" class="form-control input-sm" required>'+
                '<option value="">Seleccione...</option>'+
                '{foreach item=dat from=$UbicacionInmueble}'+
                '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                '{/foreach}'+
                '</select>'+
                '<label for="fk_a006_num_ubicacion_inmueble'+nuevoTr+'">Ubicacion Inmueble '+numero+'</label>'+
                '</div>';
            var indUbicacionInmueble='<div class="form-group">'+
                '<input type="text" class="form-control input-sm" id="ind_ubicacion_inmueble'+nuevoTr+'" name="form[txt][ind_ubicacion_inmueble]['+nuevoTr+']" required>'+
                '</div>';

            var tipoInmueble='<div class="form-group">'+
                '<select id="fk_a006_num_tipo_inmueble'+nuevoTr+'" name="form[int][fk_a006_num_tipo_inmueble]['+nuevoTr+']" class="form-control input-sm" required>'+
                '<option value="">Seleccione...</option>'+
                '{foreach item=dat from=$TipoInmueble}'+
                '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                '{/foreach}'+
                '</select>'+
                '<label for="fk_a006_num_tipo_inmueble'+nuevoTr+'">Tipo Inmueble '+numero+'</label>'+
                '</div>';
            var indTipoInmueble='<div class="form-group">'+
                '<input type="text" class="form-control input-sm" id="ind_tipo_inmueble'+nuevoTr+'" name="form[txt][ind_tipo_inmueble]['+nuevoTr+']" required>'+
                '</div>';

            var zonaResidencial='<div class="form-group">'+
                '<select id="fk_a006_num_zona_residencial'+nuevoTr+'" name="form[int][fk_a006_num_zona_residencial]['+nuevoTr+']" class="form-control input-sm" required>'+
                '<option value="">Seleccione...</option>'+
                '{foreach item=dat from=$ZonaResidencial}'+
                '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                '{/foreach}'+
                '</select>'+
                '<label for="fk_a006_num_zona_residencial'+nuevoTr+'">Zona Residencial '+numero+'</label>'+
                '</div>';
            var indZonaResidencial='<div class="form-group">'+
                '<input type="text" class="form-control input-sm" id="ind_zona_residencial'+nuevoTr+'" name="form[txt][ind_zona_residencial]['+nuevoTr+']" required>'+
                '</div>';

            var inmueble='<div class="form-group">'+
                '<input type="text" class="form-control input-sm" id="ind_inmueble'+nuevoTr+'" name="form[txt][ind_inmueble]['+nuevoTr+']" >'+
                '<label for="ind_inmueble'+nuevoTr+'">Inmueble '+numero+'</label>'+
                '</div>';
            var puntoReferencia='<div class="form-group">'+
                '<input type="text" class="form-control input-sm" id="ind_punto_referencia'+nuevoTr+'" name="form[txt][ind_punto_referencia]['+nuevoTr+']" required>'+
                '<label for="ind_punto_referencia'+nuevoTr+'">Punto de Refrencia '+numero+'</label>'+
                '</div>';
            var zonaPostal='<div class="form-group">'+
                '<input type="text" class="form-control input-sm" id="ind_zona_postal'+nuevoTr+'" name="form[txt][ind_zona_postal]['+nuevoTr+']" >'+
                '<label for="ind_zona_postal'+nuevoTr+'">Zona Postal '+numero+'</label>'+
                '</div>';

            idtabla.append('<tr id=dir'+numero+'>'+
                    '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                    '<td colspan="3" style="vertical-align: middle;"><div class="col-sm-4">'+tipoDireccion+'</div><div class="col-sm-4">'+situDomicilio+'</div><div class="col-sm-4">'+tipoZona+'</div><div class="col-sm-3">'+ubicacionInmueble+'</div><div class="col-sm-9">'+indUbicacionInmueble+'</div><div class="col-sm-3">'+tipoInmueble+'</div><div class="col-sm-9">'+indTipoInmueble+'</div><div class="col-sm-3">'+zonaResidencial+'</div><div class="col-sm-9">'+indZonaResidencial+'</div><div class="col-sm-5">'+inmueble+'</div><div class="col-sm-5">'+puntoReferencia+'</div><div class="col-sm-2">'+zonaPostal+'</div><div class="col-sm-12">'+direccion+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+eliminar+'</div></td>'+
                    '</tr>');
        });

        $('#contenidoTablaDir tbody').on( 'click', '.eliminarTMP', function () {

            var id=$(this).attr('iddir');
            $(document.getElementById(id)).remove();

        });

        $("#nuevaLic").click(function() {
            var idtabla= $("#contenidoTablaLic");
            var tr= $("#contenidoTablaLic > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;

            var eliminar = '<button class="eliminarTMP btn ink-reaction btn-raised btn-xs btn-danger" idlic=lic'+numero+'  boton="si, Eliminar" descipcion="Ha Eliminado la linea" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Linea!!"><i class="md md-delete" style="color: #ffffff;"></i></button>';

            var tipoLic='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_tipolic'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_tipolic]['+nuevoTr+']" class="form-control input-sm" required>'+
                    '<option value="">Seleccione...</option>'+
                    '{foreach item=dat from=$TipoLic}'+
                    '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                    '{/foreach}'+
                    '</select>'+
                    //'<label for="fk_a006_num_miscelaneo_detalle_tipolic'+nuevoTr+'"><i class="md md-insert-comment"></i> Tipo Lic. '+numero+'</label>'+
                    '</div>';
            var claseLic='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_claselic'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_claselic]['+nuevoTr+']" class="form-control input-sm" required>'+
                    '<option value="">Seleccione...</option>'+
                    '{foreach item=dat from=$ClaseLic}'+
                    '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                    '{/foreach}'+
                    '</select>'+
                    //'<label for="fk_a006_num_miscelaneo_detalle_claselic'+nuevoTr+'"><i class="md md-insert-comment"></i> Clase Lic. '+numero+'</label>'+
                    '</div>';
            var fecha='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" value="" name="form[txt][fec_expiracion_licencia]['+nuevoTr+']" id="fec_expiracion_licencia'+nuevoTr+'" required>'+
                    '<label for="fec_expiracion_licencia'+nuevoTr+'"><i class="md md-insert-comment"></i> Fecha. Expira</label>'+
                    '<p class="help-block"><span class="text-xs" style="color: red">(dd-mm-YYYY)</span></p>'+
                    '</div>';
            idtabla.append('<tr id=lic'+numero+'>'+
                    '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+tipoLic+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+claseLic+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+fecha+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+eliminar+'</div></td>'+
                    '</tr>');
        });

        $('#contenidoTablaLic tbody').on( 'click', '.eliminarTMP', function () {

            var id=$(this).attr('idlic');
            $(document.getElementById(id)).remove();

        });



        //ELIMINAR DIRECCION, TELEFONOS, TELEFONOS DE EMERGENCIA, LICENCIA
        $('#contenidoTablaDir tbody').on( 'click', '.eliminarBD', function () {

            var id = $(this).attr('idDir');
            var linea = $(this).attr('id');


            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/empleadosCONTROL/EliminarDireccionMET';
                $.post($url, { id: id },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById(linea)).remove();
                        $("#pk_num_persona_direccion"+linea).val("");
                        swal("Eliminado!", "Dirección Eliminada.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });


        $('#contenidoTabla tbody').on( 'click', '.eliminarBD', function () {

            var id = $(this).attr('idTlf');
            var linea = $(this).attr('id');


            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/empleadosCONTROL/EliminarTelefonoMET';
                $.post($url, { id: id },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById(linea)).remove();
                        $("#pk_num_telefono"+linea).val("");
                        swal("Eliminado!", "Telefono Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        $('#contenidoTabla1 tbody').on( 'click', '.eliminarBD', function () {

            var id = $(this).attr('idTlf1');
            var linea = $(this).attr('id');


            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/empleadosCONTROL/EliminarTelefonoMET';
                $.post($url, { id: id },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById(linea)).remove();
                        $("#pk_num_telefono1"+linea).val("");
                        swal("Eliminado!", "Telefono Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        $('#contenidoTabla2 tbody').on( 'click', '.eliminarBD', function () {

            var id = $(this).attr('idTlf2');
            var linea = $(this).attr('id');


            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/empleadosCONTROL/EliminarTelefonoMET';
                $.post($url, { id: id },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById(linea)).remove();
                        $("#pk_num_telefono2"+linea).val("");
                        swal("Eliminado!", "Telefono Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        $('#contenidoTablaLic tbody').on( 'click', '.eliminarBD', function () {

            var id = $(this).attr('idLic');
            var linea = $(this).attr('id');

            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/empleadosCONTROL/EliminarLicenciaMET';
                $.post($url, { id: id },function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById(linea)).remove();
                        $("#pk_num_licencia"+linea).val("");
                        swal("Eliminado!", "Licencia Eliminada.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

        //ACCION PARA AMPLIAR FOTO
        $('#ampliarFoto').click(function(){
            $('#formModalLabel2').html('Foto Ampliada');
            $('#ContenidoModal2').html('hola mundo');
        });

        //ANCHO DE LA MODAL SECUNDARIA
        $('#modalAncho2').css( "width", "40%" );

        /*
        AL INTRODUCIR LA CEDULA DE LA PERSONA VERIFICO SI SE ENCUETRA REGISTRADO
         */
        $("#cedula").change(function(){

            rh2.metBuscarPersona('{$_Parametros.url}modRH/gestion/empleadosCONTROL/BuscarPersonaMET',$(this).val(),false);
            var ci = $("#cedula").val().trim();
            $("#ci").html(ci);

        });


        //AL SELECIONAR CARGO (SELECT) PARA VISUALIZAR LA CATEGORIA DEL CARGO Y EL SUELDO
        $("#fk_rhc063_num_puestos_cargo").change(function () {
            $("#fk_rhc063_num_puestos_cargo option:selected").each(function () {
                $.post("{$_Parametros.url}modRH/gestion/empleadosCONTROL/ObtenerSueldoBasicoMET", { idCargo: $('#fk_rhc063_num_puestos_cargo').val() }, function(data){
                    $("#categoria").val(data['ind_nombre_detalle']);
                    $("#num_sueldo_basico").val(data['num_sueldo_basico']);
                    $("#fk_a006_num_miscelaneo_detalle_paso").val(data['paso']);
                },'json');
            });
        });


        //AL DAR CLICK EN LA IMAGEN AVATAR DEL FORMULARIO
        $("#cargarImagen").click(function() {

            $("#Foto").click();

        });

        //AL DAR CLICK EN LA IMAGEN AVATAR DEL FORMULARIO
        $("#Foto").change(function(e) {

            var files = e.target.files; // FileList object
            //obtenemos un array con los datos del archivo
            var file = $("#Foto")[0].files[0];
            //obtenemos el nombre del archivo
            var fileName = file.name;
            //obtenemos la extensión del archivo
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);

            if(isImage(fileExtension))
            {
                // Obtenemos la imagen del campo "file".
                for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }
                    var reader = new FileReader();
                    reader.onload = (function(theFile) {
                        return function(e) {
                            // Insertamos la imagen
                            $("#cargarImagen").attr("src",e.target.result);
                            $("#cargarImagen").css({ 'width':'140px', 'height':'150px' });
                            $("#img").attr("src",e.target.result);

                            var data = new FormData($("#formAjax")[0]);
                            data.append('tipo','FOTO');

                            $.ajax({
                                url: '{$_Parametros.url}modRH/gestion/empleadosCONTROL/CopiarImagenTmpMET',
                                data: data,
                                type: 'POST',
                                contentType: false,
                                processData: false,
                                success: function(data){
                                    //la asigno al campo de texto
                                    $("#aleatorio").val(data);
                                }
                            });
                        };
                    })(f);
                    reader.readAsDataURL(f);
                }
            }else{

                swal("Error En Imagen!", "Asegurese de subir una imagen tipo: jpg - jpeg - png", "error");

            }


        });


        //PARA ESCRIBIR NOMBRE COMPLETO****************************
        $("#apellido1").change(function(){
            var NomCompleto = new String();
            var ape1 = $("#apellido1").val().trim();
            var ape2 = $("#apellido2").val().trim();
            var nom1 = $("#nombre1").val().trim();
            var nom2 = $("#nombre2").val().trim();

            if (nom1 != "") NomCompleto += nom1 + " ";
            if (nom2 != "") NomCompleto += nom2 + " ";
            if (ape1 != "") NomCompleto += ape1 + " ";
            if (ape2 != "") NomCompleto += ape2 + " ";

            $("#NombCompleto").val(NomCompleto);
            $("#nom").html(NomCompleto);
        });
        $("#apellido2").change(function(){
            var NomCompleto = new String();
            var ape1 = $("#apellido1").val().trim();
            var ape2 = $("#apellido2").val().trim();
            var nom1 = $("#nombre1").val().trim();
            var nom2 = $("#nombre2").val().trim();

            if (nom1 != "") NomCompleto += nom1 + " ";
            if (nom2 != "") NomCompleto += nom2 + " ";
            if (ape1 != "") NomCompleto += ape1 + " ";
            if (ape2 != "") NomCompleto += ape2 + " ";

            $("#NombCompleto").val(NomCompleto);
            $("#nom").html(NomCompleto);
        });
        $("#nombre1").change(function(){
            var NomCompleto = new String();
            var ape1 = $("#apellido1").val().trim();
            var ape2 = $("#apellido2").val().trim();
            var nom1 = $("#nombre1").val().trim();
            var nom2 = $("#nombre2").val().trim();

            if (nom1 != "") NomCompleto += nom1 + " ";
            if (nom2 != "") NomCompleto += nom2 + " ";
            if (ape1 != "") NomCompleto += ape1 + " ";
            if (ape2 != "") NomCompleto += ape2 + " ";

            $("#NombCompleto").val(NomCompleto);
            $("#nom").html(NomCompleto);
        });
        $("#nombre2").change(function(){
            var NomCompleto = new String();
            var ape1 = $("#apellido1").val().trim();
            var ape2 = $("#apellido2").val().trim();
            var nom1 = $("#nombre1").val().trim();
            var nom2 = $("#nombre2").val().trim();

            if (nom1 != "") NomCompleto += nom1 + " ";
            if (nom2 != "") NomCompleto += nom2 + " ";
            if (ape1 != "") NomCompleto += ape1 + " ";
            if (ape2 != "") NomCompleto += ape2 + " ";

            $("#NombCompleto").val(NomCompleto);
            $("#nom").html(NomCompleto);
        });


    });

    //comprobamos si el archivo a subir es una imagen
    //para visualizarla una vez haya subido
    function isImage(extension)
    {
        switch(extension.toLowerCase())
        {
            case 'jpg':  case 'png': case 'jpeg':
            return true;
            break;
            default:
                return false;
                break;
        }
    }




</script>