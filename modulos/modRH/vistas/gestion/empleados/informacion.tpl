<div class="col-md-12">
<form action="" class="form" role="form">

    <!--EMPLEADO-->
    <div class="alert alert-info" role="alert">
        <strong>{if isset($datos['empleado_registro'])}{$datos['empleado_registro']}{/if}</strong>
    </div>

    <!--INFORMACION DE PREPARACION-->
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="preparadoPor" name="preparadoPor"  value="{if isset($datos['preparador_por'])}{$datos['preparador_por']}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                <label for="preparadoPor">Preparado Por</label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="FechaPreparadoPor" name="FechaPreparadoPor"  value="{if isset($datos['fec_preparado'])}{$datos['fec_preparado']|date_format:"d-m-Y"}{/if}">
                <label for="FechaPreparadoPor">Fecha</label>
            </div>
        </div>
    </div>

    <!--INFORMACION DE REVISION-->
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="revisadoPor" name="revisadoPor"  value="{if isset($datos['revisado_por'])}{$datos['revisado_por']}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                <label for="revisadoPor">Revisado Por</label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="FechaRevisadoPor" name="FechaRevisadoPor"  value="{if isset($datos['fec_revisado'])}{$datos['fec_revisado']|date_format:"d-m-Y"}{/if}">
                <label for="FechaRevisadoPor">Fecha</label>
            </div>
        </div>
    </div>

    <!--INFORMACION DE APROBACION-->
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="aprobadoPor" name="aprobadoPor"  value="{if isset($datos['aprobado_por'])}{$datos['aprobado_por']}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                <label for="aprobadoPor">Aprobado Por</label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="FechaAprobadoPor" name="FechaAprobadoPor"  value="{if isset($datos['fec_aprobado'])}{$datos['fec_aprobado']|date_format:"d-m-Y"}{/if}">
                <label for="FechaAprobadoPor">Fecha</label>
            </div>
        </div>
    </div>

    <div class="row">
        <div align="center">
            <button type="button" class="btn btn-default ink-reaction btn-raised" descipcionModal="Aceptar" data-dismiss="modal">Aceptar</button>
        </div>
    </div>
</form>
</div>



<script type="text/javascript">

    $(document).ready(function() {

        $('#modalAncho').css( "width", "40%" );

    });

</script>