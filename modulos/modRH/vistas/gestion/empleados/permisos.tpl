{if $Empleado==0}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->
    <div class="card">
        <div class="card-body">
            <div class="alert alert-danger" role="alert">
                <strong>NO SE HA SELECCIONADO NINGUN REGISTRO DE EMPLEADO</strong>
            </div>
        </div><!--end .card-body -->
    </div><!--end .card -->
{else}
    <!-- no se ha seleccionado ningun registro se muestra advertencia -->


    <div class="well clearfix">
        <div class="col-sm-12">
            <h4 class="text-primary text-center">Datos del Empleado</h4>
        </div>
        <div class="col-sm-12">
            Empleado: {$Empleado}
        </div>
        <div class="col-sm-12">
            Nombres y Apellidos: {$Nombres}
        </div>
    </div>

    <div class="card">
        <div class="card-head">
            <div class="col-sm-12 text-right">
                <button class="btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"
                        titulo="Reporte Permisos del Empleado" id="btnGenerarRepPermisos" >
                    <span class="md-file-download"></span> Generar Reporte &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </button>
                <br>
            </div>
        </div>

        <div class="card-body">
            <input type="hidden" name="idEmpleado" id="idEmpleado" value="{$Empleado}">
            <table id="datatable2" class="table table-striped no-margin table-condensed">
                <thead>
                <tr>
                    <th width="8%">Permiso</th>
                    <th width="20%">Motivo de Ausencia</th>
                    <th width="20%">Tipo de Evento</th>
                    <th width="18%" class="text-center">Desde</th>
                    <th width="18%" class="text-center">Hasta</th>
                    <th width="10%">Estatus</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=dat from=$listadoPermisos}
                    <tr id="idPermisos{$dat.pk_num_permiso}" valor="{$dat.pk_num_permiso}" style="font-size: 11px">
                        <td width="8%">{$dat.pk_num_permiso}</td>
                        <td width="20%">{$dat.motivo_ausencia}</td>
                        <td width="20%">{$dat.tipo_ausencia}</td>
                        <td width="18%" class="text-center">{$dat.fecha_salida}  {$dat.fec_hora_salida}</td>
                        <td width="18%" class="text-center">{$dat.fecha_entrada}  {$dat.fec_hora_entrada}</td>
                        <td width="10%">{$dat.ind_estado}</td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>


{/if}


<script type="text/javascript">

    $(document).ready(function() {

        //ancho de la ventana modal
        $('#modalAncho').css( "width", "70%" );

        var $url = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/GenerarReportePermisoMET';
        $('#btnGenerarRepPermisos').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { idEmpleado: {$Empleado} } ,function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        //inicializo el datatbales
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });




    });

</script>