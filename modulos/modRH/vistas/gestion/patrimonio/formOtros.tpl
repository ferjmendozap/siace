<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idPatrimonioOtros}" name="idPatrimonioOtros"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="ind_descripcion" name="form[txt][ind_descripcion]" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" required data-msg-required="Indique Descripción" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_descripcion">Descripción</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm bolivar-mask" id="num_valor_compra" name="form[int][num_valor_compra]" value="{if isset($formDB.num_valor_compra)}{$formDB.num_valor_compra|number_format:2:",":"."}{/if}">
                                            <label for="num_valor_compra">Valor Compra</label>
                                        </div>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm bolivar-mask" id="num_valor_actual" name="form[int][num_valor_actual]" value="{if isset($formDB.num_valor_actual)}{$formDB.num_valor_actual|number_format:2:",":"."}{/if}">
                                            <label for="num_valor_actual">Valor Actual</label>
                                        </div>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                                <div class="row">
                                    <br><br>
                                    <div align="right">
                                        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> Cancelar</button>
                                        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"> Guardar</button>
                                    </div>
                                </div>


                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">



    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/OtrosMET', datos ,function(dato){

                if(dato['status']=='error'){

                    swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");

                }else if(dato['status']=='modificar'){

                    $(document.getElementById('idPatrimonioOtros'+dato['idPatrimonioOtros'])).html('<td width="35%">'+dato['ind_descripcion']+'</td>' +
                            '<td width="20%">'+dato['num_valor_compra']+'</td>' +
                            '<td width="20%">'+dato['num_valor_actual']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idPatrimonioOtros="'+dato['idPatrimonioOtros']+'"' +
                            'descipcion="El Usuario a Modificado Patrimonio Otros" titulo="Modificar Registro Patrimonio Otros">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPatrimonioOtros="'+dato['idPatrimonioOtros']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a Eliminado Patrimonio Otros" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Otros!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');
                    /*ACTUALIZO TOTALES*/
                    $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$idEmpleado} } ,function(dato){
                        $("#inmuebleT").val(dato[0]);
                        $("#inversionT").val(dato[1]);
                        $("#vehiculoT").val(dato[2]);
                        $("#cuentaT").val(dato[3]);
                        $("#otrosT").val(dato[4]);
                        $("#total").val(dato[5]);
                    },'json');

                }else if(dato['status']=='creacion'){

                        $(document.getElementById('datatableOtros')).append('<tr id="idPatrimonioOtros'+dato['idPatrimonioOtros']+'" style="font-size: 11px">' +
                                '<td width="35%">'+dato['ind_descripcion']+'</td>' +
                                '<td width="20%">'+dato['num_valor_compra']+'</td>' +
                                '<td width="20%">'+dato['num_valor_actual']+'</td>' +
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                                'data-keyboard="false" data-backdrop="static" idPatrimonioOtros="'+dato['idPatrimonioOtros']+'"' +
                                'descipcion="El Usuario a Modificado Patrimonio Otros" titulo="Modificar Registro Patrimonio Otros">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPatrimonioOtros="'+dato['idPatrimonioOtros']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a Eliminado Patrimonio Otros" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Otros!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');
                    /*ACTUALIZO TOTALES*/
                    $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$idEmpleado} } ,function(dato){
                        $("#inmuebleT").val(dato[0]);
                        $("#inversionT").val(dato[1]);
                        $("#vehiculoT").val(dato[2]);
                        $("#cuentaT").val(dato[3]);
                        $("#otrosT").val(dato[4]);
                        $("#total").val(dato[5]);
                    },'json');
                }
            },'json');
        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "35%" );


    });






</script>