<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idPatrimonioCuenta}" name="idPatrimonioCuenta"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>



                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="ind_banco" name="form[txt][ind_banco]" value="{if isset($formDB.ind_banco)}{$formDB.ind_banco}{/if}" required data-msg-required="Indique Banco" onkeyup="$(this).val($(this).val().toUpperCase())">
                                        <label for="ind_banco">Institución</label>
                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                    </div>
                                </div><!--end .col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select id="fk_a006_num_miscelaneo_detalle_tipocta" name="form[int][fk_a006_num_miscelaneo_detalle_tipocta]" class="form-control input-sm" required data-msg-required="Seleccione Tipo de Cuenta">
                                            <option value="">Seleccione...</option>
                                            {foreach item=dat from=$listadoTipoCta}
                                                {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipocta)}
                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipocta}
                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {else}
                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                        <label for="fk_a006_num_miscelaneo_detalle_tipocta">Tipo de Cuenta</label>
                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" maxlength="20" id="ind_cuenta"  name="form[txt][ind_cuenta]" value="{if isset($formDB.ind_cuenta)}{$formDB.ind_cuenta}{/if}" data-rule-number="true" required data-msg-required="Introduzca Numero de Cuenta" data-msg-number="Por favor Introduzca solo numeros">
                                        <label for="ind_cuenta">Cuenta</label>
                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                    </div>
                                </div><!--end .col -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm bolivar-mask" id="num_monto" name="form[int][num_monto]" value="{if isset($formDB.num_monto)}{$formDB.num_monto|number_format:2:",":"."}{/if}">
                                        <label for="num_monto">Valor</label>
                                    </div>
                                </div><!--end .col -->
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div align="right">
                                        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> Cancelar</button>
                                        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"> Guardar</button>
                                    </div>
                                </div>
                            </div>


                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">



    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/CuentasMET', datos ,function(dato){

                if(dato['status']=='error'){

                    swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");

                }else if(dato['status']=='modificar'){

                    $(document.getElementById('idPatrimonioCuenta'+dato['idPatrimonioCuenta'])).html('<td width="45%">'+dato['ind_banco']+'</td>' +
                            '<td width="15%">'+dato['tipocta']+'</td>' +
                            '<td width="15%">'+dato['ind_cuenta']+'</td>' +
                            '<td width="15%">'+dato['num_monto']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idPatrimonioCuenta="'+dato['idPatrimonioCuenta']+'"' +
                            'descipcion="El Usuario a Modificado Patrimonio Cuenta" titulo="Modificar Registro Patrimonio Cuenta">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPatrimonioCuenta="'+dato['idPatrimonioCuenta']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Patrimonio Cuenta" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Cuenta!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');
                    /*ACTUALIZO TOTALES*/
                    $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$idEmpleado} } ,function(dato){
                        $("#inmuebleT").val(dato[0]);
                        $("#inversionT").val(dato[1]);
                        $("#vehiculoT").val(dato[2]);
                        $("#cuentaT").val(dato[3]);
                        $("#otrosT").val(dato[4]);
                        $("#total").val(dato[5]);
                    },'json');

                }else if(dato['status']=='creacion'){


                        $(document.getElementById('datatableCuentas')).append('<tr id="idPatrimonioCuenta'+dato['idPatrimonioCuenta']+'" style="font-size: 11px">' +
                                '<td width="45%">'+dato['ind_banco']+'</td>' +
                                '<td width="15%">'+dato['tipocta']+'</td>' +
                                '<td width="15%">'+dato['ind_cuenta']+'</td>' +
                                '<td width="15%">'+dato['num_monto']+'</td>' +
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                                'data-keyboard="false" data-backdrop="static" idPatrimonioCuenta="'+dato['idPatrimonioCuenta']+'"' +
                                'descipcion="El Usuario a Modificado Patrimonio Cuenta" titulo="Modificar Registro Patrimonio Cuenta">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPatrimonioCuenta="'+dato['idPatrimonioCuenta']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado Patrimonio Cuenta" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patrimonio Cuenta!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');
                    /*ACTUALIZO TOTALES*/
                    $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$idEmpleado} } ,function(dato){
                        $("#inmuebleT").val(dato[0]);
                        $("#inversionT").val(dato[1]);
                        $("#vehiculoT").val(dato[2]);
                        $("#cuentaT").val(dato[3]);
                        $("#otrosT").val(dato[4]);
                        $("#total").val(dato[5]);
                    },'json');

                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "55%" );


    });






</script>