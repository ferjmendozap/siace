<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idPatrimonioInversion}" name="idPatrimonioInversion"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="150" id="ind_titular" name="form[txt][ind_titular]" value="{if isset($formDB.ind_titular)}{$formDB.ind_titular}{/if}" required data-msg-required="Indique Titular" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_titular">Titular</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="150" id="ind_remitente" name="form[txt][ind_remitente]" value="{if isset($formDB.ind_remitente)}{$formDB.ind_remitente}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_remitente">Remitente</label>
                                        </div>
                                    </div><!--end .col -->
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="30" id="ind_certificado" name="form[txt][ind_certificado]" value="{if isset($formDB.ind_certificado)}{$formDB.ind_certificado}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_certificado">Certificado</label>
                                        </div>
                                    </div><!--end .col -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_cant" name="form[int][num_cant]" value="{if isset($formDB.num_cant)}{$formDB.num_cant}{/if}" required data-msg-required="Indique Cantidad" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="num_cant">Cantidad</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm bolivar-mask" id="num_valor_nominal" name="form[int][num_valor_nominal]" value="{if isset($formDB.num_valor_nominal)}{$formDB.num_valor_nominal|number_format:2:",":"."}{/if}">
                                            <label for="num_valor_nominal">Valor Nominal</label>
                                        </div>
                                    </div><!--end .col -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm bolivar-mask" id="num_valor" name="form[int][num_valor]" value="{if isset($formDB.num_valor)}{$formDB.num_valor|number_format:2:",":"."}{/if}">
                                            <label for="num_valor">Valor</label>
                                        </div>
                                    </div><!--end .col -->
                                    <div class="col-md-4">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" {if isset($formDB.num_flag_garantia) and $formDB.num_flag_garantia==1} checked{/if} value="1" name="form[int][num_flag_garantia]">
                                                <span>En Garantia?</span>
                                            </label>
                                        </div>
                                    </div><!--end .col -->
                                </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div align="right">
                                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> Cancelar</button>
                                                <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"> Guardar</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">



    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/InversionMET', datos ,function(dato){

                if(dato['status']=='modificar'){

                    $(document.getElementById('idPatrimonioInversion'+dato['idPatrimonioInversion'])).html('<td width="25%">'+dato['ind_titular']+'</td>' +
                            '<td width="25%">'+dato['ind_remitente']+'</td>' +
                            '<td width="18%">'+dato['ind_certificado']+'</td>' +
                            '<td width="12%">'+dato['num_valor_nominal']+'</td>' +
                            '<td width="12%">'+dato['num_valor']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idPatrimonioInversion="'+dato['idPatrimonioInversion']+'"' +
                            'descipcion="El Usuario a Modificado Patrimonio Inversion" titulo="Modificar Registro Patrimonio Inversión">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPatrimonioInversion="'+dato['idPatrimonioInversion']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Patrimonio Inversion" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patromonio Inversión!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');
                    /*ACTUALIZO TOTALES*/
                    $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$idEmpleado} } ,function(dato){
                        $("#inmuebleT").val(dato[0]);
                        $("#inversionT").val(dato[1]);
                        $("#vehiculoT").val(dato[2]);
                        $("#cuentaT").val(dato[3]);
                        $("#otrosT").val(dato[4]);
                        $("#total").val(dato[5]);
                    },'json');


                }else if(dato['status']=='creacion'){


                        $(document.getElementById('datatableInversion')).append('<tr id="idPatrimonioInversion'+dato['idPatrimonioInversion']+'" style="font-size: 11px">' +
                                '<td width="25%">'+dato['ind_titular']+'</td>' +
                                '<td width="25%">'+dato['ind_remitente']+'</td>' +
                                '<td width="18%">'+dato['ind_certificado']+'</td>' +
                                '<td width="12%">'+dato['num_valor_nominal']+'</td>' +
                                '<td width="12%">'+dato['num_valor']+'</td>' +
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                                'data-keyboard="false" data-backdrop="static" idPatrimonioInversion="'+dato['idPatrimonioInversion']+'"' +
                                'descipcion="El Usuario a Modificado Patrimonio Inversion" titulo="Modificar Registro Patrimonio Inversión">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPatrimonioInversion="'+dato['idPatrimonioInversion']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado Patrimonio Inversion" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Patromonio Inversión!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');
                    /*ACTUALIZO TOTALES*/
                    $.post('{$_Parametros.url}modRH/gestion/patrimonioCONTROL/ConsultarTotalesPatrimoniosMET', { idEmpleado:{$idEmpleado} } ,function(dato){
                        $("#inmuebleT").val(dato[0]);
                        $("#inversionT").val(dato[1]);
                        $("#vehiculoT").val(dato[2]);
                        $("#cuentaT").val(dato[3]);
                        $("#otrosT").val(dato[4]);
                        $("#total").val(dato[5]);
                    },'json');

                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "55%" );



    });






</script>