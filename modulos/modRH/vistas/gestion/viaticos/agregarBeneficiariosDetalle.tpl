
<section class="style-default-light">&nbsp;
    <div class="row"> {*define una fila de la grilla (numero 1)*}
        <div class="col-lg-12">{*identifica el espacio de colunnas que acupara el div en la fila*}
            <div class="card">{*define un panel tipo tarjeta*}
                <div class="card-body ">{*cuerpo del panel tipo tarjeta*}
                    <form id="formAjax" class="form form-validate " role="form"  >{*se define un form que contendra los campos de formulario, necesario para que las clases form-group floating-label en los campos funcionen correctamente*}{*la clase form-validate necesaria para que las validaciones se muestren el los campos requeridos *}
                        <div class="col-lg-6">{*espacio que ocupara el campo en la fila*}
                            <div class="form-group floating-label"   >{*clase para el correcto estilo y efecto label flotante del campo, cada campo debe tener esta clase*}
                                <div class="input-group">{*clase que define a un grupo de campos dentro de un form*}
                                    <div class="input-group-addon">{*clase para el correcto posicionamiento del check en el grupo de campos*}
                                        <div class="checkbox checkbox-inline checkbox-styled">{*clase para el estylo y efectos del checked*}
                                            <label>
                                                <input disabled checked id="check_1Detalle" type="checkbox">{*checked para que el campo este chekado*}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group-content">
                                        <select id="ind_descripcion_empresa3" disabled="disabled" class="form-control input-sm" name="ind_descripcion_empresa" >
                                            {foreach item=indice from=$datosOrganismos}
                                                <option value="{$indice.pk_num_organismo}">{$indice.ind_descripcion_empresa}</option>
                                            {/foreach}
                                        </select>
                                        <label for="ind_descripcion_empresa3">Organismo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">{*espacio que ocupara el campo en la fila*}
                            <div class="form-group floating-label"   >{*clase para el correcto estilo y efecto label flotante del campo, cada campo debe tener esta clase*}
                                <div class="input-group">{*clase que define a un grupo de campos dentro de un form*}
                                    <div class="input-group-addon">{*clase para el correcto posicionamiento del check en el grupo de campos*}
                                        <div class="checkbox checkbox-inline checkbox-styled">{*clase para el estylo y efectos del checked*}
                                            <label>
                                                <input class="cajaCheck1" id="check_2Detalle" type="checkbox">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group-content">
                                        <select id="ind_dependencia3" disabled="disabled" class="form-control input-sm selectpicker" name="form[int][ind_dependencia3]" >{*input-sm para el tamaño de la fuente*}
                                            {foreach item=indice from=$datosDependencias}
                                                <option value="{$indice.pk_num_dependencia }" >{$indice.ind_dependencia}</option>
                                            {/foreach}
                                        </select>
                                        <label for="ind_dependencia3">Dependencia</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </form>
					<br />
                </div><!--end .card-body -->
                <br>
            </div><!--end .card -->
        </div><!--end .col -->
    </div>{*fin de la fila 1*}
    <div class="row">
        <div class="col-lg-12">
            <div align="center">
                <button type="button" id="buscarBeneficiariosDetalle" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" >Buscar</button>
            </div>
        </div>
    </div>
    <br><br>
    <div class="row" >{*fila numero 2*}
        <div class="col-lg-12">
            <table id="datatable2" class="table table-striped table-hover" WIDTH="100%" >
                <thead>
                <tr  align="center">
                    <th ><i>#</i>Código</th>
                    <th ><i class="md-person"></i>Nombre Completo</th>
                    <th ><i class="md-today"></i>Nro. Documento</th>
                    <th ><i class="md-today"></i>Fecha de Ingreso</th>
                    <th ><i class="md-work"></i>Cargo</th>

                </tr>
                </thead>
                <tbody >
				{foreach item=indice from=$listasEmpleados}
                <tr class="fila_agregar" >
                    <td>{$indice.pk_num_empleado}</td>
                    <td>{$indice.ind_nombre1} {$indice.ind_nombre2} {$indice.ind_apellido1} {$indice.ind_apellido2} </td>
                    <td>{$indice.ind_cedula_documento}</td>
                    <td>{$indice.fec_ingreso}</td>
                    <td>{$indice.ind_descripcion_cargo} </td>
                    
                </tr>
                {/foreach}
			    </tbody>
            </table>
        </div>
    </div>{* fin de fila row *}
</section>
<style>
    .fila_agregar{
        cursor: pointer;
    }

    .fila_agregar_sobre{
        background-color: #6EC58B !important; /*#ffccee !important;*/
    }


</style>
<script>
$(document).ready(function() {

    $("#ind_descripcion_empresa3").select2();
    $("#ind_dependencia3").select2();

    $(document).on('change','.cajaCheck1', function(){

        if (this.checked){
            $('select[id="ind_dependencia3"]').prop('disabled', false); {*este método para seleccionar los campos funciona correctamente con bootstra*}
            $('input[id="ind_dependencia3"]').focus();
        }else{
            $('select[id="ind_dependencia3"]').prop('disabled', true);

        }
    });
    {* ************************************************* *}
    $(document).on('change','.cajaCheck2', function(){

        if (this.checked){
            $('input[id="fraceNombre"]').prop('disabled', false);
            $('input[id="fraceNombre"]').focus();
        }else{
            $('input[id="fraceNombre"]').prop('disabled', true);
            $('input[id="fraceNombre"]').val('');

        }
    });
    {* BUSCAR EMPLEADOS PARA CARGARLOS EN LA TABLA DE AGREGAR (DATATABLE2)*************************************************** *}
    $("#buscarBeneficiariosDetalle").click(function(){

		var organismo_agregar = $("#ind_descripcion_empresa3").val();
	    var dependencia_agregar = 0;
        var caja = $('input[id="check_2Detalle"]');
		if(caja[0].checked){

            dependencia_agregar = $("#ind_dependencia3 :selected").val();
        }


        var url_agregar='{$_Parametros.url}modRH/gestion/viaticosCONTROL/FiltrarBeneficiariosMET';

        $.post(url_agregar,{ organismo: organismo_agregar, dependencia: dependencia_agregar},function(respuesta_post){


            var tabla_agregar = $('#datatable2').DataTable();{*permite usar las funciones de la libreria para la tabla*}
            tabla_agregar.clear().draw();{*limpia la tabla (tbody)*}
            var cad_nombre;
            var cad_apellido;

            for(var i=0; i<respuesta_post.length; i++){

                if(respuesta_post[i].ind_nombre2 == null){
                    cad_nombre = '';
                }else{
                    cad_nombre = ' '+respuesta_post[i].ind_nombre2;
                }

                if(respuesta_post[i].ind_apellido2 == null){
                    cad_apellido = '';
                }else{
                    cad_apellido = ' '+respuesta_post[i].ind_apellido2;
                }


                tabla_agregar.row.add([ {*añade las filas al tbody permitiendo aplicar la paginación automaticamente*}

                    respuesta_post[i].pk_num_empleado,
                    respuesta_post[i].ind_nombre1+cad_nombre+' '+respuesta_post[i].ind_apellido1+cad_apellido,
                    respuesta_post[i].ind_cedula_documento,
                    respuesta_post[i].fec_ingreso,
                    respuesta_post[i].ind_descripcion_cargo


                ]).draw()
                  .nodes(){*Agregar una clase a cada fila añadida*}
                  .to$()
                  .addClass( 'fila_agregar' );
            }

        }, "json");

	});

    {* TABLA DE BENEFICIARIOS PARA MODIFICAR DATATABLE 3************************************************************* *}
    var tabla_beneficiarios = $('#datatable3').DataTable();

    $('#datatable2 tbody').on('click','.fila_agregar', function(){

        var pk_num_empleado = this.cells[0].innerHTML;{*celda 0 que contiene el codigo de la fila*}


        $(this).addClass('fila_agregar_sobre');

        var num_filas = $('#datatable3 >tbody >tr').length;
        var filas = $('#datatable3 >tbody >tr'); {*trae las filas del tbody*}
        var band = 0;
        var aux1;
        var aux2;
        for(var i=0; i<num_filas; i++){
            aux1 = trim(filas[i].cells[0].innerHTML);
            aux2 = trim($(this)[0].cells[0].innerHTML);

            if(aux1 == aux2 ){

                swal({  {*Mensaje personalizado*}
                    title: '',
                    text: 'Ya está agregado',
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Aceptar',
                    closeOnConfirm: true
                });

                band = 1;
                break;
            }
        }

        if(band == 0){

            band = 0;
            tabla_beneficiarios.row.add([
                $(this)[0].cells[0].innerHTML, {*this representa el array (rows) *}
                $(this)[0].cells[1].innerHTML,
                '<input id="FechaSalidaCelda'+$(this)[0].cells[0].innerHTML+'" class="celda_editable" required=""  aria-required="true" aria-invalid="true"  type="text" onclick="$(this).datepicker({ format: \'dd/mm/yyyy\' });" onchange="calculaTotalDias('+$(this)[0].cells[0].innerHTML+')" value="'+$('#fec_salida').val()+'" name="FechaSalidaCelda'+$(this)[0].cells[0].innerHTML+'" style="text-align:left; background-color: transparent;border: medium none;width: 100%;font-weight: normal !important;" title="Editar"  >',
                '<input id="FechaRegresoCelda'+$(this)[0].cells[0].innerHTML+'" class="celda_editable" required="" aria-required="true" aria-invalid="true" type="text" onclick="$(this).datepicker({ format: \'dd/mm/yyyy\' })" onchange="calculaTotalDias('+$(this)[0].cells[0].innerHTML+')" value="'+$('#fec_regreso').val()+'" name="FechaRegresoCelda'+$(this)[0].cells[0].innerHTML+'" style="text-align:left; background-color: transparent;border: medium none;width: 100%;font-weight: normal !important;" title="Editar" onmouseover="" >',
                '<input id="TotalDiasCelda'+$(this)[0].cells[0].innerHTML+'" class="celda_editable" required="" aria-required="true" aria-invalid="true" type="text" value="'+$('#num_total_dias').val()+'" style="text-align:left; background-color: transparent;border: medium none;width: 100%;font-weight: normal !important;" name="TotalDiasCelda'+$(this)[0].cells[0].innerHTML+'" title="Editar" >',
                '<button type="button" class="borrarDetalle gsUsuario btn ink-reaction btn-raised btn-xs btn-primary"><i class="md md-delete"></i></button>'
            ]).draw(false);
        }



    });

    {* ************************************************ *}
    $('#datatable1 tbody').on( 'click', '.eliminar', function () {

        tabla_beneficiarios.row( $(this).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}

    });
    {* **************************************************** *}
    $("#cerrarModal2").click(function(){ {*capa que hace referencia al boton cerra de la modal 2*}
       $('#datatable2').dataTable( { {*instruccion para destruir una instancia de datatable*}
            "bDestroy": true
       } );

    });

});

{*funciones para eliminar espacios en blanco*}
function ltrim(str, filter) {
    var i = str.length;
    filter || ( filter = '' );
    for (var k = 0; k < i && filtering(str.charAt(k), filter); k++);
    return str.substring(k, i);
}
function rtrim(str, filter) {
    filter || ( filter = '' );
    for (var j = str.length - 1; j >= 0 && filtering(str.charAt(j), filter); j--);
    return str.substring(0, j + 1);
}
function trim(str, filter) {
    filter || ( filter = '' );
    return ltrim(rtrim(str, filter), filter);
}
function filtering(charToCheck, filter) {
    filter || ( filter = " \t\n\r\f" );
    return (filter.indexOf(charToCheck) != -1);
}

/*$('#example').dataTable( {
  "bDestroy": true
 } );*/
//$('table.display').DataTable(); //para inicializar varias tablas
//$(document.getElementById('datatable2')).append('<tr id="pk_num_almacen'+dato['pk_num_almacen']+'"><td>'+dato['pk_num_almacen']+'</td><td>'+dato['ind_descripcion_almacen']+'</td><td>'+dato['ind_ubicacion_almacen']+'</td><td>'+dato['fk_a003_num_persona']+'</td>';
//var num_tbody = $('#datatable2 >tbody >tr').length; {* cuenta las filas del tbody *}
//$("#datatable2 > tbody").empty(); {* elimina las filas del tbody *}
/*
 if($(this).attr('class') == "fila_agregar odd fila_agregar_sobre" || $(this).attr('class') == "fila_agregar even fila_agregar_sobre" || $(this).attr('class') == "odd fila_agregar fila_agregar_sobre" || $(this).attr('class') == "even fila_agregar fila_agregar_sobre"  )
 {
 $(this).removeClass("fila_agregar_sobre");

 tabla_beneficiarios.row( $(this).parents('tr') )
 .remove()
 .draw();
 }
 else
 {
 $(this).addClass('fila_agregar_sobre');

 tabla_beneficiarios.row.add([
 $(this)[0].cells[0].innerHTML, {*this representa el array (rows) *}
 $(this)[0].cells[1].innerHTML,
 $(this)[0].cells[2].innerHTML,
 $(this)[0].cells[3].innerHTML,
 $(this)[0].cells[4].innerHTML,
 '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Descartar" descipcion="Beneficiario descartado" titulo=" mensaje="¿Desea descartar al beneficiario?" title="Descartar"><i class="md md-delete"></i></button>'

 ]).draw(false);
 }

 var campos_datatable = $('input[class="celda_editable"]');

 for(var i=0; i<campos_datatable.length; i++){
 alert(campos_datatable[i].value);
 campos_datatable[i].value = "";
 }

*/
</script>



