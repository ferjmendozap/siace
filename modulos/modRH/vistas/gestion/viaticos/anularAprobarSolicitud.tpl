<div class="modal-body">

    <div class="row">
        <div class="col-lg-12">
            {* ********************************************* *}
            <div class="col-md-12">

                        <form id="formAjax" class="form form-validation" novalidate="novalidate" role="form" >
                            <input type="hidden" name="pk_viatico_anular" id="pk_viatico_anular" class="form-control" value="{$datosViatico['pk_num_viatico']}">
                            <input type="hidden" name="band_form" id="band_form" class="form-control" value="{$band_form}">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" disabled="disabled" id="cod_interno_viatico" value="{$datosViatico['cod_interno']}" >
                                        <label for="cod_interno_viatico">Número</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="UsuarioUltimaMod" disabled="disabled" value="{$datosUltimaModificacion['ind_usuario']} - {$datosViatico['fec_ultima_modificacion']}">
                                        <label for="UsuarioUltimaMod">Ultima modificación</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {foreach item=indice from=$datosOrganismos}
                                            {if $indice.pk_num_organismo == $datosViatico['fk_a001_num_organismo']}
                                                <input type="text" class="form-control input-sm" id="ind_descripcion_empresa" disabled="disabled" value="{$indice.ind_descripcion_empresa}">
                                                <label for="ind_descripcion_empresa">Organismo</label>
                                                {break}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="usuarioActual" value="{$preparadoPor['ind_nombre1']} {$preparadoPor['ind_apellido1']}">
                                        <label for="usuarioActual">Preparado</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {foreach item=indice from=$datosDependencias}
                                            {if $indice.pk_num_dependencia == trim($datosViatico['fk_a004_num_dependencia'])}
                                                <input type="text" class="form-control input-sm" id="ind_dependencia" disabled="disabled" value="{$indice.ind_dependencia}">
                                                <label for="ind_dependencia">Dependencia</label>
                                                {break}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        {foreach item=indice from=$datosEmpleados}
                                            {if $indice.pk_num_empleado == trim($datosViatico['fk_rhb001_num_empleado_solicita'])}
                                                <input type="text" class="form-control input-sm" id="datosEmpleados" disabled="disabled" value="{$indice.pk_num_empleado} - {$indice.ind_nombre1}{if isset($indice.ind_nombre2) } {$indice.ind_nombre2} {/if} {$indice.ind_apellido1} {if isset($indice.ind_apellido2) } {$indice.ind_apellido2} {/if}">
                                                <label for="datosEmpleados">Solicitante</label>
                                                {break}
                                            {/if}
                                        {/foreach}
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="fec_salida" disabled="disabled" value="{trim($datosViatico['fec_salida'])}">
                                        <label for="fec_salida">Fecha de Salida</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="fec_regreso" disabled="disabled" value="{$datosViatico['fec_ingreso']}">
                                        <label for="fec_regreso">Fecha de Regreso</label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" id="num_total_dias" disabled="disabled" value="{$datosViatico['num_total_dias']}">
                                        <label for="num_total_dias">Total de Días</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <textarea type="text" rows="3" class="form-control input-sm" id="ind_destino" disabled="disabled" >{$datosViatico['ind_destino']}</textarea>
                                        <label for="ind_destino">Destino</label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <textarea type="text" rows="3" class="form-control input-sm" id="ind_motivo" disabled="disabled" >{$datosViatico['ind_motivo']}</textarea>
                                        <label for="ind_motivo">Motivo</label>
                                    </div>
                                </div>
                                {if $band_form == 'a'}{* modalidad anular *}
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <textarea type="text" rows="2" class="form-control input-sm" id="ind_razon_rechazo" name="ind_razon_rechazo" ></textarea>
                                            <label for="ind_razon_rechazo">Razon Rechazo:</label>
                                        </div>
                                    </div>
                                {elseif $band_form == 'p'}
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <textarea type="text" rows="2" class="form-control input-sm" id="ind_observacion" name="ind_observacion"></textarea>
                                            <label for="ind_observacion">Observación:</label>
                                        </div>
                                    </div>
                                    {if isset($unidadCombustible)}
                                        {if $unidadCombustible==1}
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <input type="text" name="num_unidad_combustible" id="num_unidad_combustible" value="{$datosViatico['num_unidad_combustible']}" class="form-control">
                                                    <label for="num_unidad_combustible" class="control-label">Unidades de Combustible</label>
                                                </div>
                                            </div>
                                        {/if}
                                    {/if}
                                {elseif $band_form == 'v' && $datosViatico['ind_estado'] == 'AN' }{* modalidad ver viatico anulado *}

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <textarea type="text" rows="2" class="form-control input-sm" id="ind_razon_rechazo" name="ind_razon_rechazo" disabled="disabled" style="text-transform: uppercase" >{$datosViatico['ind_razon_rechazo']}</textarea>
                                            <label for="ind_razon_rechazo">Razon Rechazo:</label>
                                        </div>
                                    </div>
                                {elseif $band_form == 'v' && $datosViatico['ind_estado'] == 'AP'}{* modalidad ver viatico aprobado *}
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <textarea type="text" rows="2" class="form-control input-sm" id="ind_observacion" name="ind_observacion" disabled="disabled">{$datosViatico['ind_observacion']}</textarea>
                                            <label for="ind_observacion">Observación:</label>
                                        </div>
                                    </div>
                                    {if isset($unidadCombustible)}
                                        {if $unidadCombustible==1}
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <input type="text" name="num_unidad_combustible" id="num_unidad_combustible" value="{$datosViatico['num_unidad_combustible']}" class="form-control">
                                                    <label for="num_unidad_combustible" class="control-label">Unidades de Combustible</label>
                                                </div>
                                            </div>
                                        {/if}
                                    {/if}
                                {/if}

                            </div>
                            <div class="card card-underline">
                                <div class="card-head-row">&nbsp;<i class="fa fa-fw fa-tag"></i>Beneficiarios</div>
                                <div class="card-body">
                                    <table id="datatable2" class="table table-striped table-hover" WIDTH="90%">
                                        <thead>
                                        <tr  align="center">

                                            <th width="10%"><i class="md md-dialpad"></i>Código</th>
                                            <th width="30%"><i class="md-person"></i>Nombre y Apellidos</th>
                                            <th width="10%"><i class="md-today"></i>Fecha Inicio</th>
                                            <th width="10%"><i class="md-today"></i>Fecha Fin</th>
                                            <th width="10%"><i class="md-av-timer"></i>Total Dias</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        {foreach item=indice from=$datosDetallesViaticos}
                                            <tr class="" >

                                                <td>{$indice.fk_rhb001_num_empleado_beneficiario} </td>
                                                <td>{$indice.ind_nombre1} {$indice.ind_nombre2} {$indice.ind_apellido1} {$indice.ind_apellido2} </td>
                                                <td>{$indice.fec_salida_detalle}</td>
                                                <td>{$indice.fec_ingreso_detalle}</td>
                                                <td>{$indice.num_total_dias_detalle}</td>


                                            </tr>
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            {if $band_form == 'a'}
                                <div class="card-actionbar">
                                    <div class="card-actionbar-row">
                                        <button type="submit" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" tittlemensg1 = "Estas Seguro?" textmensg1 = "Estas seguro que deseas anular el viatico" confirmensg1="si, Anular" mensgresp1="Anulado!" mensgresp2="Viatico anulado" id="botoEnviarForm">ANULAR</button>
                                    </div>
                                </div>
                            {elseif $band_form == 'p' }
                                <div class="card-actionbar">
                                    <div class="card-actionbar-row">
                                        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" tittlemensg1 = "Estas Seguro?" textmensg1 = "Estas seguro que deseas aprobar el viatico" confirmensg1="si, Aprobar" mensgresp1="Aprobado!" mensgresp2="Viatico aprobado" id="botoEnviarForm" >APROBAR</button>
                                    </div>
                                </div>
                            {/if}
                        </form>



            </div><!--end .col -->
        </div><!--end .row -->
        {* ********************************************* *}
    </div>
</div>

</div>




<script>
$(document).ready(function() {

    $("#formAjax").validate({
        submitHandler: function(form) {
            swal({
                title:$("#botoEnviarForm").attr('tittlemensg1'),
                text: $("#botoEnviarForm").attr('textmensg1'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $("#botoEnviarForm").attr('confirmensg1'),
                closeOnConfirm: false
            }, function(){

                var url_viatico;
                var band_form = $("#band_form").val();

                if( band_form == 'a') {
                    url_viatico = '{$_Parametros.url}modRH/gestion/viaticosCONTROL/RegistrarAnulacionMET';
                }else{
                    url_viatico = '{$_Parametros.url}modRH/gestion/viaticosCONTROL/RegistrarAprobacionMET';
                }

                $.post(url_viatico,$(form).serialize(),function(respuesta_post){

                    if(respuesta_post == 1){

                        swal($("#botoEnviarForm").attr("mensgresp1"), $("#botoEnviarForm").attr("mensgresp2"), "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                        //***************************************

                        var viatico = "{trim($datosViatico['cod_interno'])}";
                        var tabla_listado_anular = $('#datatable1').DataTable();
                        var num_filas = $('#datatable1 >tbody >tr').length;
                        var filas = $('#datatable1 >tbody >tr');
                        for(var i=0; i<num_filas; i++){

                            if(trim(filas[i].cells[1].innerHTML) == viatico ){
                                tabla_listado_anular.row( $(filas[i]) ).remove().draw();
                                break;

                            }
                        }

                        //***************************************

                    }else{

                        alert(respuesta_post);
                    }

                }, 'json');


            });//function swal



        }//submi
    });//validate




});//ready
{*funciones para eliminar espacios en blanco*}
function ltrim(str, filter) {
    var i = str.length;
    filter || ( filter = '' );
    for (var k = 0; k < i && filtering(str.charAt(k), filter); k++);
    return str.substring(k, i);
}
function rtrim(str, filter) {
    filter || ( filter = '' );
    for (var j = str.length - 1; j >= 0 && filtering(str.charAt(j), filter); j--);
    return str.substring(0, j + 1);
}
function trim(str, filter) {
    filter || ( filter = '' );
    return ltrim(rtrim(str, filter), filter);
}
function filtering(charToCheck, filter) {
    filter || ( filter = " \t\n\r\f" );
    return (filter.indexOf(charToCheck) != -1);
}
</script>

