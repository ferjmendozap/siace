<section class="style-default-light">&nbsp;
    <div class="row"> {*define una fila de la grilla (numero 1)*}
        <div class="col-lg-12">{*identifica el espacio de colunnas que acupara el div en la fila*}
            <div class="card">{*define un panel tipo tarjeta*}
                <div class="card-head">
                    <header class="text-primary text-xl">Aprobar Solicitudes de Viaticos</header>
                </div>
                <div class="card-body ">{*cuerpo del panel tipo tarjeta*}
                    <form id="formAjax2" class="form form-validation" role="form"   >{*se define un form que contendra los campos de formulario, necesario para que las clases form-group floating-label en los campos funcionen correctamente*}{*la clase form-validate necesaria para que las validaciones se muestren el los campos requeridos *}
                        <div class="col-lg-6">{*espacio que ocupara el campo en la fila*}
                            <div class="form-group"   >{*clase para el correcto estilo y efecto label flotante del campo, cada campo debe tener esta clase*}
                                <div class="input-group">{*clase que define a un grupo de campos dentro de un form*}
                                    <div class="input-group-addon">{*clase para el correcto posicionamiento del check en el grupo de campos*}
                                        <div class="checkbox checkbox-inline checkbox-styled">{*clase para el estylo y efectos del checked*}
                                            <label>
                                                <input disabled checked id="check_1" type="checkbox">{*checked para que el campo este chekado*}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group-content">
                                        <select id="ind_descripcion_empresa" disabled="disabled" class="form-control input-sm" name="ind_descripcion_empresa" >
                                            {foreach item=indice from=$datosOrganismos}
                                                <option value="{$indice.pk_num_organismo}" selected="selected">{$indice.ind_descripcion_empresa}</option>
                                            {/foreach}
                                        </select>
                                        <label for="ind_descripcion_empresa">Organismo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">{*espacio que ocupara el campo en la fila*}
                            <div class="form-group"   >{*clase para el correcto estilo y efecto label flotante del campo, cada campo debe tener esta clase*}
                                <div class="input-group">{*clase que define a un grupo de campos dentro de un form*}
                                    <div class="input-group-addon">{*clase para el correcto posicionamiento del check en el grupo de campos*}
                                        <div class="checkbox checkbox-inline checkbox-styled">{*clase para el estylo y efectos del checked*}
                                            <label>
                                                <input class="cajaCheck1" id="check_2" type="checkbox">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group-content">
                                        <select id="ind_dependencia2"  class="form-control input-sm selectpicker" name="form[int][ind_dependencia2]" >{*input-sm para el tamaño de la fuente*}
                                            {foreach item=indice from=$datosDependencias}
                                                <option value="{$indice.pk_num_dependencia }" >{$indice.ind_dependencia}</option>
                                            {/foreach}
                                        </select>
                                        <label for="ind_dependencia2">Dependencia</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group"   >
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <div class="checkbox checkbox-inline checkbox-styled">
                                            <label>
                                                <input disabled="disabled" class="cajaCheck2" id="check_3" type="checkbox" checked>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group-content">
                                        <select disabled="disabled" id="ind_estado" class="form-control input-sm selectpicker" name="ind_estado" >
                                            <option value="PR" selected="selected" >En preparación</option>
                                        </select>
                                        <label for="ind_estado">Estado</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group"   >
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <div class="checkbox checkbox-inline checkbox-styled">
                                            <label>
                                                <input class="cajaCheck3" id="check_4" type="checkbox">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group-content">
                                        <div id="demo-date-range" class="input-daterange input-group">
                                            <div class="input-group-content">
                                                <input disabled="disabled" id="start_fecha_prepa" class="form-control" type="text" name="start_fecha_prepa" aria-required="true" aria-invalid="true" required="" >
                                                <label>Fecha de preparación:</label>
                                            </div>
                                            <span class="input-group-addon">a</span>
                                            <div class="input-group-content">
                                                <input disabled="disabled" id="end_fecha_prepa" class="form-control" type="text" name="end_fecha_prepa"  aria-required="true" aria-invalid="true" required="" >
                                                <div class="form-control-line"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                       <div class="row">
        <div class="col-lg-12">
            <div align="center">
			    <button type="submit" id="botonRegistrar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" >Buscar</button>
            </div>
        </div>
    </div>
    
                    </form>
					<br />
                </div><!--end .card-body -->
                <br>
            </div><!--end .card -->
        </div><!--end .col -->
    </div>{*fin de la fila 1*}
   <br>
    <div class="card">
    <div class="card-body ">
    <div class="row" >{*fila numero 2*}   
    <div class="col-lg-12">
            <table id="datatable1" class="table table-striped table-hover">
                <thead>
                <tr  align="center">
                    <th ><i class=""></i>Dependencia</th>
					<th >#Solicitud</th>
					<th ><i class="md-person"></i>Solicitante</th>
                    <th ><i class="md-today"></i>Fec. Registro</th>
                    <th ><i class="md-today"></i>Fec. Salida</th>
					<th ><i class="md-today"></i>Fec. Regreso</th>
					<th ><i class=""></i>Motivo</th>
                    <th ><i ></i>Aprobar</th>
					<th ><i ></i>Anular</th>
					<th ><i ></i>Ver</th>
					<th ><i ></i>Imprimir</th>
	            </tr>
                </thead>
                <tbody >
				{foreach item=indice from=$listaViaticos}
                <tr class="fila_agregar" >
                    <td>{$indice.ind_dependencia}</td>
                    <td>{$indice.cod_interno} </td>
					<td>{$indice.ind_nombre1} {$indice.ind_nombre2} {$indice.ind_apellido1} {$indice.ind_apellido2} </td>
                    <td>{$indice.fec_registro_viatico}</td>
                    <td>{$indice.fec_salida}</td>
                 	<td>{$indice.fec_ingreso} </td>
					<td>{$indice.ind_motivo}</td>
					</td>
                    <td><button type="button"  pk_num_viatico = "{$indice.pk_num_viatico}" class="aprobar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" title="Aprobar Víatico"><i class="md md-check"></i></button>
					<td><button type="button"  pk_num_viatico = "{$indice.pk_num_viatico}" class="anular gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" title="Anular Víatico"><i class="md md-not-interested"></i></button>
					</td>
					<td><button type="button"  pk_num_viatico = "{$indice.pk_num_viatico}" class="ver gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" title="Ver Víatico"><i class="md md-remove-red-eye"></i></button>
					</td>
					<td><button type="button"  pk_num_viatico = "{$indice.pk_num_viatico}" class="imprimir gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" title="Imprimir Víatico"><i class="md md-print"></i></button>
					</td>
				</tr>
                {/foreach}
			    </tbody>
            </table>
        </div>
    </div>{* fin de fila row *}
    </div>
    </div>
</section>
<style>

</style>
<script>
$('#demo-date-range').datepicker({ {*Para el correcto funcionamiento deben ir antes del ready*}
	 format: 'dd/mm/yyyy' 
});
//$('table.display').DataTable();
$(document).ready(function() {

    $("#ind_descripcion_empresa").select2();
    $("#ind_dependencia2").select2();

    $(document).on('change','.cajaCheck1', function(){

        if (this.checked){
            $('select[id="ind_dependencia2"]').prop('disabled', false); {*este método para seleccionar los campos funciona correctamente con bootstra*}
            $('input[id="ind_dependencia2"]').focus();
        }else{
            $('select[id="ind_dependencia2"]').prop('disabled', true);

        }
    });



    $(document).on('change','.cajaCheck3', function(){

        if (this.checked){
            $('input[id="start_fecha_prepa"]').prop('disabled', false);
            $('input[id="start_fecha_prepa"]').val('');
            $('input[id="end_fecha_prepa"]').prop('disabled', false);
            $('input[id="end_fecha_prepa"]').val('');
            $('input[id="start_fecha_prepa"]').focus();
        }else{
            $('input[id="start_fecha_prepa"]').prop('disabled', true);
            $('input[id="start_fecha_prepa"]').val('');
            $('input[id="end_fecha_prepa"]').prop('disabled', true);
            $('input[id="end_fecha_prepa"]').val('');


        }
    });

	$('#datatable1 tbody').on('click','.editar', function(){

            var pk_num_viatico = $(this).attr('pk_num_viatico');

                var urlEditar = '{$_Parametros.url}modRH/gestion/viaticosCONTROL/EditarViaticoMET';
                $('#modalAncho').css("width", "75%");
                $('#formModalLabel').html("Modificar Solicitud de Víatico");
                $('#ContenidoModal').html("");
                $.post(urlEditar, { pk_num_viatico: pk_num_viatico }, function(dato){
                    $('#ContenidoModal').html(dato);
            });

	});
	
	$('#datatable1 tbody').on('click','.anular', function(){

        var pk_num_viatico = $(this).attr('pk_num_viatico');

        var urlAnular = '{$_Parametros.url}modRH/gestion/viaticosCONTROL/AnularVerAprobarViaticoMET/';
        $('#modalAncho').css("width", "75%");
        $('#formModalLabel').html("Anular Solicitudes de Víatico");
        $('#ContenidoModal').html("");
        $.post(urlAnular, { pk_num_viatico: pk_num_viatico, band_form:'a' }, function(dato){
            $('#ContenidoModal').html(dato);
        });
						 
	});
	
	$('#datatable1 tbody').on('click','.ver', function(){

        var pk_num_viatico = $(this).attr('pk_num_viatico');

        var urlVer = '{$_Parametros.url}modRH/gestion/viaticosCONTROL/AnularVerAprobarViaticoMET/';
        $('#modalAncho').css("width", "75%");
        $('#formModalLabel').html("Ver Solcitud de Víatico");
        $('#ContenidoModal').html("");
        $.post(urlVer, { pk_num_viatico: pk_num_viatico, band_form:'v' }, function(dato){
            $('#ContenidoModal').html(dato);
        });
						 
	});

    $('#datatable1 tbody').on('click','.aprobar', function(){

        var pk_num_viatico = $(this).attr('pk_num_viatico');

        var urlAprobar = '{$_Parametros.url}modRH/gestion/viaticosCONTROL/AnularVerAprobarViaticoMET/';
        $('#modalAncho').css("width", "75%");
        $('#formModalLabel').html("Aprobar Solicitudes de Víatico");
        $('#ContenidoModal').html("");
        $.post(urlAprobar, { pk_num_viatico: pk_num_viatico, band_form:'p' }, function(dato){
            $('#ContenidoModal').html(dato);
        });

    });

    $('#datatable1 tbody').on('click','.imprimir', function(){

        var pk_num_viatico = $(this).attr('pk_num_viatico');
        var urlImprimirPdf = '{$_Parametros.url}modRH/gestion/viaticosCONTROL/ReporteViaticoMET?pk_num_viatico='+pk_num_viatico;

        $('#modalAncho').css( 'width', '80%' );
        $('#formModalLabel').html("Imprimir viatico");
        $('#ContenidoModal').html("");
        $('#ContenidoModal').html("<iframe src='"+urlImprimirPdf+"' border='1' width='100%' height='600px'></iframe>");
						 
	});


    var tabla_listado = $('#datatable1').DataTable();
    $("#formAjax2").validate({
                submitHandler: function(form) {
				
				var organismo_viaticos = $("#ind_descripcion_empresa").val();
	    		var dependencia_viaticos = '';
        		var caja = $('input[id="check_2"]');
				if(caja[0].checked){
					dependencia_viaticos = $("#ind_dependencia2 :selected").val();
				}
                else{
                    dependencia_viaticos = $("#ind_dependencia2").val();
                }

				
				caja = $('input[id="check_4"]');
				var fecha_start_viaticos = '';
				var fecha_end_viaticos = '';
				var band_rango = 0;
				if(caja[0].checked){
					fecha_start_viaticos = $("#start_fecha_prepa").val();
					fecha_end_viaticos = $("#end_fecha_prepa").val();
					}
				
				 var url_listar='{$_Parametros.url}modRH/gestion/viaticosCONTROL/FiltrarListadoViaticosMET';
					$.post(url_listar,{ organismo: organismo_viaticos, dependencia: dependencia_viaticos, estado: 'PR', fec_start: fecha_start_viaticos, fec_end: fecha_end_viaticos },function(respuesta_post){
			
			     {* **************************************************** *}
				 

						tabla_listado.clear().draw();
				 		
						if(respuesta_post != -1){
							var cad_nombre;
							var cad_apellido;
							var cad_editar;
                            var cad_anulado;
							for(var i=0; i<respuesta_post.length; i++){
				
									if(respuesta_post[i].ind_nombre2 == null){
										cad_nombre = '';
									}else{
										cad_nombre = ' '+respuesta_post[i].ind_nombre2;
									}
					
									if(respuesta_post[i].ind_apellido2 == null){
										cad_apellido = '';
									}else{
										cad_apellido = ' '+respuesta_post[i].ind_apellido2;
									}
							

                                    //******************************
                                    tabla_listado.row.add([
									respuesta_post[i].ind_dependencia,
									respuesta_post[i].cod_interno,
									respuesta_post[i].ind_nombre1+cad_nombre+' '+respuesta_post[i].ind_apellido1+cad_apellido,
									respuesta_post[i].fec_registro_viatico,
									respuesta_post[i].fec_salida,
									respuesta_post[i].fec_ingreso,
									respuesta_post[i].ind_motivo,
                                    '<button type="button" pk_num_viatico = "'+respuesta_post[i].pk_num_viatico+'" class="aprobar gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" ><i class="md md-check"></i></button>',
                                    '<button type="button" pk_num_viatico = "'+respuesta_post[i].pk_num_viatico+'" class="anular gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" ><i class="md md-not-interested"></i></button>',
									'<button type="button" pk_num_viatico = "'+respuesta_post[i].pk_num_viatico+'" class="ver gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" ><i class="md md-remove-red-eye"></i></button>',
									'<button type="button" pk_num_viatico = "'+respuesta_post[i].pk_num_viatico+'" class="imprimir gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-target="#formModal" data-toggle="modal" ><i class="md md-print"></i></button>'
									]).draw()
									.nodes()
									.to$()
									.addClass( '' );	
							
							}
					
						}
						
						
						 
				 		
						
						
				 {* **************************************************** *}
				
				},'json');
		}	
				
	});

 });
//$('table.display').DataTable(); //para inicializar varias tablas
//$(document.getElementById('datatable2')).append('<tr id="pk_num_almacen'+dato['pk_num_almacen']+'"><td>'+dato['pk_num_almacen']+'</td><td>'+dato['ind_descripcion_almacen']+'</td><td>'+dato['ind_ubicacion_almacen']+'</td><td>'+dato['fk_a003_num_persona']+'</td>';
//var num_tbody = $('#datatable2 >tbody >tr').length; {* cuenta las filas del tbody *}
//$("#datatable2 > tbody").empty(); {* elimina las filas del tbody *}
/*
 if($(this).attr('class') == "fila_agregar odd fila_agregar_sobre" || $(this).attr('class') == "fila_agregar even fila_agregar_sobre" || $(this).attr('class') == "odd fila_agregar fila_agregar_sobre" || $(this).attr('class') == "even fila_agregar fila_agregar_sobre"  )
 {
 $(this).removeClass("fila_agregar_sobre");

 tabla_beneficiarios.row( $(this).parents('tr') )
 .remove()
 .draw();
 }
 else
 {
 $(this).addClass('fila_agregar_sobre');

 tabla_beneficiarios.row.add([
 $(this)[0].cells[0].innerHTML, {*this representa el array (rows) *}
 $(this)[0].cells[1].innerHTML,
 $(this)[0].cells[2].innerHTML,
 $(this)[0].cells[3].innerHTML,
 $(this)[0].cells[4].innerHTML,
 '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="si, Descartar" descipcion="Beneficiario descartado" titulo=" mensaje="¿Desea descartar al beneficiario?" title="Descartar"><i class="md md-delete"></i></button>'

 ]).draw(false);
 }

 var campos_datatable = $('input[class="celda_editable"]');

 for(var i=0; i<campos_datatable.length; i++){
 alert(campos_datatable[i].value);
 campos_datatable[i].value = "";
 }

*/
</script>



