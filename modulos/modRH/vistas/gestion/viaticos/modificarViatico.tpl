<div class="modal-body">
        <span class="text-primary text-lg">{$titulo_form}</span>
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">

                        <div id="rootwizard2" class="form-wizard form-wizard-horizontal">
                            <form id="formAjax" class="form floating-label form-validation" novalidate="novalidate" role="form" >
                                <input type="hidden" name="id_beneficiariosDetalle" class="form-control" id="id_beneficiariosDetalle" value="">
                                <input type="hidden" name="fechas_salidas_celdasDetalle" id="fechas_salidas_celdasDetalle" class="form-control" value="">
                                <input type="hidden" name="fechas_regresos_celdasDetalle" id="fechas_regresos_celdasDetalle" class="form-control" value="">
                                <input type="hidden" name="totales_dias_celdasDetalle" id="totales_dias_celdasDetalle" class="form-control" value="">
                                <input type="hidden" name="pk_viatico" id="pk_viatico" class="form-control" value="{$datosViatico['pk_num_viatico']}">
                                <!-- Navegacion form-wizard-nav ************************************************************************************  -->
                                <div class="form-wizard-nav">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-primary">
                                        </div>
                                    </div>
                                    <ul class="nav nav-justified">
                                        <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">Información General</span></a></li>
                                        <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">Beneficiarios</span></a></li>
                                    </ul>
                                </div>
                                <!-- ***************************************************************************************************************** -->

                                <!-- Contenido de los tab -->
                                <div class="tab-content clearfix">
                                    <!-- Tab numero 1 ************************************************************************************************************ -->
                                    <div class="tab-pane active" id="tab1">
                                        <br/><br/>
                                        <div class="col-lg-6">
                                            <div class="form-group floating-label"   >

                                                <input  type="text" name="cod_interno_viatico" required="" id="cod_interno_viatico" class="form-control  input-sm " value="{$datosViatico['cod_interno']}" disabled="disabled">
                                                <label for="cod_interno_viatico" class="control-label">Número</label>

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label"   >

                                                <input type="text" disabled="disabled" name="UsuarioUltimaMod" id="UsuarioUltimaMod" class="form-control input-sm " value="{$datosUltimaModificacion['ind_usuario']} - {$datosViatico['fec_ultima_modificacion']}">
                                                <label for="ind_motivo" class="control-label">Ultima modificación</label>

                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group floating-label"   >

                                                <select id="ind_descripcion_empresa" class="form-control input-sm " name="ind_descripcion_empresa" required="" disabled="disabled"  >
                                                    {foreach item=indice from=$datosOrganismos}
                                                        <option value="{$indice.pk_num_organismo}" selected="selected">{$indice.ind_descripcion_empresa}</option>
                                                    {/foreach}
                                                </select>
                                                <label for="ind_descripcion_empresa" class="control-label">Organismo</label>

                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group floating-label"   >

                                                <input type="text" name="usuarioActual" required="" id="usuarioActual" class="form-control input-sm " value="{$preparadoPor['ind_nombre1']} {$preparadoPor['ind_apellido1']}" disabled="disabled">
                                                <label for="usuarioActual" class="control-label">Preparado</label>

                                            </div>
                                            <input type="hidden" id="idUsuario" name="form[int][idUsuario]" value="{$idUsuario}" />
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label"   >

                                                <select id="ind_dependencia" required="" class="form-control input-sm " name="ind_dependencia" disabled="disabled" >
                                                    {foreach item=indice from=$datosDependencias}
                                                        <option value="{$indice.pk_num_dependencia }" selected="selected">{$indice.ind_dependencia}</option>
                                                    {/foreach}
                                                </select>
                                                <label for="ind_dependencia" class="control-label ">Dependencias</label>

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label"   >

                                                <select id="datosEmpleados" required="" class="form-control input-sm  "  name="datosEmpleados" disabled="disabled" >
                                                    {foreach item=indice from=$datosEmpleados}
                                                        <option value="{$indice.pk_num_empleado }" selected="selected">{$indice.pk_num_empleado} - {$indice.ind_nombre1}{if isset($indice.ind_nombre2) } {$indice.ind_nombre2} {/if} {$indice.ind_apellido1} {if isset($indice.ind_apellido2) } {$indice.ind_apellido2} {/if}  </option>
                                                    {/foreach}
                                                </select>
                                                <label for="datosEmpleados" class="control-label">Solicitante</label>

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label"   >

                                                <input type="text" name="fec_salida" id="fec_salida" class="form-control input-sm " required="" aria-required="true" aria-invalid="true" >
                                                <label for="fec_salida" class="control-label">Fecha de Salida</label>
                                                <p class="help-block">DD/MM/YYYY</p>

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label"   >

                                                <input type="text" name="fec_regreso" id="fec_regreso" class="form-control input-sm " required="" aria-required="true" aria-invalid="true" >
                                                <label for="fec_regreso" class="control-label">Fecha de Regreso</label>
                                                <p class="help-block">DD/MM/YYYY</p>

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label"   >

                                                <input type="text" name="num_total_dias" class="form-control input-sm " value="0"  id="num_total_dias" name="num_total_dias"  data-rule-min="1" data-rule-max="365" required="">
                                                <label for="num_total_dias" class="control-label">Total de Días</label>

                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group floating-label"   >

                                                <textarea type="text" rows="1" name="ind_destino" id="ind_destino" class="form-control input-sm  " required="" style="text-transform: uppercase" >{$datosViatico['ind_destino']}</textarea>
                                                <label for="ind_destino" class="control-label">Destino</label>

                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group floating-label"   >

                                                <textarea type="text" rows="1"  name="ind_motivo" id="ind_motivo" class="form-control input-sm  " required="" style="text-transform: uppercase"  >{$datosViatico['ind_motivo']}</textarea>
                                                <label for="ind_motivo" class="control-label">Motivo</label>

                                            </div>
                                        </div>
                                        {if $datosViatico['ind_estado'] == 'AN'}
                                            <div class="col-sm-12">
                                                <div class="form-group floating-label"   >

                                                    <input type="text"   name="ind_razon_rechazo" id="ind_razon_rechazo" class="form-control input-sm  " value="{$datosViatico['ind_razon_rechazo']}" style="text-transform: uppercase" disabled="disabled" >
                                                    <label for="ind_razon_rechazo" class="control-label">Razon Rechazo</label>

                                                </div>
                                            </div>
                                        {else}
                                            <input type="hidden" name="ind_razon_rechazo" id="ind_razon_rechazo" value="">
                                        {/if}

                                    </div>
                                    <!-- Tab numero 2 ******************************************************************************************************************************* -->
                                    <br/><br/><br/>
                                    <div class="tab-pane" id="tab2">
                                        <div class="col-md-12">
                                            <div class="card-actionbar">
                                                <div class="card-actionbar-row">
                                                    <button type="button"  class="logsUsuario btn ink-reaction btn-raised btn-info" id="agregarBeneficiarioDetalle"
                                                            descripcion="Agregar beneficiario" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#formModal2"
                                                            titulo="Agregar beneficiario">
                                                        <span class="md-queue "></span>
                                                        Agregar
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <br/><br/><br/>
                                        <table id="datatable3" class="table table-striped table-hover" WIDTH="100%">
                                            <thead>
                                            <tr  align="center">

                                                <th class="col-sm-1" ><i class="md md-dialpad"></i>Código</th>
                                                <th class="col-sm-7" ><i class="md-person"></i>Nombre y Apellidos</th>
                                                <th class="col-sm-1" ><i class="md-today"></i>Fecha Inicio</th>
                                                <th class="col-sm-1" ><i class="md-today"></i>Fecha Fin</th>
                                                <th class="col-sm-1" ><i class="md-av-timer"></i>Total Dias</th>
                                                <th class="col-sm-1" ><i class=""></i>Borrar</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {foreach item=indice from=$datosDetallesViaticos}
                                                <tr class="" >

                                                    <td>{$indice.fk_rhb001_num_empleado_beneficiario} </td>
                                                    <td>{$indice.ind_nombre1} {$indice.ind_nombre2} {$indice.ind_apellido1} {$indice.ind_apellido2} </td>
                                                    <td><input id="FechaSalidaCelda{$indice.fk_rhb001_num_empleado_beneficiario}" class="celda_editable" required=""  aria-required="true" aria-invalid="true"  type="text" onclick="$(this).datepicker({ format: 'dd/mm/yyyy' });" onchange="calculaTotalDias('{$indice.fk_rhb001_num_empleado_beneficiario}')" value="{$indice.fec_salida_detalle}" name="FechaSalidaCelda{$indice.fk_rhb001_num_empleado_beneficiario}" style="text-align:left; background-color: transparent;border: medium none;width: 100%;font-weight: normal !important;" title="Editar"  ></td>
                                                    <td><input id="FechaRegresoCelda{$indice.fk_rhb001_num_empleado_beneficiario}" class="celda_editable" required="" aria-required="true" aria-invalid="true"  type="text" onclick="$(this).datepicker({ format: 'dd/mm/yyyy' });" onchange="calculaTotalDias('{$indice.fk_rhb001_num_empleado_beneficiario}')" value="{$indice.fec_ingreso_detalle}" name="FechaRegresoCelda{$indice.fk_rhb001_num_empleado_beneficiario}" style="text-align:left; background-color: transparent;border: medium none;width: 100%;font-weight: normal !important;" title="Editar"  ></td>
                                                    <td><input id="TotalDiasCelda{$indice.fk_rhb001_num_empleado_beneficiario}" class="celda_editable" required="" aria-required="true" aria-invalid="true" type="text" value="{$indice.num_total_dias_detalle}" style="text-align:left; background-color: transparent;border: medium none;width: 100%;font-weight: normal !important;" name="TotalDiasCelda{$indice.fk_rhb001_num_empleado_beneficiario}" title="Editar" ></td>
                                                    <td><button type="button" class="borrarDetalle gsUsuario btn ink-reaction btn-raised btn-xs btn-primary" ><i class="md-delete"></i></button></td>

                                                </tr>
                                            {/foreach}
                                            </tbody>
                                        </table>
                                        <br>
                                        <div align="center">
                                            <button type="submit" id="botonGuardarModificacion" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button">Guardar</button>
                                        </div>
                                    </div>

                                    <!--******************************************************************************************** -->

                                </div><!--end .tab-content -->

                                <ul class="pager wizard">
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Previo</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                </ul>
                            </form>
                        </div><!--end #rootwizard -->


            </div><!--end .col -->
        </div><!--end .row -->

        <!-- END FORM WIZARD -->



</div>




<style>
    .celda_editable:hover{
        background-color: #afd9ee  !important;

    }
</style>
<script>

    //*****************************************

    $('#fec_salida').datepicker({ format: 'dd/mm/yyyy' });
    $('#fec_regreso').datepicker({ format: 'dd/mm/yyyy' });
    $('#fec_regreso').change(function(){
        var f1 = $('#fec_salida').val();
        var f2 = $('#fec_regreso').val();
        var dias;
        if(f1 !=''){
            var aFecha1 = f1.split('/');
            var aFecha2 = f2.split('/');
            var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]);
            var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]);
            var dif = fFecha2 - fFecha1;
            dias = (Math.floor(dif / (1000 * 60 * 60 * 24)))+1;
            if(dias<0){ dias = 0 }
        }else{ dias = 0;}

        //validar que la fecha de regreso sea mayor que la fecha de inicio
        var x2 = new Date();
        var x1 = new Date();
        var fecha2 = f2.split("/");
        var fecha1 = f1.split("/");

        x2.setFullYear(fecha2[2],fecha2[1]-1,fecha2[0]);
        x1.setFullYear(fecha1[2],fecha1[1]-1,fecha1[0]);


        if (x2 >= x1) {
            $('#num_total_dias').val(dias);
        }else{
            //return true;
            swal("Error", "La Fecha de Regreso no puede ser menor a la Fecha de Salida", "error");
            $('#fec_regreso').val(f2);
            return false;
        }



    });


    //*************************************

$(document).ready(function() {
        $('table.display').DataTable();

        $(".form-wizard-nav").on('click', function () {
            return false;
        });

        $("#ind_descripcion_empresa").val("{$datosViatico['fk_a001_num_organismo']}");
        $("#ind_descripcion_empresa").change();

        $("#ind_dependencia").val("{$datosViatico['fk_a004_num_dependencia']}");
        $("#ind_dependencia").change();

        $("#datosEmpleados").val("{$datosViatico['fk_rhb001_num_empleado_solicita']}");
        $("#datosEmpleados").change();

        $("#fec_salida").val("{trim($datosViatico['fec_salida'])}");
        $("#fec_salida").change();
        $("#fec_regreso").val("{$datosViatico['fec_ingreso']}");
        $("#fec_regreso").change();
        $("#num_total_dias").val("{$datosViatico['num_total_dias']}");
        $("#num_total_dias").change();




        //*********************************************
        $("#formAjax").validate({
            submitHandler: function(form) {

                var num_filas = $('#datatable3 >tbody >tr').length;
                var filas = $('#datatable3 >tbody >tr');
                if(num_filas == 1 &&  filas[0].cells[0].innerHTML == "No Hay Registros Disponibles"){
                    swal({
                        title: '',
                        text: 'Agregue al menos un beneficiario',
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: 'Aceptar',
                        closeOnConfirm: true
                    });

                }else{

                    var cad_cod_beneficiarios = '';
                    var cad_fecha_salida_celdas = '';
                    var cad_fecha_regreso_celdas = '';
                    var cad_total_dias = '';

                    for(var i=0; i<num_filas; i++){

                        cad_cod_beneficiarios = cad_cod_beneficiarios + filas[i].cells[0].innerHTML+'#';
                        cad_fecha_salida_celdas = cad_fecha_salida_celdas + $("#FechaSalidaCelda"+filas[i].cells[0].innerHTML).val()+'#';
                        cad_fecha_regreso_celdas = cad_fecha_regreso_celdas + $("#FechaRegresoCelda"+filas[i].cells[0].innerHTML).val()+'#';
                        cad_total_dias = cad_total_dias + $("#TotalDiasCelda"+filas[i].cells[0].innerHTML).val()+'#';
                    }

                    $('#id_beneficiariosDetalle').val(cad_cod_beneficiarios);
                    $('#fechas_salidas_celdasDetalle').val(cad_fecha_salida_celdas);
                    $('#fechas_regresos_celdasDetalle').val(cad_fecha_regreso_celdas);
                    $('#totales_dias_celdasDetalle').val(cad_total_dias);

                    var f1 = $('#fec_salida').val();
                    var f2 = $('#fec_regreso').val();

                    var x2 = new Date();
                    var x1 = new Date();
                    var fecha2 = f2.split("/");
                    var fecha1 = f1.split("/");

                    x2.setFullYear(fecha2[2],fecha2[1]-1,fecha2[0]);
                    x1.setFullYear(fecha1[2],fecha1[1]-1,fecha1[0]);


                    if (x2 >= x1) {

                        var url_nueva_solicitud = '{$_Parametros.url}modRH/gestion/viaticosCONTROL/RegistrarModificacionMET';
                        $.post(url_nueva_solicitud,$(form).serialize(),function(respuesta_post){

                            if(respuesta_post == 1) {
                                swal("Cambios efectuados", "guardado satisfactoriamente", "success");
                                $(document.getElementById('cerrarModal')).click();{*se esta cerrando la modal 1*}
                                $(document.getElementById('ContenidoModal')).html('');
                                $( "#formAjax2" ).submit();{* reenvio el formulario del listado para que recargue la lista actualizada*}

                            }else{

                                alert(respuesta_post);
                            }

                        },'json');


                    }else{
                        //return true;
                        swal("Error", "La Fecha de Regreso no puede ser menor a la Fecha de Salida", "error");
                        $('#fec_regreso').val(f2);//
                        return false;
                    }


                }



            }
        });

        //*************************************
        var tabla_beneficiarios_detalle = $('#datatable3').DataTable();
        $('#datatable3 tbody').on( 'click', '.borrarDetalle', function () {

            var obj = this;

            swal({
                title:'Estas Seguro?',
                text: 'Estas seguro que desea descartar al beneficiario(a)',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Descartar",
                closeOnConfirm: false
            }, function(){

                tabla_beneficiarios_detalle.row( $(obj).parents('tr') ).remove().draw();
                swal("Descartado!", "Descartado de la lista de beneficiarios.", "success");
            });



        });

        //*************************************
        var $urlAgregarDetalleViatico ='{$_Parametros.url}modRH/gestion/viaticosCONTROL/AgregarBeneficiariosDetalleMET';
        $('#agregarBeneficiarioDetalle').click(function(){
            $('#modalAncho2').css( "width", "85%" );
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html("");
            $.post($urlAgregarDetalleViatico,{ organismo : $('#ind_descripcion_empresa').val() },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });



});//ready


//*************************************
    function calculaTotalDias(id){
        var f1 = $('#FechaSalidaCelda'+id).val();
        var f2 = $('#FechaRegresoCelda'+id).val();
        var dias;
        if(f1 !=''){
            var aFecha1 = f1.split('/');
            var aFecha2 = f2.split('/');
            var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]);
            var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]);
            var dif = fFecha2 - fFecha1;
            dias = (Math.floor(dif / (1000 * 60 * 60 * 24)))+1;
            if(dias<0){ dias = 0 }
        }else{ dias = 0;}

        $('#TotalDiasCelda'+id).val(dias);

    }





</script>