<section class="style-default-light">

    <!-- BEGIN FORM WIZARD -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <div id="rootwizard2" class="form-wizard form-wizard-horizontal">

                        <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >
                            <!-- Navegacion form-wizard-nav ************************************************************************************  -->
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary">
                                    </div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title text-bold">INFORMACION GENERAL</span></a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title text-bold">BENEFICIARIOS</span></a></li>
                                </ul>
                            </div>
                            <!-- ***************************************************************************************************************** -->



                            <input type="hidden" name="id_beneficiarios" class="form-control" id="id_beneficiarios" value="">
                            <input type="hidden" name="fechas_salidas_celdas" id="fechas_salidas_celdas" class="form-control" value="">
                            <input type="hidden" name="fechas_regresos_celdas" id="fechas_regresos_celdas" class="form-control" value="">
                            <input type="hidden" name="totales_dias_celdas" id="totales_dias_celdas" class="form-control" value="">

                            <!-- Contenido de los tab -->
                            <div class="tab-content clearfix">
                                <!-- Tab numero 1 ************************************************************************************************************ -->
                                <div class="tab-pane active" id="tab1">
                                    <br/><br/>
                                    <div class="col-lg-6">
                                        <div class="form-group"  >
                                            <select id="ind_descripcion_empresa" class="form-control" name="ind_descripcion_empresa" required=""  >
                                                {foreach item=indice from=$datosOrganismos}
                                                    <option value="{$indice.pk_num_organismo}" selected="selected">{$indice.ind_descripcion_empresa}</option>
                                                {/foreach}
                                            </select>
                                            <label for="ind_descripcion_empresa" class="control-label">Organismo</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-group" >
                                            <input type="text" name="usuarioActual" required="" id="usuarioActual" class="form-control" value="{$usuario[0]['ind_nombre1']} {$usuario[0]['ind_apellido1']}" disabled="disabled">
                                            <label for="usuarioActual" class="control-label">Preparado</label>
                                       </div>
                                       <input type="hidden" id="idUsuario" name="form[int][idUsuario]" value="{$idUsuario}" />
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <select id="ind_dependencia" required="" class="form-control" name="ind_dependencia" >
                                                    {foreach item=indice from=$datosDependencias}
                                                        <option value="{$indice.pk_num_dependencia }" selected="selected">{$indice.ind_dependencia}</option>
                                                    {/foreach}
                                            </select>
                                            <label for="ind_dependencia" class="control-label">Dependencias</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <select id="datosEmpleados" required="" class="form-control"  name="datosEmpleados" >
                                                <option value="">...</option>
                                                {foreach item=indice from=$datosEmpleados}
                                                    <option value="{$indice.pk_num_empleado }" selected="selected">{$indice.pk_num_empleado} - {$indice.ind_nombre1}{if isset($indice.ind_nombre2) } {$indice.ind_nombre2} {/if} {$indice.ind_apellido1} {if isset($indice.ind_apellido2) } {$indice.ind_apellido2} {/if}  </option>
                                                {/foreach}
                                            </select>
                                            <label for="datosEmpleados" class="control-label">Solicitante</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="fec_salida" id="fec_salida" class="form-control" required=""  aria-required="true" aria-invalid="true" >
                                            <label for="fec_salida" class="control-label">Fecha de Salida</label>
                                            <p class="help-block">DD/MM/YYYY</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="fec_regreso" id="fec_regreso" class="form-control" required=""  aria-required="true" aria-invalid="true" >
                                            <label for="fec_regreso" class="control-label">Fecha de Regreso</label>
                                            <p class="help-block">DD/MM/YYYY</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="num_total_dias" class="form-control" value="0"  id="num_total_dias" name="num_total_dias" required data-rule-number="true"  data-msg-number="Por favor, introduzca un valor mayor o igual a 1">
                                            <label for="num_total_dias" class="control-label">Total de Días</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" name="ind_destino" id="ind_destino" class="form-control" required="" style="text-transform: uppercase" >
                                            <label for="ind_destino" class="control-label">Destino</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <textarea type="text" rows="2" name="ind_motivo" id="ind_motivo" class="form-control" required="" style="text-transform: uppercase" ></textarea>
                                            <label for="ind_motivo" class="control-label">Motivo</label>
                                        </div>
                                    </div>
                                    {if isset($unidadCombustible)}
                                        {if $unidadCombustible==1}
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <input type="text" name="num_unidad_combustible" id="num_unidad_combustible" class="form-control" disabled>
                                                <label for="num_unidad_combustible" class="control-label">Unidades de Combustible</label>
                                            </div>
                                        </div>
                                        {/if}
                                    {/if}
                                </div>
                                <!-- Tab numero 2 ******************************************************************************************************************************* -->
                                <br/><br/><br/>
                                <div class="tab-pane" id="tab2">
                                    <div class="col-md-12">
                                        <div class="card-actionbar">
                                            <div class="card-actionbar-row">
                                                <button type="button"  class="logsUsuario btn ink-reaction btn-raised btn-info" id="agregarBeneficiario"
                                                    descripcion="Agregar beneficiario" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#formModal2"
                                                    titulo="Agregar beneficiario">
                                                    <span class="md-queue "></span>
                                                    Agregar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <br/><br/><br/>
                                    <table id="datatableBeneficiarios" class="table table-striped table-hover" WIDTH="100%">
                                        <thead>
                                        <tr  align="center">
                                            <th width="5%"><i>#</i>N°</th>
                                            <th width="40%"><i class="md-person"></i>Nombre y Apellidos</th>
                                            <th width="10%"><i class="md-today"></i>Fecha Inicio</th>
                                            <th width="10%"><i class="md-today"></i>Fecha Fin</th>
                                            <th width="10%"><i class="md-av-timer"></i>Total Dias</th>
                                            <th width="5%"><i class="md-clear"></i>Descartar</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                    <br>
                                    <div align="center">
                                       <button type="submit" id="botonGuardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button"><span class="glyphicon glyphicon-floppy-disk"> Guardar</button>
                                    </div>
                                </div>

                                <!--******************************************************************************************** -->

                            </div><!--end .tab-content -->

                            <ul class="pager wizard">
                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Previo</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                            </ul>
                        </form>
                    </div><!--end #rootwizard -->
                </div><!--end .card-body -->
            </div><!--end .card -->

        </div><!--end .col -->
    </div><!--end .row -->

    <!-- END FORM WIZARD -->
</section>

<style>
    .celda_editable:hover{
        background-color: #afd9ee  !important;

    }
</style>
<script>

    //*****************************************

    $("#ind_descripcion_empresa").select2();
    $("#ind_dependencia").select2();
    $("#datosEmpleados").select2();
    $('#fec_salida').datepicker({ format: 'dd/mm/yyyy', language:'es', autoclose:true });
    $('#fec_regreso').datepicker({ format: 'dd/mm/yyyy', language:'es', autoclose:true});
    $('#fec_regreso').change(function(){
        var f1 = $('#fec_salida').val();
        var f2 = $('#fec_regreso').val();
        var dias;
        if(f1 !=''){
            var aFecha1 = f1.split('/');
            var aFecha2 = f2.split('/');
            var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]);
            var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]);
            var dif = fFecha2 - fFecha1;
            dias = (Math.floor(dif / (1000 * 60 * 60 * 24)))+1;
            if(dias<0){ dias = 0 }
        }else{ dias = 0;}

        //validar que la fecha de regreso sea mayor que la fecha de inicio
        var x2 = new Date();
        var x1 = new Date();
        var fecha2 = f2.split("/");
        var fecha1 = f1.split("/");

        x2.setFullYear(fecha2[2],fecha2[1]-1,fecha2[0]);
        x1.setFullYear(fecha1[2],fecha1[1]-1,fecha1[0]);


        if (x2 >= x1) {
            $('#num_total_dias').val(dias);
        }else{

                swal("Error", "La Fecha de Regreso no puede ser menor a la Fecha de Salida", "error");
                $('#fec_regreso').val('');
                return false;
            }



    });

    //*************************************

    $(document).ready(function() {

            $("#datosEmpleados").val("{$idUsuario}");
            $("#datosEmpleados").change(); {* se debe invocar este evento para que seleccione el valor pasado con val *}

            $("#ind_dependencia").val("{$dependenciaSolicitante}");
            $("#ind_dependencia").change();
            //*********************************************
            $(".form-wizard-nav").on('click', function () {
                var form = $('#rootwizard2').find('.form-validation');
                var valid = form.valid();
                if(!valid) {
                    form.data('validator').focusInvalid();
                    return false;
                }
            });

            $('#modalAncho').css( "width", "80%" );

        $('#datatableBeneficiarios').DataTable({
            "order": [[ 1, 'asc' ]],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }

        });

            $("#formAjax").validate({
                submitHandler: function(form) {

                var num_filas = $('#datatableBeneficiarios >tbody >tr').length;
                var filas = $('#datatableBeneficiarios >tbody >tr');
                if(num_filas == 1 &&  filas[0].cells[0].innerHTML == "No Hay Registros Disponibles"){
                    swal({
                        title: '',
                        text: 'Agregue al menos un beneficiario',
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: 'Aceptar',
                        closeOnConfirm: true
                    });
                }else{

                    var cad_cod_beneficiarios = '';
                    var cad_fecha_salida_celdas = '';
                    var cad_fecha_regreso_celdas = '';
                    var cad_total_dias = '';

                    for(var i=0; i<num_filas; i++){

                        cad_cod_beneficiarios = cad_cod_beneficiarios + filas[i].cells[0].innerHTML+'#';
                        cad_fecha_salida_celdas = cad_fecha_salida_celdas + $("#FechaSalidaCelda"+filas[i].cells[0].innerHTML).val()+'#';
                        cad_fecha_regreso_celdas = cad_fecha_regreso_celdas + $("#FechaRegresoCelda"+filas[i].cells[0].innerHTML).val()+'#';
                        cad_total_dias = cad_total_dias + $("#TotalDiasCelda"+filas[i].cells[0].innerHTML).val()+'#';
                    }

                    $('#id_beneficiarios').val(cad_cod_beneficiarios);
                    $('#fechas_salidas_celdas').val(cad_fecha_salida_celdas);
                    $('#fechas_regresos_celdas').val(cad_fecha_regreso_celdas);
                    $('#totales_dias_celdas').val(cad_total_dias);

                    var url_nueva_solicitud = '{$_Parametros.url}modRH/gestion/viaticosCONTROL/RegistrarNuevaSolicitudMET';
                    $.post(url_nueva_solicitud,$(form).serialize(),function(respuesta_post){

                        if(respuesta_post == 1) {
                            swal("Solicitud registrada", "guardado satisfactoriamente", "success");

                            /*$('#formAjax').each(function () { {*resetear los campos del formulario*}
                                this.reset();
                            });

                            var limpiarTabla = $('#datatable1').DataTable();
                            {*limpiar la tabla*}
                            limpiarTabla.clear().draw();

                            $('#formAjax a[href="#tab1"]').tab('show');*/
                            {*reiniciar el wizard al primer tab*}
                            $("#_contenido").load('{$_Parametros.url}modRH/gestion/viaticosCONTROL/ListarSolicitudesMET');
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        }else{

                            alert(respuesta_post);
                        }

                    },'json');


                }

            }
        });
    });
    //*************************************

    var $urlAgregar='{$_Parametros.url}modRH/gestion/viaticosCONTROL/AgregarBeneficiariosMET';
    $('#agregarBeneficiario').click(function(){
        $('#modalAncho2').css( "width", "85%" );
        $('#formModalLabel2').html($(this).attr('titulo'));
        $('#ContenidoModal2').html("");

        $.post($urlAgregar,{ organismo : $('#ind_descripcion_empresa').val() },function($dato){
            $('#ContenidoModal2').html($dato);
        });
    });

    //**************************************
    function calculaTotalDias(id){
        var f1 = $('#FechaSalidaCelda'+id).val();
        var f2 = $('#FechaRegresoCelda'+id).val();
        var dias;
        if(f1 !=''){
            var aFecha1 = f1.split('/');
            var aFecha2 = f2.split('/');
            var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]);
            var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]);
            var dif = fFecha2 - fFecha1;
            dias = (Math.floor(dif / (1000 * 60 * 60 * 24)))+1;
            if(dias<0){ dias = 0 }
        }else{ dias = 0;}

        $('#TotalDiasCelda'+id).val(dias);
    }

</script>