<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                    <form enctype="multipart/form-data" id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                        <input type="hidden" value="1" name="valido" />
                        <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
                        <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                        <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select id="fk_a006_num_miscelaneo_detalle_documento" name="form[int][fk_a006_num_miscelaneo_detalle_documento]" class="form-control input-sm" required data-msg-required="Seleccione Documento">
                                        <option value="">Seleccione...</option>
                                        {foreach item=dat from=$Documentos}
                                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_documento)}
                                                {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_documento}
                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fk_a006_num_miscelaneo_detalle_documento">Documento</label>
                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="checkbox checkbox-styled">
                                    <label>
                                        <input type="checkbox" {if isset($formDB.num_flag_entregado) and $formDB.num_flag_entregado==1} checked{/if} value="1" id="num_flag_entregado" name="form[int][num_flag_entregado]">
                                        Entregado?
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="checkbox checkbox-styled">
                                    <label>
                                        <input type="checkbox" {if isset($formDB.num_flag_carga) and $formDB.num_flag_carga==1} checked{/if} value="1" id="num_flag_carga" name="form[int][num_flag_carga]">
                                        Documento Familiar?
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" name="form[txt][fec_entrega]" id="fec_entrega" value="{if isset($formDB.fec_entrega)}{$formDB.fec_entrega|date_format:"d-m-Y"}{/if}" required data-msg-required="Indique Fecha de Entrega">
                                    <label for="fec_entrega">Fecha Entrega</label>
                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" name="form[txt][fec_vencimiento]" id="fec_vencimiento" value="{if isset($formDB.fec_vencimiento)}{$formDB.fec_vencimiento|date_format:"d-m-Y"}{/if}">
                                    <label for="fec_vencimiento">Fecha Vencimiento</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fk_c015_num_carga_familiar">Persona Relacionada</label>
                                    <select id="fk_c015_num_carga_familiar" name="form[int][fk_c015_num_carga_familiar]" class="form-control input-sm select2-list select2" disabled>
                                        <option value="">Seleccione...</option>
                                        {if isset($formDB)}
                                            {if $formDB.num_flag_carga==0}
                                            <option selected value="{$dat.pk_num_carga}">{$formDB.ind_nombre1} {$formDB.ind_nombre2} {$formDB.ind_apellido1} {$formDB.ind_apellido2}</option>
                                            {/if}
                                        {/if}
                                        {foreach item=dat from=$CargaFamiliar}
                                            {if isset($formDB.fk_c015_num_carga_familiar)}
                                                {if $dat.pk_num_carga==$formDB.fk_c015_num_carga_familiar}
                                                    <option selected value="{$dat.pk_num_carga}">{$dat.nombre}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_carga}">{$dat.nombre}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_carga}">{$dat.nombre}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="200" id="parentesco" name="form[txt][parentesco]" value="{if isset($formDB.parentesco)}{$formDB.parentesco}{/if}" disabled>
                                    <label for="parentesco">Parentesco</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="form[txt][txt_observaciones]" id="txt_observaciones" class="form-control input-sm" rows="3" placeholder="" onkeyup="$(this).val($(this).val().toUpperCase())">{if isset($formDB.txt_observaciones)}{$formDB.txt_observaciones}{/if}</textarea>
                                    <label for="txt_observaciones">Observaciones</label>
                                </div>
                            </div><!--end .col -->
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="fotoCargada">
                                    <img class="img-thumbnail border-gray border-lg img-responsive auto-width" src="{if isset($formDB.ind_documento)}{$_Parametros.ruta_Img}modRH/documentos/{$formDB.ind_documento}{else}{$_Parametros.ruta_Img}documentos.png{/if}" alt="Cargar o Actualizar Documento del Empleado" title="Cargar o Actualizar Documento del Empleado" id="cargarImagen" style="cursor: pointer; width: 140px; height: 140px"/>
                                </div>
                                <input type="file" name="Foto" id="Foto" style="display: none" />
                                <input type="hidden" name="form[txt][aleatorio]" id="aleatorio" value="{if isset($aleatorio)}{$aleatorio}{/if}">
                                <small id="ti">Click para Cargar Foto</small>
                            </div><!--end .col -->
                        </div><!--end .row -->
                        <div class="row">
                            <div class="col-md-12">
                                <div align="right">
                                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                    <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                                </div>
                            </div>
                        </div>
                    </form>

            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //inicializar el datepicker
    $("#fec_entrega").datepicker({
        format:'dd-mm-yyyy',
        language:'es',
        autoclose: true
    });
    $("#fec_vencimiento").datepicker({
        format:'dd-mm-yyyy',
        language:'es',
        autoclose: true
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/documentosCONTROL/DocumentosMET', datos ,function(dato){

                if(dato['status']=='error'){

                    /*Error para usuarios normales del sistema*/
                    //swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");

                }else if(dato['status']=='modificar'){

                    $(document.getElementById('idDocumento'+dato['idDocumento'])).html('<td width="14%">'+dato['documento']+'</td>' +
                            '<td width="20%">'+dato['ind_nombre1']+' '+dato['ind_nombre2']+' '+dato['ind_apellido1']+' '+dato['ind_apellido2']+'</td>' +
                            '<td width="5%"><i class="md md-check"></i></td>' +
                            '<td width="8%">'+dato['fec_entrega']+'</td>' +
                            '<td width="20%">'+dato['txt_observaciones']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado Documento del Empleado" titulo="Modificar Documentos">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDocumento="'+dato['idDocumento']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Documento del Empleado" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Documento!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){

                       $(document.getElementById('datatable2')).append('<tr id="idDocumento'+dato['idDocumento']+'" style="font-size: 11px">' +
                                '<td width="14%">'+dato['documento']+'</td>' +
                                '<td width="20%">'+dato['ind_nombre1']+' '+dato['ind_nombre2']+' '+dato['ind_apellido1']+' '+dato['ind_apellido2']+'</td>' +
                                '<td width="5%"><i class="md md-check"></i></td>' +
                                '<td width="8%">'+dato['fec_entrega']+'</td>' +
                                '<td width="20%">'+dato['txt_observaciones']+'</td>' +
                                '<td width="5%">' +
                               '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                               'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                               'descipcion="El Usuario a Modificado Documento del Empleado" titulo="Modificar Documentos">' +
                               '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                               '</td>'+
                               '<td width="5%">' +
                               '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDocumento="'+dato['idDocumento']+'"  boton="si, Eliminar"' +
                               'descipcion="El usuario a eliminado Documento del Empleado" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Documento!!">' +
                               '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');
                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "50%" );


        $("#fecha_2").datepicker({
            format:'dd-mm-yyyy'
        });

        /*BORDE DE LA VENTANA*/
        $("#contenidoModal2").css({ 'border' : '2px solid #0aa89e' });
        /*$("#headerModal2").css({ 'background-color':'#0aa89e','color' : '#FFFFFF' });*/


        /*checkbox entregado y documento familiar*/
        $('#num_flag_entregado').click(function() {
            if( $('#num_flag_entregado').is(':checked') ) {
                $("#fec_entrega").prop("disabled", false);
                $("#fec_vencimiento").prop("disabled", false);
            }else{
                $("#fec_entrega").prop("disabled", true);
                $("#fec_vencimiento").prop("disabled", true);
            }
        });
        $('#num_flag_carga').click(function() {
            if( $('#num_flag_carga').is(':checked') ) {
                $("#fk_c015_num_carga_familiar").prop("disabled", false);
            }else{
                $("#fk_c015_num_carga_familiar").prop("disabled", true);
            }
        });

        /*AL SELECCIONAR UN REGISTRO DE LA CARGA FAMILIAR*/
        $("#fk_c015_num_carga_familiar").change(function () {
            var cod = $('#fk_c015_num_carga_familiar').val();
            $("#fk_c015_num_carga_familiar option:selected").each(function () {
                $.post("{$_Parametros.url}modRH/gestion/documentosCONTROL/ParentescoCargaFamiliarMET", { idCargaFamiliar: cod }, function(data){
                       $("#parentesco").val(data['parentesco']);
                },'json');
            });
        });

        /*TAMAÑO DE LA IMAGEN*/
        $("#cargarImagen").css({ 'width':'140px', 'height':'140px' });

        //AL DAR CLICK EN LA IMAGEN AVATAR DEL FORMULARIO
        $("#cargarImagen").click(function() {

            $("#Foto").click();

        });

        //AL DAR CLICK EN LA IMAGEN AVATAR DEL FORMULARIO
        $("#Foto").change(function(e) {

            var files = e.target.files; // FileList object
            //obtenemos un array con los datos del archivo
            var file = $("#Foto")[0].files[0];
            //obtenemos el nombre del archivo
            var fileName = file.name;
            //obtenemos la extensión del archivo
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);

            if(isImage(fileExtension))
            {
                // Obtenemos la imagen del campo "file".
                for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }
                    var reader = new FileReader();
                    reader.onload = (function(theFile) {
                        return function(e) {
                            // Insertamos la imagen
                            $("#cargarImagen").attr("src",e.target.result);
                            $("#cargarImagen").css({ 'width':'140px', 'height':'140px' });
                            //document.getElementById("fotoCargada").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                            var data = new FormData($("#formAjax")[0]);
                            data.append('tipo','DOCUMENTOS');
                            data.append('empleado','DOCUMENTOS');

                            $.ajax({
                                url: '{$_Parametros.url}modRH/gestion/empleadosCONTROL/CopiarImagenTmpMET',
                                data: data,
                                type: 'POST',
                                contentType: false,
                                processData: false,
                                success: function(data){
                                    //la asigno al campo de texto
                                    $("#aleatorio").val(data);
                                }
                            });
                        };
                    })(f);
                    reader.readAsDataURL(f);
                }
            }else{

                swal("Error En Imagen!", "Asegurese de subir una imagen tipo: jpg - jpeg - png", "error");

            }


        });


    });
    //comprobamos si el archivo a subir es una imagen
    //para visualizarla una vez haya subido
    function isImage(extension)
    {
        switch(extension.toLowerCase())
        {
            case 'jpg':  case 'png': case 'jpeg':
            return true;
            break;
            default:
                return false;
                break;
        }
    }






</script>