<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idReferencia}" name="idReferencia"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select id="fk_a006_num_miscelaneo_detalle_tiporef" name="form[int][fk_a006_num_miscelaneo_detalle_tiporef]" class="form-control input-sm" required data-msg-required="Seleccione Tipo de Referencia">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$TipoReferencia}
                                                    {if isset($formDB.fk_a006_num_miscelaneo_detalle_tiporef)}
                                                        {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tiporef}
                                                            <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a006_num_miscelaneo_detalle_tiporef">Tipo de Referencia</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="200" id="ind_nombre" name="form[txt][ind_nombre]" value="{if isset($formDB.ind_nombre)}{$formDB.ind_nombre}{/if}" required data-msg-required="Indique Nombre del Jefe" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_nombre">Nombre del Jefe</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="200" id="ind_empresa" name="form[txt][ind_empresa]" value="{if isset($formDB.ind_empresa)}{$formDB.ind_empresa}{/if}" required data-msg-required="Indique Empresa" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_empresa">Empresa</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="150" id="ind_cargo" name="form[txt][ind_cargo]" value="{if isset($formDB.ind_cargo)}{$formDB.ind_cargo}{/if}" required data-msg-required="Indique Cargo" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_cargo">Cargo</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="14" id="ind_telefono" name="form[txt][ind_telefono]" value="{if isset($formDB.ind_telefono)}{$formDB.ind_telefono}{/if}" required data-msg-required="Indique Teléfono" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_telefono">Teléfono</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="200" id="ind_direccion" name="form[txt][ind_direccion]" value="{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                            <label for="ind_direccion">Dirección</label>
                                        </div>
                                    </div><!--end .col -->
                                </div><!--end .row -->
                                <div class="row">
                                    <br><br>
                                    <div align="right">
                                        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"> Cancelar</button>
                                        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"> Guardar</button>
                                    </div>
                                </div>
                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">



    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/referenciasCONTROL/ReferenciasMET', datos ,function(dato){

                if(dato['status']=='error'){

                    /*Error para usuarios normales del sistema*/
                    //swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");

                }else if(dato['status']=='modificar'){

                    $(document.getElementById('idReferencia'+dato['idReferencia'])).html('<td width="5%">'+dato['tipo_referencia']+'</td>' +
                            '<td width="20%">'+dato['ind_nombre']+'</td>' +
                            '<td width="20%">'+dato['ind_empresa']+'</td>' +
                            '<td width="15%">'+dato['ind_cargo']+'</td>' +
                            '<td width="20%">'+dato['ind_direccion']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idReferencia="'+dato['idReferencia']+'"' +
                            'descipcion="El Usuario a Modificado una Referencia titulo="Modificar Refrencia del Empleado">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idReferencia="'+dato['idReferencia']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado una Referencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Referencia!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){

                        var tabla = $('#datatable2').DataTable();

                        tabla.row.add([
                            dato['tipo_referencia'],
                            dato['ind_nombre'],
                            dato['ind_empresa'],
                            dato['ind_cargo'],
                            dato['ind_direccion'],
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idReferencia="'+dato['idReferencia']+'"' +
                            'descipcion="El Usuario a Modificado una Referencia titulo="Modificar Refrencia del Empleado">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}',
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idReferencia="'+dato['idReferencia']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado una Referencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Referencia!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}'
                        ]).draw()
                          .nodes()
                          .to$()
                          .addClass( '' );

                       /* $(document.getElementById('datatable2')).append('<tr id="idReferencia'+dato['idReferencia']+'" style="font-size: 11px">' +
                                '<td width="20%">'+dato['marca_vehiculo']+'</td>' +
                                '<td width="20%">'+dato['ind_modelo']+'</td>' +
                                '<td width="10%">'+dato['fec_anio']+'</td>' +
                                '<td width="10%">'+dato['ind_placa']+'</td>' +
                                '<td width="15%">'+dato['color_vehiculo']+'</td>' +
                                '<td width="15%">'+dato['num_valor']+'</td>' +
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                                'data-keyboard="false" data-backdrop="static" idReferencia="'+dato['idReferencia']+'"' +
                                'descipcion="El Usuario a Modificado una Referencia titulo="Modificar Refrencia del Empleado">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idReferencia="'+dato['idReferencia']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado una Referencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Referencia!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');*/
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');
                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "45%" );

        $("#contenidoModal2").css({ 'border' : '2px solid #0aa89e' });
        $("#headerModal2").css({ 'background-color':'#0aa89e','color' : '#FFFFFF' });


    });






</script>