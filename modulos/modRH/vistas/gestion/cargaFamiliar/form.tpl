<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                        <div id="rootwizard2" class="form-wizard form-wizard-horizontal">

                            <form enctype="multipart/form-data" id="formAjax" class="form form-validation" role="form" novalidate="novalidate" autocomplete="off">

                                <div class="form-wizard-nav">
                                    <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                                    <ul class="nav nav-justified">
                                        <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">DATOS GENERALES</span></a></li>
                                        <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">EDUCACIÓN Y OCUPACIÓN</span></a></li>
                                    </ul>
                                </div><!--end .form-wizard-nav -->

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idCargaFamiliar}" name="idCargaFamiliar"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" id="idPersona" name="idPersona"/>
                                <input type="hidden" value="0" name="existe" id="existe">
                                <input type="hidden" value="{$tipo}" name="tipo" id="tipo">

                                <div class="tab-content clearfix">

                                    <!-- datos genereales-->
                                    <div class="tab-pane active" id="step1">

                                        <br/><br/>
                                        <div class="col-xs-10" style="margin-top: -10px">
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <div class="form-group" data-toggle="tooltip" data-placement="top" title="Seleccionar solo si la persona NO Posee Cedula" data-original-title="Seleccionar solo si la persona NO Posee Cedula">
                                                        <div class="checkbox-inline checkbox-styled checkbox-danger">
                                                             <label>
                                                                 <input type="checkbox" value="1" name="form[int][no_posee]" id="no_posee" >
                                                             </label>
                                                        </div>
                                                        <label for="no_posee" class="text-xs">No</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <div class="input-group">   
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" id="cedula" name="form[txt][ind_cedula_documento]" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" required data-msg-required="Indique Cedula">
                                                                <input type="hidden" value="0" id="pk_num_persona" name="form[int][pk_num_persona]">
                                                                <label for="ind_cedula_documento">Cedula de Identidad</label>
                                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                            </div> 
                                                            <div class="input-group-btn">
                                                                <button class="btn btn-sm btn-info" type="button" id="buscarPersona" data-toggle="tooltip" data-placement="top" title="" data-original-title="Click Para Verificar Cedula Persona"><span class="glyphicon glyphicon-search"></span></button>
                                                            </div>
                                                        </div>
                                                    </div>        
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_sexo" name="form[int][fk_a006_num_miscelaneo_detalle_sexo]" class="form-control input-sm" required data-msg-required="Seleccione el Sexo" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=sex from=$Sexo}
                                                                {if isset($formDB.fk_a006_num_miscelaneo_detalle_sexo)}
                                                                    {if $sex.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_sexo}
                                                                        <option selected value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_sexo">Sexo</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_parentezco" name="form[int][fk_a006_num_miscelaneo_detalle_parentezco]" class="form-control input-sm" required data-msg-required="Seleccione Parentesco" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$Parentesco}
                                                                {if isset($formDB.fk_a006_num_miscelaneo_detalle_parentezco)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_parentezco}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_parentezco">Parentesco</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>


                                            </div><!--end .row -->

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="apellido1" name="form[txt][apellido1]" value="{if isset($formDB.ind_apellido1)}{$formDB.ind_apellido1}{/if}" required data-msg-required="Indique Primer Apellido" style="text-transform:uppercase" disabled>
                                                        <label for="apellido1">Primer Apellido</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="apellido2" name="form[txt][apellido2]" value="{if isset($formDB.ind_apellido2)}{$formDB.ind_apellido2}{/if}" style="text-transform:uppercase">
                                                        <label for="apellido2">Segundo Apellido</label>

                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="nombre1" name="form[txt][nombre1]" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1}{/if}" required data-msg-required="Indique Primer Nombre" style="text-transform:uppercase" disabled>
                                                        <label for="nombre1">Primer Nombre</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div><!--end .col -->
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm" id="nombre2" name="form[txt][nombre2]" value="{if isset($formDB.ind_nombre2)}{$formDB.ind_nombre2}{/if}" style="text-transform:uppercase">
                                                        <label for="nombre2">Segundo Nombre</label>

                                                    </div>
                                                </div><!--end .col -->
                                            </div><!--end .row -->

                                            <div class="row">
                                                <input type="hidden" value="{if isset($formDBdireccion.pk_num_direccion_persona)}{$formDBdireccion.pk_num_direccion_persona}{/if}" name="form[int][pk_num_direccion_persona]" />
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_tipodir" name="form[int][fk_a006_num_miscelaneo_detalle_tipodir]" class="form-control input-sm">
                                                            <option value="0">Seleccione...</option>
                                                            {foreach item=dat from=$TipoDir}
                                                                {if isset($formDBdireccion.fk_a006_num_miscelaneo_detalle_tipodir)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBdireccion.fk_a006_num_miscelaneo_detalle_tipodir}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_tipodir">Tipo. Dirección</label>                                                        
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group" id="fk_a006_num_miscelaneo_detalle_domicilioError">
                                                        <select id="fk_a006_num_miscelaneo_detalle_domicilio" name="form[int][fk_a006_num_miscelaneo_detalle_domicilio]" class="form-control input-sm" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$SituacionDomicilio}
                                                                {if isset($formDBdireccion.fk_a006_num_miscelaneo_detalle_domicilio)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBdireccion.fk_a006_num_miscelaneo_detalle_domicilio}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_domicilio">Sit. Domicilio</label>                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group" id="fk_a006_num_tipo_zonaError">
                                                        <select id="fk_a006_num_tipo_zona" name="form[int][fk_a006_num_tipo_zona]" class="form-control input-sm" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$TipoZona}
                                                                {if isset($formDBdireccion.fk_a006_num_tipo_zona)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBdireccion.fk_a006_num_tipo_zona}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_tipo_zona">Tipo Zona</label>                 
                                                    </div>
                                                </div>
                                            </div><!--end .row -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group" id="fk_a006_num_ubicacion_inmuebleError">
                                                        <select id="fk_a006_num_ubicacion_inmueble" name="form[int][fk_a006_num_ubicacion_inmueble]" class="form-control input-sm" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$UbicacionInmueble}
                                                                {if isset($formDBdireccion.fk_a006_num_ubicacion_inmueble)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBdireccion.fk_a006_num_ubicacion_inmueble}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_ubicacion_inmueble">Ubicacion Inmueble</label>              
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group" id="ind_ubicacion_inmuebleError">
                                                        <input type="text" class="form-control input-sm" id="ind_ubicacion_inmueble" name="form[txt][ind_ubicacion_inmueble]" value="{if isset($formDBdireccion.ind_ubicacion_inmueble)}{$formDBdireccion.ind_ubicacion_inmueble}{/if}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group" id="fk_a006_num_tipo_inmuebleError">
                                                        <select id="fk_a006_num_tipo_inmueble" name="form[int][fk_a006_num_tipo_inmueble]" class="form-control input-sm" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$TipoInmueble}
                                                                {if isset($formDBdireccion.fk_a006_num_tipo_inmueble)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBdireccion.fk_a006_num_tipo_inmueble}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_tipo_inmueble">Tipo Inmueble</label>                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group" id="ind_tipo_inmuebleError">
                                                        <input type="text" class="form-control input-sm" id="ind_tipo_inmueble" name="form[txt][ind_tipo_inmueble]" value="{if isset($formDBdireccion.ind_tipo_inmueble)}{$formDBdireccion.ind_tipo_inmueble}{/if}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group" id="fk_a006_num_zona_residencialError">
                                                        <select id="fk_a006_num_zona_residencial" name="form[int][fk_a006_num_zona_residencial]" class="form-control input-sm" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$ZonaResidencial}
                                                                {if isset($formDBdireccion.fk_a006_num_zona_residencial)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDBdireccion.fk_a006_num_zona_residencial}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_tipo_inmueble">Zona Residencial</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="form-group" id="ind_zona_residencialError">
                                                        <input type="text" class="form-control input-sm" id="ind_zona_residencial" name="form[txt][ind_zona_residencial]" value="{if isset($formDBdireccion.ind_zona_residencial)}{$formDBdireccion.ind_zona_residencial}{/if}" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group" id="ind_direccionError">
                                                        <input type="text" class="form-control input-sm" id="ind_direccion" name="form[txt][ind_direccion]" value="{if isset($formDBdireccion.ind_direccion)}{$formDBdireccion.ind_direccion}{/if}" disabled>
                                                        <label for="ind_direccion">Dirección</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!--end .col -->

                                        <div class="col-xs-2" style="margin-top: -10px;">
                                            <div class="row" align="center">
                                                <div class="col-md-12">
                                                    <div id="fotoCargada">
                                                        <img class="img-thumbnail border-gray border-lg img-responsive auto-width" src="{if isset($formDB.ind_foto)}{$_Parametros.ruta_Img}modRH/fotos/{$formDB.ind_foto}{else}{$_Parametros.ruta_Img}foto.png{/if}" alt="Cargar o Actualizar Foto de la Persona" title="Cargar o Actualizar Foto de la Persona" id="cargarImagen" style="cursor: pointer; width: 140px; height: 140px"/>
                                                    </div>
                                                    <input type="file" name="Foto" id="Foto" style="display: none" />
                                                    <input type="hidden" name="form[txt][aleatorio]" id="aleatorio" value="{if isset($aleatorio)}{$aleatorio}{/if}">
                                                </div><!--end .col -->
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group" id="ind_inmuebleError">
                                                        <input type="text" class="form-control input-sm" id="ind_inmueble" name="form[txt][ind_inmueble]" value="{if isset($formDBdireccion.ind_inmueble)}{$formDBdireccion.ind_inmueble}{/if}" disabled>
                                                        <label for="ind_inmueble">Inmueble</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="form-group" id="ind_punto_referenciaError">
                                                        <input type="text" class="form-control input-sm" id="ind_punto_referencia" name="form[txt][ind_punto_referencia]" value="{if isset($formDBdireccion.ind_punto_referencia)}{$formDBdireccion.ind_punto_referencia}{/if}" disabled>
                                                        <label for="ind_punto_referencia">Punto de Refrencia</label>                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" id="ind_zona_postalError">
                                                        <input type="text" class="form-control input-sm" id="ind_zona_postal" name="form[txt][ind_zona_postal]" value="{if isset($formDBdireccion.ind_zona_postal)}{$formDBdireccion.ind_zona_postal}{/if}" data-msg-required="requerido" disabled>
                                                        <label for="ind_zona_postal">Zona Postal</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xs-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <div class="input-group date" id="fecha">
                                                            <div class="input-group-content">
                                                                <input type="text" class="form-control input-sm" name="form[txt][fec_nacimiento]" id="fec_nacimiento" value="{if isset($formDB.fec_nacimiento)}{$formDB.fec_nacimiento|date_format:"d-m-Y"}{/if}" required data-msg-required="Indique Fecha de Nacimiento" disabled>
                                                                <label for="fec_nacimiento">Fecha Nacimiento</label>
                                                            </div>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group"><!--<small id="ampliarFoto" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" style="cursor: pointer">Ampliar Foto</small>-->
                                                        <select id="fk_a006_num_miscelaneo_det_gruposangre" name="form[int][fk_a006_num_miscelaneo_det_gruposangre]" class="form-control input-sm" required disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$GrupoSanguineo}
                                                                {if isset($formDB.fk_a006_num_miscelaneo_det_gruposangre)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_det_gruposangre}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_det_gruposangre">Grupo Sanguineo</label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_edocivil" name="form[int][fk_a006_num_miscelaneo_detalle_edocivil]" class="form-control input-sm" required data-msg-required="Seleccione Estado Civil" disabled>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$EdoCivil}
                                                                {if isset($formDB.fk_a006_num_miscelaneo_detalle_edocivil)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_edocivil}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a006_num_miscelaneo_detalle_edocivil">Estado Civil</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="col-lg-12">
                                                        <div class="card">
                                                            <div class="card-body no-padding">
                                                                <div class="table-responsive no-margin">
                                                                    <table class="table table-striped no-margin" id="contenidoTabla">
                                                                        <thead>
                                                                        <tr>
                                                                            <th class="text-center">#</th>
                                                                            <th>Tipo Telefono</th>
                                                                            <th>Telefono</th>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        {if isset($formDBtelefono)}
                                                                            {foreach item=datos from=$formDBtelefono}
                                                                                <tr>
                                                                                    <input type="hidden" value="{$datos.pk_num_telefono}" name="form[int][pk_num_telefono][{$n}]">
                                                                                    <td class="text-right" style="vertical-align: middle;">
                                                                                        {$numero++}
                                                                                    </td>
                                                                                    <td style="vertical-align: middle;">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="form-group">
                                                                                                <select id="fk_a006_num_miscelaneo_detalle_tipo_telefono{$n}" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_telefono][{$n}]" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">
                                                                                                    <option value="">Seleccione...</option>
                                                                                                    {foreach item=dat from=$TipoTelf}
                                                                                                        {if isset($datos.fk_a006_num_miscelaneo_detalle_tipo_telefono)}
                                                                                                            {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_tipo_telefono}
                                                                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {else}
                                                                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                            {/if}
                                                                                                        {else}
                                                                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                                                        {/if}
                                                                                                    {/foreach}
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td style="vertical-align: middle;">
                                                                                        <div class="col-sm-12">
                                                                                            <div class="form-group floating-label" id="ind_telefono{$n}Error">
                                                                                                <input type="text" class="form-control input-sm" value="{$datos.ind_telefono}" name="form[txt][ind_telefono][{$n}]" id="ind_telefono{$n}">
                                                                                                <label for="ind_telefono{$n}"><i class="md md-insert-comment"></i> Telefono </label>
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <input type="hidden" value="{$n++}">
                                                                                </tr>
                                                                            {/foreach}
                                                                        {/if}
                                                                        </tbody>
                                                                        <tfoot>
                                                                        <tr>
                                                                            <td colspan="3">
                                                                                <div class="col-sm-12 text-center">
                                                                                    <button type="button" id="nuevoTelefono" class="btn btn-info ink-reaction btn-raised">
                                                                                        <i class="md md-add"></i>
                                                                                    </button>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        </tfoot>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="checkbox checkbox-styled">
                                                        <label>
                                                            <input type="checkbox" {if isset($formDB.num_flag_discapacidad) and $formDB.num_flag_discapacidad==1} checked{/if} value="1" name="form[int][num_flag_discapacidad]" id="num_flag_discapacidad">
                                                            <span>Familiar con Discapacidad</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- fin datos generales -->

                                    <!-- nacimiento -->
                                    <div class="tab-pane" id="step2">
                                        <br><br>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <select id="fk_a006_num_miscelaneo_detalle_gradoinst" name="form[int][fk_a006_num_miscelaneo_detalle_gradoinst]" class="form-control input-sm">
                                                        <option value="">Seleccione</option>
                                                        {foreach item=grado from=$GradoInst}
                                                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_gradoinst)}
                                                                {if $grado.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_gradoinst}
                                                                    <option selected value="{$grado.pk_num_miscelaneo_detalle}">{$grado.ind_nombre_detalle}</option>
                                                                {else}
                                                                    <option value="{$grado.pk_num_miscelaneo_detalle}">{$grado.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {else}
                                                                <option value="{$grado.pk_num_miscelaneo_detalle}">{$grado.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="fk_a006_num_miscelaneo_detalle_gradoinst">Grado de Instrucción</label>

                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <select id="fk_a006_num_miscelaneo_detalle_tipoedu" name="form[int][fk_a006_num_miscelaneo_detalle_tipoedu]" class="form-control input-sm">
                                                        <option value="">Seleccione</option>
                                                        {foreach item=dat from=$TipoEdu}
                                                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipoedu)}
                                                                {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipoedu}
                                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {else}
                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="fk_a006_num_miscelaneo_detalle_tipoedu">Tipo de Educacion</label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <select id="fk_rhc064_nivel_grado_instruccion" name="form[int][fk_rhc064_nivel_grado_instruccion]" class="form-control input-sm">
                                                        <option value="">Seleccione</option>
                                                        {foreach item=dat from=$NivelGradoInstruccion}
                                                            {if isset($formDB.fk_rhc064_nivel_grado_instruccion)}
                                                                {if $dat.pk_num_nivel_grado==$formDB.fk_rhc064_nivel_grado_instruccion}
                                                                    <option selected value="{$dat.pk_num_nivel_grado}">{$dat.ind_nombre_nivel_grado}</option>
                                                                {else}
                                                                    <option value="{$dat.pk_num_nivel_grado}">{$dat.ind_nombre_nivel_grado}</option>
                                                                {/if}
                                                            {else}
                                                                <option value="{$dat.pk_num_nivel_grado}">{$dat.ind_nombre_nivel_grado}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="fk_rhc064_nivel_grado_instruccion">Nivel Grado Instrucción</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="checkbox checkbox-styled">
                                                    <label>
                                                        <input type="checkbox" {if isset($formDB.num_flag_estudia) and $formDB.num_flag_estudia==1} checked{/if} value="1" name="form[int][num_flag_estudia]" id="num_flag_estudia">
                                                        <span>Estudia?</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="fk_rhc040_num_institucion">Centro de Estudio</label>
                                                    <select id="fk_rhc040_num_institucion" name="form[int][fk_rhc040_num_institucion]" class="form-control input-sm select2-list select2">
                                                        <option value="">Seleccione Centro de Estudio</option>
                                                        {foreach item=dat from=$Centros}
                                                            {if isset($formDB.fk_rhc040_num_institucion)}
                                                                {if $dat.pk_num_institucion==$formDB.fk_rhc040_num_institucion}
                                                                    <option selected value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                                                {else}
                                                                    <option value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                                                {/if}
                                                            {else}
                                                                <option value="{$dat.pk_num_institucion}">{$dat.ind_nombre_institucion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="ind_empresa" name="form[txt][ind_empresa]" value="{if isset($formDB.ind_empresa)}{$formDB.ind_empresa}{/if}" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                    <label for="ind_empresa">Empresa</label>
                                                </div>
                                            </div><!--end .col -->
                                            <div class="col-sm-4">
                                                <div class="checkbox checkbox-styled">
                                                    <label>
                                                        <input type="checkbox" {if isset($formDB.num_flag_trabaja) and $formDB.num_flag_trabaja==1} checked{/if} value="1" name="form[int][num_flag_trabaja]" id="num_flag_trabaja">
                                                        <span>Trabaja?</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm" id="txt_direccion_empresa" name="form[txt][txt_direccion_empresa]" value="{if isset($formDB.txt_direccion_empresa)}{$formDB.txt_direccion_empresa}{/if}"  onkeyup="$(this).val($(this).val().toUpperCase())">
                                                    <label for="txt_direccion_empresa">Dirección</label>
                                                </div>
                                            </div><!--end .col -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control input-sm bolivar-mask" id="num_sueldo_mensual" name="form[int][num_sueldo_mensual]" value="{if isset($formDB.num_sueldo_mensual)}{$formDB.num_sueldo_mensual|number_format:2:",":"."}{else}0,00{/if}">
                                                    <label for="num_sueldo_mensual">Sueldo Mensual</label>
                                                </div>
                                            </div><!--end .col -->
                                        </div><!--end .row -->



                                    </div>

                                </div>
                                
                                <div class="row">
                                    <div class="col-md-12">
                                        <div align="right">
                                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="btnGuardarCF" disabled><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                                <ul class="pager wizard">
                                    <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <li class="next" id="siguiente" style="visibility: hidden"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                                </ul>

                            </form>
                        </div>

            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //inicializar el datepicker
    $("#fecha").datepicker({
        format:'dd-mm-yyyy',
        language:'es',
        autoclose:true
    });

    var app = new AppFunciones();
    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();
            
            $.post('{$_Parametros.url}modRH/gestion/cargaFamiliarCONTROL/CargaFamiliarMET', datos ,function(dato){

                if(dato['status']=='error'){
                    /*Error para usuarios normales del sistema*/
                    swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    //swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");
                }else if(dato['status']=='modificar'){

                    $(document.getElementById('idCargaFamiliar'+dato['idCargaFamiliar'])).html('<td>'+dato['ind_cedula_documento']+'</td>' +
                            '<td width="40%">'+dato['nombre1']+' '+dato['nombre2']+' '+dato['apellido1']+' '+dato['apellido2']+'</td>' +
                            '<td width="10%">Recargar</td>' +
                            '<td width="15%">'+dato['fec_nacimiento']+'</td>' +
                            '<td width="10%">Recargar</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idCargaFamiliar="'+dato['idCargaFamiliar']+'"' +
                            'descipcion="El Usuario a Modificado Carga Familiar" titulo="Modificar Carga Familiar">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCargaFamiliar="'+dato['idCargaFamiliar']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Carga Familiar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Carga Familiar!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){

                    $(document.getElementById('datatable2')).append('<tr id="idCargaFamiliar'+dato['idCargaFamiliar']+'" style="font-size: 11px">' +
                            '<td width="15%">'+dato['ind_cedula_documento']+'</td>' +
                            '<td width="40%">'+dato['nombre1']+' '+dato['nombre2']+' '+dato['apellido1']+' '+dato['apellido2']+'</td>' +
                            '<td width="10%">Recargar</td>' +
                            '<td width="15%">'+dato['fec_nacimiento']+'</td>' +
                            '<td width="10%">Recargar</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idCargaFamiliar="'+dato['idCargaFamiliar']+'"' +
                            'descipcion="El Usuario a Modificado Carga Familiar" titulo="Modificar Carga Familiar">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCargaFamiliar="'+dato['idCargaFamiliar']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Carga Familiar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Carga Familiar!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '</tr>');
                    swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');


                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "72%" );
        //Script para forzar el avance del wizard por el botón suigiente.
        $(".form-wizard-nav").on('click', function(){
            var form = $('#rootwizard2').find('.form-validation');
            var valid = form.valid();
            if(!valid) {
                form.data('validator').focusInvalid();
                return false;
            }
        });
        
        //activo los campos si vamos a modificar los rgistros
        if($("#tipo").val()=='MO'){
            $("#existe").val(1);
            $('#no_posee').attr("disabled","disabled");
            $("#apellido1").removeAttr("disabled");
            $("#nombre1").removeAttr("disabled");
            $("#fk_a006_num_miscelaneo_detalle_sexo").removeAttr("disabled");
            $("#fk_a006_num_miscelaneo_detalle_parentezco").removeAttr("disabled");
            $("#fec_nacimiento").removeAttr("disabled");
            $("#fk_a006_num_miscelaneo_det_gruposangre").removeAttr("disabled");
            $("#fk_a006_num_miscelaneo_detalle_edocivil").removeAttr("disabled");
            $("#siguiente").css({ 'visibility':'visible' });
            $("#btnGuardarCF").removeAttr("disabled");
            $("#buscarPersona").attr("disabled","disabled");
        }else{
            $("#buscarPersona").removeAttr("disabled");
        }
        
        //dependiendo del tipo de direccion habilito o no los campos de direccion
        var w = $("#fk_a006_num_miscelaneo_detalle_tipodir").val();
        if(w!=0){
           $("#fk_a006_num_tipo_zona").removeAttr("disabled");
           $("#fk_a006_num_tipo_zona").prop("required",true);
           $("#fk_a006_num_ubicacion_inmueble").removeAttr("disabled");
           $("#fk_a006_num_ubicacion_inmueble").prop("required",true);
           $("#ind_ubicacion_inmueble").removeAttr("disabled");
           $("#ind_ubicacion_inmueble").prop("required",true);
           $("#fk_a006_num_tipo_inmueble").removeAttr("disabled");
           $("#fk_a006_num_tipo_inmueble").prop("required",true);
           $("#ind_tipo_inmueble").removeAttr("disabled","disabled");
           $("#ind_tipo_inmueble").prop("required",true);
           $("#fk_a006_num_zona_residencial").removeAttr("disabled");
           $("#fk_a006_num_zona_residencial").prop("required",true);
           $("#ind_zona_residencial").removeAttr("disabled");
           $("#ind_zona_residencial").prop("required",true);
           $("#ind_inmueble").removeAttr("disabled");
           $("#ind_inmueble").prop("required",true);
           $("#ind_punto_referencia").removeAttr("disabled");
           $("#ind_punto_referencia").prop("required",true);
           $("#ind_zona_postal").removeAttr("disabled");
           $("#ind_zona_postal").prop("required",true);
           $("#ind_direccion").removeAttr("disabled");
           $("#ind_direccion").prop("required",true);
           $("#fk_a006_num_miscelaneo_detalle_domicilio").removeAttr("disabled");
           $("#fk_a006_num_miscelaneo_detalle_domicilio").prop("required",true);
          
        }else{
           $("#fk_a006_num_tipo_zona").attr("disabled","disabled");
           $("#fk_a006_num_ubicacion_inmueble").attr("disabled","disabled");
           $("#ind_ubicacion_inmueble").attr("disabled","disabled");
           $("#fk_a006_num_tipo_inmueble").attr("disabled","disabled");
           $("#ind_tipo_inmueble").attr("disabled","disabled");
           $("#fk_a006_num_zona_residencial").attr("disabled","disabled");
           $("#ind_zona_residencial").attr("disabled","disabled");
           $("#ind_inmueble").attr("disabled","disabled");
           $("#ind_punto_referencia").attr("disabled","disabled");
           $("#ind_zona_postal").attr("disabled","disabled");
           $("#ind_direccion").attr("disabled","disabled");
           $("#fk_a006_num_miscelaneo_detalle_domicilio").attr("disabled","disabled");
           $("#fk_a006_num_tipo_zona").removeAttr("required");
           $("#fk_a006_num_ubicacion_inmueble").removeAttr("required");
           $("#ind_ubicacion_inmueble").removeAttr("required");
           $("#fk_a006_num_tipo_inmueble").removeAttr("required");
           $("#ind_tipo_inmueble").removeAttr("required");
           $("#fk_a006_num_zona_residencial").removeAttr("required");
           $("#ind_zona_residencial").removeAttr("required");
           $("#ind_inmueble").removeAttr("required");
           $("#ind_punto_referencia").removeAttr("required");
           $("#ind_zona_postal").removeAttr("required");
           $("#ind_direccion").removeAttr("required");
           $("#fk_a006_num_miscelaneo_detalle_domicilio").removeAttr("required");
        }
        
        $("#buscarPersona").click(function() {

            var url_verificar = '{$_Parametros.url}modRH/gestion/cargaFamiliarCONTROL/VerificarCedulaPersonaMET';
            $.post(url_verificar,{ cedula: $("#cedula").val(), empleado: $("#idEmpleado").val() },function(dato){

                if(dato['P']==1){
                    swal("Cedula Registrada!", dato['mensaje']  , "warning");
                    $("#existe").val(0);
                    $("#siguiente").css({ 'visibility':'hidden' });
                    $("#btnGuardarCF").attr("disabled","disabled");
                    return false;
                }
                if(dato['P']==2){
                    swal("Cedula Registrada!", dato['mensaje']  , "warning");
                    $("#existe").val(0);
                    $("#siguiente").css({ 'visibility':'hidden' });
                    $("#btnGuardarCF").attr("disabled","disabled");
                    return false;
                }
                if(dato['P']==3){
                    swal("Cedula Registra!", dato['mensaje']  , "warning");
                    $("#idPersona").val(dato['data']['pk_num_persona']);
                    $("#pk_num_persona").val(dato['data']['pk_num_persona']);
                    $("#existe").val(1);
                    $("#apellido1").removeAttr("disabled");
                    $("#nombre1").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_detalle_sexo").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_detalle_parentezco").removeAttr("disabled");
                    $("#fec_nacimiento").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_det_gruposangre").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_detalle_edocivil").removeAttr("disabled");
                    $("#siguiente").css({ 'visibility':'visible' });
                    $("#btnGuardarCF").removeAttr("disabled");
                    //asigno los datos a los campos de texto y demas
                    $("#apellido1").val(dato['data']['ind_apellido1']);
                    $("#nombre1").val(dato['data']['ind_nombre1']);
                    $("#fk_a006_num_miscelaneo_detalle_sexo").val(dato['data']['fk_a006_num_miscelaneo_detalle_sexo']);                    
                    $("#fec_nacimiento").val(dato['data']['fec_nacimiento']);
                    $("#fk_a006_num_miscelaneo_det_gruposangre").val(dato['data']['fk_a006_num_miscelaneo_det_gruposangre']);
                    $("#fk_a006_num_miscelaneo_detalle_edocivil").val(dato['data']['fk_a006_num_miscelaneo_detalle_edocivil']);
                    return false;
                }
                if(dato['P']==4){
                    swal("Persona no Existe!", dato['mensaje']  , "warning");
                    $("#existe").val(0);
                    $("#apellido1").removeAttr("disabled");
                    $("#nombre1").removeAttr("disabled");
                    $("#apellido2").removeAttr("disabled");
                    $("#nombre2").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_detalle_sexo").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_detalle_parentezco").removeAttr("disabled");
                    $("#fec_nacimiento").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_det_gruposangre").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_detalle_edocivil").removeAttr("disabled");
                    $("#siguiente").css({ 'visibility':'visible' })
                    $("#btnGuardarCF").removeAttr("disabled"); 
                    return false;
                }                
            },'json');

        });        


        //AL SELECIONAR GRADO DE INSTRUCCION
        $("#fk_a006_num_miscelaneo_detalle_gradoinst").change(function () {
            $("#fk_a006_num_miscelaneo_detalle_gradoinst option:selected").each(function () {
                $.post("{$_Parametros.url}modRH/maestros/nivelGradoInstruccionCONTROL/MostrarNivelesGradoInstMET", { GradoInst: $('#fk_a006_num_miscelaneo_detalle_gradoinst').val() }, function(data){
                    var json = data,
                    obj = JSON.parse(json);
                    $("#fk_rhc064_nivel_grado_instruccion").html(obj);
                });
            });
        });

        //AL SELECCIONAR TIPO DE DIRECCION        
        $("#fk_a006_num_miscelaneo_detalle_tipodir").change(function () {
            $("#fk_a006_num_miscelaneo_detalle_tipodir option:selected").each(function () {
                var w = $("#fk_a006_num_miscelaneo_detalle_tipodir").val();
                if(w!=0){
                    $("#fk_a006_num_tipo_zona").removeAttr("disabled");
                    $("#fk_a006_num_tipo_zona").prop("required",true);
                    $("#fk_a006_num_ubicacion_inmueble").removeAttr("disabled");
                    $("#fk_a006_num_ubicacion_inmueble").prop("required",true);
                    $("#ind_ubicacion_inmueble").removeAttr("disabled");
                    $("#ind_ubicacion_inmueble").prop("required",true);
                    $("#fk_a006_num_tipo_inmueble").removeAttr("disabled");
                    $("#fk_a006_num_tipo_inmueble").prop("required",true);
                    $("#ind_tipo_inmueble").removeAttr("disabled","disabled");
                    $("#ind_tipo_inmueble").prop("required",true);
                    $("#fk_a006_num_zona_residencial").removeAttr("disabled");
                    $("#fk_a006_num_zona_residencial").prop("required",true);
                    $("#ind_zona_residencial").removeAttr("disabled");
                    $("#ind_zona_residencial").prop("required",true);
                    $("#ind_inmueble").removeAttr("disabled");
                    $("#ind_inmueble").prop("required",true);
                    $("#ind_punto_referencia").removeAttr("disabled");
                    $("#ind_punto_referencia").prop("required",true);
                    $("#ind_zona_postal").removeAttr("disabled");
                    $("#ind_zona_postal").prop("required",true);
                    $("#ind_direccion").removeAttr("disabled");
                    $("#ind_direccion").prop("required",true);
                    $("#fk_a006_num_miscelaneo_detalle_domicilio").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_detalle_domicilio").prop("required",true);
                }else{
                    $("#fk_a006_num_tipo_zona").attr("disabled","disabled");
                    $("#fk_a006_num_tipo_zona").removeAttr("aria-describedby");
                    $("#fk_a006_num_tipo_zona-error").remove();
                    $("#fk_a006_num_tipo_zonaError").removeClass('has-error');
                    $("#fk_a006_num_ubicacion_inmueble").attr("disabled","disabled");
                    $("#fk_a006_num_ubicacion_inmueble").removeAttr("aria-describedby");
                    $("#fk_a006_num_ubicacion_inmueble-error").remove();
                    $("#fk_a006_num_ubicacion_inmuebleError").removeClass('has-error');                    
                    $("#ind_ubicacion_inmueble").attr("disabled","disabled");
                    $("#ind_ubicacion_inmueble").removeAttr("aria-describedby");
                    $("#ind_ubicacion_inmueble-error").remove();
                    $("#ind_ubicacion_inmuebleError").removeClass('has-error');                     
                    $("#fk_a006_num_tipo_inmueble").attr("disabled","disabled");
                    $("#fk_a006_num_tipo_inmueble").removeAttr("aria-describedby");
                    $("#fk_a006_num_tipo_inmueble-error").remove();
                    $("#fk_a006_num_tipo_inmuebleError").removeClass('has-error');                     
                    $("#ind_tipo_inmueble").attr("disabled","disabled");
                    $("#ind_tipo_inmueble").removeAttr("aria-describedby");
                    $("#ind_tipo_inmueble-error").remove();
                    $("#ind_tipo_inmuebleError").removeClass('has-error');                     
                    $("#fk_a006_num_zona_residencial").attr("disabled","disabled");
                    $("#fk_a006_num_zona_residencial").removeAttr("aria-describedby");
                    $("#fk_a006_num_zona_residencial-error").remove();
                    $("#fk_a006_num_zona_residencialError").removeClass('has-error');                       
                    $("#ind_zona_residencial").attr("disabled","disabled");
                    $("#ind_zona_residencial").removeAttr("aria-describedby");
                    $("#ind_zona_residencial-error").remove();
                    $("#ind_zona_residencialError").removeClass('has-error');                     
                    $("#ind_inmueble").attr("disabled","disabled");
                    $("#ind_inmueble").removeAttr("aria-describedby");
                    $("#ind_inmueble-error").remove();
                    $("#ind_inmuebleError").removeClass('has-error');                     
                    $("#ind_punto_referencia").attr("disabled","disabled");
                    $("#ind_punto_referencia").removeAttr("aria-describedby");
                    $("#ind_punto_referencia-error").remove();
                    $("#ind_punto_referenciaError").removeClass('has-error');                     
                    $("#ind_zona_postal").attr("disabled","disabled");
                    $("#ind_zona_postal").removeAttr("aria-describedby");
                    $("#ind_zona_postal-error").remove();
                    $("#ind_zona_postalError").removeClass('has-error');                      
                    $("#ind_direccion").attr("disabled","disabled");
                    $("#ind_direccion").removeAttr("aria-describedby");
                    $("#ind_direccion-error").remove();
                    $("#ind_direccionError").removeClass('has-error');                      
                    $("#fk_a006_num_miscelaneo_detalle_domicilio").attr("disabled","disabled");                    
                    $("#fk_a006_num_tipo_zona").removeAttr("required");
                    $("#fk_a006_num_ubicacion_inmueble").removeAttr("required");
                    $("#ind_ubicacion_inmueble").removeAttr("required");
                    $("#fk_a006_num_tipo_inmueble").removeAttr("required");
                    $("#ind_tipo_inmueble").removeAttr("disabled","required");
                    $("#fk_a006_num_zona_residencial").removeAttr("required");
                    $("#ind_zona_residencial").removeAttr("required");
                    $("#ind_inmueble").removeAttr("required");
                    $("#ind_punto_referencia").removeAttr("required");
                    $("#ind_zona_postal").removeAttr("required");
                    $("#ind_direccion").removeAttr("required");
                    $("#fk_a006_num_miscelaneo_detalle_domicilio").removeAttr("required");
                    $("#fk_a006_num_miscelaneo_detalle_domicilio").removeAttr("aria-describedby");
                    $("#fk_a006_num_miscelaneo_detalle_domicilio-error").remove();
                    $("#fk_a006_num_miscelaneo_detalle_domicilioError").removeClass('has-error');
                }

            });
        });
        //chekboxes para habilitar campo responsable al seleccionar el checkbox
        $('#no_posee').click(function() {
            if( $('#no_posee').is(':checked') ) {
                $.post('{$_Parametros.url}modRH/gestion/cargaFamiliarCONTROL/GenerarCedulaMET', { empleado: $("#idEmpleado").val() } ,function(dato){
                    $("#cedula").val(dato['cedula']);
                    //$(".form-control").removeAttr("disabled");
                    $("#apellido1").removeAttr("disabled");
                    $("#nombre1").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_detalle_sexo").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_detalle_parentezco").removeAttr("disabled");
                    $("#fec_nacimiento").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_det_gruposangre").removeAttr("disabled");
                    $("#fk_a006_num_miscelaneo_detalle_edocivil").removeAttr("disabled");
                    $("#siguiente").css({ 'visibility':'visible' });
                    $("#btnGuardarCF").removeAttr("disabled");
                    $("#cedula").attr("readonly","readonly");
                },'json');
            }else{                
                $(".form-control").attr("disabled","disabled");
                $("#cedula").removeAttr("disabled");
                $("#cedula").removeAttr("readonly");
                $("#fk_a006_num_miscelaneo_detalle_tipodir").removeAttr("disabled");
                $("#btnGuardarCF").attr("disabled","disabled");;
                $("#cedula").val('');
                $("#cedula").focus();
            }
        });

        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });

        $("#nuevoTelefono").click(function() {
            var idtabla= $("#contenidoTabla");
            var tr= $("#contenidoTabla > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;


            var tipoTelefono='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_tipo_telefono'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_telefono]['+nuevoTr+']" class="form-control input-sm" required data-msg-required="Seleccione Tipo Telefono">'+
                    '<option value="">Seleccione...</option>'+
                    '{foreach item=dat from=$TipoTelf}'+
                    '<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>'+
                    '{/foreach}'+
                    '</select>'+
                    '</div>';
            var telefono='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" value="" name="form[txt][ind_telefono]['+nuevoTr+']" id="ind_telefono'+nuevoTr+'">'+
                    '<label for="ind_telefono'+nuevoTr+'"><i class="md md-insert-comment"></i> Telefono '+numero+'</label>'+
                    '</div>';
            idtabla.append('<tr>'+
                    '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+tipoTelefono+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+telefono+'</div></td>'+
                    '</tr>');
        });

        //AL DAR CLICK EN LA IMAGEN AVATAR DEL FORMULARIO
        $("#cargarImagen").click(function() {

            $("#Foto").click();

        });


        //chekboxes flag_estudia
        $('#num_flag_estudia').click(function() {
            if( $('#num_flag_estudia').is(':checked') ) {
                $("#fk_a006_num_miscelaneo_detalle_gradoinst").removeAttr("disabled");
                $("#fk_a006_num_miscelaneo_detalle_tipoedu").removeAttr("disabled");
                $("#fk_rhc040_num_institucion").removeAttr("disabled");
            }else{
                $("#fk_a006_num_miscelaneo_detalle_gradoinst").attr("disabled","disabled");
                $("#fk_a006_num_miscelaneo_detalle_tipoedu").attr("disabled","disabled");
                $("#fk_rhc040_num_institucion").attr("disabled","disabled");

            }
        });

        //chekboxes flag_trabaja
        $('#num_flag_trabaja').click(function() {
            if( $('#num_flag_trabaja').is(':checked') ) {
                $("#ind_empresa").removeAttr("disabled");
                $("#txt_direccion_empresa").removeAttr("disabled");
                $("#num_sueldo_mensual").removeAttr("disabled");
            }else{
                $("#ind_empresa").attr("disabled","disabled");
                $("#txt_direccion_empresa").attr("disabled","disabled");
                $("#num_sueldo_mensual").attr("disabled","disabled");

            }
        });


        //AL DAR CLICK EN LA IMAGEN AVATAR DEL FORMULARIO
        $("#Foto").change(function(e) {

            var files = e.target.files; // FileList object
            //obtenemos un array con los datos del archivo
            var file = $("#Foto")[0].files[0];
            //obtenemos el nombre del archivo
            var fileName = file.name;
            //obtenemos la extensión del archivo
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);

            if(isImage(fileExtension))
            {
                // Obtenemos la imagen del campo "file".
                for (var i = 0, f; f = files[i]; i++) {
                    //Solo admitimos imágenes.
                    if (!f.type.match('image.*')) {
                        continue;
                    }
                    var reader = new FileReader();
                    reader.onload = (function(theFile) {
                        return function(e) {
                            // Insertamos la imagen
                            $("#cargarImagen").attr("src",e.target.result);
                            $("#cargarImagen").css({ 'width':'140px', 'height':'150px' });
                            //document.getElementById("fotoCargada").innerHTML = ['<img class="thumb" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
                            var data = new FormData($("#formAjax")[0]);
                            data.append('tipo','FOTO');
                            /*$.each($('#Foto')[0].files, function(file) {
                             data.append('imagen', file);
                             });*/


                            $.ajax({
                                url: '{$_Parametros.url}modRH/gestion/empleadosCONTROL/CopiarImagenTmpMET',
                                data: data,
                                type: 'POST',
                                contentType: false,
                                processData: false,
                                success: function(data){
                                    //la asigno al campo de texto
                                    $("#aleatorio").val(data);
                                }
                            });
                        };
                    })(f);
                    reader.readAsDataURL(f);
                }
            }else{

                swal("Error En Imagen!", "Asegurese de subir una imagen tipo: jpg - jpeg - png", "error");

            }


        });



    });
    //comprobamos si el archivo a subir es una imagen
    //para visualizarla una vez haya subido
    function isImage(extension)
    {
        switch(extension.toLowerCase())
        {
            case 'jpg':  case 'png': case 'jpeg':
            return true;
            break;
            default:
                return false;
                break;
        }
    }





</script>