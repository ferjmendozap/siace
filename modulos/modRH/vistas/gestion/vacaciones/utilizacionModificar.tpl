<form action="{$_Parametros.url}modRH/gestion/vacacionesCONTROL/EditarUtilizacionMET" name="formAjax" id="formAjax" method="post"  class="form floating-label" role="form">
	<input type="hidden" value="1" name="valido" />
	<input type="hidden" value="{$periodo.periodo}" name="pk_num_periodo" id="pk_num_periodo" />
	<input type="hidden" value="{$periodo.pk_num_empleado}" id="pk_num_empleado" />
	<input type="hidden" value="{$pk_num_vacacion_utilizacion}" name="pk_num_vacacion_utilizacion" id="pk_num_vacacion_utilizacion" />
	<input type="hidden" id="fecha_termino" name="fecha_termino" />
	<div class="col-md-12 col-sm-12">
		<div class="col-md-6 col-md-6">
			<div class="form-group floating-label">
				<select id="tipo_vacacion" name="tipo_vacacion" class="form-control" disabled>
					{foreach item=tipo from=$tipoVacacion}
						{if $tipo.pk_num_miscelaneo_detalle==$formDB.fk_a006_tipo_vacacion}
							<option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
						{else}
							<option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
						{/if}
					{/foreach}
				</select>
				<label for="tipo_vacacion"><i class="glyphicon glyphicon-triangle-right"></i> Tipo</label>
				<input type="hidden" value="{$formDB.fk_a006_tipo_vacacion}" name="tipo_vacacion_d" />
			</div>
			<div class="form-group">
				<input type="text" name="num_dias" id="num_dias" class="form-control dirty" value="{if isset($formDB.num_dias_utiles)}{$formDB.num_dias_utiles}{/if}" onchange="calcularFechas(this.value)">
				<label class="control-label">Número de Dias</label>
			</div>
		</div>
		<div class="col-md-6 col-md-6">
			<div class="form-group control-width-normal">
				<div class="input-group date" id="demo-date">
					<div class="input-group-content">
						<input type="text" class="fecha form-control dirty" id="fecha_salida" name="fecha_salida" value="{if isset($formDB.fecha_inicio)}{$formDB.fecha_inicio}{/if}" onchange="calcular(this.value)">
						<label>Fecha de Inicio</label>
					</div>
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>
			<div class="form-group control-width-normal">
				<div class="input-group date" id="demo-date">
					<div class="input-group-content">
						<input type="text" class="form-control dirty" id="fechaTermino"  name="fechaTermino" value="{if isset($formDB.fecha_fin)}{$formDB.fecha_fin}{/if}" disabled>
						<label>Fecha Fin</label>
					</div>
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>
		</div>
	</div>
	<div  class="col-md-12 col-sm-12">
		<div align="right">
			<button type="submit" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>&nbsp;
			<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
		</div>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				num_dias:{
					required: true,
					number: true
				},
				tipo_vacacion:{
					required: true
				},
				fecha_salida:{
					required: true
				}
			},
			messages:{
				num_dias:{
					required: "Este campo es requerido",
					number:"Introducir un Número valido"
				},
				tipo_vacacion:{
					required:"Este campo es requerido"
				},
				fecha_salida:{
					required: "Este campo es requerido"
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(respuesta_post3){
					var tabla_listado3 = $('#datatable2').DataTable();
					tabla_listado3.clear().draw();
					var $a=1;
					var $verPeriodo = $("#pk_num_periodo").val();

					if(respuesta_post3 != -1) {
						for(var i=0; i<respuesta_post3.length; i++) {
							var codigoDetalle = respuesta_post3[i].cod_detalle;

							var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descripcion="El usuario ha eliminado una utilizacion" titulo="¿Estás Seguro?" title="Eliminar Utilización" mensaje="¿Estás seguro de eliminar la utilizacion?" boton="si, Eliminar" pk_num_vacacion_utilizacion="'+respuesta_post3[i].pk_num_vacacion_utilizacion+'"><i class="md md-delete" style="color: #ffffff;"></i></button>';
							var botonModificar = '<button class="modificarUtilizacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" title="Editar" descripcion="El Usuario a Modificado una utilización" titulo="Modificar Utilización" pk_num_periodo="'+$verPeriodo+'" pk_num_vacacion_utilizacion="'+respuesta_post3[i].pk_num_vacacion_utilizacion+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button>';


							tabla_listado3.row.add([
								$a,
								respuesta_post3[i].ind_nombre_detalle,
								respuesta_post3[i].num_dias_utiles,
								respuesta_post3[i].fecha_inicio,
								respuesta_post3[i].fecha_fin,
								botonEliminar+" "+botonModificar
							]).draw()
									.nodes()
									.to$()
							$a++;
						}
					}

				},'json');
				var $pkNumEmpleado = $("#pk_num_empleado").val();
				var $url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ListarPeriodoMET';
				$.post($url,{ pkNumEmpleado: $pkNumEmpleado },function(dato){
					var tabla = $('#datatable5').DataTable();
					tabla.clear().draw();
					if(dato != -1) {
						var $i = dato.length;
						for(var i=0; i<dato.length; i++) {
							var anioAnterior = dato[i].num_anio-1;
							if(dato[i].num_pendientes>0){
								var periodo = '<button class="periodo" periodo="'+dato[i].pk_num_periodo+'" style="font-weight:bold;">'+anioAnterior+ ' - ' + dato[i].num_anio+'</button>';
							} else {
								var periodo = '<button class="periodo" periodo="'+dato[i].pk_num_periodo+'">'+anioAnterior+ ' - ' + dato[i].num_anio+'</button>';
							}
							if(dato[i].num_mes<10){
								var num_mes = '0'+dato[i].num_mes;
							} else {
								var num_mes = dato[i].num_mes;
							}
							tabla.row.add([
								$i,
								periodo,
								num_mes,
								dato[i].num_dias_derecho,
								dato[i].num_dias_gozados,
								dato[i].num_dias_interrumpidos,
								dato[i].num_total_utilizados,
								dato[i].num_pendientes,
								''
							]).draw()
									.nodes()
									.to$()

							tabla.order([1,'desc']);

							$i--;
						}
					}
				},'json');
				var $urlPendientes ='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularDiasPendientesMET';
				$.post($urlPendientes,{ pk_num_empleado: $pkNumEmpleado },function(dato){
					$('#diasPendientes').val(dato['diasPendientes']);
				},'json');
				swal('Modificacion Exitosa', "La utilización de vacaciones fue modificada exitosamente", "success");
				$(document.getElementById('cerrarModal2')).click();
			}
		});
	});

	$("#fecha_salida").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});

	function calcularFechas(dias)
	{
		var fecha_salida = $("#fecha_salida").val();
		var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularFechasMET';
		$.post(url,{ dias: dias, fecha_salida: fecha_salida },function(dato){
			$("#fecha_termino").val(dato['fechaTermino']);
			$("#fechaTermino").val(dato['fechaTermino']);
		},'json');
	}

	function calcular(fecha_salida)
	{
		var dias = $("#num_dias").val();
		var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularFechasMET';
		$.post(url,{ dias: dias, fecha_salida: fecha_salida },function(dato){
			$("#fecha_termino").val(dato['fechaTermino']);
			$("#fechaTermino").val(dato['fechaTermino']);
		},'json');
	}

</script>