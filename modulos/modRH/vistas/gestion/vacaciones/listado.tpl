<br/>
<input type="hidden" id="estado" value="{$emp.ind_estado}" />
<section class="style-default-bright">
    <h2 class="text-primary">&nbsp;Listado de Vacaciones
        {if $emp.ind_estado=='AD'}: Administrador{/if}
        {if $emp.ind_estado=='PR'}: Revisar{/if}
        {if $emp.ind_estado=='RE'}: Conformar{/if}
        {if $emp.ind_estado=='CO'}: Aprobar{/if}
    </h2>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div align="right">
                    <div class="btn-group"><a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a></div>&nbsp;
                    <hr class="ruler-xl">
                </div>
                <br/>
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr  align="center">
                        <th><i class="glyphicon glyphicon-th"></i> N° de Solicitud</th>
                        <th><i class="md-person"></i> Empleado</th>
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Tipo</th>
                        <th><i class="glyphicon glyphicon-calendar"></i> Fecha de Solicitud</th>
                        <th><i class="glyphicon glyphicon-cog"></i> Estado</th>
                        <th >Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=periodo from=$periodoVacacion}
                        <tr id="pk_num_solicitud_vacacion{$periodo.pk_num_solicitud_vacacion}">
                            <td>{$periodo.pk_num_solicitud_vacacion}</td>
                            <td>{$periodo.ind_nombre1} {$periodo.ind_nombre2} {$periodo.ind_apellido1} {$periodo.ind_apellido2}</td>
                            <td>{$periodo.ind_nombre_detalle}</td>
                            <td>{$periodo.fec_solicitud|date_format:"%d/%m/%Y"}</td>
                            <td>
                                {if $periodo.ind_estado=='PR'}PREPARADO{/if}
                                {if $periodo.ind_estado=='RE'}REVISADO{/if}
                                {if $periodo.ind_estado=='CO'}CONFORMADO{/if}
                                {if $periodo.ind_estado=='AP'}APROBADO{/if}
                                {if $periodo.ind_estado=='AN'}ANULADO{/if}
                            </td>
                            <td align="center">
                                {if in_array('RH-01-01-07-04-V',$_Parametros.perfil) }
                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" accion="ver" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud_vacacion="{$periodo.pk_num_solicitud_vacacion}" title="Ver solicitud de vacaciones" titulo="Ver solicitud de vacaciones">
                                        <i class="icm icm-eye2"></i>
                                    </button>
                                {/if}
                                {if in_array('RH-01-01-07-04-02-RE',$_Parametros.perfil) && $emp.ind_estado=='PR'}
                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" accion="revisar" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud_vacacion="{$periodo.pk_num_solicitud_vacacion}" title="Revisar solicitud de vacaciones" titulo="Revisar solicitud de vacaciones">
                                        <i class="icm icm-rating"></i>
                                    </button>
                                {/if}
                                {if in_array('RH-01-01-07-04-03-CO',$_Parametros.perfil) && $emp.ind_estado=='RE'}
                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" accion="conformar" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud_vacacion="{$periodo.pk_num_solicitud_vacacion}" title="Conformar solicitud de vacaciones" titulo="Conformar solicitud de vacaciones">
                                        <i class="icm icm-rating2"></i>
                                    </button>
                                {/if}
                                {if in_array('RH-01-01-07-04-04-AP',$_Parametros.perfil) && $emp.ind_estado=='CO'}
                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" accion="aprobar" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud_vacacion="{$periodo.pk_num_solicitud_vacacion}" title="Aprobar solicitud de vacaciones" titulo="Aprobar solicitud de vacaciones">
                                        <i class="icm icm-rating3"></i>
                                    </button>
                                {/if}

                                {if in_array('RH-01-01-07-05-E',$_Parametros.perfil) && $emp.ind_estado == 'AD' && $periodo.ind_estado == 'PR'}
                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud_vacacion="{$periodo.pk_num_solicitud_vacacion}" descipcion="El usuario ha modificado una solicitud de vacaciones" title="Modificar solicitud de vacaciones" titulo="Modificar solicitud de vacaciones">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                {/if}

                                {if in_array('RH-01-01-07-06-DV',$_Parametros.perfil) && $periodo.cod_detalle==1 && $periodo.ind_estado == 'AP'}
                                    <button class="vacacion logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal" pk_num_solicitud_vacacion="{$periodo.pk_num_solicitud_vacacion}" data-keyboard="false" data-backdrop="static" title="Ver Solicitud de Vacación" titulo="Ver Solicitud de Vacación">
                                        <i class="md md-attach-file"></i>
                                    </button>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="6">
                            {if in_array('RH-01-01-07-01-N',$_Parametros.perfil)}
                                <button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado una solicitud de vacaciones" data-toggle="modal" data-target="#formModal" titulo="Registrar nueva solicitud de vacaciones" id="nuevo"> <span class="glyphicon glyphicon-log-out"></span> Nueva Solicitud</button>&nbsp;
                            {/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- filtro -->
<div class="offcanvas">
    <form id="formulario" class="form floating-label" role="form" method="post" novalidate>
        <div id="offcanvas-filtro" class="offcanvas-pane width-12">
            <div class="offcanvas-head">
                <header>Filtro de Búsqueda</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                </div>
            </div>
            <div class="offcanvas-body">
                {if in_array('RH-01-01-07-08-BG',$_Parametros.perfil) && $emp.ind_estado=='AD'}
                    <div class="form-group floating-label">
                        <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencias(this.value)">
                            {foreach item=org from=$listadoOrganismo}
                                {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                    <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                                {else}
                                    <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                    </div>
                    <div id="dependencia">
                        <div class="form-group floating-label">
                            <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty" onchange="cargarEmpleados(this.value)">
                                <option value="">&nbsp;</option>
                                {foreach item=dep from=$listadoDependencia}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/foreach}
                            </select>
                            <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                        </div>
                    </div>
                    <div id="empleado">
                        <div class="form-group floating-label">
                            <select id="pk_num_empleado" name="pk_num_empleado" id="pk_num_empleado" class="form-control dirty">
                                <option value="">&nbsp;</option>
                            </select>
                            <label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
                        </div>
                    </div>
                {else}
                    <div class="form-group floating-label">
                        <select id="pk_num_organismo" name="pk_num_organismo" id="pk_num_organismo" class="form-control dirty">
                            <option value="{$busqueda.pk_num_organismo}" selected>{$busqueda.ind_descripcion_empresa}</option>
                        </select>
                        <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                    </div>
                    <div class="form-group floating-label">
                        <select id="pk_num_dependencia" name="pk_num_dependencia" id="pk_num_empleado" class="form-control dirty">
                            <option value="{$busqueda.pk_num_dependencia}" selected>{$busqueda.ind_dependencia}</option>
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                    </div>
                    <div class="form-group floating-label">
                        <select id="pk_num_empleado" name="pk_num_empleado" class="form-control dirty">
                            <option value="{$busqueda.pk_num_empleado}">{$busqueda.empleado}</option>
                        </select>
                        <label for="pk_num_empleado_aprueba"><i class="glyphicon glyphicon-user"></i> Empleado</label>
                    </div>
                {/if}
                <div class="form-group">
                    <input type="text" name="pk_num_solicitud_vacacion" id="pk_num_solicitud_vacacion" class="form-control dirty">
                    <label class="control-label"><i class="glyphicon glyphicon-th"></i> N° de Solicitud</label>
                </div>
                <div class="form-group" style="width:100%">
                    <div class="input-daterange input-group" id="demo-date-range">
                        <div class="input-group-content">
                            <input type="text" class="fecha form-control dirty" name="fechaInicio" id="fecha_inicio">
                            <label><i class="glyphicon glyphicon-calendar"></i> Fecha de Registro</label>
                        </div>
                        <span class="input-group-addon">a</span>
                        <div class="input-group-content">
                            <input type="text" class="fecha form-control dirty" name="fechaFin" id="fecha_fin">
                            <div class="form-control-line"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group floating-label">
                    <select id="ind_estado" name="ind_estado" class="form-control dirty">
                        {if $emp.ind_estado=='AD'}
                            <option value=""></option>
                            <option value="PR">Preparado</option>
                            <option value="RE">Revisado</option>
                            <option value="CO">Conformado</option>
                            <option value="AP">Aprobado</option>
                            <option value="AN">Anulado</option>
                        {else}
                            {if $emp.ind_estado=='PR'}<option selected value="PR">Preparado</option>{/if}
                            {if $emp.ind_estado=='RE'}<option selected value="RE">Revisado</option>{/if}
                            {if $emp.ind_estado=='CO'}<option selected value="CO">Conformado</option>{/if}
                            {if $emp.ind_estado=='AP'}<option selected value="AP">Aprobado</option>{/if}
                        {/if}
                    </select>
                    <label for="ind_estado"><i class="glyphicon glyphicon-user"></i> Estado</label>
                </div>
                <br/>
                <div class="col-sm-12 col-sm-12" align="center">
                        <div class="col-sm-6 col-sm-6">
                            {if in_array('RH-01-01-07-09-RG',$_Parametros.perfil)}
                                <button type="button" id="buscarReporte" titulo="Reporte de Vacaciones" data-toggle="modal" data-target="#formModal" class="btn ink-reaction btn-flat btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Generando...">Generar Reporte<div style="top: 27px; left: 65px;" class="ink inverse"></div></button>
                            {/if}
                        </div>
                    <div class="col-sm-6 col-sm-6">
                        <button type="submit" id="formulario" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
                    </div>
                </div>
            </div>
    </form>
</div>
<script type="text/javascript">
    $(".fecha").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });

    var $url='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/NuevaSolicitudMET';
    $('#nuevo').click(function(){
        $('#modalAncho').css( "width", "95%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        $.post($url,'',function($dato){
            $('#ContenidoModal').html($dato);
        });
    });


    $('#datatable1 tbody').on( 'click', '.ver', function () {
        var $urlVer='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/VerVacacionMET';
        var estado = $("#estado").val();
        var accion = $(this).attr('accion');
        $('#modalAncho').css( "width", "75%" );
        $('#formModalLabel').html($(this).attr('title'));
        $('#ContenidoModal').html("");
        $.post($urlVer,{ pk_num_solicitud_vacacion: $(this).attr('pk_num_solicitud_vacacion'), estado: estado, accion: accion},function($dato){
            $('#ContenidoModal').html($dato);
        });
    });

    $('#datatable1 tbody').on( 'click', '.vacacion', function () {
        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        var pk_num_solicitud_vacacion = $(this).attr('pk_num_solicitud_vacacion');
        var urlReporte = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ReporteVacacionMET/?pk_num_solicitud_vacacion='+pk_num_solicitud_vacacion;
        $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
    });

    var $url_modificar='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/EditarVacacionMET';
     $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#modalAncho').css( "width", "85%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url_modificar,{ pk_num_solicitud_vacacion: $(this).attr('pk_num_solicitud_vacacion')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

    function cargarDependencias(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/gestion/vacacionesCONTROL/BuscarDependenciasMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function cargarEmpleados(pk_num_dependencia) {
        $("#empleado").html("");
        $.post("{$_Parametros.url}modRH/gestion/vacacionesCONTROL/BuscarEmpleadosMET",{ pk_num_dependencia:""+pk_num_dependencia }, function (dato) {
            $("#empleado").html(dato);
        });
    }

    $("#formulario").validate({
        submitHandler: function(form) {
            var fecha_inicio = $("#fecha_inicio").val();
            var fecha_fin = $("#fecha_fin").val();
            var pk_num_empleado = $("#pk_num_empleado").val();
            var pk_num_solicitud_vacacion = $("#pk_num_solicitud_vacacion").val();
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var ind_estado = $("#ind_estado").val();
            var url_listar='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/BuscarVacacionMET';
            $.post(url_listar,{ fecha_inicio: fecha_inicio, fecha_fin: fecha_fin, pk_num_empleado: pk_num_empleado, pk_num_solicitud_vacacion: pk_num_solicitud_vacacion, pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, ind_estado: ind_estado},function(respuesta_post) {
                var tabla_listado = $('#datatable1').DataTable();
                tabla_listado.clear().draw();
                if(respuesta_post != -1) {
                    for(var i=0; i<respuesta_post.length; i++) {
                        var url = "{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ReporteVacacionMET/?pk_num_solicitud_vacacion="+respuesta_post[i].pk_num_solicitud_vacacion;
                        if(respuesta_post[i].ind_estado=='PR'){
                            var botonEditar = ' {if in_array('RH-01-01-07-05-E',$_Parametros.perfil)}<button type="button" pk_num_solicitud_vacacion = "' + respuesta_post[i].pk_num_solicitud_vacacion + '" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"  descripcion="El usuario ha modificado una solicitud de vacacion" title="Modificar solicitud de vacacion"  titulo="Modificar solicitud de vacacion"><i class="fa fa-edit"></i></button>'{/if};
                        } else {
                            var botonEditar = ' ';
                        }
                        if(respuesta_post[i].ind_estado=='PR'){
                            var estado = 'PREPARADO';
                        }
                        if(respuesta_post[i].ind_estado=='RE'){
                            var estado = 'REVISADO';
                        }
                        if(respuesta_post[i].ind_estado=='CO'){
                            var estado = 'CONFORMADO';
                        }
                        if(respuesta_post[i].ind_estado=='AP'){
                            var estado = 'APROBADO';
                        }
                        if(respuesta_post[i].ind_estado=='AN'){
                            var estado = 'ANULADO';
                        }
                        var fila = tabla_listado.row.add([
                            respuesta_post[i].pk_num_solicitud_vacacion,
                            respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_nombre2+' '+ respuesta_post[i].ind_apellido1+' '+ respuesta_post[i].ind_apellido2,
                            respuesta_post[i].ind_nombre_detalle,
                            respuesta_post[i].fecha_solicitud,
                            estado,
                            '{if in_array('RH-01-01-07-04-V',$_Parametros.perfil)}<button type="button" pk_num_solicitud_vacacion = "' + respuesta_post[i].pk_num_solicitud_vacacion + '" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" descripcion="El usuario ha visualizado una solicitud de vacaciones" title="Ver solicitud de vacaciones" titulo="Ver solicitud de vacaciones"><i class="glyphicon glyphicon-search"></i></button>{/if}',
                            botonEditar,
                            '{if in_array('RH-01-01-07-06-DV',$_Parametros.perfil)}<a href="'+ url +'" target="_blank"><button type="button" class="verRegistro logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El Usuario ha visualizado la solicitud de vacaciones en pdf" title="Solicitud de vacaciones" titulo="Solicitud de vacaciones"><i class="md md-attach-file"></i></button></a>{/if}'
                        ]).draw()
                                .nodes()
                                .to$()
                        $(fila)
                                .attr('id','pk_num_solicitud_vacacion'+respuesta_post[i].pk_num_solicitud_vacacion+'');
                    }
                }

            },'json');
        }
    });

    $(document).ready(function(){
        $("#buscarReporte").live('click', function() {
            var pk_num_solicitud_vacacion = $("#pk_num_solicitud_vacacion").val();
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var pk_num_empleado = $("#pk_num_empleado").val();
            var fecha_inicio = $("#fecha_inicio").val();
            var fecha_fin = $("#fecha_fin").val();
            var ind_estado = $("#ind_estado").val();
            $('#modalAncho').css( "width", "85%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var urlBuscar = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/BuscarReporteMET/?pk_num_organismo='+pk_num_organismo+'&pk_num_solicitud_vacacion='+pk_num_solicitud_vacacion+'&pk_num_dependencia='+pk_num_dependencia+'&ind_estado='+ind_estado+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin+'&pk_num_empleado='+pk_num_empleado;
            $('#ContenidoModal').html('<iframe src="'+urlBuscar+'" width="100%" height="950"></iframe>');
            return false;
        });
    });


</script>
