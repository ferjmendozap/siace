
{if $dias.numEstatus==0}
	<br/>
	<div class="col-lg-12">
		<br/>
		<div class="card card-underline">
			<div class="card-head">
				<div class="col-md-12 col-sm-12">
					<br/>
					<div class="alert alert-info">
						<strong>Información:</strong> Su estado como empleado es inactivo. No se puede generar una solicitud de vacaciones.
					</div>
					<br/>
				</div>
			</div>
		</div>
	</div>
{else}
	<form id="formAjax" action="{$_Parametros.url}modRH/gestion/vacacionesCONTROL/NuevaSolicitudMET" class="form" novalidate="novalidate">
		<input type="hidden" name="valido" value="1" />
		<input type="hidden" id="num_pendiente" value="{$dias.diasPendientes}"/>
		<input type="hidden" id="fechaTermino" name="fechaTermino" value="{$dias.fechaTermino}"/>
		<input type="hidden" id="fechaIncorporacion" name="fechaIncorporacion" value="{$dias.fechaIncorporacion}"/>
		<br/>
		<div class="col-lg-12">
			<div class="card card-underline">
				<div class="card-head">
					<ul class="nav nav-tabs pull-right" data-toggle="tabs">
						<li class="active"><a href="#first2">Información General</a></li>
						<li><a href="#second2">Periodos Disponibles</a></li>
					</ul>
					<header><h2 class="text-primary-dark">Solicitud de Vacaciones</h2></header>
				</div>
				<div class="card-body tab-content">
					<div class="tab-pane active" id="first2">
						{if $dias.estado=='inactivo'}
							<div class="col-md-12 col-sm-12">
								<div class="alert alert-info">
									<strong>Información:</strong> Los dias de disfrute para su tiempo de servicio no se encuentran registrados en la tabla de vacaciones.
								</div>
							</div>
						{/if}
						{if $comprobar.datoComprobar==1}
							<div class="col-md-12 col-sm-12" id="comprobar">
								<div class="alert alert-info">
									<strong>Información:</strong> El funcionario ya posee una solicitud de vacaciones pendiente
								</div>
							</div>
						{else}
							<div class="col-md-12 col-sm-12" id="comprobar">
							</div>
						{/if}
						<h3 class="text-primary">Datos del Empleado</h3>
						{if in_array('RH-01-01-07-08-BG',$_Parametros.perfil)}
							<div class="col-md-12 col-sm-12">
								<div class="col-md-6 col-sm-6">
									<div class="form-group ">
										<select name="pk_num_organismo" id="pk_num_organismo" id="s2id_single" class="select2-container form-control select2 dirty" onchange="cargarDependencia(this.value)">
											<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
												{foreach item=organismo from=$listarOrganismo}
													{if $organismo.pk_num_organismo==$emp.pk_num_organismo}
														<option value="{$organismo.pk_num_organismo}" selected>{$organismo.ind_descripcion_empresa}</option>
													{else}
														<option value="{$organismo.pk_num_organismo}">{$organismo.ind_descripcion_empresa}</option>
													{/if}
												{/foreach}
										</select>
										<label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div id="empleado">
										<div class="form-group ">
											<select name="pk_num_empleado" id="pk_num_empleado" id="s2id_single" class="select2-container form-control select2 dirty" onchange="obtenerDatos(this.value)">
												<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
													{foreach item=empleado from=$listarEmpleado}
														{if $empleado.pk_num_empleado==$emp.pk_num_empleado}
															<option selected value="{$empleado.pk_num_empleado}">{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</option>
														{else}
															<option value="{$empleado.pk_num_empleado}">{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</option>
														{/if}
													{/foreach}
											</select>
											<label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12">
								<div class="col-md-6 col-sm-6">
									<div id="dependencia">
										<div class="form-group ">
											<select name="pk_num_dependencia" id="pk_num_dependenciaF" class="select2-container form-control select2 dirty" onchange="cargarEmpleado(this.value)">
												<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
													{foreach item=dependencia from=$listarDependencia}
														{if $dependencia.pk_num_dependencia==$emp.pk_num_dependencia}
															<option selected value="{$dependencia.pk_num_dependencia}">{$dependencia.ind_dependencia}</option>
														{else}
															<option value="{$dependencia.pk_num_dependencia}">{$dependencia.ind_dependencia}</option>
														{/if}
													{/foreach}
											</select>
											<label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="panel panel-default">
										<div class="panel-body">
											<div id="pendiente"><h4>Total de dias pendientes: {$dias.diasPendientes}</h4></div>
										</div>
									</div>
								</div>
							</div>
						{else}
							<div class="col-md-12 col-sm-12">
								<div class="col-md-6 col-sm-6">
									<div class="form-group ">
										<select name="pk_num_organismo" id="pk_num_organismo" class="form-control dirty">
											<option value="{$emp.pk_num_organismo}" selected>{$emp.ind_descripcion_empresa}</option>
										</select>
										<label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="form-group ">
										<select name="pk_num_empleado" id="pk_num_empleado" class="form-control dirty" >
											<option value="{$emp.pk_num_empleado}">{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}</option>
										</select>
										<label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-sm-12">
								<div class="col-md-6 col-sm-6">
									<div class="form-group ">
										<select name="pk_num_dependencia" id="pk_num_dependencia" class="form-control dirty">
											<option value="{$emp.pk_num_dependencia}" selected>{$emp.ind_dependencia}</option>
										</select>
										<label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
									<div class="panel panel-default">
										<div class="panel-body">
											<h4>Total de dias pendientes: <div id="pendiente">{$dias.diasPendientes}</div></h4>
										</div>
									</div>
								</div>
							</div>
						{/if}
						<h3 class="text-primary">Información de la Solicitud</h3>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control" disabled>
									<label class="control-label"><i class="glyphicon glyphicon-th"></i> N° de Solicitud</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control dirty" id="cedula" disabled value="{$emp.cedula}">
									<label class="control-label"><i class="glyphicon glyphicon-th"></i> Documento</label>
								</div>
								<div class="form-group ">
									<select id="tipo_vacacion" name="tipo_vacacion" class="form-control dirty">
										{foreach item=tipo from=$tipoVacacion}
											{if $tipo.ind_nombre_detalle=='GOCE'}
												<option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
											{else}
												<option disabled>{$tipo.ind_nombre_detalle}</option>
											{/if}
										{/foreach}
									</select>
									<label for="tipo_vacacion"><i class="glyphicon glyphicon-triangle-right"></i> Tipo</label>
								</div>
								<div class="form-group">
									<textarea class="form-control dirty" rows="4" name="ind_motivo" id="ind_motivo"></textarea>
									<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Justificación</label>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control" disabled>
									<label class="control-label">N° de Otorgamiento</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control dirty" disabled value="Preparado">
									<label class="control-label">Estado</label>
								</div>
								<div class="form-group">
									<input type="text" disabled class="form-control dirty" value="{$smarty.now|date_format:"%Y-%m"}">
									<label class="control-label">Periodo</label>
								</div>
								<div class="form-group">
									<input type="text" disabled class="form-control dirty" value="{$smarty.now|date_format:"%d/%m/%Y"}">
									<label class="control-label">Fecha</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control dirty" disabled value="{$emp.ind_nombre1} {$emp.ind_apellido1}">
									<label class="control-label">Creado por</label>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">

							<div class="well clearfix">
								<div class="col-md-2 col-sm-2">
							<div class="form-group ">
								<select id="periodo_vacacion" name="periodo_vacacion" class="form-control dirty">
                                    {foreach item=vp from=$periodo}
										{if $vp.num_pendientes>0}
											<option value="{$vp.num_pendientes}">{$vp.num_anio-1}-{$vp.num_anio} ({$vp.num_pendientes})</option>
										{/if}
                                    {/foreach}
								</select>
								<label for="periodo_vacacion"><i class="glyphicon glyphicon-triangle-right"></i> Detalle</label>
							</div>
								</div>
								<div class="col-md-1 col-sm-1">
									<div class="form-group">
										<input type="text" name="num_dias" id="num_dias" class="form-control dirty" onchange="calcularFechas(this.value)" value="{$dias.diasPendientes}">
										<label class="control-label">Nro Dias</label>
									</div>
								</div>

								<div class="col-md-3 col-sm-3">
									<div class="form-group control-width-normal">
										<div class="input-group date" id="demo-date">
											<div class="input-group-content">
												<input type="text" class="form-control dirty" id="fecha_salida" name="fecha_salida" value="{$smarty.now|date_format:"%d/%m/%Y"}" onchange="calcular(this.value)">
												<label>Fecha de Salida</label>
											</div>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>

								<div class="col-md-3 col-sm-3">
									<div class="form-group control-width-normal">
										<div class="input-group date" id="demo-date">
											<div class="input-group-content">
												<input type="text" class="form-control dirty" id="fecha_termino" name="fecha_termino" value="{$dias.fechaTermino}" disabled>
												<label>Fecha de Término</label>
											</div>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
								<div class="col-md-3 col-sm-3">
									<div class="form-group control-width-normal">
										<div class="input-group date" id="demo-date">
											<div class="input-group-content">
												<input type="text" class="form-control dirty" id="fecha_incorporacion"  name="fecha_incorporacion" value="{$dias.fechaIncorporacion}" disabled>
												<label>Fecha de Incorporación</label>
											</div>
											<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						{if $dias.estado=='activo'}
							{if $comprobar.datoComprobar==1}
								<div class="col-md-12 col-sm-12" id="botonGuardar">

								</div>
							{else}
								<div class="col-md-12 col-sm-12" id="botonGuardar">
									<div align="center">
										<button type="submit" class="btn btn-primary ink-reaction btn-raised"> Enviar Solicitud</button>
									</div>
								</div>
							{/if}
						{/if}
					</div>
	</form>
	<div class="tab-pane" id="second2">
		<table id="datatable2" class="table table-striped table-hover">
			<thead>
			<tr align="center">
				<th><i class="glyphicon glyphicon-th"></i> N°</th>
				<th><i class="md-person"></i> Periodo</th>
				<th><i class="md md-brightness-low"></i> Dias</th>
				<th><i class="glyphicon glyphicon-calendar"></i> Fecha Inicial</th>
				<th><i class="glyphicon glyphicon-calendar"></i> Fecha Final</th>
				<th><i class="md md-brightness-low"></i> Dias por Derecho</th>
				<th><i class="md md-brightness-low"></i> Dias Usados</th>
				<th><i class="md md-brightness-low"></i> Dias Pendientes</th>
				<th><i class="glyphicon glyphicon-cog"></i> Solicitudes</th>
			</tr>
			</thead>
			<tbody>
				{assign var=number value=1}
				{assign var=num value=1}
				{assign var=numero value=1}
				{assign var=acumuladorPendiente value=0}
				{foreach item=periodo from=$periodoVacacion}
					<tr id="pk_num_periodo{$periodo.pk_num_periodo}">
						<td align="center">{$number++}</td>
						<td>{$periodo.num_anio-1} - {$periodo.num_anio}</td>
						<td align="center">{$periodo.num_pendientes}</td>
						<td>{$fechaInicial[$num++]|date_format:"%d/%m/%Y"}</td>
						<td>{$fechaFinal[$numero++]|date_format:"%d/%m/%Y"}</td>
						<td align="center">{$periodo.num_dias_derecho}</td>
						<td align="center">{$periodo.num_dias_gozados}</td>
						<td align="center">{$periodo.num_pendientes}</td>
						<td align="center"><button type="button" class="periodoSolicitud logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" title="Solicitudes de Vacaciones" titulo="Solicitudes de Vacaciones"  pk_num_periodo="{$periodo.pk_num_periodo}"><i class="glyphicon glyphicon-search"></i></button></td>
					</tr>
					{$acumuladorPendiente = $acumuladorPendiente + $periodo.num_pendientes}
				{/foreach}
			</tbody>
			<tfoot>
				<tr>
					<td></td>
					<td></td>
					<td align="center"><b>{$acumuladorPendiente}</b></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td align="center"><b>{$acumuladorPendiente}</b></td>
					<td></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

{/if}
<script type="text/javascript">
	$("#fecha_salida").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});
    $("#periodo_vacacion").on('change',function () {
        $('#num_dias').val($(this).val());
        calcularFechas($(this).val());
    });

	$('#periodoTotal').multiSelect({ selectableOptgroup: true });

	var placeholder = "";

	$( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
	$( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult( repo ) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if ( repo.description ) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}

	function calcularFechas(dias)
	{
		var fecha_salida = $("#fecha_salida").val();
		var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularFechasMET';
		$.post(url,{ dias: dias, fecha_salida: fecha_salida },function(dato){
//			$("#num_dias").val(dato['periodo']);
			$("#fecha_termino").val(dato['fechaTermino']);
			$("#fecha_incorporacion").val(dato['fechaIncorporacion']);
			$("#fechaTermino").val(dato['fechaTermino']);
			$("#fechaIncorporacion").val(dato['fechaIncorporacion']);
		},'json');
		var pk_num_empleado = $("#pk_num_empleado").val();
		var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ComprobarDiasMET';
//		$.post(url,{ pkNumEmpleado: pk_num_empleado},function(dato){
			if(parseInt($("#periodo_vacacion").val()) < parseInt(dias)){
                $("#num_dias").val($("#periodo_vacacion").val());
                calcularFechas($("#periodo_vacacion").val());
				swal("No dispone de la cantidad de dias solicitados");
			}
//		},'json');
		// Actualizar Periodo
		var urlPeriodoSolicitud = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CargarPeriodosMET';
		var pk_num_empleado = $("#pk_num_empleado").val();
		$.post(urlPeriodoSolicitud,{ periodo: dias, fecha_salida: fecha_salida, metodo: 2, pk_num_empleado: pk_num_empleado },function(respuesta_post){
			var tabla_listado = $('#datatable2').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					tabla_listado.row.add([
						respuesta_post[i].numero,
						respuesta_post[i].anioAnterior+ '-' + respuesta_post[i].anio,
						respuesta_post[i].dias,
						respuesta_post[i].fecha_salida,
						respuesta_post[i].fecha_termino,
						respuesta_post[i].dias_derecho,
						respuesta_post[i].dias_usados,
						respuesta_post[i].dias_pendientes,
						'<button type="button" pk_num_periodo = "' + respuesta_post[i].pkNumPeriodo + '" class="periodo logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" descripcion="El usuario ha visualizado una solicitud de vacaciones" title="Solicitudes de Vacaciones" titulo="Solicitudes de Vacaciones"><i class="glyphicon glyphicon-search"></i></button>'
					]).draw()
							.nodes()
							.to$()
				}
			}
		},'json');
	}

	function calcular(fecha_salida)
	{
		var dias = $("#num_dias").val();
		if(dias>0){
			var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularFechasMET';
			$.post(url,{ dias: dias, fecha_salida: fecha_salida },function(dato){
				$("#num_dias").val(dato['periodo']);
				$("#fecha_termino").val(dato['fechaTermino']);
				$("#fecha_incorporacion").val(dato['fechaIncorporacion']);
				$("#fechaTermino").val(dato['fechaTermino']);
				$("#fechaIncorporacion").val(dato['fechaIncorporacion']);
			},'json');
			// Actualizar Periodo
			var urlPeriodoSolicitud = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CargarPeriodosMET';
			var pk_num_empleado = $("#pk_num_empleado").val();
			var pendientes = 0;
			$.post(urlPeriodoSolicitud,{ periodo: dias, fecha_salida: fecha_salida, metodo: 2, pk_num_empleado: pk_num_empleado },function(respuesta_post){
				var tabla_listado = $('#datatable2').DataTable();
				tabla_listado.clear().draw();
				if(respuesta_post != -1) {
					for(var i=0; i<respuesta_post.length; i++) {
						dias = dias + respuesta_post[i].dias;
						pendientes = pendientes + respuesta_post[i].dias_pendientes;
						tabla_listado.row.add([
							respuesta_post[i].numero,
							respuesta_post[i].anioAnterior+ '-' + respuesta_post[i].anio,
							respuesta_post[i].dias,
							respuesta_post[i].fecha_salida,
							respuesta_post[i].fecha_termino,
							respuesta_post[i].dias_derecho,
							respuesta_post[i].dias_usados,
							respuesta_post[i].dias_pendientes,
							'<button type="button" pk_num_periodo = "' + respuesta_post[i].pkNumPeriodo + '" class="periodo logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" descripcion="El usuario ha visualizado una solicitud de vacaciones" title="Solicitudes de Vacaciones" titulo="Solicitudes de Vacaciones"><i class="glyphicon glyphicon-search"></i></button>'
						]).draw()
								.nodes()
								.to$()

					}
				}
			},'json');
		}
	}

	var $urlPeriodo='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/SolicitudesPeriodoMET';
	$('#datatable2 tbody').on( 'click', '.periodoSolicitud', function () {
		$('#modalAncho2').css( "width", "80%" );
		$('#formModalLabel2').html($(this).attr('titulo'));
		$('#ContenidoModal2').html("");
		$.post($urlPeriodo,{ pk_num_periodo: $(this).attr('pk_num_periodo')},function($dato){
			$('#ContenidoModal2').html($dato);
		});
	});

	$(document).ready(function () {
		//validation rules
		$('#num_dias').val($("#periodo_vacacion").val());
		$("#formAjax").validate({
			rules:{
				fecha_salida:{
					required:true
				},
				num_dias:{
					required:true,
				}
			},
			messages:{
				fecha_salida:{
					required: "Indique la fecha de salida"
				},
				num_dias:{
					required: "Ingrese dias a solicitar"
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
                    $(document.getElementById('datatable1')).append('<tr id="pk_num_solicitud_vacacion'+dato['pk_num_solicitud_vacacion']+'">' +
                        '<td>'+dato['pk_num_solicitud_vacacion']+'</td>'+
                        '<td>'+dato['nombreCompleto']+'</td>'+
                        '<td>'+dato['ind_nombre_detalle']+'</td>'+
                        '<td>'+dato['fecha_solicitud']+'</td>'+
                        '<td>PREPARADO</td>'+
                        '<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" title="Ver solicitud de vacaciones" titulo="Ver solicitud de vacaciones" pk_num_solicitud_vacacion="'+dato['pk_num_solicitud_vacacion']+'"><i class="glyphicon glyphicon-search"></i></button></td>'+
                        '<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descipcion="El usuario ha modificado una solicitud de vacaciones" title="Modificar solicitud de vacaciones" titulo="Modificar solicitud de vacaciones" pk_num_solicitud_vacacion="'+dato['pk_num_solicitud_vacacion']+'"><i class="fa fa-edit"></i></button></td>'+
                        '<td align="center"><button class="vacacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" title="Ver Solicitud de Vacación" titulo="Ver Solicitud de Vacación" pk_num_solicitud_vacacion="'+dato['pk_num_solicitud_vacacion']+'"><i class="md md-attach-file"></i></button></td>'+
                        '</tr>');
					swal("Solicitud Guardada", "Solicitud de vacaciones guardada satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});

	function cargarDependencia(pk_num_organismo) {
		$("#dependencia").html("");
		$.post("{$_Parametros.url}modRH/gestion/vacacionesCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
			$("#dependencia").html(dato);
		});

	}

	function cargarEmpleado(pk_num_dependencia) {
		$("#empleado").html("");
		$.post("{$_Parametros.url}modRH/gestion/vacacionesCONTROL/BuscarEmpleadoMET",{ pk_num_dependencia:""+pk_num_dependencia }, function (dato) {
			$("#empleado").html(dato);
		});
		$.post("{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ObtenerEmpleadoMET",{ pk_num_dependencia:""+pk_num_dependencia }, function (datos) {
			var pk_num_empleado = datos['pk_num_empleado'];
			obtenerDatos(pk_num_empleado);
		}, 'json');
	}

	function obtenerDatos(pk_num_empleado)
	{
		
		var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ObtenerDatosMET';
		$.post(url,{ pk_num_empleado: pk_num_empleado },function(dato){
		    /****/
			$("#pendiente").html('<h4>Total de dias pendientes: '+dato['diasPendientes']+'</h4>');
			$("#cedula").val(dato['cedula']);
			 //("#num_dias").val(dato['diasPendientes']); // COMENTADO PORQUE ACTUALIZA LOS DIAS PENDIENTES Y NO ES NECESARIO
			/*****/
			$("#fecha_termino").val(dato['fechaTermino']);
			$("#fecha_incorporacion").val(dato['fechaIncorporacion']);
			$("#pk_num_dependenciaF").val(dato['id_dependencia']);
			$('#s2id_pk_num_dependenciaF .select2-chosen').html(dato['dependencia']);
			var datoComprobar = dato['datoComprobar'];
			if(datoComprobar==1){
				$("#comprobar").html('<div class="alert alert-info"><strong>Información:</strong> El funcionario ya posee una solicitud de vacaciones pendiente</div>');
				$("#botonGuardar").html('');
			} else {
				$("#comprobar").html("");
				$("#botonGuardar").html('<div align="center"><button type="submit" class="btn btn-primary ink-reaction btn-raised"> Enviar Solicitud</button></div>');
			}


		},'json');

		//obtenemos los periodos y actualizamos el select
		$.post("{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ObtenerPeriodosSelectMET", { empleado: pk_num_empleado }, function(data){
	            var json = data,
	            obj = JSON.parse(json);
	            $("#periodo_vacacion").html(obj);
	        });		


		var urlPeriodo = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ObtenerPeriodoMET';
		$.post(urlPeriodo,{ pk_num_empleado: pk_num_empleado },function(dato){
			$("#periodo").html(dato);
		}, 'html');

		var urlPeriodoSolicitud = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ObtenerPeriodosMET';
		var pk_num_empleado = $("#pk_num_empleado").val();
		var pendientes = 0;
		var fecha_salida = $("#fecha_salida").val();
		$.post(urlPeriodoSolicitud,{  fecha_salida: fecha_salida, metodo: 2, pk_num_empleado: pk_num_empleado },function(respuesta_post){
			var tabla_listado = $('#datatable2').DataTable();
			tabla_listado.clear().draw();
			if(respuesta_post != -1) {
				for(var i=0; i<respuesta_post.length; i++) {
					var dias = dias + respuesta_post[i].dias;
					pendientes = pendientes + respuesta_post[i].dias_pendientes;
					tabla_listado.row.add([
						respuesta_post[i].numero,
						respuesta_post[i].anioAnterior+ '-' + respuesta_post[i].anio,
						respuesta_post[i].dias,
						respuesta_post[i].fecha_salida,
						respuesta_post[i].fecha_termino,
						respuesta_post[i].dias_derecho,
						respuesta_post[i].dias_usados,
						respuesta_post[i].dias_pendientes,
						'<button type="button" pk_num_periodo = "' + respuesta_post[i].pkNumPeriodo + '" class="periodo logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" descripcion="El usuario ha visualizado una solicitud de vacaciones" title="Solicitudes de Vacaciones" titulo="Solicitudes de Vacaciones"><i class="glyphicon glyphicon-search"></i></button>'
					]).draw()
							.nodes()
							.to$()

				}
			}
		},'json');
	}

    $('#datatable2').DataTable( {
        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "sSearch":        "Buscar:",
            "zeroRecords": "No hay resultados",
            "info": "Mostrar _PAGE_ de _PAGES_",
            "infoEmpty": "No hay resultados",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                "previous": "<",
                "next": ">"
            }
        }
    } );

    if ( $.fn.dataTable.isDataTable( '#datatable2' ) ) {
        table = $('#datatable2').DataTable();
    }
    else {
        table = $('#datatable2').DataTable( {
            paging: true
        } );
    }
</script>
