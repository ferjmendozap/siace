<form action="{$_Parametros.url}modRH/gestion/vacacionesCONTROL/UtilizacionVacacionesMET" name="formAjax" id="formAjax" method="post"  class="form floating-label" role="form">
	<input type="hidden" value="1" name="valido" />
	{if $periodo.tipoFormulario == 1}
		<input type="hidden" value="{$periodo.periodo}" name="pk_num_periodo" />
		<input type="hidden" value="{$periodo.pk_num_empleado}" id="pk_num_empleado" />
		<input type="hidden" id="fecha_termino" name="fecha_termino" />
	{else}
		<input type="hidden" id="pk_num_solicitud_vacacion" name="pk_num_solicitud_vacacion" value="{$periodo.pkNumSolicitudVacacion}"/>
		<input type="hidden" id="dias" name="dias" {if $periodo.tipoFormulario==2} value="{$periodo.num_dias}" {/if}/>
	{/if}
	<input type="hidden" id="tipoFormulario" name="tipoFormulario" value="{$periodo.tipoFormulario}" />

	<div class="col-md-12 col-sm-12">
		<div class="col-md-6 col-md-6">
			<div class="form-group floating-label">
				{if $periodo.tipoFormulario==1}
					<select id="tipo_vacacion" name="tipo_vacacion" class="form-control">
                        {foreach item=tipo from=$tipoVacacion}
							<option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                        {/foreach}
					</select>
				{else}
					<select id="tipo_vacacion" name="tipo_vacacion" class="form-control">
                        {foreach item=tipo from=$tipoVacacion}
                            {if $tipo.cod_detalle==2}
								<option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
							{/if}
                        {/foreach}
					</select>
				{/if}
				<label for="tipo_vacacion"><i class="glyphicon glyphicon-triangle-right"></i> Tipo</label>
			</div>
			<div class="form-group">
				<input type="text" name="num_dias" id="num_dias" class="form-control dirty" {if $periodo.tipoFormulario==2} value="{$periodo.num_dias}" onchange="calcularFechasInterrumpir(this.value, {$periodo.pkNumSolicitudVacacion}, 1)" {else} onchange="calcularFechas(this.value)" {/if}>
				<label class="control-label">Número de Dias</label>
			</div>
		</div>
		<div class="col-md-6 col-md-6">
			<div class="form-group control-width-normal">
				<div class="input-group date" id="demo-date">
					<div class="input-group-content">
						<input type="text" class="fecha form-control dirty" id="fecha_salida" name="fecha_salida" {if $periodo.tipoFormulario == 1} value="{$smarty.now|date_format:"%d/%m/%Y"}"  onchange="calcular(this.value)" {else} value="{$periodo.fechaSalida|date_format:"%d/%m/%Y"}"{/if}>
                        <label> {if $periodo.tipoFormulario == 1} Fecha de Inicio {else} Fecha de Salida {/if} </label>
					</div>
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>
			<div class="form-group control-width-normal">
				<div class="input-group date" id="demo-date">
					<div class="input-group-content">
						<input type="text" class="form-control dirty" id="fechaTermino"  name="fechaTermino" disabled {if $periodo.tipoFormulario == 2} value="{$periodo.fechaIncorporacion|date_format:"%d/%m/%Y"}" {/if}>
						<label>{if $periodo.tipoFormulario == 1} Fecha Fin {else} Fecha de T&eacute;rmino {/if} </label>
					</div>
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-sm-12">
		<div class="form-group">
			<textarea class="form-control dirty" rows="4" name="ind_motivo" id="ind_motivo"></textarea>
			<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Justificación</label>
		</div>
	</div>
	<div  class="col-md-12 col-sm-12">
		<div align="right">
			<button type="submit" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>&nbsp;
			<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
		</div>
	</div>
</form>
<script type="text/javascript">

	$("#fecha_salida").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});

	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				num_dias:{
					required: true,
					number: true
				},
				tipo_vacacion:{
					required: true
				},
				fecha_salida:{
					required: true
				}
			},
			messages:{
				num_dias:{
					required: "Este campo es requerido",
					number:"Introducir un Número valido"
				},
				tipo_vacacion:{
					required:"Este campo es requerido"
				},
				fecha_salida:{
					required: "Este campo es requerido"
				}
			},
			submitHandler: function(form) {
			    var tipoFormulario = $("#tipoFormulario").val();
			    if(tipoFormulario==1){
                    $.post($(form).attr('action'), $(form).serialize(),function(respuesta_post3){
                        var tabla_listado3 = $('#datatable2').DataTable();
                        tabla_listado3.clear().draw();
                        var $verPeriodo = $("#pk_num_periodo").val();
                        var $a=1;
                        if(respuesta_post3 != -1) {
                            for(var i=0; i<respuesta_post3.length; i++) {
                                var codigoDetalle = respuesta_post3[i].cod_detalle;

                                var botonEliminar = '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descripcion="El usuario ha eliminado una utilizacion" titulo="¿Estás Seguro?" title="Eliminar Utilización" mensaje="¿Estás seguro de eliminar la utilizacion?" boton="si, Eliminar" pk_num_vacacion_utilizacion="'+respuesta_post3[i].pk_num_vacacion_utilizacion+'"><i class="md md-delete" style="color: #ffffff;"></i></button>';
                                var botonModificar = '<button class="modificarUtilizacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" title="Editar" descripcion="El Usuario a Modificado una utilización" titulo="Modificar Utilización" pk_num_periodo="'+$verPeriodo+'" pk_num_vacacion_utilizacion="'+respuesta_post3[i].pk_num_vacacion_utilizacion+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button>';

                                tabla_listado3.row.add([
                                    $a,
                                    respuesta_post3[i].ind_nombre_detalle,
                                    respuesta_post3[i].num_dias_utiles,
                                    respuesta_post3[i].fecha_inicio,
                                    respuesta_post3[i].fecha_fin,
                                    botonEliminar+" "+botonModificar
                                ]).draw()
                                    .nodes()
                                    .to$()
                                $a++;
                            }
                        }

                    },'json');
                    var $pkNumEmpleado = $("#pk_num_empleado").val();
                    var $url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/ListarPeriodoMET';
                    $.post($url,{ pkNumEmpleado: $pkNumEmpleado },function(dato){
                        var tabla = $('#datatable5').DataTable();
                        tabla.clear().draw();
                        if(dato != -1) {
                            var $i = dato.length;
                            for(var i=0; i<dato.length; i++) {
                                var anioAnterior = dato[i].num_anio-1;
                                if(dato[i].num_pendientes>0){
                                    var periodo = '<button class="periodo" periodo="'+dato[i].pk_num_periodo+'" style="font-weight:bold;">'+anioAnterior+ ' - ' + dato[i].num_anio+'</button>';
                                } else {
                                    var periodo = '<button class="periodo" periodo="'+dato[i].pk_num_periodo+'">'+anioAnterior+ ' - ' + dato[i].num_anio+'</button>';
                                }
                                if(dato[i].num_mes<10){
                                    var num_mes = '0'+dato[i].num_mes;
                                } else {
                                    var num_mes = dato[i].num_mes;
                                }
                                tabla.row.add([
                                    $i,
                                    periodo,
                                    num_mes,
                                    dato[i].num_dias_derecho,
                                    dato[i].num_dias_gozados,
                                    dato[i].num_dias_interrumpidos,
                                    dato[i].num_total_utilizados,
                                    dato[i].num_pendientes,
                                    ''
                                ]).draw()
                                    .nodes()
                                    .to$()
                                tabla.order([1,'desc']);

                                $i--;
                            }
                        }
                    },'json');
                    var $urlPendientes ='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularDiasPendientesMET';
                    $.post($urlPendientes,{ pk_num_empleado: $pkNumEmpleado },function(dato){
                        $('#diasPendientes').val(dato['diasPendientes']);
                    },'json');
                } else { // fin del condicional de interrupcion normal
                    $.post($(form).attr('action'), $(form).serialize(),function(dato){
                        $(document.getElementById('datatable1')).append('<tr id="pk_num_solicitud_vacacion'+dato['pk_num_solicitud_vacacion']+'">' +
                            '<td>'+dato['pk_num_solicitud_vacacion']+'</td>'+
                            '<td>'+dato['nombreCompleto']+'</td>'+
                            '<td>'+dato['ind_nombre_detalle']+'</td>'+
                            '<td>'+dato['fecha_solicitud']+'</td>'+
                            '<td>PREPARADO</td>'+
                            '<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" title="Ver solicitud de vacaciones" titulo="Ver solicitud de vacaciones" pk_num_solicitud_vacacion="'+dato['pk_num_solicitud_vacacion']+'"><i class="glyphicon glyphicon-search"></i></button></td>'+
                            '<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descipcion="El usuario ha modificado una solicitud de vacaciones" title="Modificar solicitud de vacaciones" titulo="Modificar solicitud de vacaciones" pk_num_solicitud_vacacion="'+dato['pk_num_solicitud_vacacion']+'"><i class="fa fa-edit"></i></button></td>'+
                            '<td align="center"></td>'+
                            '</tr>');
                    },'json');
                }
                swal('Registro Exitoso', "La utilización de vacaciones fue registrada exitosamente", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('cerrarModal2')).click();
			}
		});
	});



	function calcularFechas(dias)
	{
		var fecha_salida = $("#fecha_salida").val();
		var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularFechasMET';
		$.post(url,{ dias: dias, fecha_salida: fecha_salida },function(dato){
			$("#fecha_termino").val(dato['fechaTermino']);
			$("#fechaTermino").val(dato['fechaTermino']);
		},'json');
	}

	function calcular(fecha_salida)
	{
		var dias = $("#num_dias").val();
		var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularFechasMET';
		$.post(url,{ dias: dias, fecha_salida: fecha_salida },function(dato){
			$("#fecha_termino").val(dato['fechaTermino']);
			$("#fechaTermino").val(dato['fechaTermino']);
		},'json');
	}

	function calcularFechasInterrumpir(dias, pkNumSolicitudVacacion, opcion)
	{
        var url = '{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CargarFechasInterrupcionMET';
        if(opcion==1){
            var num_dias = dias;
            $("#dias").val(dias);
        } else {
            var num_dias = $("#dias").val();
        }
        $.post(url,{ num_dias: num_dias, pkNumSolicitudVacacion: pkNumSolicitudVacacion},function(dato){
            $("#fecha_termino").val(dato['fechaFinal']);
            $("#fechaTermino").val(dato['fechaFinal']);
            $("#fecha_salida").val(dato['fechaInicial']);
        },'json');
    }

</script>