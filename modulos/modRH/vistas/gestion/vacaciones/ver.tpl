<input type="hidden" name="pk_num_solicitud_vacacion" id="num_solicitud_vacacion" value="{$solicitud.pk_num_solicitud_vacacion}" />
<div class="form floating-label">
	<div class="col-lg-12">
		<h3 class="text-primary">Datos del Empleado</h3>
		<div class="col-md-6 col-sm-6">
			<div class="form-group floating-label">
				<select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty">
					<option selected>{$solicitud.ind_descripcion_empresa}</option>
				</select>
				<label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="form-group floating-label">
				<select id="pk_num_empleado" name="pk_num_empleado" class="form-control">
					<option selected>{$solicitud.ind_nombre1} {$solicitud.ind_nombre2} {$solicitud.ind_apellido1} {$solicitud.ind_apellido2}</option>
				</select>
				<label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="form-group floating-label">
				<select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty">
					<option selected>{$solicitud.ind_dependencia}</option>
				</select>
				<label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="panel panel-default" align="center">
				<div class="panel-body">
                    {if $solicitud.formulario=='AD'&& $solicitud.codDetalle==1}
						<button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha interrumpido vacaciones" data-toggle="modal" data-target="#formModal2" titulo="Interrumpir Vacaciones" id="nuevaInt"> <span class="glyphicon glyphicon-log-out"></span> Interrumpir Vacaciones</button>&nbsp;
					{else}
						<br/>
					{/if}
				</div>
			</div>
		</div>
		<h3 class="text-primary">Información de la Solicitud</h3>
		<div class="col-md-12 col-sm-12">
			<div class="col-md-6 col-sm-6">
				<div class="form-group">
					<input type="text" class="form-control" disabled value="{$solicitud.pk_num_solicitud_vacacion}">
					<label class="control-label"><i class="glyphicon glyphicon-th"></i> N° de Solicitud</label>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" disabled value="{$solicitud.cedula}">
					<label class="control-label"><i class="glyphicon glyphicon-th"></i> Documento</label>
				</div>
				<div class="form-group floating-label">
					<select id="tipo_vacacion" name="tipo_vacacion" class="form-control">
						<option selected>{$solicitud.tipo_vacacion}</option>
					</select>
					<label for="tipo_vacacion"><i class="glyphicon glyphicon-triangle-right"></i> Tipo</label>
				</div>
				<div class="form-group">
					<textarea class="form-control" disabled rows="4" name="ind_motivo" id="ind_motivo">{$solicitud.ind_motivo}</textarea>
					<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Justificación</label>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group">
					<input type="text" class="form-control" disabled>
					<label class="control-label">N° de Otorgamiento</label>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" disabled value="{$solicitud.estado}">
					<label class="control-label">Estado</label>
				</div>
				<div class="form-group">
					<input type="text" disabled class="form-control" value="{$solicitud.fec_solicitud|date_format:"%Y-%m"}">
					<label class="control-label">Periodo</label>
				</div>
				<div class="form-group">
					<input type="text" disabled class="form-control" value="{$solicitud.fec_solicitud|date_format:"%d/%m/%Y"}">
					<label class="control-label">Fecha</label>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" disabled value="{$solicitud.creado}">
					<label class="control-label">Creado por</label>
				</div>
			</div>
		</div>
		<div class="col-md-12 col-sm-12">
			<div class="well clearfix">
				<div class="col-md-6 col-sm-6">
					<div class="form-group control-width-normal">
						<div class="input-group date" id="demo-date">
							<div class="input-group-content">
								<input type="text" class="form-control" value="{$solicitud.fec_salida|date_format:"%d/%m/%Y"}" disabled>
								<label>Fecha de Salida</label>
							</div>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<div class="form-group">
						<input type="text" name="num_dias" id="num_dias" disabled value="{$solicitud.num_dias_solicitados}" class="form-control dirty">
						<label class="control-label">Número de Dias</label>
					</div>
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="form-group control-width-normal">
						<div class="input-group date" id="demo-date">
							<div class="input-group-content">
								<input type="text" class="form-control dirty" id="fecha_termino" name="fecha_termino" disabled value="{$solicitud.fec_termino|date_format:"%d/%m/%Y"}">
								<label>Fecha de Término</label>
							</div>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
					<div class="form-group control-width-normal">
						<div class="input-group date" id="demo-date">
							<div class="input-group-content">
								<input type="text" class="form-control dirty" id="fecha_incorporacion" name="fecha_incorporacion" disabled value="{$solicitud.fec_incorporacion|date_format:"%d/%m/%Y"}">
								<label>Fecha de Incorporación</label>
							</div>
							<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		{if ($solicitud.ind_estado=='AP')||($solicitud.ind_estado=='CO')}
            {foreach item=listar from=$listarDetalle}
				<div class="col-md-12 col-sm-12">
					<div class="well clearfix">
						<div class="form-group">
							<textarea class="form-control" rows="4" disabled>{$listar.ind_observacion}</textarea>
							<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observaciones Periodo: {$listar.num_anio-1} - {$listar.num_anio}</label>
						</div>
					</div>
				</div>
            {/foreach}
		{/if}
		{if $solicitud.ind_estado=='RE'}
			{assign var=contador value=0}
			<form id="formAjax" action="{$_Parametros.url}modRH/gestion/vacacionesCONTROL/GuardarObservacionConformacionMET" class="form floating-label" novalidate="novalidate">
			<input type="hidden" name="pk_num_solicitud_vacacion" id="num_solicitud_vacacion" value="{$solicitud.pk_num_solicitud_vacacion}" />
			<input type="hidden" name="estado" id="estado" value="CO" />
			{foreach item=listar from=$listarDetalle}
				<div class="col-md-12 col-sm-12">
					<div class="well clearfix">
						<div class="form-group">
							<textarea class="form-control" rows="4" name="ind_observacion{$contador++}" id="ind_observacion{$contador}"></textarea>
							<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observaciones Periodo: {$listar.num_anio-1} - {$listar.num_anio}</label>
						</div>
					</div>
				</div>
            {/foreach}
		{/if}
			<div class="col-md-12 col-sm-12">
			<h4>Acceso</h4>
			<div class="col-md-6 col-sm-6">
				<div class="form-group">
					<input type="text" disabled class="form-control" value="{$solicitud.ultimoUsuario}">
					<label class="control-label">Último usuario</label>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group">
					<input type="text" disabled class="form-control" value="{$solicitud.ultimaModificacion}">
					<label class="control-label">Última modificación</label>
				</div>
			</div>
		</div>
		<div align="right">
		{if $accion!='ver'}
			{if $solicitud.ind_estado =='PR' }
				{if in_array('RH-01-01-07-04-02-RE',$_Parametros.perfil )}<button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion" value="RE" onclick="estatus(this.value)"><span class="glyphicon glyphicon-floppy-disk"></span> Revisar</button>&nbsp;&nbsp;{/if}
				{if in_array('RH-01-01-07-04-05-AN',$_Parametros.perfil)}<button type="button" class="btn btn-primary ink-reaction btn-danger" id="accion" value="AN" onclick="estatus(this.value)"><span class="glyphicon glyphicon-ban-circle"></span> Anular</button>&nbsp;&nbsp;{/if}
			{/if}
			{if $solicitud.ind_estado=='RE'}
				{if in_array('RH-01-01-07-04-03-CO',$_Parametros.perfil)}<button type="submit" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-pencil"></span> Conformar</button>&nbsp;&nbsp;{/if}
				{if in_array('RH-01-01-07-04-05-AN',$_Parametros.perfil)}<button type="button" class="btn btn-primary ink-reaction btn-danger" id="accion" value="AN" onclick="estatus(this.value)"><span class="glyphicon glyphicon-ban-circle"></span> Anular</button>&nbsp;&nbsp;{/if}
			{/if}
			{if $solicitud.ind_estado=='CO'}
				{if in_array('RH-01-01-07-04-04-AP',$_Parametros.perfil)}<button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion" value="AP" onclick="estatus(this.value)"><span class="glyphicon glyphicon-ok"></span> Aprobar</button>&nbsp;&nbsp;{/if}
				{if in_array('RH-01-01-07-04-05-AN',$_Parametros.perfil)}<button type="button" class="btn btn-primary ink-reaction btn-danger" id="accion" value="AN" onclick="estatus(this.value)"><span class="glyphicon glyphicon-ban-circle"></span> Anular</button>&nbsp;&nbsp;{/if}
			{/if}
			{if $solicitud.ind_estado=='AP'}
				{if in_array('RH-01-01-07-04-05-AN',$_Parametros.perfil)}<button type="button" class="btn btn-primary ink-reaction btn-danger" id="accion" value="AN" onclick="estatus(this.value)"><span class="glyphicon glyphicon-ban-circle"></span> Anular</button>&nbsp;&nbsp;{/if}
			{/if}
        {/if}
			<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>

		</div>
	</div>
	</form>
</div>
<script type="text/javascript">

    $('#modalAncho2').css( "width", "45%" );

    var $urlUtilizacionVacacion ='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/InterrumpirVacacionesMET';
    var pkNumSolicitudVacacion = $("#num_solicitud_vacacion").val();
    var num_dias = $("#num_dias").val();
    $('#nuevaInt').click(function(){
        $('#formModalLabel2').html($(this).attr('titulo'));
        $('#ContenidoModal2').html("");
        $.post($urlUtilizacionVacacion,{ pk_num_solicitud_vacacion: pkNumSolicitudVacacion, num_dias: num_dias},function($dato){
            $('#ContenidoModal2').html($dato);
        });
    });

	function estatus(valor)
	{
		var $url='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/EstatusMET';
		var $pkNumSolicitudVacacion = $("#num_solicitud_vacacion").val();
		if(valor=='RE'){
			var nuevoValor = 'REVISADO';
		}
		if(valor=='AP'){
			var nuevoValor = 'APROBADO';
		}
		if(valor=='AN'){
			var nuevoValor = 'ANULADO';
		}
		if(valor=='AN'){
			$.post($url,{ pk_num_solicitud_vacacion: $pkNumSolicitudVacacion, valor: valor},function(dato){
				$('#pk_num_solicitud_vacacion'+dato['pk_num_solicitud_vacacion']).remove();
				if(dato['ind_estado']=='PREPARADO' ){
					var boton = '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descipcion="El usuario ha modificado una solicitud de vacaciones" title="Modificar solicitud de vacaciones" titulo="Modificar solicitud de vacaciones" pk_num_solicitud_vacacion="'+dato['pk_num_solicitud_vacacion']+'"><i class="fa fa-edit"></i></button>'
				} else {
					var boton = '';
				}
				swal(nuevoValor, "Procedimiento exitoso", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
			}, 'json');
		}
		if((valor=='RE')||(valor=='AP')){
			$.post($url,{ pk_num_solicitud_vacacion: $pkNumSolicitudVacacion, valor: valor},function(dato){
				$('#pk_num_solicitud_vacacion'+dato['pk_num_solicitud_vacacion']).remove();
				swal(nuevoValor, "Procedimiento exitoso", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
			}, 'json');
		}
	}

    $(document).ready(function () {
        //validation rules
        $("#formAjax").validate({
            submitHandler: function(form) {
                $.post($(form).attr('action'), $(form).serialize(),function(dato){
                    $('#pk_num_solicitud_vacacion'+dato['pk_num_solicitud_vacacion']).remove();
                    swal('CONFORMADO', "Procedimiento exitoso", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                },'json');
            }
        });
    });


</script>