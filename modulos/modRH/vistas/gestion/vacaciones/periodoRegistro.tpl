<form action="{$_Parametros.url}modRH/gestion/vacacionesCONTROL/EditarPeriodoMET" name="formAjax" id="formAjax" method="post"  class="form floating-label" role="form">
	<input type="hidden" value="1" name="valido" />
	<input type="hidden" value="{$total.pkNumEmpleado}" id="pkNumEmpleado" name="pkNumEmpleado"/>

	<table class="table table-striped table-hover">
		<thead>
		<tr align="center">
			<th>Periodo Inicio</th>
			<th>Periodo Fin</th>
			<th>Mes de Apertura</th>
			<th>Derecho</th>
			<th>Días Gozados</th>
			<th>Días Interrumpidos</th>
			<th>Total Utilizados</th>
			<th>Días Pendientes</th>
		</tr>
		</thead>
		<tbody>
		{for $fila=1 to $total.totalPeriodo}
			<tr>
				<td>
					<div class="form-group floating-label">
						<div class="form-group floating-label">
							<input  type="text" class="form-control" value="{$numAnio[$fila]-1}" readonly>

						</div>
					</div>
				</td>
				<td>
					<div class="form-group floating-label">
						<div class="form-group floating-label">
							<input  type="text" class="form-control" value="{$numAnio[$fila]}" name="num_anio{$fila}" id="num_anio{$fila}" readonly>

						</div>
					</div>
				</td>
				<td>
					<div class="form-group floating-label">
						<div class="form-group floating-label">
							<input disabled type="text" class="form-control" value="{$numMes[$fila]}">

						</div>
					</div>
				</td>
				<td>
					<div class="form-group floating-label">
						<div class="form-group floating-label">
						{if $numPendientes[$fila]==0}
							<input disabled type="text" class="form-control" id="dias_derecho{$fila}" name="dias_derecho{$fila}" value="{$numDiasDerecho[$fila]}">
						{else}
							<input type="text" class="form-control" id="dias_derecho{$fila}" name="dias_derecho{$fila}" value="{$numDiasDerecho[$fila]}" onchange="calcularDias(this,{$fila})">
						{/if}

						</div>
					</div>
				</td>
				<td>
					<div class="form-group floating-label">
						<div class="form-group floating-label">
							<input disabled type="text" class="form-control" id="dias_gozados{$fila}" name="dias_gozados{$fila}" value="{$numDiasGozados[$fila]}" >

						</div>
					</div>
				</td>
				<td>
					<div class="form-group floating-label">
						<div class="form-group floating-label">
							<input disabled type="text" class="form-control" id="dias_interrumpidos{$fila}" name="dias_interrumpidos{$fila}" value="{$numDiasInterrumpidos[$fila]}">

						</div>
					</div>
				</td>
				<td>
					<div class="form-group floating-label">
						<div class="form-group floating-label">
							<input disabled type="text" class="form-control" id="total_utilizados{$fila}" name="total_utilizados{$fila}" value="{$numTotalUtilizados[$fila]}" >

						</div>
					</div>
				</td>
				<td>
					<div class="form-group floating-label">
						<div class="form-group floating-label">
							<input disabled type="text" class="form-control" id="total_pendientes{$fila}" name="total_pendientes{$fila}" value="{$numPendientes[$fila]}">

						</div>
					</div>
				</td>
			</tr>
		{/for}
		</tbody>
		<tfoot>
			<th colspan="8">
				<div class="col-md-12">
					<div class="form-group">
						<p class="text-xs text-bold text-right"><span class="text-lg text-bold" style="color: red"> NOTA: </span> UTILICE ESTA OPERACIÓN SOLO SI ES ESTRICTAMENTE NECESARIO HACER UN AJUSTE EN EL PERIODO. DE LO CONTRARIO NO LO UTILICE. </p>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<p class="text-xs text-bold text-right"><span class="text-bold" style="color: red"> SOLO SE VISUALIZARÁN LOS PERIODOS CON DIAS PENDIENTES </span> </p>
					</div>
				</div>
			</th>
		</tfoot>
	</table>
	<br/>
	<div  class="col-md-12 col-sm-12">
		<div align="center">
			<button type="submit" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>&nbsp;
			<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
		</div>
	</div>
</form>
<script type="text/javascript">

	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			submitHandler: function(form) {

				var formulario = $( "#formAjax" );
				var disabled = formulario.find(':input:disabled').removeAttr('disabled');
				var datos = formulario.serialize();

				$.post($(form).attr('action'), datos,function(respuesta_post){
					var tabla_listado = $('#datatable5').DataTable();
					tabla_listado.clear().draw();
					if(respuesta_post != -1) {
						var $i = respuesta_post.length;
						for(var i=0; i<respuesta_post.length; i++) {
							var anioAnterior = respuesta_post[i].num_anio-1;
							if(respuesta_post[i].num_pendientes>0){
								var periodo = '<button class="periodo" periodo="'+respuesta_post[i].pk_num_periodo+'" style="font-weight:bold;">'+anioAnterior+ ' - ' + respuesta_post[i].num_anio+'</button>';
							} else {
								var periodo = '<button class="periodo" periodo="'+respuesta_post[i].pk_num_periodo+'">'+anioAnterior+ ' - ' + respuesta_post[i].num_anio+'</button>';
							}
							if(respuesta_post[i].num_mes<10){
								var num_mes = '0'+respuesta_post[i].num_mes;
							} else {
								var num_mes = respuesta_post[i].num_mes;
							}
							var fila = tabla_listado.row.add([
								$i,
								periodo,
								num_mes,
								respuesta_post[i].num_dias_derecho,
								respuesta_post[i].num_dias_gozados,
								respuesta_post[i].num_dias_interrumpidos,
								respuesta_post[i].num_total_utilizados,
								respuesta_post[i].num_pendientes,
								''
							]).draw()
									.nodes()
									.to$()
							tabla_listado.order([1,'desc']);
							$i--;
							$(fila)
									.attr('id','pk_num_periodo'+respuesta_post[i].pk_num_periodo+'');
						}
					}

				},'json');
				var $urlPendientes ='{$_Parametros.url}modRH/gestion/vacacionesCONTROL/CalcularDiasPendientesMET';
				$.post($urlPendientes,{ pk_num_empleado: $("#pkNumEmpleado").val() },function(dato){
					$('#diasPendientes').val(dato['diasPendientes']);
				},'json');
				swal("Solicitud Guardada", "Solicitud de vacaciones guardada satisfactoriamente", "success");
				$(document.getElementById('cerrarModal2')).click();
				$(document.getElementById('ContenidoModal2')).html('');
			}
		});
	});

	function calcularDias(id,fila)
	{
		var dias_derecho = $("#dias_derecho"+fila).val();
		var dias_gozados = $("#dias_gozados"+fila).val();
		var dias_interrumpidos = $("#dias_interrumpidos"+fila).val();
		var total_utilizados = $("#total_utilizados"+fila).val();
		var total_pendientes = $("#total_pendientes"+fila).val();

		var dias_derecho_h = $("#dias_derecho_h"+fila).val();

		if(parseFloat(dias_gozados) > parseFloat(dias_derecho_h)){
			swal("Error", "Los Días Gozados no puede ser mayor que los dias por derecho", "error");
			return false;
		}
		if(parseFloat(total_utilizados) > parseFloat(dias_derecho_h)){
			swal("Error", "Los Dias Utilizados no puede ser mayor que los dias por derech", "error");
			return false;
		}

		$("#total_pendientes"+fila).val(parseFloat(dias_derecho)-parseFloat(dias_gozados));
		$("#total_utilizados"+fila).val(parseFloat(dias_gozados));

		var totalPendiente = dias_derecho - total_utilizados;
		$("#total_pendientes"+fila).val(totalPendiente);
		/*if(dias_interrumpidos > 0){

			$("#total_pendientes"+fila).val(parseFloat(dias_derecho)-parseFloat(dias_gozados)+parseFloat(dias_interrumpidos));
			$("#total_utilizados"+fila).val(parseFloat(dias_gozados)-parseFloat(dias_interrumpidos));
			$("#dias_gozados"+fila).val(parseFloat(dias_gozados)-parseFloat(dias_interrumpidos));

		}*/



	}
</script>