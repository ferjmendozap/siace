<!-- <form action="" id="formAjax" class="form" role="form" method="post">-->
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">


                            <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                                <input type="hidden" value="1" name="valido" />
                                <input type="hidden" value="{$idExperienciaLaboral}" name="idExperienciaLaboral" id="idExperienciaLaboral"/>
                                <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                                <input type="hidden" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}" name="idPersona"/>


                                <div class="col-xs-10" style="margin-top: -10px">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="ind_empresa" name="form[txt][ind_empresa]" value="{if isset($formDB.ind_empresa)}{$formDB.ind_empresa}{/if}" required data-msg-required="Indique Empresa" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                <label for="ind_empresa">Empresa</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div><!--end .col -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select id="fk_a006_num_miscelaneo_detalle_tipo_empresa" name="form[int][fk_a006_num_miscelaneo_detalle_tipo_empresa]" class="form-control input-sm" required data-msg-required="Seleccione Tipo de Entidad">
                                                    <option value="">Seleccione...</option>
                                                    {foreach item=sex from=$Entidad}
                                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipo_empresa)}
                                                            {if $sex.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipo_empresa}
                                                                <option selected value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                            {else}
                                                                <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {else}
                                                            <option value="{$sex.pk_num_miscelaneo_detalle}">{$sex.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {/foreach}
                                                </select>
                                                <label for="fk_a006_num_miscelaneo_detalle_tipo_empresa">Tipo de Entidad</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="ind_cargo"  name="form[txt][ind_cargo]" value="{if isset($formDB.ind_cargo)}{$formDB.ind_cargo}{/if}" required data-msg-required="Indique Cargo Ocupado" onkeyup="$(this).val($(this).val().toUpperCase())">
                                                <label for="ind_cargo">Cargo</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div><!--end .col -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select id="fk_rhc032_num_motivo_cese" name="form[int][fk_rhc032_num_motivo_cese]" class="form-control input-sm" required data-msg-required="Seleccione Motivo de Cese">
                                                    <option value="">Seleccione...</option>
                                                    {foreach item=dat from=$MotivoCese}
                                                        {if isset($formDB.fk_rhc032_num_motivo_cese)}
                                                            {if $dat.pk_num_motivo_cese==$formDB.fk_rhc032_num_motivo_cese}
                                                                <option selected value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
                                                            {else}
                                                                <option value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
                                                            {/if}
                                                        {else}
                                                            <option value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
                                                        {/if}
                                                    {/foreach}
                                                </select>
                                                <label for="fk_rhc032_num_motivo_cese">Motivo de Cese</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select id="fk_a006_num_miscelaneo_detalle_areaexp" name="form[int][fk_a006_num_miscelaneo_detalle_areaexp]" class="form-control input-sm" required data-msg-required="Seleccione Area de Experiencia">
                                                    <option value="">Seleccione...</option>
                                                    {foreach item=dat from=$AreaExp}
                                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_areaexp)}
                                                            {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_areaexp}
                                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {else}
                                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {else}
                                                            <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                        {/if}
                                                    {/foreach}
                                                </select>
                                                <label for="fk_a006_num_miscelaneo_detalle_areaexp">Area de Experiencia</label>
                                                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control input-sm" id="num_sueldo_mensual" name="form[int][num_sueldo_mensual]" value="{if isset($formDB.num_sueldo_mensual)}{$formDB.num_sueldo_mensual|number_format:2:",":"."}{else}0,00{/if}">
                                                <label for="num_sueldo_mensual">Sueldo Mensual</label>
                                            </div>
                                        </div><!--end .col -->
                                    </div><!--end .row -->

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-daterange input-group" id="rango-fechas-experiencia">
                                                    <div class="input-group-content">
                                                        <input type="text" class="form-control" id="fec_ingreso" name="form[txt][fec_ingreso]" value="{if isset($formDB.fec_ingreso)}{$formDB.fec_ingreso|date_format:"d-m-Y"}{/if}" />
                                                    </div>
                                                    <span class="input-group-addon">a</span>
                                                    <div class="input-group-content">
                                                        <input type="text" class="form-control" id="fec_egreso" name="form[txt][fec_egreso]" value="{if isset($formDB.fec_egreso)}{$formDB.fec_egreso|date_format:"d-m-Y"}{/if}" />
                                                        <div class="form-control-line"></div>
                                                    </div>
                                                </div>
                                                <label for="Periodo">Periodo</label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-group">

                                                    <div class="input-group-content">
                                                        <input type="text"  class="form-control col-xs-1 input-sm text-right" id="a"  value="" disabled><p class="help-block"><span class="text-xs" style="color: red">Años</span></p>
                                                    </div>
                                                    <div class="input-group-content">
                                                        <input type="text"  class="form-control col-xs-1 input-sm text-right" id="m"  value="" disabled><p class="help-block"><span class="text-xs" style="color: red">Meses</span></p>
                                                    </div>
                                                    <div class="input-group-content">
                                                        <input type="text"  class="form-control col-xs-1 input-sm text-right" id="d"  value="" disabled><p class="help-block"><span class="text-xs" style="color: red">Días</span></p>
                                                    </div>

                                                </div>
                                                <label for="tiempo_servicio">Tiempo de Servicio</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <textarea name="form[txt][txt_funciones]" id="txt_funciones" class="form-control input-sm" rows="3" placeholder="" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">{if isset($formDB.txt_funciones)}{$formDB.txt_funciones}{/if}</textarea>
                                                <label for="txt_funciones">Funciones</label>
                                            </div>
                                        </div><!--end .col -->
                                        <div class="col-md-6">
                                            <div class="checkbox checkbox-styled">
                                                <label>
                                                    <input type="checkbox" {if isset($formDB.num_flag_antvacacion) and $formDB.num_flag_antvacacion==1} checked{/if} value="1" name="form[int][num_flag_antvacacion]">
                                                    <span>Aplicable para antecedentes de vacaciones</span>
                                                </label>
                                            </div>
                                        </div><!--end .col -->
                                    </div><!--end .row -->

                                    <div class="row">
                                        <br><br>
                                        <div align="right">
                                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                                        </div>
                                    </div>

                                </div>
                            </form>


            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">



    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/gestion/experienciaLaboralCONTROL/ExperienciaLaboralMET', datos ,function(dato){

                if(dato['status']=='modificar'){

                    $(document.getElementById('idExperienciaLaboral'+dato['idExperienciaLaboral'])).html('<td width="30%">'+dato['ind_empresa']+'</td>' +
                            '<td width="30%">'+dato['ind_cargo']+'</td>' +
                            '<td width="15%">'+dato['fec_ingreso']+'</td>' +
                            '<td width="15%">'+dato['fec_egreso']+'</td>' +
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                            'data-keyboard="false" data-backdrop="static" idExperienciaLaboral="'+dato['idExperienciaLaboral']+'"' +
                            'descipcion="El Usuario a Modificado Experiencia LAboral" titulo="Modificar Experiencia Laboral">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="5%">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idExperienciaLaboral="'+dato['idExperienciaLaboral']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Experiencia Laboral" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Experiencia Laboral!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal2')).click();
                    $(document.getElementById('ContenidoModal2')).html('');

                }else if(dato['status']=='creacion'){


                        $(document.getElementById('datatable2')).append('<tr id="idExperienciaLaboral'+dato['idExperienciaLaboral']+'" style="font-size: 11px">' +
                                '<td width="30%">'+dato['ind_empresa']+'</td>' +
                                '<td width="30%">'+dato['ind_cargo']+'</td>' +
                                '<td width="15%">'+dato['fec_ingreso']+'</td>' +
                                '<td width="15%">'+dato['fec_egreso']+'</td>' +
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"' +
                                'data-keyboard="false" data-backdrop="static" idExperienciaLaboral="'+dato['idExperienciaLaboral']+'"' +
                                'descipcion="El Usuario a Modificado Experiencia LAboral" titulo="Modificar Experiencia Laboral">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="5%">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idExperienciaLaboral="'+dato['idExperienciaLaboral']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado Experiencia Laboral" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Experiencia Laboral!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal2')).click();
                        $(document.getElementById('ContenidoModal2')).html('');

                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "72%" );

        //PARA DAR FORMATO AL CAMPO MONTO
        $("#num_sueldo_mensual").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });

        //inicializar el datepicker
        $("#rango-fechas-experiencia").datepicker({
            format:'dd-mm-yyyy',
            language:'es',
            autoclose:true
        });


        //si el campo idExperienciaLaboral es distinto de cero actualizo tiempo de servicio
        var idExperienciaLaboral = $("#idExperienciaLaboral").val();
        if(idExperienciaLaboral!=0){

            var fec_ingreso = $("#fec_ingreso").val();
            var fec_egreso  = $("#fec_egreso").val();

            $.post("{$_Parametros.url}modRH/gestion/empleadosCONTROL/TiempoServicioFechaMET/"+fec_ingreso+"/"+fec_egreso+"/json", { } , function(data){

                $("#a").val(data['anio']);
                $("#m").val(data['mes']);
                $("#d").val(data['dia']);

            },'json');

        }

        $("#fec_ingreso").change(function() {

            var fec_ingreso = $("#fec_ingreso").val();
            var fec_egreso  = $("#fec_egreso").val();

            if(fec_egreso==''){
                fec_egreso = fec_ingreso;
            }else{
                fec_egreso = fec_egreso;
            }

            $.post("{$_Parametros.url}modRH/gestion/empleadosCONTROL/TiempoServicioFechaMET/"+fec_ingreso+"/"+fec_egreso+"/json", { } , function(data){

                $("#a").val(data['anio']);
                $("#m").val(data['mes']);
                $("#d").val(data['dia']);

            },'json');

        });

        $("#fec_egreso").change(function() {

            var fec_ingreso = $("#fec_ingreso").val();
            var fec_egreso  = $("#fec_egreso").val();

            $.post("{$_Parametros.url}modRH/gestion/empleadosCONTROL/TiempoServicioFechaMET/"+fec_ingreso+"/"+fec_egreso+"/json", { } , function(data){

                $("#a").val(data['anio']);
                $("#m").val(data['mes']);
                $("#d").val(data['dia']);

            },'json');

        });


    });






</script>