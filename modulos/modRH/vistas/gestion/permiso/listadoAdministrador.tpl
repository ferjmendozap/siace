</br>
<input type="hidden" id="estado" value="{$emp.estado}" />
<section class="style-default-bright">
    <h2 class="text-primary">&nbsp;
        {if $emp.estado == 'PR'}Aprobar {/if}{if $emp.estado== 'VE'} Verificar{/if} {if $emp.estado=='GE'} Listado de {/if}
        Permisos</h2>
        <div class="section-body contain-lg">
            <div class="row">
                <div class="col-lg-12 contain-lg">
                    <div align="right">
                        {if in_array('RH-01-01-06-03-06-BG',$_Parametros.perfil)}
                        <div class="btn-group"><a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a></div>&nbsp;
                        {/if}
                        <hr class="ruler-xl">
                            <div class="col-md-12 col-md-12">
                                <div class="well clearfix">
                                    <div class="text-primary" align="left"><h4>Detalle del Listado</h4></div>
                                        <div class="col-md-12 col-md-12">
                                            <div class="col-sm-6 col-sm-6" id="detalle" align="left">
                                                <div align="left">
                                                    <label>Organismo: {if ($busqueda.ind_descripcion_empresa!='')}{$busqueda.ind_descripcion_empresa}{/if}</label>
                                                </div>
                                                {if in_array('RH-01-01-06-03-09-P',$_Parametros.perfil)}
                                                    <div>
                                                        <label>Dependencia: </label>
                                                    </div>
                                                {else}
                                                    <div>
                                                        <label>Dependencia: {if ($busqueda.ind_dependencia!='')}{$busqueda.ind_dependencia}{/if}</label>
                                                    </div>
                                                {/if}
                                                <div>
                                                    <label>Empleado: </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-sm-6" id="detalleBuscar" align="left">
                                                <div>
                                                    <label>N° de Permiso: </label>
                                                </div>
                                                <div>
                                                    <label>Fecha de Registro: </label>
                                                </div>
                                                <div>
                                                    <label>Estado: </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr  align="center">
                                <th><i class="glyphicon glyphicon-th"></i> N° de Permiso</th>
                                <th><i class="md-person"></i> Nombre Completo</th>
                                <th><i class="glyphicon glyphicon-triangle-right"></i> Motivo de Ausencia</th>
                                <th><i class="glyphicon glyphicon-triangle-right"></i> Tipo de Ausencia</th>
                                <th><i class="glyphicon glyphicon-calendar"></i> Fecha de Registro</th>
                                <th><i class="glyphicon glyphicon-cog"></i> Estado</th>
                                <th width="40">Ver</th>
                                <th width="40">Editar</th>
                                <th width="50">Reporte</th>
                            </tr>
                        </thead>
                        <tbody>
                        {foreach item=permiso from=$permisoFuncionario}
                            <tr id="pk_num_permiso{$permiso.pk_num_permiso}">
                                <td>{$permiso.pk_num_permiso}</td>
                                <td>{$permiso.ind_nombre1} {$permiso.ind_nombre2} {$permiso.ind_apellido1} {$permiso.ind_apellido2}</td>
                                <td>{$permiso.motivo_ausencia}</td>
                                <td>{$permiso.tipo_ausencia}</td>
                                <td>{$permiso.fecha_registro}</td>
                                <td>
                                    {if $permiso.ind_estado=='PR'}PREPARADO{/if}
                                    {if $permiso.ind_estado=='RE'}REVISADO{/if}
                                    {if $permiso.ind_estado=='CO'}CONFORMADO{/if}
                                    {if $permiso.ind_estado=='AP'}APROBADO{/if}
                                    {if $permiso.ind_estado=='AN'}ANULADO{/if}
                                    {if $permiso.ind_estado=='VE'}VERIFICADO{/if}
                                </td>
                                {if in_array('RH-01-01-06-03-08-V',$_Parametros.perfil)}
                                    <td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_permiso="{$permiso.pk_num_permiso}" title="Ver permiso" titulo="Ver permiso" id="ver"><i class="glyphicon glyphicon-search"></i></button></td>
                                {else}
                                    <td></td>
                                {/if}
                                {if in_array('RH-01-01-06-03-04-M',$_Parametros.perfil)}
                                    <td align="center">{if ($permiso.ind_estado=='PR')}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_permiso="{$permiso.pk_num_permiso}" descipcion="El usuario ha modificado un permiso" title="Modificar permiso"  titulo="Modificar permiso"><i class="fa fa-edit"></i></button>{/if}</td>
                                {else}
                                    <td></td>
                                {/if}
                                {if in_array('RH-01-01-06-03-05-RE',$_Parametros.perfil)}
                                    <td align="center"><button class="permiso logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" pk_num_permiso="{$permiso.pk_num_permiso}" data-keyboard="false" data-backdrop="static" title="Ver Permiso" titulo="Ver Permiso"><i class="md md-attach-file"></i></button></td>
                                {else}
                                    <td></td>
                                {/if}
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</section>
<!-- filtro -->
<div class="offcanvas">
<form id="formulario" class="form floating-label"  role="form" method="post" novalidate>
    <div id="offcanvas-filtro" class="offcanvas-pane width-12">
        <div class="offcanvas-head">
            <header>Filtro de Búsqueda</header>
            <div class="offcanvas-tools">
                <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>
        <div class="offcanvas-body">
            {if in_array('RH-01-01-06-03-09-P',$_Parametros.perfil)}
                <div class="form-group floating-label">
                    <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                        {foreach item=org from=$listadoOrganismo}
                            {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {else}
                                <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div id="dependencia">
                    <div class="form-group floating-label">
                        <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty" onchange="cargarEmpleado(this.value)">
                            <option value="">&nbsp;</option>
                            {foreach item=dep from=$listadoDependencia}
                                <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                    </div>
                </div>
                <div id="empleado">
                    <div class="form-group floating-label">
                        <select id="pk_num_empleado" name="pk_num_empleado" id="pk_num_empleado" class="form-control dirty">
                            <option value="">&nbsp;</option>
                        </select>
                        <label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
                    </div>
                </div>
            {else}
                <div class="form-group floating-label">
                    <select id="pk_num_organismo" name="pk_num_organismo" id="pk_num_organismo" class="form-control dirty">
                        <option value="{$busqueda.pk_num_organismo}" selected>{$busqueda.ind_descripcion_empresa}</option>
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div class="form-group floating-label">
                    <select id="pk_num_dependencia" name="pk_num_dependencia" id="pk_num_empleado" class="form-control dirty">
                        <option value="{$busqueda.pk_num_dependencia}" selected>{$busqueda.ind_dependencia}</option>
                    </select>
                    <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                </div>
                <div class="form-group floating-label">
                    <select id="pk_num_empleado" name="pk_num_empleado" id="s2id_single" class="select2-container form-control select2">
                        <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                            <option value="">&nbsp;</option>
                            {foreach item=emp from=$empleado}
                                    <option value="{$emp.pk_num_empleado}">{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}</option>
                            {/foreach}
                    </select>
                    <label for="pk_num_empleado_aprueba"><i class="glyphicon glyphicon-user"></i> Empleado</label>
                </div>
            {/if}
            <div class="form-group">
                <input type="text" name="pk_num_permiso" id="pk_num_permiso" class="form-control dirty">
                <label class="control-label"><i class="glyphicon glyphicon-th"></i> N° de Permiso</label>
            </div>
            <div class="form-group" style="width:100%">
                <div class="input-daterange input-group" id="demo-date-range">
                    <div class="input-group-content">
                        <input type="text" class="form-control dirty" name="fechaInicio" id="fecha_inicio">
                        <label><i class="glyphicon glyphicon-calendar"></i> Fecha de Registro</label>
                    </div>
                    <span class="input-group-addon">a</span>
                    <div class="input-group-content">
                        <input type="text" class="form-control dirty" name="fechaFin" id="fecha_fin">
                        <div class="form-control-line"></div>
                    </div>
                </div>
            </div>
            <div class="form-group floating-label">
                <select id="ind_estado" name="ind_estado" class="form-control dirty">
                    <option value=""></option>
                    {if $emp.estado == 'GE'}
                        <option value="PR">Preparado</option>
                        <option value="AP">Aprobado</option>
                        <option value="VE">Verificado</option>
                        <option value="AN">Anulado</option>
                    {/if}
                    {if $emp.estado == 'PR'}
                        <option value="PR">Preparado</option>
                    {/if}
                    {if $emp.estado == 'AP'}
                        <option value="AP">Aprobado</option>
                    {/if}
                </select>
                <label for="ind_estado"><i class="glyphicon glyphicon-list-alt"></i> Estado</label>
            </div>
            <br/>
            <div class="col-sm-12 col-sm-12" align="center">
                {if in_array('RH-01-01-06-03-07-RG',$_Parametros.perfil)}
                    <div class="col-sm-6 col-sm-6">
                        <button type="button" id="buscarReporte" titulo="Reporte de Permiso" data-toggle="modal" data-target="#formModal" class="btn ink-reaction btn-flat btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Generando...">Generar Reporte<div style="top: 27px; left: 65px;" class="ink inverse"></div></button>
                    </div>
                {/if}
                <div class="col-sm-6 col-sm-6">
                    <button type="submit" id="formu" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#fecha_inicio").datepicker({
            format: 'dd/mm/yyyy',
            language: 'es',
            autoclose: true
        });
        $("#fecha_fin").datepicker({
            format: 'dd/mm/yyyy',
            language: 'es',
            autoclose: true
        });
        var $url='{$_Parametros.url}modRH/gestion/permisoCONTROL/NuevoPermisoMET';
        var $url_modificar='{$_Parametros.url}modRH/gestion/permisoCONTROL/EditarPermisoMET';
        $('#nuevo').click(function(){
            $('#modalAncho').css( "width", "75%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.permiso', function () {
            $('#modalAncho').css( "width", "85%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var pk_num_permiso = $(this).attr('pk_num_permiso');
            var urlReporte = '{$_Parametros.url}modRH/gestion/permisoCONTROL/ReportePermisoMET/?pk_num_permiso='+pk_num_permiso;
            $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#modalAncho').css( "width", "75%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url_modificar,{ pk_num_permiso: $(this).attr('pk_num_permiso'), tipoListado: 2},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var pk_num_permiso=$(this).attr('pk_num_permiso');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/gestion/permisoCONTROL/EliminarPermisoMET';
                $.post($url, { pk_num_permiso: pk_num_permiso},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('pk_num_permiso'+$dato['pk_num_permiso'])).html('');
                        swal("Eliminado!", "La caja fue eliminada.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });


    $('#datatable1 tbody').on( 'click', '.ver', function () {
        var $urlVer='{$_Parametros.url}modRH/gestion/permisoCONTROL/VerPermisoMET';
        var estado = $("#estado").val();
        $('#modalAncho').css( "width", "75%" );
        $('#formModalLabel').html($(this).attr('title'));
        $('#ContenidoModal').html("");
        $.post($urlVer,{ pk_num_permiso: $(this).attr('pk_num_permiso'), flagFormulario: 2, estado: estado },function($dato){
            $('#ContenidoModal').html($dato);
        });
    });

    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/gestion/permisoCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function cargarEmpleado(pk_num_dependencia) {
        $("#empleado").html("");
        $.post("{$_Parametros.url}modRH/gestion/permisoCONTROL/BuscarEmpleadoMET",{ pk_num_dependencia:""+pk_num_dependencia }, function (dato) {
            $("#empleado").html(dato);
        });
    }

    $(document).ready(function(){
        $("#buscarReporte").live('click', function() {
            var fecha_inicio = $("#fecha_inicio").val();
            var fecha_fin = $("#fecha_fin").val();
            var pk_num_empleado = $("#pk_num_empleado").val();
            var pk_num_permiso = $("#pk_num_permiso").val();
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var ind_estado = $("#ind_estado").val();
            $('#modalAncho').css( "width", "85%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var urlBuscar = '{$_Parametros.url}modRH/gestion/permisoCONTROL/BusquedaReporteMET/?pk_num_organismo='+pk_num_organismo+'&pk_num_permiso='+pk_num_permiso+'&pk_num_dependencia='+pk_num_dependencia+'&ind_estado='+ind_estado+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin+'&pk_num_empleado='+pk_num_empleado;
            $('#ContenidoModal').html('<iframe src="'+urlBuscar+'" width="100%" height="950"></iframe>');
            return false;
        });
    });

    var placeholder = "";

    $( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
    $( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

    // @see https://github.com/ivaynberg/select2/commit/6661e3
    function repoFormatResult( repo ) {
        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if ( repo.description ) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }

    function repoFormatSelection( repo ) {
        return repo.full_name;
    }

    $( "button[data-select2-open]" ).click( function() {
        $( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
    });


    $("#formulario").validate({
        submitHandler: function(form) {
            var fecha_inicio = $("#fecha_inicio").val();
            var fecha_fin = $("#fecha_fin").val();
            var pk_num_empleado = $("#pk_num_empleado").val();
            var pk_num_permiso = $("#pk_num_permiso").val();
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var ind_estado = $("#ind_estado").val();
            var valor = $("#valor").val();

            var listar='{$_Parametros.url}modRH/gestion/permisoCONTROL/BuscarListarMET';
             $.post(listar,{ pk_num_empleado: pk_num_empleado,pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia},function(dato) {
                    $("#detalle").html(
                            "<div>Organización:"+dato['ind_descripcion_empresa']+"</div>"+
                            "<div>Dependencia: "+dato['dependencia']+"</div>"+
                            "<div>Empleado: "+dato['empleado']+"</div>"
                    );
                 if(ind_estado=='PR'){
                     var estado = 'PREPARADO';
                 }
                 if(ind_estado=='AP'){
                     var estado = 'APROBADO';
                 }
                 if(ind_estado=='AN'){
                     var estado = 'ANULADO';
                 }
                 if(ind_estado=='VE'){
                     var estado = 'VERIFICADO';
                 }
                 if(fecha_inicio!=''){
                     $("#detalleBuscar").html(
                             ""+
                             "<div>N° de Permiso: "+pk_num_permiso+"</div>"+
                             "<div>Fecha de Registro: "+ fecha_inicio +" al "+ fecha_fin +"</div>"+
                             "<div>Estado: "+estado+"</div>"
                     );
                 } else {
                     $("#detalleBuscar").html(
                             ""+
                             "<div>N° de Permiso: "+pk_num_permiso+"</div>"+
                             "<div>Fecha de Registro: </div>"+
                             "<div>Estado: "+estado+"</div>"
                     );
                 }
            },'json');


            var url_listar='{$_Parametros.url}modRH/gestion/permisoCONTROL/BuscarPermisoMET';
            $.post(url_listar,{ fecha_inicio: fecha_inicio, fecha_fin: fecha_fin, pk_num_empleado: pk_num_empleado, pk_num_permiso: pk_num_permiso, pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, ind_estado: ind_estado, valor: valor },function(respuesta_post) {
                var tabla_listado = $('#datatable1').DataTable();
                tabla_listado.clear().draw();
                if(respuesta_post != -1) {
                    for(var i=0; i<respuesta_post.length; i++) {
                        var url = "{$_Parametros.url}modRH/gestion/permisoCONTROL/ReportePermisoMET/?pk_num_permiso="+respuesta_post[i].pk_num_permiso;
                        if((respuesta_post[i].ind_estado=='PR')||(respuesta_post[i].ind_estado=='AN')){
                            var botonEditar = '{if in_array('RH-01-01-06-03-04-M',$_Parametros.perfil)}<button type="button" pk_num_permiso = "' + respuesta_post[i].pk_num_permiso + '" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"  descripcion="El usuario ha modificado un permiso" title="Modificar permiso"  titulo="Modificar permiso"><i class="fa fa-edit"></i></button>{/if}';
                        } else {
                            var botonEditar = ' ';
                        }
                        if(respuesta_post[i].ind_estado=='PR'){
                            var estado = 'PREPARADO';
                        }
                        if(respuesta_post[i].ind_estado=='AP'){
                            var estado = 'APROBADO';
                        }
                        if(respuesta_post[i].ind_estado=='AN'){
                            var estado = 'ANULADO';
                        }
                        if(respuesta_post[i].ind_estado=='VE'){
                            var estado = 'VERIFICADO';
                        }
                        var fila = tabla_listado.row.add([
                            respuesta_post[i].pk_num_permiso,
                            respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_apellido1,
                            respuesta_post[i].motivo_ausencia,
                            respuesta_post[i].tipo_ausencia,
                            respuesta_post[i].fecha_registro,
                            estado,
                            '{if in_array('RH-01-01-06-03-08-V',$_Parametros.perfil)}<button type="button" pk_num_permiso = "' + respuesta_post[i].pk_num_permiso + '" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" descripcion="El usuario ha visualizado un permiso" title="Ver permiso" titulo="Ver permiso" id="ver"><i class="glyphicon glyphicon-search"></i></button>{/if}',
                            botonEditar,
                            '{if in_array('RH-01-01-06-03-05-RE',$_Parametros.perfil)}<button class="permiso logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" pk_num_permiso="{$permiso.pk_num_permiso}" data-keyboard="false" data-backdrop="static" title="Ver Permiso" titulo="Ver Permiso"><i class="md md-attach-file"></i></button>{/if}'
                        ]).draw()
                          .nodes()
                          .to$()
                        $(fila)
                                .attr('id','pk_num_permiso'+respuesta_post[i].pk_num_permiso+'');
                    }
                }

            },'json');
        }
    });

</script>