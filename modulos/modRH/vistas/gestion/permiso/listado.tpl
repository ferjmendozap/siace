</br>
<input type="hidden" id="estado" value="{$emp.estado}" />
<section class="style-default-bright">
    <h2 class="text-primary">&nbsp;Permisos</h2>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div align="right">
                    {if in_array('RH-01-01-06-02-05-BE',$_Parametros.perfil)}
                        <div class="btn-group"><a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a></div>&nbsp;
                    {/if}
                    <hr class="ruler-xl">
                    <div class="col-md-12 col-md-12">
                        <div class="well clearfix">
                            <div class="text-primary" align="left"><h4>Detalle del Listado</h4></div>
                            <div class="col-sm-6 col-sm-6" align="left">
                                <div align="left">
                                    <label>Organismo: {$emp.ind_descripcion_empresa}</label>
                                </div>
                                <div align="left">
                                    <label>Dependencia: {$emp.ind_dependencia}</label>
                                </div>
                                <div align="left">
                                    <label>Empleado: {$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</label>
                                </div>
                            </div>
                            <div class="col-sm-6 col-sm-6" align="left" id="detalleBuscar">
                                <div align="left">
                                    <label>N° de Permiso: {if ($busqueda.pk_num_permiso!='')}{$busqueda.pk_num_permiso}{/if}</label>
                                </div>
                                <div align="left">
                                    <label>Fecha de Registro: {if ($busqueda.fecha_inicio!='')}{$busqueda.fecha_inicio} al {$busqueda.fecha_fin}{/if}</label>
                                </div>
                                <div align="left">
                                    <label>Estado: {if ($busqueda.ind_estado!='')}{$busqueda.ind_estado}{/if}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr  align="center">
                        <th><i class="glyphicon glyphicon-th"></i> N° de Permiso</th>
                        <th><i class="md-person"></i> Nombre Completo</th>
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Motivo de Ausencia</th>
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Tipo de Ausencia</th>
                        <th><i class="glyphicon glyphicon-calendar"></i> Fecha de Registro</th>
                        <th><i class="glyphicon glyphicon-cog"></i> Estado</th>
                        <th width="40">Ver</th>
                        <th width="40">Editar</th>
                        <th width="50">Reporte</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=permiso from=$permisoFuncionario}
                        <tr id="pk_num_permiso{$permiso.pk_num_permiso}">
                            <td>{$permiso.pk_num_permiso}</td>
                            <td>{$permiso.ind_nombre1} {$permiso.ind_nombre2} {$permiso.ind_apellido1} {$permiso.ind_apellido2}</td>
                            <td>{$permiso.motivo_ausencia}</td>
                            <td>{$permiso.tipo_ausencia}</td>
                            <td>{$permiso.fecha_registro}</td>
                            <td>
                                {if $permiso.ind_estado=='PR'}PREPARADO{/if}
                                {if $permiso.ind_estado=='RE'}REVISADO{/if}
                                {if $permiso.ind_estado=='CO'}CONFORMADO{/if}
                                {if $permiso.ind_estado=='AP'}APROBADO{/if}
                                {if $permiso.ind_estado=='AN'}ANULADO{/if}
                                {if $permiso.ind_estado=='VE'}VERIFICADO{/if}
                            </td>
                            {if in_array('RH-01-01-06-02-01-V',$_Parametros.perfil)}
                                <td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_permiso="{$permiso.pk_num_permiso}" title="Ver permiso" titulo="Ver permiso" id="ver"><i class="glyphicon glyphicon-search"></i></button></td>
                            {else}
                                <td></td>
                            {/if}
                            {if in_array('RH-01-01-06-02-02-M',$_Parametros.perfil)}
                                <td align="center">{if ($permiso.ind_estado=='PR')}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_permiso="{$permiso.pk_num_permiso}" descipcion="El usuario ha modificado un permiso" title="Modificar permiso"  titulo="Modificar permiso"><i class="fa fa-edit"></i></button>{/if}</td>
                            {else}
                                <td></td>
                            {/if}
                            {if in_array('RH-01-01-06-02-03-RE',$_Parametros.perfil)}
                                <td align="center"><button class="permiso logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" pk_num_permiso="{$permiso.pk_num_permiso}" data-keyboard="false" data-backdrop="static" title="Ver Permiso" titulo="Ver Permiso"><i class="md md-attach-file"></i></button></td>
                            {else}
                                <td></td>
                            {/if}
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="9">
                            <button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado un permiso" data-toggle="modal" data-target="#formModal" titulo="Registrar nuevo permiso" id="nuevo"> <span class="glyphicon glyphicon-log-out"></span> Nuevo Permiso</button>&nbsp;
                            {if in_array('RH-01-01-06-02-04-RG',$_Parametros.perfil)}
                                <button id="reportePermiso" class="logsUsuario btn ink-reaction btn-raised btn-primary" descripcion="Generar Reporte de Permisos" titulo="Listado de Permisos: " title="Reporte de permisos del funcionario: {$empleado.ind_nombre1} {$empleado.ind_apellido1}" data-toggle="modal" data-target="#formModal"><span class="md-file-download"></span>Reporte de Permisos</button></a>
                            {/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- filtro -->
<div class="offcanvas">
    <form id="formulario" class="form floating-label" role="form" method="post" novalidate>
        <div id="offcanvas-filtro" class="offcanvas-pane width-12">
            <div class="offcanvas-head">
                <header>Filtro de Búsqueda</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                </div>
            </div>
            <div class="offcanvas-body">
                <div class="form-group floating-label">
                    <select id="pk_num_organismo" name="pk_num_organismo" id="pk_num_organismo" class="form-control dirty">
                        <option value="{$emp.pk_num_organismo}" selected>{$emp.ind_descripcion_empresa}</option>
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div class="form-group floating-label">
                    <select id="pk_num_dependencia" name="pk_num_dependencia" id="pk_num_empleado" class="form-control dirty">
                        <option value="{$emp.pk_num_dependencia}" selected>{$emp.ind_dependencia}</option>
                    </select>
                    <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                </div>
                <div class="form-group floating-label">
                    <select id="pk_num_empleado" name="pk_num_empleado" id="pk_num_empleado" class="form-control dirty">
                        <option value="{$empleado.pk_num_empleado}" selected>{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</option>
                    </select>
                    <label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
                </div>
                <div class="form-group">
                    <input type="text" name="pk_num_permiso" id="pk_num_permiso" class="form-control dirty">
                    <label class="control-label"><i class="glyphicon glyphicon-th"></i> N° de Permiso</label>
                </div>
                <div class="form-group floating-label">
                    <div class="input-daterange input-group" id="demo-date-range">
                        <div class="input-group-content">
                            <input type="text" class="form-control dirty" name="fechaInicio" id="fecha_inicio">
                            <label><i class="glyphicon glyphicon-calendar"></i> Fecha de Registro</label>
                        </div>
                        <span class="input-group-addon">a</span>
                        <div class="input-group-content">
                            <input type="text" class="form-control dirty" name="fechaFin" id="fecha_fin">
                            <div class="form-control-line"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group floating-label">
                    <select name="ind_estado" id="ind_estado" class="form-control dirty">
                        <option value=""></option>
                        <option value="PR">Preparado</option>
                        <option value="AP">Aprobado</option>
                        <option value="VE">Verificado</option>
                        <option value="AN">Anulado</option>
                    </select>
                    <label for="ind_estado"><i class="glyphicon glyphicon-list-alt"></i> Estado</label>
                </div>
                <br/>
                <div class="col-sm-12 col-sm-12" align="center">
                    {if in_array('RH-01-01-06-03-06-BG',$_Parametros.perfil)}
                        <div class="col-sm-6 col-sm-6">
                            <button type="button" id="buscarReporte" class="btn ink-reaction btn-flat btn-primary btn-loading-state" titulo="Reporte de Permiso" data-toggle="modal" data-target="#formModal" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Generando...">Generar Reporte<div style="top: 27px; left: 65px;" class="ink inverse"></div></button>
                        </div>
                    {/if}
                    <div class="col-sm-6 col-sm-6">
                        <button type="submit" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
                    </div>
                </div>
            </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#fecha_inicio").datepicker({
            format: 'dd/mm/yyyy',
            language: 'es',
            autoclose: true
        });
        $("#fecha_fin").datepicker({
            format: 'dd/mm/yyyy',
            language: 'es',
            autoclose: true
        });

        var $url='{$_Parametros.url}modRH/gestion/permisoCONTROL/CrearPermisoMET';
        $('#nuevo').click(function(){
            $('#modalAncho').css( "width", "95%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        var $url_modificar='{$_Parametros.url}modRH/gestion/permisoCONTROL/EditarPermisoMET';
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#modalAncho').css( "width", "85%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url_modificar,{ pk_num_permiso: $(this).attr('pk_num_permiso'), tipoListado: 1},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });

    var $urlVer='{$_Parametros.url}modRH/gestion/permisoCONTROL/VerPermisoMET';
    $('#datatable1 tbody').on( 'click', '.ver', function () {
        var estado = $("#estado").val();
        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('title'));
        $('#ContenidoModal').html("");
        $.post($urlVer,{ pk_num_permiso: $(this).attr('pk_num_permiso'), flagFormulario: 1, estado: estado},function($dato){
            $('#ContenidoModal').html($dato);
        });
    });

    $('#datatable1 tbody').on( 'click', '.permiso', function () {
        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        var pk_num_permiso = $(this).attr('pk_num_permiso');
        var urlReporte = '{$_Parametros.url}modRH/gestion/permisoCONTROL/ReportePermisoMET/?pk_num_permiso='+pk_num_permiso;
        $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
    });

    $(document).ready(function(){
        $("#buscarReporte").live('click', function() {
            var fecha_inicio = $("#fecha_inicio").val();
            var fecha_fin = $("#fecha_fin").val();
            var pk_num_empleado = $("#pk_num_empleado").val();
            var pk_num_permiso = $("#pk_num_permiso").val();
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var ind_estado = $("#ind_estado").val();
            $('#modalAncho').css( "width", "85%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var urlBuscar = '{$_Parametros.url}modRH/gestion/permisoCONTROL/BusquedaReporteMET/?pk_num_organismo='+pk_num_organismo+'&pk_num_permiso='+pk_num_permiso+'&pk_num_dependencia='+pk_num_dependencia+'&ind_estado='+ind_estado+'&fecha_inicio='+fecha_inicio+'&fecha_fin='+fecha_fin+'&pk_num_empleado='+pk_num_empleado;
            $('#ContenidoModal').html('<iframe src="'+urlBuscar+'" width="100%" height="950"></iframe>');
        });
    });

    $(document).ready(function(){
        $("#reportePermiso").live('click', function() {
            $('#modalAncho').css( "width", "85%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var urlBuscar = '{$_Parametros.url}modRH/gestion/permisoCONTROL/ListadoPermisoMET';
            $('#ContenidoModal').html('<iframe src="'+urlBuscar+'" width="100%" height="950"></iframe>');
        });
    });

    $("#formulario").validate({
        submitHandler: function(form) {
            var fecha_inicio = $("#fecha_inicio").val();
            var fecha_fin = $("#fecha_fin").val();
            var pk_num_empleado = $("#pk_num_empleado").val();
            var pk_num_permiso = $("#pk_num_permiso").val();
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var ind_estado = $("#ind_estado").val();
            var valor = $("#valor").val();
            var url_listar='{$_Parametros.url}modRH/gestion/permisoCONTROL/BuscarPermisoMET';
            $.post(url_listar,{ fecha_inicio: fecha_inicio, fecha_fin: fecha_fin, pk_num_empleado: pk_num_empleado, pk_num_permiso: pk_num_permiso, pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, ind_estado: ind_estado, valor: valor },function(respuesta_post) {
                var tabla_listado = $('#datatable1').DataTable();
                tabla_listado.clear().draw();
                if(respuesta_post != -1) {
                    for(var i=0; i<respuesta_post.length; i++) {
                        var url = "{$_Parametros.url}modRH/gestion/permisoCONTROL/ReportePermisoMET/?pk_num_permiso="+respuesta_post[i].pk_num_permiso;
                        if((respuesta_post[i].ind_estado=='PR')||(respuesta_post[i].ind_estado=='AN')){
                            var botonEditar = ' {if in_array('RH-01-01-06-02-02-M',$_Parametros.perfil)}<button type="button" pk_num_permiso = "' + respuesta_post[i].pk_num_permiso + '" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"  descripcion="El usuario ha modificado un permiso" title="Modificar permiso"  titulo="Modificar permiso"><i class="fa fa-edit"></i></button>{/if}';
                        } else {
                            var botonEditar = ' ';
                        }
                        if(respuesta_post[i].ind_estado=='PR'){
                            var estado = 'PREPARADO';
                        }
                        if(respuesta_post[i].ind_estado=='AP'){
                            var estado = 'APROBADO';
                        }
                        if(respuesta_post[i].ind_estado=='AN'){
                            var estado = 'ANULADO';
                        }
                        if(respuesta_post[i].ind_estado=='VE'){
                            var estado = 'VERIFICADO';
                        }
                        var fila = tabla_listado.row.add([
                            respuesta_post[i].pk_num_permiso,
                            respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_apellido1,
                            respuesta_post[i].motivo_ausencia,
                            respuesta_post[i].tipo_ausencia,
                            respuesta_post[i].fecha_registro,
                            estado,
                            ' {if in_array('RH-01-01-06-02-01-V',$_Parametros.perfil)}<button type="button" pk_num_permiso = "' + respuesta_post[i].pk_num_permiso + '" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" descripcion="El usuario ha modificado visualizado un permiso" title="Ver permiso" titulo="Ver permiso" id="ver"><i class="glyphicon glyphicon-search"></i></button>{/if}',
                            botonEditar,
                            '{if in_array('RH-01-01-06-02-03-RE',$_Parametros.perfil)}<a href="'+ url +'" target="_blank"><button type="button" class="verRegistro logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El Usuario ha visualizado el permiso en pdf" title="Solicitud de permiso" titulo="Solicitud de Permiso"><i class="md md-attach-file"></i></button></a>{/if}'
                        ]).draw()
                                .nodes()
                                .to$()
                        $(fila).attr('id','pk_num_permiso'+respuesta_post[i].pk_num_permiso+'');
                    }
                }
                if(fecha_inicio!=''){
                    $("#detalleBuscar").html(
                            ""+
                            "<div>N° de Permiso: "+pk_num_permiso+"</div>"+
                            "<div>Fecha de Registro: "+ fecha_inicio +" al "+ fecha_fin +"</div>"+
                            "<div>Estado: "+estado +"</div>"
                    );
                } else {
                    $("#detalleBuscar").html(
                            ""+
                            "<div>N° de Permiso: "+pk_num_permiso+"</div>"+
                            "<div>Fecha de Registro: </div>"+
                            "<div>Estado: "+estado +"</div>"
                    );
                }
            },'json');
        }
    });
</script>