<div class="form floating-label">
    <div class="col-md-12">
        <ul class="nav nav-tabs" data-toggle="tabs">
            <li class="active"><a href="#first1">Semanal</a></li>
            <li><a href="#second1">Mensual</a></li>
            <li><a href="#second2">General</a></li>
            <li><a href="#second3">Consolidado</a></li>
        </ul>
        <div class="card-body tab-content">
            <div class="tab-pane active" id="first1">
                <iframe src="{$_Parametros.url}modRH/reportes/resumenEventoCONTROL/GenerarResumenSemanalMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&centro_costo={$datos.centro_costo}&tipoNomina={$datos.tipoNomina}&cargo={$datos.cargo}&edoReg={$datos.edoReg}&sitTrab={$datos.sitTrab}&pkNumEmpleado={$datos.pkNumEmpleado}&periodo={$datos.periodo}&mes_periodo={$datos.mes_periodo}" width="100%" height="950" scrolling="auto"></iframe>
            </div>
            <div class="tab-pane" id="second1">
                <iframe src="{$_Parametros.url}modRH/reportes/resumenEventoCONTROL/GenerarResumenMensualMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&centro_costo={$datos.centro_costo}&tipoNomina={$datos.tipoNomina}&cargo={$datos.cargo}&edoReg={$datos.edoReg}&sitTrab={$datos.sitTrab}&pkNumEmpleado={$datos.pkNumEmpleado}&periodo={$datos.periodo}&mes_periodo={$datos.mes_periodo}" width="100%" height="950" scrolling="auto"></iframe>
            </div>
            <div class="tab-pane" id="second2">
                <iframe src="{$_Parametros.url}modRH/reportes/resumenEventoCONTROL/GenerarResumenGeneralMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&centro_costo={$datos.centro_costo}&tipoNomina={$datos.tipoNomina}&cargo={$datos.cargo}&edoReg={$datos.edoReg}&sitTrab={$datos.sitTrab}&pkNumEmpleado={$datos.pkNumEmpleado}&periodo={$datos.periodo}&mes_periodo={$datos.mes_periodo}" width="100%" height="950" scrolling="auto"></iframe>
            </div>
            <div class="tab-pane" id="second3">
                <iframe src="{$_Parametros.url}modRH/reportes/resumenEventoCONTROL/GenerarResumenConsolidadoMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&centro_costo={$datos.centro_costo}&tipoNomina={$datos.tipoNomina}&cargo={$datos.cargo}&edoReg={$datos.edoReg}&sitTrab={$datos.sitTrab}&pkNumEmpleado={$datos.pkNumEmpleado}&periodo={$datos.periodo}&mes_periodo={$datos.mes_periodo}" width="100%" height="950" scrolling="auto"></iframe>
            </div>
        </div>
    </div>
</div>