<div class="form floating-label">
    <br/>
    <section class="style-default-bright">
        <h2 class="text-primary">&nbsp;Antecedente de Servicio</h2>
        <br/>
        <div class="well clearfix">
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                        {foreach item=org from=$listadoOrganismo}
                            {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {else}
                                <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div id="dependencia">
                    <div class="form-group floating-label">
                        <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty" onchange="cargarCentroCosto(this.value)">
                            <option value="0">&nbsp;</option>
                            {foreach item=dep from=$listadoDependencia}
                                {if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
                                    <option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-home"></i> Dependencia</label>
                    </div>
                </div>
                <div id="centroCosto">
                    <div class="form-group floating-label">
                        <select id="centro_costo" name="centro_costo" class="form-control dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=centro from=$centroCosto}
                                <option value="{$centro.pk_num_centro_costo}">{$centro.ind_descripcion_centro_costo}</option>
                            {/foreach}
                        </select>
                        <label for="centro_costo"><i class="glyphicon glyphicon-briefcase"></i> Centro de Costo</label>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="edo_reg" name="edo_reg" class="form-control dirty">
                        <option value="1">ACTIVO</option>
                        <option value="0">INACTIVO</option>
                    </select>
                    <label><i class="glyphicon glyphicon-briefcase"></i> Estado del Registro</label>
                </div>
                <div class="form-group floating-label">
                    <select id="sit_trab" name="sit_trab" class="form-control dirty">
                        <option value="1">ACTIVO</option>
                        <option value="0">INACTIVO</option>
                    </select>
                    <label><i class="glyphicon glyphicon-briefcase"></i> Situación de Trabajo</label>
                </div>
                <div id="empleado">
                    <div class="form-group floating-label">
                        <select id="pk_num_empleado" name="pk_num_empleado" id="s2id_single" class="select2-container form-control select2 dirty">
                            <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                                <option value="">&nbsp;</option>
                                {foreach item=empleado from=$datos}
                                    <option value="{$empleado.pk_num_empleado}">{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</option>
                                {/foreach}
                        </select>
                        <label for="pk_num_empleado"><i class="md md-person"></i> Buscar</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="well clearfix">
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="tipo_entidad" name="tipo_entidad" class="form-control dirty">
                        <option value=""></option>
                        {foreach item=entidad from=$tipoEntidad}
                        <option value="{$entidad.pk_num_miscelaneo_detalle}">{$entidad.ind_nombre_detalle}</option>
                        {/foreach}
                    </select>
                    <label><i class="glyphicon glyphicon-home"></i> Tipo de Entidad</label>
                </div>
                <div class="form-group floating-label">
                    <select id="areaExperiencia" name="areaExperiencia" class="form-control dirty">
                        <option value=""></option>
                        {foreach item=area from=$areaExperiencia}
                            <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                        {/foreach}
                    </select>
                    <label><i class="glyphicon glyphicon-th-list"></i> Área de Experiencia</label>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="motivo_cese" name="motivo_cese" class="form-control dirty">
                        <option value=""></option>
                        {foreach item=motivo from=$motivoCese}
                            <option value="{$motivo.pk_num_motivo_cese}">{$motivo.ind_nombre_cese}</option>
                        {/foreach}
                    </select>
                    <label><i class="glyphicon glyphicon-ban-circle"></i> Motivo de Cese</label>
                </div>
                <div class="form-group" style="width:100%">
                    <div class="input-daterange input-group" id="demo-date-range">
                        <div class="input-group-content">
                            <input type="text" class="fecha form-control dirty" name="fechaInicio" id="fechaInicio">
                            <label><i class="glyphicon glyphicon-calendar"></i> Fecha de Ingreso</label>
                        </div>
                        <span class="input-group-addon">a</span>
                        <div class="input-group-content">
                            <input type="text" class="fecha form-control dirty" name="fechaFin" id="fechaFin">
                            <div class="form-control-line"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-sm-12" align="center">
            <button type="submit" onclick="buscarTrabajador()" titulo="Listado de Empleados" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
        </div>
<br/><br/><br/>
        <div>
            <table id="datatable1" class="table table-striped table-hover">
                <thead>
                <tr align="center">
                    <th><i class="md-person"></i> Código</th>
                    <th><i class="md-person"></i> Nombre Completo</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i> Nro. Documento</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i>Fecha de Ingreso</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i>Dependencia</th>
                    <th> Seleccionar</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=dato from=$datos}
                    <tr>
                        <td>{$dato.pk_num_empleado}</td>
                        <td>{$dato.ind_nombre1} {$dato.ind_nombre2} {$dato.ind_apellido1} {$dato.ind_apellido2}</td>
                        <td>{$dato.ind_cedula_documento}</td>
                        <td>{$dato.fecha_ingreso}</td>
                        <td>{$dato.ind_dependencia}</td>
                        <td><button type="button" pk_num_empleado="{$dato.pk_num_empleado}" class="cargarAntecedente logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Antecedente de Servicio" data-toggle="modal" data-target="#formModal" titulo="Antecedente de Servicio"><i class="glyphicon glyphicon-download"></i></button></td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(".fecha").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });

    var $url='{$_Parametros.url}modRH/reportes/antecedenteServicioCONTROL/GenerarAntecedenteMET';
    $('#datatable1 tbody').on( 'click', '.cargarAntecedente', function () {
        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        $.post($url,{ pk_num_empleado: $(this).attr('pk_num_empleado')},function($dato){
            $('#ContenidoModal').html($dato);
        });
    });

    function buscarTrabajador()
    {
        var pk_num_organismo = $("#pk_num_organismo").val();
        var pk_num_dependencia = $("#pk_num_dependencia").val();
        var centro_costo = $("#centro_costo").val();
        var edo_reg = $("#edo_reg").val();
        var sit_trab = $("#sit_trab").val();
        var pk_num_empleado = $("#pk_num_empleado").val();
        var tipo_entidad = $("#tipo_entidad").val();
        var areaExperiencia = $("#areaExperiencia").val();
        var motivo_cese = $("#motivo_cese").val();
        var fechaInicio = $("#fechaInicio").val();
        var fechaFin = $("#fechaFin").val();
        var url_listar='{$_Parametros.url}modRH/reportes/antecedenteServicioCONTROL/ListarEmpleadoMET';
        $.post(url_listar,{ pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, centro_costo: centro_costo, edo_reg: edo_reg, sit_trab: sit_trab, pk_num_empleado: pk_num_empleado, tipo_entidad: tipo_entidad, areaExperiencia: areaExperiencia, motivo_cese: motivo_cese, fechaInicio: fechaInicio, fechaFin: fechaFin },function(respuesta_post) {
            var tabla_listado = $('#datatable1').DataTable();
            tabla_listado.clear().draw();
            if(respuesta_post != -1) {
                for(var i=0; i<respuesta_post.length; i++) {
                    var botonCargar = '<button type="button" pk_num_empleado = "' + respuesta_post[i].pk_num_empleado + '" class="cargarAntecedente logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" titulo="Antecedente de Servicio" title="Antecedente de Servicio" descripcion="El usuario ha buscado un empleado"><i class="glyphicon glyphicon-download"></i></button>';
                    tabla_listado.row.add([
                        respuesta_post[i].pk_num_empleado,
                        respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_nombre2+' '+ respuesta_post[i].ind_apellido1+' '+ respuesta_post[i].ind_apellido2,
                        respuesta_post[i].ind_cedula_documento,
                        respuesta_post[i].fecha_ingreso,
                        respuesta_post[i].ind_dependencia,
                        botonCargar
                    ]).draw()
                            .nodes()
                            .to$()
                }
            }

        },'json');
    }
    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/reportes/antecedenteServicioCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function cargarCentroCosto(pk_num_dependencia) {
        var pk_num_organismo = $("#pk_num_organismo").val();
        $("#centroCosto").html("");
        $.post("{$_Parametros.url}modRH/reportes/antecedenteServicioCONTROL/BuscarCentroCostoMET",{ pk_num_dependencia:""+pk_num_dependencia, pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#centroCosto").html(dato);
        });
    }

    var placeholder = "";

    $( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
    $( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

    // @see https://github.com/ivaynberg/select2/commit/6661e3
    function repoFormatResult( repo ) {
        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if ( repo.description ) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }

    function repoFormatSelection( repo ) {
        return repo.full_name;
    }

    $( "button[data-select2-open]" ).click( function() {
        $( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
    });
</script>