<div class="col-md-12">
    <ul class="nav nav-tabs" data-toggle="tabs">
        <li class="active"><a href="#first1">Actuación en el Organismo</a></li>
        <li><a href="#second1">Experiencia Laboral</a></li>
        <li><a href="#second2">Consolidado</a></li>
    </ul>
    <div class="card-body tab-content">
        <div class="tab-pane active" id="first1">
           <iframe src="{$_Parametros.url}modRH/reportes/antecedenteServicioCONTROL/AntecendenteOrganismoMET/?pk_num_empleado={$empleado.pkNumEmpleado}" width="100%" height="950"></iframe>
        </div>
        <div class="tab-pane" id="second1">
            <iframe src="{$_Parametros.url}modRH/reportes/antecedenteServicioCONTROL/ReporteExperienciaLaboralMET/?pk_num_empleado={$empleado.pkNumEmpleado}" width="100%" height="950"></iframe>
        </div>
        <div class="tab-pane" id="second2">
            <iframe src="{$_Parametros.url}modRH/reportes/antecedenteServicioCONTROL/ReporteConsolidadoMET/?pk_num_empleado={$empleado.pkNumEmpleado}" width="100%" height="950"></iframe>
        </div>
    </div>
</div>
