<div class="form floating-label">
    <br/>
    <section class="style-default-bright">
        <h2 class="text-primary">&nbsp;Reporte Detallado de Útiles Escolares</h2>
        <br/>
        <div class="well clearfix">
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                        {foreach item=org from=$listadoOrganismo}
                            {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {else}
                                <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div id="dependencia">
                    <div class="form-group floating-label">
                        <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty" onchange="cargarEmpleado(this.value)">
                            <option value="">&nbsp;</option>
                            {foreach item=dep from=$listadoDependencia}
                                {if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
                                    <option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-home"></i> Dependencia</label>
                    </div>
                </div>
                <div id="empleado">
                    <div class="form-group floating-label">
                        <select id="pk_num_empleado" name="pk_num_empleado" class="form-control dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=empleado from=$listarEmpleado}
                                <option value="{$empleado.pk_num_empleado}">{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</option>
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-user"></i> Empleado</label>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="pk_num_tipo_nomina" name="pk_num_tipo_nomina" class="form-control dirty" onchange="cargarPeriodo(this.value)">
                        <option value="">&nbsp;</option>
                        {foreach item=nomina from=$listarNomina}
                            <option value="{$nomina.pk_num_tipo_nomina}">{$nomina.ind_nombre_nomina}</option>
                        {/foreach}
                    </select>
                    <label for="pk_num_tipo_nomina"><i class="glyphicon glyphicon-list-alt"></i> Nómina</label>
                </div>
                <div id="periodo">
                    <div class="form-group floating-label">
                        <select id="periodoLista" name="periodoLista" class="form-control dirty">
                            {foreach item=periodo from=$listarPeriodo}
                                <option value="{$periodo.periodo}">{$periodo.periodo}</option>
                            {/foreach}
                        </select>
                        <label for="periodo"><i class="glyphicon glyphicon-calendar"></i> Periodo</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-sm-12" align="center">
            <button type="submit" id="utilesGeneral" data-toggle="modal" data-target="#formModal" titulo="Listado de Útiles Escolares Detallado" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
        </div>
        <br/><br/><br/>
</div>
</section>
<script type="text/javascript">

    function cargarEmpleado(pk_num_dependencia) {
        $("#empleado").html("");
        $.post("{$_Parametros.url}modRH/reportes/utilesEscolaresCONTROL/BuscarEmpleadoMET",{ pk_num_dependencia:""+pk_num_dependencia }, function (dato) {
            $("#empleado").html(dato);
        });
    }

    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/reportes/resumenEventoCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function cargarPeriodo(pk_num_tipo_nomina) {
        var pk_num_organismo = $("#pk_num_organismo").val();
        $("#periodo").html("");
        $.post("{$_Parametros.url}modRH/reportes/utilesEscolaresCONTROL/BuscarPeriodoMET",{ pk_num_tipo_nomina:""+pk_num_tipo_nomina, pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#periodo").html(dato);
        });
    }

    $(document).ready(function() {
        $('#utilesGeneral').click(function () {
            $('#modalAncho').css( 'width', '85%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var pk_num_empleado = $("#pk_num_empleado").val();
            var pk_num_tipo_nomina = $("#pk_num_tipo_nomina").val();
            var periodo = $("#periodoLista").val();
            var urlReporte = '{$_Parametros.url}modRH/reportes/utilesEscolaresCONTROL/GenerarUtilesDetalladoMET/?pk_num_organismo='+pk_num_organismo+'&pk_num_dependencia='+pk_num_dependencia+'&pk_num_empleado='+pk_num_empleado+'&pk_num_tipo_nomina='+pk_num_tipo_nomina+'&periodo='+periodo;
            $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
        });
    });


</script>