<div class="form floating-label">
    <br/>
    <section class="style-default-bright">
        <h2 class="text-primary">&nbsp;Cese/Reingreso</h2>
        <br/>
        <div class="well clearfix">
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                        {foreach item=org from=$listadoOrganismo}
                            {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {else}
                                <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div id="dependencia">
                    <div class="form-group floating-label">
                        <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control" onchange="cargarCentroCosto(this.value)">
                            <option value="">&nbsp;</option>
                            {foreach item=dep from=$listadoDependencia}
                                {if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
                                    <option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                    </div>
                </div>
                <div id="centroCosto">
                    <div class="form-group floating-label">
                        <select id="centro_costo" name="centro_costo" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=centro from=$centroCosto}
                                <option value="{$centro.pk_num_centro_costo}">{$centro.ind_descripcion_centro_costo}</option>
                            {/foreach}
                        </select>
                        <label for="centro_costo"><i class="glyphicon glyphicon-home"></i> Centro de Costo</label>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div id="empleado">
                    <div class="form-group floating-label">
                        <select id="pk_num_empleado" name="pk_num_empleado" id="s2id_single" class="select2-container form-control select2">
                            <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                                <option value="">&nbsp;</option>
                                {foreach item=empleado from=$datos}
                                    <option value="{$empleado.pk_num_empleado}">{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</option>
                                {/foreach}
                        </select>
                        <label for="pk_num_empleado"><i class="md md-person"></i>Buscar</label>
                    </div>
                </div>
                <div class="form-group floating-label">
                    <select id="tipo" name="tipo" class="form-control dirty">
                        {foreach item=tipo from=$tipoCese}
                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                        {/foreach}
                    </select>
                    <label><i class="glyphicon glyphicon-briefcase"></i> Tipo</label>
                </div>
                <div class="form-group" style="width:100%">
                    <div class="input-daterange input-group" id="demo-date-range">
                        <div class="input-group-content">
                            <input type="text" class="fecha form-control" name="fechaInicio" id="fechaInicio">
                            <label><i class="glyphicon glyphicon-calendar"></i> Fecha</label>
                        </div>
                        <span class="input-group-addon">a</span>
                        <div class="input-group-content">
                            <input type="text" class="fecha form-control" name="fechaFin" id="fechaFin">
                            <div class="form-control-line"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-sm-12" align="center">
            <button type="submit" id="buscarTrabajador" data-toggle="modal" data-target="#formModal" titulo="Listado de Cese/Reingreso" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk" ></span> Buscar</button>
        </div>
<br/><br/><br/>
    </div>
</section>
<script type="text/javascript">
    $(".fecha").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });

    $(document).ready(function() {
        $('#buscarTrabajador').click(function () {
            $('#modalAncho').css( 'width', '85%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var centro_costo = $("#centro_costo").val();
            var pk_num_empleado = $("#pk_num_empleado").val();
            var tipo = $("#tipo").val();
            var fechaInicio = $("#fechaInicio").val();
            var fechaFin = $("#fechaFin").val();
            var urlReporte = '{$_Parametros.url}modRH/reportes/ceseReingresoCONTROL/GenerarReporteCeseMET/?pk_num_organismo='+pk_num_organismo+'&pk_num_dependencia='+pk_num_dependencia+'&centro_costo='+centro_costo+'&pk_num_empleado='+pk_num_empleado+'&tipo='+tipo+'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin;
            $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
        });
    });

    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/reportes/cumpleanioCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function cargarCentroCosto(pk_num_dependencia) {
        var pk_num_organismo = $("#pk_num_organismo").val();
        $("#centroCosto").html("");
        $.post("{$_Parametros.url}modRH/reportes/cumpleanioCONTROL/BuscarCentroCostoMET",{ pk_num_dependencia:""+pk_num_dependencia, pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#centroCosto").html(dato);
        });
    }

    var placeholder = "";

    $( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
    $( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

    // @see https://github.com/ivaynberg/select2/commit/6661e3
    function repoFormatResult( repo ) {
        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if ( repo.description ) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }

    function repoFormatSelection( repo ) {
        return repo.full_name;
    }

    $( "button[data-select2-open]" ).click( function() {
        $( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
    });
</script>