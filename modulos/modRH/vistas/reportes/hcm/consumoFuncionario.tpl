<div class="form floating-label">
    <br/>
    <section class="style-default-bright">
        <h2 class="text-primary">&nbsp;Listar Beneficio</h2>
        <br/>
        <div class="well clearfix">
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                        {foreach item=org from=$listadoOrganismo}
                            {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {else}
                                <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div id="dependencia">
                    <div class="form-group floating-label">
                        <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=dep from=$listadoDependencia}
                                {if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
                                    <option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-home"></i> Dependencia</label>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="pk_num_ayuda_global" name="pk_num_ayuda_global" class="form-control dirty">
                        {foreach item=ayuda from=$ayudaMedica}
                            <option value="{$ayuda.pk_num_ayuda_global}">{$ayuda.ind_descripcion}</option>
                        {/foreach}
                    </select>
                    <label for="pk_num_ayuda_global"><i class="glyphicon glyphicon-heart"></i> Ayuda</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-sm-12" align="center">
            <button type="submit" onclick="buscarTrabajador()" titulo="Listado de Empleados" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
        </div>
        <br/><br/><br/>
        <div>
            <table id="datatable1" class="table table-striped table-hover">
                <thead>
                <tr align="center">
                    <th><i class="md-person"></i> N°</th>
                    <th><i class="md-person"></i> Cédula</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i> Funcionario</th>
                    <th> Imprimir</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=listado from=$listarEmpleado}
                    <tr>
                        <td>{$listado.pk_num_empleado}</td>
                        <td>{$listado.ind_cedula_documento|number_format:0:'':'.'}</td>
                        <td>{$listado.ind_nombre1} {$listado.ind_nombre2} {$listado.ind_apellido1} {$listado.ind_apellido2}</td>
                        <td><button type="button" pk_num_empleado="{$listado.pk_num_empleado}"  class="cargarBeneficio logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" data-toggle="modal" data-target="#formModal" titulo="Estado de Cuenta por Beneficio" title="Estado de Cuenta por Beneficio"><i class="glyphicon glyphicon-download"></i></button></td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>

<br/><br/><br/>
    </div>
</section>
<script type="text/javascript">

    $('#datatable1 tbody').on( 'click', '.cargarBeneficio', function () {
        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        var pk_num_empleado = $(this).attr('pk_num_empleado');
        var pk_num_ayuda_global = $("#pk_num_ayuda_global").val();
        var urlReporte = '{$_Parametros.url}modRH/reportes/hcmCONTROL/GenerarConsumoFuncionarioMET/?pk_num_empleado='+pk_num_empleado+'&pk_num_ayuda_global='+pk_num_ayuda_global;
        $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
    });

    function buscarTrabajador()
    {
        var pk_num_organismo = $("#pk_num_organismo").val();
        var pk_num_ayuda_global = $("#pk_num_ayuda_global").val();
        var pk_num_dependencia = $("#pk_num_dependencia").val();
        var url_listar='{$_Parametros.url}modRH/reportes/hcmCONTROL/ListarEmpleadoMET';
        $.post(url_listar,{ pk_num_organismo: pk_num_organismo, pk_num_ayuda_global: pk_num_ayuda_global, pk_num_dependencia: pk_num_dependencia},function(respuesta_post) {
            var tabla_listado = $('#datatable1').DataTable();
            tabla_listado.clear().draw();
            if(respuesta_post != -1) {
                for(var i=0; i<respuesta_post.length; i++) {
                    var botonCargar = '<button type="button" pk_num_empleado = "' + respuesta_post[i].pk_num_empleado + '" class="cargarBeneficio logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" titulo="Estado de Cuenta por Beneficio" title="Estado de Cuenta por Beneficio" descripcion="El usuario ha visualizado un beneficio"><i class="glyphicon glyphicon-download"></i></button>';
                    tabla_listado.row.add([
                        respuesta_post[i].pk_num_empleado,
                        respuesta_post[i].ind_cedula_documento,
                        respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_nombre2+' '+ respuesta_post[i].ind_apellido1+' '+ respuesta_post[i].ind_apellido2,
                        botonCargar
                    ]).draw()
                        .nodes()
                            .to$()
                }
            }

        },'json');
    }

    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/reportes/resumenEventoCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }
</script>