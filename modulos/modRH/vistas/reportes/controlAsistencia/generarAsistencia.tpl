<div class="col-md-12">
    <ul class="nav nav-tabs" data-toggle="tabs">
        <li class="active"><a href="#first1">Asistencias</a></li>
        <li><a href="#second1">Permisos</a></li>
    </ul>
    <div class="card-body tab-content">
        <div class="tab-pane active" id="first1">
           <iframe src="{$_Parametros.url}modRH/reportes/controlAsistenciaCONTROL/GenerarControlAsistenciaMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&tipoNomina={$datos.tipoNomina}&indCedulaDocumento={$datos.indCedulaDocumento}&simbolo={$datos.simbolo}&empleado={$datos.empleado}&cargo={$datos.cargo}&edoReg={$datos.edoReg}&sitTrab={$datos.sitTrab}&pk_num_empleado={$datos.pkNumEmpleado}&ordenar={$datos.ordenar}&fechaInicio={$datos.fechaInicio}&fechaFin={$datos.fechaFin}&tipoAusencia={$datos.tipoAusencia}"" width="100%" height="950"></iframe>
        </div>
        <div class="tab-pane" id="second1">
            <iframe src="{$_Parametros.url}modRH/reportes/controlAsistenciaCONTROL/GenerarControlPermisoMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&tipoNomina={$datos.tipoNomina}&indCedulaDocumento={$datos.indCedulaDocumento}&simbolo={$datos.simbolo}&empleado={$datos.empleado}&cargo={$datos.cargo}&edoReg={$datos.edoReg}&sitTrab={$datos.sitTrab}&pk_num_empleado={$datos.pkNumEmpleado}&ordenar={$datos.ordenar}&fechaInicio={$datos.fechaInicio}&fechaFin={$datos.fechaFin}&tipoAusencia={$datos.tipoAusencia}" width="100%" height="950"></iframe>
        </div>
    </div>
</div>
