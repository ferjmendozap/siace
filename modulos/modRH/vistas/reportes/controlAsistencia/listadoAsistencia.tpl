<div class="form">
    <br/>
    <section class="style-default-bright">
        <h2 class="text-primary">&nbsp;Control de Asistencia</h2>
        <br/>
        <div class="well clearfix">
            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                        {foreach item=org from=$listadoOrganismo}
                            {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {else}
                                <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div id="dependencia">
                    <div class="form-group">
                        <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=dep from=$listadoDependencia}
                                {if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
                                    <option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                    </div>
                </div>
                <div class="form-group">
                    <select id="tipo_nomina" name="tipo_nomina" class="form-control">
                        <option value="">&nbsp;</option>
                        {foreach item=nom from=$nomina}
                            <option value="{$nom.pk_num_tipo_nomina}">{$nom.ind_nombre_nomina}</option>
                        {/foreach}
                    </select>
                    <label for="tipo_nomina"><i class="glyphicon glyphicon-briefcase"></i> Tipo de Nómina</label>
                </div>
                <div class="form-group">
                    <input type="text" name="ind_cedula_documento" id="ind_cedula_documento" class="form-control">
                    <label class="control-label">N° de Documento</label>
                </div>
                <div class="form-group">
                    <select id="cargo" name="cargo" class="form-control">
                        <option value="">&nbsp;</option>
                        {foreach item=cargo from=$listarCargos}
                            <option value="{$cargo.pk_num_puestos}">{$cargo.ind_descripcion_cargo}</option>
                        {/foreach}
                    </select>
                    <label for="cargo"><i class="glyphicon glyphicon-briefcase"></i> Cargo al cual reporta</label>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <select id="edo_reg" name="edo_reg" class="form-control dirty">
                        <option value="1">ACTIVO</option>
                        <option value="0">INACTIVO</option>
                    </select>
                    <label><i class="glyphicon glyphicon-briefcase"></i> Estado del Registro</label>
                </div>
                <div class="form-group">
                    <select id="sit_trab" name="sit_trab" class="form-control dirty">
                        <option value="1">ACTIVO</option>
                        <option value="0">INACTIVO</option>
                    </select>
                    <label><i class="glyphicon glyphicon-briefcase"></i> Situación de Trabajo</label>
                </div>
                <div id="empleado">
                    <div class="form-group">
                        <select id="pk_num_empleado" name="pk_num_empleado" id="s2id_single" class="select2-container form-control select2">
                            <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                                <option value="">&nbsp;</option>
                                {foreach item=empleado from=$datos}
                                    <option value="{$empleado.pk_num_empleado}">{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</option>
                                {/foreach}
                        </select>
                        <label for="pk_num_empleado"><i class="md md-person"></i>Buscar</label>
                    </div>
                </div>
                <div class="form-group">
                    <select id="ordenar" name="ordenar" class="form-control">
                        <option value="0">&nbsp;</option>
                        <option value="1">Persona</option>
                        <option value="2">Nombre Completo</option>
                        <option value="3">N° de Documento</option>
                        <option value="4">Fecha de Ingreso</option>
                        <option value="5">Cargo</option>
                    </select>
                    <label for="ordenar"><i class="glyphicon glyphicon-briefcase"></i> Ordenar por:</label>
                </div>
                <div class="form-group" style="width:100%">
                    <div class="input-daterange input-group" id="demo-date-range">
                        <div class="input-group-content">
                            <input type="text" class="fecha form-control" name="fechaInicio" id="fechaInicio">
                            <label><i class="glyphicon glyphicon-calendar"></i> Fecha de Eventos</label>
                        </div>
                        <span class="input-group-addon">a</span>
                        <div class="input-group-content">
                            <input type="text" class="fecha form-control" name="fechaFin" id="fechaFin">
                            <div class="form-control-line"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <select id="tipo_ausencia" name="tipo_ausencia" class="form-control">
                        <option value="">&nbsp;</option>
                        {foreach item=tip from=$tipoAusencia}
                            <option value="{$tip.pk_num_miscelaneo_detalle}">{$tip.ind_nombre_detalle}</option>
                        {/foreach}
                    </select>
                    <label for="tipo_ausencia"><i class="glyphicon glyphicon-triangle-right"></i> Tipo de Ausencia</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-sm-12" align="center">
            <button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha visualizado el reporte de control de asistencia" data-toggle="modal" data-target="#formModal" titulo="Control de Asistencia" id="asistencia"> <span class="glyphicon glyphicon-log-out"></span> Buscar</button>
        </div>
<br/><br/><br/>
    </div>
</section>
<script type="text/javascript">
    $(".fecha").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });

    var $url='{$_Parametros.url}modRH/reportes/controlAsistenciaCONTROL/GenerarAsistenciaMET';
    $('#asistencia').click(function(){



        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        var pkNumOrganismo = $("#pk_num_organismo").val();
        var pk_num_dependencia = $("#pk_num_dependencia").val();
        var tipo_nomina = $("#tipo_nomina").val();
        var ind_cedula_documento = $("#ind_cedula_documento").val();
        var cargo = $("#cargo").val();
        var edo_reg = $("#edo_reg").val();
        var sit_trab = $("#sit_trab").val();
        var pk_num_empleado = $("#pk_num_empleado").val();
        var ordenar = $("#ordenar").val();
        var fechaInicio = $("#fechaInicio").val();
        var fechaFin = $("#fechaFin").val();
        var tipo_ausencia = $("#tipo_ausencia").val();

        if(fechaInicio=='' || fechaFin==''){
            swal("Error", "Debe Indicar la fecha de los Eventos", "error");
            return false;
        }
        else{
            $.post($url,{ pk_num_organismo: pkNumOrganismo, pk_num_dependencia: pk_num_dependencia, tipo_nomina: tipo_nomina, ind_cedula_documento: ind_cedula_documento, cargo: cargo, edo_reg: edo_reg, sit_trab: sit_trab, pk_num_empleado: pk_num_empleado, ordenar: ordenar, fechaInicio: fechaInicio, fechaFin: fechaFin, tipo_ausencia: tipo_ausencia },function($dato){
                $('#ContenidoModal').html($dato);
            });
        }


    });


    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/reportes/controlAsistenciaCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    var placeholder = "";

    $( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
    $( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

    // @see https://github.com/ivaynberg/select2/commit/6661e3
    function repoFormatResult( repo ) {
        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if ( repo.description ) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }

    function repoFormatSelection( repo ) {
        return repo.full_name;
    }

    $( "button[data-select2-open]" ).click( function() {
        $( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
    });
</script>