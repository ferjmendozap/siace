<div class="form floating-label">
    <div class="col-md-12">
        <ul class="nav nav-tabs" data-toggle="tabs">
            <li class="active"><a href="#first1">Utilizacion</a></li>
            <li><a href="#second1">Pendientes</a></li>
            <li><a href="#second2">Resumen</a></li>
            <li><a href="#second3">General</a></li>
        </ul>
        <div class="card-body tab-content">
            <div class="tab-pane active" id="first1">
                <iframe src="{$_Parametros.url}modRH/reportes/disfruteVacacionesCONTROL/GenerarUtilizacionMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&tipo_nomina={$datos.tipoNomina}&sit_trab={$datos.sitTrab}&pk_num_empleado={$datos.pkNumEmpleado}&periodo={$datos.periodo}&empleado={$datos.empleado}" width="100%" height="950" scrolling="auto"></iframe>
            </div>
            <div class="tab-pane" id="second1">
                <iframe src="{$_Parametros.url}modRH/reportes/disfruteVacacionesCONTROL/GenerarPendienteMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&tipo_nomina={$datos.tipoNomina}&sit_trab={$datos.sitTrab}&pk_num_empleado={$datos.pkNumEmpleado}&periodo={$datos.periodo}&empleado={$datos.empleado}" width="100%" height="950" scrolling="auto"></iframe>
            </div>
            <div class="tab-pane" id="second2">
                <iframe src="{$_Parametros.url}modRH/reportes/disfruteVacacionesCONTROL/GenerarResumenVacacionMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&tipo_nomina={$datos.tipoNomina}&sit_trab={$datos.sitTrab}&pk_num_empleado={$datos.pkNumEmpleado}&periodo={$datos.periodo}&empleado={$datos.empleado}" width="100%" height="950" scrolling="auto"></iframe>
            </div>
            <div class="tab-pane" id="second3">
                <iframe src="{$_Parametros.url}modRH/reportes/disfruteVacacionesCONTROL/GenerarResumenGeneralMET/?pk_num_organismo={$datos.pk_num_organismo}&pk_num_dependencia={$datos.pk_num_dependencia}&tipo_nomina={$datos.tipoNomina}&sit_trab={$datos.sitTrab}&pk_num_empleado={$datos.pkNumEmpleado}&periodo={$datos.periodo}&empleado={$datos.empleado}" width="100%" height="950" scrolling="auto"></iframe>
            </div>
        </div>
    </div>
</div>