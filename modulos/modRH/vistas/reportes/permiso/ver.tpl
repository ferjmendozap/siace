<div class="form floating-label">
	<div class="modal-body" >
		<h3 class="text-primary">&nbsp;Datos del Permiso</h3>
		<hr class="ruler-xl">
		{foreach item=per from=$permiso}
		<div class="col-md-6 col-sm-6">
			<div class="form-group floating-label">
				<input type="text" class="form-control" value="{$per.pk_num_permiso}" size="45%" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-th"></i> N° de Permiso </label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" value="{$per.ind_nombre1} {$per.ind_nombre2} {$per.ind_apellido1} {$per.ind_apellido2}" size="45%" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-user"></i> Empleado</label>
			</div>
			{foreach item=fun from=$funcionario}
				<div class="form-group floating-label">
					<input type="text" class="form-control" value="{$fun.ind_nombre1} {$fun.ind_nombre2} {$fun.ind_apellido1} {$fun.ind_apellido2}" size="45%" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-user"></i> Aprueba</label>
				</div>
			{/foreach}
			<div class="form-group floating-label">
				<input type="text" class="form-control" value="{$per.motivo_ausencia}" size="45%" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-triangle-right"></i> Motivo de Ausencia</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" value="{$per.tipo_ausencia}" size="45%" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-triangle-right"></i> Tipo de Ausencia</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" value="{$per.ind_periodo_contable}" size="45%" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-calendar"></i> Periodo Contable</label>
			</div>
			<div class="form-group floating-label">
				<input type="text" class="form-control" value="{$per.fecha_ingreso}" size="45%" disabled="disabled">
				<label for="regular2"><i class="glyphicon glyphicon-calendar"></i> Fecha de Ingreso</label>
			</div>
		</div>
		<div class="col-md-6 col-sm-6">
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" value="{$per.fecha_salida}" size="45%" disabled="disabled">
					<label><i class="glyphicon glyphicon-calendar"></i> Fecha del Permiso</label>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" value="{$per.fecha_entrada}" size="45%" disabled="disabled">
					<label></label>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-4 col-sm-8">
					<div class="form-group">
						<input type="text" class="form-control" value="{$datoHora.horaInicio}:{$datoHora.minutoInicio}" disabled="disabled">
						<label class="control-label"><i class="glyphicon glyphicon-time"></i> Hora Inicial</label>
					</div>
				</div>
				<div class="col-md-2 col-sm-3">
					<div class="form-group floating-label">
						<select class="form-control" disabled="disabled">
							<option>{$datoHora.horarioInicio}</option>
						</select>
						<label for="horario_inicio"></label>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="form-group">
						<input type="text" class="form-control" value="{$datoHora.horaFin}:{$datoHora.minutoFin}" disabled="disabled">
						<label class="control-label">Hora Final</label>
					</div>
				</div>
				<div class="col-md-2 col-sm-3">
					<div class="form-group floating-label">
						<select class="form-control" disabled="disabled">
							<option>{$datoHora.horarioFin}</option>
						</select>
						<label></label>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div class="well clearfix">
					<table width="100%">
						<tr>
							<td class="text-primary"><h4>Total</h4></td>
						</tr>
						<tr>
							<td>Dias: <input type="text" value="{$per.num_total_dia}" disabled size="7"/></td>
							<td>Horas: <input type="text" value="{$per.num_total_hora}" disabled size="7"/></td>
							<td>Minutos: <input type="text" value="{$per.num_total_minuto}" disabled size="7"/></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div class="form-group">
					<textarea class="form-control" rows="2" disabled="disabled">{ucfirst($per.ind_motivo)}</textarea>
					<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Descripción del Motivo</label>
				</div>
			</div>
			<div class="col-md-12 col-sm-12">
				<div class="col-md-4 col-sm-6">
					<div class="checkbox checkbox-styled"><label><input type="checkbox"  name="num_remunerado" id="num_remunerado" {if ($per.num_remuneracion==1)}checked{/if} {if (($per.ind_estado=='Aprobado')||($per.ind_estado=='Verificado'))}disabled{/if} /><span>¿Remunerado?</span></div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="checkbox checkbox-styled"><label><input type="checkbox" name="num_justificativo" id="num_justificativo" {if ($per.num_justificativo==1)}checked{/if}  {if (($per.ind_estado=='Aprobado')||($per.ind_estado=='Verificado'))}disabled{/if} /><span>¿Entregar Justificativo?</span></div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="checkbox checkbox-styled"><label><input type="checkbox"  name="num_exento" id="num_exento" {if ($per.num_exento==1)}checked{/if}  {if (($per.ind_estado=='Aprobado')||($per.ind_estado=='Verificado'))}disabled{/if}/><span>¿Exento?</span></div>
				</div>
			</div>
			<div class="col-md-6 col-sm-6">
				<div class="form-group">
					<input type="text" class="form-control" value="{if $per.ind_estado=='PR'}PREPARADO{/if}{if $per.ind_estado=='AP'}APROBADO{/if}{if $per.ind_estado=='VE'}VERIFICADO{/if}{if $per.ind_estado=='AN'}ANULADO{/if}" disabled>
					<label class="control-label"><i class="glyphicon glyphicon-cog"></i> Estatus</label>
				</div>
			</div>
		</div>
		<div class="col-md-12 col-sm-12">
			<h3 class="text-primary">&nbsp;Acceso</h3>
			<hr class="ruler-xl">
			{foreach item=ac from=$acceso}
			<div class="col-md-4 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" value="{$ac.ind_usuario}" size="45%" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-user"></i> Último usuario</label>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="form-group floating-label">
					<input type="text" class="form-control" value="{$ac.fecha_modificacion}" size="45%" disabled="disabled">
					<label for="regular2"><i class="glyphicon glyphicon-calendar"></i> Última fecha de modificación</label>
				</div>
			</div>
			{/foreach}
		</div>
	</div>
	{if $datoHora.flagFormulario==2}
		{if in_array('RH-01-01-06-03-01-AP',$_Parametros.perfil)}
			{if $per.ind_estado=='PR'}
				<div class="col-md-12 col-sm-12">
					<div class="well clearfix">
						<div class="form-group" style="width: 50%">
							<textarea class="form-control" rows="2" name="ind_observacion_aprobacion" id="ind_observacion_aprobacion"></textarea>
							<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observaciones de aprobación</label>
						</div>
					</div>
				</div>
			{/if}
		{/if}
		{if in_array('RH-01-01-06-03-03-VE',$_Parametros.perfil)}
			{if $per.ind_estado=='AP'}
				<div class="col-md-12 col-sm-12">
					<div class="well clearfix">
						<div class="form-group" style="width: 50%">
							<textarea class="form-control" rows="2" name="ind_observacion_verificacion" id="ind_observacion_verificacion"></textarea>
							<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observaciones de verificación</label>
						</div>
					</div>
				</div>
			{/if}
		{/if}
	{/if}
	</div>
	<div class="col-md-12 col-sm-12">
		<div align="right">
			{if $datoHora.flagFormulario==2}
				{if in_array('RH-01-01-06-03-01-AP',$_Parametros.perfil)}
					{if $per.ind_estado=='Preparado'}
						<button type="button" class="btn btn-primary ink-reaction btn-raised"  id="accion1" pk_num_permiso="{$per.pk_num_permiso}"><span class="md md-done"></span> Aprobar</button>&nbsp;
					{/if}
				{/if}
				{if in_array('RH-01-01-06-03-02-AN',$_Parametros.perfil)}
					{if $per.ind_estado=='Preparado'}
						<button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion2" pk_num_permiso="{$per.pk_num_permiso}"><span class="md md-block"></span> Anular</button>&nbsp;
					{/if}
				{/if}
				{if in_array('RH-01-01-06-03-03-VE',$_Parametros.perfil)}
					{if $per.ind_estado=='Aprobado'}
						<button type="button" class="btn btn-primary ink-reaction btn-raised"  id="accion3" pk_num_permiso="{$per.pk_num_permiso}"><span class="md md-done"></span> Verificar</button>&nbsp;
					{/if}
				{/if}
			{/if}
			<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha regresado al listado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cerrar</button>
		</div>
	</div>
	{/foreach}
</div>
<script type="text/javascript">
	var $url='{$_Parametros.url}modRH/gestion/permisoCONTROL/estatusMET';
	$("#accion1").click(function() {
		var $valor = 'Aprobado';
		var $ind_observacion_aprobacion = $("#ind_observacion_aprobacion").val();
		if($("#num_remunerado").is(':checked')) {
			var $num_remunerado = 1;
		} else {
			var $num_remunerado = 0;
		}
		if($("#num_justificativo").is(':checked')) {
			var $num_justificativo = 1;
		} else {
			var $num_justificativo = 0;
		}
		if($("#num_exento").is(':checked')) {
			var $num_exento = 1;
		} else {
			var $num_exento = 0;
		}
		$.post($url,{ pk_num_permiso: $(this).attr('pk_num_permiso'), valor: $valor, ind_observacion_aprobacion: $ind_observacion_aprobacion, num_remunerado: $num_remunerado, num_justificativo: $num_justificativo, num_exento: $num_exento},function(dato){
			$('#pk_num_permiso'+dato['pk_num_permiso']).remove();
			$(document.getElementById('datatable1')).append('<tr id="pk_num_permiso'+dato['pk_num_permiso']+'">' +
					'<td>'+dato['pk_num_permiso']+'</td><td>'+dato['nombreCompleto']+'</td>' +
					'<td>'+dato['motivo_ausencia']+'</td><td>'+dato['tipo_ausencia']+'</td>' +
					'<td>'+dato['fecha_ingreso']+'</td><td>'+dato['ind_estado']+'</td>' +
					'{if in_array('RH-01-01-06-03-08-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado el permiso" titulo="Ver Permiso" data-toggle="modal" data-target="#formModal" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
					'{if in_array('RH-01-01-06-03-05-RE',$_Parametros.perfil)}<td align="center"></td><td align="center"><button class="verRegistro logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado un permiso" titulo="Reporte de Permiso" title="Reporte de Permiso" data-toggle="modal" data-target="#formModal" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="md md-attach-file" style="color: #ffffff;"></i></button></td>{/if}</tr>');
			swal("Aprobado", "Permiso aprobado exitosamente", "success");
			$(document.getElementById('cerrarModal')).click();
			$(document.getElementById('ContenidoModal')).html('');
		}, 'json');
	});

	var $url2='{$_Parametros.url}modRH/gestion/permisoCONTROL/estatusMET';
	$("#accion2").click(function() {
		var $valor = 'Anulado';
		$.post($url2,{ pk_num_permiso: $(this).attr('pk_num_permiso'), valor: $valor},function(dato){
			$('#pk_num_permiso'+dato['pk_num_permiso']).remove();
			$(document.getElementById('datatable1')).append('<tr id="pk_num_permiso'+dato['pk_num_permiso']+'">' +
					'<td>'+dato['pk_num_permiso']+'</td><td>'+dato['nombreCompleto']+'</td>' +
					'<td>'+dato['motivo_ausencia']+'</td><td>'+dato['tipo_ausencia']+'</td>' +
					'<td>'+dato['fecha_ingreso']+'</td><td>'+dato['ind_estado']+'</td>' +
					'{if in_array('RH-01-01-06-03-08-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado el permiso" titulo="Ver Permiso" data-toggle="modal" data-target="#formModal" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
					'{if in_array('RH-01-01-06-03-05-RE',$_Parametros.perfil)}<td align="center"></td><td align="center"><button class="verRegistro logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado un permiso" titulo="Reporte de Permiso" title="Reporte de Permiso" data-toggle="modal" data-target="#formModal" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="md md-attach-file" style="color: #ffffff;"></i></button></td>{/if}</tr>');
			swal("Anulado", "Permiso anulado exitosamente", "success");
			$(document.getElementById('cerrarModal')).click();
			$(document.getElementById('ContenidoModal')).html('');
		}, 'json');
	});

	var $url3='{$_Parametros.url}modRH/gestion/permisoCONTROL/estatusMET';
	$("#accion3").click(function() {
		var $valor = 'Verificado';
		var $ind_observacion_verificacion = $("#ind_observacion_verificacion").val();
		$.post($url3,{ pk_num_permiso: $(this).attr('pk_num_permiso'), valor: $valor, ind_observacion_verificacion: $ind_observacion_verificacion},function(dato){
			$('#pk_num_permiso'+dato['pk_num_permiso']).remove();
			$(document.getElementById('datatable1')).append('<tr id="pk_num_permiso'+dato['pk_num_permiso']+'">' +
					'<td>'+dato['pk_num_permiso']+'</td><td>'+dato['nombreCompleto']+'</td>' +
					'<td>'+dato['motivo_ausencia']+'</td><td>'+dato['tipo_ausencia']+'</td>' +
					'<td>'+dato['fecha_ingreso']+'</td><td>'+dato['ind_estado']+'</td>' +
					'{if in_array('RH-01-01-06-03-08-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado el permiso" titulo="Ver Permiso" data-toggle="modal" data-target="#formModal" pk_num_permiso="'+dato['pk_num_permiso']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
					'{if in_array('RH-01-01-06-03-05-RE',$_Parametros.perfil)}<td align="center"></td><td align="center"><a href="http://localhost/SIACE/modRH/gestion/permisoCONTROL/ReportePermisoMET/?pk_num_permiso='+dato['pk_num_permiso']+'" target="_blank"><button class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado un permiso" titulo="Reporte de Permiso" title="Reporte de Permiso"><i class="md md-attach-file" style="color: #ffffff;"></i></button></td>{/if}</tr>');
			swal("Verificado", "Permiso verificado exitosamente", "success");
			$(document.getElementById('cerrarModal')).click();
			$(document.getElementById('ContenidoModal')).html('');
		}, 'json');
	});
</script>