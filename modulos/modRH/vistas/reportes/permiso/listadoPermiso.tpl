<div class="form floating-label">
    <br/>
    <section class="style-default-bright">
            <h2 class="text-primary">&nbsp;Diario de Permisos</h2>
        <br/>
        <div class="well clearfix">
            <div class="col-md-6 col-sm-6">
                <div class="form-group floating-label">
                    <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                        {foreach item=org from=$listadoOrganismo}
                            {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {else}
                                <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                </div>
                <div id="dependencia">
                    <div class="form-group floating-label">
                        <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty" onchange="cargarEmpleado(this.value)">
                            <option value="0">&nbsp;</option>
                            {foreach item=dep from=$listadoDependencia}
                                {if $dep.pk_num_dependencia == $emp.pk_num_dependencia}
                                    <option selected value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-home"></i> Dependencia</label>
                    </div>
                </div>
                <div id="empleado">
                    <div class="form-group floating-label">
                        <select id="pk_num_empleado" name="pk_num_empleado" class="form-control dirty">
                            <option value="">&nbsp;</option>
                            {foreach item=empleado from=$listadoEmpleado}
                                    <option value="{$empleado.pk_num_empleado}">{$empleado.ind_nombre1} {$empleado.ind_nombre2} {$empleado.ind_apellido1} {$empleado.ind_apellido2}</option>
                            {/foreach}
                        </select>
                        <label for="pk_num_dependencia"><i class="glyphicon glyphicon-user"></i> Empleado</label>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="form-group">
                    <input type="text" name="num_permiso" id="num_permiso" class="form-control dirty">
                    <label class="control-label"><i class="glyphicon glyphicon-th-large"></i> N° de Permiso</label>
                </div>
                <div class="form-group" style="width:100%">
                    <div class="input-daterange input-group" id="demo-date-range">
                        <div class="input-group-content">
                            <input type="text" class="fecha form-control dirty" name="fechaInicio" id="fechaInicio">
                            <label><i class="glyphicon glyphicon-calendar"></i> Fecha del Permiso</label>
                        </div>
                        <span class="input-group-addon">a</span>
                        <div class="input-group-content">
                            <input type="text" class="fecha form-control dirty" name="fechaFin" id="fechaFin">
                            <div class="form-control-line"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group floating-label">
                    <select id="tipo_ausencia" name="tipo_ausencia" class="form-control dirty">
                        <option value="">&nbsp;</option>
                        {foreach item=tip from=$tipoAusencia}
                            <option value="{$tip.pk_num_miscelaneo_detalle}">{$tip.ind_nombre_detalle}</option>
                        {/foreach}
                    </select>
                    <label for="tipo_ausencia"><i class="glyphicon glyphicon-triangle-right"></i> Tipo de Ausencia</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-sm-12">
            <div class="col-sm-6 col-sm-6" align="right">
                <button type="button" id="buscarPermiso" titulo="Diario de Permisos" data-toggle="modal" data-target="#formModal" class="btn ink-reaction btn-flat btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Generando...">Generar Reporte<div style="top: 27px; left: 65px;" class="ink inverse"></div></button>
            </div>
            <div class="col-sm-6 col-sm-6">
                <button type="submit" onclick="buscarTrabajador()" titulo="Listado de Empleados" class="btn btn-primary ink-reaction btn-raised"> Buscar</button>
            </div>
        </div>
        <br/><br/><br/>
        <div>
            <table id="datatable1" class="table table-striped table-hover">
                <thead>
                <tr align="center">
                    <th> N° Permiso</th>
                    <th> Empleado</th>
                    <th>Nombre Completo</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i> Fec. Desde</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i> Fec. Hasta</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i> H. Desde</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i> H. Hasta</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i> T. Horas</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i> T. Dias</th>
                    <th><i class="glyphicon glyphicon-triangle-right"></i> Tipo Permiso</th>
                    <th> Ver</th>
                    <th> Imprimir</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=permiso from=$listadoPermiso}
                    <tr>
                        <td align="center">{$permiso.pk_num_permiso}</td>
                        <td align="center">{$permiso.pk_num_empleado}</td>
                        <td>{$permiso.ind_nombre1} {$permiso.ind_nombre2} {$permiso.ind_apellido1} {$permiso.ind_apellido2}</td>
                        <td align="center">{$permiso.fecha_salida}</td>
                        <td align="center">{$permiso.fecha_entrada}</td>
                        <td align="center">{$permiso.fec_hora_salida}</td>
                        <td align="center">{$permiso.fec_hora_entrada}</td>
                        <td align="center">{$permiso.num_total_hora|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:{$permiso.num_total_minuto|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}:00</td>
                        <td align="center">{$permiso.num_total_dia}</td>
                        <td>{$permiso.tipo_ausencia}</td>
                        <td align="center"><button type="button" pk_num_permiso="{$permiso.pk_num_permiso}" class="verPermiso logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" data-toggle="modal" data-target="#formModal" titulo="Permiso" title="Ver Permiso"><i class="glyphicon glyphicon-search"></i></button></td>
                        <td align="center"><button type="button" pk_num_empleado="{$permiso.pk_num_empleado}" class="cargarPermiso logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" data-toggle="modal" data-target="#formModal" titulo="Permiso" title="Imprimir Permiso"><i class="glyphicon glyphicon-download"></i></button></td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(".fecha").datepicker({
        format: 'dd/mm/yyyy',
        language: 'es',
        autoclose: true
    });

    var $urlVer='{$_Parametros.url}modRH/reportes/permisoCONTROL/VerPermisoMET';
    $('#datatable1 tbody').on( 'click', '.verPermiso', function () {
        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('title'));
        $('#ContenidoModal').html("");
        $.post($urlVer,{ pk_num_permiso: $(this).attr('pk_num_permiso'), flagFormulario: 1},function($dato){
            $('#ContenidoModal').html($dato);
        });
    });


    $('#datatable1 tbody').on( 'click', '.cargarPermiso', function () {
        $('#modalAncho').css( "width", "85%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        var pk_num_empleado = $(this).attr('pk_num_empleado');
        var urlReporte = '{$_Parametros.url}modRH/reportes/permisoCONTROL/GenerarDiarioPermisoMET/?pk_num_empleado='+pk_num_empleado;
        $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
    });

    $(document).ready(function() {
        $('#buscarPermiso').click(function () {
            $('#modalAncho').css( 'width', '85%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var pk_num_empleado = $("#pk_num_empleado").val();
            var num_permiso = $("#num_permiso").val();
            var fechaInicio = $("#fechaInicio").val();
            var fechaFin = $("#fechaFin").val();
            var tipo_ausencia = $("#tipo_ausencia").val();
            var urlReporte = '{$_Parametros.url}modRH/reportes/permisoCONTROL/GenerarReportePermisoMET/?pk_num_organismo='+pk_num_organismo+'&pk_num_dependencia='+pk_num_dependencia+'&pk_num_empleado='+pk_num_empleado+'&num_permiso='+num_permiso+'&fechaInicio='+fechaInicio+'&fechaFin='+fechaFin+'&tipo_ausencia='+tipo_ausencia;
            $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
        });
    });

    function buscarTrabajador()
    {
        var pk_num_organismo = $("#pk_num_organismo").val();
        var pk_num_dependencia = $("#pk_num_dependencia").val();
        var pk_num_empleado = $("#pk_num_empleado").val();
        var num_permiso = $("#num_permiso").val();
        var fechaInicio = $("#fechaInicio").val();
        var fechaFin = $("#fechaFin").val();
        var tipo_ausencia = $("#tipo_ausencia").val();
        var url_listar='{$_Parametros.url}modRH/reportes/permisoCONTROL/ListarPermisoMET';
        $.post(url_listar,{ pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, pk_num_empleado: pk_num_empleado, num_permiso: num_permiso, fechaInicio: fechaInicio, fechaFin: fechaFin, tipo_ausencia: tipo_ausencia },function(respuesta_post) {
            var tabla_listado = $('#datatable1').DataTable();
            tabla_listado.clear().draw();
            if(respuesta_post != -1) {
                for(var i=0; i<respuesta_post.length; i++) {
                    var botonVer = '<button type="button" pk_num_permiso = "' + respuesta_post[i].pk_num_permiso + '" class="verPermiso logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" titulo="Permiso" title="Permiso" descripcion="El usuario ha buscado un empleado"><i class="glyphicon glyphicon-search"></i></button>';
                    var botonImprimir = '<button type="button" pk_num_empleado = "' + respuesta_post[i].pk_num_empleado + '" class="cargarPermiso logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" titulo="Permiso" title="Permiso" descripcion="El usuario ha buscado un empleado"><i class="glyphicon glyphicon-download"></i></button>';
                    tabla_listado.row.add([
                        respuesta_post[i].pk_num_permiso,
                        respuesta_post[i].pk_num_empleado,
                        respuesta_post[i].ind_nombre1+' '+ respuesta_post[i].ind_nombre2+' '+ respuesta_post[i].ind_apellido1+' '+ respuesta_post[i].ind_apellido2,
                        respuesta_post[i].fecha_salida,
                        respuesta_post[i].fecha_entrada,
                        respuesta_post[i].fec_hora_salida,
                        respuesta_post[i].fec_hora_entrada,
                        respuesta_post[i].num_total_hora+':'+respuesta_post[i].num_total_minuto+':00',
                        respuesta_post[i].num_total_dia,
                        respuesta_post[i].tipo_ausencia,
                        botonVer,
                        botonImprimir

                    ]).draw()
                            .nodes()
                            .to$()
                }
            }

        },'json');
    }

    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/reportes/cargaFamiliarCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function cargarEmpleado(pk_num_dependencia) {
        $("#empleado").html("");
        $.post("{$_Parametros.url}modRH/reportes/permisoCONTROL/BuscarEmpleadoMET",{ pk_num_dependencia:""+pk_num_dependencia }, function (dato) {
            $("#empleado").html(dato);
        });
    }
</script>