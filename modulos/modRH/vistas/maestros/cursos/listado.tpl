
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Cursos</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatableCursos" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="170">Curso</th>
                                    <th>Area</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=cursos from=$Cursos}

                                    <tr id="idCurso{$cursos.pk_num_curso}">
                                        <td>{$cursos.pk_num_curso}</td>
                                        <td>{$cursos.ind_nombre_detalle}</td>
                                        <td>{$cursos.ind_nombre_curso}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-02-19-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idCurso="{$cursos.pk_num_curso}"
                                                        descipcion="El Usuario a Modificado un Curso" titulo="Modificar Curso">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-02-20-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" title="Eliminar" idCurso="{$cursos.pk_num_curso}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Curso" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Curso!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="4">
                                        {if in_array('RH-01-04-02-18-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Nuevo Curso" title="Nuevo Curso" titulo="Nuevo Curso" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Curso &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    tr.group,
    tr.group:hover {
        background-color: #d1f5e3 !important;
        font-weight: bold;
    }
</style>
<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/cursosCONTROL/nuevoCursoMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatableCursos tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idCurso: $(this).attr('idCurso')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatableCursos tbody').on( 'click', '.eliminar', function () {

            var idCurso=$(this).attr('idCurso');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/cursosCONTROL/EliminarCursoMET';
                $.post($url, { idCurso: idCurso},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idCurso'+$dato['idCurso'])).html('');
                        swal("Eliminado!", "El Curso fue Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        var table = $('#datatableCursos').DataTable({
            "columnDefs": [
                { "visible" : false, "targets": 1 }
            ],
            "order": [[ 1, 'asc' ]],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "drawCallback" : function (settings) {
                var api = this.api();
                var rows = api.rows( { page:'current'} ).nodes();
                var last = null;

                api.column(1, { page:'current'} ).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group"><td colspan="4"> Area: ' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        // Order by the grouping
        $('#datatableCursos tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                table.order([1, 'desc']).draw();
            }
            else {
                table.order([1, 'asc']).draw();
            }
        });
    });
</script>