<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idCurso}" name="idCurso"/>


        <!-- ***************************ID CURSO*********************--------->
        <div class="col-xs-12">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_curso)}{$formDB.pk_num_curso}{/if}" name="form[int][pk_num_curso]" id="pk_num_curso" disabled>
                <label for="pk_num_curso">Curso</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>


        <!-- ***************************AREA*********************--------->
        <div class="col-xs-6">
            <div class="form-group floating-label">
                <select id="fk_a006_num_miscelaneo_detalle_areaformacion" name="form[int][fk_a006_num_miscelaneo_detalle_areaformacion]" class="form-control input-sm" required data-msg-required="Seleccione el Area">
                    <option value="">&nbsp;</option>
                    {foreach item=area from=$Area}
                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_areaformacion)}
                            {if $area.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_areaformacion}
                                <option selected value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                            {/if}
                        {else}
                            <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_areaformacion">Area</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>
        </div>



        <!-- ***************************DESCRIPCION DEL CURSO*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nombre_curso)}{$formDB.ind_nombre_curso}{/if}" name="form[alphaNum][ind_nombre_curso]" id="ind_nombre_curso" required data-msg-required="Introduzca descripcion del Curso" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_curso">Descripción </label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************ESTATUS GRADO INSTRUCCION*********************--------->
        <div class="col-xs-6">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>


        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/cursosCONTROL/NuevoCursoMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idCurso'+dato['idCurso'])).html('<td>'+dato['idCurso']+'</td>' +
                            '<td>'+dato['ind_nombre_curso']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-02-19-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idCurso="'+dato['idCurso']+'"' +
                            'descipcion="El Usuario a Modificado un Curso" titulo="Modificar Curso">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-02-20-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCurso="'+dato['idCurso']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Curso" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Curso!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Curso se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatableCursos')).append('<tr  id="idCurso'+dato['idCurso']+'">' +
                            '<td>'+dato['idCurso']+'</td>' +
                            '<td>'+dato['ind_nombre_curso']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-02-19-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCurso="'+dato['idCurso']+'"' +
                            'descipcion="El Usuario a Modificado un Curso" title="Editar" titulo="Modificar Curso">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-02-20-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCurso="'+dato['idCurso']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Curso" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Curso!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "EL Curso fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "55%" );


    });






</script>