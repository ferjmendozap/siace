
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro Dependencias</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatableDependencias" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="120">Codigo</th>
                                    <th width="350">Organismo</th>
                                    <th>Dependencia</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=dep from=$Dependencias}


                                    <tr id="idDependencia{$dep.pk_num_dependencia}">
                                        <td>{$dep.pk_num_dependencia}</td>
                                        <td>{$dep.ind_descripcion_empresa}</td>
                                        <td>{$dep.ind_dependencia}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-09-03-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idDependencia="{$dep.pk_num_dependencia}"
                                                        descipcion="El Usuario a Modificado una Dependencia" titulo="Modificar Dependencia">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-09-04-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDependencia="{$dep.pk_num_dependencia}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado una Dependencia" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Dependencia!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="4">
                                        {if in_array('RH-01-04-09-02-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado una Dependencia" title="Nueva Dependencia"  titulo="Nueva Dependencia" id="nuevo" >
                                                <i class="md md-create"></i> Nueva Dependencia &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    tr.group,
    tr.group:hover {
        background-color: #d1f5e3 !important;
        font-weight: bold;
    }
</style>
<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/dependenciasCONTROL/DependenciasMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatableDependencias tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idDependencia: $(this).attr('idDependencia')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatableDependencias tbody').on( 'click', '.eliminar', function () {

            var idDependencia=$(this).attr('idDependencia');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/dependenciasCONTROL/EliminarDependenciasMET';
                $.post($url, { idDependencia: idDependencia},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idDependencia'+$dato['idDependencia'])).html('');
                        swal("Eliminado!", "El Registro fue Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
        var table = $('#datatableDependencias').DataTable({
            "columnDefs": [
                { "visible" : false, "targets": 1 }
            ],
            "order": [[ 1, 'asc' ]],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "drawCallback" : function (settings) {
                var api = this.api();
                var rows = api.rows( { page:'current'} ).nodes();
                var last = null;

                api.column(1, { page:'current'} ).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group"><td colspan="4"> ORGANISMO: ' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        // Order by the grouping
        $('#datatableDependencias tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                table.order([1, 'desc']).draw();
            }
            else {
                table.order([1, 'asc']).draw();
            }
        });
    });
</script>