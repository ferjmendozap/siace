
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Nivel Grado de Instrucción</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatableGradoInstruccion" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="200">Cod</th>
                                    <th>Grado Instrucción</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=nivel from=$NivelGradoInstruccion}


                                    <tr id="idNivelGradoInstruccion{$nivel.pk_num_nivel_grado}">
                                        <td>{$nivel.pk_num_nivel_grado}</td>
                                        <td>{$nivel.grado_instruccion}</td>
                                        <td>{$nivel.ind_nombre_nivel_grado}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-02-07-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idNivelGradoInstruccion="{$nivel.pk_num_nivel_grado}"
                                                        descipcion="El Usuario a Modificado un Grado de Instrucción" titulo="Modificar Nivel Grado Instrucción">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-02-08-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idNivelGradoInstruccion="{$nivel.pk_num_nivel_grado}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Grado de Instrucción" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Nivel Grado de Instrucción!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="4">
                                        {if in_array('RH-01-04-02-06-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Nivel Grado de Instruccion" title="Nuevo Nivel Grado de Instrucción"  titulo="Nuevo Nivel Grado de Instrucción" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Nivel Grado de Instrucción &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    tr.group,
    tr.group:hover {
        background-color: #d1f5e3 !important;
        font-weight: bold;
    }
</style>
<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/nivelGradoInstruccionCONTROL/nuevoNivelGradoInstruccionMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatableGradoInstruccion tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idNivelGradoInstruccion: $(this).attr('idNivelGradoInstruccion')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatableGradoInstruccion tbody').on( 'click', '.eliminar', function () {

            var idNivelGradoInstruccion=$(this).attr('idNivelGradoInstruccion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/nivelGradoInstruccionCONTROL/EliminarNivelGradoInstruccionMET';
                $.post($url, { idNivelGradoInstruccion: idNivelGradoInstruccion},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idNivelGradoInstruccion'+$dato['idNivelGradoInstruccion'])).html('');
                        swal("Eliminado!", "El Nivel Grado de Instrucción fue Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

        var table = $('#datatableGradoInstruccion').DataTable({
            "columnDefs": [
                { "visible" : false, "targets": 1 }
            ],
            "order": [[ 1, 'asc' ]],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "drawCallback" : function (settings) {
                var api = this.api();
                var rows = api.rows( { page:'current'} ).nodes();
                var last = null;

                api.column(1, { page:'current'} ).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group"><td colspan="4"> Grado Instrucción: ' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        // Order by the grouping
        $('#datatableGradoInstruccion tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                table.order([1, 'desc']).draw();
            }
            else {
                table.order([1, 'asc']).draw();
            }
        });
    });
</script>