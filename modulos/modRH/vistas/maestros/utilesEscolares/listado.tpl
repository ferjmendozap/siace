
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro Utiles Escolares</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">

                        <div class="card-body">
                            <input type="hidden" value="{if isset($idMaestro.fk_a005_num_miscelaneo_maestro)}{$idMaestro.fk_a005_num_miscelaneo_maestro}{/if}" name="idMaestro" id="idMaestro"/>
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="150">Id</th>
                                    <th>Descripción Utiles</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=f from=$Utiles}



                                        <tr id="idUtiles{$f.pk_num_miscelaneo_detalle}">
                                            <td>{$f.pk_num_miscelaneo_detalle}</td>
                                            <td>{$f.ind_nombre_detalle}</td>
                                            <td align="center" width="100">
                                                {if in_array('RH-01-04-08-11-M',$_Parametros.perfil)}
                                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                            data-keyboard="false" data-backdrop="static" title="Editar" idUtiles="{$f.pk_num_miscelaneo_detalle}"
                                                            descipcion="El Usuario a Modificado Utiles Escolares" titulo="Modificar Utiles Escolares">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                &nbsp;&nbsp;
                                                {if in_array('RH-01-04-08-12-E',$_Parametros.perfil)}
                                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idUtiles="{$f.pk_num_miscelaneo_detalle}"  boton="si, Eliminar"
                                                            descipcion="El usuario a eliminado Utiles Escolares" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Utiles Escolares!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            </td>
                                        </tr>




                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="3">
                                        {if in_array('RH-01-04-08-11-M',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado Utiles Escolares" title="Nuevo Registro Utiles Escolares" titulo="Nuevo Registro Utiles Escolares" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Registro Utiles &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/utilesEscolaresCONTROL/UtilesEscolaresMET';

        $('#nuevo').click(function(){
            var maestro = $("#idMaestro").val();
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { maestro:maestro } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idUtiles: $(this).attr('idUtiles')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idUtiles=$(this).attr('idUtiles');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/utilesEscolaresCONTROL/EliminarUtilesEscolaresMET';
                $.post($url, { idUtiles: idUtiles},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idUtiles'+$dato['idUtiles'])).html('');
                        swal("Eliminado!", "Utiles Escolares Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>