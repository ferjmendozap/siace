<!-- BEGIN 500 MESSAGE -->
<section>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h1><span class="text-xxxL text-light">NO DISPONIBLE <i class="fa fa-exclamation-circle text-danger"></i></span></h1>
                <h2 class="text-light">...</h2>
            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .section-body -->
</section>
<!-- END 500 MESSAGE -->