<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro Dias Feriados</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="tablaFeriados" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="150">Fecha</th>
                                    <th>Año</th>
                                    <th>Descripción Feriado</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=f from=$Feriado}

                                        <tr id="idFeriado{$f.pk_num_feriado}">
                                            <td>{$f.fec_dia}</td>
                                            <td>{$f.fec_anio}</td>
                                            <td>{$f.ind_descripcion_feriado}</td>
                                            <td align="center" width="100">
                                                {if in_array('RH-01-04-10-04-M',$_Parametros.perfil)}
                                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                            data-keyboard="false" data-backdrop="static" title="Editar" idFeriado="{$f.pk_num_feriado}"
                                                            descipcion="El Usuario a Modificado un Día Feriado" titulo="Modificar Día Feriado">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                &nbsp;&nbsp;
                                                {if in_array('RH-01-04-10-05-E',$_Parametros.perfil)}
                                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idFeriado="{$f.pk_num_feriado}"  boton="si, Eliminar"
                                                            descipcion="El usuario a eliminado un Día Feriado" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Día Feriado!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            </td>
                                        </tr>




                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="4">
                                        {if in_array('RH-01-04-10-03-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Dia Feriado" title="Nuevo Dia Feriado"  titulo="Nuevo Dia Feriado" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Dia Feriado &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    tr.group,
    tr.group:hover {
        background-color: #d1f5e3 !important;
        font-weight: bold;
    }
</style>
<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/diasFeriadosCONTROL/DiasFeriadosMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#tablaFeriados tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idFeriado: $(this).attr('idFeriado')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#tablaFeriados tbody').on( 'click', '.eliminar', function () {

            var idFeriado=$(this).attr('idFeriado');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/diasFeriadosCONTROL/EliminarDiasFeriadosMET';
                $.post($url, { idFeriado: idFeriado},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idFeriado'+$dato['idFeriado'])).html('');
                        swal("Eliminado!", "Día Feriado Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

        var table = $('#tablaFeriados').DataTable({
            "columnDefs": [
                { "visible" : false, "targets": 1 }
            ],
            "order": [[ 1, 'asc' ]],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "drawCallback" : function (settings) {
                var api = this.api();
                var rows = api.rows( { page:'current'} ).nodes();
                var last = null;

                api.column(1, { page:'current'} ).data().each(function (group, i) {
                    if (last !== group) {

                        if(group==''){

                            $(rows).eq(i).before(

                                    '<tr class="group"><td colspan="4"> Mes/Día</td></tr>'
                            );

                        }else{
                            $(rows).eq(i).before(

                                    '<tr class="group"><td colspan="4"> Año ' + group + '</td></tr>'
                            );

                        }

                        last = group;
                    }
                });
            }
        });

        // Order by the grouping
        $('#tablaFeriados tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                table.order([1, 'desc']).draw();
            }
            else {
                table.order([1, 'asc']).draw();
            }
        });


    });
</script>