<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Profesiones</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatableProfesiones" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="175">Profesión</th>
                                    <th>Area</th>
                                    <th>Descripción</th>
                                    <th>Grado de Instrucción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=pro from=$Profesiones}


                                    <tr id="idProfesion{$pro.pk_num_profesion}">
                                        <td>{$pro.pk_num_profesion}</td>
                                        <td>{$pro.areaformacion}</td>
                                        <td>{$pro.ind_nombre_profesion}</td>
                                        <td>{$pro.grado_instruccion}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-02-11-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idProfesion="{$pro.pk_num_profesion}"
                                                        descipcion="El Usuario a Modificado una Profesión" titulo="Modificar Profesión">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-02-12-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idProfesion="{$pro.pk_num_profesion}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado una Profesión" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Profesión!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="5">
                                        {if in_array('RH-01-04-02-10-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descripcion="el Usuario a creado una Nueva Profesión" title="Nueva Profesión"  titulo="Nueva Profesión" id="nuevo">
                                            <i class="md md-create"></i> Nueva Profesión &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    tr.group,
    tr.group:hover {
        background-color: #d1f5e3 !important;
        font-weight: bold;
    }
</style>
<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/profesionesCONTROL/NuevaProfesionMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatableProfesiones tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idProfesion: $(this).attr('idProfesion')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });

        $('#datatableProfesiones tbody').on( 'click', '.eliminar', function () {

            var idProfesion=$(this).attr('idProfesion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/profesionesCONTROL/EliminarProfesionMET';
                $.post($url, { idProfesion: idProfesion},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idProfesion'+$dato['idProfesion'])).html('');
                        swal("Eliminado!", "La Profesion fue Eliminada.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

        var table = $('#datatableProfesiones').DataTable({
            "columnDefs": [
                { "visible" : false, "targets": 1 }
            ],
            "order": [[ 1, 'asc' ]],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "drawCallback" : function (settings) {
                var api = this.api();
                var rows = api.rows( { page:'current'} ).nodes();
                var last = null;

                api.column(1, { page:'current'} ).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group"><td colspan="4"> Area: ' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        // Order by the grouping
        $('#datatableProfesiones tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                table.order([1, 'desc']).draw();
            }
            else {
                table.order([1, 'asc']).draw();
            }
        });
    });
</script>