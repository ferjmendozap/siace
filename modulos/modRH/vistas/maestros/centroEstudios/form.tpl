<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idCentro}" name="idCentro"/>


        <!-- ***************************ID PROFESION*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_institucion)}{$formDB.pk_num_institucion}{/if}" name="form[int][pk_num_institucion]" id="pk_num_institucion" disabled>
                <label for="pk_num_institucion">Centro</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>

        <!-- ***************************DESCRIPCION CENTRO DE ESTUDIOS*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nombre_institucion)}{$formDB.ind_nombre_institucion}{/if}" name="form[txt][ind_nombre_institucion]" id="ind_nombre_institucion" required data-msg-required="Introduzca descripcion del centro de estudio" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_institucion">Descripción </label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************PROFESIÓNN*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_ubicacion)}{$formDB.ind_ubicacion}{/if}" name="form[alphaNum][ind_ubicacion]" id="ind_ubicacion" required data-msg-required="Introduzca ubicacion del centro de estudio" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_ubicacion">Ubicación </label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************ESTATUS*********************--------->
        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_flag_estudio) and $formDB.num_flag_estudio==1} checked{/if} value="1" name="form[int][ind_flag_estudio]">
                    <span>Centro de Estudios</span>
                </label>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_flag_curso) and $formDB.num_flag_curso==1} checked{/if} value="1" name="form[int][ind_flag_curso]">
                    <span>Cursos</span>
                </label>
            </div>
        </div>


        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/centroEstudiosCONTROL/NuevoCentroEstudioMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idCentro'+dato['idCentro'])).html('<td>'+dato['idCentro']+'</td>' +
                            '<td>'+dato['ind_nombre_institucion']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-02-15-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idCentro="'+dato['idCentro']+'"' +
                            'descipcion="El Usuario a Modificado un Centro de Estudio" titulo="Modificar Centro de Estudio">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-02-16-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCentro="'+dato['idCentro']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Centro de Estudio" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Centro de Estudio!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "EL Centro de Estudio se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatable1')).append('<tr  id="idCentro'+dato['idCentro']+'">' +
                            '<td>'+dato['idCentro']+'</td>' +
                            '<td>'+dato['ind_nombre_institucion']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-02-15-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCentro="'+dato['idCentro']+'"' +
                            'descipcion="El Usuario a Modificado un Centro de Estudio" title="Editar" titulo="Modificar Centro de Estudio">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-02-16-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCentro="'+dato['idCentro']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Centro de Estudio" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Centro de Estudio!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "EL Centro de Estudio fue guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "70%" );


    });






</script>