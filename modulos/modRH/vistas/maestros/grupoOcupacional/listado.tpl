
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Grupo Ocupacional</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <input type="hidden" value="{if isset($idMaestro.fk_a005_num_miscelaneo_maestro)}{$idMaestro.fk_a005_num_miscelaneo_maestro}{/if}" name="idMaestro" id="idMaestro"/>
                            <table id="datatableGrupoOcupacional" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Cod</th>
                                    <th>Grupo Ocupacional</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tfoot>
                                    <th colspan="3">
                                        {if in_array('RH-01-04-01-10-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Grupo Ocupacional" title="Nuevo Grupo Ocupacional" titulo="Nuevo Grupo Ocupacional" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Grupo Ocupacional &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/grupoOcupacionalCONTROL/NuevoGrupoOcupacionalMET';

        //INICIALIZAR LA DATATABLES
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#datatableGrupoOcupacional',
                "{$_Parametros.url}modRH/maestros/grupoOcupacionalCONTROL/JsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_miscelaneo_detalle" },
                    { "data": "ind_nombre_detalle" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );

        //EVENTO DEL BOTON NUEVO
        $('#nuevo').click(function(){
            var maestro = $("#idMaestro").val();
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { maestro:maestro } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //EVENTO DEL BOTON MODIFICAR
        $('#datatableGrupoOcupacional tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idGrupoOcupacional: $(this).attr('idGrupoOcupacional')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });

        //EVENTO DEL BOTON ELIMINAR
        $('#datatableGrupoOcupacional tbody').on( 'click', '.eliminar', function () {

            var idGrupoOcupacional=$(this).attr('idGrupoOcupacional');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/grupoOcupacionalCONTROL/EliminarGrupoOcupacionalMET';
                $.post($url, { idGrupoOcupacional: idGrupoOcupacional},function($dato){
                    if($dato['status']=='OK'){
                        app.metEliminarRegistroJson('datatableGrupoOcupacional','El Grupo Ocupacional fue eliminado.','cerrarModal','ContenidoModal');
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>