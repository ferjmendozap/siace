<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idHorario}" name="idHorario"/>


        <div class="card">
            <div class="card-body">
                <div class="col-xs-6">
                    <div class="form-group">
                        <input type="text" class="form-control input-sm" value="{if isset($formDB1.pk_num_horario)}{$formDB1.pk_num_horario}{/if}" name="form[int][pk_num_horario]" id="pk_num_horario" disabled>
                        <label for="pk_num_horario">Horario</label>
                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                    </div>
                </div>
                <!-- ***************************DESCRIPCION*********************--------->
                <div class="col-xs-7">
                    <div class="form-group floating-label">
                        <input type="text" class="form-control input-sm" value="{if isset($formDB1.ind_descripcion)}{$formDB1.ind_descripcion}{/if}" name="form[txt][ind_descripcion]" id="ind_descripcion" maxlength="100" required data-msg-required="Introduzca Descripción del Horario" autocomplete="off" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                        <label for="ind_descripcion">Descripción Horario</label>
                        <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
                    </div>
                </div>
                <!-- RESOLUCION-->
                <div class="col-xs-5">
                    <div class="form-group floating-label">
                        <input type="text" class="form-control input-sm" value="{if isset($formDB1.ind_resolucion)}{$formDB1.ind_resolucion}{/if}" name="form[txt][ind_resolucion]" id="ind_resolucion" maxlength="100" required data-msg-required="Introduzca Resolución del Horario" autocomplete="off" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                        <label for="ind_resolucion">Resolución</label>
                        <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
                    </div>
                </div>

                <div class="col-xs-4">

                    <div class="form-group date" id="demo-date">
                        <div class="input-group-content">
                            <input type="text" class="form-control input-sm" value="{if isset($formDB1.fec_resolucion)}{$formDB1.fec_resolucion|date_format:"d-m-Y"}{/if}" name="form[txt][fec_resolucion]" id="fec_resolucion" maxlength="10" required data-msg-required="Introduzca Fecha Resolución del Horario">
                            <label>Fecha Resolución</label>
                            <p class="help-block"><span class="text-xs" style="color: red"> * </span></p>
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>

                </div>

                <!-- ***************************ESTATUS*********************--------->
                <div class="col-xs-2">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB1.num_flag_corrido) and $formDB1.num_flag_corrido==1} checked{/if} value="1" name="form[int][num_flag_corrido]" id="num_flag_corrido">
                            <span>Corrido</span>
                        </label>
                    </div>
                </div>

                <div class="col-xs-2">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB1.num_estatus) and $formDB1.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" name="form[int][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>




            </div>
            <div class="card-body style-info small-padding text-center">
                <div class="col-xs-2">
                    Laborable
                </div>
                <div class="col-xs-2 ">
                    Dia
                </div>
                <div class="col-xs-4" align="center">
                    Turno 1
                </div>
                <div class="col-xs-4" align="center">
                    Turno 2
                </div>
            </div>
            <div class="card-body small-padding text-center">
                <div class="col-xs-2">
                    <p>&nbsp;</p>
                </div>
                <div class="col-xs-2">
                    <p>&nbsp;</p>
                </div>
                <div class="col-xs-2" align="center">
                    <p>Entrada</p>
                </div>
                <div class="col-xs-2" align="center">
                    <p>Salida</p>
                </div>
                <div class="col-xs-2" align="center">
                    <p>Entrada</p>
                </div>
                <div class="col-xs-2" align="center">
                    <p>Salida</p>
                </div>
            </div>
            <div class="card-body small-padding text-center">

                {if isset($formDB2)}

                    {foreach item=op from=$formDB2}

                    <div class="row">

                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="checkbox" {if $op.num_flag_laborable==1} checked{/if}  value="1" name="form[int][num_flag_laborable][]">
                            </div>
                        </div>
                        <div class="col-md-2" align="left">
                            <div class="form-group">
                                {if $op.num_dia eq 1} LUNES
                                {elseif $op.num_dia eq '2'} MARTES
                                {elseif $op.num_dia eq '3'} MIERCOLES
                                {elseif $op.num_dia eq '4'} JUEVES
                                {elseif $op.num_dia eq '5'} VIERNES
                                {elseif $op.num_dia eq '6'} SABADO
                                {else} DOMINGO
                                {/if}
                                <input type="hidden" value="{$op.num_dia}" name="form[int][dia][]" id="dia">
                            </div>
                        </div>
                        <div class="col-md-2" align="left">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm time12-mask" value="{if $op.fec_entrada1 eq '00:00:00'}{else}{$op.fec_entrada1|date_format:"H:i a"}{/if}" name="form[formulas][fec_entrada1][]" id="fec_entrada1{$n}">
                                <label></label>
                                <p class="help-block"><span class="text-xs" style="color: red">am/pm</span></p>
                            </div>
                        </div>
                        <div class="col-md-2" align="left">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm time12-mask" value="{if $op.fec_salida1 eq '00:00:00'}{else}{$op.fec_salida1|date_format:"H:i a"}{/if}" name="form[formulas][fec_salida1][]" id="fec_salida1{$n}">
                                <label></label>
                                <p class="help-block"><span class="text-xs" style="color: red">am/pm</span></p>
                            </div>
                        </div>
                        <div class="col-md-2" align="left">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm time12-mask" value="{if $op.fec_entrada2 eq '00:00:00'}{else}{$op.fec_entrada2|date_format:"H:i a"}{/if}" name="form[formulas][fec_entrada2][]" id="fec_entrada2{$n}">
                                <label></label>
                                <p class="help-block"><span class="text-xs" style="color: red">am/pm</span></p>
                            </div>
                        </div>
                        <div class="col-md-2" align="left">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm time12-mask" value="{if $op.fec_salida2 eq '00:00:00'}{else}{$op.fec_salida2|date_format:"H:i a"}{/if}" name="form[formulas][fec_salida2][]" id="fec_salida2{$n}">
                                <label></label>
                                <p class="help-block"><span class="text-xs" style="color: red">am/pm</span></p>
                            </div>
                        </div>
                        <input type="hidden" value="{$n++}">
                        <input type="hidden" name="form[int][pk_num_horario_detalle][]" id="pk_num_horario_detalle" value="{$op.pk_num_horario_detalle}">

                    </div>
                    {/foreach}


                {else}

                    {foreach item=d from=$Dias}
                    <div class="row">

                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="checkbox" {if $d.FlagLaborable==1} checked{/if}  value="1" name="form[int][num_flag_laborable][]">
                        </div>
                    </div>
                    <div class="col-md-2" align="left">
                        <div class="form-group">
                            {$d.dia_nombre}
                            <input type="hidden" value="{$d.num_dia}" name="form[int][dia][]" id="dia">
                        </div>
                    </div>
                    <div class="col-md-2" align="left">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm time12-mask" value="" name="form[formulas][fec_entrada1][]" id="fec_entrada1{$n}" >
                            <label></label>
                            <p class="help-block"><span class="text-xs" style="color: red">am/pm</span></p>
                        </div>
                    </div>
                    <div class="col-md-2" align="left">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm time12-mask" value="" name="form[formulas][fec_salida1][]" id="fec_salida1{$n}" >
                            <label></label>
                            <p class="help-block"><span class="text-xs" style="color: red">am/pm</span></p>
                        </div>
                    </div>
                    <div class="col-md-2" align="left">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm time12-mask" value="" name="form[formulas][fec_entrada2][]" id="fec_entrada2{$n}" >
                            <label></label>
                            <p class="help-block"><span class="text-xs" style="color: red">am/pm</span></p>
                        </div>
                    </div>
                    <div class="col-md-2" align="left">
                        <div class="form-group">
                            <input type="text" class="form-control input-sm time12-mask" value="" name="form[formulas][fec_salida2][]" id="fec_salida2{$n}" >
                            <label></label>
                            <p class="help-block"><span class="text-xs" style="color: red">am/pm</span></p>
                        </div>
                    </div>
                    <input type="hidden" value="{$n++}">
                    </div>
                    {/foreach}

                {/if}

            </div>

        </div>

        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">

   $("#fec_resolucion").datepicker({
        format:'dd-mm-yyyy',
        language: 'es',
        autoclose:true
    });

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            if( $('#num_flag_corrido').is(':checked') ) {

                if( $("#fec_entrada10").val()=='' || $("#fec_entrada11").val()=='' || $("#fec_entrada12").val()=='' || $("#fec_entrada13").val()=='' || $("#fec_entrada14").val()=='' ||  $("#fec_salida10").val()=='' || $("#fec_salida11").val()=='' || $("#fec_salida12").val()=='' || $("#fec_salida13").val()=='' || $("#fec_salida14").val()==''){
                    swal("Campos Vacios!", "Debe llenar los campos de horas que estan en blanco", "warning");
                    return false;
                }


            }else{


                if( $("#fec_entrada10").val()=='' || $("#fec_entrada11").val()=='' || $("#fec_entrada12").val()=='' || $("#fec_entrada13").val()=='' || $("#fec_entrada14").val()=='' ||  $("#fec_salida10").val()=='' || $("#fec_salida11").val()=='' || $("#fec_salida12").val()=='' || $("#fec_salida13").val()=='' || $("#fec_salida14").val()=='' || $("#fec_entrada20").val()=='' || $("#fec_entrada21").val()=='' || $("#fec_entrada22").val()=='' || $("#fec_entrada23").val()=='' || $("#fec_entrada24").val()=='' || $("#fec_salida20").val()=='' || $("#fec_salida21").val()=='' || $("#fec_salida22").val()=='' || $("#fec_salida23").val()=='' || $("#fec_salida24").val()==''){
                    swal("Campos Vacios!", "Debe llenar los campos de horas que estan en blanco", "warning");
                    return false;
                }


            }

            $.post('{$_Parametros.url}modRH/maestros/horarioLaboralCONTROL/NuevoHorarioLaboralMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idHorario'+dato['idHorario'])).html('<td>'+dato['idHorario']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['num_flag_corrido']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-03-17-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idHorario="'+dato['idHorario']+'"' +
                            'descipcion="El Usuario a Modificado un Horario Laboral" titulo="Modificar Horario Laboral">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-03-18-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idHorario="'+dato['idHorario']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Horario Laboral" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Horario Laboral!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Registro se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatable1')).append('<tr id="idHorario'+dato['idHorario']+'">' +
                            '<td>'+dato['idHorario']+'</td>' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['num_flag_corrido']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-03-17-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idHorario="'+dato['idHorario']+'"' +
                            'descipcion="El Usuario a Modificado un Horario Laboral" titulo="Modificar Horario Laboral">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-03-18-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idHorario="'+dato['idHorario']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Horario Laboral" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Horario Laboral!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "Registro guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "80%" );

        //check flag corrido (cuando lo activo desactivo el 2do turno)
        $("#num_flag_corrido").click(function(){

            if( $('#num_flag_corrido').is(':checked') ) {

                $("#fec_entrada20").prop('readonly',true);
                $("#fec_entrada21").prop('readonly',true);
                $("#fec_entrada22").prop('readonly',true);
                $("#fec_entrada23").prop('readonly',true);
                $("#fec_entrada24").prop('readonly',true);


                $("#fec_salida20").prop('readonly',true);
                $("#fec_salida21").prop('readonly',true);
                $("#fec_salida22").prop('readonly',true);
                $("#fec_salida23").prop('readonly',true);
                $("#fec_salida24").prop('readonly',true);

                $("#fec_entrada10").prop('required',true);
                $("#fec_entrada11").prop('required',true);
                $("#fec_entrada12").prop('required',true);
                $("#fec_entrada13").prop('required',true);
                $("#fec_entrada14").prop('required',true);

                $("#fec_salida10").prop('required',true);
                $("#fec_salida11").prop('required',true);
                $("#fec_salida12").prop('required',true);
                $("#fec_salida13").prop('required',true);
                $("#fec_salida14").prop('required',true);

            }else{
                $("#fec_entrada20").prop('readonly',false);
                $("#fec_entrada21").prop('readonly',false);
                $("#fec_entrada22").prop('readonly',false);
                $("#fec_entrada23").prop('readonly',false);
                $("#fec_entrada24").prop('readonly',false);


                $("#fec_salida20").prop('readonly',false);
                $("#fec_salida21").prop('readonly',false);
                $("#fec_salida22").prop('readonly',false);
                $("#fec_salida23").prop('readonly',false);
                $("#fec_salida24").prop('readonly',false);
            }



        });

    });






</script>