
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro Horario Laboral</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="170">Horario</th>
                                    <th>Descripción</th>
                                    <th>Corrido</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=horario from=$Horario}

                                    <tr id="idHorario{$horario.pk_num_horario}">
                                        <td>{$horario.pk_num_horario}</td>
                                        <td>{$horario.ind_descripcion}</td>
                                        <td>{if $horario.num_flag_corrido eq "0"} No {else} Si {/if}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-03-17-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idHorario="{$horario.pk_num_horario}"
                                                        descipcion="El Usuario a Modificado un Horario Laboral" titulo="Modificar Horario Laboral">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-03-18-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idHorario="{$horario.pk_num_horario}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Horario Laboral" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Horario Laboral!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="4">
                                        {if in_array('RH-01-04-03-16-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Horario Laboral" title="Nuevo Horario Laboral"  titulo="Nuevo Horario Laboral" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Horario Laboral &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/horarioLaboralCONTROL/NuevoHorarioLaboralMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idHorario: $(this).attr('idHorario')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idHorario=$(this).attr('idHorario');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/horarioLaboralCONTROL/EliminarHorarioLaboralMET';
                $.post($url, { idHorario: idHorario},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idHorario'+$dato['idHorario'])).html('');
                        swal("Eliminado!", "El Horario Laboral fue Eliminado.", "success");
                        $('#cerrar').click();
                    }else if($dato['status']=='ERROR'){
                        swal("Error!", $dato['mensaje'], "error");
                    }
                },'json');
            });
        });
    });
</script>