
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro de Preguntas de Plantillas Clima Laboral</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">

                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="130">Id</th>
                                    <th>Pregunta</th>
                                    <th>Valor Minimo</th>
                                    <th>Valor Maximo</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=b from=$Preguntas}

                                    {if $enc != $b.fk_a006_num_miscelaneo_detalle_area}
                                        {$enc = $b.fk_a006_num_miscelaneo_detalle_area}
                                        <tr style="background: #b9ddf3">
                                            <td ></td>
                                            <td style="font-weight: bold">Area: {$b.ind_nombre_detalle}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    {/if}

                                    <tr id="idPregunta{$b.pk_num_preguntas}">
                                        <td>{$b.pk_num_preguntas}</td>
                                        <td>{$b.ind_descripcion_preguntas}</td>
                                        <td>{$b.num_valor_minimo}</td>
                                        <td>{$b.num_valor_maximo}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-04-03-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idPregunta="{$b.pk_num_preguntas}"
                                                        descipcion="El Usuario a Modificado una Pregunta" titulo="Modificar Pregunta">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-04-04-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPregunta="{$b.pk_num_preguntas}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado una Pregunta" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Preguntas!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="5">
                                        {if in_array('RH-01-04-04-02-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado una Pregunta" title="Nueva Pregunta"  titulo="Nueva Pregunta" id="nuevo" >
                                                <i class="md md-create"></i> Nueva Pregunta &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/preguntasCONTROL/PreguntasMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPregunta: $(this).attr('idPregunta')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idPregunta=$(this).attr('idPregunta');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/preguntasCONTROL/EliminarPreguntasMET';
                $.post($url, { idPregunta: idPregunta},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idPregunta'+$dato['idPregunta'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>