<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idPregunta}" name="idPregunta"/>



        <!-- ***************************ID ASIGNACION*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_preguntas)}{$formDB.pk_num_preguntas}{/if}" name="form[int][pk_num_preguntas]" id="pk_num_preguntas" readonly>
                <label for="pk_num_preguntas">Id</label>
            </div>
        </div>


        <!-- ***************************AREA*********************--------->
        <div class="col-xs-6">
            <div class="form-group floating-label">
                <select id="fk_a006_num_miscelaneo_detalle_area" name="form[int][fk_a006_num_miscelaneo_detalle_area]" class="form-control input-sm" required data-msg-required="Seleccione el Area">
                    <option value="">&nbsp;</option>
                    {foreach item=area from=$Area}
                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_area)}
                            {if $area.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_area}
                                <option selected value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                            {/if}
                        {else}
                            <option value="{$area.pk_num_miscelaneo_detalle}">{$area.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_area">Area</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>
        </div>


        <!-- ***************************DESCRIPCION*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_descripcion_preguntas)}{$formDB.ind_descripcion_preguntas}{/if}" name="form[txt][ind_descripcion_preguntas]" id="ind_descripcion_preguntas" maxlength="250" required data-msg-required="Introduzca Descripción de la Pregunta" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_descripcion_preguntas">Descripción Pregunta</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.num_valor_minimo)}{$formDB.num_valor_minimo}{else}0{/if}" name="form[int][num_valor_minimo]" id="num_valor_min" data-rule-number="true" required data-msg-required="Introduzca Valor Minimo" data-msg-number="Por favor Introduzca un numero valido">
                <label for="num_valor_minimo">Valor Minimo</label>
            </div>
        </div>

        <div class="col-xs-6">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.num_valor_maximo)}{$formDB.num_valor_maximo}{else}0{/if}" name="form[int][num_valor_maximo]" id="num_valor_max" data-rule-number="true" required data-msg-required="Introduzca Valor Maximo" data-msg-number="Por favor Introduzca un numero valido">
                <label for="num_valor_maximo">Valor Maximo</label>
            </div>
        </div>


        <!-- ***************************ESTATUS*********************--------->
        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/preguntasCONTROL/PreguntasMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idPregunta'+dato['idPregunta'])).html('<td>'+dato['idPregunta']+'</td>' +
                            '<td>'+dato['ind_descripcion_preguntas']+'</td>' +
                            '<td>'+dato['num_valor_minimo']+'</td>' +
                            '<td>'+dato['num_valor_maximo']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-04-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idPregunta="'+dato['idPregunta']+'"' +
                            'descipcion="El Usuario a Modificado una Pregunta" titulo="Modificar Pregunta">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-04-04-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPregunta="'+dato['idPregunta']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado una Pregunta" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Pregunta!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Registro se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                }else if(dato['status']=='creacion'){


                    if(dato['idPregunta']=='rep'){

                        $("#ind_descripcion_preguntas").focus();
                        swal("Error!", "Verifique la descripción, ya se encuentra registrada para el Area seleccionada.", "error");

                    }else{
                        $(document.getElementById('datatable1')).append('<tr id="idPregunta'+dato['idPregunta']+'">' +
                                '<td>'+dato['idPregunta']+'</td>' +
                                '<td>'+dato['ind_descripcion_preguntas']+'</td>' +
                                '<td>'+dato['num_valor_minimo']+'</td>' +
                                '<td>'+dato['num_valor_maximo']+'</td>' +
                                '<td  align="center">' +
                                '{if in_array('RH-01-04-04-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" title="Editar" idPregunta="'+dato['idPregunta']+'"' +
                                'descipcion="El Usuario a Modificado una Pregunta" titulo="Modificar Pregunta">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '{if in_array('RH-01-04-04-04-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPregunta="'+dato['idPregunta']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado una Pregunta" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Pregunta!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>' +
                                '</tr>');
                        swal("Registro Guardado!", "Registro guardado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }

                }
            },'json');
        }
    });




    $(document).ready(function() {
        //valido formulario
        $("#formAjax").validate();
        //ancho de la modal
        $('#modalAncho').css( "width", "65%" );


        $("#num_valor_min").change(function() {
            var min = $("#num_valor_min").val();
            var max = $("#num_valor_max").val();

            if(min >= max){
                swal("Información!", "El valor minimo no puede ser mayor o igual que el valor maximo.", "info");
            }

        });

        $("#num_valor_max").change(function() {
            var min = $("#num_valor_min").val();
            var max = $("#num_valor_max").val();

            if(max <= min){
                swal("Información!", "El valor maximo no puede ser menor o igual que el valor minimo.", "info");
            }
        });

    });






</script>