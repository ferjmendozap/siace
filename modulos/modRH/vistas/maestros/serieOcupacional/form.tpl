<form action="" id="formAjax" class="form" role="form" method="post" autocomplete="off">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idSerieOcupacional}" name="idSerieOcupacional"/>


        <div class="col-xs-5">
            <div class="form-group floating-label">
                <select id="fk_a006_num_miscelaneo_detalle_grupoocupacional" name="form[int][fk_a006_num_miscelaneo_detalle_grupoocupacional]" class="form-control input-sm" required data-msg-required="Seleccione el Grupo Ocupacional">
                    <option value="">&nbsp;</option>
                    {foreach item=grupo from=$GrupoOcupacional}
                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_grupoocupacional)}
                            {if $grupo.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_grupoocupacional}
                                <option selected value="{$grupo.pk_num_miscelaneo_detalle}">{$grupo.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$grupo.pk_num_miscelaneo_detalle}">{$grupo.ind_nombre_detalle}</option>
                            {/if}
                        {else}
                            <option value="{$grupo.pk_num_miscelaneo_detalle}">{$grupo.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle_grupoocupacional">Grupo Ocupacional</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <div class="col-xs-12">

            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nombre_serie)}{$formDB.ind_nombre_serie}{/if}" name="form[txt][ind_nombre_serie]" id="ind_nombre_serie" required data-msg-required="Introduzca descripción de la Serie Ocupacional" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_serie">Descripción</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>

            </div>

        </div>

        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"> <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            var app = new  AppFunciones();

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/serieOcupacionalCONTROL/NuevaSerieOcupacionalMET', datos ,function(dato){

                if(dato['status']=='modificacion'){
                    app.metActualizarRegistroTablaJson('datatableSerieOcupacional','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='creacion'){
                    app.metNuevoRegistroTablaJson('datatableSerieOcupacional','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }

            },'json');


        }
    });




    $().ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //dependiendo del tipo cargo seleccionado consulto en la base de datos cual es su ultimo nivel asigando
        //y los muestro en el formulario
        $("#fk_rhc006_num_cargo").change(function(){


            $.post('{$_Parametros.url}modRH/NivelesTiposCargosCONTROL/VerificarNivelMET', { cargo: $('#fk_rhc006_num_cargo').val() } ,function(dato){


                if(dato['ultimo']==null){
                    $("#num_nivel").val('0');
                    $("#num_nivel").focus();
                }else{
                    $("#num_nivel").val(dato['ultimo']);
                    $("#num_nivel").focus();
                }


            },'json');

        });


        //ancho de la modal
        $('#modalAncho').css( "width", "70%" );


    });








</script>