

<table class="table table-condensed no-margin">
    <thead>
    <tr>
        <th>Grado</th>
        <th>Paso</th>
        <th>Sueldo Minimo</th>
        <th>Sueldo Maximo</th>
        <th>Sueldo Promedio</th>
    </tr>
    </thead>
    <tbody>
    {foreach item=grado from=$Pasos}

        <tr id="idGradoSalarial{$grado.pk_num_grado}">
            <td>{$nombre_grado}</td>
            <td>{$grado.ind_nombre_detalle}</td>
            <td>{$grado.num_sueldo_minimo|number_format:2:",":"."}</td>
            <td>{$grado.num_sueldo_maximo|number_format:2:",":"."}</td>
            <td>{$grado.num_sueldo_promedio|number_format:2:",":"."}</td>

        </tr>
    {/foreach}
    </tbody>

</table>


<script type="text/javascript">

    $(document).ready(function() {


    });

</script>