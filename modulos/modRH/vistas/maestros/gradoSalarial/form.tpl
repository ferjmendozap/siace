<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idGradoSalarial}" name="idGradoSalarial"/>

        <div class="col-xs-5">
            <div class="form-group">
                <select id="fk_a006_num_miscelaneo_detalle" name="form[int][fk_a006_num_miscelaneo_detalle]" class="form-control input-sm" required data-msg-required="Seleccione la Categoria">
                    <option value="">Seleccione...</option>
                    {foreach item=cat from=$Categoria}
                        {if isset($formDB.fk_a006_num_miscelaneo_detalle)}
                            {if $cat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle}
                                <option selected value="{$cat.pk_num_miscelaneo_detalle}">{$cat.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$cat.pk_num_miscelaneo_detalle}">{$cat.ind_nombre_detalle}</option>
                            {/if}
                        {else}
                            <option value="{$cat.pk_num_miscelaneo_detalle}">{$cat.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_num_miscelaneo_detalle">Categoria</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <div class="col-xs-7">

            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_grado)}{$formDB.ind_grado}{/if}" name="form[txt][ind_grado]" id="ind_grado" required data-msg-required="Introduzca Grado Salarial" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_grado">Grado</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>

            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_nombre_grado)}{$formDB.ind_nombre_grado}{/if}" name="form[txt][ind_nombre_grado]" id="ind_nombre_grado" required data-msg-required="Introduzca Descripción del Grado" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_grado">Descripción</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>

            </div>

        </div>


        <div class="col-xs-2">

            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.flag_pasos) and $formDB.flag_pasos==1} checked{/if} value="1" name="form[int][flag_pasos]" id="flag_pasos">
                    <span>Aplica Pasos?</span>
                </label>
            </div>

        </div>
        <div class="col-xs-1">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked {elseif $default==1 } checked {/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="col-xs-3">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.num_sueldo_minimo)}{$formDB.num_sueldo_minimo|number_format:2:",":"."}{/if}" name="form[int][num_sueldo_minimo]" id="num_sueldo_minimo" required data-msg-required="Introduzca Sueldo Minimo">
                <label for="num_sueldo_minimo">Sueldo Minimo</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.num_sueldo_maximo)}{$formDB.num_sueldo_maximo|number_format:2:",":"."}{/if}" name="form[int][num_sueldo_maximo]" id="num_sueldo_maximo" required data-msg-required="Introduzca Sueldo Maximo">
                <label for="num_sueldo_maximo">Sueldo Maximo</label>
                <p class="help-block "><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>
        <div class="col-xs-3">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.num_sueldo_promedio)}{$formDB.num_sueldo_promedio|number_format:2:",":"."}{/if}" name="form[int][num_sueldo_promedio]" id="num_sueldo_promedio" required data-msg-required="Introduzca Sueldo Promedio">
                <label for="num_sueldo_promedio">Sueldo Promedio</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>


    <div id="pasos">

        <div class="row">
            <div class="col-sm-12 text-center"><span class="label label-primary">Informaci&oacute;n de los Pasos</span></div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body no-padding">
                        <div class="table-responsive no-margin">
                            <table class="table table-striped no-margin" id="contenidoTabla">
                                <thead>
                                <tr>
                                    <th>Paso</th>
                                    <th>Sueldo Minimo</th>
                                    <th>Sueldo Maximo</th>
                                    <th>Sueldo Promedio</th>
                                </tr>
                                </thead>
                                <tbody>

                                {if isset($formDBpasos)}
                                    {foreach item=datos from=$formDBpasos}
                                        <tr>
                                            <input type="hidden" value="{$datos.pk_num_grados_pasos}" name="form[int][pk_num_grados_pasos][{$n}]">

                                            <td style="vertical-align: middle;">
                                                <div class="col-sm-8">
                                                    <div class="form-group">
                                                        <select id="fk_a006_num_miscelaneo_detalle_paso{$n}" name="form[int][fk_a006_num_miscelaneo_detalle_paso][{$n}]" class="form-control input-sm" required>
                                                            <option value="">Seleccione...</option>
                                                            {foreach item=dat from=$Pasos}
                                                                {if isset($datos.fk_a006_num_miscelaneo_detalle_paso)}
                                                                    {if $dat.pk_num_miscelaneo_detalle==$datos.fk_a006_num_miscelaneo_detalle_paso}
                                                                        <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm bolivar-mask" value="{$datos.num_sueldo_minimo|number_format:2:",":"."}" name="form[int][num_sueldo_minimo][{$n}]" id="num_sueldo_minimo{$n}" required>
                                                        <label for="num_sueldo_minimo">Sueldo Minimo</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm bolivar-mask" value="{$datos.num_sueldo_maximo|number_format:2:",":"."}" name="form[int][num_sueldo_maximo][{$n}]" id="num_sueldo_maximo{$n}" required>
                                                        <label for="num_sueldo_maximo">Sueldo Maximo</label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="vertical-align: middle;">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control input-sm bolivar-mask" value="{$datos.num_sueldo_promedio|number_format:2:",":"."}" name="form[int][num_sueldo_promedio][{$n}]" id="num_sueldo_promedio{$n}" required>
                                                        <label for="num_sueldo_promedio">Sueldo Promedio</label>
                                                    </div>
                                                </div>
                                            </td>

                                            <input type="hidden" value="{$n++}">
                                            <input type="hidden" value="{$numero++}">

                                        </tr>
                                    {/foreach}
                                {/if}
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="4">
                                        <div class="col-sm-12 text-center">
                                            <button type="button" id="nuevoPaso" class="btn btn-info ink-reaction btn-raised">
                                                <i class="md md-add"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


        <!-- ******************************* sueldos ***************************************************-->



        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            var app = new AppFunciones();

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/gradoSalarialCONTROL/NuevoGradoSalarialMET', datos ,function(dato){


                if(dato['status']=='error'){

                    /*Error para usuarios normales del sistema*/
                    //swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");

                }
                else if(dato['status']=='modificacion'){
                    app.metActualizarRegistroTablaJson('datatableGradoSalarial','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='creacion'){
                    app.metNuevoRegistroTablaJson('datatableGradoSalarial','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //para dar formato a los montos
        $("#num_sueldo_minimo").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        $("#num_sueldo_maximo").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        $("#num_sueldo_promedio").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });

        //al teclear sobre el sueldo maximo calculo el sueldo promedio
        $("#num_sueldo_maximo").keyup(function(){

            var s_min = $("#num_sueldo_minimo").val();
            var s_max = $("#num_sueldo_maximo").val();

        });

        //oculto el formulario de pasos
        if ($('#flag_pasos').is(':checked')){
            $("#pasos").show();
            $("#num_sueldo_minimo").prop("disabled",true);
            $("#num_sueldo_maximo").prop("disabled",true);
            $("#num_sueldo_promedio").prop("disabled",true);
            //$("#flag_pasos").prop("disabled",true);
        }else{
            $("#pasos").hide();
        }


        //ancho de la modal
        $('#modalAncho').css( "width", "70%" );

        $("#flag_pasos").click(function() {

            if ($('#flag_pasos').is(':checked')) {

               $("#pasos").show();
               $("#num_sueldo_minimo").prop("disabled",true);
               $("#num_sueldo_maximo").prop("disabled",true);
               $("#num_sueldo_promedio").prop("disabled",true);
               $("#num_sueldo_minimo").val('');
               $("#num_sueldo_maximo").val('');
               $("#num_sueldo_promedio").val('');

            }else{

                $("#pasos").hide();
                $("#num_sueldo_minimo").prop("disabled",false);
                $("#num_sueldo_maximo").prop("disabled",false);
                $("#num_sueldo_promedio").prop("disabled",false);
                $("#contenidoTabla > tbody > tr").html('');

            }

        });

        $('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });

        $("#nuevoPaso").click(function() {
            var idtabla= $("#contenidoTabla");
            var tr= $("#contenidoTabla > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;

            var eliminar = '<button class="eliminarTMP btn ink-reaction btn-raised btn-xs btn-danger" idPaso=paso'+numero+'  boton="si, Eliminar" descipcion="Ha Eliminado la linea" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Linea!!"><i class="md md-delete" style="color: #ffffff;"></i></button>';


            var pasos='<div class="form-group">'+
                    '<select id="fk_a006_num_miscelaneo_detalle_paso'+nuevoTr+'" name="form[int][fk_a006_num_miscelaneo_detalle_paso]['+nuevoTr+']" class="form-control input-sm" required data-msg-required="Seleccione El Paso">' +
                    '{foreach item=i from=$Pasos}' +
                    '<option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>' +
                    '{/foreach}' +
                    '</select>' +
                    '</div>';


            var sueldo_minimo='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" name="form[int][num_sueldo_minimo]['+nuevoTr+']" id="num_sueldo_minimo'+nuevoTr+'" required>'+
                    '<label for="num_sueldo_minimo'+nuevoTr+'"><i class="md md-insert-comment"></i> Sueldo Minimo</label>'+
                    '</div>';

            var sueldo_maximo='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" name="form[int][num_sueldo_maximo]['+nuevoTr+']" id="num_sueldo_maximo'+nuevoTr+'" required>'+
                    '<label for="num_sueldo_maximo'+nuevoTr+'"><i class="md md-insert-comment"></i> Sueldo Maximo</label>'+
                    '</div>';

            var sueldo_promedio='<div class="form-group">'+
                    '<input type="text" class="form-control input-sm" name="form[int][num_sueldo_promedio]['+nuevoTr+']" id="num_sueldo_promedio'+nuevoTr+'" required>'+
                    '<label for="num_sueldo_promedio'+nuevoTr+'"><i class="md md-insert-comment"></i> Sueldo Promedio</label>'+
                    '</div>';

            idtabla.append('<tr id=paso'+numero+'>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+pasos+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+sueldo_minimo+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+sueldo_maximo+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+sueldo_promedio+'</div></td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+eliminar+'</div></td>'+
                    '</tr>');

            $("#num_sueldo_minimo"+nuevoTr).maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
            $("#num_sueldo_maximo"+nuevoTr).maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
            $("#num_sueldo_promedio"+nuevoTr).maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        });

        $('#contenidoTabla tbody').on( 'click', '.eliminarTMP', function () {

            var id=$(this).attr('idPaso');
            $(document.getElementById(id)).remove();
            /*swal("Eliminado!", "Linea Eliminada.", "success");
            $('#cerrar').click();*/
        });



    });






</script>