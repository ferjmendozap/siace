
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro de Asignacion de Beneficios HCM</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">

                        <div class="card-body">
                            <table id="datatableBeneficios" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="80">Id</th>
                                    <th>Beneficio</th>
                                    <th>Descripción Asignación</th>
                                    <th>Limite</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=b from=$BeneficiosHcm}

                                    <tr id="idBeneficioHcm{$b.pk_num_ayuda_especifica}">
                                        <td>{$b.pk_num_ayuda_especifica}</td>
                                        <td>{$b.ind_descripcion}</td>
                                        <td>{$b.ind_descripcion_especifica}</td>
                                        <td>{$b.num_limite_esp|number_format:2:",":"."}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idBeneficioHcm="{$b.pk_num_ayuda_especifica}"
                                                        descipcion="El Usuario a Modificado un Beneficio HCM" titulo="Modificar Beneficio HCM">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" title="Eliminar" idBeneficioHcm="{$b.pk_num_ayuda_especifica}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Beneficio HCM" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Beneficio HCM!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="5">
                                        {if in_array('RH-01-04-07-06-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Beneficio HCM"  title="Nuevo Beneficio HCM" titulo="Nuevo Beneficio HCM" id="nuevo" >
                                                    <i class="md md-create"></i> Nuevo Beneficio HCM &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    tr.group,
    tr.group:hover {
        background-color: #d1f5e3 !important;
        font-weight: bold;
    }
</style>
<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/beneficiosHcmCONTROL/BeneficioHcmMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatableBeneficios tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idBeneficioHcm: $(this).attr('idBeneficioHcm')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatableBeneficios tbody').on( 'click', '.eliminar', function () {

            var idBeneficioHcm=$(this).attr('idBeneficioHcm');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/beneficiosHcmCONTROL/EliminarBeneficioHcmMET';
                $.post($url, { idBeneficioHcm: idBeneficioHcm},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idBeneficioHcm'+$dato['idBeneficioHcm'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

        var table = $('#datatableBeneficios').DataTable({
            "columnDefs": [
                { "visible" : false, "targets": 1 }
            ],
            "order": [[ 1, 'asc' ]],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "drawCallback" : function (settings) {
                var api = this.api();
                var rows = api.rows( { page:'current'} ).nodes();
                var last = null;

                api.column(1, { page:'current'} ).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        // Order by the grouping
        $('#tablaFeriados tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                table.order([1, 'desc']).draw();
            }
            else {
                table.order([1, 'asc']).draw();
            }
        });
    });
</script>