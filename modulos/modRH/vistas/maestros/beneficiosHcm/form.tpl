<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idBeneficioHcm}" name="idBeneficioHcm"/>



        <!-- ***************************ID ASIGNACION*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_ayuda_especifica)}{$formDB.pk_num_ayuda_especifica}{/if}" name="form[int][pk_num_ayuda_especifica]" id="pk_num_ayuda_especifica" readonly>
                <label for="pk_num_ayuda_especifica">Id</label>
            </div>
        </div>

        <!-- ************************** BENEFICIO GLOBAL*****************---------->
        <div class="col-xs-7">
            <div class="form-group floating-label">
                <select id="fk_rhb008_num_ayuda_global" name="form[int][fk_rhb008_num_ayuda_global]" class="form-control input-sm" required data-msg-required="Seleccione el beneficio Global">
                    <option value="">&nbsp;</option>
                    {foreach item=b from=$Global}
                        {if isset($formDB.fk_rhb008_num_ayuda_global)}
                            {if $b.pk_num_ayuda_global==$formDB.fk_rhb008_num_ayuda_global}
                                <option selected value="{$b.pk_num_ayuda_global}">{$b.ind_descripcion}</option>
                            {else}
                                <option value="{$b.pk_num_ayuda_global}">{$b.ind_descripcion}</option>
                            {/if}
                        {else}
                            <option value="{$b.pk_num_ayuda_global}">{$b.ind_descripcion}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_rhb008_num_ayuda_global">Beneficio Global</label>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************DESCRIPCION*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_descripcion_especifica)}{$formDB.ind_descripcion_especifica}{/if}" name="form[txt][ind_descripcion_especifica]" id="ind_descripcion_especifica" maxlength="200" required data-msg-required="Introduzca Descripción de la Asignación" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_descripcion_especifica">Descripción Asignación</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************LIMITE*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm bolivar-mask" value="{if isset($formDB.num_limite_esp)}{$formDB.num_limite_esp|number_format:2:",":"."}{/if}" name="form[int][num_limite_esp]" id="num_limite_esp" maxlength="20" required data-msg-required="Introduzca Limite de la Asignación" >
                <label for="num_limite_esp">Limite Asignación</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- *************************** APROBADOR **************** --------->
        <div class="col-xs-12">
            <div class="form-group">
                <label for="num_empleado_apro">Aprueba</label>
                <select id="num_empleado_apro" name="form[int][num_empleado_apro]" class="form-control select2-list select2">
                    <option value="">Seleccione Empleado</option>
                    {foreach item=i from=$Empleado}
                        {if isset($formDB.fk_rhb001_num_empleado)}
                            {if $i.pk_num_empleado==$formDB.fk_rhb001_num_empleado}
                                <option selected value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                            {else}
                                <option value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                            {/if}
                        {else}
                            <option value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                        {/if}
                    {/foreach}
                </select>
                <p class="help-block"><span class="text-xs" style="color: red">* El campo es Obligatorio</span></p>
            </div>
        </div>

        <!-- ***************************ESTATUS*********************--------->
        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    {if $control==1}
                        <input type="checkbox" {if isset($formDB.flag_extensible) and $formDB.flag_extensible==1} checked{/if} value="1" name="form[int][flag_extensible]" disabled>
                    {elseif $control==2}
                        <input type="checkbox" {if isset($formDB.flag_extensible) and $formDB.flag_extensible==1} checked{/if} value="1" name="form[int][flag_extensible]">
                    {/if}
                    <span>Extensible ?</span>
                </label>
            </div>
        </div>





        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            if($("#num_empleado_apro").val()==''){
                swal("Error!", "Debe indicar Empleado Aprobador.", "error");
                return false;
            }

            $.post('{$_Parametros.url}modRH/maestros/beneficiosHcmCONTROL/BeneficioHcmMET', datos ,function(dato){


                if(dato['status']=='error'){
                    /*Error para usuarios normales del sistema*/
                    swal("Fallo!", "No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    /*Error para los programadores para visualizar en detalle el ERROR*/
                    //swal("Fallo!", "Error: "+ dato['detalleERROR'], "error");
                }
                else if(dato['status']=='errorLimite'){
                    swal("Error!", dato['mensaje'], "error");
                }
                else if(dato['status']=='modificacion'){
                    $(document.getElementById('idBeneficioHcm'+dato['idBeneficioHcm'])).html('<td>'+dato['idBeneficioHcm']+'</td>' +
                            '<td>'+dato['ind_descripcion_especifica']+'</td>' +
                            '<td>'+dato['num_limite_esp']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idBeneficioHcm="'+dato['idBeneficioHcm']+'"' +
                            'descipcion="El Usuario a Modificado un Bebeficio HCM" titulo="Modificar Beneficio HCM">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficioHcm="'+dato['idBeneficioHcm']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Beneficio HCM" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Beneficio HCM!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Registro se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){
                    $(document.getElementById('datatableBeneficios')).append('<tr id="idBeneficioHcm'+dato['idBeneficioHcm']+'">' +
                            '<td>'+dato['idBeneficioHcm']+'</td>' +
                            '<td>'+dato['ind_descripcion_especifica']+'</td>' +
                            '<td>'+dato['num_limite_esp']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idBeneficioHcm="'+dato['idBeneficioHcm']+'"' +
                            'descipcion="El Usuario a Modificado un Bebeficio HCM" titulo="Modificar Beneficio HCM">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficioHcm="'+dato['idBeneficioHcm']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Beneficio HCM" title="Eliminar"  titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Beneficio HCM!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                    swal("Registro Guardado!", "Registro guardado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "70%" );


    });






</script>