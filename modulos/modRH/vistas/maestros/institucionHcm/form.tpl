<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />

        <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"    id="accion" />
        <input type="hidden" value="{if isset($formDB[0]['pk_num_institucion'])}{$formDB[0]['pk_num_institucion']}{/if}"    name="form[int][id_institucion]" />


        <div class="col-lg-6">
        <!-- *************************** NOMBRE INSTITUCIÓN *********************--------->
        <div class="col-lg-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB[0]['ind_nombre_institucion'])}{$formDB[0]['ind_nombre_institucion']}{/if}" name="form[txt][ind_nombre_institucion]" id="ind_nombre_institucion" maxlength="50" required data-msg-required="Introduzca Nombre Institución" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_descripcion_utiles">Nombre</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>



            <!-- *************************** TELEFONO 1  *********************--------->
            <div class="col-lg-12">
                <div class="form-group floating-label">
                    <input type="text" class="form-control input-sm" value="{if isset($formDB[0]['ind_telefono1'])}{$formDB[0]['ind_telefono1']}{/if}" name="form[txt][ind_telefono1]" id="ind_telefono1" maxlength="11">
                    <label for="ind_descripcion_utiles">Telefono 1 </label>

                </div>
            </div>

            <!-- *************************** TELEFONO 2  *********************--------->
            <div class="col-lg-12">
                <div class="form-group floating-label">
                    <input type="text" class="form-control input-sm" value="{if isset($formDB[0]['ind_telefono2'])}{$formDB[0]['ind_telefono2']}{/if}" name="form[txt][ind_telefono2]" id="ind_telefono2" maxlength="11">
                    <label for="ind_descripcion_utiles">Telefono 2 </label>

                </div>
            </div>
            <!-- *************************** TIPO DE INSTITUCIÓN *********************--------->
            <div class="col-lg-12">
                <div class="form-group floating-label">
                    <select id="fk_rhb008_num_ayuda_global" name="form[int][fk_a006_tipo_centro]" class="form-control input-sm" required data-msg-required="Seleccione el campo">

                        {foreach item=b from=$_TipoPost}
                            {if isset($formDB[0]['fk_a006_tipo_centro'])}
                                {if $b.cod_detalle==$formDB[0]['fk_a006_tipo_centro']}
                                    <option selected value="{$b.cod_detalle}">{$b.ind_nombre_detalle}</option>
                                {else}
                                    <option value="{$b.cod_detalle}">{$b.ind_nombre_detalle}</option>
                                {/if}
                            {else}
                                <option value="{$b.cod_detalle}">{$b.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_rhb008_num_ayuda_global">Tipo  </label>
                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                </div>
            </div>

            <!-- *************************** CLASE DE INSTITUCIÓN *********************--------->
            <div class="col-lg-12">
                <div class="form-group floating-label">
                    <select id="fk_rhb008_num_ayuda_global" name="form[int][fk_a006_clase_centro]" class="form-control input-sm" required data-msg-required="Seleccione el campo">

                        {foreach item=b from=$_ClasePost}
                            {if isset($formDB[0]['fk_a006_clase_centro'])}
                                {if $b.cod_detalle==$formDB[0]['fk_a006_clase_centro']}
                                    <option selected value="{$b.cod_detalle}">{$b.ind_nombre_detalle}</option>
                                {else}
                                    <option value="{$b.cod_detalle}">{$b.ind_nombre_detalle}</option>
                                {/if}
                            {else}
                                <option value="{$b.cod_detalle}">{$b.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_rhb008_num_ayuda_global">Clase </label>
                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                </div>
            </div>






        </div>
        <div class="col-lg-6">
        <!-- *************************** NOMBRE DIRECCIÓN  *********************--------->
        <div class="col-lg-12">

            <div class="form-group floating-label">
                <textarea placeholder="" rows="3" class="form-control" id="ind_ubicacion" name="form[txt][ind_ubicacion]">{if isset($formDB[0]['ind_ubicacion'])}{$formDB[0]['ind_ubicacion']}{/if}</textarea>
                <label for="textarea1">Dirección </label>
            </div>

            <div class="card">
                <div class="card-body no-padding">
                    <ul class="list" data-sortable="true">

                        <li class="tile">
                            <div class="checkbox checkbox-styled  ">
                                <label>
                                    <input type="checkbox" name="form[txt][flag_convenio_ces]" {if isset($formDB[0]['flag_convenio_ces'])}{if $formDB[0]['flag_convenio_ces']==1}checked  {/if}{/if} >
                                            <span>
                                                Convenio
                                            </span>
                                </label>
                            </div>
                        </ul>
                </div>
            </div>

        </div>
        </div>
        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({

        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();
            var accion = $("#accion").val();
            if(accion=="Registrar") {
                $.post('{$_Parametros.url}modRH/maestros/institucionHcmCONTROL/NuevaInstitucionMET', datos, function (dato) {

                    if(dato['status']=='error'){

                        swal("Error!", "No se pudo realizar Operación. Consultar con el Administrador del Sistema.", "error");

                    }
                    else if(dato['status']=='OK'){

                        swal("Registro Agregado!", "La institución se agregó satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                        $("#_contenido").load('{$_Parametros.url}modRH/maestros/institucionHcmCONTROL');

                    }

                }, 'json');

            }else if(accion=="Modificar") {
                $.post('{$_Parametros.url}modRH/maestros/institucionHcmCONTROL/ModificarInstitucionMET', datos, function (dato) {

                    if(dato['status']=='error'){

                        swal("Error!", "No se pudo realizar Operación. Consultar con el Administrador del Sistema.", "error");

                    }
                    else if(dato['status']=='OK'){

                        swal("Registro Modificado!", "La institución se modificó satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                        $("#_contenido").load('{$_Parametros.url}modRH/maestros/institucionHcmCONTROL');
                    }

                }, 'json');

            }
        }

    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "65%" );


    });






</script>