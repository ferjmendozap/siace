
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Médicos HCM </h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="150">Id</th>
                                    <th class="col-sm-9">Médico</th>
                                    <th class="col-sm-1">Modificar</th>
                                    <th class="col-sm-1">Eliminar</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=f from=$Medicos}

                                        <tr id="idMedico{$f.pk_num_medico_hcm}">
                                            <td>{$f.pk_num_medico_hcm}</td>
                                            <td>{$f.ind_nombre1} {$f.ind_apellido1}</td>
                                            <td align="center" width="100">
                                                {if in_array('RH-01-04-07-16-M',$_Parametros.perfil)}
                                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                            data-keyboard="false" data-backdrop="static" title="Editar" idMedico="{$f.pk_num_medico_hcm}"
                                                            descipcion="El Usuario a Modificado los datos de un Médico" titulo="Modificar Médico">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                &nbsp;&nbsp;
                                            </td>
                                            <td>
                                                {if in_array('RH-01-04-07-17-E',$_Parametros.perfil)}
                                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idMedico="{$f.pk_num_medico_hcm}"  boton="si, Eliminar"
                                                            descipcion="El usuario a eliminado el registro de un médico" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar médico!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            </td>
                                        </tr>

                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="4">
                                        {if in_array('RH-01-04-07-15-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="El Usuario a registrado una Médico" title="Registrar Médico"  titulo="Registrar Médico" id="nuevo" >
                                                <i class="md md-create"></i> Ingresar Médico &nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        $('#nuevo').click(function(){
            var $url='{$_Parametros.url}modRH/maestros/medicosHcmCONTROL/RegistrarMedicoMET';

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            var $url='{$_Parametros.url}modRH/maestros/medicosHcmCONTROL/ModificarMedicoMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idMedico')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idMedico=$(this).attr('idMedico');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/medicosHcmCONTROL/EliminarMedicoHcmMET';
                $.post($url, { idMedico: idMedico},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idMedico'+$dato['idMedico'])).html('');
                        swal("Eliminado!", "Utiles Escolares Eliminado.", "success");
                        $('#cerrar').click();
                    }

                },'json');
            });
        });
    });
</script>