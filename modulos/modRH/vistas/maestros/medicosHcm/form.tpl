<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />

        <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"    id="accion" />
        <input type="hidden" value="{if isset($formDB[0]['pk_num_medico_hcm'])}{$formDB[0]['pk_num_medico_hcm']}{/if}"    name="form[int][id_medico]" />



            <div class="col-lg-6">
            <!-- *************************** PRIMER NOMBRE   *********************--------->
            <div class="col-lg-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB[0]['ind_nombre1'])}{$formDB[0]['ind_nombre1']}{/if}" name="form[txt][nombre]" id="nombre" maxlength="50" required data-msg-required="Introduzca Primer Nombre " onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_descripcion_utiles">Nombre</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
            </div>

            <!-- *************************** PRIMER APELLIDO  *********************--------->
            <div class="col-lg-12">
                <div class="form-group floating-label">
                    <input type="text" class="form-control input-sm" value="{if isset($formDB[0]['ind_apellido1'])}{$formDB[0]['ind_apellido1']}{/if}" name="form[txt][apellido]" id="apellido" maxlength="50" required data-msg-required="Introduzca Primer Apellido " onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                    <label for="ind_descripcion_utiles">Apellido</label>
                    <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
                </div>
            </div>

            <!-- *************************** CÉDULA  *********************--------->
            <div class="col-lg-12">
                <div class="form-group floating-label">
                    <input type="text" class="form-control input-sm" value="{if isset($formDB[0]['ind_cedula_documento'])}{$formDB[0]['ind_cedula_documento']}{/if}" name="form[txt][cedula]" id="cedula" maxlength="50" required data-msg-required="Introduzca Primer Apellido " onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                    <label for="ind_descripcion_utiles">Cédula</label>

                </div>
            </div>


            <!-- *************************** TELEFONO 1  *********************--------->
            <div class="col-lg-12">
                <div class="form-group floating-label">
                    <input type="text" class="form-control input-sm" value="{if isset($formDB[0]['ind_telefono'])}{$formDB[0]['ind_telefono']}{/if}" name="form[txt][telefono]" id="telefono" maxlength="11">
                    <label for="ind_descripcion_utiles">Telefono  </label>

                </div>
            </div>




            <!-- *************************** CLASE DE INSTITUCIÓN *********************--------->
            <div class="col-lg-12">
                <div class="form-group floating-label">
                    <select id="institucion" name="form[int][institucion]" class="form-control input-sm" >

                        {foreach item=b from=$_InstitucionesPost}
                            {if isset($formDB[0]['fk_rhc040_institucion'])}
                                {if $b.pk_num_institucion==$formDB[0]['fk_rhc040_institucion']}
                                    <option selected value="{$b.pk_num_institucion}">{$b.ind_nombre_institucion}</option>
                                {else}
                                    <option value="{$b.pk_num_institucion}">{$b.ind_nombre_institucion}</option>
                                {/if}
                            {else}
                                <option value="{$b.pk_num_institucion}">{$b.ind_nombre_institucion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_rhb008_num_ayuda_global">Centro Médico </label>

                </div>
            </div>






        </div>
        <div class="col-lg-6">
        <!-- *************************** NOMBRE DIRECCIÓN  *********************--------->
        <div class="col-lg-12">



            <div class="card">
                <div class="card-body no-padding">
                    <ul class="list" data-sortable="true">

                        <li class="tile">
                            <div class="checkbox checkbox-styled  ">
                                <label>
                                    <!--<input type="checkbox" name="form[txt][flag_convenio_ces]" {if isset($formDB[0]['flag_convenio_ces'])}{if $formDB[0]['flag_convenio_ces']==1}checked  {/if}{/if} >-->
                                    <input type="checkbox" {if isset($formDB[0].flag_convenio_ces) and $formDB[0].flag_convenio_ces==1} checked{/if} value="1" id="flag_convenio_ces" name="form[int][flag_convenio_ces]">
                                            <span>
                                                Convenio
                                            </span>
                                </label>

                            </div>
                        </li>
                        <li class="tile">
                            <div class="checkbox checkbox-styled  ">
                                <label>
                                    <!--<input type="checkbox" name="form[txt][flag_interno_ces]" {if isset($formDB[0]['flag_interno_ces'])}{if $formDB[0]['flag_interno_ces']==1}checked  {/if}{/if} >-->
                                    <input type="checkbox" {if isset($formDB[0].flag_interno_ces) and $formDB[0].flag_interno_ces==1} checked{/if} value="1" id="flag_interno_ces" name="form[int][flag_interno_ces]">
                                            <span>
                                                Interno
                                            </span>
                                </label>

                            </div>
                        </li>
                        </ul>
                </div>
            </div>

        </div>
        </div>
        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();
            var accion = $("#accion").val();

            if(accion=="Registrar") {
                $.post('{$_Parametros.url}modRH/maestros/medicosHcmCONTROL/RegistrarMedicoMET', datos, function (dato) {

                    if(dato['status']=='error'){

                        swal("Error!", "No se pudo realizar Operación. Consultar con el Administrador del Sistema.", "error");

                    }
                    else if(dato['status']=='OK'){

                        swal("Registro Agregado!", "El médico se agregó satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                        $("#_contenido").load('{$_Parametros.url}modRH/maestros/medicosHcmCONTROL');

                    }


                }, 'json');

            }else if(accion=="Modificar") {
                $.post('{$_Parametros.url}modRH/maestros/medicosHcmCONTROL/ModificarMedicoMET', datos, function (dato) {

                    if(dato['status']=='error'){

                        swal("Error!", "No se pudo realizar Operación. Consultar con el Administrador del Sistema.", "error");

                    }
                    else if(dato['status']=='OK'){

                        swal("Registro Modificado!", "El médico se modificó satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                        $("#_contenido").load('{$_Parametros.url}modRH/maestros/medicosHcmCONTROL');
                    }

                }, 'json');

            }

        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "65%" );


    });






</script>