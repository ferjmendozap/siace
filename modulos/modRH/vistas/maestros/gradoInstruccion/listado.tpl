
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Grado de Instrucción</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <input type="hidden" value="{if isset($idMaestro.fk_a005_num_miscelaneo_maestro)}{$idMaestro.fk_a005_num_miscelaneo_maestro}{/if}" name="idMaestro" id="idMaestro"/>
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="150">Grado</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=grado from=$GradoInstruccion}

                                    <tr id="idGradoInstruccion{$grado.pk_num_miscelaneo_detalle}">
                                        <td>{$grado.pk_num_miscelaneo_detalle}</td>
                                        <td>{$grado.ind_nombre_detalle}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-02-03-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idGradoInstruccion="{$grado.pk_num_miscelaneo_detalle}"
                                                        descipcion="El Usuario a Modificado un Cargo" titulo="Modificar Cargo">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-02-04-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idGradoInstruccion="{$grado.pk_num_miscelaneo_detalle}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Cargo" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Cargo!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="3">
                                        {if in_array('RH-01-04-02-02-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Grado de Instruccion" title="Nuevo Grado de Instrucción" titulo="Nuevo Grado de Instrucción" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Grado de Instrucción &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/gradoInstruccionCONTROL/NuevoGradoInstruccionMET';

        $('#nuevo').click(function(){
            var maestro = $("#idMaestro").val();
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ maestro:maestro },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idGradoInstruccion: $(this).attr('idGradoInstruccion')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idGradoInstruccion=$(this).attr('idGradoInstruccion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/gradoInstruccionCONTROL/EliminarGradoInstruccionMET';
                $.post($url, { idGradoInstruccion: idGradoInstruccion},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idGradoInstruccion'+$dato['idGradoInstruccion'])).html('');
                        swal("Eliminado!", "El Grado de Instruccion fue Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>