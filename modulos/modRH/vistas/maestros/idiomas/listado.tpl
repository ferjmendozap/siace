
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Idiomas</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="170">Idioma</th>
                                    <th>Descripción Local</th>
                                    <th>Descripción Extranjera</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=idioma from=$Idiomas}

                                    <tr id="idIdioma{$idioma.pk_num_idioma}">
                                        <td>{$idioma.pk_num_idioma}</td>
                                        <td>{$idioma.ind_nombre_idioma}</td>
                                        <td>{$idioma.ind_nombre_ext}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-02-23-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Eliminar" idIdioma="{$idioma.pk_num_idioma}"
                                                        descipcion="El Usuario a Modificado un Idioma" titulo="Modificar Idioma">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-02-24-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idIdioma="{$idioma.pk_num_idioma}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Idioma" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Idioma!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="4">
                                        {if in_array('RH-01-04-02-22-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Nuevo Idioma" title="Nuevo Idioma" titulo="Nuevo Idioma" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Idioma &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/idiomasCONTROL/nuevoIdiomaMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idIdioma: $(this).attr('idIdioma')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idIdioma=$(this).attr('idIdioma');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/idiomasCONTROL/EliminarIdiomaMET';
                $.post($url, { idIdioma: idIdioma},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idIdioma'+$dato['idIdioma'])).html('');
                        swal("Eliminado!", "El Idioma fue Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>