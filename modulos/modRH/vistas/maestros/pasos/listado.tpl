
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Pasos</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <input type="hidden" value="{if isset($idMaestro.fk_a005_num_miscelaneo_maestro)}{$idMaestro.fk_a005_num_miscelaneo_maestro}{/if}" name="idMaestro" id="idMaestro"/>
                            <table id="datatablePasos" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="col-sm-1">Cod</th>
                                    <th>Pasos</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tfoot>
                                    <th colspan="3">
                                        {if in_array('RH-01-04-01-22-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Paso" title="Nuevo Paso" titulo="Nuevo Paso" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Paso&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/pasosCONTROL/NuevoPasoMET';

        //INICIALIZAR LA DATATABLES
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#datatablePasos',
                "{$_Parametros.url}modRH/maestros/pasosCONTROL/JsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_miscelaneo_detalle" },
                    { "data": "ind_nombre_detalle" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );

        $('#nuevo').click(function(){
            var maestro = $("#idMaestro").val();
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { maestro:maestro } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatablePasos tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPasos: $(this).attr('idPasos')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatablePasos tbody').on( 'click', '.eliminar', function () {

            var idPasos=$(this).attr('idPasos');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/pasosCONTROL/EliminarPasosMET';
                $.post($url, { idPasos: idPasos},function($dato){
                    if($dato['status']=='OK'){
                        app.metEliminarRegistroJson('datatablePasos','El Paso fue eliminado.','cerrarModal','ContenidoModal');
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>