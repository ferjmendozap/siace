
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Niveles Tipos de Cargos</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatableNivelTipoCargo" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Nivel</th>
                                    <th>Tipos de Cargos</th>
                                    <th>Descripción</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <!--
                                <tbody>
                                {foreach item=niveles from=$NivelesTiposCargos}
                                    <tr id="idNivelesTiposCargos{$niveles.pk_num_nivel}">
                                        <td>{$niveles.num_nivel}</td>
                                        <td>{$niveles.ind_nombre_cargo}</td>
                                        <td>{$niveles.ind_nombre_nivel}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-01-07-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idNivelesTiposCargos="{$niveles.pk_num_nivel}"
                                                        descipcion="El Usuario a Modificado un Nivel de cargo" titulo="Modificar Nivel de Cargo">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-01-08-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idNivelesTiposCargos="{$niveles.pk_num_nivel}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Nivel del cargo" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Nivel de Cargo!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                -->
                                <tfoot>
                                    <th colspan="4">
                                        {if in_array('RH-01-04-01-06-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Nuevo Nivel Tipo Cargo" title="Nuevo Nivel Tipo Cargo" titulo="Nuevo Nivel Tipo Cargo" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Nivel Tipo de Cargo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    tr.group,
    tr.group:hover {
        background-color: #d1f5e3 !important;
        font-weight: bold;
    }
</style>
<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/nivelesTiposCargosCONTROL/NuevoNivelesTiposCargosMET';

        var app = new AppFunciones();

        //EVENTO DEL BOTON NUEVO
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });


        //INICIALIZAR DATA
        var table = $('#datatableNivelTipoCargo').DataTable({
            "dom": 'lCfrtip',
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": '{$_Parametros.url}modRH/maestros/nivelesTiposCargosCONTROL/JsonDataTablaMET',
                "type": "POST",
                "data": function ( d ) {
                    //d.filtro = filtrado;
                }
            },
            "columnDefs": [
                { "visible" : false, "targets": 1 }
            ],
            "order": [[ 1, 'asc' ]],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "processing":     "Procesando...",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "loadingRecords": "Cargando...",
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            },
            "columns": [
                { "data": "num_nivel" },
                { "data": "ind_nombre_cargo" },
                { "data": "ind_nombre_nivel" },
                { "orderable": false,"data": "acciones",'width':100 }
            ],
            "drawCallback" : function (settings) {
                var api = this.api();
                var rows = api.rows( { page:'current'} ).nodes();
                var last = null;

                api.column(1, { page:'current'} ).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                                '<tr class="group"><td colspan="4"> Tipo de Cargo: ' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        // Order by the grouping
        $('#datatableNivelTipoCargo tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 1 && currentOrder[1] === 'asc') {
                table.order([1, 'desc']).draw();
            }
            else {
                table.order([1, 'asc']).draw();
            }
        });


        //EVENTO DEL BOTON MODIFICAR
        $('#datatableNivelTipoCargo tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idNivelesTiposCargos: $(this).attr('idNivelesTiposCargos')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });

        //EVENTO DEL BOTON ELIMINAR
        $('#datatableNivelTipoCargo tbody').on( 'click', '.eliminar', function () {

            var idNivelesTiposCargos=$(this).attr('idNivelesTiposCargos');

            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/nivelesTiposCargosCONTROL/EliminarNivelTipoCargoMET';
                $.post($url, { idNivelesTiposCargos: idNivelesTiposCargos},function($dato){
                    if($dato['status']=='OK'){
                        app.metEliminarRegistroJson('datatableNivelTipoCargo','El Nivel  de Cargo fue eliminado con Exito.','cerrarModal','ContenidoModal');
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

    });
</script>