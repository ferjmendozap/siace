
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro de Asignación de Beneficios Global (HCM)</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatableBeneficiosGlobal" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="170">Id</th>
                                    <th>Año</th>
                                    <th>Beneficio</th>
                                    <th>Limite</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <!--
                                <tbody>
                                {foreach item=b from=$BeneficioGlobal}

                                    <tr id="idBeneficioGlobal{$b.pk_num_ayuda_global}">
                                        <td>{$b.pk_num_ayuda_global}</td>
                                        <td>{$b.fec_anio}</td>
                                        <td>{$b.ind_descripcion}</td>
                                        <td>{$b.num_limite|number_format:2:",":"."}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-07-03-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idBeneficioGlobal="{$b.pk_num_ayuda_global}"
                                                        descipcion="El Usuario a Modificado un Beneficio Global" titulo="Modificar Benificio Global">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-07-04-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" title="Eliminar" idBeneficioGlobal="{$b.pk_num_ayuda_global}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado Beneficio Global" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Beneficio Global!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                -->
                                <tfoot>
                                    <th colspan="5">
                                        {if in_array('RH-01-04-07-02-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado una Nueva Asignación de Global de Beneficios" title="Nueva Asignación"  titulo="Nueva Asignación Global de Beneficios" id="nuevo" >
                                                    <i class="md md-create"></i> Nueva Asignación &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/beneficiosGlobalHcmCONTROL/BeneficioGlobalHcmMET';

        var app = new AppFunciones();
        var dt = app.dataTable(
                '#datatableBeneficiosGlobal',
                "{$_Parametros.url}modRH/maestros/beneficiosGlobalHcmCONTROL/JsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_ayuda_global" },
                    { "data": "fec_anio" },
                    { "data": "ind_descripcion" },
                    { "data": "num_limite" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );


        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatableBeneficiosGlobal tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idBeneficioGlobal: $(this).attr('idBeneficioGlobal')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatableBeneficiosGlobal tbody').on( 'click', '.eliminar', function () {

            var idBeneficioGlobal=$(this).attr('idBeneficioGlobal');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/beneficiosGlobalHcmCONTROL/EliminarBeneficioGlobalMET';
                $.post($url, { idBeneficioGlobal: idBeneficioGlobal},function($dato){
                    if($dato['status']=='OK'){
                        app.metEliminarRegistroJson('datatableBeneficiosGlobal','Operación Realizada Satisfactoriamente.','cerrarModal','ContenidoModal');
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>