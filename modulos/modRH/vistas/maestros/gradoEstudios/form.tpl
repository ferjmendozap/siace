<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idGradoEstudio}" name="idGradoEstudio"/>
        <input type="hidden" value="{if isset($idMaestro)}{$idMaestro}{/if}" name="form[int][idMaestro]"/>


        <div class="col-xs-6">

            <div class="form-group floating-label" id="ind_nombre_cargoError">

                <input type="text" class="form-control input-sm" maxlength="4" value="{if isset($formDB.cod_detalle)}{$formDB.cod_detalle}{/if}" name="form[txt][cod_detalle]" id="cod_detalle" required data-msg-required="Introduzca Codigo Detalle" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="cod_detalle">Codigo Detalle</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>

        <div class="col-xs-12">

            <div class="form-group floating-label" id="ind_nombre_cargoError">

                <input type="text" class="form-control input-sm" maxlength="100" value="{if isset($formDB.ind_nombre_detalle)}{$formDB.ind_nombre_detalle}{/if}" name="form[txt][ind_nombre_grado]" id="ind_nombre_grado" required data-msg-required="Introduzca la Descripcion del Grado de Estudio" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_nombre_grado">Descripción Grado de Estudio</label>
                <p class="help-block"><span class="text-xs">* El campo es Obligatorio</span></p>
            </div>

        </div>


        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/gradoEstudiosCONTROL/GradoEstudiosMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idGradoEstudio'+dato['idGradoEstudio'])).html('<td>'+dato['idGradoEstudio']+'</td>' +
                            '<td>'+dato['ind_nombre_grado']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-08-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idGradoEstudio="'+dato['idGradoEstudio']+'"' +
                            'descipcion="El Usuario a Modificado Grado de Estudio" titulo="Modificar Grado de Estudio">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-08-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idGradoEstudio="'+dato['idGradoEstudio']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Grado de Estudio" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Grado de Estudio!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Registro se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                }else if(dato['status']=='creacion'){



                    if(dato['idGradoEstudio']=='rep'){

                        swal("Error!", "Verifique la descripción, ya se encuentra registrada.", "error");

                    }else{

                        $(document.getElementById('datatable1')).append('<tr id="idGradoEstudio'+dato['idGradoEstudio']+'">' +
                                '<td>'+dato['idGradoEstudio']+'</td>' +
                                '<td>'+dato['ind_nombre_grado']+'</td>' +
                                '<td  align="center">' +
                                '{if in_array('RH-01-04-08-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" idGradoEstudio="'+dato['idGradoEstudio']+'"' +
                                'descipcion="El Usuario a Modificado Grado de Estudio" title="Editar" titulo="Modificar Grado de Estudio">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '{if in_array('RH-01-04-08-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idGradoEstudio="'+dato['idGradoEstudio']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado Grado de Estudio" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Grado de Estudio!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>' +
                                '</tr>');
                        swal("Registro Guardado!", "Registro guardado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                    }
                }
            },'json');


        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la modal
        $('#modalAncho').css( "width", "65%" );


    });






</script>