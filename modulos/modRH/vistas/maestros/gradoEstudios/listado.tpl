
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro Grados de Estudios</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <input type="hidden" value="{if isset($idMaestro.fk_a005_num_miscelaneo_maestro)}{$idMaestro.fk_a005_num_miscelaneo_maestro}{/if}" name="idMaestro" id="idMaestro"/>
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="150">Id</th>
                                    <th>Descripción Grado Estudio</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=f from=$GradoEstudios}

                                        <tr id="idGradoEstudio{$f.pk_num_miscelaneo_detalle}">
                                            <td>{$f.pk_num_miscelaneo_detalle}</td>
                                            <td>{$f.ind_nombre_detalle}</td>
                                            <td align="center" width="100">
                                                {if in_array('RH-01-04-08-07-M',$_Parametros.perfil)}
                                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                            data-keyboard="false" data-backdrop="static" title="Editar" idGradoEstudio="{$f.pk_num_miscelaneo_detalle}"
                                                            descipcion="El Usuario a Modificado Grado de Estudio" titulo="Modificar Grado de Estudio">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                                &nbsp;&nbsp;
                                                {if in_array('RH-01-04-08-08-E',$_Parametros.perfil)}
                                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idGradoEstudio="{$f.pk_num_miscelaneo_detalle}"  boton="si, Eliminar"
                                                            descipcion="El usuario a eliminado Grado de Estudio" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Grado de Estudio!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            </td>
                                        </tr>

                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="3">
                                        {if in_array('RH-01-04-08-06-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado Grados de Estudio"  titulo="Nuevo Grado de Estudios" id="nuevo" >
                                                <i class="md md-create"></i> Nuevo Grado de Estudio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/gradoEstudiosCONTROL/GradoEstudiosMET';

        $('#nuevo').click(function(){
            var maestro = $("#idMaestro").val();
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { maestro:maestro } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idGradoEstudio: $(this).attr('idGradoEstudio')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idGradoEstudio=$(this).attr('idGradoEstudio');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/gradoEstudiosCONTROL/EliminarGradoEstudiosMET';
                $.post($url, { idGradoEstudio: idGradoEstudio},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idGradoEstudio'+$dato['idGradoEstudio'])).html('');
                        swal("Eliminado!", "Grado de Estudio Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>