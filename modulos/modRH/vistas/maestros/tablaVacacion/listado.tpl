<br/>
<section class="style-default-bright">
    <h2 class="text-primary">&nbsp;Tabla de Vacaciones</h2>
    <hr class="ruler-xl">
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr  align="center">
                        <th><i class="glyphicon glyphicon-th"></i> N°</th>
                        <th><i class="md md-account-circle"></i> Nómina</th>
                        <th><i class="md md-alarm"></i> Años</th>
                        <th><i class="fa fa-certificate"></i> Días de Disfrute</th>
                        <th><i class="md md-done-all"></i> Días Adicionales</th>
                        <th><i class="md md-add-alarm"></i> Total Días a Disfrutar</th>
                        <th width="40">Ver</th>
                        <th width="40">Editar</th>
                        <th width="50">Eliminar</th>
                    </tr>
                    </thead>
                    <tbody>
                        {foreach item=tabla from=$tablaVacacion}
                        <tr id="pk_num_tabla_vacacion{$tabla.pk_num_tabla_vacacion}">
                            <td>{$tabla.pk_num_tabla_vacacion}</td>
                            <td>{$tabla.ind_nombre_nomina}</td>
                            <td>{$tabla.num_anios}</td>
                            <td>{$tabla.num_dias_disfrute}</td>
                            <td>{$tabla.num_dias_adicionales}</td>
                            <td>{$tabla.num_total_disfrutar}</td>
                            <td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" pk_num_tabla_vacacion="{$tabla.pk_num_tabla_vacacion}" data-backdrop="static" title="Ver Disfrute" titulo="Ver Disfrute" id="ver"><i class="glyphicon glyphicon-search"></i></button></td>
                            <td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" pk_num_tabla_vacacion="{$tabla.pk_num_tabla_vacacion}" data-backdrop="static" descipcion="El usuario ha modificado un disfrute" title="Modificar disfrute"  titulo="Modificar disfrute"><i class="fa fa-edit"></i></button></td>
                            <td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  boton="si, Eliminar" descipcion="El usuario ha eliminado un disfrute" pk_num_tabla_vacacion="{$tabla.pk_num_tabla_vacacion}" titulo="¿Estás Seguro?" mensaje="¿Desea eliminar el disfrute vacacional?" title="Eliminar almacén"><i class="md md-delete"></i></button></td>
                        </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="9">
                           <button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado un dia de disfrute vacacional" data-toggle="modal" data-target="#formModal" titulo="Registrar nuevo disfrute" id="nuevo"> <span class="glyphicon glyphicon-log-out"></span> Nuevo Disfrute</button>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/tablaVacacionCONTROL/NuevaTablaMET';
        $('#nuevo').click(function(){
            $('#modalAncho').css( "width", "45%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        var $url_modificar='{$_Parametros.url}modRH/maestros/tablaVacacionCONTROL/EditarTablaMET';
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#modalAncho').css( "width", "45%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url_modificar,{ pk_num_tabla_vacacion: $(this).attr('pk_num_tabla_vacacion')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var pk_num_tabla_vacacion=$(this).attr('pk_num_tabla_vacacion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/tablaVacacionCONTROL/EliminarTablaMET';
                $.post($url, { pk_num_tabla_vacacion: pk_num_tabla_vacacion},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('pk_num_tabla_vacacion'+$dato['pk_num_tabla_vacacion'])).html('');
                        swal("Eliminado!", "El disfrute de vacaciones fue eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });

    var $urlVer='{$_Parametros.url}modRH/maestros/tablaVacacionCONTROL/VerTablaMET';
    $('#datatable1 tbody').on( 'click', '.ver', function () {
        $('#modalAncho').css( "width", "45%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        $.post($urlVer,{ pk_num_tabla_vacacion: $(this).attr('pk_num_tabla_vacacion')},function($dato){
            $('#ContenidoModal').html($dato);
        });
    });
</script>
