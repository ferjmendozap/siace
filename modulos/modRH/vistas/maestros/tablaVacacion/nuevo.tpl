<form id="formAjax" action="{$_Parametros.url}modRH/maestros/tablaVacacionCONTROL/NuevaTablaMET" class="form floating-label" novalidate="novalidate">
<input type="hidden" name="valido" value="1" />
<div class="modal-body">
	<div class="form-group floating-label">
		<select name="tipo_nomina" id="s2id_single" class="select2-container form-control select2">
			<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
			<option value="">&nbsp;</option>
			{foreach item=nomina from=$tipoNomina}
			<option value="{$nomina.pk_num_tipo_nomina}">{$nomina.ind_nombre_nomina}</option>
			{/foreach}
		</select>
		<label for="tipo_nomina"><i class="md md-account-circle"></i>Tipo de Nómina</label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="anios" id="anios">
		<label for="anios"><i class="md md-create"></i> Años </label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="dias_disfrute" id="dias_disfrute" onchange="calcularTiempo()">
		<label for="dias_disfrute"><i class="fa fa-certificate"></i> Dias de Disfrute </label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="dias_adicionales" id="dias_adicionales" onchange="calcularTiempo()">
		<label for="dias_adicionales"><i class="md md-done-all"></i> Dias de Adicionales </label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control dirty" disabled  name="total_disfrute" id="total_disfrute">
		<label for="total_disfrute"><i class="md md-add-alarm"></i> Total Disfrute </label>
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				tipo_nomina:{
					required:true
				},
				anios:{
					required:true
				},
				dias_disfrute:{
					required:true
				},
				dias_adicionales:{
					required:true
				}
			},
			messages:{
				tipo_nomina:{
					required: "Este campo es requerido"
				},
				anios:{
					required: "Este campo es requerido"
				},
				dias_disfrute:{
					required: "Este campo es requerido"
				},
				dias_adicionales:{
					required: "Este campo es requerido"
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					$(document.getElementById('datatable1')).append('<tr id="pk_num_tabla_vacacion'+dato['pk_num_tabla_vacacion']+'"><td>'+dato['pk_num_tabla_vacacion']+'</td><td>'+dato['tipoNomina']+'</td><td>'+dato['anios']+'</td><td>'+dato['diasDisfrute']+'</td><td>'+dato['diasAdicionales']+'</td><td>'+dato['totalDisfrute']+'</td>' +
							'<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado un disfrute" titulo="Visualizar disfrute de vacaciones" title="Visualizar disfrute de vacaciones" data-toggle="modal" data-target="#formModal" pk_num_tabla_vacacion="'+dato['pk_num_tabla_vacacion']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>' +
							'<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha modificado el disfrute de vacaciones" titulo="Modificar disfrute de vacaciones" title="Modificar disfrute de vacaciones" data-toggle="modal" data-target="#formModal" pk_num_tabla_vacacion="'+dato['pk_num_tabla_vacacion']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>' +
							'<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un almacén" titulo="¿Estás Seguro?" title="Eliminar disfrute de vacaciones" mensaje="¿Estás seguro de eliminar el disfrute de vacaciones?" boton="si, Eliminar" pk_num_tabla_vacacion="'+dato['pk_num_tabla_vacacion']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td></tr>');
					swal("Disfrute de Vacaciones Guardado", "Disfrute guardado satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
	var placeholder = "";

	$( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
	$( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult( repo ) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if ( repo.description ) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}

	function repoFormatSelection( repo ) {
		return repo.full_name;
	}

	$( "button[data-select2-open]" ).click( function() {
		$( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
	});

	function calcularTiempo() {
		var urlComprueba = "{$_Parametros.url}modRH/maestros/tablaVacacionCONTROL/CalcularTiempoMET";
		var dias_disfrute = $("#dias_disfrute").val();
		var dias_adicionales = $("#dias_adicionales").val();
		$.post(urlComprueba,{ dias_disfrute:""+dias_disfrute, dias_adicionales:""+dias_adicionales},function(dato){
			$("#total_disfrute").val(dato['total']);
		},'json');
	}
</script>
