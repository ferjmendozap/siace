<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idPlantilla}" name="idPlantilla"/>



        <!-- ***************************ID ASIGNACION*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_plantillas)}{$formDB.pk_num_plantillas}{/if}" name="form[int][pk_num_plantillas]" id="pk_num_plantillas" readonly>
                <label for="pk_num_plantillas">Id</label>
            </div>
        </div>

        <!-- ***************************DESCRIPCION*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_descripcion_plantilla)}{$formDB.ind_descripcion_plantilla}{/if}" name="form[txt][ind_descripcion_plantilla]" id="ind_descripcion_plantilla" maxlength="200" required data-msg-required="Introduzca Descripción de la Plantilla" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_descripcion_plantilla">Descripción Plantilla</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>

        <!-- ****************************PREGUNTAS**********************--------->
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">Agregar Preguntas a la Plantilla</div>
            </div>
            <div class="row">
                <div class="col-xs-5 col-sm-offset-1"><i class="fa fa-square-o"></i> POR SELECCIONAR</div>
                <div class="col-sm-4 col-sm-offset-2"><i class="fa fa-check-square-o"></i> SELECCIONADAS</div>
            </div>

            <div class="row">
                <select id="optgroup" multiple="multiple" name="form[int][preguntas][]">
                    {foreach item=preguntas from=$listaPreguntas}
                        <optgroup label="{$preguntas.ind_nombre_detalle}">
                                {if in_array($preguntas.pk_num_preguntas,$formDBPreguntas)}
                                    <option selected="selected" value="{$preguntas.pk_num_preguntas}">{$preguntas.ind_descripcion_preguntas}</option>
                                {else}
                                    <option value="{$preguntas.pk_num_preguntas}">{$preguntas.ind_descripcion_preguntas}</option>
                                {/if}
                        </optgroup>
                    {/foreach}
                </select>
            </div>

        </div>


        <!-- ***************************ESTATUS*********************--------->
        <div class="col-xs-4">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                    <span>Estatus</span>
                </label>
            </div>
        </div>

        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            var datos = $( "#formAjax" ).serialize();
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });

            $.post('{$_Parametros.url}modRH/maestros/plantillasCONTROL/PlantillasMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idPlantilla'+dato['idPlantilla'])).html('<td>'+dato['idPlantilla']+'</td>' +
                            '<td>'+dato['ind_descripcion_plantilla']+'</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-04-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar" idPlantilla="'+dato['idPlantilla']+'"' +
                            'descipcion="El Usuario a Modificado una Plantilla" titulo="Modificar Plantilla">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-04-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPlantilla="'+dato['idPlantilla']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado una Plantilla" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Plantilla!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "El Registro se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                }else if(dato['status']=='creacion'){


                    if(dato['idPlantilla']=='rep'){

                        $("#ind_descripcion_plantilla").focus();
                        swal("Error!", "Verifique la descripción, ya se encuentra registrada.", "error");

                    }else{
                        $(document.getElementById('datatable1')).append('<tr id="idPlantilla'+dato['idPlantilla']+'">' +
                                '<td>'+dato['idPlantilla']+'</td>' +
                                '<td>'+dato['ind_descripcion_plantilla']+'</td>' +
                                '<td  align="center">' +
                                '{if in_array('RH-01-04-04-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" title="Editar"  idPlantilla="'+dato['idPlantilla']+'"' +
                                'descipcion="El Usuario a Modificado una Plantilla" titulo="Modificar Plantilla">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '{if in_array('RH-01-04-04-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPlantilla="'+dato['idPlantilla']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado una Plantilla" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Plantilla!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>' +
                                '</tr>');
                        swal("Registro Guardado!", "Registro guardado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }

                }
            },'json');
        }
    });




    $(document).ready(function() {
        //valido formulario
        $("#formAjax").validate();
        //ancho de la modal
        $('#modalAncho').css( "width", "65%" );

        $('#optgroup').multiSelect({ selectableOptgroup: true });

    });






</script>