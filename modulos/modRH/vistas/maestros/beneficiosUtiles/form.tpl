<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idBeneficiosUtiles}" name="idBeneficiosUtiles"/>


        <!-- ***************************ID BENEFICIO*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.pk_num_beneficio_utiles)}{$formDB.pk_num_beneficio_utiles}{/if}" name="form[int][pk_num_beneficio_utiles]" id="pk_num_beneficio_utiles" disabled>
                <label for="pk_num_beneficio_utiles">Beneficio Utiles</label>
                <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
            </div>
        </div>

        <!-- ***************************DESCRIPCION*********************--------->
        <div class="col-xs-12">
            <div class="form-group floating-label">
                <input type="text" class="form-control input-sm" value="{if isset($formDB.ind_descripcion_beneficio)}{$formDB.ind_descripcion_beneficio}{/if}" name="form[txt][ind_descripcion_beneficio]" id="ind_descripcion_beneficio" maxlength="150" required data-msg-required="Introduzca Descripción Beneficio Útiles" onkeyup="jQuery(this).val(jQuery(this).val().toUpperCase())">
                <label for="ind_descripcion_beneficio">Descripción Beneficio</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>


        <!-- ***************************PERIODO*********************--------->
        {$foo=$desde}
        <div class="col-xs-3">
            <div class="form-group">
                <select id="ind_periodo_ini" name="form[txt][ind_periodo_ini]" class="form-control" required>
                    <option value="">....</option>
                    {while $foo>1999}
                        {if isset($formDB.ind_periodo_ini)}
                            {if $foo==$formDB.ind_periodo_ini}
                                <option value="{$foo}" selected>{$foo}</option>
                            {else}
                                <option value="{$foo}">{$foo}</option>
                            {/if}
                        {else}
                            <option value="{$foo}">{$foo}</option>
                        {/if}
                        {$foo--}
                    {/while}
                </select>
                <label for="ind_periodo_ini"><i class="glyphicon glyphicon-briefcase"></i> Periodo Inicio</label>
            </div>
        </div>
        {$foo1=$desde}
        <div class="col-xs-3">
            <div class="form-group">
                <select id="ind_periodo_fin" name="form[txt][ind_periodo_fin]" class="form-control" required>
                    <option value="">....</option>
                    {while $foo1>1999}
                        {if isset($formDB.ind_periodo_fin)}
                            {if $foo1==$formDB.ind_periodo_fin}
                                <option value="{$foo1}" selected>{$foo1}</option>
                            {else}
                                <option value="{$foo1}">{$foo1}</option>
                            {/if}
                        {else}
                            <option value="{$foo1}">{$foo1}</option>
                        {/if}
                        {$foo1--}
                    {/while}
                </select>
                <label for="ind_periodo_fin"><i class="glyphicon glyphicon-briefcase"></i> Periodo Fin</label>
            </div>
        </div>

        <!-- ***************************MONTO*********************--------->
        <div class="col-xs-6">
            <div class="form-group">
                <input type="text" class="form-control input-sm bolivar-mask" value="{if isset($formDB.num_monto_asignado)}{$formDB.num_monto_asignado|number_format:2:",":"."}{/if}" name="form[int][num_monto_asignado]" id="num_monto_asignado" maxlength="100" required data-msg-required="Introduzca Monto de Beneficio" >
                <label for="num_monto_asignado">Monto Beneficio</label>
                <p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
            </div>
        </div>



        <div class="row">
            &nbsp;
        </div>
        <div class="row">
            &nbsp;
        </div>
        <span class="clearfix"></span>

    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
    </div>

</form>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();
            var app   = new  AppFunciones();

            var p_ini = $("#ind_periodo_ini").val();
            var p_fin = $("#ind_periodo_fin").val();

            if(p_fin <= p_ini){
                swal("Error!", "El Periodo Final no puede menor que el Periodo Inicial", "error");
                return false;
            }

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });


            $.post('{$_Parametros.url}modRH/maestros/beneficiosUtilesCONTROL/BeneficiosUtilesMET', datos ,function(dato){


                if(dato['status']=='modificacion'){
                    $(document.getElementById('idBeneficiosUtiles'+dato['idBeneficiosUtiles'])).html('<td>'+dato['idBeneficiosUtiles']+'</td>' +
                            '<td>' + dato['ind_descripcion_beneficio'] + '</td>' +
                            '<td>' + dato['ind_periodo_ini'] + '-' + dato['ind_periodo_fin'] + '</td>' +
                            '<td>' + dato['num_monto_asignado'] + '</td>' +
                            '<td  align="center">' +
                            '{if in_array('RH-01-04-08-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" title="Editar"  idBeneficiosUtiles="' + dato['idBeneficiosUtiles'] + '"' +
                            'descipcion="El Usuario a Modificado Beneficio Utiles" titulo="Modificar Beneficio Útiles">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '{if in_array('RH-01-04-08-04-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficiosUtiles="' + dato['idBeneficiosUtiles'] + '"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado Beneficio Útiles" title="Eliminar"  titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Beneficio de Útiles!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>');
                    swal("Registro Modificado!", "EL Beneficio de Útiles se modifico satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');



                }else if(dato['status']=='creacion'){

                    if(dato['idBeneficiosUtiles']=='repdes'){

                        $("#ind_descripcion_beneficio").focus();
                        swal("Error!", "Verifique la descripción, ya existe un registro con la descripción.", "error");


                    }else if(dato['idBeneficiosUtiles']=='repper'){

                        $("#ind_periodo").focus();
                        swal("Error!", "Verifique el Periodo, ya existe un registro con el perido introducido.", "error");


                    }else {

                        $(document.getElementById('datatable1')).append('<tr id="idBeneficiosUtiles' + dato['idBeneficiosUtiles'] + '">' +
                                '<td>' + dato['idBeneficiosUtiles'] + '</td>' +
                                '<td>' + dato['ind_descripcion_beneficio'] + '</td>' +
                                '<td>' + dato['ind_periodo_ini'] + '-' + dato['ind_periodo_fin'] + '</td>' +
                                '<td>' + dato['num_monto_asignado'] + '</td>' +
                                '<td  align="center">' +
                                '{if in_array('RH-01-04-08-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" title="Editar"  idBeneficiosUtiles="' + dato['idBeneficiosUtiles'] + '"' +
                                'descipcion="El Usuario a Modificado Beneficio Utiles" titulo="Modificar Beneficio Útiles">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                                '{if in_array('RH-01-04-08-04-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficiosUtiles="' + dato['idBeneficiosUtiles'] + '"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado Beneficio Útiles" title="Eliminar"  titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar Beneficio de Útiles!!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>' +
                                '</tr>');
                        swal("Registro Guardado!", "EL Beneficio de Útiles fue guardado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');



                    }

                }


            },'json');


        }
    });




    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();



        //ancho de la modal
        $('#modalAncho').css( "width", "70%" );


    });






</script>