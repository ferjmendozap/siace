
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Maestro Beneficios Útiles</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="130">Beneficio</th>
                                    <th>Descripción</th>
                                    <th>Periodo</th>
                                    <th>Monto</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=b from=$BenefeciosUtiles}

                                    <tr id="idBeneficiosUtiles{$b.pk_num_beneficio_utiles}">
                                        <td>{$b.pk_num_beneficio_utiles}</td>
                                        <td>{$b.ind_descripcion_beneficio}</td>
                                        <td>{$b.ind_periodo_ini}-{$b.ind_periodo_fin}</td>
                                        <td>{$b.num_monto_asignado|number_format:2:",":"."}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-04-08-03-M',$_Parametros.perfil)}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idBeneficiosUtiles="{$b.pk_num_beneficio_utiles}"
                                                        descipcion="El Usuario a Modificado un Beneficio Utiles" titulo="Modificar Beneficios Utiles">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            &nbsp;&nbsp;
                                            {if in_array('RH-01-04-08-04-E',$_Parametros.perfil)}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" title="Eliminar" idBeneficiosUtiles="{$b.pk_num_beneficio_utiles}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Beneficio Utiles" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Beneficio Utiles!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="5">
                                        {if in_array('RH-01-04-08-02-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a creado un Beneficio Útiles"  title="Nuevo Beneficio Utiles" titulo="Nuevo Beneficio Útiles" id="nuevo" >
                                                    <i class="md md-create"></i> Nuevo Beneficio Útiles &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            </button>
                                        {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/maestros/beneficiosUtilesCONTROL/BeneficiosUtilesMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idBeneficiosUtiles: $(this).attr('idBeneficiosUtiles')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idBeneficiosUtiles=$(this).attr('idBeneficiosUtiles');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/maestros/beneficiosUtilesCONTROL/EliminarBeneficiosUtilesMET';
                $.post($url, { idBeneficiosUtiles: idBeneficiosUtiles},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idBeneficiosUtiles'+$dato['idBeneficiosUtiles'])).html('');
                        swal("Eliminado!", "Beneficio Utiles Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>