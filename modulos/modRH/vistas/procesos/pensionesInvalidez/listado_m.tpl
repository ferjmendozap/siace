<br/>
<section class="style-default-bright">
    <h2 class="text-primary">&nbsp;Lista de Jubilaciones</h2>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div align="right">
                    <div class="btn-group"><a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-danger" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a></div>&nbsp;
                    <button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado una solicitud de jubilación" data-toggle="modal" data-target="#formModal" titulo="Registrar nueva jubilación" id="nuevo"> <span class="glyphicon glyphicon-log-out"></span> Nueva Jubilación</button>
                    <hr class="ruler-xl">
                </div>
                <br/>
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th><i class="md-person"></i> Empleado</th>
                        <th><i class="md-person"></i> Nombre Completo</th>
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Nro. Documento</th>
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Nro. Proceso</th>
                        <th><i class="glyphicon glyphicon-cog"></i> Aprobado Por</th>
                        <th width="40">Ver</th>
                        <th width="40">Editar</th>
                        <th width="50">Reporte</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- filtro -->
<div class="offcanvas">
    <form id="formulario" class="form floating-label" role="form" method="post" novalidate>
        <div id="offcanvas-filtro" class="offcanvas-pane width-12">
            <div class="offcanvas-head">
                <header>Filtro de Búsqueda</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                </div>
            </div>
            <div class="offcanvas-body">
                    <div class="form-group floating-label">
                        <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                            {foreach item=org from=$listadoOrganismo}
                                {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                    <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                                {else}
                                    <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                    </div>
                    <div id="dependencia">
                        <div class="form-group floating-label">
                            <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty" onchange="cargarEmpleado(this.value)">
                                <option value="">&nbsp;</option>
                                {foreach item=dep from=$listadoDependencia}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/foreach}
                            </select>
                            <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                        </div>
                    </div>
                    <div id="empleado">
                        <div class="form-group floating-label">
                            <select id="pk_num_empleado" name="pk_num_empleado" id="pk_num_empleado" class="form-control">
                                <option value="">&nbsp;</option>
                            </select>
                            <label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
                        </div>
                    </div>
                <div class="form-group">
                    <input type="text" name="pk_num_solicitud_vacacion" id="pk_num_solicitud_vacacion" class="form-control">
                    <label class="control-label"><i class="glyphicon glyphicon-th"></i> N° de Documento</label>
                </div>
                <div class="form-group floating-label">
                    <select id="txt_estatus" name="txt_estatus" class="form-control">
                        <option value=""></option>
                        <option value="Preparado">Preparado</option>
                        <option value="Verificado">Conformado</option>
                        <option value="Aprobado">Aprobado</option>
                        <option value="Anulado">Anulado</option>
                    </select>
                    <label for="txt_estatus"><i class="glyphicon glyphicon-user"></i> Estado</label>
                </div>
                <br/>
                <div class="col-sm-12 col-sm-12" align="center">
                    <div class="col-sm-6 col-sm-6">
                        <button type="button" id="buscarReporte" class="btn ink-reaction btn-flat btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Generando...">Generar Reporte<div style="top: 27px; left: 65px;" class="ink inverse"></div></button>
                    </div>
                    <div class="col-sm-6 col-sm-6">
                        <button type="submit" id="formu" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk"></span> Buscar</button>
                    </div>
                </div>
            </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/NuevaJubilacionMET';
        $('#nuevo').click(function(){
            $('#modalAncho').css( "width", "90%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#modalAncho').css( "width", "45%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url_modificar,{ pk_num_almacen: $(this).attr('pk_num_almacen')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var pk_num_almacen=$(this).attr('pk_num_almacen');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modAD/almacenCONTROL/EliminarAlmacenMET';
                $.post($url, { pk_num_almacen: pk_num_almacen},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('pk_num_almacen'+$dato['pk_num_almacen'])).html('');
                        swal("Eliminado!", "El almacén fue eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });

    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/procesos/jubilacionCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function cargarEmpleado(pk_num_dependencia) {
        $("#empleado").html("");
        $.post("{$_Parametros.url}modRH/procesos/jubilacionCONTROL/BuscarEmpleadoMET",{ pk_num_dependencia:""+pk_num_dependencia }, function (dato) {
            $("#empleado").html(dato);
        });
    }
</script>