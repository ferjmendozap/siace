<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
	<form id="formAjax" action="{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/RegistrarMET" class="form form-validation" role="form" novalidate="novalidate">
		<input type="hidden" name="valido" value="1" />
		<input type="hidden" name="pkNumEmpleado" id="pkNumEmpleado"/>
		<input type="hidden" value="{$idPensionInvalidez}" name="idPensionInvalidez"/>
		<input type="hidden" name="proceso"  id="proceso" value="{$proceso}">
		<div class="form-wizard-nav">
			<div class="progress"><div class="progress-bar progress-bar-primary"></div>
			</div>
			<ul class="nav nav-justified">
				<li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">	Información General</span></a></li>
				<li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Detalle Pensión Invalidez</span></a></li>
				<li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Detalle Registro</span></a></li>

			</ul>
		</div><!--end .form-wizard-nav -->
		<!-- *********** PASO 1 ******************-->
		<div class="tab-content clearfix">
			<br/>
			<div class="tab-pane active" id="step1">
						<div class="col-md-6 col-sm-6">
							<div class="well clearfix">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-content">
											<input type="text" class="form-control input-sm dirty" id="nombreTrabajador" value="{if isset($formDB.nombre_completo)}{$formDB.nombre_completo}{/if}" disabled>
											<input type="hidden" name="form[txt][fk_rhb001_num_empleado]" id="fk_rhb001_num_empleado" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{/if}"  disabled>
											<label for="nombreTrabajador">Empleado</label>
										</div>
										<div class="input-group-btn">
											<button class="btn btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado">Buscar</button>
										</div>
									</div>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][ind_organismo]" id="ind_organismo" class="form-control input-sm dirty" value="{if isset($formDB.ind_organismo)}{$formDB.ind_organismo}{/if}"  disabled>
									<input type="hidden" name="form[int][fk_a001_num_organismo]" id="fk_a001_num_organismo" value="{if isset($formDB.fk_a001_num_organismo)}{$formDB.fk_a001_num_organismo}{/if}" disabled>
									<label for="ind_organismo"> Organismo</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][ind_dependencia]" id="ind_dependencia" class="form-control input-sm dirty" value="{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}" disabled>
									<input type="hidden" name="form[int][fk_a004_num_dependencia]" id="fk_a004_num_dependencia" value="{if isset($formDB.fk_a004_num_dependencia)}{$formDB.fk_a004_num_dependencia}{/if}" disabled>
									<label for="ind_dependencia"> Dependencia</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][ind_cargo]" id="ind_cargo" class="form-control input-sm dirty" value="{if isset($formDB.ind_cargo)}{$formDB.ind_cargo}{/if}" disabled>
									<input type="hidden" name="form[int][fk_rhc063_num_puestos]" id="fk_rhc063_num_puestos" value="{if isset($formDB.fk_rhc063_num_puestos)}{$formDB.fk_rhc063_num_puestos}{/if}" disabled>
									<label for="ind_cargo"> Cargo</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[int][num_ultimo_sueldo]" id="num_ultimo_sueldo" class="form-control input-sm dirty" value="{if isset($formDB.num_ultimo_sueldo)}{$formDB.num_ultimo_sueldo|number_format:2:",":"."}{/if}" disabled>
									<label class="control-label">Sueldo Actual</label>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="well clearfix">
								<div class="form-group">
									<input type="text" name="cedula" id="cedula" class="form-control input-sm dirty" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" disabled>
									<label class="control-label">Nro de documento</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][ind_sexo]" id="ind_sexo" class="form-control input-sm dirty" value="{if isset($formDB.sexo)}{$formDB.sexo}{/if}" disabled>
									<input type="hidden" name="form[int][sexo]" id="sexo" class="form-control input-sm dirty" value="{if isset($formDB.fk_a006_num_miscelaneo_detalle_sexo)}{$formDB.fk_a006_num_miscelaneo_detalle_sexo}{/if}" disabled>
									<label class="control-label">Sexo</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][nacimiento]" id="nacimiento" class="form-control input-sm dirty" value="{if isset($formDB.fec_nacimiento)}{$formDB.fec_nacimiento|date_format:"d-m-Y"}{/if}" disabled>
									<label class="control-label">Fecha de Nacimiento</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[int][num_edad]" id="num_edad" class="form-control input-sm dirty" value="{if isset($edad)}{$edad}{/if}" disabled>
									<label class="control-label">Edad</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][fec_fecha_ingreso]" id="fec_fecha_ingreso" class="form-control input-sm dirty" value="{if isset($fec_ingreso)}{$fec_ingreso|date_format:"d-m-Y"}{/if}" disabled>
									<label class="control-label">Fecha de Ingreso</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control input-sm" id="num_anio_servicio" name="form[int][num_anio_servicio]" value="{if isset($anio_servicio)}{$anio_servicio}{/if}" disabled>
									<label for="num_anio_servicio">Años de Servicio</label>
								</div>
							</div>
						</div>
					<div class="col-md-12 col-sm-12" id="requisitoVer">
						<!-- Aqui se carga el mensaje informativo si el empleado cumple o no los requisitos para optar a la jubilación-->

					</div>
			</div><!--end #step1 -->
			<!-- *********** PASO 2 ******************-->
			<div class="tab-pane" id="step2">

				<h4 class="text-primary text-center text-lg">Datos de la Pensión</h4>
				<hr class="ruler-lg">

				<div class="row contain-lg">
					<div class="col-md-6">
						<div class="form-group">
							<select id="fk_a006_num_miscelaneo_detalle_motivopension" name="form[int][fk_a006_num_miscelaneo_detalle_motivopension]" class="form-control input-sm" disabled>
								<option value="">Seleccione...</option>
								{foreach item=dat from=$MotivoPension}
									{if isset($formDB.fk_a006_num_miscelaneo_detalle_motivopension)}
										{if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_motivopension}
											<option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
										{else}
											<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
										{/if}
									{else}
										<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
									{/if}
								{/foreach}
							</select>
							<label for="fk_a006_num_miscelaneo_detalle_motivopension">Motivo</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" class="form-control input-sm bolivar-mask" value="{if isset($formDB.num_monto_pension)}{$formDB.num_monto_pension|number_format:2:",":"."}{/if}" name="form[int][num_monto_pension]" id="num_monto_pension" required data-msg-required="Introduzca Monto de Pensión" >
							<label for="num_monto_pension">Sueldo Pension</label>
							<p class="help-block"><span class="text-xs" style="color: red">* Campo Obligatorio</span></p>
						</div>
					</div>
					<input type="hidden" name="form[int][fk_a006_num_miscelaneo_detalle_tipopension]" id="fk_a006_num_miscelaneo_detalle_tipopension" value="{if isset($formDB.fk_a006_num_miscelaneo_detalle_tipopension)}{$formDB.fk_a006_num_miscelaneo_detalle_tipopension}{/if}" >
					<input type="hidden" name="form[txt][ind_detalle_tipopension]" id="ind_detalle_tipopension" value="{if isset($formDB.ind_detalle_tipopension)}{$formDB.ind_detalle_tipopension}{/if}" >
					<input type="hidden" name="form[txt][ind_detalle_motivopension]" id="ind_detalle_motivopension" value="{if isset($formDB.ind_detalle_motivopension)}{$formDB.ind_detalle_motivopension}{/if}" >
				</div>

				<div class="row contain-lg">
					<div class="col-md-6">
						<div class="form-group">
							<select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control input-sm" required>
								<option value="">Seleccione...</option>
								{foreach item=dat from=$listadoTipoNomina}
									{if isset($formDB.fk_nmb001_num_tipo_nomina)}
										{if $dat.pk_num_tipo_nomina==$formDB.fk_nmb001_num_tipo_nomina}
											<option selected value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
										{else}
											<option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
										{/if}
									{else}
										<option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
									{/if}
								{/foreach}
							</select>
							<label for="fk_nmb001_num_tipo_nomina">Tipo Nomina</label>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<select id="fk_a006_num_miscelaneo_detalle_tipotrab" name="form[int][fk_a006_num_miscelaneo_detalle_tipotrab]" class="form-control input-sm" required>
								<option value="">Seleccione...</option>
								{foreach item=dat from=$listadoTipoTrabajador}
									{if isset($formDB.fk_a006_num_miscelaneo_detalle_tipotrab)}
										{if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipotrab}
											<option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
										{else}
											<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
										{/if}
									{else}
										<option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
									{/if}
								{/foreach}
							</select>
							<label for="fk_a006_num_miscelaneo_detalle_tipotrab">Tipo Trabajador</label>
						</div>
					</div>
				</div>
				<div class="row contain-lg">
					<div class="col-md-6">
						<div class="form-group">
							<input type="text" class="form-control input-sm" id="ind_resolucion" name="form[txt][ind_resolucion]" value="{if isset($formDB.ind_resolucion)}{$formDB.ind_resolucion}{/if}" style="text-transform: uppercase" required>
							<label for="ind_resolucion">Nro. Resolución</label>
						</div>
					</div>
				</div>

				<h4 class="text-primary text-center text-lg">Cese</h4>
				<hr class="ruler-lg">

				<div class="row contain-lg">
					<div class="col-md-4">
						<div class="form-group">
							<select id="num_situacion_trabajo" name="form[int][num_situacion_trabajo]" class="form-control input-sm" disabled required>
								{foreach key=key item=item from=$EstadoRegistro}
									{if isset($formDB.num_situacion_trabajo)}
										{if $key==$formDB.num_situacion_trabajo}
											<option selected value="{$key}">{$item}</option>
										{else}
											<option value="{$key}">{$item}</option>
										{/if}
									{else}
										<option value="{$key}">{$item}</option>
									{/if}
								{/foreach}
							</select>
							<label for="num_situacion_trabajo">Situacion de Trabajo</label>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="input-group date" id="fecha_egreso">
								<div class="input-group-content">
									<input type="text" class="form-control input-sm" id="fec_fecha_egreso" name="form[txt][fec_fecha_egreso]" value="{if isset($formDB.fec_fecha_egreso)}{$formDB.fec_fecha_egreso|date_format:"d-m-Y"}{/if}" required>
									<label>Fecha Egreso</label>
								</div>
								<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<select id="fk_rhc032_num_motivo_cese" name="form[int][fk_rhc032_num_motivo_cese]" class="form-control input-sm" required>
								<option value="">Seleccione...</option>
								{foreach item=dat from=$listadoMotivoCese}
									{if isset($formDB.fk_rhc032_num_motivo_cese)}
										{if $dat.pk_num_motivo_cese==$formDB.fk_rhc032_num_motivo_cese}
											<option selected value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
										{else}
											<option value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
										{/if}
									{else}
										<option value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
									{/if}
								{/foreach}
							</select>
							<label for="fk_rhc032_num_motivo_cese">Motivo Cese</label>
						</div>
					</div>
				</div>
				<div class="row contain-lg">
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_egreso" name="form[txt][txt_observacion_egreso]" value="{if isset($formDB.txt_observacion_egreso)}{$formDB.txt_observacion_egreso}{/if}" style="text-transform: uppercase" required>
							<label for="txt_observacion_egreso">Explicación</label>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12" id="requisitoVer2">
					<!-- Aqui se carga el mensaje informativo si el empleado cumple o no los requisitos para optar a la pension-->

				</div>


			</div><!--end #step2 -->
			<!-- *********** PASO 3 ******************-->
			<div class="tab-pane" id="step3">


				{if isset($formDB.txt_estatus)}

					{if $formDB.txt_estatus=='PR' || $formDB.txt_estatus=='CO' || $formDB.txt_estatus=='AP'}

						<!-- PREPARADO -->
						<div class="row contain-lg">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control input-sm"  id="preparado_por" name="form[txt][preparado_por]" value="{if isset($formDBoperacionesPR[0].nombre_registro)}{$formDBoperacionesPR[0].nombre_registro}{/if}" disabled>
									<input type="hidden" id="usuario_preparado_por">
									<label for="preparado_por">Preparado Por</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control input-sm" id="fecha_preparado" name="form[txt][fecha_preparado]" value="{if isset($formDBoperacionesPR[0].fec_operacion)}{$formDBoperacionesPR[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
									<label for="fecha_preparado">Fecha</label>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<input type="text" class="form-control input-sm" maxlength="7" id="ind_periodo" name="form[txt][ind_periodo]" value="{if isset($formDB.ind_periodo)}{$formDB.ind_periodo}{else}{$periodo}{/if}" disabled>
									<label for="ind_periodo">Periodo</label>
								</div>
							</div>
						</div>
						<div class="row contain-lg">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
									<label for="txt_observacion_pre">Observaciones</label>
								</div>
							</div>
						</div>
						<!-- CONFORMADO -->
						<div class="row contain-lg">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control input-sm"  id="conformado_por" name="form[txt][conformado_por]" value="{if isset($formDBoperacionesCO[0].nombre_registro)}{$formDBoperacionesCO[0].nombre_registro}{/if}" disabled>
									<input type="hidden" id="usuario_conformado_por">
									<label for="conformado_por">Conformado Por</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control input-sm" id="fecha_conformado" name="form[txt][fecha_conformado]" value="{if isset($formDBoperacionesCO[0].fec_operacion)}{$formDBoperacionesCO[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
									<label for="fecha_conformado">Fecha</label>
								</div>
							</div>
						</div>
						<div class="row contain-lg">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_con" name="form[txt][txt_observacion_con]" value="{if isset($formDBoperacionesCO[0].txt_observaciones)}{$formDBoperacionesCO[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
									<label for="txt_observacion_con">Observaciones</label>
								</div>
							</div>
						</div>
						<!-- APROBADO -->
						<div class="row contain-lg">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control input-sm"  id="aprobado_por" name="form[txt][aprobado_por]" value="{if isset($formDBoperacionesAP[0].nombre_registro)}{$formDBoperacionesAP[0].nombre_registro}{/if}" disabled>
									<input type="hidden" id="usuario_aprobado_por">
									<label for="aprobado_por">Aprobado Por</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control input-sm" id="fecha_aprobado" name="form[txt][fecha_aprobado]" value="{if isset($formDBoperacionesAP[0].fec_operacion)}{$formDBoperacionesAP[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
									<label for="fecha_aprobado">Fecha</label>
								</div>
							</div>
						</div>
						<div class="row contain-lg">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apro" name="form[txt][txt_observacion_apro]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
									<label for="txt_observacion_apro">Observaciones</label>
								</div>
							</div>
						</div>

					{else}

						<!-- ANULADO -->
						<div class="row contain-lg">
							<div class="col-md-6">
								<div class="form-group">
									<input type="text" class="form-control input-sm"  id="anulado_por" name="form[txt][anulado_por]" value="{if isset($formDBoperacionesAN[0].nombre_registro)}{$formDBoperacionesAN[0].nombre_registro}{/if}" disabled>
									<input type="hidden" id="usuario_anulado_por">
									<label for="anulado_por">Anulado Por</label>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<input type="text" class="form-control input-sm" id="fecha_anulado" name="form[txt][fecha_anulado]" value="{if isset($formDBoperacionesAN[0].fec_operacion)}{$formDBoperacionesAN[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
									<label for="fecha_anulado">Fecha</label>
								</div>
							</div>
						</div>
						<div class="row contain-lg">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_anulado" name="form[txt][txt_observacion_anulado]" value="{if isset($formDBoperacionesAN[0].txt_observaciones)}{$formDBoperacionesAN[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
									<label for="txt_observacion_anulado">Observaciones</label>
								</div>
							</div>
						</div>

					{/if}

				{else}
					<!-- PREPARADO -->
					<div class="row contain-lg">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control input-sm"  id="preparado_por" name="form[txt][preparado_por]" value="{if isset($formDBoperacionesPR[0].nombre_registro)}{$formDBoperacionesPR[0].nombre_registro}{/if}" disabled>
								<input type="hidden" id="usuario_preparado_por">
								<label for="preparado_por">Preparado Por</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control input-sm" id="fecha_preparado" name="form[txt][fecha_preparado]" value="{if isset($formDBoperacionesPR[0].fec_operacion)}{$formDBoperacionesPR[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
								<label for="fecha_preparado">Fecha</label>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<input type="text" class="form-control input-sm" maxlength="7" id="ind_periodo" name="form[txt][ind_periodo]" value="{if isset($formDB.ind_periodo)}{$formDB.ind_periodo}{else}{$periodo}{/if}" disabled>
								<label for="ind_periodo">Periodo</label>
							</div>
						</div>
					</div>
					<div class="row contain-lg">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
								<label for="txt_observacion_pre">Observaciones</label>
							</div>
						</div>
					</div>
					<!-- CONFORMADO -->
					<div class="row contain-lg">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control input-sm"  id="conformado_por" name="form[txt][conformado_por]" value="{if isset($formDBoperacionesCO[0].nombre_registro)}{$formDBoperacionesCO[0].nombre_registro}{/if}" disabled>
								<input type="hidden" id="usuario_conformado_por">
								<label for="conformado_por">Conformado Por</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control input-sm" id="fecha_conformado" name="form[txt][fecha_conformado]" value="{if isset($formDBoperacionesCO[0].fec_operacion)}{$formDBoperacionesCO[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
								<label for="fecha_conformado">Fecha</label>
							</div>
						</div>
					</div>
					<div class="row contain-lg">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_con" name="form[txt][txt_observacion_con]" value="{if isset($formDBoperacionesCO[0].txt_observaciones)}{$formDBoperacionesCO[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
								<label for="txt_observacion_con">Observaciones</label>
							</div>
						</div>
					</div>
					<!-- APROBADO -->
					<div class="row contain-lg">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control input-sm"  id="aprobado_por" name="form[txt][aprobado_por]" value="{if isset($formDBoperacionesAP[0].nombre_registro)}{$formDBoperacionesAP[0].nombre_registro}{/if}" disabled>
								<input type="hidden" id="usuario_aprobado_por">
								<label for="aprobado_por">Aprobado Por</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control input-sm" id="fecha_aprobado" name="form[txt][fecha_aprobado]" value="{if isset($formDBoperacionesAP[0].fec_operacion)}{$formDBoperacionesAP[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
								<label for="fecha_aprobado">Fecha</label>
							</div>
						</div>
					</div>
					<div class="row contain-lg">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apro" name="form[txt][txt_observacion_apro]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
								<label for="txt_observacion_apro">Observaciones</label>
							</div>
						</div>
					</div>
				{/if}

				{if $proceso=='Anular'}

					<!-- ANULADO -->
					<div class="row contain-lg">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" class="form-control input-sm"  id="anulado_por" name="form[txt][anulado_por]" value="{if isset($formDBoperaciones[2].nombre_registro)}{$formDBoperaciones[2].nombre_registro}{/if}" disabled>
								<input type="hidden" id="usuario_anulado_por">
								<label for="anulado_por">Anulador Por</label>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" class="form-control input-sm" id="fecha_anulado" name="form[txt][fecha_anulado]" value="{if isset($formDBoperaciones[2].fec_operacion)}{$formDBoperaciones[2].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
								<label for="fecha_anulado">Fecha</label>
							</div>
						</div>
					</div>
					<div class="row contain-lg">
						<div class="col-md-12">
							<div class="form-group">
								<input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_anulado" name="form[txt][txt_observacion_anulado]" value="{if isset($formDBoperaciones[2].txt_observaciones)}{$formDBoperaciones[2].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
								<label for="txt_observacion_anulado">Observaciones</label>
							</div>
						</div>
					</div>

				{/if}
				<input type="hidden" id="fec_fecha" name="form[txt][fec_fecha]" value="{if isset($formDB.fec_fecha)}{$formDB.fec_fecha|date_format:"d-m-Y"}{else}{$fecha}{/if}">
				<div class="row contain-lg">
					<!-- botones para importar empleados, botones para insertar uno a uso y eliminar -->
					<br>
					<div align="right">
						<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
						<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="botonGuardar" style="display: none;"><span class="glyphicon glyphicon-floppy-disk"></span> Procesar </button>
						<input type="hidden" id="motivo" name="form[txt][motivo]" value="{if isset($T)}{$T}{/if}">
					</div>
					<br>
				</div>

			</div><!--end #step3 -->


		</div><!--end .tab-content -->
	</form>
	<ul class="pager wizard">
		<!--<li class="previous first"><a class="btn-raised" href="javascript:void(0);"> First </a></li>-->
		<li class="previous"><a class="btn-raised" href="javascript:void(0);"> Anterior </a></li>
		<!--<li class="next last"><a class="btn-raised" href="javascript:void(0);"> Last </a></li>-->
		<li class="next"><a class="btn-raised" href="javascript:void(0);"> Siguiente </a></li>
	</ul>
</div><!--end #rootwizard -->
<script type="text/javascript">
	$("#fecha_egreso").datepicker({
		todayHighlight: true,
		format:'dd-mm-yyyy',
		autoclose: true,
		language:'es'
	});

	$(".form-wizard-nav").on('click', function(){ //enables click event
		return false;
	});

	var $url = '{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/ConsultarEmpleadoMET';
	$('#buscarEmpleado').click(function(){
		$('#formModalLabel2').html($(this).attr('titulo'));
		$.post($url, { proceso: 'Nuevo' },function($dato){
			$('#ContenidoModal2').html($dato);
		});
	});
	$('#modalAncho').css( "width", "80%" );

	$('#modalAncho2').css( "width", "75%" );


	//si todo es correcto y cumple con los requimientos de validacion Envio
	$.validator.setDefaults({
		submitHandler: function() {

			//alert("Envio el formulario");
			var formulario = $( "#formAjax" );

			var disabled = formulario.find(':input:disabled').removeAttr('disabled');

			var datos = formulario.serialize();

			if($("#proceso").val()=='Nuevo'){
				var url = '{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/RegistrarMET';
				$.post(url, datos ,function(dato){

					if(dato['status']=='error'){
						{*para usuarios normales*}
						swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
						{*para usuarios programadores*}
						//swal("Error",respuesta['detalleERROR'], "error");
					}

					if(dato['status']=='registrar'){

						swal({
							title: 'Exito',
							text: 'Operación Realizada con Exito!!!.',
							type: "success",
							showCancelButton: false,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: 'Aceptar',
							closeOnConfirm: true
						}, function(){
							$("#_contenido").load('{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL');
						});

						$(document.getElementById('cerrarModal')).click();
						$(document.getElementById('ContenidoModal')).html('');


					}
				},'json');
			}
			if($("#proceso").val()=='Modificar'){
				var url = '{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/ModificarMET';
				$.post(url, datos ,function(dato){
					if(dato['status']=='error'){
						{*para usuarios normales*}
						swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
						{*para usuarios programadores*}
						//swal("Error",respuesta['detalleERROR'], "error");
					}
					if(dato['status']=='modificar'){

						swal("Exito","Operación Realizada con Exito!!!.", "success");
						$(document.getElementById('cerrarModal')).click();
						$(document.getElementById('ContenidoModal')).html('');

					}
				},'json');
			}
			if($("#proceso").val()=='Conformar'){
				var url = '{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/ConformarMET';

				swal({
					title: 'Conformar',
					text: 'Esta seguro que desea Conformar esta Pensión?',
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: 'Si, Conformar',
					closeOnConfirm: false
				}, function(){
					$.post(url, datos ,function(dato){
						if(dato['status']=='conformado'){

							swal({
								title: 'Exito',
								text: 'Operación Realizada con Exito!!!.',
								type: "success",
								showCancelButton: false,
								confirmButtonColor: "#DD6B55",
								confirmButtonText: 'Aceptar',
								closeOnConfirm: true
							}, function(){
								$("#_contenido").load('{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/ListadoConformarMET');
							});

							$(document.getElementById('cerrarModal')).click();
							$(document.getElementById('ContenidoModal')).html('');


						}
					},'json');
				});


			}
			if($("#proceso").val()=='Aprobar'){
				var url = '{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/AprobarMET';

				swal({
					title: 'Aprobar',
					text: 'Esta seguro que desea Aprobar esta Pensión?',
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: 'Si, Aprobar',
					closeOnConfirm: false
				}, function(){
					$.post(url, datos ,function(dato){
						if(dato['status']=='aprobado'){

							swal({
								title: 'Exito',
								text: 'Operación Realizada con Exito!!!.',
								type: "success",
								showCancelButton: false,
								confirmButtonColor: "#DD6B55",
								confirmButtonText: 'Aceptar',
								closeOnConfirm: true
							}, function(){
								$("#_contenido").load('{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/ListadoAprobarMET');
							});

							$(document.getElementById('cerrarModal')).click();
							$(document.getElementById('ContenidoModal')).html('');


						}
					},'json');
				});
			}

			if($("#proceso").val()=='Anular'){
				var url = '{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/AnularMET';
				swal({
					title: 'Anular',
					text: 'Esta seguro que desea anular este registro?',
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: 'Si, Anular',
					closeOnConfirm: false
				}, function(){
					$.post(url, datos ,function(dato){
						if(dato['status']=='anular'){

							swal({
								title: 'Exito',
								text: 'Operación Realizada con Exito!!!.',
								type: "success",
								showCancelButton: false,
								confirmButtonColor: "#DD6B55",
								confirmButtonText: 'Aceptar',
								closeOnConfirm: true
							}, function(){
								$("#_contenido").load('{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL');
							});

							$(document.getElementById('cerrarModal')).click();
							$(document.getElementById('ContenidoModal')).html('');

						}
					},'json');

				});
			}

		}
	});



	$("#num_monto_pension").change(function() {

		{*verifico si el monto de la pension cumple con los requisitos para optar por la pension*}
		var $url_verificar = '{$_Parametros.url}modRH/procesos/pensionesInvalidezCONTROL/VerificarRequisitosMET';
		$.post($url_verificar, { idEmpleado: $('#fk_rhb001_num_empleado').val(), tipo:'MONTO', monto: $("#num_monto_pension").val(), sueldo: $("#num_ultimo_sueldo").val()  }, function(resultado){

			$requisito = resultado['requisito'];
			if($requisito==1){
				$('#botonGuardar').show();
				$('#requisitoVer2').html('');
			} else {
				$('#botonGuardar').hide();
				$('#requisitoVer2').html('<div class="alert alert-danger"><strong><i class="glyphicon glyphicon-alert"></i></strong> El Sueldo NO puede ser menor que el '+resultado['PENPORMIN']+'% ni mayor que el '+resultado['PENPORMAX']+'% del Ultimo Sueldo </div>');
			}

		},'json');
	});


	var hoy = new Date();
	var fecha = hoy.getDate()+"-"+(hoy.getMonth()+1)+"-"+hoy.getFullYear();

	{*VER DATOS EN EL FORMULARIO*}
	if($("#proceso").val()=='Ver'){
		$(".form-control").attr("disabled","disabled");
		$("#buscarEmpleado").attr("disabled","disabled");
		$("#accion").hide();
	}
	{*PROCESO DE MODIFICAR*}
	if($("#proceso").val()=='Modificar'){

		{*desabilitado todo el formulario*}
		$(".form-control").attr("disabled","disabled");

		{*cambio nombre al boton*}
		$("#accion").html('Modificar');

		$("#txt_observacion_pre").removeAttr('disabled');
		$("#num_monto_pension").removeAttr('disabled');
		$("#buscarEmpleado").attr("disabled","disabled");
		{*habilito el boton guardar y le cambio el nombre*}
		$('#botonGuardar').show();
		$("#botonGuardar").html('Modificar');
	}
	{*PROCESO DE CONFORMAR*}
	if($("#proceso").val()=='Conformar'){
		$(".form-control").attr("disabled","disabled");
		$("#txt_observacion_con").removeAttr('disabled');
		$("#fecha_conformado").val(fecha);
		$("#conformado_por").val('{$nombreUsuario}');
		$("#usuario_conformado_por").val({$idUsuario});
		$("#buscarEmpleado").attr("disabled","disabled");
		{*cambio nombre al boton*}
		$('#botonGuardar').show();
		$("#botonGuardar").html('Conformar');
	}
	{*PROCESO DE APROBAR*}
	if($("#proceso").val()=='Aprobar'){
		$(".form-control").attr("disabled","disabled");
		$("#txt_observacion_apro").removeAttr('disabled');
		$("#fecha_aprobado").val(fecha);
		$("#aprobado_por").val('{$nombreUsuario}');
		$("#usuario_aprobado_por").val({$idUsuario});
		$("#buscarEmpleado").attr("disabled","disabled");
		{*cambio nombre al boton*}
		$('#botonGuardar').show();
		$("#botonGuardar").html('Aprobar')
		{*cambio algunos campos*}
		$("#fk_nmb001_num_tipo_nomina").removeAttr('disabled');
		$("#fk_a006_num_miscelaneo_detalle_tipotrab").removeAttr('disabled');
		$("#ind_resolucion").removeAttr('disabled');
		$("#num_situacion_trabajo").removeAttr('disabled');
		$("#fec_fecha_egreso").removeAttr('disabled');
		$("#fk_rhc032_num_motivo_cese").removeAttr('disabled');
		$("#txt_observacion_egreso").removeAttr('disabled');

	}
	{*ANULAR PROCESO*}
	if($("#proceso").val()=='Anular'){
		$(".form-control").attr("disabled","disabled");
		$('#botonGuardar').show();
		$("#botonGuardar").html('Anular');
		$("#txt_observacion_anulado").removeAttr('disabled');
		$("#fecha_anulado").val(fecha);
		$("#anulado_por").val('{$nombreUsuario}');
		$("#usuario_anulado_por").val({$idUsuario});
	}



</script>