<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form id="formAjax" action="#" class="form floating-label form-validation" role="form" novalidate>
        <input type="hidden" name="pk_num_jubilacion" id="pk_num_jubilacion" value="{$jub.pk_num_jubilacion}"/>
        <div class="form-wizard-nav">
            <div class="progress"><div class="progress-bar progress-bar-primary"></div>
            </div>
            <ul class="nav nav-justified">
                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">	Información General</span></a></li>
                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Antecedentes de Servicio</span></a></li>
                <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Relación de Sueldos</span></a></li>
                <li><a href="#step4" data-toggle="tab"><span class="step">4</span> <span class="title">Jubilación</span></a></li>
            </ul>
        </div><!--end .form-wizard-nav -->
        <!-- *********** PASO 1 ******************-->
        <div class="tab-content clearfix">
            <br/>
            <div class="tab-pane active" id="step1">
                <div class="col-md-6 col-sm-6">
                    <div class="well clearfix">
                        <div class="form-group floating-label">
                            <input type="text" value="{$jub.ind_nombre1} {$jub.ind_nombre2} {$jub.ind_apellido1} {$jub.ind_apellido2}" class="form-control dirty" disabled>
                            <label for="pk_num_empleado"> Empleado</label>
                        </div>
                        <div class="form-group floating-label">
                            <input type="text" name="num_organismo_previo" id="num_organismo_previo" value="{$jub.ind_descripcion_empresa}" class="form-control dirty" disabled>
                            <label for="num_organismo"> Organismo</label>
                        </div>
                        <div class="form-group floating-label">
                            <input type="text" name="num_dependencia_preevio" id="num_dependencia_previo" value="{$jub.ind_dependencia}" class="form-control dirty" disabled>
                            <label for="num_dependencia"> Dependencia</label>
                        </div>
                        <div class="form-group floating-label">
                            <input type="text" name="cargo_previo" id="cargo_previo" value="{$jub.ind_descripcion_cargo}" class="form-control dirty" disabled>
                            <label for="cargo"> Cargo</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="salario_previo" id="salario_previo" value="{$jub.num_ultimo_sueldo|number_format:2:',':'.'}" class="form-control dirty" disabled>
                            <label class="control-label">Sueldo Actual</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="well clearfix">
                        <div class="form-group">
                            <input type="text" name="num_documento_previo" id="num_documento_previo" value="{$jub.ind_cedula_documento|number_format:0:'':'.'}" class="form-control dirty" disabled>
                            <label class="control-label">Nro de documento</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="sexo_previo" id="sexo_previo" value="{$jub.ind_nombre_detalle}" class="form-control dirty" disabled>
                            <label class="control-label">Sexo</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="nacimiento_previo" id="nacimiento_previo" value="{$jub.fec_nacimiento|date_format:"%d/%m/%Y"}" class="form-control dirty" disabled>
                            <label class="control-label">Fecha de Nacimiento</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="edad_previo" id="edad_previo" value="{$jub.num_edad}" class="form-control dirty" disabled>
                            <label class="control-label">Edad</label>
                        </div>
                        <div class="form-group">
                            <input type="text" name="fecha_ingreso_previo" id="fecha_ingreso_previo" value="{$jub.fec_ingreso|date_format:"%d/%m/%Y"}" class="form-control dirty" disabled>
                            <label class="control-label">Fecha de Ingreso</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12" id="requisitoVer">
                    <!-- Aqui se carga el mensaje informativo si el empleado cumple o no los requisitos para optar a la jubilación-->
                </div>
            </div><!--end #step1 -->
            <!-- *********** PASO 2 ******************-->
            <div class="tab-pane" id="step2">
                <table id="datatable3" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th><i class="md-person"></i> Nº</th>
                        <th><i class="md-person"></i> Organismo</th>
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Fecha de Ingreso</th>
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Fecha de Egreso</th>
                        <th><i class="glyphicon glyphicon-cog"></i> Años</th>
                        <th><i class="glyphicon glyphicon-cog"></i> Meses</th>
                        <th><i class="glyphicon glyphicon-cog"></i> Dias</th>
                    </tr>
                    </thead>
                    <tbody>
                    {assign var=numero value=1}
                    {if $variableServicio.valorServicio==1}
                    {foreach item=serv from=$servicio}
                        <tr>
                            <td>{$numero++}</td>
                            <td>{$serv.empresa}</td>
                            <td>{$serv.fechaIngreso}</td>
                            <td>{$serv.fechaEgreso}</td>
                            <td>{$serv.anio}</td>
                            <td>{$serv.mes}</td>
                            <td>{$serv.dia}</td>
                        </tr>
                    {/foreach}
                    {/if}
                    <tr style="font-weight:bold;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Antecedentes:</td>
                        <td>{$total.anio}</td>
                        <td>{$total.mes}</td>
                        <td>{$total.dia}</td>
                    </tr>
                    <tr style="font-weight:bold;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>En el Organismo:</td>
                        <td>{$total.anioInst}</td>
                        <td>{$total.mesInst}</td>
                        <td>{$total.diaInst}</td>
                    </tr>
                    <tr style="font-weight:bold;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Tiempo de Servicio:</td>
                        <td>{$total.anioTotal}</td>
                        <td>{$total.mesTotal}</td>
                        <td>{$total.diaTotal}</td>
                    </tr>
                    </tbody>
                </table>
            </div><!--end #step2 -->
            <!-- *********** PASO 3 ******************-->
            <div class="tab-pane" id="step3">
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-6 col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Sueldo Básico</h3>
                            </div>
                            <div class="panel-body">
                                <table id="datatable4" class="table table-striped table-hover">
                                    <thead>
                                    <tr align="center">
                                        <th><i class="md-person"></i> Nº</th>
                                        <th><i class="md-person"></i> Periodo</th>
                                        <th><i class="glyphicon glyphicon-triangle-right"></i> Monto</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {assign var="numero" value="1"}
                                    {foreach item=sueldo from=$sueldoBase}
                                        <tr>
                                            <td>{$numero++}</td>
                                            <td>{$sueldo.num_anio_periodo}-{$sueldo.ind_mes_periodo|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}</td>
                                            <td>{$sueldo.num_monto|number_format:2:',':'.'}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Primas por Años de Servicio</h3>
                            </div>
                            <div class="panel-body">
                                <table id="datatable5" class="table table-striped table-hover">
                                    <thead>
                                    <tr align="center">
                                        <th><i class="md-person"></i> Nº</th>
                                        <th><i class="md-person"></i> Periodo</th>
                                        <th><i class="glyphicon glyphicon-triangle-right"></i> Monto</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {assign var="numero" value="1"}
                                    {foreach item=prima from=$sueldoPrima}
                                        <tr>
                                            <td>{$numero++}</td>
                                            <td>{$prima.num_anio_periodo}-{$prima.ind_mes_periodo|str_pad:2:"0":$smarty.const.STR_PAD_LEFT}</td>
                                            <td>{$prima.num_monto|number_format:2:',':'.'}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="sueldo_previo" id="sueldo_previo" value="{$jub.num_total_sueldo|number_format:2:',':'.'}" class="form-control dirty" disabled>
                            <label class="control-label">Sueldo</label>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="prima_previo" id="prima_previo" value="{$jub.num_total_primas|number_format:2:',':'.'}" class="form-control dirty" disabled>
                            <label class="control-label">Primas</label>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="total_previo" id="total_previo" value="{$jub.total|number_format:2:',':'.'}" class="form-control dirty" disabled>
                            <label class="control-label">Total</label>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="porcentaje_previo" id="porcentaje_previo" value="{$jub.num_porcentaje}" class="form-control dirty" disabled>
                            <label class="control-label">Porcentaje</label>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="baseJubilacion_previo" id="baseJubilacion_previo" value="{$jub.num_sueldo_base|number_format:2:',':'.'}" class="form-control dirty" disabled>
                            <label class="control-label">Base</label>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="jubilacion_previo" id="jubilacion_previo" value="{$jub.num_monto_jubilacion|number_format:2:',':'.'}" class="form-control dirty" disabled>
                            <label class="control-label">Jubilación</label>
                        </div>
                    </div>
                </div>
            </div><!--end #step3 -->
            <!-- *********** PASO 4 ******************-->
            <div class="tab-pane" id="step4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Datos de Jubilación</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12 col-sm-12">
                            <div class="col-sm-4 col-sm-4">
                                <div class="form-group">
                                    <input type="text"  value="{if $jub.ind_estado=='PR'}PREPARADO{/if}{if $jub.ind_estado=='CO'}CONFORMADO{/if}{if $jub.ind_estado=='AP'}APROBADO{/if}{if $jub.ind_estado=='AN'}ANULADO{/if}" class="form-control dirty" disabled>
                                    <label class="control-label">Estado</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-6 col-sm-6">
                                <div class="col-sm-9 col-sm-9">
                                    <div class="form-group">
                                        <input type="text" class="form-control dirty" value="{$estatusPreparado.procesadoPor}" disabled>
                                        <label class="control-label">Procesado por</label>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-sm-3">
                                    <div class="form-group">
                                        <input type="text" class="form-control dirty" value="{$estatusPreparado.fecha_operacion_preparado|date_format:"%d/%m/%Y"}" disabled>
                                        <label class="control-label">Fecha</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                {if $jub.ind_estado=='CO'}
                                    <div class="col-sm-9 col-sm-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control dirty" disabled value="{$datosUsuario.nombreUsuario}">
                                            <label class="control-label">Aprobado por</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-sm-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control dirty" disabled value="{$datosUsuario.fechaActual}">
                                            <label class="control-label">Fecha</label>
                                        </div>
                                    </div>
                                {else}
                                    <div class="col-sm-9 col-sm-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control dirty" disabled value="{$estatusAprobado.procesadoPor}">
                                            <label class="control-label">Aprobado por</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-sm-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control dirty" disabled value="{$estatusAprobado.fecha_operacion_aprobado|date_format:"%d/%m/%Y"}">
                                            <label class="control-label">Fecha</label>
                                        </div>
                                    </div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <textarea class="form-control dirty" rows="2" name="observacion_preparado" id="observacion_preparado" disabled>{$estatusPreparado.ind_observacion}</textarea>
                                    <label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observación</label>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                {if $jub.ind_estado=='CO'}
                                    <div class="form-group">
                                        <textarea class="form-control dirty" rows="2" name="observacion_aprobado" id="observacion_aprobado" {if $datosUsuario.valorFormulario == 'LI'} disabled {/if}></textarea>
                                        <label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observación</label>
                                    </div>
                                {else}
                                    <div class="form-group">
                                        <textarea class="form-control dirty" rows="2" name="observacion_aprobado" id="observacion_aprobado" disabled>{$estatusAprobado.ind_observacion}</textarea>
                                        <label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observación</label>
                                    </div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-6 col-sm-6">
                                {if $jub.ind_estado=='PR'}
                                    <div class="col-sm-9 col-sm-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control dirty" disabled value="{$datosUsuario.nombreUsuario}">
                                            <label class="control-label">Conformado por</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-sm-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control dirty" disabled value="{$datosUsuario.fechaActual}">
                                            <label class="control-label">Fecha</label>
                                        </div>
                                    </div>
                                {else}
                                    <div class="col-sm-9 col-sm-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control dirty" disabled value="{$estatusConformado.procesadoPor}">
                                            <label class="control-label">Conformado por</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3 col-sm-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control dirty" disabled value="{$estatusConformado.fecha_operacion_conformado|date_format:"%d/%m/%Y"}">
                                            <label class="control-label">Fecha</label>
                                        </div>
                                    </div>
                                {/if}
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-6 col-sm-6">
                                {if $jub.ind_estado=='PR'}
                                        <div class="form-group">
                                            <textarea class="form-control dirty" rows="2" name="observacion_conformado" id="observacion_conformado"  {if $datosUsuario.valorFormulario == 'LI'} disabled {/if}></textarea>
                                            <label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observación</label>
                                        </div>
                                {else}
                                    <div class="form-group">
                                        <textarea class="form-control dirty" rows="2" name="observacion_conformado" id="observacion_conformado" disabled>{$estatusConformado.ind_observacion}</textarea>
                                        <label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observación</label>
                                    </div>
                                {/if}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Planilla</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group floating-label">
                                    <select id="tipo_nomina" name="tipo_nomina" class="form-control dirty">
                                        {foreach item=nom from=$tipoNomina}
                                            {if $nom.pk_num_tipo_nomina==$jub.fk_nmb001_num_tipo_nomina}
                                                <option value="{$nom.pk_num_tipo_nomina}" selected>{$nom.ind_nombre_nomina}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="tipo_nomina"><i class="glyphicon glyphicon-home"></i> Tipo de Nómina</label>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group floating-label">
                                    <select id="tipo_trabajador" name="tipo_trabajador" class="form-control dirty">
                                        {foreach item=tipo from=$tipoTrabajador}
                                            {if $tipo.pk_num_miscelaneo_detalle==$jub.fk_a006_num_tipo_trabajador}
                                                <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="tipo_trabajador"><i class="glyphicon glyphicon-home"></i> Tipo de Trabajador</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cese</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-4 col-sm-4">
                                <table>
                                    <tr>
                                        <td><h4>Estado:&nbsp;&nbsp;</h4></td>
                                        <td>
                                            <label class="radio-inline radio-styled">
                                                <input type="radio" name="estado" value="1" ><span>Activo</span>
                                            </label>
                                        </td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>
                                            <label class="radio-inline radio-styled">
                                                <input type="radio" name="estado" value="0"  checked><span>Inactivo</span>
                                            </label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-3 col-sm-3">
                                <div class="form-group floating-label">
                                    <select id="motivo_cese" name="motivo_cese" class="form-control dirty">
                                        {foreach item=cese from=$motivoCese}
                                            {if $cese.pk_num_motivo_cese==$jub.fk_rhc032_num_motivo_cese}
                                                <option selected value="{$cese.pk_num_motivo_cese}">{$cese.ind_nombre_cese}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="motivo_cese"><i class="glyphicon glyphicon-home"></i> Motivo del Cese</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group control-width-normal">
                                    <div class="input-group date" id="demo-date-format">
                                        <div class="input-group-content">
                                            <input type="text" class="form-control dirty" name="fecha_cese" id="fecha_cese" disabled value="{$jub.fec_egreso|date_format:"%d/%m/%Y"}">
                                            <label>Fecha de Cese</label>
                                        </div>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div><!--end .form-group -->
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="form-group">
                                    <textarea class="form-control dirty" rows="2" name="explicacion_cese" id="explicacion_cese" disabled>{$jub.ind_observacion_cese}</textarea>
                                    <label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Explicación</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="botonGuardar" align="center">
                    {if $datosUsuario.valorFormulario == 'PR'}
                        {if $jub.ind_estado=='PR'}
                            {if in_array('RH-01-02-02-02-L',$_Parametros.perfil)}
                                <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion" value="CO" onclick="estatus(this.value)"><span class="glyphicon glyphicon-pencil"></span> Conformar</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-primary ink-reaction btn-danger" id="accion" value="AN" onclick="estatus(this.value)"><span class="glyphicon glyphicon-ban-circle"></span> Anular</button>&nbsp;&nbsp;
                            {/if}
                        {/if}
                    {/if}
                    {if $datosUsuario.valorFormulario == 'CO'}
                        {if $jub.ind_estado=='CO'}
                            {if in_array('RH-01-02-02-03-L',$_Parametros.perfil)}
                                <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion" value="AP" onclick="estatus(this.value)"><span class="glyphicon glyphicon-ok"></span> Aprobar</button>&nbsp;&nbsp;
                                <button type="button" class="btn btn-primary ink-reaction btn-danger" id="accion" value="AN" onclick="estatus(this.value)"><span class="glyphicon glyphicon-ban-circle"></span> Anular</button>&nbsp;&nbsp;
                            {/if}
                        {/if}
                    {/if}
                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
                </div>
            </div><!--end #step4 -->
        </div><!--end .tab-content -->
    </form>
    <ul class="pager wizard">
        <li class="previous first"><a class="btn-raised" href="javascript:void(0);"> First </a></li>
        <li class="previous"><a class="btn-raised" href="javascript:void(0);"> Previous </a></li>
        <li class="next last"><a class="btn-raised" href="javascript:void(0);"> Last </a></li>
        <li class="next"><a class="btn-raised" href="javascript:void(0);"> Next </a></li>
    </ul>
</div><!--end #rootwizard -->
<script type="text/javascript">
    function estatus(valor)
    {
        var $url='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/EstatusMET';
        var $pkNumJubilacion = $("#pk_num_jubilacion").val();
        if(valor=='AN'){
            var $ind_observacion = '';
        }
        if(valor =='CO') {
            var $ind_observacion = $("#observacion_conformado").val();
        }
        if(valor =='AP') {
            var $ind_observacion = $("#observacion_aprobado").val();
        }
        $.post($url,{ pk_num_jubilacion: $pkNumJubilacion, valor: valor, ind_observacion: $ind_observacion},function(dato){
            if(dato['ind_estado']=='PR'){
                var estado = 'PREPARADO';
            }
            if(dato['ind_estado']=='CO'){
                var estado = 'CONFORMADO';
            }
            if(dato['ind_estado']=='AP'){
                var estado = 'APROBADO';
            }
            if(dato['ind_estado']=='AN'){
                var estado = 'ANULADO';
            }
            $('#pk_num_jubilacion'+dato['pk_num_jubilacion']).remove();
            swal(estado, "Procedimiento exitoso", "success");
            $(document.getElementById('cerrarModal')).click();
            $(document.getElementById('ContenidoModal')).html('');
        }, 'json');
    }
</script>