<br/>
<input type="hidden" id="valorFormulario" value="{$emp.estado}" />
<section class="style-default-bright">
    <h2 class="text-primary">&nbsp;Lista de Jubilaciones {if $emp.estado=='PR'}: Conformar{/if}{if $emp.estado=='CO'}: Aprobar{/if}</h2>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div align="right">
                {if in_array('RH-01-02-02-05-F',$_Parametros.perfil)}
                    <div class="btn-group"><a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a></div>&nbsp;
                {/if}
                    <hr class="ruler-xl">
                </div>
                <br/>
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Nro. Proceso</th>
                        <th><i class="md-person"></i> Empleado</th>
                        <th><i class="md-person"></i> Nombre Completo</th>
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Nro. Documento</th>
                        <th><i class="glyphicon glyphicon-cog"></i> Estatus</th>
                        <th width="40">Ver</th>
                        <th width="40">Editar</th>
                        <th width="40">Reporte</th>
                    </tr>
                    </thead>
                    <tbody>
                        {foreach item=listar from=$listarJubilacion}
                            <tr id="pk_num_jubilacion{$listar.pk_num_jubilacion}">
                                <td>{$listar.pk_num_jubilacion}</td>
                                <td>{$listar.pk_num_empleado}</td>
                                <td>{$listar.ind_nombre1} {$listar.ind_nombre2} {$listar.ind_apellido1} {$listar.ind_apellido2}</td>
                                <td>{$listar.ind_cedula_documento|number_format:0:'':'.'}</td>
                                <td>
                                    {if $listar.ind_estado=='PR'}PREPARADO{/if}
                                    {if $listar.ind_estado=='AP'}APROBADO{/if}
                                    {if $listar.ind_estado=='CO'}CONFORMADO{/if}
                                    {if $listar.ind_estado=='AN'}ANULADO{/if}
                                </td>
                                <td align="center">{if in_array('RH-01-02-02-02-V',$_Parametros.perfil)}<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_jubilacion="{$listar.pk_num_jubilacion}" title="Ver Jubilacion"  titulo="Ver Jubilacion"><i class="glyphicon glyphicon-search"></i></button>{/if}</td>
                                {if $listar.ind_estado=='PR'}
                                    <td align="center">{if in_array('RH-01-02-02-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_jubilacion="{$listar.pk_num_jubilacion}" title="Modificar Jubilacion"  titulo="Modificar Jubilacion"><i class="fa fa-edit"></i></button>{/if}</td>
                                   {else}
                                    <td></td>
                                {/if}
                                <td align="center">{if in_array('RH-01-02-02-04-R',$_Parametros.perfil)}<button class="reporte logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_jubilacion="{$listar.pk_num_jubilacion}" title="Ver Reporte de Jubilacion"  titulo="Ver Reporte de Jubilacion"><i class="md md-attach-file"></i></button>{/if}</td>
                            </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="8">
                            {if $emp.estado=='LI'}
                                {if in_array('RH-01-02-02-01-N',$_Parametros.perfil)}
                                    <button  class="logsUsuario btn ink-reaction btn-raised btn-info" descripcion="El usuario ha creado una solicitud de jubilación" data-toggle="modal" data-target="#formModal" titulo="Registrar nueva jubilación" id="nuevo"> <span class="glyphicon glyphicon-log-out"></span> Nueva Jubilación</button>
                                {/if}
                            {/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<!-- filtro -->
<div class="offcanvas">
    <form id="formulario" class="form floating-label" role="form" method="post" novalidate>
        <div id="offcanvas-filtro" class="offcanvas-pane width-12">
            <div class="offcanvas-head">
                <header>Filtro de Búsqueda</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                </div>
            </div>
            <div class="offcanvas-body">
                    <div class="form-group floating-label">
                        <select id="pk_num_organismo" name="pk_num_organismo" class="form-control dirty" onchange="cargarDependencia(this.value)">
                            {foreach item=org from=$listadoOrganismo}
                                {if $org.pk_num_organismo == $emp.pk_num_organismo}
                                    <option selected value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                                {else}
                                    <option value="{$org.pk_num_organismo}">{$org.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="pk_num_organismo"><i class="glyphicon glyphicon-home"></i> Organismo</label>
                    </div>
                    <div id="dependencia">
                        <div class="form-group floating-label">
                            <select id="pk_num_dependencia" name="pk_num_dependencia" class="form-control dirty" onchange="cargarEmpleado(this.value)">
                                <option value="">&nbsp;</option>
                                {foreach item=dep from=$listadoDependencia}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/foreach}
                            </select>
                            <label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label>
                        </div>
                    </div>
                    <div id="empleado">
                        <div class="form-group floating-label">
                            <select id="pk_num_empleado" name="pk_num_empleado" id="pk_num_empleado" class="form-control dirty">
                                <option value="">&nbsp;</option>
                            </select>
                            <label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label>
                        </div>
                    </div>
                <div class="form-group">
                    <input type="text" name="ind_cedula_documento" id="ind_cedula_documento" class="form-control dirty">
                    <label class="control-label"><i class="glyphicon glyphicon-th"></i> N° de Documento</label>
                </div>
                <div class="form-group floating-label">
                    <select id="ind_estado" name="ind_estado" class="form-control dirty">
                        <option value=""></option>
                        {if $emp.estado=='LI'}
                        <option value="PR">Preparado</option>
                        <option value="CO">Conformado</option>
                        <option value="AP">Aprobado</option>
                        <option value="AN">Anulado</option>
                        {/if}
                        {if $emp.estado=='PR'}
                            <option value="PR">Preparado</option>
                        {/if}
                        {if $emp.estado=='CO'}
                            <option value="CO">Conformado</option>
                        {/if}
                    </select>
                    <label for="ind_estado"><i class="glyphicon glyphicon-user"></i> Estado</label>
                </div>
                <br/>
                <div class="col-sm-12 col-sm-12" align="center">
                    <div class="col-sm-6 col-sm-6">
                        <button type="button" id="buscarJubilacion" titulo="Listado de Jubilación" data-toggle="modal" data-target="#formModal" class="btn ink-reaction btn-flat btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Generando...">Generar Reporte<div style="top: 27px; left: 65px;" class="ink inverse"></div></button>
                    </div>
                    <div class="col-sm-6 col-sm-6">
                        <button type="submit" id="formulario" class="btn btn-primary ink-reaction btn-raised"><span class="glyphicon glyphicon-floppy-disk"></span> Buscar</button>
                    </div>
                </div>
            </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/NuevaJubilacionMET';
        $('#nuevo').click(function(){
            $('#modalAncho').css( "width", "90%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        var $urlEditar='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/EditarJubilacionMET';
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            var valorFormulario = $("#valorFormulario").val();
            $('#modalAncho').css( "width", "90%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($urlEditar,{ pk_num_jubilacion: $(this).attr('pk_num_jubilacion'), valorFormulario: valorFormulario},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        var $urlVer='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/VerJubilacionMET';
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            var valorFormulario = $("#valorFormulario").val();
            $('#modalAncho').css( "width", "90%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($urlVer,{ pk_num_jubilacion: $(this).attr('pk_num_jubilacion'), valorFormulario: valorFormulario },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.reporte', function () {
            $('#modalAncho').css( 'width', '85%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var pk_num_jubilacion = $(this).attr('pk_num_jubilacion');
            var urlReporte = '{$_Parametros.url}modRH/procesos/jubilacionCONTROL/ReporteJubilacionMET/?pk_num_jubilacion='+pk_num_jubilacion;
            $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
        });
    });

    function cargarDependencia(pk_num_organismo) {
        $("#dependencia").html("");
        $.post("{$_Parametros.url}modRH/procesos/jubilacionCONTROL/BuscarDependenciaMET",{ pk_num_organismo:""+pk_num_organismo }, function (dato) {
            $("#dependencia").html(dato);
        });
    }

    function cargarEmpleado(pk_num_dependencia) {
        $("#empleado").html("");
        $.post("{$_Parametros.url}modRH/procesos/jubilacionCONTROL/BuscarEmpleadoMET",{ pk_num_dependencia:""+pk_num_dependencia }, function (dato) {
            $("#empleado").html(dato);
        });
    }

    $("#formulario").validate({
        submitHandler: function(form) {
            var pk_num_empleado = $("#pk_num_empleado").val();
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var ind_cedula_documento = $("#ind_cedula_documento").val();
            var ind_estado = $("#ind_estado").val();

            var url_listar='{$_Parametros.url}modRH/procesos/jubilacionCONTROL/BuscarJubilacionMET';
            $.post(url_listar,{ pk_num_empleado: pk_num_empleado, ind_cedula_documento: ind_cedula_documento, pk_num_organismo: pk_num_organismo, pk_num_dependencia: pk_num_dependencia, ind_estado: ind_estado},function(respuesta_post) {
                var tabla_listado = $('#datatable1').DataTable();
                tabla_listado.clear().draw();
                if(respuesta_post != -1) {
                    for(var i=0; i<respuesta_post.length; i++) {
                        if(respuesta_post[i].ind_estado=='PR'){
                            var botonEditar = '{if in_array('RH-01-02-02-03-M',$_Parametros.perfil)}<button type="button" pk_num_jubilacion = "' + respuesta_post[i].pk_num_jubilacion + '" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"  descripcion="El usuario ha modificado una jubilación" title="Modificar Jubilación"  titulo="Modificar Jubilación"><i class="fa fa-edit"></i></button>{/if}';
                        } else {
                            var botonEditar = ' ';
                        }
                        if(respuesta_post[i].ind_estado=='PR'){
                            var estado = 'PREPARADO';
                        }
                        if(respuesta_post[i].ind_estado=='CO'){
                            var estado = 'CONFORMADO';
                        }
                        if(respuesta_post[i].ind_estado=='AP'){
                            var estado = 'APROBADO';
                        }
                        if(respuesta_post[i].ind_estado=='AN'){
                            var estado = 'ANULADO';
                        }
                       var fila = tabla_listado.row.add([
                            respuesta_post[i].pk_num_jubilacion,
                            respuesta_post[i].pk_num_empleado,
                            respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_nombre2+' '+ respuesta_post[i].ind_apellido1+' '+ respuesta_post[i].ind_apellido2,
                            respuesta_post[i].ind_cedula_documento,
                            estado,
                            '{if in_array('RH-01-02-02-02-V',$_Parametros.perfil)}<button type="button" pk_num_jubilacion = "' + respuesta_post[i].pk_num_jubilacion + '" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" descripcion="El usuario ha visualizado una jubilacion" title="Ver Jubilación" titulo="Ver Jubilación"><i class="glyphicon glyphicon-search"></i></button>{/if}',
                            botonEditar,
                            '{if in_array('RH-01-02-02-04-R',$_Parametros.perfil)}<button class="reporte logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado la jubilación" title="Ver Reporte de Jubilacion"  titulo="Ver Reporte de Jubilacion" data-toggle="modal" data-target="#formModal" pk_num_jubilacion="' + respuesta_post[i].pk_num_jubilacion + '"><i class="md md-attach-file" style="color: #ffffff;"></i></button>{/if}'
                        ]).draw()
                                .nodes()
                                .to$()
                        $(fila).attr('id','pk_num_jubilacion'+respuesta_post[i].pk_num_jubilacion+'');
                    }
                }

            },'json');
        }
    });

    $(document).ready(function() {
        $('#buscarJubilacion').click(function () {
            $('#modalAncho').css( 'width', '85%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            var pk_num_empleado = $("#pk_num_empleado").val();
            var pk_num_organismo = $("#pk_num_organismo").val();
            var pk_num_dependencia = $("#pk_num_dependencia").val();
            var ind_cedula_documento = $("#ind_cedula_documento").val();
            var ind_estado = $("#ind_estado").val();
            var urlReporte = '{$_Parametros.url}modRH/procesos/jubilacionCONTROL/GenerarReporteJubilacionMET/?pk_num_organismo='+pk_num_organismo+'&pk_num_dependencia='+pk_num_dependencia+'&pk_num_empleado='+pk_num_empleado+'&ind_cedula_documento='+ind_cedula_documento+'&ind_estado='+ind_estado;
            $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');
        });
    });

</script>