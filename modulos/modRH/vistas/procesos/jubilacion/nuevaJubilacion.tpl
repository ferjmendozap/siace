<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
	<form id="formAjax" action="{$_Parametros.url}modRH/procesos/jubilacionCONTROL/NuevaJubilacionMET" class="form floating-label form-validation" role="form" novalidate>
		<input type="hidden" name="valido" value="1" />
		<input type="hidden" name="pkNumEmpleado" id="pkNumEmpleado"/>
		<input type="hidden" name="anioServicio" id="anioServicio"/>
		<input type="hidden" name="anioExperiencia" id="anioExperiencia"/>
		<input type="hidden" name="anioTotal" id="anioTotal"/>
		<input type="hidden" name="num_organismo" id="num_organismo"/>
		<input type="hidden" name="num_dependencia" id="num_dependencia"/>
		<input type="hidden" name="cargo" id="cargo"/>
		<input type="hidden" name="salario" id="salario"/>
		<input type="hidden" name="num_documento" id="num_documento"/>
		<input type="hidden" name="sexo" id="sexo"/>
		<input type="hidden" name="nacimiento" id="nacimiento"/>
		<input type="hidden" name="edad" id="edad"/>
		<input type="hidden" name="fecha_ingreso" id="fecha_ingreso"/>
		<input type="hidden" name="sueldo" id="sueldo"/>
		<input type="hidden" name="prima" id="prima"/>
		<input type="hidden" name="total" id="total"/>
		<input type="hidden" name="porcentaje" id="porcentaje"/>
		<input type="hidden" name="baseJubilacion" id="baseJubilacion"/>
		<input type="hidden" name="jubilacion" id="jubilacion"/>
		<input type="hidden" name="coeficiente" id="coeficiente"/>
		<div class="form-wizard-nav">
			<div class="progress"><div class="progress-bar progress-bar-primary"></div>
			</div>
			<ul class="nav nav-justified">
				<li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">	Información General</span></a></li>
				<li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Antecedentes de Servicio</span></a></li>
				<li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Relación de Sueldos</span></a></li>
				<li><a href="#step4" data-toggle="tab"><span class="step">4</span> <span class="title">Jubilación</span></a></li>
			</ul>
		</div><!--end .form-wizard-nav -->
		<!-- *********** PASO 1 ******************-->
		<div class="tab-content clearfix">
			<br/>
			<div class="tab-pane active" id="step1">
						<div class="col-md-6 col-sm-6">
							<div class="well clearfix">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-content">
											<input type="text" class="form-control dirty" id="nombreTrabajador" name="nombreTrabajador">
											<label for="groupbutton9">Empleado</label>
										</div>
										<div class="input-group-btn">
											<button class="btn btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado">Buscar</button>
										</div>
									</div>
								</div>
								<div class="form-group floating-label">
									<input type="text" name="num_organismo_previo" id="num_organismo_previo" class="form-control dirty" disabled>
									<label for="num_organismo"> Organismo</label>
								</div>
								<div class="form-group floating-label">
									<input type="text" name="num_dependencia_preevio" id="num_dependencia_previo" class="form-control dirty" disabled>
									<label for="num_dependencia"> Dependencia</label>
								</div>
								<div class="form-group floating-label">
									<input type="text" name="cargo_previo" id="cargo_previo" class="form-control dirty" disabled>
									<label for="cargo"> Cargo</label>
								</div>
								<div class="form-group">
									<input type="text" name="salario_previo" id="salario_previo" class="form-control dirty" disabled>
									<label class="control-label">Sueldo Actual</label>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="well clearfix">
								<div class="form-group">
									<input type="text" name="num_documento_previo" id="num_documento_previo" class="form-control dirty" disabled>
									<label class="control-label">Nro de documento</label>
								</div>
								<div class="form-group">
									<input type="text" name="sexo_previo" id="sexo_previo" class="form-control dirty" disabled>
									<label class="control-label">Sexo</label>
								</div>
								<div class="form-group">
									<input type="text" name="nacimiento_previo" id="nacimiento_previo" class="form-control dirty" disabled>
									<label class="control-label">Fecha de Nacimiento</label>
								</div>
								<div class="form-group">
									<input type="text" name="edad_previo" id="edad_previo" class="form-control dirty" disabled>
									<label class="control-label">Edad</label>
								</div>
								<div class="form-group">
									<input type="text" name="fecha_ingreso_previo" id="fecha_ingreso_previo" class="form-control dirty" disabled>
									<label class="control-label">Fecha de Ingreso</label>
								</div>
							</div>
						</div>
					<div class="col-md-12 col-sm-12" id="requisitoVer">
						<!-- Aqui se carga el mensaje informativo si el empleado cumple o no los requisitos para optar a la jubilación-->
					</div>
			</div><!--end #step1 -->
			<!-- *********** PASO 2 ******************-->
			<div class="tab-pane" id="step2">
				<table id="datatable3" class="table table-striped table-hover">
					<thead>
					<tr align="center">
						<th><i class="md-person"></i> Nº</th>
						<th><i class="md-person"></i> Organismo</th>
						<th><i class="glyphicon glyphicon-triangle-right"></i> Fecha de Ingreso</th>
						<th><i class="glyphicon glyphicon-triangle-right"></i> Fecha de Egreso</th>
						<th><i class="glyphicon glyphicon-cog"></i> Años</th>
						<th><i class="glyphicon glyphicon-cog"></i> Meses</th>
						<th><i class="glyphicon glyphicon-cog"></i> Dias</th>
					</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div><!--end #step2 -->
			<!-- *********** PASO 3 ******************-->
			<div class="tab-pane" id="step3">
				<div class="col-md-12 col-sm-12">
					<div class="col-md-6 col-sm-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Sueldo Básico</h3>
							</div>
							<div class="panel-body">
								<table id="datatable4" class="table table-striped table-hover">
									<thead>
									<tr align="center">
										<th><i class="md-person"></i> Nº</th>
										<th><i class="md-person"></i> Periodo</th>
										<th><i class="glyphicon glyphicon-triangle-right"></i> Monto</th>
									</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Primas por Años de Servicio</h3>
							</div>
							<div class="panel-body">
								<table id="datatable5" class="table table-striped table-hover">
									<thead>
									<tr align="center">
										<th><i class="md-person"></i> Nº</th>
										<th><i class="md-person"></i> Periodo</th>
										<th><i class="glyphicon glyphicon-triangle-right"></i> Monto</th>
									</tr>
									</thead>
									<tbody></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 col-sm-12">
					<div class="col-sm-2 col-sm-2">
						<div class="form-group">
							<input type="text" name="sueldo_previo" id="sueldo_previo" class="form-control dirty" disabled>
							<label class="control-label">Sueldo</label>
						</div>
					</div>
					<div class="col-sm-2 col-sm-2">
						<div class="form-group">
							<input type="text" name="prima_previo" id="prima_previo" class="form-control dirty" disabled>
							<label class="control-label">Primas</label>
						</div>
					</div>
					<div class="col-sm-2 col-sm-2">
						<div class="form-group">
							<input type="text" name="total_previo" id="total_previo" class="form-control dirty" disabled>
							<label class="control-label">Total</label>
						</div>
					</div>
					<div class="col-sm-2 col-sm-2">
						<div class="form-group">
							<input type="text" name="porcentaje_previo" id="porcentaje_previo" class="form-control dirty" disabled>
							<label class="control-label">Porcentaje</label>
						</div>
					</div>
					<div class="col-sm-2 col-sm-2">
						<div class="form-group">
							<input type="text" name="baseJubilacion_previo" id="baseJubilacion_previo" class="form-control dirty" disabled>
							<label class="control-label">Base</label>
						</div>
					</div>
					<div class="col-sm-2 col-sm-2">
						<div class="form-group">
							<input type="text" name="jubilacion_previo" id="jubilacion_previo" class="form-control dirty" disabled>
							<label class="control-label">Jubilación</label>
						</div>
					</div>
				</div>
			</div><!--end #step3 -->
			<!-- *********** PASO 4 ******************-->
			<div class="tab-pane" id="step4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Datos de Jubilación</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-12 col-sm-12">
							<div class="col-sm-4 col-sm-4">
								<div class="form-group">
									<input type="text"  value="En Preparación" class="form-control" disabled>
									<label class="control-label">Estado</label>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-6 col-sm-6">
								<div class="col-sm-9 col-sm-9">
									<div class="form-group">
										<input type="text" class="form-control" value="{$emp.ind_nombre1} {$emp.ind_nombre2} {$emp.ind_apellido1} {$emp.ind_apellido2}" disabled>
										<label class="control-label">Procesado por</label>
									</div>
								</div>
								<div class="col-sm-3 col-sm-3">
									<div class="form-group">
										<input type="text" class="form-control" value="{$emp.fecha_actual}" disabled>
										<label class="control-label">Fecha</label>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="col-sm-9 col-sm-9">
									<div class="form-group">
										<input type="text" class="form-control" disabled>
										<label class="control-label">Aprobado por</label>
									</div>
								</div>
								<div class="col-sm-3 col-sm-3">
									<div class="form-group">
										<input type="text" class="form-control" disabled>
										<label class="control-label">Fecha</label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<textarea class="form-control" rows="2" name="observacion_preparado" id="observacion_preparado"></textarea>
									<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observación</label>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<textarea class="form-control" rows="2" name="observacion_conformado" id="observacion_aprobado" disabled></textarea>
									<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observación</label>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-6 col-sm-6">
								<div class="col-sm-9 col-sm-9">
									<div class="form-group">
										<input type="text" class="form-control" disabled>
										<label class="control-label">Conformado por</label>
									</div>
								</div>
								<div class="col-sm-3 col-sm-3">
									<div class="form-group">
										<input type="text" class="form-control" disabled>
										<label class="control-label">Fecha</label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<textarea class="form-control" rows="2" name="observacion_preparado" id="observacion_conformado" disabled></textarea>
									<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Observación</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Planilla</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-4">
								<div class="form-group floating-label">
									<select id="tipo_nomina" name="tipo_nomina" class="form-control dirty">
										{foreach item=nom from=$tipoNomina}
											{if $nom.ind_nombre_nomina=='JUBILADOS'}
												<option value="{$nom.pk_num_tipo_nomina}" selected>{$nom.ind_nombre_nomina}</option>
											{else}
												<option value="{$nom.pk_num_tipo_nomina}">{$nom.ind_nombre_nomina}</option>
											{/if}
										{/foreach}
									</select>
									<label for="tipo_nomina"><i class="glyphicon glyphicon-home"></i> Tipo de Nómina</label>
								</div>
							</div>
							<div class="col-md-4 col-sm-4">
								<div class="form-group floating-label">
									<select id="tipo_trabajador" name="tipo_trabajador" class="form-control dirty">
										{foreach item=tipo from=$tipoTrabajador}
											{if $tipo.ind_nombre_detalle=='JUBILADO'}
												<option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
											{else}
												<option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
											{/if}
										{/foreach}
									</select>
									<label for="tipo_trabajador"><i class="glyphicon glyphicon-home"></i> Tipo de Trabajador</label>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Cese</h3>
					</div>
					<div class="panel-body">
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-4">
								<table>
									<tr>
										<td><h4>Estado:&nbsp;&nbsp;</h4></td>
										<td>
											<label class="radio-inline radio-styled">
												<input type="radio" name="estado" value="1" ><span>Activo</span>
											</label>
										</td>
										<td>&nbsp;&nbsp;</td>
										<td>
											<label class="radio-inline radio-styled">
												<input type="radio" name="estado" value="0"  checked><span>Inactivo</span>
											</label>
										</td>
									</tr>
								</table>
							</div>
							<div class="col-md-3 col-sm-3">
								<div class="form-group floating-label">
									<select id="motivo_cese" name="motivo_cese" class="form-control dirty">
										{foreach item=cese from=$motivoCese}
											{if $cese.ind_nombre_cese=='JUBILACIÓN Y/O PENSIÓN'}
												<option selected value="{$cese.pk_num_motivo_cese}">{$cese.ind_nombre_cese}</option>
											{else}
												<option value="{$cese.pk_num_motivo_cese}">{$cese.ind_nombre_cese}</option>
											{/if}
										{/foreach}
									</select>
									<label for="motivo_cese"><i class="glyphicon glyphicon-home"></i> Motivo del Cese</label>
								</div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-4">
								<div class="form-group control-width-normal">
									<div class="input-group date" id="demo-date-format">
										<div class="input-group-content">
											<input type="text" class="form-control" name="fecha_cese" id="fecha_cese">
											<label>Fecha de Cese</label>
										</div>
										<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
									</div>
								</div><!--end .form-group -->
							</div>
							<div class="col-md-4 col-sm-4">
								<div class="form-group">
									<textarea class="form-control" rows="2" name="explicacion_cese" id="explicacion_cese"></textarea>
									<label class="control-label"><i class="glyphicon glyphicon-pencil"></i> Explicación</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="botonGuardar" align="center">

				</div>

			</div><!--end #step4 -->
		</div><!--end .tab-content -->
	</form>
	<ul class="pager wizard">
		<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
		<li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
		<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>
		<li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
	</ul>
</div><!--end #rootwizard -->
<script type="text/javascript">
	$("#fecha_salida").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});

	$("#fecha_cese").datepicker({
		format: 'dd/mm/yyyy',
		language: 'es',
		autoclose: true
	});

	var $url = '{$_Parametros.url}modRH/procesos/jubilacionCONTROL/ConsultarEmpleadoMET';
	$('#buscarEmpleado').click(function(){
		$('#formModalLabel2').html($(this).attr('titulo'));
		$.post($url,'',function($dato){
			$('#ContenidoModal2').html($dato);
		});
	});


	$('#modalAncho2').css( "width", "90%" );

	$(document).ready(function() {
		$('#datatable3').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"previous": "<",
					"next": ">"
				}
			}
		} );

		$('#datatable4').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"previous": "<",
					"next": ">"
				}
			}
		} );

		$('#datatable5').DataTable( {
			"language": {
				"lengthMenu": "Mostrar _MENU_ ",
				"sSearch":        "Buscar:",
				"zeroRecords": "No hay resultados",
				"info": "Mostrar _PAGE_ de _PAGES_",
				"infoEmpty": "No hay resultados",
				"infoFiltered": "(filtered from _MAX_ total records)",
				"paginate": {
					"previous": "<",
					"next": ">"
				}
			}
		} );
	} );

	if ( $.fn.dataTable.isDataTable( '#datatable3' ) ) {
		table = $('#datatable3').DataTable();
	}
	else {
		table = $('#datatable3').DataTable( {
			paging: true
		} );
	}


	if ( $.fn.dataTable.isDataTable( '#datatable4' ) ) {
		table = $('#datatable4').DataTable();
	}
	else {
		table = $('#datatable4').DataTable( {
			paging: true
		} );
	}

	if ( $.fn.dataTable.isDataTable( '#datatable5' ) ) {
		table = $('#datatable5').DataTable();
	}
	else {
		table = $('#datatable5').DataTable( {
			paging: true
		} );
	}



	$(document).ready(function(){
		$("#formAjax").validate({
			rules:{
				nombreTrabajador: {
					required: true
				},
				fecha_cese: {
					required: true
				}
			},
			messages:{
				nombreTrabajador: {
					required: "Seleccione un empleado"
				},
				fecha_cese: {
					required: "Indique la fecha de cese"
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato) {
					if(dato['ind_estado']=='PR'){
						var estado = 'PREPARADO';
					}
					if(dato['ind_estado']=='RE'){
						var estado = 'REVISADO';
					}
					if(dato['ind_estado']=='CO'){
						var estado = 'CONFORMADO';
					}
					if(dato['ind_estado']=='AP'){
						var estado = 'APROBADO';
					}
					if(dato['ind_estado']=='AN'){
						var estado = 'ANULADO';
					}
					$(document.getElementById('datatable1')).append('<tr id="pk_num_jubilacion'+dato['pk_num_jubilacion']+'">' +
							'<td>'+dato['pk_num_jubilacion']+'</td>' +
							'<td>'+dato['pk_num_empleado']+'</td>' +
							'<td>'+dato['ind_nombre1']+' '+dato['ind_nombre2']+' '+dato['ind_apellido1']+' '+dato['ind_apellido2']+'</td>' +
							'<td>'+dato['ind_cedula_documento']+'</td>' +
							'<td>'+estado+'</td>' +
							'<td align="center">{if in_array('RH-01-02-02-02-V',$_Parametros.perfil)}<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha visualizado una jubilación" titulo="Visualizar Jubilación" data-toggle="modal" data-target="#formModal" pk_num_jubilacion="'+dato['pk_num_jubilacion']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button>{/if}</td>' +
							'<td align="center">{if in_array('RH-01-02-02-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El usuario ha modificado una jubilación" titulo="Modificar Jubilación" data-toggle="modal" data-target="#formModal" pk_num_jubilacion="'+dato['pk_num_jubilacion']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}</td>' +
							'<td align="center">{if in_array('RH-01-02-02-04-R',$_Parametros.perfil)}<button class="reporte logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha visualizado la jubilación" title="Ver Reporte de Jubilacion"  titulo="Ver Reporte de Jubilacion" data-toggle="modal" data-target="#formModal" pk_num_jubilacion="'+dato['pk_num_jubilacion']+'"><i class="md md-attach-file" style="color: #ffffff;"></i></button>{/if}</td>' +
							'</tr>');
					swal("Jubilación Procesada", "Jubilación guardada satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				} ,'json');
			}
		});
	});
</script>
