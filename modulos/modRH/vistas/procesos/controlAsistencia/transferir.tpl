<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Transfencia de Eventos</h2>
    </div>
    <div class="section-body">
        <form enctype="multipart/form-data" id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="file" class="form-control" name="archivo" id="archivo"  />
                        <label for="archivo">Archivo de Eventos</label>
                    </div>
                    <div align="right">
                        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion" disabled><span class="glyphicon glyphicon-floppy-disk"></span> Transferir Eventos</button>
                    </div>
                    <div id="cargando" align="right"></div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body no-padding">
                            <div class="alert alert-callout alert-danger no-margin">
                                <strong class="text-xl">Observaciones a Tomar en Cuenta</strong><br>
                                <ul>
                                    <li>Solo se podra cargar archivos de tipo Excel ( .xls, .xlsx ) o Calc ( .ods ) Respectivamente.</li>
                                    <li>El boton TRANSFERIR EVENTOS se activa solo y unicamente al seleccionar el archivo.</li>
                                    <li class="text-bold">El Archivo debe contener la siguiente información:</li>
                                    <ul>
                                        <li>FECHA <span class="text-sm text-primary-dark">Formato: xx/xx/xxxx</span> </li>
                                        <li>HORA <span class="text-sm text-primary-dark">Formato: xx:xx</span></li>
                                        <li>EVENTO</li>
                                        <li>CEDULA</li>
                                    </ul>
                                    <li>
                                        <div class="blog-image">
                                            <img class="img-responsive" src="{$_Parametros.ruta_Img}modRH/asistencia.png" alt="">
                                        </div>
                                    </li>
                                </ul>
                                <div class="stick-bottom-left-right">
                                    <div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"><canvas style="display: inline-block; width: 349px; height: 80px; vertical-align: top;" width="349" height="80"></canvas></div>
                                </div>
                            </div>
                        </div><!--end .card-body -->
                    </div>
                </div>

            </div>
        </form>
    </div>
</section>

<script type="text/javascript">





    $(document).ready(function() {

        //si todo es correcto y cumple con los requimientos de validacion Envio
        $("#accion").click(function() {

            $('#cargando').html('Procesando Archivo...');

           var tipo = 'PROCESAR';

            $.post('{$_Parametros.url}modRH/procesos/controlAsistenciaCONTROL/TransferirAsistenciaMET', { tipo:tipo, archivo: $("#archivo").val() } ,function(dato){


                if(dato==1) {
                    swal("Transferencia Exitosa!", "Operación Realizada Con Exito!", "success");
                    $('#formAjax').each(function () { {*resetear los campos del formulario*}
                        this.reset();
                    });
                    $('#cargando').html('');
                }
                else if(dato==2) {
                    swal("Error En Archivo!", "El Archivo contiene Horas y/o Fechas con Formato Incorrecto. Por Favor Verifique archivo de Eventos", "error");
                    $('#cargando').html('');
                    return false;
                }
                else{
                    swal("Error En Archivo!", dato, "error");
                    $('#cargando').html('');
                    return false;
                }

            },'json');

        });


        //AL SELECCIONAR EL ARCHIVO VALIDAMOS QUE SEAN LOS ARCHIVOS PERMITIDOS
        $("#archivo").change(function(e) {

            var files = e.target.files; // FileList object
            //obtenemos un array con los datos del archivo
            var file = $("#archivo")[0].files[0];
            //obtenemos el nombre del archivo
            var fileName = file.name;
            //obtenemos la extensión del archivo
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);

            if(archivosPermitidos(fileExtension))
            {
                //habilitamos el boton Transferir Eventos para procesar
                $("#accion").removeAttr("disabled");


                for (var i = 0, f; f = files[i]; i++) {

                    var reader = new FileReader();
                    reader.onload = (function(theFile) {
                        return function(e) {

                            var data = new FormData($("#formAjax")[0]);
                            data.append('tipo','TRANSFERIR');

                            $.ajax({
                                url: '{$_Parametros.url}modRH/procesos/controlAsistenciaCONTROL/TransferirAsistenciaMET',
                                data: data,
                                type: 'POST',
                                contentType: false,
                                processData: false,
                                success: function(data){


                                }
                            });
                        };
                    })(f);
                    reader.readAsDataURL(f);
                }


            }else{

                swal("Error En Archivo!", "Asegurese de subir una archivo tipo: xls - xlsx - cvs - ods", "error");

            }


        });


    });

    function archivosPermitidos(extension)
    {
        switch(extension.toLowerCase())
        {
            case 'xls':  case 'xlsx': case 'ods':
            return true;
            break;
            default:
                return false;
                break;
        }
    }

</script>