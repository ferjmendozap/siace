<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
	<form id="formAjax" action="{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/RegistrarMET" class="form form-validation" role="form" novalidate="novalidate">
		<input type="hidden" name="valido" value="1" />
		<input type="hidden" name="pkNumEmpleado" id="pkNumEmpleado"/>
		<input type="hidden" value="{$idPensionSobreviviente}" name="idPensionSobreviviente"/>
		<input type="hidden" name="proceso"  id="proceso" value="{$proceso}">
		<div class="form-wizard-nav">
			<div class="progress"><div class="progress-bar progress-bar-primary"></div>
			</div>
			<ul class="nav nav-justified">
				<li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">	Información General</span></a></li>
				<li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Antecedentes de Servicio</span></a></li>
				<li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Relación de Sueldos</span></a></li>
				<li><a href="#step4" data-toggle="tab"><span class="step">4</span> <span class="title">Beneficiarios</span></a></li>
				<li><a href="#step5" data-toggle="tab"><span class="step">5</span> <span class="title">Detalle Pensión Sobreviviente</span></a></li>
                <li><a href="#step6" data-toggle="tab"><span class="step">6</span> <span class="title">Detalle Registro</span></a></li>
			</ul>
		</div><!--end .form-wizard-nav -->
		<!-- *********** PASO 1 ******************-->
		<div class="tab-content clearfix">
			<br/>
			<div class="tab-pane active" id="step1">
						<div class="col-md-6 col-sm-6">
							<div class="well clearfix">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-content">
											<input type="text" class="form-control input-sm dirty" id="nombreTrabajador" value="{if isset($formDB.nombre_completo)}{$formDB.nombre_completo}{/if}" disabled>
											<input type="hidden" name="form[txt][fk_rhb001_num_empleado]" id="fk_rhb001_num_empleado" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{/if}"  disabled>
											<label for="nombreTrabajador">Empleado</label>
										</div>
										<div class="input-group-btn">
											<button class="btn btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado">Buscar</button>
										</div>
									</div>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][ind_organismo]" id="ind_organismo" class="form-control input-sm dirty" value="{if isset($formDB.ind_organismo)}{$formDB.ind_organismo}{/if}"  disabled>
									<input type="hidden" name="form[int][fk_a001_num_organismo]" id="fk_a001_num_organismo" value="{if isset($formDB.fk_a001_num_organismo)}{$formDB.fk_a001_num_organismo}{/if}" disabled>
									<label for="ind_organismo"> Organismo</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][ind_dependencia]" id="ind_dependencia" class="form-control input-sm dirty" value="{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}" disabled>
									<input type="hidden" name="form[int][fk_a004_num_dependencia]" id="fk_a004_num_dependencia" value="{if isset($formDB.fk_a004_num_dependencia)}{$formDB.fk_a004_num_dependencia}{/if}" disabled>
									<label for="ind_dependencia"> Dependencia</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][ind_cargo]" id="ind_cargo" class="form-control input-sm dirty" value="{if isset($formDB.ind_cargo)}{$formDB.ind_cargo}{/if}" disabled>
									<input type="hidden" name="form[int][fk_rhc063_num_puestos]" id="fk_rhc063_num_puestos" value="{if isset($formDB.fk_rhc063_num_puestos)}{$formDB.fk_rhc063_num_puestos}{/if}" disabled>
									<label for="ind_cargo"> Cargo</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[int][num_ultimo_sueldo]" id="num_ultimo_sueldo" class="form-control input-sm dirty" value="{if isset($formDB.num_ultimo_sueldo)}{$formDB.num_ultimo_sueldo|number_format:2:",":"."}{/if}" disabled>
									<label class="control-label">Sueldo Actual</label>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="well clearfix">
								<div class="form-group">
									<input type="text" name="form[txt][cedula]" id="cedula" class="form-control input-sm dirty" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" disabled>
									<label class="control-label">Nro de documento</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][ind_sexo]" id="ind_sexo" class="form-control input-sm dirty" value="{if isset($formDB.sexo)}{$formDB.sexo}{/if}" disabled>
									<input type="hidden" name="form[int][sexo]" id="sexo" class="form-control input-sm dirty" value="{if isset($formDB.fk_a006_num_miscelaneo_detalle_sexo)}{$formDB.fk_a006_num_miscelaneo_detalle_sexo}{/if}" disabled>
									<label class="control-label">Sexo</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][nacimiento]" id="nacimiento" class="form-control input-sm dirty" value="{if isset($formDB.fec_nacimiento)}{$formDB.fec_nacimiento|date_format:"d-m-Y"}{/if}" disabled>
									<label class="control-label">Fecha de Nacimiento</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[int][num_edad]" id="num_edad" class="form-control input-sm dirty" value="{if isset($edad)}{$edad}{/if}" disabled>
									<label class="control-label">Edad</label>
								</div>
								<div class="form-group">
									<input type="text" name="form[txt][fec_fecha_ingreso]" id="fec_fecha_ingreso" class="form-control input-sm dirty" value="{if isset($fec_ingreso)}{$fec_ingreso|date_format:"d-m-Y"}{/if}" disabled>
									<label class="control-label">Fecha de Ingreso</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control input-sm" id="num_anio_servicio" name="form[int][num_anio_servicio]" value="{if isset($anio_servicio)}{$anio_servicio}{/if}" disabled>
									<label for="num_anio_servicio">Años de Servicio</label>
								</div>
							</div>
						</div>
					<div class="col-md-12 col-sm-12" id="requisitoVer">
						<!-- Aqui se carga el mensaje informativo si el empleado cumple o no los requisitos para optar a la jubilación-->

					</div>
			</div><!--end #step1 -->
			<!-- *********** PASO 2 ******************-->
			<div class="tab-pane" id="step2">

                <table id="datatable3" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th><i class="md-person"></i> Nº</th>
                        <th class="col-sm-3"><i class="md-person"></i> Organismo</th>
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Fecha de Ingreso</th>
                        <th><i class="glyphicon glyphicon-triangle-right"></i> Fecha de Egreso</th>
                        <th><i class="glyphicon glyphicon-cog"></i> Años</th>
                        <th><i class="glyphicon glyphicon-cog"></i> Meses</th>
                        <th><i class="glyphicon glyphicon-cog"></i> Dias</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=dat from=$antecedentes}
                        <tr>
                            <td>{$i}</td>
                            <td>{$dat.ind_organismo}</td>
                            <td>{$dat.fec_fecha_ingreso}</td>
                            <td>{$dat.fec_fecha_egreso}</td>
                            <td>{$dat.num_anio}</td>
                            <td>{$dat.num_meses}</td>
                            <td>{$dat.num_dias}</td>
                        </tr>
                        <input type="hidden" value="{$i++}"
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr >
                        <th></th>
                        <th></th>
                        <th></th>
                        <th bgcolor="#caf0ee">Antecedentes</th>
                        <th bgcolor="#caf0ee" id="antAnio">{$tiempoServicio.anio}</th>
                        <th bgcolor="#caf0ee" id="antMes">{$tiempoServicio.mes}</th>
                        <th bgcolor="#caf0ee" id="antDia">{$tiempoServicio.dia}</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th bgcolor="#caf0ee">En el Organismo</th>
                        <th bgcolor="#caf0ee" id="orgAnio">{$tiempoServicio.anioInst}</th>
                        <th bgcolor="#caf0ee" id="orgMes">{$tiempoServicio.mesInst}</th>
                        <th bgcolor="#caf0ee" id="orgDia">{$tiempoServicio.diaInst}</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th bgcolor="#caf0ee">Tiempo de Servicio</th>
                        <th bgcolor="#caf0ee" id="tAnio">{$tiempoServicio.anioTotal}</th>
                        <th bgcolor="#caf0ee" id="tMes">{$tiempoServicio.mesTotal}</th>
                        <th bgcolor="#caf0ee" id="tDia">{$tiempoServicio.diaTotal}</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>

			</div><!--end #step2 -->

			<!-- *********** PASO 3 ******************-->
			<div class="tab-pane" id="step3">

                <div class="col-md-12 col-sm-12">
                    {foreach item=i from=$conceptos}
                    <div class="col-md-6 col-sm-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-sm text-bold">{$i.ind_descripcion}</h3>
                            </div>
                            <div class="panel-body">
                                <table  id="datatable{$ct}" class="table table-striped table-hover nowrap">
                                    <thead>
                                    <tr align="center">
                                        <th><i class="md-person"></i> Nº</th>
                                        <th><i class="md-person"></i> Periodo</th>
                                        <th><i class="glyphicon glyphicon-triangle-right"></i> Monto</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    {if $i.pk_num_concepto==$SB}

                                        {foreach item=dat from=$relacionSB}
                                            <tr>
                                                <td>{$i1}</td>
                                                <td>{$dat.num_anio_periodo}-{$dat.ind_mes_periodo}</td>
                                                <td>{$dat.num_monto}</td>
                                            </tr>
                                            <input type="hidden" value="{$i1++}">
                                        {/foreach}

                                    {elseif $i.pk_num_concepto==$PR}

                                        {foreach item=dat from=$relacionPR}
                                            <tr>
                                                <td>{$i2}</td>
                                                <td>{$dat.num_anio_periodo}-{$dat.ind_mes_periodo}</td>
                                                <td>{$dat.num_monto}</td>
                                            </tr>
                                            <input type="hidden" value="{$i2++}">
                                        {/foreach}


                                    {else}

                                    {/if}


                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <input type="hidden" value="{$i.pk_num_concepto}" id="concepto{$ct}">
                        <input type="hidden" value="{$ct++}">
                    </div>


                    {/foreach}
                </div>
                <!-- montos -->
                <div class="col-md-12 col-sm-12">
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="form[int][sueldo]" id="sueldo" value="{if isset($sueldo)}{$sueldo|number_format:2:",":"."}{/if}" class="form-control input-sm dirty" disabled>
                            <label class="control-label">Sueldo</label>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="form[int][prima]" id="prima"  value="{if isset($prima)}{$prima|number_format:2:",":"."}{/if}" class="form-control input-sm dirty" disabled>
                            <label class="control-label">Primas</label>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="form[int][total]" id="total" value="{if isset($total)}{$total|number_format:2:",":"."}{/if}" class="form-control input-sm dirty" disabled>
                            <label class="control-label">Total</label>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="form[int][porcentaje]" id="porcentaje" value="{if isset($porcentaje)}{$porcentaje|number_format:2:",":"."}{/if}" class="form-control input-sm dirty" disabled>
                            <label class="control-label">Porcentaje</label>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="form[int][s_base]" id="s_base" value="{if isset($base)}{$base|number_format:2:",":"."}{/if}" class="form-control input-sm dirty" disabled>
                            <label class="control-label">Base</label>
                        </div>
                    </div>
                    <div class="col-sm-2 col-sm-2">
                        <div class="form-group">
                            <input type="text" name="form[int][jubilacion]" id="jubilacion" value="{if isset($jubilacion)}{$jubilacion|number_format:2:",":"."}{/if}" class="form-control input-sm dirty" disabled>
                            <label class="control-label">Jubilación</label>
                        </div>
                    </div>
                    <input type="hidden" name="form[int][num_anios_servicio_exceso]" id="num_anios_servicio_exceso">
                </div>

			</div><!--end #step3 -->

			<!-- *********** PASO 4 ******************-->
			<div class="tab-pane" id="step4">



                    <table id="datatableBeneficiarios" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="10" class="sort-numeric">Nº Cedula</th>
                            <th width="200" class="sort-alpha">Nombres y Apellidos</th>
                            <th class="sort-alpha">Parentesco</th>
                            <th>Fecha Nacimiento</th>
                            <th class="sort-alpha">Sexo</th>
                            <th class="sort-numeric">Edad</th>
                        </tr>
                        </thead>

                        <tbody>
                        {foreach item=dat from=$beneficiarios}
                            <tr>
                                <td>{$dat.ind_cedula_documento}</td>
                                <td>{$dat.nombre_completo}</td>
                                <td>{$dat.parentesco}</td>
                                <td>{$dat.fec_nacimiento}</td>
                                <td>{$dat.sexo}</td>
                                <td>{$dat.edad}</td>
                            </tr>
                        {/foreach}

                        </tbody>
                    </table>



			</div><!--end #step4 -->

			<!-- *********** PASO 5 ******************-->
			<div class="tab-pane" id="step5">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Planilla</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row contain-lg">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select id="fk_a006_num_miscelaneo_detalle_motivopension" name="form[int][fk_a006_num_miscelaneo_detalle_motivopension]" class="form-control input-sm">
                                        <option value="">Seleccione...</option>
                                        {foreach item=dat from=$MotivoPension}
                                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_motivopension)}
                                                {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_motivopension}
                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fk_a006_num_miscelaneo_detalle_motivopension">Motivo</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm bolivar1-mask" value="{if isset($formDB.num_monto_pension)}{$formDB.num_monto_pension|number_format:2:",":"."}{/if}" name="form[int][num_monto_pension]" id="num_monto_pension" required data-msg-required="Introduzca Monto de Pensión" >
                                    <label for="num_monto_pension">Monto Pensión</label>
                                </div>
                            </div>
                            <input type="hidden" name="form[int][fk_a006_num_miscelaneo_detalle_tipopension]" id="fk_a006_num_miscelaneo_detalle_tipopension" value="{if isset($formDB.fk_a006_num_miscelaneo_detalle_tipopension)}{$formDB.fk_a006_num_miscelaneo_detalle_tipopension}{/if}" >
                            <input type="hidden" name="form[txt][ind_detalle_tipopension]" id="ind_detalle_tipopension" value="{if isset($formDB.ind_detalle_tipopension)}{$formDB.ind_detalle_tipopension}{/if}" >
                            <input type="hidden" name="form[txt][ind_detalle_motivopension]" id="ind_detalle_motivopension" value="{if isset($formDB.ind_detalle_motivopension)}{$formDB.ind_detalle_motivopension}{/if}" >
                        </div>

                        <div class="row contain-lg">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control input-sm" required>
                                        <option value="">Seleccione...</option>
                                        {foreach item=dat from=$listadoTipoNomina}
                                            {if isset($formDB.fk_nmb001_num_tipo_nomina)}
                                                {if $dat.pk_num_tipo_nomina==$formDB.fk_nmb001_num_tipo_nomina}
                                                    <option selected value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fk_nmb001_num_tipo_nomina">Tipo Nomina</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select id="fk_a006_num_miscelaneo_detalle_tipotrab" name="form[int][fk_a006_num_miscelaneo_detalle_tipotrab]" class="form-control input-sm" required>
                                        <option value="">Seleccione...</option>
                                        {foreach item=dat from=$listadoTipoTrabajador}
                                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_tipotrab)}
                                                {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_tipotrab}
                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fk_a006_num_miscelaneo_detalle_tipotrab">Tipo Trabajador</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="ind_resolucion" name="form[txt][ind_resolucion]" value="{if isset($formDB.ind_resolucion)}{$formDB.ind_resolucion}{/if}" style="text-transform: uppercase" required>
                                    <label for="ind_resolucion">Nro. Resolución</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Detalle del Cese</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row contain-lg">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select id="num_situacion_trabajo" name="form[int][num_situacion_trabajo]" class="form-control input-sm" disabled required>
                                        {foreach key=key item=item from=$EstadoRegistro}
                                            {if isset($formDB.num_situacion_trabajo)}
                                                {if $key==$formDB.num_situacion_trabajo}
                                                    <option selected value="{$key}">{$item}</option>
                                                {else}
                                                    <option value="{$key}">{$item}</option>
                                                {/if}
                                            {else}
                                                <option value="{$key}">{$item}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="num_situacion_trabajo">Situacion de Trabajo</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <div class="input-group date" id="fecha_egreso">
                                        <div class="input-group-content">
                                            <input type="text" class="form-control input-sm" id="fec_fecha_egreso" name="form[txt][fec_fecha_egreso]" value="{if isset($formDB.fec_fecha_egreso)}{$formDB.fec_fecha_egreso|date_format:"d-m-Y"}{/if}" required>
                                            <label>Fecha Egreso</label>
                                        </div>
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <select id="fk_rhc032_num_motivo_cese" name="form[int][fk_rhc032_num_motivo_cese]" class="form-control input-sm" required>
                                        <option value="">Seleccione...</option>
                                        {foreach item=dat from=$listadoMotivoCese}
                                            {if isset($formDB.fk_rhc032_num_motivo_cese)}
                                                {if $dat.pk_num_motivo_cese==$formDB.fk_rhc032_num_motivo_cese}
                                                    <option selected value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_motivo_cese}">{$dat.ind_nombre_cese}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fk_rhc032_num_motivo_cese">Motivo Cese</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_egreso" name="form[txt][txt_observacion_egreso]" value="{if isset($formDB.txt_observacion_egreso)}{$formDB.txt_observacion_egreso}{/if}" style="text-transform: uppercase" required>
                                    <label for="txt_observacion_egreso">Explicación</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


			</div><!--end #step5 -->

            <!-- *********** PASO 6 ******************-->
            <div class="tab-pane" id="step6">

                {if isset($formDB.txt_estatus)}

                    {if $formDB.txt_estatus=='PR' || $formDB.txt_estatus=='CO' || $formDB.txt_estatus=='AP'}

                        <!-- PREPARADO -->
                        <div class="row contain-lg">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm"  id="preparado_por" name="form[txt][preparado_por]" value="{if isset($formDBoperacionesPR[0].nombre_registro)}{$formDBoperacionesPR[0].nombre_registro}{/if}" disabled>
                                    <input type="hidden" id="usuario_preparado_por">
                                    <label for="preparado_por">Preparado Por</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="fecha_preparado" name="form[txt][fecha_preparado]" value="{if isset($formDBoperacionesPR[0].fec_operacion)}{$formDBoperacionesPR[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                    <label for="fecha_preparado">Fecha</label>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="7" id="ind_periodo" name="form[txt][ind_periodo]" value="{if isset($formDB.ind_periodo)}{$formDB.ind_periodo}{else}{$periodo}{/if}" disabled>
                                    <label for="ind_periodo">Periodo</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                    <label for="txt_observacion_pre">Observaciones</label>
                                </div>
                            </div>
                        </div>
                        <!-- CONFORMADO -->
                        <div class="row contain-lg">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm"  id="conformado_por" name="form[txt][conformado_por]" value="{if isset($formDBoperacionesCO[0].nombre_registro)}{$formDBoperacionesCO[0].nombre_registro}{/if}" disabled>
                                    <input type="hidden" id="usuario_conformado_por">
                                    <label for="conformado_por">Conformado Por</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="fecha_conformado" name="form[txt][fecha_conformado]" value="{if isset($formDBoperacionesCO[0].fec_operacion)}{$formDBoperacionesCO[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                    <label for="fecha_conformado">Fecha</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_con" name="form[txt][txt_observacion_con]" value="{if isset($formDBoperacionesCO[0].txt_observaciones)}{$formDBoperacionesCO[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                    <label for="txt_observacion_con">Observaciones</label>
                                </div>
                            </div>
                        </div>
                        <!-- APROBADO -->
                        <div class="row contain-lg">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm"  id="aprobado_por" name="form[txt][aprobado_por]" value="{if isset($formDBoperacionesAP[0].nombre_registro)}{$formDBoperacionesAP[0].nombre_registro}{/if}" disabled>
                                    <input type="hidden" id="usuario_aprobado_por">
                                    <label for="aprobado_por">Aprobado Por</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="fecha_aprobado" name="form[txt][fecha_aprobado]" value="{if isset($formDBoperacionesAP[0].fec_operacion)}{$formDBoperacionesAP[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                    <label for="fecha_aprobado">Fecha</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apro" name="form[txt][txt_observacion_apro]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                    <label for="txt_observacion_apro">Observaciones</label>
                                </div>
                            </div>
                        </div>

                    {else}

                        <!-- ANULADO -->
                        <div class="row contain-lg">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm"  id="anulado_por" name="form[txt][anulado_por]" value="{if isset($formDBoperacionesAN[0].nombre_registro)}{$formDBoperacionesAN[0].nombre_registro}{/if}" disabled>
                                    <input type="hidden" id="usuario_anulado_por">
                                    <label for="anulado_por">Anulado Por</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" id="fecha_anulado" name="form[txt][fecha_anulado]" value="{if isset($formDBoperacionesAN[0].fec_operacion)}{$formDBoperacionesAN[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                    <label for="fecha_anulado">Fecha</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_anulado" name="form[txt][txt_observacion_anulado]" value="{if isset($formDBoperacionesAN[0].txt_observaciones)}{$formDBoperacionesAN[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                    <label for="txt_observacion_anulado">Observaciones</label>
                                </div>
                            </div>
                        </div>

                    {/if}

                {else}
                    <!-- PREPARADO -->
                    <div class="row contain-lg">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="preparado_por" name="form[txt][preparado_por]" value="{if isset($formDBoperacionesPR[0].nombre_registro)}{$formDBoperacionesPR[0].nombre_registro}{/if}" disabled>
                                <input type="hidden" id="usuario_preparado_por">
                                <label for="preparado_por">Preparado Por</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="fecha_preparado" name="form[txt][fecha_preparado]" value="{if isset($formDBoperacionesPR[0].fec_operacion)}{$formDBoperacionesPR[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                <label for="fecha_preparado">Fecha</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" maxlength="7" id="ind_periodo" name="form[txt][ind_periodo]" value="{if isset($formDB.ind_periodo)}{$formDB.ind_periodo}{else}{$periodo}{/if}" disabled>
                                <label for="ind_periodo">Periodo</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                <label for="txt_observacion_pre">Observaciones</label>
                            </div>
                        </div>
                    </div>
                    <!-- CONFORMADO -->
                    <div class="row contain-lg">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="conformado_por" name="form[txt][conformado_por]" value="{if isset($formDBoperacionesCO[0].nombre_registro)}{$formDBoperacionesCO[0].nombre_registro}{/if}" disabled>
                                <input type="hidden" id="usuario_conformado_por">
                                <label for="conformado_por">Conformado Por</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="fecha_conformado" name="form[txt][fecha_conformado]" value="{if isset($formDBoperacionesCO[0].fec_operacion)}{$formDBoperacionesCO[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                <label for="fecha_conformado">Fecha</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_con" name="form[txt][txt_observacion_con]" value="{if isset($formDBoperacionesCO[0].txt_observaciones)}{$formDBoperacionesCO[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                <label for="txt_observacion_con">Observaciones</label>
                            </div>
                        </div>
                    </div>
                    <!-- APROBADO -->
                    <div class="row contain-lg">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="aprobado_por" name="form[txt][aprobado_por]" value="{if isset($formDBoperacionesAP[0].nombre_registro)}{$formDBoperacionesAP[0].nombre_registro}{/if}" disabled>
                                <input type="hidden" id="usuario_aprobado_por">
                                <label for="aprobado_por">Aprobado Por</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="fecha_aprobado" name="form[txt][fecha_aprobado]" value="{if isset($formDBoperacionesAP[0].fec_operacion)}{$formDBoperacionesAP[0].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                <label for="fecha_aprobado">Fecha</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apro" name="form[txt][txt_observacion_apro]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                <label for="txt_observacion_apro">Observaciones</label>
                            </div>
                        </div>
                    </div>
                {/if}

                {if $proceso=='Anular'}

                    <!-- ANULADO -->
                    <div class="row contain-lg">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="anulado_por" name="form[txt][anulado_por]" value="{if isset($formDBoperaciones[2].nombre_registro)}{$formDBoperaciones[2].nombre_registro}{/if}" disabled>
                                <input type="hidden" id="usuario_anulado_por">
                                <label for="anulado_por">Anulador Por</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="fecha_anulado" name="form[txt][fecha_anulado]" value="{if isset($formDBoperaciones[2].fec_operacion)}{$formDBoperaciones[2].fec_operacion|date_format:"d-m-Y"}{/if}" disabled>
                                <label for="fecha_anulado">Fecha</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_anulado" name="form[txt][txt_observacion_anulado]" value="{if isset($formDBoperaciones[2].txt_observaciones)}{$formDBoperaciones[2].txt_observaciones}{/if}" style="text-transform: uppercase" disabled>
                                <label for="txt_observacion_anulado">Observaciones</label>
                            </div>
                        </div>
                    </div>

                {/if}
                <input type="hidden" id="fec_fecha" name="form[txt][fec_fecha]" value="{if isset($formDB.fec_fecha)}{$formDB.fec_fecha|date_format:"d-m-Y"}{else}{$fecha}{/if}">
                <div class="row contain-lg">
                    <!-- botones para importar empleados, botones para insertar uno a uso y eliminar -->
                    <br>
                    <div class="row text-center">
                        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="botonGuardar" style="display: none;">Procesar </button>
                        <input type="hidden" id="motivo" name="form[txt][motivo]" value="{if isset($T)}{$T}{/if}">
                    </div>
                    <br>
                </div>


            </div><!--end #step6 -->


		</div><!--end .tab-content -->
	</form>
	<ul class="pager wizard">
		<li class="previous first"><a class="btn-raised" href="javascript:void(0);"> Primero </a></li>
		<li class="previous"><a class="btn-raised" href="javascript:void(0);"> Anterior </a></li>
		<li class="next last"><a class="btn-raised" href="javascript:void(0);"> Ultimo </a></li>
		<li class="next"><a class="btn-raised" href="javascript:void(0);"> Siguiente </a></li>
	</ul>
</div><!--end #rootwizard -->
<script type="text/javascript">
	$("#fecha_egreso").datepicker({
		todayHighlight: true,
		format:'dd-mm-yyyy',
		autoclose: true,
		language:'es'
	});

	$(".form-wizard-nav").on('click', function(){ //enables click event
		return false;
	});

	var $url = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/ConsultarEmpleadoMET';
	$('#buscarEmpleado').click(function(){
		$('#formModalLabel2').html($(this).attr('titulo'));
		$.post($url, { proceso: 'Nuevo', accion:'PENSIONADO_SOBREVIVIENTE' },function($dato){
			$('#ContenidoModal2').html($dato);
		});
	});
	$('#modalAncho').css( "width", "85%" );

	$('#modalAncho2').css( "width", "75%" );


	//si todo es correcto y cumple con los requimientos de validacion Envio
	$.validator.setDefaults({
		submitHandler: function() {

			//alert("Envio el formulario");
			var formulario = $( "#formAjax" );

			var disabled = formulario.find(':input:disabled').removeAttr('disabled');

			var datos = formulario.serialize();

			if($("#proceso").val()=='Nuevo'){
				var url = '{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/RegistrarMET';
				$.post(url, datos ,function(dato){

					if(dato['status']=='error'){
						{*para usuarios normales*}
						//swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
						{*para usuarios programadores*}
						swal("Error",dato['detalleERROR'], "error");
					}

					if(dato['status']=='registrar'){

						swal({
							title: 'Exito',
							text: 'Operación Realizada con Exito!!!.',
							type: "success",
							showCancelButton: false,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: 'Aceptar',
							closeOnConfirm: true
						}, function(){
							$(location).attr('href','{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL');
						});

						$(document.getElementById('cerrarModal')).click();
						$(document.getElementById('ContenidoModal')).html('');


					}
				},'json');
			}
			if($("#proceso").val()=='Modificar'){
				var url = '{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ModificarMET';
				$.post(url, datos ,function(dato){
					if(dato['status']=='error'){
						{*para usuarios normales*}
						swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
						{*para usuarios programadores*}
						//swal("Error",respuesta['detalleERROR'], "error");
					}
					if(dato['status']=='modificar'){

						swal("Exito","Operación Realizada con Exito!!!.", "success");
						$(document.getElementById('cerrarModal')).click();
						$(document.getElementById('ContenidoModal')).html('');

					}
				},'json');
			}
			if($("#proceso").val()=='Conformar'){

				var url = '{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ConformarMET';
				swal({
					title: 'Conformar',
					text: 'Esta seguro que desea Conformar esta Pensión?',
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: 'Si, Conformar',
					closeOnConfirm: false
				}, function(){
					$.post(url, datos ,function(dato){
						if(dato['status']=='conformado') {

                            swal({
                                title: 'Exito',
                                text: 'Operación Realizada con Exito!!!.',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: 'Aceptar',
                                closeOnConfirm: true
                            }, function () {
                                $(location).attr('href', '{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ListadoConformarMET');
                            });
                        }
					},'json');
				});
			}
			if($("#proceso").val()=='Aprobar'){
				var url = '{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/AprobarMET';

				swal({
					title: 'Aprobar',
					text: 'Esta seguro que desea Aprobar esta Pensión?',
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: 'Si, Aprobar',
					closeOnConfirm: false
				}, function(){
					$.post(url, datos ,function(dato){
						if(dato['status']=='aprobado'){

							swal({
								title: 'Exito',
								text: 'Operación Realizada con Exito!!!.',
								type: "success",
								showCancelButton: false,
								confirmButtonColor: "#DD6B55",
								confirmButtonText: 'Aceptar',
								closeOnConfirm: true
							}, function(){
								$(location).attr('href','{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/ListadoAprobarMET');
							});

							$(document.getElementById('cerrarModal')).click();
							$(document.getElementById('ContenidoModal')).html('');


						}
					},'json');
				});
			}

			if($("#proceso").val()=='Anular'){
				var url = '{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL/AnularMET';
				swal({
					title: 'Anular',
					text: 'Esta seguro que desea anular este registro? \n La operación sera irreversible!',
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: 'Si, Anular',
					closeOnConfirm: false
				}, function(){
					$.post(url, datos ,function(dato){
						if(dato['status']=='anular'){

							swal({
								title: 'Exito',
								text: 'Operación Realizada con Exito!!!.',
								type: "success",
								showCancelButton: false,
								confirmButtonColor: "#DD6B55",
								confirmButtonText: 'Aceptar',
								closeOnConfirm: true
							}, function(){
                                $(document.getElementById('cerrarModal')).click();
                                $(document.getElementById('ContenidoModal')).html('');
								$(location).attr('href','{$_Parametros.url}modRH/procesos/pensionesSobrevivienteCONTROL');
							});

						}
					},'json');

				});
			}

		}
	});





    {*INICIALIZANDO TABLAS*}
    //inicializo el datatbales
    $('#datatable3').DataTable({
        "dom": 'lCfrtip',
        "order": [],
        "colVis": {
            "buttonText": "Columnas",
            "overlayFade": 0,
            "align": "right"
        },
        "language": {
            "lengthMenu": 'Mostrar _MENU_',
            "search": '<i class="fa fa-search"></i>',
            "paginate": {
                "previous": '<i class="fa fa-angle-left"></i>',
                "next": '<i class="fa fa-angle-right"></i>'
            }
        },
        "paging":false
    });

    if ( $.fn.dataTable.isDataTable( '#datatable90' ) ) {
        table = $('#datatable90').DataTable();
    }
    else {
        table = $('#datatable90').DataTable( {
            paging: false,
            searching: false,
        } );
    }

    if ( $.fn.dataTable.isDataTable( '#datatable91' ) ) {
        table = $('#datatable91').DataTable();
    }
    else {
        table = $('#datatable91').DataTable( {
            paging: false,
            searching: false,
        } );
    }

    if ( $.fn.dataTable.isDataTable( '#datatableBeneficiarios' ) ) {
        table = $('#datatableBeneficiarios').DataTable();
    }
    else {
        table = $('#datatableBeneficiarios').DataTable( {
            paging: false,
            searching: false,
        } );
    }


	var hoy = new Date();
	var fecha = hoy.getDate()+"-"+(hoy.getMonth()+1)+"-"+hoy.getFullYear();

	{*VER DATOS EN EL FORMULARIO*}
	if($("#proceso").val()=='Ver'){
		$(".form-control").attr("disabled","disabled");
		$("#buscarEmpleado").attr("disabled","disabled");
		$("#accion").hide();
	}
	{*PROCESO DE MODIFICAR*}
	if($("#proceso").val()=='Modificar'){

		{*desabilitado todo el formulario*}
		$(".form-control").attr("disabled","disabled");

		{*cambio nombre al boton*}
		$("#accion").html('Modificar');
		$("#txt_observacion_pre").removeAttr('disabled');
		$("#num_monto_pension").removeAttr('disabled');
		$("#buscarEmpleado").attr("disabled","disabled");
        $("#fk_nmb001_num_tipo_nomina").removeAttr('disabled');
        $("#fk_a006_num_miscelaneo_detalle_tipotrab").removeAttr('disabled');
        $("#ind_resolucion").removeAttr('disabled');
        $("#num_situacion_trabajo").removeAttr('disabled');
        $("#fec_fecha_egreso").removeAttr('disabled');
        $("#fk_rhc032_num_motivo_cese").removeAttr('disabled');
        $("#txt_observacion_egreso").removeAttr('disabled');

		{*habilito el boton guardar y le cambio el nombre*}
		$('#botonGuardar').show();
		$("#botonGuardar").html('Modificar');
	}
	{*PROCESO DE CONFORMAR*}
	if($("#proceso").val()=='Conformar'){
		$(".form-control").attr("disabled","disabled");
		$("#txt_observacion_con").removeAttr('disabled');
		$("#fecha_conformado").val(fecha);
		$("#conformado_por").val('{$nombreUsuario}');
		$("#usuario_conformado_por").val({$idUsuario});
		$("#buscarEmpleado").attr("disabled","disabled");
		{*cambio nombre al boton*}
		$('#botonGuardar').show();
		$("#botonGuardar").html('Conformar');
	}
	{*PROCESO DE APROBAR*}
	if($("#proceso").val()=='Aprobar'){
		$(".form-control").attr("disabled","disabled");
		$("#txt_observacion_apro").removeAttr('disabled');
		$("#fecha_aprobado").val(fecha);
		$("#aprobado_por").val('{$nombreUsuario}');
		$("#usuario_aprobado_por").val({$idUsuario});
		$("#buscarEmpleado").attr("disabled","disabled");
		{*cambio nombre al boton*}
		$('#botonGuardar').show();
		$("#botonGuardar").html('Aprobar')


	}
	{*ANULAR PROCESO*}
	if($("#proceso").val()=='Anular'){
		$(".form-control").attr("disabled","disabled");
		$('#botonGuardar').show();
		$("#botonGuardar").html('Anular');
		$("#txt_observacion_anulado").removeAttr('disabled');
		$("#fecha_anulado").val(fecha);
		$("#anulado_por").val('{$nombreUsuario}');
		$("#usuario_anulado_por").val({$idUsuario});
	}



</script>