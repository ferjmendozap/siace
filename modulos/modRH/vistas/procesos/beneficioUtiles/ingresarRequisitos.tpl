<div class="card card-underline">

    <!------ TITULO ----------->
    <div class="card-head">
        <header class="text-primary text-xl">{$proceso} Periodo Bono Alimentación</header>
    </div>

    <div class="card-body">

        <div class="row">
            <!-- WIZARD BONO DE ELIMENTACION -->
            <div id="rootwizard2" class="form-wizard form-wizard-horizontal">

                <!-- PESTAÑAS WIZAARD -->
                <div class="form-wizard-nav">
                    <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                    <ul class="nav nav-justified">
                        <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">Información General</span></a></li>
                        <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Empleados</span></a></li>
                    </ul>
                </div>

                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >


                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idBonoAlimentacion}" name="idBonoAlimentacion"/>
                    <input type="hidden" name="form[txt][id_empleados]"  id="id_empleados">
                    <input type="hidden" name="proceso"  id="proceso" value="{$proceso}">

                    <!-- CONTENIDO -->
                    <div class="tab-content clearfix">

                        <!-- INFORMACIÓN GENERAL -->
                        <div class="tab-pane active" id="step1">
                            <h4 class="text-primary text-center text-lg">Información del Periodo</h4>

                            <div class="well clearfix">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" required data-msg-required="Seleccione Organismo">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoOrganismos}
                                                    {if isset($formDB.fk_a001_num_organismo)}
                                                        {if $dat.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                                            <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a001_num_organismo">Organismo</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="500" id="ind_descripcion" name="form[txt][ind_descripcion]" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" required data-msg-required="Indique Descripción" style="text-transform: uppercase">
                                            <label for="ind_descripcion">Descripción</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control input-sm" required data-msg-required="Seleccione Tipo de Nomina">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoTipoNomina}
                                                    {if isset($formDB.fk_nmb001_num_tipo_nomina)}
                                                        {if $dat.pk_num_tipo_nomina==$formDB.fk_nmb001_num_tipo_nomina}
                                                            <option selected value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_nmb001_num_tipo_nomina">Tipo Nomina</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="7" id="ind_periodo" name="form[txt][ind_periodo]" value="{if isset($formDB.ind_periodo)}{$formDB.ind_periodo}{else}{$periodo}{/if}" required data-msg-required="Indique Periodo">
                                            <label for="ind_periodo">Periodo</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group control-width-normal">
                                            <div class="input-group date" id="fecha_ini">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control input-sm" id="fec_inicio_periodo" name="form[txt][fec_inicio_periodo]" value="{if isset($formDB.fec_inicio_periodo)}{$formDB.fec_inicio_periodo|date_format:"d-m-Y"}{else}{$fecha_ini}{/if}" required data-msg-required="Indique Fecha Inicio">
                                                    <label>Fecha Inicio</label>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div><!--end .form-group -->
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group control-width-normal">
                                            <div class="input-group date" id="fecha_fin">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control input-sm" id="fec_fin_periodo" name="form[txt][fec_fin_periodo]" value="{if isset($formDB.fec_fin_periodo)}{$formDB.fec_fin_periodo|date_format:"d-m-Y"}{else}{$fecha_fin}{/if}" required data-msg-required="Indique Fecha Fin">
                                                    <label>Fecha Fin</label>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div><!--end .form-group -->
                                    </div>
                                </div>

                            </div>

                            <h4 class="text-primary text-center text-lg">Detalle del Periodo</h4>
                            <div class="well clearfix">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_dias_periodo" name="form[int][num_dias_periodo]" value="{if isset($formDB.num_dias_perido)}{$formDB.num_dias_perido}{/if}" readonly>
                                            <label for="num_dias_periodo">Dia del Periodo</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="num_dias_pago" name="form[int][num_dias_pago]" value="{if isset($formDB.num_dias_pago)}{$formDB.num_dias_pago}{/if}" readonly>
                                            <label for="num_dias_pago">Dias de Pago</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_total_feriados" name="form[int][num_total_feriados]" value="{if isset($formDB.num_total_feriados)}{$formDB.num_total_feriados}{/if}" readonly>
                                            <label for="num_total_feriados">Dias Feriado</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_valor_diario" name="form[int][num_valor_diario]" value="{if isset($formDB.num_valor_diario)}{$formDB.num_valor_diario}{/if}" readonly>
                                            <label for="num_valor_diario">Valor Diario</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="fec_horas_diarias" name="form[txt][fec_horas_diarias]" value="{if isset($formDB.fec_horas_diarias)}{$formDB.fec_horas_diarias}{/if}" readonly>
                                            <label for="fec_horas_diarias">Horas Diarias</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="fec_horas_semanales" name="form[txt][fec_horas_semanales]" value="{if isset($formDB.fec_horas_semanales)}{$formDB.fec_horas_semanales}{/if}" readonly>
                                            <label for="fec_horas_semanales">Horas Semanales</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_valor_semanal" name="form[txt][num_valor_semanal]" value="{if isset($formDB.num_valor_semanal)}{$formDB.num_valor_semanal|number_format:2:",":"."}{/if}" readonly>
                                            <label for="num_valor_semanal">Valor Semanal</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_valor_mes" name="form[int][num_valor_mes]" value="{if isset($formDB.num_valor_mes)}{$formDB.num_valor_mes}{/if}" readonly>
                                            <label for="num_valor_mes">Valor Mensual</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row text-center">
                                    <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="regresarListado">Volver al Listado</button>
                                </div>
                            </div>
                        </div>

                        <!-- EMPLEADOS -->
                        <div class="tab-pane" id="step2">

                            <!-- botones para importar empleados, botones para insertar uno a uso y eliminar -->
                            <br>
                            <div class="row text-center">
                                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="importarEmpleados">Importar Empleados</button>
                                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="insertarEmpleado" descripcion="Agregar Empleado" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#formModal"
                                        titulo="Agregar Empleado">Insertar Empleado</button>
                                <button type="submit" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" id="guardarPeriodo"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
                                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="regresarListado2">Volver al Listado</button>
                            </div>
                            <br>

                            <div class="col-lg-12">

                                    <table id="datatable1" class="table table-striped no-margin table-condensed" width="100%">
                                        <thead>
                                        <tr>
                                            <th>Emp.</th>
                                            <th>Nro. Documento</th>
                                            <th style="width: 200px;">Nombre Completo</th>
                                            <th style="width: 200px;">Cargo</th>
                                            <th style="width: 200px;">Dependencia</th>
                                            <th>Eliminar</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>

                            </div>
                        </div>
                    </div>

                    <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                    <ul class="pager wizard">
                        <li class="previous first" id="primero"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                        <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                        <li class="next last" id="ultimo"><a class="ink-reaction btn-raised" href="javascript:void(0);">Ultimo</a></li>
                        <li class="next" id="siguiente"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                    </ul>

                </form>


            </div>
        </div>

    </div>

</div>


<script type="text/javascript">

    //inicializar el datepicker
    $("#fecha_ini").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $("#fecha_fin").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            var num_filas = $('#datatable1 >tbody >tr').length;
            var filas = $('#datatable1 >tbody >tr');

            if(num_filas == 1 &&  filas[0].cells[0].innerHTML == "No Hay Registros Disponibles"){
                swal({
                    title: '',
                    text: 'Agregue los Empleados',
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Aceptar',
                    closeOnConfirm: true
                });
            }else{

                var url= '{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/BonoAlimentacionMET';
                $.post(url,datos,function(respuesta){

                    if(respuesta['status']=='OK'){

                        swal("Bono Alimentación Registrado", "Guardado Satisfactoriamente", "success");

                        $('#formAjax').each(function () { {*resetear los campos del formulario*}
                            this.reset();
                        });

                        var limpiarTabla = $('#datatable1').DataTable();
                        {*limpiar la tabla*}
                        limpiarTabla.clear().draw();

                        $("#anterior").click();

                        //$('#formAjax a[href="#step1"]').tab('show');
                        {*reiniciar el wizard al primer tab*}

                    }else if(respuesta['status']=='OK_mod'){

                        //swal("Bono Alimentación Modificado", "Guardado Satisfactoriamente", "success");
                        swal({
                            title: 'Bono Alimentación Modificado',
                            text: 'Guardado Satisfactoriamente',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Aceptar",
                            closeOnConfirm: true
                        }, function () {

                            $(location).attr('href','{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL')

                        });

                    }else if(respuesta['status']=='error'){
                        {*para usuarios normales*}
                        //swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        swal("Error",respuesta['detalleERROR'], "error");

                    }
                },'json');
            }
        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //Script para forzar el avance del wizard por el botón suigiente.
        $(".form-wizard-nav").on('click', function () {
            return false;
        });

        var proceso = $("#proceso").val();

        if (proceso == 'Nuevo') {
            $("#regresarListado").hide();
        }

        if (proceso == 'Ver') {
            $(".form-control").attr("disabled", "disabled");
            $("#fk_a001_num_organismo").removeAttr("readonly");
            $("#fk_nmb001_num_tipo_nomina").removeAttr("readonly");
            $("#fk_a001_num_organismo").attr("disabled", "disabled");
            $("#fk_nmb001_num_tipo_nomina").attr("disabled", "disabled");
            //oculto botones
            $("#importarEmpleados").hide();
            $("#insertarEmpleado").hide();
            $("#guardarPeriodo").hide();
            $("#regresarListado").show();

            //listo lo empleados
            $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/MostrarBonoAlimentacionEmpleadosMET', { bono_alimentacion: {$idBonoAlimentacion} }, function (dato) {


                var tabla_agregar = $('#datatable1').DataTable();

                tabla_agregar.clear().draw();
                {*limpia la tabla (tbody)*}

                var cad_id_empleados = '';

                for (var i = 0; i < dato.length; i++) {

                    var secuencia = i + 1;

                    tabla_agregar.row.add([{*añade las filas al tbody permitiendo aplicar la paginación automaticamente*}
                        dato[i].pk_num_empleado,
                        dato[i].ind_cedula_documento,
                        dato[i].nombre_completo,
                        dato[i].ind_descripcion_cargo,
                        dato[i].ind_dependencia,
                        ''


                    ]).draw()
                            .nodes(){*Agregar una clase a cada fila añadida*}
                            .to$()
                            .addClass();

                    cad_id_empleados = cad_id_empleados + dato[i].pk_num_empleado + '#';

                }

                $('#id_empleados').val(cad_id_empleados);

            }, 'json');

        }

        if (proceso == 'Modificar') {
            $(".form-control").attr("readonly", "readonly");
            $("#ind_descripcion").removeAttr("readonly");
            $("#fec_inicio_periodo").removeAttr("readonly");
            $("#fec_fin_periodo").removeAttr("readonly");
            $("#ind_periodo").removeAttr("readonly");
            //botones
            $("#regresarListado").show();

            //listo lo empleados
            $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/MostrarBonoAlimentacionEmpleadosMET', { bono_alimentacion: {$idBonoAlimentacion} }, function (dato) {


                var tabla_agregar = $('#datatable1').DataTable();

                tabla_agregar.clear().draw();
                {*limpia la tabla (tbody)*}

                var cad_id_empleados = '';

                for (var i = 0; i < dato.length; i++) {

                    var secuencia = i + 1;

                    tabla_agregar.row.add([{*añade las filas al tbody permitiendo aplicar la paginación automaticamente*}
                        dato[i].pk_num_empleado,
                        dato[i].ind_cedula_documento,
                        dato[i].nombre_completo,
                        dato[i].ind_descripcion_cargo,
                        dato[i].ind_dependencia,
                        '<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'


                    ]).draw()
                            .nodes(){*Agregar una clase a cada fila añadida*}
                            .to$()
                            .addClass();

                    cad_id_empleados = cad_id_empleados + dato[i].pk_num_empleado + '#';

                }

                $('#id_empleados').val(cad_id_empleados);

            }, 'json');


        }


        //accion boton regresar listado
        var $url = '{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/';
        $("#regresarListado").click(function () {
            $(location).attr('href', $url);
        });
        $("#regresarListado2").click(function () {
            $(location).attr('href', $url);
        });


        $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CarlularDiasPeriodoBonoMET', {
            fecha_ini: $("#fec_inicio_periodo").val(),
            fecha_fin: $("#fec_fin_periodo").val()
        }, function (dato) {

            $("#num_dias_periodo").val(dato['num_dias_periodo']);
            $("#num_dias_pago").val(dato['num_dias_pago']);
            $("#num_total_feriados").val(dato['num_total_feriados']);
            $("#num_valor_diario").val(dato['num_valor_diario']);
            $("#num_valor_mes").val(dato['num_valor_mes']);

        }, 'json');

        $("#fec_inicio_periodo").change(function () {

            $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CarlularDiasPeriodoBonoMET', {
                fecha_ini: $("#fec_inicio_periodo").val(),
                fecha_fin: $("#fec_fin_periodo").val()
            }, function (dato) {

                $("#num_dias_periodo").val(dato['num_dias_periodo']);
                $("#num_dias_pago").val(dato['num_dias_pago']);
                $("#num_total_feriados").val(dato['num_total_feriados']);
                $("#num_valor_diario").val(dato['num_valor_diario']);
                $("#num_valor_mes").val(dato['num_valor_mes']);

            }, 'json');

        });

        $("#fec_fin_periodo").change(function () {

            $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CarlularDiasPeriodoBonoMET', {
                fecha_ini: $("#fec_inicio_periodo").val(),
                fecha_fin: $("#fec_fin_periodo").val()
            }, function (dato) {

                $("#num_dias_periodo").val(dato['num_dias_periodo']);
                $("#num_dias_pago").val(dato['num_dias_pago']);
                $("#num_total_feriados").val(dato['num_total_feriados']);
                $("#num_valor_diario").val(dato['num_valor_diario']);
                $("#num_valor_mes").val(dato['num_valor_mes']);

            }, 'json');

        });

        var tabla_agregar = $('#datatable1').DataTable();

        {*BOTON IMPORTAR EMPLEADOS*}
        $("#importarEmpleados").click(function () {

            {*AVISO PARA CONFIRMAR EL PROCEDIMIENTO*}
            swal({
                title: 'Estas Seguro?',
                text: 'Estas seguro que desea importar todos los empleados?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Seguro",
                closeOnConfirm: true
            }, function () {

                $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ImportarEmpleadosMET', {
                    tipo_nomina: $("#fk_nmb001_num_tipo_nomina").val(),
                    organismo: $("#fk_a001_num_organismo").val()
                }, function (dato) {


                    tabla_agregar.clear().draw();
                    {*limpia la tabla (tbody)*}

                    var cad_id_empleados = '';

                    for (var i = 0; i < dato.length; i++) {

                        var secuencia = i + 1;

                        tabla_agregar.row.add([{*añade las filas al tbody permitiendo aplicar la paginación automaticamente*}
                            dato[i].pk_num_empleado,
                            dato[i].ind_cedula_documento,
                            dato[i].nombre_completo,
                            dato[i].ind_descripcion_cargo,
                            dato[i].ind_dependencia,
                            '<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'


                        ]).draw()
                                .nodes(){*Agregar una clase a cada fila añadida*}
                                .to$()
                                .addClass();

                        cad_id_empleados = cad_id_empleados + dato[i].pk_num_empleado + '#';

                    }

                    $('#id_empleados').val(cad_id_empleados);


                }, 'json');

            });

        });

        {* ************************************************ *}
        $('#datatable1 tbody').on('click', '.eliminar', function () {

            var obj = this;

            swal({
                title: 'Estas Seguro?',
                text: 'Estas seguro que desea descartar al Empleado(a)',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Descartar",
                closeOnConfirm: false
            }, function () {

                tabla_agregar.row($(obj).parents('tr')).remove().draw();

                {*Elimina la fila en cuestion*}
                swal("Descartado!", "Descartado de la lista el Empleado", "success");


                {*verifico la tablas donde estan los empleados para volver armar el arreglo de empleados*}
                var num_filas = $('#datatable1 >tbody >tr').length;
                var filas = $('#datatable1 >tbody >tr'); {*trae las filas del tbody*}


                var cad_id_empleados = '';
                $('#id_empleados').val('');

                for(var i=0; i<num_filas; i++) {

                    {*voy armando arreglo de empleados*}

                    cad_id_empleados = cad_id_empleados + filas[i].cells[0].innerHTML + '#';

                }
                {*asigno arreglo de empleados*}
                $('#id_empleados').val(cad_id_empleados);






            });


        });
        {* **************************************************** *}


        {*para agregar un empleado a la lista de nomina de bono alimentacion*}
        var $urlAgregar='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/AgregarBeneficiariosMET';
        $('#insertarEmpleado').click(function(){
            $('#modalAncho').css( "width", "80%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");

            $.post($urlAgregar,{ organismo : $('#fk_a001_num_organismo').val() , tipo_nomina: $('#fk_nmb001_num_tipo_nomina').val()  },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //**************************************



    });







</script>