
    <section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Beneficios</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-head">
                            
                            <div class="tools">
                                <div class="btn-group">
                                    <a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="col-sm-1" width="170">Nro </th>
                                    <th class="col-sm-1">Tipo</th>
                                    <th>Funcionario</th>
                                    <th>Beneficio</th>
                                    <th class="col-sm-1">Monto</th>
                                    <th class="col-sm-1"> Estado </th>
                                    <th class="col-sm-1"> Bitácora </th>
                                    <th class="col-sm-1"> Ver </th>
                                    <th class="col-sm-1">Modificar</th>
                                    <th class="col-sm-1">PDF</th>
                                    <th class="col-sm-1">Anular</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=b from=$Beneficio}

                                    <tr id="idBeneficio{$b.pk_beneficio_medico}">
                                        <td>{$b.pk_beneficio_medico}</td>
                                        <td>{$b.ind_nombre_detalle}</td>
                                        <td>{$b.ind_nombre1} {$b.ind_apellido1}</td>
                                        <td>{$b.ind_descripcion_especifica}</td>
                                        <td>{$b.num_monto|number_format:2:",":"."}</td>
                                        <td>{$b.estado_nombre}</td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-02-04-05-B',$_Parametros.perfil)}
                                                <button class="bitacora logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Ver Bitácora de Movimientos" descipcion="El Usuario a Visualizado la Bitácora de Movimientos" idBitacora="{$b.pk_beneficio_medico}" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal">
                                                    <i class="md md-perm-identity" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-02-04-06-V',$_Parametros.perfil)}
                                                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idVer="{$b.pk_beneficio_medico}"
                                                        descipcion="Se ha visualizado un Beneficio Personal" title="Ver Beneficio HCM" titulo="Ver Beneficio Personal">
                                                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                        <td align="center" width="100">
                                            {if in_array('RH-01-02-04-07-M',$_Parametros.perfil)}
                                            {if ( $b.num_estado=='1')}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idModificar="{$b.pk_beneficio_medico}"
                                                        descipcion="El Usuario a Modificado un Beneficio Personal" title="Editar Beneficio HCM" titulo="Modificar Beneficio Personal">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            {/if}
                                        </td>
                                        <td align="center" width="100">

                                                <button class="HcmPdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal"   data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idPdf="{$b.pk_beneficio_medico}"
                                                        descipcion="El Usuario a visualizado planila HCM" title="Planilla HCM" titulo="Ver Planilla">
                                                    <i class="md md-attach-file" style="color: #ffffff;"></i>
                                                </button>

                                        </td>
                                        <td>
                                            {if in_array('RH-01-02-04-08-A',$_Parametros.perfil)}
                                            {if ( $b.num_estado<='2')}
                                                <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficio="{$b.pk_beneficio_medico}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado Beneficio " title="Eliminar Beneficio HCM" titulo="Estas Seguro?" mensaje="Estas seguro que desea anular el Beneficio !!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                                <tfoot>
                                    <th colspan="11">
                                        {if in_array('RH-01-02-04-04-N',$_Parametros.perfil)}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="el Usuario a registrado una Nueva Asignación de Beneficio"  titulo="Nuevo Asignación de Beneficio" id="nuevo" >
                                                Nuevo Beneficio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                            </button>
                                         {/if}
                                    </th>
                                </tfoot>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!-- filtro -->
    <div class="offcanvas">
        <div id="offcanvas-filtro" class="offcanvas-pane width-10">
            <div class="offcanvas-head">
                <header>Filtro</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                </div>
            </div>

            <div class="offcanvas-body">
                <form id="formulario" class="form"  role="form" method="post" novalidate>
                    <input type="hidden" name="filtro" value="1">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group ">
                                <select id="EstadoFiltro" name="form[int][Estado]" class="form-control">
                                    <option value="">&nbsp;</option>

                                    {foreach item=app from=$EstadoHCM}

                                            <option value="{$app.cod_detalle}">{$app.ind_nombre_detalle} </option>

                                    {/foreach}

                                </select>
                                <label for="rangelength2">  Estado de la Solicitud</label>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <label >Funcionario</label>
                                <div class="form-group">
                                    <select id="EmpleadoFiltro" name="form[int][num_empleado_beneficio]" class="form-control select2-list select2">
                                        <option value="">Seleccione Empleado</option>
                                        {foreach item=i from=$_EmpleadoPost}

                                                <option value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>

                                        {/foreach}
                                    </select>

                            </div>
                        </div>


                        <div class="col-lg-12">
                            <div class="form-group">
                                <select id="BeneficioFiltro" name="form[int][Beneficio]" class="form-control">
                                    <option value="">&nbsp;</option>

                                    {foreach item=app from=$BeneficiosPost}


                                        <option value="{$app.pk_num_ayuda_especifica}">{$app.ind_descripcion_especifica} </option>

                                    {/foreach}

                                </select>
                                <label for="rangelength2"> Beneficio Personal </label>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="form-group">
                                <select id="AyudaFiltro" name="form[int][Ayuda]" class="form-control">
                                    <option value="">&nbsp;</option>

                                    {foreach item=app from=$Ayuda}


                                        <option value="{$app.pk_num_ayuda_global }">{$app.ind_descripcion} </option>

                                    {/foreach}

                                </select>
                                <label for="fk_rhb008_num_ayuda_global">Año de la Ayuda</label>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                                Filtrar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {






        $('#nuevo').click(function(){
            var $url='{$_Parametros.url}modRH/procesos/HcmCONTROL/NuevoBeneficioMET';
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{   tipo:"registrar"},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            var $url='{$_Parametros.url}modRH/procesos/HcmCONTROL/ModificarBeneficioMET';
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPost: $(this).attr('idModificar'), tipo:"modificar"},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.ver', function () {
            var $url='{$_Parametros.url}modRH/procesos/HcmCONTROL/ModificarBeneficioMET';
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPost: $(this).attr('idVer'), tipo:"ver"},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });


        $('#datatable1 tbody').on( 'click', '.bitacora', function () {
            var $url='{$_Parametros.url}modRH/procesos/HcmCONTROL/BitacoraMET';

            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url,{ idHcm:$(this).attr('idBitacora')},function($dato){
                $('#ContenidoModal2').html($dato);
            });
            $('#modalAncho2').css( "width", "55%" );
        });


        $('#datatable1 tbody').on( 'click', '.eliminar', function () {

            var idBeneficio=$(this).attr('idBeneficio');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/procesos/HcmCONTROL/AnularBeneficioMET';
                $.post($url, { idBeneficio: idBeneficio},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idBeneficio'+$dato['idBeneficio'])).html('');
                        swal("Anulado!", "Beneficio Anulado.", "success");

                    }
                },'json');

            });
        });

        $('#datatable1 tbody').on( 'click', '.HcmPdf', function () {

            $('#ContenidoModal').html("");
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#modalAncho').css( 'width', '85%' );
            var beneficio= $(this).attr('idPdf');
            var urlReporte = '{$_Parametros.url}modRH/procesos/HcmCONTROL/GenerarEntradaHCMMET/'+beneficio;
            $('#ContenidoModal').html('<iframe src="'+urlReporte+'" width="100%" height="950"></iframe>');

        });



        $("#formulario").validate({
            submitHandler: function(form) {

                var EstadoFiltro = $("#EstadoFiltro").val();
                var EmpleadoFiltro = $("#EmpleadoFiltro").val();
                var BeneficioFiltro = $("#BeneficioFiltro").val();
                var AyudaFiltro = $("#AyudaFiltro").val();




                var url_listar='{$_Parametros.url}modRH/procesos/HcmCONTROL/BuscarBeneficioMET';
                $.post(url_listar,{ EstadoFiltro: EstadoFiltro,EmpleadoFiltro: EmpleadoFiltro,BeneficioFiltro: BeneficioFiltro,AyudaFiltro: AyudaFiltro },function(respuesta_post) {
                    var tabla_listado = $('#datatable1').DataTable();
                    tabla_listado.clear().draw();

                    if(respuesta_post != -1) {
                        for(var i=0; i<respuesta_post.length; i++) {
                            var url = "{$_Parametros.url}modRH/gestion/permisoCONTROL/ReportePermisoMET/?pk_num_permiso="+respuesta_post[i].pk_num_permiso;
                            if(respuesta_post[i].txt_estatus=='Preparado'){
                                var botonEditar = '{if in_array('RH-01-01-06-03-04-M',$_Parametros.perfil)}<button type="button" pk_num_permiso = "' + respuesta_post[i].pk_num_permiso + '" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"  descripcion="El usuario ha modificado un permiso" title="Modificar permiso"  titulo="Modificar permiso"><i class="md md-perm-identity"></i></button>{/if}';
                            } else {
                                var botonEditar = '{if in_array('RH-01-01-06-03-04-M',$_Parametros.perfil)}{/if} ';
                            }

                            tabla_listado.row.add([
                                respuesta_post[i].pk_beneficio_medico,
                                respuesta_post[i].ind_nombre_detalle,
                                respuesta_post[i].ind_nombre1+ ' ' + respuesta_post[i].ind_apellido1,
                                respuesta_post[i].ind_descripcion_especifica,
                                respuesta_post[i].num_monto,
                                respuesta_post[i].estado_nombre,

                                '{if in_array('RH-01-02-04-05-B',$_Parametros.perfil)}<button type="button" idBitacora = "' + respuesta_post[i].pk_beneficio_medico + '" class="bitacora logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"    titulo="Ver Bitácora de Movimientos" descipcion="El Usuario ha Visualizado la Bitácora de Movimientos"  ><i class="md md-perm-identity"></i></button>{/if}',

                                '{if in_array('RH-01-02-04-06-V',$_Parametros.perfil)}<button type="button" idVer = "' + respuesta_post[i].pk_beneficio_medico + '" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"    titulo="Ver Beneficio HCM" descipcion="El Usuario ha Visualizado el Beneficio HCM"  ><i class="fa fa-edit"></i></button>{/if}',

                                '{if in_array('RH-01-02-04-07-M',$_Parametros.perfil)}<button type="button" idModificar = "' + respuesta_post[i].pk_beneficio_medico + '" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"    titulo="Modificar Beneficio HCM" descipcion="El Usuario ha Visualizado el Beneficio HCM"  ><i class="fa fa-edit"></i></button>{/if}',


                                '<button class="HcmPdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal"   data-target="#formModal"  data-keyboard="false" data-backdrop="static" idPdf="{$b.pk_beneficio_medico}" descipcion="El Usuario a visualizado planila HCM" title="Planilla HCM" titulo="Ver Planilla">  <i class="md md-attach-file" style="color: #ffffff;"></i></button>',




                                '{if in_array('RH-01-02-04-08-A',$_Parametros.perfil)}<button type="button" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficio="{$b.pk_beneficio_medico}"  boton="si, Eliminar" descipcion="El usuario a eliminado Beneficio " titulo="Estas Seguro?" mensaje="Estas seguro que desea anular el Beneficio !!"> <i class="md md-delete" style="color: #ffffff;"></i></button>{/if}'
                            ]).draw()
                                    .nodes()
                                    .to$()
                        }
                    }

                },'json');
            }
        });


    });






</script>