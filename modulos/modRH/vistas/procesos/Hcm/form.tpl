
<link type="text/css"   rel="stylesheet" href="{$_Parametros.ruta_Complementos}theme-default/libs/wizard/wizard.css?1425466601" />

<!-- BEGIN VALIDATION FORM WIZARD -->
<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax" class="form   form-validation" role="form"  >

        <input type="hidden" value="1" name="valido"  id="valido" />
        <input type="hidden" value="{if isset($_formDB[0]['pk_beneficio_medico'])}{$_formDB[0]['pk_beneficio_medico']}{/if}" name="idHcm"/>

        <div class="form-wizard-nav">
            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
            <ul class="nav nav-justified">
                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>

                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title"> SOPORTE </span></a></li>

                <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title"> FACTURA </span></a></li>

            </ul>
            <div class="col-lg-12">
                <div class="well well-sm clearfix">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label style="text-decoration: underline; color: black" class="text-bold">AYUDA</label>
                            <input type="text" id="ayudaN" name="ayudaN" class="form-control" value="" disabled>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label style="text-decoration: underline; color: black" class="text-bold">BENEFICIO</label>
                            <input type="text" id="beneficioN" name="beneficioN" class="form-control" value="" disabled>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label style="text-decoration: underline; color: black" class="text-bold">Disponible</label>
                            <input type="text" id="disponible" name="disponible" class="form-control" value="0,00" disabled>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label style="text-decoration: underline; color: black" class="text-bold">Consumido</label>
                            <input type="text" id="consumido" name="consumido" class="form-control" value="0,00" disabled>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label style="text-decoration: underline; color: black" class="text-bold">Disponible del Beneficio</label>
                            <input type="text" id="disponibleB" name="disponibleB" class="form-control" value="0,00" disabled>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label style="text-decoration: underline; color: black" class="text-bold">Consumido del Beneficio</label>
                            <input type="text" id="consumidoB" name="consumidoB" class="form-control" value="0,00" disabled>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--end .form-wizard-nav -->

        <div class="tab-content clearfix">
            <div class="tab-pane active" id="step1">



                <div class="col-lg-6">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <select    {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} name="form[int][organismo]" class="form-control input-sm" required data-msg-required="Seleccione el Organismo">
                                <option value="">&nbsp;</option>
                                {foreach item=b from=$_OrganismoPost}
                                    {if isset($_formDB[0]['fk_a001_organismo'])}
                                        {if $b.pk_num_organismo==$_formDB[0]['fk_a001_organismo']}
                                            <option selected value="{$b.pk_num_organismo}">{$b.ind_descripcion_empresa}</option>
                                        {else}
                                            <option value="{$b.pk_num_organismo}">{$b.ind_descripcion_empresa}</option>
                                        {/if}
                                    {else}
                                        <option value="{$b.pk_num_organismo}">{$b.ind_descripcion_empresa}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label >Organismo</label>
                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <select   {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if}  name="form[int][tipo_solicitud]" class="form-control input-sm" required data-msg-required="Seleccione el tipo de solicitud">
                                <option value="">&nbsp;</option>
                                {foreach item=b from=$_TipoSolicitudPost}
                                    {if isset($_formDB[0]['fk_a006_tipo_solicitud'])}
                                        {if $b.cod_detalle==$_formDB[0]['fk_a006_tipo_solicitud']}
                                            <option selected value="{$b.cod_detalle}">{$b.ind_nombre_detalle}</option>
                                        {else}
                                            <option value="{$b.cod_detalle}">{$b.ind_nombre_detalle}</option>
                                        {/if}
                                    {else}
                                        <option value="{$b.cod_detalle}">{$b.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label  >Tipo de Solicitud</label>
                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label >Funcionario  </label>
                            <select {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} id="num_empleado_beneficio" name="form[int][num_empleado_beneficio]" required class="form-control select2-list select2">
                                <option value="">Seleccione Empleado</option>
                                {foreach item=i from=$_EmpleadoPost}
                                    {if isset($_formDB[0]['fk_rhb001_empleado'])}
                                        {if $i.pk_num_empleado==$_formDB[0]['fk_rhb001_empleado']}
                                            <option selected value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                        {else}
                                            <option value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                        {/if}
                                    {else}
                                        <option value="{$i.pk_num_empleado}">{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6">

                    <div class="col-lg-12">
                        <div class="form-group">
                            <select {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} id="CargaFamiliar"  name="form[int][carga_familiar]" class="form-control input-sm"   data-msg-required="Seleccione el beneficio Global">
                                <option value="">&nbsp;</option>
                                {foreach item=b from=$_CargaFamiliarDB}
                                    {$b.pk_num_persona} -- {$_formDB[0]['fk_a003_persona']}
                                    {if isset($_formDB[0]['fk_a003_persona'])}

                                        {if $b.pk_num_persona==$_formDB[0]['fk_a003_persona']}
                                            <option selected value="{$b.pk_num_persona}">{$b.nombre}</option>
                                        {else}
                                            <option value="{$b.pk_num_persona}">{$b.nombre}</option>
                                        {/if}
                                    {else}
                                        <option value="{$b.pk_num_persona}">{$b.nombre}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label  >Carga Familiar</label>
                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <select {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} id="fk_rhb008_num_ayuda_global" name="form[int][num_ayuda_global]" class="form-control input-sm" required data-msg-required="Seleccione el beneficio Global">
                                <option value="">Seleccione la Ayuda...</option>
                                {foreach item=b from=$_AyudaPost}
                                    {if isset($_formDB[0]['fk_rhb008_ayuda_global'])}
                                        {if $b.pk_num_ayuda_global==$_formDB[0]['fk_rhb008_ayuda_global']}
                                            <option selected value="{$b.pk_num_ayuda_global}">{$b.ind_descripcion}</option>
                                        {else}
                                            <option value="{$b.pk_num_ayuda_global}">{$b.ind_descripcion}</option>
                                        {/if}
                                    {else}
                                        <option value="{$b.pk_num_ayuda_global}">{$b.ind_descripcion}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="fk_rhb008_num_ayuda_global">Año de la Ayuda</label>
                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label  >Beneficio</label>
                            <select {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} id="Beneficio" name="form[int][num_beneficio]" required class="form-control select2-list select2">
                                <option value="">Seleccione Beneficio...</option>
                                {foreach item=i from=$_BeneficiosPost}
                                    {if isset($_formDB[0]['fk_rhc068_ayuda_especifica'])}
                                        {if $i.pk_num_ayuda_especifica==$_formDB[0]['fk_rhc068_ayuda_especifica']}
                                            <option selected value="{$i.pk_num_ayuda_especifica}">{$i.ind_descripcion_especifica} -   {$i.num_limite_esp|number_format:2:",":"."}</option>
                                        {else}
                                            <option value="{$i.pk_num_ayuda_especifica}">{$i.ind_descripcion_especifica} -   {$i.num_limite_esp|number_format:2:",":"."}</option>
                                        {/if}
                                    {else}
                                        <option value="{$i.pk_num_ayuda_especifica}">{$i.ind_descripcion_especifica} -    {$i.num_limite_esp|number_format:2:",":"."}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                        </div>
                    </div>
                </div>

            </div><!--end #step1 -->

            <div class="tab-pane" id="step2">


                <div class="col-lg-9">

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="num_rama">Rama </label>
                            <select  {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if}  name="form[int][rama]" class="form-control select2-list select2">
                                <option value="">Seleccione Rama</option>
                                {foreach item=i from=$_RamasPost}
                                    {if isset($_formDB[0]['fk_a006_rama'])}
                                        {if $i.cod_detalle==$_formDB[0]['fk_a006_rama']}
                                            <option selected value="{$i.cod_detalle}">{$i.ind_nombre_detalle}</option>
                                        {else}
                                            <option value="{$i.cod_detalle}">{$i.ind_nombre_detalle}</option>
                                        {/if}
                                    {else}
                                        <option value="{$i.cod_detalle}">{$i.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label  >Institución</label>
                            <select  {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} id="institucion" name="form[int][institucion]" class="form-control select2-list select2">
                                <option value="">Seleccione Institución</option>
                                {foreach item=i from=$_InstitutucionPost}
                                    {if isset($_formDB[0]['fk_rhc040_institucion'])}
                                        {if $i.pk_num_institucion==$_formDB[0]['fk_rhc040_institucion']}
                                            <option selected value="{$i.pk_num_institucion}">{$i.ind_nombre_institucion} </option>
                                        {else}
                                            <option value="{$i.pk_num_institucion}">{$i.ind_nombre_institucion}</option>
                                        {/if}
                                    {else}
                                        <option value="{$i.pk_num_institucion}"> {$i.ind_nombre_institucion} </option>
                                    {/if}
                                {/foreach}
                            </select>
                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            <label  > Medico </label>
                            <select  {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} name="form[int][medico]" class="form-control select2-list select2">
                                <option value="">Seleccione Médico</option>
                                {foreach item=i from=$_MedicosPost}
                                    {if isset($_formDB[0]['fk_rhc047_medico_hcm'])}
                                        {if $i.pk_num_medico_hcm==$_formDB[0]['fk_rhc047_medico_hcm']}
                                            <option selected value="{$i.pk_num_medico_hcm}">{$i.ind_nombre1} {$i.ind_apellido1} </option>
                                        {else}
                                            <option value="{$i.pk_num_medico_hcm}">{$i.ind_nombre1} {$i.ind_apellido1} </option>
                                        {/if}
                                    {else}
                                        <option value="{$i.pk_num_medico_hcm}">{$i.ind_nombre1} {$i.ind_apellido1}  </option>
                                    {/if}
                                {/foreach}
                            </select>
                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body no-padding">
                            <ul class="list" data-sortable="true">
                                <li class="tile">
                                    <div class="checkbox checkbox-styled h4">
                                        <label>
                                            <span>
                                                Soportes
                                            </span>
                                        </label>
                                    </div>

                                </li>
                                <li class="tile">
                                    <div class="checkbox checkbox-styled  ">
                                        <label>
                                            <input {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} type="checkbox" name="form[txt][flag_planilla_solicitud]" {if isset($_formDB[0]['flag_planilla_solicitud'])}{if $_formDB[0]['flag_planilla_solicitud']==1}checked  {/if}{/if} >
                                            <span>
                                                Planillas de Solicitud
                                            </span>
                                        </label>
                                    </div>

                                </li>
                                <li class="tile">
                                    <div class="checkbox checkbox-styled ">
                                        <label>
                                            <input {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} type="checkbox" name="form[txt][flag_informe_medico]" {if isset($_formDB[0]['flag_informe_medico'])}{if $_formDB[0]['flag_informe_medico']==1}checked  {/if}{/if} >
                                            <span>
                                                Informe Médico
                                            </span>
                                        </label>
                                    </div>

                                </li>
                                <li class="tile">
                                    <div class="checkbox checkbox-styled ">
                                        <label>
                                            <input {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} type="checkbox" name="form[txt][flag_factura]" {if isset($_formDB[0]['flag_factura'])}{if $_formDB[0]['flag_factura']==1}checked  {/if}{/if} >
                                            <span>
                                                Facturas
                                            </span>
                                        </label>
                                    </div>

                                </li>
                                <li class="tile">
                                    <div class="checkbox checkbox-styled ">
                                        <label>
                                            <input {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} type="checkbox" name="form[txt][flag_recipe_medico]" {if isset($_formDB[0]['flag_recipe_medico'])}{if $_formDB[0]['flag_recipe_medico']==1}checked  {/if}{/if} >
                                            <span>
                                                Recipe Médico
                                            </span>
                                        </label>
                                    </div>

                                </li>
                                <li class="tile">
                                    <div class="checkbox checkbox-styled ">
                                        <label>
                                            <input  {if isset($_formDB[0]['ind_estado'])}{if  ($_formDB[0]['ind_estado'])!=1 } disabled  {/if}{/if} type="checkbox" name="form[txt][flag_otro]" {if isset($_formDB[0]['flag_otro'])}{if $_formDB[0]['flag_otro']==1}checked  {/if}{/if} >
                                            <span>
                                                Otro
                                            </span>
                                        </label>
                                    </div>

                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>

            <div class="tab-pane" id="step3">
                <div class="col-lg-12">
                <div class="col-lg-2">

                        <div class="form-group">
                            <input type="hidden"  name="form[txt][accion]" value="{$_Acciones.accion}"    id="accion" />


                                <input    value="{if isset($post.ind_marca)}{$post.ind_marca}{/if}" type="text" class="form-control" id="Factura"  name="Factura"    required="">



                            <label for="rangelength2">Nro Factura</label>
                        </div>
                </div>

                <div class="col-lg-6">
                    <form class="form form-validate "  >
                        <div class="form-group">
                            {foreach item=post from=$_Acciones}

                                <input    value="{if isset($post.ind_marca)}{$post.ind_marca}{/if}" type="text" class="form-control" id="Descripcion"  name="Descripcion"   required="">

                            {/foreach}


                            <label for="rangelength2">Descripción</label>
                        </div>
                </div>


                <div class="col-lg-2">
                    <form class="form form-validate "  >
                        <div class="form-group">
                            {foreach item=post from=$_Acciones}

                                <input value="{if isset($post.ind_marca)}{$post.ind_marca}{/if}" type="text" class="form-control" id="Monto" name="Monto" required="">

                            {/foreach}


                            <label for="rangelength2">Monto</label>
                        </div>
                </div>

                <div class="col-lg-2">
                    {if ($_Acciones.accion!="ver")}
                    <button class=" btn ink-reaction btn-raised btn-info"  type="button" titulo="Nuevo Asignación de Beneficio" id="Agregar" > Agregar  </button>
                    {/if}
                </div>

                </div>
                <div class="col-lg-12">

                            <div class="card">
                                <div class="card-body">
                                    <table class="table no-margin" id="tablaFacturas">
                                        <thead> <input type="hidden" name="prueba" required>
                                        <tr>

                                            <th class="col-lg-2">Factura </th>
                                            <th class="col-lg-8">Descripción</th>
                                            <th class="col-lg-2">Monto</th>
                                            <th class="col-lg-2">Borrar</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        {$i=1}
                                        {if isset($_FacturasDB)}
                                        {foreach item=b from=$_FacturasDB}

                                            <tr id="id{$i}">
                                                <td>{$b.num_factura}</td>
                                                <td>{$b.ind_descripcion}</td>
                                                <td>{$b.num_monto|number_format:2:",":"."}</td>

                                                    {if in_array('RH-01-04-07-04-E',$_Parametros.perfil)}
                                                         <td>
                                                           <button class=" btn ink-reaction btn-raised btn-xs btn-danger" type="button" onclick="Borrando('{$i}');  " >
                                                           <i class="md md-delete" style="color: #ffffff;"></i>
                                                           </button>
                                                         </td>
                                                    {/if}

                                                 <input type="hidden" id="factura{$i}" name="form[int][factura{$i}]" value="{$b.num_factura}">
                                                 <input type="hidden" id="descripcion{$i}" name="form[txt][descripcion{$i}]" value="{$b.ind_descripcion}">
                                                 <input type="hidden" id="montofactura{$i}" name="form[txt][montofactura{$i}]" value="{$b.num_monto|number_format:2:",":"."}">
                                            </tr>
                                        {$i=$i+1}
                                        {/foreach}
                                        {/if}
                                        <input type="hidden"   name="facturas"  value="{$i}" id="facturas" />
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>

                <div class="col-lg-3">
                    <div class="card">
                        <div class="card-body">
                        TOTAL  <input type="text" name="form[txt][total]" id="Total"  style="text-align: right;border:none;" readonly value="0">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>


                    {if ($_Acciones.accion=="revisar")}
                        <button type="submit" id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button"><span class="glyphicon glyphicon-floppy-disk"></span> Revisar</button>
                    {/if}



                    {if ($_Acciones.accion=="modificar")}
                    <button type="submit"     id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button"><span class="glyphicon glyphicon-floppy-disk"></span>Modificar</button>
                    {/if}


                    {if ($_Acciones.accion=="aprobar")}
                        <button type="submit" id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button"><span class="glyphicon glyphicon-floppy-disk"></span>Aprobar</button>
                    {/if}

                    {if ($_Acciones.accion=="registrar")}
                        <button type="button" id="boton" class="registrar btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button"><span class="glyphicon glyphicon-floppy-disk"></span> Registrar</button>
                    {/if}

                </div>
                    </div>



            </div>





    </form>

    <ul class="pager wizard">

        <li class="previous"><a class="btn-raised" href="javascript:void(0);">Previo</a></li>

        <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>

    </ul>
</div>








</div>




<script type="text/javascript">

    var i=   $("#facturas").val();

    $("#formAjax").submit(function(){
        return false;
    });

    $('#boton').click(function () {

             //alert("Envio el formulario");
            var datos = $("#formAjax").serialize();
            var accion = $("#accion").val();
            if (i <= 1) {
                alert(" No ha agregado una factura. " + i);
            } else {


                if (accion == "registrar") {
                    $.post('{$_Parametros.url}modRH/procesos/HcmCONTROL/NuevoBeneficioMET', datos, function (dato) {

                        if(dato['status']=='error') {

                            swal("Error", "No se pudo realizar operacion. Verifique que los valores sean los correctos o Consulte con el Administrador del Sistema.", "error");

                        }else if(dato['status']=='error2') {


                            swal("Error", dato['detalleERROR'], "error");


                        }else {

                            /*$(document.getElementById('datatable1')).append('<tr  id="Beneficio'+dato['pk_beneficio_medico']+'">' +
                                    '<td>'+dato['pk_beneficio_medico']+'</td>' +
                                    '<td>'+dato['tipo']+'</td>' +
                                    '<td>'+dato['funcionario']+'</td>' +
                                    '<td>'+dato['beneficio']+'</td>' +
                                    '<td>'+dato['monto']+'</td>' +
                                    '<td>'+dato['estado']+'</td>' +
                                    '<td   class="sort-alpha col-sm-1" >' +

                                    '{if in_array('RH-01-02-04-05-B',$_Parametros.perfil)} <button class="bitacora logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Ver Bitácora de Movimientos" descipcion="El Usuario a Visualizado la Bitácora de Movimientos" idBitacora="'+dato['pk_beneficio_medico']+'" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal"><i class="md md-perm-identity" style="color: #ffffff;"></i></button>{/if}' +

                                    '<td   class="sort-alpha col-sm-1" >' +
                                    '{if in_array('RH-01-02-04-06-V',$_Parametros.perfil)} <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idVer="'+dato['pk_beneficio_medico']+'"  title="Ver Beneficio HCM" titulo="Ver Beneficio Personal"> <i class="md md-remove-red-eye" style="color: #ffffff;"></i></button>{/if}' +


                                    '<td   class="sort-alpha col-sm-1" >' +
                                    '{if in_array('RH-01-02-04-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idModificar="'+dato['pk_beneficio_medico']+'"   title="Editar Beneficio HCM" titulo="Modificar Beneficio Personal"> <i class="fa fa-edit" style="color: #ffffff;"></i> </button>{/if}' +

                                    '<td   class="sort-alpha col-sm-1" >' +
                                    '{if in_array('RH-01-02-04-06-V',$_Parametros.perfil)}<button class="HcmPdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal"   data-target="#formModal" data-keyboard="false" data-backdrop="static" idPdf="'+dato['pk_beneficio_medico']+'"  title="Planilla HCM" titulo="Ver Planilla"> <i class="md md-attach-file" style="color: #ffffff;"></i>   </button>{/if}'+

                                    '<td class="pdf sort-alpha col-sm-1" > </td>' +
                                    '</tr>');
                                */

                                swal("Beneficio Personal Agregado!", "El beneficio se agregó satisfactoriamente.", "success");
                                $(document.getElementById('cerrarModal')).click();
                                $(document.getElementById('ContenidoModal')).html('');
                               $("#_contenido").load('{$_Parametros.url}modRH/procesos/HcmCONTROL');


                            }
                    }, 'json');

                    // $("#_contenido").load('{$_Parametros.url}modRH/procesos/HcmCONTROL');
                }else if (accion == "modificar") {

                    $.post('{$_Parametros.url}modRH/procesos/HcmCONTROL/ModificarBeneficioMET', datos, function (dato) {
 							
							if(dato['status']=='error') {

                            swal("Error", "No se pudo realizar operacion. Verifique que los valores sean los correctos o Consulte con el Administrador del Sistema.", "error");

                        }else if(dato['status']=='error2') {


                            swal("Error", dato['detalleERROR'], "error");


                        }else {
							/*$(document.getElementById('idSolicitud'+dato['pk_beneficio_medico'])).html('<td>'+dato['pk_beneficio_medico']+'</td>' +
                                    '<td>'+dato['tipo']+'</td>' +
                                    '<td>'+dato['funcionario']+'</td>' +
                                    '<td>'+dato['beneficio']+'</td>' +
                                    '<td>'+dato['monto']+'</td>' +
                                    '<td>'+dato['estado']+'</td>' +
                                    '<td   class="sort-alpha col-sm-1" >' +

                                    '{if in_array('RH-01-02-04-05-B',$_Parametros.perfil)} <button class="bitacora logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Ver Bitácora de Movimientos" descipcion="El Usuario a Visualizado la Bitácora de Movimientos" idBitacora="'+dato['pk_beneficio_medico']+'" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal"><i class="md md-perm-identity" style="color: #ffffff;"></i></button>{/if}' +

                                    '<td   class="sort-alpha col-sm-1" >' +
                                    '{if in_array('RH-01-02-04-06-V',$_Parametros.perfil)} <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idVer="'+dato['pk_beneficio_medico']+'"  title="Ver Beneficio HCM" titulo="Ver Beneficio Personal"> <i class="md md-remove-red-eye" style="color: #ffffff;"></i></button>{/if}' +


                                    '<td   class="sort-alpha col-sm-1" >' +
                                    '{if in_array('RH-01-02-04-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idModificar="'+dato['pk_beneficio_medico']+'"   title="Editar Beneficio HCM" titulo="Modificar Beneficio Personal"> <i class="fa fa-edit" style="color: #ffffff;"></i> </button>{/if}' +

                                    '<td   class="sort-alpha col-sm-1" >' +
                                    '{if in_array('RH-01-02-04-06-V',$_Parametros.perfil)}<button class="HcmPdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal"   data-target="#formModal" data-keyboard="false" data-backdrop="static" idPdf="'+dato['pk_beneficio_medico']+'"  title="Planilla HCM" titulo="Ver Planilla"> <i class="md md-attach-file" style="color: #ffffff;"></i>   </button>{/if}'+

                                    '<td class="pdf sort-alpha col-sm-1" > </td>' +
                            '</td>');*/
                                swal("Beneficio Personal Agregado!", "El beneficio se Modifico satisfactoriamente.", "success");
                                $(document.getElementById('cerrarModal')).click();
                                $(document.getElementById('ContenidoModal')).html('');
                                $("#_contenido").load('{$_Parametros.url}modRH/procesos/HcmCONTROL');
						}
							
							
                    }, 'json');


                }else if (accion == "revisar") {

                    $.post('{$_Parametros.url}modRH/procesos/HcmCONTROL/RevisarBeneficioMET', datos, function (dato) {

                        if(dato['status']=='error') {
                            swal("Error", "No se pudo realizar operacion. Verifique que los valores sean los correctos o Consulte con el Administrador del Sistema.", "error");
                        }else{
                            swal("Beneficio Personal Revisado!", "El beneficio ha sido revisado satisfactoriamente.", "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                            $("#_contenido").load('{$_Parametros.url}modRH/procesos/HcmCONTROL');
                        }

                    }, 'json');


                }else if (accion == "aprobar") {

                    $.post('{$_Parametros.url}modRH/procesos/HcmCONTROL/AprobarBeneficioMET', datos, function (dato) {

                        if(dato['status']=='error') {
                            swal("Error", "No se pudo realizar operacion. Verifique que los valores sean los correctos o Consulte con el Administrador del Sistema.", "error");
                        }else{
                            swal("Beneficio Personal Aprobado!", "El beneficio ha sido aprobado satisfactoriamente.", "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                            $("#_contenido").load('{$_Parametros.url}modRH/procesos/HcmCONTROL');
                        }

                    }, 'json');

                }

            }

    });



    $("#fk_rhb008_num_ayuda_global").change(function () {


        if ($("#num_empleado_beneficio").val()==0){
            swal("Información!", "Debe seleccionar un Empleado.", "info");
            $("#fk_rhb008_num_ayuda_global").val('');
            return false;
        }else {

            $("#fk_rhb008_num_ayuda_global option:selected").each(function () {

                $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/ActualizarDisponibilidadMET", { Ayuda: $('#fk_rhb008_num_ayuda_global').val(), Beneficio: $('#Beneficio').val(), Empleado: $('#num_empleado_beneficio').val() }, function (data) {
                    $("#disponible").val(data['disponible']);
                    $("#consumido").val(data['consumido']);
                    $("#disponibleB").val(data['disponibleB']);
                    $("#consumidoB").val(data['consumidoB']);
                },'json');

                $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/NombreAyudaMET", { Ayuda: $('#fk_rhb008_num_ayuda_global').val() }, function (ayu) {
                    $("#ayudaN").val(ayu);
                },'json');

                $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/ListarAsignacionesMET", { Ayuda: $('#fk_rhb008_num_ayuda_global').val() }, function (data2) {
                    var json2 = data2,
                            obj2 = JSON.parse(json2);
                    $("#Beneficio").html(obj2);
                });

            });

        }
    })

    $("#Beneficio").change(function () {


        if ($("#num_empleado_beneficio").val()==0){
            swal("Información!", "Debe seleccionar un Empleado.", "info");
            $("#Beneficio").val('');
            return false;
        }else {

            $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/ActualizarDisponibilidadMET", { Ayuda: $('#fk_rhb008_num_ayuda_global').val(),Beneficio: $('#Beneficio').val(),Empleado: $('#num_empleado_beneficio').val()}, function (data) {
                $("#disponible").val(data['disponible']);
                $("#consumido").val(data['consumido']);
                $("#disponibleB").val(data['disponibleB']);
                $("#consumidoB").val(data['consumidoB']);
            },'json');

            $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/NombreBeneficioMET", { Beneficio: $('#Beneficio').val() }, function (ben) {
                $("#beneficioN").val(ben);
            },'json');

        }
    })

    $('#num_empleado_beneficio').change(function () {

        $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/ActualizarDisponibilidadMET", { Ayuda: $('#fk_rhb008_num_ayuda_global').val(),Beneficio: $('#Beneficio').val(),Empleado: $('#num_empleado_beneficio').val() }, function(data){
            $("#disponible").val(data['disponible']);
            $("#consumido").val(data['consumido']);
            $("#disponibleB").val(data['disponibleB']);
            $("#consumidoB").val(data['consumidoB']);
        },'json');

        $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/NombreAyudaMET", { Ayuda: $('#fk_rhb008_num_ayuda_global').val() }, function (ayu) {
            $("#ayudaN").val(ayu);
        },'json');

        $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/NombreBeneficioMET", { Beneficio: $('#Beneficio').val() }, function (ben) {
            $("#beneficioN").val(ben);
        },'json');

        $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/ActualizarCargaFamiliarMET", { Empleado: $('#num_empleado_beneficio').val() }, function(data){
            var json = data,
                    obj = JSON.parse(json);
            $("#CargaFamiliar").html(obj);
        });

    })



    $(document).ready(function() {
        ActualizarTotal();


        $('#Monto').inputmask(' 999.999.999,99 BS', { numericInput: true });
        if(($("#accion").val())=="modificar"||($("#accion").val())=="revisar"||($("#accion").val())=="ver"||($("#accion").val())=="aprobar") {

            $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/ActualizarDisponibilidadMET", { Ayuda: $('#fk_rhb008_num_ayuda_global').val(),Beneficio: $('#Beneficio').val(),Empleado: $('#num_empleado_beneficio').val() }, function(data){
                $("#disponible").val(data['disponible']);
                $("#consumido").val(data['consumido']);
                $("#disponibleB").val(data['disponibleB']);
                $("#consumidoB").val(data['consumidoB']);
            },'json');

            $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/NombreAyudaMET", { Ayuda: $('#fk_rhb008_num_ayuda_global').val() }, function (ayu) {
                $("#ayudaN").val(ayu);
            },'json');

            $.post("{$_Parametros.url}modRH/procesos/HcmCONTROL/NombreBeneficioMET", { Beneficio: $('#Beneficio').val() }, function (ben) {
                $("#beneficioN").val(ben);
            },'json');

            $("#Factura").val(" ");
            $("#Descripcion").val(" ");
            $("#Monto").val("0");

        }

        //ancho de la modal
        $('#modalAncho').css( "width", "85%" );

        $( "#Agregar" ).click(function() {

            var factura = $("#Factura").val();
            var descripcion = $("#Descripcion").val();
            var monto = $("#Monto").val();


            monto = monto.replace(/[^,\d]/g, '');
            monto = monto.replace("BS", "");


            factura=$.trim(factura);
            descripcion=$.trim(descripcion);
            monto=$.trim(monto);
            $("#facturas").val(i);
            if(factura!="") {
                if(descripcion!="") {
                    if(monto!="") {

                $(document.getElementById('tablaFacturas')).append('<tr id="id' + i + '">' +
                '<td>' + factura + ' </td>' +
                '<td>' + descripcion + '</td>' +
                '<td> ' + monto + ' </td>' +
                ' <td> <button class=" btn ink-reaction btn-raised btn-xs btn-danger" type="button" onclick="Borrando(\'' + i + '\');" > ' +
                '<i class="md md-delete" style="color: #ffffff;"></i>' +
                '</button> </td>' +

                '<input type="hidden" id="factura' + i + '" name="form[int][factura' + i + ']" value="' + factura + '">' +
                '<input type="hidden" id="descripcion' + i + '" name="form[txt][descripcion' + i + ']" value="' + descripcion + '">' +
                '<input type="hidden" id="montofactura' + i + '" name="form[txt][montofactura' + i + ']" value="' + monto + '"> ')+
                '</tr>';


                i++;
                $("#Factura").val(" ");
                $("#Descripcion").val(" ");
                $("#Monto").val("0");

                    }else{
                        alert("Agregue el monto");
                    }
                }else{
                    alert("Agregue la descripción");
                }

            }else{
                alert("Agregue el Nro de Factura");
            }

            $("#facturas").val(i);






            ActualizarTotal ();

        });



    });


    function ActualizarTotal (){

        var montoSuma = 0;
        var total =0;

        for(var z=1;z<i;z++){

            if(montoSuma=$("#montofactura"+z).val()!=undefined) {

                montoSuma = $("#montofactura" + z).val();
                montoSuma = montoSuma.replace(/[^\d]/g, '');
                total = parseFloat(total) + parseFloat(montoSuma);
            }
        }
        $("#Total").val((total/100));


    }
    function Borrando (id) {

        $('#id'+id).remove();

        var montoSuma = 0;
        var total =0;
        for(var z=1;z<=100;z++){

            if(montoSuma=$("#montofactura"+z).val()!=undefined) {
            montoSuma=$("#montofactura"+z).val();

            montoSuma = montoSuma.replace(/[^\d]/g, '');

            total=parseFloat(total)+parseFloat(montoSuma);
            }
        }
        $("#Total").val((total/100));



    }

</script>