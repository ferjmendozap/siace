<div class="modal-body">

    <form id="formAjaxRequisito" class="form form-validation" role="form" novalidate="novalidate" >
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <select id="fk_a006_num_miscelaneo_detalle_requisito" name="form[int][fk_a006_num_miscelaneo_detalle_requisito]" class="form-control input-sm" required>
                        <option value="">...</option>
                        {foreach item=dat from=$tipo_requisito}
                            {if isset($formDB.fk_a006_num_miscelaneo_detalle_requisito)}
                                {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_requisito}
                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                {else}
                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                {/if}
                            {else}
                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="requisito">Tipo de Requisito</label>
                </div>
            </div>
            <div class="col-md-12">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($formDB.num_flag_entregado) and $formDB.num_flag_entregado==1} checked{/if} value="1" name="form[int][num_flag_entregado]" id="num_flag_entregado" required>
                        <span>Entregado ?</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <br><br>
            <div align="right">
                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accionRequisito">Guardar</button>
            </div>
        </div>
    </form>

</div>






<script type="text/javascript">





    $(document).ready(function() {

        //ancho de la ventana
        $('#modalAncho3').css( "width", "30%" );


        $("#accionRequisito").click(function(){

            var data_b = $("#d_requisitos").val();
            var data_r = $("#t_requisitos").val();
            var carga = {$id_carga_familiar};

            if($('#requisito').val()==''){
                swal("Error", "Debe seleccionar el tipo de requisito", "error");
                return false;
            }

            if( $('#num_flag_entregado').is(':checked') ) {

            }else{
                swal("Error", "Debe chequear que esta entregado el Requisito", "error");
                return false;
            }


            var arreglo = data_b.split("#");
            var aux=0;
            for (i=0; i<=arreglo.length; i++)
            {
                if(arreglo[i]=={$id_carga_familiar}){
                    swal("Advertencia", "Ya existe un requisito guardado para este Beneficiario", "warning");
                    aux=1;
                    return false;
                }else{
                    aux=0;
                }
            }



            //alert(aux);

            if(aux==0) {

                if(data_b==''){
                    data_b = data_b + carga +'#';
                    $("#d_requisitos").val(data_b);
                }else{
                    data_b = data_b + {$id_carga_familiar} +'#';
                    $("#d_requisitos").val(data_b);
                }

            }

            if(data_r==''){
                data_r = data_r + $("#fk_a006_num_miscelaneo_detalle_requisito").val() +'#';
            }else{
                data_r = data_r + $("#fk_a006_num_miscelaneo_detalle_requisito").val() +'#';
            }

            $("#t_requisitos").val(data_r);

            $(document.getElementById('cerrarModal3')).click();
            $(document.getElementById('ContenidoModal3')).html('');


        });



    });

</script>