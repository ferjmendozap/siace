<div class="modal-body">

        <div class="row">



                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >


                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idBeneficio}" name="idBeneficio"/>
                    <input type="hidden" name="proceso"  id="proceso" value="{$proceso}">

                    <div class="row contain-lg">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="num_orden" name="form[txt][num_orden]" value="{if isset($formDB.num_orden)}{$formDB.num_orden}{else}{$orden}{/if}" disabled>
                                <label for="num_orden">Nº Orden</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select id="fk_a006_num_miscelaneo_detalle_asignacion" name="form[int][fk_a006_num_miscelaneo_detalle_asignacion]" class="form-control input-sm" required>
                                    <option value="">Seleccione...</option>
                                    {foreach item=dat from=$tipoAsignacion}
                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_asignacion)}
                                            {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_asignacion}
                                                <option selected value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_miscelaneo_detalle}-{$dat.cod_detalle}">{$dat.ind_nombre_detalle}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_a006_num_miscelaneo_detalle_asignacion">Tipo Asignación</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select id="fk_rhc056_num_beneficio_utiles" name="form[int][fk_rhc056_num_beneficio_utiles]" class="form-control input-sm" required>
                                    <option value="">Seleccione...</option>
                                    {foreach item=dat from=$periodos}
                                        {if isset($formDB.fk_rhc056_num_beneficio_utiles)}
                                            {if $dat.pk_num_beneficio_utiles==$formDB.fk_rhc056_num_beneficio_utiles}
                                                <option selected value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                            {else}
                                                <option value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_beneficio_utiles}">{$dat.ind_descripcion_beneficio}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="fk_rhc056_num_beneficio_utiles">Periodo</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control" id="nombreTrabajador" value="{if isset($formDB.nombre_completo)}{$formDB.nombre_completo}{/if}" disabled>
                                        <input type="hidden" name="form[int][fk_rhb001_num_empleado]" id="fk_rhb001_num_empleado" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{/if}"  disabled>
                                        <label for="nombreTrabajador">Empleado</label>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" id="buscarEmpleado" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Empleado">Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <select id="lg_b022_num_proveedor" name="form[int][lg_b022_num_proveedor]" class="form-control input-xs select2-list select2" disabled>
                                    <option value="">&nbsp;</option>
                                    {foreach item=dat from=$proveedores}
                                        {if isset($formDB.lg_b022_num_proveedor)}
                                            {if $dat.pk_num_proveedor == $formDB.lg_b022_num_proveedor}
                                                <option selected value="{$dat.pk_num_proveedor}">{$dat.nomProveedor}</option>
                                            {else}
                                                <option value="{$dat.pk_num_proveedor}">{$dat.nomProveedor}</option>
                                            {/if}
                                        {else}
                                            <option value="{$dat.pk_num_proveedor}">{$dat.nomProveedor}</option>
                                        {/if}

                                    {/foreach}
                                </select>
                                <label for="lg_b022_num_proveedor">Proveedor</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-6">
                        <div class="form-group">
                            <select id="hijos" name="hijos" class="form-control input-sm">
                                <option value="">Seleccione...</option>
                            </select>
                            <label for="hijos">Hijos</label>
                        </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm bolivar-mask" id="monto" value="">
                                        <input type="hidden" class="form-control input-sm" id="monto_t" name="form[int][monto_t]" value="0,00">
                                        <label for="monto">Monto</label>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" id="AgregarBeneficiario">Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row contain-lg">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                <label for="txt_observacion_pre">Observaciones Preparado</label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row contain-lg">
                        <input type="hidden" name="form[txt][d_requisitos]" id="d_requisitos" value="" placeholder="requisitos">
                        <input type="hidden" name="form[txt][t_requisitos]" id="t_requisitos" value="" placeholder="tipo requisito">
                        <input type="hidden" name="form[txt][d_beneficiarios]" id="d_beneficiarios" value="" placeholder="beneficiarios">
                        <input type="hidden" name="form[txt][carga]" id="carga" value="" placeholder="beneficiarios">
                        <input type="hidden" name="form[txt][monto_a]" id="monto_a" value="" placeholder="monto asignado">
                        <table id="listaBeneficarios" class="table table-striped table-hover">
                            <thead>
                            <tr align="center">
                                <th class="col-sm-1"><i class="md-person"></i> Id</th>
                                <th class="col-sm-4"><i class="md-person"></i> Nombres y apellidos</th>
                                <th class="col-sm-2"><i class="md-person"></i> Monto</th>
                                <th class="col-sm-1">Acción</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <div class="row contain-lg">
                        <br>
                        <div align="right">
                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>Cancelar</button>
                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accionBotonBeneficio"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar </button>
                        </div>
                        <br>
                    </div>
                </form>

        </div>

</div>




<style>
    .letra_pequena{
        font-size: 11px;
    }



</style>

<script type="text/javascript">


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();

            var tabla = $('#datatable1').DataTable();



            var aux  = $("#fk_a006_num_miscelaneo_detalle_asignacion").val();
            var tipo = aux.split("-");

            if(tipo[1]=='AF'){

                if($("#lg_b022_num_proveedor").val()==''){
                    swal("Error","Debe Seleccionar Proveedor", "error");
                    return false;
                }

            }


            if($("#proceso").val()=='NuevoBeneficio'){

                var url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/NuevoBeneficioMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        //swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        swal("Error",dato['detalleERROR'], "error");
                    }
                    if(dato['status']=='registrado'){


                        if(dato['cod_detalle']=='AD'){
                            var btn_revisar = '<button class="revisar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficio="'+dato['idBeneficio']+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Revisar Beneficio de Utiles" titulo="Revisar Beneficio de Utiles">REVISAR</button>';
                            var btn_info = '<button class="infoReg logsUsuario btn ink-reaction btn-raised btn-xs btn- btn-danger" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idBeneficio="'+dato['idBeneficio']+'" descripcion="Consultar operaciones" titulo="Información de Registro"><i class="md md-people" style="color: #ffffff;"></i></button>';
                            var btn_orden = '';
                            var estado = 'Preparado';
                        }else{
                            var btn_revisar = '';
                            var btn_orden = '<button class="generar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBeneficio="'+dato['idBeneficio']+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Generar Orden" titulo="Orden Beneficio Utiles"><i class="md md-print"></i> ORDEN</button>';
                            var btn_info = '';
                            var estado = 'Por Asignar Utiles';
                        }


                        var fila = tabla.row.add([
                            dato['num_orden'],
                            dato['ind_descripcion_beneficio'],
                            dato['nombre_empleado'],
                            dato['num_monto'],
                            dato['ind_nombre_detalle'],
                            estado,
                            btn_info,
                            '{if in_array('RH-01-02-05-02-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idBeneficio="'+dato['idBeneficio']+'"' +
                            'descripcion="Modificar Beneficio Utiles" titulo="Modificar Beneficio Utiles">' +
                            '<i class="fa md-edit" style="color: #ffffff;"></i></button>{/if}',
                            '{if in_array('RH-01-02-05-02-04-V',$_Parametros.perfil)}<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idBeneficio="'+dato['idBeneficio']+'" ' +
                            'data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Ver Datos Beneficio Utiles" titulo="Ver Datos Beneficio Utiles"> ' +
                            '<i class="md md-search" style="color: #ffffff;"></i></button>{/if}',
                            '{if in_array('RH-01-02-05-02-05-RV',$_Parametros.perfil)}'+btn_revisar+'{/if}',
                            '{if in_array('RH-01-02-05-02-03-G',$_Parametros.perfil)}'+btn_orden+'{/if}'

                        ]).draw()
                                .nodes()
                                .to$()
                                .addClass();

                        $(fila)
                                .attr('id','idBeneficio'+dato['idBeneficio']+'')
                                .css('font-size','11px')


                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        });
                    }
                },'json');

            }
            if($("#proceso").val()=='Modificar'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ModificarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        //swal("Error",respuesta['detalleERROR'], "error");
                    }
                    if(dato['status']=='modificar'){

                        swal("Exito","Operación Realizada con Exito!!!.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                    }
                },'json');
            }
            if($("#proceso").val()=='Conformar'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ConformarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='conformado'){

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $(location).attr('href','{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ListadoConformarMET');
                        });

                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');


                    }
                },'json');
            }
            if($("#proceso").val()=='Aprobar'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/AprobarMET';
                $.post(url, datos ,function(dato){
                    if(dato['status']=='aprobado'){

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $(location).attr('href','{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/ListadoAprobarMET');
                        });
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');


                    }
                },'json');
            }
            if($("#proceso").val()=='Anular'){
                var url = '{$_Parametros.url}modRH/procesos/ceseReingresoCONTROL/AnularMET';
                swal({
                    title: 'Anular',
                    text: 'Esta seguro que desea anular este registro? \n La operación sera irreversible!',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Si, Anular',
                    closeOnConfirm: false
                }, function(){
                    $.post(url, datos ,function(dato){
                        if(dato['status']=='anular'){

                            swal("Exito","Operación Realizada con Exito!!!.", "success");
                            $('#cerrar').click();
                        }
                    },'json');

                });
            }

        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la ventana
        $('#modalAncho').css( "width", "65%" );

        var $url = '{$_Parametros.url}modRH/gestion/empleadosCONTROL/ConsultarEmpleadoMET';
        $('#buscarEmpleado').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', accion:'BENEFICIOS_UTILES_HIJOS' },function($dato){
                $('#ContenidoModal2').html($dato);
                $('#modalAncho2').css( "width", "75%" );
            });
        });

        if ( $.fn.dataTable.isDataTable( '#listaBeneficarios' ) ) {
            table = $('#listaBeneficarios').DataTable();
        }
        else {
            table = $('#listaBeneficarios').DataTable( {
                paging: false,
                searching: false
            } );
        }

        $("#accionBotonBeneficio").click(function(){


           var num_filas = $('#listaBeneficarios >tbody >tr').length;
           var filas = $('#listaBeneficarios >tbody >tr'); {*trae las filas del tbody*}

            for(var i=0; i<num_filas; i++){

                if(filas[i].cells[0].innerHTML == 'No Hay Registros Disponibles'  ){

                    swal({  {*Mensaje personalizado*}
                        title: 'Error',
                        text: 'Por lo menos debe registrar un Beneficiario',
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: 'Aceptar',
                        closeOnConfirm: true
                    });

                    return false;
                    break;
                }

            }

        });

        /*boton agregar beneficiario*/
        $("#AgregarBeneficiario").click(function(){

            var aux = $("#hijos").val();
            var monto = $("#monto").val();
            var monto_t = $("#monto_t").val();

            monto = monto.replace(/[^,\d]/g, '');
            monto = monto.replace("BS", "");


            var hijos = aux.split("-");
            var hijos_id = hijos[0];
            var hijos_nom = hijos[1];

            if(aux==''){
                swal("Advertencia!","Debe Seleccionar Hijos.", "warning");
                return false;
            }
            if(monto==''){
                swal("Advertencia!","Debe Introducir Monto.", "warning");
                return false;
            }

            var info = '<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>';

            var carga_familiar = $("#carga").val();
            var monto_asignado = $("#monto_a").val();

            carga_familiar = carga_familiar + hijos_id +'#';
            monto_asignado = monto_asignado + monto +'#';


            var num_filas = $('#listaBeneficarios >tbody >tr').length;
            var filas = $('#listaBeneficarios >tbody >tr'); {*trae las filas del tbody*}
            var band = 0;
            for(var i=0; i<num_filas; i++){
                if(filas[i].cells[0].innerHTML == hijos_id  ){

                    swal({  {*Mensaje personalizado*}
                        title: 'Registro Repetido',
                        text: 'Ya está agregado',
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: 'Aceptar',
                        closeOnConfirm: true
                    });

                    band = 1;
                    break;
                }
            }

            if(band==0){

                var tabla = $('#listaBeneficarios').DataTable();
                //tabla.clear().draw();

                var fila = tabla.row.add([
                    hijos_id,
                    hijos_nom,
                    monto,
                    info
                ]).draw()
                        .nodes()
                        .to$()
                        .addClass();

                $("#carga").val(carga_familiar);
                $("#monto_a").val(monto_asignado);
                $("#hijos").val('');
                $("#monto").val('0,00');
                var monto_t = parseFloat(monto_t) + parseFloat(monto);

            }

            //monto_t = monto_t + monto;

            $("#monto_t").val(monto_t);



        });

        var tabla = $('#listaBeneficarios').DataTable();
        {* ************************************************ *}
        $('#listaBeneficarios tbody').on( 'click', '.eliminar', function () {

            var obj = this;

            swal({
                title:'Estas Seguro?',
                text: 'Estas seguro que desea descartar al beneficiario(a)',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Descartar",
                closeOnConfirm: false
            }, function(){

                tabla.row( $(obj).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}
                swal("Descartado!", "Descartado de la lista de beneficiarios", "success");


                {*verifico la tablas donde estan los empleados para volver armar el arreglo de empleados*}
                var num_filas = $('#listaBeneficarios >tbody >tr').length;
                var filas = $('#listaBeneficarios >tbody >tr'); {*trae las filas del tbody*}


                var cad_id_beneficiarios = '';
                var monto_asignado = '';
                var monto = 0;
                $('#carga').val('');
                $('#monto_a').val('');
                $('#monto_t').val('0,00');

                for(var i=0; i<num_filas; i++) {

                    {*voy armando arreglo de beneficiarios*}

                    cad_id_beneficiarios = cad_id_beneficiarios + filas[i].cells[0].innerHTML + '#';

                    monto_asignado = monto_asignado + filas[i].cells[2].innerHTML + '#';

                    monto = parseFloat(monto) + parseFloat(filas[i].cells[2].innerHTML);

                }
                {*asigno arreglo de empleados*}
                $('#carga').val(cad_id_beneficiarios);
                $('#monto_t').val(monto);
                $('#monto_a').val(monto_asignado);


            });
        });
        {* **************************************************** *}



        /*al selecionar tipo de asigancion*/
        $("#fk_a006_num_miscelaneo_detalle_asignacion").change(function(){

            var aux  = $(this).val();
            var tipo = aux.split("-");

            if(tipo[1]=='AF'){
                $("#lg_b022_num_proveedor").removeAttr('disabled');
                $("#txt_observacion_pre").attr('disabled','disabled');

            }else{
                $("#lg_b022_num_proveedor").attr('disabled','disabled');
                $("#txt_observacion_pre").removeAttr('disabled');
            }

        });


        //al dar click en el boton requisitos del listado de hijos de la carga familiar
        var $url_req = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/IngresarRequisitosMET';
        $('#cargaFamiliarHijos tbody').on( 'click', '.requisitosBeneficiario', function () {
            $('#formModalLabel3').html('Registrar Requisitos: '+$(this).attr('tituloReq'));
            $('#carga').val($(this).attr('pk_num_carga'));

            $.post($url_req,{ idEmpleado: $(this).attr('idEmpleado'), idCargaFamiliar: $(this).attr('pk_num_carga') },function(dato){
                $('#ContenidoModal3').html(dato);

                var data_b = $("#d_requisitos").val();
                var data_r = $("#t_requisitos").val();

                var arr_b  = data_b.split("#");
                var arr_r  = data_r.split("#");

                for (i=0; i<=arr_b.length; i++){
                    if(arr_b[i] == $('#carga').val()){
                        $("#fk_a006_num_miscelaneo_detalle_requisito").val(arr_r[i]);
                        $("#num_flag_entregado").prop("checked", true);
                    }
                }
            });
        });




    });

</script>