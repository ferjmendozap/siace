<form action="" class="form" role="form">

<div class="modal-body">

    <!--INFORMACION DE PREPARACION-->
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="preparadoPor" name="preparadoPor"  value="{if isset($formDBoperacionesPR[0].nombre_registro)}{$formDBoperacionesPR[0].nombre_registro}{/if}">
                <label for="preparadoPor">Preparado Por</label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="FechaPreparadoPor" name="FechaPreparadoPor"  value="{if isset($formDBoperacionesPR[0].fec_operacion)}{$formDBoperacionesPR[0].fec_operacion|date_format:"d-m-Y"}{/if}">
                <label for="FechaPreparadoPor">Fecha</label>
            </div>
        </div>
    </div>

    <!--INFORMACION DE REVISION-->
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="revisadoPor" name="revisadoPor"  value="{if isset($formDBoperacionesRV[0].nombre_registro)}{$formDBoperacionesRV[0].nombre_registro}{/if}">
                <label for="revisadoPor">Revisado Por</label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="FechaRevisadoPor" name="FechaRevisadoPor"  value="{if isset($formDBoperacionesRV[0].fec_operacion)}{$formDBoperacionesRV[0].fec_operacion|date_format:"d-m-Y"}{/if}">
                <label for="FechaRevisadoPor">Fecha</label>
            </div>
        </div>
    </div>

    <!--INFORMACION DE CONFORMACION-->
    <!--<div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="conformadoPor" name="conformadoPor"  value="{if isset($formDBoperacionesCO[0].nombre_registro)}{$formDBoperacionesCO[0].nombre_registro}{/if}" >
                <label for="conformadoPor">Conformado Por</label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="FechaConformadoPor" name="FechaConformadoPor"  value="{if isset($formDBoperacionesCO[0].fec_operacion)}{$formDBoperacionesCO[0].fec_operacion|date_format:"d-m-Y"}{/if}">
                <label for="FechaConformadoPor">Fecha</label>
            </div>
        </div>
    </div>-->

    <!--INFORMACION DE APROBACION-->
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="aprobadoPor" name="aprobadoPor"  value="{if isset($formDBoperacionesAP[0].nombre_registro)}{$formDBoperacionesAP[0].nombre_registro}{/if}">
                <label for="aprobadoPor">Aprobado Por</label>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="FechaAprobadoPor" name="FechaAprobadoPor"  value="{if isset($formDBoperacionesAP[0].fec_operacion)}{$formDBoperacionesAP[0].fec_operacion|date_format:"d-m-Y"}{/if}">
                <label for="FechaAprobadoPor">Fecha</label>
            </div>
        </div>
    </div>

    <div class="row">
        <div align="center">
            <button type="button" class="btn btn-default ink-reaction btn-raised" descripcionModal="Aceptar" data-dismiss="modal">Aceptar</button>
        </div>
    </div>

</div>

</form>


<script type="text/javascript">

    $(document).ready(function() {

        $('#modalAncho').css( "width", "40%" );

    });

</script>