<div class="modal-body">

        <div class="row">



                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >


                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idAsignacion}" name="idAsignacion" id="idAsignacion"/>
                    <input type="hidden" value="{$idBeneficio}" name="idBeneficio" id="idBeneficio"/>
                    <input type="hidden" name="proceso"  id="proceso" value="{$proceso}">

                    <div class="row contain-lg">
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="orden" name="orden" value="{if isset($formDB.num_orden)}{$formDB.num_orden}{else}{$orden}{/if}" disabled>
                                <label for="num_orden">Nº Orden</label>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="beneficio" name="beneficio" value="{if isset($formDB.ind_descripcion_beneficio)}{$formDB.ind_descripcion_beneficio}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhc056_num_beneficio_utiles" name="form[int][fk_rhc056_num_beneficio_utiles]" value="{if isset($formDB.fk_rhc056_num_beneficio_utiles)}{$formDB.fk_rhc056_num_beneficio_utiles}{else}{$idBeneficioUtiles}{/if}" disabled>
                                <label for="fk_rhc056_num_beneficio_utiles">Beneficio Periodo</label>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="nombEmpleado" name="nombEmpleado" value="{if isset($formDB.nombre_funcionario)}{$formDB.nombre_funcionario}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhb001_num_empleado" name="form[int][fk_rhb001_num_empleado]" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{else}{$idEmpleado}{/if}" disabled>
                                <label for="fk_rhb001_num_empleado">Funcionario</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="nombHijo" name="nombHijo" value="{if isset($formDB.nombre_hijo)}{$formDB.nombre_hijo}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhc015_num_carga_familiar" name="form[int][fk_rhc015_num_carga_familiar]" value="{if isset($formDB.fk_rhc015_num_carga_familiar)}{$formDB.fk_rhc015_num_carga_familiar}{else}{$idCargaFamiliar}{/if}" disabled>
                                <label for="fk_rhc015_num_carga_familiar">Hijo</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="nivelGradoInstruccion" name="nivelGradoInstruccion" value="{if isset($formDB.ind_nombre_nivel_grado)}{$formDB.ind_nombre_nivel_grado}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhc064_nivel_grado_instruccion" name="form[int][fk_rhc064_nivel_grado_instruccion]" value="{if isset($formDB.fk_rhc064_nivel_grado_instruccion)}{$formDB.fk_rhc064_nivel_grado_instruccion}{else}{$idNivelGradoInstruccion}{/if}" disabled>
                                <label for="fk_rhc064_nivel_grado_instruccion">Grado Escolar</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="num_factura" name="form[txt][num_factura]" value="{if isset($formDB.num_factura)}{$formDB.num_factura}{/if}" disabled>
                                <label for="num_factura">Factura</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group control-width-normal">
                                <div class="input-group date" id="fecha">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm" id="fec_fecha_factura" name="form[txt][fec_fecha_factura]" value="{if isset($formDB.fec_fecha_factura)}{$formDB.fec_fecha_factura|date_format:"d-m-Y"}{/if}" disabled>
                                        <label>Fecha Factura</label>
                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                    </div>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div><!--end .form-group -->
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <!--<div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm text-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}">
                                <label for="txt_observacion_pre">Observaciones Preparado</label>
                            </div>
                        </div>-->

                        <!-- estado preparado -->
                        {if $formDB.ind_estado=='PR'}
                            <div class="row contain-lg">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                        <label for="txt_observacion_pre">Observaciones Preparado</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row contain-lg">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                        <label for="txt_observacion_rev">Observaciones Revisado</label>
                                    </div>
                                </div>
                            </div>
                        {/if}

                        <!-- estado revisado -->
                        {if $formDB.ind_estado=='RV'}
                            <div class="row contain-lg">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                        <label for="txt_observacion_pre">Observaciones Preparado</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row contain-lg">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                        <label for="txt_observacion_rev">Observaciones Revisado</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row contain-lg">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apr" name="form[txt][txt_observacion_apr]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                        <label for="txt_observacion_apr">Observaciones Aprobado</label>
                                    </div>
                                </div>
                            </div>
                        {/if}

                        <!-- estado aprobado -->
                        {if $formDB.ind_estado=='AP'}
                            <div class="row contain-lg">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                        <label for="txt_observacion_pre">Observaciones Preparado</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row contain-lg">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                        <label for="txt_observacion_rev">Observaciones Revisado</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row contain-lg">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apr" name="form[txt][txt_observacion_apr]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                        <label for="txt_observacion_apr">Observaciones Aprobado</label>
                                    </div>
                                </div>
                            </div>
                        {/if}
                    </div>



                    <div class="row contain-lg">
                        <table class="table" width="100%" cellspacing="0" style="margin-bottom: 0px">
                            <thead>
                                <tr align="center" style="width: 100%">
                                    <th class="col-sm-1">#</th>
                                    <th class="col-sm-1">Cant.</th>
                                    <th class="col-sm-5">Descripción</th>
                                    <th class="col-sm-1">Tipo</th>
                                    <th class="col-sm-1 text-right">Monto</th>
                                    <th class="col-sm-1">Acción</th>
                                </tr>
                            </thead>
                        </table>
                        <div id="div1">
                            <!--<table class="table table-condensed table-hover listaUtilesAsignacion" width="100%" cellspacing="0">
                                <tbody>

                                </tbody>
                            </table>-->
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="exento" name="form[int][exento]" value="{if isset($formDB.num_excento)}{$formDB.num_excento|number_format:2:",":"."}{/if}">
                                <label for="exento" class="text-medium">Exento</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="bi_g" name="form[int][bi_g]" value="{if isset($formDB.num_base_imponible)}{$formDB.num_base_imponible|number_format:2:",":"."}{/if}">
                                <label for="bi_g" class="text-medium">BI G 12,00%</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="iva_g" name="form[int][iva_g]" value="{if isset($formDB.num_iva)}{$formDB.num_iva|number_format:2:",":"."}{/if}">
                                <label for="iva_g" class="text-medium">IVA G 12,00%</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="total" name="form[int][total]" value="{if isset($formDB.num_monto)}{$formDB.num_monto|number_format:2:",":"."}{/if}">
                                <label for="total" class="text-medium">TOTAL</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <br>
                        <div align="right">
                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accionBotonBeneficio">Revisar </button>
                            <button type="button" class="btn btn-danger ink-reaction btn-raised logsUsuarioModal" id="accionAnular"><span class="md md-thumb-down"></span> Anular </button>
                        </div>
                        <br>
                    </div>
                </form>

        </div>

</div>




<style>
    .letra_pequena{
        font-size: 11px;
    }
    #div1 {
        overflow:scroll;
        height:120px;
        width:100%;
        margin-top: 0px;
    }

</style>

<script type="text/javascript">

    //inicializar el datepicker
    $("#fecha").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });


    var OBJ_BENEFICIO_UTILES = new itemUtiles();

    function itemUtiles()
    {
        /*
         inicializar variables
         */
        this.codigo     	= new Array();
        this.cant			= new Array();
        this.descripcion	= new Array();
        this.tipo   		= new Array();
        this.monto      	= new Array();
    }

    function eliminarItem(i)
    {
        OBJ_BENEFICIO_UTILES.codigo.splice(i,1);
        OBJ_BENEFICIO_UTILES.cant.splice(i,1);
        OBJ_BENEFICIO_UTILES.descripcion.splice(i,1);
        OBJ_BENEFICIO_UTILES.tipo.splice(i,1);
        OBJ_BENEFICIO_UTILES.monto.splice(i,1);
        j = OBJ_BENEFICIO_UTILES.codigo.length;
        mostrarItemBeneficio(j,'');
    }

    function agregarItem()
    {

        var i= OBJ_BENEFICIO_UTILES.codigo.length;


        OBJ_BENEFICIO_UTILES.cant[i] 		= $('#cantidad').val()+' ';
        OBJ_BENEFICIO_UTILES.codigo[i] 	    = $('#id_descripcion').val()+' ';
        OBJ_BENEFICIO_UTILES.descripcion[i] = $('#descripcion').val()+' ';
        OBJ_BENEFICIO_UTILES.monto[i]		= $('#monto').val()+' ';
        OBJ_BENEFICIO_UTILES.tipo[i]		= $('#tipo').val();

        i= OBJ_BENEFICIO_UTILES.codigo.length;
        mostrarItemBeneficio(i,'');
        limpiarCamposItem();
    }

    function mostrarItemBeneficio(i,cond)
    {

        total='0.00';

        var total=0;
        var totalExento =0;
        var iva=0;
        var monto=0;
        var exento =0;
        var totalImponible =0;


        var tabla = document.getElementById('div1');

        var montoDis = 0.00;

        var tbl = document.createElement("table");
        tbl.width = '100%';
        tbl.setAttribute('border','0px');
        tbl.setAttribute("cellpadding", "1px");
        tbl.setAttribute("cellspacing", "1px");

        var tblBody = document.createElement("tbody");


        if(i==0)// no existe
        {
            total='0.00';

            var row = document.createElement("tr");
            var br = document.createElement("br");
            var cell1 = document.createElement("td");
            cell1.width='100%';
            cell1.setAttribute('align','center');
            var cellText1 =document.createTextNode('No existe movimiento');
            cell1.appendChild(br);
            cell1.appendChild(cellText1);

            row.appendChild(cell1);
            tblBody.appendChild(row);
            tbl.appendChild(tblBody);
            tabla.innerHTML='';
            tabla.appendChild(tbl);

            tbl.setAttribute("border", "0");
            $('#exento').val(formatNumber.new(0)) ;
            $('#bi_g').val(formatNumber.new(0));
            $('#iva_g').val(formatNumber.new(0));
            $('#total').val(formatNumber.new(0));

        }
        else
        {


            for(var j=0;j<i;j++)
            {
                // creates a table row
                var row = document.createElement("tr");
                row.setAttribute("class","trListaBody");

                if(j%2==1) { row.setAttribute("style","background-color:#ccc"); }



                // Create a <td> element and a text node, make the text
                // node the contents of the <td>, and put the <td> at
                // the end of the table row
                var cell1 = document.createElement("td");
                var cell2 = document.createElement("td");
                var cell3 = document.createElement("td");
                var cell4 = document.createElement("td");
                var cell5 = document.createElement("td");
                var cell6 = document.createElement("td");


                cell1.width = '30px';cell1.setAttribute('align','left');
                cell2.width = '30px';cell2.setAttribute('align','left');
                cell3.width = '142px';cell3.setAttribute('align','left');
                cell4.width = '30px';cell4.setAttribute('align','right');
                cell5.width = '30px';cell5.setAttribute('align','right');
                cell6.width = '30px';cell6.setAttribute('align','center');



                monto = OBJ_BENEFICIO_UTILES.monto[j].replace(/[^,\d]/g, '');


                var cellText1 = document.createTextNode(j+1);
                var cellText2 = document.createTextNode(OBJ_BENEFICIO_UTILES.cant[j]);
                var cellText3 = document.createTextNode(OBJ_BENEFICIO_UTILES.descripcion[j]);
                var cellText4 = document.createTextNode(OBJ_BENEFICIO_UTILES.tipo[j]);
                var cellText5 = document.createTextNode(monto);
                //var cellText6 = document.createTextNode(info);
                var cellText6 = document.createElement('i');
                    cellText6.setAttribute('class','md md-delete');
                    cellText6.setAttribute('title','Eliminar Item');
                    cellText6.setAttribute('style','cursor:pointer');
                    cellText6.setAttribute('onclick','eliminarItem('+j+')');


                if(OBJ_BENEFICIO_UTILES.tipo[j]=='E')
                {
                    exento = OBJ_BENEFICIO_UTILES.monto[j].replace(/[^,\d]/g, '');
                    exento = quitarMiles(exento);
                    exento = exento.replace(',','.');
                    exento = Math.round (exento * 100) / 100;
                    totalExento = totalExento+exento;
                    exento = parseFloat(exento);
                    //	totalExento = totalExento+exento;

                }
                else
                {
                    monto = OBJ_BENEFICIO_UTILES.monto[j].replace(/[^,\d]/g, '');
                    monto = quitarMiles(monto);
                    monto = monto.replace(',','.');
                    monto = Math.round (monto * 100) / 100;
                    monto = parseFloat(monto);



                    totalImponible = totalImponible+monto;

                    iva = iva+(monto*0.12);

                }



                //total = total + monto-descuento;
                /////////////////
                totalExento = parseFloat(totalExento);
                totalImponible = parseFloat(totalImponible);
                iva=parseFloat(iva);

                cell1.appendChild(cellText1);
                cell2.appendChild(cellText2);
                cell3.appendChild(cellText3);
                cell4.appendChild(cellText4);
                cell5.appendChild(cellText5);
                cell6.appendChild(cellText6);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);
                row.appendChild(cell5);
                row.appendChild(cell6);

                tblBody.appendChild(row);

            }


            tblBody.setAttribute('border','0px');
            tbl.appendChild(tblBody);

            tabla.innerHTML='';
            tabla.appendChild(tbl);

        }

        total = totalExento+totalImponible+iva;
        //total=parseFloat(total);

        iva = iva.toFixed(2);
        totalExento = totalExento.toFixed(2);
        totalImponible = totalImponible.toFixed(2);
        total = total.toFixed(2);

        $('#exento').val(formatNumber.new(totalExento));
        $('#bi_g').val(formatNumber.new(totalImponible));
        $('#iva_g').val(formatNumber.new(iva));
        $('#total').val(formatNumber.new(total));


    }

    function limpiarCamposItem()
    {
        $('#cantidad').val('');
        $('#descripcion').val('');
        $('#id_descripcion').val('');
        $('#tipo').val('G');
        $('#monto').val('');
    }

    var formatNumber = {
        separador: ".", // separador para los miles
        sepDecimal: ',', // separador para los decimales
        formatear:function (num){
            num +=' ';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
            }
            return this.simbol + splitLeft  +splitRight;
        },
        new:function(num, simbol){
            this.simbol = simbol ||'';
            return this.formatear(num);
        }
    }

    function quitarMiles(valor)
    {
        while (valor.indexOf('.')>-1) {
            pos= valor.indexOf('.');
            valor = "" + (valor.substring(0, pos) + '' +
                    valor.substring((pos + '.'.length), valor.length));
        }
        return valor;
    }

    /*permite buscar los items de beneficio*/
    function buscarItemBeneficio(idAsignacion)
    {

        var url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/BuscarAsignacionDetalleMET';

        $.post(url, { idAsignacion: idAsignacion } ,function(dato){


            for(var i=0; i<dato.length;i++)
            {

                OBJ_BENEFICIO_UTILES.cant[i] 		= dato[i]['num_cantidad']+" ";
                OBJ_BENEFICIO_UTILES.codigo[i] 	    = dato[i]['fk_a006_num_miscelaneo_detalle_utiles']+" ";
                OBJ_BENEFICIO_UTILES.descripcion[i] = dato[i]['ind_descripcion_utiles']+" ";
                OBJ_BENEFICIO_UTILES.monto[i]		= toCurrency(dato[i]['nom_monto'])+" ";
                OBJ_BENEFICIO_UTILES.tipo[i]		= dato[i]['ind_exento'];

                var tam= OBJ_BENEFICIO_UTILES.codigo.length;

            }

            mostrarItemBeneficio(tam,'');


        },'json');

    }

    function toCurrency(cnt){
        cnt = cnt.toString().replace(/\$|\,/g,'');
        if (isNaN(cnt))
            return 0;
        var sgn = (cnt == (cnt = Math.abs(cnt)));
        cnt = Math.floor(cnt * 100 + 0.5);
        cvs = cnt % 100;
        cnt = Math.floor(cnt / 100).toString();
        if (cvs < 10)
            cvs = '0' + cvs;
        for (var i = 0; i < Math.floor((cnt.length - (1 + i)) / 3); i++)
            cnt = cnt.substring(0, cnt.length - (4 * i + 3)) + '.' + cnt.substring(cnt.length - (4 * i + 3));
        return (((sgn) ? '' : '-') + cnt + ',' + cvs);
    }

    /**********************************************************************************************************************************************/
    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();


            if($("#proceso").val()=='RevisarAsignacion'){

                var url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/RevisarAsignacionMET';
                $.post(url, datos ,function(dato){

                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        //swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        swal("Error",dato['detalleERROR'], "error");
                    }
                    if(dato['status']=='revisado'){



                        var btn_aprobar = '<button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAsignacion="'+dato['idAsignacion']+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Aprobar Asignación de Utiles" titulo="Aprobar Asignación de Utiles">APROBAR</button>';

                        var tabla = $('#datatable1').DataTable();

                        tabla.row('#idAsignacion'+dato['idAsignacion']+'').remove().draw();

                        var fila = tabla.row.add([
                            dato['num_factura'],
                            dato['ind_descripcion_beneficio'],
                            dato['nombre_funcionario'],
                            dato['nombre_carga_familiar'],
                            dato['num_monto'],
                            'Revisado',
                            '<button class="infoReg logsUsuario btn ink-reaction btn-raised btn-xs btn- btn-danger" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idAsignacion="'+dato['idAsignacion']+'" descripcion="Consultar operaciones" titulo="Información de Registro"><i class="md md-people" style="color: #ffffff;"></i></button>',
                            '',
                            '<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idAsignacion="'+dato['idAsignacion']+'" data-toggle="modal"  data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Ver Datos Asignación Utiles" titulo="Ver Datos Asignación Utiles"><i class="md md-search" style="color: #ffffff;"></i></button>',
                            '{if in_array('RH-01-02-05-03-05-AP',$_Parametros.perfil)}'+btn_aprobar+'{/if}'

                        ]).draw()
                                .nodes()
                                .to$()
                                .addClass();

                        $(fila)
                                .attr('id','idAsignacion'+dato['idAsignacion']+'')
                                .css('font-size','11px')

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        });

                    }

                },'json');


            }


            if($("#proceso").val()=='AprobarAsignacion'){

                var url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/AprobarAsignacionMET';
                $.post(url, datos ,function(dato){

                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        //swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        swal("Error",dato['detalleERROR'], "error");
                    }
                    if(dato['status']=='aprobado'){


                        var btn_imprimir = '<button class="imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAsignacion="'+dato['idAsignacion']+'" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Imprimir Asignación de Utiles" titulo="Imprimir Asignación de Utiles">Imprimir</button>';

                        var tabla = $('#datatable1').DataTable();

                        tabla.row('#idAsignacion'+dato['idAsignacion']+'').remove().draw();

                        var fila = tabla.row.add([
                            dato['num_factura'],
                            dato['ind_descripcion_beneficio'],
                            dato['nombre_funcionario'],
                            dato['nombre_carga_familiar'],
                            dato['num_monto'],
                            'Aprobado',
                            '<button class="infoReg logsUsuario btn ink-reaction btn-raised btn-xs btn- btn-danger" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idAsignacion="'+dato['idAsignacion']+'" descripcion="Consultar operaciones" titulo="Información de Registro"><i class="md md-people" style="color: #ffffff;"></i></button>',
                            '',
                            '<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idAsignacion="'+dato['idAsignacion']+'" data-toggle="modal"  data-target="#formModal" data-keyboard="false" data-backdrop="static" descripcion="Ver Datos idAsignacion Utiles" titulo="Ver Datos idAsignacion Utiles"><i class="md md-search" style="color: #ffffff;"></i></button>',
                            btn_imprimir,

                        ]).draw()
                                .nodes()
                                .to$()
                                .addClass();

                        $(fila)
                                .attr('id','idAsignacion'+dato['idAsignacion']+'')
                                .css('font-size','11px')

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        });

                    }

                },'json');

            }



            if($("#proceso").val()=='ModificarAsignacion'){


                var parametros = '&cantidadItem='+OBJ_BENEFICIO_UTILES.cant+'&idItem='+OBJ_BENEFICIO_UTILES.codigo+'&descripcionItem='+OBJ_BENEFICIO_UTILES.descripcion+'&montoItem='+OBJ_BENEFICIO_UTILES.monto+'&gravableItem='+OBJ_BENEFICIO_UTILES.tipo;

                var url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ModificarAsignacionMET';

                var data = datos+parametros;

                $.post(url, data ,function(dato){


                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        //swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        swal("Error",dato['detalleERROR'], "error");
                    }

                    if(dato['status']=='modificado'){

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            //cierro modal
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                            //cargo vista listado de asignaciones de utiles
                            $.post('{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ListarAsignacionUtilesMET',{  },function(dato){
                                $('#_contenido').html(dato);
                            });
                        });

                    }

                },'json');

            }

        }
    });
    /**********************************************************************************************************************************************/


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la ventana
        $('#modalAncho').css( "width", "65%" );

        $("#monto").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });

        var $url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ConsultarUtilesMET';
        $('#buscarUtiles').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', accion:'BENEFICIOS_UTILES_HIJOS' },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        /*busco los item para posible modificacion*/
        var idAsignacion = $("#idAsignacion").val();
        buscarItemBeneficio({$idAsignacion});


        /*boton agregar beneficiario*/
        $("#AgregarLinea").click(function(){

            var cantidad = $("#cantidad").val();
            var idUtiles = $("#id_descripcion").val();
            var Utiles   = $("#descripcion").val();
            var tipo     = $("#tipo").val();
            var monto    = $("#monto").val();

            /*llamo la funcion para agregar un nuevo Item*/
            if(cantidad=='') {
                swal("Advertencia!","Debe Indicar Cantidad", "warning");
                $("#cantidad").focus();
            }
            else if(Utiles=='') {
                swal("Advertencia!","Debe Seleccionar Utiles Escolares", "warning");
                $("#descripcion").focus();
            }
            else if(monto==''){
                swal("Advertencia!","Debe Introducir Monto.", "warning");
                $("#monto").focus();
            }else{
                agregarItem();
            }

        });

        /*boton anular*/
        $("#accionAnular").click(function(){

            var idAsignacion = $("#idAsignacion").val();
            var idBeneficio  = $("#idBeneficio").val();

            var $url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/AnularAsignacionMET';


            swal({
                title: 'Anular',
                text: 'Esta seguro que desea anular este registro?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Si, Anular',
                closeOnConfirm: false
            }, function(){

                $.post($url, { idBeneficio: idBeneficio, idAsignacion: idAsignacion } ,function(dato){

                    if(dato['status']=='anulado'){

                        swal("Exito","Operación Realizada con Exito!!!.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                        $('#cerrar').click();
                        $.post('{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ListarAsignacionUtilesMET',{  },function(dato){
                            $('#_contenido').html(dato);
                        });
                    }

                },'json');

            });




        });

        if($("#proceso").val()=='RevisarAsignacion'){
            $(".form-control").attr("disabled","disabled");
            $("#buscarEmpleado").attr("disabled","disabled");
            $("#txt_observacion_rev").removeAttr('disabled');
            $("#accionBotonBeneficio").html('Revisar');
        }


        if($("#proceso").val()=='AprobarAsignacion'){
            $(".form-control").attr("disabled","disabled");
            $("#buscarEmpleado").attr("disabled","disabled");
            $("#txt_observacion_apr").removeAttr('disabled');
            $("#accionBotonBeneficio").html('Aprobar');
        }

    });



</script>