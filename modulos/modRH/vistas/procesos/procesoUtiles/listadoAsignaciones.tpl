<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado Asignación Beneficios de Utiles Escolares</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">


                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="col-sm-1">Factura</th>
                                    <th class="col-sm-2">Periodo</th>
                                    <th class="col-sm-3">Funcionario</th>
                                    <th class="col-sm-2">Hijo</th>
                                    <th class="col-sm-1">Monto</th>
                                    <th style="width: 4%;">Estatus</th>
                                    <th style="width: 4%;">Info</th>
                                    <th style="width: 4%;">Mod</th>
                                    <th style="width: 4%;">Ver</th>
                                    <th class="col-sm-1 text-center">Accion</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=dat from=$listadoAsignacionesUtiles}

                                    <tr id="idAsignacion{$dat.pk_num_utiles_asignacion}">
                                        <td>{$dat.num_factura}</td>
                                        <td>{$dat.ind_descripcion_beneficio}</td>
                                        <td>{$dat.nombre_funcionario}</td>
                                        <td>{$dat.nombre_carga_familiar}</td>
                                        <td>{$dat.num_monto|number_format:2:",":"."}</td>
                                        <td>
                                            {if $dat.ind_estado=='PR'}
                                                Preparado
                                            {elseif $dat.ind_estado=='RV'}
                                                Revisado
                                            {elseif $dat.ind_estado=='AP'}
                                                Aprobado
                                            {else}
                                                Anulado
                                            {/if}
                                        </td>

                                        <td>
                                            <button class="infoReg logsUsuario btn ink-reaction btn-raised btn-xs btn- btn-danger" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idAsignacion="{$dat.pk_num_utiles_asignacion}"
                                                    descipcion="El Usuario a consultado operaciones" titulo="Información de Registro">
                                                <i class="md md-people" style="color: #ffffff;"></i>
                                            </button>
                                        </td>
                                        <td>
                                            {if in_array('RH-01-02-05-03-02-M',$_Parametros.perfil) && $dat.ind_estado=='PR'}
                                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  idAsignacion="{$dat.pk_num_utiles_asignacion}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descipcion="Modificar Asignación Beneficio Utiles" title="Modificar Asignación" titulo="Modificar Asignación Beneficio Utiles">
                                                    <i class="fa md-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                        <td>
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idAsignacion="{$dat.pk_num_utiles_asignacion}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="Ver Datos Asignación Utiles" title="Ver Datos Asignación" titulo="Ver Datos Asignación Utiles">
                                                <i class="fa md-search" style="color: #ffffff;"></i>
                                            </button>
                                        </td>
                                        <td class="col-sm-1 text-center">
                                            {if $dat.ind_estado=='PR'}
                                                {if in_array('RH-01-02-05-03-03-RV',$_Parametros.perfil)}
                                                <button class="revisar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idAsignacion="{$dat.pk_num_utiles_asignacion}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descripcion="Revisar Asignación Beneficio de Utiles" title="Revisar Asignación" titulo="Revisar Asignación Beneficio de Utiles">
                                                    <i class="md md-done"></i> REVISAR
                                                </button>
                                                {/if}
                                            {elseif $dat.ind_estado=='RV'}
                                                {if in_array('RH-01-02-05-03-05-AP',$_Parametros.perfil)}
                                                <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idAsignacion="{$dat.pk_num_utiles_asignacion}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descripcion="Aprobar Asignación Beneficio de Utiles" title="Aprobar Asignación" titulo="Aprobar Asignación Beneficio de Utiles">
                                                    <i class="md md-done-all"></i> APROBAR
                                                </button>
                                                {/if}
                                            {else}
                                                {if $dat.ind_estado!='AN'}
                                                <button class="imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  idAsignacion="{$dat.pk_num_utiles_asignacion}" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        descripcion="Imprimir Asignación Beneficio de Utiles" title="Imprimir" titulo="Imprimir Asignación Beneficio de Utiles">
                                                    <i class="md md-print"></i> IMPRIMIR
                                                </button>
                                                {/if}
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url_requisito='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/NuevoRequisitoMET';
        var $url_beneficio='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/NuevoBeneficioMET';

        //NUEVO REQUISITO
        $('#nuevoReq').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_requisito,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //NUEVO REQUISITO
        $('#nuevoBen').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_beneficio,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //VER
        var $url_ver ='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/VerAsignacionMET';
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_ver, { idAsignacion: $(this).attr('idAsignacion')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //MODIFICAR
        var $url_modificar='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ModificarAsignacionMET';
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_modificar, { idAsignacion: $(this).attr('idAsignacion')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });


        //REVISAR
        var $url_revisar  ='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/RevisarAsignacionMET';
        $('#datatable1 tbody').on( 'click', '.revisar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_revisar, { idAsignacion: $(this).attr('idAsignacion')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //APROBAR
        var $url_aprobar  ='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/AprobarAsignacionMET';
        $('#datatable1 tbody').on( 'click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_aprobar, { idAsignacion: $(this).attr('idAsignacion')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //INFORMACION DE REGISTRO
        var $url_info_reg ='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/InformacionRegistroAsignacionesMET';
        $('#datatable1 tbody').on( 'click', '.infoReg', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_info_reg, { idAsignacion: $(this).attr('idAsignacion')  } ,function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //IMPRIMIR ASIGNACION DE UTILES
        $('#datatable1 tbody').on( 'click', '.imprimir', function () {
            var $url_reporte='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ImprimirAsignacionUtilesMET/'+$(this).attr('idAsignacion')+'';
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html('<iframe src="'+$url_reporte+'" width="100%" height="950"></iframe>');
        });

    });
</script>