<div class="modal-body">

    <input type="hidden" value="{if isset($idMaestro.fk_a005_num_miscelaneo_maestro)}{$idMaestro.fk_a005_num_miscelaneo_maestro}{/if}" name="idMaestro" id="idMaestro"/>
    <table id="datatableUtiles" class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="col-sm-1">Id</th>
            <th class="col-sm-11">Descripción Utiles</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        {foreach item=f from=$Utiles}



            <tr id="idUtiles{$f.pk_num_miscelaneo_detalle}">
                <td>{$f.pk_num_miscelaneo_detalle}</td>
                <td>{$f.ind_nombre_detalle}</td>
                <td>
                    <button type="button" idUtiles="{$f.pk_num_miscelaneo_detalle}" nombUtiles="{$f.ind_nombre_detalle}" class="cargar btn ink-reaction btn-raised btn-xs btn-primary"  title="Cargar Utiles"><i class="glyphicon glyphicon-download"></i></button>
                </td>
            </tr>




        {/foreach}
        </tbody>

    </table>

    <div class="row">
        <div align="center">
            <button type="button" class="btn btn-default ink-reaction btn-raised" descripcionModal="Aceptar" data-dismiss="modal">Aceptar</button>
        </div>
    </div>

</div>


<script type="text/javascript">

    $(document).ready(function() {

        $('#modalAncho2').css( "width", "50%" );

        $('#datatableUtiles').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatableUtiles tbody').on( 'click', '.cargar', function () {

            var idUtiles = ($(this).attr('idUtiles'));
            var Utiles   = ($(this).attr('nombUtiles'));


            $("#descripcion").val(Utiles);
            $("#id_descripcion").val(idUtiles);

            /***************************************************************************************/
            $(document.getElementById('cerrarModal2')).click();
            $(document.getElementById('ContenidoModal2')).html('');
            /***************************************************************************************/


        });

    });


</script>