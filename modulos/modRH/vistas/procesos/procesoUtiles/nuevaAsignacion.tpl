<div class="modal-body">

        <div class="row">



                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >


                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idAsignacion}" name="idAsignacion"/>
                    <input type="hidden" value="{$idBeneficio}" name="idBeneficio"/>
                    <input type="hidden" name="proceso"  id="proceso" value="{$proceso}">

                    <div class="row contain-lg">
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="orden" name="orden" value="{if isset($formDB.num_orden)}{$formDB.num_orden}{else}{$orden}{/if}" disabled>
                                <label for="num_orden">Nº Orden</label>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="beneficio" name="beneficio" value="{if isset($beneficio)}{$beneficio}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhc056_num_beneficio_utiles" name="form[int][fk_rhc056_num_beneficio_utiles]" value="{if isset($formDB.fk_rhc056_num_beneficio_utiles)}{$formDB.fk_rhc056_num_beneficio_utiles}{else}{$idBeneficioUtiles}{/if}" disabled>
                                <label for="fk_rhc056_num_beneficio_utiles">Beneficio Periodo</label>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="nombEmpleado" name="nombEmpleado" value="{if isset($nombEmpleado)}{$nombEmpleado}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhb001_num_empleado" name="form[int][fk_rhb001_num_empleado]" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{else}{$idEmpleado}{/if}" disabled>
                                <label for="fk_rhb001_num_empleado">Funcionario</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="nombHijo" name="nombHijo" value="{if isset($nombHijo)}{$nombHijo}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhc015_num_carga_familiar" name="form[int][fk_rhc015_num_carga_familiar]" value="{if isset($formDB.fk_rhc015_num_carga_familiar)}{$formDB.fk_rhc015_num_carga_familiar}{else}{$idCargaFamiliar}{/if}" disabled>
                                <label for="fk_rhc015_num_carga_familiar">Hijo</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="nivelGradoInstruccion" name="nivelGradoInstruccion" value="{if isset($nivelGradoInstruccion)}{$nivelGradoInstruccion}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhc064_nivel_grado_instruccion" name="form[int][fk_rhc064_nivel_grado_instruccion]" value="{if isset($formDB.fk_rhc064_nivel_grado_instruccion)}{$formDB.fk_rhc064_nivel_grado_instruccion}{else}{$idNivelGradoInstruccion}{/if}" disabled>
                                <label for="fk_rhc064_nivel_grado_instruccion">Grado Escolar</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="num_factura" name="form[txt][num_factura]" value="{if isset($formDB.num_factura)}{$formDB.num_factura}{/if}" required>
                                <label for="num_factura">Factura</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group control-width-normal">
                                <div class="input-group date" id="fecha">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm" id="fec_fecha_factura" name="form[txt][fec_fecha_factura]" value="{if isset($formDB.fec_fecha_factura)}{$formDB.fec_fecha_factura|date_format:"d-m-Y"}{/if}" required>
                                        <label>Fecha Factura</label>
                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                    </div>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div><!--end .form-group -->
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm text-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}">
                                <label for="txt_observacion_pre">Observaciones Preparado</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-1">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="cantidad" name="form[txt][cantidad]" value="">
                                <label for="cantidad">Cant.</label>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm"  id="descripcion" name="form[txt][descripcion]" value="">
                                        <input type="hidden" class="form-control input-sm"  id="id_descripcion" name="form[int][id_descripcion]" value="">
                                        <label for="descripcion">Descripción</label>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" id="buscarUtiles" titulo="Listado de Utiles Escolares" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <select id="tipo" name="form[txt][tipo]" class="form-control input-sm">
                                    <option value="G">G</option>
                                    <option value="E">E</option>
                                </select>
                                <label for="tipo">Tipo</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm"  id="monto" name="form[int][monto]" value="">
                                        <label for="monto">Monto</label>
                                    </div>
                                    <div class="input-group-btn">
                                        <button class="btn btn-sm btn-default" type="button" id="AgregarLinea">Agregar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <table class="table" width="100%" cellspacing="0" style="margin-bottom: 0px">
                            <thead>
                                <tr align="center" style="width: 100%">
                                    <th class="col-sm-1">#</th>
                                    <th class="col-sm-1">Cant.</th>
                                    <th class="col-sm-5">Descripción</th>
                                    <th class="col-sm-1">Tipo</th>
                                    <th class="col-sm-1">Monto</th>
                                    <th class="col-sm-1">Acción</th>
                                </tr>
                            </thead>
                        </table>
                        <div id="div1">
                            <!--<table class="table table-condensed table-hover listaUtilesAsignacion" width="100%" cellspacing="0">
                                <tbody>

                                </tbody>
                            </table>-->
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="exento" name="form[int][exento]" value="0,00">
                                <label for="exento" class="text-medium">Exento</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="bi_g" name="form[int][bi_g]" value="0,00">
                                <label for="bi_g" class="text-medium">BI G 12,00%</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="iva_g" name="form[int][iva_g]" value="0,00">
                                <label for="iva_g" class="text-medium">IVA G 12,00%</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="total" name="form[int][total]" value="0,00">
                                <label for="total" class="text-medium">TOTAL</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <br>
                        <div align="right">
                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>Cancelar</button>
                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accionBotonBeneficio"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar </button>
                        </div>
                        <br>
                    </div>
                </form>

        </div>

</div>




<style>
    .letra_pequena{
        font-size: 11px;
    }
    #div1 {
        overflow:scroll;
        height:120px;
        width:100%;
        margin-top: 0px;
    }

</style>

<script type="text/javascript">

    //inicializar el datepicker
    $("#fecha").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });


    var OBJ_BENEFICIO_UTILES = new itemUtiles();

    function itemUtiles()
    {
        /*
         inicializar variables
         */
        this.codigo     	= new Array();
        this.cant			= new Array();
        this.descripcion	= new Array();
        this.tipo   		= new Array();
        this.monto      	= new Array();
    }

    function eliminarItem(i)
    {
        OBJ_BENEFICIO_UTILES.codigo.splice(i,1);
        OBJ_BENEFICIO_UTILES.cant.splice(i,1);
        OBJ_BENEFICIO_UTILES.descripcion.splice(i,1);
        OBJ_BENEFICIO_UTILES.tipo.splice(i,1);
        OBJ_BENEFICIO_UTILES.monto.splice(i,1);
        j = OBJ_BENEFICIO_UTILES.codigo.length;
        mostrarItemBeneficio(j,'');
    }

    function agregarItem()
    {

        var i= OBJ_BENEFICIO_UTILES.codigo.length;

        OBJ_BENEFICIO_UTILES.cant[i] 		= $('#cantidad').val()+' ';
        OBJ_BENEFICIO_UTILES.codigo[i] 	    = $('#id_descripcion').val()+' ';
        OBJ_BENEFICIO_UTILES.descripcion[i] = $('#descripcion').val()+' ';
        OBJ_BENEFICIO_UTILES.monto[i]		= $('#monto').val()+' ';
        OBJ_BENEFICIO_UTILES.tipo[i]		= $('#tipo').val();

        i= OBJ_BENEFICIO_UTILES.codigo.length;
        mostrarItemBeneficio(i,'');
        limpiarCamposItem();

    }

    function mostrarItemBeneficio(i,cond)
    {
        total='0.00';

        var total=0;
        var totalExento =0;
        var iva=0;
        var monto=0;
        var exento =0;
        var totalImponible =0;


        var tabla = document.getElementById('div1');

        var montoDis = 0.00;

        var tbl = document.createElement("table");
        tbl.width = '100%';
        tbl.setAttribute('border','0px');
        tbl.setAttribute("cellpadding", "1px");
        tbl.setAttribute("cellspacing", "1px");

        var tblBody = document.createElement("tbody");


        if(i==0)// no existe
        {
            total='0.00';

            var row = document.createElement("tr");
            var br = document.createElement("br");
            var cell1 = document.createElement("td");
            cell1.width='100%';
            cell1.setAttribute('align','center');
            var cellText1 =document.createTextNode('No existe movimiento');
            cell1.appendChild(br);
            cell1.appendChild(cellText1);

            row.appendChild(cell1);
            tblBody.appendChild(row);
            tbl.appendChild(tblBody);
            tabla.innerHTML='';
            tabla.appendChild(tbl);

            tbl.setAttribute("border", "0");
            $('#exento').val(formatNumber.new(0)) ;
            $('#bi_g').val(formatNumber.new(0));
            $('#iva_g').val(formatNumber.new(0));
            $('#total').val(formatNumber.new(0));

        }
        else
        {


            for(var j=0;j<i;j++)
            {
                // creates a table row
                var row = document.createElement("tr");
                row.setAttribute("class","trListaBody");

                if(j%2==1) { row.setAttribute("style","background-color:#ccc"); }



                // Create a <td> element and a text node, make the text
                // node the contents of the <td>, and put the <td> at
                // the end of the table row
                var cell1 = document.createElement("td");
                var cell2 = document.createElement("td");
                var cell3 = document.createElement("td");
                var cell4 = document.createElement("td");
                var cell5 = document.createElement("td");
                var cell6 = document.createElement("td");


                cell1.width = '30px';cell1.setAttribute('align','left');
                cell2.width = '30px';cell2.setAttribute('align','left');
                cell3.width = '142px';cell3.setAttribute('align','left');
                cell4.width = '30px';cell4.setAttribute('align','center');
                cell5.width = '30px';cell5.setAttribute('align','right');
                cell6.width = '30px';cell6.setAttribute('align','center');



                monto = OBJ_BENEFICIO_UTILES.monto[j].replace(/[^,\d]/g, '');
                monto = monto.replace("BS", "");


                var cellText1 = document.createTextNode(j+1);
                var cellText2 = document.createTextNode(OBJ_BENEFICIO_UTILES.cant[j]);
                var cellText3 = document.createTextNode(OBJ_BENEFICIO_UTILES.descripcion[j]);
                var cellText4 = document.createTextNode(OBJ_BENEFICIO_UTILES.tipo[j]);
                var cellText5 = document.createTextNode(monto);
                //var cellText6 = document.createTextNode(info);
                var cellText6 = document.createElement('i');
                    cellText6.setAttribute('class','md md-delete');
                    cellText6.setAttribute('title','Eliminar Item');
                    cellText6.setAttribute('style','cursor:pointer');
                    cellText6.setAttribute('onclick','eliminarItem('+j+')');


                if(OBJ_BENEFICIO_UTILES.tipo[j]=='E')
                {
                    exento = OBJ_BENEFICIO_UTILES.monto[j].replace(/[^,\d]/g, '');
                    exento = exento.replace("BS", "");
                    exento = quitarMiles(exento);
                    exento = exento.replace(',','.');
                    exento = Math.round (exento * 100) / 100;
                    totalExento = totalExento+exento;
                    exento = parseFloat(exento);
                    //	totalExento = totalExento+exento;

                }
                else
                {
                    monto = OBJ_BENEFICIO_UTILES.monto[j].replace(/[^,\d]/g, '');
                    monto = monto.replace("BS", "");
                    monto = quitarMiles(monto);
                    monto = monto.replace(',','.');
                    monto = Math.round (monto * 100) / 100;
                    monto = parseFloat(monto);



                    totalImponible = totalImponible+monto;

                    iva = iva+(monto*0.12);

                }



                //total = total + monto-descuento;
                /////////////////
                totalExento = parseFloat(totalExento);
                totalImponible = parseFloat(totalImponible);
                iva=parseFloat(iva);

                cell1.appendChild(cellText1);
                cell2.appendChild(cellText2);
                cell3.appendChild(cellText3);
                cell4.appendChild(cellText4);
                cell5.appendChild(cellText5);
                cell6.appendChild(cellText6);

                row.appendChild(cell1);
                row.appendChild(cell2);
                row.appendChild(cell3);
                row.appendChild(cell4);
                row.appendChild(cell5);
                row.appendChild(cell6);

                tblBody.appendChild(row);

            }


            tblBody.setAttribute('border','0px');
            tbl.appendChild(tblBody);

            tabla.innerHTML='';
            tabla.appendChild(tbl);

        }

        total = totalExento+totalImponible+iva;
        //total=parseFloat(total);

        iva = iva.toFixed(2);
        totalExento = totalExento.toFixed(2);
        totalImponible = totalImponible.toFixed(2);
        total = total.toFixed(2);

        $('#exento').val(formatNumber.new(totalExento));
        $('#bi_g').val(formatNumber.new(totalImponible));
        $('#iva_g').val(formatNumber.new(iva));
        $('#total').val(formatNumber.new(total));


    }

    function limpiarCamposItem()
    {
        $('#cantidad').val('');
        $('#descripcion').val('');
        $('#id_descripcion').val('');
        $('#tipo').val('G');
        $('#monto').val('');
    }

    var formatNumber = {
        separador: ".", // separador para los miles
        sepDecimal: ',', // separador para los decimales
        formatear:function (num){
            num +=' ';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
            }
            return this.simbol + splitLeft  +splitRight;
        },
        new:function(num, simbol){
            this.simbol = simbol ||'';
            return this.formatear(num);
        }
    }

    function quitarMiles(valor)
    {
        while (valor.indexOf('.')>-1) {
            pos= valor.indexOf('.');
            valor = "" + (valor.substring(0, pos) + '' +
                    valor.substring((pos + '.'.length), valor.length));
        }
        return valor;
    }


    /**********************************************************************************************************************************************/
    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var formulario = $( "#formAjax" );

            var disabled = formulario.find(':input:disabled').removeAttr('disabled');

            var datos = formulario.serialize();


            var i= OBJ_BENEFICIO_UTILES.codigo.length;

            if(i==0){
                swal("Error","Debe registrar detalle de la factura", "error");
                return false;
            }


            if($("#proceso").val()=='NuevaAsignacion'){


                var parametros = '&cantidadItem='+OBJ_BENEFICIO_UTILES.cant+'&idItem='+OBJ_BENEFICIO_UTILES.codigo+'&descripcionItem='+OBJ_BENEFICIO_UTILES.descripcion+'&montoItem='+OBJ_BENEFICIO_UTILES.monto+'&gravableItem='+OBJ_BENEFICIO_UTILES.tipo;

                var url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/NuevaAsignacionMET';

                var data = datos+parametros;

                $.post(url, data ,function(dato){


                    if(dato['status']=='error'){
                        {*para usuarios normales*}
                        //swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        swal("Error",dato['detalleERROR'], "error");
                    }

                    if(dato['status']=='registrado'){

                        swal({
                            title: 'Exito',
                            text: 'Operación Realizada con Exito!!!.',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Aceptar',
                            closeOnConfirm: true
                        }, function(){
                            //cierro modal
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                            //cargo vista listado de asignaciones de utiles
                            $.post('{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ListarAsignacionUtilesMET',{  },function(dato){
                                $('#_contenido').html(dato);
                            });
                        });

                    }

                },'json');

            }

        }
    });
    /**********************************************************************************************************************************************/


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la ventana
        $('#modalAncho').css( "width", "65%" );

        $("#monto").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });

        /*$("#exento").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        $("#bi_g").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        $("#iva_g").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
        $("#total").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });*/

        var $url = '{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/ConsultarUtilesMET';
        $('#buscarUtiles').click(function(){
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { proceso: 'Nuevo', accion:'BENEFICIOS_UTILES_HIJOS' },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });



        /*boton agregar beneficiario*/
        $("#AgregarLinea").click(function(){

            var cantidad = $("#cantidad").val();
            var idUtiles = $("#id_descripcion").val();
            var Utiles   = $("#descripcion").val();
            var tipo     = $("#tipo").val();
            var monto    = $("#monto").val();

            /*llamo la funcion para agregar un nuevo Item*/
            if(cantidad=='') {
                swal("Advertencia!","Debe Indicar Cantidad", "warning");
                $("#cantidad").focus();
            }
            else if(Utiles=='') {
                swal("Advertencia!","Debe Seleccionar Utiles Escolares", "warning");
                $("#descripcion").focus();s
            }
            else if(monto==''){
                swal("Advertencia!","Debe Introducir Monto.", "warning");
                $("#monto").focus();
            }else{
                agregarItem();
            }

        });




    });

    function fnEliminarItem(valor)
    {

        $(document.getElementById('idItem'+valor)).html('');{*Elimina la fila en cuestion*}
        swal("Descartado!", "Descartado de la lista de Utiles", "success");

        fnActualizarMontos();

    }

    function fnActualizarMontos()
    {

        var num_filas = $('.listaUtilesAsignacion >tbody >tr').length;
        var filas = $('.listaUtilesAsignacion >tbody >tr'); {*trae las filas del tbody*}


        var exento = 0;
        var base = 0;
        var iva  = 0;
        var total = 0;
        var montoT = 0;

        //alert(filas);

        for(var i=0; i<num_filas; i++) {

            var tipo  = filas[i].cells[3].innerHTML;
            var monto = filas[i].cells[4].innerHTML;
            monto = monto.replace(',','.');
            monto = parseFloat(monto);
            montoT = montoT + monto;

            //alert("Tipo: "+tipo+" Monto: "+monto);

            /*if(tipo=='G'){
                montoT = montoT.toFixed(2);
                iva    = (montoT * 0.12);
                total  = parseFloat(montoT) + parseFloat(iva);

                iva    = iva.toFixed(2);
                total  = total.toFixed(2);

                montoT = montoT.replace('.',',');
                iva    = iva.replace('.',',');
                total  = total.replace('.',',');

                $("#bi_g").val(montoT);
                $("#iva_g").val(iva);
                $("#total").val(total);
            }else{


            }*/

        }


    }

</script>