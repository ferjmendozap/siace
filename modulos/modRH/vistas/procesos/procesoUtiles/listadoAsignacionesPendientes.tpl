<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado Asignaciones Pendientes</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">

                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="col-sm-1">Orden</th>
                                    <th class="col-sm-3">Periodo</th>
                                    <th class="col-sm-3">Funcionario</th>
                                    <th class="col-sm-3">Hijo</th>
                                    <th class="col-sm-2 text-center">Accion</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=dat from=$listadoBeneficioPendientes}

                                    <tr id="idBeneficio{$dat.pk_num_utiles_beneficio}">
                                        <td>{$dat.num_orden}</td>
                                        <td>{$dat.ind_descripcion_beneficio}</td>
                                        <td>{$dat.nombre_empleado}</td>
                                        <td>{$dat.nombre_hijo}</td>
                                        <td class="col-sm-2 text-center">
                                            {if in_array('RH-01-02-05-03-01-N',$_Parametros.perfil)}
                                            <button class="asignar logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                    idBeneficio="{$dat.pk_num_utiles_beneficio}"
                                                    orden="{$dat.num_orden}"
                                                    idBeneficioUtiles="{$dat.fk_rhc056_num_beneficio_utiles}"
                                                    beneficioUtiles="{$dat.ind_descripcion_beneficio}"
                                                    idEmpleado="{$dat.fk_rhb001_num_empleado}"
                                                    nombEmpleado="{$dat.nombre_empleado}"
                                                    idCargaFamiliar="{$dat.fk_c015_num_carga_familiar}"
                                                    nombHijo="{$dat.nombre_hijo}"
                                                    idNivelGradoInstruccion="{$dat.fk_rhc064_nivel_grado_instruccion}"
                                                    nivelGradoInstruccion="{$dat.ind_nombre_nivel_grado}"
                                                    data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                    descripcion="Crear Asignación de Utiles" titulo="Crear Asignación de Utiles">
                                                ASIGNAR UTILES
                                            </button>
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url_asignar='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/NuevaAsignacionMET';
        var $url_beneficio='{$_Parametros.url}modRH/procesos/procesoUtilesCONTROL/NuevoBeneficioMET';

        //NUEVA ASIGNACION
        $('.asignar').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_asignar, { idBeneficio: ($(this).attr('idBeneficio')), idBeneficioUtiles: ($(this).attr('idBeneficioUtiles')) , beneficio: ($(this).attr('beneficioUtiles')), idEmpleado: ($(this).attr('idEmpleado')), nombEmpleado: ($(this).attr('nombEmpleado')), idCargaFamiliar: ($(this).attr('idCargaFamiliar')), nombHijo: ($(this).attr('nombHijo')), idNivelGradoInstruccion: ($(this).attr('idNivelGradoInstruccion')), nivelGradoInstruccion: ($(this).attr('nivelGradoInstruccion')), orden: ($(this).attr('orden')) },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //NUEVO REQUISITO
        $('#nuevoBen').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url_beneficio,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        //MODIFICAR PERIODO BONO ALIMENTACION
        $('#datatable1 tbody').on( 'click', '.modificar', function () {

            $(location).attr('href',$url2+'/'+$(this).attr('idBonoAlimentacion'))

        });

        //VER PERIODO BONO ALIMENTACION
        $('#datatable1 tbody').on( 'click', '.ver', function () {

            $(location).attr('href',$url+'/'+$(this).attr('idBonoAlimentacion'))

        });

        //CERRAR PERIODO BONO ALIMENTACION
        $('#datatable1 tbody').on( 'click', '.cerrar', function () {

            var idBonoAlimentacion=$(this).attr('idBonoAlimentacion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                    var $url='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CerrarPeriodoMET';
                    $.post($url, { idBonoAlimentacion: idBonoAlimentacion},function(dato){
                        if(dato['status']=='OK'){
                            swal({
                                title: 'Cerrado!',
                                text: 'El Periodo Bono Alimentacion fue Cerrado con Exito.',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonText: 'Aceptar',
                                closeOnConfirm: true
                            }, function(){

                                $(location).attr('href','{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL');

                            });
                        }
                    },'json');
            });
        });

        //REGISTRAR EVENTOS PERIODO BONO ALIMENTACION
        $('#datatable1 tbody').on( 'click', '.eventos', function () {

            $(location).attr('href','{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/RegistrarEventosMET/'+$(this).attr('idBonoAlimentacion'))

        });
    });
</script>