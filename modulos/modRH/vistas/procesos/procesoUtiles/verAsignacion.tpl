<div class="modal-body">

        <div class="row">



                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >


                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idAsignacion}" name="idAsignacion"/>
                    <input type="hidden" value="{$idBeneficio}" name="idBeneficio"/>
                    <input type="hidden" name="proceso"  id="proceso" value="{$proceso}">

                    <div class="row contain-lg">
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="orden" name="orden" value="{if isset($formDB.num_orden)}{$formDB.num_orden}{else}{$orden}{/if}" disabled>
                                <label for="num_orden">Nº Orden</label>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="beneficio" name="beneficio" value="{if isset($formDB.ind_descripcion_beneficio)}{$formDB.ind_descripcion_beneficio}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhc056_num_beneficio_utiles" name="form[int][fk_rhc056_num_beneficio_utiles]" value="{if isset($formDB.fk_rhc056_num_beneficio_utiles)}{$formDB.fk_rhc056_num_beneficio_utiles}{else}{$idBeneficioUtiles}{/if}" disabled>
                                <label for="fk_rhc056_num_beneficio_utiles">Beneficio Periodo</label>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="nombEmpleado" name="nombEmpleado" value="{if isset($formDB.nombre_funcionario)}{$formDB.nombre_funcionario}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhb001_num_empleado" name="form[int][fk_rhb001_num_empleado]" value="{if isset($formDB.fk_rhb001_num_empleado)}{$formDB.fk_rhb001_num_empleado}{else}{$idEmpleado}{/if}" disabled>
                                <label for="fk_rhb001_num_empleado">Funcionario</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="nombHijo" name="nombHijo" value="{if isset($formDB.nombre_hijo)}{$formDB.nombre_hijo}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhc015_num_carga_familiar" name="form[int][fk_rhc015_num_carga_familiar]" value="{if isset($formDB.fk_rhc015_num_carga_familiar)}{$formDB.fk_rhc015_num_carga_familiar}{else}{$idCargaFamiliar}{/if}" disabled>
                                <label for="fk_rhc015_num_carga_familiar">Hijo</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="nivelGradoInstruccion" name="nivelGradoInstruccion" value="{if isset($formDB.ind_nombre_nivel_grado)}{$formDB.ind_nombre_nivel_grado}{/if}" disabled>
                                <input type="hidden" class="form-control input-sm"  id="fk_rhc064_nivel_grado_instruccion" name="form[int][fk_rhc064_nivel_grado_instruccion]" value="{if isset($formDB.fk_rhc064_nivel_grado_instruccion)}{$formDB.fk_rhc064_nivel_grado_instruccion}{else}{$idNivelGradoInstruccion}{/if}" disabled>
                                <label for="fk_rhc064_nivel_grado_instruccion">Grado Escolar</label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="num_factura" name="form[txt][num_factura]" value="{if isset($formDB.num_factura)}{$formDB.num_factura}{/if}" disabled>
                                <label for="num_factura">Factura</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group control-width-normal">
                                <div class="input-group date" id="fecha">
                                    <div class="input-group-content">
                                        <input type="text" class="form-control input-sm" id="fec_fecha_factura" name="form[txt][fec_fecha_factura]" value="{if isset($formDB.fec_fecha_factura)}{$formDB.fec_fecha_factura|date_format:"d-m-Y"}{/if}" DISABLED>
                                        <label>Fecha Factura</label>
                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                    </div>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div><!--end .form-group -->
                        </div>
                    </div>
                    {if $formDB.ind_estado=='PR'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_pre">Observaciones Preparado</label>
                                </div>
                            </div>
                        </div>
                    {/if}
                    {if $formDB.ind_estado=='RV'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_pre">Observaciones Preparado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_rev">Observaciones Revisado</label>
                                </div>
                            </div>
                        </div>
                    {/if}
                    {if $formDB.ind_estado=='AP'}
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_pre" name="form[txt][txt_observacion_pre]" value="{if isset($formDBoperacionesPR[0].txt_observaciones)}{$formDBoperacionesPR[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_pre">Observaciones Preparado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_rev" name="form[txt][txt_observacion_rev]" value="{if isset($formDBoperacionesRV[0].txt_observaciones)}{$formDBoperacionesRV[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_rev">Observaciones Revisado</label>
                                </div>
                            </div>
                        </div>
                        <div class="row contain-lg">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" maxlength="250" id="txt_observacion_apr" name="form[txt][txt_observacion_apr]" value="{if isset($formDBoperacionesAP[0].txt_observaciones)}{$formDBoperacionesAP[0].txt_observaciones}{/if}" style="text-transform: uppercase">
                                    <label for="txt_observacion_apr">Observaciones Aprobado</label>
                                </div>
                            </div>
                        </div>
                    {/if}

                    <div class="row contain-lg">
                        <table class="table" width="100%" cellspacing="0" style="margin-bottom: 0px">
                            <thead>
                                <tr align="center" style="width: 100%">
                                    <th class="col-sm-1">#</th>
                                    <th class="col-sm-1">Cant.</th>
                                    <th class="col-sm-5">Descripción</th>
                                    <th class="col-sm-1">Tipo</th>
                                    <th class="col-sm-1">Monto</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach item=dat from=$formDBdetalle}
                                    <tr>
                                        <td></td>
                                        <td>{$dat.num_cantidad}</td>
                                        <td>{$dat.ind_descripcion_utiles}</td>
                                        <td>{$dat.ind_exento}</td>
                                        <td>{$dat.nom_monto|number_format:2:",":"."}</td>
                                    </tr>

                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                    <br><br>
                    <div class="row contain-lg">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="exento" name="form[int][exento]" value="{if isset($formDB.num_excento)}{$formDB.num_excento|number_format:2:",":"."}{/if}">
                                <label for="exento" class="text-medium">Exento</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="bi_g" name="form[int][bi_g]" value="{if isset($formDB.num_base_imponible)}{$formDB.num_base_imponible|number_format:2:",":"."}{/if}">
                                <label for="bi_g" class="text-medium">BI G 12,00%</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="iva_g" name="form[int][iva_g]" value="{if isset($formDB.num_iva)}{$formDB.num_iva|number_format:2:",":"."}{/if}">
                                <label for="iva_g" class="text-medium">IVA G 12,00%</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm"  id="total" name="form[int][total]" value="{if isset($formDB.num_monto)}{$formDB.num_monto|number_format:2:",":"."}{/if}">
                                <label for="total" class="text-medium">TOTAL</label>
                            </div>
                        </div>
                    </div>
                    <div class="row contain-lg">
                        <br>
                        <div align="right">
                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">Cerrar</button>
                        </div>
                        <br>
                    </div>
                </form>

        </div>

</div>




<style>
    .letra_pequena{
        font-size: 11px;
    }
    #div1 {
        overflow:scroll;
        height:120px;
        width:100%;
        margin-top: 0px;
    }

</style>

<script type="text/javascript">



    var formatNumber = {
        separador: ".", // separador para los miles
        sepDecimal: ',', // separador para los decimales
        formatear:function (num){
            num +=' ';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
            }
            return this.simbol + splitLeft  +splitRight;
        },
        new:function(num, simbol){
            this.simbol = simbol ||'';
            return this.formatear(num);
        }
    }

    function quitarMiles(valor)
    {
        while (valor.indexOf('.')>-1) {
            pos= valor.indexOf('.');
            valor = "" + (valor.substring(0, pos) + '' +
                    valor.substring((pos + '.'.length), valor.length));
        }
        return valor;
    }



    /**********************************************************************************************************************************************/


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        //ancho de la ventana
        $('#modalAncho').css("width", "65%");



    });




</script>