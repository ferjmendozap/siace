<div class="col-lg-12 contain-lg">
    <div class="table-responsive">
        <table id="datatable2" class="table table-striped table-hover">
            <thead>
            <tr>
                <th>Codigo</th>
                <th>Nombre</th>
            </tr>
            </thead>
            <tbody>
            {foreach item=i from=$lista}
                <tr id="" partida="">
                    <input type="hidden" value="{$i.pk_num_partida_presupuestaria}" class="partidasCuentas"
                     nombre="{$i.ind_denominacion}"
                     cod="{$i.cod_partida}">
                    <td>{$i.cod_partida}</td>
                    <td>{$i.ind_denominacion}</td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');

            $("#cod_partida").val(input.attr('cod'));
            $("#fk_b002_num_partida_presupuestaria").val(input.attr('value'));
            $("#ind_denominacion_partida").val(input.attr('nombre'));

            /***************************************************************************************/
            $(document.getElementById('cerrarModal2')).click();
            $(document.getElementById('ContenidoModal2')).html('');
            /***************************************************************************************/

            {if $lugar == 'obligacionesCxp'}
                $(document.getElementById('{$tipo}'+input.attr('value'))).remove();
                $(document.getElementById('partidasCuenta')).append(
                    '<tr id="{$tipo}'+input.attr('value')+'">' +
                        '<input type="hidden" value="'+input.attr('value')+'" name="form[txt][cod][]"> '+
                        '<input type="hidden" value="{$tipo}" name="form[txt][tipo][]"> '+
                        '<td  style="vertical-align: middle;">'+input.attr('nombre')+'</td>' +
                        '<td  style="vertical-align: middle;">'+input.attr('cod')+'</td>' +
                        '<td width="35" width="10px" class="text-center" style="vertical-align: middle;">'+
                            '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" idTr="{$tipo}'+input.attr('value')+'">'+
                            '<i class="md md-delete"></i>'+
                            '</button>'+
                        '</td>'+
                    '</tr>'
                );
                $('#cerrarModal2').click();


            {elseif $lugar == 'conceptos'}
                $(document.getElementById('{$idCampo}')).val(input.attr('cod'));
                $('#cerrarModal3').click();
            {else}

            {/if}
        });
    });
</script>