
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Periodos Bono de Alimentación</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">

                    <div class="card card-underline">
                        <div class="card-head">
                            
                            <div class="tools">
                                <div class="btn-group">
                                    {if in_array('RH-01-04-01-22-N',$_Parametros.perfil)}
                                        <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                                descripcion="el Usuario va a registrar periodo bono alimentacion"  id="nuevo" >
                                            Nuevo Periodo Bono Alimentacion &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                        </button>
                                    {/if}
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="datatable1" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Nro.</th>
                                    <th>Periodo</th>
                                    <th>Nomina</th>
                                    <th>Descripción</th>
                                    <th>Organismo</th>
                                    <th>Estado</th>
                                    <th  width="40">Mod</th>
                                    <th  width="40">Ver</th>
                                    <th  width="50">Cerrar</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=dat from=$listadoBonoAlimentacion}

                                    <tr id="idBonoAlimentacion{$dat.pk_num_beneficio}">
                                        <td>{$dat.pk_num_beneficio}</td>
                                        <td>{$dat.ind_periodo}</td>
                                        <td>{$dat.ind_nombre_nomina}</td>
                                        <td style="text-transform: uppercase">{$dat.ind_descripcion}</td>
                                        <td>{$dat.ind_descripcion_empresa}</td>
                                        <td>{if $dat.num_estado==1}Abierto{else}Cerrado{/if}</td>
                                        <td class="text-center" width="40">
                                            {if $dat.num_estado==1}
                                                {if in_array('RH-01-04-01-23-M',$_Parametros.perfil)}
                                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  idBonoAlimentacion="{$dat.pk_num_beneficio}"
                                                            descipcion="El Usuario a Modificado un Cargo" titulo="Modificar Cargo">
                                                        <i class="fa md-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            {/if}
                                        </td>
                                        <td class="text-center" width="40">
                                            {if in_array('RH-01-04-01-24-E',$_Parametros.perfil)}
                                                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idBonoAlimentacion="{$dat.pk_num_beneficio}"
                                                        descipcion="El usuario a visualizado datos del Empleado" titulo="Ver Datos del Empleado">
                                                    <i class="fa md-search" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                        <td class="text-center" width="50">
                                            {if $dat.num_estado==1}
                                                {if in_array('RH-01-04-01-24-E',$_Parametros.perfil)}
                                                    <button class="cerrar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idBonoAlimentacion="{$dat.pk_num_beneficio}"  boton="si, Cerrar"
                                                            descipcion="El usuario va cerrar un periodo" titulo="Cerrar Periodo?" mensaje="Estas seguro que desea Cerrar el Periodo!!">
                                                        <i class="fa md-block" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            {/if}
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/VerMET';
        var $url2='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ModMET';

        $('#nuevo').click(function(){
            $(location).attr('href','{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/BonoAlimentacionMET');
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {

            $(location).attr('href',$url2+'/'+$(this).attr('idBonoAlimentacion'))

        });

        $('#datatable1 tbody').on( 'click', '.ver', function () {

            $(location).attr('href',$url+'/'+$(this).attr('idBonoAlimentacion'))

        });

        $('#datatable1 tbody').on( 'click', '.cerrar', function () {

            var idBonoAlimentacion=$(this).attr('idBonoAlimentacion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                    var $url='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CerrarPeriodoMET';
                    $.post($url, { idBonoAlimentacion: idBonoAlimentacion},function(dato){
                        if(dato['status']=='OK'){
                            swal({
                                title: 'Cerrado!',
                                text: 'El Periodo Bono Alimentacion fue Cerrado con Exito.',
                                type: "success",
                                showCancelButton: false,
                                confirmButtonText: 'Aceptar',
                                closeOnConfirm: true
                            }, function(){

                                $(location).attr('href','{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL');

                            });
                        }
                    },'json');
            });
        });
    });
</script>