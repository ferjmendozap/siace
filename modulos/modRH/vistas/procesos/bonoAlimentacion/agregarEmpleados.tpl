
<section class="style-default-light">&nbsp;
    <div class="row"> {*define una fila de la grilla (numero 1)*}
        <div class="col-lg-12">{*identifica el espacio de colunnas que acupara el div en la fila*}
            <div class="card">{*define un panel tipo tarjeta*}
                <div class="card-body ">{*cuerpo del panel tipo tarjeta*}
                    <form id="formAjax" class="form form-validate " role="form"  >{*se define un form que contendra los campos de formulario, necesario para que las clases form-group floating-label en los campos funcionen correctamente*}{*la clase form-validate necesaria para que las validaciones se muestren el los campos requeridos *}
                        <div class="col-lg-6">{*espacio que ocupara el campo en la fila*}
                            <div class="form-group floating-label"   >{*clase para el correcto estilo y efecto label flotante del campo, cada campo debe tener esta clase*}
                                <div class="input-group">{*clase que define a un grupo de campos dentro de un form*}
                                    <div class="input-group-addon">{*clase para el correcto posicionamiento del check en el grupo de campos*}
                                        <div class="checkbox checkbox-inline checkbox-styled">{*clase para el estylo y efectos del checked*}
                                            <label>
                                                <input disabled checked id="check_1" type="checkbox">{*checked para que el campo este chekado*}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group-content">
                                        <select id="ind_descripcion_empresa" disabled="disabled" class="form-control input-sm" name="ind_descripcion_empresa" >
                                            {foreach item=indice from=$datosOrganismos}
                                                <option value="{$indice.pk_num_organismo}" selected="selected">{$indice.ind_descripcion_empresa}</option>
                                            {/foreach}
                                        </select>
                                        <label for="ind_descripcion_empresa">Organismo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">{*espacio que ocupara el campo en la fila*}
                            <div class="form-group floating-label"   >{*clase para el correcto estilo y efecto label flotante del campo, cada campo debe tener esta clase*}
                                <div class="input-group">{*clase que define a un grupo de campos dentro de un form*}
                                    <div class="input-group-addon">{*clase para el correcto posicionamiento del check en el grupo de campos*}
                                        <div class="checkbox checkbox-inline checkbox-styled">{*clase para el estylo y efectos del checked*}
                                            <label>
                                                <input class="cajaCheck1" id="check_2" type="checkbox">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group-content">
                                        <select id="ind_dependencia2" disabled="disabled" class="form-control input-sm selectpicker" name="form[int][ind_dependencia2]" >{*input-sm para el tamaño de la fuente*}
                                            {foreach item=indice from=$datosDependencias}
                                                <option value="{$indice.pk_num_dependencia }" >{$indice.ind_dependencia}</option>
                                            {/foreach}
                                        </select>
                                        <label for="ind_descripcion_empresa">Dependencia</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">{*clase para el correcto posicionamiento del check en el grupo de campos*}
                                        <div class="checkbox checkbox-inline checkbox-styled">{*clase para el estylo y efectos del checked*}
                                            <label>
                                                <input disabled checked id="check_1" type="checkbox">{*checked para que el campo este chekado*}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="input-group-content">
                                        <select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control input-sm" disabled>
                                            <option value="">Seleccione...</option>
                                            {foreach item=dat from=$listadoTipoNomina}
                                                {if isset($tipo_nomina)}
                                                    {if $dat.pk_num_tipo_nomina==$tipo_nomina}
                                                        <option selected value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                    {else}
                                                        <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                    {/if}
                                                {else}
                                                    <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                        <label for="fk_nmb001_num_tipo_nomina">Tipo Nomina</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
					<br />
                </div><!--end .card-body -->
                <br>
            </div><!--end .card -->
        </div><!--end .col -->
    </div>{*fin de la fila 1*}
    <div class="row">
        <div class="col-lg-12">
            <div align="center">
                <button type="button" id="buscarEmpleados" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" >Buscar</button>
            </div>
        </div>
    </div>
    <br><br>
    <div class="row" >{*fila numero 2*}
        <div class="col-lg-12">
            <table id="datatable2" class="table table-striped table-hover" WIDTH="100%" >
                <thead>
                <tr  align="center">
                    <th ><i>#</i>Código</th>
                    <th ><i class="md-today"></i>Nro. Documento</th>
                    <th ><i class="md-person"></i>Nombre Completo</th>
                    <th ><i class="md-work"></i>Cargo</th>
                    <th ><i class="md-work"></i>Dependencia</th>

                </tr>
                </thead>
                <tbody >
				{foreach item=indice from=$listasEmpleados}
                <tr class="fila_agregar" >
                    <td>{$indice.pk_num_empleado}</td>
                    <td>{$indice.ind_cedula_documento}</td>
                    <td>{$indice.ind_nombre1} {$indice.ind_nombre2} {$indice.ind_apellido1} {$indice.ind_apellido2} </td>
                    <td>{$indice.ind_descripcion_cargo} </td>
                    <td>{$indice.ind_dependencia} </td>
                    
                </tr>
                {/foreach}
			    </tbody>
            </table>
        </div>
    </div>{* fin de fila row *}
</section>
<style>
    .fila_agregar{
        cursor: pointer;
    }

    .fila_agregar_sobre{
        background-color: #6EC58B !important; /*#ffccee !important;*/
    }


</style>
<script>
$(document).ready(function() {

    $("#ind_descripcion_empresa").select2();
    $("#ind_dependencia2").select2();
    $("#datosEmpleados").select2();

    $(document).on('change','.cajaCheck1', function(){

        if (this.checked){
            $('select[id="ind_dependencia2"]').prop('disabled', false); {*este método para seleccionar los campos funciona correctamente con bootstra*}
            $('input[id="ind_dependencia2"]').focus();
        }else{
            $('select[id="ind_dependencia2"]').prop('disabled', true);

        }
    });
    {* ************************************************* *}
    $(document).on('change','.cajaCheck2', function(){

        if (this.checked){
            $('input[id="fraceNombre"]').prop('disabled', false);
            $('input[id="fraceNombre"]').focus();
        }else{
            $('input[id="fraceNombre"]').prop('disabled', true);
            $('input[id="fraceNombre"]').val('');

        }
    });
    {* *************************************************** *}
    $("#buscarEmpleados").click(function(){
       	
		var organismo_agregar = $("#ind_descripcion_empresa").val();
        var nomina_agregar = $("#fk_nmb001_num_tipo_nomina").val();
	    var dependencia_agregar = 0;
        var caja = $('input[id="check_2"]');
		if(caja[0].checked){
            dependencia_agregar = $("#ind_dependencia2 :selected").val();
        }

        var url_agregar='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/FiltrarEmpleadosMET';

        $.post(url_agregar,{ organismo: organismo_agregar, dependencia: dependencia_agregar, tipo_nomina: nomina_agregar},function(respuesta_post){


            var tabla_agregar = $('#datatable2').DataTable();{*permite usar las funciones de la libreria para la tabla*}
            tabla_agregar.clear().draw();{*limpia la tabla (tbody)*}
            var cad_nombre;
            var cad_apellido;
            for(var i=0; i<respuesta_post.length; i++){

                if(respuesta_post[i].ind_nombre2 == null){
                    cad_nombre = '';
                }else{
                    cad_nombre = ' '+respuesta_post[i].ind_nombre2;
                }

                if(respuesta_post[i].ind_apellido2 == null){
                    cad_apellido = '';
                }else{
                    cad_apellido = ' '+respuesta_post[i].ind_apellido2;
                }


                tabla_agregar.row.add([ {*añade las filas al tbody permitiendo aplicar la paginación automaticamente*}
                    respuesta_post[i].pk_num_empleado,
                    respuesta_post[i].ind_cedula_documento,
                    respuesta_post[i].ind_nombre1+cad_nombre+' '+respuesta_post[i].ind_apellido1+cad_apellido,
                    respuesta_post[i].ind_descripcion_cargo,
                    respuesta_post[i].ind_dependencia


                ]).draw()
                  .nodes(){*Agregar una clase a cada fila añadida*}
                  .to$()
                  .addClass( 'fila_agregar' );
            }

        }, "json");

	});

    {* ************************************************************* *}
    var tabla_beneficiarios = $('#datatable1').DataTable();

    $('#datatable2 tbody').on('click','.fila_agregar', function(){

        var pk_num_empleado = this.cells[0].innerHTML;{*celda 0 que contiene el codigo de la fila*}


        $(this).addClass('fila_agregar_sobre');

        var num_filas = $('#datatable1 >tbody >tr').length;
        var filas = $('#datatable1 >tbody >tr'); {*trae las filas del tbody*}
        var band = 0;
        for(var i=0; i<num_filas; i++){
            if(filas[i].cells[0].innerHTML == $(this)[0].cells[0].innerHTML ){

                swal({  {*Mensaje personalizado*}
                    title: '',
                    text: 'Ya está agregado',
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Aceptar',
                    closeOnConfirm: true
                });

                band = 1;
                break;
            }
        }

        var cad_id_empleados = $('#id_empleados').val();

        if(band == 0){

            band = 0;
            tabla_beneficiarios.row.add([
                $(this)[0].cells[0].innerHTML, {*this representa el array (rows) *}
                $(this)[0].cells[1].innerHTML,
                $(this)[0].cells[2].innerHTML,
                $(this)[0].cells[3].innerHTML,
                $(this)[0].cells[4].innerHTML,
                '<button type="button" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button>'
            ]).draw(false);


            cad_id_empleados = cad_id_empleados + $(this)[0].cells[0].innerHTML + '#';
        }

        $('#id_empleados').val(cad_id_empleados);


    });

    {* ************************************************ *}
    $('#datatable1 tbody').on( 'click', '.eliminar', function () {

        var obj = this;

        swal({
            title:'Estas Seguro?',
            text: 'Estas seguro que desea descartar al Empleado(a)',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "si, Descartar",
            closeOnConfirm: false
        }, function(){

            tabla_beneficiarios.row( $(obj).parents('tr') ).remove().draw();{*Elimina la fila en cuestion*}
            swal("Descartado!", "Descartado de la lista de Empleados", "success");
        });

    });
    {* **************************************************** *}



});

</script>



