<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Empleados Bono Alimentacion</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="well clearfix">
                    <div class="col-sm-5 text-accent" style="text-transform: uppercase;">
                        {$datos.ind_descripcion}
                    </div>
                    <div class="col-sm-2" style="text-transform: uppercase">
                        Nomina: {$datos.ind_nombre_nomina}
                    </div>
                    <div class="col-sm-2" style="text-transform: uppercase">
                        Periodo: {$datos.ind_periodo}
                    </div>
                    <div class="col-sm-2" style="text-transform: uppercase">
                        Inicio: {$datos.fec_inicio_periodo|date_format:"d-m-Y"}
                    </div>
                    <div class="col-sm-1" style="text-transform: uppercase">
                        Fin: {$datos.fec_fin_periodo|date_format:"d-m-Y"}
                    </div>
                </div>
            </div>
        </div>
        <div align="right">
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="regresar"><span class="glyphicon glyphicon-arrow-left"></span> Atras</button>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="empleadosPeriodo" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nro.</th>
                                    <th>Nombre Completo</th>
                                    <th>Dependencia</th>
                                    <th width="80">Registrar</th>
                                    <th width="80">Ver Detalle</th>
                                    <th width="80">Mas Info</th>

                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=dat from=$datosEmpleados}

                                    <tr id="idBonoAlimentacion{$dat.pk_num_empleado}">
                                        <td>{$i}</td>
                                        <td>{$dat.ind_cedula_documento}</td>
                                        <td>{$dat.nombre_completo}</td>
                                        <td>{$dat.ind_dependencia}</td>
                                        <td class="text-center" width="80">
                                            {if in_array('RH-01-02-01-02-01-N',$_Parametros.perfil)}
                                                <button class="eventos logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="tooltip" data-placement="left" title="" data-original-title="Registrar Eventos del Emplado" pk_num_empleado="{$dat.pk_num_empleado}"
                                                        descipcion="" titulo="Registrar Eventos del Emplado">
                                                    <i class="fa fa-user-plus" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                        <td class="text-center" width="80">
                                            {if in_array('RH-01-02-01-02-02-C',$_Parametros.perfil)}
                                                <button class="ver btn ink-reaction btn-raised btn-xs btn-info"
                                                        title="Ver Detalle de los Eventos del Empleado"
                                                        data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        pk_num_empleado="{$dat.pk_num_empleado}"
                                                        fk_a001_num_organismo="{$dat.fk_a001_num_organismo}"
                                                        nombreCompleto="{$dat.nombre_completo}"
                                                        fechaIni="{$dat.fec_inicio_periodo}"
                                                        descipcion="El usuario a visualizado datos del Empleado" titulo="Detalle de los Eventos del Empleado">
                                                    <i class="fa fa-search" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        </td>
                                        <td class="text-center" width="80">
                                            <button onclick="dataVentanaInformacion('{$dat.nombre_completo}',{$dat.num_dias_periodo},{$dat.num_dias_pago},{$dat.num_dias_trabajados},{$dat.num_dias_descuento},{$dat.num_dias_feriados},{$dat.num_dias_inactivos})"
                                                        class="info logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        title="Ver mas información acerca del periodo" data-toggle="modal" data-target="#modal"
                                                        data-keyboard="false" data-backdrop="static" idBonoAlimentacion="{$dat.pk_num_empleado}"
                                                        descipcion="El usuario a visualizado datos del Empleado" titulo="Información Acerca del Periodo">
                                                    <i class="fa md-people" style="color: #ffffff;"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <input type="hidden" value="{$i++}">
                                {/foreach}
                                </tbody>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="cerrarModal3" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modalLabel">Información Acerca del Periodo</h4>
            </div>
            <div class="modal-body"  id="contenido">
                <div class="row">
                    <div class="col-md-12">
                        <div id="nombre" class="text-center text-info" >

                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <form class="form" role="form">
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="dias_periodo">
                                <label for="dias_periodo">Dias Periodo</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="dias_habiles">
                                <label for="dias_habiles">Dias Habiles</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="dias_trabajados">
                                <label for="dias_trabajados">Dias Trabajados</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="dias_descuento">
                                <label for="dias_descuento">Dias Descuento</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="dias_feriados">
                                <label for="dias_feriados">Dias Feriados</label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <input type="text" class="form-control input-sm" id="dias_inactivos">
                                <label for="dias_inactivos">Dias Inactivos</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>



<script type="text/javascript">


    function dataVentanaInformacion(nombre,dias_periodo,dias_habiles,dias_trabajados,dias_descuento,dias_feriados,dias_inactivos){
        $("#nombre").html(nombre);
        $("#dias_periodo").val(dias_periodo);
        $("#dias_habiles").val(dias_habiles);
        $("#dias_trabajados").val(dias_trabajados);
        $("#dias_descuento").val(dias_descuento);
        $("#dias_feriados").val(dias_feriados);
        $("#dias_inactivos").val(dias_inactivos);

    }


    $(document).ready(function() {

        var table = $('#empleadosPeriodo').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        var $url='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/RegistrarEventosEmpleadoMET';
        var $url2='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ModMET';

        $('#nuevo').click(function(){
            $("#_contenido").load('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/BonoAlimentacionMET');
        });

        $('#empleadosPeriodo tbody').on( 'click', '.eventos', function () {

            $("#_contenido").load($url+'/{$idBonoAlimentacion}/'+$(this).attr('pk_num_empleado'));

        });

        $('#empleadosPeriodo tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/VerDetalleEventosMET', { idEmpleado: $(this).attr('pk_num_empleado') , idBonoAlimentacion: {$idBonoAlimentacion}, organismo: $(this).attr('fk_a001_num_organismo'), nombre: $(this).attr('nombreCompleto')   },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });


        $('#regresar').click(function(){
            $("#_contenido").load('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ListarMET');
        });


    });



</script>