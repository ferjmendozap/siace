<section class="style-default-bright">
    <div class="section-header">
        <h3 class="text-primary">Interfaz Cuentas Por Pagar (Bono de Alimentacion)</h3>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form class="form-horizontal" role="form">
                <div class="col-sm-offset-1 col-sm-10">

                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="fk_a001_num_organismo">Organismo</label>
                                <div class="col-sm-9">
                                    <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control" required data-msg-required="Seleccione Organismo">
                                        {foreach item=dat from=$listadoOrganismos}
                                            <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                        {/foreach}
                                    </select>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="fk_nmb001_num_tipo_nomina">Nominas</label>
                                <div class="col-sm-9">
                                    <select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control select2-list">
                                        <option value="">Seleccione La Nomina</option>
                                        {foreach item=i from=$tipoNomina}
                                            <option value="{$i.pk_num_tipo_nomina}">{$i.ind_nombre_nomina}</option>
                                        {/foreach}
                                    </select>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="fk_nmc001_num_tipo_nomina_periodo">Periodos</label>
                            <div class="col-sm-9">
                                <select id="fk_nmc001_num_tipo_nomina_periodo" name="form[txt][fk_nmc001_num_tipo_nomina_periodo]" class="form-control select2-list select2">
                                    <option value="">Seleccione el Periodo</option>
                                </select>
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="proceso">Proceso</label>
                            <div class="col-sm-9">
                                <select id="proceso" name="form[int][proceso]" class="form-control select2-list select2">
                                    <option value="">Seleccione el Proceso</option>
                                </select>
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-control-line"></div>
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-xs btn-primary" id="buscar" titulo="Calcular Obligaciones">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<br>
<div class="row" id="listado">

</div>
<script type="text/javascript">
    $(document).ready(function () {
        var rh = new  ModRhFunciones();
        //Periodos
        rh.metJsonPeriodosDisponibles('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/PeriodosDisponiblesMET',false);
        //Procesos Disponibles
        rh.metJsonProcesosDisponibles('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ProcesosDisponiblesMET',false);

        var $url='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ListadoEmpleadosPeriodoCalculoMET';
        $('#buscar').click(function(){
            if($('#fk_nmb003_num_tipo_proceso').val()=='' ||  $('#fk_nmc001_num_tipo_nomina_periodo').val()==''){
                if($('#fk_nmb001_num_tipo_nomina').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar una Nomina', "error");
                }else if($('#fk_nmc001_num_tipo_nomina_periodo').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar EL Periodo', "error");
                }
            }else{
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ pk_num_tipo_nomina:$('#fk_nmb001_num_tipo_nomina').val(), periodo: $("#fk_nmc001_num_tipo_nomina_periodo").val() },function(dato){
                    $('#listado').html(dato);
                });
            }
        });
    });
</script>