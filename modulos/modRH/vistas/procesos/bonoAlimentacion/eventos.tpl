
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >

                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idEvento}" name="idEvento"/>
                    <input type="hidden" value="{$idBonoAlimentacion}" name="idBonoAlimentacion"/>
                    <input type="hidden" value="{$idEmpleado}" name="idEmpleado"/>
                    <input type="hidden" value="{$periodo}" name="periodo"/>
                    <input type="hidden" value="{$organismo}" name="organismo"/>

                    <div class="col-xs-12" style="margin-top: -10px">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                       <select id="fec_fecha" name="form[txt][fec_fecha]" class="form-control input-sm" required data-msg-required="Seleccione la Fecha del Evento">
                                        <option value="">Seleccione...</option>
                                        {foreach item=dat from=$fecha}
                                            {if isset($formDB.fec_fecha)}
                                                {if $dat|date_format:"%e-%m-%Y" == $formDB.fec_fecha|date_format:"%e-%m-%Y"}
                                                    <option selected value="{$dat}">{$dat}</option>
                                                {else}
                                                    <option value="{$dat}">{$dat}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat}">{$dat}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fec_fecha">Fecha</label>
                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm time12-mask" value="{if isset($formDB.fec_hora_salida)}{$formDB.fec_hora_salida|date_format:"H:i a"}{else}{$hora.horaE}{/if}" name="form[formulas][fec_hora_salida]" id="fec_hora_salida" required data-msg-required="Indique Hora Salida">
                                    <label>Hora Salida</label>
                                    <p class="help-block"><span class="text-xs" style="color: red">am/pm</span></p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm time12-mask" value="{if isset($formDB.fec_hora_entrada)}{$formDB.fec_hora_entrada|date_format:"H:i a"}{else}{$hora.horaS}{/if}" name="form[formulas][fec_hora_entrada]" id="fec_hora_entrada" required data-msg-required="Seleccione Hora Entrada">
                                    <label>Hora Entrada</label>
                                    <p class="help-block"><span class="text-xs" style="color: red">am/pm</span></p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" class="form-control input-sm" value="{if isset($formDB.fec_total_horas)}{$formDB.fec_total_horas}{/if}" name="form[formulas][fec_total_horas]" id="fec_total_horas" readonly>
                                    <label>Horas</label>
                                </div>
                            </div>
                        </div><!--end .row -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select id="fk_a006_num_miscelaneo_motivo_ausencia" name="form[int][fk_a006_num_miscelaneo_motivo_ausencia]" class="form-control input-sm" required data-msg-required="Seleccione Motivo de Ausencia">
                                        <option value="">...</option>
                                        {foreach item=dat from=$MotivoAusencia}
                                            {if isset($formDB.fk_a006_num_miscelaneo_motivo_ausencia)}
                                                {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_motivo_ausencia}
                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fk_a006_num_miscelaneo_motivo_ausencia">Motivo Ausencia</label>
                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select id="fk_a006_num_miscelaneo_tipo_ausencia" name="form[int][fk_a006_num_miscelaneo_tipo_ausencia]" class="form-control input-sm" required data-msg-required="Seleccione Tipo de Evento">
                                        <option value="">...</option>
                                        {foreach item=dat from=$TipoEvento}
                                            {if isset($formDB.fk_a006_num_miscelaneo_tipo_ausencia)}
                                                {if $dat.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_tipo_ausencia}
                                                    <option selected value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {else}
                                                    <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                                {/if}
                                            {else}
                                                <option value="{$dat.pk_num_miscelaneo_detalle}">{$dat.ind_nombre_detalle}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="fk_a006_num_miscelaneo_tipo_ausencia">Tipo de Evento</label>
                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="form[txt][txt_observaciones]" id="txt_observaciones" class="form-control input-sm" rows="3" placeholder="" onkeyup="$(this).val($(this).val().toUpperCase())">{if isset($formDB.txt_observaciones)}{$formDB.txt_observaciones}{/if}</textarea>
                                    <label for="txt_observaciones">Observaciones</label>
                                </div>
                            </div><!--end .col -->
                        </div>

                        <div class="row">
                            <br><br>
                            <div align="right">
                                <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                                <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>  Guardar</button>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>


<!-- </form> -->

<script type="text/javascript">

    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/EventosEmpleadoMET', datos ,function(dato){


                if(dato['status']=='error'){
                    {*para usuarios normales*}
                    //swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                    {*para usuarios programadores*}
                    swal("Error",dato['detalleERROR'], "error");
                }
                else if(dato['status']=='modificar'){

                    $(document.getElementById('idEvento'+dato['idEvento'])).html('<td>'+dato['fec_fecha']+'</td>' +
                            '<td>'+dato['fec_hora_salida']+'</td>' +
                            '<td>'+dato['fec_hora_entrada']+'</td>' +
                            '<td>'+dato['fec_total_horas']+'</td>' +
                            '<td>'+dato['mot_ausencia']+'</td>' +
                            '<td>'+dato['tip_ausencia']+'</td>' +
                            '<td>'+dato['txt_observaciones']+'</td>' +
                            '<td>NO</td>' +
                            '<td width="60">' +
                            '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idEvento="'+dato['idEvento']+'"' +
                            'descripcion="El Usuario a Modificado un Evento" titulo="Modificar Evento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '<td width="60">' +
                            '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEvento="'+dato['idEvento']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Evento" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Evento!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>'+
                            '</td>');
                    swal("Registro Modificado!", "Registro Modificado Satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                }else if(dato['status']=='creacion'){

                        $(document.getElementById('empleadosEventos')).append('<tr id="idEvento'+dato['idEvento']+'">' +
                                '<td>'+dato['fec_fecha']+'</td>' +
                                '<td>'+dato['fec_hora_salida']+'</td>' +
                                '<td>'+dato['fec_hora_entrada']+'</td>' +
                                '<td>'+dato['fec_total_horas']+'</td>' +
                                '<td>'+dato['mot_ausencia']+'</td>' +
                                '<td>'+dato['tip_ausencia']+'</td>' +
                                '<td>'+dato['txt_observaciones']+'</td>' +
                                '<td>NO</td>' +
                                '<td width="60">' +
                                '{if in_array('RH-01-04-07-07-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                                'data-keyboard="false" data-backdrop="static" idEvento="'+dato['idEvento']+'"' +
                                'descripcion="El Usuario a Modificado un Evento" titulo="Modificar Evento">' +
                                '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '<td width="60">' +
                                '{if in_array('RH-01-04-07-08-E',$_Parametros.perfil)}<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEvento="'+dato['idEvento']+'"  boton="si, Eliminar"' +
                                'descipcion="El usuario a eliminado un Evento" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Evento!">' +
                                '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                                '</td>'+
                                '</tr>');
                        swal("Registro Guardado!", "Registro Guardado Satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                }

            },'json');


        }
    });


    $(document).ready(function() {


        //valido formulario
        $("#formAjax").validate();

        //ANCHO DE LA MODAL PRINCIPAL
        $('#modalAncho2').css( "width", "72%" );

        //eventos al colocar hora de salida y hora de entrada
        $("#fec_fecha").click(function() {

            $.post("{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CalcularDiferenciaHorasMET", { hora_salida: $('#fec_hora_salida').val(), hora_entrada: $('#fec_hora_entrada').val(), empleado:{$idEmpleado}, fecha: $("#fec_fecha").val() }, function(data){
                $("#fec_total_horas").val(data);
            },'json');

        });
       //eventos al colocar hora de salida y hora de entrada
        $("#fec_hora_salida").change(function() {

            $.post("{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CalcularDiferenciaHorasMET", { hora_salida: $('#fec_hora_salida').val(), hora_entrada: $('#fec_hora_entrada').val(), empleado:{$idEmpleado}, fecha: $("#fec_fecha").val() }, function(data){
                $("#fec_total_horas").val(data);
            },'json');

        });

        $("#fec_hora_entrada").change(function() {

            $.post("{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CalcularDiferenciaHorasMET", { hora_salida: $('#fec_hora_salida').val(), hora_entrada: $('#fec_hora_entrada').val(), empleado:{$idEmpleado} , fecha: $("#fec_fecha").val() }, function(data){
                $("#fec_total_horas").val(data);
            },'json');

        });
        /************************************************************/




    });






</script>