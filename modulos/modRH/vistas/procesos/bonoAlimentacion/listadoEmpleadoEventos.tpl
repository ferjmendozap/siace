<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Eventos del Empleado</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="well clearfix">
                    <div class="col-sm-5 text-accent" style="text-transform: uppercase">
                        {$datos.ind_descripcion}
                    </div>
                    <div class="col-sm-2" style="text-transform: uppercase">
                        Nomina: {$datos.ind_nombre_nomina}
                    </div>
                    <div class="col-sm-2" style="text-transform: uppercase">
                        Periodo: {$datos.ind_periodo}
                        <input type="hidden" id="periodo" value="{$datos.ind_periodo}">
                        <input type="hidden" id="organismo" value="{$datos.fk_a001_num_organismo}">
                    </div>
                    <div class="col-sm-3" style="text-transform: uppercase">
                        Empleado: {$datosSoloEmpleados.nombre_completo}
                    </div>
                </div>
            </div>
        </div>
        <div align="right">
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="regresar"><span class="glyphicon glyphicon-arrow-left"></span> Atras</button>
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="listarPermisos"><span class="glyphicon glyphicon-arrow-down"></span> Ver Permisos</button>
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="nuevoEvento" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Nuevo Evento"><span class="md md-create"></span> Nuevo Evento</button>
            <button type="button" class="btn btn-danger ink-reaction btn-raised" id="procesarEventos"><span class="glyphicon glyphicon-saved"></span> Procesar Eventos</button>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <div class="card card-underline">
                        <div class="card-body">
                            <table id="empleadosEventos" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Salida</th>
                                    <th>Entrada</th>
                                    <th>Horas</th>
                                    <th>Motivo de Ausencia</th>
                                    <th>Tipo de Evento</th>
                                    <th>Observaciones</th>
                                    <th>Procesado</th>
                                    <th width="60">Mod</th>
                                    <th width="60">Eli</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=dat from=$datosEventos}

                                    <tr id="idEvento{$dat.pk_num_beneficio_alimentacion_eventos}">
                                        <td>{$dat.fec_fecha|date_format:"d-m-Y"}</td>
                                        <td>{$dat.fec_hora_salida|date_format:"%I:%M:%S %p"}</td>
                                        <td>{$dat.fec_hora_entrada|date_format:"%I:%M:%S %p"}</td>
                                        <td>{$dat.fec_total_horas|date_format:"H:i"}</td>
                                        <td>{$dat.mot_ausencia}</td>
                                        <td>{$dat.tip_ausencia}</td>
                                        <td>{$dat.txt_observaciones}</td>
                                        <td>{if $dat.num_flag_procesado==0}NO{else}SI{/if}</td>
                                        <td class="text-center" width="60">
                                            {if $dat.num_flag_procesado==0}<!-- si no esta procesado se puede modificar -->
                                                {if in_array('RH-01-02-01-02-03-M',$_Parametros.perfil)}
                                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar Evento" idEvento="{$dat.pk_num_beneficio_alimentacion_eventos}" descripcion="El Usuario a Modificado un Evento" titulo="Modificar Evento">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            {/if}
                                        </td>
                                        <td class="text-center" width="60">
                                            {if $dat.num_flag_procesado==0}<!-- si no esta procesado se puede eliminar -->
                                                {if in_array('RH-01-02-01-02-04-E',$_Parametros.perfil)}
                                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEvento="{$dat.pk_num_beneficio_alimentacion_eventos}"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Evento" title="Eliminar Evento" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Evento!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>
                                                {/if}
                                            {/if}
                                        </td>
                                    </tr>
                                    <input type="hidden" value="{$i++}">
                                {/foreach}
                                </tbody>

                            </table>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>




<script type="text/javascript">


    function dataVentanaInformacion(nombre,dias_periodo,dias_habiles,dias_trabajados,dias_descuento,dias_feriados,dias_inactivos){
        $("#nombre").html(nombre);
        $("#dias_periodo").val(dias_periodo);
        $("#dias_habiles").val(dias_habiles);
        $("#dias_trabajados").val(dias_trabajados);
        $("#dias_descuento").val(dias_descuento);
        $("#dias_feriados").val(dias_feriados);
        $("#dias_inactivos").val(dias_inactivos);

    }


    $(document).ready(function() {

        var table = $('#empleadosEventos').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        var $url='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/EventosEmpleadoMET';
        var $url2='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ProcesarEventosEmpleadoMET';

        //al dar click en nuevo evento
        $('#nuevoEvento').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idEmpleado:{$datosSoloEmpleados.fk_rhb001_num_empleado}, idBonoAlimentacion:{$idBonoAlimentacion}, periodo: $("#periodo").val(), organismo: $("#organismo").val() },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });


        $('#procesarEventos').click(function(){

            swal({
                title:'Estas Seguro?',
                text: 'Estas seguro que desea Procesar los Eventos!!!',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Procesar",
                closeOnConfirm: false
            }, function(){

                $.post($url2,{ idEmpleado:{$datosSoloEmpleados.fk_rhb001_num_empleado}, idBonoAlimentacion:{$idBonoAlimentacion}, periodo: $("#periodo").val(), organismo: $("#organismo").val() },function(dato){

                    if(dato==1){
                        var $url='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/RegistrarEventosEmpleadoMET';
                        swal("Exito", "Eventos Procesados Satisfactoriamente", "success");
                        $("#_contenido").load($url+'/{$idBonoAlimentacion}/'+{$datosSoloEmpleados.fk_rhb001_num_empleado});

                    }else{
                        swal("Información", "No existen Eventos por Procesar", "info");
                    }


                },'json');
            });

        });

        $('#empleadosEventos tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idEvento: $(this).attr('idEvento') , idEmpleado:{$datosSoloEmpleados.fk_rhb001_num_empleado}, idBonoAlimentacion:{$idBonoAlimentacion}, periodo: $("#periodo").val(), organismo: $("#organismo").val() },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#empleadosEventos tbody').on( 'click', '.eliminar', function () {

            var idEvento = $(this).attr('idEvento');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/EliminarEventoEmpleadoMET';
                $.post($url, { idEvento: idEvento},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('idEvento'+$dato['idEvento'])).html('');
                        swal("Eliminado!", "Registro Eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

        $('#regresar').click(function(){
            $("#_contenido").load('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/RegistrarEventosMET/'+{$idBonoAlimentacion});
        });



    });



</script>