<div class="modal-body">

        <div class="row">
            <!-- WIZARD BONO DE ELIMENTACION -->
            <div id="rootwizard2" class="form-wizard form-wizard-horizontal">

                <!-- PESTAÑAS WIZAARD -->
                <div class="form-wizard-nav">
                    <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                    <ul class="nav nav-justified">
                        <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">Información General</span></a></li>
                        <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title">Empleados</span></a></li>
                    </ul>
                </div>

                <form id="formAjax" class="form form-validation" role="form" novalidate="novalidate" >


                    <input type="hidden" value="1" name="valido" />
                    <input type="hidden" value="{$idBonoAlimentacion}" name="idBonoAlimentacion"/>
                    <input type="hidden" name="form[txt][id_empleados]"  id="id_empleados">
                    <input type="hidden" name="procesoM"  id="procesoM" value="{$proceso}">

                    <!-- CONTENIDO -->
                    <div class="tab-content clearfix">

                        <!-- INFORMACIÓN GENERAL -->
                        <div class="tab-pane active" id="step1">
                            <h4 class="text-primary text-center text-lg">Información del Periodo</h4>

                            <div class="well clearfix">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select id="fk_a001_num_organismo" name="form[int][fk_a001_num_organismo]" class="form-control input-sm" required data-msg-required="Seleccione Organismo">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoOrganismos}
                                                    {if isset($formDB.fk_a001_num_organismo)}
                                                        {if $dat.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                                            <option selected value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_organismo}">{$dat.ind_descripcion_empresa}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_a001_num_organismo">Organismo</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="500" id="ind_descripcion" name="form[txt][ind_descripcion]" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" required data-msg-required="Indique Descripción" style="text-transform: uppercase">
                                            <label for="ind_descripcion">Descripción</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control input-sm" required data-msg-required="Seleccione Tipo de Nomina">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoTipoNomina}
                                                    {if isset($formDB.fk_nmb001_num_tipo_nomina)}
                                                        {if $dat.pk_num_tipo_nomina==$formDB.fk_nmb001_num_tipo_nomina}
                                                            <option selected value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_tipo_nomina}">{$dat.ind_nombre_nomina}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_nmb001_num_tipo_nomina">Tipo Nomina</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" maxlength="7" id="ind_periodo" name="form[txt][ind_periodo]" value="{if isset($formDB.ind_periodo)}{$formDB.ind_periodo}{else}{$periodo}{/if}" required data-msg-required="Indique Periodo">
                                            <label for="ind_periodo">Periodo</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group control-width-normal">
                                            <div class="input-group date" id="fecha_ini">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control input-sm" id="fec_inicio_periodo" name="form[txt][fec_inicio_periodo]" value="{if isset($formDB.fec_inicio_periodo)}{$formDB.fec_inicio_periodo|date_format:"d-m-Y"}{else}{$fecha_ini}{/if}" required data-msg-required="Indique Fecha Inicio">
                                                    <label>Fecha Inicio</label>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div><!--end .form-group -->
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group control-width-normal">
                                            <div class="input-group date" id="fecha_fin">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control input-sm" id="fec_fin_periodo" name="form[txt][fec_fin_periodo]" value="{if isset($formDB.fec_fin_periodo)}{$formDB.fec_fin_periodo|date_format:"d-m-Y"}{else}{$fecha_fin}{/if}" required data-msg-required="Indique Fecha Fin">
                                                    <label>Fecha Fin</label>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            </div>
                                        </div><!--end .form-group -->
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select id="fk_cpb002_tipo_documento" name="form[int][fk_cpb002_tipo_documento]" class="form-control input-sm" required data-msg-required="Seleccione Tipo Documento">
                                                <option value="">Seleccione...</option>
                                                {foreach item=dat from=$listadoDocumento}
                                                    {if isset($formDB.fk_cpb002_tipo_documento)}
                                                        {if $dat.pk_num_tipo_documento==$formDB.fk_cpb002_tipo_documento}
                                                            <option selected value="{$dat.pk_num_tipo_documento}">{$dat.ind_descripcion}</option>
                                                        {else}
                                                            <option value="{$dat.pk_num_tipo_documento}">{$dat.ind_descripcion}</option>
                                                        {/if}
                                                    {else}
                                                        <option value="{$dat.pk_num_tipo_documento}">{$dat.ind_descripcion}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="fk_cpb002_tipo_documento">Tipo Documento</label>
                                            <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control input-sm" name="form[txt][cod_partida]" id="cod_partida" value="{if isset($formDB.cod_partida)}{$formDB.cod_partida}{/if}"  required>
                                                    <input type="hidden" id="fk_b002_num_partida_presupuestaria" name="form[int][fk_b002_num_partida_presupuestaria]" value="{if isset($formDB.fk_b002_num_partida_presupuestaria)}{$formDB.fk_b002_num_partida_presupuestaria}{/if}" >
                                                    <input type="hidden" id="ind_denominacion_partida" name="form[txt][ind_denominacion_partida]" value="{if isset($formDB.ind_denominacion_partida)}{$formDB.ind_denominacion_partida}{/if}" >
                                                    <label for="partida">Partida Presupuestaria</label>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                                <div class="input-group-btn">
                                                    <button class="buscarPartida btn btn-xs btn-info" type="button" id="buscarPartida" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Partidas" title="Listar Partidas Presupuestarias"><i class="md md-search" style="color: #ffffff;"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-content">
                                                    <input type="text" class="form-control input-sm" name="form[txt][ind_cod_presupuesto]" id="ind_cod_presupuesto" value="{if isset($formDB.ind_cod_presupuesto)}{$formDB.ind_cod_presupuesto}{/if}"  required>
                                                    <input type="hidden" id="fk_prb004_num_presupuesto" name="form[int][fk_prb004_num_presupuesto]" value="{if isset($formDB.fk_prb004_num_presupuesto)}{$formDB.fk_prb004_num_presupuesto}{/if}" >
                                                    <label for="presupuesto">Presupuesto</label>
                                                    <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                </div>
                                                <div class="input-group-btn">
                                                    <button class="buscarPresupuesto btn btn-xs btn-info" type="button" id="buscarPresupuesto" data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" titulo="Listado de Presupuestos" title="Listar Presupuestos"><i class="md md-search" style="color: #ffffff;"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- FERNANDO MENDOZA 23Feb18. Agregado para descuesto aplicable al cestaticket -->
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="ind_descripcion_descuento" name="form[txt][ind_descripcion_descuento]" value="{if isset($formDB.ind_descripcion_descuento)}{$formDB.ind_descripcion_descuento}{/if}">
                                            <label for="ind_descripcion_descuento">Descripcion del Descuento</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="num_monto_descuento" name="form[int][num_monto_descuento]" value="{if isset($formDB.num_monto_descuento)}{$formDB.num_monto_descuento|number_format:2:',':'.'}{else}0,00{/if}">
                                            <label for="num_monto_descuento">Monto Descuento</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <h4 class="text-primary text-center text-lg">Detalle del Periodo</h4>
                            <div class="well clearfix">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_dias_periodo" name="form[int][num_dias_periodo]" value="{if isset($formDB.num_dias_periodo)}{$formDB.num_dias_periodo}{/if}" readonly>
                                            <label for="num_dias_periodo">Dia del Periodo</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="num_dias_pago" name="form[int][num_dias_pago]" value="{if isset($formDB.num_dias_pago)}{$formDB.num_dias_pago}{/if}" readonly>
                                            <label for="num_dias_pago">Dias de Pago</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_total_feriados" name="form[int][num_total_feriados]" value="{if isset($formDB.num_total_feriados)}{$formDB.num_total_feriados}{/if}" readonly>
                                            <label for="num_total_feriados">Dias Feriado</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_valor_diario" name="form[int][num_valor_diario]" value="{if isset($formDB.num_valor_diario)}{$formDB.num_valor_diario|number_format:2:",":"."}{/if}" readonly>
                                            <label for="num_valor_diario">Valor Diario</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="fec_horas_diarias" name="form[txt][fec_horas_diarias]" value="{if isset($formDB.fec_horas_diarias)}{$formDB.fec_horas_diarias}{/if}" readonly>
                                            <label for="fec_horas_diarias">Horas Diarias</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm" id="fec_horas_semanales" name="form[txt][fec_horas_semanales]" value="{if isset($formDB.fec_horas_semanales)}{$formDB.fec_horas_semanales}{/if}" readonly>
                                            <label for="fec_horas_semanales">Horas Semanales</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_valor_semanal" name="form[int][num_valor_semanal]" value="{if isset($formDB.num_valor_semanal)}{$formDB.num_valor_semanal|number_format:2:",":"."}{/if}" readonly>
                                            <label for="num_valor_semanal">Valor Semanal</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <input type="text" class="form-control input-sm"  id="num_valor_mes" name="form[int][num_valor_mes]" value="{if isset($formDB.num_valor_mes)}{$formDB.num_valor_mes|number_format:2:",":"."}{/if}" readonly>
                                            <label for="num_valor_mes">Valor Mensual</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <!-- EMPLEADOS -->
                        <div class="tab-pane" id="step2">

                            <!-- botones para importar empleados, botones para insertar uno a uso y eliminar -->
                            <br>
                            <div align="right">
                                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="importarEmpleados"><span class="glyphicon glyphicon-save"></span> Importar Empleados</button>
                                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="insertarEmpleado" descripcion="Agregar Empleado" data-keyboard="false" data-backdrop="static" data-toggle="modal" data-target="#formModal2"
                                        titulo="Agregar Empleado"><span class="glyphicon glyphicon-user"></span> Insertar Empleado</button>


                            </div>
                            <br>

                            <div class="col-lg-12">

                                <div style="height: 300px; overflow-y: scroll">

                                    <table id="listadoEmpleados" class="table table-striped no-margin table-condensed" width="100%">
                                        <thead>
                                        <tr>
                                            <th class="col-sm-1">Emp.</th>
                                            <th class="col-sm-1">Nro. Documento</th>
                                            <th class="col-sm-2">Nombre Completo</th>
                                            <th class="col-sm-3">Cargo</th>
                                            <th class="col-sm-3">Dependencia</th>
                                            <th class="col-sm-1">Descuento</th>
                                            <th>Eliminar</th>
                                        </tr>
                                        </thead>
                                        <tbody id="data">

                                        </tbody>
                                    </table>

                                </div>

                            </div>
                        </div>

                        <div class="row text-center">
                            <button type="button" id="btnCerrar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>
                            <button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="guardarPeriodo"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                        </div>
                    </div>

                    <!-- BOTONES PRIMERO ANTERIOR SIGUIENTE ULTIMO -->
                    <ul class="pager wizard">
                        <li class="previous first" id="primero"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                        <li class="previous" id="anterior"><a class="ink-reaction btn-raised" href="javascript:void(0);">Anterior</a></li>
                        <li class="next last" id="ultimo"><a class="ink-reaction btn-raised" href="javascript:void(0);">Ultimo</a></li>
                        <li class="next" id="siguiente"><a class="ink-reaction btn-raised" href="javascript:void(0);">Siguente</a></li>
                    </ul>

                </form>


            </div>
        </div>



</div>


<script type="text/javascript">

    //inicializar el datepicker
    $("#fecha_ini").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });
    $("#fecha_fin").datepicker({
        todayHighlight: true,
        format:'dd-mm-yyyy',
        autoclose: true,
        language:'es'
    });


    //si todo es correcto y cumple con los requimientos de validacion Envio
    $.validator.setDefaults({
        submitHandler: function() {

            //alert("Envio el formulario");
            var datos = $( "#formAjax" ).serialize();

            var num_filas = $('#listadoEmpleados >tbody >tr').length;
            var filas = $('#listadoEmpleados >tbody >tr');

            if(num_filas == 1 &&  filas[0].cells[0].innerHTML == "No Hay Registros Disponibles"){
                swal({
                    title: '',
                    text: 'Agregue los Empleados',
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Aceptar',
                    closeOnConfirm: true
                });
            }else{

                var url= '{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/BonoAlimentacionMET';
                $.post(url,datos,function(respuesta){

                    if(respuesta['status']=='OK'){

                        swal("Bono Alimentación Registrado", "Guardado Satisfactoriamente", "success");
                        $("#_contenido").load('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL');
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                    }else if(respuesta['status']=='OK_mod'){

                        //swal("Bono Alimentación Modificado", "Guardado Satisfactoriamente", "success");
                        swal({
                            title: 'Bono Alimentación Modificado',
                            text: 'Guardado Satisfactoriamente',
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Aceptar",
                            closeOnConfirm: true
                        }, function () {
                            $("#_contenido").load('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL');
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        });

                    }else if(respuesta['status']=='error'){
                        {*para usuarios normales*}
                        //swal("Error","No se pudo realizar operacion. Consulte con el Administrador del Sistema.", "error");
                        {*para usuarios programadores*}
                        swal("Error",respuesta['detalleERROR'], "error");

                    }
                },'json');
            }
        }
    });


    $(document).ready(function() {

        //valido formulario
        $("#formAjax").validate();

        $('#modalAncho').css( "width", "80%" );

        $("#num_monto_descuento").maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });

        //Script para forzar el avance del wizard por el botón suigiente.
        $(".form-wizard-nav").on('click', function () {
            var form = $('#rootwizard2').find('.form-validation');
            var valid = form.valid();
            if(!valid) {
                form.data('validator').focusInvalid();
                return false;
            }
        });

        $("#buscarPartida").on('click', function(){
            var $url = '{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ListarPartidasMET';
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { } ,function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });
        $("#buscarPresupuesto").on('click', function(){
            var $url = '{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ListarPresupuestoMET';
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url, { } ,function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });


        var proceso = $("#procesoM").val();

        if (proceso == 'Ver') {
            $(".form-control").attr("disabled", "disabled");
            $("#fk_a001_num_organismo").removeAttr("readonly");
            $("#fk_nmb001_num_tipo_nomina").removeAttr("readonly");
            $("#fk_a001_num_organismo").attr("disabled", "disabled");
            $("#fk_nmb001_num_tipo_nomina").attr("disabled", "disabled");
            //oculto botones
            $("#importarEmpleados").hide();
            $("#insertarEmpleado").hide();
            $("#guardarPeriodo").hide();

            //listo lo empleados
            $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/MostrarBonoAlimentacionEmpleadosMET', { bono_alimentacion: {$idBonoAlimentacion} }, function (dato) {


                $("#data").html('');
                var cad_id_empleados = '';

                for (var i = 0; i < dato.length; i++) {

                    $(document.getElementById('listadoEmpleados')).append('<tr id="listadoEmpleados' + dato[i].pk_num_empleado + '">' +
                        '<td>' + dato[i].pk_num_empleado + '</td>' +
                        '<td>' + dato[i].ind_cedula_documento + '</td>' +
                        '<td>' + dato[i].nombre_completo + '</td>' +
                        '<td>' + dato[i].ind_descripcion_cargo + '</td>' +
                        '<td>' + dato[i].ind_dependencia + '</td>' +
                        '<td><input type="text" class="form-control input-sm" id="num_monto_descuento_otros"'+ i +'  name="form[int][num_monto_descuento_otros]['+ i +']" value="'+ dato[i].num_monto_descuento_otros +'"></td>' +
                        '<td>...</td>' +
                        '</tr>');

                    cad_id_empleados = cad_id_empleados + dato[i].pk_num_empleado + '#';
                    $("#num_monto_descuento_otros"+i).maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });
                }

                $('#id_empleados').val(cad_id_empleados);

            }, 'json');

        }

        if (proceso == 'Modificar') {

            $(".form-control").attr("readonly", "readonly");
            $("#ind_descripcion").removeAttr("readonly");
            $("#fec_inicio_periodo").removeAttr("readonly");
            $("#fec_fin_periodo").removeAttr("readonly");
            $("#ind_periodo").removeAttr("readonly");


            //listo lo empleados
            $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/MostrarBonoAlimentacionEmpleadosMET', { bono_alimentacion: {$idBonoAlimentacion} }, function (dato) {

                $("#data").html('');
                var cad_id_empleados = '';

                for (var i = 0; i < dato.length; i++) {

                    $(document.getElementById('listadoEmpleados')).append('<tr id="listadoEmpleados' + dato[i].pk_num_empleado + '">' +
                        '<td>' + dato[i].pk_num_empleado + '</td>' +
                        '<td>' + dato[i].ind_cedula_documento + '</td>' +
                        '<td>' + dato[i].nombre_completo + '</td>' +
                        '<td>' + dato[i].ind_descripcion_cargo + '</td>' +
                        '<td>' + dato[i].ind_dependencia + '</td>' +
                        '<td><input type="text" class="form-control input-sm" id="num_monto_descuento_otros'+ i +'"  name="form[int][num_monto_descuento_otros]['+ i +']" value="'+ formatoNumero(dato[i].num_monto_descuento_otros,2,',','.') +'"></td>' +
                        '<td><button type="button" valor="' + dato[i].pk_num_empleado + '" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button></td>' +
                        '</tr>');

                    cad_id_empleados = cad_id_empleados + dato[i].pk_num_empleado + '#';
                    $("#num_monto_descuento_otros"+i).maskMoney({ showSymbol:false, symbol:"", decimal:",", thousands:"." });

                }

                $('#id_empleados').val(cad_id_empleados);

            }, 'json');


        }


        if (proceso == 'Nuevo') {

            $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CalcularDiasPeriodoBonoMET', {
                fecha_ini: $("#fec_inicio_periodo").val(),
                fecha_fin: $("#fec_fin_periodo").val()
            }, function (dato) {

                $("#num_dias_periodo").val(dato['num_dias_periodo']);
                $("#num_dias_pago").val(dato['num_dias_pago']);
                $("#num_total_feriados").val(dato['num_total_feriados']);
                $("#num_valor_diario").val(dato['num_valor_diario']);
                $("#num_valor_mes").val(dato['num_valor_mes']);
                $("#fec_horas_diarias").val(dato['fec_horas_diarias']);
                $("#fec_horas_semanales").val(dato['fec_horas_semanales']);
                $("#num_valor_semanal").val(dato['num_valor_semanal']);

            }, 'json');

        }

        $("#fec_inicio_periodo").change(function () {

            $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CalcularDiasPeriodoBonoMET', {
                fecha_ini: $("#fec_inicio_periodo").val(),
                fecha_fin: $("#fec_fin_periodo").val()
            }, function (dato) {

                $("#num_dias_periodo").val(dato['num_dias_periodo']);
                $("#num_dias_pago").val(dato['num_dias_pago']);
                $("#num_total_feriados").val(dato['num_total_feriados']);
                $("#num_valor_diario").val(dato['num_valor_diario']);
                $("#num_valor_mes").val(dato['num_valor_mes']);
                $("#fec_horas_diarias").val(dato['fec_horas_diarias']);
                $("#fec_horas_semanales").val(dato['fec_horas_semanales']);
                $("#num_valor_semanal").val(dato['num_valor_semanal']);

            }, 'json');

        });

        $("#fec_fin_periodo").change(function () {

            var f1 = $('#fec_inicio_periodo').val();
            var f2 = $('#fec_fin_periodo').val();

            //validar que la fecha de regreso sea mayor que la fecha de inicio
            var x2 = new Date();
            var x1 = new Date();
            var fecha2 = f2.split("-");
            var fecha1 = f1.split("-");

            x2.setFullYear(fecha2[2],fecha2[1]-1,fecha2[0]);
            x1.setFullYear(fecha1[2],fecha1[1]-1,fecha1[0]);


            if (x2 >= x1) {


                $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/CalcularDiasPeriodoBonoMET', {
                    fecha_ini: $("#fec_inicio_periodo").val(),
                    fecha_fin: $("#fec_fin_periodo").val()
                }, function (dato) {

                    $("#num_dias_periodo").val(dato['num_dias_periodo']);
                    $("#num_dias_pago").val(dato['num_dias_pago']);
                    $("#num_total_feriados").val(dato['num_total_feriados']);
                    $("#num_valor_diario").val(dato['num_valor_diario']);
                    $("#num_valor_mes").val(dato['num_valor_mes']);
                    $("#fec_horas_diarias").val(dato['fec_horas_diarias']);
                    $("#fec_horas_semanales").val(dato['fec_horas_semanales']);
                    $("#num_valor_semanal").val(dato['num_valor_semanal']);

                }, 'json');

                return true;


            }else{

                swal("Error", "La Fecha Final del Periodo no puede ser menor a la Fecha de Inicio del Periodo", "error");
                $('#fec_fin_periodo').focus();
                $('#fec_fin_periodo').val(f2);
                return false;
            }


        });


        {*BOTON IMPORTAR EMPLEADOS*}
        $("#importarEmpleados").click(function () {

            //var tabla_agregar = $('#listadoEmpleados').DataTable();

            {*AVISO PARA CONFIRMAR EL PROCEDIMIENTO*}
            swal({
                title: 'Estas Seguro?',
                text: 'Estas seguro que desea importar todos los empleados?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Seguro",
                closeOnConfirm: true
            }, function () {

                $.post('{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ImportarEmpleadosMET', {
                    tipo_nomina: $("#fk_nmb001_num_tipo_nomina").val(),
                    organismo: $("#fk_a001_num_organismo").val()
                }, function (dato) {

                    $("#data").html('');
                    var cad_id_empleados = '';

                    for (var i = 0; i < dato.length; i++) {

                        var monto_descuento = $('#num_monto_descuento').val();

                        $(document.getElementById('listadoEmpleados')).append('<tr id="listadoEmpleados' + dato[i].pk_num_empleado + '">' +
                            '<td>' + dato[i].pk_num_empleado + '</td>' +
                            '<td>' + dato[i].ind_cedula_documento + '</td>' +
                            '<td>' + dato[i].nombre_completo + '</td>' +
                            '<td>' + dato[i].ind_descripcion_cargo + '</td>' +
                            '<td>' + dato[i].ind_dependencia + '</td>' +
                            '<td><input type="text" class="form-control input-sm" id="num_monto_descuento_otros"'+ i +'  name="form[int][num_monto_descuento_otros]['+ i +']" value="'+ monto_descuento +'"></td>' +
                            '<td><button type="button" valor="' + dato[i].pk_num_empleado + '" class="eliminar gsUsuario btn ink-reaction btn-raised btn-xs btn-danger"><i class="md md-delete"></i></button></td>' +
                            '</tr>');

                        cad_id_empleados = cad_id_empleados + dato[i].pk_num_empleado + '#';

                    }

                    $('#id_empleados').val(cad_id_empleados);


                }, 'json');

            });

        });

        {* ************************************************ *}
        $('#listadoEmpleados tbody').on('click', '.eliminar', function () {

            //var tabla_agregar = $('#listadoEmpleados').DataTable();
            var obj = $(this).attr('valor');

            swal({
                title: 'Estas Seguro?',
                text: 'Estas seguro que desea descartar al Empleado(a)',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "si, Descartar",
                closeOnConfirm: false
            }, function () {

                //tabla_agregar.row($(obj).parents('tr')).remove().draw();
                $(document.getElementById('listadoEmpleados'+obj)).html('');

                {*Elimina la fila en cuestion*}
                swal("Descartado!", "Descartado de la lista el Empleado", "success");


                {*verifico la tablas donde estan los empleados para volver armar el arreglo de empleados*}
                var num_filas = $('#listadoEmpleados >tbody >tr').length;
                var filas = $('#listadoEmpleados >tbody >tr'); {*trae las filas del tbody*}


                var cad_id_empleados = '';
                $('#id_empleados').val('');

                for(var i=0; i<num_filas; i++) {

                    {*voy armando arreglo de empleados*}
                    cad_id_empleados = cad_id_empleados + filas[i].cells[0].innerHTML + '#';

                }
                {*asigno arreglo de empleados*}
                $('#id_empleados').val(cad_id_empleados);


            });


        });
        {* **************************************************** *}


        {*para agregar un empleado a la lista de nomina de bono alimentacion*}
        var $urlAgregar='{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/AgregarBeneficiariosMET';
        $('#insertarEmpleado').click(function(){
            $('#modalAncho2').css( "width", "80%" );
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#ContenidoModal2').html("");

            $.post($urlAgregar,{ organismo : $('#fk_a001_num_organismo').val() , tipo_nomina: $('#fk_nmb001_num_tipo_nomina').val()  },function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        //**************************************



    });

    function formatoNumero(numero, decimales, separadorDecimal, separadorMiles) {
        var partes, array;
        if ( !isFinite(numero) || isNaN(numero = parseFloat(numero)) ) {
            return "";     }
        if (typeof separadorDecimal==="undefined") {
            separadorDecimal = ",";
        }
        if (typeof separadorMiles==="undefined") {
            separadorMiles = "";
        }     // Redondeamos
        if ( !isNaN(parseInt(decimales)) ) {
            if (decimales >= 0) {
                numero = numero.toFixed(decimales);
            } else {
                numero = (Math.round(numero / Math.pow(10, Math.abs(decimales))) * Math.pow(10, Math.abs(decimales))
                ).toFixed();
            }
        } else {
            numero = numero.toString();
        }
        // Damos formato
        partes = numero.split(".", 2);
        array = partes[0].split("");
        for (var i=array.length-3; i>0 && array[i-1]!=="-"; i-=3) {
            array.splice(i, 0, separadorMiles);
        }
        numero = array.join("");
        if (partes.length>1) {
            numero += separadorDecimal + partes[1];
        }
        return numero;
    }







</script>