<div class="col-sm-12">
    <section class="style-default-bright">
        <div class="section-header">
            <div class="col-md-6">
                <button class="logsUsuario btn btn-sm ink-reaction btn-default text-bold calcular"
                        titulo="Consolidar Bono Alimentacion"
                        mensaje="Esta Seguro que desea Consolidar el Bono de Alimentacion?"
                        descipcion=""
                        url="{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ConsolidarBonoAlimentacionMET">
                    1. Calcular Obligaciones &nbsp;&nbsp;
                </button>


                <button class="logsUsuario btn btn-sm ink-reaction btn-default text-bold calcular"
                        titulo="Consolidar Bono Alimentacion"
                        mensaje="Esta Seguro que desea Consolidar el Bono de Alimentacion?"
                        descipcion=""
                        url="{$_Parametros.url}modRH/procesos/bonoAlimentacionCONTROL/ConsolidarBonoAlimentacionMET">
                    2. Consolidar Obligaciones&nbsp;&nbsp;&nbsp;&nbsp;
                </button>

                <button class="logsUsuario btn btn-sm ink-reaction btn-default text-bold calcular"
                        titulo="Calcular Aportes y Retenciones"
                        mensaje="Esta Seguro que desea Calcular las obligaciones??"
                        descipcion="el Usuario a calculado las Obligaciones de Nomina"
                        url="{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/calcularAportesRetencionesMET">
                    3. Verificar Presupuesto &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </button>
            </div>
            <div class="col-md-6 text-right">
                <button class="logsUsuario btn btn-sm ink-reaction btn-danger text-bold calcular"
                        titulo="Calcular Aportes y Retenciones"
                        mensaje="Esta Seguro que desea Calcular las obligaciones??"
                        descipcion="el Usuario a calculado las Obligaciones de Nomina"
                        url="{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/calcularAportesRetencionesMET">
                    4. Generar Obligaciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                </button>
            </div>

        </div>
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-left"> <input type="checkbox" id="checkTodos" /> </th>
                                    <th class="text-left"> PROVEEDOR</th>
                                    <th class="text-center"> NOMBRE DEL PROVEEDOR </th>
                                    <th class="text-center"> CON.</th>
                                    <th class="text-left"> VER.</th>
                                    <th class="text-left"> TRA.</th>
                                    <th class="text-center"> TOTAL OBLIGACION</th>
                                    <th class="text-center"> DOC.</th>
                                    <th class="text-center"> NRO. DOCUMENTO</th>
                                    <th class="text-center"> FECHA</th>
                                    <th class="text-center"> REGISTRO</th>

                                </tr>
                            </thead>
                            <tbody>
                                {foreach item=i from=$listado}
                                    <tr id="idCalculo{$i.pk_num_bono_alimentacion_calculo}">
                                        <th class="text-left"> <input type="checkbox" value="{$i.pk_num_bono_alimentacion_calculo}" name="listado" /> </th>
                                        <th class="text-left"> {$i.ind_cedula_documento}</th>
                                        <th class="text-center"> {$i.empleado} </th>
                                        <th class="text-center">-</th>
                                        <th class="text-left">-</th>
                                        <th class="text-left">-</th>
                                        <th class="text-right"> {$i.num_monto_pagar} </th>
                                        <th class="text-right"> BA </th>
                                        <th class="text-right"> - </th>
                                        <th class="text-right"> - </th>
                                        <th class="text-right"> - </th>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        $("#checkTodos").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });

        $('#datatable1 tfoot').on( 'click', '.calcular', function () {
                var url=$(this).attr('url');

                var lista = new Array();
                var suma = 0;
                $("input:checkbox[name^='listado']").each(function(index,e){
                    var $this = $(this);
                    if($this.is(":checked")){
                        lista.push($(this).val());
                        suma++;
                    }
                });
                if(suma == 0){
                    swal("Seleccione", 'Debe Seleccionar al menos un (1) Registro!', "error");
                }else{
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        swal({
                            title: "¡Por favor espere!",
                            text: "Se esta procesando su solicitud, puede demorar un poco.",
                            timer: 50000000,
                            showConfirmButton: false
                        });

                        $.post(url, { pk_num_tipo_nomina:$('#fk_nmb001_num_tipo_nomina').val(), periodo: $("#fk_nmc001_num_tipo_nomina_periodo").val(), registros:lista  },function(dato){
                         if (dato['status'] == 'listo') {
                         swal("Obligacion Calculada!", 'La Obligacion fue calculada satisfactoriamente', "success");
                         $.post('{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/listadoObligacionesMET',{ pk_num_proceso_periodo:$('#fk_nmb003_num_tipo_proceso').val(),pk_num_tipo_nomina:$('#fk_nmb001_num_tipo_nomina').val() },function(dato){
                         $('#listadoObligaciones').html(dato);
                         });
                         }
                         },'json');*/
                    });
                }



        });
        var $url='{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/';
        $('#datatable1 tbody').on( 'click', '.verificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url+'verificarObligacionMET',{ idObligacion: $(this).attr('idObligacion')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.transferir', function () {
            var idObligacion = $(this).attr('idObligacion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post($url+'transferirObligacionMET', { idObligacion: idObligacion},function(dato){
                    if (dato['status'] == 'ok') {
                        swal("Obligacion Transferida!", 'La Obligacion fue transferida satisfactoriamente', "success");
                        $.post('{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/listadoObligacionesMET',{ pk_num_proceso_periodo:$('#fk_nmb003_num_tipo_proceso').val(),pk_num_tipo_nomina:$('#fk_nmb001_num_tipo_nomina').val() },function(dato){
                            $('#listadoObligaciones').html(dato);
                        });
                    }
                },'json');
            });

        });
    });
</script>