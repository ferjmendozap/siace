<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class idiomasModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }


    public function metConsultarIdiomas()
    {

        $con = $this->_db->query("
                SELECT
                 *
                FROM
                rh_c013_idioma
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR O CONSULTAR UN IDIOMA ESPECIFICO
    public function metMostrarIdiomas($idIdioma)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c013_idioma
                WHERE  pk_num_idioma='$idIdioma'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE REGISTRAR UN IDIOMA
    public function metRegistrarIdiomas($local,$ext,$est)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c013_idioma
             SET
                ind_nombre_idioma=:ind_nombre_idioma,
                ind_nombre_ext=:ind_nombre_ext,
                num_estatus=:ind_estatus,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_nombre_idioma' => $local,
            ':ind_nombre_ext'    => $ext,
            ':ind_estatus'       => $est
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR UN IDIOMA
    public function metModificarIdiomas($idIdioma,$local,$ext,$est)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_c013_idioma
             SET
                ind_nombre_idioma=:ind_nombre_idioma,
                ind_nombre_ext=:ind_nombre_ext,
                num_estatus=:ind_estatus,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE
               pk_num_idioma='$idIdioma'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_nombre_idioma' => $local,
            ':ind_nombre_ext'    => $ext,
            ':ind_estatus'       => $est
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    #PERMITE ELIMINAR UN IDIOMA
    public function metEliminarIdioma($idIdioma)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c013_idioma where pk_num_idioma ='$idIdioma'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
