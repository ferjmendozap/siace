<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class grupoOcupacionalModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metConsultarGrupoOcupacional()
    {

        $con = $this->_db->query("
             SELECT
              a006.pk_num_miscelaneo_detalle,
              a006.ind_nombre_detalle,
              a006.cod_detalle,
              a006.num_estatus,
              a006.fk_a005_num_miscelaneo_maestro
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE a005.cod_maestro='GRUPOCUP'
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metMostrarGrupoOcupacional($idGrupoOcupacional)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT
              a006.pk_num_miscelaneo_detalle,
              a006.ind_nombre_detalle,
              a006.cod_detalle,
              a006.num_estatus,
              a006.fk_a005_num_miscelaneo_maestro
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE a005.cod_maestro='GRUPOCUP' AND pk_num_miscelaneo_detalle='$idGrupoOcupacional'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    public function metObtenerMaestro()
    {

        $con = $this->_db->query("
             SELECT
              a006.fk_a005_num_miscelaneo_maestro
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE a005.cod_maestro='GRUPOCUP' GROUP BY a006.fk_a005_num_miscelaneo_maestro
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }


    public function metRegistrarGrupoOcupacional($cod_detalle,$descripcion,$idMaestro)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                a006_miscelaneo_detalle
                (cod_detalle,ind_nombre_detalle,num_estatus,fk_a005_num_miscelaneo_maestro)
                VALUES
                (:cod_detalle,:ind_nombre_grupo,:ind_estatus,:fk_a005_num_miscelaneo_maestro)
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':cod_detalle'      => $cod_detalle,
            ':ind_nombre_grupo' => $descripcion,
            ':ind_estatus'      => '1',
            ':fk_a005_num_miscelaneo_maestro' => $idMaestro,
        ));



        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }


    public function metModificarGrupoOcupacional($idGrupoOcupacional,$cod_detalle,$descripcion)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              a006_miscelaneo_detalle
            SET
              cod_detalle=:cod_detalle,
              ind_nombre_detalle=:ind_nombre_grupo
            WHERE
              pk_num_miscelaneo_detalle='$idGrupoOcupacional'
            "
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':cod_detalle'      => $cod_detalle,
            ':ind_nombre_grupo' => $descripcion,
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    public function metEliminarGrupoOcupacional($idGrupoOcupacional)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from a006_miscelaneo_detalle where pk_num_miscelaneo_detalle ='$idGrupoOcupacional'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }

}
