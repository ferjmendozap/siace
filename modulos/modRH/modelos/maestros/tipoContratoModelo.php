<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class tipoContratoModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarTipoContrato()
    {

        $con = $this->_db->query("
                SELECT
                 *
                FROM
                rh_c066_tipo_contrato
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR O CONSULTAR UN TIPO CONTRATO ESPECIFICO
    public function metMostrarTipoContrato($idTipoContrato)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c066_tipo_contrato
                WHERE  pk_num_tipo_contrato='$idTipoContrato'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE REGISTRAR UN TIPO DE CONTRATO
    public function metRegistrarTipoContrato($tipo,$flag_nomina,$flag_vencimiento,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c066_tipo_contrato
             SET
                ind_tipo_contrato=:ind_tipo_contrato,
                num_flag_nomina=:num_flag_nomina,
                num_flag_vencimiento=:num_flag_vencimiento,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_tipo_contrato'    => $tipo,
            ':num_flag_nomina'      => $flag_nomina,
            ':num_flag_vencimiento' => $flag_vencimiento,
            ':num_estatus'          => $estatus
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR UN TIPO DE CONTRATO
    public function metModificarTipoContrato($idTipoContrato,$tipo,$flag_nomina,$flag_vencimiento,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_c066_tipo_contrato
             SET
                ind_tipo_contrato=:ind_tipo_contrato,
                num_flag_nomina=:num_flag_nomina,
                num_flag_vencimiento=:num_flag_vencimiento,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
             WHERE
               pk_num_tipo_contrato='$idTipoContrato'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_tipo_contrato'    => $tipo,
            ':num_flag_nomina'      => $flag_nomina,
            ':num_flag_vencimiento' => $flag_vencimiento,
            ':num_estatus'          => $estatus
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    #PERMITE ELIMINAR UN TIPO DE CONTRATO
    public function metEliminarTipoContrato($idTipoContrato)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c066_tipo_contrato where pk_num_tipo_contrato ='$idTipoContrato'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
