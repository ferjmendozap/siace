<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class nivelesTiposCargosModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metConsultarNivelesTiposCargos()
    {

        $con = $this->_db->query("
             SELECT
            rh_c009_nivel.pk_num_nivel,
            rh_c009_nivel.fk_rhc006_num_cargo,
            rh_c009_nivel.num_nivel,
            rh_c009_nivel.ind_nombre_nivel,
            rh_c006_tipo_cargo.pk_num_cargo,
            rh_c006_tipo_cargo.ind_nombre_cargo
            FROM
            rh_c009_nivel
            INNER JOIN rh_c006_tipo_cargo ON rh_c006_tipo_cargo.pk_num_cargo = rh_c009_nivel.fk_rhc006_num_cargo");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #MUESTRA LOS REGISTROS SEGUN UN NIVEL ESPECIFICO
    public function metMostrarNivelesTiposCargos($idNivelesTiposCargos)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT * FROM rh_c009_nivel WHERE pk_num_nivel='$idNivelesTiposCargos'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #MUESTRA LOS NIVELES SEGUN EL TIPO DE CARGO
    public function metMostrarNivelesTipos($Tipo)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT * FROM rh_c009_nivel WHERE fk_rhc006_num_cargo='$Tipo'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

     public function metUltimoNivelCargo($cargo)
     {

         #ejecuto la consulta a la base de datos
         $resultado =  $this->_db->query(
             "SELECT
                MAX(rh_c009_nivel.num_nivel)+1 as ultimo
                FROM
                rh_c006_tipo_cargo
                INNER JOIN rh_c009_nivel ON rh_c009_nivel.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo where fk_rhc006_num_cargo = $cargo"
         );
         #devuelvo la consulta para ser usada en php con formato json.
         #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
         $resultado->setFetchMode(PDO::FETCH_ASSOC);
         #retorno lo consultado al controlador para ser usado.
         #fetch — Devuelve un unico registro seleccionado
         return $resultado->fetch();


     }


    //registra el nuevo nivel en la bd
    public function metRegistrarNivelesTipoCargo($cargo, $nivel, $nombre)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                 rh_c009_nivel
                (fk_rhc006_num_cargo,num_nivel,ind_nombre_nivel,fk_a018_num_seguridad_usuario,fec_ultima_modificacion)
                VALUES
                (:fk_rhc006_num_cargo,:num_nivel,:ind_nombre_nivel,'$this->atIdUsuario',NOW())
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_rhc006_num_cargo' => $cargo,
            ':num_nivel' => $nivel,
            ':ind_nombre_nivel' => $nombre
        ));



        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }



    public function metModificarNivelesTipoCargo($idNivelesTiposCargos,$cargo, $nivel, $nombre)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              rh_c009_nivel
            SET
              fk_rhc006_num_cargo=:fk_rhc006_num_cargo,
              num_nivel=:num_nivel,
              ind_nombre_nivel=:ind_nombre_nivel,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
            WHERE
            pk_num_nivel='$idNivelesTiposCargos'
            "
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_rhc006_num_cargo' => $cargo,
            ':num_nivel' => $nivel,
            ':ind_nombre_nivel' => $nombre
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }



    public function metEliminarNivelesTipoCargo($idNivelesTiposCargos)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "  delete from rh_c009_nivel where pk_num_nivel ='$idNivelesTiposCargos'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }



}
