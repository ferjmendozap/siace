<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class preguntasModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarPreguntas()
    {

        $con = $this->_db->query("
                SELECT
                rh_c074_preguntas.pk_num_preguntas,
                rh_c074_preguntas.ind_descripcion_preguntas,
                rh_c074_preguntas.num_valor_minimo,
                rh_c074_preguntas.num_valor_maximo,
                rh_c074_preguntas.num_estatus,
                rh_c074_preguntas.fk_a018_num_seguridad_usuario,
                rh_c074_preguntas.fec_ultima_modificacion,
                rh_c074_preguntas.fk_a006_num_miscelaneo_detalle_area,
                a006_miscelaneo_detalle.ind_nombre_detalle
                FROM
                rh_c074_preguntas
                INNER JOIN a006_miscelaneo_detalle ON rh_c074_preguntas.fk_a006_num_miscelaneo_detalle_area = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR O CONSULTAR PREGUNTAS
    public function metMostrarPreguntas($idPregunta)
    {

        $con = $this->_db->query("
                SELECT
                rh_c074_preguntas.pk_num_preguntas,
                rh_c074_preguntas.ind_descripcion_preguntas,
                rh_c074_preguntas.num_valor_minimo,
                rh_c074_preguntas.num_valor_maximo,
                rh_c074_preguntas.num_estatus,
                rh_c074_preguntas.fk_a018_num_seguridad_usuario,
                rh_c074_preguntas.fec_ultima_modificacion,
                rh_c074_preguntas.fk_a006_num_miscelaneo_detalle_area,
                a006_miscelaneo_detalle.ind_nombre_detalle
                FROM
                rh_c074_preguntas
                INNER JOIN a006_miscelaneo_detalle ON rh_c074_preguntas.fk_a006_num_miscelaneo_detalle_area = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                WHERE  rh_c074_preguntas.pk_num_preguntas='$idPregunta'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    #PERMITE REGISTRAR PREGUNTAS
    public function metRegistrarPreguntas($descripcion,$miscelaneo,$v_min,$v_max,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #verificar con las descripcion que no este repetido
        $con = $this->_db->query("select * from rh_c074_preguntas where fk_a006_num_miscelaneo_detalle_area='$miscelaneo' and ind_descripcion_preguntas='$descripcion'");
        if($con->fetchColumn() > 0){#hay filas con la misma descripcion

            $x = 'rep';
            return $x;

        }else {#no hay filas con la misma descripcion

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                rh_c074_preguntas
             SET
                ind_descripcion_preguntas=:ind_descripcion_preguntas,
                fk_a006_num_miscelaneo_detalle_area=:fk_a006_num_miscelaneo_detalle_area,
                num_valor_minimo=:num_valor_minimo,
                num_valor_maximo=:num_valor_maximo,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':ind_descripcion_preguntas' => $descripcion,
                ':fk_a006_num_miscelaneo_detalle_area' => $miscelaneo,
                ':num_valor_minimo' => $v_min,
                ':num_valor_maximo' => $v_max,
                ':num_estatus' => $estatus
            ));

        }

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR PLANTILLAS
    public function metModificarPreguntas($idPregunta,$descripcion,$miscelaneo,$v_min,$v_max,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_c074_preguntas
             SET
                ind_descripcion_preguntas=:ind_descripcion_preguntas,
                fk_a006_num_miscelaneo_detalle_area=:fk_a006_num_miscelaneo_detalle_area,
                num_valor_minimo=:num_valor_minimo,
                num_valor_maximo=:num_valor_maximo,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
             WHERE
               pk_num_preguntas'$idPregunta'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_descripcion_preguntas' => $descripcion,
            ':fk_a006_num_miscelaneo_detalle_area' => $miscelaneo,
            ':num_valor_minimo' => $v_min,
            ':num_valor_maximo' => $v_max,
            ':num_estatus' => $estatus
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    #PERMITE ELIMINAR PREGUNTAS
    public function metEliminarPreguntas($idPregunta)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c074_preguntas where pk_num_preguntas ='$idPregunta'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }

    #para llenar select miscelaneos
    public function metMiscelaneos($maestro,$app)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                vi_rh_miscelaneos
                WHERE cod_maestro='$maestro' AND cod_aplicacion='$app'
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();

    }


}
