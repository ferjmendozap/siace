<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class beneficiosUtilesModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarBeneficiosUtiles()
    {

        $con = $this->_db->query("
                SELECT
                 *
                FROM
                rh_c056_beneficio_utiles
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR O CONSULTAR BENEFICIOS UTILES
    public function metMostrarBeneficiosUtiles($idBeneficiosUtiles)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c056_beneficio_utiles
                WHERE  pk_num_beneficio_utiles='$idBeneficiosUtiles'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE REGISTRAR BENEFICIO UTILES
    public function metRegistrarBeneficiosUtiles($descripcion,$periodo_ini,$periodo_fin,$monto)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #verificar con las descripcion que no este repetido
        $con1 = $this->_db->query("select * from rh_c056_beneficio_utiles where ind_descripcion_beneficio='$descripcion'");
        if($con1->fetchColumn() > 0){

            return 'repdes';//repetido descripcion

        }else{

            #verifico con los periodo para no esten repetidos
            $con2 = $this->_db->query("select * from rh_c056_beneficio_utiles where ind_periodo_ini='$periodo_ini' and ind_periodo_fin='$periodo_fin'");
            if($con2->fetchColumn() > 0){

                return 'repper';//repetido periodo

            }else{
                //inserto

                #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
                $NuevoRegistro = $this->_db->prepare(
                    "INSERT INTO
                    rh_c056_beneficio_utiles
                 SET
                    ind_descripcion_beneficio=:ind_descripcion_beneficio,
                    ind_periodo_ini=:ind_periodo_ini,
                    ind_periodo_fin=:ind_periodo_fin,
                    num_monto_asignado=:num_monto_asignado,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
                  ");

                #execute — Ejecuta una sentencia preparada
                $NuevoRegistro->execute(array(
                    ':ind_descripcion_beneficio' => $descripcion,
                    ':ind_periodo_ini'           => $periodo_ini,
                    ':ind_periodo_fin'           => $periodo_fin,
                    ':num_monto_asignado'        => $monto
                ));

            }

        }


        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR MOTIVO CESE
    public function metModificarBeneficiosUtiles($idBeneficiosUtiles,$descripcion,$periodo_ini,$periodo_fin,$monto)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_c056_beneficio_utiles
             SET
                ind_descripcion_beneficio=:ind_descripcion_beneficio,
                ind_periodo_ini=:ind_periodo_ini,
                ind_periodo_fin=:ind_periodo_fin,
                num_monto_asignado=:num_monto_asignado,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
             WHERE
               pk_num_beneficio_utiles='$idBeneficiosUtiles'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_descripcion_beneficio' => $descripcion,
            ':ind_periodo_ini'           => $periodo_ini,
            ':ind_periodo_fin'           => $periodo_fin,
            ':num_monto_asignado'        => $monto
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    #PERMITE ELIMINAR MOTIVO CESE
    public function metEliminarBeneficiosUtiles($idBeneficiosUtiles)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c056_beneficio_utiles where pk_num_beneficio_utiles ='$idBeneficiosUtiles'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
