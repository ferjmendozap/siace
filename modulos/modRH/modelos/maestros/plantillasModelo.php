<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class plantillasModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarPlantillas()
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c073_plantillas
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR O CONSULTAR PLANTILLAS
    public function metMostrarPlantillas($idPlantilla)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c073_plantillas
                WHERE  pk_num_plantillas='$idPlantilla'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE MOSTRAR O CONSULTAR PLANTILLAS
    public function metMostrarPlantillasPreguntas($idPlantilla)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c075_plantilla_preguntas
                WHERE  fk_rhc073_num_plantillas='$idPlantilla'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE REGISTRAR PLANTILLAS
    public function metRegistrarPlantillas($descripcion,$preguntas=false,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #verificar con las descripcion que no este repetido
        $con = $this->_db->query("select * from rh_c073_plantillas where ind_descripcion_plantilla='$descripcion'");
        if($con->fetchColumn() > 0){#hay filas con la misma descripcion

            $x = 'rep';
            return $x;

        }else {#no hay filas con la misma descripcion

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                rh_c073_plantillas
             SET
                ind_descripcion_plantilla=:ind_descripcion_plantilla,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':ind_descripcion_plantilla' => $descripcion,
                ':num_estatus' => $estatus
            ));

            $idPlantilla = $this->_db->lastInsertId();

            if($preguntas) {

                $InsertPreguntas = $this->_db->prepare(
                    "INSERT INTO rh_c075_plantilla_preguntas
                     SET
                     fk_rhc073_num_plantillas=:pk_num_plantillas,
                     fk_rhc074_num_preguntas=:pk_num_preguntas
                    "
                );
                for ($i = 0; $i < count($preguntas); $i++) {

                    $InsertPreguntas->execute(array(
                        ':pk_num_plantillas' => $idPlantilla,
                        ':pk_num_preguntas'  => $preguntas[$i]
                    ));
                }
            }

        }

        $error1 = $NuevoRegistro->errorInfo();

        if($preguntas){
            $error2 = $InsertPreguntas->errorInfo();
        }else{
            $error2 =array(1=>null,2=>null);
        }

        if(!empty($error1[1]) && !empty($error1[2])  && !empty($error2[1]) && !empty($error2[2])){
            $this->_db->rollBack();
            return $error1;
        }else{
            //$idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idPlantilla;
        }
    }

    #PERMITE MODIFICAR PLANTILLAS
    public function metModificarPlantillas($idPlantilla,$descripcion,$preguntas=false,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_c073_plantillas
             SET
                ind_descripcion_plantilla=:ind_descripcion_plantilla,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
             WHERE
               pk_num_plantillas='$idPlantilla'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_descripcion_plantilla' => $descripcion,
            ':num_estatus' => $estatus
        ));

        #elimino las preguntas asignadas a esa plantilla
        $eliminarPreguntas=$this->_db->prepare(
            "DELETE FROM rh_c075_plantilla_preguntas WHERE  fk_rhc073_num_plantillas=:pk_num_plantillas"
        );
        $eliminarPreguntas->execute(array(':pk_num_plantillas'=>$idPlantilla));

        #inserto las preguntas a la plantilla
        if($preguntas) {

            $InsertPreguntas = $this->_db->prepare(
                "INSERT INTO rh_c075_plantilla_preguntas
                     SET
                     fk_rhc073_num_plantillas='$idPlantilla',
                     fk_rhc074_num_preguntas=:pk_num_preguntas
                    "
            );
            for ($i = 0; $i < count($preguntas); $i++) {

                $InsertPreguntas->execute(array(
                    ':pk_num_preguntas'  => $preguntas[$i]
                ));
            }
        }

        $error1 = $NuevoRegistro->errorInfo();
        $error2 = $eliminarPreguntas->errorInfo();
        if($preguntas){
            $error3 = $InsertPreguntas->errorInfo();
        }else{
            $error3 =array(1=>null,2=>null);
        }
        if(!empty($error1[1]) && !empty($error1[2]) && !empty($error2[1]) && !empty($error2[2]) && !empty($error3[1]) && !empty($error3[2])){
            $this->_db->rollBack();
            return $error1;
        }else{
            $this->_db->commit();
            return $idPlantilla;
        }
    }

    #PERMITE ELIMINAR PLANTILLA
    public function metEliminarPlantillas($idPlantilla)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "
             delete from rh_c075_plantilla_preguntas where fk_rhc073_num_plantillas ='$idPlantilla';
             delete from rh_c073_plantillas where pk_num_plantillas ='$idPlantilla';
             
            "
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
