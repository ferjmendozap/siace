<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class centroEstudiosModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metConsultarCentros()
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c040_institucion
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #BUSCAR UN CENTRO DE ESTUDIO ESPECIFICO
    public function metMostrarCentro($idCentro)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c040_institucion
                WHERE  pk_num_institucion='$idCentro'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE REGISTRAR UN CENTRO DE ESTUDIO
    public function metRegistrarCentro($descripcion,$ubicacion,$flag_est,$flag_curso,$est)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c040_institucion
             SET
                ind_nombre_institucion=:ind_nombre_institucion,
                ind_ubicacion=:ind_ubicacion,
                num_flag_estudio=:ind_flag_estudio,
                num_flag_curso=:ind_flag_curso,
                num_estatus=:ind_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_nombre_institucion' => $descripcion,
            ':ind_ubicacion'          => $ubicacion,
            ':ind_flag_estudio'       => $flag_est,
            ':ind_flag_curso'         => $flag_curso,
            ':ind_estatus'            => $est
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR UN CENTRO DE ESTUDIO
    public function metModificarCentro($idCentro,$descripcion,$ubicacion,$flag_est,$flag_curso,$est)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_c040_institucion
             SET
                ind_nombre_institucion=:ind_nombre_institucion,
                ind_ubicacion=:ind_ubicacion,
                num_flag_estudio=:ind_flag_estudio,
                num_flag_curso=:ind_flag_curso,
                num_estatus=:ind_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
             WHERE
               pk_num_institucion='$idCentro'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_nombre_institucion' => $descripcion,
            ':ind_ubicacion'          => $ubicacion,
            ':ind_flag_estudio'       => $flag_est,
            ':ind_flag_curso'         => $flag_curso,
            ':ind_estatus'            => $est
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    #PERMITE ELIMINAR UNA PROFESION DE LA BD
    public function metEliminarCentro($idCentro)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c040_institucion where pk_num_institucion ='$idCentro'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
