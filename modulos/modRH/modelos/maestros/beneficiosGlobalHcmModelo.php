<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class beneficiosGlobalHcmModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarBeneficioGlobal()
    {

        $con = $this->_db->query("
                SELECT
                 *
                FROM
                rh_b008_ayuda_global
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR O CONSULTAR BENEFICIO GLOBAL HCM
    public function metMostrarBeneficioGlobal($idBeneficioGlobal)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_b008_ayuda_global
                WHERE  pk_num_ayuda_global='$idBeneficioGlobal'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE MOSTRAR O CONSULTAR BENEFICIO GLOBAL HCM SEGUN AÑO
    public function metMostrarBeneficioGlobalAnio($anio)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_b008_ayuda_global
                WHERE  fec_anio='$anio'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE REGISTRAR BENEFICIO GLOBAL HCM
    public function metRegistrarBeneficioGlobal($descripcion,$limite,$anio)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_b008_ayuda_global
             SET
                fec_anio=:fec_anio,
                ind_descripcion=:ind_descripcion,
                num_limite=:num_limite,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fec_anio' => $anio,
            ':ind_descripcion' => $descripcion,
            ':num_limite'      => $limite
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR BENEFICIO GLOBAL HCM
    public function metModificarBeneficioGlobal($idBeneficioGlobal,$descripcion,$limite)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_b008_ayuda_global
             SET
                ind_descripcion=:ind_descripcion,
                num_limite=:num_limite,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
             WHERE
               pk_num_ayuda_global='$idBeneficioGlobal'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_descripcion' => $descripcion,
            ':num_limite'      => $limite
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idBeneficioGlobal;
        }
    }

    #PERMITE ELIMINAR BENEFICIO GLOBAL HCM
    public function metEliminarBeneficioGlobal($idBeneficioGlobal)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_b008_ayuda_global where pk_num_ayuda_global ='$idBeneficioGlobal'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
