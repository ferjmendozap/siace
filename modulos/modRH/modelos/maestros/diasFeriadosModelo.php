<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class diasFeriadosModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarFeriados()
    {

        $con = $this->_db->query("
                SELECT
                 *
                FROM
                rh_c069_feriados ORDER BY fec_anio ASC
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR O CONSULTAR UN DIA FERIADO
    public function metMostrarFeriado($idFeriado)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c069_feriados
                WHERE  pk_num_feriado='$idFeriado'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE REGISTRAR DIA FERIADO
    public function metRegistrarFeriado($anio,$dia,$descripcion,$flag_variable,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();



            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                rh_c069_feriados
             SET
                fec_anio=:fec_anio,
                fec_dia=:fec_dia,
                ind_descripcion_feriado=:ind_descripcion_feriado,
                num_flag_variable=:num_flag_variable,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              ");

        if($flag_variable==1){

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fec_anio'                 => $anio,
                ':fec_dia'                  => $dia,
                ':ind_descripcion_feriado'  => $descripcion,
                ':num_flag_variable'        => $flag_variable,
                ':num_estatus'              => $estatus
            ));

        }else{

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fec_anio'                 => '',
                ':fec_dia'                  => $dia,
                ':ind_descripcion_feriado'  => $descripcion,
                ':num_flag_variable'        => $flag_variable,
                ':num_estatus'              => $estatus
            ));

        }



        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR DIA FERIADO
    public function metModificarFeriado($idFeriado,$anio,$dia,$descripcion,$flag_variable,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_c069_feriados
             SET
                fec_anio=:fec_anio,
                fec_dia=:fec_dia,
                ind_descripcion_feriado=:ind_descripcion_feriado,
                num_flag_variable=:num_flag_variable,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
             WHERE
               pk_num_feriado='$idFeriado'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fec_anio'                 => $anio,
            ':fec_dia'                  => $dia,
            ':ind_descripcion_feriado'  => $descripcion,
            ':num_flag_variable'        => $flag_variable,
            ':num_estatus'              => $estatus
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    #PERMITE OBTENER DIAS FERIADOS SEGUN RANGO DE FECHAS
    public function metObtenerDiasFeriados($fecha_ini,$fecha_fin)
    {

        list($dia_desde, $mes_desde, $anio_desde) = explode('-', $fecha_ini); $DiaDesde = "$mes_desde-$dia_desde";
        list($dia_hasta, $mes_hasta, $anio_hasta) = explode('-', $fecha_fin); $DiaHasta = "$mes_hasta-$dia_hasta";

        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    rh_c069_feriados
                WHERE
                    (num_flag_variable = 1 AND
				    (fec_anio = '$anio_desde' OR fec_anio = '$anio_hasta') AND
				    (fec_dia >= '$DiaDesde' AND fec_dia <= '$DiaHasta')) OR
				    (num_flag_variable = 0 AND fec_dia >= '$DiaDesde' AND fec_dia <= '$DiaHasta')
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $data = $con->fetchAll();

        $dias_feriados=0;

        for($i=0;$i<count($data);$i++){

            list($mes, $dia) = explode('-', $data[$i]['fec_dia']);

            if ($data[$i]['fec_anio'] == "") $anio = $anio_desde; else $anio = $data[$i]['fec_anio'];

            $fecha = "$dia-$mes-$anio";

            $dia_semana = $this->metObtenerDiaSemana($fecha);

            if ($dia_semana >= 1 && $dia_semana <= 5) $dias_feriados++;

            if ($anio_desde != $anio_hasta) {

                if ($data[$i]['fec_anio'] == "") $anio = $anio_hasta; else $anio = $data[$i]['fec_anio'];

                $fecha = "$dia-$mes-$anio";

                $dia_semana = $this->metObtenerDiaSemana($fecha);

                if ($dia_semana >= 1 && $dia_semana <= 5) $dias_feriados++;

            }

        }

        return $dias_feriados;

    }

    #PERMITE OBTENER EL DIA DE LA SEMANA DE UNA FECHA
    public function metObtenerDiaSemana($fecha)
    {

        // primero creo un array para saber los días de la semana
        $dias = array(0, 1, 2, 3, 4, 5, 6);
        $dia = substr($fecha, 0, 2);
        $mes = substr($fecha, 3, 2);
        $anio = substr($fecha, 6, 4);
        // en la siguiente instrucción $pru toma el día de la semana, lunes, martes,
        $dato = strtoupper($dias[intval((date("w",mktime(0,0,0,$mes,$dia,$anio))))]);
        return $dato;

    }

    #PERMITE OBTENER DIAS HABILES
    public function metObtenerDiasHabiles($fecha_ini,$fecha_fin)
    {

        $dias_completos = $this->metObtenerDiasFecha($fecha_ini,$fecha_fin);

        $dias_feriados = $this->metObtenerDiasFeriados($fecha_ini,$fecha_fin);

        $dia_semana = $this->metObtenerDiaSemana($fecha_ini);

        $dias_habiles = 0;

        for ($i=0; $i<=$dias_completos; $i++) {

            if ($dia_semana >= 1 && $dia_semana <= 5) $dias_habiles++;

            $dia_semana++;

            if ($dia_semana == 7) $dia_semana = 0;

        }

        $dias_habiles -= $dias_feriados;

        return $dias_habiles;

    }

    public function metObtenerDiasFecha($fecha_ini,$fecha_fin)
    {

        list($dd, $md, $ad) = explode( '-', $fecha_ini);	$desde = "$ad-$md-$dd";

        list($dh, $mh, $ah) = explode( '-', $fecha_fin);	$hasta = "$ah-$mh-$dh";

        #diferencia de dias entre las fechas
        $con = $this->_db->query("SELECT DATEDIFF('$hasta', '$desde') as valor");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        $f = $con->fetch();

        return $f['valor'];

    }


    #PERMITE ELIMINAR DIA FERIADO
    public function metEliminarFeriado($idFeriado)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c069_feriados where pk_num_feriado ='$idFeriado'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
