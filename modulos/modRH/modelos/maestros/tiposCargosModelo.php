<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class tiposCargosModelo extends Modelo
{

    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    //conuslta todos los tipos de cargo en la bd
    public function metConsultarTiposCargos()
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query("
              SELECT
                *
              FROM
                rh_c006_tipo_cargo");

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metMostrarTiposCargos($idTiposCargos)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT * FROM rh_c006_tipo_cargo WHERE pk_num_cargo='$idTiposCargos'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    //registra el nuevo tipo de cargo en la bd
    public function metRegistrarTipoCargo($nombre, $descripcion, $funcion)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c006_tipo_cargo
                (ind_nombre_cargo,txt_descripcion,txt_funcion,fec_ultima_modificacion,fk_a018_num_seguridad_usuario)
                VALUES
                (:ind_nombre_cargo, :txt_descripcion, :txt_funcion,NOW(),'$this->atIdUsuario')
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_nombre_cargo' => $nombre,
            ':txt_descripcion' => $descripcion,
            ':txt_funcion' => $funcion
        ));



        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }


    public function metModificarTipoCargo($idTiposCargos,$nombre, $descripcion, $funcion)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              rh_c006_tipo_cargo
            SET
              ind_nombre_cargo=:ind_nombre_cargo,
              txt_descripcion=:txt_descripcion,
              txt_funcion=:txt_funcion,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE
            pk_num_cargo='$idTiposCargos'
            "
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_nombre_cargo' => $nombre,
            ':txt_descripcion' => $descripcion,
            ':txt_funcion' => $funcion
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }


    public function metEliminarTipoCargo($idTiposCargos)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "  delete from rh_c006_tipo_cargo where pk_num_cargo ='$idTiposCargos'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


    #metodo para obtener todos los registros guardados de los post
    public function getAllPruebaPost()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
                        "SELECT * FROM Prueba_Post"
                    );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #Metodo para consultar el post seleccionado
    public function getPruebaPost($idPost)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query(
            "SELECT * FROM Prueba_Post where idPost = $idPost"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetch — Devuelve un unico registro seleccionado
        return $pruebaPost->fetch();
    }

    #metodo para guardar el registro del post
    public function setPruebaPost($titulo,$descripcion,$usuario)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "insert into Prueba_Post values (null, :titulo, :descripcion, :App_Usuarios)"
        );
        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':titulo' => $titulo,
            ':descripcion' => $descripcion,
            ':App_Usuarios' => $usuario
        ));
        $idPost= $this->_db->lastInsertId();
        #commit — Consigna una transacción
        $this->_db->commit();
        return $idPost;
    }

    public function updatePruebaPost($titulo,$descripcion,$usuario,$idPost)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "update Prueba_Post set titulo = '$titulo', " .
            "descripcion = '$descripcion', App_Usuarios = '$usuario'" .
            "where idPost = '$idPost'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function deletePruebaPost($idPost)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from Prueba_Post " .
            "where idPost = '$idPost'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
    }

}
