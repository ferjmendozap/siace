<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class sueldosMinimosModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarSueldoMinimo()
    {

        $con = $this->_db->query("
                SELECT
                 *
                FROM
                rh_c072_sueldos_minimos
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR O CONSULTAR SUELDO MINIMO
    public function metMostrarSueldoMinimo($idSueldoMinimo)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                rh_c072_sueldos_minimos
                WHERE  pk_num_sueldo_minimo='$idSueldoMinimo'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE REGISTRAR SUELDO MINIMO
    public function metRegistrarSueldoMinimo($periodo,$monto)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #verifico con los periodo para no esten repetidos
        $con = $this->_db->query("select * from rh_c072_sueldos_minimos where ind_periodo_sueldo='$periodo'");
        if($con->fetchColumn() > 0){

            return 'rep';//repetido PERIODO

        }else{

                #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
                $NuevoRegistro = $this->_db->prepare(
                    "INSERT INTO
                    rh_c072_sueldos_minimos
                 SET
                    ind_periodo_sueldo=:ind_periodo_sueldo,
                    num_monto_sueldo=:num_monto_sueldo,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
                  ");

                #execute — Ejecuta una sentencia preparada
                $NuevoRegistro->execute(array(
                    ':ind_periodo_sueldo' => $periodo,
                    ':num_monto_sueldo'   => $monto,
                ));


        }


        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR SUELDO MINIMO
    public function metModificarSueldoMinimo($idSueldoMinimo,$periodo,$monto)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_c072_sueldos_minimos
             SET
                    ind_periodo_sueldo=:ind_periodo_sueldo,
                    num_monto_sueldo=:num_monto_sueldo,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
             WHERE
               pk_num_sueldo_minimo='$idSueldoMinimo'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':ind_periodo_sueldo' => $periodo,
            ':num_monto_sueldo'   => $monto,
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    #PERMITE ELIMINAR SUELDO MINIMO
    public function metEliminarSueldoMinimo($idSueldoMinimo)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c072_sueldos_minimos where pk_num_sueldo_minimo ='$idSueldoMinimo'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
