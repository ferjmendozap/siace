<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class gradoSalarialModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE MOSTRAR TODOS LOS GRADOS SALARIAL EXISTENTES
    public function metConsultarGradoSalarial()
    {

        $con = $this->_db->query("
             SELECT
            rh_c007_grado_salarial.pk_num_grado,
            rh_c007_grado_salarial.fk_a006_num_miscelaneo_detalle,
            a006_miscelaneo_detalle.ind_nombre_detalle,
            rh_c007_grado_salarial.ind_grado,
            rh_c007_grado_salarial.ind_nombre_grado,
            rh_c007_grado_salarial.num_sueldo_minimo,
            rh_c007_grado_salarial.num_sueldo_maximo,
            rh_c007_grado_salarial.num_sueldo_promedio,
            rh_c007_grado_salarial.num_estatus,
            rh_c007_grado_salarial.flag_pasos
            FROM
            a006_miscelaneo_detalle
            INNER JOIN rh_c007_grado_salarial ON rh_c007_grado_salarial.fk_a006_num_miscelaneo_detalle = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            ORDER BY rh_c007_grado_salarial.ind_grado
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE BUSCAR UN GRADO SALARIAL ESPECIFICO PARA SER MOSTRADO EN EL FORMULARIO
    public function metMostrarGradoSalarial($idGradoSalarial)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT
            rh_c007_grado_salarial.pk_num_grado,
            rh_c007_grado_salarial.fk_a006_num_miscelaneo_detalle,
            a006_miscelaneo_detalle.ind_nombre_detalle,
            rh_c007_grado_salarial.ind_grado,
            rh_c007_grado_salarial.ind_nombre_grado,
            rh_c007_grado_salarial.num_sueldo_minimo,
            rh_c007_grado_salarial.num_sueldo_maximo,
            rh_c007_grado_salarial.num_sueldo_promedio,
            rh_c007_grado_salarial.num_estatus,
            rh_c007_grado_salarial.flag_pasos
            FROM
            a006_miscelaneo_detalle
            INNER JOIN rh_c007_grado_salarial ON rh_c007_grado_salarial.fk_a006_num_miscelaneo_detalle = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            WHERE rh_c007_grado_salarial.pk_num_grado='$idGradoSalarial'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    public function metMostrarPasosSueldo($idGradoSalarial=false,$Paso)
    {
        #ejecuto la consulta a la base de datos


            $con = $this->_db->query(
                "SELECT
                rh_c027_grado_salarial_pasos.num_sueldo_minimo,
                rh_c027_grado_salarial_pasos.num_sueldo_maximo,
                rh_c027_grado_salarial_pasos.num_sueldo_promedio
            FROM
                rh_c027_grado_salarial_pasos
            WHERE rh_c027_grado_salarial_pasos.fk_rhc007_num_grado='$idGradoSalarial' and fk_a006_num_miscelaneo_detalle_paso='$Paso'"
            );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    public function metMostrarPasosCargoSueldo($idCargo=false,$Paso)
    {
        #busco primero el grado salarial segun el cargo

        $con = $this->_db->query(
            "SELECT
                fk_rhc007_num_grado
            FROM
                rh_c063_puestos
            WHERE pk_num_puestos='$idCargo'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $idGradoSalarial = $resultado['fk_rhc007_num_grado'];

        $con1 = $this->_db->query(
            "SELECT
                rh_c027_grado_salarial_pasos.num_sueldo_minimo,
                rh_c027_grado_salarial_pasos.num_sueldo_maximo,
                rh_c027_grado_salarial_pasos.num_sueldo_promedio
            FROM
                rh_c027_grado_salarial_pasos
            WHERE rh_c027_grado_salarial_pasos.fk_rhc007_num_grado='$idGradoSalarial' and fk_a006_num_miscelaneo_detalle_paso='$Paso'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con1->setFetchMode(PDO::FETCH_ASSOC);
        return $con1->fetch();
    }

    #PERMITE VISUALIZAR LOS PASOS DE UN GRADO
    public function metVisualizarPasos($idGradoSalarial)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT
                rh_c027_grado_salarial_pasos.pk_num_grados_pasos,
                rh_c027_grado_salarial_pasos.fk_rhc007_num_grado,
                rh_c027_grado_salarial_pasos.fk_a006_num_miscelaneo_detalle_paso,
                a006_miscelaneo_detalle.cod_detalle,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                rh_c027_grado_salarial_pasos.num_sueldo_minimo,
                rh_c027_grado_salarial_pasos.num_sueldo_maximo,
                rh_c027_grado_salarial_pasos.num_sueldo_promedio
            FROM
                rh_c027_grado_salarial_pasos
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c027_grado_salarial_pasos.fk_a006_num_miscelaneo_detalle_paso
            WHERE rh_c027_grado_salarial_pasos.fk_rhc007_num_grado='$idGradoSalarial'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metVerificarPasos($idGradoSalarial)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT
                count(*) as total
            FROM
                rh_c027_grado_salarial_pasos
            WHERE rh_c027_grado_salarial_pasos.fk_rhc007_num_grado='$idGradoSalarial'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    public function metPasosGradoSalarial($idGradoSalarial)
    {

        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT
                rh_c027_grado_salarial_pasos.pk_num_grados_pasos,
                rh_c027_grado_salarial_pasos.fk_rhc007_num_grado,
                rh_c027_grado_salarial_pasos.fk_a006_num_miscelaneo_detalle_paso,
                rh_c027_grado_salarial_pasos.num_sueldo_minimo,
                rh_c027_grado_salarial_pasos.num_sueldo_maximo,
                rh_c027_grado_salarial_pasos.num_sueldo_promedio,
                a006_miscelaneo_detalle.cod_detalle,
                a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
                rh_c027_grado_salarial_pasos
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c027_grado_salarial_pasos.fk_a006_num_miscelaneo_detalle_paso
            WHERE rh_c027_grado_salarial_pasos.fk_rhc007_num_grado='$idGradoSalarial'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();

    }

    #MUESTRA LOS GRADOS SALARIALES DE ACUERDO A LA CATEGORIA DEL CARGO SELECCIONADA
    public function metMostrarGrado($Categoria)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT
            rh_c007_grado_salarial.pk_num_grado,
            rh_c007_grado_salarial.fk_a006_num_miscelaneo_detalle,
            a006_miscelaneo_detalle.ind_nombre_detalle,
            rh_c007_grado_salarial.ind_grado,
            rh_c007_grado_salarial.ind_nombre_grado,
            FORMAT(rh_c007_grado_salarial.num_sueldo_minimo,2) as num_sueldo_minimo,
            FORMAT(rh_c007_grado_salarial.num_sueldo_maximo,2) as num_sueldo_maximo,
            FORMAT(rh_c007_grado_salarial.num_sueldo_promedio,2) as num_sueldo_promedio,
            rh_c007_grado_salarial.num_estatus
            FROM
            a006_miscelaneo_detalle
            INNER JOIN rh_c007_grado_salarial ON rh_c007_grado_salarial.fk_a006_num_miscelaneo_detalle = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            WHERE rh_c007_grado_salarial.fk_a006_num_miscelaneo_detalle='$Categoria'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metConsultarCategoria()
    {

        $con = $this->_db->query("
             SELECT
            a005_miscelaneo_maestro.cod_maestro,
            a006_miscelaneo_detalle.pk_num_miscelaneo_detalle,
            a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
            a005_miscelaneo_maestro
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro = a005_miscelaneo_maestro.pk_num_miscelaneo_maestro
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #METODO PARA REGISTRAR EL GRADO SALARIAL
    public function metRegistrarGradoSalarial($misc,$grado,$nombre,$min = false,$max = false,$pro = false,$est,$pasos = false,$flag_pasos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c007_grado_salarial
                (fk_a006_num_miscelaneo_detalle,ind_grado,ind_nombre_grado,flag_pasos,num_sueldo_minimo,num_sueldo_maximo,num_sueldo_promedio,num_estatus,fk_a018_num_seguridad_usuario,fec_ultima_modificacion)
                VALUES
                (:fk_a006_num_miscelaneo_detalle,:ind_grado,:ind_nombre_grado,:flag_pasos,:num_sueldo_minimo,:num_sueldo_maximo,:num_sueldo_promedio,:num_estatus,'$this->atIdUsuario',NOW())
              ");

        if($flag_pasos==1){
            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle' => $misc,
                ':ind_grado' => $grado,
                ':ind_nombre_grado' => $nombre,
                ':flag_pasos' => $flag_pasos,
                ':num_sueldo_minimo' => 0,
                ':num_sueldo_maximo' => 0,
                ':num_sueldo_promedio' => 0,
                ':num_estatus' => $est
            ));
        }else{
            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle' => $misc,
                ':ind_grado' => $grado,
                ':ind_nombre_grado' => $nombre,
                ':flag_pasos' => $flag_pasos,
                ':num_sueldo_minimo' => $min,
                ':num_sueldo_maximo' => $max,
                ':num_sueldo_promedio' => $pro,
                ':num_estatus' => $est
            ));
        }


        $error = $NuevoRegistro->errorInfo();

        $idRegistro= $this->_db->lastInsertId();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            if($flag_pasos==1){

                if($pasos){

                    $regPasos = $this->_db->prepare(
                        "
                        INSERT INTO rh_c027_grado_salarial_pasos
                        SET 
                          fk_rhc007_num_grado=:fk_rhc007_num_grado,
                          fk_a006_num_miscelaneo_detalle_paso=:fk_a006_num_miscelaneo_detalle_paso,
                          num_sueldo_minimo=:num_sueldo_minimo,
                          num_sueldo_maximo=:num_sueldo_maximo,
                          num_sueldo_promedio=:num_sueldo_promedio,
                          fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                          fec_ultima_modificacion=now()
                        "
                    );

                    for ($i = 0; $i < count($pasos); $i++) {

                        $regPasos->execute(array(
                            'fk_rhc007_num_grado' => $idRegistro,
                            'fk_a006_num_miscelaneo_detalle_paso' => $pasos[$i],
                            'num_sueldo_minimo' => $min[$i],
                            'num_sueldo_maximo' => $max[$i],
                            'num_sueldo_promedio' => $pro[$i]
                        ));
                    }
                }

                $error1 = $regPasos->errorInfo();

                if(!empty($error1[1]) && !empty($error1[2])){
                    $this->_db->rollBack();
                    return $error1;
                }else{
                    $this->_db->commit();
                    return $idRegistro;
                }


            }else{

                $this->_db->commit();
                return $idRegistro;

            }
        }
    }

    #PERMITE MODIFICAR UN REGISTRO EN LA BASE DE DATOS (GRADO SALARIAL)
    public function metModificarGradoSalarial($idGradoSalarial,$misc,$grado,$nombre,$min = false,$max = false,$pro = false,$est,$pasos = false, $idPasos = false,$flag_pasos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              rh_c007_grado_salarial
            SET
              fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle,
              ind_grado=:ind_grado,
              ind_nombre_grado=:ind_nombre_grado,
              flag_pasos=:flag_pasos,
              num_sueldo_minimo=:num_sueldo_minimo,
              num_sueldo_maximo=:num_sueldo_maximo,
              num_sueldo_promedio=:num_sueldo_promedio,
              num_estatus=:num_estatus,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
            WHERE
            pk_num_grado='$idGradoSalarial'");

        if($flag_pasos==1){
            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle' => $misc,
                ':ind_grado' => $grado,
                ':ind_nombre_grado' => $nombre,
                ':flag_pasos' => $flag_pasos,
                ':num_sueldo_minimo' => 0,
                ':num_sueldo_maximo' => 0,
                ':num_sueldo_promedio' => 0,
                ':num_estatus' => $est
            ));
        }else{
            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle' => $misc,
                ':ind_grado' => $grado,
                ':ind_nombre_grado' => $nombre,
                ':flag_pasos' => $flag_pasos,
                ':num_sueldo_minimo' => $min,
                ':num_sueldo_maximo' => $max,
                ':num_sueldo_promedio' => $pro,
                ':num_estatus' => $est
            ));
        }

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            if($pasos){

                for ($i = 0; $i < count($pasos); $i++) {

                    if (isset($idPasos[$i])) {

                        $nuevoDetalle = $this->_db->prepare("
                            UPDATE
                              rh_c027_grado_salarial_pasos
                            SET
                              num_sueldo_minimo=:num_sueldo_minimo,
                              num_sueldo_maximo=:num_sueldo_maximo,
                              num_sueldo_promedio=:num_sueldo_promedio,
                              fk_a018_num_seguridad_usuario=$this->atIdUsuario,
                              fec_ultima_modificacion=NOW()
                            WHERE
                              fk_rhc007_num_grado=:fk_rhc007_num_grado AND pk_num_grados_pasos=$idPasos[$i]
                        ");

                        $nuevoDetalle->execute(array(
                            'fk_rhc007_num_grado' => $idGradoSalarial,
                            'num_sueldo_minimo' => $min[$i],
                            'num_sueldo_maximo' => $max[$i],
                            'num_sueldo_promedio' => $pro[$i]
                        ));


                    }else{

                            $nuevoDetalle = $this->_db->prepare("
                                INSERT INTO
                                  rh_c027_grado_salarial_pasos
                                SET
                                  fk_rhc007_num_grado=:fk_rhc007_num_grado,
                                  fk_a006_num_miscelaneo_detalle_paso=:fk_a006_num_miscelaneo_detalle_paso,
                                  num_sueldo_minimo=:num_sueldo_minimo,
                                  num_sueldo_maximo=:num_sueldo_maximo,
                                  num_sueldo_promedio=:num_sueldo_promedio,
                                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                                  fec_ultima_modificacion=NOW()
                            ");

                            $nuevoDetalle->execute(array(
                                'fk_rhc007_num_grado' => $idGradoSalarial,
                                'fk_a006_num_miscelaneo_detalle_paso' => $pasos[$i],
                                'num_sueldo_minimo' => $min[$i],
                                'num_sueldo_maximo' => $max[$i],
                                'num_sueldo_promedio' => $pro[$i]
                            ));

                    }



                }
            }

            if (isset($nuevoDetalle)) {
                $error1 = $nuevoDetalle->errorInfo();
            } else {
                $error1 = array(1 => null, 2 => null);
            }

            if(!empty($error1[1]) && !empty($error1[2])){
                $this->_db->rollBack();
                return $error1;
            }else{

                $this->_db->commit();
                return $idGradoSalarial;

            }

        }
    }

    #PERMITE ELIMINAR UN GRADO SALARIAL DE LA BD
    public function metEliminarGradoSalarial($idGradoSalarial)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c007_grado_salarial where pk_num_grado ='$idGradoSalarial'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}//fin clase
