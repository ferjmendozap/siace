<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';
class profesionesModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metConsultarProfesiones()
    {

        $con = $this->_db->query("
                SELECT
                rh_c057_profesiones.pk_num_profesion,
                rh_c057_profesiones.fk_a006_num_miscelaneo_detalle_areaformacion,
                rh_c057_profesiones.fk_a006_num_miscelaneo_detalle_gradoinst,
                rh_c057_profesiones.ind_nombre_profesion,
                rh_c057_profesiones.num_estatus,
                rh_c057_profesiones.ind_ultimo_usuario,
                rh_c057_profesiones.fec_ultima_modificacion,
                misc1.ind_nombre_detalle AS areaformacion,
                misc2.ind_nombre_detalle AS grado_instruccion
                FROM
                rh_c057_profesiones
                INNER JOIN a006_miscelaneo_detalle as misc1 ON rh_c057_profesiones.fk_a006_num_miscelaneo_detalle_areaformacion = misc1.pk_num_miscelaneo_detalle
                INNER JOIN a006_miscelaneo_detalle as misc2 ON misc2.pk_num_miscelaneo_detalle = rh_c057_profesiones.fk_a006_num_miscelaneo_detalle_gradoinst
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metMostrarProfesion($idProfesion)
    {

        $con = $this->_db->query("
                SELECT
                rh_c057_profesiones.pk_num_profesion,
                rh_c057_profesiones.fk_a006_num_miscelaneo_detalle_areaformacion,
                rh_c057_profesiones.fk_a006_num_miscelaneo_detalle_gradoinst,
                rh_c057_profesiones.ind_nombre_profesion,
                rh_c057_profesiones.num_estatus,
                rh_c057_profesiones.ind_ultimo_usuario,
                rh_c057_profesiones.fec_ultima_modificacion,
                misc1.ind_nombre_detalle AS areaformacion,
                misc2.ind_nombre_detalle AS grado_instruccion
                FROM
                rh_c057_profesiones
                INNER JOIN a006_miscelaneo_detalle as misc1 ON rh_c057_profesiones.fk_a006_num_miscelaneo_detalle_areaformacion = misc1.pk_num_miscelaneo_detalle
                INNER JOIN a006_miscelaneo_detalle as misc2 ON misc2.pk_num_miscelaneo_detalle = rh_c057_profesiones.fk_a006_num_miscelaneo_detalle_gradoinst
                WHERE  rh_c057_profesiones.pk_num_profesion='$idProfesion'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    public function metMostrarProfesionesGradoArea($GradoInst,$Area)
    {

        $con = $this->_db->query("
               SELECT pk_num_profesion,ind_nombre_profesion
               FROM rh_c057_profesiones
               WHERE fk_a006_num_miscelaneo_detalle_gradoinst='$GradoInst' AND fk_a006_num_miscelaneo_detalle_areaformacion='$Area' AND num_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();

    }

    #PERMITE REGISTRAR UNA PROFESION
    public function metRegistrarProfesion($area,$grado,$prof,$est)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c057_profesiones
             SET
                fk_a006_num_miscelaneo_detalle_areaformacion=:fk_a006_num_miscelaneo_detalle_areaformacion,
                fk_a006_num_miscelaneo_detalle_gradoinst=:fk_a006_num_miscelaneo_detalle_gradoinst,
                ind_nombre_profesion=:ind_nombre_profesion,
                num_estatus=:ind_estatus,
                ind_ultimo_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_areaformacion' => $area,
            ':fk_a006_num_miscelaneo_detalle_gradoinst'  => $grado,
            ':ind_nombre_profesion'           => $prof,
            ':ind_estatus'                    => $est
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR UNA PROFESION EN LA BD
    public function metModificarProfesion($idProfesion,$area,$grado,$prof,$est)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_c057_profesiones
             SET
               fk_a006_num_miscelaneo_detalle_areaformacion=:fk_a006_num_miscelaneo_detalle_areaformacion,
               fk_a006_num_miscelaneo_detalle_gradoinst=:fk_a006_num_miscelaneo_detalle_gradoinst,
               ind_nombre_profesion=:ind_nombre_profesion,
               num_estatus=:ind_estatus,
               ind_ultimo_usuario='$this->atIdUsuario',
               fec_ultima_modificacion=NOW()
             WHERE
               pk_num_profesion='$idProfesion'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_areaformacion' => $area,
            ':fk_a006_num_miscelaneo_detalle_gradoinst'     => $grado,
            ':ind_nombre_profesion'           => $prof,
            ':ind_estatus'                    => $est
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
        }
    }

    #PERMITE ELIMINAR UNA PROFESION DE LA BD
    public function metEliminarProfesion($idProfesion)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c057_profesiones where pk_num_profesion ='$idProfesion'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}
