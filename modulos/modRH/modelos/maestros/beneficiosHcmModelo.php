<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class beneficiosHcmModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    #PERMITE CONSULTAR TODOS LOS REGISTROS
    public function metConsultarBeneficiosHcm()
    {

        $con = $this->_db->query("
                SELECT
                rh_c068_ayuda_especifica.pk_num_ayuda_especifica,
                rh_c068_ayuda_especifica.fk_rhb008_num_ayuda_global,
                rh_c068_ayuda_especifica.ind_descripcion_especifica,
                rh_c068_ayuda_especifica.num_limite_esp,
                rh_c068_ayuda_especifica.fk_rhb001_num_empleado,
                rh_c068_ayuda_especifica.num_estatus,
                rh_c068_ayuda_especifica.fk_a018_num_seguridad_usuario,
                rh_c068_ayuda_especifica.fec_ultima_modificacion,
                rh_c068_ayuda_especifica.flag_extensible,
                rh_b008_ayuda_global.ind_descripcion
                FROM
                rh_c068_ayuda_especifica
                INNER JOIN rh_b001_empleado ON rh_c068_ayuda_especifica.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_b008_ayuda_global ON rh_c068_ayuda_especifica.fk_rhb008_num_ayuda_global = rh_b008_ayuda_global.pk_num_ayuda_global
                INNER JOIN a018_seguridad_usuario ON rh_c068_ayuda_especifica.fk_a018_num_seguridad_usuario = a018_seguridad_usuario.pk_num_seguridad_usuario AND rh_b008_ayuda_global.fk_a018_num_seguridad_usuario = a018_seguridad_usuario.pk_num_seguridad_usuario

               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR O CONSULTAR BENEFICIO GLOBAL HCM
    public function metMostrarBeneficioHcm($idBeneficioHcm)
    {

        $con = $this->_db->query("
                SELECT
                rh_c068_ayuda_especifica.pk_num_ayuda_especifica,
                rh_c068_ayuda_especifica.fk_rhb008_num_ayuda_global,
                rh_c068_ayuda_especifica.ind_descripcion_especifica,
                rh_c068_ayuda_especifica.num_limite_esp,
                rh_c068_ayuda_especifica.fk_rhb001_num_empleado,
                rh_c068_ayuda_especifica.num_estatus,
                rh_c068_ayuda_especifica.fk_a018_num_seguridad_usuario,
                rh_c068_ayuda_especifica.fec_ultima_modificacion,
                rh_c068_ayuda_especifica.flag_extensible,
                rh_b008_ayuda_global.ind_descripcion
                FROM
                rh_c068_ayuda_especifica
                INNER JOIN rh_b001_empleado ON rh_c068_ayuda_especifica.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_b008_ayuda_global ON rh_c068_ayuda_especifica.fk_rhb008_num_ayuda_global = rh_b008_ayuda_global.pk_num_ayuda_global
                INNER JOIN a018_seguridad_usuario ON rh_c068_ayuda_especifica.fk_a018_num_seguridad_usuario = a018_seguridad_usuario.pk_num_seguridad_usuario AND rh_b008_ayuda_global.fk_a018_num_seguridad_usuario = a018_seguridad_usuario.pk_num_seguridad_usuario
                WHERE  rh_c068_ayuda_especifica.pk_num_ayuda_especifica='$idBeneficioHcm'"
        );

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    #PERMITE LISTAR LOS EMPLEADOS
    public function metListarEmpleados()
    {

        $con = $this->_db->query("
              SELECT
                rh_b001_empleado.pk_num_empleado,
                rh_b001_empleado.fk_a003_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2
                FROM
                rh_b001_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();

    }

    #PERMITE REGISTRAR BENEFICIO HCM
    public function metRegistrarBeneficioHcm($ben_global,$descripcion,$limite,$aprueba,$estatus,$extensible)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c068_ayuda_especifica
             SET
                fk_rhb008_num_ayuda_global=:fk_rhb008_num_ayuda_global,
                ind_descripcion_especifica=:ind_descripcion_especifica,
                num_limite_esp=:num_limite_esp,
                fk_rhb001_num_empleado=:num_empleado_apro,
                num_estatus=:num_estatus,
                flag_extensible=:flag_extensible,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_rhb008_num_ayuda_global' => $ben_global,
            ':ind_descripcion_especifica' => $descripcion,
            ':num_limite_esp' => $limite,
            ':num_empleado_apro' => $aprueba,
            ':num_estatus' => $estatus,
            ':flag_extensible' => $extensible
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PERMITE MODIFICAR BENEFICIO HCM
    public function metModificarBeneficioHcm($idBeneficioHcm,$ben_global,$descripcion,$limite,$aprueba,$estatus,$extensible)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
               rh_c068_ayuda_especifica
             SET
                fk_rhb008_num_ayuda_global=:fk_rhb008_num_ayuda_global,
                ind_descripcion_especifica=:ind_descripcion_especifica,
                num_limite_esp=:num_limite_esp,
                fk_rhb001_num_empleado=:num_empleado_apro,
                num_estatus=:num_estatus,
                flag_extensible=:flag_extensible,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
             WHERE
               pk_num_ayuda_especifica='$idBeneficioHcm'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_rhb008_num_ayuda_global' => $ben_global,
            ':ind_descripcion_especifica' => $descripcion,
            ':num_limite_esp' => $limite,
            ':num_empleado_apro' => $aprueba,
            ':num_estatus' => $estatus,
            ':flag_extensible' => $extensible
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idBeneficioHcm;
        }
    }

    #PERMITE ELIMINAR BENEFICIO HCM
    public function metEliminarBeneficioHcm($idBeneficioHcm)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c068_ayuda_especifica where pk_num_ayuda_especifica ='$idBeneficioHcm'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


    #PERMITE VERIFICAR EL TOTAL DE LOS BENEFICIOS CON EL MONTO LIMUTE DE LA AYUDA GLOBAL
    public function metVerificarLimite($ayudaGlobal,$monto,$beneficio=false)
    {
        $con = $this->_db->query("
              SELECT
                num_limite
                FROM
                rh_b008_ayuda_global
                WHERE pk_num_ayuda_global='$ayudaGlobal'
        ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado =  $con->fetch();
        $limite = $resultado['num_limite'];

        if(!$beneficio) {
            $con1 = $this->_db->query("
                  select SUM(num_limite_esp) as total FROM rh_c068_ayuda_especifica where fk_rhb008_num_ayuda_global='$ayudaGlobal'
            ");
        }else{
            $con1 = $this->_db->query("
                  select SUM(num_limite_esp) as total FROM rh_c068_ayuda_especifica where fk_rhb008_num_ayuda_global='$ayudaGlobal' and pk_num_ayuda_especifica!='$beneficio'
            ");
        }
        $con1->setFetchMode(PDO::FETCH_ASSOC);
        $resultado1 =  $con1->fetch();
        $total_asignaciones = $resultado1['total'];
        $total = $total_asignaciones + $monto;

        if($total > $limite){
            return $total."#".$limite;//el total de los montos de las asignaciones no puede ser mayor al limite global del beneficio
        }else{
            return 1;
        }


    }


    public function metMostrarAsignacionesExtensibles($idBeneficioGlobal)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT * FROM rh_c068_ayuda_especifica WHERE fk_rhb008_num_ayuda_global='$idBeneficioGlobal' and flag_extensible=1"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metMostrarAsignaciones($idBeneficioGlobal)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT * FROM rh_c068_ayuda_especifica WHERE fk_rhb008_num_ayuda_global='$idBeneficioGlobal'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metMontoAsignado($idAsignacion)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT num_limite_esp FROM rh_c068_ayuda_especifica WHERE pk_num_ayuda_especifica='$idAsignacion'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


}
