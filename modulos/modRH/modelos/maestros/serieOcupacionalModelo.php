<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class serieOcupacionalModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metConsultarSerieOcupacional()
    {

        $con = $this->_db->query("
             SELECT
            rh_c010_serie.pk_num_serie,
            rh_c010_serie.ind_nombre_serie,
            rh_c010_serie.fk_a006_num_miscelaneo_detalle_grupoocupacional,
            a006_miscelaneo_detalle.pk_num_miscelaneo_detalle,
            a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
            rh_c010_serie
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c010_serie.fk_a006_num_miscelaneo_detalle_grupoocupacional
            ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metMostrarSerieOcupacional($idSerieOcupacional)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT
            rh_c010_serie.pk_num_serie,
            rh_c010_serie.ind_nombre_serie,
            rh_c010_serie.fk_a006_num_miscelaneo_detalle_grupoocupacional,
            a006_miscelaneo_detalle.pk_num_miscelaneo_detalle,
            a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
            rh_c010_serie
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c010_serie.fk_a006_num_miscelaneo_detalle_grupoocupacional
            WHERE rh_c010_serie.pk_num_serie='$idSerieOcupacional'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #MUESTRA LA SERIE OCUPACIONAL SEGUN EL GRUPO OCUPACIONAL
    public function metMostrarSerie($GrupoOcupacional)
    {
        #ejecuto la consulta a la base de datos
        $con = $this->_db->query(
            "SELECT
            rh_c010_serie.pk_num_serie,
            rh_c010_serie.ind_nombre_serie,
            rh_c010_serie.fk_a006_num_miscelaneo_detalle_grupoocupacional,
            a006_miscelaneo_detalle.pk_num_miscelaneo_detalle,
            a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
            rh_c010_serie
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c010_serie.fk_a006_num_miscelaneo_detalle_grupoocupacional
            WHERE rh_c010_serie.fk_a006_num_miscelaneo_detalle_grupoocupacional='$GrupoOcupacional'"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metRegistrarSerieOcupacional($grupo,$descripcion)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c010_serie
                (fk_a006_num_miscelaneo_detalle_grupoocupacional,ind_nombre_serie,fk_a018_num_seguridad_usuario,fec_ultima_modificacion)
                VALUES
                (:fk_a006_num_miscelaneo_detalle_grupoocupacional,:ind_nombre_serie,'$this->atIdUsuario',NOW())
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_grupoocupacional' => $grupo,
            ':ind_nombre_serie' => $descripcion,
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarSerieOcupacional($idSerieOcupacional,$grupo,$descripcion)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              rh_c010_serie
            SET
              fk_a006_num_miscelaneo_detalle_grupoocupacional=:fk_a006_num_miscelaneo_detalle_grupoocupacional,
              ind_nombre_serie=:ind_nombre_serie,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
            WHERE
            pk_num_serie='$idSerieOcupacional'
            "
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_grupoocupacional' => $grupo,
            ':ind_nombre_serie' => $descripcion,
        ));

        $error = $NuevoRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idSerieOcupacional;
        }
    }


    public function metEliminarSerieOcupacional($idSerieOcupacional)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from rh_c010_serie where pk_num_serie ='$idSerieOcupacional'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


    public function metJsonSerieOcupacional($grupo)
    {
        $serie = $this->_db->query(
            "SELECT
              *
            FROM
              rh_c010_serie
            WHERE
              fk_a006_num_miscelaneo_detalle_grupoocupacional='$grupo'
            ");
        $serie->setFetchMode(PDO::FETCH_ASSOC);
        return $serie->fetchAll();
    }

}
