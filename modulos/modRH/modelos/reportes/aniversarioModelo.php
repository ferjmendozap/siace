<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class aniversarioModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.fk_a001_num_organismo, a.fk_a004_num_dependencia, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.pk_num_empleado, c.ind_nombre_detalle from vl_rh_persona_empleado_datos_laborales as a, vl_rh_persona_empleado as b, a006_miscelaneo_detalle as c where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b. 	fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }

    // Método que permite listar lo empleados de acuerdo a la busqueda
    public function metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $centroCosto, $mesIngresoNuevo, $obtenerFecha1, $obtenerFecha2, $edoReg, $sitTrab, $tipoNomina, $tipoTrabajador)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and b.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if ($centroCosto!='') {
            $filtro .= " and b.fk_a023_num_centro_costo=$centroCosto";
        }
        if (($obtenerFecha1!='')&&($obtenerFecha2!='')) {
            $filtro .= " and b.fec_ingreso between '$obtenerFecha2' and '$obtenerFecha1'";
        }
        if ($mesIngresoNuevo!='') {
            $filtro .= " and  MONTH(b.fec_ingreso) ='$mesIngresoNuevo'";
        }
        if ($edoReg!='') {
            $filtro .= " and b.ind_estatus=$edoReg";
        }
        if ($sitTrab!='') {
            $filtro .= " and b.ind_estatus=$sitTrab";
        }
        if ($tipoNomina!='') {
            $filtro .= " and b.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if ($tipoTrabajador!='') {
            $filtro .= " and b.fk_a006_num_miscelaneo_detalle_tipotrabajador=$tipoTrabajador";
        }
        $empleado = $this->_db->query(
                "select a.pk_num_empleado, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.fec_nacimiento, b.fec_ingreso, date_format(b.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, c.ind_dependencia, d.ind_descripcion_cargo, date_format(b.fec_ingreso, '%m') as mes_ingreso from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, a004_dependencia as c, rh_c063_puestos as d where a.pk_num_empleado=b.pk_num_empleado and b.fk_a004_num_dependencia=c.pk_num_dependencia and b.fk_rhc063_num_puestos_cargo=d.pk_num_puestos $filtro"
            );

        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

    // Método que permite listar los centros de costos
    public function metListarCentroCosto($pkNumOrganismo, $pkNumDependencia)
    {
        if($pkNumDependencia!=0){
            $centroCosto =  $this->_db->query(
                "select pk_num_centro_costo, ind_descripcion_centro_costo from a023_centro_costo where fk_a004_num_dependencia=$pkNumDependencia"
            );
        } else {
            $centroCosto =  $this->_db->query(
                "select a.pk_num_centro_costo, a.ind_descripcion_centro_costo from a023_centro_costo as a, a004_dependencia as b, a001_organismo as c where a.fk_a004_num_dependencia=b.pk_num_dependencia and b.fk_a001_num_organismo=c.pk_num_organismo and c.pk_num_organismo=$pkNumOrganismo"
            );
        }
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }

    // Método que permite listar los tipos de nóminas
    public function metListarNomina()
    {
        $nomina =  $this->_db->query(
            "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
        );
        $nomina->setFetchMode(PDO::FETCH_ASSOC);
        return $nomina->fetchAll();
    }

    // Método encargado de obtener la actuación en el organismo de  un funcionario
    public function metEmpleadoNivelación($pkNumEmpleado)
    {
        $empleadoNivelacion = $this->_db->query(
            "select a.pk_num_empleado_nivelacion, b.ind_cargo, b.fec_fecha_registro, a.fec_fecha_hasta, d.ind_grado from rh_c059_empleado_nivelacion as a,
rh_c060_empleado_nivelacion_historial as b, rh_c063_puestos as c, rh_c007_grado_salarial as d where a.fk_rhb001_num_empleado=$pkNumEmpleado and a.pk_num_empleado_nivelacion=b.fk_rhc059_num_empleado_nivelacion and a.fk_rhc063_num_puestos=c.pk_num_puestos and c.fk_rhc007_num_grado=d.pk_num_grado"
        );
        $empleadoNivelacion->setFetchMode(PDO::FETCH_ASSOC);
        return $empleadoNivelacion->fetchAll();
    }

    // Método que permite obtener el antecedente de servicio de un empleado
    public function metConsultarAntecedente($pkNumEmpleado)
    {
        $buscarAntecedente = $this->_db->query(
            "select a.fec_ingreso, date_format(a.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, a.fec_egreso, date_format(a.fec_egreso, '%d/%m/%Y') as fecha_egreso, a.ind_empresa from vl_rh_empleado_experiencia_laboral as a,
a006_miscelaneo_detalle as b where a.pk_num_empleado=$pkNumEmpleado and a.fk_a006_num_miscelaneo_detalle_tipo_empresa=b.pk_num_miscelaneo_detalle and  b.cod_detalle='G'"
        );
        $buscarAntecedente->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarAntecedente->fetchAll();
    }


}// fin de la clase
