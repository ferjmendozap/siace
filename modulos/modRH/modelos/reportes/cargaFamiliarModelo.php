<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class cargaFamiliarModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select fk_a001_num_organismo, fk_a004_num_dependencia from vl_rh_persona_empleado_datos_laborales where pk_num_empleado=$pkNumEmpleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite  listar a los empleados de acuerdo al buscador de trabajador al momento de registrar una nueva jubilación
    public function metBuscarTrabajador($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $tipoTrabajador, $estado_registro, $situacionTabajo, $fecha_inicio, $fecha_fin)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and b.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($situacionTabajo!=''){
            $filtro .= " and a.ind_estatus_empleado=$situacionTabajo";
        }
        if($centroCosto!=''){
            $filtro .= " and b.fk_a023_num_centro_costo=$centroCosto";
        }
        if($tipoNomina!=''){
            $filtro .= " and b.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if($tipoTrabajador!=''){
            $filtro .= " and b.fk_a006_num_miscelaneo_detalle_tipotrabajador=$tipoTrabajador";
        }
        if(($fecha_inicio!='')&&($fecha_fin!='')){
            $explodeInicio = explode("/", $fecha_inicio);
            $fechaInicio = $explodeInicio[2].'-'.$explodeInicio[1].'-'.$explodeInicio[0];
            $explodeFin = explode("/", $fecha_fin);
            $fechaFin = $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];
            $filtro .= " and b.fec_ingreso between '$fechaInicio' and '$fechaFin' ";
        }

        $buscarTrabajador = $this->_db->query(
            "select a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2 from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b where a.pk_num_empleado=b.pk_num_empleado $filtro"
        );
        $buscarTrabajador->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarTrabajador->fetchAll();
    }

    public function metListarEmpleado($pkNumDependencia, $metodo)
    {
        if($metodo==1){
            $empleado =  $this->_db->query(
                "select a.pk_num_empleado, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from vl_rh_persona_empleado as a, rh_b001_empleado as b, a003_persona as c, vl_rh_persona_empleado_datos_laborales as d where a.pk_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and d.pk_num_empleado=a.pk_num_empleado and d.fk_a004_num_dependencia=$pkNumDependencia and a.ind_estatus_empleado=1"
            );
        } else {
            $empleado =  $this->_db->query(
                "select a.pk_num_empleado, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from vl_rh_persona_empleado as a, rh_b001_empleado as b, a003_persona as c, vl_rh_persona_empleado_datos_laborales as d where a.pk_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and and d.pk_num_empleado=a.pk_num_empleado and a.ind_estatus_empleado=1"
            );
        }
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    // Método que permite listar los centros de costos
    public function metListarCentroCosto($pkNumOrganismo, $pkNumDependencia)
    {
        if($pkNumDependencia!=0){
            $centroCosto =  $this->_db->query(
                "select pk_num_centro_costo, ind_descripcion_centro_costo from a023_centro_costo where fk_a004_num_dependencia=$pkNumDependencia"
            );
        } else {
            $centroCosto =  $this->_db->query(
                "select a.pk_num_centro_costo, a.ind_descripcion_centro_costo from a023_centro_costo as a, a004_dependencia as b, a001_organismo as c where a.fk_a004_num_dependencia=b.pk_num_dependencia and b.fk_a001_num_organismo=c.pk_num_organismo and c.pk_num_organismo=$pkNumOrganismo"
            );
        }
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }

    // Método que permite listar los tipos de nóminas
    public function metListarNomina()
    {
        $nomina =  $this->_db->query(
            "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
        );
        $nomina->setFetchMode(PDO::FETCH_ASSOC);
        return $nomina->fetchAll();
    }

    // Método que permite listar lo empleados asociados a una dependencia
    public function metDependenciaEmpleado($pkNumDependencia)
    {
        $empleado =  $this->_db->query(
            "select a.pk_num_empleado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2 from rh_b001_empleado as a, a003_persona as b, rh_c076_empleado_organizacion as c where a.fk_a003_num_persona=b.pk_num_persona and c.fk_a004_num_dependencia=$pkNumDependencia and c.fk_rhb001_num_empleado=a.pk_num_empleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }

    public function metConsultarEmpleados($pkNumOrganismo, $pk_num_dependencia, $centro_costo, $tipo_nomina, $edo_reg, $sit_trab, $sexo_empleado)
    {
        if($pkNumOrganismo!=''){
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if($pk_num_dependencia!=''){
            $filtro .= " and b.fk_a004_num_dependencia=$pk_num_dependencia";
        }
        if($centro_costo!=''){
            $filtro .= " and b.fk_a023_num_centro_costo=$centro_costo";
        }
        if($tipo_nomina!=''){
            $filtro .= " and b.fk_nmb001_num_tipo_nomina=$tipo_nomina";
        }
        if($edo_reg!=''){
            $filtro .= " and b.ind_estatus=$edo_reg";
        }
        if($sit_trab!=''){
            $filtro .= " and b.ind_estatus=$sit_trab";
        }
        if($sexo_empleado!=''){
            $filtro .= " and a.fk_a006_num_miscelaneo_detalle_sexo=$sexo_empleado";
        }

        $empleado =  $this->_db->query(
            "select a.pk_num_empleado, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, c.ind_descripcion_cargo from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, rh_c063_puestos as c where a.pk_num_empleado=b.pk_num_empleado and b.fk_rhc063_num_puestos_cargo=c.pk_num_puestos $filtro"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    public function metListadoCargaFamiliar($pkNumEmpleado, $fechaFin, $fechaInicio, $parentesco, $edoCivil, $sexoFamiliar, $discapacidad, $estudio)
    {
        if($pkNumEmpleado!=''){
            $filtro = " and a.fk_rhb001_num_empleado=$pkNumEmpleado";
        }
        if(($fechaInicio!='')&&($fechaFin!='')){
            $anioActual = explode("-", $fechaInicio);
            $fechaComparar = $anioActual[0].'-01-01';
            $filtro .= " and b.fec_nacimiento between '$fechaComparar' and '$fechaFin'";
        }
        if(($fechaInicio!='')&&($fechaFin=='')){
            $filtro .= " and b.fec_nacimiento<='$fechaInicio'";
        }
        if(($fechaInicio=='')&&($fechaFin!='')){
            $filtro .= " and b.fec_nacimiento>='$fechaFin'";
        }
        if($parentesco!=''){
            $filtro .= " and a.fk_a006_num_miscelaneo_detalle_parentezco=$parentesco";
        }
        if($edoCivil!=''){
            $filtro .= " and b.fk_a006_num_miscelaneo_detalle_edocivil=$edoCivil";
        }
        if($sexoFamiliar!=''){
            $filtro .= " and b.fk_a006_num_miscelaneo_detalle_sexo=$sexoFamiliar";
        }
        if($discapacidad!=''){
            $filtro .= " and a.num_flag_discapacidad=$discapacidad";
        }
        if($estudio!=''){
            $filtro .= " and a.num_flag_estudia=$estudio";
        }

    $empleado = $this->_db->query(
            "select b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, date_format(b.fec_nacimiento, '%d/%m/%Y') as fecha_nacimiento, b.fec_nacimiento, b.ind_cedula_documento, c.ind_nombre_detalle from rh_c015_carga_familiar as a, a003_persona as b, a006_miscelaneo_detalle as c where a.fk_a003_num_persona=b.pk_num_persona and c.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_detalle_parentezco $filtro group by pk_num_carga"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

}// fin de la clase
