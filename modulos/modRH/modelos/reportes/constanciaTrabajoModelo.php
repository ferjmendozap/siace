<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class constanciaTrabajoModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.fk_a001_num_organismo, a.fk_a004_num_dependencia, a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, (c.ind_nombre_detalle) as sexo, date_format(a.fec_ingreso, '%d-%m-%Y') as fecha_ingreso, d.ind_descripcion_cargo from vl_rh_persona_empleado_datos_laborales as a, vl_rh_persona_empleado as b, a006_miscelaneo_detalle as c, rh_c063_puestos as d where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b.fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle and a.fk_rhc063_num_puestos_cargo=d.pk_num_puestos"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }

    // Método que permite listar lo empleados de acuerdo a la busqueda
    public function metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $tipoNomina, $tipoTrabajador, $cedula, $codigoEmpleado, $rango, $fechaEdad, $edo_reg, $sit_trab, $ordenar, $fechaInicio, $fechaFin, $buscar)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and a.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and a.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($tipoNomina!=''){
            $filtro .= " and a.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if($tipoTrabajador!=''){
            $filtro .= " and a.fk_a006_num_miscelaneo_detalle_tipotrabajador=$tipoTrabajador";
        }
        if($cedula!=''){
            $filtro .= " and a.ind_cedula_documento=$cedula";
        }
        if($codigoEmpleado!=''){
            $filtro .= " and a.pk_num_empleado=$codigoEmpleado";
        }
        if($buscar!=''){
            $filtro .= " and a.pk_num_empleado=$buscar";
        }
        if(($fechaInicio!='')&&($fechaFin!='')){
            $filtro .= " and a.fec_ingreso between '$fechaInicio' and '$fechaFin'";
        }
        if($sit_trab!=''){
            $filtro .= " and a.ind_estatus=$sit_trab";
        }
        if(($fechaEdad!='')&&($rango!='')){
            $filtro .= " and d.fec_nacimiento$rango'$fechaEdad'";
        }
        if($ordenar!=''){
            switch($ordenar){
                case 1:
                    $ordenarListado = " a.pk_num_empleado";
                    break;
                case 2:
                    $ordenarListado = " a.ind_nombre1";
                    break;
                case 3:
                    $ordenarListado = " a.ind_cedula_documento";
                    break;
                case 4:
                    $ordenarListado = " a.fec_ingreso";
                    break;
                case 5:
                    $ordenarListado = " a.fk_a004_num_dependencia";
                    break;
                case 6:
                    $ordenarListado = ' a.fk_rhc063_num_puestos_cargo';
                    break;
            }
        } else {
            $ordenarListado = " a.pk_num_empleado";
        }
        $empleado = $this->_db->query(
            "select a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_estatus, date_format(a.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, b.ind_dependencia, c.ind_descripcion_cargo from vl_rh_persona_empleado_datos_laborales as a, a004_dependencia as b, rh_c063_puestos as c, vl_rh_persona_empleado as d where a.fk_a004_num_dependencia=b.pk_num_dependencia and a.fk_rhc063_num_puestos_cargo=c.pk_num_puestos and a.pk_num_empleado=d.pk_num_empleado $filtro order by $ordenarListado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

    // Método que permite listar los tipos de nóminas
    public function metListarNomina()
    {
        $nomina =  $this->_db->query(
            "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
        );
        $nomina->setFetchMode(PDO::FETCH_ASSOC);
        return $nomina->fetchAll();
    }

    public function metConsultarEmpleados()
    {
        $empleado =  $this->_db->query(
            "select pk_num_empleado, ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2 from vl_rh_persona_empleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    public function metConsultarResponsableRH($pkNumOrganismo, $parametroDependencia)
    {
        $director =  $this->_db->query(
            "select a.ind_dependencia, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, c.ind_nombre_detalle, d.ind_descripcion_empresa, b.ind_cedula_documento, e.pk_num_empleado from a004_dependencia as a, a003_persona as b, a006_miscelaneo_detalle as c, a001_organismo as d, vl_rh_persona_empleado as e where a.fk_a003_num_persona_responsable=b.pk_num_persona and a.fk_a001_num_organismo=$pkNumOrganismo and a.fk_a001_num_organismo=d.pk_num_organismo and a.ind_codinterno='$parametroDependencia' and b.fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle and b.pk_num_persona=e.pk_num_persona"
        );
        $director->setFetchMode(PDO::FETCH_ASSOC);
        return $director->fetch();
    }

    // Método que permite consultar la nacionalidad de un funcionario
    public function metConsultarNacionalidad($pkNumEmpleado)
    {
        $nacionalidad = $this->_db->query(
            "select (b.ind_nombre_detalle) as nacionalidad from vl_rh_persona_empleado as a, a006_miscelaneo_detalle as b where a.pk_num_empleado=$pkNumEmpleado and a.fk_a006_num_miscelaneo_detalle_nacionalidad=b.pk_num_miscelaneo_detalle"
        );
        $nacionalidad->setFetchMode(PDO::FETCH_ASSOC);
        return  $nacionalidad->fetch();
    }

    // Método que permite consultar el salario y las primas del mes anterior en la nómina del periodo anterior
    public function metConsultarSueldo($pkNumEmpleado, $mesPeriodo, $anioPeriodo, $mesAntPeriodo = false)
    {
        $sueldo = $this->_db->query(
            "SELECT
                          (nm_e002_prestaciones_sociales_calculo.num_sueldo_base) AS num_sueldo_base,
                          nm_e002_prestaciones_sociales_calculo.num_total_asignaciones,
                          (nm_e001_prestaciones_sociales_calculo_retroactivo.num_sueldo_base) AS num_sueldo_retro,
                          nm_e001_prestaciones_sociales_calculo_retroactivo.num_total_asignaciones AS asignaciones_retro
                        FROM
                          nm_e002_prestaciones_sociales_calculo,
                          nm_e001_prestaciones_sociales_calculo_retroactivo
                        WHERE
                          (nm_e002_prestaciones_sociales_calculo.fec_anio = '$anioPeriodo' AND 
                          nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$anioPeriodo') AND
                          (nm_e002_prestaciones_sociales_calculo.fec_mes = '$mesPeriodo' AND
                          nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes = '$mesPeriodo' OR 
                          nm_e002_prestaciones_sociales_calculo.fec_mes = '$mesAntPeriodo' AND 
                          nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes = '$mesAntPeriodo') AND 
                          (nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado = '$pkNumEmpleado' AND
                          nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado = '$pkNumEmpleado')
            "
        );
        //var_dump($sueldo);
        $sueldo->setFetchMode(PDO::FETCH_ASSOC);
        return  $sueldo->fetch();
    }
}// fin de la clase
