<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class empleadoReporteModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select fk_a001_num_organismo, fk_a004_num_dependencia from vl_rh_persona_empleado_datos_laborales where pk_num_empleado=$pkNumEmpleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }

    // Método que permite listar lo empleados de acuerdo a la busqueda
    public function metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $tipoNomina, $tipoTrabajador,  $edo_reg, $sit_trab, $fechaInicio, $fechaFin, $buscar, $centroCosto)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and a.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and a.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($tipoNomina!=''){
            $filtro .= " and a.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if($tipoTrabajador!=''){
            $filtro .= " and a.fk_a006_num_miscelaneo_detalle_tipotrabajador=$tipoTrabajador";
        }
        if($buscar!=''){
            $filtro .= " and a.pk_num_empleado=$buscar";
        }
        if(($fechaInicio!='')&&($fechaFin!='')){
            $filtro .= " and a.fec_ingreso between '$fechaInicio' and '$fechaFin'";
        }
        if($sit_trab!=''){
            $filtro .= " and a.ind_estatus=$sit_trab";
        }
        if($centroCosto!=''){
            $filtro .= " and a.fk_a023_num_centro_costo=$centroCosto";
        }
        $empleado = $this->_db->query(
            "select a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_estatus, b.ind_dependencia, c.ind_descripcion_cargo from vl_rh_persona_empleado_datos_laborales as a, a004_dependencia as b, rh_c063_puestos as c, vl_rh_persona_empleado as d where a.fk_a004_num_dependencia=b.pk_num_dependencia and a.fk_rhc063_num_puestos_cargo=c.pk_num_puestos and a.pk_num_empleado=d.pk_num_empleado $filtro order by a.pk_num_empleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

    // Método que permite listar los tipos de nóminas
    public function metListarNomina()
    {
        $nomina =  $this->_db->query(
            "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
        );
        $nomina->setFetchMode(PDO::FETCH_ASSOC);
        return $nomina->fetchAll();
    }

    public function metConsultarEmpleados()
    {
        $empleado =  $this->_db->query(
            "select pk_num_empleado, ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2 from vl_rh_persona_empleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    // Método que permite listar los centros de costos
    public function metListarCentroCosto()
    {
        $centroCosto =  $this->_db->query(
            "select pk_num_centro_costo, ind_descripcion_centro_costo from a023_centro_costo"
        );
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }
}// fin de la clase
