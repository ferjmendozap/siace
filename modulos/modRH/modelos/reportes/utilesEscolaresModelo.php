<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class utilesEscolaresModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.fk_a001_num_organismo, a.fk_a004_num_dependencia, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.pk_num_empleado, c.ind_nombre_detalle from vl_rh_persona_empleado_datos_laborales as a, vl_rh_persona_empleado as b, a006_miscelaneo_detalle as c where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b. 	fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }
    }

    // Método que permite listar lo empleados de acuerdo a la busqueda
    public function metListarEmpleados($pkNumOrganismo, $pkNumDependencia)
    {
        if($pkNumOrganismo!=''){
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if($pkNumDependencia!=''){
            $filtro = " and c.pk_num_dependencia=$pkNumDependencia";
        }
        $empleado = $this->_db->query(
                "select a.pk_num_empleado, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.fec_nacimiento, b.fec_ingreso, date_format(b.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, c.ind_dependencia, date_format(b.fec_ingreso, '%m') as mes_ingreso from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, a004_dependencia as c where a.pk_num_empleado=b.pk_num_empleado and b.fk_a004_num_dependencia=c.pk_num_dependencia $filtro"
            );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

    // Método que permite listar los tipos de nóminas
    public function metListarNomina($pkNumTipoNomina)
    {
        if($pkNumTipoNomina!=''){
            $nomina =  $this->_db->query(
                "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina where pk_num_tipo_nomina=$pkNumTipoNomina"
            );
            $nomina->setFetchMode(PDO::FETCH_ASSOC);
            return $nomina->fetch();
        } else {
            $nomina =  $this->_db->query(
                "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
            );
            $nomina->setFetchMode(PDO::FETCH_ASSOC);
            return $nomina->fetchAll();
        }
    }

    // Método que permite listar los elementos de la busqueda de beneficios de utiles Escolares
    public function metListarUtiles($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $pkNumTipoNomina, $periodo)
    {
        if($pkNumOrganismo!=''){
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if($pkNumDependencia!=''){
            $filtro .= " and b.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($pkNumEmpleado!=''){
            $filtro .= " and b.pk_num_empleado=$pkNumEmpleado";
        }
        if($pkNumTipoNomina!=''){
            $filtro .= " and b.fk_nmb001_num_tipo_nomina=$pkNumTipoNomina";
        }
        if($periodo!=''){
            $filtro .= " and date_format(a.fec_fecha, '%Y-%m') ='$periodo'";
        }
        $listarUtiles =  $this->_db->query(
            "select b.ind_cedula_documento, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, SUM(a.num_monto) as monto, date_format(a.fec_fecha, '%Y-%m') as periodo from rh_c091_utiles_beneficio as a, vl_rh_persona_empleado_datos_laborales as b where a.fk_rhb001_num_empleado=b.pk_num_empleado and a.ind_estado='AP' $filtro group by b.pk_num_empleado"
        );
        $listarUtiles->setFetchMode(PDO::FETCH_ASSOC);
        return $listarUtiles->fetchAll();
    }

    // Método que permite listar los elementos de la busqueda de beneficios de utiles Escolares
    public function metListarUtilesDetallado($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $pkNumTipoNomina, $periodo)
    {
        if($pkNumOrganismo!=''){
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if($pkNumDependencia!=''){
            $filtro .= " and b.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($pkNumEmpleado!=''){
            $filtro .= " and b.pk_num_empleado=$pkNumEmpleado";
        }
        if($pkNumTipoNomina!=''){
            $filtro .= " and b.fk_nmb001_num_tipo_nomina=$pkNumTipoNomina";
        }
        if($periodo!=''){
            $filtro .= " and date_format(a.fec_fecha, '%Y-%m') ='$periodo'";
        }
        $listarUtiles =  $this->_db->query(
            "select a.pk_num_utiles_beneficio, b.ind_cedula_documento, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, a.num_monto, date_format(a.fec_fecha, '%Y-%m') as periodo, date_format(a.fec_fecha, '%d/%m/%Y') as fecha from rh_c091_utiles_beneficio as a, vl_rh_persona_empleado_datos_laborales as b where a.fk_rhb001_num_empleado=b.pk_num_empleado and a.ind_estado='AP' $filtro"
        );
        $listarUtiles->setFetchMode(PDO::FETCH_ASSOC);
        return $listarUtiles->fetchAll();
    }

    // Método que permite listar los periodos de utiles Escolares
    public function metMostrarListadoUtiles($pkNumOrganismo)
    {
        if($pkNumOrganismo!=''){
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        $listarUtiles =  $this->_db->query(
            "select b.ind_cedula_documento, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, date_format(a.fec_fecha, '%Y-%m') as periodo from rh_c091_utiles_beneficio as a, vl_rh_persona_empleado_datos_laborales as b where a.fk_rhb001_num_empleado=b.pk_num_empleado and a.ind_estado='AP' $filtro group by date_format(a.fec_fecha, '%Y-%m'),  b.ind_cedula_documento desc"
        );
        $listarUtiles->setFetchMode(PDO::FETCH_ASSOC);
        return $listarUtiles->fetchAll();
    }

    // Método que permite listar los periodos de utiles Escolares
    public function metListarBeneficiario($pkNumUtilesBeneficio)
    {
        $listarBeneficiarioUtiles =  $this->_db->query(
            " select a.num_monto_asignado, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, c.ind_cedula_documento from rh_c092_utiles_beneficarios as a, rh_c015_carga_familiar as b, a003_persona as c where a.fk_rhc091_utiles_beneficio=$pkNumUtilesBeneficio and a.fk_c015_num_carga_familiar=b.pk_num_carga and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $listarBeneficiarioUtiles->setFetchMode(PDO::FETCH_ASSOC);
        return $listarBeneficiarioUtiles->fetchAll();
    }
}// fin de la clase

