<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class permisoModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.fk_a001_num_organismo, a.fk_a004_num_dependencia, a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, (c.ind_nombre_detalle) as sexo, date_format(a.fec_ingreso, '%d-%m-%Y') as fecha_ingreso, d.ind_descripcion_cargo, e.ind_dependencia from vl_rh_persona_empleado_datos_laborales as a, vl_rh_persona_empleado as b, a006_miscelaneo_detalle as c, rh_c063_puestos as d, a004_dependencia as e where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b.fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle and a.fk_rhc063_num_puestos_cargo=d.pk_num_puestos and a.fk_a004_num_dependencia=e.pk_num_dependencia"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }

    // Método que permite listar los empleados de acuerdo a la dependencia
    public function metListarEmpleado($pkNumDependencia)
    {
        $listarEmpleado =  $this->_db->query(
            "select pk_num_empleado, ind_cedula_documento, ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2 from vl_rh_persona_empleado_datos_laborales where fk_a004_num_dependencia=$pkNumDependencia"
        );
        $listarEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEmpleado->fetchAll();
    }

    // Método que permite listar lo empleados de acuerdo a la busqueda
    public function metListarPermiso($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $numPermiso, $fechaInicio, $fechaFin, $tipoAusencia)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and c.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and c.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if ($pkNumEmpleado!='') {
            $filtro .= " and c.pk_num_empleado=$pkNumEmpleado";
        }
        if ($numPermiso!='') {
            $filtro .= " and a.pk_num_permiso=$numPermiso";
        }
        if (($fechaInicio!='')&&($fechaFin!='')) {
            $filtro .= " and a.fec_ingreso between '$fechaInicio' and '$fechaFin'";
        }
        if ($tipoAusencia!='') {
            $filtro .= " and a.fk_a006_num_miscelaneo_tipo_ausencia=$tipoAusencia";
        }

        $empleado = $this->_db->query(
            "select a.pk_num_permiso, date_format(a.fec_salida, '%d/%m/%Y') as fecha_salida, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, date_format(a.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, a.fec_hora_salida, a.fec_hora_entrada, a.num_total_dia, a.num_total_hora, a.num_total_minuto, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_motivo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='MOTAUS') as motivo_ausencia, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_tipo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='TIPAUS') as tipo_ausencia, a.ind_motivo, b.ind_estado, c.pk_num_empleado, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from rh_c036_permiso as a, rh_c037_operacion_permiso as b, vl_rh_persona_empleado_datos_laborales as c where a.pk_num_permiso=b.fk_rhc036_num_permiso and a.fk_rhb001_num_empleado=c.pk_num_empleado and b.num_estatus=1  $filtro group by a.pk_num_permiso, b.ind_estado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

    // Método que permite ver un determinado permiso
    public function metVerPermiso($pkNumPermiso, $metodo)
    {
        $verPermiso =  $this->_db->query(
            "select a.pk_num_permiso, a.fk_rhb007_num_horario, a.fk_rhb001_num_empleado_aprueba, a.ind_observacion_aprobacion, a.ind_observacion_aprobacion, a.ind_observacion_verificacion, a.fk_a006_num_miscelaneo_motivo_ausencia, a.fk_a006_num_miscelaneo_tipo_ausencia, a.ind_periodo_contable, date_format(a.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, date_format(a.fec_salida, '%d/%m/%Y') as fecha_salida, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, a.ind_motivo, a.num_total_dia, a.num_total_hora, a.num_total_minuto, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_motivo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='MOTAUS') as motivo_ausencia, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_tipo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='TIPAUS') as tipo_ausencia, a.fec_hora_salida, a.fec_hora_entrada, a.num_remuneracion, a.num_justificativo, a.num_exento, c.pk_num_empleado, b.ind_estado, d.ind_cedula_documento, d.ind_nombre1, d.ind_nombre2, d.ind_apellido1, d.ind_apellido2, e.ind_descripcion_cargo, f.ind_dependencia from rh_c036_permiso as a, rh_c037_operacion_permiso as b, rh_b001_empleado as c, a003_persona as d, rh_c063_puestos as e, a004_dependencia as f where a.pk_num_permiso='$pkNumPermiso' and a.pk_num_permiso=b.fk_rhc036_num_permiso and b.num_estatus=1 and a.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.fk_rhc063_num_puesto=e.pk_num_puestos and a.fk_a004_num_dependencia=f.pk_num_dependencia"
        );
        $verPermiso->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verPermiso->fetch();
        } else {
            return $verPermiso->fetchAll();
        }
    }

    // Método que permite ver el último acceso de un determinado permiso
    public function metAcceso($pkNumPermiso)
    {
        $acceso = $this->_db->query(
            "select b.ind_usuario, date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%m:%S') as fecha_modificacion, c.pk_num_empleado from rh_c036_permiso as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.pk_num_permiso='$pkNumPermiso' and a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona
            "
        );
        $acceso->setFetchMode(PDO::FETCH_ASSOC);
        return $acceso->fetchAll();
    }

    // Método que muestra el listado de responsables de las dependencias
    public function metFuncionarioAprueba($pkNumPermiso)
    {
        $funcionarioAprueba = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from rh_c036_permiso as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_permiso='$pkNumPermiso' and a.fk_rhb001_num_empleado_aprueba=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $funcionarioAprueba->setFetchMode(PDO::FETCH_ASSOC);
        return $funcionarioAprueba->fetchAll();
    }
}// fin de la clase
