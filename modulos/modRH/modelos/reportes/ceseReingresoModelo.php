<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class ceseReingresoModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.fk_a001_num_organismo, a.fk_a004_num_dependencia, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.pk_num_empleado, c.ind_nombre_detalle from vl_rh_persona_empleado_datos_laborales as a, vl_rh_persona_empleado as b, a006_miscelaneo_detalle as c where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b. 	fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }

    // Método que permite listar lo empleados de acuerdo a la busqueda
    public function metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $centroCosto, $pkNumEmpleado, $tipo, $fechaInicio, $fechaFin, $valor)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and b.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if ($centroCosto!='') {
            $filtro .= " and b.fk_a023_num_centro_costo=$centroCosto";
        }
        if ($pkNumEmpleado!='') {
            $filtro .= " and c.pk_num_empleado_cese_reingreso=$pkNumEmpleado";
        }
        if ($tipo!='') {
            $filtro .= " and c.fk_a006_num_miscelaneo_detalle_tipocr=$tipo";
        }
        if (($fechaInicio!='')&&($fechaFin!='')&&($valor==1)) {
            $filtro .= " and c.fec_fecha_egreso between '$fechaInicio' and '$fechaFin'";
        }
        if (($fechaInicio!='')&&($fechaFin!='')&&($valor==2)) {
            $filtro .= " and c.fec_fecha_ingreso between '$fechaInicio' and '$fechaFin'";
        }
        $empleado = $this->_db->query(
                "select a.pk_num_empleado, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, c.pk_num_empleado_cese_reingreso, e.ind_nombre_detalle, date_format(c.fec_fecha_egreso, '%d/%m/%Y') as fecha_egreso, date_format(c.fec_fecha_ingreso, '%d/%m/%Y') as fecha_ingreso from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, rh_c077_empleado_cese_reingreso as c, rh_c049_operaciones_cese_reingreso as d, a006_miscelaneo_detalle as e where a.pk_num_empleado=b.pk_num_empleado and b.pk_num_empleado=c.fk_rhb001_num_empleado and d.num_flag_estatus=1 and c.pk_num_empleado_cese_reingreso=d.fk_rhc077_num_empleado_cese_reingreso and c.fk_a006_num_miscelaneo_detalle_tipocr=e.pk_num_miscelaneo_detalle $filtro"
            );

        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

    // Método que permite listar los centros de costos
    public function metListarCentroCosto($pkNumOrganismo, $pkNumDependencia)
    {
        if($pkNumDependencia!=0){
            $centroCosto =  $this->_db->query(
                "select pk_num_centro_costo, ind_descripcion_centro_costo from a023_centro_costo where fk_a004_num_dependencia=$pkNumDependencia"
            );
        } else {
            $centroCosto =  $this->_db->query(
                "select a.pk_num_centro_costo, a.ind_descripcion_centro_costo from a023_centro_costo as a, a004_dependencia as b, a001_organismo as c where a.fk_a004_num_dependencia=b.pk_num_dependencia and b.fk_a001_num_organismo=c.pk_num_organismo and c.pk_num_organismo=$pkNumOrganismo"
            );
        }
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }

    // Método que permite listar los centros de costos
    public function metListarCeseReingreso($valor)
    {
        $listarCeseReingreso =  $this->_db->query(
            "select b.ind_nombre_detalle from a005_miscelaneo_maestro as a, a006_miscelaneo_detalle as b where a.pk_num_miscelaneo_maestro=	fk_a005_num_miscelaneo_maestro and a.cod_maestro='TIPOCR' and b.pk_num_miscelaneo_detalle=$valor"
        );
        $listarCeseReingreso->setFetchMode(PDO::FETCH_ASSOC);
        return $listarCeseReingreso->fetch();
    }

    // Método que permite listar los empleados registrados, activos e inactivos
    public function metBuscarEmpleados($pkNumOrganismo)
    {
        $buscarEmpleado =  $this->_db->query(
            "select a.pk_num_empleado, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2 from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b where b.fk_a001_num_organismo=$pkNumOrganismo group by a.pk_num_empleado"
        );
        $buscarEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarEmpleado->fetchAll();
    }
}// fin de la clase
