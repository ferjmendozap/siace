<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class controlAsistenciaModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.fk_a001_num_organismo, a.fk_a004_num_dependencia, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.pk_num_empleado, c.ind_nombre_detalle from vl_rh_persona_empleado_datos_laborales as a, vl_rh_persona_empleado as b, a006_miscelaneo_detalle as c where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b. 	fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );

            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        }

    }

    // Método que permite listar lo empleados de acuerdo a la busqueda
    public function metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $cedula, $ordenar)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and b.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if ($pkNumEmpleado!='') {
            $filtro .= " and a.pk_num_empleado=$pkNumEmpleado";
        }
        if ($cedula!='') {
            $filtro .= " and a.ind_cedula_documento=$cedula";
        }
        $orden= "";
        if ($ordenar!='') {
            switch($ordenar){
                case 0:
                    // Por persona
                    $orden = " ";
                    break;
                case 1:
                    // Por persona
                    $orden = " order by b.pk_num_empleado";
                    break;
                case 2:
                    // Por nombre
                    $orden = " order by b.ind_nombre1 desc";
                    break;
                case 3:
                    // Por número de documento
                    $orden = " order by b.ind_cedula_documento";
                    break;
                case 4:
                    // Por fecha de Ingreso
                    $orden = " order by b.fec_ingreso";
                    break;
                case 5:
                    // Por cargo
                    $orden = " order by b.fk_rhc063_num_puestos_cargo";
                    break;
            }
        }
        $empleado = $this->_db->query(
                "select a.pk_num_empleado, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.fec_nacimiento, b.fec_ingreso, date_format(b.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, c.ind_dependencia, d.ind_descripcion_cargo, date_format(b.fec_ingreso, '%m') as mes_ingreso from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, a004_dependencia as c, rh_c063_puestos as d where a.pk_num_empleado=b.pk_num_empleado and b.fk_a004_num_dependencia=c.pk_num_dependencia and b.fk_rhc063_num_puestos_cargo=d.pk_num_puestos $filtro $orden"
            );

        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

    // Método que permite listar los tipos de nóminas
    public function metListarNomina()
    {
        $nomina =  $this->_db->query(
            "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
        );
        $nomina->setFetchMode(PDO::FETCH_ASSOC);
        return $nomina->fetchAll();
    }

    // Método que permite listar los cargos
    public function metListarCargos()
    {
        $cargo =  $this->_db->query(
            "select pk_num_puestos, ind_descripcion_cargo from rh_c063_puestos"
        );
        $cargo->setFetchMode(PDO::FETCH_ASSOC);
        return $cargo->fetchAll();
    }

    // Método que permite listar los permisos aprobados
    public function metListarPermiso($pkNumOrganismo, $pkNumDependencia, $tipoNomina, $indCedulaDocumento,$cargo, $edoReg, $sitTrab, $pkNumEmpleado, $fechaInicio, $fechaFin, $tipoAusencia)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and c.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and c.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if ($pkNumEmpleado!='') {
            $filtro .= " and c.pk_num_empleado=$pkNumEmpleado";
        }
        if (($fechaInicio!='')&&($fechaFin!='')) {
            $filtro .= " and a.fec_ingreso between '$fechaInicio' and '$fechaFin'";
        }
        if ($tipoAusencia!='') {
            $filtro .= " and a.fk_a006_num_miscelaneo_tipo_ausencia=$tipoAusencia";
        }
        if ($tipoNomina!='') {
            $filtro .= " and c.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if ($indCedulaDocumento!='') {
            $filtro .= " and c.ind_cedula_documento=$indCedulaDocumento";
        }
        if ($cargo!='') {
            $filtro .= " and c.fk_rhc063_num_puestos_cargo=$cargo";
        }
        if ($edoReg!='') {
            $filtro .= " and c.ind_estatus_persona=$edoReg";
        }
        if ($sitTrab!='') {
            $filtro .= " and c.ind_estatus=$sitTrab";
        }


        $empleado = $this->_db->query(
                "select a.pk_num_permiso, date_format(a.fec_salida, '%d/%m/%Y') as fecha_salida, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, date_format(a.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, a.fec_hora_salida, a.fec_hora_entrada, a.num_total_dia, a.num_total_hora, a.num_total_minuto, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_motivo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='MOTAUS') as motivo_ausencia, (select ind_nombre_detalle from a006_miscelaneo_detalle as e, a005_miscelaneo_maestro as f where e.pk_num_miscelaneo_detalle=a.fk_a006_num_miscelaneo_tipo_ausencia and e.fk_a005_num_miscelaneo_maestro=f.pk_num_miscelaneo_maestro and f.cod_maestro='TIPAUS') as tipo_ausencia, a.ind_motivo, b.ind_estado, c.pk_num_empleado, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from rh_c036_permiso as a, rh_c037_operacion_permiso as b, vl_rh_persona_empleado_datos_laborales as c, vl_rh_persona_empleado as d where a.pk_num_permiso=b.fk_rhc036_num_permiso and a.fk_rhb001_num_empleado=c.pk_num_empleado and b.num_estatus=1 and (b.ind_estado='AP' or b.ind_estado='VE') and c.pk_num_empleado=d.pk_num_empleado $filtro group by a.pk_num_permiso");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);


        return  $empleado->fetchAll();
    }


    // Método que permite listar las asistencias de los empleados
    public function metListarAsistencias($pkNumOrganismo, $pkNumDependencia, $tipoNomina, $indCedulaDocumento,$cargo, $edoReg, $sitTrab, $pkNumEmpleado, $fechaInicio, $fechaFin)
    {

        if ($pkNumOrganismo!='') {
            $filtro = " and org.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and org.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if ($pkNumEmpleado!='') {
            $filtro .= " and me.pk_num_empleado=$pkNumEmpleado";
        }
        if (($fechaInicio!='')&&($fechaFin!='')) {
            $filtro .= " and a.fec_fecha_format between '$fechaInicio' and '$fechaFin'";
        }
        if ($tipoNomina!='') {
            $filtro .= " and org.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if ($indCedulaDocumento!='') {
            $filtro .= " and a.ind_cedula_empleado=$indCedulaDocumento";
        }
        if ($cargo!='') {
            $filtro .= " and lab.pk_num_puestos=$cargo";
        }
        if ($edoReg!='') {
            $filtro .= " and mp.num_estatus=$edoReg";
        }
        if ($sitTrab!='') {
            $filtro .= " and me.num_estatus=$sitTrab";
        }


        $asistencias = $this->_db->query(
            "SELECT
                a.pk_num_controlasistencia,
                me.pk_num_empleado,
                mp.pk_num_persona,
                a.ind_cedula_empleado,
                mp.ind_nombre1,
                mp.ind_nombre2,
                mp.ind_apellido1,
                mp.ind_apellido2,
                a.fec_fecha,
                a.fec_hora,
                a.fec_fecha_format,
                a.fec_hora_format,
                a.ind_evento,
                a.ind_estado,
                dep.ind_dependencia,
                p.pk_num_puestos,
                p.ind_descripcion_cargo,
                org.fk_a001_num_organismo,
                org.fk_a004_num_dependencia,
                org.fk_nmb001_num_tipo_nomina,
                me.num_estatus AS situacion_trabajo,
                mp.num_estatus AS estado_registro
                FROM
                rh_c105_controlasistencia AS a
                INNER JOIN rh_b001_empleado AS me ON a.fk_rhb001_num_empleado = me.pk_num_empleado
                INNER JOIN a003_persona AS mp ON mp.pk_num_persona = me.fk_a003_num_persona
                INNER JOIN rh_c076_empleado_organizacion AS org ON me.pk_num_empleado = org.fk_rhb001_num_empleado
                INNER JOIN a004_dependencia AS dep ON dep.pk_num_dependencia = org.fk_a004_num_dependencia
                INNER JOIN rh_c005_empleado_laboral AS lab ON a.fk_rhb001_num_empleado = lab.fk_rhb001_num_empleado
                INNER JOIN rh_c063_puestos AS p ON p.pk_num_puestos = lab.fk_rhc063_num_puestos_cargo
                $filtro and a.ind_estado='P'
            "
        );
        $asistencias->setFetchMode(PDO::FETCH_ASSOC);

        return  $asistencias->fetchAll();

    }

}// fin de la clase

