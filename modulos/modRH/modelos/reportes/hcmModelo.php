<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de HCM. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de vacaciones por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class hcmModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }


    // Método que permite obtener los datos del empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select a.fk_a001_num_organismo, a.fk_a004_num_dependencia, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.pk_num_empleado, c.ind_nombre_detalle from vl_rh_persona_empleado_datos_laborales as a, vl_rh_persona_empleado as b, a006_miscelaneo_detalle as c where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b.fk_a006_num_miscelaneo_detalle_sexo=c.pk_num_miscelaneo_detalle"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }
    }

    // Método que permite listar lo empleados de acuerdo a la busqueda
    public function metListarEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumAyudaGlobal)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and c.pk_num_dependencia=$pkNumDependencia";
        }
        if ($pkNumAyudaGlobal!='') {
            $filtro .= " and d.fk_rhb008_ayuda_global=$pkNumAyudaGlobal";
        }
        $empleado = $this->_db->query(
                "select a.pk_num_empleado, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.ind_cedula_documento, a.fec_nacimiento, b.fec_ingreso, date_format(b.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, c.ind_dependencia, date_format(b.fec_ingreso, '%m') as mes_ingreso from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, a004_dependencia as c, rh_c046_beneficio_medico as d where a.pk_num_empleado=b.pk_num_empleado and b.fk_a004_num_dependencia=c.pk_num_dependencia and b.pk_num_empleado=d.fk_rhb001_empleado $filtro group by b.pk_num_empleado,  b.fec_ingreso, c.ind_dependencia"
            );

        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return  $empleado->fetchAll();
    }

    // Método que permite listar la ayuda Global
    public function metObtenerAyudaMedica($metodo)
    {
        $ayudaMedica = $this->_db->query(
            "select pk_num_ayuda_global, ind_descripcion, num_limite from rh_b008_ayuda_global order by pk_num_ayuda_global desc"
        );
        $ayudaMedica->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $ayudaMedica->fetchAll();
        } else {
            return $ayudaMedica->fetch();
        }
    }

    // Método que permite obtener la ayuda Global
    public function metObtenerAyudaGlobal($pkNumAyudaGlobal)
    {
        $ayudaMedica = $this->_db->query(
            "select pk_num_ayuda_global, ind_descripcion, num_limite from rh_b008_ayuda_global where pk_num_ayuda_global=$pkNumAyudaGlobal"
        );
        $ayudaMedica->setFetchMode(PDO::FETCH_ASSOC);
        return  $ayudaMedica->fetch();
    }

    // Método que permite listar la ayuda Global y especifica
    public function metObtenerAyudaEspecifica($pkNumAyudaGlobal)
    {
        $ayudaMedicaEspecifica = $this->_db->query(
            "select pk_num_ayuda_especifica, ind_descripcion_especifica, num_limite_esp from rh_c068_ayuda_especifica where fk_rhb008_num_ayuda_global=$pkNumAyudaGlobal"
        );
        $ayudaMedicaEspecifica->setFetchMode(PDO::FETCH_ASSOC);
        return  $ayudaMedicaEspecifica->fetchAll();
    }

    // Método que permite listar la ayuda especifica de un funcionario
    public function metObtenerAyudaFuncionario($pkNumAyudaGlobal, $pkNumAyudaEspecifica, $pkNumEmpleado)
    {
        $ayudaMedicaEspecifica = $this->_db->query(
            "select pk_beneficio_medico, date_format(fec_creacion, '%d/%m/%Y') as fecha_solicitud, num_monto from rh_c046_beneficio_medico where fk_rhb008_ayuda_global=$pkNumAyudaGlobal and fk_rhc068_ayuda_especifica=$pkNumAyudaEspecifica and fk_rhb001_empleado=$pkNumEmpleado"
        );
        $ayudaMedicaEspecifica->setFetchMode(PDO::FETCH_ASSOC);
        return  $ayudaMedicaEspecifica->fetchAll();
    }

    // Método que permite listar las instituciones de las cuales el empleado acude
    public function metListarInstitucion($pkNumOrganismo, $pkNumDependencia, $pkNumAyudaGlobal, $pkNumInstitucion)
    {
        if($pkNumOrganismo!=''){
            $filtro = " and a.fk_a001_organismo=$pkNumOrganismo";
        }
        if($pkNumDependencia!=''){
            $filtro .= " and c.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($pkNumAyudaGlobal!=''){
            $filtro .= " and a.fk_rhb008_ayuda_global=$pkNumAyudaGlobal";
        }
        if($pkNumInstitucion!=''){
            $filtro .= " and a.fk_rhc040_institucion=$pkNumInstitucion";
        }

        $listarInstituciones = $this->_db->query(
            "select b.ind_nombre_institucion, b.pk_num_institucion, SUM(a.num_monto) as total, count(a.fk_rhc040_institucion) as casos from rh_c046_beneficio_medico as a, rh_c040_institucion as b, vl_rh_persona_empleado_datos_laborales as c where a.fk_rhc040_institucion=b.pk_num_institucion and a.fk_rhb001_empleado=c.pk_num_empleado $filtro group by a.fk_rhc040_institucion"
        );
        $listarInstituciones->setFetchMode(PDO::FETCH_ASSOC);
        return  $listarInstituciones->fetchAll();
    }

    public function metObtenerPartidas($pkNumOrganismo, $pkNumDependencia, $pkNumAyudaGlobal, $pkNumAyudaEspecifica)
    {
        if($pkNumOrganismo!=''){
            $filtro = " and a.fk_a001_organismo=$pkNumOrganismo";
        }
        if($pkNumDependencia!=''){
            $filtro .= " and c.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($pkNumAyudaGlobal!=''){
            $filtro .= " and a.fk_rhb008_ayuda_global=$pkNumAyudaGlobal";
        }
        if($pkNumAyudaEspecifica!=''){
            $filtro .= " and a.fk_rhc068_ayuda_especifica=$pkNumAyudaEspecifica";
        }
        $listarInstituciones = $this->_db->query(
            "select b.ind_descripcion_especifica, b.pk_num_ayuda_especifica, SUM(a.num_monto) as total, count(a.fk_rhc040_institucion) as casos from rh_c046_beneficio_medico as a, rh_c068_ayuda_especifica as b, vl_rh_persona_empleado_datos_laborales as c where a.fk_rhc068_ayuda_especifica=b.pk_num_ayuda_especifica and a.fk_rhb001_empleado=c.pk_num_empleado $filtro group by a.fk_rhc068_ayuda_especifica"
        );
        $listarInstituciones->setFetchMode(PDO::FETCH_ASSOC);
        return  $listarInstituciones->fetchAll();
    }

}// fin de la clase

