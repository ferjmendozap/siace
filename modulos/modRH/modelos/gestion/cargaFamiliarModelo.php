<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class cargaFamiliarModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR TODA LA CARGA FAMILIAR DEL EMPLEADO
    public function metListarCargaFamiliar($empleado)
    {
        $con = $this->_db->query("
                SELECT
                vl_rh_empleado_carga_familiar.pk_num_carga,
                vl_rh_empleado_carga_familiar.pk_num_empleado,
                vl_rh_empleado_carga_familiar.pk_num_persona,
                vl_rh_empleado_carga_familiar.fk_a003_num_persona,
                vl_rh_empleado_carga_familiar.ind_cedula_documento,
                vl_rh_empleado_carga_familiar.nombre,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_sexo,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_nacionalidad,
                vl_rh_empleado_carga_familiar.fec_nacimiento,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_edocivil,
                vl_rh_empleado_carga_familiar.ind_estatus,
                vl_rh_empleado_carga_familiar.ind_estatus_persona,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco,
                c1.ind_nombre_detalle AS sexo,
                c2.ind_nombre_detalle AS nacionalidad,
                c3.ind_nombre_detalle AS parentesco
                FROM
                vl_rh_empleado_carga_familiar
                INNER JOIN a006_miscelaneo_detalle AS c1 ON c1.pk_num_miscelaneo_detalle = vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_sexo
                LEFT JOIN a006_miscelaneo_detalle AS c2 ON c2.pk_num_miscelaneo_detalle = vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_nacionalidad
                INNER JOIN a006_miscelaneo_detalle AS c3 ON c3.pk_num_miscelaneo_detalle = vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco
                WHERE pk_num_empleado='$empleado'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE MOSTRAR LA CARGA FAMILIAR EN EL FORMULARIO
    public function metMostrarCargaFamiliar($idCargaFamiliar)
    {

        $con = $this->_db->query("
                SELECT
                vl_rh_empleado_carga_familiar.pk_num_carga,
                vl_rh_empleado_carga_familiar.pk_num_empleado,
                vl_rh_empleado_carga_familiar.pk_num_persona,
                vl_rh_empleado_carga_familiar.fk_a003_num_persona,
                vl_rh_empleado_carga_familiar.ind_cedula_documento,
                vl_rh_empleado_carga_familiar.nombre,
                vl_rh_empleado_carga_familiar.ind_nombre1,
                vl_rh_empleado_carga_familiar.ind_nombre2,
                vl_rh_empleado_carga_familiar.ind_apellido1,
                vl_rh_empleado_carga_familiar.ind_apellido2,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_sexo,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_nacionalidad,
                vl_rh_empleado_carga_familiar.fec_nacimiento,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_edocivil,
                vl_rh_empleado_carga_familiar.ind_estatus,
                vl_rh_empleado_carga_familiar.ind_estatus_persona,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco,
                c1.ind_nombre_detalle AS sexo,
                c2.ind_nombre_detalle AS nacionalidad,
                c3.ind_nombre_detalle AS parentesco,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_gradoinst,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_tipoedu,
                vl_rh_empleado_carga_familiar.fk_rhc040_num_institucion,
                vl_rh_empleado_carga_familiar.ind_empresa,
                vl_rh_empleado_carga_familiar.txt_direccion_empresa,
                vl_rh_empleado_carga_familiar.fk_rhc064_nivel_grado_instruccion,
                vl_rh_empleado_carga_familiar.num_sueldo_mensual,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_det_gruposangre,
                vl_rh_empleado_carga_familiar.ind_foto,
                vl_rh_empleado_carga_familiar.num_flag_discapacidad,
                vl_rh_empleado_carga_familiar.num_flag_trabaja,
                vl_rh_empleado_carga_familiar.num_flag_estudia
                FROM
                vl_rh_empleado_carga_familiar
                INNER JOIN a006_miscelaneo_detalle AS c1 ON c1.pk_num_miscelaneo_detalle = vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_sexo
                LEFT JOIN a006_miscelaneo_detalle AS c2 ON c2.pk_num_miscelaneo_detalle = vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_nacionalidad
                INNER JOIN a006_miscelaneo_detalle AS c3 ON c3.pk_num_miscelaneo_detalle = vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco
                WHERE pk_num_carga='$idCargaFamiliar'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    public function metMostrarParentesco($idCargaFamiliar)
    {
        $con = $this->_db->query("
                SELECT
                a006_miscelaneo_detalle.ind_nombre_detalle AS parentesco
                FROM
                rh_c015_carga_familiar
                INNER JOIN a006_miscelaneo_detalle ON rh_c015_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                WHERE pk_num_carga='$idCargaFamiliar'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    #PERMITE MOSTRAR LA DIRECCION DE LA CARGA FAMILIAR EN EL FORMULARIO
    public function metMostrarDireccionCargaFamiliar($idCargaFamiliar)
    {
        $con = $this->_db->query("
                SELECT
                *
                FROM
                vl_rh_empleado_carga_familiar_direccion
                WHERE pk_num_carga='$idCargaFamiliar'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #PERMITE MOSTRAR LOS TELEFONOS DE LA CARGA FAMILIAR EN EL FORMULARIO
    public function metMostrarTelefonoCargaFamiliar($idCargaFamiliar)
    {
        $con = $this->_db->query("
                SELECT
                *
                FROM
                vl_rh_empleado_carga_familiar_telefono
                WHERE pk_num_carga='$idCargaFamiliar'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }
    
    #PERMITE VERFICAR LA CEDULA DE LA PERSONA 
    public function metVerificarCedulaPersona($cedula,$empleado)
    {
        
        #verifico si ya existe en la carga familiar del empleado
        $con1 = $this->_db->prepare("
            SELECT
                pe.pk_num_persona
            FROM
                a003_persona AS pe
            INNER JOIN rh_c015_carga_familiar AS cf ON pe.pk_num_persona = cf.fk_a003_num_persona
            INNER JOIN rh_b001_empleado AS em ON em.pk_num_empleado = cf.fk_rhb001_num_empleado
            WHERE
                    cf.fk_rhb001_num_empleado = $empleado
                    AND pe.ind_cedula_documento = '$cedula'
        ");
        $con1->execute();
        $filas1 = $con1->rowCount();        
        if($filas1==1){//existe en la carga familair del empleado
            return array('P'=>1,'data'=>'','mensaje'=>'La Cedula introducida ya esta Registrada en la Carga Familiar del Empleado');
        }else{            
            #verifico si existe en la carga familiar de otro empleado
            $con2 = $this->_db->prepare("
            SELECT
                pe.pk_num_persona
            FROM
                a003_persona AS pe
            INNER JOIN rh_c015_carga_familiar AS cf ON pe.pk_num_persona = cf.fk_a003_num_persona
            INNER JOIN rh_b001_empleado AS em ON em.pk_num_empleado = cf.fk_rhb001_num_empleado
            WHERE  pe.ind_cedula_documento = '$cedula'
            ");
            $con2->execute();
            $filas2 = $con2->rowCount();
            if($filas2==1){//existe en la carga familiar de otro empleado
                return array('P'=>2,'data'=>'','mensaje'=>'La Cedula introducida ya esta Registrada en la Carga Familiar de Otro Empleado');
            }else{
                #verifico si existe en la tabla personas
                $con3 = $this->_db->prepare("
                SELECT
                    pe.pk_num_persona,
                    pe.ind_cedula_documento,
                    pe.ind_nombre1,
                    pe.ind_nombre2,
                    pe.ind_apellido1,
                    pe.ind_apellido2,
                    pe.fk_a006_num_miscelaneo_detalle_sexo,
                    pe.fec_nacimiento,
                    pe.fk_a006_num_miscelaneo_detalle_edocivil,
                    pe.fk_a006_num_miscelaneo_det_gruposangre
                FROM
                    a003_persona AS pe
                WHERE pe.ind_cedula_documento = '$cedula'
                ");
                $con3->execute();
                $filas3 = $con3->rowCount();
                if($filas3==1){//existe en la tabla personas
                    //muestro los datos en el formulario
                    $datos = $con3->fetch();
                    return array('P'=>3,'data'=>$datos,'mensaje'=>'La Persona existe debe terminar el Registro.');
                }else{
                    //significa que no existe la persona y se debe registrar 
                    return array('P'=>4,'data'=>'','mensaje'=>'La Persona no existe debe Registrar a la Persona');
                }
                
            }
            
        }
        
    }
            

    #PERMITE INSERTAR CARGA FAMILIAR DEL EMPLEADO
    public function metRegistrarCargaFamiliar($empleado,$datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ORGANIZO LOS DATOS OBTENIDOS DEL ARRAY DE DATOS
        $CEDULA = $datos[0];
        $SEXO   = $datos[1];
        $PARENTESCO = $datos[2];
        $APE_1 = mb_strtoupper($datos[3],'utf-8');
        $APE_2 = mb_strtoupper($datos[4],'utf-8');
        $NOM_1 = mb_strtoupper($datos[5],'utf-8');
        $NOM_2 = mb_strtoupper($datos[6],'utf-8');
        $FECHA_NAC = explode("-",$datos[7]);
        $TIPO_DIR  = $datos[8];
        $DIRECCION = $datos[9];
        //grupo sanguineo
        $SANGRE   = $datos[10];
        if($SANGRE==false){
            $SANGRE=NULL;
        }else{
            $SANGRE=$SANGRE;
        }
        $EDOCIVIL = $datos[11];
        $NUM_TELF  = $datos[12];
        $TIPO_TELF = $datos[13];
        $TELF      = $datos[14];
        #grado de instruccion
        $GRADO_INST = $datos[15];
        if($GRADO_INST==false){
            $GRADO_INST=NULL;
        }else{
            $GRADO_INST=$GRADO_INST;
        }
        $TIPO_EDU   = $datos[16];
        #tipo de educacion
        if($TIPO_EDU==false){
            $TIPO_EDU=NULL;
        }else{
            $TIPO_EDU=$TIPO_EDU;
        }
        $CENTRO     = $datos[17];
        #tipo de educacion
        if($CENTRO==false){
            $CENTRO=NULL;
        }else{
            $CENTRO=$CENTRO;
        }
        $EMPRESA    = $datos[18];
        $DIR_EMPRE  = $datos[19];
        $SUELDO     = $datos[20];
        $FOTO       = $datos[21];
        $SIT_DOM    = $datos[22];
        $FLAG_DISCAPACIDAD = $datos[23];
        $FLAG_ESTUDIA      = $datos[24];
        $FLAG_TRABAJA      = $datos[25];
        $NIVEL_INST  = $datos[26];
        #nivel grado instruccion
        if($NIVEL_INST==false){
            $NIVEL_INST=NULL;
        }else{
            $NIVEL_INST=$NIVEL_INST;
        }
        $ID_TIPO_ZONA = $datos[27];
        $ID_UBI_INMUEBLE = $datos[28];
        $IND_UBI_INMUEBLE = $datos[29];
        $ID_TIPO_INMUEBLE = $datos[30];
        $IND_TIPO_INMUEBLE = $datos[31];
        $ID_ZONA_RESIDENCIAL = $datos[32];
        $IND_ZONA_RESIDENCIAL = $datos[33];
        $IND_INMUEBLE = $datos[34];
        $PUNTO_REF = $datos[35];
        $ZONA_POSTAL = $datos[36];
        $EXISTE = $datos[37];//si la persona existe o no
        $idPersona = $datos[38];
        
        
        if($EXISTE==0){//si no existe la persona
            #Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    a003_persona
                 SET
                    ind_cedula_documento=:ind_cedula_documento,
                    ind_documento_fiscal=:ind_documento_fiscal,
                    ind_nombre1=:ind_nombre1,
                    ind_nombre2=:ind_nombre2,
                    ind_apellido1=:ind_apellido1,
                    ind_apellido2=:ind_apellido2,
                    fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
                    fec_nacimiento=:fec_nacimiento,
                    fk_a006_num_miscelaneo_detalle_edocivil=:fk_a006_num_miscelaneo_detalle_edocivil,
                    ind_tipo_persona='N',
                    num_estatus=1,
                    fk_a006_num_miscelaneo_det_gruposangre=:fk_a006_num_miscelaneo_det_gruposangre,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':ind_cedula_documento'  => $CEDULA,
                ':ind_documento_fiscal'  => $CEDULA,
                ':ind_nombre1'  => $NOM_1,
                ':ind_nombre2'  => $NOM_2,
                ':ind_apellido1' => $APE_1,
                ':ind_apellido2' => $APE_2,
                ':fk_a006_num_miscelaneo_detalle_sexo' => $SEXO,
                ':fec_nacimiento' => $FECHA_NAC[2]."-".$FECHA_NAC[1]."-".$FECHA_NAC[0],
                ':fk_a006_num_miscelaneo_detalle_edocivil' => $EDOCIVIL,
                ':fk_a006_num_miscelaneo_det_gruposangre'  => $SANGRE
            ));
            $errorRegPersona = $NuevoRegistro->errorInfo();
            //obtengo id de la persona
            $idRegistroPersona= $this->_db->lastInsertId();
            
            
        }else{//existe la persona
            $idRegistroPersona = $idPersona;
        }
        
        
        //VERIFICO PARA REGISTRAR DIRECCION DE LA PERSONA
        if($TIPO_DIR!=0 && !empty($TIPO_DIR)){

            $RegistroDireccion = $this->_db->prepare(
                "INSERT INTO
                    a036_persona_direccion
                 SET
                    fk_a003_num_persona='$idRegistroPersona',
                    fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                    fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                    fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                    ind_zona_residencial=:ind_zona_residencial,
                    fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                    ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                    fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                    ind_tipo_inmueble=:ind_tipo_inmueble,
                    fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                    ind_inmueble=:ind_inmueble,
                    ind_punto_referencia=:ind_punto_referencia,
                    ind_zona_postal=:ind_zona_postal,
                    ind_direccion=:ind_direccion
                  ");

            $RegistroDireccion->execute(array(
                ':fk_a006_num_miscelaneo_detalle_tipodir'  => $TIPO_DIR,
                ':fk_a006_num_miscelaneo_detalle_domicilio'  => $SIT_DOM,
                ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL,
                ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL,'utf8'),
                ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE,
                ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE,'utf8'),
                ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE,
                ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE,'utf8'),
                ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA,
                ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE,'utf8'),
                ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF,'utf8'),
                ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL,'utf8'),
                ':ind_direccion'  => mb_strtoupper($DIRECCION,'utf8')
            ));

            $errorRegDireccion = $RegistroDireccion->errorInfo();

        }else{
            $errorRegDireccion = '';
        }

        if(!empty($errorRegDireccion[1]) && !empty($errorRegDireccion[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $errorRegDireccion;

        }else{

            //VERIFICO PARA REGISTRAR TELEFONOS DE LA PERSONA
            if(isset($TIPO_TELF) && !empty($TIPO_TELF)){

                $RegistroTelefono = $this->_db->prepare(
                    "INSERT INTO
                        a007_persona_telefono
                     SET
                        ind_telefono=:ind_telefono,
                        fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                        fk_a003_num_persona='$idRegistroPersona'
                      ");

                for ($i = 0; $i < count($TIPO_TELF); $i++) {
                    #execute — Ejecuta una sentencia preparada
                    $RegistroTelefono->execute(array(
                        ':ind_telefono' => $TELF[$i],
                        ':fk_a006_num_miscelaneo_detalle_tipo_telefono' => $TIPO_TELF[$i],
                    ));
                }

                $errorRegTelefono = $RegistroTelefono->errorInfo();

            }else{

                $errorRegTelefono='';
            }

            if(!empty($errorRegTelefono[1]) && !empty($errorRegTelefono[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $errorRegTelefono;
            }else{

                //REGISTRO LOS DATOS DE LA CARGA FAMILIAR
                $RegistroDatos = $this->_db->prepare(
                    "INSERT INTO
                        rh_c015_carga_familiar
                     SET
                        fk_a006_num_miscelaneo_detalle_parentezco=:fk_a006_num_miscelaneo_detalle_parentezco,
                        fk_rhb001_num_empleado='$empleado',
                        fk_a003_num_persona='$idRegistroPersona',
                        fk_a006_num_miscelaneo_detalle_gradoinst=:fk_a006_num_miscelaneo_detalle_gradoinst,
                        fk_a006_num_miscelaneo_detalle_tipoedu=:fk_a006_num_miscelaneo_detalle_tipoedu,
                        fk_rhc040_num_institucion=:fk_rhc040_num_institucion,
                        ind_empresa=:ind_empresa,
                        txt_direccion_empresa=:txt_direccion_empresa,
                        num_sueldo_mensual=:num_sueldo_mensual,
                        num_flag_discapacidad=:num_flag_discapacidad,
                        num_flag_estudia=:num_flag_estudia,
                        num_flag_trabaja=:num_flag_trabaja,
                        fk_rhc064_nivel_grado_instruccion=:fk_rhc064_nivel_grado_instruccion,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                      ");
                $RegistroDatos->execute(array(
                    ':fk_a006_num_miscelaneo_detalle_parentezco' => $PARENTESCO,
                    ':fk_a006_num_miscelaneo_detalle_gradoinst' => $GRADO_INST,
                    ':fk_a006_num_miscelaneo_detalle_tipoedu' => $TIPO_EDU,
                    ':fk_rhc040_num_institucion' => $CENTRO,
                    ':ind_empresa' => $EMPRESA,
                    ':txt_direccion_empresa' => $DIR_EMPRE,
                    ':num_sueldo_mensual' => $SUELDO,
                    ':num_flag_discapacidad' => $FLAG_DISCAPACIDAD,
                    ':num_flag_estudia' => $FLAG_ESTUDIA,
                    ':num_flag_trabaja' => $FLAG_TRABAJA,
                    ':fk_rhc064_nivel_grado_instruccion' => $NIVEL_INST
                ));

                $errorRegDatos= $RegistroDatos->errorInfo();

                $idCargaFamiliar= $this->_db->lastInsertId();

                if(!empty($errorRegDatos[1]) && !empty($errorRegDatos[2])){
                    //en caso de error hago rollback
                    $this->_db->rollBack();
                    return $errorRegDatos;
                }else{

                    #verifico si existe una foto temporal, la renombro y la guardo
                    if($FOTO){

                        $ruta_tmp = "publico/imagenes/modRH/tmp/".$FOTO;
                        $ruta     = "publico/imagenes/modRH/fotos/".$FOTO;
                        $partes   = explode(".",$FOTO);

                        if(file_exists($ruta_tmp)){

                            if(!copy($ruta_tmp,$ruta)){
                                echo "Error al copiar la foto";
                            }else{
                                chmod($ruta, 0777);
                                rename($ruta,"publico/imagenes/modRH/fotos/".$idRegistroPersona.".".$partes[1]);
                                unlink($ruta_tmp);

                                $actualizoFoto = $this->_db->prepare("
                                         UPDATE
                                              a003_persona
                                         SET
                                              ind_foto=:foto
                                         WHERE pk_num_persona='$idRegistroPersona'

                                ");
                                $actualizoFoto->execute(array(
                                    ':foto' => $idRegistroPersona.".".$partes[1]
                                ));

                                $errorFoto = $actualizoFoto->errorInfo();

                                if(!empty($errorFoto[1]) && !empty($errorFoto[2])){
                                    $this->_db->rollBack();
                                    return $errorFoto;
                                }else{
                                    $this->_db->commit();
                                    return $idRegistroPersona;
                                }
                            }

                        }

                    }else{
                        //en caso de exito hago commit de la operacion
                        $this->_db->commit();
                        return $idCargaFamiliar;
                    }

                }

            }

        }

    }

    #PERMITE MODIFICAR LA CARGA FAMILIAR DEL EMPLEADO
    public function metModificarCargaFamiliar($idCargaFamiliar,$persona,$empleado,$datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ORGANIZO LOS DATOS OBTENIDOS DEL ARRAY DE DATOS
        $CEDULA = $datos[0];
        $SEXO   = $datos[1];
        $PARENTESCO = $datos[2];
        $APE_1 = $datos[3];
        $APE_2 = $datos[4];
        $NOM_1 = $datos[5];
        $NOM_2 = $datos[6];
        $FECHA_NAC = explode("-",$datos[7]);
        $TIPO_DIR  = $datos[8];
        $DIRECCION = $datos[9];
        $SANGRE   = $datos[10];
        $EDOCIVIL = $datos[11];
        $ID_TELF  = $datos[12];
        $TIPO_TELF = $datos[13];
        $TELF      = $datos[14];
        #grado de instruccion
        $GRADO_INST = $datos[15];
        if($GRADO_INST==false){
            $GRADO_INST=NULL;
        }else{
            $GRADO_INST=$GRADO_INST;
        }
        $TIPO_EDU   = $datos[16];
        #tipo de educacion
        if($TIPO_EDU==false){
            $TIPO_EDU=NULL;
        }else{
            $TIPO_EDU=$TIPO_EDU;
        }
        $CENTRO     = $datos[17];
        #tipo de educacion
        if($CENTRO==false){
            $CENTRO=NULL;
        }else{
            $CENTRO=$CENTRO;
        }
        $EMPRESA    = $datos[18];
        $DIR_EMPRE  = $datos[19];
        //$T_SERVICIO = $datos[20];
        $SUELDO     = $datos[20];
        $ID_DIR     = $datos[21];
        $ID_TELF    = $datos[22];
        $FOTO       = $datos[23];
        $SIT_DOM    = $datos[24];
        $FLAG_DISCAPACIDAD = $datos[25];
        $FLAG_ESTUDIA      = $datos[26];
        $FLAG_TRABAJA      = $datos[27];
        $NIVEL_INST  = $datos[28];
        #nivel grado instruccion
        if($NIVEL_INST==false){
            $NIVEL_INST=NULL;
        }else{
            $NIVEL_INST=$NIVEL_INST;
        }
        $ID_TIPO_ZONA = $datos[29];
        $ID_UBI_INMUEBLE = $datos[30];
        $IND_UBI_INMUEBLE = $datos[31];
        $ID_TIPO_INMUEBLE = $datos[32];
        $IND_TIPO_INMUEBLE = $datos[33];
        $ID_ZONA_RESIDENCIAL = $datos[34];
        $IND_ZONA_RESIDENCIAL = $datos[35];
        $IND_INMUEBLE = $datos[36];
        $PUNTO_REF = $datos[37];
        $ZONA_POSTAL = $datos[38];
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $ActualizarRegistro = $this->_db->prepare(
            "UPDATE
                a003_persona
             SET
                ind_cedula_documento=:ind_cedula_documento,
                ind_nombre1=:ind_nombre1,
                ind_nombre2=:ind_nombre2,
                ind_apellido1=:ind_apellido1,
                ind_apellido2=:ind_apellido2,
                fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
                fec_nacimiento=:fec_nacimiento,
                fk_a006_num_miscelaneo_detalle_edocivil=:fk_a006_num_miscelaneo_detalle_edocivil,
                ind_tipo_persona='N',
                num_estatus=1,
                fk_a006_num_miscelaneo_det_gruposangre=:fk_a006_num_miscelaneo_det_gruposangre
             WHERE pk_num_persona='$persona'
              ");

        #execute — Ejecuta una sentencia preparada
        $ActualizarRegistro->execute(array(
            ':ind_cedula_documento'  => $CEDULA,
            ':ind_nombre1'  => $NOM_1,
            ':ind_nombre2'  => $NOM_2,
            ':ind_apellido1' => $APE_1,
            ':ind_apellido2' => $APE_2,
            ':fk_a006_num_miscelaneo_detalle_sexo' => $SEXO,
            ':fec_nacimiento' => $FECHA_NAC[2]."-".$FECHA_NAC[1]."-".$FECHA_NAC[0],
            ':fk_a006_num_miscelaneo_detalle_edocivil' => $EDOCIVIL,
            ':fk_a006_num_miscelaneo_det_gruposangre'  => $SANGRE
        ));

        $errorRegPersona = $ActualizarRegistro->errorInfo();

        //obtengo id de la persona
        $idPersona= $persona;

        if(!empty($errorRegPersona[1]) && !empty($errorRegPersona[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $errorRegPersona;

        }else{

            //VERIFICO PARA REGISTRAR DIRECCION DE LA PERSONA
            if(isset($TIPO_DIR) && !empty($TIPO_DIR)){

                if($ID_DIR!=0){

                    $ActualizarDireccion = $this->_db->prepare(
                        "UPDATE
                        a036_persona_direccion
                     SET
                        fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                        fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                        fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                        ind_zona_residencial=:ind_zona_residencial,
                        fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                        ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                        fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                        ind_tipo_inmueble=:ind_tipo_inmueble,
                        fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                        ind_inmueble=:ind_inmueble,
                        ind_punto_referencia=:ind_punto_referencia,
                        ind_zona_postal=:ind_zona_postal,
                        ind_direccion=:ind_direccion
                     WHERE  fk_a003_num_persona='$idPersona' and pk_num_direccion_persona='$ID_DIR'
                      ");

                    $ActualizarDireccion->execute(array(
                        ':fk_a006_num_miscelaneo_detalle_tipodir'  => $TIPO_DIR,
                        ':fk_a006_num_miscelaneo_detalle_domicilio' => $SIT_DOM,
                        ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL,
                        ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL,'utf8'),
                        ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE,
                        ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE,'utf8'),
                        ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE,
                        ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE,'utf8'),
                        ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA,
                        ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE,'utf8'),
                        ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF,'utf8'),
                        ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL,'utf8'),
                        ':ind_direccion'  => mb_strtoupper($DIRECCION,'utf8')
                    ));

                    $errorRegDireccion = $ActualizarDireccion->errorInfo();

                }else{

                    $RegistroDireccion = $this->_db->prepare(
                        "INSERT INTO
                            a036_persona_direccion
                         SET
                            fk_a003_num_persona='$idPersona',
                            fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                            fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                            fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                            ind_zona_residencial=:ind_zona_residencial,
                            fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                            ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                            fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                            ind_tipo_inmueble=:ind_tipo_inmueble,
                            fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                            ind_inmueble=:ind_inmueble,
                            ind_punto_referencia=:ind_punto_referencia,
                            ind_zona_postal=:ind_zona_postal,
                            ind_direccion=:ind_direccion
                          ");

                    $RegistroDireccion->execute(array(
                        ':fk_a006_num_miscelaneo_detalle_tipodir'  => $TIPO_DIR,
                        ':fk_a006_num_miscelaneo_detalle_domicilio'  => $SIT_DOM,
                        ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL,
                        ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL,'utf8'),
                        ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE,
                        ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE,'utf8'),
                        ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE,
                        ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE,'utf8'),
                        ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA,
                        ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE,'utf8'),
                        ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF,'utf8'),
                        ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL,'utf8'),
                        ':ind_direccion'  => mb_strtoupper($DIRECCION,'utf8')
                    ));

                    $errorRegDireccion = $RegistroDireccion->errorInfo();

                }

            }else{
                $errorRegDireccion = '';
            }

            if(!empty($errorRegDireccion[1]) && !empty($errorRegDireccion[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $errorRegDireccion;

            }else{

                //VERIFICO PARA MODIFICAR TELEFONOS DE LA PERSONA
                if($TIPO_TELF!=0){

                    for ($i = 0; $i < count($TIPO_TELF); $i++) {

                        if (isset($ID_TELF[$i])) {//actualizo telefono

                            $RegistroTelefono = $this->_db->prepare(
                                "UPDATE
                                    a007_persona_telefono
                                 SET
                                    ind_telefono=:ind_telefono,
                                    fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono
                                 WHERE fk_a003_num_persona='$idPersona' and pk_num_telefono='$ID_TELF[$i]'
                                  ");

                        }else{//registro nuevo telefono

                            $RegistroTelefono = $this->_db->prepare(
                                "INSERT INTO
                                    a007_persona_telefono
                                 SET
                                    ind_telefono=:ind_telefono,
                                    fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                                    fk_a003_num_persona='$idPersona'
                                  ");

                        }

                        #execute — Ejecuta una sentencia preparada
                        $RegistroTelefono->execute(array(
                            ':ind_telefono' => $TELF[$i],
                            ':fk_a006_num_miscelaneo_detalle_tipo_telefono' => $TIPO_TELF[$i],
                        ));

                        $errorRegTelefono = $RegistroTelefono->errorInfo();

                    }

                }else{

                    $errorRegTelefono='';
                }

                if(!empty($errorRegTelefono[1]) && !empty($errorRegTelefono[2])){
                    //en caso de error hago rollback
                    $this->_db->rollBack();
                    return $errorRegTelefono;
                }else{

                    //ACTUALIZO LOS DATOS DE LA CARGA FAMILIAR
                    $RegistroDatos = $this->_db->prepare(
                        "UPDATE
                            rh_c015_carga_familiar
                         SET
                            fk_a006_num_miscelaneo_detalle_parentezco=:fk_a006_num_miscelaneo_detalle_parentezco,
                            fk_a006_num_miscelaneo_detalle_gradoinst=:fk_a006_num_miscelaneo_detalle_gradoinst,
                            fk_a006_num_miscelaneo_detalle_tipoedu=:fk_a006_num_miscelaneo_detalle_tipoedu,
                            fk_rhc040_num_institucion=:fk_rhc040_num_institucion,
                            ind_empresa=:ind_empresa,
                            txt_direccion_empresa=:txt_direccion_empresa,
                            num_sueldo_mensual=:num_sueldo_mensual,
                            num_flag_discapacidad=:num_flag_discapacidad,
                            num_flag_estudia=:num_flag_estudia,
                            num_flag_trabaja=:num_flag_trabaja,
                            fk_rhc064_nivel_grado_instruccion=:fk_rhc064_nivel_grado_instruccion,
                            fec_ultima_modificacion=NOW(),
                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                          WHERE pk_num_carga='$idCargaFamiliar'
                          ");
                    $RegistroDatos->execute(array(
                        ':fk_a006_num_miscelaneo_detalle_parentezco' => $PARENTESCO,
                        ':fk_a006_num_miscelaneo_detalle_gradoinst' => $GRADO_INST,
                        ':fk_a006_num_miscelaneo_detalle_tipoedu' => $TIPO_EDU,
                        ':fk_rhc040_num_institucion' => $CENTRO,
                        ':ind_empresa' => $EMPRESA,
                        ':txt_direccion_empresa' => $DIR_EMPRE,
                        ':num_sueldo_mensual' => $SUELDO,
                        ':num_flag_discapacidad' => $FLAG_DISCAPACIDAD,
                        ':num_flag_estudia' => $FLAG_ESTUDIA,
                        ':num_flag_trabaja' => $FLAG_TRABAJA,
                        ':fk_rhc064_nivel_grado_instruccion' => $NIVEL_INST
                    ));

                    $errorRegDatos= $RegistroDatos->errorInfo();

                    if(!empty($errorRegDatos[1]) && !empty($errorRegDatos[2])){
                        //en caso de error hago rollback
                        $this->_db->rollBack();
                        return $errorRegDatos;
                    }else{

                        #verifico si existe una foto temporal, la renombro y la guardo
                        if($FOTO){

                            $ruta_tmp = "publico/imagenes/modRH/tmp/".$FOTO;
                            $ruta     = "publico/imagenes/modRH/fotos/".$FOTO;
                            $partes   = explode(".",$FOTO);
                            $ruta_f = "publico/imagenes/modRH/fotos/".$idPersona;

                            if (file_exists($ruta_f)){

                                unlink($ruta_f);

                            }else{

                                if(file_exists($ruta_tmp)){

                                    if(!copy($ruta_tmp,$ruta)){
                                        echo "Error al copiar la foto";
                                    }else{
                                        chmod($ruta, 0777);
                                        rename($ruta,"publico/imagenes/modRH/fotos/".$idPersona.".".$partes[1]);
                                        unlink($ruta_tmp);

                                        $actualizoFoto = $this->_db->prepare("
                                                 UPDATE
                                                      a003_persona
                                                 SET
                                                      ind_foto=:foto
                                                 WHERE pk_num_persona='$idPersona'

                                        ");
                                        $actualizoFoto->execute(array(
                                            ':foto' => $idPersona.".".$partes[1]
                                        ));

                                        $errorFoto = $actualizoFoto->errorInfo();

                                        if(!empty($errorFoto[1]) && !empty($errorFoto[2])){
                                            $this->_db->rollBack();
                                            return $errorFoto;
                                        }else{
                                            $this->_db->commit();
                                            return $idCargaFamiliar;
                                        }

                                    }

                                }

                            }

                        }else{
                            //en caso de exito hago commit de la operacion
                            $this->_db->commit();
                            return $idCargaFamiliar;
                        }

                    }

                }

            }

        }

    }

    #PERMITE ELIMINAR CARGA FAMILIAR DEL EMPLEADO
    public function metEliminarCargaFamiliar($idCargaFamiliar)
    {

        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c015_carga_familiar WHERE pk_num_carga=:pk_num_carga
            ");
        $eliminar->execute(array(
            'pk_num_carga'=>$idCargaFamiliar
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCargaFamiliar;
        }

    }


    public function metCargaFamiliarEmpleado($empleado)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                vl_rh_empleado_carga_familiar
                WHERE pk_num_empleado='$empleado'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metCargaFamiliarEmpleadoRequisitos($empleado)
    {
        $con = $this->_db->query("
                SELECT
                vl_rh_empleado_carga_familiar.pk_num_carga,
                vl_rh_empleado_carga_familiar.pk_num_empleado,
                vl_rh_empleado_carga_familiar.pk_num_persona,
                vl_rh_empleado_carga_familiar.ind_cedula_documento,
                vl_rh_empleado_carga_familiar.nombre,
                vl_rh_empleado_carga_familiar.fec_nacimiento,
                date_format(vl_rh_empleado_carga_familiar.fec_nacimiento, '%d-%m-%Y') AS fecha_nacimiento,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_sexo,
                vl_rh_empleado_carga_familiar.sexo,
                vl_rh_empleado_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco,
                vl_rh_empleado_carga_familiar.parentesco,
                vl_rh_empleado_carga_familiar.fk_rhc064_nivel_grado_instruccion,
                rh_c064_nivel_grado_instruccion.ind_nombre_nivel_grado AS nivel_grado_instruccion,
                rh_c064_nivel_grado_instruccion.num_estatus AS estado,
                rh_c064_nivel_grado_instruccion.num_flag_utiles
                FROM
                vl_rh_empleado_carga_familiar
                INNER JOIN rh_c064_nivel_grado_instruccion ON rh_c064_nivel_grado_instruccion.pk_num_nivel_grado = vl_rh_empleado_carga_familiar.fk_rhc064_nivel_grado_instruccion
                WHERE pk_num_empleado='$empleado' AND rh_c064_nivel_grado_instruccion.num_flag_utiles=1
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metGenerarCedula($empleado)
    {
        #1
        /*OBTENGO CEDULA DEL EMPLEADO*/
        $consulta = $this->_db->prepare("SELECT ind_cedula_documento FROM vl_rh_persona_empleado WHERE	pk_num_empleado = '$empleado'");
        $consulta->execute();
        $datoCedula = $consulta->fetch(PDO::FETCH_ASSOC);
        $cedulaEmpleado = $datoCedula['ind_cedula_documento'];
        $cedulaEmpleado = $cedulaEmpleado."-";
        #2
        /*VERIFICO LA CARGA FAMILIAR DEL EMPLEADO*/
        $con = $this->_db->query("
                SELECT
                *
                FROM
                vl_rh_empleado_carga_familiar
                WHERE pk_num_empleado='$empleado' and ind_cedula_documento LIKE '%$cedulaEmpleado%'
             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetchAll();
        #3
        /*SI NO TIENE CARGA FAMILIAR INICIO EL CONTADOR EN 1*/
        /*LA CEDULADEL HIJO QUEDARIA ASI: cedulaEmpleado-1*/
        if(count($resultado)==0){
            $cont=1;
        }else{
            #4
            #verifico la cantidad de hijos y genero la cedula
            $cont=1;
            foreach($resultado as $data){

                $cont++;
            }
        }
        #5
        /*GENERO LA CEDULA*/
        $cedula = $datoCedula['ind_cedula_documento']."-".$cont;
        return $cedula;
    }




}//fin clase
