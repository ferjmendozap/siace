<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class experienciaLaboralModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR TODA LA EXPERIENCIA LABORAL DEL EMPLEADO
    public function metListarExperienciaLaboral($empleado)
    {
        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    vl_rh_empleado_experiencia_laboral
                WHERE pk_num_empleado='$empleado'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE MOSTRAR LA EXPERIENCIA LABORAL EN EL FORMULARIO
    public function metMostrarExperienciaLaboral($idExperienciaLaboral)
    {

        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    vl_rh_empleado_experiencia_laboral
                WHERE pk_num_experiencia='$idExperienciaLaboral'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    #PERMITE INSERTAR EXPERIENCIA LABORAL DEL EMPLEADO
    public function metRegistrarExperiencialaboral($empleado,$cargo,$empresa,$areaexp,$fec_ingreso,$fec_egreso,$t_empresa,$cese,$sueldo,$flag_antvacacion,$funciones)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

            $fi = explode("-",$fec_ingreso);
            $fe = explode("-",$fec_egreso);

            #buscar area de experiencia
            $consulta = $this->_db->query("
                    SELECT ind_nombre_detalle
                    FROM
                        a006_miscelaneo_detalle
                    WHERE pk_num_miscelaneo_detalle='$areaexp'
                 ");

            $consulta->setFetchMode(PDO::FETCH_ASSOC);
            $datos =  $consulta->fetch();
            $ind_area = $datos['ind_nombre_detalle'];

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c011_experiencia
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    ind_cargo=:ind_cargo,
                    ind_empresa=:ind_empresa,
                    fk_a006_num_miscelaneo_detalle_areaexp=:fk_a006_num_miscelaneo_detalle_areaexp,
                    ind_area=:ind_area,
                    fec_ingreso=:fec_ingreso,
                    fec_egreso=:fec_egreso,
                    fk_a006_num_miscelaneo_detalle_tipo_empresa=:fk_a006_num_miscelaneo_detalle_tipo_empresa,
                    fk_rhc032_num_motivo_cese=:fk_rhc032_num_motivo_cese,
                    num_sueldo_mensual=:num_sueldo_mensual,
                    num_flag_antvacacion=:num_flag_antvacacion,
                    txt_funciones=:txt_funciones,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':ind_cargo'  => $cargo,
                ':ind_empresa'  => $empresa,
                ':fk_a006_num_miscelaneo_detalle_areaexp'  => $areaexp,
                ':ind_area'  => $ind_area,
                ':fec_ingreso'  => $fi[2]."-".$fi[1]."-".$fi[0],
                ':fec_egreso' => $fe[2]."-".$fe[1]."-".$fe[0],
                ':fk_a006_num_miscelaneo_detalle_tipo_empresa' => $t_empresa,
                ':fk_rhc032_num_motivo_cese' => $cese,
                ':num_sueldo_mensual' => $sueldo,
                ':num_flag_antvacacion' => $flag_antvacacion,
                ':txt_funciones' => $funciones
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{

                $this->_db->commit();
                return $idRegistro;

            }


    }

    #PERMITE MODIFICAR LA EXPERIENCIA LABORAL DEL EMPLEADO
    public function metModificarExperienciaLaboral($idExperienciaLaboral,$cargo,$empresa,$areaexp,$fec_ingreso,$fec_egreso,$t_empresa,$cese,$sueldo,$flag_antvacacion,$funciones)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        $fi = explode("-",$fec_ingreso);
        $fe = explode("-",$fec_egreso);

        #buscar area de experiencia
        $consulta = $this->_db->query("
                    SELECT ind_nombre_detalle
                    FROM
                        a006_miscelaneo_detalle
                    WHERE pk_num_miscelaneo_detalle='$areaexp'
                 ");

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        $datos =  $consulta->fetch();
        $ind_area = $datos['ind_nombre_detalle'];

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $ActualizarRegistro = $this->_db->prepare(
            "UPDATE
                rh_c011_experiencia
             SET
                    ind_cargo=:ind_cargo,
                    ind_empresa=:ind_empresa,
                    fk_a006_num_miscelaneo_detalle_areaexp=:fk_a006_num_miscelaneo_detalle_areaexp,
                    ind_area=:ind_area,
                    fec_ingreso=:fec_ingreso,
                    fec_egreso=:fec_egreso,
                    fk_a006_num_miscelaneo_detalle_tipo_empresa=:fk_a006_num_miscelaneo_detalle_tipo_empresa,
                    fk_rhc032_num_motivo_cese=:fk_rhc032_num_motivo_cese,
                    num_sueldo_mensual=:num_sueldo_mensual,
                    num_flag_antvacacion=:num_flag_antvacacion,
                    txt_funciones=:txt_funciones,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE pk_num_experiencia='$idExperienciaLaboral'
              ");

        #execute — Ejecuta una sentencia preparada
        $ActualizarRegistro->execute(array(
            ':ind_cargo'  => $cargo,
            ':ind_empresa'  => $empresa,
            ':fk_a006_num_miscelaneo_detalle_areaexp'  => $areaexp,
            ':ind_area'  => $ind_area,
            ':fec_ingreso'  => $fi[2]."-".$fi[1]."-".$fi[0],
            ':fec_egreso' => $fe[2]."-".$fe[1]."-".$fe[0],
            ':fk_a006_num_miscelaneo_detalle_tipo_empresa' => $t_empresa,
            ':fk_rhc032_num_motivo_cese' => $cese,
            ':num_sueldo_mensual' => $sueldo,
            ':num_flag_antvacacion' => $flag_antvacacion,
            ':txt_funciones' => $funciones
        ));

        $error= $ActualizarRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $error;

        }else{
            $this->_db->commit();
            return $idExperienciaLaboral;
        }

    }

    #PERMITE ELIMINAR EXPERIENCIA LABORAL DEL EMPLEADO
    public function metEliminarExperienciaLaboral($idExperienciaLaboral)
    {

        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c011_experiencia WHERE pk_num_experiencia=:pk_num_experiencia
            ");
        $eliminar->execute(array(
            'pk_num_experiencia'=>$idExperienciaLaboral
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idExperienciaLaboral;
        }

    }




}//fin clase
