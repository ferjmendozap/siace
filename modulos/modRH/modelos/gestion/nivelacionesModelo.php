<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class nivelacionesModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR HISTORIAL DE NIVELACIONES DEL EMPLEADO
    public function metListarHistorialNivelaciones($empleado)
    {
        $con = $this->_db->query("
                SELECT
                rh_c060_empleado_nivelacion_historial.pk_num_empleado_nivelacion_historial,
                rh_c060_empleado_nivelacion_historial.fk_rhc059_num_empleado_nivelacion,
                rh_c059_empleado_nivelacion.fk_rhb001_num_empleado,
                CONCAT(persona_empleado.ind_nombre1,' ',persona_empleado.ind_nombre2,' ',persona_empleado.ind_apellido1,' ',persona_empleado.ind_apellido2) AS nombre_empleado,
                rh_c060_empleado_nivelacion_historial.fec_fecha_registro,
                rh_c060_empleado_nivelacion_historial.ind_organismo,
                rh_c060_empleado_nivelacion_historial.ind_dependencia,
                rh_c060_empleado_nivelacion_historial.ind_cargo,
                rh_c060_empleado_nivelacion_historial.num_nivel_salarial,
                rh_c060_empleado_nivelacion_historial.ind_categoria_cargo,
                rh_c060_empleado_nivelacion_historial.ind_tipo_nomina,
                rh_c060_empleado_nivelacion_historial.ind_centro_costo,
                rh_c060_empleado_nivelacion_historial.ind_tipo_accion,
                rh_c060_empleado_nivelacion_historial.num_estatus,
                rh_c059_empleado_nivelacion.fk_rhb001_num_empleado_responsable,
                CONCAT(persona_responsable.ind_nombre1,' ',persona_responsable.ind_nombre2,' ',persona_responsable.ind_apellido1,' ',persona_responsable.ind_apellido2) AS nombre_responsable,
                rh_c060_empleado_nivelacion_historial.ind_responsable,
                rh_c060_empleado_nivelacion_historial.fec_ultima_modificacion,
                rh_c060_empleado_nivelacion_historial.fk_a018_num_seguridad_usuario
                FROM
                rh_c060_empleado_nivelacion_historial
                INNER JOIN rh_c059_empleado_nivelacion ON rh_c060_empleado_nivelacion_historial.fk_rhc059_num_empleado_nivelacion = rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion
                INNER JOIN rh_b001_empleado AS empleado ON rh_c059_empleado_nivelacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
                INNER JOIN a003_persona AS persona_empleado ON persona_empleado.pk_num_persona = empleado.fk_a003_num_persona
                LEFT JOIN rh_b001_empleado AS responsable ON responsable.pk_num_empleado = rh_c059_empleado_nivelacion.fk_rhb001_num_empleado_responsable
                LEFT JOIN a003_persona AS persona_responsable ON persona_responsable.pk_num_persona = responsable.fk_a003_num_persona
                WHERE fk_rhb001_num_empleado='$empleado'
             ");


        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #permite consultar las condiciones anteriores del empleado para la nivelacion
    public function metConsultarDatosAnterioresNivelacion($empleado)
    {
        $con = $this->_db->query("select * from vl_rh_persona_empleado_datos_laborales where pk_num_empleado='$empleado'");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    public function metConsultarDatosAnterioresNivelacionSueldo($empleado)
    {

        $con = $this->_db->prepare("select * from vl_rh_sueldo_empleado where pk_num_empleado='$empleado'");
        $con->execute();
        $registros = $con->rowCount();
        if($registros>0){
            return $con->fetch(PDO::FETCH_ASSOC);
        }else{
            $con1 = $this->_db->prepare("select * from vl_rh_sueldo_empleado_paso where pk_num_empleado='$empleado'");
            $con1->execute();
            return $con1->fetch(PDO::FETCH_ASSOC);
        }

    }



    #permite listar centros de costos
    public function metListadoCentroCostos()
    {
        $centros = $this->_db->query(
            "SELECT
              pk_num_centro_costo,
              ind_descripcion_centro_costo
            FROM
              a023_centro_costo

            ");
        $centros->setFetchMode(PDO::FETCH_ASSOC);
        return $centros->fetchAll();
    }

    #PERMITE MOSTRAR HISTORIAL NIVELACIONES DEL EMPLEADO
    public function metMostrarHistorialNivelaciones($historial)
    {

        $con = $this->_db->query("
                SELECT
                rh_c060_empleado_nivelacion_historial.pk_num_empleado_nivelacion_historial,
                rh_c060_empleado_nivelacion_historial.fk_rhc059_num_empleado_nivelacion,
                rh_c059_empleado_nivelacion.fk_rhb001_num_empleado,
                CONCAT(persona_empleado.ind_nombre1,' ',persona_empleado.ind_nombre2,' ',persona_empleado.ind_apellido1,' ',persona_empleado.ind_apellido2) AS nombre_empleado,
                rh_c060_empleado_nivelacion_historial.fec_fecha_registro,
                rh_c060_empleado_nivelacion_historial.ind_organismo,
                rh_c060_empleado_nivelacion_historial.ind_dependencia,
                rh_c060_empleado_nivelacion_historial.ind_cargo,
                rh_c060_empleado_nivelacion_historial.num_nivel_salarial,
                rh_c060_empleado_nivelacion_historial.ind_categoria_cargo,
                rh_c060_empleado_nivelacion_historial.ind_tipo_nomina,
                rh_c060_empleado_nivelacion_historial.ind_centro_costo,
                rh_c060_empleado_nivelacion_historial.ind_tipo_accion,
                rh_c060_empleado_nivelacion_historial.num_estatus,
                rh_c059_empleado_nivelacion.fk_rhb001_num_empleado_responsable,
                CONCAT(persona_responsable.ind_nombre1,' ',persona_responsable.ind_nombre2,' ',persona_responsable.ind_apellido1,' ',persona_responsable.ind_apellido2) AS nombre_responsable,
                rh_c060_empleado_nivelacion_historial.ind_responsable,
                rh_c060_empleado_nivelacion_historial.fec_ultima_modificacion,
                rh_c060_empleado_nivelacion_historial.fk_a018_num_seguridad_usuario
                FROM
                rh_c060_empleado_nivelacion_historial
                INNER JOIN rh_c059_empleado_nivelacion ON rh_c060_empleado_nivelacion_historial.fk_rhc059_num_empleado_nivelacion = rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion
                INNER JOIN rh_b001_empleado AS empleado ON rh_c059_empleado_nivelacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
                INNER JOIN a003_persona AS persona_empleado ON persona_empleado.pk_num_persona = empleado.fk_a003_num_persona
                LEFT JOIN rh_b001_empleado AS responsable ON responsable.pk_num_empleado = rh_c059_empleado_nivelacion.fk_rhb001_num_empleado_responsable
                LEFT JOIN a003_persona AS persona_responsable ON persona_responsable.pk_num_persona = responsable.fk_a003_num_persona
                WHERE rh_c060_empleado_nivelacion_historial.pk_num_empleado_nivelacion_historial='$historial'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    #PERMITE INSERTAR LAS NIVELACIONES DEL EMPLEADO
    public function metRegistrarNivelacion($empleado,$fecha,$org,$dep,$centroCosto,$cargo,$nomina,$tipoaccion,$motivo,$documento,$responsable,$accion,$cargoAnt,$depAnt,$estatus,$paso,$sueldo,$obs)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

            /*fecha registro*/
            $fe = explode("-",$fecha);

            /*tipo accion*/
            $ta = explode("-",$tipoaccion);

            /*OBTENGO EL DIA ANTERIOR DE LA FECHA DE NIVELACION*/
             $con = $this->_db->query("SELECT DATE_ADD('".$fe[2]."-".$fe[1]."-".$fe[0]."', INTERVAL -1 DAY) AS Fecha");
             $con->setFetchMode(PDO::FETCH_ASSOC);
             $f = $con->fetch();

            /*DEPENDIENDO DE LA ACCION REALIZO OPERACIONES*/
            if(strcmp($accion,'ET')==0){/*si es encargaduria temporal*/

                /*ACTUALIZO NIVELACION*/
                $ActualizarRegistro = $this->_db->prepare(
                    "UPDATE
                        rh_c059_empleado_nivelacion
                     SET
                        fec_fecha_hasta=:fec_fecha_hasta
                     WHERE
                        fk_rhb001_num_empleado='$empleado' AND
                        fk_a006_num_miscelaneo_detalle_tipoaccion=:fk_a006_num_miscelaneo_detalle_tipoaccion AND
                        fec_fecha_hasta=NULL
                 ");

                #execute — Ejecuta una sentencia preparada
                $ActualizarRegistro->execute(array(
                    ':fec_fecha_hasta' => $f['Fecha'],
                    ':fk_a006_num_miscelaneo_detalle_tipoaccion'  => $ta[0]
                ));

                /*ACTUALIZO EMPLEADO*/
                if($depAnt != $dep) $dependenciaTemp = $dep; else $dependenciaTemp = $depAnt;
                if($cargoAnt != $cargo) $cargoTemp = $cargo; else $cargoTemp = $cargoAnt;

                #actualizo datos organizacion y laborales
                $ActualizarEmpleadoOrg = $this->_db->prepare(
                    "UPDATE
                        rh_c076_empleado_organizacion
                     SET
                        fk_a004_num_dependencia_tmp=:fk_a004_num_dependencia_tmp,
                        fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                        fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina
                     WHERE
                        fk_rhb001_num_empleado='$empleado'
                 ");
                #execute — Ejecuta una sentencia preparada
                $ActualizarEmpleadoOrg->execute(array(
                    ':fk_a004_num_dependencia_tmp' => $dependenciaTemp,
                    ':fk_a023_num_centro_costo'  => $centroCosto,
                    ':fk_nmb001_num_tipo_nomina'  => $nomina,
                ));
                $ActualizarEmpleadoLab = $this->_db->prepare(
                    "UPDATE
                        rh_c005_empleado_laboral
                     SET
                        fk_rhc063_num_puestos_cargotmp=:fk_rhc063_num_puestos_cargotmp
                     WHERE
                        fk_rhb001_num_empleado='$empleado'
                 ");
                #execute — Ejecuta una sentencia preparada
                $ActualizarEmpleadoLab->execute(array(
                    ':fk_rhc063_num_puestos_cargotmp' => $cargoTemp,
                ));

            }
            else if(strcmp($accion,'CP')==0) {/*si es CAMBIO DE PASO*/


                /*ACTUALIZO NIVELACION*/
                $ActualizarRegistro = $this->_db->prepare(
                    "UPDATE
                        rh_c059_empleado_nivelacion
                     SET
                        fec_fecha_hasta=:fec_fecha_hasta
                     WHERE
                        fk_rhb001_num_empleado='$empleado' AND
                        fk_a006_num_miscelaneo_detalle_tipoaccion=:fk_a006_num_miscelaneo_detalle_tipoaccion AND
                        fec_fecha_hasta=NULL
                 ");
                #execute — Ejecuta una sentencia preparada
                $ActualizarRegistro->execute(array(
                    ':fec_fecha_hasta' => $f['Fecha'],
                    ':fk_a006_num_miscelaneo_detalle_tipoaccion'  => $ta[0]
                ));

                $ActualizarEmpleadoLab = $this->_db->prepare(
                    "UPDATE
                        rh_c005_empleado_laboral
                     SET
                        fk_a006_num_miscelaneo_detalle_paso=:fk_a006_num_miscelaneo_detalle_paso,
                        num_sueldo = :num_sueldo
                     WHERE
                        fk_rhb001_num_empleado='$empleado'
                 ");
                #execute — Ejecuta una sentencia preparada
                $ActualizarEmpleadoLab->execute(array(
                    ':fk_a006_num_miscelaneo_detalle_paso' => $paso,
                    ':num_sueldo' => $sueldo,
                ));


            }
            else{/*si no es encargaduria temporal*/

                $ActualizarRegistro = $this->_db->prepare(
                    "UPDATE
                        rh_c059_empleado_nivelacion
                     SET
                        fec_fecha_hasta=:fec_fecha_hasta
                     WHERE
                        fk_rhb001_num_empleado='$empleado' AND
                        fec_fecha_hasta=NULL
                 ");
                #execute — Ejecuta una sentencia preparada
                $ActualizarRegistro->execute(array(
                    ':fec_fecha_hasta' => $f['Fecha'],

                ));
                /*ACTUALIZO EMPLEADO*/
                #actualizo datos organizacion y laborales
                $ActualizarEmpleadoOrg = $this->_db->prepare(
                    "UPDATE
                        rh_c076_empleado_organizacion
                     SET
                        fk_a001_num_organismo=:fk_a001_num_organismo,
                        fk_a004_num_dependencia=:fk_a004_num_dependencia,
                        fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                        fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                        fk_a004_num_dependencia_tmp=NULL
                     WHERE
                        fk_rhb001_num_empleado='$empleado'
                 ");
                #execute — Ejecuta una sentencia preparada
                $ActualizarEmpleadoOrg->execute(array(
                    ':fk_a001_num_organismo'  => $org,
                    ':fk_a004_num_dependencia'  => $dep,
                    ':fk_nmb001_num_tipo_nomina'  => $nomina,
                    ':fk_a023_num_centro_costo'  => $centroCosto
                ));
                $errorActOrg = $ActualizarEmpleadoOrg->errorInfo();
                if(!empty($errorActOrg[1]) && !empty($errorActOrg[2])){
                    //en caso de error hago rollback
                    $this->_db->rollBack();
                    return $errorActOrg;

                }else{

                }

                $ActualizarEmpleadoLab = $this->_db->prepare(
                    "UPDATE
                        rh_c005_empleado_laboral
                     SET
                        fk_rhc063_num_puestos_cargo=:fk_rhc063_num_puestos_cargo,
                        fk_rhc063_num_puestos_cargotmp=NULL
                     WHERE
                        fk_rhb001_num_empleado='$empleado'
                 ");
                #execute — Ejecuta una sentencia preparada
                $ActualizarEmpleadoLab->execute(array(
                    ':fk_rhc063_num_puestos_cargo' => $cargo,
                ));
                $errorActOrg = $ActualizarEmpleadoLab->errorInfo();
                if(!empty($errorActOrg[1]) && !empty($errorActOrg[2])){
                    //en caso de error hago rollback
                    $this->_db->rollBack();
                    return $errorActOrg;

                }else{

                }


            }

            #inserto la nivelacion
            $NuevoRegistroNivelacion = $this->_db->prepare(
                "INSERT INTO
                    rh_c059_empleado_nivelacion
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fec_fecha_registro=:fec_fecha_registro,
                    fk_a001_num_organismo=:fk_a001_num_organismo,
                    fk_a004_num_dependencia=:fk_a004_num_dependencia,
                    fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                    fk_rhc063_num_puestos=:fk_rhc063_num_puestos,
                    fk_a006_num_miscelaneo_detalle_paso=:fk_a006_num_miscelaneo_detalle_paso,
                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                    fk_a006_num_miscelaneo_detalle_tipoaccion=:fk_a006_num_miscelaneo_detalle_tipoaccion,
                    ind_motivo=:ind_motivo,
                    ind_documento=:ind_documento,
                    fk_rhb001_num_empleado_responsable=:fk_rhb001_num_empleado_responsable,
                    num_estatus=:num_estatus,
                    fec_fecha_hasta=:fec_fecha_hasta,
                    txt_observaciones=:txt_observaciones,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistroNivelacion->execute(array(
                ':fec_fecha_registro' => $fe[2]."-".$fe[1]."-".$fe[0],
                ':fk_a001_num_organismo'  => $org,
                ':fk_a004_num_dependencia'  => $dep,
                ':fk_a023_num_centro_costo'  => $centroCosto,
                ':fk_rhc063_num_puestos'  => $cargo,
                ':fk_a006_num_miscelaneo_detalle_paso'  => $paso,
                ':fk_nmb001_num_tipo_nomina' => $nomina,
                ':fk_a006_num_miscelaneo_detalle_tipoaccion' => $ta[0],
                ':ind_motivo' => $motivo,
                ':ind_documento' => $documento,
                ':fk_rhb001_num_empleado_responsable' => $responsable,
                ':num_estatus' => $estatus,
                ':fec_fecha_hasta' => NULL,
                ':txt_observaciones' => $obs
            ));

            $error = $NuevoRegistroNivelacion->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{

                #registro nivelacion historial
                $historial_nivelacion = $this->_db->prepare(
                    "INSERT INTO
                                    rh_c060_empleado_nivelacion_historial
                                    (fk_rhc059_num_empleado_nivelacion,
                                     fec_fecha_registro,
                                     ind_organismo,
                                     ind_dependencia,
                                     ind_cargo,
                                     num_nivel_salarial,
                                     ind_categoria_cargo,
                                     ind_tipo_nomina,
                                     ind_centro_costo,
                                     ind_tipo_accion,
                                     num_estatus,
                                     ind_responsable,
                                     fec_ultima_modificacion,
                                     fk_a018_num_seguridad_usuario)
                                SELECT
                                    rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion,
                                    rh_c059_empleado_nivelacion.fec_fecha_registro,
                                    organismo.ind_descripcion_empresa AS ind_organismo,
                                    dependencia.ind_dependencia AS ind_dependencia,
                                    cargo.ind_descripcion_cargo AS ind_cargo,
                                    cargo.num_sueldo_basico AS num_nivel_salarial,
                                    categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                    nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                    centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                    tipo_accion.ind_nombre_detalle AS ind_tipo_accion,
                                    rh_c059_empleado_nivelacion.num_estatus,
                                    CONCAT(responsable.ind_nombre1,' ',responsable.ind_nombre2,' ',responsable.ind_apellido1,' ',responsable.ind_apellido2) AS ind_responsable,
                                    NOW() as fec_ultima_modificacion,
                                    '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                                FROM
                                    rh_c059_empleado_nivelacion
                                INNER JOIN a001_organismo AS organismo ON organismo.pk_num_organismo = rh_c059_empleado_nivelacion.fk_a001_num_organismo
                                INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = rh_c059_empleado_nivelacion.fk_a004_num_dependencia
                                INNER JOIN a023_centro_costo AS centro_costo ON centro_costo.pk_num_centro_costo = rh_c059_empleado_nivelacion.fk_a023_num_centro_costo
                                INNER JOIN rh_c063_puestos AS cargo ON cargo.pk_num_puestos = rh_c059_empleado_nivelacion.fk_rhc063_num_puestos
                                INNER JOIN nm_b001_tipo_nomina AS nomina ON nomina.pk_num_tipo_nomina = rh_c059_empleado_nivelacion.fk_nmb001_num_tipo_nomina
                                LEFT JOIN a006_miscelaneo_detalle AS tipo_accion ON tipo_accion.pk_num_miscelaneo_detalle = rh_c059_empleado_nivelacion.fk_a006_num_miscelaneo_detalle_tipoaccion
                                INNER JOIN a006_miscelaneo_detalle AS categoria ON cargo.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                LEFT JOIN rh_b001_empleado AS empleado_responsable ON rh_c059_empleado_nivelacion.fk_rhb001_num_empleado_responsable = empleado_responsable.pk_num_empleado
                                LEFT JOIN a003_persona AS responsable ON empleado_responsable.fk_a003_num_persona = responsable.pk_num_persona
                                WHERE rh_c059_empleado_nivelacion.fk_rhb001_num_empleado='$empleado'
                                AND rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion='$idRegistro'
                                  ");

                $historial_nivelacion->execute();

                $errorHistorialNivelacion = $historial_nivelacion->errorInfo();

                $idHistorial= $this->_db->lastInsertId();

                if(!empty($errorHistorialNivelacion[1]) && !empty($errorHistorialNivelacion[2])){
                    //en caso de error hago rollback
                    $this->_db->rollBack();
                    return $errorHistorialNivelacion;
                }else {

                    #registro historial del empleado
                    $historial_empleado = $this->_db->prepare(
                        "INSERT INTO
                                            rh_c061_empleado_historial
                                            (fk_rhb001_num_empleado,
                                             ind_periodo,
                                             fec_ingreso,
                                             ind_organismo,
                                             ind_dependencia,
                                             ind_centro_costo,
                                             ind_cargo,
                                             num_nivel_salarial,
                                             ind_categoria_cargo,
                                             ind_tipo_nomina,
                                             ind_tipo_pago,
                                             num_estatus,
                                             -- ind_motivo_cese,
                                             -- fec_egreso,
                                             -- txt_obs_cese,
                                             ind_tipo_trabajador,
                                             fec_ultima_modificacion,
                                             fk_a018_num_seguridad_usuario)
                                        SELECT
                                            empleado.pk_num_empleado,
                                            DATE_FORMAT(NOW(),'%Y-%m') as ind_periodo,
                                            rh_c005_empleado_laboral.fec_ingreso,
                                            a001_organismo.ind_descripcion_empresa AS ind_organismo,
                                            a004_dependencia.ind_dependencia,
                                            a023_centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                            rh_c063_puestos.ind_descripcion_cargo AS ind_cargo,
                                            rh_c063_puestos.num_sueldo_basico AS ind_nivel_salarial,
                                            categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                            nm_b001_tipo_nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                            tipo_pago.ind_nombre_detalle AS ind_tipo_pago,
                                            empleado.num_estatus,
                                            tipo_trabajador.ind_nombre_detalle AS ind_tipo_trabajador,
                                            NOW() as fec_ultima_modificacion,
                                            '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                                        FROM
                                            rh_b001_empleado AS empleado
                                            INNER JOIN rh_c076_empleado_organizacion ON empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                                            INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                            INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                                            INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                                            INNER JOIN a023_centro_costo ON rh_c076_empleado_organizacion.fk_a023_num_centro_costo = a023_centro_costo.pk_num_centro_costo
                                            INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                                            INNER JOIN a006_miscelaneo_detalle AS categoria ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                            INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                                            INNER JOIN a006_miscelaneo_detalle AS tipo_pago ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipopago = tipo_pago.pk_num_miscelaneo_detalle
                                            INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                                     WHERE
                                            empleado.pk_num_empleado='$empleado'

						        ");

                    $historial_empleado->execute();

                    $errorHistorialEmpleado = $historial_empleado->errorInfo();

                    if(!empty($errorHistorialEmpleado[1]) && !empty($errorHistorialEmpleado[2])){
                        //en caso de error hago rollback
                        $this->_db->rollBack();
                        return $errorHistorialEmpleado;
                    }else {
                        $this->_db->commit();
                        return $idHistorial;
                    }

                }

            }//fin if

    }


}//fin clase
