<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class referenciasModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR REFERENCIAS DEL EMPLEADO
    public function metListarReferencias($empleado)
    {
        $con = $this->_db->query("
                SELECT
                rh_c028_empleado_referencias.pk_num_empleado_referencia,
                rh_c028_empleado_referencias.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c028_empleado_referencias.fk_a006_num_miscelaneo_detalle_tiporef,
                a006_miscelaneo_detalle.ind_nombre_detalle AS tipo_referencia,
                rh_c028_empleado_referencias.ind_nombre,
                rh_c028_empleado_referencias.ind_empresa,
                rh_c028_empleado_referencias.ind_direccion,
                rh_c028_empleado_referencias.ind_telefono,
                rh_c028_empleado_referencias.ind_cargo,
                rh_c028_empleado_referencias.fec_ultima_modificacion,
                rh_c028_empleado_referencias.fk_a018_num_seguridad_usuario
                FROM
                rh_c028_empleado_referencias
                INNER JOIN rh_b001_empleado ON rh_c028_empleado_referencias.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle ON rh_c028_empleado_referencias.fk_a006_num_miscelaneo_detalle_tiporef = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                WHERE fk_rhb001_num_empleado='$empleado'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE MOSTRAR LAS REFERENCIAS DEL EMPLEADO
    public function metMostrarReferencias($idReferencia)
    {

        $con = $this->_db->query("
                SELECT
                rh_c028_empleado_referencias.pk_num_empleado_referencia,
                rh_c028_empleado_referencias.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c028_empleado_referencias.fk_a006_num_miscelaneo_detalle_tiporef,
                a006_miscelaneo_detalle.ind_nombre_detalle AS tipo_referencia,
                rh_c028_empleado_referencias.ind_nombre,
                rh_c028_empleado_referencias.ind_empresa,
                rh_c028_empleado_referencias.ind_direccion,
                rh_c028_empleado_referencias.ind_telefono,
                rh_c028_empleado_referencias.ind_cargo,
                rh_c028_empleado_referencias.fec_ultima_modificacion,
                rh_c028_empleado_referencias.fk_a018_num_seguridad_usuario
                FROM
                rh_c028_empleado_referencias
                INNER JOIN rh_b001_empleado ON rh_c028_empleado_referencias.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle ON rh_c028_empleado_referencias.fk_a006_num_miscelaneo_detalle_tiporef = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                WHERE pk_num_empleado_referencia='$idReferencia'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    #PERMITE INSERTAR LAS REFERENCIAS DEL EMPLEADO
    public function metRegistrarReferencias($empleado,$tiporef,$nombre,$empresa,$direccion,$telefono,$cargo)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();


            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c028_empleado_referencias
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fk_a006_num_miscelaneo_detalle_tiporef=:fk_a006_num_miscelaneo_detalle_tiporef,
                    ind_nombre=:ind_nombre,
                    ind_empresa=:ind_empresa,
                    ind_direccion=:ind_direccion,
                    ind_telefono=:ind_telefono,
                    ind_cargo=:ind_cargo,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle_tiporef' => $tiporef,
                ':ind_nombre'  => $nombre,
                ':ind_empresa'  => $empresa,
                ':ind_direccion' => $direccion,
                ':ind_telefono' => $telefono,
                ':ind_cargo' => $cargo
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{

                $this->_db->commit();
                return $idRegistro;

            }

    }

    #PERMITE MODIFICAR REFERENCIAS DEL EMPLEADO
    public function metModificarReferencias($idReferencia,$tiporef,$nombre,$empresa,$direccion,$telefono,$cargo)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $ActualizarRegistro = $this->_db->prepare(
            "UPDATE
                rh_c028_empleado_referencias
             SET
                fk_a006_num_miscelaneo_detalle_tiporef=:fk_a006_num_miscelaneo_detalle_tiporef,
                ind_nombre=:ind_nombre,
                ind_empresa=:ind_empresa,
                ind_direccion=:ind_direccion,
                ind_telefono=:ind_telefono,
                ind_cargo=:ind_cargo,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE pk_num_empleado_referencia='$idReferencia'
              ");

        #execute — Ejecuta una sentencia preparada
        $ActualizarRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_tiporef' => $tiporef,
            ':ind_nombre'  => $nombre,
            ':ind_empresa'  => $empresa,
            ':ind_direccion' => $direccion,
            ':ind_telefono' => $telefono,
            ':ind_cargo' => $cargo
        ));

        $error= $ActualizarRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $error;

        }else{
            $this->_db->commit();
            return $idReferencia;
        }

    }

    #PERMITE ELIMINAR REFERENCIAS DEL EMPLEADO
    public function metEliminarReferencias($idReferencia)
    {

        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c028_empleado_referencias WHERE pk_num_empleado_referencia=:pk_num_empleado_referencia
            ");
        $eliminar->execute(array(
            'pk_num_empleado_referencia'=>$idReferencia
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idReferencia;
        }

    }




}//fin clase
