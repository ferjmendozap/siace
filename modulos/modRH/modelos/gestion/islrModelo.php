<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class islrModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR TODOS LOS IMPUESTO SOBRE LA RENTA DEL EMPLEADO
    public function metListarIslr($empleado)
    {
        $con = $this->_db->query("
                SELECT
                rh_c033_empleado_islr.pk_num_empleado_islr,
                rh_c033_empleado_islr.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c033_empleado_islr.fec_anio,
                rh_c033_empleado_islr.ind_desde,
                rh_c033_empleado_islr.ind_hasta,
                rh_c033_empleado_islr.num_porcentaje,
                rh_c033_empleado_islr.fec_ultima_modificacion,
                rh_c033_empleado_islr.fk_a018_num_seguridad_usuario
                FROM
                rh_c033_empleado_islr
                INNER JOIN rh_b001_empleado ON rh_c033_empleado_islr.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE fk_rhb001_num_empleado='$empleado'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE MOSTRAR LA EXPERIENCIA LABORAL EN EL FORMULARIO
    public function metMostrarIslr($idIslr)
    {

        $con = $this->_db->query("
                SELECT
                rh_c033_empleado_islr.pk_num_empleado_islr,
                rh_c033_empleado_islr.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c033_empleado_islr.fec_anio,
                rh_c033_empleado_islr.ind_desde,
                rh_c033_empleado_islr.ind_hasta,
                rh_c033_empleado_islr.num_porcentaje,
                rh_c033_empleado_islr.fec_ultima_modificacion,
                rh_c033_empleado_islr.fk_a018_num_seguridad_usuario
                FROM
                rh_c033_empleado_islr
                INNER JOIN rh_b001_empleado ON rh_c033_empleado_islr.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE pk_num_empleado_islr='$idIslr'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    #PERMITE INSERTAR IMPUESTO SOBRE LA RENTA DEL EMPLEADO
    public function metRegistrarIslr($empleado,$anio,$desde,$hasta,$porcentaje)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();


            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c033_empleado_islr
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fec_anio=:fec_anio,
                    ind_desde=:ind_desde,
                    ind_hasta=:ind_hasta,
                    num_porcentaje=:num_porcentaje,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fec_anio'  => $anio,
                ':ind_desde'  => $desde,
                ':ind_hasta'  => $hasta,
                ':num_porcentaje'  => $porcentaje
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{

                $this->_db->commit();
                return $idRegistro;

            }


    }

    #PERMITE MODIFICAR IMPUESTO SOBRE LA RENTA DEL EMPLEADO
    public function metModificarIslr($idIslr,$anio,$desde,$hasta,$porcentaje)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();


        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $ActualizarRegistro = $this->_db->prepare(
            "UPDATE
                    rh_c033_empleado_islr
                 SET
                    fec_anio=:fec_anio,
                    ind_desde=:ind_desde,
                    ind_hasta=:ind_hasta,
                    num_porcentaje=:num_porcentaje,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE pk_num_empleado_islr='$idIslr'
              ");

        #execute — Ejecuta una sentencia preparada
        $ActualizarRegistro->execute(array(
            ':fec_anio'  => $anio,
            ':ind_desde'  => $desde,
            ':ind_hasta'  => $hasta,
            ':num_porcentaje'  => $porcentaje
        ));

        $error= $ActualizarRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $error;

        }else{
            $this->_db->commit();
            return $idIslr;
        }

    }

    #PERMITE ELIMINAR IMPUESTO SOBRE LA RENTA DEL EMPLEADO
    public function metEliminarIslr($idIslr)
    {

        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c033_empleado_islr WHERE pk_num_empleado_islr=:pk_num_empleado_islr
            ");
        $eliminar->execute(array(
            'pk_num_empleado_islr'=>$idIslr
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idIslr;
        }

    }




}//fin clase
