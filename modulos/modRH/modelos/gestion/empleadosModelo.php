<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
require_once ROOT. 'librerias/' . 'Fecha.php';
class empleadosModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    public function metListarOrganismos()
    {
        $con = $this->_db->query("
             SELECT
             pk_num_organismo,
             ind_descripcion_empresa
             FROM
             a001_organismo
             WHERE ind_tipo_organismo='I'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metJsonCentroCosto($dependencia)
    {

        $centros = $this->_db->query(
            "SELECT
              pk_num_centro_costo,
              ind_descripcion_centro_costo
            FROM
              a023_centro_costo
            WHERE
              fk_a004_num_dependencia ='$dependencia' AND num_estatus=1
            ");
        $centros->setFetchMode(PDO::FETCH_ASSOC);
        return $centros->fetchAll();

    }

    public function metJsonParroquia($municipio)
    {

        $centros = $this->_db->query(
            "SELECT
              pk_num_parroquia,
              ind_parroquia
            FROM
              a012_parroquia
            WHERE
              fk_a011_num_municipio ='$municipio' AND num_estatus=1
            ");
        $centros->setFetchMode(PDO::FETCH_ASSOC);
        return $centros->fetchAll();

    }

    public function metJsonSector($parroquia)
    {

        $centros = $this->_db->query(
            "SELECT
              pk_num_sector,
              ind_sector
            FROM
              a013_sector
            WHERE
              fk_a012_num_parroquia ='$parroquia' AND num_estatus=1
            ");
        $centros->setFetchMode(PDO::FETCH_ASSOC);
        return $centros->fetchAll();

    }

    //obtener sueldo basico
    public function metObtenerSueldoBasico($idCargo)
    {

        $con = $this->_db->query("
                SELECT
                rh_c063_puestos.num_sueldo_basico,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                paso.cod_detalle,
                rh_c063_puestos.fk_a006_num_miscelaneo_detalle_paso
                FROM
                rh_c063_puestos
                LEFT JOIN a006_miscelaneo_detalle ON (a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria)
                INNER JOIN a005_miscelaneo_maestro ON (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro = a005_miscelaneo_maestro.pk_num_miscelaneo_maestro and a005_miscelaneo_maestro.cod_maestro='CATCARGO')
                INNER JOIN a006_miscelaneo_detalle AS paso ON paso.pk_num_miscelaneo_detalle = rh_c063_puestos.fk_a006_num_miscelaneo_detalle_paso
                where rh_c063_puestos.pk_num_puestos='$idCargo'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();


    }

    #permite listar empleado para su revision o aprobacion
    public function metListarEmpleado($estatus_apro,$tipo,$estatus_empleado=false,$estatus_persona=false)
    {

        if($estatus_empleado && $estatus_persona){
            $where = "rh_b001_empleado.ind_estado_aprobacion='$estatus_apro' and rh_b001_empleado.num_estatus='$estatus_empleado' and a003_persona.num_estatus='$estatus_persona'";
        }else{
            $where = "rh_b001_empleado.ind_estado_aprobacion='$estatus_apro'";
        }

        if(strcmp($tipo,'operaciones')==0){

            $con = $this->_db->query("
                SELECT
                rh_b001_empleado.pk_num_empleado,
                rh_b001_empleado.fk_a003_num_persona,
                rh_b001_empleado.cod_empleado,
                a003_persona.ind_cedula_documento,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                CONCAT_WS(' ',a003_persona.ind_nombre1,a003_persona.ind_nombre2,a003_persona.ind_apellido1,a003_persona.ind_apellido2) AS nombre_empleado,
                rh_b001_empleado.ind_estado_aprobacion,
                rh_b001_empleado.num_estatus,
                a004_dependencia.ind_dependencia,
                rh_c063_puestos.ind_descripcion_cargo,
                a003_persona.ind_foto
                FROM
                rh_b001_empleado
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
                INNER JOIN a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE $where
              ");

            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();

        }

        if(strcmp($tipo,'listado')==0){

            #primero obtengo la dependencia del usuario actual de la session para armar la consulta
            $con = $this->_db->query("
                SELECT fk_a004_num_dependencia FROM vl_rh_usuario_empleado_dependencia where pk_num_seguridad_usuario='$this->atIdUsuario'
            ");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            $datos = $con->fetch();
            $dependencia = $datos['fk_a004_num_dependencia'];

            $list = $this->_db->query("
                SELECT
                rh_b001_empleado.pk_num_empleado,
                rh_b001_empleado.cod_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                CONCAT_WS(' ',a003_persona.ind_nombre1,a003_persona.ind_nombre2,a003_persona.ind_apellido1,a003_persona.ind_apellido2) AS nombre_empleado,
                rh_b001_empleado.ind_estado_aprobacion,
                rh_b001_empleado.num_estatus,
                a004_dependencia.ind_dependencia,
                rh_c063_puestos.ind_descripcion_cargo,
                a003_persona.ind_foto
                FROM
                rh_b001_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                WHERE $where and a004_dependencia.pk_num_dependencia='$dependencia'
              ");

            $list->setFetchMode(PDO::FETCH_ASSOC);
            return $list->fetchAll();

        }

    }

    public function metListarEmpleadoOrganizacion($organismo,$dependencia,$estatus_persona,$estatus_empleado)
    {

        $datosEmpleado = $this->_db->query(
            "SELECT
            rh_b001_empleado.pk_num_empleado,
            a003_persona.pk_num_persona,
            a001_organismo.pk_num_organismo,
            a001_organismo.ind_descripcion_empresa,
            a004_dependencia.pk_num_dependencia,
            a004_dependencia.ind_dependencia,
            rh_b001_empleado.num_estatus AS num_situacion_trabajo,
            a003_persona.num_estatus AS num_estado_registro,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) as nombre_completo
            FROM
            rh_b001_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
            INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
            AND a001_organismo.pk_num_organismo = $organismo
            AND a004_dependencia.pk_num_dependencia = $dependencia
            AND a003_persona.num_estatus = $estatus_persona
            AND rh_b001_empleado.num_estatus = $estatus_empleado

	        ");
         $datosEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $datosEmpleado->fetchAll();

    }

    public function metListarEmpleadoNomina($nomina)
    {

        $con = $this->_db->query("
                SELECT
                rh_b001_empleado.pk_num_empleado,
                rh_b001_empleado.fk_a003_num_persona,
                a003_persona.ind_cedula_documento,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_b001_empleado.ind_estado_aprobacion,
                rh_b001_empleado.num_estatus,
                a004_dependencia.ind_dependencia,
                rh_c063_puestos.ind_descripcion_cargo
                FROM
                rh_b001_empleado
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
                INNER JOIN a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE rh_b001_empleado.num_estatus='1' AND a003_persona.num_estatus AND rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina='$nomina'
              ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();

    }

    #permite consultar datos del empleado para mostrarlos en el form
    public function metMostrarEmpleado($parametro,$busqueda,$operacion,$sql=false)
    {

        #identifo el tipo bsuqueda para preparar las consultas
        if(strcmp($busqueda,'PorCedula')==0){
            $filtro = "WHERE ind_cedula_documento='$parametro'";
        }
        if(strcmp($busqueda,'PorIdPersona')==0){
            $filtro = "WHERE pk_num_persona='$parametro'";
        }
        if(strcmp($busqueda,'PorIdEmpleado')==0){
            $filtro = "WHERE pk_num_empleado='$parametro'";
        }
        if(isset($sql)){
            $adicional = $sql;
        }else{
            $adicional = '';
        }
        /*
         * MUESTRA LOS DATOS PRINCIPALES DEL EMPLEADO
         * VISTA: vl_rh_persona_empleado
         */
        if(strcmp($operacion,'PRINCIPALES')==0){
            $con = $this->_db->query("select * from vl_rh_persona_empleado $filtro");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
        }
        /*
         * MUESTRA LOS DATOS DE NACIMIENTO
         * VISTA: vl_rh_persona_empleado_nacimiento
         */
        if(strcmp($operacion,'NACIMIENTO')==0){
            $con = $this->_db->query("select * from vl_rh_persona_empleado_nacimiento $filtro");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
        }
        /*
         * MUESTRA LOS DATOS DE DOMICILIO
         * VISTA: vl_rh_persona_empleado_domicilio
         */
        if(strcmp($operacion,'DOMICILIO')==0){
            $con = $this->_db->query("select * from vl_rh_persona_empleado_domicilio $filtro");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
        }
        /*
        * MUESTRA LOS DATOS DE DOMICILIO
        * VISTA: vl_rh_persona_empleado_domicilio
        */
        if(strcmp($operacion,'DIRECCION')==0){
            $con = $this->_db->query("select * from vl_rh_persona_empleado_direccion $filtro");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();
        }
        /*
         * MUESTRA LOS DATOS LABORALES Y ORGANIZACION DEL EMPLEADO
         * VISTA: vl_rh_persona_empleado_datos_laborales
         */
        if(strcmp($operacion,'LABORALES')==0){
            $con = $this->_db->query("select * from vl_rh_persona_empleado_datos_laborales $filtro");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
        }
        /*
         * MUESTRA el SUELDO DEL EMPLEADO
         * VISTA: vl_rh_sueldo_empleado
         *        vl_rh_sueldo_empleado_paso
         */
        if(strcmp($operacion,'SUELDO')==0){

            $con = $this->_db->prepare("select * from vl_rh_sueldo_empleado $filtro $adicional");
            $con->execute();
            $registros = $con->rowCount();
            if($registros>0){
                return $con->fetch(PDO::FETCH_ASSOC);
            }else{
                $con1 = $this->_db->prepare("select * from vl_rh_sueldo_empleado_paso $filtro $adicional");
                $con1->execute();
                return $con1->fetch(PDO::FETCH_ASSOC);
            }


        }
        /*
         * MUESTRA LOS DATOS DE TELEFONO
         * VISTA: vl_rh_persona_empleado_telefono
         */
        if(strcmp($operacion,'TELEFONO')==0){
            $con = $this->_db->query("select * from vl_rh_persona_empleado_telefono $filtro");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();
        }
        /*
         * MUESTRA LOS DATOS REFERENCIAS DE EMERGENCIA DEL EMPLEADO
         * VISTA: vl_rh_persona_empleado_emergencia
         */
        if(strcmp($operacion,'EMERGENCIA')==0){
            $con = $this->_db->query("select * from vl_rh_persona_empleado_emergencia $filtro");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();
        }
        /*
        * MUESTRA LOS DATOS DE TELEFONO PERSONAS DE EMERGENCIA
        * VISTA: vl_rh_persona_empleado_emergencia
         */
        if(strcmp($operacion,'TELEFONO_EMERGENCIA')==0){
            $con = $this->_db->query("select * from vl_rh_persona_empleado_emergencia_telefono $filtro $adicional");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();
        }
        /*
        * MUESTRA LOS DATOS DE TELEFONO PERSONAS DE EMERGENCIA
        * VISTA: vl_rh_persona_empleado_emergencia
         */
        if(strcmp($operacion,'LICENCIA')==0){
            $con = $this->_db->query("select * from vl_rh_persona_empleado_licencia $filtro");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();
        }


    }
    /*
     * permite buscar a la persona por cedula o cod persona
     * $parametro = cedula, id persona, id empleado
     * $busqueda = ejm: PorCedula , PorIdPersona, PorIdEmpleado
     */
    public function metBuscarPersona($parametro,$busqueda)
    {
        #identifo el tipo bsuqueda para preparar las consultas
        if(strcmp($busqueda,'PorCedula')==0){
            $filtro = "WHERE ind_cedula_documento='$parametro'";
        }
        if(strcmp($busqueda,'PorIdPersona')==0){
            $filtro = "WHERE pk_num_persona='$parametro'";
        }
        if(strcmp($busqueda,'PorIdEmpleado')==0){
            $filtro = "WHERE pk_num_empleado='$parametro'";
        }

        $con = $this->_db->query("select * from vl_rh_persona_empleado $filtro");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }


    #METODO PARA REGISTRAR EMPLEADO
    public function metRegistrarEmpleado($datosIniciales,$nacimiento,$domicilio,$datos,$organizacion,$laborales)
    {

        #obtengo codigo persona para verificar el tipo de registro
        $PERSONA = $datosIniciales[1];
        $FOTO    = $datosIniciales[10];

        #si persona es nulo (REGISTRO LOS DATOS INCLUYENDO LA TABLA PERSONAS)
        if(!$PERSONA){

            #beginTransaction — Inicia una transacción
            $this->_db->beginTransaction();

            $tipoPersona = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='TIP_PERSON' AND
              a006.cod_detalle='EMP'
            ");
            $tipoPersona->setFetchMode(PDO::FETCH_ASSOC);
            $tp = $tipoPersona->fetch();

            //REGISTRO LA PERSONA
            $regPersona = $this->_db->prepare("
                 INSERT INTO
                      a003_persona
                 SET
                      ind_cedula_documento=:ind_cedula_documento,
                      ind_documento_fiscal=:ind_documento_fiscal,
                      ind_nombre1=:ind_nombre1,
                      ind_nombre2=:ind_nombre2,
                      ind_apellido1=:ind_apellido1,
                      ind_apellido2=:ind_apellido2,
                      fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
                      fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
                      fec_nacimiento=:fec_nacimiento,
                      ind_lugar_nacimiento=:ind_lugar_nacimiento,
                      fk_a006_num_miscelaneo_detalle_edocivil=:fk_a006_num_miscelaneo_detalle_edocivil,
                      ind_email=:ind_email,
                      ind_foto='foto.png',
                      ind_tipo_persona=:ind_tipo_persona,
                      num_estatus=:ind_estatus_persona,
                      fk_a006_num_miscelaneo_det_gruposangre=:fk_a006_num_miscelaneo_det_gruposangre,
                      fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona,
                      fk_a010_num_ciudad=:fk_a010_num_ciudad,
                      fk_a013_num_sector=:fk_a013_num_sector,
                      fec_ultima_modificacion=NOW(),
                      fk_a018_num_seguridad_usuario='$this->atIdUsuario'

            ");
            $fn = explode("-",$nacimiento[4]);
            #execute — Ejecuta una sentencia preparada
            $regPersona->execute(array(
                ':ind_cedula_documento' => $datosIniciales[0],
                ':ind_documento_fiscal' => $datosIniciales[8],
                ':ind_nombre1' => $datosIniciales[2],
                ':ind_nombre2' => $datosIniciales[3],
                ':ind_apellido1' => $datosIniciales[4],
                ':ind_apellido2' => $datosIniciales[5],
                ':fk_a006_num_miscelaneo_detalle_sexo' => $datosIniciales[6],
                ':fk_a006_num_miscelaneo_detalle_nacionalidad' => $datosIniciales[7],
                ':fec_nacimiento' => $fn[2]."-".$fn[1]."-".$fn[0],
                ':ind_lugar_nacimiento' => $nacimiento[5],
                ':fk_a006_num_miscelaneo_detalle_edocivil' => $datos[1],
                ':ind_email' => $datosIniciales[9],
                ':ind_tipo_persona' => 'N',
                ':ind_estatus_persona' => '1',
                ':fk_a006_num_miscelaneo_det_gruposangre' => $datos[0],
                ':fk_a006_num_miscelaneo_det_tipopersona' => $tp['pk_num_miscelaneo_detalle'],
                ':fk_a010_num_ciudad' => $nacimiento[3],
                ':fk_a013_num_sector' => $domicilio[5]
            ));

            $errorRegPersona = $regPersona->errorInfo();

            //obtengo id de la persona
            $idRegistroPersona= $this->_db->lastInsertId();

            if(!empty($errorRegPersona[1]) && !empty($errorRegPersona[2])){

                //en caso de error hago rollback
                $this->_db->rollBack();
                return $errorRegPersona;

            }else{
                  //REGISTRO AL EMPLEADO
                 //si el registro de personsa esta bien. Registro empleado
                 $registroEmpleado = $this->_db->prepare(
                    "INSERT INTO
                       rh_b001_empleado
                     SET
                       fk_a003_num_persona=:fk_a003_num_persona,
                       ind_estado_aprobacion=:ind_estado_aprobacion,
                       num_estatus=:num_estatus,
                       fec_preparado=:fec_preparado,
                       fk_rhb001_num_empleado_preparacion=:fk_rhb001_num_empleado_preparacion,
                       fec_ultima_modificacion=NOW(),
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

                 #execute — Ejecuta una sentencia preparada
                 $registroEmpleado->execute(array(
                    ':fk_a003_num_persona' => $idRegistroPersona,
                    ':ind_estado_aprobacion' => 'PR',
                    ':num_estatus' => '0',
                    ':fec_preparado' => date("Y-m-d"),
                    ':fk_rhb001_num_empleado_preparacion' => Session::metObtener('idEmpleado')
                 ));

                 $errorReg1 = $registroEmpleado->errorInfo();

                if(!empty($errorReg1[1]) && !empty($errorReg1[2])){
                    //en caso de error hago rollback
                    $this->_db->rollBack();
                    return $errorReg1;
                }else{
                    //obtengo id del empleado
                    $idRegistroEmpleado= $this->_db->lastInsertId();
                    //si el registro de empleado esta bien. registro datos de organizacion
                    $errorReg2 = $this->metRegistrarEmpOrganizacion($idRegistroEmpleado,$organizacion,'INSERT');

                    if(!empty($errorReg2[1]) && !empty($errorReg2[2])){

                        //en caso de error hago rollback
                        $this->_db->rollBack();
                        return $errorReg2;

                    }else{

                        //si el registro datos de organizacion esta bien. registro datos de laborales
                        $errorReg3 = $this->metRegistrarEmpLaboral($idRegistroEmpleado,$laborales,'INSERT');

                        if(!empty($errorReg3[1]) && !empty($errorReg3[2])){
                            //en caso de error hago rollback
                            $this->_db->rollBack();
                            return $errorReg3;

                        }else{

                            $errorReg4 = $this->metRegistrarEmpDomicilio($idRegistroPersona,$domicilio,'INSERT');

                            if(!empty($errorReg4[1]) && !empty($errorReg4[2])){
                                //en caso de error hago rollback
                                $this->_db->rollBack();
                                return $errorReg4;

                            }else{

                                $errorReg5 = $this->metRegistrarTelefono($idRegistroPersona,$domicilio,'INSERT');

                                if(!empty($errorReg5[1]) && !empty($errorReg5[2])){
                                    //en caso de error hago rollback
                                    $this->_db->rollBack();
                                    return $errorReg5;

                                }else{


                                    $errorReg6 = $this->metRegistrarEmpEmergencia($idRegistroEmpleado,$datos,'INSERT');

                                    if(!empty($errorReg6[1]) && !empty($errorReg6[2])){
                                        //en caso de error hago rollback
                                        $this->_db->rollBack();
                                        return $errorReg6;

                                    }else{

                                        $errorReg7 = $this->metRegistrarEmpLicencia($idRegistroPersona,$datos,'INSERT');

                                        if(!empty($errorReg7[1]) && !empty($errorReg7[2])){
                                            //en caso de error hago rollback
                                            $this->_db->rollBack();
                                            return $errorReg7;

                                        }else{
                                                  #verifico si existe una foto temporal, la renombro y la guardo
                                                  #verifico si existe una foto temporal, la renombro y la guardo
                                            if($FOTO){

                                                $ruta_tmp = "publico/imagenes/modRH/tmp/".$FOTO;
                                                $ruta     = "publico/imagenes/modRH/fotos/".$FOTO;
                                                $partes   = explode(".",$FOTO);

                                                if(file_exists($ruta_tmp)){

                                                    if(!copy($ruta_tmp,$ruta)){
                                                        echo "Error al copiar la foto";
                                                    }else{
                                                        chmod($ruta, 0777);
                                                        rename($ruta,"publico/imagenes/modRH/fotos/".$idRegistroPersona.".".$partes[1]);
                                                        unlink($ruta_tmp);

                                                        $actualizoFoto = $this->_db->prepare("
                                                                 UPDATE
                                                                      a003_persona
                                                                 SET
                                                                      ind_foto=:foto
                                                                 WHERE pk_num_persona='$idRegistroPersona'

                                                        ");
                                                        $actualizoFoto->execute(array(
                                                            ':foto' => $idRegistroPersona.".".$partes[1]
                                                        ));

                                                        $errorFoto = $actualizoFoto->errorInfo();

                                                        if(!empty($errorFoto[1]) && !empty($errorFoto[2])){
                                                            $this->_db->rollBack();
                                                            return $errorFoto;
                                                        }else{
                                                            $this->_db->commit();
                                                            return $idRegistroEmpleado;
                                                        }
                                                    }

                                                }

                                            }else{
                                                //en caso de exito hago commit de la operacion
                                                $this->_db->commit();
                                                return $idRegistroEmpleado;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #si persona existe(REGISTRO LOS DATOS EXCLUYENDO LA TABLA PERSONAS)
        else{


            #beginTransaction — Inicia una transacción
            $this->_db->beginTransaction();

            //obtengo el id de la persona
            $PERSONA = $datosIniciales[1];
            $FOTO    = $datosIniciales[10];

            if($nacimiento[3]==false){
                $nacimiento[3]=NULL;
            }else{
                $nacimiento[3]=$nacimiento[3];
            }

            if($domicilio[5]==false){
                $domicilio[5]=NULL;
            }else{
                $domicilio[5]=$domicilio[5];
            }

            $tipoPersona = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='TIP_PERSON' AND
              a006.cod_detalle='EMP'
            ");
            $tipoPersona->setFetchMode(PDO::FETCH_ASSOC);
            $tp = $tipoPersona->fetch();

            //ACTUALIZO LA PERSONA
            $modPersona = $this->_db->prepare("
                 UPDATE
                      a003_persona
                 SET
                      ind_cedula_documento=:ind_cedula_documento,
                      ind_documento_fiscal=:ind_documento_fiscal,
                      ind_nombre1=:ind_nombre1,
                      ind_nombre2=:ind_nombre2,
                      ind_apellido1=:ind_apellido1,
                      ind_apellido2=:ind_apellido2,
                      fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
                      fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
                      fec_nacimiento=:fec_nacimiento,
                      ind_lugar_nacimiento=:ind_lugar_nacimiento,
                      fk_a006_num_miscelaneo_detalle_edocivil=:fk_a006_num_miscelaneo_detalle_edocivil,
                      ind_email=:ind_email,
                      ind_tipo_persona=:ind_tipo_persona,
                      num_estatus=:ind_estatus_persona,
                      fk_a006_num_miscelaneo_det_gruposangre=:fk_a006_num_miscelaneo_det_gruposangre,
                      fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona,
                      fk_a010_num_ciudad=:fk_a010_num_ciudad,
                      fk_a013_num_sector=:fk_a013_num_sector,
                      fec_ultima_modificacion=NOW(),
                      fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_persona='$PERSONA'

            ");
            $fn = explode("-",$nacimiento[4]);
            #execute — Ejecuta una sentencia preparada
            $modPersona->execute(array(
                ':ind_cedula_documento' => $datosIniciales[0],
                ':ind_documento_fiscal' => $datosIniciales[8],
                ':ind_nombre1' => $datosIniciales[2],
                ':ind_nombre2' => $datosIniciales[3],
                ':ind_apellido1' => $datosIniciales[4],
                ':ind_apellido2' => $datosIniciales[5],
                ':fk_a006_num_miscelaneo_detalle_sexo' => $datosIniciales[6],
                ':fk_a006_num_miscelaneo_detalle_nacionalidad' => $datosIniciales[7],
                ':fec_nacimiento' => $fn[2]."-".$fn[1]."-".$fn[0],
                ':ind_lugar_nacimiento' => $nacimiento[5],
                ':fk_a006_num_miscelaneo_detalle_edocivil' => $datos[1],
                ':ind_email' => $datosIniciales[9],
                ':ind_tipo_persona' => 'N',
                ':ind_estatus_persona' => '1',
                ':fk_a006_num_miscelaneo_det_gruposangre' => $datos[0],
                ':fk_a006_num_miscelaneo_det_tipopersona' => $tp['pk_num_miscelaneo_detalle'],
                ':fk_a010_num_ciudad' => $nacimiento[3],
                ':fk_a013_num_sector' => $domicilio[5]
            ));

            $error = $modPersona->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{


                //REGISTRO AL EMPLEADO
                //si el registro de personsa esta bien. Registro empleado
                $registroEmpleado = $this->_db->prepare(
                    "INSERT INTO
                       rh_b001_empleado
                     SET
                       fk_a003_num_persona=:fk_a003_num_persona,
                       ind_estado_aprobacion=:ind_estado_aprobacion,
                       num_estatus=:num_estatus,
                       fec_preparado=:fec_preparado,
                       fk_rhb001_num_empleado_preparacion=:fk_rhb001_num_empleado_preparacion,
                       fec_ultima_modificacion=NOW(),
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

                #execute — Ejecuta una sentencia preparada
                $registroEmpleado->execute(array(
                    ':fk_a003_num_persona' => $PERSONA,
                    ':ind_estado_aprobacion' => 'PR',
                    ':num_estatus' => '0',
                    ':fec_preparado' => date("Y-m-d"),
                    ':fk_rhb001_num_empleado_preparacion' => Session::metObtener('idEmpleado')
                ));



                $errorReg1 = $registroEmpleado->errorInfo();

                if(!empty($errorReg1[1]) && !empty($errorReg1[2])){
                    //en caso de error hago rollback
                    $this->_db->rollBack();
                    return $errorReg1;
                }else{
                    //obtengo id del empleado
                    $idRegistroEmpleado= $this->_db->lastInsertId();
                    //si el registro de empleado esta bien. registro datos de organizacion
                    $errorReg2 = $this->metRegistrarEmpOrganizacion($idRegistroEmpleado,$organizacion,'INSERT');

                    if(!empty($errorReg2[1]) && !empty($errorReg2[2])){

                        //en caso de error hago rollback
                        $this->_db->rollBack();
                        return $errorReg2;

                    }else{

                        //si el registro datos de organizacion esta bien. registro datos de laborales
                        $errorReg3 = $this->metRegistrarEmpLaboral($idRegistroEmpleado,$laborales,'INSERT');

                        if(!empty($errorReg3[1]) && !empty($errorReg3[2])){
                            //en caso de error hago rollback
                            $this->_db->rollBack();
                            return $errorReg3;

                        }else{

                            $errorReg4 = $this->metRegistrarEmpDomicilio($PERSONA,$domicilio,'INSERT');

                            if(!empty($errorReg4[1]) && !empty($errorReg4[2])){
                                //en caso de error hago rollback
                                $this->_db->rollBack();
                                return $errorReg4;

                            }else{

                                $errorReg5 = $this->metRegistrarTelefono($PERSONA,$domicilio,'INSERT');

                                if(!empty($errorReg5[1]) && !empty($errorReg5[2])){
                                    //en caso de error hago rollback
                                    $this->_db->rollBack();
                                    return $errorReg5;

                                }else{


                                    $errorReg6 = $this->metRegistrarEmpEmergencia($idRegistroEmpleado,$datos,'INSERT');

                                    if(!empty($errorReg6[1]) && !empty($errorReg6[2])){
                                        //en caso de error hago rollback
                                        $this->_db->rollBack();
                                        return $errorReg6;

                                    }else{

                                        $errorReg7 = $this->metRegistrarEmpLicencia($PERSONA,$datos,'INSERT');

                                        if(!empty($errorReg7[1]) && !empty($errorReg7[2])){
                                            //en caso de error hago rollback
                                            $this->_db->rollBack();
                                            return $errorReg7;

                                        }else{


                                            $registroOperacion = $this->_db->prepare(
                                                "INSERT INTO
                                                                rh_c003_operaciones_empleado
                                                             SET
                                                                ind_estado=:txt_estatus,
                                                                fk_rhb001_num_empleado_registro=:fk_rhb001_num_empleado_registro,
                                                                fk_rhb001_num_empleado_operacion=:fk_rhb001_num_empleado_operacion,
                                                                num_flag_operacion=1,
                                                                fec_operacion=NOW()
                                                            ");

                                            #execute — Ejecuta una sentencia preparada
                                            $registroOperacion->execute(array(
                                                ':txt_estatus' => 'PR',
                                                ':fk_rhb001_num_empleado_registro' => $idRegistroEmpleado,
                                                ':fk_rhb001_num_empleado_operacion' => $this->atIdUsuario
                                            ));

                                            $errorRegistroOperacion = $registroOperacion->errorInfo();

                                            if(!empty($errorRegistroOperacion[1]) && !empty($errorRegistroOperacion[2])){
                                                //en caso de error hago rollback
                                                $this->_db->rollBack();
                                                return $errorRegistroOperacion;

                                            }else{

                                                #verifico si existe una foto temporal, la renombro y la guardo
                                                if($FOTO) {

                                                    $ruta_tmp = "publico/imagenes/modRH/tmp/" . $FOTO;
                                                    $ruta = "publico/imagenes/modRH/fotos/" . $FOTO;
                                                    $ruta_f = "publico/imagenes/modRH/fotos/".$PERSONA;
                                                    $partes = explode(".", $FOTO);


                                                    if (file_exists($ruta_f)){

                                                        unlink($ruta_f);

                                                    }else{

                                                        if (file_exists($ruta_tmp)) {

                                                            if (!copy($ruta_tmp, $ruta)) {
                                                                echo "Error al copiar la foto";
                                                            } else {
                                                                chmod($ruta, 0777);
                                                                rename($ruta, "publico/imagenes/modRH/fotos/" . $PERSONA . "." . $partes[1]);
                                                                unlink($ruta_tmp);

                                                                $actualizoFoto = $this->_db->prepare("
                                                                                 UPDATE
                                                                                      a003_persona
                                                                                 SET
                                                                                      ind_foto=:foto
                                                                                 WHERE pk_num_persona='$PERSONA'

                                                                        ");
                                                                $actualizoFoto->execute(array(
                                                                    ':foto' => $PERSONA . "." . $partes[1]
                                                                ));

                                                                $errorFoto = $actualizoFoto->errorInfo();

                                                                if (!empty($errorFoto[1]) && !empty($errorFoto[2])) {
                                                                    $this->_db->rollBack();
                                                                    return $errorFoto;
                                                                } else {
                                                                    $this->_db->commit();
                                                                    return $idRegistroEmpleado;
                                                                }
                                                            }

                                                        }

                                                    }

                                                }else{
                                                    //en caso de exito hago commit de la operacion
                                                    $this->_db->commit();
                                                    return $idRegistroEmpleado;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }//FIN REGISTRAR EMPLEADO


    #METODO PARA MODIFICAR EMPLEADO
    public function metModificarEmpleado($idEmpleado,$datosIniciales,$nacimiento,$domicilio,$datos,$organizacion,$laborales)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        //obtengo el id de la persona
        $PERSONA = $datosIniciales[1];
        $FOTO    = $datosIniciales[10];

        #CIUDAD DE NACIMIENTO
        if($nacimiento[3]==false){
            $nacimiento[3]=NULL;
        }else{
            $nacimiento[3]=$nacimiento[3];
        }

        #SECTOR DIRECCION DOMICILIO
        if($domicilio[5]==false){
            $domicilio[5]=NULL;
        }else{
            $domicilio[5]=$domicilio[5];
        }


            $tipoPersona = $this->_db->query("
                SELECT
                  a006.pk_num_miscelaneo_detalle
                FROM
                  a006_miscelaneo_detalle AS a006
                INNER JOIN
                  a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
                WHERE
                  a005.cod_maestro='TIP_PERSON' AND
                  a006.cod_detalle='EMP'
                ");
            $tipoPersona->setFetchMode(PDO::FETCH_ASSOC);
            $tp = $tipoPersona->fetch();


            //ACTUALIZO LA PERSONA
            $modPersona = $this->_db->prepare("
                 UPDATE
                      a003_persona
                 SET
                      ind_cedula_documento=:ind_cedula_documento,
                      ind_documento_fiscal=:ind_documento_fiscal,
                      ind_nombre1=:ind_nombre1,
                      ind_nombre2=:ind_nombre2,
                      ind_apellido1=:ind_apellido1,
                      ind_apellido2=:ind_apellido2,
                      fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
                      fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
                      fec_nacimiento=:fec_nacimiento,
                      ind_lugar_nacimiento=:ind_lugar_nacimiento,
                      fk_a006_num_miscelaneo_detalle_edocivil=:fk_a006_num_miscelaneo_detalle_edocivil,
                      ind_email=:ind_email,
                      ind_tipo_persona=:ind_tipo_persona,
                      num_estatus=:ind_estatus_persona,
                      fk_a006_num_miscelaneo_det_gruposangre=:fk_a006_num_miscelaneo_det_gruposangre,
                      fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona,
                      fk_a010_num_ciudad=:fk_a010_num_ciudad,
                      fk_a013_num_sector=:fk_a013_num_sector,
                      fec_ultima_modificacion=NOW(),
                      fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_persona='$PERSONA'

            ");
            $fn = explode("-",$nacimiento[4]);
            #execute — Ejecuta una sentencia preparada
            $modPersona->execute(array(
                ':ind_cedula_documento' => $datosIniciales[0],
                ':ind_documento_fiscal' => $datosIniciales[8],
                ':ind_nombre1' => $datosIniciales[2],
                ':ind_nombre2' => $datosIniciales[3],
                ':ind_apellido1' => $datosIniciales[4],
                ':ind_apellido2' => $datosIniciales[5],
                ':fk_a006_num_miscelaneo_detalle_sexo' => $datosIniciales[6],
                ':fk_a006_num_miscelaneo_detalle_nacionalidad' => $datosIniciales[7],
                ':fec_nacimiento' => $fn[2]."-".$fn[1]."-".$fn[0],
                ':ind_lugar_nacimiento' => $nacimiento[5],
                ':fk_a006_num_miscelaneo_detalle_edocivil' => $datos[1],
                ':ind_email' => $datosIniciales[9],
                ':ind_tipo_persona' => 'N',
                ':ind_estatus_persona' => '1',
                ':fk_a006_num_miscelaneo_det_gruposangre' => $datos[0],
                ':fk_a006_num_miscelaneo_det_tipopersona' => $tp['pk_num_miscelaneo_detalle'],
                ':fk_a010_num_ciudad' => $nacimiento[3],
                ':fk_a013_num_sector' => $domicilio[5]
            ));

            $error = $modPersona->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{

                $errorReg2 = $this->metRegistrarEmpOrganizacion($idEmpleado,$organizacion,'UPDATE');

                if(!empty($errorReg2[1]) && !empty($errorReg2[2])){

                    //en caso de error hago rollback
                    $this->_db->rollBack();
                    return $errorReg2;

                }else{

                    $errorReg3 = $this->metRegistrarEmpLaboral($idEmpleado,$laborales,'UPDATE');

                    if(!empty($errorReg3[1]) && !empty($errorReg3[2])){
                        //en caso de error hago rollback
                        $this->_db->rollBack();
                        return $errorReg3;

                    }else{

                        $errorReg4 = $this->metRegistrarEmpDomicilio($PERSONA,$domicilio,'UPDATE');

                        if(!empty($errorReg4[1]) && !empty($errorReg4[2])){
                            //en caso de error hago rollback
                            $this->_db->rollBack();
                            return $errorReg4;

                        }else{


                            $errorReg5 = $this->metRegistrarTelefono($PERSONA,$domicilio,'UPDATE');

                            if(!empty($errorReg5[1]) && !empty($errorReg5[2])){
                                //en caso de error hago rollback
                                $this->_db->rollBack();
                                return $errorReg5;

                            }else{

                                $errorReg6 = $this->metRegistrarEmpEmergencia($idEmpleado,$datos,'UPDATE');

                                if(!empty($errorReg6[1]) && !empty($errorReg6[2])){
                                    //en caso de error hago rollback
                                    $this->_db->rollBack();
                                    return $errorReg6;

                                }else {


                                    $errorReg7 = $this->metRegistrarEmpLicencia($PERSONA,$datos,'UPDATE');

                                    if(!empty($errorReg7[1]) && !empty($errorReg7[2])){
                                        //en caso de error hago rollback
                                        $this->_db->rollBack();
                                        return $errorReg7;

                                    }else{

                                        $historial_empleado = $this->_db->prepare(
                                            "INSERT INTO
                                            rh_c061_empleado_historial
                                            (fk_rhb001_num_empleado,
                                             ind_periodo,
                                             fec_ingreso,
                                             ind_organismo,
                                             ind_dependencia,
                                             ind_centro_costo,
                                             ind_cargo,
                                             num_nivel_salarial,
                                             ind_categoria_cargo,
                                             ind_tipo_nomina,
                                             ind_tipo_pago,
                                             num_estatus,
                                             -- ind_motivo_cese,
                                             -- fec_egreso,
                                             -- txt_obs_cese,
                                             ind_tipo_trabajador,
                                             fec_ultima_modificacion,
                                             fk_a018_num_seguridad_usuario)
                                        SELECT
                                            empleado.pk_num_empleado,
                                            DATE_FORMAT(NOW(),'%Y-%m') as ind_periodo,
                                            rh_c005_empleado_laboral.fec_ingreso,
                                            a001_organismo.ind_descripcion_empresa AS ind_organismo,
                                            a004_dependencia.ind_dependencia,
                                            a023_centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                            rh_c063_puestos.ind_descripcion_cargo AS ind_cargo,
                                            rh_c063_puestos.num_sueldo_basico AS ind_nivel_salarial,
                                            categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                            nm_b001_tipo_nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                            tipo_pago.ind_nombre_detalle AS ind_tipo_pago,
                                            empleado.num_estatus,
                                            tipo_trabajador.ind_nombre_detalle AS ind_tipo_trabajador,
                                            NOW() as fec_ultima_modificacion,
                                            '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                                        FROM
                                            rh_b001_empleado AS empleado
                                            INNER JOIN rh_c076_empleado_organizacion ON empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                                            INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                            INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                                            INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                                            INNER JOIN a023_centro_costo ON rh_c076_empleado_organizacion.fk_a023_num_centro_costo = a023_centro_costo.pk_num_centro_costo
                                            INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                                            INNER JOIN a006_miscelaneo_detalle AS categoria ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                            INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                                            INNER JOIN a006_miscelaneo_detalle AS tipo_pago ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipopago = tipo_pago.pk_num_miscelaneo_detalle
                                            INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                                     WHERE
                                            empleado.pk_num_empleado='$idEmpleado'

						                ");

                                        $historial_empleado->execute();

                                        $errorHistorialEmpleado = $historial_empleado->errorInfo();

                                        if(!empty($errorHistorialEmpleado[1]) && !empty($errorHistorialEmpleado[2])){
                                            //en caso de error hago rollback
                                            $this->_db->rollBack();
                                            return $errorHistorialEmpleado;
                                        }else {

                                            #verifico si existe una foto temporal, la renombro y la guardo
                                            if($FOTO) {

                                                $ruta_tmp = "publico/imagenes/modRH/tmp/" . $FOTO;
                                                $ruta = "publico/imagenes/modRH/fotos/" . $FOTO;
                                                $partes = explode(".", $FOTO);
                                                $ruta_f = "publico/imagenes/modRH/fotos/".$PERSONA;

                                                if (file_exists($ruta_f)){

                                                    unlink($ruta_f);

                                                }else{

                                                    if (file_exists($ruta_tmp)) {

                                                        if (!copy($ruta_tmp, $ruta)) {
                                                            echo "Error al copiar la foto";
                                                        } else {
                                                            chmod($ruta, 0777);
                                                            rename($ruta, "publico/imagenes/modRH/fotos/" . $PERSONA . "." . $partes[1]);
                                                            unlink($ruta_tmp);

                                                            $actualizoFoto = $this->_db->prepare("
                                                                                 UPDATE
                                                                                      a003_persona
                                                                                 SET
                                                                                      ind_foto=:foto
                                                                                 WHERE pk_num_persona='$PERSONA'

                                                                        ");
                                                            $actualizoFoto->execute(array(
                                                                ':foto' => $PERSONA . "." . $partes[1]
                                                            ));

                                                            $errorFoto = $actualizoFoto->errorInfo();

                                                            if (!empty($errorFoto[1]) && !empty($errorFoto[2])) {
                                                                $this->_db->rollBack();
                                                                return $errorFoto;
                                                            } else {
                                                                $this->_db->commit();
                                                                return $idEmpleado;
                                                            }
                                                        }

                                                    }

                                                }

                                            }else{
                                                //en caso de exito hago commit de la operacion
                                                $this->_db->commit();
                                                return $idEmpleado;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

    }


    #METODO PARA MODIFICAR EMPLEADO CUANDO ESTA EN ESTADO PREPARADO
    public function metModificarEmpleadoPreparado($idEmpleado,$datosIniciales,$nacimiento,$domicilio,$datos,$organizacion,$laborales)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        //obtengo el id de la persona
        $PERSONA = $datosIniciales[1];
        $FOTO    = $datosIniciales[10];

        #CIUDAD DE NACIMIENTO
        if($nacimiento[3]==false){
            $nacimiento[3]=NULL;
        }else{
            $nacimiento[3]=$nacimiento[3];
        }

        #SECTOR DIRECCION DOMICILIO
        if($domicilio[5]==false){
            $domicilio[5]=NULL;
        }else{
            $domicilio[5]=$domicilio[5];
        }

        $tipoPersona = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='TIP_PERSON' AND
              a006.cod_detalle='EMP'
            ");
        $tipoPersona->setFetchMode(PDO::FETCH_ASSOC);
        $tp = $tipoPersona->fetch();

        //ACTUALIZO LA PERSONA
        $modPersona = $this->_db->prepare("
                 UPDATE
                      a003_persona
                 SET
                      ind_cedula_documento=:ind_cedula_documento,
                      ind_documento_fiscal=:ind_documento_fiscal,
                      ind_nombre1=:ind_nombre1,
                      ind_nombre2=:ind_nombre2,
                      ind_apellido1=:ind_apellido1,
                      ind_apellido2=:ind_apellido2,
                      fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
                      fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
                      fec_nacimiento=:fec_nacimiento,
                      ind_lugar_nacimiento=:ind_lugar_nacimiento,
                      fk_a006_num_miscelaneo_detalle_edocivil=:fk_a006_num_miscelaneo_detalle_edocivil,
                      ind_email=:ind_email,
                      ind_tipo_persona=:ind_tipo_persona,
                      num_estatus=:ind_estatus_persona,
                      fk_a006_num_miscelaneo_det_gruposangre=:fk_a006_num_miscelaneo_det_gruposangre,
                      fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona,
                      fk_a010_num_ciudad=:fk_a010_num_ciudad,
                      fk_a013_num_sector=:fk_a013_num_sector,
                      fec_ultima_modificacion=NOW(),
                      fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_persona='$PERSONA'

            ");
        $fn = explode("-",$nacimiento[4]);
        #execute — Ejecuta una sentencia preparada
        $modPersona->execute(array(
            ':ind_cedula_documento' => $datosIniciales[0],
            ':ind_documento_fiscal' => $datosIniciales[8],
            ':ind_nombre1' => $datosIniciales[2],
            ':ind_nombre2' => $datosIniciales[3],
            ':ind_apellido1' => $datosIniciales[4],
            ':ind_apellido2' => $datosIniciales[5],
            ':fk_a006_num_miscelaneo_detalle_sexo' => $datosIniciales[6],
            ':fk_a006_num_miscelaneo_detalle_nacionalidad' => $datosIniciales[7],
            ':fec_nacimiento' => $fn[2]."-".$fn[1]."-".$fn[0],
            ':ind_lugar_nacimiento' => $nacimiento[5],
            ':fk_a006_num_miscelaneo_detalle_edocivil' => $datos[1],
            ':ind_email' => $datosIniciales[9],
            ':ind_tipo_persona' => 'N',
            ':ind_estatus_persona' => '1',
            ':fk_a006_num_miscelaneo_det_gruposangre' => $datos[0],
            ':fk_a006_num_miscelaneo_det_tipopersona' => $tp['pk_num_miscelaneo_detalle'],
            ':fk_a010_num_ciudad' => $nacimiento[3],
            ':fk_a013_num_sector' => $domicilio[5]
        ));

        $error = $modPersona->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            $errorReg2 = $this->metRegistrarEmpOrganizacion($idEmpleado,$organizacion,'UPDATE');

            if(!empty($errorReg2[1]) && !empty($errorReg2[2])){

                //en caso de error hago rollback
                $this->_db->rollBack();
                return $errorReg2;

            }else{

                $errorReg3 = $this->metRegistrarEmpLaboral($idEmpleado,$laborales,'UPDATE');

                if(!empty($errorReg3[1]) && !empty($errorReg3[2])){
                    //en caso de error hago rollback
                    $this->_db->rollBack();
                    return $errorReg3;

                }else{

                    $errorReg4 = $this->metRegistrarEmpDomicilio($PERSONA,$domicilio,'UPDATE');

                    if(!empty($errorReg4[1]) && !empty($errorReg4[2])){
                        //en caso de error hago rollback
                        $this->_db->rollBack();
                        return $errorReg4;

                    }else{


                        $errorReg5 = $this->metRegistrarTelefono($PERSONA,$domicilio,'UPDATE');

                        if(!empty($errorReg5[1]) && !empty($errorReg5[2])){
                            //en caso de error hago rollback
                            $this->_db->rollBack();
                            return $errorReg5;

                        }else{

                            $errorReg6 = $this->metRegistrarEmpEmergencia($idEmpleado,$datos,'UPDATE');

                            if(!empty($errorReg6[1]) && !empty($errorReg6[2])){
                                //en caso de error hago rollback
                                $this->_db->rollBack();
                                return $errorReg6;

                            }else {

                                $errorReg7 = $this->metRegistrarEmpLicencia($PERSONA,$datos,'UPDATE');

                                if(!empty($errorReg7[1]) && !empty($errorReg7[2])){
                                    //en caso de error hago rollback
                                    $this->_db->rollBack();
                                    return $errorReg7;

                                }else{
                                    #verifico si existe una foto temporal, la renombro y la guardo
                                    if($FOTO) {

                                        $ruta_tmp = "publico/imagenes/modRH/tmp/" . $FOTO;
                                        $ruta = "publico/imagenes/modRH/fotos/" . $FOTO;
                                        $partes = explode(".", $FOTO);
                                        $ruta_f = "publico/imagenes/modRH/fotos/".$PERSONA;

                                        if (file_exists($ruta_f)){

                                            unlink($ruta_f);

                                        }else{

                                            if (file_exists($ruta_tmp)) {

                                                if (!copy($ruta_tmp, $ruta)) {
                                                    echo "Error al copiar la foto";
                                                } else {
                                                    chmod($ruta, 0777);
                                                    rename($ruta, "publico/imagenes/modRH/fotos/" . $PERSONA . "." . $partes[1]);
                                                    unlink($ruta_tmp);

                                                    $actualizoFoto = $this->_db->prepare("
                                                                             UPDATE
                                                                                  a003_persona
                                                                             SET
                                                                                  ind_foto=:foto
                                                                             WHERE pk_num_persona='$PERSONA'

                                                                    ");
                                                    $actualizoFoto->execute(array(
                                                        ':foto' => $PERSONA . "." . $partes[1]
                                                    ));

                                                    $errorFoto = $actualizoFoto->errorInfo();

                                                    if (!empty($errorFoto[1]) && !empty($errorFoto[2])) {
                                                        $this->_db->rollBack();
                                                        return $errorFoto;
                                                    } else {
                                                        $this->_db->commit();
                                                        return $idEmpleado;
                                                    }
                                                }

                                            }

                                        }

                                    }else{
                                        //en caso de exito hago commit de la operacion
                                        $this->_db->commit();
                                        return $idEmpleado;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }



    #permite registrar la operaciones y cambiar de estatus al empleado:
    # RV: cuando se revisa un empleado
    # AP: cuando se aprueba un empleado
    public function metOperacionesEmpleado($idEmpleado,$operacion,$datos)
    {

        $this->_db->beginTransaction();

        #para nivelacion del empleado cuando aprueban el registro
        $fe  = explode("-",$datos[0]);#fecha ingreso
        $res = $datos[1];#resolucion ingreso
        $org = $datos[2];#organismo
        $dep = $datos[3];#dependencia
        $cc  = $datos[4];#centro de costo
        $nom = $datos[5];#nomina
        $car = $datos[6];#cargo
        $paso = $datos[7];#paso



        #OPERACION REVISAR
        if(strcmp($operacion,'revisar')==0){

            $registroOperacion = $this->_db->prepare(
                "UPDATE
                   rh_b001_empleado
                SET
                   ind_estado_aprobacion=:ind_estado_aprobacion,
                   fec_revisado=:fec_revisado,
                   fk_rhb001_num_empleado_revision=:fk_rhb001_num_empleado_revision,
                   fec_ultima_modificacion=NOW(),
                   fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                WHERE pk_num_empleado='$idEmpleado'
            "
            );

            #execute — Ejecuta una sentencia preparada
            $registroOperacion->execute(array(
                ':ind_estado_aprobacion' => 'RV',
                ':fec_revisado' => date("Y-m-d"),
                'fk_rhb001_num_empleado_revision' => Session::metObtener('idEmpleado')
            ));

            $error = $registroOperacion->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idEmpleado;
            }
        }//FIN REVISAR

        #OPERACION APROBAR
        if(strcmp($operacion,'aprobar')==0){

            $registroOperacion = $this->_db->prepare(
                "UPDATE
                   rh_b001_empleado
                 SET
                    cod_empleado=:cod_empleado,
                    ind_estado_aprobacion=:ind_estado_aprobacion,
                    num_estatus=1,
                    fec_aprobado=:fec_aprobado,
                    fk_rhb001_num_empleado_aprobado=:fk_rhb001_num_empleado_aprobado,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_empleado='$idEmpleado'
            "
            );
            #execute — Ejecuta una sentencia preparada
            $registroOperacion->execute(array(
                ':ind_estado_aprobacion' => 'AP',
                ':fec_aprobado' => date ("Y-m-d"),
                ':fk_rhb001_num_empleado_aprobado' => Session::metObtener('idEmpleado'),
                ':cod_empleado' => $this->metCodigoEmpleado()
            ));

            $error = $registroOperacion->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{


                #registro nivelacion, nivelacion historial, historial

                #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
                $nivelacion = $this->_db->prepare(
                    "INSERT INTO
                        rh_c059_empleado_nivelacion
                     SET
                        fk_rhb001_num_empleado='$idEmpleado',
                        fec_fecha_registro=:fec_fecha_registro,
                        fk_a001_num_organismo=:fk_a001_num_organismo,
                        fk_a004_num_dependencia=:fk_a004_num_dependencia,
                        fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                        fk_rhc063_num_puestos=:fk_rhc063_num_puestos,
                        fk_a006_num_miscelaneo_detalle_paso=:fk_a006_num_miscelaneo_detalle_paso,
                        fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                        ind_documento=:ind_documento,
                        num_estatus=:num_estatus,
                        fec_fecha_hasta=:fec_fecha_hasta,
                        txt_observaciones=:txt_observaciones,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                      ");

                #execute — Ejecuta una sentencia preparada
                $nivelacion->execute(array(
                    ':fec_fecha_registro' => $fe[2]."-".$fe[1]."-".$fe[0],
                    ':fk_a001_num_organismo'  => $org,
                    ':fk_a004_num_dependencia'  => $dep,
                    ':fk_a023_num_centro_costo'  => $cc,
                    ':fk_rhc063_num_puestos'  => $car,
                    ':fk_a006_num_miscelaneo_detalle_paso' => $paso,
                    ':fk_nmb001_num_tipo_nomina' => $nom,
                    ':ind_documento' => $res,
                    ':num_estatus' => 1,
                    ':fec_fecha_hasta' => NULL,
                    ':txt_observaciones' => 'NUEVO INGRESO'
                ));
                $errorNivelacion = $nivelacion->errorInfo();
                $id_nivelacion = $this->_db->lastInsertId();

                if(!empty($errorNivelacion[1]) && !empty($errorNivelacion[2])){
                    //en caso de error hago rollback
                    $this->_db->rollBack();
                    return $errorNivelacion;
                }else {

                    #registro nivelacion historial
                    $historial_nivelacion = $this->_db->prepare(
                        "INSERT INTO
                            rh_c060_empleado_nivelacion_historial
                            (fk_rhc059_num_empleado_nivelacion,
                             fec_fecha_registro,
                             ind_organismo,
                             ind_dependencia,
                             ind_cargo,
                             num_nivel_salarial,
                             ind_categoria_cargo,
                             ind_tipo_nomina,
                             ind_centro_costo,
                             ind_tipo_accion,
                             num_estatus,
                             fec_ultima_modificacion,
                             fk_a018_num_seguridad_usuario)
                        SELECT
                            rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion,
                            rh_c059_empleado_nivelacion.fec_fecha_registro,
                            organismo.ind_descripcion_empresa AS ind_organismo,
                            dependencia.ind_dependencia AS ind_dependencia,
                            cargo.ind_descripcion_cargo AS ind_cargo,
                            cargo.num_sueldo_basico AS num_nivel_salarial,
                            categoria.ind_nombre_detalle AS ind_categoria_cargo,
                            nomina.ind_nombre_nomina AS ind_tipo_nomina,
                            centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                            tipo_accion.ind_nombre_detalle AS ind_tipo_accion,
                            rh_c059_empleado_nivelacion.num_estatus,
                            NOW() as fec_ultima_modificacion,
                            '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                        FROM
                            rh_c059_empleado_nivelacion
                        INNER JOIN a001_organismo AS organismo ON organismo.pk_num_organismo = rh_c059_empleado_nivelacion.fk_a001_num_organismo
                        INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = rh_c059_empleado_nivelacion.fk_a004_num_dependencia
                        INNER JOIN a023_centro_costo AS centro_costo ON centro_costo.pk_num_centro_costo = rh_c059_empleado_nivelacion.fk_a023_num_centro_costo
                        INNER JOIN rh_c063_puestos AS cargo ON cargo.pk_num_puestos = rh_c059_empleado_nivelacion.fk_rhc063_num_puestos
                        INNER JOIN nm_b001_tipo_nomina AS nomina ON nomina.pk_num_tipo_nomina = rh_c059_empleado_nivelacion.fk_nmb001_num_tipo_nomina
                        LEFT JOIN a006_miscelaneo_detalle AS tipo_accion ON tipo_accion.pk_num_miscelaneo_detalle = rh_c059_empleado_nivelacion.fk_a006_num_miscelaneo_detalle_tipoaccion
                        INNER JOIN a006_miscelaneo_detalle AS categoria ON cargo.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                        WHERE rh_c059_empleado_nivelacion.fk_rhb001_num_empleado='$idEmpleado'
                        AND rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion='$id_nivelacion'
                          ");

                    $historial_nivelacion->execute();

                    $errorHistorialNivelacion = $historial_nivelacion->errorInfo();

                    if(!empty($errorHistorialNivelacion[1]) && !empty($errorHistorialNivelacion[2])){
                        //en caso de error hago rollback
                        $this->_db->rollBack();
                        return $errorHistorialNivelacion;
                    }else {

                        #registro historial del empleado
                        $historial_empleado = $this->_db->prepare(
                            "INSERT INTO
                                    rh_c061_empleado_historial
                                    (fk_rhb001_num_empleado,
                                     ind_periodo,
                                     fec_ingreso,
                                     ind_organismo,
                                     ind_dependencia,
                                     ind_centro_costo,
                                     ind_cargo,
                                     num_nivel_salarial,
                                     ind_categoria_cargo,
                                     ind_tipo_nomina,
                                     ind_tipo_pago,
                                     num_estatus,
                                     -- ind_motivo_cese,
                                     -- fec_egreso,
                                     -- txt_obs_cese,
                                     ind_tipo_trabajador,
                                     fec_ultima_modificacion,
                                     fk_a018_num_seguridad_usuario)
                                SELECT
                                    empleado.pk_num_empleado,
                                    DATE_FORMAT(NOW(),'%Y-%m') as ind_periodo,
                                    rh_c005_empleado_laboral.fec_ingreso,
                                    a001_organismo.ind_descripcion_empresa AS ind_organismo,
                                    a004_dependencia.ind_dependencia,
                                    a023_centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                    rh_c063_puestos.ind_descripcion_cargo AS ind_cargo,
                                    rh_c063_puestos.num_sueldo_basico AS ind_nivel_salarial,
                                    categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                    nm_b001_tipo_nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                    tipo_pago.ind_nombre_detalle AS ind_tipo_pago,
                                    empleado.num_estatus,
                                    tipo_trabajador.ind_nombre_detalle AS ind_tipo_trabajador,
                                    NOW() as fec_ultima_modificacion,
                                    '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                                FROM
                                    rh_b001_empleado AS empleado
                                    INNER JOIN rh_c076_empleado_organizacion ON empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                                    INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                    INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                                    INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                                    INNER JOIN a023_centro_costo ON rh_c076_empleado_organizacion.fk_a023_num_centro_costo = a023_centro_costo.pk_num_centro_costo
                                    INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                                    INNER JOIN a006_miscelaneo_detalle AS categoria ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                    INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                                    INNER JOIN a006_miscelaneo_detalle AS tipo_pago ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipopago = tipo_pago.pk_num_miscelaneo_detalle
                                    INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                             WHERE
                                    empleado.pk_num_empleado='$idEmpleado'

                        ");

                        $historial_empleado->execute();

                        $errorHistorialEmpleado = $historial_empleado->errorInfo();

                        if(!empty($errorHistorialEmpleado[1]) && !empty($errorHistorialEmpleado[2])){
                            //en caso de error hago rollback
                            $this->_db->rollBack();
                            return $errorHistorialEmpleado;
                        }else {

                            $lfecha = new Fecha();
                            #obtengo la fecha de ingreso del empleado
                            $con1 = $this->_db->query(
                                "SELECT
                                    rh_c005_empleado_laboral.fec_ingreso,
                                    rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina
                                FROM
                                    rh_b001_empleado
                                INNER JOIN rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado
                                INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado where pk_num_empleado='$idEmpleado'
                                "
                            );
                            $datos1 = $con1->fetch();
                            $fecha  = explode("-",$datos1['fec_ingreso']);
                            $fechaAMD = $datos1['fec_ingreso'];
                            $tipo_nom = $datos1['fk_nmb001_num_tipo_nomina'];
                            $DiaIngreso = $fecha[2];
                            $MesIngreso = $fecha[1];
                            $AnioIngreso= $fecha[0];

                            ## bono de alimentacion
                            ## verifico bono de alimentacion si esta creado y hago el corte del nuevo ingreso
                            $resultado = $this->_db->query(
                                "select * from rh_c019_beneficio_alimentacion where fec_anio='$AnioIngreso' and  fk_nmb001_num_tipo_nomina='$tipo_nom' and '$fechaAMD' >= fec_inicio_periodo and '$fechaAMD'  <= fec_fin_periodo"
                            );

                            $datos2 = $resultado->fetch();

                            if($datos2){

                                #valor diario
                                $valor_diario = $datos2['num_valor_diario'];
                                $dias_inactivos = 0;
                                $dia_semana = $lfecha->dia_semana(date("d-m-Y", strtotime($datos2['fec_inicio_periodo'])));
                                $fecha = date("d-m-Y", strtotime($datos2['fec_inicio_periodo']));
                                $inicio = $lfecha->dias_fecha($datos2['fec_inicio_periodo'],$datos1['fec_ingreso']);

                                //	obtengo la leyenda de los dias
                                for ($i=1; $i<=$datos2['num_dias_periodo']; $i++) {

                                    if ($dia_semana == 7) $dia_semana = 0;
                                    if ($dia_semana >= 1 && $dia_semana <= 5) {
                                        if ($i <= ($inicio)) { $l = "IN"; $dias_inactivos++; }
                                        elseif ($this->metObtenerDiasFeriados($fecha, $fecha) > 0) $l = "F";
                                        else $l = "X";
                                    }
                                    elseif ($dia_semana == 0 || $dia_semana == 6) {
                                        $l = "IN"; $dias_inactivos++;
                                    }

                                    $_Dia[$i] = $l;
                                    $dia_semana++;

                                    $conf = $this->_db->query("
                                        SELECT ADDDATE('".date("Y-m-d", strtotime($fecha))."', ".intval(1).") AS FechaResultado
                                    ");
                                    $datos3 = $conf->fetch();
                                    $fecha  = date("d-m-Y", strtotime($datos3['FechaResultado']));

                                }

                                $dias_pago   = $datos2['num_dias_periodo'] - $dias_inactivos;
                                $valor_pagar = $datos2['num_valor_diario'] * $dias_pago;

                                if(isset($_Dia[28])){
                                    $_Dia[28]=$_Dia[28];
                                }else{$_Dia[28]=NULL;}
                                if(isset($_Dia[29])){
                                    $_Dia[29]=$_Dia[29];
                                }else{$_Dia[29]=NULL;}
                                if(isset($_Dia[30])){
                                    $_Dia[30]=$_Dia[30];
                                }else{$_Dia[30]=NULL;}
                                if(isset($_Dia[31])){
                                    $_Dia[31]=$_Dia[31];
                                }else{$_Dia[31]=NULL;}

                                $NuevoRegistroDetalle = $this->_db->prepare(
                                    "INSERT INTO
                                            rh_c039_beneficio_alimentacion_detalle
                                         SET
                                            fk_a001_num_organismo=:fk_a001_num_organismo,
                                            fk_rhc019_num_beneficio=:fk_rhc019_num_beneficio,
                                            fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                                            fec_anio=:fec_anio,
                                            num_dia1=:num_dia1,
                                            num_dia2=:num_dia3,
                                            num_dia3=:num_dia3,
                                            num_dia4=:num_dia4,
                                            num_dia5=:num_dia5,
                                            num_dia6=:num_dia6,
                                            num_dia7=:num_dia7,
                                            num_dia8=:num_dia8,
                                            num_dia9=:num_dia9,
                                            num_dia10=:num_dia10,
                                            num_dia11=:num_dia11,
                                            num_dia12=:num_dia12,
                                            num_dia13=:num_dia13,
                                            num_dia14=:num_dia14,
                                            num_dia15=:num_dia15,
                                            num_dia16=:num_dia16,
                                            num_dia17=:num_dia17,
                                            num_dia18=:num_dia18,
                                            num_dia19=:num_dia19,
                                            num_dia20=:num_dia20,
                                            num_dia21=:num_dia21,
                                            num_dia22=:num_dia22,
                                            num_dia23=:num_dia23,
                                            num_dia24=:num_dia24,
                                            num_dia25=:num_dia25,
                                            num_dia26=:num_dia26,
                                            num_dia27=:num_dia27,
                                            num_dia28=:num_dia28,
                                            num_dia29=:num_dia29,
                                            num_dia30=:num_dia30,
                                            num_dia31=:num_dia31,
                                            num_dias_periodo=:num_dias_periodo,
                                            num_dias_pago=:num_dias_pago,
                                            num_dias_feriados=:num_dias_feriados,
                                            num_dias_inactivos=:num_dias_inactivos,
                                            num_dias_descuento=:num_dias_descuento,
                                            num_valor_pago=:num_valor_pago,
                                            num_valor_descuento=:num_valor_descuento,
                                            num_valor_total=:num_valor_total,
                                            ind_estado=1,
                                            fec_ultima_modificacion=NOW(),
                                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                                    ");
                                #execute — Ejecuta una sentencia preparada
                                $NuevoRegistroDetalle->execute(array(
                                    ':fk_a001_num_organismo'  => $datos2['fk_a001_num_organismo'],
                                    ':fk_rhc019_num_beneficio'  => $datos2['pk_num_beneficio'],
                                    ':fk_rhb001_num_empleado' => $idEmpleado,
                                    ':fec_anio'  => $datos2['fec_anio'],
                                    ':num_dia1'  => $_Dia[1],
                                    ':num_dia2'  => $_Dia[2],
                                    ':num_dia3'  => $_Dia[3],
                                    ':num_dia4'  => $_Dia[4],
                                    ':num_dia5'  => $_Dia[5],
                                    ':num_dia6'  => $_Dia[6],
                                    ':num_dia7'  => $_Dia[7],
                                    ':num_dia8'  => $_Dia[8],
                                    ':num_dia9'  => $_Dia[9],
                                    ':num_dia10'  => $_Dia[10],
                                    ':num_dia11'  => $_Dia[11],
                                    ':num_dia12'  => $_Dia[12],
                                    ':num_dia13'  => $_Dia[13],
                                    ':num_dia14'  => $_Dia[14],
                                    ':num_dia15'  => $_Dia[15],
                                    ':num_dia16'  => $_Dia[16],
                                    ':num_dia17'  => $_Dia[17],
                                    ':num_dia18'  => $_Dia[18],
                                    ':num_dia19'  => $_Dia[19],
                                    ':num_dia20'  => $_Dia[20],
                                    ':num_dia21'  => $_Dia[21],
                                    ':num_dia22'  => $_Dia[22],
                                    ':num_dia23'  => $_Dia[23],
                                    ':num_dia24'  => $_Dia[24],
                                    ':num_dia25'  => $_Dia[25],
                                    ':num_dia26'  => $_Dia[26],
                                    ':num_dia27'  => $_Dia[27],
                                    ':num_dia28'  => $_Dia[28],
                                    ':num_dia29'  => $_Dia[29],
                                    ':num_dia30'  => $_Dia[30],
                                    ':num_dia31'  => $_Dia[31],
                                    ':num_dias_periodo' => $datos2['num_dias_periodo'],
                                    ':num_dias_pago' => $dias_pago,
                                    ':num_dias_feriados'  => $datos2['num_total_feriados'],
                                    ':num_dias_inactivos'  => $dias_inactivos,
                                    ':num_dias_descuento'  => 0,
                                    ':num_valor_pago'  => $valor_pagar,
                                    ':num_valor_descuento'  => 0,
                                    ':num_valor_total'  => $valor_pagar
                                ));

                                $errorRegBonoAlimentacionDet = $NuevoRegistroDetalle->errorInfo();

                                if(!empty($errorRegBonoAlimentacionDet[1]) && !empty($errorRegBonoAlimentacionDet[2])){
                                    //en caso de error hago rollback
                                    $this->_db->rollBack();
                                    return $errorRegBonoAlimentacionDet;
                                }else{
                                    $this->_db->commit();
                                    return $idEmpleado;
                                }

                            }else{
                                //no hay beneficio bono alimentacion creado
                                $this->_db->commit();
                                return $idEmpleado;
                            }

                        }

                    }

                }

            }

        }//FIN APROBAR

    }

    #PERMITE OBTENER DIAS FERIADOS SEGUN RANGO DE FECHAS
    public function metObtenerDiasFeriados($fecha_ini,$fecha_fin)
    {
        $lfecha = new Fecha();

        list($dia_desde, $mes_desde, $anio_desde) = explode('-', $fecha_ini); $DiaDesde = "$mes_desde-$dia_desde";
        list($dia_hasta, $mes_hasta, $anio_hasta) = explode('-', $fecha_fin); $DiaHasta = "$mes_hasta-$dia_hasta";

        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    rh_c069_feriados
                WHERE
                    (num_flag_variable = 1 AND
				    (fec_anio = '$anio_desde' OR fec_anio = '$anio_hasta') AND
				    (fec_dia >= '$DiaDesde' AND fec_dia <= '$DiaHasta')) OR
				    (num_flag_variable = 0 AND fec_dia >= '$DiaDesde' AND fec_dia <= '$DiaHasta')
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $data = $con->fetchAll();

        $dias_feriados=0;

        for($i=0;$i<count($data);$i++){

            list($mes, $dia) = explode('-', $data[$i]['fec_dia']);

            if ($data[$i]['fec_anio'] == "") $anio = $anio_desde; else $anio = $data[$i]['fec_anio'];

            $fecha = "$dia-$mes-$anio";

            $dia_semana = $lfecha->dia_semana($fecha);

            if ($dia_semana >= 1 && $dia_semana <= 5) $dias_feriados++;

            if ($anio_desde != $anio_hasta) {

                if ($data[$i]['fec_anio'] == "") $anio = $anio_hasta; else $anio = $data[$i]['fec_anio'];

                $fecha = "$dia-$mes-$anio";

                $dia_semana = $lfecha->dia_semana($fecha);

                if ($dia_semana >= 1 && $dia_semana <= 5) $dias_feriados++;

            }

        }

        return $dias_feriados;

    }


    protected function metRegistrarEmpLicencia($persona,$datos,$operacion)
    {


        $tipo_lic = $datos[18];
        $tipo_cla = $datos[19];
        $fecha_exp = $datos[20];


        if(strcmp($operacion,'INSERT')==0){//para insertar

            if($tipo_lic!=0){

                $registroLicencia = $this->_db->prepare(
                    "INSERT INTO
                        a032_persona_licencia
                     SET
                        fk_a003_num_persona=:fk_a003_num_persona,
                        fk_a006_num_miscelaneo_detalle_tipolic=:fk_a006_num_miscelaneo_detalle_tipolic,
                        fk_a006_num_miscelaneo_detalle_claselic=:fk_a006_num_miscelaneo_detalle_claselic,
                        fec_expiracion_licencia=:fec_expiracion_licencia
                    "
                 );

                for ($i = 0; $i < count($tipo_lic); $i++) {

                    $fn = explode("-",$fecha_exp[$i]);
                    #execute — Ejecuta una sentencia preparada
                    $registroLicencia->execute(array(
                        ':fk_a003_num_persona' => $persona,
                        ':fk_a006_num_miscelaneo_detalle_tipolic' => $tipo_lic[$i],
                        ':fk_a006_num_miscelaneo_detalle_claselic' => $tipo_cla[$i],
                        ':fec_expiracion_licencia' => $fn[2]."-".$fn[1]."-".$fn[0],
                    ));
                }

                $errorReg7 = $registroLicencia->errorInfo();



            }else{
                $errorReg7='';
            }

            return $errorReg7;


        }else{//para modificar


            $idLicencia = $datos[30];

            if($tipo_lic!=0){

                for ($i = 0; $i < count($tipo_lic); $i++) {

                    if (isset($idLicencia[$i])){//actualizo licencia


                        $registroLicencia = $this->_db->prepare(
                            "UPDATE
                                a032_persona_licencia
                             SET
                                fk_a006_num_miscelaneo_detalle_tipolic=:fk_a006_num_miscelaneo_detalle_tipolic,
                                fk_a006_num_miscelaneo_detalle_claselic=:fk_a006_num_miscelaneo_detalle_claselic,
                                fec_expiracion_licencia=:fec_expiracion_licencia
                             WHERE fk_a003_num_persona='$persona' and pk_num_licencia='$idLicencia[$i]'
                            "
                        );


                    }else{//inserto licencia

                        $registroLicencia = $this->_db->prepare(
                            "INSERT INTO
                                a032_persona_licencia
                             SET
                                fk_a003_num_persona='$persona',
                                fk_a006_num_miscelaneo_detalle_tipolic=:fk_a006_num_miscelaneo_detalle_tipolic,
                                fk_a006_num_miscelaneo_detalle_claselic=:fk_a006_num_miscelaneo_detalle_claselic,
                                fec_expiracion_licencia=:fec_expiracion_licencia
                            "
                        );

                    }

                    $fn = explode("-",$fecha_exp[$i]);
                    #execute — Ejecuta una sentencia preparada
                    $registroLicencia->execute(array(
                        ':fk_a006_num_miscelaneo_detalle_tipolic' => $tipo_lic[$i],
                        ':fk_a006_num_miscelaneo_detalle_claselic' => $tipo_cla[$i],
                        ':fec_expiracion_licencia' => $fn[2]."-".$fn[1]."-".$fn[0]
                    ));

                    $errorReg7 = $registroLicencia->errorInfo();


                }

            }else{
                $errorReg7='';
            }

            return $errorReg7;



        }




    }

    protected function metRegistrarEmpDomicilio($persona,$domicilio,$operacion)
    {
        $tipo_dir  = $domicilio[6];
        $direccion = $domicilio[7];
        $situ_domicilio = $domicilio[8];
        $ID_TIPO_ZONA = $domicilio[13];
        $ID_UBI_INMUEBLE = $domicilio[14];
        $IND_UBI_INMUEBLE = $domicilio[15];
        $ID_TIPO_INMUEBLE = $domicilio[16];
        $IND_TIPO_INMUEBLE = $domicilio[17];
        $ID_ZONA_RESIDENCIAL = $domicilio[18];
        $IND_ZONA_RESIDENCIAL = $domicilio[19];
        $IND_INMUEBLE = $domicilio[20];
        $PUNTO_REF = $domicilio[21];
        $ZONA_POSTAL = $domicilio[22];

        if(strcmp($operacion,'INSERT')==0){

            if($tipo_dir!=0){


                $registro = $this->_db->prepare(
                    "INSERT INTO
                        a036_persona_direccion
                     SET
                        fk_a003_num_persona=:fk_a003_num_persona,
                        fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                        fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                        fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                        ind_zona_residencial=:ind_zona_residencial,
                        fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                        ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                        fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                        ind_tipo_inmueble=:ind_tipo_inmueble,
                        fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                        ind_inmueble=:ind_inmueble,
                        ind_punto_referencia=:ind_punto_referencia,
                        ind_zona_postal=:ind_zona_postal,
                        ind_direccion=:ind_direccion
                    "
                    );
                for ($i = 0; $i < count($tipo_dir); $i++) {

                    #execute — Ejecuta una sentencia preparada
                    $registro->execute(array(
                        ':fk_a003_num_persona' => $persona,
                        ':fk_a006_num_miscelaneo_detalle_tipodir' => $tipo_dir[$i],
                        ':fk_a006_num_miscelaneo_detalle_domicilio' => $situ_domicilio[$i],
                        ':ind_direccion' => $direccion[$i],
                        ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL[$i],
                        ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL[$i],'utf8'),
                        ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE[$i],
                        ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE[$i],'utf8'),
                        ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE[$i],
                        ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE[$i],'utf8'),
                        ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA[$i],
                        ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE[$i],'utf8'),
                        ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF[$i],'utf8'),
                        ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL[$i],'utf8')
                    ));
                }

                $errorReg4 = $registro->errorInfo();

            }else{
                $errorReg4 = '';
            }

            return $errorReg4;


        }else{//Modificar


            $idDireccion = $domicilio[11];

            if($tipo_dir!=0){

                for ($i = 0; $i < count($tipo_dir); $i++) {

                    if (isset($idDireccion[$i])){

                        //echo "update";
                        $registro = $this->_db->prepare(
                            "UPDATE
                                a036_persona_direccion
                             SET
                                fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                                fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                                fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                                ind_zona_residencial=:ind_zona_residencial,
                                fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                                ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                                fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                                ind_tipo_inmueble=:ind_tipo_inmueble,
                                fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                                ind_inmueble=:ind_inmueble,
                                ind_punto_referencia=:ind_punto_referencia,
                                ind_zona_postal=:ind_zona_postal,
                                ind_direccion=:ind_direccion
                             WHERE fk_a003_num_persona='$persona' and pk_num_direccion_persona='$idDireccion[$i]'
                            "
                         );

                    }else{
                        //echo "insert";
                       $registro = $this->_db->prepare(
                            "INSERT INTO
                                a036_persona_direccion
                             SET
                                fk_a003_num_persona='$persona',
                                fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                                fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                                fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                                ind_zona_residencial=:ind_zona_residencial,
                                fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                                ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                                fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                                ind_tipo_inmueble=:ind_tipo_inmueble,
                                fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                                ind_inmueble=:ind_inmueble,
                                ind_punto_referencia=:ind_punto_referencia,
                                ind_zona_postal=:ind_zona_postal,
                                ind_direccion=:ind_direccion
                             "
                         );
                    }

                    #execute — Ejecuta una sentencia preparada
                    $registro->execute(array(
                        ':fk_a006_num_miscelaneo_detalle_tipodir' => $tipo_dir[$i],
                        ':fk_a006_num_miscelaneo_detalle_domicilio' => $situ_domicilio[$i],
                        ':ind_direccion' => mb_strtoupper($direccion[$i],'utf8'),
                        ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL[$i],
                        ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL[$i],'utf8'),
                        ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE[$i],
                        ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE[$i],'utf8'),
                        ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE[$i],
                        ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE[$i],'utf8'),
                        ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA[$i],
                        ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE[$i],'utf8'),
                        ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF[$i],'utf8'),
                        ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL[$i],'utf8')
                    ));

                    $errorReg4 = $registro->errorInfo();

                }

            }else{
                $errorReg4='';
            }

            return $errorReg4;


        }

    }

    protected function metRegistrarTelefono($persona,$domicilio,$operacion)
    {
        $tipo_telf  = $domicilio[9];
        $telefono   = $domicilio[10];

        if(strcmp($operacion,'INSERT')==0){

            if($tipo_telf!=0){

                $registro = $this->_db->prepare(
                    "INSERT INTO
                    a007_persona_telefono
                 SET
                    ind_telefono=:ind_telefono,
                    fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                    fk_a003_num_persona=:fk_a003_num_persona
                "
                );
                for ($i = 0; $i < count($tipo_telf); $i++) {
                    #execute — Ejecuta una sentencia preparada
                    $registro->execute(array(
                        ':fk_a003_num_persona' => $persona,
                        ':ind_telefono' => $telefono[$i],
                        ':fk_a006_num_miscelaneo_detalle_tipo_telefono' => $tipo_telf[$i],
                    ));
                }

                $errorReg5 = $registro->errorInfo();

            }else{
                $errorReg5='';
            }

            return $errorReg5;

        }else{

            $idTelefono = $domicilio[12];

            if($tipo_telf!=0){

                for ($i = 0; $i < count($tipo_telf); $i++) {

                    if (isset($idTelefono[$i]) && !empty($idTelefono[$i])) {


                        $registro = $this->_db->prepare(
                            "UPDATE
                                a007_persona_telefono
                             SET
                                ind_telefono=:ind_telefono,
                                fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono
                              WHERE fk_a003_num_persona='$persona' and pk_num_telefono='$idTelefono[$i]'
                             "
                         );

                    }else{

                        $registro = $this->_db->prepare(
                            "INSERT INTO
                                a007_persona_telefono
                              SET
                                ind_telefono=:ind_telefono,
                                fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                                fk_a003_num_persona='$persona'
                             "
                         );

                    }

                   $registro->execute(array(
                        ':ind_telefono' => $telefono[$i],
                        ':fk_a006_num_miscelaneo_detalle_tipo_telefono' => $tipo_telf[$i]
                    ));

                    $errorReg5 = $registro->errorInfo();

                }

            }else{
                $errorReg5='';
            }

            return $errorReg5;

        }



    }

    protected function metRegistrarEmpDatos($empleado,$datos)
    {

        $registro = $this->_db->prepare(
            "INSERT INTO
            rh_c062_empleado_datos
            (fk_rhb001_num_empleado,
             fk_a006_num_miscelaneo_detalle_gsanguineo,
             fk_a006_num_miscelaneo_detalle_edocivil,
             fk_a006_num_miscelaneo_detalle_tlicencia,
             num_licencia_conducir,
             fec_expiracion_licencia,
             ind_observaciones,
             fec_ultima_modificacion,fk_a018_num_seguridad_usuario)
        VALUES
            (:fk_rhb001_num_empleado,
            :fk_a006_num_miscelaneo_detalle_gsanguineo,
            :fk_a006_num_miscelaneo_detalle_edocivil,
            :fk_a006_num_miscelaneo_detalle_tlicencia,
            :num_licencia_conducir,
            :fec_expiracion_licencia,
            :ind_observaciones,NOW(),'$this->atIdUsuario')
        ");

        #execute — Ejecuta una sentencia preparada
        $registro->execute(array(
            ':fk_rhb001_num_empleado' => $empleado,
            ':fk_a006_num_miscelaneo_detalle_gsanguineo' => $datos[0],
            ':fk_a006_num_miscelaneo_detalle_edocivil' => $datos[1],
            ':fk_a006_num_miscelaneo_detalle_tlicencia' => $datos[12],
            ':num_licencia_conducir' => $datos[13],
            ':fec_expiracion_licencia' => $datos[14],
            ':ind_observaciones' => $datos[15],
        ));

        $errorReg6 = $registro->errorInfo();

        return $errorReg6;

    }

    protected function metRegistrarEmpEmergencia($empleado,$datos,$operacion)
    {

        $tipo_telf1 = $datos[7];//TIPO TLF EMERGENCIA 1
        $tipo_telf2 = $datos[15];//TIPO TLF EMERGENCIA 2
        $telf1 = $datos[8];//TELEFONO EMERGENCIA 1
        $telf2 = $datos[16];//TELEFONO EMERGENCIA 2
        $tipo_dir1 = $datos[5];//TIPO DIRECCION EMERGENCIA 1
        $tipo_dir2 = $datos[13];//TIPO DIRECCION EMERGENCIA 2S
        $dir1 = $datos[6];//DIRECCION EMERGENCIA 1
        $dir2 = $datos[14];//DIRECCION EMERGENCIA 2
        $domicilio1 = $datos[31];//SITUACION DOMICILIO EMERGENCIA 1
        $domicilio2 = $datos[32];//SITUACION DOMICILIO EMERGENCIA 2

        $empEmergencia1 = $datos[23];//id empleado emergencia 1
        $empEmergencia2 = $datos[24];//id empleado emergencia 2

        $idPersonaEmergencia1 = $datos[25];//id persona emergencia 1
        $idPersonaEmergencia2 = $datos[26];//id persona emergencia 2

        $idDirecionEmergencia1 = $datos[27];
        $idDirecionEmergencia2 = $datos[28];

        #COMPLEMENTOS DATOS EMERGENCIA DIRECION 1
        $ID_TIPO_ZONA1 = $datos[33];
        $ID_UBI_INMUEBLE1 = $datos[34];
        $IND_UBI_INMUEBLE1 = $datos[35];
        $ID_TIPO_INMUEBLE1 = $datos[36];
        $IND_TIPO_INMUEBLE1 = $datos[37];
        $ID_ZONA_RESIDENCIAL1 = $datos[38];
        $IND_ZONA_RESIDENCIAL1 = $datos[39];
        $IND_INMUEBLE1 = $datos[40];
        $PUNTO_REF1 = $datos[41];
        $ZONA_POSTAL1 = $datos[42];

        #COMPLEMENTOS DATOS EMERGENCIA DIRECION 2
        $ID_TIPO_ZONA2 = $datos[43];
        $ID_UBI_INMUEBLE2 = $datos[44];
        $IND_UBI_INMUEBLE2 = $datos[45];
        $ID_TIPO_INMUEBLE2 = $datos[46];
        $IND_TIPO_INMUEBLE2 = $datos[47];
        $ID_ZONA_RESIDENCIAL2 = $datos[48];
        $IND_ZONA_RESIDENCIAL2 = $datos[49];
        $IND_INMUEBLE2 = $datos[50];
        $PUNTO_REF2 = $datos[51];
        $ZONA_POSTAL2 = $datos[52];

        $registroPersona1 = $this->_db->prepare(
            "INSERT INTO
                        a003_persona
                     SET
                        ind_cedula_documento=:ind_cedula_documento,
                        ind_documento_fiscal=:ind_documento_fiscal,
                        ind_nombre1=:ind_nombre1,
                        ind_apellido1=:ind_apellido1,
                        ind_tipo_persona=:ind_tipo_persona,
                        num_estatus=:ind_estatus_persona,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            "
        );

        $registroPersona2 = $this->_db->prepare(
            "INSERT INTO
                        a003_persona
                     SET
                        ind_cedula_documento=:ind_cedula_documento,
                        ind_documento_fiscal=:ind_documento_fiscal,
                        ind_nombre1=:ind_nombre1,
                        ind_apellido1=:ind_apellido1,
                        ind_tipo_persona=:ind_tipo_persona,
                        num_estatus=:ind_estatus_persona,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            "
        );

        $actualizoPersona1 = $this->_db->prepare(
            "UPDATE
                        a003_persona
                     SET
                        ind_cedula_documento=:ind_cedula_documento,
                        ind_documento_fiscal=:ind_documento_fiscal,
                        ind_nombre1=:ind_nombre1,
                        ind_apellido1=:ind_apellido1,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                     WHERE pk_num_persona=:pk_num_persona
            "
        );

        $registroEmergencia = $this->_db->prepare(
            "INSERT INTO
                        rh_c002_empleado_emergencia
                     SET
                        fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                        fk_a006_num_miscelaneo_detalle_parentesco=:fk_a006_num_miscelaneo_detalle_parentesco,
                        fk_a003_num_persona=:fk_a003_num_persona,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            "
        );

        $actualizoEmergencia = $this->_db->prepare(
            "UPDATE
                        rh_c002_empleado_emergencia
                     SET
                        fk_a006_num_miscelaneo_detalle_parentesco=:fk_a006_num_miscelaneo_detalle_parentesco,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
                     WHERE fk_a003_num_persona=:fk_a003_num_persona AND fk_rhb001_num_empleado=:fk_rhb001_num_empleado
            ");

        $registroPersonaTelefono = $this->_db->prepare(
            "INSERT INTO
                        a007_persona_telefono
                     SET
                        ind_telefono=:ind_telefono,
                        fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                        fk_a003_num_persona=:fk_a003_num_persona
            "
        );

        $registroPersonaDireccion = $this->_db->prepare(
            "INSERT INTO
                        a036_persona_direccion
                     SET
                        fk_a003_num_persona=:fk_a003_num_persona,
                        fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                        fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                        fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                        ind_zona_residencial=:ind_zona_residencial,
                        fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                        ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                        fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                        ind_tipo_inmueble=:ind_tipo_inmueble,
                        fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                        ind_inmueble=:ind_inmueble,
                        ind_punto_referencia=:ind_punto_referencia,
                        ind_zona_postal=:ind_zona_postal,
                        ind_direccion=:ind_direccion
            "
        );

        $registroPersonaDireccion2 = $this->_db->prepare(
            "INSERT INTO
                        a036_persona_direccion
                     SET
                        fk_a003_num_persona=:fk_a003_num_persona,
                        fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                        fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                        fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                        ind_zona_residencial=:ind_zona_residencial,
                        fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                        ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                        fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                        ind_tipo_inmueble=:ind_tipo_inmueble,
                        fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                        ind_inmueble=:ind_inmueble,
                        ind_punto_referencia=:ind_punto_referencia,
                        ind_zona_postal=:ind_zona_postal,
                        ind_direccion=:ind_direccion
            "
        );


        if(strcmp($operacion,'INSERT')==0){

            if($datos[2]){

                $registroPersona1->execute(array(
                    ':ind_cedula_documento' => $datos[2],
                    ':ind_documento_fiscal' => $datos[2],
                    ':ind_nombre1' => $datos[3],
                    ':ind_apellido1' => $datos[4],
                    ':ind_tipo_persona' => 'N',
                    ':ind_estatus_persona' => '1'
                ));

                $error1 = $registroPersona1->errorInfo();
                //obtengo id de la persona
                $idRegistroPersona1 = $this->_db->lastInsertId();

                if (!empty($error1[1]) && !empty($error1[2])) {
                    $this->_db->rollBack();
                    return $error1;
                } else {

                    #execute — Ejecuta una sentencia preparada
                    $registroEmergencia->execute(array(
                        ':fk_rhb001_num_empleado' => $empleado,
                        ':fk_a006_num_miscelaneo_detalle_parentesco' => $datos[9],
                        ':fk_a003_num_persona' => $idRegistroPersona1,
                        ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario
                    ));

                    $error2 = $registroEmergencia->errorInfo();

                    if (!empty($error2[1]) && !empty($error2[2])) {
                        $this->_db->rollBack();
                        return $error2;
                    } else {

                        if ($tipo_telf1 != 0) {
                            for ($i = 0; $i < count($tipo_telf1); $i++) {
                                #execute — Ejecuta una sentencia preparada
                                $registroPersonaTelefono->execute(array(
                                    ':ind_telefono' => $telf1[$i],
                                    ':fk_a006_num_miscelaneo_detalle_tipo_telefono' => $tipo_telf1[$i],
                                    ':fk_a003_num_persona' => $idRegistroPersona1
                                ));
                            }
                        }
                        $error3 = $registroPersonaTelefono->errorInfo();

                        if (!empty($error3[1]) && !empty($error3[2])) {
                            $this->_db->rollBack();
                            return $error3;
                        } else {

                            if ($tipo_dir1 != 0) {

                                $registroPersonaDireccion->execute(array(
                                    ':fk_a003_num_persona' => $idRegistroPersona1,
                                    ':fk_a006_num_miscelaneo_detalle_tipodir' => $tipo_dir1,
                                    ':fk_a006_num_miscelaneo_detalle_domicilio' => $domicilio1,
                                    ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL1,
                                    ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL1,'utf8'),
                                    ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE1,
                                    ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE1,'utf8'),
                                    ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE1,
                                    ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE1,'utf8'),
                                    ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA1,
                                    ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE1,'utf8'),
                                    ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF1,'utf8'),
                                    ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL1,'utf8'),
                                    ':ind_direccion' => mb_strtoupper($dir1,'utf8')
                                ));

                            }
                            $error4 = $registroPersonaDireccion->errorInfo();

                            if (!empty($error4[1]) && !empty($error4[2])) {
                                $this->_db->rollBack();
                                return $error4;
                            } else {
                                $ejecucion1=1;
                            }


                        }
                    }

                }

            }
            if($datos[10]){

                #execute — Ejecuta una sentencia preparada
                $registroPersona2->execute(array(
                    ':ind_cedula_documento' => $datos[10],
                    ':ind_documento_fiscal' => $datos[10],
                    ':ind_nombre1' => $datos[11],
                    ':ind_apellido1' => $datos[12],
                    ':ind_tipo_persona' => 'N',
                    ':ind_estatus_persona' => '1'
                ));

                $error1 = $registroPersona2->errorInfo();
                //obtengo id de la persona
                $idRegistroPersona2= $this->_db->lastInsertId();

                if(!empty($error1[1]) && !empty($error1[2])){
                    $this->_db->rollBack();
                    return $error1;
                }
                else {

                    #execute — Ejecuta una sentencia preparada
                    $registroEmergencia->execute(array(
                        ':fk_rhb001_num_empleado' => $empleado,
                        ':fk_a006_num_miscelaneo_detalle_parentesco' => $datos[17],
                        ':fk_a003_num_persona' => $idRegistroPersona2,
                        ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario
                    ));

                    $error2 = $registroEmergencia->errorInfo();

                    if(!empty($error2[1]) && !empty($error2[2])){
                        $this->_db->rollBack();
                        return $error2;
                    }
                    else {
                        if($tipo_telf2!=0){
                            for ($i = 0; $i < count($tipo_telf2); $i++) {
                                #execute — Ejecuta una sentencia preparada
                                $registroPersonaTelefono->execute(array(
                                    ':ind_telefono' => $telf2[$i],
                                    ':fk_a006_num_miscelaneo_detalle_tipo_telefono' => $tipo_telf2[$i],
                                    ':fk_a003_num_persona' => $idRegistroPersona2
                                ));
                            }
                        }

                        $error3 = $registroPersonaTelefono->errorInfo();

                        if(!empty($error3[1]) && !empty($error3[2])){
                            $this->_db->rollBack();
                            return $error3;
                        }else{

                            if ($tipo_dir2 != 0) {

                                #execute — Ejecuta una sentencia preparada
                                $registroPersonaDireccion2->execute(array(
                                    ':fk_a003_num_persona' => $idRegistroPersona2,
                                    ':fk_a006_num_miscelaneo_detalle_tipodir' => $tipo_dir2,
                                    ':fk_a006_num_miscelaneo_detalle_domicilio' => $domicilio2,
                                    ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL2,
                                    ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL2,'utf8'),
                                    ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE2,
                                    ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE2,'utf8'),
                                    ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE2,
                                    ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE2,'utf8'),
                                    ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA2,
                                    ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE2,'utf8'),
                                    ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF2,'utf8'),
                                    ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL2,'utf8'),
                                    ':ind_direccion' => mb_strtoupper($dir2,'utf8')
                                ));

                            }
                            $error4 = $registroPersonaDireccion2->errorInfo();

                            if (!empty($error4[1]) && !empty($error4[2])) {
                                $this->_db->rollBack();
                                return $error4;
                            } else {

                                $ejecucion2=1;
                            }
                        }
                    }

                }

            }

            if(isset($ejecucion1) || isset($ejecucion2)){

                return $errorReg7 = '';

            }else if(isset($ejecucion1)){

                return $errorReg7 = '';

            }else if(isset($ejecucion2)){

                return $errorReg7 = '';
            }


        }else{//MODIFICAR


              /***************************************** REGISTRO DE EMERGENCIA 1 *****************************************/
              if($empEmergencia1==0){//no existe registro... inserto datos de emergencia

                  if($datos[2]){

                     //$this->_db->beginTransaction();
                     $registroPersona1->execute(array(
                          ':ind_cedula_documento' => $datos[2],
                          ':ind_documento_fiscal' => $datos[2],
                          ':ind_nombre1' => $datos[3],
                          ':ind_apellido1' => $datos[4],
                          ':ind_tipo_persona' => 'N',
                          ':ind_estatus_persona' => '1'
                      ));

                      $error1 = $registroPersona1->errorInfo();
                      //obtengo id de la persona
                      $idRegistroPersona1 = $this->_db->lastInsertId();

                      if (!empty($error1[1]) && !empty($error1[2])) {
                          //$this->_db->rollBack();
                          return $error1;
                      } else {

                          #execute — Ejecuta una sentencia preparada
                          $registroEmergencia->execute(array(
                              ':fk_rhb001_num_empleado' => $empleado,
                              ':fk_a006_num_miscelaneo_detalle_parentesco' => $datos[9],
                              ':fk_a003_num_persona' => $idRegistroPersona1,
                              ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario
                          ));

                          $error2 = $registroEmergencia->errorInfo();

                          if (!empty($error2[1]) && !empty($error2[2])) {
                              //$this->_db->rollBack();
                              return $error2;
                          } else {

                              if ($tipo_telf1 != 0) {
                                  for ($i = 0; $i < count($tipo_telf1); $i++) {
                                      #execute — Ejecuta una sentencia preparada
                                      $registroPersonaTelefono->execute(array(
                                          ':ind_telefono' => $telf1[$i],
                                          ':fk_a006_num_miscelaneo_detalle_tipo_telefono' => $tipo_telf1[$i],
                                          ':fk_a003_num_persona' => $idRegistroPersona1
                                      ));
                                  }
                              }
                              $error3 = $registroPersonaTelefono->errorInfo();

                              if (!empty($error3[1]) && !empty($error3[2])) {
                                  //$this->_db->rollBack();
                                  return $error3;
                              } else {

                                  if ($tipo_dir1 != 0) {

                                      $registroPersonaDireccion->execute(array(
                                          ':fk_a003_num_persona' => $idRegistroPersona1,
                                          ':fk_a006_num_miscelaneo_detalle_tipodir' => $tipo_dir1,
                                          ':fk_a006_num_miscelaneo_detalle_domicilio' => $domicilio1,
                                          ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL1,
                                          ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL1,'utf8'),
                                          ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE1,
                                          ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE1,'utf8'),
                                          ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE1,
                                          ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE1,'utf8'),
                                          ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA1,
                                          ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE1,'utf8'),
                                          ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF1,'utf8'),
                                          ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL1,'utf8'),
                                          ':ind_direccion' => mb_strtoupper($dir1,'utf8')
                                      ));

                                  }
                                  $error4 = $registroPersonaDireccion->errorInfo();

                                  if (!empty($error4[1]) && !empty($error4[2])) {
                                      //$this->_db->rollBack();
                                      return $error4;
                                  } else {
                                      $ejecucion1=1;
                                  }


                              }

                          }

                      }

                  }


              }else{//existe datos de emergencia.... actualizo datos de emergencia


                  if($datos[2]){

                      $actualizoPersona1->execute(array(
                          ':ind_cedula_documento' => $datos[2],
                          ':ind_documento_fiscal' => $datos[2],
                          ':ind_nombre1' => $datos[3],
                          ':ind_apellido1' => $datos[4],
                          ':pk_num_persona' => $idPersonaEmergencia1,
                      ));

                      $error1 = $actualizoPersona1->errorInfo();

                      if (!empty($error1[1]) && !empty($error1[2])) {
                          return $error1;
                      } else {

                          $actualizoEmergencia->execute(array(
                              ':fk_rhb001_num_empleado' => $empleado,
                              ':fk_a006_num_miscelaneo_detalle_parentesco' => $datos[9],
                              ':fk_a003_num_persona' => $idPersonaEmergencia1,
                              ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario
                          ));

                          $error2 = $actualizoEmergencia->errorInfo();

                          if (!empty($error2[1]) && !empty($error2[2])) {
                              return $error2;
                          } else {

                              $idTelefono = $datos[21];

                              if ($tipo_telf1 != 0) {

                                  for ($i = 0; $i < count($tipo_telf1); $i++) {

                                      if (isset($idTelefono[$i])) {//actualizo

                                          $PersonaTelefono = $this->_db->prepare(
                                              "UPDATE
                                                    a007_persona_telefono
                                                 SET
                                                    ind_telefono=:ind_telefono,
                                                    fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono
                                                 WHERE fk_a003_num_persona='$idPersonaEmergencia1' and pk_num_telefono='$idTelefono[$i]'
                                              "
                                           );

                                      }else{//inserto

                                          $PersonaTelefono = $this->_db->prepare(
                                              "INSERT INTO
                                                    a007_persona_telefono
                                                 SET
                                                    ind_telefono=:ind_telefono,
                                                    fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                                                    fk_a003_num_persona='$idPersonaEmergencia1'
                                               "
                                          );

                                      }
                                      #execute — Ejecuta una sentencia preparada
                                      $PersonaTelefono->execute(array(
                                          ':ind_telefono' => $telf1[$i],
                                          ':fk_a006_num_miscelaneo_detalle_tipo_telefono' => $tipo_telf1[$i]

                                      ));

                                  }


                              }

                              if ($tipo_telf1 != 0) {
                                  $error3 = $PersonaTelefono->errorInfo();
                              } else {
                                  $error3 = array(1 => null, 2 => null);
                              }



                              if (!empty($error3[1]) && !empty($error3[2])) {
                                  return $error3;
                              } else {

                                    if($idDirecionEmergencia1!=0){//actualizo

                                        $PersonaDireccion = $this->_db->prepare(
                                            "UPDATE
                                                    a036_persona_direccion
                                                 SET
                                                    fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                                                    fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                                                    fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                                                    ind_zona_residencial=:ind_zona_residencial,
                                                    fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                                                    ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                                                    fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                                                    ind_tipo_inmueble=:ind_tipo_inmueble,
                                                    fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                                                    ind_inmueble=:ind_inmueble,
                                                    ind_punto_referencia=:ind_punto_referencia,
                                                    ind_zona_postal=:ind_zona_postal,
                                                    ind_direccion=:ind_direccion
                                                 WHERE fk_a003_num_persona='$idPersonaEmergencia1' AND pk_num_direccion_persona='$idDirecionEmergencia1'
                                            "
                                        );

                                    }else{//inserto


                                        $PersonaDireccion = $this->_db->prepare(
                                            "INSERT INTO
                                                    a036_persona_direccion
                                                 SET
                                                    fk_a003_num_persona='$idPersonaEmergencia1',
                                                    fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                                                    fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                                                    fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                                                    ind_zona_residencial=:ind_zona_residencial,
                                                    fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                                                    ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                                                    fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                                                    ind_tipo_inmueble=:ind_tipo_inmueble,
                                                    fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                                                    ind_inmueble=:ind_inmueble,
                                                    ind_punto_referencia=:ind_punto_referencia,
                                                    ind_zona_postal=:ind_zona_postal,
                                                    ind_direccion=:ind_direccion
                                         "
                                         );

                                    }

                                      $PersonaDireccion->execute(array(
                                          ':fk_a006_num_miscelaneo_detalle_tipodir' => $tipo_dir1,
                                          ':fk_a006_num_miscelaneo_detalle_domicilio' => $domicilio1,
                                          ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL1,
                                          ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL1,'utf8'),
                                          ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE1,
                                          ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE1,'utf8'),
                                          ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE1,
                                          ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE1,'utf8'),
                                          ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA1,
                                          ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE1,'utf8'),
                                          ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF1,'utf8'),
                                          ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL1,'utf8'),
                                          ':ind_direccion' => mb_strtoupper($dir1,'utf8')
                                      ));

                                      $error4 = $PersonaDireccion->errorInfo();

                                      if (!empty($error4[1]) && !empty($error4[2])) {
                                          //$this->_db->rollBack();
                                          return $error4;
                                      } else {
                                          $ejecucion1=1;
                                      }

                              }

                          }

                      }

                  }

              }



             /***************************************** REGISTRO DE EMERGENCIA 2 *****************************************/
             if($empEmergencia2==0){//no existe registro... inserto datos de emergencia

                 if($datos[10]){

                     #execute — Ejecuta una sentencia preparada
                     $registroPersona2->execute(array(
                         ':ind_cedula_documento' => $datos[10],
                         ':ind_documento_fiscal' => $datos[10],
                         ':ind_nombre1' => $datos[11],
                         ':ind_apellido1' => $datos[12],
                         ':ind_tipo_persona' => 'N',
                         ':ind_estatus_persona' => '1'
                     ));

                     $error1 = $registroPersona2->errorInfo();
                     //obtengo id de la persona
                     $idRegistroPersona2= $this->_db->lastInsertId();

                     if(!empty($error1[1]) && !empty($error1[2])){
                         //$this->_db->rollBack();
                         return $error1;
                     }
                     else {

                         #execute — Ejecuta una sentencia preparada
                         $registroEmergencia->execute(array(
                             ':fk_rhb001_num_empleado' => $empleado,
                             ':fk_a006_num_miscelaneo_detalle_parentesco' => $datos[17],
                             ':fk_a003_num_persona' => $idRegistroPersona2,
                             ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario
                         ));

                         $error2 = $registroEmergencia->errorInfo();

                         if(!empty($error2[1]) && !empty($error2[2])){
                             //$this->_db->rollBack();
                             return $error2;
                         }
                         else {
                             if($tipo_telf2!=0){
                                 for ($i = 0; $i < count($tipo_telf2); $i++) {
                                     #execute — Ejecuta una sentencia preparada
                                     $registroPersonaTelefono->execute(array(
                                         ':ind_telefono' => $telf2[$i],
                                         ':fk_a006_num_miscelaneo_detalle_tipo_telefono' => $tipo_telf2[$i],
                                         ':fk_a003_num_persona' => $idRegistroPersona2
                                     ));
                                 }
                             }

                             $error3 = $registroPersonaTelefono->errorInfo();

                             if(!empty($error3[1]) && !empty($error3[2])){
                                 //$this->_db->rollBack();
                                 return $error3;
                             }else{

                                 if ($tipo_dir2 != 0) {

                                     #execute — Ejecuta una sentencia preparada
                                     $registroPersonaDireccion2->execute(array(
                                         ':fk_a003_num_persona' => $idRegistroPersona2,
                                         ':fk_a006_num_miscelaneo_detalle_tipodir' => $tipo_dir2,
                                         ':fk_a006_num_miscelaneo_detalle_domicilio' => $domicilio2,
                                         ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL2,
                                         ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL2,'utf8'),
                                         ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE2,
                                         ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE2,'utf8'),
                                         ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE2,
                                         ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE2,'utf8'),
                                         ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA2,
                                         ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE2,'utf8'),
                                         ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF2,'utf8'),
                                         ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL2,'utf8'),
                                         ':ind_direccion' => mb_strtoupper($dir2,'utf8')
                                     ));

                                 }
                                 $error4 = $registroPersonaDireccion2->errorInfo();

                                 if (!empty($error4[1]) && !empty($error4[2])) {
                                     //$this->_db->rollBack();
                                     return $error4;
                                 } else {

                                     $ejecucion2=1;
                                 }
                             }
                         }

                     }

                 }


             }else{//existe datos de emergencia.... actualizo datos de emergencia


                 if($datos[10]){

                     $actualizoPersona1->execute(array(
                         ':ind_cedula_documento' => $datos[10],
                         ':ind_documento_fiscal' => $datos[10],
                         ':ind_nombre1' => $datos[11],
                         ':ind_apellido1' => $datos[12],
                         ':pk_num_persona' => $idPersonaEmergencia2,
                     ));

                     $error1 = $actualizoPersona1->errorInfo();

                     if (!empty($error1[1]) && !empty($error1[2])) {
                         return $error1;
                     } else {

                         $actualizoEmergencia->execute(array(
                             ':fk_rhb001_num_empleado' => $empleado,
                             ':fk_a006_num_miscelaneo_detalle_parentesco' => $datos[17],
                             ':fk_a003_num_persona' => $idPersonaEmergencia2,
                             ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario
                         ));

                         $error2 = $actualizoEmergencia->errorInfo();

                         if (!empty($error2[1]) && !empty($error2[2])) {
                             return $error2;
                         } else {

                             $idTelefono = $datos[22];

                             if ($tipo_telf2 != 0) {

                                 for ($i = 0; $i < count($tipo_telf2); $i++) {

                                     if (isset($idTelefono[$i])) {//actualizo

                                         $PersonaTelefono = $this->_db->prepare(
                                             "UPDATE
                                                    a007_persona_telefono
                                                 SET
                                                    ind_telefono=:ind_telefono,
                                                    fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono
                                                 WHERE fk_a003_num_persona='$idPersonaEmergencia2' and pk_num_telefono='$idTelefono[$i]'
                                              "
                                         );

                                     }else{//inserto

                                         $PersonaTelefono = $this->_db->prepare(
                                             "INSERT INTO
                                                    a007_persona_telefono
                                                 SET
                                                    ind_telefono=:ind_telefono,
                                                    fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                                                    fk_a003_num_persona='$idPersonaEmergencia2'
                                               "
                                         );

                                     }
                                     #execute — Ejecuta una sentencia preparada
                                     $PersonaTelefono->execute(array(
                                         ':ind_telefono' => $telf2[$i],
                                         ':fk_a006_num_miscelaneo_detalle_tipo_telefono' => $tipo_telf2[$i]

                                     ));

                                 }


                             }

                             if ($tipo_telf2 != 0) {
                                 $error3 = $PersonaTelefono->errorInfo();
                             } else {
                                 $error3 = array(1 => null, 2 => null);
                             }

                             if (!empty($error3[1]) && !empty($error3[2])) {
                                 return $error3;
                             } else {

                                 if($idDirecionEmergencia2!=0){//actualizo

                                     $PersonaDireccion = $this->_db->prepare(
                                         "UPDATE
                                                    a036_persona_direccion
                                                 SET
                                                    fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                                                    fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                                                    fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                                                    ind_zona_residencial=:ind_zona_residencial,
                                                    fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                                                    ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                                                    fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                                                    ind_tipo_inmueble=:ind_tipo_inmueble,
                                                    fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                                                    ind_inmueble=:ind_inmueble,
                                                    ind_punto_referencia=:ind_punto_referencia,
                                                    ind_zona_postal=:ind_zona_postal,
                                                    ind_direccion=:ind_direccion
                                                 WHERE fk_a003_num_persona='$idPersonaEmergencia2' AND pk_num_direccion_persona='$idDirecionEmergencia2'
                                            "
                                     );

                                 }else{//inserto


                                     $PersonaDireccion = $this->_db->prepare(
                                         "INSERT INTO
                                                    a036_persona_direccion
                                                 SET
                                                    fk_a003_num_persona='$idPersonaEmergencia2',
                                                    fk_a006_num_miscelaneo_detalle_domicilio=:fk_a006_num_miscelaneo_detalle_domicilio,
                                                    fk_a006_num_miscelaneo_detalle_tipodir=:fk_a006_num_miscelaneo_detalle_tipodir,
                                                    fk_a006_num_zona_residencial=:fk_a006_num_zona_residencial,
                                                    ind_zona_residencial=:ind_zona_residencial,
                                                    fk_a006_num_ubicacion_inmueble=:fk_a006_num_ubicacion_inmueble,
                                                    ind_ubicacion_inmueble=:ind_ubicacion_inmueble,
                                                    fk_a006_num_tipo_inmueble=:fk_a006_num_tipo_inmueble,
                                                    ind_tipo_inmueble=:ind_tipo_inmueble,
                                                    fk_a006_num_tipo_zona=:fk_a006_num_tipo_zona,
                                                    ind_inmueble=:ind_inmueble,
                                                    ind_punto_referencia=:ind_punto_referencia,
                                                    ind_zona_postal=:ind_zona_postal,
                                                    ind_direccion=:ind_direccion
                                         "
                                     );

                                 }

                                 $PersonaDireccion->execute(array(
                                     ':fk_a006_num_miscelaneo_detalle_tipodir' => $tipo_dir2,
                                     ':fk_a006_num_miscelaneo_detalle_domicilio' => $domicilio2,
                                     ':fk_a006_num_zona_residencial'  => $ID_ZONA_RESIDENCIAL2,
                                     ':ind_zona_residencial'  => mb_strtoupper($IND_ZONA_RESIDENCIAL2,'utf8'),
                                     ':fk_a006_num_ubicacion_inmueble'  => $ID_UBI_INMUEBLE2,
                                     ':ind_ubicacion_inmueble'  => mb_strtoupper($IND_UBI_INMUEBLE2,'utf8'),
                                     ':fk_a006_num_tipo_inmueble'  => $ID_TIPO_INMUEBLE2,
                                     ':ind_tipo_inmueble'  => mb_strtoupper($IND_TIPO_INMUEBLE2,'utf8'),
                                     ':fk_a006_num_tipo_zona'  => $ID_TIPO_ZONA2,
                                     ':ind_inmueble'  => mb_strtoupper($IND_INMUEBLE2,'utf8'),
                                     ':ind_punto_referencia'  => mb_strtoupper($PUNTO_REF2,'utf8'),
                                     ':ind_zona_postal'  => mb_strtoupper($ZONA_POSTAL2,'utf8'),
                                     ':ind_direccion' => mb_strtoupper($dir2,'utf8')
                                 ));

                                 $error4 = $PersonaDireccion->errorInfo();

                                 if (!empty($error4[1]) && !empty($error4[2])) {
                                     //$this->_db->rollBack();
                                     return $error4;
                                 } else {
                                     $ejecucion2=1;
                                 }

                             }

                         }

                     }

                 }

             }


            if(isset($ejecucion1) || isset($ejecucion2)){

                return $errorReg7 = '';

            }else if(isset($ejecucion1)){

                return $errorReg7 = '';

            }else if(isset($ejecucion2)){

                return $errorReg7 = '';
            }

        }//fin modificar

    }

    protected function metRegistrarEmpOrganizacion($empleado,$organizacion,$operacion)
    {

        if(strcmp($operacion,'INSERT')==0){

            $registro = $this->_db->prepare(
                "INSERT INTO
            rh_c076_empleado_organizacion
            (fk_rhb001_num_empleado,
             fk_a001_num_organismo,
             fk_a004_num_dependencia,
             fk_a023_num_centro_costo,
             fk_nmb001_num_tipo_nomina,
             fk_a006_num_miscelaneo_detalle_tipopago,
             fk_a006_num_miscelaneo_detalle_tipotrabajador,
             fec_ultima_modificacion,fk_a018_num_seguridad_usuario)
         VALUES
            (:fk_rhb001_num_empleado,
            :fk_a001_num_organismo,
            :fk_a004_num_dependencia,
            :fk_a023_num_centro_costo,
            :fk_nmb001_num_tipo_nomina,
            :fk_a006_num_miscelaneo_detalle_tipopago,
            :fk_a006_num_miscelaneo_detalle_tipotrabajador,
            NOW(),'$this->atIdUsuario')
         ");

            $registro->execute(array(
                ':fk_rhb001_num_empleado' => $empleado,
                ':fk_a001_num_organismo' => $organizacion[0],
                ':fk_a004_num_dependencia' => $organizacion[1],
                ':fk_a023_num_centro_costo' => $organizacion[2],
                ':fk_nmb001_num_tipo_nomina' => $organizacion[3],
                ':fk_a006_num_miscelaneo_detalle_tipopago' => $organizacion[4],
                ':fk_a006_num_miscelaneo_detalle_tipotrabajador' => $organizacion[5]
            ));

            $errorReg2 = $registro->errorInfo();

            return $errorReg2;

        }else{

            $actualizo = $this->_db->prepare(
                "UPDATE
                    rh_c076_empleado_organizacion
                 SET
                    fk_a001_num_organismo=:fk_a001_num_organismo,
                    fk_a004_num_dependencia=:fk_a004_num_dependencia,
                    fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                    fk_a006_num_miscelaneo_detalle_tipopago=:fk_a006_num_miscelaneo_detalle_tipopago,
                    fk_a006_num_miscelaneo_detalle_tipotrabajador=:fk_a006_num_miscelaneo_detalle_tipotrabajador,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE  fk_rhb001_num_empleado='$empleado'
              ");

            $actualizo->execute(array(
                ':fk_a001_num_organismo' => $organizacion[0],
                ':fk_a004_num_dependencia' => $organizacion[1],
                ':fk_a023_num_centro_costo' => $organizacion[2],
                ':fk_nmb001_num_tipo_nomina' => $organizacion[3],
                ':fk_a006_num_miscelaneo_detalle_tipopago' => $organizacion[4],
                ':fk_a006_num_miscelaneo_detalle_tipotrabajador' => $organizacion[5]
            ));

            $errorReg2 = $actualizo->errorInfo();

            return $errorReg2;

        }



    }

    protected function metRegistrarEmpLaboral($empleado,$laborales,$operacion)
    {

        if(strcmp($operacion,'INSERT')==0){

            $aux = str_replace(".","",$laborales[7]);

            $registro = $this->_db->prepare(
                "INSERT INTO
                    rh_c005_empleado_laboral
                 SET
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                    fec_ingreso=:fec_ingreso,
                    ind_resolucion_ingreso=:ind_resolucion_ingreso,
                    fk_rhc063_num_puestos_cargo=:fk_rhc063_num_puestos_cargo,
                    fk_a006_num_miscelaneo_detalle_paso=:fk_a006_num_miscelaneo_detalle_paso,
                    num_sueldo=:num_sueldo,
                    fk_rhb007_num_horario=:fk_rhb007_num_horario,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 ");

            $fn = explode("-",$laborales[0]);

            $registro->execute(array(
                ':fk_rhb001_num_empleado' => $empleado,
                ':fec_ingreso' => $fn[2]."-".$fn[1]."-".$fn[0],
                ':ind_resolucion_ingreso' => $laborales[1],
                ':fk_rhc063_num_puestos_cargo' => $laborales[5],
                ':fk_rhb007_num_horario' => $laborales[6],
                ':fk_a006_num_miscelaneo_detalle_paso' => $laborales[8],
                ':num_sueldo' => '0.00'
            ));

            $errorReg3 = $registro->errorInfo();

            return $errorReg3;

        }else{

            $aux = str_replace(".","",$laborales[7]);

            $actualizo = $this->_db->prepare(
                "UPDATE
                    rh_c005_empleado_laboral
                 SET
                    fec_ingreso=:fec_ingreso,
                    ind_resolucion_ingreso=:ind_resolucion_ingreso,
                    fk_rhc063_num_puestos_cargo=:fk_rhc063_num_puestos_cargo,
                    fk_rhb007_num_horario=:fk_rhb007_num_horario,
                    fk_a006_num_miscelaneo_detalle_paso=:fk_a006_num_miscelaneo_detalle_paso,
                    num_sueldo=:num_sueldo,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE
                    fk_rhb001_num_empleado='$empleado'
                "
            );
            $fn = explode("-",$laborales[0]);

            $actualizo->execute(array(
                ':fec_ingreso' => $fn[2]."-".$fn[1]."-".$fn[0],
                ':ind_resolucion_ingreso' => $laborales[1],
                ':fk_rhc063_num_puestos_cargo' => $laborales[5],
                ':fk_rhb007_num_horario' => $laborales[6],
                ':fk_a006_num_miscelaneo_detalle_paso' => $laborales[8],
                ':num_sueldo' => '0.00'
            ));

            $errorReg3 = $actualizo->errorInfo();

            return $errorReg3;

        }


    }

    #permite obtener los datos del empleado
    public function metObtenerDatosEmpleado($idEmpleado)
    {

        $con =  $this->_db->query("

                SELECT
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                rh_c076_empleado_organizacion.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa AS organismo,
                a003_persona.fk_a006_num_miscelaneo_detalle_sexo,
                sexo.ind_nombre_detalle AS sexo,
                a003_persona.fec_nacimiento,
                rh_c076_empleado_organizacion.fk_a004_num_dependencia,
                a004_dependencia.ind_dependencia AS dependencia,
                rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo,
                rh_c063_puestos.ind_descripcion_cargo AS cargo,
                rh_c063_puestos.num_sueldo_basico,
                rh_c005_empleado_laboral.fec_ingreso,
                rh_b001_empleado.num_estatus AS estatus_empleado,
                a003_persona.num_estatus AS estatus_persona,
                rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador,
                tipo_trabajador.ind_nombre_detalle AS tipo_trabajador
                FROM
                rh_b001_empleado
                INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN a006_miscelaneo_detalle AS sexo ON a003_persona.fk_a006_num_miscelaneo_detalle_sexo = sexo.pk_num_miscelaneo_detalle
                INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                WHERE rh_b001_empleado.pk_num_empleado='$idEmpleado'

        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    #para visualizar onformacion del registro del empleado
    public function metInformacionRegistroEmpleado($idEmpleado)
    {

        $con = $this->_db->query("
                SELECT
                *
                FROM
                vl_rh_info_registro_empleado
                WHERE
                pk_num_empleado='$idEmpleado'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }


    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite obtener la organismo,dependencia,estado_registro, situacion_trabajo del empleado
    public function metEstadoEmpleado($pkNumEmpleado)
    {
        $estadoEmpleado = $this->_db->query(
            "SELECT
                a.pk_num_empleado,
                c.fk_a004_num_dependencia,
                g.pk_num_dependencia,
                g.ind_dependencia,
                e.pk_num_puestos,
                e.ind_descripcion_cargo,
                f.pk_num_organismo,
                f.ind_descripcion_empresa,
                b.num_estatus AS num_estado_registro,
                a.num_estatus AS num_situacion_trabajo,
                CONCAT(
                    b.ind_nombre1,
                    ' ',
                    b.ind_nombre2,
                    ' ',
                    b.ind_apellido1,
                    ' ',
                    b.ind_apellido2
                ) AS nombre_completo,
                b.fec_nacimiento AS fecha_nacimiento,
                d.fec_ingreso AS fecha_ingreso,
                date_format(
                    b.fec_nacimiento,
                    '%d/%m/%Y'
                ) AS fec_nacimiento,
                date_format(d.fec_ingreso, '%d/%m/%Y') AS fec_ingreso
            FROM
                rh_b001_empleado AS a
            INNER JOIN a003_persona AS b ON a.fk_a003_num_persona = b.pk_num_persona
            INNER JOIN rh_c076_empleado_organizacion AS c ON c.fk_rhb001_num_empleado = a.pk_num_empleado
            INNER JOIN rh_c005_empleado_laboral AS d ON d.fk_rhb001_num_empleado = a.pk_num_empleado
            INNER JOIN rh_c063_puestos AS e ON d.fk_rhc063_num_puestos_cargo = e.pk_num_puestos
            INNER JOIN a001_organismo AS f ON c.fk_a001_num_organismo = f.pk_num_organismo
            INNER JOIN a004_dependencia AS g ON c.fk_a004_num_dependencia = g.pk_num_dependencia
            WHERE a.pk_num_empleado='$pkNumEmpleado'
	        ");
        $estadoEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $estadoEmpleado->fetch();
    }


    public function metBuscarTrabajador($filtro)
    {

        if(strcmp('TODOS',$filtro)==0){
            $FILTRO = " ";
        }
        if(strcmp('ACTIVOS',$filtro)==0){
            $FILTRO = "WHERE b.num_estatus=1 and a.num_estatus=1";
        }
        if(strcmp('NOACTIVOS-NOACTIVOS',$filtro)==0){
            $FILTRO = "WHERE b.num_estatus=0 and a.num_estatus=0";
        }
        if(strcmp('ACTIVOS-NOACTIVOS',$filtro)==0){
            $FILTRO = "WHERE b.num_estatus=1 and a.num_estatus=0";
        }

        $buscarTrabajador = $this->_db->query(
            "SELECT
                a.pk_num_empleado,
                b.pk_num_persona,
                b.ind_cedula_documento,
                b.ind_nombre1,
                b.ind_nombre2,
                b.ind_apellido1,
                b.ind_apellido2,
                b.ind_foto,
                c.fk_a001_num_organismo,
                d.ind_descripcion_empresa,
                c.fk_a004_num_dependencia,
                e.ind_dependencia,
                c.fk_a023_num_centro_costo,
                c.fk_nmb001_num_tipo_nomina,
                c.fk_a006_num_miscelaneo_detalle_tipotrabajador,
                b.num_estatus AS estado_registro,
                a.num_estatus AS situacion_trabajo,
                f.fec_ingreso,
                g.ind_descripcion_cargo
            FROM
                rh_b001_empleado AS a
            INNER JOIN a003_persona AS b ON a.fk_a003_num_persona = b.pk_num_persona
            INNER JOIN rh_c076_empleado_organizacion AS c ON c.fk_rhb001_num_empleado = a.pk_num_empleado
            INNER JOIN a001_organismo AS d ON c.fk_a001_num_organismo = d.pk_num_organismo
            INNER JOIN a004_dependencia AS e ON c.fk_a004_num_dependencia = e.pk_num_dependencia
            INNER JOIN rh_c005_empleado_laboral AS f ON f.fk_rhb001_num_empleado = a.pk_num_empleado
            INNER JOIN rh_c063_puestos AS g ON f.fk_rhc063_num_puestos_cargo = g.pk_num_puestos
            $FILTRO
            "
        );

        $buscarTrabajador->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarTrabajador->fetchAll();
    }

    // Método que permite  listar a los empleados de acuerdo al buscador de empleados
    public function metBuscarTrabajadorFiltro($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $tipoTrabajador, $estado_registro, $situacionTrabajo, $fechaInicio, $fecha_fin)
    {
        if ($pkNumOrganismo!='') {
            $FILTRO = "c.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $FILTRO .= " and c.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if ($centroCosto!='') {
            $FILTRO .= " and c.fk_a023_num_centro_costo=$centroCosto";
        }
        if ($tipoNomina!='') {
            $FILTRO .= " and c.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if ($tipoTrabajador!='') {
            $FILTRO .= " and c.fk_a006_num_miscelaneo_detalle_tipotrabajador=$tipoTrabajador";
        }
        if ($estado_registro!='') {
            $FILTRO .= " and b.num_estatus=$estado_registro";
        }
        if ($situacionTrabajo!='') {
            $FILTRO .= " and a.num_estatus=$situacionTrabajo";
        }
        if ($fechaInicio!='' && $fecha_fin!='') {
            $f1=explode("-",$fechaInicio);
            $f2=explode("-",$fecha_fin);
            $fi = $f1[2]."-".$f1[1]."-".$f1[0];
            $ff = $f2[2]."-".$f2[1]."-".$f2[0];

            $FILTRO .= " and f.fec_ingreso >= '$fi' and f.fec_ingreso <= '$ff'";
        }


        $buscarTrabajador = $this->_db->query(
            "SELECT
                a.pk_num_empleado,
                b.pk_num_persona,
                b.ind_cedula_documento,
                b.ind_nombre1,
                b.ind_nombre2,
                b.ind_apellido1,
                b.ind_apellido2,
                b.ind_foto,
                c.fk_a001_num_organismo,
                d.ind_descripcion_empresa,
                c.fk_a004_num_dependencia,
                e.ind_dependencia,
                c.fk_a023_num_centro_costo,
                c.fk_nmb001_num_tipo_nomina,
                c.fk_a006_num_miscelaneo_detalle_tipotrabajador,
                b.num_estatus AS estado_registro,
                a.num_estatus AS situacion_trabajo,
                f.fec_ingreso,
                g.ind_descripcion_cargo
            FROM
                rh_b001_empleado AS a
            INNER JOIN a003_persona AS b ON a.fk_a003_num_persona = b.pk_num_persona
            INNER JOIN rh_c076_empleado_organizacion AS c ON c.fk_rhb001_num_empleado = a.pk_num_empleado
            INNER JOIN a001_organismo AS d ON c.fk_a001_num_organismo = d.pk_num_organismo
            INNER JOIN a004_dependencia AS e ON c.fk_a004_num_dependencia = e.pk_num_dependencia
            INNER JOIN rh_c005_empleado_laboral AS f ON f.fk_rhb001_num_empleado = a.pk_num_empleado
            INNER JOIN rh_c063_puestos AS g ON f.fk_rhc063_num_puestos_cargo = g.pk_num_puestos
            WHERE $FILTRO
            "
        );

        $buscarTrabajador->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarTrabajador->fetchAll();
    }

    // Método que permite listar los centros de costos
    public function metListarCentroCosto()
    {
        $centroCosto =  $this->_db->query(
            "select pk_num_centro_costo, ind_descripcion_centro_costo from a023_centro_costo"
        );
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }
    
    
    public function metEliminarTelefono($persona_telefono)
    {
        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM a007_persona_telefono WHERE pk_num_telefono=:pk_num_telefono
            ");
        $eliminar->execute(array(
            'pk_num_telefono'=>$persona_telefono
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $persona_telefono;
        }
    }

    public function metEliminarDireccion($persona_direccion)
    {
        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM a036_persona_direccion WHERE pk_num_direccion_persona=:pk_num_direccion_persona
            ");
        $eliminar->execute(array(
            'pk_num_direccion_persona'=>$persona_direccion
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $persona_direccion;
        }
    }

    public function metEliminarLicencia($persona_licencia)
    {
        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM a032_persona_licencia WHERE pk_num_licencia=:pk_num_licencia
            ");
        $eliminar->execute(array(
            'pk_num_licencia'=>$persona_licencia
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $persona_licencia;
        }
    }


    public function metCodigoEmpleado()
    {
        $con = $this->_db->query("SELECT MAX(cod_empleado) as cod FROM rh_b001_empleado");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();
        $cod = $resultado['cod'];
        $codigoFinal=(int) ($cod+1);
        $codigoFinal=(string) str_repeat("0", 6-strlen($codigoFinal)).$codigoFinal;
        return $codigoFinal;

    }


    #permite anular una operacion del empleado
    public function metAnularOperacion($idEmpleado,$estado,$motivo)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        $actualizo = $this->_db->prepare(
            "UPDATE
                rh_b001_empleado
             SET
                ind_estado_aprobacion=:ind_estado_aprobacion,
                fec_anulado=:fec_anulado,
                ind_motivo_anulacion=:ind_motivo_anulacion,
                fk_rhb001_num_empleado_anulado='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE
                pk_num_empleado='$idEmpleado'
            "
        );

        $actualizo->execute(array(
            ':ind_estado_aprobacion' => $estado,
            ':fec_anulado' => date("Y-m-d"),
            ':ind_motivo_anulacion' => strtoupper($motivo)
        ));

        $error = $actualizo->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
        }




        if(strcmp($estado,'RV')==0){

        }



    }




}//fin clase
