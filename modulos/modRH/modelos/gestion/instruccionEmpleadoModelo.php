<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class instruccionEmpleadoModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR INSTRUCCION DEL EMPLEADO
    public function metListarInstruccion($empleado,$tipo)
    {

        //VERIFICO LA INSTRUCCIONLISTO SEGUN EL TIPO
        if(strcmp($tipo,'CARRERAS')==0){
            $carreras = $this->_db->query("
                SELECT
                rh_c018_empleado_instruccion.pk_num_empleado_instruccion,
                rh_c018_empleado_instruccion.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                grado_instruccion.ind_nombre_detalle as grado_instruccion,
                rh_c064_nivel_grado_instruccion.ind_nombre_nivel_grado,
                rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_area,
                colegiatura.ind_nombre_detalle AS area_formacion,
                rh_c057_profesiones.ind_nombre_profesion,
                rh_c018_empleado_instruccion.fk_rhc057_num_profesion,
                rh_c040_institucion.ind_nombre_institucion,
                rh_c018_empleado_instruccion.fk_rhc040_num_institucion,
                rh_c018_empleado_instruccion.fec_graduacion,
                rh_c018_empleado_instruccion.fec_ingreso,
                rh_c018_empleado_instruccion.fec_egreso,
                rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_colegiatura,
                colegiatura.ind_nombre_detalle AS colegiatura,
                rh_c018_empleado_instruccion.ind_colegiatura,
                rh_c018_empleado_instruccion.txt_observaciones,
                rh_c018_empleado_instruccion.fec_ultima_modificacion,
                rh_c018_empleado_instruccion.fk_a018_num_seguridad_usuario
                FROM
                rh_c018_empleado_instruccion
                INNER JOIN rh_b001_empleado ON rh_c018_empleado_instruccion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS grado_instruccion ON rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_gradoinst = grado_instruccion.pk_num_miscelaneo_detalle
                INNER JOIN rh_c064_nivel_grado_instruccion ON rh_c018_empleado_instruccion.fk_rhc064_num_nivel_grado = rh_c064_nivel_grado_instruccion.pk_num_nivel_grado
                INNER JOIN a006_miscelaneo_detalle AS area ON area.pk_num_miscelaneo_detalle = rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_area
                INNER JOIN rh_c057_profesiones ON rh_c057_profesiones.pk_num_profesion = rh_c018_empleado_instruccion.fk_rhc057_num_profesion
                INNER JOIN rh_c040_institucion ON rh_c018_empleado_instruccion.fk_rhc040_num_institucion = rh_c040_institucion.pk_num_institucion
                LEFT JOIN a006_miscelaneo_detalle AS colegiatura ON area.pk_num_miscelaneo_detalle = rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_colegiatura
                WHERE fk_rhb001_num_empleado='$empleado'
            ");
            $carreras->setFetchMode(PDO::FETCH_ASSOC);
            return $carreras->fetchAll();
        }
        if(strcmp($tipo,'OTROSESTUDIOS')==0){
            $otros = $this->_db->query("
                SELECT
                rh_c047_empleado_curso.pk_num_empleado_curso,
                rh_c047_empleado_curso.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c047_empleado_curso.fk_rhc048_num_curso,
                rh_c048_curso.ind_nombre_curso,
                rh_c047_empleado_curso.fk_a006_num_miscelaneo_detalle_tipocurso,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                rh_c047_empleado_curso.fk_rhc040_num_institucion,
                rh_c040_institucion.ind_nombre_institucion,
                rh_c047_empleado_curso.num_horas,
                rh_c047_empleado_curso.fec_desde,
                rh_c047_empleado_curso.fec_hasta,
                rh_c047_empleado_curso.ind_periodo,
                rh_c047_empleado_curso.txt_observaciones,
                rh_c047_empleado_curso.fec_ultima_modificacion,
                rh_c047_empleado_curso.fk_a018_num_seguridad_usuario
                FROM
                rh_c047_empleado_curso
                INNER JOIN rh_b001_empleado ON rh_c047_empleado_curso.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN rh_c048_curso ON rh_c047_empleado_curso.fk_rhc048_num_curso = rh_c048_curso.pk_num_curso
                INNER JOIN a006_miscelaneo_detalle ON rh_c047_empleado_curso.fk_a006_num_miscelaneo_detalle_tipocurso = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                INNER JOIN rh_c040_institucion ON rh_c047_empleado_curso.fk_rhc040_num_institucion = rh_c040_institucion.pk_num_institucion
                WHERE fk_rhb001_num_empleado='$empleado'
            ");
            $otros->setFetchMode(PDO::FETCH_ASSOC);
            return $otros->fetchAll();
        }
        if(strcmp($tipo,'IDIOMAS')==0){
            $idiomas = $this->_db->query("
                    SELECT
                    rh_c029_empleado_idioma.pk_num_empleado_idioma,
                    rh_c029_empleado_idioma.fk_rhb001_num_empleado,
                    a003_persona.pk_num_persona,
                    a003_persona.ind_nombre1,
                    a003_persona.ind_nombre2,
                    a003_persona.ind_apellido1,
                    a003_persona.ind_apellido2,
                    rh_c029_empleado_idioma.fk_rhc013_num_idioma,
                    rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_lectura,
                    lectura.ind_nombre_detalle AS lectura,
                    rh_c013_idioma.ind_nombre_idioma,
                    oral.ind_nombre_detalle AS oral,
                    rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_oral,
                    rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_escritura,
                    escritura.ind_nombre_detalle AS escritura,
                    rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_general,
                    general.ind_nombre_detalle AS general,
                    rh_c029_empleado_idioma.fec_ultima_modificacion,
                    rh_c029_empleado_idioma.fk_a018_num_seguridad_usuario
                    FROM
                    rh_c029_empleado_idioma
                    INNER JOIN rh_b001_empleado ON rh_c029_empleado_idioma.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                    INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                    INNER JOIN rh_c013_idioma ON rh_c029_empleado_idioma.fk_rhc013_num_idioma = rh_c013_idioma.pk_num_idioma
                    INNER JOIN a006_miscelaneo_detalle AS lectura ON rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_lectura = lectura.pk_num_miscelaneo_detalle
                    INNER JOIN a006_miscelaneo_detalle AS oral ON oral.pk_num_miscelaneo_detalle = rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_oral
                    INNER JOIN a006_miscelaneo_detalle AS escritura ON escritura.pk_num_miscelaneo_detalle = rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_escritura
                    INNER JOIN a006_miscelaneo_detalle AS general ON rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_general = general.pk_num_miscelaneo_detalle
                    WHERE fk_rhb001_num_empleado='$empleado'
            ");
            $idiomas->setFetchMode(PDO::FETCH_ASSOC);
            return $idiomas->fetchAll();
        }


    }


    #PERMITE MOSTRAR LA INSTRUCCION DEL EMPLEADO
    public function metMostrarInstruccion($idInstruccion,$tipo)
    {

        if(strcmp($tipo,'CARRERAS')==0){

            $carreras = $this->_db->query("
                SELECT
                rh_c018_empleado_instruccion.pk_num_empleado_instruccion,
                rh_c018_empleado_instruccion.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                grado_instruccion.ind_nombre_detalle as grado_instruccion,
                rh_c064_nivel_grado_instruccion.ind_nombre_nivel_grado,
                rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_area,
                colegiatura.ind_nombre_detalle AS area_formacion,
                rh_c057_profesiones.ind_nombre_profesion,
                rh_c018_empleado_instruccion.fk_rhc057_num_profesion,
                rh_c040_institucion.ind_nombre_institucion,
                rh_c018_empleado_instruccion.fk_rhc040_num_institucion,
                rh_c018_empleado_instruccion.fec_graduacion,
                rh_c018_empleado_instruccion.fec_ingreso,
                rh_c018_empleado_instruccion.fec_egreso,
                rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_colegiatura,
                colegiatura.ind_nombre_detalle AS colegiatura,
                rh_c018_empleado_instruccion.ind_colegiatura,
                rh_c018_empleado_instruccion.txt_observaciones,
                rh_c018_empleado_instruccion.fec_ultima_modificacion,
                rh_c018_empleado_instruccion.fk_a018_num_seguridad_usuario,
                rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_gradoinst,
                rh_c018_empleado_instruccion.fk_rhc064_num_nivel_grado
                FROM
                rh_c018_empleado_instruccion
                INNER JOIN rh_b001_empleado ON rh_c018_empleado_instruccion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS grado_instruccion ON rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_gradoinst = grado_instruccion.pk_num_miscelaneo_detalle
                INNER JOIN rh_c064_nivel_grado_instruccion ON rh_c018_empleado_instruccion.fk_rhc064_num_nivel_grado = rh_c064_nivel_grado_instruccion.pk_num_nivel_grado
                INNER JOIN a006_miscelaneo_detalle AS area ON area.pk_num_miscelaneo_detalle = rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_area
                INNER JOIN rh_c057_profesiones ON rh_c057_profesiones.pk_num_profesion = rh_c018_empleado_instruccion.fk_rhc057_num_profesion
                INNER JOIN rh_c040_institucion ON rh_c018_empleado_instruccion.fk_rhc040_num_institucion = rh_c040_institucion.pk_num_institucion
                LEFT JOIN a006_miscelaneo_detalle AS colegiatura ON area.pk_num_miscelaneo_detalle = rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_colegiatura
                WHERE pk_num_empleado_instruccion='$idInstruccion'
            ");
            $carreras->setFetchMode(PDO::FETCH_ASSOC);
            return $carreras->fetch();

        }
        if(strcmp($tipo,'OTROSESTUDIOS')==0){
            $otros = $this->_db->query("
                SELECT
                rh_c047_empleado_curso.pk_num_empleado_curso,
                rh_c047_empleado_curso.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c047_empleado_curso.fk_rhc048_num_curso,
                rh_c048_curso.ind_nombre_curso,
                rh_c047_empleado_curso.fk_a006_num_miscelaneo_detalle_tipocurso,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                rh_c047_empleado_curso.fk_rhc040_num_institucion,
                rh_c040_institucion.ind_nombre_institucion,
                rh_c047_empleado_curso.num_horas,
                rh_c047_empleado_curso.fec_desde,
                rh_c047_empleado_curso.fec_hasta,
                rh_c047_empleado_curso.ind_periodo,
                rh_c047_empleado_curso.txt_observaciones,
                rh_c047_empleado_curso.fec_ultima_modificacion,
                rh_c047_empleado_curso.fk_a018_num_seguridad_usuario
                FROM
                rh_c047_empleado_curso
                INNER JOIN rh_b001_empleado ON rh_c047_empleado_curso.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN rh_c048_curso ON rh_c047_empleado_curso.fk_rhc048_num_curso = rh_c048_curso.pk_num_curso
                INNER JOIN a006_miscelaneo_detalle ON rh_c047_empleado_curso.fk_a006_num_miscelaneo_detalle_tipocurso = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                INNER JOIN rh_c040_institucion ON rh_c047_empleado_curso.fk_rhc040_num_institucion = rh_c040_institucion.pk_num_institucion
                WHERE pk_num_empleado_curso='$idInstruccion'
            ");
            $otros->setFetchMode(PDO::FETCH_ASSOC);
            return $otros->fetch();

        }
        if(strcmp($tipo,'IDIOMAS')==0){

            $idiomas = $this->_db->query("
                    SELECT
                    rh_c029_empleado_idioma.pk_num_empleado_idioma,
                    rh_c029_empleado_idioma.fk_rhb001_num_empleado,
                    a003_persona.pk_num_persona,
                    a003_persona.ind_nombre1,
                    a003_persona.ind_nombre2,
                    a003_persona.ind_apellido1,
                    a003_persona.ind_apellido2,
                    rh_c029_empleado_idioma.fk_rhc013_num_idioma,
                    rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_lectura,
                    lectura.ind_nombre_detalle AS lectura,
                    rh_c013_idioma.ind_nombre_idioma,
                    oral.ind_nombre_detalle AS oral,
                    rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_oral,
                    rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_escritura,
                    escritura.ind_nombre_detalle AS escritura,
                    rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_general,
                    general.ind_nombre_detalle AS general,
                    rh_c029_empleado_idioma.fec_ultima_modificacion,
                    rh_c029_empleado_idioma.fk_a018_num_seguridad_usuario
                    FROM
                    rh_c029_empleado_idioma
                    INNER JOIN rh_b001_empleado ON rh_c029_empleado_idioma.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                    INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                    INNER JOIN rh_c013_idioma ON rh_c029_empleado_idioma.fk_rhc013_num_idioma = rh_c013_idioma.pk_num_idioma
                    INNER JOIN a006_miscelaneo_detalle AS lectura ON rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_lectura = lectura.pk_num_miscelaneo_detalle
                    INNER JOIN a006_miscelaneo_detalle AS oral ON oral.pk_num_miscelaneo_detalle = rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_oral
                    INNER JOIN a006_miscelaneo_detalle AS escritura ON escritura.pk_num_miscelaneo_detalle = rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_escritura
                    INNER JOIN a006_miscelaneo_detalle AS general ON rh_c029_empleado_idioma.fk_a006_num_miscelaneo_detalle_general = general.pk_num_miscelaneo_detalle
                    WHERE pk_num_empleado_idioma='$idInstruccion'
            ");
            $idiomas->setFetchMode(PDO::FETCH_ASSOC);
            return $idiomas->fetch();

        }


    }

    #PERMITE INSERTAR INSTRUCCION DEL EMPLEADO
    public function metRegistrarInstruccion($empleado,$tipo,$datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        if(strcmp($tipo,'CARRERAS')==0){

            $f_g = explode("-",$datos[5]);
            $f_i = explode("-",$datos[6]);
            $f_e = explode("-",$datos[7]);

            if($datos[8]==NULL){
                $datos[8]=NULL;
            }

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c018_empleado_instruccion
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fk_a006_num_miscelaneo_detalle_gradoinst=:fk_a006_num_miscelaneo_detalle_gradoinst,
                    fk_rhc064_num_nivel_grado=:fk_rhc064_num_nivel_grado,
                    fk_a006_num_miscelaneo_detalle_area=:fk_a006_num_miscelaneo_detalle_area,
                    fk_rhc057_num_profesion=:fk_rhc057_num_profesion,
                    fk_rhc040_num_institucion=:fk_rhc040_num_institucion,
                    fec_graduacion=:fec_graduacion,
                    fec_ingreso=:fec_ingreso,
                    fec_egreso=:fec_egreso,
                    fk_a006_num_miscelaneo_detalle_colegiatura=:fk_a006_num_miscelaneo_detalle_colegiatura,
                    ind_colegiatura=:ind_colegiatura,
                    txt_observaciones=:txt_observaciones,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle_gradoinst'  => $datos[0],
                ':fk_rhc064_num_nivel_grado'  => $datos[1],
                ':fk_a006_num_miscelaneo_detalle_area'  => $datos[2],
                ':fk_rhc057_num_profesion' => $datos[3],
                ':fk_rhc040_num_institucion' => $datos[4],
                ':fec_graduacion' => $f_g[2]."-".$f_g[1]."-".$f_g[0],
                ':fec_ingreso' => $f_i[2]."-".$f_i[1]."-".$f_i[0],
                ':fec_egreso' => $f_e[2]."-".$f_e[1]."-".$f_e[0],
                ':fk_a006_num_miscelaneo_detalle_colegiatura' => $datos[8],
                ':ind_colegiatura' => $datos[9],
                ':txt_observaciones' => $datos[10]
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{

                $this->_db->commit();
                return $idRegistro;

            }

        }
        if(strcmp($tipo,'OTROSESTUDIOS')==0){

            $f_d = explode("-",$datos[4]);
            $f_h = explode("-",$datos[5]);

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c047_empleado_curso
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fk_rhc048_num_curso=:fk_rhc048_num_curso,
                    fk_a006_num_miscelaneo_detalle_tipocurso=:fk_a006_num_miscelaneo_detalle_tipocurso,
                    fk_rhc040_num_institucion=:fk_rhc040_num_institucion,
                    num_horas=:num_horas,
                    fec_desde=:fec_desde,
                    fec_hasta=:fec_hasta,
                    ind_periodo=:ind_periodo,
                    txt_observaciones=:txt_observaciones,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_rhc048_num_curso'  => $datos[0],
                ':fk_a006_num_miscelaneo_detalle_tipocurso'  => $datos[2],
                ':fk_rhc040_num_institucion'  => $datos[1],
                ':num_horas' => $datos[3],
                ':fec_desde' => $f_d[2]."-".$f_d[1]."-".$f_d[0],
                ':fec_hasta' => $f_h[2]."-".$f_h[1]."-".$f_h[0],
                ':ind_periodo' => $datos[6]."-".$datos[7],
                ':txt_observaciones' => $datos[8]
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{

                $this->_db->commit();
                return $idRegistro;

            }

        }
        if(strcmp($tipo,'IDIOMAS')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c029_empleado_idioma
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fk_rhc013_num_idioma=:fk_rhc013_num_idioma,
                    fk_a006_num_miscelaneo_detalle_lectura=:fk_a006_num_miscelaneo_detalle_lectura,
                    fk_a006_num_miscelaneo_detalle_oral=:fk_a006_num_miscelaneo_detalle_oral,
                    fk_a006_num_miscelaneo_detalle_escritura=:fk_a006_num_miscelaneo_detalle_escritura,
                    fk_a006_num_miscelaneo_detalle_general=:fk_a006_num_miscelaneo_detalle_general,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_rhc013_num_idioma'  => $datos[0],
                ':fk_a006_num_miscelaneo_detalle_lectura'  => $datos[1],
                ':fk_a006_num_miscelaneo_detalle_oral'  => $datos[2],
                ':fk_a006_num_miscelaneo_detalle_escritura' => $datos[3],
                ':fk_a006_num_miscelaneo_detalle_general' => $datos[4]
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{

                $this->_db->commit();
                return $idRegistro;

            }

        }

    }

    #PERMITE MODIFICAR INSTRUCCION DEL EMPLEADO
    public function metModificarInstruccion($idInstruccion,$tipo,$datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        if(strcmp($tipo,'CARRERAS')==0){

            $f_g = explode("-",$datos[5]);
            $f_i = explode("-",$datos[6]);
            $f_e = explode("-",$datos[7]);

            if($datos[8]==NULL){
                $datos[8]=NULL;
            }

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $ActualizarRegistro = $this->_db->prepare(
                "UPDATE
                    rh_c018_empleado_instruccion
                 SET
                    fk_a006_num_miscelaneo_detalle_gradoinst=:fk_a006_num_miscelaneo_detalle_gradoinst,
                    fk_rhc064_num_nivel_grado=:fk_rhc064_num_nivel_grado,
                    fk_a006_num_miscelaneo_detalle_area=:fk_a006_num_miscelaneo_detalle_area,
                    fk_rhc057_num_profesion=:fk_rhc057_num_profesion,
                    fk_rhc040_num_institucion=:fk_rhc040_num_institucion,
                    fec_graduacion=:fec_graduacion,
                    fec_ingreso=:fec_ingreso,
                    fec_egreso=:fec_egreso,
                    fk_a006_num_miscelaneo_detalle_colegiatura=:fk_a006_num_miscelaneo_detalle_colegiatura,
                    ind_colegiatura=:ind_colegiatura,
                    txt_observaciones=:txt_observaciones,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_empleado_instruccion='$idInstruccion'
              ");

            #execute — Ejecuta una sentencia preparada
            $ActualizarRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle_gradoinst'  => $datos[0],
                ':fk_rhc064_num_nivel_grado'  => $datos[1],
                ':fk_a006_num_miscelaneo_detalle_area'  => $datos[2],
                ':fk_rhc057_num_profesion' => $datos[3],
                ':fk_rhc040_num_institucion' => $datos[4],
                ':fec_graduacion' => $f_g[2]."-".$f_g[1]."-".$f_g[0],
                ':fec_ingreso' => $f_i[2]."-".$f_i[1]."-".$f_i[0],
                ':fec_egreso' => $f_e[2]."-".$f_e[1]."-".$f_e[0],
                ':fk_a006_num_miscelaneo_detalle_colegiatura' => $datos[8],
                ':ind_colegiatura' => $datos[9],
                ':txt_observaciones' => $datos[10]
            ));

            $error= $ActualizarRegistro->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{
                $this->_db->commit();
                return $idInstruccion;
            }

        }
        if(strcmp($tipo,'OTROSESTUDIOS')==0){

            $f_d = explode("-",$datos[4]);
            $f_h = explode("-",$datos[5]);

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $ActualizarRegistro = $this->_db->prepare(
                "UPDATE
                    rh_c047_empleado_curso
                 SET
                    fk_rhc048_num_curso=:fk_rhc048_num_curso,
                    fk_a006_num_miscelaneo_detalle_tipocurso=:fk_a006_num_miscelaneo_detalle_tipocurso,
                    fk_rhc040_num_institucion=:fk_rhc040_num_institucion,
                    num_horas=:num_horas,
                    fec_desde=:fec_desde,
                    fec_hasta=:fec_hasta,
                    ind_periodo=:ind_periodo,
                    txt_observaciones=:txt_observaciones,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_empleado_curso='$idInstruccion'
              ");

            #execute — Ejecuta una sentencia preparada
            $ActualizarRegistro->execute(array(
                ':fk_rhc048_num_curso'  => $datos[0],
                ':fk_a006_num_miscelaneo_detalle_tipocurso'  => $datos[2],
                ':fk_rhc040_num_institucion'  => $datos[1],
                ':num_horas' => $datos[3],
                ':fec_desde' => $f_d[2]."-".$f_d[1]."-".$f_d[0],
                ':fec_hasta' => $f_h[2]."-".$f_h[1]."-".$f_h[0],
                ':ind_periodo' => $datos[6]."-".$datos[7],
                ':txt_observaciones' => $datos[8]
            ));

            $error= $ActualizarRegistro->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{
                $this->_db->commit();
                return $idInstruccion;
            }

        }
        if(strcmp($tipo,'IDIOMAS')==0){

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $ActualizarRegistro = $this->_db->prepare(
                "UPDATE
                    rh_c029_empleado_idioma
                 SET
                    fk_rhc013_num_idioma=:fk_rhc013_num_idioma,
                    fk_a006_num_miscelaneo_detalle_lectura=:fk_a006_num_miscelaneo_detalle_lectura,
                    fk_a006_num_miscelaneo_detalle_oral=:fk_a006_num_miscelaneo_detalle_oral,
                    fk_a006_num_miscelaneo_detalle_escritura=:fk_a006_num_miscelaneo_detalle_escritura,
                    fk_a006_num_miscelaneo_detalle_general=:fk_a006_num_miscelaneo_detalle_general,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE pk_num_empleado_idioma='$idInstruccion'
              ");

            #execute — Ejecuta una sentencia preparada
            $ActualizarRegistro->execute(array(
                ':fk_rhc013_num_idioma'  => $datos[0],
                ':fk_a006_num_miscelaneo_detalle_lectura'  => $datos[1],
                ':fk_a006_num_miscelaneo_detalle_oral'  => $datos[2],
                ':fk_a006_num_miscelaneo_detalle_escritura' => $datos[3],
                ':fk_a006_num_miscelaneo_detalle_general' => $datos[4]
            ));

            $error= $ActualizarRegistro->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{
                $this->_db->commit();
                return $idInstruccion;
            }

        }

    }

    #PERMITE ELIMINAR INSTRUCCION DEL EMPLEADO
    public function metEliminarInstruccion($idInstruccion,$tipo)
    {

        $this->_db->beginTransaction();

        if(strcmp($tipo,'CARRERAS')==0){

            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c018_empleado_instruccion WHERE pk_num_empleado_instruccion=:pk_num_empleado_instruccion
            ");
            $eliminar->execute(array(
                'pk_num_empleado_instruccion'=>$idInstruccion
            ));
            $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idInstruccion;
            }

        }
        if(strcmp($tipo,'OTROSESTUDIOS')==0){

            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c047_empleado_curso WHERE pk_num_empleado_curso=:pk_num_empleado_curso
            ");
            $eliminar->execute(array(
                'pk_num_empleado_curso'=>$idInstruccion
            ));
            $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idInstruccion;
            }

        }
        if(strcmp($tipo,'IDIOMAS')==0){

            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c029_empleado_idioma WHERE pk_num_empleado_idioma=:pk_num_empleado_idioma
            ");
            $eliminar->execute(array(
                'pk_num_empleado_idioma'=>$idInstruccion
            ));
            $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idInstruccion;
            }

        }





    }




}//fin clase
