<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class retencionesJudicialesModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR TODAS LA RETENCIONES JUDICIALES
    public function metListarRetenciones()
    {
        $con = $this->_db->query("
                SELECT
                    rh_c102_retencion_judicial.pk_num_retencion,
                    rh_c102_retencion_judicial.fk_a001_num_organismo,
                    a001_organismo.ind_descripcion_empresa,
                    rh_c102_retencion_judicial.fk_rhb001_num_empleado,
                    persona_empleado.ind_cedula_documento AS cedula_empleado,
                    CONCAT_WS(
                        ' ',
                        persona_empleado.ind_nombre1,
                        persona_empleado.ind_nombre2,
                        persona_empleado.ind_apellido1,
                        persona_empleado.ind_apellido2
                    ) AS nombre_empleado,
                    rh_c102_retencion_judicial.fk_a003_num_persona_demandante,
                    persona_demandante.ind_cedula_documento AS cedula_demandante,
                    CONCAT_WS(
                        ' ',
                        persona_demandante.ind_nombre1,
                        persona_demandante.ind_nombre2,
                        persona_demandante.ind_apellido1,
                        persona_demandante.ind_apellido2
                    ) AS nombre_demandante,
                    rh_c102_retencion_judicial.ind_expediente,
                    rh_c102_retencion_judicial.fec_fecha_resolucion,
                    rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tiporetencion,
                    tipo_retencion.cod_detalle AS cod_detalle_tiporetencion,
                    tipo_retencion.ind_nombre_detalle AS nombre_tiporetencion,
                    rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tipopago,
                    tipo_pago.cod_detalle AS cod_detalle_tipopago,
                    tipo_pago.ind_nombre_detalle AS nombre_tipopago,
                    rh_c102_retencion_judicial.ind_juzgado,
                    rh_c102_retencion_judicial.txt_observaciones,
                    rh_c102_retencion_judicial.num_estatus,
                    rh_c102_retencion_judicial.fec_ultima_modificacion,
                    rh_c102_retencion_judicial.fk_a018_num_seguridad_usuario
                FROM
                    rh_c102_retencion_judicial
                INNER JOIN a001_organismo ON rh_c102_retencion_judicial.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN rh_b001_empleado ON rh_c102_retencion_judicial.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona AS persona_demandante ON rh_c102_retencion_judicial.fk_a003_num_persona_demandante = persona_demandante.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS tipo_retencion ON tipo_retencion.pk_num_miscelaneo_detalle = rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tiporetencion
                INNER JOIN a006_miscelaneo_detalle AS tipo_pago ON rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tipopago = tipo_pago.pk_num_miscelaneo_detalle
                INNER JOIN a003_persona AS persona_empleado ON persona_empleado.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    #PERMITE MOSTRAR LOS DATOS GENERALES DE LA RETENCION
    public function metMostrarRetenciones($idRetencion)
    {

        $con = $this->_db->query("
                SELECT
                    rh_c102_retencion_judicial.pk_num_retencion,
                    rh_c102_retencion_judicial.fk_a001_num_organismo,
                    a001_organismo.ind_descripcion_empresa,
                    rh_c102_retencion_judicial.fk_rhb001_num_empleado,
                    persona_empleado.ind_cedula_documento AS cedula_empleado,
                    CONCAT_WS(
                        ' ',
                        persona_empleado.ind_nombre1,
                        persona_empleado.ind_nombre2,
                        persona_empleado.ind_apellido1,
                        persona_empleado.ind_apellido2
                    ) AS nombre_empleado,
                    rh_c102_retencion_judicial.fk_a003_num_persona_demandante,
                    persona_demandante.ind_cedula_documento AS cedula_demandante,
                    CONCAT_WS(
                        ' ',
                        persona_demandante.ind_nombre1,
                        persona_demandante.ind_nombre2,
                        persona_demandante.ind_apellido1,
                        persona_demandante.ind_apellido2
                    ) AS nombre_demandante,
                    rh_c102_retencion_judicial.ind_expediente,
                    rh_c102_retencion_judicial.fec_fecha_resolucion,
                    rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tiporetencion,
                    tipo_retencion.cod_detalle AS cod_detalle_tiporetencion,
                    tipo_retencion.ind_nombre_detalle AS nombre_tiporetencion,
                    rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tipopago,
                    tipo_pago.cod_detalle AS cod_detalle_tipopago,
                    tipo_pago.ind_nombre_detalle AS nombre_tipopago,
                    rh_c102_retencion_judicial.ind_juzgado,
                    rh_c102_retencion_judicial.txt_observaciones,
                    rh_c102_retencion_judicial.num_estatus,
                    rh_c102_retencion_judicial.fec_ultima_modificacion,
                    rh_c102_retencion_judicial.fk_a018_num_seguridad_usuario
                FROM
                    rh_c102_retencion_judicial
                INNER JOIN a001_organismo ON rh_c102_retencion_judicial.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN rh_b001_empleado ON rh_c102_retencion_judicial.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona AS persona_demandante ON rh_c102_retencion_judicial.fk_a003_num_persona_demandante = persona_demandante.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS tipo_retencion ON tipo_retencion.pk_num_miscelaneo_detalle = rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tiporetencion
                INNER JOIN a006_miscelaneo_detalle AS tipo_pago ON rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tipopago = tipo_pago.pk_num_miscelaneo_detalle
                INNER JOIN a003_persona AS persona_empleado ON persona_empleado.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                WHERE pk_num_retencion='$idRetencion'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    #PERMITE MOSTRAR EL DETALLE  DE LA RETENCION
    public function metMostrarRetencionesDetalle($idRetencion)
    {

        $con = $this->_db->query("
                SELECT
                    rh_c103_retencion_judicial_conceptos.pk_num_retencion_conceptos,
                    rh_c103_retencion_judicial_conceptos.fk_rhc102_num_retencion,
                    rh_c103_retencion_judicial_conceptos.fk_a006_num_miscelaneo_detalle_tipodescuento,
                    rh_c103_retencion_judicial_conceptos.ind_tipo_descuento,
                    rh_c103_retencion_judicial_conceptos.fk_nmb002_num_concepto,
                    rh_c103_retencion_judicial_conceptos.ind_concepto,
                    rh_c103_retencion_judicial_conceptos.num_descuento,
                    rh_c103_retencion_judicial_conceptos.fec_ultima_modificacion,
                    rh_c103_retencion_judicial_conceptos.fk_a018_num_seguridad_usuario
                FROM
                  rh_c103_retencion_judicial_conceptos
                WHERE rh_c103_retencion_judicial_conceptos.fk_rhc102_num_retencion='$idRetencion'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();

    }

    #PERMITE INSERTAR LA RETENCIONES
    public function metRegistrarRetenciones($datosGenerales,$datosConceptos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #ORGANIZO LOS DATOS OBTENIDOS DEL ARRAY DE DATOS
        $ORGANISMO  = $datosGenerales[0];
        $EMPLEADO   = $datosGenerales[1];
        $DEMANDANTE = $datosGenerales[2];
        $EXPEDIENTE = $datosGenerales[3];
        $FECHA_RESO = explode("-",$datosGenerales[4]);
        $TIPO_RET   = $datosGenerales[5];
        $TIPO_PAGO  = $datosGenerales[6];
        $JUZGADO    = $datosGenerales[7];
        $OBSERVACIONES = $datosGenerales[8];
        $ESTATUS    = $datosGenerales[9];

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c102_retencion_judicial
             SET
                fk_a001_num_organismo=:fk_a001_num_organismo,
                fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                fk_a003_num_persona_demandante=:fk_a003_num_persona_demandante,
                ind_expediente=:ind_expediente,
                fec_fecha_resolucion=:fec_fecha_resolucion,
                fk_a006_num_miscelaneo_detalle_tiporetencion=:fk_a006_num_miscelaneo_detalle_tiporetencion,
                fk_a006_num_miscelaneo_detalle_tipopago=:fk_a006_num_miscelaneo_detalle_tipopago,
                ind_juzgado=:ind_juzgado,
                txt_observaciones=:txt_observaciones,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_a001_num_organismo'  => $ORGANISMO,
            ':fk_rhb001_num_empleado'  => $EMPLEADO,
            ':fk_a003_num_persona_demandante'  => $DEMANDANTE,
            ':ind_expediente'  => $EXPEDIENTE,
            ':fec_fecha_resolucion' => $FECHA_RESO[2]."-".$FECHA_RESO[1]."-".$FECHA_RESO[0],
            ':fk_a006_num_miscelaneo_detalle_tiporetencion' => $TIPO_RET,
            ':fk_a006_num_miscelaneo_detalle_tipopago' => $TIPO_PAGO,
            ':ind_juzgado' => $JUZGADO,
            ':txt_observaciones' => mb_strtoupper($OBSERVACIONES,'utf8'),
            ':num_estatus'  => $ESTATUS
        ));

        $errorRetenciones = $NuevoRegistro->errorInfo();
        //obtengo id de la retencion registrada
        $idRegistro= $this->_db->lastInsertId();

        if(!empty($errorRetenciones[1]) && !empty($errorRetenciones[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $errorRetenciones;

        }else{

            $TIPO_DESC   = $datosConceptos[0];
            $IDCONCEPTO  = $datosConceptos[1];
            $CONCEPTO    = $datosConceptos[2];
            $DESCUENTO   = $datosConceptos[3];
            //INSERTO DETALLE DE LA RETENCION
            if(isset($IDCONCEPTO) && !empty($IDCONCEPTO)){

                $NuevoDetalle = $this->_db->prepare(
                    "INSERT INTO
                        rh_c103_retencion_judicial_conceptos
                     SET
                        fk_rhc102_num_retencion=:fk_rhc102_num_retencion,
                        fk_a006_num_miscelaneo_detalle_tipodescuento=:fk_a006_num_miscelaneo_detalle_tipodescuento,
                        ind_tipo_descuento=:ind_tipo_descuento,
                        fk_nmb002_num_concepto=:fk_nmb002_num_concepto,
                        ind_concepto=:ind_concepto,
                        num_descuento=:num_descuento,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                      ");

                for ($i = 0; $i < count($IDCONCEPTO); $i++) {

                    #para saber el codigo detalle del micelaneo tipo descuento
                    $con = $this->_db->query("
                        SELECT cod_detalle from a006_miscelaneo_detalle where pk_num_miscelaneo_detalle='$TIPO_DESC[$i]'
                    ");
                    $con->setFetchMode(PDO::FETCH_ASSOC);
                    $resultado = $con->fetch();

                    $NuevoDetalle->execute(array(
                        ':fk_rhc102_num_retencion' => $idRegistro,
                        ':fk_a006_num_miscelaneo_detalle_tipodescuento' => $TIPO_DESC[$i],
                        ':ind_tipo_descuento' => $resultado['cod_detalle'],
                        ':fk_nmb002_num_concepto' => $IDCONCEPTO[$i],
                        ':ind_concepto' => $CONCEPTO[$i],
                        ':num_descuento' => str_replace(',','.',$DESCUENTO[$i])
                    ));
                }


            }
            if($IDCONCEPTO){
                $errorDetalle = $NuevoDetalle->errorInfo();
            }else{
                $errorDetalle =array(1=>null,2=>null);
            }
            if(!empty($errorDetalle[1]) && !empty($errorDetalle[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $errorDetalle;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }

        }

    }

    #PERMITE MODIFICAR LA CARGA FAMILIAR DEL EMPLEADO
    public function metModificarRetenciones($idRetencion,$datosGenerales,$datosConceptos)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #ORGANIZO LOS DATOS OBTENIDOS DEL ARRAY DE DATOS
        $ORGANISMO  = $datosGenerales[0];
        $EMPLEADO   = $datosGenerales[1];
        $DEMANDANTE = $datosGenerales[2];
        $EXPEDIENTE = $datosGenerales[3];
        $FECHA_RESO = explode("-",$datosGenerales[4]);
        $TIPO_RET   = $datosGenerales[5];
        $TIPO_PAGO  = $datosGenerales[6];
        $JUZGADO    = $datosGenerales[7];
        $OBSERVACIONES = $datosGenerales[8];
        $ESTATUS    = $datosGenerales[9];

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "UPDATE
                rh_c102_retencion_judicial
             SET
                fk_a001_num_organismo=:fk_a001_num_organismo,
                fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                fk_a003_num_persona_demandante=:fk_a003_num_persona_demandante,
                ind_expediente=:ind_expediente,
                fec_fecha_resolucion=:fec_fecha_resolucion,
                fk_a006_num_miscelaneo_detalle_tiporetencion=:fk_a006_num_miscelaneo_detalle_tiporetencion,
                fk_a006_num_miscelaneo_detalle_tipopago=:fk_a006_num_miscelaneo_detalle_tipopago,
                ind_juzgado=:ind_juzgado,
                txt_observaciones=:txt_observaciones,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                WHERE pk_num_retencion='$idRetencion'
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_a001_num_organismo'  => $ORGANISMO,
            ':fk_rhb001_num_empleado'  => $EMPLEADO,
            ':fk_a003_num_persona_demandante'  => $DEMANDANTE,
            ':ind_expediente'  => $EXPEDIENTE,
            ':fec_fecha_resolucion' => $FECHA_RESO[2]."-".$FECHA_RESO[1]."-".$FECHA_RESO[0],
            ':fk_a006_num_miscelaneo_detalle_tiporetencion' => $TIPO_RET,
            ':fk_a006_num_miscelaneo_detalle_tipopago' => $TIPO_PAGO,
            ':ind_juzgado' => mb_strtoupper($JUZGADO,'utf8'),
            ':txt_observaciones' => mb_strtoupper($OBSERVACIONES,'utf8'),
            ':num_estatus'  => $ESTATUS
        ));

        $errorRetenciones = $NuevoRegistro->errorInfo();

        if(!empty($errorRetenciones[1]) && !empty($errorRetenciones[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $errorRetenciones;

        }else {

            $eliminar=$this->_db->prepare(
                "DELETE FROM rh_c103_retencion_judicial_conceptos WHERE  fk_rhc102_num_retencion=:fk_rhc102_num_retencion"
            );
            $eliminar->execute(array(':fk_rhc102_num_retencion'=>$idRetencion));

            $TIPO_DESC = $datosConceptos[0];
            $IDCONCEPTO = $datosConceptos[1];
            $CONCEPTO = $datosConceptos[2];
            $DESCUENTO = $datosConceptos[3];

            //INSERTO DETALLE DE LA RETENCION
            if (isset($IDCONCEPTO) && !empty($IDCONCEPTO)) {

                $NuevoDetalle = $this->_db->prepare(
                    "INSERT INTO
                        rh_c103_retencion_judicial_conceptos
                     SET
                        fk_rhc102_num_retencion=:fk_rhc102_num_retencion,
                        fk_a006_num_miscelaneo_detalle_tipodescuento=:fk_a006_num_miscelaneo_detalle_tipodescuento,
                        ind_tipo_descuento=:ind_tipo_descuento,
                        fk_nmb002_num_concepto=:fk_nmb002_num_concepto,
                        ind_concepto=:ind_concepto,
                        num_descuento=:num_descuento,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                      ");

                for ($i = 0; $i < count($IDCONCEPTO); $i++) {

                    #para saber el codigo detalle del micelaneo tipo descuento
                    $con = $this->_db->query("
                        SELECT cod_detalle from a006_miscelaneo_detalle where pk_num_miscelaneo_detalle='$TIPO_DESC[$i]'
                    ");
                    $con->setFetchMode(PDO::FETCH_ASSOC);
                    $resultado = $con->fetch();

                    $NuevoDetalle->execute(array(
                        ':fk_rhc102_num_retencion' => $idRetencion,
                        ':fk_a006_num_miscelaneo_detalle_tipodescuento' => $TIPO_DESC[$i],
                        ':ind_tipo_descuento' => $resultado['cod_detalle'],
                        ':fk_nmb002_num_concepto' => $IDCONCEPTO[$i],
                        ':ind_concepto' => $CONCEPTO[$i],
                        ':num_descuento' => str_replace(',','.', preg_replace('/[^0-9 ,]/i', '', $DESCUENTO[$i]))
                    ));
                }


            }
            if ($IDCONCEPTO) {
                $errorDetalle = $NuevoDetalle->errorInfo();
            } else {
                $errorDetalle = array(1 => null, 2 => null);
            }
            if (!empty($errorDetalle[1]) && !empty($errorDetalle[2])) {
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $errorDetalle;
            } else {
                $this->_db->commit();
                return $idRetencion;
            }

        }

    }

    #permite eliminar conceptos de la retencion ya registrada
    public function metEliminarRetencionesDetalle($id)
    {
        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c103_retencion_judicial_conceptos WHERE pk_num_retencion_conceptos=:pk_num_retencion_conceptos
            ");
        $eliminar->execute(array(
            'pk_num_retencion_conceptos'=>$id
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $id;
        }
    }


    #PERMITE BUSCAR LAS RETENCIONES SEGUN CRITERIO DE BUSQUEDA DEL FILTRO
    public function metBuscarRetenciones($pkNumOrganismo, $buscar, $estado_registro, $fechaInicio, $fecha_fin)
    {
        if ($pkNumOrganismo!='') {
            $FILTRO = "rh_c102_retencion_judicial.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($buscar!='') {
            $FILTRO .= " and persona_empleado.ind_nombre1 LIKE '%$buscar%' ";
        }
        if ($estado_registro!='') {
            $FILTRO .= " and rh_c102_retencion_judicial.num_estatus='$estado_registro'";
        }
        if ($fechaInicio!='' && $fecha_fin!='') {
            $f1=explode("-",$fechaInicio);
            $f2=explode("-",$fecha_fin);
            $fi = $f1[2]."-".$f1[1]."-".$f1[0];
            $ff = $f2[2]."-".$f2[1]."-".$f2[0];

            $FILTRO .= " and rh_c102_retencion_judicial.fec_fecha_resolucion >= '$fi' and rh_c102_retencion_judicial.fec_fecha_resolucion <= '$ff'";
        }


        $buscarRetenciones = $this->_db->query("
                SELECT
                    rh_c102_retencion_judicial.pk_num_retencion,
                    rh_c102_retencion_judicial.fk_a001_num_organismo,
                    a001_organismo.ind_descripcion_empresa,
                    rh_c102_retencion_judicial.fk_rhb001_num_empleado,
                    persona_empleado.ind_cedula_documento AS cedula_empleado,
                    CONCAT_WS(
                        ' ',
                        persona_empleado.ind_nombre1,
                        persona_empleado.ind_nombre2,
                        persona_empleado.ind_apellido1,
                        persona_empleado.ind_apellido2
                    ) AS nombre_empleado,
                    rh_c102_retencion_judicial.fk_a003_num_persona_demandante,
                    persona_demandante.ind_cedula_documento AS cedula_demandante,
                    CONCAT_WS(
                        ' ',
                        persona_demandante.ind_nombre1,
                        persona_demandante.ind_nombre2,
                        persona_demandante.ind_apellido1,
                        persona_demandante.ind_apellido2
                    ) AS nombre_demandante,
                    rh_c102_retencion_judicial.ind_expediente,
                    rh_c102_retencion_judicial.fec_fecha_resolucion,
                    rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tiporetencion,
                    tipo_retencion.cod_detalle AS cod_detalle_tiporetencion,
                    tipo_retencion.ind_nombre_detalle AS nombre_tiporetencion,
                    rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tipopago,
                    tipo_pago.cod_detalle AS cod_detalle_tipopago,
                    tipo_pago.ind_nombre_detalle AS nombre_tipopago,
                    rh_c102_retencion_judicial.ind_juzgado,
                    rh_c102_retencion_judicial.txt_observaciones,
                    rh_c102_retencion_judicial.num_estatus,
                    rh_c102_retencion_judicial.fec_ultima_modificacion,
                    rh_c102_retencion_judicial.fk_a018_num_seguridad_usuario
                FROM
                    rh_c102_retencion_judicial
                INNER JOIN a001_organismo ON rh_c102_retencion_judicial.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN rh_b001_empleado ON rh_c102_retencion_judicial.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona AS persona_demandante ON rh_c102_retencion_judicial.fk_a003_num_persona_demandante = persona_demandante.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS tipo_retencion ON tipo_retencion.pk_num_miscelaneo_detalle = rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tiporetencion
                INNER JOIN a006_miscelaneo_detalle AS tipo_pago ON rh_c102_retencion_judicial.fk_a006_num_miscelaneo_detalle_tipopago = tipo_pago.pk_num_miscelaneo_detalle
                INNER JOIN a003_persona AS persona_empleado ON persona_empleado.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                WHERE $FILTRO
             ");

        $buscarRetenciones->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarRetenciones->fetchAll();
    }


}//fin clase
