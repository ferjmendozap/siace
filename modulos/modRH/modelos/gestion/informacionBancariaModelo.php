<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class informacionBancariaModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR TODA LA INFORMACION BANCARIA DEL EMPLEADO
    public function metListarInformacionBancaria($empleado)
    {
        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    vl_rh_empleado_informacion_bancaria
                WHERE pk_num_empleado='$empleado'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE MOSTRAR LA INFORMACION BANCARIA EN EL FORMULARIO
    public function metMostrarInformacionBancaria($idInformacionBancaria)
    {

        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    vl_rh_empleado_informacion_bancaria
                WHERE pk_num_empleado_banco='$idInformacionBancaria'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    #PERMITE INSERTAR INFORMACION BANCARIA DEL EMPLEADO
    public function metRegistrarInformacionBancaria($empleado,$banco,$tipocta,$tipoapo,$cuenta,$sueldo,$flag_principal)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #para verificar si existe la cuenta introducida
        $con = $this->_db->query("select * from rh_c012_empleado_banco where ind_cuenta='$cuenta'");
        if($con->fetchColumn() > 0){#hay filas con la misma numero de cuenta

            $x = 'rep';
            return $x;

        }
        else{

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c012_empleado_banco
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fk_a006_num_miscelaneo_detalle_banco=:fk_a006_num_miscelaneo_detalle_banco,
                    fk_a006_num_miscelaneo_detalle_tipocta=:fk_a006_num_miscelaneo_detalle_tipocta,
                    fk_a006_num_miscelaneo_detalle_tipoapo=:fk_a006_num_miscelaneo_detalle_tipoapo,
                    ind_cuenta=:ind_cuenta,
                    num_sueldo_mensual=:num_sueldo_mensual,
                    num_flag_principal=:num_flag_principal,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle_banco'  => $banco,
                ':fk_a006_num_miscelaneo_detalle_tipocta'  => $tipocta,
                ':fk_a006_num_miscelaneo_detalle_tipoapo'  => $tipoapo,
                ':ind_cuenta'  => $cuenta,
                ':num_sueldo_mensual' => $sueldo,
                ':num_flag_principal' => $flag_principal
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{

                $this->_db->commit();
                return $idRegistro;

            }
        }

    }

    #PERMITE MODIFICAR LA CARGA FAMILIAR DEL EMPLEADO
    public function metModificarInformacionBancaria($idInformacionBancaria,$banco,$tipocta,$tipoapo,$cuenta,$sueldo,$flag_principal)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $ActualizarRegistro = $this->_db->prepare(
            "UPDATE
                rh_c012_empleado_banco
             SET
                fk_a006_num_miscelaneo_detalle_banco=:fk_a006_num_miscelaneo_detalle_banco,
                fk_a006_num_miscelaneo_detalle_tipocta=:fk_a006_num_miscelaneo_detalle_tipocta,
                fk_a006_num_miscelaneo_detalle_tipoapo=:fk_a006_num_miscelaneo_detalle_tipoapo,
                ind_cuenta=:ind_cuenta,
                num_sueldo_mensual=:num_sueldo_mensual,
                num_flag_principal=:num_flag_principal,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE pk_num_empleado_banco='$idInformacionBancaria'
              ");

        #execute — Ejecuta una sentencia preparada
        $ActualizarRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_banco'  => $banco,
            ':fk_a006_num_miscelaneo_detalle_tipocta'  => $tipocta,
            ':fk_a006_num_miscelaneo_detalle_tipoapo'  => $tipoapo,
            ':ind_cuenta'  => $cuenta,
            ':num_sueldo_mensual' => $sueldo,
            ':num_flag_principal' => $flag_principal
        ));

        $error= $ActualizarRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $error;

        }else{
            $this->_db->commit();
            return $idInformacionBancaria;
        }

    }

    #PERMITE ELIMINAR INFORMACION BANCARIA DEL EMPLEADO
    public function metEliminarInformacionBancaria($idInformacionBancaria)
    {

        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c012_empleado_banco WHERE pk_num_empleado_banco=:pk_num_empleado_banco
            ");
        $eliminar->execute(array(
            'pk_num_empleado_banco'=>$idInformacionBancaria
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idInformacionBancaria;
        }

    }




}//fin clase
