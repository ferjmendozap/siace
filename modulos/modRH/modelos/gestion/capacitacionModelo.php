<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class capacitacionModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }


    #PERMITE LISTAR TODAS LA CAPACITACIONES PARA MOSTRARLAS EN UN LISTADO
    public function metListarCapacitacion($tipo)
    {

        if(strcmp($tipo,'TODOS')==0){
            $condicion='';
        }
        if(strcmp($tipo,'PE')==0){
            $condicion='WHERE estado_capacitacion="PE"';
        }
        if(strcmp($tipo,'AP')==0){
            $condicion='WHERE estado_capacitacion="AP" OR estado_capacitacion="IN"';
        }
        if(strcmp($tipo,'IN')==0){
            $condicion='WHERE estado_capacitacion="IN"';
        }
        if(strcmp($tipo,'TE')==0){
            $condicion='WHERE estado_capacitacion="TE"';
        }

        $con = $this->_db->query("
             SELECT
             *
             FROM
             vl_rh_capacitaciones $condicion
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE BUSCAR LAS CAPACITACIONES SEGUN CRITERIO DE BUSQUEDA DEL FILTRO
    public function metBuscarCapacitaciones($pkNumOrganismo, $curso, $institucion, $tipocap, $modalidad, $estado_registro, $fechaInicio, $fecha_fin)
    {
        if ($pkNumOrganismo!='') {
            $FILTRO = "fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($curso!='') {
            $FILTRO .= " and fk_rhc048_num_curso=$curso";
        }
        if ($institucion!='') {
            $FILTRO .= " and fk_rhc040_centro_estudio=$institucion";
        }
        if ($tipocap!='') {
            $FILTRO .= " and fk_a006_num_miscelnaeo_detalle_tipocap=$tipocap";
        }
        if ($modalidad!='') {
            $FILTRO .= " and fk_a006_num_miscelnaeo_detalle_modalidad=$modalidad";
        }
        if ($estado_registro!='') {
            $FILTRO .= " and estado_capacitacion='$estado_registro'";
        }
        if ($fechaInicio!='' && $fecha_fin!='') {
            $f1=explode("-",$fechaInicio);
            $f2=explode("-",$fecha_fin);
            $fi = $f1[2]."-".$f1[1]."-".$f1[0];
            $ff = $f2[2]."-".$f2[1]."-".$f2[0];

            $FILTRO .= " and fec_fecha_inicio >= '$fi' and fec_fecha_termino <= '$ff'";
        }


        $buscarCapacitaciones = $this->_db->query("
             SELECT
             *
             FROM
             vl_rh_capacitaciones WHERE $FILTRO
             ");

        $buscarCapacitaciones->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarCapacitaciones->fetchAll();
    }

    #permite obtener los datos del empleado
    public function metObtenerDatosEmpleado($idEmpleado)
    {

        $con =  $this->_db->query("

                SELECT
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                rh_c076_empleado_organizacion.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa AS organismo,
                a003_persona.fk_a006_num_miscelaneo_detalle_sexo,
                sexo.ind_nombre_detalle AS sexo,
                a003_persona.fec_nacimiento,
                rh_c076_empleado_organizacion.fk_a004_num_dependencia,
                a004_dependencia.ind_dependencia AS dependencia,
                rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo,
                rh_c063_puestos.ind_descripcion_cargo AS cargo,
                rh_c063_puestos.num_sueldo_basico,
                rh_c005_empleado_laboral.fec_ingreso,
                rh_b001_empleado.num_estatus AS estatus_empleado,
                a003_persona.num_estatus AS estatus_persona,
                rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador,
                tipo_trabajador.ind_nombre_detalle AS tipo_trabajador
                FROM
                rh_b001_empleado
                INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN a006_miscelaneo_detalle AS sexo ON a003_persona.fk_a006_num_miscelaneo_detalle_sexo = sexo.pk_num_miscelaneo_detalle
                INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                WHERE rh_b001_empleado.pk_num_empleado='$idEmpleado'

        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    /*
     * permite mostrar los datos de una capacitacion en especifico
     * $tipo: valores posibles PR, EM, HO, FU, GA
     * PR: pricipales
     * EM: empleados en la capacitacion
     * HO: horario de la capacitacion
     * FU: fundamentos
     * GA: gastos
     * UB: ubicacion
     */
    public function metMostrarCapacitaciones($idCapacitacion,$tipo)
    {

        if(strcmp($tipo,'PR')==0){
            $con =  $this->_db->query("
                SELECT * FROM vl_rh_capacitaciones WHERE pk_num_capacitacion='$idCapacitacion'
            ");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
        }
        if(strcmp($tipo,'FU')==0){
            $con =  $this->_db->query("
                SELECT * FROM vl_rh_capacitaciones_fundamentos WHERE fk_rhc097_num_capacitacion='$idCapacitacion'
            ");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
        }
        if(strcmp($tipo,'EM')==0){
            $con =  $this->_db->query("
                SELECT * FROM vl_rh_capacitaciones_empleados WHERE fk_rhc097_num_capacitacion='$idCapacitacion'
            ");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();
        }
        if(strcmp($tipo,'HO')==0){
            $con =  $this->_db->query("
                SELECT * FROM vl_rh_capacitaciones_horarios WHERE fk_rhc097_num_capacitacion='$idCapacitacion'
                GROUP BY fec_fecha
            ");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();
        }
        if(strcmp($tipo,'UB')==0){
            $con =  $this->_db->query("
                SELECT * FROM vl_rh_capacitaciones_ubicacion WHERE pk_num_capacitacion='$idCapacitacion'
            ");
            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();
        }




    }

    #permite registrar una capacitacion
    public function metRegistrarCapacitacion($datosCapacitacion,$datosFundamentos,$datosParticipantes,$datosHorarios)
    {

        $this->_db->beginTransaction();


        $regCapacitacion = $this->_db->prepare("
                     INSERT INTO
                          rh_c097_capacitacion
                     SET
                          fk_a001_num_organismo=:fk_a001_num_organismo,
                          fec_anio=:fec_anio,
                          fk_rhb001_empleado_solicitante=:fk_rhb001_empleado_solicitante,
                          fk_rhc048_num_curso=:fk_rhc048_num_curso,
                          fk_rhc040_centro_estudio=:fk_rhc040_centro_estudio,
                          fk_a010_num_ciudad=:fk_a010_num_ciudad,
                          fk_a006_num_miscelnaeo_detalle_tipocap=:fk_a006_num_miscelnaeo_detalle_tipocap,
                          fk_a006_num_miscelnaeo_detalle_modalidad=:fk_a006_num_miscelnaeo_detalle_modalidad,
                          fk_a006_num_miscelnaeo_detalle_origen=:fk_a006_num_miscelnaeo_detalle_origen,
                          ind_periodo=:ind_periodo,
                          num_vacantes=:num_vacantes,
                          num_participantes=:num_participantes,
                          fec_fecha_inicio=:fec_fecha_inicio,
                          fec_fecha_termino=:fec_fecha_termino,
                          ind_expositor=:ind_expositor,
                          ind_telefonos=:ind_telefonos,
                          flag_costos=:flag_costos,
                          txt_observaciones=:txt_observaciones,
                          num_monto_estimado=:num_monto_estimado,
                          num_monto_asumido=:num_monto_asumido,
                          num_total_dias=:num_total_dias,
                          ind_total_horas=:ind_total_horas,
                          ind_estado=:ind_estado,
                          fec_fecha_creado=:fec_fecha_creado,
                          fk_rhb001_num_empleado_creado=:fk_rhb001_num_empleado_creado,
                          fec_ultima_modificacion=NOW(),
                          fk_a018_num_seguridad_usuario='$this->atIdUsuario'

                ");

        /*para darle el formato adecuado a las fecha para la base de datos*/
        $aux1 = explode("-",$datosCapacitacion[12]);
        $aux2 = explode("-",$datosCapacitacion[13]);

        /*limpiar montos*/
        $aux3 = str_replace(".","",$datosCapacitacion[18]);
        $aux4 = str_replace(".","",$datosCapacitacion[19]);

        #execute — Ejecuta una sentencia preparada
        $regCapacitacion->execute(array(
            ':fk_a001_num_organismo' => $datosCapacitacion[0],
            ':fec_anio' => $datosCapacitacion[1],
            ':fk_rhb001_empleado_solicitante' => $datosCapacitacion[2],
            ':fk_rhc048_num_curso' => $datosCapacitacion[3],
            ':fk_rhc040_centro_estudio' => $datosCapacitacion[4],
            ':fk_a010_num_ciudad' => $datosCapacitacion[5],
            ':fk_a006_num_miscelnaeo_detalle_tipocap' => $datosCapacitacion[6],
            ':fk_a006_num_miscelnaeo_detalle_modalidad' => $datosCapacitacion[7],
            ':fk_a006_num_miscelnaeo_detalle_origen' => $datosCapacitacion[8],
            ':ind_periodo' => $datosCapacitacion[9],
            ':num_vacantes' => $datosCapacitacion[10],
            ':num_participantes' => $datosCapacitacion[11],
            ':fec_fecha_inicio' => $aux1[2]."-".$aux1[1]."-".$aux1[0],
            ':fec_fecha_termino' => $aux2[2]."-".$aux2[1]."-".$aux2[0],
            ':ind_expositor' => $datosCapacitacion[14],
            ':ind_telefonos' => $datosCapacitacion[15],
            ':flag_costos' => $datosCapacitacion[16],
            ':txt_observaciones' => $datosCapacitacion[17],
            ':num_monto_estimado' => str_replace(",",".",$aux3),
            ':num_monto_asumido' => str_replace(",",".",$aux4),
            ':num_total_dias' => $datosCapacitacion[20],
            ':ind_total_horas' => $datosCapacitacion[21],
            ':ind_estado' => $datosCapacitacion[22],
            ':fec_fecha_creado' => $datosCapacitacion[23],
            ':fk_rhb001_num_empleado_creado' => $datosCapacitacion[24]
        ));

        $errorRegCapacitacion = $regCapacitacion->errorInfo();

        //obtengo id de la Capacitacion
        $idCapacitacion= $this->_db->lastInsertId();

        if(!empty($errorRegCapacitacion[1]) && !empty($errorRegCapacitacion[2])){
            $this->_db->rollBack();
            return $errorRegCapacitacion;
        }else{


            /* si es exitoso el registro GUARDO LOS FUNDAMENTOS*/
            $regFundamentos = $this->_db->prepare("
                     INSERT INTO
                          rh_c098_capacitacion_fundamentos
                     SET
                          fk_rhc097_num_capacitacion=:fk_rhc097_num_capacitacion,
                          txt_fundamentacion_1=:txt_fundamentacion_1,
                          txt_fundamentacion_2=:txt_fundamentacion_2,
                          txt_fundamentacion_3=:txt_fundamentacion_3,
                          txt_fundamentacion_4=:txt_fundamentacion_4,
                          txt_fundamentacion_5=:txt_fundamentacion_5,
                          txt_fundamentacion_6=:txt_fundamentacion_6
                ");

            $regFundamentos->execute(array(
                ':fk_rhc097_num_capacitacion' => $idCapacitacion,
                ':txt_fundamentacion_1' => $datosFundamentos[0],
                ':txt_fundamentacion_2' => $datosFundamentos[1],
                ':txt_fundamentacion_3' => $datosFundamentos[2],
                ':txt_fundamentacion_4' => $datosFundamentos[3],
                ':txt_fundamentacion_5' => $datosFundamentos[4],
                ':txt_fundamentacion_6' => $datosFundamentos[5]
            ));

            $errorRegFundamentos = $regFundamentos->errorInfo();

            if(!empty($errorRegFundamentos[1]) && !empty($errorRegFundamentos[2])){
                $this->_db->rollBack();
                return $errorRegFundamentos;
            }else{

                /* si es exitoso el registro GUARDO LOS PARTICIPANTES O BENEFICIARIOS DE LA CAPACITACION*/

                $lista_empleado = explode("#",$datosParticipantes[0]);
                array_pop($lista_empleado);//eliminamos el ultimo elemento del array (que esta en blanco)

                $totalAux = count($lista_empleado);

                for($i=0; $i<$totalAux; $i++){

                    $con =  $this->_db->query("
                        SELECT
                            rh_c076_empleado_organizacion.fk_a004_num_dependencia
                        FROM
                            rh_b001_empleado
                        INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                        WHERE rh_b001_empleado.pk_num_empleado=$lista_empleado[$i]
                    ");


                    $con->setFetchMode(PDO::FETCH_ASSOC);
                    $result = $con->fetch();


                    $regParticipantes = $this->_db->prepare("
                     INSERT INTO
                          rh_c099_capacitacion_empleados
                     SET
                          fk_rhc097_num_capacitacion=:fk_rhc097_num_capacitacion,
                          fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                          fk_a004_num_dependencia=:fk_a004_num_dependencia,
                          num_asistencias=:num_asistencias,
                          ind_horas_asistencia=:ind_horas_asistencia,
                          fec_ultima_modificacion=NOW(),
                          fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    ");

                    $regParticipantes->execute(array(
                        ':fk_rhc097_num_capacitacion' => $idCapacitacion,
                        ':fk_rhb001_num_empleado' => $lista_empleado[$i],
                        ':fk_a004_num_dependencia' => $result['fk_a004_num_dependencia'],
                        'num_asistencias' => $datosHorarios[4],
                        'ind_horas_asistencia' => $datosHorarios[5]

                    ));


                }//fin for participantes

                $errorRegParticipantes = $regParticipantes->errorInfo();

                if(!empty($errorRegParticipantes[1]) && !empty($errorRegParticipantes[2])){
                    $this->_db->rollBack();
                    return $errorRegParticipantes;
                }else{

                    /*si OK guardo los horarios*/

                    /*recorro el array de empleados participantes*/

                    $regHorarios = $this->_db->prepare("
                         INSERT INTO
                              rh_c101_capacitacion_horario
                         SET
                              fk_rhc097_num_capacitacion=:fk_rhc097_num_capacitacion,
                              fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                              fec_fecha=:fec_fecha,
                              fec_hora_ini=:fec_hora_ini,
                              fec_hora_fin=:fec_hora_fin,
                              ind_total_horas=:ind_total_horas,
                              fec_ultima_modificacion=NOW(),
                              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                        ");

                    for($i=0; $i<$totalAux; $i++) {


                        if($datosHorarios[0]){

                            for($j = 0; $j < count($datosHorarios[0]); $j++){

                                $aux = explode("-",$datosHorarios[0][$j]);

                                $regHorarios->execute(array(
                                    ':fk_rhc097_num_capacitacion' => $idCapacitacion,
                                    ':fk_rhb001_num_empleado' => $lista_empleado[$i],
                                    ':fec_fecha' => $aux[2]."-".$aux[1]."-".$aux[0],
                                    ':fec_hora_ini' => $this->formatoHora24($datosHorarios[1][$j]),
                                    ':fec_hora_fin' => $this->formatoHora24($datosHorarios[2][$j]),
                                    ':ind_total_horas' => $datosHorarios[3][$j]
                                ));


                            }// fin for j interno


                        }//fin if for


                    }//fin for

                    if($datosHorarios[0]){
                        $errorRegHorarios = $regHorarios->errorInfo();
                    }else{
                        $errorRegHorarios =array(1=>null,2=>null);
                    }

                    if(!empty($errorRegHorarios[1]) && !empty($errorRegHorarios[2])){
                        $this->_db->rollBack();
                        return $errorRegHorarios;
                    }else{
                        $this->_db->commit();
                        return $idCapacitacion;
                    }

                }

            }

        }

    }


    #permite modificar una capacitacion
    public function metModificarCapacitacion($idCapacitacion,$datosCapacitacion,$datosFundamentos,$datosParticipantes,$datosHorarios)
    {

        $this->_db->beginTransaction();

        $regCapacitacion = $this->_db->prepare("
                     UPDATE
                          rh_c097_capacitacion
                     SET
                          fk_a001_num_organismo=:fk_a001_num_organismo,
                          fec_anio=:fec_anio,
                          fk_rhb001_empleado_solicitante=:fk_rhb001_empleado_solicitante,
                          fk_rhc048_num_curso=:fk_rhc048_num_curso,
                          fk_rhc040_centro_estudio=:fk_rhc040_centro_estudio,
                          fk_a010_num_ciudad=:fk_a010_num_ciudad,
                          fk_a006_num_miscelnaeo_detalle_tipocap=:fk_a006_num_miscelnaeo_detalle_tipocap,
                          fk_a006_num_miscelnaeo_detalle_modalidad=:fk_a006_num_miscelnaeo_detalle_modalidad,
                          fk_a006_num_miscelnaeo_detalle_origen=:fk_a006_num_miscelnaeo_detalle_origen,
                          ind_periodo=:ind_periodo,
                          num_vacantes=:num_vacantes,
                          num_participantes=:num_participantes,
                          fec_fecha_inicio=:fec_fecha_inicio,
                          fec_fecha_termino=:fec_fecha_termino,
                          ind_expositor=:ind_expositor,
                          ind_telefonos=:ind_telefonos,
                          flag_costos=:flag_costos,
                          txt_observaciones=:txt_observaciones,
                          num_monto_estimado=:num_monto_estimado,
                          num_monto_asumido=:num_monto_asumido,
                          num_total_dias=:num_total_dias,
                          ind_total_horas=:ind_total_horas,
                          ind_estado=:ind_estado,
                          fec_fecha_creado=:fec_fecha_creado,
                          fk_rhb001_num_empleado_creado=:fk_rhb001_num_empleado_creado,
                          fec_ultima_modificacion=NOW(),
                          fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                     WHERE
                           pk_num_capacitacion = '$idCapacitacion'

                ");

        /*para darle el formato adecuado a las fecha para la base de datos*/
        $aux1 = explode("-",$datosCapacitacion[12]);
        $aux2 = explode("-",$datosCapacitacion[13]);

        /*limpiar montos*/
        $aux3 = str_replace(".","",$datosCapacitacion[18]);
        $aux4 = str_replace(".","",$datosCapacitacion[19]);

        #execute — Ejecuta una sentencia preparada
        $regCapacitacion->execute(array(
            ':fk_a001_num_organismo' => $datosCapacitacion[0],
            ':fec_anio' => $datosCapacitacion[1],
            ':fk_rhb001_empleado_solicitante' => $datosCapacitacion[2],
            ':fk_rhc048_num_curso' => $datosCapacitacion[3],
            ':fk_rhc040_centro_estudio' => $datosCapacitacion[4],
            ':fk_a010_num_ciudad' => $datosCapacitacion[5],
            ':fk_a006_num_miscelnaeo_detalle_tipocap' => $datosCapacitacion[6],
            ':fk_a006_num_miscelnaeo_detalle_modalidad' => $datosCapacitacion[7],
            ':fk_a006_num_miscelnaeo_detalle_origen' => $datosCapacitacion[8],
            ':ind_periodo' => $datosCapacitacion[9],
            ':num_vacantes' => $datosCapacitacion[10],
            ':num_participantes' => $datosCapacitacion[11],
            ':fec_fecha_inicio' => $aux1[2]."-".$aux1[1]."-".$aux1[0],
            ':fec_fecha_termino' => $aux2[2]."-".$aux2[1]."-".$aux2[0],
            ':ind_expositor' => $datosCapacitacion[14],
            ':ind_telefonos' => $datosCapacitacion[15],
            ':flag_costos' => $datosCapacitacion[16],
            ':txt_observaciones' => $datosCapacitacion[17],
            ':num_monto_estimado' => str_replace(",",".",$aux3),
            ':num_monto_asumido' => str_replace(",",".",$aux4),
            ':num_total_dias' => $datosCapacitacion[20],
            ':ind_total_horas' => $datosCapacitacion[21],
            ':ind_estado' => $datosCapacitacion[22],
            ':fec_fecha_creado' => $datosCapacitacion[23],
            ':fk_rhb001_num_empleado_creado' => $datosCapacitacion[24]
        ));

        $errorRegCapacitacion = $regCapacitacion->errorInfo();


        if(!empty($errorRegCapacitacion[1]) && !empty($errorRegCapacitacion[2])){
            $this->_db->rollBack();
            return $errorRegCapacitacion;
        }else{

            ##	fundamentos

            $regFundamentos = $this->_db->prepare("
                     UPDATE
                          rh_c098_capacitacion_fundamentos
                     SET
                          txt_fundamentacion_1=:txt_fundamentacion_1,
                          txt_fundamentacion_2=:txt_fundamentacion_2,
                          txt_fundamentacion_3=:txt_fundamentacion_3,
                          txt_fundamentacion_4=:txt_fundamentacion_4,
                          txt_fundamentacion_5=:txt_fundamentacion_5,
                          txt_fundamentacion_6=:txt_fundamentacion_6
                     WHERE fk_rhc097_num_capacitacion='$idCapacitacion'
                ");

            $regFundamentos->execute(array(
                ':txt_fundamentacion_1' => $datosFundamentos[0],
                ':txt_fundamentacion_2' => $datosFundamentos[1],
                ':txt_fundamentacion_3' => $datosFundamentos[2],
                ':txt_fundamentacion_4' => $datosFundamentos[3],
                ':txt_fundamentacion_5' => $datosFundamentos[4],
                ':txt_fundamentacion_6' => $datosFundamentos[5]
            ));

            $errorRegFundamentos = $regFundamentos->errorInfo();

            if(!empty($errorRegFundamentos[1]) && !empty($errorRegFundamentos[2])){
                $this->_db->rollBack();
                return $errorRegFundamentos;
            }else{

                ## participantes

                $eliminar=$this->_db->prepare("
                  DELETE FROM rh_c099_capacitacion_empleados WHERE  fk_rhc097_num_capacitacion=:fk_rhc097_num_capacitacion
                ");
                $eliminar->execute(array(
                    'fk_rhc097_num_capacitacion'=>$idCapacitacion
                ));
                $error=$eliminar->errorInfo();

                if(!empty($error[1]) && !empty($error[2])){
                    $this->_db->rollBack();
                    return $error;
                }else{

                    $lista_empleado = explode("#",$datosParticipantes[0]);
                    array_pop($lista_empleado);//eliminamos el ultimo elemento del array (que esta en blanco)

                    $totalAux = count($lista_empleado);

                    for($i=0; $i<$totalAux; $i++){

                        $con =  $this->_db->query("
                            SELECT
                                rh_c076_empleado_organizacion.fk_a004_num_dependencia
                            FROM
                                rh_b001_empleado
                            INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                            WHERE rh_b001_empleado.pk_num_empleado=$lista_empleado[$i]
                        ");


                        $con->setFetchMode(PDO::FETCH_ASSOC);
                        $result = $con->fetch();


                        $regParticipantes = $this->_db->prepare("
                         INSERT INTO
                              rh_c099_capacitacion_empleados
                         SET
                              fk_rhc097_num_capacitacion=:fk_rhc097_num_capacitacion,
                              fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                              fk_a004_num_dependencia=:fk_a004_num_dependencia,
                              num_asistencias=:num_asistencias,
                              ind_horas_asistencia=:ind_horas_asistencia,
                              fec_ultima_modificacion=NOW(),
                              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                        ");

                        $regParticipantes->execute(array(
                            ':fk_rhc097_num_capacitacion' => $idCapacitacion,
                            ':fk_rhb001_num_empleado' => $lista_empleado[$i],
                            ':fk_a004_num_dependencia' => $result['fk_a004_num_dependencia'],
                            'num_asistencias' => $datosHorarios[4],
                            'ind_horas_asistencia' => $datosHorarios[5]

                        ));


                    }//fin for participantes

                    $errorRegParticipantes = $regParticipantes->errorInfo();

                    if(!empty($errorRegParticipantes[1]) && !empty($errorRegParticipantes[2])){
                        $this->_db->rollBack();
                        return $errorRegParticipantes;
                    }else{

                        ## horario

                        $eliminar=$this->_db->prepare("
                          DELETE FROM rh_c101_capacitacion_horario WHERE  fk_rhc097_num_capacitacion=:fk_rhc097_num_capacitacion
                        ");
                        $eliminar->execute(array(
                            'fk_rhc097_num_capacitacion'=>$idCapacitacion
                        ));
                        $error=$eliminar->errorInfo();

                        if(!empty($error[1]) && !empty($error[2])){
                            $this->_db->rollBack();
                            return $error;
                        }else{


                            $regHorarios = $this->_db->prepare("
                             INSERT INTO
                                  rh_c101_capacitacion_horario
                             SET
                                  fk_rhc097_num_capacitacion=:fk_rhc097_num_capacitacion,
                                  fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                                  fec_fecha=:fec_fecha,
                                  fec_hora_ini=:fec_hora_ini,
                                  fec_hora_fin=:fec_hora_fin,
                                  ind_total_horas=:ind_total_horas,
                                  fec_ultima_modificacion=NOW(),
                                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                            ");

                            for($i=0; $i<$totalAux; $i++) {


                                if($datosHorarios[0]){

                                    for($j = 0; $j < count($datosHorarios[0]); $j++){

                                        $aux = explode("-",$datosHorarios[0][$j]);

                                        $regHorarios->execute(array(
                                            ':fk_rhc097_num_capacitacion' => $idCapacitacion,
                                            ':fk_rhb001_num_empleado' => $lista_empleado[$i],
                                            ':fec_fecha' => $aux[2]."-".$aux[1]."-".$aux[0],
                                            ':fec_hora_ini' => $this->formatoHora24($datosHorarios[1][$j]),
                                            ':fec_hora_fin' => $this->formatoHora24($datosHorarios[2][$j]),
                                            ':ind_total_horas' => $datosHorarios[3][$j]
                                        ));


                                    }// fin for j interno


                                }//fin if for


                            }//fin for

                            if($datosHorarios[0]){
                                $errorRegHorarios = $regHorarios->errorInfo();
                            }else{
                                $errorRegHorarios =array(1=>null,2=>null);
                            }

                            if(!empty($errorRegHorarios[1]) && !empty($errorRegHorarios[2])){
                                $this->_db->rollBack();
                                return $errorRegHorarios;
                            }else{
                                $this->_db->commit();
                                return $idCapacitacion;
                            }

                        }

                    }

                }

            }

        }

    }


    #permite aprobar la capacitacion
    public function metAprobarCapacitacion($datos)
    {

        $this->_db->beginTransaction();

        /*organizo los datos recibidos*/
        $idCapacitacion = $datos[0];
        $estado = $datos[1];
        $fecha  = $datos[2];
        $vacantes = $datos[3];
        $participantes = $datos[4];


        //valido que el numero de participantes no se mayor que el numero de vacantes

        if($vacantes >= $participantes){

            $regAprobacion = $this->_db->prepare("
             UPDATE
                  rh_c097_capacitacion
             SET
                  ind_estado=:ind_estado,
                  fec_fecha_aprobado=:fec_fecha_aprobado,
                  fk_rhb001_num_empleado_aprobado=:fk_rhb001_num_empleado_aprobado,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE  pk_num_capacitacion='$idCapacitacion'
            ");

            $regAprobacion->execute(array(
                ':ind_estado' => $estado,
                ':fec_fecha_aprobado' => $fecha,
                ':fk_rhb001_num_empleado_aprobado' => $this->atIdUsuario
            ));

            $errorRegAprobacion = $regAprobacion->errorInfo();

            if(!empty($errorRegAprobacion[1]) && !empty($errorRegAprobacion[2])){
                $this->_db->rollBack();
                return $errorRegAprobacion;
            }else{
                $this->_db->commit();
                return $idCapacitacion;
            }

        }else{

            return array('errorVacantes');

        }

    }


    #PERMITE INCIAR LA CAPACITACION
    public function metIniciarCapacitacion($datos)
    {
        /*inicio transaccion*/
        $this->_db->beginTransaction();

        $idCapacitacion = $datos[0];
        $estado = $datos[1];

        $regIniciar = $this->_db->prepare("
             UPDATE
                  rh_c097_capacitacion
             SET
                  ind_estado=:ind_estado,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE  pk_num_capacitacion='$idCapacitacion'
            ");

        $regIniciar->execute(array(
            ':ind_estado' => $estado
        ));

        $errorRegIniciar = $regIniciar->errorInfo();

        if(!empty($errorRegIniciar[1]) && !empty($errorRegIniciar[2])){
            $this->_db->rollBack();
            return $errorRegIniciar;
        }else{

            /*actualizo gasto empleados*/

            /*consulto el total de los gastos*/
            $gastos = $this->metMostrarGastos($idCapacitacion);

            $total = $gastos['num_total'];


            /*consulto el total de participantes del curso*/
            $con1 =  $this->_db->query("
                SELECT
                count(*) as participantes
                FROM
                    rh_c099_capacitacion_empleados
                WHERE fk_rhc097_num_capacitacion='$idCapacitacion'
            ");
            $con1->setFetchMode(PDO::FETCH_ASSOC);
            $resultado = $con1->fetch();

            /*gastos promediados por empleado*/
            $ImporteGastos = round(($total / $resultado['participantes']),6);

            /*actualizo gastos*/
            $regGastos = $this->_db->prepare("
             UPDATE
                  rh_c099_capacitacion_empleados
             SET
                  num_importe_gastos=:num_importe_gastos,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE  fk_rhc097_num_capacitacion='$idCapacitacion'
            ");

            $regGastos->execute(array(
                ':num_importe_gastos' => $ImporteGastos
            ));

            $errorRegGastos = $regGastos->errorInfo();

            if(!empty($errorRegGastos[1]) && !empty($errorRegGastos[2])){
                $this->_db->rollBack();
                return $errorRegGastos;
            }else{

                $this->_db->commit();
                return $idCapacitacion;

            }

        }

    }


    #PERMITE TERMINAR LA CAPACITACION
    public function metTerminarCapacitacion($datos)
    {
        /*inicio transaccion*/
        $this->_db->beginTransaction();

        $idCapacitacion = $datos[0];
        $estado = $datos[1];

        $regIniciar = $this->_db->prepare("
             UPDATE
                  rh_c097_capacitacion
             SET
                  ind_estado=:ind_estado,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE  pk_num_capacitacion='$idCapacitacion'
            ");

        $regIniciar->execute(array(
            ':ind_estado' => $estado
        ));

        $errorRegIniciar = $regIniciar->errorInfo();

        if(!empty($errorRegIniciar[1]) && !empty($errorRegIniciar[2])){
            $this->_db->rollBack();
            return $errorRegIniciar;
        }else{

            if($datos[3]){


                /*actualizo gastos*/
                $regGastos = $this->_db->prepare("
                 UPDATE
                      rh_c099_capacitacion_empleados
                 SET
                      num_dias_asistidos=:num_dias_asistidos,
                      ind_calificacion=:ind_calificacion,
                      flag_aprobado=:flag_aprobado,
                      fec_ultima_modificacion=NOW(),
                      fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  WHERE  fk_rhc097_num_capacitacion='$idCapacitacion' and fk_rhb001_num_empleado=:fk_rhb001_num_empleado
                ");

                for ($i = 0; $i < count($datos[3]); $i++) {
                    $regGastos->execute(array(
                        ':fk_rhb001_num_empleado' => $datos[2][$i],
                        ':num_dias_asistidos' => $datos[3][$i],
                        ':ind_calificacion' => $datos[5][$i],
                        ':flag_aprobado' => $datos[4][$i]
                    ));
                }

            }


            $errorRegGastos = $regGastos->errorInfo();

            if(!empty($errorRegGastos[1]) && !empty($errorRegGastos[2])){
                $this->_db->rollBack();
                return $errorRegGastos;
            }else{

                $this->_db->commit();
                return $idCapacitacion;

            }

        }

    }


    #PERMITE GUARDAR LOS GATOS DE LA CAPACITACION
    public function metGuardarGastos($capacitacion,$detalle,$fecha,$num_sub_total,$num_impuestos,$num_total)
    {
        /*inicio transacción*/
        $this->_db->beginTransaction();


        /*formateo la fecha recibida*/
        $aux = explode("-",$fecha);

        /*sql para el registro de gastos*/
        $regGastos = $this->_db->prepare("
                     INSERT INTO
                          rh_c100_capacitacion_gastos
                     SET
                          fk_rhc097_num_capacitacion=:fk_rhc097_num_capacitacion,
                          ind_detalle=:ind_detalle,
                          fec_fecha=:fec_fecha,
                          num_sub_total=:num_sub_total,
                          num_impuestos=:num_impuestos,
                          num_total=:num_total,
                          fec_ultima_modificacion=NOW(),
                          fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    ");

        $regGastos->execute(array(
            ':fk_rhc097_num_capacitacion' => $capacitacion,
            ':ind_detalle' => $detalle,
            ':fec_fecha' => $aux[2]."-".$aux[1]."-".$aux[0],
            ':num_sub_total' => $num_sub_total,
            ':num_impuestos' => $num_impuestos,
            ':num_total' => $num_total
        ));
        $idGasto = $this->_db->lastInsertId();
        $errorRegGastos = $regGastos->errorInfo();

        if(!empty($errorRegGastos[1]) && !empty($errorRegGastos[2])){
            $this->_db->rollBack();
            return $errorRegGastos;
        }else{
            $this->_db->commit();
            return $idGasto;
        }



    }


    #permite eliminar los gastos generados en la capacitacion
    public function metEliminarGastos($idCapacitacion,$idGasto)
    {
        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c100_capacitacion_gastos WHERE pk_num_capacitacion_gastos=:pk_num_capacitacion_gastos and fk_rhc097_num_capacitacion=:fk_rhc097_num_capacitacion
            ");
        $eliminar->execute(array(
            'pk_num_capacitacion_gastos'=>$idGasto,
            'fk_rhc097_num_capacitacion'=>$idCapacitacion,
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idGasto;
        }
    }

    #permite obterner la sumatoria totao de los gastos de la capacitacion
    public function metMostrarGastos($idCapacitacion)
    {

        $con =  $this->_db->query("
                SELECT
                rh_c100_capacitacion_gastos.fk_rhc097_num_capacitacion,
                rh_c100_capacitacion_gastos.ind_detalle,
                rh_c100_capacitacion_gastos.fec_fecha,
                Sum(rh_c100_capacitacion_gastos.num_sub_total) AS num_sub_total,
                Sum(rh_c100_capacitacion_gastos.num_impuestos) AS num_impuestos,
                Sum(rh_c100_capacitacion_gastos.num_total) AS num_total
                FROM
                    rh_c100_capacitacion_gastos
                WHERE rh_c100_capacitacion_gastos.fk_rhc097_num_capacitacion='$idCapacitacion'
                GROUP BY
                rh_c100_capacitacion_gastos.fk_rhc097_num_capacitacion
            ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
        
        
    }

    #permite listar los gastos de la capacitación
    public function metListadoGastos($idCapacitacion)
    {
        $con =  $this->_db->query("
                SELECT
                rh_c100_capacitacion_gastos.pk_num_capacitacion_gastos,
                rh_c100_capacitacion_gastos.fk_rhc097_num_capacitacion,
                rh_c100_capacitacion_gastos.ind_detalle,
                rh_c100_capacitacion_gastos.fec_fecha,
                rh_c100_capacitacion_gastos.num_sub_total,
                rh_c100_capacitacion_gastos.num_impuestos,
                rh_c100_capacitacion_gastos.num_total
                FROM
                    rh_c100_capacitacion_gastos
                WHERE rh_c100_capacitacion_gastos.fk_rhc097_num_capacitacion='$idCapacitacion'
            ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }



    #permite eliminar un empleado de la capacitacion
    public function metEliminarEmpleadoCapacitacion($idCapacitacion,$idEmpleado)
    {
        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c099_capacitacion_empleados WHERE fk_rhc097_num_capacitacion=:fk_rhc097_num_capacitacion and fk_rhb001_num_empleado=:fk_rhb001_num_empleado
            ");
        $eliminar->execute(array(
            'fk_rhc097_num_capacitacion'=>$idCapacitacion,
            'fk_rhb001_num_empleado'=> $idEmpleado
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCapacitacion;
        }
    }


    #permite actualizar la cantidad de participantes
    public function metActualizarParticipantesCapacitacion($idCapacitacion)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
                select count(*) as participantes from rh_c099_capacitacion_empleados WHERE fk_rhc097_num_capacitacion='$idCapacitacion'
             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $actualizar = $this->_db->prepare("
                     UPDATE
                          rh_c097_capacitacion
                     SET
                          num_participantes=:num_participantes,
                          fec_ultima_modificacion=NOW(),
                          fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    ");

        $actualizar->execute(array(
            'num_participantes'=>$resultado['participantes']
        ));

        $error = $actualizar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $resultado['participantes'];
        }

    }


    #permite ajustar la hora a formato 24
    public function formatoHora24($hora)
    {

        return date("H:i",strtotime($hora));

    }




    //obtener sueldo basico
    public function metObtenerSueldoBasico($idCargo)
    {

        $con = $this->_db->query("
                SELECT
                rh_c063_puestos.num_sueldo_basico,
                a006_miscelaneo_detalle.ind_nombre_detalle
                FROM
                rh_c063_puestos
                LEFT JOIN a006_miscelaneo_detalle ON (a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria)
                INNER JOIN a005_miscelaneo_maestro ON (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro = a005_miscelaneo_maestro.pk_num_miscelaneo_maestro and a005_miscelaneo_maestro.cod_maestro='CATCARGO')
                where rh_c063_puestos.pk_num_puestos='$idCargo'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();


    }





}//fin clase
