<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class documentosModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE LISTAR DOCUMENTOS DEL EMPLEADO
    public function metListarDocumentos($empleado)
    {
        $con = $this->_db->query("
                SELECT
                rh_c017_empleado_documentos.pk_num_empleado_documento,
                rh_c017_empleado_documentos.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c017_empleado_documentos.fk_a006_num_miscelaneo_detalle_documento,
                a006_miscelaneo_detalle.ind_nombre_detalle AS documento,
                rh_c017_empleado_documentos.num_flag_entregado,
                rh_c017_empleado_documentos.fec_entrega,
                rh_c017_empleado_documentos.fec_vencimiento,
                rh_c017_empleado_documentos.num_flag_carga,
                rh_c017_empleado_documentos.fk_c015_num_carga_familiar,
                persona_carga_familiar.ind_nombre1 AS nombre1_carga,
                persona_carga_familiar.ind_nombre2 AS nombre2_carga,
                persona_carga_familiar.ind_apellido1 AS apellido1_carga,
                persona_carga_familiar.ind_apellido2 AS apellido2_carga,
                rh_c015_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco,
                parentesco.ind_nombre_detalle AS parentesco,
                rh_c017_empleado_documentos.txt_observaciones,
                rh_c017_empleado_documentos.fec_ultima_modificacion,
                rh_c017_empleado_documentos.fk_a018_num_seguridad_usuario
                FROM
                rh_c017_empleado_documentos
                INNER JOIN rh_b001_empleado ON rh_c017_empleado_documentos.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle ON rh_c017_empleado_documentos.fk_a006_num_miscelaneo_detalle_documento = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                LEFT JOIN rh_c015_carga_familiar ON rh_c017_empleado_documentos.fk_c015_num_carga_familiar = rh_c015_carga_familiar.pk_num_carga
                LEFT JOIN a003_persona AS persona_carga_familiar ON rh_c015_carga_familiar.fk_a003_num_persona = persona_carga_familiar.pk_num_persona
                LEFT JOIN a006_miscelaneo_detalle AS parentesco ON rh_c015_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco = parentesco.pk_num_miscelaneo_detalle
                WHERE rh_c017_empleado_documentos.fk_rhb001_num_empleado='$empleado'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #PERMITE MOSTRAR DOCUMENTOS DEL EMPLEADO
    public function metMostrarDocumentos($idDocumento)
    {

        $con = $this->_db->query("
                SELECT
                rh_c017_empleado_documentos.pk_num_empleado_documento,
                rh_c017_empleado_documentos.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                rh_c017_empleado_documentos.fk_a006_num_miscelaneo_detalle_documento,
                a006_miscelaneo_detalle.ind_nombre_detalle AS documento,
                rh_c017_empleado_documentos.num_flag_entregado,
                rh_c017_empleado_documentos.fec_entrega,
                rh_c017_empleado_documentos.fec_vencimiento,
                rh_c017_empleado_documentos.num_flag_carga,
                rh_c017_empleado_documentos.fk_c015_num_carga_familiar,
                persona_carga_familiar.ind_nombre1 AS nombre1_carga,
                persona_carga_familiar.ind_nombre2 AS nombre2_carga,
                persona_carga_familiar.ind_apellido1 AS apellido1_carga,
                persona_carga_familiar.ind_apellido2 AS apellido2_carga,
                rh_c015_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco,
                parentesco.ind_nombre_detalle AS parentesco,
                rh_c017_empleado_documentos.txt_observaciones,
                rh_c017_empleado_documentos.ind_documento,
                rh_c017_empleado_documentos.fec_ultima_modificacion,
                rh_c017_empleado_documentos.fk_a018_num_seguridad_usuario
                FROM
                rh_c017_empleado_documentos
                INNER JOIN rh_b001_empleado ON rh_c017_empleado_documentos.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle ON rh_c017_empleado_documentos.fk_a006_num_miscelaneo_detalle_documento = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
                LEFT JOIN rh_c015_carga_familiar ON rh_c017_empleado_documentos.fk_c015_num_carga_familiar = rh_c015_carga_familiar.pk_num_carga
                LEFT JOIN a003_persona AS persona_carga_familiar ON rh_c015_carga_familiar.fk_a003_num_persona = persona_carga_familiar.pk_num_persona
                LEFT JOIN a006_miscelaneo_detalle AS parentesco ON rh_c015_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco = parentesco.pk_num_miscelaneo_detalle
                WHERE rh_c017_empleado_documentos.pk_num_empleado_documento='$idDocumento'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }

    #PERMITE INSERTAR LOS DOCUMENTOS DEL EMPLEADO
    public function metRegistrarDocumentos($empleado,$documento,$flag_entregado,$flag_carga,$entrega,$vencimiento,$carga_familiar=false,$obs,$imagen)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

            $fe = explode("-",$entrega);
            $fv = explode("-",$vencimiento);

            if($carga_familiar==false){
                $carga_familiar=NULL;
            }

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c017_empleado_documentos
                 SET
                    fk_rhb001_num_empleado='$empleado',
                    fk_a006_num_miscelaneo_detalle_documento=:fk_a006_num_miscelaneo_detalle_documento,
                    num_flag_entregado=:num_flag_entregado,
                    fec_entrega=:fec_entrega,
                    fec_vencimiento=:fec_vencimiento,
                    num_flag_carga=:num_flag_carga,
                    fk_c015_num_carga_familiar=:fk_c015_num_carga_familiar,
                    txt_observaciones=:txt_observaciones,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a006_num_miscelaneo_detalle_documento' => $documento,
                ':num_flag_entregado'  => $flag_entregado,
                ':fec_entrega'  => $fe[2]."-".$fe[1]."-".$fe[0],
                ':fec_vencimiento' => $fv[2]."-".$fv[1]."-".$fv[0],
                ':num_flag_carga' => $flag_carga,
                ':fk_c015_num_carga_familiar' => $carga_familiar,
                ':txt_observaciones' => $obs
            ));

            $error = $NuevoRegistro->errorInfo();

            $idRegistro= $this->_db->lastInsertId();

            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;

            }else{

                #verifico si existe una imagen temporal, la renombro y la guardo
                if($imagen){

                    $ruta_tmp = "publico/imagenes/modRH/tmp/".$imagen;
                    $ruta     = "publico/imagenes/modRH/documentos/".$imagen;
                    $partes   = explode(".",$imagen);

                    if(file_exists($ruta_tmp)){


                        /*mkdir("publico/imagenes/modRH/documentos/".$empleado,0777,TRUE);
                        chmod("publico/imagenes/modRH/documentos/".$empleado, 0777);*/

                        if(!copy($ruta_tmp,$ruta)){
                            echo "Error al copiar la Imagen";
                        }else{
                            chmod($ruta, 0777);
                            rename($ruta,"publico/imagenes/modRH/documentos/".$idRegistro."_".$empleado.".".$partes[1]);
                            unlink($ruta_tmp);

                            $actualizoImagen = $this->_db->prepare("
                                                     UPDATE
                                                          rh_c017_empleado_documentos
                                                     SET
                                                          ind_documento=:imagen
                                                     WHERE pk_num_empleado_documento='$idRegistro'
                                                    ");
                            $actualizoImagen->execute(array(
                                ':imagen' => $idRegistro."_".$empleado.".".$partes[1]
                            ));

                            $errorImagen = $actualizoImagen->errorInfo();

                            if(!empty($errorImagen[1]) && !empty($errorImagen[2])){
                                $this->_db->rollBack();
                                return $errorImagen;
                            }else{
                                $this->_db->commit();
                                return $idRegistro;
                            }
                        }

                    }

                }else{
                    //en caso de exito hago commit de la operacion
                    $this->_db->commit();
                    return $idRegistro;
                }

            }

    }

    #PERMITE MODIFICAR DOCUMENTOS DEL EMPLEADO
    public function metModificarDocumentos($idDocumento,$empleado,$documento,$flag_entregado,$flag_carga,$entrega,$vencimiento,$carga_familiar=false,$obs,$imagen)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        $fe = explode("-",$entrega);
        $fv = explode("-",$vencimiento);

        if($carga_familiar==false){
            $carga_familiar=NULL;
        }else{
            $carga_familiar=$carga_familiar;
        }

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $ActualizarRegistro = $this->_db->prepare(
            "UPDATE
                rh_c017_empleado_documentos
             SET
                fk_a006_num_miscelaneo_detalle_documento=:fk_a006_num_miscelaneo_detalle_documento,
                num_flag_entregado=:num_flag_entregado,
                fec_entrega=:fec_entrega,
                fec_vencimiento=:fec_vencimiento,
                num_flag_carga=:num_flag_carga,
                fk_c015_num_carga_familiar=:fk_c015_num_carga_familiar,
                txt_observaciones=:txt_observaciones,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE pk_num_empleado_documento='$idDocumento'
              ");

        #execute — Ejecuta una sentencia preparada
        $ActualizarRegistro->execute(array(
            ':fk_a006_num_miscelaneo_detalle_documento' => $documento,
            ':num_flag_entregado'  => $flag_entregado,
            ':fec_entrega'  => $fe[2]."-".$fe[1]."-".$fe[0],
            ':fec_vencimiento' => $fv[2]."-".$fv[1]."-".$fv[0],
            ':num_flag_carga' => $flag_carga,
            ':fk_c015_num_carga_familiar' => $carga_familiar,
            ':txt_observaciones' => $obs
        ));

        $error= $ActualizarRegistro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $error;

        }else{

            #verifico si existe una imagen temporal, la renombro y la guardo
            if($imagen){

                $ruta_tmp = "publico/imagenes/modRH/tmp/".$imagen;
                $ruta     = "publico/imagenes/modRH/documentos/".$empleado."/".$imagen;
                $partes   = explode(".",$imagen);

                if(file_exists($ruta_tmp)){


                    mkdir("publico/imagenes/modRH/documentos/".$empleado,0777,TRUE);
                    chmod("publico/imagenes/modRH/documentos/".$empleado, 0777);

                    if(!copy($ruta_tmp,$ruta)){
                        echo "Error al copiar la Imagen";
                    }else{
                        chmod($ruta, 0777);
                        rename($ruta,"publico/imagenes/modRH/documentos/".$empleado."/".$idDocumento."_".$empleado.".".$partes[1]);
                        unlink($ruta_tmp);

                        $actualizoImagen = $this->_db->prepare("
                                                     UPDATE
                                                          rh_c017_empleado_documentos
                                                     SET
                                                          ind_documento=:imagen
                                                     WHERE pk_num_empleado_documento='$idRegistro'
                                                    ");
                        $actualizoImagen->execute(array(
                            ':imagen' => $idRegistro."_".$empleado.".".$partes[1]
                        ));

                        $errorImagen = $actualizoImagen->errorInfo();

                        if(!empty($errorImagen[1]) && !empty($errorImagen[2])){
                            $this->_db->rollBack();
                            return $errorImagen;
                        }else{
                            $this->_db->commit();
                            return $idDocumento;
                        }
                    }

                }

            }else{
                //en caso de exito hago commit de la operacion
                $this->_db->commit();
                return $idDocumento;
            }
        }

    }

    #PERMITE ELIMINAR DOCUMENTOS DEL EMPLEADO
    public function metEliminarDocumentos($idDocumento)
    {

        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c017_empleado_documentos WHERE pk_num_empleado_documento=:pk_num_empleado_documento
            ");
        $eliminar->execute(array(
            'pk_num_empleado_documento'=>$idDocumento
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idDocumento;
        }

    }




}//fin clase
