<?php
class scriptCargaModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
    }


    public function metBusquedaSimple($tabla, $innerJoin, $where)
    {
        $busquedaSimple = $this->_db->query("
            SELECT
              *
            FROM
              $tabla
              $innerJoin
            WHERE
              $where
        ");
        $busquedaSimple->setFetchMode(PDO::FETCH_ASSOC);
        return $busquedaSimple->fetch();
    }

    #MENU (act.17-04-2017)
    public function metCargarMenu($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a027_seguridad_menu
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, ind_descripcion=:ind_descripcion,
                fec_ultima_modificacion=NOW(), num_estatus=1, cod_interno=:cod_interno, cod_padre=:cod_padre,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion, ind_icono=:ind_icono, ind_nombre=:ind_nombre,
                ind_rol=:ind_rol, ind_ruta=:ind_ruta, num_nivel=:num_nivel
            ");
        $registroMenuPerfil = $this->_db->prepare(
            "INSERT INTO
                a028_seguridad_menupermiso
              SET
                fk_a017_num_seguridad_perfil=1, fk_a027_num_seguridad_menu=:fk_a027_num_seguridad_menu
            ");


        foreach ($arrays as $array) {

            $busqueda = $this->metBusquedaSimple('a027_seguridad_menu', false, "cod_interno = '" . $array['cod_interno'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':num_nivel' => $array['num_nivel'],
                    ':ind_ruta' => $array['ind_ruta'],
                    ':ind_rol' => $array['ind_rol'],
                    ':ind_nombre' => mb_strtoupper($array['ind_nombre'], 'utf-8'),
                    ':ind_icono' => $array['ind_icono'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':cod_padre' => mb_strtoupper($array['cod_padre'], 'utf-8'),
                    ':cod_interno' => mb_strtoupper($array['cod_interno'], 'utf-8'),
                    ':ind_descripcion' => mb_strtoupper($array['ind_descripcion'], 'utf-8'),
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
                $idMenu = $this->_db->lastInsertId();
                $registroMenuPerfil->execute(array(
                    ':fk_a027_num_seguridad_menu' => $idMenu
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #MISCELANEOS (act.17-04-2017)
    public function metCargarMiscelaneos($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a005_miscelaneo_maestro
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, ind_descripcion=:ind_descripcion,
                fec_ultima_modificacion=NOW(), num_estatus=1, cod_maestro=:cod_maestro,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion, ind_nombre_maestro=:ind_nombre_maestro
            ");

        $registroDetalle = $this->_db->prepare(
            "INSERT INTO
                a006_miscelaneo_detalle
              SET
                cod_detalle=:cod_detalle, fk_a005_num_miscelaneo_maestro=:fk_a005_num_miscelaneo_maestro, num_estatus=:ind_estatus,
                ind_nombre_detalle=:ind_nombre_detalle
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a005_miscelaneo_maestro', false, "cod_maestro = '" . $array['cod_maestro'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':cod_maestro' => $array['cod_maestro'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':ind_nombre_maestro' => $array['ind_nombre_maestro']
                ));
                $idMiscelanneo = $this->_db->lastInsertId();
                foreach ($array['Det'] as $arrayDet) {
                    $registroDetalle->execute(array(
                        ':fk_a005_num_miscelaneo_maestro' => $idMiscelanneo,
                        ':cod_detalle' => $arrayDet['cod_detalle'],
                        ':ind_estatus' => $arrayDet['ind_estatus'],
                        ':ind_nombre_detalle' => $arrayDet['ind_nombre_detalle']
                    ));
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #PARAMETROS (Act. 18-04-2017)
    public function metCargarParametros($arrays)
    {
        $this->_db->beginTransaction();

        $registro = $this->_db->prepare(
            "INSERT INTO
                a035_parametros
              SET
                ind_descripcion=:ind_descripcion,ind_explicacion=:ind_explicacion,
                ind_parametro_clave=:ind_parametro_clave, ind_valor_parametro=:ind_valor_parametro, num_estatus=:ind_estatus, fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion, fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle
            ");

        foreach ($arrays as $array) {

            $busqueda = $this->metBusquedaSimple('a035_parametros', false, "ind_parametro_clave = '" . $array['ind_parametro_clave'] . "'");

            if (!$busqueda) {
                $idMiscelaneosDet = $this->metMostrarSelect('TPPARAMET', $array['cod_detalle']);
                $registro->execute(array(
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':ind_explicacion' => $array['ind_explicacion'],
                    ':ind_parametro_clave' => $array['ind_parametro_clave'],
                    ':ind_valor_parametro' => $array['ind_valor_parametro'],
                    ':ind_estatus' => $array['num_estatus'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fk_a006_num_miscelaneo_detalle' => $idMiscelaneosDet['pk_num_miscelaneo_detalle']
                ));
            }

        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }



    public function metCargarMaestroTipoCargo($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c006_tipo_cargo
              SET
                pk_num_cargo=:pk_num_cargo,
                ind_nombre_cargo=:ind_nombre_cargo,
                txt_descripcion=:txt_descripcion,
                txt_funcion=:txt_funcion,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c006_tipo_cargo', false, "pk_num_cargo = '" . $array['pk_num_cargo'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_cargo' => $array['pk_num_cargo'],
                    ':ind_nombre_cargo' => $array['ind_nombre_cargo'],
                    ':txt_descripcion' => $array['txt_descripcion'],
                    ':txt_funcion' => $array['txt_funcion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }


    public function metCargarMaestroNivelTipoCargo($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c009_nivel
              SET
                pk_num_nivel=:pk_num_nivel,
                fk_rhc006_num_cargo=:fk_rhc006_num_cargo,
                num_nivel=:num_nivel,
                ind_nombre_nivel=:ind_nombre_nivel,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c009_nivel', false, "pk_num_nivel = '" . $array['pk_num_nivel'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_nivel' => $array['pk_num_nivel'],
                    ':fk_rhc006_num_cargo' => $array['fk_rhc006_num_cargo'],
                    ':num_nivel' => $array['num_nivel'],
                    ':ind_nombre_nivel' => $array['ind_nombre_nivel'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }


    public function metCargarMaestroSerieOcupacional($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c010_serie
              SET
                pk_num_serie=:pk_num_serie,
                ind_nombre_serie=:ind_nombre_serie,
                fk_a006_num_miscelaneo_detalle_grupoocupacional	=:fk_a006_num_miscelaneo_detalle_grupoocupacional,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c010_serie', false, "pk_num_serie = '" . $array['pk_num_serie'] . "'");
            if (!$busqueda) {
                $idMiscelaneosDet = $this->metMostrarSelect('GRUPOCUP', $array['cod_detalle']);
                $registro->execute(array(
                    ':pk_num_serie' => $array['pk_num_serie'],
                    ':ind_nombre_serie' => $array['ind_nombre_serie'],
                    ':fk_a006_num_miscelaneo_detalle_grupoocupacional' => $idMiscelaneosDet['pk_num_miscelaneo_detalle'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }


    public function metCargarMaestroGradoSalarial($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c007_grado_salarial
              SET
                pk_num_grado=:pk_num_grado,
                fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle,
                ind_grado=:ind_grado,
                ind_nombre_grado=:ind_nombre_grado,
                flag_pasos=:flag_pasos,
                num_sueldo_minimo=:num_sueldo_minimo,
                num_sueldo_maximo=:num_sueldo_maximo,
                num_sueldo_promedio=:num_sueldo_promedio,
                num_estatus=:ind_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c007_grado_salarial', false, "pk_num_grado = '" . $array['pk_num_grado'] . "'");
            if (!$busqueda) {
                $detalle = $this->metMostrarSelect('CATCARGO', $array['cod_detalle']);
                $registro->execute(array(
                    ':pk_num_grado' => $array['pk_num_grado'],
                    ':fk_a006_num_miscelaneo_detalle' => $detalle['pk_num_miscelaneo_detalle'],
                    ':ind_grado' => $array['ind_grado'],
                    ':ind_nombre_grado' => $array['ind_nombre_grado'],
                    ':flag_pasos' => $array['flag_pasos'],
                    ':num_sueldo_minimo' => $array['num_sueldo_minimo'],
                    ':num_sueldo_maximo' => $array['num_sueldo_maximo'],
                    ':num_sueldo_promedio' => $array['num_sueldo_promedio'],
                    ':ind_estatus' => $array['num_estatus'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion']
                ));

            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }


    public function metCargarMaestroCargos($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c063_puestos
              SET
                pk_num_puestos=:pk_num_puestos,
                fk_rhc006_num_cargo	=:fk_rhc006_num_cargo,
                fk_rhc007_num_grado=:fk_rhc007_num_grado,
                fk_a006_num_miscelaneo_detalle_paso=:fk_a006_num_miscelaneo_detalle_paso,
                fk_a006_num_miscelaneo_detalle_grupoocupacional=:fk_a006_num_miscelaneo_detalle_grupoocupacional,
                fk_a006_num_miscelaneo_detalle_categoria=:fk_a006_num_miscelaneo_detalle_categoria,
                fk_rhc009_num_nivel=:fk_rhc009_num_nivel,
                fk_rhc010_num_serie=:fk_rhc010_num_serie,
                ind_descripcion_cargo=:ind_descripcion_cargo,
                txt_descripcion_gen=:txt_descripcion_gen,
                num_sueldo_basico=:num_sueldo_basico,
                num_estatus=:ind_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c063_puestos', false, "pk_num_puestos = '" . $array['pk_num_puestos'] . "'");
            if (!$busqueda) {
                $grupoocupacional = $this->metMostrarSelect('GRUPOCUP', $array['cod_detalle_grupoocupacional']);
                $categoria = $this->metMostrarSelect('CATCARGO', $array['cod_detalle_categoria']);
                $pasos = $this->metMostrarSelect('PASOS', $array['cod_detalle_paso']);
                $registro->execute(array(
                    ':pk_num_puestos' => $array['pk_num_puestos'],
                    ':fk_rhc006_num_cargo' => $array['fk_rhc006_num_cargo'],
                    ':fk_rhc007_num_grado' => $array['fk_rhc007_num_grado'],
                    ':fk_a006_num_miscelaneo_detalle_paso' => $pasos['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_grupoocupacional' => $grupoocupacional['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_categoria' => $categoria['pk_num_miscelaneo_detalle'],
                    ':fk_rhc009_num_nivel' => $array['fk_rhc009_num_nivel'],
                    ':fk_rhc010_num_serie' => $array['fk_rhc010_num_serie'],
                    ':ind_descripcion_cargo' => $array['ind_descripcion_cargo'],
                    ':txt_descripcion_gen' => $array['txt_descripcion_gen'],
                    ':num_sueldo_basico' => $array['num_sueldo_basico'],
                    ':ind_estatus' => $array['num_estatus'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }


    public function metCargarMaestroNivelGradoInstruccion($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c064_nivel_grado_instruccion
              SET
                pk_num_nivel_grado=:pk_num_nivel_grado,
                fk_a006_num_miscelaneo_detalle_gradoinst=:fk_a006_num_miscelaneo_detalle_gradoinst,
                ind_nombre_nivel_grado=:ind_nombre_nivel_grado,
                num_estatus=:num_estatus,
                num_flag_utiles=:num_flag_utiles,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c064_nivel_grado_instruccion', false, "pk_num_nivel_grado = '" . $array['pk_num_nivel_grado'] . "'");
            if (!$busqueda) {
                $gradoinst = $this->metMostrarSelect('GRADOINST', $array['cod_detalle']);
                $registro->execute(array(
                    ':pk_num_nivel_grado' => $array['pk_num_nivel_grado'],
                    ':fk_a006_num_miscelaneo_detalle_gradoinst' => $gradoinst['pk_num_miscelaneo_detalle'],
                    ':ind_nombre_nivel_grado' => $array['ind_nombre_nivel_grado'],
                    ':num_estatus' => $array['num_estatus'],
                    ':num_flag_utiles' => $array['num_flag_utiles'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarMaestroProfesiones($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c057_profesiones
              SET
                pk_num_profesion=:pk_num_profesion,
                fk_a006_num_miscelaneo_detalle_areaformacion=:fk_a006_num_miscelaneo_detalle_areaformacion,
                fk_a006_num_miscelaneo_detalle_gradoinst=:fk_a006_num_miscelaneo_detalle_gradoinst,
                ind_nombre_profesion=:ind_nombre_profesion,
                num_estatus=:num_estatus,
                ind_ultimo_usuario=:ind_ultimo_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c057_profesiones', false, "pk_num_profesion = '" . $array['pk_num_profesion'] . "'");
            if (!$busqueda) {
                $area = $this->metMostrarSelect('AREA', $array['cod_detalle_area']);
                $gradoinst = $this->metMostrarSelect('GRADOINST', $array['cod_detalle_grado']);
                $registro->execute(array(
                    ':pk_num_profesion' => $array['pk_num_profesion'],
                    ':fk_a006_num_miscelaneo_detalle_areaformacion' => $area['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_gradoinst' => $gradoinst['pk_num_miscelaneo_detalle'],
                    ':ind_nombre_profesion' => $array['ind_nombre_profesion'],
                    ':num_estatus' => $array['num_estatus'],
                    ':ind_ultimo_usuario' => $array['ind_ultimo_usuario'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarMaestroCentrosEstudios($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c040_institucion
              SET
                pk_num_institucion=:pk_num_institucion,
                ind_nombre_institucion=:ind_nombre_institucion,
                ind_ubicacion=:ind_ubicacion,
                num_flag_estudio=:num_flag_estudio,
                num_flag_curso=:num_flag_curso,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c040_institucion', false, "pk_num_institucion = '" . $array['pk_num_institucion'] . "'");
            if (!$busqueda) {

                $registro->execute(array(
                    ':pk_num_institucion' => $array['pk_num_institucion'],
                    ':ind_nombre_institucion' => $array['ind_nombre_institucion'],
                    ':ind_ubicacion' => $array['ind_ubicacion'],
                    ':num_flag_estudio' => $array['num_flag_estudio'],
                    ':num_flag_curso' => $array['num_flag_curso'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }



    public  function metCargarMaestroCursos($arrays)
    {

        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c048_curso
              SET
                pk_num_curso=:pk_num_curso,
                fk_a006_num_miscelaneo_detalle_areaformacion=:fk_a006_num_miscelaneo_detalle_areaformacion,
                ind_nombre_curso=:ind_nombre_curso,
                fec_duracion=:fec_duracion,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c048_curso', false, "pk_num_curso = '" . $array['pk_num_curso'] . "'");
            if (!$busqueda) {
                $area = $this->metMostrarSelect('AREA', $array['cod_detalle']);
                $registro->execute(array(
                    ':pk_num_curso' => $array['pk_num_curso'],
                    ':fk_a006_num_miscelaneo_detalle_areaformacion' => $area['pk_num_miscelaneo_detalle'],
                    ':ind_nombre_curso' => $array['ind_nombre_curso'],
                    ':fec_duracion' => $array['fec_duracion'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }


    public function metCargarMaestroIdiomas($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c013_idioma
              SET
                pk_num_idioma=:pk_num_idioma,
                ind_nombre_idioma=:ind_nombre_idioma,
                ind_nombre_ext=:ind_nombre_ext,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c013_idioma', false, "pk_num_idioma = '" . $array['pk_num_idioma'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_idioma' => $array['pk_num_idioma'],
                    ':ind_nombre_idioma' => $array['ind_nombre_idioma'],
                    ':ind_nombre_ext' => $array['ind_nombre_ext'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }


    public function metCargarMaestroTiposContratos($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c066_tipo_contrato
              SET
                pk_num_tipo_contrato=:pk_num_tipo_contrato,
                ind_tipo_contrato=:ind_tipo_contrato,
                num_flag_nomina=:num_flag_nomina,
                num_flag_vencimiento=:num_flag_vencimiento,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c066_tipo_contrato', false, "pk_num_tipo_contrato = '" . $array['pk_num_tipo_contrato'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_tipo_contrato' => $array['pk_num_tipo_contrato'],
                    ':ind_tipo_contrato' => $array['ind_tipo_contrato'],
                    ':num_flag_nomina' => $array['num_flag_nomina'],
                    ':num_flag_vencimiento' => $array['num_flag_vencimiento'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarMaestroMotivoCese($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c032_motivo_cese
              SET
                pk_num_motivo_cese=:pk_num_motivo_cese,
                ind_nombre_cese=:ind_nombre_cese,
                num_flag_falta_grave=:num_flag_falta_grave,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c032_motivo_cese', false, "pk_num_motivo_cese = '" . $array['pk_num_motivo_cese'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_motivo_cese' => $array['pk_num_motivo_cese'],
                    ':ind_nombre_cese' => $array['ind_nombre_cese'],
                    ':num_flag_falta_grave' => $array['num_flag_falta_grave'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }


    public function metCargarMaestroHorarioLaboral($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_b007_horario_laboral
              SET
                pk_num_horario=:pk_num_horario,
                ind_descripcion=:ind_descripcion,
                num_flag_corrido=:num_flag_corrido,
                ind_resolucion=:ind_resolucion,
                fec_resolucion=:fec_resolucion,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_b007_horario_laboral', false, "pk_num_horario = '" . $array['pk_num_horario'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_horario' => $array['pk_num_horario'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':num_flag_corrido' => $array['num_flag_corrido'],
                    ':ind_resolucion' => $array['ind_resolucion'],
                    ':fec_resolucion' => $array['fec_resolucion'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarMaestroHorarioLaboralDetalle($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c067_horario_detalle
              SET
                pk_num_horario_detalle=:pk_num_horario_detalle,
                fk_rhb007_num_horario=:fk_rhb007_num_horario,
                num_dia=:num_dia,
                num_flag_laborable=:num_flag_laborable,
                fec_entrada1=:fec_entrada1,
                fec_salida1=:fec_salida1,
                fec_entrada2=:fec_entrada2,
                fec_salida2=:fec_salida2,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c067_horario_detalle', false, "pk_num_horario_detalle = '" . $array['pk_num_horario_detalle'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_horario_detalle' => $array['pk_num_horario_detalle'],
                    ':fk_rhb007_num_horario' => $array['fk_rhb007_num_horario'],
                    ':num_dia' => $array['num_dia'],
                    ':num_flag_laborable' => $array['num_flag_laborable'],
                    ':fec_entrada1' => $array['fec_entrada1'],
                    ':fec_salida1' => $array['fec_salida1'],
                    ':fec_entrada2' => $array['fec_entrada2'],
                    ':fec_salida2' => $array['fec_salida2'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarOrganismo($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a001_organismo
              SET
                pk_num_organismo=:pk_num_organismo,
                ind_descripcion_empresa=:ind_descripcion_empresa,
                ind_documento_fiscal=:ind_documento_fiscal,
                ind_numero_registro=:ind_numero_registro,
                ind_tomo_registro=:ind_tomo_registro,
                ind_logo=:ind_logo,
                ind_logo_secundario=:ind_logo_secundario,
                num_estatus=:num_estatus,
                ind_tipo_organismo=:ind_tipo_organismo,
                ind_direccion=:ind_direccion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a006_num_miscelaneo_detalle_carcater_social=:fk_a006_num_miscelaneo_detalle_carcater_social
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a001_organismo', false, "pk_num_organismo = '" . $array['pk_num_organismo'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_organismo' => $array['pk_num_organismo'],
                    ':ind_descripcion_empresa' => $array['ind_descripcion_empresa'],
                    ':ind_documento_fiscal' => $array['ind_documento_fiscal'],
                    ':ind_numero_registro' => $array['ind_numero_registro'],
                    ':ind_tomo_registro' => $array['ind_tomo_registro'],
                    ':ind_logo' => $array['ind_logo'],
                    ':ind_logo_secundario' => $array['ind_logo_secundario'],
                    ':ind_tipo_organismo' => $array['ind_tipo_organismo'],
                    ':num_estatus' => $array['num_estatus'],
                    ':ind_direccion' => $array['ind_direccion'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fk_a006_num_miscelaneo_detalle_carcater_social' => $array['fk_a006_num_miscelaneo_detalle_carcater_social']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarDependencias($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a004_dependencia
              SET
                pk_num_dependencia=:pk_num_dependencia,
                ind_dependencia=:ind_dependencia,
                ind_codinterno=:ind_codinterno,
                ind_codpadre=:ind_codpadre,
                ind_nivel=:ind_nivel,
                num_flag_controlfiscal=:num_flag_controlfiscal,
                num_flag_principal=:num_flag_principal,
                num_piso=:num_piso,
                num_estatus=:num_estatus,
                fk_a001_num_organismo=:fk_a001_num_organismo,
                fk_a003_num_persona_responsable=:fk_a003_num_persona_responsable,
                num_flag_responsable=:num_flag_responsable,
                txt_abreviacion=:txt_abreviacion,
                fec_ultima_modificacion=:fec_ultima_modificacion,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a004_dependencia', false, "pk_num_dependencia = '" . $array['pk_num_dependencia'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_dependencia' => $array['pk_num_dependencia'],
                    ':ind_dependencia' => $array['ind_dependencia'],
                    ':ind_codinterno' => $array['ind_codinterno'],
                    ':ind_codpadre' => $array['ind_codpadre'],
                    ':ind_nivel' => $array['ind_nivel'],
                    ':num_flag_controlfiscal' => $array['num_flag_controlfiscal'],
                    ':num_flag_principal' => $array['num_flag_principal'],
                    ':num_piso' => $array['num_piso'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fk_a001_num_organismo' => $array['fk_a001_num_organismo'],
                    ':fk_a003_num_persona_responsable' => $array['fk_a003_num_persona_responsable'],
                    ':num_flag_responsable' => $array['num_flag_responsable'],
                    ':txt_abreviacion' => $array['txt_abreviacion'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }


    public function metCargarTablaVacaciones($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c024_tabla_vacacion
              SET
                fk_nmb001_num_nomina=:fk_nmb001_num_nomina,
                num_anios=:num_anios,
                num_dias_disfrute=:num_dias_disfrute,
                num_dias_adicionales=:num_dias_adicionales,
                num_total_disfrutar=:num_total_disfrutar,
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                fec_ultima_modificacion=:fec_ultima_modificacion
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c024_tabla_vacacion', false, "pk_num_tabla_vacacion = '" . $array['pk_num_tabla_vacacion'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':fk_nmb001_num_nomina' => $array['fk_nmb001_num_nomina'],
                    ':num_anios' => $array['num_anios'],
                    ':num_dias_disfrute' => $array['num_dias_disfrute'],
                    ':num_dias_adicionales' => $array['num_dias_adicionales'],
                    ':num_total_disfrutar' => $array['num_total_disfrutar'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }


    public function metCargarCentroCostoGrupo($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a024_grupo_centro_costo
              SET
                pk_num_grupo_centro_costo=:pk_num_grupo_centro_costo,
                cod_grupo_centro_costo=:cod_grupo_centro_costo,
                ind_descripcion=:ind_descripcion,
                num_estatus=:num_estatus
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a024_grupo_centro_costo', false, "pk_num_grupo_centro_costo = '" . $array['pk_num_grupo_centro_costo'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_grupo_centro_costo' => $array['pk_num_grupo_centro_costo'],
                    ':cod_grupo_centro_costo' => $array['cod_grupo_centro_costo'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':num_estatus' => $array['num_estatus']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarCentroCostoSubGrupo($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a025_subgrupo_centro_costo
              SET
                pk_num_subgrupo_centro_costo=:pk_num_subgrupo_centro_costo,
                ind_descripcion=:ind_descripcion,
                num_estatus=:num_estatus,
                fk_a024_num_grupo_centro_costo=:fk_a024_num_grupo_centro_costo
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a025_subgrupo_centro_costo', false, "pk_num_subgrupo_centro_costo = '" . $array['pk_num_subgrupo_centro_costo'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_subgrupo_centro_costo' => $array['pk_num_subgrupo_centro_costo'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fk_a024_num_grupo_centro_costo' => $array['fk_a024_num_grupo_centro_costo']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }


    public function metCargarCentroCosto($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a023_centro_costo
              SET
                pk_num_centro_costo=:pk_num_centro_costo,
                ind_descripcion_centro_costo=:ind_descripcion_centro_costo,
                ind_abreviatura=:ind_abreviatura,
                num_flag_administrativo=:num_flag_administrativo,
                num_flag_ventas=:num_flag_ventas,
                num_flag_financiera=:num_flag_financiera,
                num_flag_produccion=:num_flag_produccion,
                num_flag_centro_ingresos=:num_flag_centro_ingresos,
                num_estatus=:num_estatus,
                fk_a003_num_persona=:fk_a003_num_persona,
                fk_a025_num_subgrupo_centro_costo=:fk_a025_num_subgrupo_centro_costo,
                fk_a004_num_dependencia=:fk_a004_num_dependencia,
                fk_a018_num_seguridad_usuario=1,
                fec_ultima_modificacion=NOW(),
                cod_centro_costo=:cod_centro_costo
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a023_centro_costo', false, "pk_num_centro_costo = '" . $array['pk_num_centro_costo'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_centro_costo' => $array['pk_num_centro_costo'],
                    ':ind_descripcion_centro_costo' => $array['ind_descripcion_centro_costo'],
                    ':ind_abreviatura' => $array['ind_abreviatura'],
                    ':num_flag_administrativo' => $array['num_flag_administrativo'],
                    ':num_flag_ventas' => $array['num_flag_ventas'],
                    ':num_flag_financiera' => $array['num_flag_financiera'],
                    ':num_flag_produccion' => $array['num_flag_produccion'],
                    ':num_flag_centro_ingresos' => $array['num_flag_centro_ingresos'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fk_a003_num_persona' => $array['fk_a003_num_persona'],
                    ':fk_a025_num_subgrupo_centro_costo' => $array['fk_a025_num_subgrupo_centro_costo'],
                    ':fk_a004_num_dependencia' => $array['fk_a004_num_dependencia'],
                    ':cod_centro_costo' => $array['cod_centro_costo']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }




    public function metCargarPersona($arrays)
    {
        $this->_db->query(
            "SET FOREIGN_KEY_CHECKS=0;"
        );
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a003_persona
              SET
                fk_a010_num_ciudad=:fk_a010_num_ciudad, ind_cedula_documento=:ind_cedula_documento,
                ind_documento_fiscal=:ind_documento_fiscal, ind_estatus=1, ind_nombre1=:ind_nombre1, ind_tipo_persona=:ind_tipo_persona,
                pk_num_persona=:pk_num_persona, ind_apellido1=:ind_apellido1
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a003_persona', false, "pk_num_persona = '" . $array['pk_num_persona'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_persona' => $array['pk_num_persona'],
                    ':fk_a010_num_ciudad' => $array['fk_a010_num_ciudad'],
                    ':ind_cedula_documento' => $array['ind_cedula_documento'],
                    ':ind_documento_fiscal' => $array['ind_documento_fiscal'],
                    ':ind_nombre1' => $array['ind_nombre1'],
                    ':ind_apellido1' => $array['ind_apellido1'],
                    ':ind_tipo_persona' => $array['ind_tipo_persona']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarPersonaLaboral($arrays)
    {
        $this->_db->query(
            "SET FOREIGN_KEY_CHECKS=0;"
        );
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c005_empleado_laboral
              SET
                pk_num_empleado_laboral=:pk_num_empleado_laboral,
                fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                fec_ingreso=:fec_ingreso,
                fec_ingreso_anterior=:fec_ingreso_anterior,
                ind_resolucion_ingreso=:ind_resolucion_ingreso,
                fec_egreso=:fec_egreso,
                ind_resolucion_egreso=:ind_resolucion_egreso,
                num_sueldo_ant=:num_sueldo_ant,
                fk_rhc063_num_puestos_cargo=:fk_rhc063_num_puestos_cargo,
                fk_rhc063_num_puestos_cargotmp=:fk_rhc063_num_puestos_cargotmp,
                fk_a006_num_miscelaneo_detalle_paso=:fk_a006_num_miscelaneo_detalle_paso,
                num_sueldo=:num_sueldo,
                fk_rhb007_num_horario=:fk_rhb007_num_horario,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1
              ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c005_empleado_laboral', false, "pk_num_empleado_laboral = '" . $array['pk_num_empleado_laboral'] . "'");
            if (!$busqueda) {
                $pasos = $this->metMostrarSelect('PASOS', $array['cod_detalle_paso']);
                $registro->execute(array(
                    ':pk_num_empleado_laboral' => $array['pk_num_empleado_laboral'],
                    ':fk_rhb001_num_empleado' => $array['fk_rhb001_num_empleado'],
                    ':fec_ingreso' => $array['fec_ingreso'],
                    ':fec_ingreso_anterior' => $array['fec_ingreso_anterior'],
                    ':ind_resolucion_ingreso' => $array['ind_resolucion_ingreso'],
                    ':fec_egreso' => $array['fec_egreso'],
                    ':ind_resolucion_egreso' => $array['ind_resolucion_egreso'],
                    ':fec_egreso' => $array['fec_egreso'],
                    ':num_sueldo_ant' => $array['num_sueldo_ant'],
                    ':fk_rhc063_num_puestos_cargo' => $array['fk_rhc063_num_puestos_cargo'],
                    ':fk_rhc063_num_puestos_cargotmp' => $array['fk_rhc063_num_puestos_cargotmp'],
                    ':fk_a006_num_miscelaneo_detalle_paso' => $pasos['pk_num_miscelaneo_detalle'],
                    ':num_sueldo' => $array['num_sueldo'],
                    ':fk_rhb007_num_horario' => $array['fk_rhb007_num_horario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }


    public function metCargarPersonaOrganizacion($arrays)
    {
        $this->_db->query(
            "SET FOREIGN_KEY_CHECKS=0;"
        );
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c076_empleado_organizacion
              SET
                pk_num_empleado_organizacion=:pk_num_empleado_organizacion,
                fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                fk_a001_num_organismo=:fk_a001_num_organismo,
                fk_a004_num_dependencia=:fk_a004_num_dependencia,
                fk_a004_num_dependencia_tmp=:fk_a004_num_dependencia_tmp,
                fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                fk_a006_num_miscelaneo_detalle_tipopago=:fk_a006_num_miscelaneo_detalle_tipopago,
                fk_a006_num_miscelaneo_detalle_tipotrabajador=:fk_a006_num_miscelaneo_detalle_tipotrabajador,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=1
              ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_c076_empleado_organizacion', false, "pk_num_empleado_organizacion = '" . $array['pk_num_empleado_organizacion'] . "'");
            if (!$busqueda) {
                $tpago = $this->metMostrarSelect('TIPOPAGO', $array['cod_detalle_pago']);
                $ttrabajador = $this->metMostrarSelect('TIPOTRAB', $array['cod_detalle_trabajador']);
                $registro->execute(array(
                    ':pk_num_empleado_organizacion' => $array['pk_num_empleado_organizacion'],
                    ':fk_rhb001_num_empleado' => $array['fk_rhb001_num_empleado'],
                    ':fk_a001_num_organismo' => $array['fk_a001_num_organismo'],
                    ':fk_a004_num_dependencia' => $array['fk_a004_num_dependencia'],
                    ':fk_a004_num_dependencia_tmp' => $array['fk_a004_num_dependencia_tmp'],
                    ':fk_a023_num_centro_costo' => $array['fk_a023_num_centro_costo'],
                    ':fk_nmb001_num_tipo_nomina' => $array['fk_nmb001_num_tipo_nomina'],
                    ':fk_a006_num_miscelaneo_detalle_tipopago' => $tpago['pk_num_miscelaneo_detalle'],
                    ':fk_a006_num_miscelaneo_detalle_tipotrabajador' => $ttrabajador['pk_num_miscelaneo_detalle']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metCargarEmpleado($arrays)
    {
        $this->_db->beginTransaction();
        $registroEmpleado = $this->_db->prepare(
            "INSERT INTO
                rh_b001_empleado
              SET
                fec_ultima_modificacion=NOW(), fk_a003_num_persona=:fk_a003_num_persona, fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                ind_estado_aprobacion='AP', ind_estatus=1, pk_num_empleado=:pk_num_empleado
            ");

        $registroUsuario = $this->_db->prepare(
            "INSERT INTO
                a018_seguridad_usuario
              SET
                fec_ultima_modificacion=NOW(), fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                ind_estatus=1, fk_rhb001_num_empleado=:fk_rhb001_num_empleado, ind_password=:ind_password,
                ind_template=:ind_template,pk_num_seguridad_usuario=:pk_num_seguridad_usuario,ind_usuario=:ind_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('rh_b001_empleado', false, "pk_num_empleado = '" . $array['pk_num_empleado'] . "'");
            if (!$busqueda) {
                $registroEmpleado->execute(array(
                    ':pk_num_empleado' => $array['pk_num_empleado'],
                    ':fk_a003_num_persona' => $array['fk_a003_num_persona'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
                $registroUsuario->execute(array(
                    ':pk_num_seguridad_usuario' => $array['pk_num_seguridad_usuario'],
                    ':fk_rhb001_num_empleado' => $array['fk_rhb001_num_empleado'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_password' => $array['ind_password'],
                    ':ind_usuario' => $array['ind_usuario'],
                    ':ind_template' => $array['ind_template']
                ));
            }
        }
        $error = $registroUsuario->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            var_dump($error);
            echo '<br><br>';
            $this->_db->rollBack();
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
        }
        $this->_db->query(
            "SET FOREIGN_KEY_CHECKS=1;"
        );

    }

    public function metCargarAplicacion($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a015_seguridad_aplicacion
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, fec_creacion=NOW(), cod_aplicacion=:cod_aplicacion,
                ind_descripcion=:ind_descripcion, ind_nombre_modulo=:ind_nombre_modulo, num_estatus=1,
                pk_num_seguridad_aplicacion=:pk_num_seguridad_aplicacion

            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a015_seguridad_aplicacion', false, "pk_num_seguridad_aplicacion = '" . $array['pk_num_seguridad_aplicacion'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_seguridad_aplicacion' => $array['pk_num_seguridad_aplicacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':cod_aplicacion' => $array['cod_aplicacion'],
                    ':ind_descripcion' => mb_strtoupper($array['ind_descripcion'], 'utf-8'),
                    ':ind_nombre_modulo' => mb_strtoupper($array['ind_nombre_modulo'], 'utf-8')
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCargarPerfil($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO
                a017_seguridad_perfil
              SET
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario, ind_descripcion=:ind_descripcion,
                fec_ultima_modificacion=NOW(), ind_estatus=1, ind_nombre_perfil=:ind_nombre_perfil,
                pk_num_seguridad_perfil=:pk_num_seguridad_perfil
        ");

        $registroPerfil = $this->_db->prepare(
            "INSERT INTO
                a020_seguridad_usuarioperfil
              SET
                fk_a018_num_seguridad_usuario=1, fk_a017_num_seguridad_perfil=:fk_a017_num_seguridad_perfil
        ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a017_seguridad_perfil', false, "pk_num_seguridad_perfil = '" . $array['pk_num_seguridad_perfil'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_seguridad_perfil' => $array['pk_num_seguridad_perfil'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':ind_nombre_perfil' => $array['ind_nombre_perfil']
                ));
                $registroPerfil->execute(array(
                    ':fk_a017_num_seguridad_perfil' => $array['pk_num_seguridad_perfil']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }


    public function metMostrarSelect($codMaestro, $codDetalle)
    {
        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='$codMaestro' AND
              a006.cod_detalle='$codDetalle'
        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetch();
    }


}
