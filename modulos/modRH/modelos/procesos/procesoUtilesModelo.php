<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-02-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/


class procesoUtilesModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }


    #permite listar todos los beneficios de utiles registrados
    public function metListarBeneficioUtiles()
    {
        $con =  $this->_db->query("
            SELECT
            rh_c091_utiles_beneficio.pk_num_utiles_beneficio,
            rh_c091_utiles_beneficio.num_orden,
            rh_c091_utiles_beneficio.fk_rhc056_num_beneficio_utiles,
            rh_c056_beneficio_utiles.ind_descripcion_beneficio,
            rh_c091_utiles_beneficio.fk_a006_num_miscelaneo_detalle_asignacion,
            tipo_asignacion.cod_detalle,
            tipo_asignacion.ind_nombre_detalle,
            rh_c091_utiles_beneficio.fk_rhb001_num_empleado,
            a003_persona.ind_cedula_documento,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_empleado,
            rh_c091_utiles_beneficio.fec_fecha,
            rh_c091_utiles_beneficio.lg_b022_num_proveedor,
            CONCAT(persona_proveedor.ind_nombre1,' ',persona_proveedor.ind_nombre2,' ',persona_proveedor.ind_apellido1,' ',persona_proveedor.ind_apellido2) AS proveedor,
            rh_c091_utiles_beneficio.num_monto,
            rh_c091_utiles_beneficio.ind_estado,
            rh_c091_utiles_beneficio.fec_ultima_modificacion,
            rh_c091_utiles_beneficio.fk_a018_num_seguridad_usuario
            FROM
            rh_c091_utiles_beneficio
            INNER JOIN rh_c056_beneficio_utiles ON rh_c091_utiles_beneficio.fk_rhc056_num_beneficio_utiles = rh_c056_beneficio_utiles.pk_num_beneficio_utiles
            INNER JOIN a006_miscelaneo_detalle AS tipo_asignacion ON rh_c091_utiles_beneficio.fk_a006_num_miscelaneo_detalle_asignacion = tipo_asignacion.pk_num_miscelaneo_detalle
            INNER JOIN rh_b001_empleado ON rh_c091_utiles_beneficio.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            LEFT JOIN lg_b022_proveedor AS proveedor ON rh_c091_utiles_beneficio.lg_b022_num_proveedor = proveedor.pk_num_proveedor
            LEFT JOIN a003_persona AS persona_proveedor ON proveedor.fk_a003_num_persona_proveedor = persona_proveedor.pk_num_persona
            ORDER BY rh_c091_utiles_beneficio.pk_num_utiles_beneficio desc
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #listar beneficio utiles para asignacion de utiles
    #solor entran lo que AF. asignacion por facturas
    public function metListarBeneficioAsignacionUtiles()
    {
        $con =  $this->_db->query("
            SELECT
            rh_c091_utiles_beneficio.pk_num_utiles_beneficio,
            rh_c091_utiles_beneficio.num_orden,
            rh_c091_utiles_beneficio.fk_rhc056_num_beneficio_utiles,
            rh_c056_beneficio_utiles.ind_descripcion_beneficio,
            rh_c091_utiles_beneficio.fk_a006_num_miscelaneo_detalle_asignacion,
            tipo_asignacion.cod_detalle,
            tipo_asignacion.ind_nombre_detalle,
            rh_c091_utiles_beneficio.fk_rhb001_num_empleado,
            a003_persona.ind_cedula_documento,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_empleado,
            rh_c091_utiles_beneficio.fec_fecha,
            rh_c091_utiles_beneficio.lg_b022_num_proveedor,
            CONCAT(persona_proveedor.ind_nombre1,' ',persona_proveedor.ind_nombre2,' ',persona_proveedor.ind_apellido1,' ',persona_proveedor.ind_apellido2) AS proveedor,
            rh_c091_utiles_beneficio.num_monto,
            rh_c091_utiles_beneficio.ind_estado,
            rh_c091_utiles_beneficio.fec_ultima_modificacion,
            rh_c091_utiles_beneficio.fk_a018_num_seguridad_usuario,
            rh_c092_utiles_beneficarios.fk_c015_num_carga_familiar,
            rh_c015_carga_familiar.fk_a003_num_persona AS pk_num_persona_carga_familiar,
            CONCAT(persona_carga_familiar.ind_nombre1,' ',persona_carga_familiar.ind_nombre2,' ',persona_carga_familiar.ind_apellido1,' ',persona_carga_familiar.ind_apellido2) AS nombre_hijo
            FROM
            rh_c091_utiles_beneficio
            INNER JOIN rh_c056_beneficio_utiles ON rh_c091_utiles_beneficio.fk_rhc056_num_beneficio_utiles = rh_c056_beneficio_utiles.pk_num_beneficio_utiles
            INNER JOIN a006_miscelaneo_detalle AS tipo_asignacion ON rh_c091_utiles_beneficio.fk_a006_num_miscelaneo_detalle_asignacion = tipo_asignacion.pk_num_miscelaneo_detalle
            INNER JOIN rh_b001_empleado ON rh_c091_utiles_beneficio.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            LEFT JOIN lg_b022_proveedor AS proveedor ON rh_c091_utiles_beneficio.lg_b022_num_proveedor = proveedor.pk_num_proveedor
            LEFT JOIN a003_persona AS persona_proveedor ON proveedor.fk_a003_num_persona_proveedor = persona_proveedor.pk_num_persona
            INNER JOIN rh_c092_utiles_beneficarios ON rh_c092_utiles_beneficarios.fk_rhc091_utiles_beneficio = rh_c091_utiles_beneficio.pk_num_utiles_beneficio
            INNER JOIN rh_c015_carga_familiar ON rh_c092_utiles_beneficarios.fk_c015_num_carga_familiar = rh_c015_carga_familiar.pk_num_carga
            INNER JOIN a003_persona AS persona_carga_familiar ON rh_c015_carga_familiar.fk_a003_num_persona = persona_carga_familiar.pk_num_persona
            WHERE tipo_asignacion.cod_detalle='AF'
            ORDER BY
            rh_c091_utiles_beneficio.pk_num_utiles_beneficio DESC
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }



    #permite mostrar  los beneficios de utiles registrados
    public function metMostrarBeneficioUtiles($idBeneficio)
    {
        $con =  $this->_db->query("
            SELECT
            rh_c091_utiles_beneficio.pk_num_utiles_beneficio,
            rh_c091_utiles_beneficio.num_orden,
            rh_c091_utiles_beneficio.fk_rhc056_num_beneficio_utiles,
            rh_c056_beneficio_utiles.ind_descripcion_beneficio,
            rh_c091_utiles_beneficio.fk_a006_num_miscelaneo_detalle_asignacion,
            tipo_asignacion.cod_detalle,
            tipo_asignacion.ind_nombre_detalle,
            rh_c091_utiles_beneficio.fk_rhb001_num_empleado,
            a003_persona.ind_cedula_documento,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_empleado,
            rh_c091_utiles_beneficio.fec_fecha,
            rh_c091_utiles_beneficio.lg_b022_num_proveedor,
            CONCAT(persona_proveedor.ind_nombre1,' ',persona_proveedor.ind_nombre2,' ',persona_proveedor.ind_apellido1,' ',persona_proveedor.ind_apellido2) AS proveedor,
            rh_c091_utiles_beneficio.num_monto,
            rh_c091_utiles_beneficio.ind_estado,
            rh_c091_utiles_beneficio.fec_ultima_modificacion,
            rh_c091_utiles_beneficio.fk_a018_num_seguridad_usuario,
            a004_dependencia.ind_dependencia
            FROM
            rh_c091_utiles_beneficio
            INNER JOIN rh_c056_beneficio_utiles ON rh_c091_utiles_beneficio.fk_rhc056_num_beneficio_utiles = rh_c056_beneficio_utiles.pk_num_beneficio_utiles
            INNER JOIN a006_miscelaneo_detalle AS tipo_asignacion ON rh_c091_utiles_beneficio.fk_a006_num_miscelaneo_detalle_asignacion = tipo_asignacion.pk_num_miscelaneo_detalle
            INNER JOIN rh_b001_empleado ON rh_c091_utiles_beneficio.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            LEFT JOIN lg_b022_proveedor AS proveedor ON rh_c091_utiles_beneficio.lg_b022_num_proveedor = proveedor.pk_num_proveedor
            LEFT JOIN a003_persona AS persona_proveedor ON proveedor.fk_a003_num_persona_proveedor = persona_proveedor.pk_num_persona
            INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
            INNER JOIN a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
            WHERE  rh_c091_utiles_beneficio.pk_num_utiles_beneficio='$idBeneficio'
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    #permite mostrar  las asignaciones de utiles registrados para mostrarlos en el funcionario
    public function metMostrarAsignacionUtiles($idAsignacion)
    {
        $con =  $this->_db->query("
            SELECT
            rh_c093_utiles_asignacion.pk_num_utiles_asignacion,
            rh_c093_utiles_asignacion.fk_rh091_utiles_beneficio,
            rh_c091_utiles_beneficio.num_orden,
            rh_c093_utiles_asignacion.fk_rhc056_num_beneficio_utiles,
            rh_c056_beneficio_utiles.ind_descripcion_beneficio,
            rh_c056_beneficio_utiles.num_monto_asignado,
            rh_c093_utiles_asignacion.fk_rhb001_num_empleado,
            persona_empleado.pk_num_persona,
            persona_empleado.ind_cedula_documento,
            CONCAT(persona_empleado.ind_nombre1,' ',persona_empleado.ind_nombre2,' ',persona_empleado.ind_apellido1,' ',persona_empleado.ind_apellido2) AS nombre_funcionario,
            CONCAT(persona_carga_familiar.ind_nombre1,' ',persona_carga_familiar.ind_nombre2,' ',persona_carga_familiar.ind_apellido1,' ',persona_carga_familiar.ind_apellido2) AS nombre_hijo,
            rh_c093_utiles_asignacion.fk_rhc015_num_carga_familiar,
            rh_c093_utiles_asignacion.fk_rhc064_nivel_grado_instruccion,
            rh_c064_nivel_grado_instruccion.ind_nombre_nivel_grado,
            rh_c093_utiles_asignacion.num_anio,
            rh_c093_utiles_asignacion.num_factura,
            rh_c093_utiles_asignacion.fec_fecha_factura,
            rh_c093_utiles_asignacion.num_excento,
            rh_c093_utiles_asignacion.num_base_imponible,
            rh_c093_utiles_asignacion.num_iva,
            rh_c093_utiles_asignacion.num_monto,
            rh_c093_utiles_asignacion.ind_estado,
            rh_c093_utiles_asignacion.fec_ultima_modificacion,
            rh_c093_utiles_asignacion.fk_a018_num_seguridad_usuario,
            a004_dependencia.ind_dependencia,
            rh_c091_utiles_beneficio.fec_fecha
            FROM
            rh_c093_utiles_asignacion
            INNER JOIN rh_c091_utiles_beneficio ON rh_c093_utiles_asignacion.fk_rh091_utiles_beneficio = rh_c091_utiles_beneficio.pk_num_utiles_beneficio
            INNER JOIN rh_c015_carga_familiar ON rh_c093_utiles_asignacion.fk_rhc015_num_carga_familiar = rh_c015_carga_familiar.pk_num_carga
            INNER JOIN rh_c064_nivel_grado_instruccion ON rh_c093_utiles_asignacion.fk_rhc064_nivel_grado_instruccion = rh_c064_nivel_grado_instruccion.pk_num_nivel_grado
            INNER JOIN rh_c056_beneficio_utiles ON rh_c093_utiles_asignacion.fk_rhc056_num_beneficio_utiles = rh_c056_beneficio_utiles.pk_num_beneficio_utiles
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c093_utiles_asignacion.fk_rhb001_num_empleado
            INNER JOIN a003_persona AS persona_empleado ON rh_b001_empleado.fk_a003_num_persona = persona_empleado.pk_num_persona
            INNER JOIN a003_persona AS persona_carga_familiar ON rh_c015_carga_familiar.fk_a003_num_persona = persona_carga_familiar.pk_num_persona
            INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
            INNER JOIN a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
            WHERE  rh_c093_utiles_asignacion.pk_num_utiles_asignacion='$idAsignacion'
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }

    public function metMostrarAsignacionUtilesDetalle($idAsignacion)
    {
        $con =  $this->_db->query("
            SELECT
              *
            FROM
             rh_c094_utiles_asignacion_detalle
            WHERE  fk_rhc093_utiles_asignacion='$idAsignacion'
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #permite mostrar los hijos beneficiarios del beneficio en cuestion
    public function metMostrarBeneficiarios($idBeneficio)
    {
        $con =  $this->_db->query("
            SELECT
            rh_c091_utiles_beneficio.pk_num_utiles_beneficio,
            rh_c092_utiles_beneficarios.pk_num_utiles_beneficiarios,
            rh_c092_utiles_beneficarios.fk_c015_num_carga_familiar,
            rh_c092_utiles_beneficarios.num_monto_asignado,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_hijo,
            a003_persona.ind_nombre1,
            a003_persona.ind_nombre2,
            a003_persona.ind_apellido1,
            a003_persona.ind_apellido2
            FROM
            rh_c091_utiles_beneficio
            INNER JOIN rh_c092_utiles_beneficarios ON rh_c092_utiles_beneficarios.fk_rhc091_utiles_beneficio = rh_c091_utiles_beneficio.pk_num_utiles_beneficio
            INNER JOIN rh_c015_carga_familiar ON rh_c092_utiles_beneficarios.fk_c015_num_carga_familiar = rh_c015_carga_familiar.pk_num_carga
            INNER JOIN a003_persona ON rh_c015_carga_familiar.fk_a003_num_persona = a003_persona.pk_num_persona
            WHERE  rh_c091_utiles_beneficio.pk_num_utiles_beneficio='$idBeneficio'
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #permite obtener los datos del empleado
    public function metObtenerDatosEmpleado($idEmpleado)
    {

        $con =  $this->_db->query("

                SELECT
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                rh_c076_empleado_organizacion.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa AS organismo,
                a003_persona.fk_a006_num_miscelaneo_detalle_sexo,
                sexo.ind_nombre_detalle AS sexo,
                a003_persona.fec_nacimiento,
                rh_c076_empleado_organizacion.fk_a004_num_dependencia,
                a004_dependencia.ind_dependencia AS dependencia,
                rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo,
                rh_c063_puestos.ind_descripcion_cargo AS cargo,
                rh_c063_puestos.num_sueldo_basico,
                rh_c005_empleado_laboral.fec_ingreso,
                rh_b001_empleado.num_estatus AS estatus_empleado,
                a003_persona.num_estatus AS estatus_persona,
                rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador,
                tipo_trabajador.ind_nombre_detalle AS tipo_trabajador
                FROM
                rh_b001_empleado
                INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN a006_miscelaneo_detalle AS sexo ON a003_persona.fk_a006_num_miscelaneo_detalle_sexo = sexo.pk_num_miscelaneo_detalle
                INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                WHERE rh_b001_empleado.pk_num_empleado='$idEmpleado'

        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }


    #permite registrar los requisitos exigidos por el ente para poder optar al beneficio de utiles
    public function metRegistrarRequisitosUtiles($datos)
    {

        $this->_db->beginTransaction();

        /*regogo los datos enviados en el array*/
        $periodo  = $datos[0];
        $empleado = $datos[1];
        $fecha    = $datos[2];
        $beneficiarios = $datos[3];//benefiacios carga familiar
        $t_requisitos  = $datos[4];//requisitos entregados
        $tmp  = $datos[5];//indica la carpeta temporal donde estan almacenadas los requisitos


        #inserto nuevo registro
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c089_beneficio_utiles_requisito
             SET
                fk_rhc056_num_beneficio_utiles=:fk_rhc056_num_beneficio_utiles,
                fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                fec_fecha=:fec_fecha,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            "
        );

        $registro->execute(array(
            'fk_rhc056_num_beneficio_utiles' => $periodo,
            'fk_rhb001_num_empleado' => $empleado,
            'fec_fecha' => $fecha
        ));

        $id_requisito = $this->_db->lastInsertId();

        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            #inserto el detalle de los beneficiarios
            $lista_carga_familiar = explode("#",$beneficiarios);
            array_pop($lista_carga_familiar);//eliminamos el ultimo elemento del array (que esta en blanco)

            $lista_requisitos = explode("#",$t_requisitos);
            array_pop($lista_requisitos);//eliminamos el ultimo elemento del array (que esta en blanco)

            $aux = count($lista_carga_familiar);

            $registrodet = $this->_db->prepare(
                "INSERT INTO
                    rh_c090_beneficio_utiles_requisito_detalle
                 SET
                    fk_rhc089_beneficio_utiles_requisitos=:fk_rhc089_beneficio_utiles_requisitos,
                    fk_rhc015_num_carga_familiar=:fk_rhc015_num_carga_familiar,
                    fk_a006_num_miscelaneo_detalle_requisito=:fk_a006_num_miscelaneo_detalle_requisito,
                    num_flag_entregado=1,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            ");

            for($i=0; $i<$aux; $i++){

                $registrodet->execute(array(
                    'fk_rhc089_beneficio_utiles_requisitos' => $id_requisito,
                    'fk_rhc015_num_carga_familiar' => $lista_carga_familiar[$i],
                    'fk_a006_num_miscelaneo_detalle_requisito' => $lista_requisitos[$i]
                ));

            }

            $error2 = $registrodet->errorInfo();

            if(!empty($error2[1]) && !empty($error2[2])){
                $this->_db->rollBack();
                return $error2;
            }else{

                #verificamos si existen imagen en la carpetas temporales
                $ruta_tmp = "publico/imagenes/modRH/requisitosUtiles/tmp/".$tmp."/";
                $ruta_des = "publico/imagenes/modRH/requisitosUtiles/".$id_requisito."/";

                if(file_exists($ruta_tmp)){

                    if ($vcarga = opendir($ruta_tmp)){

                        mkdir($ruta_des, 0777);
                        chmod($ruta_des,0777);

                        while($file = readdir($vcarga)){//lo recorro enterito

                            if (!is_dir($ruta_tmp.$file)) //pregunto si no es directorio
                            {
                                if(copy($ruta_tmp.$file, $ruta_des.$file)) //como no es directorio, copio de origen a destino
                                {
                                    chmod($ruta_des.$file,0777);
                                }else{
                                    rmdir($ruta_des);
                                }

                            }else{

                            }

                        }

                        foreach(glob($ruta_tmp."*.*") as $archivos_carpeta)
                        {
                            unlink($archivos_carpeta);     // Eliminamos todos los archivos de la carpeta hasta dejarla vacia
                        }
                        rmdir($ruta_tmp);

                    }

                }

                $this->_db->commit();
                return $id_requisito;


            }

        }

    }


    #permite registrar el beneficio de utiles
    public function metRegistrarBeneficioUtiles($datos)
    {

        $this->_db->beginTransaction();

        /*recojo los datos enviados en el array*/
        $orden        = $datos[0];
        $t_asignacion = $datos[1];
        $b_utiles     = $datos[2];
        $empleado     = $datos[3];//empleado
        $fecha        = $datos[4];//fecha
        $proveedor    = $datos[5];//proveedor
        $monto_t      = $datos[6];//monto_total del beneficio
        $estatus      = $datos[7];//estaus preparacion
        $lista_carga  = $datos[8];//lista de cargar familiar
        $monto_asig   = $datos[9];//monto asignado por carga familiar
        $obs_preparado= strtoupper($datos[10]);


        #inserto nuevo registro
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c091_utiles_beneficio
             SET
                num_orden=:num_orden,
                fk_rhc056_num_beneficio_utiles=:fk_rhc056_num_beneficio_utiles,
                fk_a006_num_miscelaneo_detalle_asignacion=:fk_a006_num_miscelaneo_detalle_asignacion,
                fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                fec_fecha=:fec_fecha,
                lg_b022_num_proveedor=:lg_b022_num_proveedor,
                num_monto=:num_monto,
                ind_estado=:ind_estado,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            "
        );

        $registro->execute(array(
            'num_orden' => $orden,
            'fk_rhc056_num_beneficio_utiles' => $b_utiles,
            'fk_a006_num_miscelaneo_detalle_asignacion' => $t_asignacion,
            'fk_rhb001_num_empleado' => $empleado,
            'fec_fecha' => $fecha,
            'lg_b022_num_proveedor' => $proveedor,
            'num_monto' => str_replace(".", ",", $monto_t),
            'ind_estado' => $estatus
        ));

        $id_beneficio = $this->_db->lastInsertId();

        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            $lista_carga_familiar = explode("#",$lista_carga);
            array_pop($lista_carga_familiar);//eliminamos el ultimo elemento del array (que esta en blanco)

            $lista_montos = explode("#",$monto_asig);
            array_pop($lista_montos);//eliminamos el ultimo elemento del array (que esta en blanco)

            $aux = count($lista_carga_familiar);


            $registrodet = $this->_db->prepare(
                "INSERT INTO
                    rh_c092_utiles_beneficarios
                 SET
                    fk_rhc091_utiles_beneficio = :fk_rhc091_utiles_beneficio,
                    fk_c015_num_carga_familiar=:fk_c015_num_carga_familiar,
                    num_monto_asignado=:num_monto_asignado,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            ");

            for($i=0; $i<$aux; $i++){

                $registrodet->execute(array(
                    'fk_rhc091_utiles_beneficio' => $id_beneficio,
                    'fk_c015_num_carga_familiar' => $lista_carga_familiar[$i],
                    'num_monto_asignado' => str_replace(".", ",", $lista_montos[$i])
                ));

            }

            $error2 = $registrodet->errorInfo();

            if(!empty($error2[1]) && !empty($error2[2])){
                $this->_db->rollBack();
                return $error2;
            }else{

                //finalizo con el estatus
                //*******************************************************************************************************
                $ESTATUS = 'PR';//preparado
                $FLAG = 1;

                //ejecuto procedimiento alamcenado para guardar las operaciones
                $con =  $this->_db->prepare("
                                CALL w_rh_operacionesBeneficioUtiles(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
                            ");
                $con->execute(array(
                    ':estatus' => $ESTATUS,
                    ':id' => $id_beneficio,
                    ':empleado' => $empleado,
                    ':flag' => $FLAG,
                    ':fecha' => $fecha,
                    ':observaciones' => $obs_preparado,
                    ':usuario' => $this->atIdUsuario
                ));

                $this->_db->commit();
                return $id_beneficio;

            }

        }

    }


    #permite modificar el beneficio de utiles
    public function metModificarBeneficioUtiles($idBeneficio,$datos)
    {

        $this->_db->beginTransaction();

        /*recojo los datos enviados en el array*/
        $orden        = $datos[0];
        $t_asignacion = $datos[1];
        $b_utiles     = $datos[2];
        $empleado     = $datos[3];//empleado
        $fecha        = $datos[4];//fecha
        $proveedor    = $datos[5];//proveedor
        $monto_t      = $datos[6];//monto_total del beneficio
        $estatus      = $datos[7];//estaus preparacion
        $lista_carga  = $datos[8];//lista de cargar familiar
        $monto_asig   = $datos[9];//monto asignado por carga familiar
        $obs_preparado= strtoupper($datos[10]);


        #inserto nuevo registro
        $registro = $this->_db->prepare(
            "UPDATE
                rh_c091_utiles_beneficio
             SET
                fk_a006_num_miscelaneo_detalle_asignacion=:fk_a006_num_miscelaneo_detalle_asignacion,
                lg_b022_num_proveedor=:lg_b022_num_proveedor,
                num_monto=:num_monto,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE pk_num_utiles_beneficio='$idBeneficio'
            "
        );

        $registro->execute(array(
            'fk_a006_num_miscelaneo_detalle_asignacion' => $t_asignacion,
            'lg_b022_num_proveedor' => $proveedor,
            'num_monto' => $monto_t
        ));

        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            $lista_carga_familiar = explode("#",$lista_carga);
            array_pop($lista_carga_familiar);//eliminamos el ultimo elemento del array (que esta en blanco)

            $lista_montos = explode("#",$monto_asig);
            array_pop($lista_montos);//eliminamos el ultimo elemento del array (que esta en blanco)

            $aux = count($lista_carga_familiar);

            /*ELIMINO TODOS LOS BENEFICIARIOS PRIMERO PARA EVITAR CONFLICTOS*/
            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c092_utiles_beneficarios WHERE fk_rhc091_utiles_beneficio=:fk_rhc091_utiles_beneficio
            ");
            $eliminar->execute(array(
                'fk_rhc091_utiles_beneficio'=>$idBeneficio
            ));
            $errorE=$eliminar->errorInfo();

            if(!empty($errorE[1]) && !empty($errorE[2])){
                $this->_db->rollBack();
                return $errorE;
            }else{

                $registrodet = $this->_db->prepare(
                    "INSERT INTO
                            rh_c092_utiles_beneficarios
                         SET
                            fk_rhc091_utiles_beneficio = :fk_rhc091_utiles_beneficio,
                            fk_c015_num_carga_familiar=:fk_c015_num_carga_familiar,
                            num_monto_asignado=:num_monto_asignado,
                            fec_ultima_modificacion=NOW(),
                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    ");

                for($i=0; $i<$aux; $i++){

                    $registrodet->execute(array(
                        'fk_rhc091_utiles_beneficio' => $idBeneficio,
                        'fk_c015_num_carga_familiar' => $lista_carga_familiar[$i],
                        'num_monto_asignado' => str_replace(".", ",", $lista_montos[$i])
                    ));

                }

                $error2 = $registrodet->errorInfo();

                if(!empty($error2[1]) && !empty($error2[2])){
                    $this->_db->rollBack();
                    return $error2;
                }else{

                    $con = $this->_db->prepare(
                        "UPDATE
                            rh_c095_operaciones_beneficios_utiles
                         SET
                            txt_observaciones='$obs_preparado'
                         WHERE fk_rhc091_num_utiles_beneficio='$idBeneficio' and ind_estado='PR' and num_flag_estatus=1
                         "
                    );
                    $con->execute();
                    $error1 = $con->errorInfo();
                    if(!empty($error1[1]) && !empty($error1[2])){
                        $this->_db->rollBack();
                        return $error1;
                    }else{
                        $this->_db->commit();
                        return $idBeneficio;
                    }

                }

            }

        }

    }

    #permite  mostrar detalle operacion segun beneficio y estatus
    public function metMostrarBeneficiosOperaciones($idBeneficio,$estatus)
    {
        $con = $this->_db->query("
            SELECT
                rh_c095_operaciones_beneficios_utiles.pk_num_operaciones_beneficios_utiles,
                rh_c091_utiles_beneficio.pk_num_utiles_beneficio,
                rh_c095_operaciones_beneficios_utiles.ind_estado AS txt_estatus,
                rh_c095_operaciones_beneficios_utiles.num_flag_estatus,
                rh_c095_operaciones_beneficios_utiles.fec_operacion,
                rh_c095_operaciones_beneficios_utiles.txt_observaciones,
                rh_c095_operaciones_beneficios_utiles.fk_a018_num_seguridad_usuario,
                CONCAT(
                    a003_persona.ind_nombre1,
                    ' ',
                    a003_persona.ind_nombre2,
                    ' ',
                    a003_persona.ind_apellido1,
                    ' ',
                    a003_persona.ind_apellido2
                ) AS nombre_registro,
                CONCAT_WS(
                    ' ',
                    persona_operacion.ind_nombre1,
                    persona_operacion.ind_nombre2,
                    persona_operacion.ind_apellido1,
                    persona_operacion.ind_apellido2
                ) AS nombre_operacion
            FROM
                rh_c091_utiles_beneficio
            INNER JOIN rh_c095_operaciones_beneficios_utiles ON rh_c095_operaciones_beneficios_utiles.fk_rhc091_num_utiles_beneficio = rh_c091_utiles_beneficio.pk_num_utiles_beneficio
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c095_operaciones_beneficios_utiles.fk_rhb001_num_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = rh_c095_operaciones_beneficios_utiles.fk_a018_num_seguridad_usuario
            INNER JOIN rh_b001_empleado AS empleado_operacion ON empleado_operacion.pk_num_empleado = a018_seguridad_usuario.fk_rhb001_num_empleado
            INNER JOIN a003_persona AS persona_operacion ON persona_operacion.pk_num_persona = empleado_operacion.fk_a003_num_persona
            WHERE rh_c091_utiles_beneficio.pk_num_utiles_beneficio='$idBeneficio' AND rh_c095_operaciones_beneficios_utiles.ind_estado='$estatus'
             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metMostrarAsignacionOperaciones($idAsignacion,$estatus)
    {
        $con = $this->_db->query("
            SELECT
            rh_c096_operaciones_asignaciones_utiles.pk_num_operaciones_asignaciones_utiles,
            rh_c093_utiles_asignacion.pk_num_utiles_asignacion,
            rh_c096_operaciones_asignaciones_utiles.ind_estado as txt_estatus,
            rh_c096_operaciones_asignaciones_utiles.num_flag_estatus,
            rh_c096_operaciones_asignaciones_utiles.fec_operacion,
            rh_c096_operaciones_asignaciones_utiles.txt_observaciones,
            rh_c096_operaciones_asignaciones_utiles.fk_a018_num_seguridad_usuario,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) as nombre_registro,
            CONCAT_WS(
                    ' ',
                    persona_operacion.ind_nombre1,
                    persona_operacion.ind_nombre2,
                    persona_operacion.ind_apellido1,
                    persona_operacion.ind_apellido2
                ) AS nombre_operacion
            FROM
            rh_c093_utiles_asignacion
            INNER JOIN rh_c096_operaciones_asignaciones_utiles ON rh_c096_operaciones_asignaciones_utiles.fk_rhc093_utiles_asignacion = rh_c093_utiles_asignacion.pk_num_utiles_asignacion
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c096_operaciones_asignaciones_utiles.fk_rhb001_num_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = rh_c096_operaciones_asignaciones_utiles.fk_a018_num_seguridad_usuario
            INNER JOIN rh_b001_empleado AS empleado_operacion ON empleado_operacion.pk_num_empleado = a018_seguridad_usuario.fk_rhb001_num_empleado
            INNER JOIN a003_persona AS persona_operacion ON persona_operacion.pk_num_persona = empleado_operacion.fk_a003_num_persona
            WHERE rh_c093_utiles_asignacion.pk_num_utiles_asignacion='$idAsignacion' AND rh_c096_operaciones_asignaciones_utiles.ind_estado='$estatus'
             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #permite revisar el beneficio de utiles
    public function metRevisarBeneficioUtiles($idBeneficio,$observaciones)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
               select * from rh_c095_operaciones_beneficios_utiles
               where fk_rhc091_num_utiles_beneficio='$idBeneficio'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $fecha = date("Y-m-d");


        $registro = $this->_db->prepare(
            "UPDATE
                    rh_c091_utiles_beneficio
                 SET
                    ind_estado = :ind_estado,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_utiles_beneficio='$idBeneficio'
            ");

        $registro->execute(array(
            'ind_estado' => 'RV',
        ));
        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            /*ejecuto procedimiento alamcenado para guardar las operaciones*/
            $con =  $this->_db->prepare("
                    CALL w_rh_operacionesBeneficioUtiles(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
            $con->execute(array(
                ':estatus' => 'RV',
                ':id' => $idBeneficio,
                ':empleado' => $resultado['fk_rhb001_num_empleado'],
                ':flag' => 1,
                ':fecha' => $fecha,
                ':observaciones' => strtoupper($observaciones),
                ':usuario' => $this->atIdUsuario
            ));
            $this->_db->commit();
            return $idBeneficio;

        }

    }



    #permite aprobar el beneficio de utiles
    public function metAprobarBeneficioUtiles($idBeneficio,$observaciones)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
               select * from rh_c095_operaciones_beneficios_utiles
               where fk_rhc091_num_utiles_beneficio='$idBeneficio'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $fecha = date("Y-m-d");


        $registro = $this->_db->prepare(
            "UPDATE
                    rh_c091_utiles_beneficio
                 SET
                    ind_estado = :ind_estado,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE pk_num_utiles_beneficio='$idBeneficio'
            ");

        $registro->execute(array(
            'ind_estado' => 'AP',
        ));
        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            /*ejecuto procedimiento alamcenado para guardar las operaciones*/
            $con =  $this->_db->prepare("
                    CALL w_rh_operacionesBeneficioUtiles(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
            $con->execute(array(
                ':estatus' => 'AP',
                ':id' => $idBeneficio,
                ':empleado' => $resultado['fk_rhb001_num_empleado'],
                ':flag' => 1,
                ':fecha' => $fecha,
                ':observaciones' => strtoupper($observaciones),
                ':usuario' => $this->atIdUsuario
            ));
            $this->_db->commit();
            return $idBeneficio;

        }

    }


    #permite registrar la asignacion de utiles
    public function metRegistrarAsignacionUtiles($arrayDatos,$detalle)
    {

        $this->_db->beginTransaction();

        /*organizo los datos del arreglo*/
        $idBeneficio = $arrayDatos[0]; #beneficio de utiles
        $empleado    = $arrayDatos[1]; #empleado
        $carga_fam   = $arrayDatos[2]; #carga familiar
        $n_gra_inst  = $arrayDatos[3]; #nievl grado de instrucion de la carga familiar
        $b_utiles    = $arrayDatos[4]; #beneficio utiles creado en el maestro
        $anio        = $arrayDatos[5]; #año en curso
        $factura     = $arrayDatos[6]; #numero de la factura
        $fecha       = $arrayDatos[7]; #fecha de la factura
        $exento      = $arrayDatos[8]; #exento
        $bi_g        = $arrayDatos[9]; #base imponible
        $iva_g       = $arrayDatos[10]; #total iva
        $total       = $arrayDatos[11]; #total con iva
        $estatus     = $arrayDatos[12]; #estatus
        $observacion = mb_strtoupper($arrayDatos[13],'utf-8'); #observacion de preparado


        #datos del detalle
        $idItem = explode(" ,",$detalle[0]);
        $cantidadItem = explode(" ,",$detalle[1]);
        $descripcionItem = explode(" ,",$detalle[2]);
        $montoItem = explode(" ,",$detalle[3]);
        $gravableItem = explode(",",$detalle[4]);

        #guardo registro de asignacion
        #inserto nuevo registro
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_c093_utiles_asignacion
             SET
                fk_rh091_utiles_beneficio=:fk_rh091_utiles_beneficio,
                fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                fk_rhc015_num_carga_familiar=:fk_rhc015_num_carga_familiar,
                fk_rhc064_nivel_grado_instruccion=:fk_rhc064_nivel_grado_instruccion,
                fk_rhc056_num_beneficio_utiles=:fk_rhc056_num_beneficio_utiles,
                num_anio=:num_anio,
                num_factura=:num_factura,
                fec_fecha_factura=:fec_fecha_factura,
                num_excento=:num_excento,
                num_base_imponible=:num_base_imponible,
                num_iva=:num_iva,
                num_monto=:num_monto,
                ind_estado=:ind_estado,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            "
        );

        $f = explode("-",$fecha);

        $registro->execute(array(
            'fk_rh091_utiles_beneficio' => $idBeneficio,
            'fk_rhb001_num_empleado' => $empleado,
            'fk_rhc015_num_carga_familiar' => $carga_fam,
            'fk_rhc064_nivel_grado_instruccion' => $n_gra_inst,
            'fk_rhc056_num_beneficio_utiles' => $b_utiles,
            'num_anio' => $anio,
            'num_factura' => $factura,
            'fec_fecha_factura' => $f[2]."-".$f[1]."-".$f[0],
            'num_excento' => $exento,
            'num_base_imponible' => $bi_g,
            'num_iva' => $iva_g,
            'num_monto' => $total,
            'ind_estado' => $estatus
        ));

        $idAsignacion = $this->_db->lastInsertId();

        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            #guardo detalle de la asignacion

            $registroDetalle = $this->_db->prepare(
                "INSERT INTO
                    rh_c094_utiles_asignacion_detalle
                 SET
                    fk_rhc093_utiles_asignacion=:fk_rhc093_utiles_asignacion,
                    fk_a006_num_miscelaneo_detalle_utiles=:fk_a006_num_miscelaneo_detalle_utiles,
                    ind_descripcion_utiles=:ind_descripcion_utiles,
                    num_cantidad=:num_cantidad,
                    ind_exento=:ind_exento,
                    num_descuento=:num_descuento,
                    nom_monto=:nom_monto,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            "
            );


            for($i=0; $i<count($idItem); $i++)
            {

                $registroDetalle->execute(array(
                    'fk_rhc093_utiles_asignacion' => $idAsignacion,
                    'fk_a006_num_miscelaneo_detalle_utiles' => $idItem[$i],
                    'ind_descripcion_utiles' => $descripcionItem[$i],
                    'num_cantidad' => $cantidadItem[$i],
                    'ind_exento' => $gravableItem[$i],
                    'num_descuento' => 0,
                    'nom_monto' => str_replace(",",".", $montoItem[$i])
                ));

            }

            $errorDetalle = $registroDetalle->errorInfo();

            if(!empty($errorDetalle[1]) && !empty($errorDetalle[2])){
                $this->_db->rollBack();
                return $errorDetalle;
            }else{

                #finalizo con el estatus del registro
                $fecha = date("Y-m-d");
                $FLAG = 1;
                //ejecuto procedimiento alamcenado para guardar las operaciones
                $con =  $this->_db->prepare("
                                CALL w_rh_operacionesAsignacionUtiles(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
                            ");
                $con->execute(array(
                    ':estatus' => $estatus,
                    ':id' => $idAsignacion,
                    ':empleado' => $empleado,
                    ':fecha' => $fecha,
                    ':flag' => $FLAG,
                    ':observaciones' => $observacion,
                    ':usuario' => $this->atIdUsuario
                ));

                $errorProc = $con->errorInfo();

                if(!empty($errorProc[1]) && !empty($errorProc[2])){
                    $this->_db->rollBack();
                    return $errorProc;
                }else{


                    #actualizo el estado del registro del beneficio a asignado
                    $up = $this->_db->prepare(
                        "UPDATE
                            rh_c091_utiles_beneficio
                         SET
                            ind_estado='AS',
                            fec_ultima_modificacion=NOW(),
                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                         WHERE pk_num_utiles_beneficio='$idBeneficio'
                        ");

                    $up->execute();

                    $this->_db->commit();
                    return $idAsignacion;

                }

            }

        }

    }


    #permite modificar la asignacion de utiles
    public function metModificarAsignacionUtiles($idAsignacion,$arrayDatos,$detalle)
    {

        $this->_db->beginTransaction();

        /*organizo los datos del arreglo*/
        $idBeneficio = $arrayDatos[0]; #beneficio de utiles
        $empleado    = $arrayDatos[1]; #empleado
        $carga_fam   = $arrayDatos[2]; #carga familiar
        $n_gra_inst  = $arrayDatos[3]; #nievl grado de instrucion de la carga familiar
        $b_utiles    = $arrayDatos[4]; #beneficio utiles creado en el maestro
        $anio        = $arrayDatos[5]; #año en curso
        $factura     = $arrayDatos[6]; #numero de la factura
        $fecha       = $arrayDatos[7]; #fecha de la factura
        $exento      = $arrayDatos[8]; #exento
        $bi_g        = $arrayDatos[9]; #base imponible
        $iva_g       = $arrayDatos[10]; #total iva
        $total       = $arrayDatos[11]; #total con iva
        $estatus     = $arrayDatos[12]; #estatus
        $observacion = mb_strtoupper($arrayDatos[13],'utf-8'); #observacion de preparado


        #datos del detalle
        $idItem = explode(" ,",$detalle[0]);
        $cantidadItem = explode(" ,",$detalle[1]);
        $descripcionItem = explode(" ,",$detalle[2]);
        $montoItem = explode(" ,",$detalle[3]);
        $gravableItem = explode(",",$detalle[4]);

        #guardo registro de asignacion
        #inserto nuevo registro
        $registro = $this->_db->prepare(
            "UPDATE
                rh_c093_utiles_asignacion
             SET
                num_excento=:num_excento,
                num_base_imponible=:num_base_imponible,
                num_iva=:num_iva,
                num_monto=:num_monto,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE pk_num_utiles_asignacion='$idAsignacion'
            "
        );

        $f = explode("-",$fecha);

        $registro->execute(array(
            'num_excento' => $exento,
            'num_base_imponible' => $bi_g,
            'num_iva' => $iva_g,
            'num_monto' => $total
        ));

        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            #elimino el detalle para luego insertar
            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c094_utiles_asignacion_detalle WHERE fk_rhc093_utiles_asignacion=:fk_rhc093_utiles_asignacion
            ");
            $eliminar->execute(array(
                'fk_rhc093_utiles_asignacion'=>$idAsignacion
            ));
            $errorE=$eliminar->errorInfo();

            if(!empty($errorE[1]) && !empty($errorE[2])){
                $this->_db->rollBack();
                return $errorE;
            }else{

                $registroDetalle = $this->_db->prepare(
                    "INSERT INTO
                    rh_c094_utiles_asignacion_detalle
                 SET
                    fk_rhc093_utiles_asignacion=:fk_rhc093_utiles_asignacion,
                    fk_a006_num_miscelaneo_detalle_utiles=:fk_a006_num_miscelaneo_detalle_utiles,
                    ind_descripcion_utiles=:ind_descripcion_utiles,
                    num_cantidad=:num_cantidad,
                    ind_exento=:ind_exento,
                    num_descuento=:num_descuento,
                    nom_monto=:nom_monto,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 ");

                for($i=0; $i<count($idItem); $i++)
                {

                    $registroDetalle->execute(array(
                        'fk_rhc093_utiles_asignacion' => $idAsignacion,
                        'fk_a006_num_miscelaneo_detalle_utiles' => $idItem[$i],
                        'ind_descripcion_utiles' => $descripcionItem[$i],
                        'num_cantidad' => $cantidadItem[$i],
                        'ind_exento' => $gravableItem[$i],
                        'num_descuento' => 0,
                        'nom_monto' => str_replace(",",".", $montoItem[$i])
                    ));

                }

                $errorDetalle = $registroDetalle->errorInfo();

                if(!empty($errorDetalle[1]) && !empty($errorDetalle[2])){
                    $this->_db->rollBack();
                    return $errorDetalle;
                }else{

                    #finalizo con el estatus del registro

                    $this->_db->commit();
                    return $idAsignacion;

                }

            }

        }

    }


    #permite revisar la asignacion de utiles
    public function metRevisarAsignacionUtiles($idAsignacion,$observaciones)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
               select * from rh_c096_operaciones_asignaciones_utiles
               where fk_rhc093_utiles_asignacion='$idAsignacion'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $fecha = date("Y-m-d");


        $registro = $this->_db->prepare(
            "UPDATE
                    rh_c093_utiles_asignacion
                 SET
                    ind_estado = :ind_estado,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE 	pk_num_utiles_asignacion='$idAsignacion'
            ");

        $registro->execute(array(
            'ind_estado' => 'RV',
        ));
        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            /*ejecuto procedimiento alamcenado para guardar las operaciones*/
            $con =  $this->_db->prepare("
                    CALL w_rh_operacionesAsignacionUtiles(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
            $con->execute(array(
                ':estatus' => 'RV',
                ':id' => $idAsignacion,
                ':empleado' => $resultado['fk_rhb001_num_empleado'],
                ':flag' => 1,
                ':fecha' => $fecha,
                ':observaciones' => strtoupper($observaciones),
                ':usuario' => $this->atIdUsuario
            ));
            $this->_db->commit();
            return $idAsignacion;

        }

    }


    #permite aprobar la asignacion  de utiles
    public function metAprobarAsignacionUtiles($idAsignacion,$observaciones)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
               select * from rh_c096_operaciones_asignaciones_utiles
               where fk_rhc093_utiles_asignacion='$idAsignacion'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $fecha = date("Y-m-d");


        $registro = $this->_db->prepare(
            "UPDATE
                    rh_c093_utiles_asignacion
                 SET
                    ind_estado = :ind_estado,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE 	pk_num_utiles_asignacion='$idAsignacion'
            ");

        $registro->execute(array(
            'ind_estado' => 'AP',
        ));
        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            /*ejecuto procedimiento alamcenado para guardar las operaciones*/
            $con =  $this->_db->prepare("
                    CALL w_rh_operacionesAsignacionUtiles(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
            $con->execute(array(
                ':estatus' => 'AP',
                ':id' => $idAsignacion,
                ':empleado' => $resultado['fk_rhb001_num_empleado'],
                ':flag' => 1,
                ':fecha' => $fecha,
                ':observaciones' => strtoupper($observaciones),
                ':usuario' => $this->atIdUsuario
            ));
            $this->_db->commit();
            return $idAsignacion;

        }

    }


    public function metListarAsignacionesUtiles()
    {
        $con = $this->_db->query("
            SELECT
                rh_c093_utiles_asignacion.pk_num_utiles_asignacion,
                rh_c093_utiles_asignacion.fk_rh091_utiles_beneficio,
                rh_c091_utiles_beneficio.num_orden,
                rh_c093_utiles_asignacion.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona AS num_persona_funcionario,
                CONCAT(
                    a003_persona.ind_nombre1,
                    ' ',
                    a003_persona.ind_nombre2,
                    ' ',
                    a003_persona.ind_apellido1,
                    ' ',
                    a003_persona.ind_apellido2
                ) AS nombre_funcionario,
                rh_c093_utiles_asignacion.fk_rhc015_num_carga_familiar,
                CONCAT(
                    persona_carga_familiar.ind_nombre1,
                    ' ',
                    persona_carga_familiar.ind_nombre2,
                    ' ',
                    persona_carga_familiar.ind_apellido1,
                    ' ',
                    persona_carga_familiar.ind_apellido2
                ) AS nombre_carga_familiar,
                rh_c093_utiles_asignacion.fk_rhc064_nivel_grado_instruccion,
                rh_c064_nivel_grado_instruccion.ind_nombre_nivel_grado,
                rh_c093_utiles_asignacion.fk_rhc056_num_beneficio_utiles,
                rh_c056_beneficio_utiles.ind_descripcion_beneficio,
                rh_c056_beneficio_utiles.num_monto_asignado,
                rh_c093_utiles_asignacion.num_anio,
                rh_c093_utiles_asignacion.num_factura,
                rh_c093_utiles_asignacion.fec_fecha_factura,
                rh_c093_utiles_asignacion.num_excento,
                rh_c093_utiles_asignacion.num_base_imponible,
                rh_c093_utiles_asignacion.num_iva,
                rh_c093_utiles_asignacion.num_monto,
                rh_c093_utiles_asignacion.ind_estado,
                rh_c093_utiles_asignacion.fec_ultima_modificacion,
                rh_c093_utiles_asignacion.fk_a018_num_seguridad_usuario
            FROM
                rh_c093_utiles_asignacion
            INNER JOIN rh_c091_utiles_beneficio ON rh_c093_utiles_asignacion.fk_rh091_utiles_beneficio = rh_c091_utiles_beneficio.pk_num_utiles_beneficio
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c093_utiles_asignacion.fk_rhb001_num_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            INNER JOIN rh_c015_carga_familiar ON rh_c093_utiles_asignacion.fk_rhc015_num_carga_familiar = rh_c015_carga_familiar.pk_num_carga
            INNER JOIN rh_c064_nivel_grado_instruccion ON rh_c093_utiles_asignacion.fk_rhc064_nivel_grado_instruccion = rh_c064_nivel_grado_instruccion.pk_num_nivel_grado
            INNER JOIN rh_c056_beneficio_utiles ON rh_c093_utiles_asignacion.fk_rhc056_num_beneficio_utiles = rh_c056_beneficio_utiles.pk_num_beneficio_utiles
            INNER JOIN a003_persona AS persona_carga_familiar ON rh_c015_carga_familiar.fk_a003_num_persona = persona_carga_familiar.pk_num_persona
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetchAll();

        return $resultado;
    }


    #permite consultar las asignaciones de benficios pendientes
    public function metListarAsignacionesPendientes()
    {
        $con = $this->_db->query("
                SELECT
                rh_c091_utiles_beneficio.pk_num_utiles_beneficio,
                rh_c091_utiles_beneficio.num_orden,
                rh_c091_utiles_beneficio.fk_rhc056_num_beneficio_utiles,
                rh_c056_beneficio_utiles.ind_descripcion_beneficio,
                rh_c091_utiles_beneficio.fk_a006_num_miscelaneo_detalle_asignacion,
                tipo_asignacion.cod_detalle,
                tipo_asignacion.ind_nombre_detalle,
                rh_c091_utiles_beneficio.fk_rhb001_num_empleado,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_empleado,
                rh_c091_utiles_beneficio.fec_fecha,
                rh_c091_utiles_beneficio.lg_b022_num_proveedor,
                CONCAT(persona_proveedor.ind_nombre1,' ',persona_proveedor.ind_nombre2,' ',persona_proveedor.ind_apellido1,' ',persona_proveedor.ind_apellido2) AS proveedor,
                rh_c091_utiles_beneficio.num_monto,
                rh_c091_utiles_beneficio.ind_estado,
                rh_c091_utiles_beneficio.fec_ultima_modificacion,
                rh_c091_utiles_beneficio.fk_a018_num_seguridad_usuario,
                rh_c092_utiles_beneficarios.fk_c015_num_carga_familiar,
                rh_c015_carga_familiar.fk_a003_num_persona AS pk_num_persona_carga_familiar,
                CONCAT(persona_carga_familiar.ind_nombre1,' ',persona_carga_familiar.ind_nombre2,' ',persona_carga_familiar.ind_apellido1,' ',persona_carga_familiar.ind_apellido2) AS nombre_hijo,
                rh_c015_carga_familiar.fk_rhc064_nivel_grado_instruccion,
                rh_c064_nivel_grado_instruccion.ind_nombre_nivel_grado
                FROM
                rh_c091_utiles_beneficio
                INNER JOIN rh_c056_beneficio_utiles ON rh_c091_utiles_beneficio.fk_rhc056_num_beneficio_utiles = rh_c056_beneficio_utiles.pk_num_beneficio_utiles
                INNER JOIN a006_miscelaneo_detalle AS tipo_asignacion ON rh_c091_utiles_beneficio.fk_a006_num_miscelaneo_detalle_asignacion = tipo_asignacion.pk_num_miscelaneo_detalle
                INNER JOIN rh_b001_empleado ON rh_c091_utiles_beneficio.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                LEFT JOIN lg_b022_proveedor AS proveedor ON rh_c091_utiles_beneficio.lg_b022_num_proveedor = proveedor.pk_num_proveedor
                LEFT JOIN a003_persona AS persona_proveedor ON proveedor.fk_a003_num_persona_proveedor = persona_proveedor.pk_num_persona
                INNER JOIN rh_c092_utiles_beneficarios ON rh_c092_utiles_beneficarios.fk_rhc091_utiles_beneficio = rh_c091_utiles_beneficio.pk_num_utiles_beneficio
                INNER JOIN rh_c015_carga_familiar ON rh_c092_utiles_beneficarios.fk_c015_num_carga_familiar = rh_c015_carga_familiar.pk_num_carga
                INNER JOIN a003_persona AS persona_carga_familiar ON rh_c015_carga_familiar.fk_a003_num_persona = persona_carga_familiar.pk_num_persona
                LEFT JOIN rh_c064_nivel_grado_instruccion ON rh_c015_carga_familiar.fk_rhc064_nivel_grado_instruccion = rh_c064_nivel_grado_instruccion.pk_num_nivel_grado
                WHERE tipo_asignacion.cod_detalle='AF' AND rh_c091_utiles_beneficio.ind_estado != 'AS'
                ORDER BY
                rh_c091_utiles_beneficio.pk_num_utiles_beneficio DESC
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetchAll();

        return $resultado;
    }


    #permite consultar informacion acerca del registro del beneficio (PREPARADO, REVISADO, CONFORMADO, APROBADO, ANULADO)
    public function metConsultarInformacionRegistro($idBeneficio)
    {
        $con = $this->_db->query("
               SELECT
                rh_c091_utiles_beneficio.pk_num_utiles_beneficio,
                rh_c095_operaciones_beneficios_utiles.pk_num_operaciones_beneficios_utiles,
                rh_c095_operaciones_beneficios_utiles.ind_estado as txt_estatus,
                rh_c095_operaciones_beneficios_utiles.fec_operacion,
                rh_c095_operaciones_beneficios_utiles.txt_observaciones,
                rh_c095_operaciones_beneficios_utiles.fk_a018_num_seguridad_usuario,
                rh_b001_empleado.fk_a003_num_persona,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_resposable
                FROM
                rh_c091_utiles_beneficio
                INNER JOIN rh_c095_operaciones_beneficios_utiles ON rh_c095_operaciones_beneficios_utiles.fk_rhc091_num_utiles_beneficio = rh_c091_utiles_beneficio.pk_num_utiles_beneficio
                INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = rh_c095_operaciones_beneficios_utiles.fk_a018_num_seguridad_usuario
                INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = a018_seguridad_usuario.fk_rhb001_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                ORDER BY rh_c095_operaciones_beneficios_utiles.pk_num_operaciones_beneficios_utiles DESC
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetchAll();

        return $resultado;
    }


    #permite obtener la ultima orden del beneficio
    public function metUltimaOrden()
    {
        $con =  $this->_db->query("
                select MAX(num_orden) as orden from rh_c091_utiles_beneficio
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        return str_pad($resultado['orden']+1, 6, "0", STR_PAD_LEFT);
    }

    #permite eliminar un hijo beneficiario del beneficio en cuestion
    public function metEliminarBeneficiario($idBeneficio,$beneficiario)
    {

        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c092_utiles_beneficarios WHERE fk_rhc091_utiles_beneficio=:fk_rhc091_utiles_beneficio and fk_c015_num_carga_familiar=:fk_c015_num_carga_familiar
            ");
        $eliminar->execute(array(
            'fk_rhc091_utiles_beneficio'=>$idBeneficio,
            'fk_c015_num_carga_familiar'=>$beneficiario
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idBeneficio;
        }

    }


    #permite anular (reverso) una asignacion de utiles
    public function metAnularAsignacion($idBeneficio,$idAsignacion)
    {

        $this->_db->beginTransaction();

        $fecha = date("Y-m-d");

        $con = $this->_db->query("
               select * from rh_c096_operaciones_asignaciones_utiles
               where fk_rhc093_utiles_asignacion='$idAsignacion'
               and num_flag_estatus=1
        ");


        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        if(strcmp($resultado['ind_estado'],'PR')==0){
            $nuevoEstatus = 'AN';

            $actualizar =  $this->_db->prepare("
                    UPDATE
                        rh_c093_utiles_asignacion
                    SET
                        ind_estado='$nuevoEstatus',
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    WHERE
                        pk_num_utiles_asignacion='$idAsignacion'
            ");

            $actualizar->execute();

            $error = $actualizar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{

                $actualizar2 = $this->_db->prepare("
                         UPDATE
                            rh_c091_utiles_beneficio
                         SET
                            ind_estado='PR',
                            fec_ultima_modificacion=NOW(),
                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                         WHERE pk_num_utiles_beneficio='$idBeneficio'
                        "
                    );
                $actualizar2->execute();
                $error2 = $actualizar2->errorInfo();

                if(!empty($error2[1]) && !empty($error2[2])){
                    $this->_db->rollBack();
                    return $error2;
                }else{
                    /*ejecuto procedimiento alamcenado para guardar las operaciones*/
                    $con1 =  $this->_db->prepare("
                        CALL w_rh_operacionesAsignacionUtiles(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
                ");
                    $con1->execute(array(
                        ':estatus' => $nuevoEstatus,
                        ':id' => $idAsignacion,
                        ':empleado' => $resultado['fk_rhb001_num_empleado'],
                        ':flag' => 1,
                        ':fecha' => $fecha,
                        ':observaciones' => strtoupper('ANULADO'),
                        ':usuario' => $this->atIdUsuario
                    ));
                    $this->_db->commit();
                    return $idAsignacion;
                }

            }

        }
        elseif(strcmp($resultado['ind_estado'],'RV')==0){

            $nuevoEstatus = 'PR';

            $actualizar =  $this->_db->prepare("
                    UPDATE
                        rh_c093_utiles_asignacion
                    SET
                        ind_estado='$nuevoEstatus',
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    WHERE
                        pk_num_utiles_asignacion='$idAsignacion'
            ");

            $actualizar->execute();

            $error = $actualizar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{

                /*ejecuto procedimiento alamcenado para guardar las operaciones*/
                $con1 =  $this->_db->prepare("
                        CALL w_rh_operacionesAsignacionUtiles(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
                ");
                $con1->execute(array(
                    ':estatus' => $nuevoEstatus,
                    ':id' => $idAsignacion,
                    ':empleado' => $resultado['fk_rhb001_num_empleado'],
                    ':flag' => 1,
                    ':fecha' => $fecha,
                    ':observaciones' => strtoupper('(ANULADO) REVERSO DE '.$resultado['ind_estado'].' A '.$nuevoEstatus),
                    ':usuario' => $this->atIdUsuario
                ));
                $this->_db->commit();
                return $idAsignacion;
            }


        }
        elseif(strcmp($resultado['ind_estado'],'AP')==0){
            $nuevoEstatus = 'RV';

            $actualizar =  $this->_db->prepare("
                    UPDATE
                        rh_c093_utiles_asignacion
                    SET
                        ind_estado='$nuevoEstatus',
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    WHERE
                        pk_num_utiles_asignacion='$idAsignacion'
            ");

            $actualizar->execute();

            $error = $actualizar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{

                /*ejecuto procedimiento alamcenado para guardar las operaciones*/
                $con1 =  $this->_db->prepare("
                        CALL w_rh_operacionesAsignacionUtiles(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
                ");
                $con1->execute(array(
                    ':estatus' => $nuevoEstatus,
                    ':id' => $idAsignacion,
                    ':empleado' => $resultado['fk_rhb001_num_empleado'],
                    ':flag' => 1,
                    ':fecha' => $fecha,
                    ':observaciones' => strtoupper('(ANULADO) REVERSO DE '.$resultado['ind_estado'].' A '.$nuevoEstatus),
                    ':usuario' => $this->atIdUsuario
                ));
                $this->_db->commit();
                return $idAsignacion;
            }

        }

    }



}//fin clase
