<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-02-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/


class pensionesSobrevivienteModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE OBTENER LISTADO DE LAS PENSIONES SOBREVIVIENTE
    public function metListarPensionesSobreviviente($estatus)
    {

        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    vl_rh_pension_sobreviviente
                WHERE txt_estatus='$estatus' and num_flag_estatus=1
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    // Método que permite  listar a los empleados de acuerdo al buscador de trabajador al momento de registrar una nueva pension
    public function metBuscarTrabajador($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $tipoTrabajador, $estado_registro, $situacionTabajo, $fechaInicio, $fecha_fin)
    {
        if ($pkNumOrganismo!='') {
            $FILTRO = "c.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $FILTRO .= " and c.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if ($centroCosto!='') {
            $FILTRO .= " and c.fk_a023_num_centro_costo=$centroCosto";
        }
        if ($tipoNomina!='') {
            $FILTRO .= " and c.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if ($tipoTrabajador!='') {
            $FILTRO .= " and c.fk_a006_num_miscelaneo_detalle_tipotrabajador=$tipoTrabajador";
        }
        if ($estado_registro!='') {
            $FILTRO .= " and b.num_estatus=$estado_registro";
        }
        if ($situacionTabajo!='') {
            $FILTRO .= " and a.num_estatus=$situacionTabajo";
        }
        if ($fechaInicio!='' && $fecha_fin!='') {
            $f1=explode("-",$fechaInicio);
            $f2=explode("-",$fecha_fin);
            $fi = $f1[2]."-".$f1[1]."-".$f1[0];
            $ff = $f2[2]."-".$f2[1]."-".$f2[0];

            $FILTRO .= " and f.fec_ingreso >= '$fi' and f.fec_ingreso <= '$ff'";
        }

        $buscarTrabajador = $this->_db->query(
            "SELECT
                a.pk_num_empleado,
                b.pk_num_persona,
                b.ind_cedula_documento,
                b.ind_nombre1,
                b.ind_nombre2,
                b.ind_apellido1,
                b.ind_apellido2,
                c.fk_a001_num_organismo,
                d.ind_descripcion_empresa,
                c.fk_a004_num_dependencia,
                e.ind_dependencia,
                c.fk_a023_num_centro_costo,
                c.fk_nmb001_num_tipo_nomina,
                c.fk_a006_num_miscelaneo_detalle_tipotrabajador,
                b.num_estatus AS estado_registro,
                a.num_estatus AS situacion_trabajo,
                f.fec_ingreso
            FROM
                rh_b001_empleado AS a
            INNER JOIN a003_persona AS b ON a.fk_a003_num_persona = b.pk_num_persona
            INNER JOIN rh_c076_empleado_organizacion AS c ON c.fk_rhb001_num_empleado = a.pk_num_empleado
            INNER JOIN a001_organismo AS d ON c.fk_a001_num_organismo = d.pk_num_organismo
            INNER JOIN a004_dependencia AS e ON c.fk_a004_num_dependencia = e.pk_num_dependencia
            INNER JOIN rh_c005_empleado_laboral AS f ON f.fk_rhb001_num_empleado = a.pk_num_empleado
            WHERE $FILTRO
            "
        );

        $buscarTrabajador->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarTrabajador->fetchAll();
    }


    // Método que permite obtener el antecedente de servicio de un empleado
    public function metConsultarAntecedente($pkNumEmpleado,$id_tipo_entidad)
    {
        $buscarAntecedente = $this->_db->query(
            "select fec_ingreso, date_format(fec_ingreso, '%d/%m/%Y') as fecha_ingreso, fec_egreso, date_format(fec_egreso, '%d/%m/%Y') as fecha_egreso,ind_empresa from rh_c011_experiencia  where fk_rhb001_num_empleado=$pkNumEmpleado and fk_a006_num_miscelaneo_detalle_tipo_empresa=$id_tipo_entidad"
        );
        $buscarAntecedente->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarAntecedente->fetchAll();
    }

    // Método que permite listar los centros de costos
    public function metListarCentroCosto()
    {
        $centroCosto =  $this->_db->query(
            "select pk_num_centro_costo, ind_descripcion_centro_costo from a023_centro_costo"
        );
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }


    public function metMostrarPensionesSobreviviente($idPensionSobreviviente)
    {
        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    vl_rh_pension_sobreviviente
                WHERE pk_num_proceso_pension = '$idPensionSobreviviente' and num_flag_estatus=1

             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    public function metListarPensionesSobrevivienteFiltro($organismo,$dependencia,$estatus)
    {


        $filtro = "txt_estatus='$estatus' AND
                   num_flag_estatus=1 AND
                   fk_a001_num_organismo='$organismo' AND
                   fk_a004_num_dependencia='$dependencia'
        ";

        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    vl_rh_pension_sobreviviente
                WHERE $filtro

             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metMostrarPensionesSobrevivienteOperaciones($idPensionSobreviviente,$estatus)
    {
        $con = $this->_db->query("
            SELECT
            rh_c086_operaciones_pension.pk_num_operaciones_pension,
            rh_b003_pension.pk_num_proceso_pension,
            rh_c086_operaciones_pension.txt_estatus,
            rh_c086_operaciones_pension.num_flag_estatus,
            rh_c086_operaciones_pension.fec_operacion,
            rh_c086_operaciones_pension.txt_observaciones,
            rh_c086_operaciones_pension.fk_a018_num_seguridad_usuario,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) as nombre_registro
            FROM
            rh_b003_pension
            INNER JOIN rh_c035_sobreviviente ON rh_b003_pension.pk_num_proceso_pension = rh_c035_sobreviviente.fk_rhb003_num_proceso_pension
            INNER JOIN rh_b001_empleado ON rh_b003_pension.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            INNER JOIN rh_c086_operaciones_pension ON rh_b003_pension.pk_num_proceso_pension = rh_c086_operaciones_pension.fk_rhb003_num_pension
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = rh_c086_operaciones_pension.fk_a018_num_seguridad_usuario
            WHERE rh_b003_pension.pk_num_proceso_pension='$idPensionSobreviviente' AND rh_c086_operaciones_pension.txt_estatus='$estatus'
             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #permite obtener los datos del empleado
    public function metObtenerDatosEmpleado($idEmpleado)
    {

        $con =  $this->_db->query("

                SELECT
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                rh_c076_empleado_organizacion.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa AS organismo,
                a003_persona.fk_a006_num_miscelaneo_detalle_sexo,
                sexo.ind_nombre_detalle AS sexo,
                a003_persona.fec_nacimiento,
                rh_c076_empleado_organizacion.fk_a004_num_dependencia,
                a004_dependencia.ind_dependencia AS dependencia,
                rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo,
                rh_c063_puestos.ind_descripcion_cargo AS cargo,
                rh_c063_puestos.num_sueldo_basico,
                rh_c005_empleado_laboral.fec_ingreso,
                rh_b001_empleado.num_estatus AS estatus_empleado,
                a003_persona.num_estatus AS estatus_persona,
                rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador,
                tipo_trabajador.ind_nombre_detalle AS tipo_trabajador
                FROM
                rh_b001_empleado
                INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN a006_miscelaneo_detalle AS sexo ON a003_persona.fk_a006_num_miscelaneo_detalle_sexo = sexo.pk_num_miscelaneo_detalle
                INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                WHERE rh_b001_empleado.pk_num_empleado='$idEmpleado'

        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }


    #PERMITE REGISTRAR LA SOLICITUD DE PENSION SOBREVIVIENTE
    public function metRegistrarPensionSobreviviente($datos)
    {
        $this->_db->beginTransaction();

        /*datos para tabla rh_b003_pension*/
        $empleado = $datos[0];#id_empleado
        $id_organismo  = $datos[1];#id_organismo
        $organismo  = $datos[2];#organismo
        $id_dependencia = $datos[3];#id_dependencia
        $dependencia = $datos[4];#dependencia
        $id_cargo = $datos[5];#id_cargo
        $cargo    = $datos[6];#cargo
        $ultimo_sueldo  = $datos[7];#sueldo actual
        $anio_serv  = $datos[8];#años de servicio
        $edad    = $datos[9];#fecha
        $fecha  = explode("-",$datos[10]);#fecha
        $fecha_ing = explode("-",$datos[11]);#fecha_ingreso
        $id_tipo_pension = $datos[12];#id_tipo_pension
        $tipo_pension = $datos[13];#tipo_pension
        $id_motivo_pension = $datos[14];#id_motivo_pension
        $motivo_pension = $datos[15];#motivo_pension
        /*datos para tabla rh_b003_pension*/

        /*datos para tabla rh_c035_sobreviviente*/
        $num_monto_pension  = $datos[16];#monto_pension
        $num_monto_jubilacion = $datos[17];#monto_jubilacion
        $num_coeficiente = $datos[18] / 100; //porcentaje /100;
        $num_total_sueldo = $datos[19];
        $num_anios_servicio_exceso = $datos[20];
        $periodo = $datos[21];#periodo
        $tipo_nom = $datos[22];
        $tipo_trab= $datos[23];
        $sit_trab  = $datos[24];#situacion de trabajo
        $mot_cese  = $datos[25];#motivo cese

        $fecha_egr = explode("-",$datos[26]);#fecha de egreso

        if(empty($datos[26])){
            $fecha_egr='0000-00-00';
        }else{
            $fecha_egr = explode("-",$datos[26]);#fecha de egreso
        }
        $obs_egre  = strtoupper($datos[27]);#observacion egreso
        $resolucion  = strtoupper($datos[28]);#resolucion
        $observacion_preparado = strtoupper($datos[29]);#observacion preparado
        $sueldo_base = $datos[34]/24;
        /*datos para tabla rh_c035_sobreviviente*/


        /*valido monto de jubilacion*/
        $PORCLIMITJUB =  Session::metObtener('PORCLIMITJUB');//PORCENTAJE LIMITE CON RESPECTO AL SUELDO BASE DEL CALCULO DE LA JUBILACION
        if($num_monto_jubilacion > ($sueldo_base * $PORCLIMITJUB / 100)){
            return array('El Monto de la Jubilación no puede ser Mayor al '.$PORCLIMITJUB.' % del Sueldo Base');
        }


        #inserto nuevo registro
        $registro = $this->_db->prepare(
            "INSERT INTO
                rh_b003_pension
             SET
                fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                fk_a001_num_organismo=:fk_a001_num_organismo,
                ind_organismo=:ind_organismo,
                fk_a004_num_dependencia=:fk_a004_num_dependencia,
                ind_dependencia=:ind_dependencia,
                fk_rhc063_num_puestos=:fk_rhc063_num_puestos,
                ind_cargo=:ind_cargo,
                num_ultimo_sueldo=:num_ultimo_sueldo,
                num_anio_servicio=:num_anio_servicio,
                num_edad=:num_edad,
                fec_fecha=:fec_fecha,
                fec_fecha_ingreso=:fec_fecha_ingreso,
                fk_a006_num_miscelaneo_detalle_tipopension=:fk_a006_num_miscelaneo_detalle_tipopension,
                ind_detalle_tipopension=:ind_detalle_tipopension,
                fk_a006_num_miscelaneo_detalle_motivopension=:fk_a006_num_miscelaneo_detalle_motivopension,
                ind_detalle_motivopension=:ind_detalle_motivopension,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            "
        );

        $registro->execute(array(
            'fk_rhb001_num_empleado' => $empleado,
            'fk_a001_num_organismo' => $id_organismo,
            'ind_organismo' => $organismo,
            'fk_a004_num_dependencia' => $id_dependencia,
            'ind_dependencia' => $dependencia,
            'fk_rhc063_num_puestos' => $id_cargo,
            'ind_cargo' => $cargo,
            'num_ultimo_sueldo' => $ultimo_sueldo,
            'num_anio_servicio' => $anio_serv,
            'num_edad' => $edad,
            'fec_fecha' => $fecha[2]."-".$fecha[1]."-".$fecha[0],
            'fec_fecha_ingreso' => $fecha_ing[2]."-".$fecha_ing[1]."-".$fecha_ing[0],
            'fk_a006_num_miscelaneo_detalle_tipopension' => $id_tipo_pension,
            'ind_detalle_tipopension' => $tipo_pension,
            'fk_a006_num_miscelaneo_detalle_motivopension' => $id_motivo_pension,
            'ind_detalle_motivopension' => $motivo_pension
        ));

        $id_pension = $this->_db->lastInsertId();

        $error = $registro->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{

            /*registro en la tbala rh_c035_invalidez*/
            $registro2 = $this->_db->prepare(
                "INSERT INTO
                rh_c035_sobreviviente
             SET
                fk_rhb003_num_proceso_pension='$id_pension',
                num_monto_pension=:num_monto_pension,
                num_monto_jubilacion=:num_monto_jubilacion,
                num_coeficiente=:num_coeficiente,
                num_total_sueldo=:num_total_sueldo,
                num_anios_servicio_exceso=:num_anios_servicio_exceso,
                ind_periodo=:ind_periodo,
                fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                fk_a006_num_miscelaneo_detalle_tipotrab=:fk_a006_num_miscelaneo_detalle_tipotrab,
                num_situacion_trabajo=:num_situacion_trabajo,
                fk_rhc032_num_motivo_cese=:fk_rhc032_num_motivo_cese,
                fec_fecha_egreso=:fec_fecha_egreso,
                txt_observacion_egreso=:txt_observacion_egreso,
                ind_resolucion=:ind_resolucion,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            "
            );
            $registro2->execute(array(
                'num_monto_pension' => $num_monto_pension,
                'num_monto_jubilacion' => $num_monto_jubilacion,
                'num_coeficiente' => $num_coeficiente,
                'num_total_sueldo' => $num_total_sueldo,
                'num_anios_servicio_exceso' => $num_anios_servicio_exceso,
                'ind_periodo' => $periodo,
                'fk_nmb001_num_tipo_nomina' => $tipo_nom,
                'fk_a006_num_miscelaneo_detalle_tipotrab' => $tipo_trab,
                'num_situacion_trabajo' => $sit_trab,
                'fk_rhc032_num_motivo_cese' => $mot_cese,
                'fec_fecha_egreso' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0],
                'txt_observacion_egreso' => $obs_egre,
                'ind_resolucion' => $resolucion
            ));
            $error2 = $registro2->errorInfo();

            if(!empty($error2[1]) && !empty($error2[2])){
                $this->_db->rollBack();
                return $error2;
            }else{

                /*GUARDO ANTECEDENTES*/

                /*$cod_detalle identifica al miscelaneo tipo entidad, G quiere decir gubernamental*/
                /*los antecedentes de servicios son todas aquellas experiencias laborales tipo gubernamentales*/
                $cod_detalle = 'G';
                /*consulto el miscelaneo tipo de entidad*/
                $miscelaneoTipoEntidad = $this->_db->query(
                    "SELECT
                      a006.pk_num_miscelaneo_detalle,
                      a006.ind_nombre_detalle,
                      a006.cod_detalle,
                      a006.num_estatus
                    FROM
                      a006_miscelaneo_detalle AS a006
                    INNER JOIN
                      a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
                    WHERE a005.cod_maestro='TIPOENTE'
                    "
                );
                /*recorro el arreglo para obtener el id del miscelaneo detalle*/
                foreach($miscelaneoTipoEntidad as $d){
                    if(strcmp($d['cod_detalle'],$cod_detalle)==0){
                        $id_tipo_entidad = $d['pk_num_miscelaneo_detalle'];
                    }
                }

                $antecedentes = $this->metConsultarAntecedente($empleado,$id_tipo_entidad);
                //recorro el arreglo y guardo en la tabla rh_c087_empelado_antecedentes
                foreach($antecedentes as $ant){

                    $fechaInicial = new DateTime($ant['fec_ingreso']);
                    $fechaFinal   = new DateTime($ant['fec_egreso']);
                    $f            = $fechaInicial->diff($fechaFinal);

                    $registro_antecedente = $this->_db->prepare(
                        "INSERT INTO
                              rh_c087_empleado_antecedentes
                         SET
                              fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                              fk_rhb003_num_proceso_pension=:fk_rhb003_num_proceso_pension,
                              ind_organismo=:ind_organismo,
                              fec_fecha_ingreso=:fec_fecha_ingreso,
                              fec_fecha_egreso=:fec_fecha_egreso,
                              ind_proceso=:ind_proceso,
                              num_anio=:num_anio,
                              num_meses=:num_meses,
                              num_dias=:num_dias,
                              fec_ultima_modificacion=NOW(),
                              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                         "
                    );
                    $registro_antecedente->execute(array(
                           'fk_rhb001_num_empleado' => $empleado,
                            'fk_rhb003_num_proceso_pension' => $id_pension,
                            'ind_organismo' => $ant['ind_empresa'],
                            'fec_fecha_ingreso' => $ant['fec_ingreso'],
                            'fec_fecha_egreso' => $ant['fec_egreso'],
                            'ind_proceso' => 'P',
                            'num_anio'  => $f->format('%y'),
                            'num_meses' => $f->format('%m'),
                            'num_dias'  => $f->format('%d')
                        )
                    );


                }
                if($antecedentes){
                    $error3 = $registro_antecedente->errorInfo();
                }else{
                    $error3 =array(1=>null,2=>null);
                }

                if(!empty($error3[1]) && !empty($error3[2])){
                    $this->_db->rollBack();
                    return $error3;
                }else{

                    /*GUARDO RELACION DE SUELDOS*/
                    $cod_detalle = 'I';
                    /*consulto miscelaneo tipo de concepto*/
                    $miscelaneoTipoConcepto = $this->_db->query(
                            "SELECT
                              a006.pk_num_miscelaneo_detalle,
                              a006.ind_nombre_detalle,
                              a006.cod_detalle,
                              a006.num_estatus
                            FROM
                              a006_miscelaneo_detalle AS a006
                            INNER JOIN
                              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
                            WHERE a005.cod_maestro='TDCNM'
                        "
                        );
                    /*recorro el arreglo para obtener el id del miscelaneo detalle tipo concepto*/
                    foreach($miscelaneoTipoConcepto as $d){
                        if(strcmp($d['cod_detalle'],$cod_detalle)==0){
                            $id_tipo_concepto = $d['pk_num_miscelaneo_detalle'];
                        }
                    }
                    //consulto los conceptos disponibles de nomina para el calculo de las jubilaciones y pensiones
                    $conceptos_aplicables = $this->metConsultarConceptosJP($id_tipo_concepto);

                    $CODSUELDOBAS =  Session::metObtener('CODSUELDOBAS');//CODIGO DEL CONCEPTO DE SUELDO BASICO
                    $CODPRIANIOSERV =  Session::metObtener('CODPRIANIOSERV');//CODIGO DEL CONCEPTO PRIMAS POR AÑOS DE SERVICIO
                    foreach($conceptos_aplicables as $con) {


                        if($con['pk_num_concepto']==$CODSUELDOBAS){

                            $relacion = $this->metConsultarRelacionSueldos($empleado,$con['pk_num_concepto']);

                            foreach($relacion as $r_sueldo){

                                $aux  = explode("-",$r_sueldo['periodo']);
                                $anio = $aux[0];
                                $mes  = $aux[1];

                                $i_relacion_s = $this->_db->prepare(
                                    "INSERT INTO
                                        rh_c084_relacion_sueldo_jubilacion
                                     SET
                                        fk_b003_num_proceso_pension=:fk_b003_num_proceso_pension,
                                        num_anio_periodo=:num_anio_periodo,
                                        ind_mes_periodo=:ind_mes_periodo,
                                        num_monto=:num_monto,
                                        fk_nmb002_num_concepto=:fk_nmb002_num_concepto
                                    "
                                );
                                $i_relacion_s->execute(array(
                                   'fk_b003_num_proceso_pension' => $id_pension,
                                   'num_anio_periodo' => $anio,
                                   'ind_mes_periodo' => $mes,
                                   'num_monto' => $r_sueldo['monto'],
                                   'fk_nmb002_num_concepto' => $r_sueldo['fk_nmb002_num_concepto']
                                ));

                            }
                            if($relacion){
                                $error4 = $i_relacion_s->errorInfo();
                            }else{
                                $error4 =array(1=>null,2=>null);
                            }



                        }elseif($con['pk_num_concepto']==$CODPRIANIOSERV){

                            $relacion = $this->metConsultarRelacionSueldos($empleado,$con['pk_num_concepto']);

                            foreach($relacion as $r_prima){

                                $aux  = explode("-",$r_prima['periodo']);
                                $anio = $aux[0];
                                $mes  = $aux[1];

                                $i_relacion_p = $this->_db->prepare(
                                    "INSERT INTO
                                        rh_c084_relacion_sueldo_jubilacion
                                     SET
                                        fk_b003_num_proceso_pension=:fk_b003_num_proceso_pension,
                                        num_anio_periodo=:num_anio_periodo,
                                        ind_mes_periodo=:ind_mes_periodo,
                                        num_monto=:num_monto,
                                        fk_nmb002_num_concepto=:fk_nmb002_num_concepto
                                    "
                                );
                                $i_relacion_p->execute(array(
                                    'fk_b003_num_proceso_pension' => $id_pension,
                                    'num_anio_periodo' => $anio,
                                    'ind_mes_periodo' => $mes,
                                    'num_monto' => $r_prima['monto'],
                                    'fk_nmb002_num_concepto' => $r_prima['fk_nmb002_num_concepto']
                                ));

                            }
                            if($relacion){
                                $error5 = $i_relacion_p->errorInfo();
                            }else{
                                $error5 =array(1=>null,2=>null);
                            }

                        }

                    }

                    if(!empty($error4[1]) && !empty($error4[2]) && !empty($error5[1]) && !empty($error5[2])){
                        $this->_db->rollBack();
                        return $error4;
                    }else{

                        //GUARDO BENEFICIARIOS
                        $con = $this->_db->query("SELECT * FROM vl_rh_empleado_carga_familiar WHERE pk_num_empleado='$empleado'");
                        $con->setFetchMode(PDO::FETCH_ASSOC);
                        $carga_familiar = $con->fetchAll();

                        $flag_cumple = false;

                        foreach($carga_familiar as $data){

                            $f_n  = explode("-",$data['fec_nacimiento'],3);
                            $dias = mktime(0,0,0,$f_n[1],$f_n[0],$f_n[2]);
                            $edadBeneficiario = (int)((time()-$dias)/31556926);

                            //verifico los requisitos para ver si cumple para ser beneficiario de la pension
                            if($data['detalle_parentesco']=="CB"){
                                $flag_cumple=true;
                            }
                            if($data['detalle_parentesco']=="ES" && $data['detalle_sexo']=="M" && $edadBeneficiario>=60 ){
                                $flag_cumple=true;
                            }
                            if($data['detalle_parentesco']=="ES" && $data['detalle_sexo']=="F"){
                                $flag_cumple=true;
                            }
                            if($data['detalle_parentesco']=="HI" && $edadBeneficiario<=14){
                                $flag_cumple=true;
                            }
                            if($data['detalle_parentesco']=="HI" && $data['num_falg_estudia']==1 && $edadBeneficiario<=18){
                                $flag_cumple=true;
                            }
                            if($data['num_flag_discapacidad']==1){
                                $flag_cumple=true;
                            }

                            if($flag_cumple){

                                $registro_beneficarios = $this->_db->prepare(
                                    "INSERT INTO
                                        rh_c088_beneficiarios_pension
                                     SET
                                        fk_rhb003_num_proceso_pension='$id_pension',
                                        fk_a003_num_persona=:fk_a003_num_persona,
                                        fk_a006_num_miscelaneo_detalle_parentesco=:fk_a006_num_miscelaneo_detalle_parentesco,
                                        ind_parentesco=:ind_parentesco,
                                        fec_nacimiento=:fec_nacimiento,
                                        fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
                                        ind_sexo=:ind_sexo,
                                        num_flag_discapacidad=:num_flag_discapacidad,
                                        num_flag_estudia=:num_flag_estudia,
                                        fec_ultima_modificacion=NOW(),
                                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                                    "
                                );
                                $registro_beneficarios->execute(array(
                                        'fk_a003_num_persona' =>  $data['pk_num_persona'],
                                        'fk_a006_num_miscelaneo_detalle_parentesco' => $data['fk_a006_num_miscelaneo_detalle_parentezco'],
                                        'ind_parentesco' => $data['parentesco'],
                                        'fec_nacimiento' => $data['fec_nacimiento'],
                                        'fk_a006_num_miscelaneo_detalle_sexo' => $data['fk_a006_num_miscelaneo_detalle_sexo'],
                                        'ind_sexo' => $data['sexo'],
                                        'num_flag_discapacidad' => $data['num_flag_discapacidad'],
                                        'num_flag_estudia' => $data['num_flag_estudia']
                                ));

                            }

                        }

                        if($carga_familiar){
                            $error6 = $registro_beneficarios->errorInfo();
                        }else{
                            $error6 =array(1=>null,2=>null);
                        }

                        if(!empty($error6[1]) && !empty($error6[2])){
                            $this->_db->rollBack();
                            return $error6;
                        }else{

                            //finalizo con el estatus
                            //*******************************************************************************************************
                            $ESTATUS = 'PR';//preparado
                            $FLAG = 1;

                            //ejecuto procedimiento alamcenado para guardar las operaciones
                            $con =  $this->_db->prepare("
                                CALL w_rh_operacionesProcesoPension(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
                            ");
                            $con->execute(array(
                                ':estatus' => $ESTATUS,
                                ':id' => $id_pension,
                                ':empleado' => $empleado,
                                ':flag' => $FLAG,
                                ':fecha' => $fecha[2]."-".$fecha[1]."-".$fecha[0],
                                ':observaciones' => $observacion_preparado,
                                ':usuario' => $this->atIdUsuario
                            ));

                            $this->_db->commit();
                            return $id_pension;
                            //********************************************************************************************************

                        }

                    }

                }

            }

        }

    }


    #PERMITE MODIFICAR PENSIONES SOBREVIVIENTE
    public function metModificarPensionesSobreviviente($idPensionSobreviviente,$datos)
    {
        $this->_db->beginTransaction();

        /*organizo los datos enviados*/
        /*solo se ṕuede modifcar si esta en preparación*/
        $fk_nmb001_num_tipo_nomina = $datos[0];
        $fk_a006_num_miscelaneo_detalle_tipotrab = $datos[1];
        $fk_rhc032_num_motivo_cese = $datos[2];
        $aux = explode("-",$datos[3]);
        $txt_observacion_egreso = $datos[4];
        $num_situacion_trabajo = $datos[5];
        $ind_resolucion = $datos[6];
        $observacion_preparado = strtoupper($datos[7]);#observacion preparado
        $num_monto_pension  = $datos[8];#monto_pension


        $actualizar = $this->_db->prepare(
            "UPDATE
                    rh_c035_sobreviviente
                 SET
                    num_monto_pension=:num_monto_pension,
                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                    fk_a006_num_miscelaneo_detalle_tipotrab=:fk_a006_num_miscelaneo_detalle_tipotrab,
                    num_situacion_trabajo=:num_situacion_trabajo,
                    fk_rhc032_num_motivo_cese=:fk_rhc032_num_motivo_cese,
                    fec_fecha_egreso=:fec_fecha_egreso,
                    txt_observacion_egreso=:txt_observacion_egreso,
                    ind_resolucion=:ind_resolucion,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
                  WHERE fk_rhb003_num_proceso_pension = '$idPensionSobreviviente'
                "
        );
        $actualizar->execute(array(
            ':fk_nmb001_num_tipo_nomina' => $fk_nmb001_num_tipo_nomina,
            ':fk_a006_num_miscelaneo_detalle_tipotrab' => $fk_a006_num_miscelaneo_detalle_tipotrab,
            ':num_situacion_trabajo' => $num_situacion_trabajo,
            ':fk_rhc032_num_motivo_cese' => $fk_rhc032_num_motivo_cese,
            ':fec_fecha_egreso' => $aux[2]."-".$aux[1]."-".$aux[0],
            ':txt_observacion_egreso' => $txt_observacion_egreso,
            ':ind_resolucion' => $ind_resolucion,
            ':num_monto_pension' => $num_monto_pension
        ));
        $error = $actualizar->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $con = $this->_db->prepare(
                "UPDATE
                        rh_c086_operaciones_pension
                     SET
                        txt_observaciones='$observacion_preparado'
                     WHERE fk_rhb003_num_pension='$idPensionSobreviviente' and txt_estatus='PR'
                     "
            );
            $con->execute();
            $error1 = $con->errorInfo();
            if(!empty($error1[1]) && !empty($error1[2])){
                $this->_db->rollBack();
                return $error1;
            }else{
                $this->_db->commit();
                return $idPensionSobreviviente;
            }
        }
    }//fin modificar


    public function metAnularPensionesSobreviviente($idPensionSobreviviente,$observaciones)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
               select * from rh_c086_operaciones_pension
               where fk_rhb003_num_pension='$idPensionSobreviviente'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $fecha = date("Y-m-d");

        if(strcmp($resultado['txt_estatus'],'PR')==0){
            $nuevoEstatus = 'AN';
        }
        elseif(strcmp($resultado['txt_estatus'],'CO')==0){
            $nuevoEstatus = 'PR';
        }
        elseif(strcmp($resultado['txt_estatus'],'AP')==0){
            $nuevoEstatus = 'CO';
        }


        /*ejecuto procedimiento alamcenado para guardar las operaciones*/
        $con =  $this->_db->prepare("
                    CALL w_rh_operacionesProcesoPension(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
        $con->execute(array(
            ':estatus' => $nuevoEstatus,
            ':id' => $idPensionSobreviviente,
            ':empleado' => $resultado['fk_rhb0001_num_empleado'],
            ':flag' => 1,
            ':fecha' => $fecha,
            ':observaciones' => strtoupper($observaciones),
            ':usuario' => $this->atIdUsuario
        ));
        $this->_db->commit();
        return $idPensionSobreviviente;

    }


    public function metConformarPensionesSobreviviente($idPensionSobreviviente,$observaciones)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
               select * from rh_c086_operaciones_pension
               where fk_rhb003_num_pension='$idPensionSobreviviente'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $fecha = date("Y-m-d");

        /*ejecuto procedimiento alamcenado para guardar las operaciones*/
        $con =  $this->_db->prepare("
                    CALL w_rh_operacionesProcesoPension(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
        $con->execute(array(
            ':estatus' => 'CO',
            ':id' => $idPensionSobreviviente,
            ':empleado' => $resultado['fk_rhb0001_num_empleado'],
            ':flag' => 1,
            ':fecha' => $fecha,
            ':observaciones' => strtoupper($observaciones),
            ':usuario' => $this->atIdUsuario
        ));
        $this->_db->commit();
        return $idPensionSobreviviente;


    }


    public function metAprobarPensionesSobreviviente($idPensionSobreviviente,$observaciones,$datos)
    {

        $this->_db->beginTransaction();

        /*organizo los datos enviados*/
        $empleado  = $datos[0];#id_empleado,
        $tipo_nom  = $datos[1];#tipo_nomina
        $tipo_trab = $datos[2];#tipo_trabajador
        $sit_trab  = $datos[3];#situacion de trabajo
        $mot_cese  = $datos[4];#motivo cese
        $fecha_egr = explode("-",$datos[5]);#fecha de egreso
        $obs_egre  = strtoupper($datos[6]);#observacion egreso
        $resolucion  = strtoupper($datos[7]);#resolucion
        $ultimo_sueldo  = $datos[8];#ultimo_sueldo
        $organismo = $datos[9];
        $dependencia = $datos[10];
        $cargo = $datos[11];



        /*****************************ACTUALIZO ESTATUS APAROBADO***************************************************/
        $fecha = date("Y-m-d");

        $con = $this->_db->query("
               select * from rh_c086_operaciones_pension
               where 	fk_rhb003_num_pension='$idPensionSobreviviente'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $empleado = $resultado['fk_rhb0001_num_empleado'];
        /*****************************ACTUALIZO ESTATUS APAROBADO***************************************************/


       /*actualizo el empleado*/
        $c1 = $this->_db->prepare("
        UPDATE
            rh_b001_empleado
        SET
            num_estatus=0,
            fec_ultima_modificacion=NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
        WHERE pk_num_empleado='$empleado'
        ");
        $c1->execute();
        $c1Error = $c1->errorInfo();
        if(!empty($c1Error[1]) && !empty($c1Error[2])){
            $this->_db->rollBack();
            return $c1Error."-empleado";
        }else{

            /*----------------------------------------------------------------------------*/
            $c2 = $this->_db->prepare("
            UPDATE rh_c076_empleado_organizacion
            SET
              fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
              fk_a006_num_miscelaneo_detalle_tipotrabajador=:fk_a006_num_miscelaneo_detalle_tipotrabajador,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE fk_rhb001_num_empleado='$empleado'
            ");
            $c2->execute(array(
                ':fk_nmb001_num_tipo_nomina' => $tipo_nom,
                ':fk_a006_num_miscelaneo_detalle_tipotrabajador' => $tipo_trab
            ));
            $c2Error = $c2->errorInfo();
            if(!empty($c2Error[1]) && !empty($c2Error[2])){
                $this->_db->rollBack();
                return $c2Error."-organizacion";
            }else{
                /*----------------------------------------------------------------------------*/
                $c3 = $this->_db->prepare("
                UPDATE rh_c005_empleado_laboral
                SET
                  fec_egreso=:fec_egreso,
                  ind_resolucion_egreso=:ind_resolucion_egreso,
                  num_sueldo_ant=:num_sueldo_ant,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                WHERE fk_rhb001_num_empleado='$empleado'
                ");
                $c3->execute(array(
                    ':fec_egreso' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0],
                    ':ind_resolucion_egreso' => $resolucion,
                    ':num_sueldo_ant' => $ultimo_sueldo,
                ));
                $c3Error = $c3->errorInfo();
                if(!empty($c3Error[1]) && !empty($c3Error[2])){
                    $this->_db->rollBack();
                    return $c3Error."laboral";
                }else{

                    /*actualizo e inserto nivelaciones*/
                    $con = $this->_db->query("SELECT ADDDATE('".$fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0]."', ".intval(-1).") AS FechaResultado");
                    $con->setFetchMode(PDO::FETCH_ASSOC);
                    $dato = $con->fetch();
                    $FechaHasta= $dato['FechaResultado'];

                    /*actualizo la ultima nivelacion*/
                    $c4 = $this->_db->prepare("
                    UPDATE rh_c059_empleado_nivelacion
                    SET
                      fec_fecha_hasta=:fec_fecha_hasta,
                      fec_ultima_modificacion=NOW(),
                      fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    WHERE fk_rhb001_num_empleado='$empleado' AND fec_fecha_hasta='0000-00-00'
                    ");
                    $c4->execute(array(
                        ':fec_fecha_hasta' => $FechaHasta
                    ));
                    $c4Error = $c4->errorInfo();
                    if(!empty($c4Error[1]) && !empty($c4Error[2])){
                        $this->_db->rollBack();
                        return $c4Error."-ultima-nivelacion";
                    }else{

                        /*insero nueva nivelacion*/
                        $cc = $this->_db->query("select fk_a023_num_centro_costo from rh_c076_empleado_organizacion where fk_rhb001_num_empleado='$empleado'");
                        $cc->setFetchMode(PDO::FETCH_ASSOC);
                        $centroCosto = $cc->fetch();

                        $c5 = $this->_db->prepare(
                            "INSERT INTO
                            rh_c059_empleado_nivelacion
                         SET
                            fk_rhb001_num_empleado='$empleado',
                            fec_fecha_registro=:fec_fecha_registro,
                            fk_a001_num_organismo=:fk_a001_num_organismo,
                            fk_a004_num_dependencia=:fk_a004_num_dependencia,
                            fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                            fk_rhc063_num_puestos=:fk_rhc063_num_puestos,
                            fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                            ind_documento=:ind_documento,
                            num_estatus=:num_estatus,
                            fec_fecha_hasta=:fec_fecha_hasta,
                            fec_ultima_modificacion=NOW(),
                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                          ");
                        #execute — Ejecuta una sentencia preparada
                        $c5->execute(array(
                            ':fec_fecha_registro' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0],
                            ':fk_a001_num_organismo'  => $organismo,
                            ':fk_a004_num_dependencia'  => $dependencia,
                            ':fk_a023_num_centro_costo'  => $centroCosto['fk_a023_num_centro_costo'],
                            ':fk_rhc063_num_puestos'  => $cargo,
                            ':fk_nmb001_num_tipo_nomina' => $tipo_nom,
                            ':ind_documento' => $resolucion,
                            ':num_estatus' => $sit_trab,
                            ':fec_fecha_hasta' => '0000-00-00'
                        ));
                        $c5Error = $c5->errorInfo();
                        $id_nivelacion = $this->_db->lastInsertId();
                        if(!empty($c5Error[1]) && !empty($c5Error[2])){
                            $this->_db->rollBack();
                            return $c5Error."-nivelacion";
                        }else{

                            /*inserto nivelacion historial*/
                            $c6 = $this->_db->prepare(
                                "INSERT INTO
                                rh_c060_empleado_nivelacion_historial
                                (fk_rhc059_num_empleado_nivelacion,
                                 fec_fecha_registro,
                                 ind_organismo,
                                 ind_dependencia,
                                 ind_cargo,
                                 num_nivel_salarial,
                                 ind_categoria_cargo,
                                 ind_tipo_nomina,
                                 ind_centro_costo,
                                 ind_tipo_accion,
                                 num_estatus,
                                 fec_ultima_modificacion,
                                 fk_a018_num_seguridad_usuario)
                            SELECT
                                rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion,
                                rh_c059_empleado_nivelacion.fec_fecha_registro,
                                organismo.ind_descripcion_empresa AS ind_organismo,
                                dependencia.ind_dependencia AS ind_dependencia,
                                cargo.ind_descripcion_cargo AS ind_cargo,
                                cargo.num_sueldo_basico AS num_nivel_salarial,
                                categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                tipo_accion.ind_nombre_detalle AS ind_tipo_accion,
                                rh_c059_empleado_nivelacion.num_estatus,
                                NOW() as fec_ultima_modificacion,
                                '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                            FROM
                                rh_c059_empleado_nivelacion
                            INNER JOIN a001_organismo AS organismo ON organismo.pk_num_organismo = rh_c059_empleado_nivelacion.fk_a001_num_organismo
                            INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = rh_c059_empleado_nivelacion.fk_a004_num_dependencia
                            INNER JOIN a023_centro_costo AS centro_costo ON centro_costo.pk_num_centro_costo = rh_c059_empleado_nivelacion.fk_a023_num_centro_costo
                            INNER JOIN rh_c063_puestos AS cargo ON cargo.pk_num_puestos = rh_c059_empleado_nivelacion.fk_rhc063_num_puestos
                            INNER JOIN nm_b001_tipo_nomina AS nomina ON nomina.pk_num_tipo_nomina = rh_c059_empleado_nivelacion.fk_nmb001_num_tipo_nomina
                            LEFT JOIN a006_miscelaneo_detalle AS tipo_accion ON tipo_accion.pk_num_miscelaneo_detalle = rh_c059_empleado_nivelacion.fk_a006_num_miscelaneo_detalle_tipoaccion
                            INNER JOIN a006_miscelaneo_detalle AS categoria ON cargo.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                            WHERE rh_c059_empleado_nivelacion.fk_rhb001_num_empleado='$empleado'
                            AND rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion='$id_nivelacion'
                              ");

                            $c6->execute();
                            $c6Error = $c6->errorInfo();
                            if(!empty($c6Error[1]) && !empty($c6Error[2])){
                                $this->_db->rollBack();
                                return $c6Error."-nivelacion-historial";
                            }else{

                                /*inserto historial empleado*/
                                $c7 = $this->_db->prepare(
                                    "INSERT INTO
                                    rh_c061_empleado_historial
                                    (fk_rhb001_num_empleado,
                                     ind_periodo,
                                     fec_ingreso,
                                     ind_organismo,
                                     ind_dependencia,
                                     ind_centro_costo,
                                     ind_cargo,
                                     num_nivel_salarial,
                                     ind_categoria_cargo,
                                     ind_tipo_nomina,
                                     ind_tipo_pago,
                                     num_estatus,
                                     ind_motivo_cese,
                                     fec_egreso,
                                     txt_obs_cese,
                                     ind_tipo_trabajador,
                                     fec_ultima_modificacion,
                                     fk_a018_num_seguridad_usuario)
                                SELECT
                                    empleado.pk_num_empleado,
                                    NOW() as ind_periodo,
                                    rh_c005_empleado_laboral.fec_ingreso,
                                    a001_organismo.ind_descripcion_empresa AS ind_organismo,
                                    a004_dependencia.ind_dependencia,
                                    a023_centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                    rh_c063_puestos.ind_descripcion_cargo AS ind_cargo,
                                    rh_c063_puestos.num_sueldo_basico AS ind_nivel_salarial,
                                    categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                    nm_b001_tipo_nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                    tipo_pago.ind_nombre_detalle AS ind_tipo_pago,
                                    empleado.num_estatus,
                                    motivo_cese.ind_nombre_cese AS ind_motivo_cese,
                                    pension_sobreviviente.fec_fecha_egreso AS fec_egreso,
                                    pension_sobreviviente.txt_observacion_egreso AS txt_obs_cese,
                                    tipo_trabajador.ind_nombre_detalle AS ind_tipo_trabajador,
                                    NOW() as fec_ultima_modificacion,
                                    '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                                FROM
                                    rh_b001_empleado AS empleado
                                    INNER JOIN rh_c076_empleado_organizacion ON empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                                    INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                    INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                                    INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                                    INNER JOIN a023_centro_costo ON rh_c076_empleado_organizacion.fk_a023_num_centro_costo = a023_centro_costo.pk_num_centro_costo
                                    INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                                    INNER JOIN a006_miscelaneo_detalle AS categoria ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                    INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                                    INNER JOIN a006_miscelaneo_detalle AS tipo_pago ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipopago = tipo_pago.pk_num_miscelaneo_detalle
                                    INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                                    INNER JOIN rh_b003_pension ON rh_b003_pension.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                    INNER JOIN rh_c035_sobreviviente AS pension_sobreviviente ON rh_b003_pension.pk_num_proceso_pension = pension_sobreviviente.fk_rhb003_num_proceso_pension
                                    INNER JOIN rh_c032_motivo_cese AS motivo_cese ON motivo_cese.pk_num_motivo_cese = pension_sobreviviente.fk_rhc032_num_motivo_cese
                                WHERE
                                    empleado.pk_num_empleado='$empleado'

                            ");
                                $c7->execute();
                                $c7Error = $c7->errorInfo();
                                if(!empty($c7Error[1]) && !empty($c7Error[2])){
                                    $this->_db->rollBack();
                                    return $c7Error."-historial";
                                }else{

                                    /*ejecuto procedimiento alamcenado para guardar las operaciones*/
                                    $con =  $this->_db->prepare("
                                                CALL w_rh_operacionesProcesoPension(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
                                    ");
                                    $con->execute(array(
                                        ':estatus' => 'AP',
                                        ':id' => $idPensionSobreviviente,
                                        ':empleado' => $empleado,
                                        ':flag' => 1,
                                        ':fecha' => $fecha,
                                        ':observaciones' => strtoupper($observaciones),
                                        ':usuario' => $this->atIdUsuario
                                    ));
                                    $errorCall = $con->errorInfo();
                                    if(!empty($errorCall[1]) && !empty($errorCall[2])){
                                        $this->_db->rollBack();
                                        return $errorCall;
                                    }else {
                                        $this->_db->commit();
                                        return $idPensionSobreviviente;
                                    }

                                }

                            }

                        }

                    }

                }

            }

        }

    }/*FIN METODO*/

    /*PERMITE CONSULTAR TODOS LOS CONCEPTOS DISPONIBLES PARA CALCULO DE JUBILACIONES Y PENSIONES*/
    public function metConsultarConceptosJP($tipo_concepto)
    {

        $conceptos = $this->_db->query("

            SELECT pk_num_concepto, ind_descripcion FROM nm_b002_concepto WHERE num_flag_jubilacion=1 AND fk_a006_num_tipo_concepto=$tipo_concepto

        ");
        $conceptos->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $conceptos->fetchAll();

        return $resultado;

    }

    /*PERMITE BUSCAR LA RELACION DE SUELDO YA CALCULADAS DE LA PENSION*/
    public function metConsultarRelacionSueldosPension($idPension,$concepto)
    {
        $relacion = $this->_db->query("
            SELECT
                fk_b003_num_proceso_pension,
                num_anio_periodo,
                ind_mes_periodo,
                num_monto,
                fk_nmb002_num_concepto
            FROM
                rh_c084_relacion_sueldo_jubilacion
            WHERE
                fk_b003_num_proceso_pension='$idPension' AND
                fk_nmb002_num_concepto='$concepto'

        ");
        $relacion->setFetchMode(PDO::FETCH_ASSOC);
        return $relacion->fetchAll();
    }

    /*PERMITE CONSULTAR LA RELACION DE SUEDOS Y PRIMAS DEL EMPLEADO*/
    public function metConsultarRelacionSueldos($idEmpleado,$concepto)
    {


        $conceptos = $this->_db->query("

            SELECT
            nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
            nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto,
            nm_b002_concepto.ind_descripcion,
            CONCAT(nm_c001_tipo_nomina_periodo.fec_anio,'-',nm_c001_tipo_nomina_periodo.fec_mes) AS periodo,
            SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS monto
            FROM
            nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
            INNER JOIN nm_b002_concepto ON nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
            INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo = nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo
            WHERE
                nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado='$idEmpleado' AND
                nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto='$concepto' AND
                CONCAT(nm_c001_tipo_nomina_periodo.fec_anio,'-',nm_c001_tipo_nomina_periodo.fec_mes) < SUBSTRING(NOW(),1,7)
            GROUP BY CONCAT(nm_c001_tipo_nomina_periodo.fec_anio,'-',nm_c001_tipo_nomina_periodo.fec_mes) DESC
            LIMIT 0,24

        ");
        $conceptos->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $conceptos->fetchAll();

        return $resultado;

    }


    /*PERMITE CONSULTAR LOS ANTECEDENTES DE SERVICIO DEL EMPLEADO*/
    public function metConsultarAntecedentesServicio($empleado,$id_pension)
    {

        $antecedentes = $this->_db->query("
            SELECT * FROM rh_c087_empleado_antecedentes WHERE fk_rhb001_num_empleado='$empleado' and fk_rhb003_num_proceso_pension='$id_pension'
        ");
        $antecedentes->setFetchMode(PDO::FETCH_ASSOC);
        return $antecedentes->fetchAll();

    }


    /*PERMITE CONSULTAR LOS BENEFICIARIOS DE LA PENSION SOBREVIVIENTE YA CREADA*/
    public function metConsultarBeneficiariosPension($idPensionSobreviviente)
    {
        $beneficiarios = $this->_db->query("
                SELECT
                    rh_c088_beneficiarios_pension.pk_num_beneficiario_pension,
                    rh_c088_beneficiarios_pension.fk_rhb003_num_proceso_pension,
                    a003_persona.pk_num_persona,
                    a003_persona.ind_cedula_documento,
                    CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) as nombre_completo,
                    rh_c088_beneficiarios_pension.fk_a006_num_miscelaneo_detalle_parentesco,
                    parentesco.ind_nombre_detalle AS parentesco,
                    a003_persona.fec_nacimiento,
                    date_format(a003_persona.fec_nacimiento, '%d-%m-%Y') AS fecha_nacimiento,
                    rh_c088_beneficiarios_pension.fk_a006_num_miscelaneo_detalle_sexo,
                    rh_c088_beneficiarios_pension.ind_sexo as sexo
                FROM
                    rh_c088_beneficiarios_pension
                INNER JOIN a003_persona ON rh_c088_beneficiarios_pension.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS parentesco ON rh_c088_beneficiarios_pension.fk_a006_num_miscelaneo_detalle_parentesco = parentesco.pk_num_miscelaneo_detalle
                WHERE rh_c088_beneficiarios_pension.fk_rhb003_num_proceso_pension='$idPensionSobreviviente'
        ");
        $beneficiarios->setFetchMode(PDO::FETCH_ASSOC);
        return $beneficiarios->fetchAll();
    }



}//fin clase
