<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-02-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/

require_once ROOT. 'librerias/' . 'Fecha.php';
class ceseReingresoModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE OBTENER LISTADO DE LOS CESE Y REINGRESOS
    public function metListarCeseReingreso($estatus)
    {

        $con = $this->_db->query("
                SELECT
                rh_c077_empleado_cese_reingreso.pk_num_empleado_cese_reingreso,
                rh_c077_empleado_cese_reingreso.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_documento_fiscal,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) as nombre_completo,
                rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipocr,
                tipocr.ind_nombre_detalle AS tipocr,
                rh_c077_empleado_cese_reingreso.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa,
                rh_c077_empleado_cese_reingreso.fk_a004_num_dependencia,
                rh_c077_empleado_cese_reingreso.ind_dependencia,
                rh_c077_empleado_cese_reingreso.fk_rhc063_num_puestos,
                rh_c077_empleado_cese_reingreso.ind_cargo,
                rh_c077_empleado_cese_reingreso.num_sueldo_actual,
                rh_c077_empleado_cese_reingreso.ind_periodo,
                rh_c077_empleado_cese_reingreso.fec_fecha,
                rh_c077_empleado_cese_reingreso.num_anio_servicio,
                rh_c077_empleado_cese_reingreso.num_edad,
                rh_c077_empleado_cese_reingreso.fec_fecha_ingreso,
                rh_c077_empleado_cese_reingreso.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipotrab,
                tipo_trabajador.ind_nombre_detalle AS tipo_trabajador,
                rh_c077_empleado_cese_reingreso.fk_rhc032_num_motivo_cese,
                rh_c032_motivo_cese.ind_nombre_cese,
                rh_c077_empleado_cese_reingreso.fec_fecha_egreso,
                rh_c077_empleado_cese_reingreso.txt_observacion_egreso,
                rh_c077_empleado_cese_reingreso.num_situacion_trabajo,
                rh_c049_operaciones_cese_reingreso.ind_estado as txt_estatus,
                rh_c049_operaciones_cese_reingreso.num_flag_estatus,
                rh_c049_operaciones_cese_reingreso.fec_operacion
                FROM
                rh_c077_empleado_cese_reingreso
                INNER JOIN rh_b001_empleado ON rh_c077_empleado_cese_reingreso.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS tipocr ON tipocr.pk_num_miscelaneo_detalle = rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipocr
                INNER JOIN a001_organismo ON rh_c077_empleado_cese_reingreso.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN nm_b001_tipo_nomina ON rh_c077_empleado_cese_reingreso.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                LEFT JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipotrab = tipo_trabajador.pk_num_miscelaneo_detalle
                LEFT JOIN rh_c032_motivo_cese ON rh_c077_empleado_cese_reingreso.fk_rhc032_num_motivo_cese = rh_c032_motivo_cese.pk_num_motivo_cese
                INNER JOIN rh_c049_operaciones_cese_reingreso ON rh_c077_empleado_cese_reingreso.pk_num_empleado_cese_reingreso = rh_c049_operaciones_cese_reingreso.fk_rhc077_num_empleado_cese_reingreso
                WHERE rh_c049_operaciones_cese_reingreso.ind_estado='$estatus' and rh_c049_operaciones_cese_reingreso.num_flag_estatus=1
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metMostrarCeseReingreso($idCeseReingreso)
    {
        $con = $this->_db->query("
                SELECT
                rh_c077_empleado_cese_reingreso.pk_num_empleado_cese_reingreso,
                rh_c077_empleado_cese_reingreso.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) as nombre_completo,
                a003_persona.fk_a006_num_miscelaneo_detalle_sexo,
                sexo.ind_nombre_detalle AS sexo,
                a003_persona.fec_nacimiento,
                rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipocr,
                tipocr.ind_nombre_detalle AS tipocr,
                rh_c077_empleado_cese_reingreso.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa,
                rh_c077_empleado_cese_reingreso.fk_a004_num_dependencia,
                rh_c077_empleado_cese_reingreso.ind_dependencia,
                rh_c077_empleado_cese_reingreso.fk_rhc063_num_puestos,
                rh_c077_empleado_cese_reingreso.ind_cargo,
                rh_c077_empleado_cese_reingreso.num_sueldo_actual,
                rh_c077_empleado_cese_reingreso.ind_periodo,
                rh_c077_empleado_cese_reingreso.fec_fecha,
                rh_c077_empleado_cese_reingreso.num_anio_servicio,
                rh_c077_empleado_cese_reingreso.num_edad,
                rh_c077_empleado_cese_reingreso.fec_fecha_ingreso,
                rh_c077_empleado_cese_reingreso.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipotrab,
                tipo_trabajador.ind_nombre_detalle AS tipo_trabajador,
                rh_c077_empleado_cese_reingreso.fk_rhc032_num_motivo_cese,
                rh_c032_motivo_cese.ind_nombre_cese,
                rh_c077_empleado_cese_reingreso.fec_fecha_egreso,
                rh_c077_empleado_cese_reingreso.txt_observacion_egreso,
                rh_c077_empleado_cese_reingreso.num_situacion_trabajo,
                rh_c077_empleado_cese_reingreso.ind_resolucion,
                rh_c049_operaciones_cese_reingreso.ind_estado as txt_estatus,
                rh_c049_operaciones_cese_reingreso.num_flag_estatus,
                rh_c049_operaciones_cese_reingreso.fec_operacion
                FROM
                rh_c077_empleado_cese_reingreso
                INNER JOIN rh_b001_empleado ON rh_c077_empleado_cese_reingreso.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS tipocr ON tipocr.pk_num_miscelaneo_detalle = rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipocr
                INNER JOIN a001_organismo ON rh_c077_empleado_cese_reingreso.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN a006_miscelaneo_detalle AS sexo ON a003_persona.fk_a006_num_miscelaneo_detalle_sexo = sexo.pk_num_miscelaneo_detalle
                INNER JOIN nm_b001_tipo_nomina ON rh_c077_empleado_cese_reingreso.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                LEFT JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipotrab = tipo_trabajador.pk_num_miscelaneo_detalle
                LEFT JOIN rh_c032_motivo_cese ON rh_c077_empleado_cese_reingreso.fk_rhc032_num_motivo_cese = rh_c032_motivo_cese.pk_num_motivo_cese
                INNER JOIN rh_c049_operaciones_cese_reingreso ON rh_c077_empleado_cese_reingreso.pk_num_empleado_cese_reingreso = rh_c049_operaciones_cese_reingreso.fk_rhc077_num_empleado_cese_reingreso
                WHERE rh_c077_empleado_cese_reingreso.pk_num_empleado_cese_reingreso='$idCeseReingreso'
             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    public function metListarCeseReingresoFiltro($organismo,$dependencia,$estatus,$empleado=false)
    {

        if($empleado){
            $filtro = "rh_c049_operaciones_cese_reingreso.ind_estado='$estatus' AND
                       rh_c049_operaciones_cese_reingreso.num_flag_estatus=1 AND
                       rh_c077_empleado_cese_reingreso.fk_a001_num_organismo='$organismo' AND
                       rh_c077_empleado_cese_reingreso.fk_a004_num_dependencia='$dependencia' AND
                       rh_c077_empleado_cese_reingreso.fk_rhb001_num_empleado='$empleado'
                        ";
        }else{
            $filtro = "rh_c049_operaciones_cese_reingreso.ind_estado='$estatus' AND
                       rh_c049_operaciones_cese_reingreso.num_flag_estatus=1 AND
                       rh_c077_empleado_cese_reingreso.fk_a001_num_organismo='$organismo' AND
                       rh_c077_empleado_cese_reingreso.fk_a004_num_dependencia='$dependencia'
                        ";
        }

        $con = $this->_db->query("
                SELECT
                rh_c077_empleado_cese_reingreso.pk_num_empleado_cese_reingreso,
                rh_c077_empleado_cese_reingreso.fk_rhb001_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_documento_fiscal,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) as nombre_completo,
                rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipocr,
                tipocr.ind_nombre_detalle AS tipocr,
                rh_c077_empleado_cese_reingreso.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa,
                rh_c077_empleado_cese_reingreso.fk_a004_num_dependencia,
                rh_c077_empleado_cese_reingreso.ind_dependencia,
                rh_c077_empleado_cese_reingreso.fk_rhc063_num_puestos,
                rh_c077_empleado_cese_reingreso.ind_cargo,
                rh_c077_empleado_cese_reingreso.num_sueldo_actual,
                rh_c077_empleado_cese_reingreso.ind_periodo,
                rh_c077_empleado_cese_reingreso.fec_fecha,
                rh_c077_empleado_cese_reingreso.num_anio_servicio,
                rh_c077_empleado_cese_reingreso.num_edad,
                rh_c077_empleado_cese_reingreso.fec_fecha_ingreso,
                rh_c077_empleado_cese_reingreso.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipotrab,
                tipo_trabajador.ind_nombre_detalle AS tipo_trabajador,
                rh_c077_empleado_cese_reingreso.fk_rhc032_num_motivo_cese,
                rh_c032_motivo_cese.ind_nombre_cese,
                rh_c077_empleado_cese_reingreso.fec_fecha_egreso,
                rh_c077_empleado_cese_reingreso.txt_observacion_egreso,
                rh_c077_empleado_cese_reingreso.num_situacion_trabajo,
                rh_c049_operaciones_cese_reingreso.ind_estado as txt_estatus,
                rh_c049_operaciones_cese_reingreso.num_flag_estatus,
                rh_c049_operaciones_cese_reingreso.fec_operacion
                FROM
                rh_c077_empleado_cese_reingreso
                INNER JOIN rh_b001_empleado ON rh_c077_empleado_cese_reingreso.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS tipocr ON tipocr.pk_num_miscelaneo_detalle = rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipocr
                INNER JOIN a001_organismo ON rh_c077_empleado_cese_reingreso.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN nm_b001_tipo_nomina ON rh_c077_empleado_cese_reingreso.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                LEFT JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c077_empleado_cese_reingreso.fk_a006_num_miscelaneo_detalle_tipotrab = tipo_trabajador.pk_num_miscelaneo_detalle
                LEFT JOIN rh_c032_motivo_cese ON rh_c077_empleado_cese_reingreso.fk_rhc032_num_motivo_cese = rh_c032_motivo_cese.pk_num_motivo_cese
                INNER JOIN rh_c049_operaciones_cese_reingreso ON rh_c077_empleado_cese_reingreso.pk_num_empleado_cese_reingreso = rh_c049_operaciones_cese_reingreso.fk_rhc077_num_empleado_cese_reingreso
                WHERE $filtro


             ");


        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metMostrarCeseReingresoOperaciones($idCeseReingreso,$estatus)
    {
        $con = $this->_db->query("
                SELECT
                rh_c049_operaciones_cese_reingreso.pk_num_operaciones_cr,
                rh_c077_empleado_cese_reingreso.pk_num_empleado_cese_reingreso,
                rh_c049_operaciones_cese_reingreso.ind_estado as txt_estatus,
                rh_c049_operaciones_cese_reingreso.num_flag_estatus,
                rh_c049_operaciones_cese_reingreso.fec_operacion,
                rh_c049_operaciones_cese_reingreso.txt_observaciones,
                rh_c049_operaciones_cese_reingreso.fk_a018_num_seguridad_usuario,
                CONCAT_WS(' ',a003_persona.ind_nombre1,a003_persona.ind_nombre2,a003_persona.ind_apellido1,a003_persona.ind_apellido2) as nombre_registro
                FROM
                rh_c077_empleado_cese_reingreso
                INNER JOIN rh_c049_operaciones_cese_reingreso ON rh_c077_empleado_cese_reingreso.pk_num_empleado_cese_reingreso = rh_c049_operaciones_cese_reingreso.fk_rhc077_num_empleado_cese_reingreso
                INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = rh_c049_operaciones_cese_reingreso.fk_a018_num_seguridad_usuario
                INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = a018_seguridad_usuario.fk_rhb001_num_empleado
                INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                 WHERE rh_c077_empleado_cese_reingreso.pk_num_empleado_cese_reingreso='$idCeseReingreso'
                 AND rh_c049_operaciones_cese_reingreso.ind_estado='$estatus'
             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    #permite obtener los datos del empleado
    public function metObtenerDatosEmpleado($idEmpleado)
    {

        $con =  $this->_db->query("

                SELECT
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                rh_c076_empleado_organizacion.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa AS organismo,
                a003_persona.fk_a006_num_miscelaneo_detalle_sexo,
                sexo.ind_nombre_detalle AS sexo,
                a003_persona.fec_nacimiento,
                rh_c076_empleado_organizacion.fk_a004_num_dependencia,
                a004_dependencia.ind_dependencia AS dependencia,
                rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo,
                rh_c063_puestos.ind_descripcion_cargo AS cargo,
                rh_c063_puestos.num_sueldo_basico,
                rh_c005_empleado_laboral.fec_ingreso,
                rh_b001_empleado.num_estatus AS estatus_empleado,
                a003_persona.num_estatus AS estatus_persona,
                rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador,
                tipo_trabajador.ind_nombre_detalle AS tipo_trabajador
                FROM
                rh_b001_empleado
                INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN a006_miscelaneo_detalle AS sexo ON a003_persona.fk_a006_num_miscelaneo_detalle_sexo = sexo.pk_num_miscelaneo_detalle
                INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                WHERE rh_b001_empleado.pk_num_empleado='$idEmpleado'

        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();

    }


    #PERMITE REGISTRAR LA SOLICITUD DE CESE REINGRESO DEL EMPLEADO
    public function metRegistrarCeseReingreso($datos)
    {
        $this->_db->beginTransaction();

        /*organizo los datos enviados*/
        $empleado = $datos[0];#id_empleado
        $tipocr   = $datos[1];#tipo:cese:reingreso
        $organismo  = $datos[2];#organismo
        $id_dependencia = $datos[3];#id_dependencia
        $dependencia = $datos[4];#dependencia
        $id_cargo = $datos[5];#id_cargo
        $cargo    = $datos[6];#cargo
        $sueldo   = $datos[7];#sueldo actual
        $periodo  = $datos[8];#periodo
        $fecha    = explode("-",$datos[9]);#fecha
        $anio_serv = $datos[10];#años de servicio
        $edad = $datos[11];#edad
        $fecha_ing = explode("-",$datos[12]);#fecha_ingreso
        $tipo_nom  = $datos[13];#tipo_nomina
        $tipo_trab = $datos[14];#tipo_trabajador
        $mot_cese  = $datos[15];#motivo cese
        $fecha_egr = explode("-",$datos[16]);#fecha de egreso
        $obs_egre  = strtoupper($datos[17]);#observacion egreso
        $sit_trab  = $datos[18];#situacion de trabajo
        $resolucion  = strtoupper($datos[19]);#resolucion
        $observacion_preparado = strtoupper($datos[20]);#observacion preparado

        if($sit_trab==0){//cese

            $campos = 'fk_rhc032_num_motivo_cese=:fk_rhc032_num_motivo_cese, fec_fecha_egreso=:fec_fecha_egreso, txt_observacion_egreso=:txt_observacion_egreso,';
            $parametros = array(
                ':fk_rhb001_num_empleado' => $empleado,
                ':fk_a006_num_miscelaneo_detalle_tipocr' => $tipocr,
                ':fk_a001_num_organismo' => $organismo,
                ':fk_a004_num_dependencia' => $id_dependencia,
                ':ind_dependencia' => $dependencia,
                ':fk_rhc063_num_puestos' => $id_cargo,
                ':ind_cargo' => $cargo,
                ':num_sueldo_actual' => $sueldo,
                ':ind_periodo' => $periodo,
                ':fec_fecha' => $fecha[2]."-".$fecha[1]."-".$fecha[0],
                ':num_anio_servicio' => $anio_serv,
                ':num_edad' => $edad,
                ':fec_fecha_ingreso' => $fecha_ing[2]."-".$fecha_ing[1]."-".$fecha_ing[0],
                ':fk_nmb001_num_tipo_nomina' => $tipo_nom,
                ':fk_a006_num_miscelaneo_detalle_tipotrab' => $tipo_trab,
                ':fk_rhc032_num_motivo_cese' => $mot_cese,
                ':fec_fecha_egreso' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0],
                ':txt_observacion_egreso' => $obs_egre,
                ':num_situacion_trabajo' => $sit_trab,
                ':ind_resolucion' => $resolucion
            );

        }else{//reingreso

            $campos = '';
            $parametros = array(
                ':fk_rhb001_num_empleado' => $empleado,
                ':fk_a006_num_miscelaneo_detalle_tipocr' => $tipocr,
                ':fk_a001_num_organismo' => $organismo,
                ':fk_a004_num_dependencia' => $id_dependencia,
                ':ind_dependencia' => $dependencia,
                ':fk_rhc063_num_puestos' => $id_cargo,
                ':ind_cargo' => $cargo,
                ':num_sueldo_actual' => $sueldo,
                ':ind_periodo' => $periodo,
                ':fec_fecha' => $fecha[2]."-".$fecha[1]."-".$fecha[0],
                ':num_anio_servicio' => $anio_serv,
                ':num_edad' => $edad,
                ':fec_fecha_ingreso' => $fecha_ing[2]."-".$fecha_ing[1]."-".$fecha_ing[0],
                ':fk_nmb001_num_tipo_nomina' => $tipo_nom,
                ':fk_a006_num_miscelaneo_detalle_tipotrab' => $tipo_trab,
                ':num_situacion_trabajo' => $sit_trab,
                ':ind_resolucion' => $resolucion
            );

        }

                #inserto nuevo registro en la bitacora
                $registro = $this->_db->prepare(
                    "INSERT INTO
                        rh_c077_empleado_cese_reingreso
                     SET
                        fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                        fk_a006_num_miscelaneo_detalle_tipocr=:fk_a006_num_miscelaneo_detalle_tipocr,
                        fk_a001_num_organismo=:fk_a001_num_organismo,
                        fk_a004_num_dependencia=:fk_a004_num_dependencia,
                        ind_dependencia=:ind_dependencia,
                        fk_rhc063_num_puestos=:fk_rhc063_num_puestos,
                        ind_cargo=:ind_cargo,
                        num_sueldo_actual=:num_sueldo_actual,
                        ind_periodo=:ind_periodo,
                        fec_fecha=:fec_fecha,
                        num_anio_servicio=:num_anio_servicio,
                        num_edad=:num_edad,
                        fec_fecha_ingreso=:fec_fecha_ingreso,
                        fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                        fk_a006_num_miscelaneo_detalle_tipotrab=:fk_a006_num_miscelaneo_detalle_tipotrab,
                        $campos
                        num_situacion_trabajo=:num_situacion_trabajo,
                        ind_resolucion=:ind_resolucion,
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW()
                    "
                );

                $registro->execute($parametros);

                $id = $this->_db->lastInsertId();

                $error = $registro->errorInfo();

                if(!empty($error[1]) && !empty($error[2])){
                    $this->_db->rollBack();
                    return $error;
                }else{

                    $ESTATUS = 'PR';/*preparado*/
                    $FLAG = 1;

                    /*ejecuto procedimiento alamcenado para guardar las operaciones*/
                    $con =  $this->_db->prepare("
                            CALL w_rh_operacionesCeseReingreso(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
                    ");
                    $con->execute(array(
                        ':estatus' => $ESTATUS,
                        ':id' => $id,
                        ':empleado' => $empleado,
                        ':flag' => $FLAG,
                        ':fecha' => $fecha[2]."-".$fecha[1]."-".$fecha[0],
                        ':observaciones' => $observacion_preparado,
                        ':usuario' => $this->atIdUsuario
                    ));

                    $this->_db->commit();
                    return $id;
                }

    }


    #PERMITE MODIFICAR CESE REINGRESO
    public function metModificarCeseReingreso($idCeseReingreso,$datos)
    {
        $this->_db->beginTransaction();

        /*organizo los datos enviados*/
        $motivo = $datos[0];

        if(strcmp($motivo,'C')==0){/*CESE*/

           $nomina =  $datos[1];
           $tipo_trabajador = $datos[2];
           $mot_cese =  $datos[3];
           $fecha_egr = explode("-",$datos[4]);
           $obs_egre = $datos[5];
           $sit_trab = $datos[6];
           $resolucion =  strtoupper($datos[7]);
           $observacion_preparado =  strtoupper($datos[8]);

            $actualizar = $this->_db->prepare(
                "UPDATE
                    rh_c077_empleado_cese_reingreso
                 SET
                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                    fk_a006_num_miscelaneo_detalle_tipotrab=:fk_a006_num_miscelaneo_detalle_tipotrab,
                    fk_rhc032_num_motivo_cese=:fk_rhc032_num_motivo_cese,
                    fec_fecha_egreso=:fec_fecha_egreso,
                    txt_observacion_egreso=:txt_observacion_egreso,
                    num_situacion_trabajo=:num_situacion_trabajo,
                    ind_resolucion=:ind_resolucion,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
                  WHERE pk_num_empleado_cese_reingreso='$idCeseReingreso'
                "
            );
            $actualizar->execute(array(
                ':fk_nmb001_num_tipo_nomina' => $nomina,
                ':fk_a006_num_miscelaneo_detalle_tipotrab' => $tipo_trabajador,
                ':fk_rhc032_num_motivo_cese' => $mot_cese,
                ':fec_fecha_egreso' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0],
                ':txt_observacion_egreso' => $obs_egre,
                ':num_situacion_trabajo' => $sit_trab,
                ':ind_resolucion' => $resolucion
            ));

            $error = $actualizar->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{

                $con = $this->_db->prepare(
                    "UPDATE
                        rh_c049_operaciones_cese_reingreso
                     SET
                        txt_observaciones='$observacion_preparado'
                     WHERE fk_rhc077_num_empleado_cese_reingreso='$idCeseReingreso' and ind_estado='PR'
                     "
                );
                $con->execute();
                $error1 = $con->errorInfo();
                if(!empty($error1[1]) && !empty($error1[2])){
                    $this->_db->rollBack();
                    return $error1;
                }else{
                    $this->_db->commit();
                    return $idCeseReingreso;
                }

            }

        }
        if(strcmp($motivo,'R')==0){/*REINGRESO*/

            $organismo = $datos[1];
            $id_dependencia = $datos[2];
            $dependencia = $datos[3];
            $id_cargo = $datos[4];
            $cargo = $datos[5];
            $nomina = $datos[6];
            $tipo_trabajador = $datos[7];
            $sit_trab = $datos[8];
            $resolucion =  strtoupper($datos[9]);
            $observacion_preparado =  strtoupper($datos[10]);

            $actualizar = $this->_db->prepare(
                "UPDATE
                    rh_c077_empleado_cese_reingreso
                 SET
                    fk_a001_num_organismo=:fk_a001_num_organismo,
                    fk_a004_num_dependencia=:fk_a004_num_dependencia,
                    ind_dependencia=:ind_dependencia,
                    fk_rhc063_num_puestos=:fk_rhc063_num_puestos,
                    ind_cargo=:ind_cargo,
                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                    fk_a006_num_miscelaneo_detalle_tipotrab=:fk_a006_num_miscelaneo_detalle_tipotrab,
                    num_situacion_trabajo=:num_situacion_trabajo,
                    ind_resolucion=:ind_resolucion,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
                  WHERE pk_num_empleado_cese_reingreso='$idCeseReingreso'
                "
            );
            $actualizar->execute(array(
                ':fk_a001_num_organismo' => $organismo,
                ':fk_a004_num_dependencia' => $id_dependencia,
                ':ind_dependencia' => $dependencia,
                ':fk_rhc063_num_puestos' => $id_cargo,
                ':ind_cargo' => $cargo,
                ':fk_nmb001_num_tipo_nomina' => $nomina,
                ':fk_a006_num_miscelaneo_detalle_tipotrab' => $tipo_trabajador,
                ':num_situacion_trabajo' => $sit_trab,
                ':ind_resolucion' => $resolucion
            ));
            $error = $actualizar->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{

                $con = $this->_db->prepare(
                    "UPDATE
                        rh_c049_operaciones_cese_reingreso
                     SET
                        txt_observaciones='$observacion_preparado'
                     WHERE fk_rhc077_num_empleado_cese_reingreso='$idCeseReingreso' and ind_estado='PR'
                     "
                );
                $con->execute();
                $error1 = $con->errorInfo();
                if(!empty($error1[1]) && !empty($error1[2])){
                    $this->_db->rollBack();
                    return $error1;
                }else{
                    $this->_db->commit();
                    return $idCeseReingreso;
                }

            }

        }

    }//fin modificar


    public function metAnularCeseReingreso($idCeseReingreso,$observaciones)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
               select * from rh_c049_operaciones_cese_reingreso
               where fk_rhc077_num_empleado_cese_reingreso='$idCeseReingreso'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        if(strcmp($resultado['ind_estado'],'PR')==0){
            $nuevoEstatus = 'AN';
        }
        elseif(strcmp($resultado['ind_estado'],'CO')==0){
            $nuevoEstatus = 'PR';
        }
        elseif(strcmp($resultado['ind_estado'],'AP')==0){
            $nuevoEstatus = 'CO';
        }

        $fecha = date("Y-m-d");

        /*ejecuto procedimiento alamcenado para guardar las operaciones*/
        $con =  $this->_db->prepare("
                    CALL w_rh_operacionesCeseReingreso(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
        $con->execute(array(
            ':estatus' => $nuevoEstatus,
            ':id' => $idCeseReingreso,
            ':empleado' => $resultado['fk_rhb0001_num_empleado'],
            ':flag' => 1,
            ':fecha' => $fecha,
            ':observaciones' => $observaciones,
            ':usuario' => $this->atIdUsuario
        ));
        $this->_db->commit();
        return $idCeseReingreso;

    }


    public function metConformarCeseReingreso($idCeseReingreso,$observaciones)
    {
        $this->_db->beginTransaction();

        $con = $this->_db->query("
               select * from rh_c049_operaciones_cese_reingreso
               where fk_rhc077_num_empleado_cese_reingreso='$idCeseReingreso'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $fecha = date("Y-m-d");

        /*ejecuto procedimiento alamcenado para guardar las operaciones*/
        $con =  $this->_db->prepare("
                    CALL w_rh_operacionesCeseReingreso(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
        $con->execute(array(
            ':estatus' => 'CO',
            ':id' => $idCeseReingreso,
            ':empleado' => $resultado['fk_rhb0001_num_empleado'],
            ':flag' => 1,
            ':fecha' => $fecha,
            ':observaciones' => $observaciones,
            ':usuario' => $this->atIdUsuario
        ));
        $this->_db->commit();
        return $idCeseReingreso;


    }


    public function metAprobarCeseReingreso($idCeseReingreso,$observaciones,$datos)
    {
        $this->_db->beginTransaction();

        /*organizo los datos enviados*/
        $motivo = $datos[0];
        $empleado = $datos[1];#id_empleado
        $tipocr   = $datos[2];#tipo:cese:reingreso
        $organismo  = $datos[3];#organismo
        $id_dependencia = $datos[4];#id_dependencia
        $dependencia = $datos[5];#dependencia
        $id_cargo = $datos[6];#id_cargo
        $cargo    = $datos[7];#cargo
        $sueldo   = $datos[8];#sueldo actual
        $periodo  = $datos[9];#periodo
        $fecha    = explode("-",$datos[10]);#fecha
        $anio_serv = $datos[11];#años de servicio
        $edad = $datos[12];#edad
        $fecha_ing = explode("-",$datos[13]);#fecha_ingreso
        $tipo_nom  = $datos[14];#tipo_nomina
        $tipo_trab = $datos[15];#tipo_trabajador
        $mot_cese  = $datos[16];#motivo cese
        $fecha_egr = explode("-",$datos[17]);#fecha de egreso
        $obs_egre  = strtoupper($datos[18]);#observacion egreso
        $sit_trab  = $datos[19];#situacion de trabajo
        $resolucion  = strtoupper($datos[20]);#resolucion


        /*****************************ACTUALIZO ESTATUS APAROBADO***************************************************/
        $fecha = date("Y-m-d");

        $con = $this->_db->query("
               select * from rh_c049_operaciones_cese_reingreso
               where fk_rhc077_num_empleado_cese_reingreso='$idCeseReingreso'
               and num_flag_estatus=1
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        $empleado = $resultado['fk_rhb0001_num_empleado'];

        /*ejecuto procedimiento alamcenado para guardar las operaciones*/
        $con =  $this->_db->prepare("
                    CALL w_rh_operacionesCeseReingreso(:estatus,:id,:empleado,:flag,:fecha,:observaciones,:usuario)
            ");
        $con->execute(array(
            ':estatus' => 'AP',
            ':id' => $idCeseReingreso,
            ':empleado' => $resultado['fk_rhb0001_num_empleado'],
            ':flag' => 1,
            ':fecha' => $fecha,
            ':observaciones' => strtoupper($observaciones),
            ':usuario' => $this->atIdUsuario
        ));
        $errorCall = $con->errorInfo();
        if(!empty($errorCall[1]) && !empty($errorCall[2])){
            $this->_db->rollBack();
            return $errorCall;
        }else{


            /*****************************ACTUALIZO ESTATUS APAROBADO***************************************************/


            /*********SEGUN LA OPERACION SI ES CESE O REINGRESO HAGO LAS OPERACIONES CORRESPONDIENTE*********************/

            if(strcmp($motivo,'C')==0) {/*CESE*/

                /*actualizo el empleado*/
                $c1 = $this->_db->prepare("
                UPDATE rh_b001_empleado
                SET
                num_estatus=0,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                WHERE pk_num_empleado='$empleado'
            ");

                $c1->execute();
                $c1Error = $c1->errorInfo();
                if(!empty($c1Error[1]) && !empty($c1Error[2])){
                    $this->_db->rollBack();
                    return $c1Error;
                }else{
                    /*----------------------------------------------------------------------------*/
                    $c2 = $this->_db->prepare("
                UPDATE rh_c076_empleado_organizacion
                SET
                  fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                  fk_a006_num_miscelaneo_detalle_tipotrabajador=:fk_a006_num_miscelaneo_detalle_tipotrabajador,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                WHERE fk_rhb001_num_empleado='$empleado'
                ");

                    $c2->execute(array(
                        ':fk_nmb001_num_tipo_nomina' => $tipo_nom,
                        ':fk_a006_num_miscelaneo_detalle_tipotrabajador' => $tipo_trab
                    ));
                    $c2Error = $c2->errorInfo();
                    if(!empty($c2Error[1]) && !empty($c2Error[2])){
                        $this->_db->rollBack();
                        return $c2Error;
                    }else{
                        /*----------------------------------------------------------------------------*/
                        $c3 = $this->_db->prepare("
                    UPDATE rh_c005_empleado_laboral
                    SET
                      fec_egreso=:fec_egreso,
                      ind_resolucion_egreso=:ind_resolucion_egreso,
                      num_sueldo_ant=:num_sueldo_ant,
                      fec_ultima_modificacion=NOW(),
                      fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    WHERE fk_rhb001_num_empleado='$empleado'
                    ");

                        $c3->execute(array(
                            ':fec_egreso' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0],
                            ':ind_resolucion_egreso' => $resolucion,
                            ':num_sueldo_ant' => $sueldo
                        ));
                        $c3Error = $c3->errorInfo();
                        if(!empty($c3Error[1]) && !empty($c3Error[2])){
                            $this->_db->rollBack();
                            return $c3Error;
                        }else{
                            /*inserto nivelacion, historial*/

                            $FechaHasta = $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0];
                            $con = $this->_db->query("SELECT ADDDATE('".$fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0]."', ".intval(0).") AS FechaResultado");
                            $con->setFetchMode(PDO::FETCH_ASSOC);
                            $dato = $con->fetch();
                            $FechaHastaAnterior = $dato['FechaResultado'];

                            /*actualizo nivelacion anterior*/

                            $c4 = $this->_db->prepare("
                            UPDATE rh_c059_empleado_nivelacion
                            SET
                              fec_fecha_hasta=:fec_fecha_hasta,
                              fec_ultima_modificacion=NOW(),
                              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                            WHERE fk_rhb001_num_empleado='$empleado' AND (fec_fecha_hasta='0000-00-00' OR  fec_fecha_hasta='' OR fec_fecha_hasta IS  NULL )
                            ");
                            $c4->execute(array(
                                ':fec_fecha_hasta' => $FechaHastaAnterior
                            ));
                            $c4Error = $c4->errorInfo();
                            if(!empty($c4Error[1]) && !empty($c4Error[2])){
                                $this->_db->rollBack();
                                return $c4Error;
                            }else{
                                /*insero nueva nivelacion*/
                                $cc = $this->_db->query("select fk_a023_num_centro_costo from rh_c076_empleado_organizacion where fk_rhb001_num_empleado='$empleado'");
                                $cc->setFetchMode(PDO::FETCH_ASSOC);
                                $centroCosto = $cc->fetch();

                                $c5 = $this->_db->prepare(
                                    "INSERT INTO
                                    rh_c059_empleado_nivelacion
                                 SET
                                    fk_rhb001_num_empleado='$empleado',
                                    fec_fecha_registro=:fec_fecha_registro,
                                    fk_a001_num_organismo=:fk_a001_num_organismo,
                                    fk_a004_num_dependencia=:fk_a004_num_dependencia,
                                    fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                                    fk_rhc063_num_puestos=:fk_rhc063_num_puestos,
                                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                                    ind_documento=:ind_documento,
                                    num_estatus=:num_estatus,
                                    fec_fecha_hasta=:fec_fecha_hasta,
                                    fec_ultima_modificacion=NOW(),
                                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                                  ");
                                #execute — Ejecuta una sentencia preparada
                                $c5->execute(array(
                                    ':fec_fecha_registro' => $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0],
                                    ':fk_a001_num_organismo'  => $organismo,
                                    ':fk_a004_num_dependencia'  => $id_dependencia,
                                    ':fk_a023_num_centro_costo'  => $centroCosto['fk_a023_num_centro_costo'],
                                    ':fk_rhc063_num_puestos'  => $id_cargo,
                                    ':fk_nmb001_num_tipo_nomina' => $tipo_nom,
                                    ':ind_documento' => $resolucion,
                                    ':num_estatus' => $sit_trab,
                                    ':fec_fecha_hasta' => $FechaHasta
                                ));
                                $c5Error = $c5->errorInfo();
                                $id_nivelacion = $this->_db->lastInsertId();
                                if(!empty($c5Error[1]) && !empty($c5Error[2])){
                                    $this->_db->rollBack();
                                    return $c5Error;
                                }else{
                                    /*inserto nivelacion historial*/
                                    $c6 = $this->_db->prepare(
                                        "INSERT INTO
                                        rh_c060_empleado_nivelacion_historial
                                        (fk_rhc059_num_empleado_nivelacion,
                                         fec_fecha_registro,
                                         ind_organismo,
                                         ind_dependencia,
                                         ind_cargo,
                                         num_nivel_salarial,
                                         ind_categoria_cargo,
                                         ind_tipo_nomina,
                                         ind_centro_costo,
                                         ind_tipo_accion,
                                         num_estatus,
                                         fec_ultima_modificacion,
                                         fk_a018_num_seguridad_usuario)
                                    SELECT
                                        rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion,
                                        rh_c059_empleado_nivelacion.fec_fecha_registro,
                                        organismo.ind_descripcion_empresa AS ind_organismo,
                                        dependencia.ind_dependencia AS ind_dependencia,
                                        cargo.ind_descripcion_cargo AS ind_cargo,
                                        cargo.num_sueldo_basico AS num_nivel_salarial,
                                        categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                        nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                        centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                        tipo_accion.ind_nombre_detalle AS ind_tipo_accion,
                                        rh_c059_empleado_nivelacion.num_estatus,
                                        NOW() as fec_ultima_modificacion,
                                        '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                                    FROM
                                        rh_c059_empleado_nivelacion
                                    INNER JOIN a001_organismo AS organismo ON organismo.pk_num_organismo = rh_c059_empleado_nivelacion.fk_a001_num_organismo
                                    INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = rh_c059_empleado_nivelacion.fk_a004_num_dependencia
                                    INNER JOIN a023_centro_costo AS centro_costo ON centro_costo.pk_num_centro_costo = rh_c059_empleado_nivelacion.fk_a023_num_centro_costo
                                    INNER JOIN rh_c063_puestos AS cargo ON cargo.pk_num_puestos = rh_c059_empleado_nivelacion.fk_rhc063_num_puestos
                                    INNER JOIN nm_b001_tipo_nomina AS nomina ON nomina.pk_num_tipo_nomina = rh_c059_empleado_nivelacion.fk_nmb001_num_tipo_nomina
                                    LEFT JOIN a006_miscelaneo_detalle AS tipo_accion ON tipo_accion.pk_num_miscelaneo_detalle = rh_c059_empleado_nivelacion.fk_a006_num_miscelaneo_detalle_tipoaccion
                                    INNER JOIN a006_miscelaneo_detalle AS categoria ON cargo.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                    WHERE rh_c059_empleado_nivelacion.fk_rhb001_num_empleado='$empleado'
                                    AND rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion='$id_nivelacion'
                                      ");

                                    $c6->execute();
                                    $c6Error = $c6->errorInfo();
                                    if(!empty($c6Error[1]) && !empty($c6Error[2])){
                                        $this->_db->rollBack();
                                        return $c6Error;
                                    }else{
                                        /*inserto historial empleado*/
                                        $c7 = $this->_db->prepare(
                                            "INSERT INTO
                                            rh_c061_empleado_historial
                                            (fk_rhb001_num_empleado,
                                             ind_periodo,
                                             fec_ingreso,
                                             ind_organismo,
                                             ind_dependencia,
                                             ind_centro_costo,
                                             ind_cargo,
                                             num_nivel_salarial,
                                             ind_categoria_cargo,
                                             ind_tipo_nomina,
                                             ind_tipo_pago,
                                             num_estatus,
                                             ind_motivo_cese,
                                             fec_egreso,
                                             txt_obs_cese,
                                             ind_tipo_trabajador,
                                             fec_ultima_modificacion,
                                             fk_a018_num_seguridad_usuario)
                                        SELECT
                                            empleado.pk_num_empleado,
                                            NOW() as ind_periodo,
                                            rh_c005_empleado_laboral.fec_ingreso,
                                            a001_organismo.ind_descripcion_empresa AS ind_organismo,
                                            a004_dependencia.ind_dependencia,
                                            a023_centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                            rh_c063_puestos.ind_descripcion_cargo AS ind_cargo,
                                            rh_c063_puestos.num_sueldo_basico AS ind_nivel_salarial,
                                            categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                            nm_b001_tipo_nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                            tipo_pago.ind_nombre_detalle AS ind_tipo_pago,
                                            empleado.num_estatus,
                                            motivo_cese.ind_nombre_cese AS ind_motivo_cese,
                                            cese_reingreso.fec_fecha_egreso AS fec_egreso,
                                            cese_reingreso.txt_observacion_egreso AS txt_obs_cese,
                                            tipo_trabajador.ind_nombre_detalle AS ind_tipo_trabajador,
                                            NOW() as fec_ultima_modificacion,
                                            '".$this->atIdUsuario."' as fk_a018_num_seguridad_usuario
                                        FROM
                                            rh_b001_empleado AS empleado
                                            INNER JOIN rh_c076_empleado_organizacion ON empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                                            INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                            INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                                            INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                                            INNER JOIN a023_centro_costo ON rh_c076_empleado_organizacion.fk_a023_num_centro_costo = a023_centro_costo.pk_num_centro_costo
                                            INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                                            INNER JOIN a006_miscelaneo_detalle AS categoria ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                            INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                                            INNER JOIN a006_miscelaneo_detalle AS tipo_pago ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipopago = tipo_pago.pk_num_miscelaneo_detalle
                                            INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                                            INNER JOIN rh_c077_empleado_cese_reingreso AS cese_reingreso ON cese_reingreso.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                            INNER JOIN rh_c032_motivo_cese AS motivo_cese ON cese_reingreso.fk_rhc032_num_motivo_cese = motivo_cese.pk_num_motivo_cese
                                        WHERE
                                            empleado.pk_num_empleado='$empleado'

                                     ");

                                        $c7->execute();
                                        $c7Error = $c7->errorInfo();
                                        if(!empty($c7Error[1]) && !empty($c7Error[2])){
                                            $this->_db->rollBack();
                                            return $c7Error;
                                        }else{
                                            /*elimino periodo vacacional si fue creado*/
                                            $consulta = $this->_db->query("
                                               SELECT MAX(num_anio) as anio
                                               FROM rh_c081_periodo_vacacion
                                               WHERE fk_rhb001_num_empleado='$empleado'
                                            ");

                                            $consulta->setFetchMode(PDO::FETCH_ASSOC);
                                            $resultado = $consulta->fetch();
                                            $anio = $resultado['anio'];
                                            if($anio!=''){
                                                $AnioActual = date("Y");
                                                list($di, $mi, $ai) = explode("-", $datos[13]);
                                                list($de, $me, $ae) = explode("-", $datos[17]);
                                                if ("$me-$de" < "$mi-$di" && ($anio+1) == $AnioActual) {

                                                    $c8=$this->_db->prepare("
                                                    DELETE FROM rh_c081_periodo_vacacion WHERE fk_rhb001_num_empleado='$empleado' AND num_anio='$anio'
                                                    ");

                                                    $c8->execute();
                                                    $c8Error = $c8->errorInfo();
                                                    if(!empty($c8Error[1]) && !empty($c8Error[2])){
                                                        $this->_db->rollBack();
                                                        return $c8Error;
                                                    }else{


                                                        $lfecha = new Fecha();
                                                        //	inactivo de bono de alimentacion los dias
                                                        ##	obtengo el periodo de bono de alimentacion
                                                        $fechaAMD = $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0];
                                                        $resultado = $this->_db->query(
                                                            "select * from rh_c019_beneficio_alimentacion where fk_nmb001_num_tipo_nomina='$tipo_nom' and '$fechaAMD' >= fec_inicio_periodo and '$fechaAMD'  <= fec_fin_periodo"
                                                        );

                                                        $datos = $resultado->fetch();

                                                        $fecha = $datos['fec_anio'];
                                                        $org = $datos['fk_a001_num_organismo'];
                                                        $bono = $datos['pk_num_beneficio'];
                                                        $persona = $empleado;
                                                        ##	obtengo el detalle del empleado
                                                        $resultado1 = $this->_db->query(
                                                            "select * from rh_c039_beneficio_alimentacion_detalle where fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'"
                                                        );
                                                        $datos1 = $resultado1->fetch();

                                                        $campos_dias = "";
                                                        $conf = $this->_db->query("
                                                            SELECT ADDDATE('".date("Y-m-d", strtotime($fechaAMD))."', ".intval(0).") AS FechaResultado
                                                        ");
                                                        $datos2 = $conf->fetch();
                                                        $FechaHasta = $datos2['FechaResultado'];
                                                        $inicio = $lfecha->dias_fecha($datos['fec_inicio_periodo'],$FechaHasta);
                                                        $DiasPago = 0;
                                                        $DiasFeriados = 0;
                                                        $DiasInactivos = 0;

                                                        for($i=$inicio+2;$i<=$datos['num_dias_periodo'];$i++) {
                                                            $Dia = "num_dia".$i;
                                                            if ($datos1[$Dia] == "X") { ++$DiasPago; ++$DiasInactivos; $campos_dias .= ", num_dia".$i." = 'I'"; }
                                                            elseif ($datos1[$Dia] == "F") { ++$DiasFeriados; ++$DiasInactivos; $campos_dias .= ", num_dia".$i." = 'I'"; }
                                                        }

                                                        ##	actualizo periodo del empleado
                                                        $c8 = $this->_db->prepare("
                                                            UPDATE rh_c039_beneficio_alimentacion_detalle
                                                            SET
                                                            num_dias_pago = num_dias_pago - ".intval($DiasPago).",
                                                            num_dias_feriados = num_dias_feriados - ".intval($DiasFeriados).",
                                                            num_dias_inactivos = num_dias_inactivos + ".intval($DiasInactivos)."
                                                            $campos_dias,
                                                            fec_ultima_modificacion=NOW(),
                                                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                                                            WHERE fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'
                                                        ");
                                                        $c8->execute();
                                                        $c8Error = $c8->errorInfo();
                                                        if(!empty($c8Error[1]) && !empty($c8Error[2])){
                                                            $this->_db->rollBack();
                                                            return $c8Error;
                                                        }else {
                                                            ##	consulto nuevos valores
                                                            $resultado3 = $this->_db->query(
                                                                "select num_dias_pago,num_dias_descuento from rh_c039_beneficio_alimentacion_detalle where fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'"
                                                            );
                                                            $datos3 = $resultado3->fetch();

                                                            ##	actualizo el nuevo monto

                                                            $ValorPagar = $datos3['num_dias_pago'] * $datos['num_valor_diario'];
                                                            $ValorDescuento = $datos3['num_dias_descuento'] * $datos['num_valor_diario'];
                                                            $TotalPagar = $ValorPagar - $ValorDescuento;

                                                            $c9 = $this->_db->prepare("
                                                            UPDATE rh_c039_beneficio_alimentacion_detalle
                                                            SET
                                                            num_valor_pago = ".floatval($ValorPagar).",
                                                            num_valor_descuento = ".floatval($ValorDescuento).",
                                                            num_valor_total = ".floatval($TotalPagar).",
                                                            fec_ultima_modificacion=NOW(),
                                                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                                                            WHERE fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'
                                                            ");
                                                            $c9->execute();
                                                            $c9Error = $c9->errorInfo();
                                                            if (!empty($c9Error[1]) && !empty($c9Error[2])) {
                                                                $this->_db->rollBack();
                                                                return $c9Error;
                                                            } else {

                                                                $this->_db->commit();
                                                                return $idCeseReingreso;

                                                            }

                                                        }

                                                    }

                                                }else{

                                                    $lfecha = new Fecha();
                                                    //	inactivo de bono de alimentacion los dias
                                                    ##	obtengo el periodo de bono de alimentacion
                                                    $fechaAMD = $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0];
                                                    $resultado = $this->_db->query(
                                                        "select * from rh_c019_beneficio_alimentacion where fk_nmb001_num_tipo_nomina='$tipo_nom' and '$fechaAMD' >= fec_inicio_periodo and '$fechaAMD'  <= fec_fin_periodo"
                                                    );

                                                    $datos = $resultado->fetch();

                                                    $fecha = $datos['fec_anio'];
                                                    $org = $datos['fk_a001_num_organismo'];
                                                    $bono = $datos['pk_num_beneficio'];
                                                    $persona = $empleado;
                                                    ##	obtengo el detalle del empleado
                                                    $resultado1 = $this->_db->query(
                                                        "select * from rh_c039_beneficio_alimentacion_detalle where fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'"
                                                    );
                                                    $datos1 = $resultado1->fetch();

                                                    $campos_dias = "";
                                                    $conf = $this->_db->query("
                                                            SELECT ADDDATE('".date("Y-m-d", strtotime($fechaAMD))."', ".intval(0).") AS FechaResultado
                                                        ");
                                                    $datos2 = $conf->fetch();
                                                    $FechaHasta = $datos2['FechaResultado'];
                                                    $inicio = $lfecha->dias_fecha($datos['fec_inicio_periodo'],$FechaHasta);
                                                    $DiasPago = 0;
                                                    $DiasFeriados = 0;
                                                    $DiasInactivos = 0;

                                                    for($i=$inicio+2;$i<=$datos['num_dias_periodo'];$i++) {
                                                        $Dia = "num_dia".$i;
                                                        if ($datos1[$Dia] == "X") { ++$DiasPago; ++$DiasInactivos; $campos_dias .= ", num_dia".$i." = 'I'"; }
                                                        elseif ($datos1[$Dia] == "F") { ++$DiasFeriados; ++$DiasInactivos; $campos_dias .= ", num_dia".$i." = 'I'"; }
                                                    }

                                                    ##	actualizo periodo del empleado
                                                    $c8 = $this->_db->prepare("
                                                            UPDATE rh_c039_beneficio_alimentacion_detalle
                                                            SET
                                                            num_dias_pago = num_dias_pago - ".intval($DiasPago).",
                                                            num_dias_feriados = num_dias_feriados - ".intval($DiasFeriados).",
                                                            num_dias_inactivos = num_dias_inactivos + ".intval($DiasInactivos)."
                                                            $campos_dias,
                                                            fec_ultima_modificacion=NOW(),
                                                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                                                            WHERE fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'
                                                        ");
                                                    $c8->execute();
                                                    $c8Error = $c8->errorInfo();
                                                    if(!empty($c8Error[1]) && !empty($c8Error[2])){
                                                        $this->_db->rollBack();
                                                        return $c8Error;
                                                    }else {
                                                        ##	consulto nuevos valores
                                                        $resultado3 = $this->_db->query(
                                                            "select num_dias_pago,num_dias_descuento from rh_c039_beneficio_alimentacion_detalle where fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'"
                                                        );
                                                        $datos3 = $resultado3->fetch();

                                                        ##	actualizo el nuevo monto


                                                        $ValorPagar = $datos3['num_dias_pago'] * $datos['num_valor_diario'];
                                                        $ValorDescuento = $datos3['num_dias_descuento'] * $datos['num_valor_diario'];
                                                        $TotalPagar = $ValorPagar - $ValorDescuento;

                                                        $c9 = $this->_db->prepare("
                                                            UPDATE rh_c039_beneficio_alimentacion_detalle
                                                            SET
                                                            num_valor_pago = ".floatval($ValorPagar).",
                                                            num_valor_descuento = ".floatval($ValorDescuento).",
                                                            num_valor_total = ".floatval($TotalPagar).",
                                                            fec_ultima_modificacion=NOW(),
                                                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                                                            WHERE fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'
                                                            ");
                                                        $c9->execute();
                                                        $c9Error = $c9->errorInfo();
                                                        if (!empty($c9Error[1]) && !empty($c9Error[2])) {
                                                            $this->_db->rollBack();
                                                            return $c9Error;
                                                        } else {

                                                            $this->_db->commit();
                                                            return $idCeseReingreso;

                                                        }

                                                    }

                                                }

                                            }else{

                                                $lfecha = new Fecha();
                                                //	inactivo de bono de alimentacion los dias
                                                ##	obtengo el periodo de bono de alimentacion
                                                $fechaAMD = $fecha_egr[2]."-".$fecha_egr[1]."-".$fecha_egr[0];
                                                $resultado = $this->_db->query(
                                                    "select * from rh_c019_beneficio_alimentacion where fk_nmb001_num_tipo_nomina='$tipo_nom' and '$fechaAMD' >= fec_inicio_periodo and '$fechaAMD'  <= fec_fin_periodo"
                                                );

                                                $datos = $resultado->fetch();

                                                $fecha = $datos['fec_anio'];
                                                $org = $datos['fk_a001_num_organismo'];
                                                $bono = $datos['pk_num_beneficio'];
                                                $persona = $empleado;
                                                ##	obtengo el detalle del empleado
                                                $resultado1 = $this->_db->query(
                                                    "select * from rh_c039_beneficio_alimentacion_detalle where fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'"
                                                );
                                                $datos1 = $resultado1->fetch();

                                                $campos_dias = "";
                                                $conf = $this->_db->query("
                                                            SELECT ADDDATE('".date("Y-m-d", strtotime($fechaAMD))."', ".intval(0).") AS FechaResultado
                                                        ");
                                                $datos2 = $conf->fetch();
                                                $FechaHasta = $datos2['FechaResultado'];
                                                $inicio = $lfecha->dias_fecha($datos['fec_inicio_periodo'],$FechaHasta);
                                                $DiasPago = 0;
                                                $DiasFeriados = 0;
                                                $DiasInactivos = 0;

                                                for($i=$inicio+2;$i<=$datos['num_dias_periodo'];$i++) {
                                                    $Dia = "num_dia".$i;
                                                    if ($datos1[$Dia] == "X") { ++$DiasPago; ++$DiasInactivos; $campos_dias .= ", num_dia".$i." = 'I'"; }
                                                    elseif ($datos1[$Dia] == "F") { ++$DiasFeriados; ++$DiasInactivos; $campos_dias .= ", num_dia".$i." = 'I'"; }
                                                }

                                                ##	actualizo periodo del empleado
                                                $c8 = $this->_db->prepare("
                                                            UPDATE rh_c039_beneficio_alimentacion_detalle
                                                            SET
                                                            num_dias_pago = num_dias_pago - ".intval($DiasPago).",
                                                            num_dias_feriados = num_dias_feriados - ".intval($DiasFeriados).",
                                                            num_dias_inactivos = num_dias_inactivos + ".intval($DiasInactivos)."
                                                            $campos_dias,
                                                            fec_ultima_modificacion=NOW(),
                                                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                                                            WHERE fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'
                                                        ");
                                                $c8->execute();
                                                $c8Error = $c8->errorInfo();
                                                if(!empty($c8Error[1]) && !empty($c8Error[2])){
                                                    $this->_db->rollBack();
                                                    return $c8Error;
                                                }else {
                                                    ##	consulto nuevos valores
                                                    $resultado3 = $this->_db->query(
                                                        "select num_dias_pago,num_dias_descuento from rh_c039_beneficio_alimentacion_detalle where fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'"
                                                    );
                                                    $datos3 = $resultado3->fetch();

                                                    ##	actualizo el nuevo monto

                                                    $ValorPagar = $datos3['num_dias_pago'] * $datos['num_valor_diario'];
                                                    $ValorDescuento = $datos3['num_dias_descuento'] * $datos['num_valor_diario'];
                                                    $TotalPagar = $ValorPagar - $ValorDescuento;

                                                    $c9 = $this->_db->prepare("
                                                            UPDATE rh_c039_beneficio_alimentacion_detalle
                                                            SET
                                                            num_valor_pago = ".floatval($ValorPagar).",
                                                            num_valor_descuento = ".floatval($ValorDescuento).",
                                                            num_valor_total = ".floatval($TotalPagar).",
                                                            fec_ultima_modificacion=NOW(),
                                                            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                                                            WHERE fec_anio='$fecha' and fk_a001_num_organismo='$org' and fk_rhc019_num_beneficio='$bono'  and fk_rhb001_num_empleado='$persona'
                                                            ");
                                                    $c9->execute();
                                                    $c9Error = $c9->errorInfo();
                                                    if (!empty($c9Error[1]) && !empty($c9Error[2])) {
                                                        $this->_db->rollBack();
                                                        return $c9Error;
                                                    } else {

                                                        $this->_db->commit();
                                                        return $idCeseReingreso;

                                                    }

                                                }

                                            }

                                        }

                                    }

                                }

                            }

                        }

                    }

                }

            }else{/*REINGRESO*/

                /*actualizo el empleado*/
                $c1 = $this->_db->prepare("
                UPDATE rh_b001_empleado
                SET
                num_estatus=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                WHERE pk_num_empleado='$empleado'
            ");
                $c1->execute();
                $c1Error = $c1->errorInfo();
                if(!empty($c1Error[1]) && !empty($c1Error[2])){
                    $this->_db->rollBack();
                    return $c1Error;
                }else {
                    /*----------------------------------------------------------------------------*/
                    $c2 = $this->_db->prepare("
                UPDATE rh_c076_empleado_organizacion
                SET
                  fk_a001_num_organismo=:fk_a001_num_organismo,
                  fk_a004_num_dependencia=:fk_a004_num_dependencia,
                  fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                  fk_a006_num_miscelaneo_detalle_tipotrabajador=:fk_a006_num_miscelaneo_detalle_tipotrabajador,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                WHERE fk_rhb001_num_empleado='$empleado'
                ");
                    $c2->execute(array(
                        ':fk_a001_num_organismo' => $organismo,
                        ':fk_a004_num_dependencia' => $id_dependencia,
                        ':fk_nmb001_num_tipo_nomina' => $tipo_nom,
                        ':fk_a006_num_miscelaneo_detalle_tipotrabajador' => $tipo_trab
                    ));
                    $c2Error = $c2->errorInfo();
                    if (!empty($c2Error[1]) && !empty($c2Error[2])) {
                        $this->_db->rollBack();
                        return $c2Error;
                    } else {
                        /*----------------------------------------------------------------------------*/
                        $consulta = $this->_db->query("
                                               SELECT fec_ingreso,num_sueldo
                                               FROM rh_c005_empleado_laboral
                                               WHERE fk_rhb001_num_empleado='$empleado'
                    ");
                        $consulta->setFetchMode(PDO::FETCH_ASSOC);
                        $resultado = $consulta->fetch();

                        $c3 = $this->_db->prepare("
                    UPDATE rh_c005_empleado_laboral
                    SET
                      fk_rhc063_num_puestos_cargo=:fk_rhc063_num_puestos_cargo,
                      fec_ingreso=:fec_ingreso,
                      ind_resolucion_ingreso=:ind_resolucion_ingreso,
                      fec_ingreso_anterior=:fec_ingreso_anterior,
                      num_sueldo_ant=:num_sueldo_ant,
                      num_sueldo=:num_sueldo,
                      fec_ultima_modificacion=NOW(),
                      fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                    WHERE fk_rhb001_num_empleado='$empleado'
                    ");
                        $c3->execute(array(
                            ':fk_rhc063_num_puestos_cargo' => $id_cargo,
                            ':fec_ingreso' => $fecha_ing[2] . "-" . $fecha_ing[1] . "-" . $fecha_ing[0],
                            ':ind_resolucion_ingreso' => $resolucion,
                            ':fec_ingreso_anterior' => $resultado['fec_ingreso'],
                            ':num_sueldo_ant' => $resultado['num_sueldo'],
                            ':num_sueldo' => $sueldo
                        ));
                        $c3Error = $c3->errorInfo();
                        if (!empty($c3Error[1]) && !empty($c3Error[2])) {
                            $this->_db->rollBack();
                            return $c3Error;
                        } else {

                            /*inserto nueva nivelacion*/
                            $cc = $this->_db->query("select fk_a023_num_centro_costo from rh_c076_empleado_organizacion where fk_rhb001_num_empleado='$empleado'");
                            $cc->setFetchMode(PDO::FETCH_ASSOC);
                            $centroCosto = $cc->fetch();

                            $c5 = $this->_db->prepare(
                                "INSERT INTO
                                    rh_c059_empleado_nivelacion
                                 SET
                                    fk_rhb001_num_empleado='$empleado',
                                    fec_fecha_registro=:fec_fecha_registro,
                                    fk_a001_num_organismo=:fk_a001_num_organismo,
                                    fk_a004_num_dependencia=:fk_a004_num_dependencia,
                                    fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                                    fk_rhc063_num_puestos=:fk_rhc063_num_puestos,
                                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                                    ind_documento=:ind_documento,
                                    num_estatus=:num_estatus,
                                    fec_fecha_hasta=:fec_fecha_hasta,
                                    fec_ultima_modificacion=NOW(),
                                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                                  ");
                            #execute — Ejecuta una sentencia preparada
                            $c5->execute(array(
                                ':fec_fecha_registro' => $fecha,
                                ':fk_a001_num_organismo' => $organismo,
                                ':fk_a004_num_dependencia' => $id_dependencia,
                                ':fk_a023_num_centro_costo' => $centroCosto['fk_a023_num_centro_costo'],
                                ':fk_rhc063_num_puestos' => $id_cargo,
                                ':fk_nmb001_num_tipo_nomina' => $tipo_nom,
                                ':ind_documento' => $resolucion,
                                ':num_estatus' => $sit_trab,
                                ':fec_fecha_hasta' => '0000-00-00'
                            ));
                            $c5Error = $c5->errorInfo();
                            $id_nivelacion = $this->_db->lastInsertId();
                            if (!empty($c5Error[1]) && !empty($c5Error[2])) {
                                $this->_db->rollBack();
                                return $c5Error;
                            } else {
                                /*inserto nivelacion historial*/
                                $c6 = $this->_db->prepare(
                                    "INSERT INTO
                                        rh_c060_empleado_nivelacion_historial
                                        (fk_rhc059_num_empleado_nivelacion,
                                         fec_fecha_registro,
                                         ind_organismo,
                                         ind_dependencia,
                                         ind_cargo,
                                         num_nivel_salarial,
                                         ind_categoria_cargo,
                                         ind_tipo_nomina,
                                         ind_centro_costo,
                                         ind_tipo_accion,
                                         num_estatus,
                                         fec_ultima_modificacion,
                                         fk_a018_num_seguridad_usuario)
                                    SELECT
                                        rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion,
                                        rh_c059_empleado_nivelacion.fec_fecha_registro,
                                        organismo.ind_descripcion_empresa AS ind_organismo,
                                        dependencia.ind_dependencia AS ind_dependencia,
                                        cargo.ind_descripcion_cargo AS ind_cargo,
                                        cargo.num_sueldo_basico AS num_nivel_salarial,
                                        categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                        nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                        centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                        tipo_accion.ind_nombre_detalle AS ind_tipo_accion,
                                        rh_c059_empleado_nivelacion.num_estatus,
                                        NOW() as fec_ultima_modificacion,
                                        '" . $this->atIdUsuario . "' as fk_a018_num_seguridad_usuario
                                    FROM
                                        rh_c059_empleado_nivelacion
                                    INNER JOIN a001_organismo AS organismo ON organismo.pk_num_organismo = rh_c059_empleado_nivelacion.fk_a001_num_organismo
                                    INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = rh_c059_empleado_nivelacion.fk_a004_num_dependencia
                                    INNER JOIN a023_centro_costo AS centro_costo ON centro_costo.pk_num_centro_costo = rh_c059_empleado_nivelacion.fk_a023_num_centro_costo
                                    INNER JOIN rh_c063_puestos AS cargo ON cargo.pk_num_puestos = rh_c059_empleado_nivelacion.fk_rhc063_num_puestos
                                    INNER JOIN nm_b001_tipo_nomina AS nomina ON nomina.pk_num_tipo_nomina = rh_c059_empleado_nivelacion.fk_nmb001_num_tipo_nomina
                                    LEFT JOIN a006_miscelaneo_detalle AS tipo_accion ON tipo_accion.pk_num_miscelaneo_detalle = rh_c059_empleado_nivelacion.fk_a006_num_miscelaneo_detalle_tipoaccion
                                    INNER JOIN a006_miscelaneo_detalle AS categoria ON cargo.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                    WHERE rh_c059_empleado_nivelacion.fk_rhb001_num_empleado='$empleado'
                                    AND rh_c059_empleado_nivelacion.pk_num_empleado_nivelacion='$id_nivelacion'
                                      ");

                                $c6->execute();
                                $c6Error = $c6->errorInfo();
                                if (!empty($c6Error[1]) && !empty($c6Error[2])) {
                                    $this->_db->rollBack();
                                    return $c6Error;
                                } else {
                                    /*inserto historial empleado*/
                                    $c7 = $this->_db->prepare(
                                        "INSERT INTO
                                            rh_c061_empleado_historial
                                            (fk_rhb001_num_empleado,
                                             ind_periodo,
                                             fec_ingreso,
                                             ind_organismo,
                                             ind_dependencia,
                                             ind_centro_costo,
                                             ind_cargo,
                                             num_nivel_salarial,
                                             ind_categoria_cargo,
                                             ind_tipo_nomina,
                                             ind_tipo_pago,
                                             num_estatus,
                                             -- ind_motivo_cese,
                                             -- fec_egreso,
                                             -- txt_obs_cese,
                                             ind_tipo_trabajador,
                                             fec_ultima_modificacion,
                                             fk_a018_num_seguridad_usuario)
                                        SELECT
                                            empleado.pk_num_empleado,
                                            NOW() as ind_periodo,
                                            rh_c005_empleado_laboral.fec_ingreso,
                                            a001_organismo.ind_descripcion_empresa AS ind_organismo,
                                            a004_dependencia.ind_dependencia,
                                            a023_centro_costo.ind_descripcion_centro_costo AS ind_centro_costo,
                                            rh_c063_puestos.ind_descripcion_cargo AS ind_cargo,
                                            rh_c063_puestos.num_sueldo_basico AS ind_nivel_salarial,
                                            categoria.ind_nombre_detalle AS ind_categoria_cargo,
                                            nm_b001_tipo_nomina.ind_nombre_nomina AS ind_tipo_nomina,
                                            tipo_pago.ind_nombre_detalle AS ind_tipo_pago,
                                            empleado.num_estatus,
                                            -- motivo_cese.ind_nombre_cese AS ind_motivo_cese,
                                            -- cese_reingreso.fec_fecha_egreso AS fec_egreso,
                                            -- cese_reingreso.txt_observacion_egreso AS txt_obs_cese,
                                            tipo_trabajador.ind_nombre_detalle AS ind_tipo_trabajador,
                                            NOW() as fec_ultima_modificacion,
                                            '" . $this->atIdUsuario . "' as fk_a018_num_seguridad_usuario
                                        FROM
                                            rh_b001_empleado AS empleado
                                            INNER JOIN rh_c076_empleado_organizacion ON empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
                                            INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                            INNER JOIN a001_organismo ON rh_c076_empleado_organizacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                                            INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                                            INNER JOIN a023_centro_costo ON rh_c076_empleado_organizacion.fk_a023_num_centro_costo = a023_centro_costo.pk_num_centro_costo
                                            INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                                            INNER JOIN a006_miscelaneo_detalle AS categoria ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria = categoria.pk_num_miscelaneo_detalle
                                            INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                                            INNER JOIN a006_miscelaneo_detalle AS tipo_pago ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipopago = tipo_pago.pk_num_miscelaneo_detalle
                                            INNER JOIN a006_miscelaneo_detalle AS tipo_trabajador ON rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipotrabajador = tipo_trabajador.pk_num_miscelaneo_detalle
                                            -- INNER JOIN rh_c077_empleado_cese_reingreso AS cese_reingreso ON cese_reingreso.fk_rhb001_num_empleado = empleado.pk_num_empleado
                                            -- INNER JOIN rh_c032_motivo_cese AS motivo_cese ON cese_reingreso.fk_rhc032_num_motivo_cese = motivo_cese.pk_num_motivo_cese
                                        WHERE
                                            empleado.pk_num_empleado='$empleado'

                                     ");
                                    $c7->execute();
                                    $c7Error = $c7->errorInfo();
                                    if (!empty($c7Error[1]) && !empty($c7Error[2])) {
                                        $this->_db->rollBack();
                                        return $c7Error;
                                    } else {
                                        $this->_db->commit();
                                        return $idCeseReingreso;
                                        /*PENDIENTE*/
                                        /*actualizo bono alimentacion*/
                                    }

                                }

                            }

                        }

                    }

                }

            }/*FIN ELSE*/


        }//fin else call


        /*********SEGUN LA OPERACION SI ES CESE O REINGRESO HAGO LAS OPERACIONES CORRESPONDIENTE*********************/

    }/*FIN METODO*/


    public function metVerificarSolicitudes($idEmpleado)
    {

        $con = $this->_db->query("
                SELECT
                  COUNT(*) AS num
                FROM
                    rh_c077_empleado_cese_reingreso
                WHERE
                    fk_rhb001_num_empleado='$idEmpleado'
             ");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado = $con->fetch();

        if($resultado['num']== 0){

            return 0;

        }else{

            $con1 = $this->_db->query("
                SELECT
                a.fk_rhb001_num_empleado,
                a006_miscelaneo_detalle.cod_detalle,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                b.ind_estado,
                b.num_flag_estatus,
                b.fec_operacion,
                b.txt_observaciones,
                a.fk_a006_num_miscelaneo_detalle_tipocr
                FROM
                    rh_c077_empleado_cese_reingreso AS a
                INNER JOIN rh_c049_operaciones_cese_reingreso AS b ON a.pk_num_empleado_cese_reingreso = b.fk_rhc077_num_empleado_cese_reingreso
                AND a.fk_rhb001_num_empleado = b.fk_rhb0001_num_empleado
                INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = a.fk_a006_num_miscelaneo_detalle_tipocr
                WHERE
                    b.num_flag_estatus = 1 and a.fk_rhb001_num_empleado='$idEmpleado'
             ");
            $con1->setFetchMode(PDO::FETCH_ASSOC);
            return $con1->fetchAll();

        }




    }



}//fin clase
