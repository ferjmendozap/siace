<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';
class bonoAlimentacionModelo extends miscelaneoModelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE OBTENER VALOR UNIDAD TRIBUTARIA
    public function metObtenerUT($anio,$secuencia=null)
    {


        if ($secuencia != "") {
            $filtro = " AND ut.num_secuencia = '".$secuencia."'";
        } else {
            $filtro = " AND ut.num_secuencia = (SELECT MAX(num_secuencia) FROM cp_b018_unidad_tributaria WHERE ind_anio = ut.ind_anio)";
        }

        $con = $this->_db->query("
                SELECT ut.ind_valor
                FROM 
                cp_b018_unidad_tributaria ut
                WHERE ut.ind_anio = '".$anio."' 
                order by ut.pk_num_unidad_tributaria desc
                LIMIT 0, 1
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    #PERMITE BUSCAR TODOS LOS EMPLEADOS DE ACUERDO AL TIPO DE NOMINA E IMPORTARLOS
    public function metImportarEmpleados($organismo,$nomina)
    {

        $con = $this->_db->query("
                SELECT
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                a004_dependencia.ind_dependencia,
                rh_c063_puestos.ind_descripcion_cargo,
                rh_b001_empleado.num_estatus as estatus_empleado,
                a003_persona.num_estatus as estatus_persona
                FROM
                rh_b001_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                WHERE rh_c076_empleado_organizacion.fk_a001_num_organismo='$organismo' AND
                      rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina='$nomina' AND
                      a003_persona.num_estatus=1 AND
                      rh_b001_empleado.num_estatus=1
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();

    }


    #PERMITE LISTAR LOS PERIODOS DE BONO DE ALIMENTACION
    public function metListarBonoAlimentacion()
    {
        $con = $this->_db->query("
                SELECT
                rh_c019_beneficio_alimentacion.pk_num_beneficio,
                rh_c019_beneficio_alimentacion.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa,
                rh_c019_beneficio_alimentacion.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.cod_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c019_beneficio_alimentacion.fec_anio,
                rh_c019_beneficio_alimentacion.ind_periodo,
                rh_c019_beneficio_alimentacion.ind_descripcion,
                rh_c019_beneficio_alimentacion.fec_inicio_periodo,
                rh_c019_beneficio_alimentacion.fec_fin_periodo,
                rh_c019_beneficio_alimentacion.fk_b002_num_partida_presupuestaria,
                rh_c019_beneficio_alimentacion.cod_partida,
                rh_c019_beneficio_alimentacion.ind_denominacion_partida,
                rh_c019_beneficio_alimentacion.fk_cpb002_tipo_documento,
                rh_c019_beneficio_alimentacion.num_dias_periodo,
                rh_c019_beneficio_alimentacion.num_dias_pago,
                rh_c019_beneficio_alimentacion.num_total_feriados,
                rh_c019_beneficio_alimentacion.num_valor_diario,
                rh_c019_beneficio_alimentacion.fec_horas_diarias,
                rh_c019_beneficio_alimentacion.fec_horas_semanales,
                rh_c019_beneficio_alimentacion.num_valor_semanal,
                rh_c019_beneficio_alimentacion.num_valor_mes,
                rh_c019_beneficio_alimentacion.ind_estado as num_estado,
                rh_c019_beneficio_alimentacion.fec_ultima_modificacion,
                rh_c019_beneficio_alimentacion.fk_a018_num_seguridad_usuario
                FROM
                rh_c019_beneficio_alimentacion
                INNER JOIN a001_organismo ON rh_c019_beneficio_alimentacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN nm_b001_tipo_nomina ON rh_c019_beneficio_alimentacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                ORDER BY rh_c019_beneficio_alimentacion.pk_num_beneficio DESC
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }

    public function metMostrarBonoAlimentacion($bonoAlimentacion)
    {
        $con = $this->_db->query("
                SELECT
                rh_c019_beneficio_alimentacion.pk_num_beneficio,
                rh_c019_beneficio_alimentacion.fk_a001_num_organismo,
                a001_organismo.ind_descripcion_empresa,
                rh_c019_beneficio_alimentacion.fk_nmb001_num_tipo_nomina,
                nm_b001_tipo_nomina.cod_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina,
                rh_c019_beneficio_alimentacion.fec_anio,
                rh_c019_beneficio_alimentacion.ind_periodo,
                rh_c019_beneficio_alimentacion.ind_descripcion,
                rh_c019_beneficio_alimentacion.fec_inicio_periodo,
                rh_c019_beneficio_alimentacion.fec_fin_periodo,
                rh_c019_beneficio_alimentacion.fk_b002_num_partida_presupuestaria,
                rh_c019_beneficio_alimentacion.cod_partida,
                rh_c019_beneficio_alimentacion.ind_denominacion_partida,   
                rh_c019_beneficio_alimentacion.fk_cpb002_tipo_documento,
                rh_c019_beneficio_alimentacion.fk_prb004_num_presupuesto,
                rh_c019_beneficio_alimentacion.ind_cod_presupuesto,
                rh_c019_beneficio_alimentacion.num_dias_periodo,
                rh_c019_beneficio_alimentacion.num_dias_pago,
                rh_c019_beneficio_alimentacion.num_total_feriados,
                rh_c019_beneficio_alimentacion.num_valor_diario,
                rh_c019_beneficio_alimentacion.fec_horas_diarias,
                rh_c019_beneficio_alimentacion.fec_horas_semanales,
                rh_c019_beneficio_alimentacion.num_valor_semanal,
                rh_c019_beneficio_alimentacion.num_valor_mes,
                rh_c019_beneficio_alimentacion.ind_descripcion_descuento,
                rh_c019_beneficio_alimentacion.num_monto_descuento,
                rh_c019_beneficio_alimentacion.ind_estado as num_estado,
                rh_c019_beneficio_alimentacion.fec_ultima_modificacion,
                rh_c019_beneficio_alimentacion.fk_a018_num_seguridad_usuario
                FROM
                rh_c019_beneficio_alimentacion
                INNER JOIN a001_organismo ON rh_c019_beneficio_alimentacion.fk_a001_num_organismo = a001_organismo.pk_num_organismo
                INNER JOIN nm_b001_tipo_nomina ON rh_c019_beneficio_alimentacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                WHERE rh_c019_beneficio_alimentacion.pk_num_beneficio='$bonoAlimentacion'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetch();
    }


    public function metMostrarBonoAlimentacionEmpleados($bonoAlimentacion)
    {
        $con = $this->_db->query("
                SELECT
                rh_c039_beneficio_alimentacion_detalle.pk_num_beneficio_alimentacion_detalle,
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                a004_dependencia.ind_dependencia,
                rh_c063_puestos.ind_descripcion_cargo,
                rh_b001_empleado.num_estatus as estatus_empleado,
                a003_persona.num_estatus as estatus_persona,
                rh_c039_beneficio_alimentacion_detalle.num_monto_descuento_otros
                FROM
                rh_c039_beneficio_alimentacion_detalle
                INNER JOIN rh_b001_empleado ON rh_c039_beneficio_alimentacion_detalle.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                WHERE rh_c039_beneficio_alimentacion_detalle.fk_rhc019_num_beneficio='$bonoAlimentacion'
             ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        return $con->fetchAll();
    }


    public function metMostrarBonoAlimentacionEmpleadosDetalle($bonoAlimentacion,$modo,$empleado)
    {

        if(strcmp($modo,'encabezado')==0){

            $con = $this->_db->query("
                SELECT
                rh_c019_beneficio_alimentacion.pk_num_beneficio,
                rh_c019_beneficio_alimentacion.fec_anio,
                rh_c019_beneficio_alimentacion.ind_periodo,
                rh_c019_beneficio_alimentacion.ind_descripcion,
                rh_c019_beneficio_alimentacion.fec_inicio_periodo,
                rh_c019_beneficio_alimentacion.fec_fin_periodo,
                rh_c019_beneficio_alimentacion.fk_a001_num_organismo,
                nm_b001_tipo_nomina.ind_nombre_nomina
                FROM
                rh_c019_beneficio_alimentacion
                INNER JOIN nm_b001_tipo_nomina ON rh_c019_beneficio_alimentacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                WHERE rh_c019_beneficio_alimentacion.pk_num_beneficio='$bonoAlimentacion'
             ");

            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();

        }
        if(strcmp($modo,'completo')==0){

            $con = $this->_db->query("
                SELECT
                rh_c039_beneficio_alimentacion_detalle.pk_num_beneficio_alimentacion_detalle,
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                a004_dependencia.ind_dependencia,
                rh_c063_puestos.ind_descripcion_cargo,
                rh_b001_empleado.num_estatus AS estatus_empleado,
                a003_persona.num_estatus AS estatus_persona,
                rh_c039_beneficio_alimentacion_detalle.fec_anio,
                rh_c039_beneficio_alimentacion_detalle.num_dias_periodo,
                rh_c039_beneficio_alimentacion_detalle.num_dias_pago,
                rh_c039_beneficio_alimentacion_detalle.num_dias_feriados,
                rh_c039_beneficio_alimentacion_detalle.num_dias_inactivos,
                rh_c039_beneficio_alimentacion_detalle.num_dias_descuento,
                rh_c039_beneficio_alimentacion_detalle.num_valor_pago,
                rh_c039_beneficio_alimentacion_detalle.num_valor_descuento,
                rh_c039_beneficio_alimentacion_detalle.num_monto_descuento_otros,
                rh_c039_beneficio_alimentacion_detalle.num_valor_total,
                (rh_c039_beneficio_alimentacion_detalle.num_dias_periodo - rh_c039_beneficio_alimentacion_detalle.num_dias_descuento) as num_dias_trabajados,
                rh_c039_beneficio_alimentacion_detalle.fk_a001_num_organismo
                FROM
                rh_c039_beneficio_alimentacion_detalle
                INNER JOIN rh_b001_empleado ON rh_c039_beneficio_alimentacion_detalle.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
                INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                WHERE rh_c039_beneficio_alimentacion_detalle.fk_rhc019_num_beneficio='$bonoAlimentacion'
             ");

            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetchAll();

        }

        if(strcmp($modo,'empleado')==0){

            $con = $this->_db->query("
                SELECT
                rh_c039_beneficio_alimentacion_detalle.fk_rhc019_num_beneficio,
                rh_c039_beneficio_alimentacion_detalle.pk_num_beneficio_alimentacion_detalle,
                rh_c039_beneficio_alimentacion_detalle.fk_rhb001_num_empleado,
                a003_persona.ind_cedula_documento,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo
                FROM
                rh_c039_beneficio_alimentacion_detalle
                INNER JOIN rh_b001_empleado ON rh_c039_beneficio_alimentacion_detalle.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE rh_c039_beneficio_alimentacion_detalle.fk_rhc019_num_beneficio='$bonoAlimentacion'
                and rh_c039_beneficio_alimentacion_detalle.fk_rhb001_num_empleado='$empleado'

             ");

            $con->setFetchMode(PDO::FETCH_ASSOC);
            return $con->fetch();

        }

    }

    #PERMITE REGISTRAR UN NUEVO PERIODO BONO ALIMENTACION
    public function metRegistrarBonoAlimentacion($datos,$empleados)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #organizo los datos recibidos
        $organismo   = $datos[0];
        $descripcion = $datos[1];
        $nomina      = $datos[2];
        $periodo     = $datos[3];
        $fecha_ini   = explode("-",$datos[4]);
        $fecha_fin   = explode("-",$datos[5]);
        $num_dias_periodo = $datos[6];
        $num_dias_pago    = $datos[7];
        $num_total_feriados = $datos[8];
        $num_valor_diario = $datos[9];
        $fec_horas_diarias = $datos[10];
        $fec_horas_semanales = $datos[11];
        $num_valor_semanal = $datos[12];
        $num_valor_mes     = $datos[13];
        $id_partida = $datos[14];
        $cod_partida = $datos[15];
        $ind_denominacion = $datos[16];
        $tipo_documento = $datos[17];
        $cod_presupuesto = $datos[18];
        $presupuesto = $datos[19];
        $fec_anio = date("Y");
        $descripcion_descuento = mb_strtoupper($datos[20],'utf8');
        $monto_descuento = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $datos[21]));
        $monto_descuento_otros = $datos[22];


        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoRegistro = $this->_db->prepare(
            "INSERT INTO
                rh_c019_beneficio_alimentacion
             SET
                fk_a001_num_organismo=:fk_a001_num_organismo,
                fec_anio=:fec_anio,
                ind_periodo=:ind_periodo,
                ind_descripcion=:ind_descripcion,
                fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                fec_inicio_periodo=:fec_inicio_periodo,
                fec_fin_periodo=:fec_fin_periodo,
                fk_b002_num_partida_presupuestaria=:fk_b002_num_partida_presupuestaria,
                cod_partida=:cod_partida,
                ind_denominacion_partida=:ind_denominacion_partida,
                fk_cpb002_tipo_documento=:fk_cpb002_tipo_documento,
                fk_prb004_num_presupuesto=:fk_prb004_num_presupuesto,
                ind_cod_presupuesto=:ind_cod_presupuesto,
                num_dias_periodo=:num_dias_periodo,
                num_dias_pago=:num_dias_pago,
                num_total_feriados=:num_total_feriados,
                num_valor_diario=:num_valor_diario,
                fec_horas_diarias=:fec_horas_diarias,
                fec_horas_semanales=:fec_horas_semanales,
                num_valor_semanal=:num_valor_semanal,
                num_valor_mes=:num_valor_mes,
                ind_descripcion_descuento=:ind_descripcion_descuento,
                num_monto_descuento=:num_monto_descuento,
                ind_estado=1,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");

        #execute — Ejecuta una sentencia preparada
        $NuevoRegistro->execute(array(
            ':fk_a001_num_organismo'  => $organismo,
            ':fec_anio'  => $fec_anio,
            ':ind_periodo'  => $periodo,
            ':ind_descripcion'  => $descripcion,
            ':fk_nmb001_num_tipo_nomina' => $nomina,
            ':fec_inicio_periodo' => $fecha_ini[2]."-".$fecha_ini[1]."-".$fecha_ini[0],
            ':fec_fin_periodo' => $fecha_fin[2]."-".$fecha_fin[1]."-".$fecha_fin[0],
            ':fk_b002_num_partida_presupuestaria' => $id_partida,
            ':cod_partida' => $cod_partida,
            ':ind_denominacion_partida' => $ind_denominacion,
            ':fk_cpb002_tipo_documento' => $tipo_documento,
            ':fk_prb004_num_presupuesto' => $presupuesto,
            ':ind_cod_presupuesto' => $cod_presupuesto,
            ':num_dias_periodo' => $num_dias_periodo,
            ':num_dias_pago' => $num_dias_pago,
            ':num_total_feriados'  => $num_total_feriados,
            ':num_valor_diario'  => $num_valor_diario,
            ':fec_horas_diarias'  => $fec_horas_diarias,
            ':fec_horas_semanales'  => $fec_horas_semanales,
            ':num_valor_semanal'  => $num_valor_semanal,
            ':num_valor_mes'  => $num_valor_mes,
            ':ind_descripcion_descuento'  => $descripcion_descuento,
            ':num_monto_descuento'  => $monto_descuento
        ));

        $errorRegBonoAlimentacion = $NuevoRegistro->errorInfo();

        $id_bono_alimentacion = $this->_db->lastInsertId();

        if(!empty($errorRegBonoAlimentacion[1]) && !empty($errorRegBonoAlimentacion[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $errorRegBonoAlimentacion;
        }else{

            $lista_empleado = explode("#",$empleados);
            array_pop($lista_empleado);//eliminamos el ultimo elemento del array (que esta en blanco)

            $totalAux = count($lista_empleado);

            $_dias_completos = $num_dias_periodo;

            for($i=0; $i<$totalAux; $i++){

                $_ValorPagar = 0;
                $_DiasInactivos = 0;
                $_dia_semana = $this->metObtenerDiaSemana($datos[4]);
                $_fecha = $datos[4];
                //	obtengo la leyenda de los dias
                for ($j=1; $j<=$_dias_completos; $j++) {
                    if ($_dia_semana == 7) $_dia_semana = 0;
                    if ($_dia_semana >= 1 && $_dia_semana <= 5) {

                        if ($this->metObtenerDiasFeriados($_fecha, $_fecha) > 0)
                        {
                            $l = "F";
                            $_ValorPagar += $num_valor_diario; //agregado modificacion

                        } else {

                            $l = "X";
                            $_ValorPagar += $num_valor_diario;
                        }
                    }
                    elseif ($_dia_semana == 0 || $_dia_semana == 6)
                    {
                        $l = "I";
                        $_DiasInactivos++;
                        $_ValorPagar += $num_valor_diario; //agregado modificacion
                    }
                    $_Dia[$j] = $l;
                    ##
                    $_dia_semana++;

                    $f = explode("-",$_fecha);
                    $con = $this->_db->query("SELECT ADDDATE('".$f[2]."-".$f[1]."-".$f[0]."', '1') AS FechaResultado");
                    $con->setFetchMode(PDO::FETCH_ASSOC);
                    $x = $con->fetch();
                    $resp   = explode("-",$x['FechaResultado']);
                    $_fecha = $resp[2]."-".$resp[1]."-".$resp[0];

                }//forn interno

                if(isset($_Dia[28])){
                    $_Dia[28]=$_Dia[28];
                }else{ $_Dia[28]=NULL;}
                if(isset($_Dia[29])){
                    $_Dia[29]=$_Dia[29];
                }else{ $_Dia[29]=NULL;}
                if(isset($_Dia[30])){
                    $_Dia[30]=$_Dia[30];
                }else{ $_Dia[30]=NULL;}
                if(isset($_Dia[31])){
                    $_Dia[31]=$_Dia[31];
                }else{ $_Dia[31]=NULL;}

                $NuevoRegistroDetalle = $this->_db->prepare(
                    "INSERT INTO
                        rh_c039_beneficio_alimentacion_detalle
                     SET
                        fk_a001_num_organismo=:fk_a001_num_organismo,
                        fk_rhc019_num_beneficio=:fk_rhc019_num_beneficio,
                        fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                        fec_anio=:fec_anio,
                        num_dia1=:num_dia1,
                        num_dia2=:num_dia3,
                        num_dia3=:num_dia3,
                        num_dia4=:num_dia4,
                        num_dia5=:num_dia5,
                        num_dia6=:num_dia6,
                        num_dia7=:num_dia7,
                        num_dia8=:num_dia8,
                        num_dia9=:num_dia9,
                        num_dia10=:num_dia10,
                        num_dia11=:num_dia11,
                        num_dia12=:num_dia12,
                        num_dia13=:num_dia13,
                        num_dia14=:num_dia14,
                        num_dia15=:num_dia15,
                        num_dia16=:num_dia16,
                        num_dia17=:num_dia17,
                        num_dia18=:num_dia18,
                        num_dia19=:num_dia19,
                        num_dia20=:num_dia20,
                        num_dia21=:num_dia21,
                        num_dia22=:num_dia22,
                        num_dia23=:num_dia23,
                        num_dia24=:num_dia24,
                        num_dia25=:num_dia25,
                        num_dia26=:num_dia26,
                        num_dia27=:num_dia27,
                        num_dia28=:num_dia28,
                        num_dia29=:num_dia29,
                        num_dia30=:num_dia30,
                        num_dia31=:num_dia31,
                        num_dias_periodo=:num_dias_periodo,
                        num_dias_pago=:num_dias_pago,
                        num_dias_feriados=:num_dias_feriados,
                        num_dias_inactivos=:num_dias_inactivos,
                        num_dias_descuento=:num_dias_descuento,
                        num_valor_pago=:num_valor_pago,
                        num_valor_descuento=:num_valor_descuento,
                        num_monto_descuento_otros=:num_monto_descuento_otros,
                        num_valor_total=:num_valor_total,
                        ind_estado=1,
                        fec_ultima_modificacion=NOW(),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                ");
                #execute — Ejecuta una sentencia preparada
                $NuevoRegistroDetalle->execute(array(
                    ':fk_a001_num_organismo'  => $organismo,
                    ':fk_rhc019_num_beneficio'  => $id_bono_alimentacion,
                    ':fk_rhb001_num_empleado' => $lista_empleado[$i],
                    ':fec_anio'  => $fec_anio,
                    ':num_dia1'  => $_Dia[1],
                    ':num_dia2'  => $_Dia[2],
                    ':num_dia3'  => $_Dia[3],
                    ':num_dia4'  => $_Dia[4],
                    ':num_dia5'  => $_Dia[5],
                    ':num_dia6'  => $_Dia[6],
                    ':num_dia7'  => $_Dia[7],
                    ':num_dia8'  => $_Dia[8],
                    ':num_dia9'  => $_Dia[9],
                    ':num_dia10'  => $_Dia[10],
                    ':num_dia11'  => $_Dia[11],
                    ':num_dia12'  => $_Dia[12],
                    ':num_dia13'  => $_Dia[13],
                    ':num_dia14'  => $_Dia[14],
                    ':num_dia15'  => $_Dia[15],
                    ':num_dia16'  => $_Dia[16],
                    ':num_dia17'  => $_Dia[17],
                    ':num_dia18'  => $_Dia[18],
                    ':num_dia19'  => $_Dia[19],
                    ':num_dia20'  => $_Dia[20],
                    ':num_dia21'  => $_Dia[21],
                    ':num_dia22'  => $_Dia[22],
                    ':num_dia23'  => $_Dia[23],
                    ':num_dia24'  => $_Dia[24],
                    ':num_dia25'  => $_Dia[25],
                    ':num_dia26'  => $_Dia[26],
                    ':num_dia27'  => $_Dia[27],
                    ':num_dia28'  => $_Dia[28],
                    ':num_dia29'  => $_Dia[29],
                    ':num_dia30'  => $_Dia[30],
                    ':num_dia31'  => $_Dia[31],
                    ':num_dias_periodo' => $num_dias_periodo,
                    ':num_dias_pago' => $num_dias_pago,
                    ':num_dias_feriados'  => $num_total_feriados,
                    ':num_dias_inactivos'  => $_DiasInactivos,
                    ':num_dias_descuento'  => 0,
                    ':num_valor_pago'  => $_ValorPagar,
                    ':num_valor_descuento'  => 0,
                    ':num_monto_descuento_otros'  => str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $monto_descuento_otros[$i])),
                    ':num_valor_total'  => $_ValorPagar
                ));

                $errorRegBonoAlimentacionDet = $NuevoRegistroDetalle->errorInfo();


            }//for externo

            if(!empty($errorRegBonoAlimentacionDet[1]) && !empty($errorRegBonoAlimentacionDet[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $errorRegBonoAlimentacionDet;
            }else{
                $this->_db->commit();
                return 1;
            }

        }//fin else

    }


    public function metModificarBonoAlimentacion($bonoAlimentacion,$datos,$empleados)
    {


        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #organizo los datos recibidos
        $organismo   = $datos[0];
        $descripcion = $datos[1];
        $nomina      = $datos[2];
        $periodo     = $datos[3];
        $fecha_ini   = explode("-",$datos[4]);
        $fecha_fin   = explode("-",$datos[5]);
        $num_dias_periodo = $datos[6];
        $num_dias_pago    = $datos[7];
        $num_total_feriados = $datos[8];
        $num_valor_diario = $datos[9];
        $fec_horas_diarias = $datos[10];
        $fec_horas_semanales = $datos[11];
        $num_valor_semanal = $datos[12];
        $num_valor_mes     = $datos[13];
        $id_partida = $datos[14];
        $cod_partida = $datos[15];
        $ind_denominacion = $datos[16];
        $tipo_documento = $datos[17];
        $cod_presupuesto = $datos[18];
        $presupuesto = $datos[19];
        $fec_anio = date("Y");
        $descripcion_descuento = mb_strtoupper($datos[20],'utf8');
        $monto_descuento = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $datos[21]));
        $monto_descuento_otros = $datos[22];


        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $actualizarRegistro = $this->_db->prepare(
            "UPDATE
                rh_c019_beneficio_alimentacion
             SET
                ind_periodo=:ind_periodo,
                ind_descripcion=:ind_descripcion,
                fec_inicio_periodo=:fec_inicio_periodo,
                fec_fin_periodo=:fec_fin_periodo,
                fk_b002_num_partida_presupuestaria=:fk_b002_num_partida_presupuestaria,
                cod_partida=:cod_partida,
                ind_denominacion_partida=:ind_denominacion_partida,
                fk_cpb002_tipo_documento=:fk_cpb002_tipo_documento,
                fk_prb004_num_presupuesto=:fk_prb004_num_presupuesto,
                ind_cod_presupuesto=:ind_cod_presupuesto,
                ind_descripcion_descuento=:ind_descripcion_descuento,
                num_monto_descuento=:num_monto_descuento,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE  pk_num_beneficio='$bonoAlimentacion'
              ");

        #execute — Ejecuta una sentencia preparada
        $actualizarRegistro->execute(array(
            ':ind_periodo'  => $periodo,
            ':ind_descripcion'  => $descripcion,
            ':fec_inicio_periodo' => $fecha_ini[2]."-".$fecha_ini[1]."-".$fecha_ini[0],
            ':fec_fin_periodo' => $fecha_fin[2]."-".$fecha_fin[1]."-".$fecha_fin[0],
            ':fk_b002_num_partida_presupuestaria' => $id_partida,
            ':cod_partida' => $cod_partida,
            ':ind_denominacion_partida' => $ind_denominacion,
            ':fk_cpb002_tipo_documento' => $tipo_documento,
            ':fk_prb004_num_presupuesto' => $presupuesto,
            ':ind_cod_presupuesto' => $cod_presupuesto,
            ':ind_descripcion_descuento'  => $descripcion_descuento,
            ':num_monto_descuento'  => $monto_descuento
        ));

        $errorActBonoAlimentacion = $actualizarRegistro->errorInfo();

        if(!empty($errorActBonoAlimentacion[1]) && !empty($errorActBonoAlimentacion[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $errorActBonoAlimentacion;
        }else{


            #elimino detalle del bono alimentacion

            $eliminar=$this->_db->prepare("
                DELETE FROM rh_c039_beneficio_alimentacion_detalle WHERE fk_rhc019_num_beneficio=:fk_rhc019_num_beneficio
            ");
            $eliminar->execute(array(
                'fk_rhc019_num_beneficio'=>$bonoAlimentacion
            ));
            $error=$eliminar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{


                    ###agrego nuevo detalle del bono de adlimentacion###

                    $lista_empleado = explode("#",$empleados);
                    array_pop($lista_empleado);//eliminamos el ultimo elemento del array (que esta en blanco)

                    $totalAux = count($lista_empleado);

                    $_dias_completos = $num_dias_periodo;

                    for($i=0; $i<$totalAux; $i++){

                        $_ValorPagar = 0;
                        $_DiasInactivos = 0;
                        $_dia_semana = $this->metObtenerDiaSemana($datos[4]);
                        $_fecha = $datos[4];
                        //	obtengo la leyenda de los dias
                        for ($j=1; $j<=$_dias_completos; $j++) {
                            if ($_dia_semana == 7) $_dia_semana = 0;
                            if ($_dia_semana >= 1 && $_dia_semana <= 5) {

                                if ($this->metObtenerDiasFeriados($_fecha, $_fecha) > 0)
                                {
                                    $l = "F";
                                    $_ValorPagar += $num_valor_diario; //agregado modificacion

                                } else {

                                    $l = "X";
                                    $_ValorPagar += $num_valor_diario;
                                }
                            }
                            elseif ($_dia_semana == 0 || $_dia_semana == 6)
                            {
                                $l = "I";
                                $_DiasInactivos++;
                                $_ValorPagar += $num_valor_diario; //agregado modificacion
                            }
                            $_Dia[$j] = $l;
                            ##
                            $_dia_semana++;

                            $f = explode("-",$_fecha);
                            $con = $this->_db->query("SELECT ADDDATE('".$f[2]."-".$f[1]."-".$f[0]."', '1') AS FechaResultado");
                            $con->setFetchMode(PDO::FETCH_ASSOC);
                            $x = $con->fetch();
                            $resp   = explode("-",$x['FechaResultado']);
                            $_fecha = $resp[2]."-".$resp[1]."-".$resp[0];

                        }//forn interno

                        if(isset($_Dia[28])){
                            $_Dia[28]=$_Dia[28];
                        }else{ $_Dia[28]=NULL;}
                        if(isset($_Dia[29])){
                            $_Dia[29]=$_Dia[29];
                        }else{ $_Dia[29]=NULL;}
                        if(isset($_Dia[30])){
                            $_Dia[30]=$_Dia[30];
                        }else{ $_Dia[30]=NULL;}
                        if(isset($_Dia[31])){
                            $_Dia[31]=$_Dia[31];
                        }else{ $_Dia[31]=NULL;}



                        $NuevoRegistroDetalle = $this->_db->prepare(
                            "INSERT INTO
                                rh_c039_beneficio_alimentacion_detalle
                             SET
                                fk_a001_num_organismo=:fk_a001_num_organismo,
                                fk_rhc019_num_beneficio=:fk_rhc019_num_beneficio,
                                fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                                fec_anio=:fec_anio,
                                num_dia1=:num_dia1,
                                num_dia2=:num_dia3,
                                num_dia3=:num_dia3,
                                num_dia4=:num_dia4,
                                num_dia5=:num_dia5,
                                num_dia6=:num_dia6,
                                num_dia7=:num_dia7,
                                num_dia8=:num_dia8,
                                num_dia9=:num_dia9,
                                num_dia10=:num_dia10,
                                num_dia11=:num_dia11,
                                num_dia12=:num_dia12,
                                num_dia13=:num_dia13,
                                num_dia14=:num_dia14,
                                num_dia15=:num_dia15,
                                num_dia16=:num_dia16,
                                num_dia17=:num_dia17,
                                num_dia18=:num_dia18,
                                num_dia19=:num_dia19,
                                num_dia20=:num_dia20,
                                num_dia21=:num_dia21,
                                num_dia22=:num_dia22,
                                num_dia23=:num_dia23,
                                num_dia24=:num_dia24,
                                num_dia25=:num_dia25,
                                num_dia26=:num_dia26,
                                num_dia27=:num_dia27,
                                num_dia28=:num_dia28,
                                num_dia29=:num_dia29,
                                num_dia30=:num_dia30,
                                num_dia31=:num_dia31,
                                num_dias_periodo=:num_dias_periodo,
                                num_dias_pago=:num_dias_pago,
                                num_dias_feriados=:num_dias_feriados,
                                num_dias_inactivos=:num_dias_inactivos,
                                num_dias_descuento=:num_dias_descuento,
                                num_valor_pago=:num_valor_pago,
                                num_valor_descuento=:num_valor_descuento,
                                num_monto_descuento_otros=:num_monto_descuento_otros,
                                num_valor_total=:num_valor_total,
                                ind_estado=1,
                                fec_ultima_modificacion=NOW(),
                                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                        ");
                        #execute — Ejecuta una sentencia preparada
                        $NuevoRegistroDetalle->execute(array(
                            ':fk_a001_num_organismo'  => $organismo,
                            ':fk_rhc019_num_beneficio'  => $bonoAlimentacion,
                            ':fk_rhb001_num_empleado' => $lista_empleado[$i],
                            ':fec_anio'  => $fec_anio,
                            ':num_dia1'  => $_Dia[1],
                            ':num_dia2'  => $_Dia[2],
                            ':num_dia3'  => $_Dia[3],
                            ':num_dia4'  => $_Dia[4],
                            ':num_dia5'  => $_Dia[5],
                            ':num_dia6'  => $_Dia[6],
                            ':num_dia7'  => $_Dia[7],
                            ':num_dia8'  => $_Dia[8],
                            ':num_dia9'  => $_Dia[9],
                            ':num_dia10'  => $_Dia[10],
                            ':num_dia11'  => $_Dia[11],
                            ':num_dia12'  => $_Dia[12],
                            ':num_dia13'  => $_Dia[13],
                            ':num_dia14'  => $_Dia[14],
                            ':num_dia15'  => $_Dia[15],
                            ':num_dia16'  => $_Dia[16],
                            ':num_dia17'  => $_Dia[17],
                            ':num_dia18'  => $_Dia[18],
                            ':num_dia19'  => $_Dia[19],
                            ':num_dia20'  => $_Dia[20],
                            ':num_dia21'  => $_Dia[21],
                            ':num_dia22'  => $_Dia[22],
                            ':num_dia23'  => $_Dia[23],
                            ':num_dia24'  => $_Dia[24],
                            ':num_dia25'  => $_Dia[25],
                            ':num_dia26'  => $_Dia[26],
                            ':num_dia27'  => $_Dia[27],
                            ':num_dia28'  => $_Dia[28],
                            ':num_dia29'  => $_Dia[29],
                            ':num_dia30'  => $_Dia[30],
                            ':num_dia31'  => $_Dia[31],
                            ':num_dias_periodo' => $num_dias_periodo,
                            ':num_dias_pago' => $num_dias_pago,
                            ':num_dias_feriados'  => $num_total_feriados,
                            ':num_dias_inactivos'  => $_DiasInactivos,
                            ':num_dias_descuento'  => 0,
                            ':num_valor_pago'  => $_ValorPagar,
                            ':num_valor_descuento'  => 0,
                            ':num_monto_descuento_otros'  => str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $monto_descuento_otros[$i])),
                            ':num_valor_total'  => $_ValorPagar
                        ));

                        $errorRegBonoAlimentacionDet = $NuevoRegistroDetalle->errorInfo();



                    }//for externo

                    if(!empty($errorRegBonoAlimentacionDet[1]) && !empty($errorRegBonoAlimentacionDet[2])){
                        //en caso de error hago rollback
                        $this->_db->rollBack();
                        return $errorRegBonoAlimentacionDet;
                    }else{
                        $this->_db->commit();
                        return 1;
                    }

            }

        }//fin else


    }


    /*
     * PERMITE REGISTRAR LOS EVENTOS DEL EMPLEADO
     */
    public function metRegistrarEventosEmpleado($idBonoAlimentacion,$empleado,$datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #organizo los datos recibidos
        $fecha    = explode("-",$datos[0]);
        $h_salida = $datos[1];
        $h_entrada= $datos[2];
        $t_horas  = $datos[3];
        $mot_aus  = $datos[4];
        $tip_aus  = $datos[5];
        $obs      = $datos[6];
        $org      = $datos[7];
        $anio     = date("Y");



        #verifico que el tiempo del total de horas sea mayor al valor del parametro (UTDESC)
        #donde se establece el tiempo minimo necesario para tomar en cuenta el evento
        $parametro = explode(":",Session::metObtener('UTDESC'));
        $aux = explode(":",$t_horas);
        $_HorasPar = $parametro[0];
        $_MinutosPar = $parametro[1];
        $_Horas = $aux[0];
        $_Minutos = $aux[1];
        /*list($_HorasPar, $_MinutosPar) = $parametro;
        list($_Horas, $_Minutos) = $t_horas;*/

        if (($_Horas > $_HorasPar) || ($_Horas == $_HorasPar && $_Minutos >= $_MinutosPar)) {

            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "INSERT INTO
                    rh_c041_beneficio_alimentacion_eventos
                 SET
                    fk_a001_num_organismo=:fk_a001_num_organismo,
                    fk_rhc019_num_beneficio=:fk_rhc019_num_beneficio,
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                    fec_anio=:fec_anio,
                    fec_fecha=:fec_fecha,
                    fec_hora_salida=:fec_hora_salida,
                    fec_hora_entrada=:fec_hora_entrada,
                    fec_total_horas=:fec_total_horas,
                    fk_a006_num_miscelaneo_tipo_ausencia=:fk_a006_num_miscelaneo_tipo_ausencia,
                    fk_a006_num_miscelaneo_motivo_ausencia=:fk_a006_num_miscelaneo_motivo_ausencia,
                    txt_observaciones=:txt_observaciones,
                    num_flag_procesado=0,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a001_num_organismo'  => $org,
                ':fk_rhc019_num_beneficio'  => $idBonoAlimentacion,
                ':fk_rhb001_num_empleado'  => $empleado,
                ':fec_anio'  => $anio,
                ':fec_fecha' => $fecha[2]."-".$fecha[1]."-".$fecha[0],
                ':fec_hora_salida' => date("H:i",strtotime($h_salida)),
                ':fec_hora_entrada' => date("H:i",strtotime($h_entrada)),
                ':fec_total_horas' => $t_horas,
                ':fk_a006_num_miscelaneo_tipo_ausencia' => $tip_aus,
                ':fk_a006_num_miscelaneo_motivo_ausencia'  => $mot_aus,
                ':txt_observaciones'  => $obs
            ));

            $errorReg = $NuevoRegistro->errorInfo();

            $id = $this->_db->lastInsertId();

            if(!empty($errorReg[1]) && !empty($errorReg[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $errorReg;
            }else{
                $this->_db->commit();
                return $id;
            }


        }else{

            //no se puede guardar evento
            return array("El tiempo total para el dia \n".$datos[0]." es (".$t_horas.") \ny no puede ser menor a ".Session::metObtener('UTDESC')."");

        }

    }



    /*
     * PERMITE MODIFICAR LOS EVENTOS DEL EMPLEADO
     */
    public function metModificarEventosEmpleado($idBonoAlimentacion,$empleado,$datos,$idEvento)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #organizo los datos recibidos
        $fecha    = explode("-",$datos[0]);
        $h_salida = $datos[1];
        $h_entrada= $datos[2];
        $t_horas  = $datos[3];
        $mot_aus  = $datos[4];
        $tip_aus  = $datos[5];
        $obs      = $datos[6];
        $org      = $datos[7];
        $anio     = date("Y");


        #verifico que el tiempo del total de horas sea mayor al valor del parametro (UTDESC)
        #donde se establece el tiempo minimo necesario para tomar en cuenta el evento
        $parametro = explode(":",Session::metObtener('UTDESC'));
        $aux = explode(":",$t_horas);
        $_HorasPar = $parametro[0];
        $_MinutosPar = $parametro[1];
        $_Horas = $aux[0];
        $_Minutos = $aux[1];
        /*list($_HorasPar, $_MinutosPar) = $parametro;
        list($_Horas, $_Minutos) = $t_horas;*/

        if (($_Horas > $_HorasPar) || ($_Horas == $_HorasPar && $_Minutos >= $_MinutosPar)) {



           #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $NuevoRegistro = $this->_db->prepare(
                "UPDATE
                    rh_c041_beneficio_alimentacion_eventos
                 SET
                    fec_fecha=:fec_fecha,
                    fec_hora_salida=:fec_hora_salida,
                    fec_hora_entrada=:fec_hora_entrada,
                    fec_total_horas=:fec_total_horas,
                    fk_a006_num_miscelaneo_tipo_ausencia=:fk_a006_num_miscelaneo_tipo_ausencia,
                    fk_a006_num_miscelaneo_motivo_ausencia=:fk_a006_num_miscelaneo_motivo_ausencia,
                    txt_observaciones=:txt_observaciones,
                    num_flag_procesado=0,
                    fec_ultima_modificacion=NOW(),
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                 WHERE
                    fk_a001_num_organismo=:fk_a001_num_organismo AND
                    fk_rhc019_num_beneficio=:fk_rhc019_num_beneficio AND
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado AND
                    pk_num_beneficio_alimentacion_eventos='$idEvento'
                  ");

            #execute — Ejecuta una sentencia preparada
            $NuevoRegistro->execute(array(
                ':fk_a001_num_organismo'  => $org,
                ':fk_rhc019_num_beneficio'  => $idBonoAlimentacion,
                ':fk_rhb001_num_empleado'  => $empleado,
                ':fec_fecha' => $fecha[2]."-".$fecha[1]."-".$fecha[0],
                ':fec_hora_salida' => date("H:i",strtotime($h_salida)),
                ':fec_hora_entrada' => date("H:i",strtotime($h_entrada)),
                ':fec_total_horas' => $t_horas,
                ':fk_a006_num_miscelaneo_tipo_ausencia' => $tip_aus,
                ':fk_a006_num_miscelaneo_motivo_ausencia'  => $mot_aus,
                ':txt_observaciones'  => $obs
            ));

            $errorReg = $NuevoRegistro->errorInfo();

            if(!empty($errorReg[1]) && !empty($errorReg[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $errorReg;
            }else{
                $this->_db->commit();
                return $idEvento;
            }


        }else{

            //no se puede guardar evento
           return array("El tiempo total para el dia \n".$datos[0]." es (".$t_horas.") \ny no puede ser menor a ".Session::metObtener('UTDESC')."");

        }

    }


    #PERMITE PROCESAR LOS EVENTOS DEL EMPLEADO
    public function metProcesarEventosEmpleado($idBonoAlimentacion,$empleado)
    {

        $this->_db->beginTransaction();

        //verifico primero si hay eventos por procesar
        $con = $this->_db->query("
            SELECT COUNT(*) as eventos
            FROM rh_c041_beneficio_alimentacion_eventos
            WHERE fk_rhc019_num_beneficio='$idBonoAlimentacion' AND
            fk_rhb001_num_empleado='$empleado' AND
            num_flag_procesado=0
        ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $dat = $con->fetch();
        $numEventos = $dat['eventos'];

            if($numEventos > 0){//hay eventos que procesar

                //consulto todos los eventos para procesarlos
                $con1 = $this->_db->query("
                        SELECT
                        rh_c041_beneficio_alimentacion_eventos.*,
                        rh_c019_beneficio_alimentacion.fec_inicio_periodo,
                        rh_c019_beneficio_alimentacion.fec_fin_periodo
                        FROM
                        rh_c041_beneficio_alimentacion_eventos
                        INNER JOIN rh_c019_beneficio_alimentacion ON rh_c019_beneficio_alimentacion.pk_num_beneficio = rh_c041_beneficio_alimentacion_eventos.fk_rhc019_num_beneficio
                        WHERE fk_rhc019_num_beneficio='$idBonoAlimentacion' AND fk_rhb001_num_empleado='$empleado' and num_flag_procesado=0
                    ");

                 $con1->setFetchMode(PDO::FETCH_ASSOC);
                 $resultado = $con1->fetchAll();
/*
                //consulto los eventos procesados para acumular los dias de descuento
                $con2 = $this->_db->query("
                    SELECT COUNT(*) as eventos
                    FROM rh_c041_beneficio_alimentacion_eventos
                    WHERE fk_rhc019_num_beneficio='$idBonoAlimentacion' AND
                    fk_rhb001_num_empleado='$empleado' AND num_flag_procesado=1
                ");
                $con2->setFetchMode(PDO::FETCH_ASSOC);
                $dat2 = $con2->fetch();

                $dias_descuento = $dat2['eventos'];
                */

                $dias_descuento = count($resultado);

                foreach($resultado as $datos) {


                    #tranformo fecha en formato dd-mm-yyyyy
                    $f1 = explode("-", $datos['fec_inicio_periodo']);
                    $f2 = explode("-", $datos['fec_fecha']);
                    $f_ini_periodo = $f1[2] . "-" . $f1[1] . "-" . $f1[0];
                    $f_evento = $f2[2] . "-" . $f2[1] . "-" . $f2[0];


                    $dia = $this->metObtenerDiasFecha($f_ini_periodo, $f_evento) + 1;
                    $dia = (int)$f2[2];

                    $actualizarDetalle = $this->_db->prepare(
                    "UPDATE
                    rh_c039_beneficio_alimentacion_detalle
                    SET
                    num_dia$dia=:detalle
                    WHERE
                    fec_anio =:fec_anio AND
                    fk_a001_num_organismo =:fk_a001_num_organismo AND
                    fk_rhc019_num_beneficio = '$idBonoAlimentacion' AND
                    fk_rhb001_num_empleado ='$empleado'
                    
                    ");
                    $actualizarDetalle->execute(array(
                        ':detalle' => 'D',
                        ':fec_anio' => $datos['fec_anio'],
                        ':fk_a001_num_organismo' => $datos['fk_a001_num_organismo']
                    ));

                    $errorAct = $actualizarDetalle->errorInfo();

                    if (!empty($errorAct[1]) && !empty($errorAct[2])) {
                        //en caso de error hago rollback
                        $this->_db->rollBack();
                        return $errorAct;
                    } else {

                        //actualizo los descuento
                        $actualizarDesc = $this->_db->prepare(
                        "UPDATE
                        rh_c039_beneficio_alimentacion_detalle
                        SET
                        num_dias_descuento=:num_dias_descuento,
                        num_valor_descuento=((num_valor_pago / num_dias_pago) * " . intval($dias_descuento) . "),
                        num_valor_total=(num_valor_pago - ((num_valor_pago / num_dias_pago) * " . intval($dias_descuento) . "))
                        WHERE
                        fec_anio = :fec_anio AND
                        fk_a001_num_organismo = :fk_a001_num_organismo AND
                        fk_rhc019_num_beneficio = '$idBonoAlimentacion' AND
                        fk_rhb001_num_empleado ='$empleado'
                        ");
                        $actualizarDesc->execute(array(
                            ':num_dias_descuento' => $dias_descuento,
                            ':fec_anio' => $datos['fec_anio'],
                            ':fk_a001_num_organismo' => $datos['fk_a001_num_organismo']
                        ));

                        $errorDesc = $actualizarDesc->errorInfo();

                        if (!empty($errorAct[1]) && !empty($errorAct[2])) {
                            //en caso de error hago rollback
                            $this->_db->rollBack();
                            return $errorAct;
                        } else {

                        //actualizo evento y lo pongo como procesado
                            $actualizarEve = $this->_db->prepare(
                            "UPDATE
                            rh_c041_beneficio_alimentacion_eventos
                            SET
                            num_flag_procesado=1
                            WHERE
                            fec_anio = :fec_anio AND
                            fk_a001_num_organismo = :fk_a001_num_organismo AND
                            fk_rhc019_num_beneficio = '$idBonoAlimentacion' AND
                            fk_rhb001_num_empleado ='$empleado'
                            ");
                            $actualizarEve->execute(array(
                                ':fec_anio' => $datos['fec_anio'],
                                ':fk_a001_num_organismo' => $datos['fk_a001_num_organismo']
                            ));
                            $errorEve = $actualizarEve->errorInfo();
                            if (!empty($errorEve[1]) && !empty($errorEve[2])) {
                                //en caso de error hago rollback
                                $this->_db->rollBack();
                                return $errorEve;
                            }

                        }

                    }

                }
                $this->_db->commit();
                return 1;

            }else{

                return 0;

            }



    }


    #PERMITE ELIMINAR UN EVENTO
    public function metEliminarEventoEmpleado($idEvento)
    {

        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare("
                DELETE FROM rh_c041_beneficio_alimentacion_eventos WHERE pk_num_beneficio_alimentacion_eventos=:pk_num_beneficio_alimentacion_eventos
            ");
        $eliminar->execute(array(
            'pk_num_beneficio_alimentacion_eventos'=>$idEvento
        ));
        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idEvento;
        }

    }



    #PERMITE LISTAR LOS VENTOS DEL EMPLEADO, DE ACUERDO AL PERIODO DE BONO DE ALIMENTACION
    public function metListarEventosEmpleado($bonoAlimentacion,$empleado,$evento,$operacion)
    {


        if(strcmp($operacion,'TODOS')==0){

            $lista = $this->_db->query("
                SELECT
                rh_c041_beneficio_alimentacion_eventos.pk_num_beneficio_alimentacion_eventos,
                rh_c041_beneficio_alimentacion_eventos.fk_rhb001_num_empleado,
                rh_c041_beneficio_alimentacion_eventos.fk_rhc019_num_beneficio,
                a003_persona.pk_num_persona,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                rh_c041_beneficio_alimentacion_eventos.fec_anio,
                rh_c041_beneficio_alimentacion_eventos.fec_fecha,
                rh_c041_beneficio_alimentacion_eventos.fec_hora_salida,
                rh_c041_beneficio_alimentacion_eventos.fec_hora_entrada,
                rh_c041_beneficio_alimentacion_eventos.fec_total_horas,
                rh_c041_beneficio_alimentacion_eventos.fk_a006_num_miscelaneo_motivo_ausencia,
                motivo_ausencia.ind_nombre_detalle AS mot_ausencia,
                rh_c041_beneficio_alimentacion_eventos.fk_a006_num_miscelaneo_tipo_ausencia,
                tipo_ausencia.ind_nombre_detalle AS tip_ausencia,
                rh_c041_beneficio_alimentacion_eventos.txt_observaciones,
                rh_c041_beneficio_alimentacion_eventos.num_flag_procesado,
                rh_c041_beneficio_alimentacion_eventos.fec_ultima_modificacion,
                rh_c041_beneficio_alimentacion_eventos.fk_a018_num_seguridad_usuario

                FROM
                rh_c041_beneficio_alimentacion_eventos
                INNER JOIN rh_b001_empleado ON rh_c041_beneficio_alimentacion_eventos.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS motivo_ausencia ON rh_c041_beneficio_alimentacion_eventos.fk_a006_num_miscelaneo_motivo_ausencia = motivo_ausencia.pk_num_miscelaneo_detalle
                INNER JOIN a006_miscelaneo_detalle AS tipo_ausencia ON rh_c041_beneficio_alimentacion_eventos.fk_a006_num_miscelaneo_tipo_ausencia = tipo_ausencia.pk_num_miscelaneo_detalle
                WHERE rh_c041_beneficio_alimentacion_eventos.fk_rhc019_num_beneficio='$bonoAlimentacion'
                AND rh_c041_beneficio_alimentacion_eventos.fk_rhb001_num_empleado='$empleado'
         ");

            $lista->setFetchMode(PDO::FETCH_ASSOC);
            return $lista->fetchAll();

        }
        if(strcmp($operacion,'ID')==0){

            $lista = $this->_db->query("
                SELECT
                rh_c041_beneficio_alimentacion_eventos.pk_num_beneficio_alimentacion_eventos,
                rh_c041_beneficio_alimentacion_eventos.fk_rhb001_num_empleado,
                rh_c041_beneficio_alimentacion_eventos.fk_rhc019_num_beneficio,
                a003_persona.pk_num_persona,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_nombre2,' ',a003_persona.ind_apellido1,' ',a003_persona.ind_apellido2) AS nombre_completo,
                rh_c041_beneficio_alimentacion_eventos.fec_anio,
                rh_c041_beneficio_alimentacion_eventos.fec_fecha,
                rh_c041_beneficio_alimentacion_eventos.fec_hora_salida,
                rh_c041_beneficio_alimentacion_eventos.fec_hora_entrada,
                rh_c041_beneficio_alimentacion_eventos.fec_total_horas,
                rh_c041_beneficio_alimentacion_eventos.fk_a006_num_miscelaneo_motivo_ausencia,
                motivo_ausencia.ind_nombre_detalle AS mot_ausencia,
                rh_c041_beneficio_alimentacion_eventos.fk_a006_num_miscelaneo_tipo_ausencia,
                tipo_ausencia.ind_nombre_detalle AS tip_ausencia,
                rh_c041_beneficio_alimentacion_eventos.txt_observaciones,
                rh_c041_beneficio_alimentacion_eventos.num_flag_procesado,
                rh_c041_beneficio_alimentacion_eventos.fec_ultima_modificacion,
                rh_c041_beneficio_alimentacion_eventos.fk_a018_num_seguridad_usuario

                FROM
                rh_c041_beneficio_alimentacion_eventos
                INNER JOIN rh_b001_empleado ON rh_c041_beneficio_alimentacion_eventos.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS motivo_ausencia ON rh_c041_beneficio_alimentacion_eventos.fk_a006_num_miscelaneo_motivo_ausencia = motivo_ausencia.pk_num_miscelaneo_detalle
                INNER JOIN a006_miscelaneo_detalle AS tipo_ausencia ON rh_c041_beneficio_alimentacion_eventos.fk_a006_num_miscelaneo_tipo_ausencia = tipo_ausencia.pk_num_miscelaneo_detalle
                WHERE rh_c041_beneficio_alimentacion_eventos.fk_rhc019_num_beneficio='$bonoAlimentacion'
                AND rh_c041_beneficio_alimentacion_eventos.fk_rhb001_num_empleado='$empleado'
                AND rh_c041_beneficio_alimentacion_eventos.pk_num_beneficio_alimentacion_eventos='$evento'
         ");

            $lista->setFetchMode(PDO::FETCH_ASSOC);
            return $lista->fetch();

        }



    }



    #PERMITE LISTAR LOS EMPLEADOS DE ACUERDO  A SU ORGANISMO, DEP, NOMINA
    public function metListarEmpleados($opc, $organismo, $dependencia,$nomina)
    {
        if($opc == 1){//filtro por organismo
            $empleado = $this->_db->query("SELECT
                         rh_b001_empleado.pk_num_empleado,
                         a003_persona.ind_nombre1,
                         a003_persona.ind_nombre2,
                         a003_persona.ind_apellido1,
                         a003_persona.ind_apellido2,
                         a003_persona.ind_cedula_documento,
                         rh_c005_empleado_laboral.fec_ingreso,
                         rh_c006_tipo_cargo.txt_descripcion,
                         rh_c009_nivel.num_nivel,
                         rh_c007_grado_salarial.ind_grado,
                        a001_organismo.pk_num_organismo,
                        a001_organismo.ind_descripcion_empresa,
                        a004_dependencia.pk_num_dependencia,
                        a004_dependencia.ind_dependencia,
                        rh_c063_puestos.ind_descripcion_cargo
                        FROM a003_persona
                        INNER JOIN  rh_b001_empleado ON  rh_b001_empleado.fk_a003_num_persona =  a003_persona.pk_num_persona
                        INNER JOIN  rh_c005_empleado_laboral ON   rh_c005_empleado_laboral.fk_rhb001_num_empleado  = rh_b001_empleado.pk_num_empleado
                        INNER JOIN  rh_c063_puestos ON  rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                        INNER JOIN  rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo
                        INNER JOIN  rh_c009_nivel ON rh_c063_puestos.fk_rhc009_num_nivel = rh_c009_nivel.pk_num_nivel
                        INNER JOIN  rh_c007_grado_salarial ON rh_c063_puestos.fk_rhc007_num_grado = rh_c007_grado_salarial.pk_num_grado
                        INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                        INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = rh_c076_empleado_organizacion.fk_a001_num_organismo
                        INNER JOIN  a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
                        WHERE a001_organismo.pk_num_organismo = '$organismo' AND rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina='$nomina'");

        }else{
            $empleado = $this->_db->query("SELECT
                         rh_b001_empleado.pk_num_empleado,
                         a003_persona.ind_nombre1,
                         a003_persona.ind_nombre2,
                         a003_persona.ind_apellido1,
                         a003_persona.ind_apellido2,
                         a003_persona.ind_cedula_documento,
                         rh_c005_empleado_laboral.fec_ingreso,
                         rh_c006_tipo_cargo.txt_descripcion,
                         rh_c009_nivel.num_nivel,
                         rh_c007_grado_salarial.ind_grado,
                        a001_organismo.pk_num_organismo,
                        a001_organismo.ind_descripcion_empresa,
                        a004_dependencia.pk_num_dependencia,
                        a004_dependencia.ind_dependencia,
                        rh_c063_puestos.ind_descripcion_cargo
                        FROM a003_persona
                        INNER JOIN  rh_b001_empleado ON  rh_b001_empleado.fk_a003_num_persona =  a003_persona.pk_num_persona
                        INNER JOIN  rh_c005_empleado_laboral ON   rh_c005_empleado_laboral.fk_rhb001_num_empleado  = rh_b001_empleado.pk_num_empleado
                        INNER JOIN  rh_c063_puestos ON  rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
                        INNER JOIN  rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo
                        INNER JOIN  rh_c009_nivel ON rh_c063_puestos.fk_rhc009_num_nivel = rh_c009_nivel.pk_num_nivel
                        INNER JOIN  rh_c007_grado_salarial ON rh_c063_puestos.fk_rhc007_num_grado = rh_c007_grado_salarial.pk_num_grado
                        INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                        INNER JOIN a001_organismo ON a001_organismo.pk_num_organismo = rh_c076_empleado_organizacion.fk_a001_num_organismo
                        INNER JOIN  a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
                        WHERE a001_organismo.pk_num_organismo = '$organismo'
                        AND a004_dependencia.pk_num_dependencia = '$dependencia' AND rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina='$nomina'");
        }

        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }


    #PERMITE CERRAR UN PERIODO BONO ALIMENTACION
    public function metCerrarPeriodo($bonoAlimentacion)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $cerrarPeriodo = $this->_db->prepare(
            "UPDATE
                rh_c019_beneficio_alimentacion
             SET
                ind_estado=:num_estado,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE  pk_num_beneficio='$bonoAlimentacion'
              ");

        #execute — Ejecuta una sentencia preparada
        $cerrarPeriodo->execute(array(
            ':num_estado'  => 0,
        ));

        $errorCerrarPeriodo = $cerrarPeriodo->errorInfo();

        if(!empty($errorCerrarPeriodo[1]) && !empty($errorCerrarPeriodo[2])){
            //en caso de error hago rollback
            $this->_db->rollBack();
            return $errorCerrarPeriodo;
        }else{

            #actualizo detalle
            #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
            $cerrarPeriodoDetalle = $this->_db->prepare(
                "UPDATE
                rh_c039_beneficio_alimentacion_detalle
             SET
                ind_estado=:num_estado,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE  fk_rhc019_num_beneficio='$bonoAlimentacion'
              ");

            #execute — Ejecuta una sentencia preparada
            $cerrarPeriodoDetalle->execute(array(
                ':num_estado'  => 0,
            ));

            $errorCerrarPeriodoDetalle = $cerrarPeriodoDetalle->errorInfo();

            if(!empty($errorCerrarPeriodoDetalle[1]) && !empty($errorCerrarPeriodoDetalle[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $errorCerrarPeriodoDetalle;
            }else{

                $this->_db->commit();
                return $bonoAlimentacion;

            }

        }

    }



    #permite obtener el detalle de los eventos del empleado de un periodo especifico
    public function metVerDetalleEventos($empleado,$bono_alimentacion,$organismo)
    {

        $detalle = $this->_db->query("
                   SELECT
                        *
                   FROM
                        rh_c039_beneficio_alimentacion_detalle
                   WHERE
                        fk_a001_num_organismo = '$organismo' AND
                        fk_rhc019_num_beneficio = '$bono_alimentacion' AND
                        fk_rhb001_num_empleado = '$empleado'
        ");

        $detalle->setFetchMode(PDO::FETCH_ASSOC);
        return $detalle->fetch();

    }



    #PERMITE OBTENER EL DIA DE LA SEMANA DE UNA FECHA
    public function metObtenerDiaSemana($fecha)
    {

        // primero creo un array para saber los días de la semana
        $dias = array(0, 1, 2, 3, 4, 5, 6);
        $dia = substr($fecha, 0, 2);
        $mes = substr($fecha, 3, 2);
        $anio = substr($fecha, 6, 4);
        // en la siguiente instrucción $pru toma el día de la semana, lunes, martes,
        $dato = strtoupper($dias[intval((date("w",mktime(0,0,0,$mes,$dia,$anio))))]);
        return $dato;

    }

    #PERMITE OBTENER DIAS HABILES
    public function metObtenerDiasHabiles($fecha_ini,$fecha_fin)
    {

        $dias_completos = $this->metObtenerDiasFecha($fecha_ini,$fecha_fin);

        $dias_feriados = $this->metObtenerDiasFeriados($fecha_ini,$fecha_fin);

        $dia_semana = $this->metObtenerDiaSemana($fecha_ini);

        $dias_habiles = 0;

        for ($i=0; $i<=$dias_completos; $i++) {

            if ($dia_semana >= 1 && $dia_semana <= 5) $dias_habiles++;

            $dia_semana++;

            if ($dia_semana == 7) $dia_semana = 0;

        }

        $dias_habiles -= $dias_feriados;

        return $dias_habiles;

    }

    #diferencia dias entre dos fechas
    public function metObtenerDiasFecha($fecha_ini,$fecha_fin)
    {

        list($dd, $md, $ad) = explode( '-', $fecha_ini);	$desde = "$ad-$md-$dd";

        list($dh, $mh, $ah) = explode( '-', $fecha_fin);	$hasta = "$ah-$mh-$dh";

        #diferencia de dias entre las fechas
        $con = $this->_db->query("SELECT DATEDIFF('$hasta', '$desde') as valor");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        $f = $con->fetch();

        return $f['valor'];

    }

    #PERMITE OBTENER DIAS FERIADOS SEGUN RANGO DE FECHAS
    public function metObtenerDiasFeriados($fecha_ini,$fecha_fin)
    {

        list($dia_desde, $mes_desde, $anio_desde) = explode('-', $fecha_ini); $DiaDesde = "$mes_desde-$dia_desde";
        list($dia_hasta, $mes_hasta, $anio_hasta) = explode('-', $fecha_fin); $DiaHasta = "$mes_hasta-$dia_hasta";

        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    rh_c069_feriados
                WHERE
                    (num_flag_variable = 1 AND
				    (fec_anio = '$anio_desde' OR fec_anio = '$anio_hasta') AND
				    (fec_dia >= '$DiaDesde' AND fec_dia <= '$DiaHasta')) OR
				    (num_flag_variable = 0 AND fec_dia >= '$DiaDesde' AND fec_dia <= '$DiaHasta')
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $data = $con->fetchAll();

        $dias_feriados=0;

        for($i=0;$i<count($data);$i++){

            list($mes, $dia) = explode('-', $data[$i]['fec_dia']);

            if ($data[$i]['fec_anio'] == "") $anio = $anio_desde; else $anio = $data[$i]['fec_anio'];

            $fecha = "$dia-$mes-$anio";

            $dia_semana = $this->metObtenerDiaSemana($fecha);

            if ($dia_semana >= 1 && $dia_semana <= 5) $dias_feriados++;

            if ($anio_desde != $anio_hasta) {

                if ($data[$i]['fec_anio'] == "") $anio = $anio_hasta; else $anio = $data[$i]['fec_anio'];

                $fecha = "$dia-$mes-$anio";

                $dia_semana = $this->metObtenerDiaSemana($fecha);

                if ($dia_semana >= 1 && $dia_semana <= 5) $dias_feriados++;

            }

        }

        return $dias_feriados;

    }



    #PERMITE CLACULAR DIFERENCIA ENTRE DOS HORAS
    public function metDiferenciaHoras($empleado,$fecha,$hora_salida,$hora_entrada)
    {

        #transformo las horas a hora militar
        if(!empty($hora_salida)){
            $hs = strtotime($hora_salida);
            $hora_salida = date("H:i:s",$hs);
        }else{
            $hora_salida = '';
        }

        if(!empty($hora_entrada)){
            $he = strtotime($hora_entrada);
            $hora_entrada = date("H:i:s",$he);
        }else{
            $hora_entrada = '';
        }


        #busco el horario que tiene asignado el empleado
        $con1 = $this->_db->query("
                SELECT
                    fk_rhb007_num_horario
                FROM
                    rh_c005_empleado_laboral
                WHERE
                    fk_rhb001_num_empleado='$empleado'
               ");

        $con1->setFetchMode(PDO::FETCH_ASSOC);
        $data1 = $con1->fetch();
        $horario = $data1['fk_rhb007_num_horario'];

        #formato de fecha AMD - anio - mes -dia
        $f = explode("-",$fecha);
        $fecha = $f[2]."-".$f[1]."-".$f[0];

        #obtengo el dia de la semana para la fecha
        $dia = date("N", strtotime("".$fecha.""));

        if($horario != ''){

            #obtengo el detalle del horario
            $con2 = $this->_db->query("
                SELECT
                    *
                FROM
                    rh_c067_horario_detalle
                WHERE
                    fk_rhb007_num_horario='$horario' AND
                    num_dia='$dia' AND
                    num_flag_laborable=1
               ");

            $con2->setFetchMode(PDO::FETCH_ASSOC);
            $data2 = $con2->fetch();


            //	si tiene salida y no entrada
            if($hora_salida && !$hora_entrada){

                //	si la salida es en el primer turno
                if ($hora_salida >= $data2['fec_entrada1'] && $hora_salida < $data2['fec_salida1']) {
                    $turno1 = $this->metDiffHora($hora_salida, $data2['fec_salida1']);
                    $turno2 = $this->metDiffHora($data2['fec_entrada2'], $data2['fec_salida2']);
                }
                //	si la salida es en el segundo turno
                elseif ($hora_salida >= $data2['fec_entrada2'] && $hora_salida < $data2['fec_salida2']) {
                    $turno1 = "00:00:00";
                    $turno2 = $this->metDiffHora($hora_salida, $data2['fec_salida2']);

                }


            }


            //	si tiene salida y entrada
            elseif ($hora_salida && $hora_entrada) {
                //	si la salida y la entrada estan en el mismo turno
                if (($hora_salida >= $data2['fec_entrada1'] && $hora_salida < $data2['fec_salida1'] && $hora_entrada >= $data2['fec_entrada1'] && $hora_entrada <= $data2['fec_salida1']) || ($hora_salida >= $data2['fec_entrada2'] && $hora_salida < $data2['fec_salida2'] && $hora_entrada >= $data2['fec_entrada2'] && $hora_entrada <= $data2['fec_salida2'])) {
                    $turno1 = $this->metDiffHora($hora_salida, $hora_entrada);
                    $turno2 = "00:00:00";
                }
                //	si la salida es en el turno de la mañana y la entrada en el turno de la tarde
                elseif ($hora_salida >= $data2['fec_entrada1'] && $hora_salida < $data2['fec_salida1'] && $hora_entrada > $data2['fec_entrada2'] && $hora_entrada <= $data2['fec_salida2']) {
                    $turno1 = $this->metDiffHora($hora_salida, $data2['fec_salida1']);
                    $turno2 = $this->metDiffHora($data2['fec_entrada2'], $hora_entrada);
                }


            }

            //	si no tiene salida y si entrada
            elseif (!$hora_salida && $hora_entrada) {
                //	si la salida es en el primer turno
                if ($hora_entrada > $data2['fec_entrada1'] && $hora_entrada < $data2['fec_salida1']) {
                    $turno1 = $this->metDiffHora($data2['fec_entrada1'], $hora_entrada);
                    $turno2 = "00:00:00";
                }
                //	si la salida es en el segundo turno
                elseif ($hora_entrada > $data2['fec_entrada2'] && $hora_entrada < $data2['fec_salida2']) {
                    $turno1 = "00:00:00";
                    $turno2 = $this->metDiffHora($data2['fec_entrada2'], $hora_entrada);
                }


            }

            list($h1, $m1) = explode(":", $turno1);
            list($h2, $m2) = explode(":", $turno2);
            $totalh = intval($h1) + intval($h2);
            $totalm = intval($m1) + intval($m2);
            if ($totalm >= 60) {
                $hsumar = intval($totalm / 60);
                $totalh += $hsumar;
                $totalm = $totalm - (60 * $hsumar);
            }

            $total = $totalh.":".$totalm;

            return $total;



        }else{

            return $this->metDiffHora($hora_salida,$hora_entrada);

        }


    }


    public function metDiffHora($Desde, $Hasta) {

        $con = $this->_db->query("
                SELECT TIMEDIFF('$Hasta', '$Desde') AS TotalHoras
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $data = $con->fetch();

        return substr($data['TotalHoras'], 0, 6);

    }


    public function metListarPeriodosDisponibles($Organismo,$idNomina)
    {
        $periodosNomina = $this->_db->query("
            SELECT
              pk_num_beneficio,
              fec_anio,
              ind_periodo
            FROM
              rh_c019_beneficio_alimentacion
            WHERE
              fk_a001_num_organismo='$Organismo' and fk_nmb001_num_tipo_nomina='$idNomina'
        ");
        $periodosNomina->setFetchMode(PDO::FETCH_ASSOC);
        return $periodosNomina->fetchAll();
    }

    public function metListarProcesosDisponibles($Organismo,$Periodo,$idNomina)
    {
        $procesos = $this->_db->query("
            SELECT
              pk_num_beneficio,
              fec_anio,
              ind_periodo,
              DATE_FORMAT(fec_inicio_periodo,'%d-%m-%Y') as fec_inicio_periodo,
			  DATE_FORMAT(fec_fin_periodo,'%d-%m-%Y') as fec_fin_periodo
            FROM
              rh_c019_beneficio_alimentacion
            WHERE
              fk_a001_num_organismo='$Organismo' and fk_nmb001_num_tipo_nomina='$idNomina' and ind_periodo='$Periodo' and ind_estado=1
        ");
        $procesos->setFetchMode(PDO::FETCH_ASSOC);
        return $procesos->fetchAll();
    }

    public function metCalculoBonoAlimentacion($idTipoNomina,$periodo)
    {
        $this->_db->beginTransaction();

        $verificar = $this->_db->prepare("
            SELECT * FROM rh_c106_bono_alimentacion_calculo WHERE ind_periodo='$periodo' and fk_nmb001_num_tipo_nomina='$idTipoNomina'
        ");
        $verificar->execute();
        $totalRegistros = $verificar->rowCount();

        if($totalRegistros > 0){

            $consulta = $this->_db->query("
                SELECT
                    rh_c106_bono_alimentacion_calculo.pk_num_bono_alimentacion_calculo,
                    rh_c106_bono_alimentacion_calculo.fec_anio,
                    rh_c106_bono_alimentacion_calculo.ind_periodo,
                    rh_c106_bono_alimentacion_calculo.fk_rhc019_num_beneficio,
                    rh_c106_bono_alimentacion_calculo.fk_nmb001_num_tipo_nomina,
                    nm_b001_tipo_nomina.cod_tipo_nomina,
                    nm_b001_tipo_nomina.ind_nombre_nomina,
                    rh_c106_bono_alimentacion_calculo.fk_rhb001_num_empleado,
                    rh_b001_empleado.cod_empleado,
                    a003_persona.ind_cedula_documento,
                    CONCAT_WS(
                        ' ',
                        a003_persona.ind_nombre1,
                        a003_persona.ind_nombre2,
                        a003_persona.ind_apellido1,
                        a003_persona.ind_apellido2
                    ) AS empleado,
                    rh_c106_bono_alimentacion_calculo.num_monto_pagar,
                    rh_c106_bono_alimentacion_calculo.num_flag_verificado,
                    rh_c106_bono_alimentacion_calculo.num_flag_consolidado
                FROM
                    rh_c106_bono_alimentacion_calculo
                INNER JOIN rh_c019_beneficio_alimentacion ON rh_c019_beneficio_alimentacion.pk_num_beneficio = rh_c106_bono_alimentacion_calculo.fk_rhc019_num_beneficio
                INNER JOIN nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = rh_c106_bono_alimentacion_calculo.fk_nmb001_num_tipo_nomina
                INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c106_bono_alimentacion_calculo.fk_rhb001_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE
                    rh_c106_bono_alimentacion_calculo.fk_nmb001_num_tipo_nomina = $idTipoNomina
                AND rh_c106_bono_alimentacion_calculo.ind_periodo = '$periodo'                
    
                ");
            $consulta->setFetchMode(PDO::FETCH_ASSOC);
            return $consulta->fetchAll();

        }else{

            $consulta = $this->_db->query("
                SELECT
                    rh_c019_beneficio_alimentacion.pk_num_beneficio,
                    rh_c019_beneficio_alimentacion.fk_a001_num_organismo,
                    rh_c019_beneficio_alimentacion.fec_anio,
                    rh_c019_beneficio_alimentacion.ind_periodo,
                    rh_c019_beneficio_alimentacion.ind_descripcion,
                    rh_b001_empleado.pk_num_empleado,
                    a003_persona.ind_cedula_documento,
                    CONCAT_WS(
                        ' ',
                        a003_persona.ind_nombre1,
                        a003_persona.ind_nombre2,
                        a003_persona.ind_apellido1,
                        a003_persona.ind_apellido2
                    ) AS empleado,
                    rh_c039_beneficio_alimentacion_detalle.num_valor_total,
                    rh_c019_beneficio_alimentacion.fk_nmb001_num_tipo_nomina,
                    nm_b001_tipo_nomina.ind_nombre_nomina
                FROM
                    rh_c019_beneficio_alimentacion
                INNER JOIN rh_c039_beneficio_alimentacion_detalle ON rh_c039_beneficio_alimentacion_detalle.fk_rhc019_num_beneficio = rh_c019_beneficio_alimentacion.pk_num_beneficio
                INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c039_beneficio_alimentacion_detalle.fk_rhb001_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                INNER JOIN nm_b001_tipo_nomina ON rh_c019_beneficio_alimentacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
                WHERE
                    rh_c019_beneficio_alimentacion.fk_nmb001_num_tipo_nomina = $idTipoNomina
                AND rh_c019_beneficio_alimentacion.ind_periodo = '$periodo'
                AND rh_c019_beneficio_alimentacion.ind_estado = 1
    
            ");
            $consulta->setFetchMode(PDO::FETCH_ASSOC);
            $resultado = $consulta->fetchAll();
            $Registro = $this->_db->prepare(
                "INSERT INTO
                    rh_c106_bono_alimentacion_calculo
                 SET
                    fec_anio=:fec_anio,
                    ind_periodo=:ind_periodo,
                    fk_rhc019_num_beneficio=:fk_rhc019_num_beneficio,
                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                    num_monto_pagar=:num_monto_pagar,
                    num_flag_verificado=0,
                    num_flag_consolidado=0
                  ");

            foreach ($resultado as $reg)
            {
                #execute — Ejecuta una sentencia preparada
                $Registro->execute(array(
                    ':fec_anio'  => $reg['fec_anio'],
                    ':ind_periodo'  => $reg['ind_periodo'],
                    ':fk_rhc019_num_beneficio'  => $reg['pk_num_beneficio'],
                    ':fk_rhb001_num_empleado'  => $reg['pk_num_empleado'],
                    ':fk_nmb001_num_tipo_nomina' => $reg['fk_nmb001_num_tipo_nomina'],
                    ':num_monto_pagar'  => $reg['num_valor_total']
                ));

            }

            $error = $Registro->errorInfo();


            if(!empty($error[1]) && !empty($error[2])){
                //en caso de error hago rollback
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                $consulta = $this->_db->query("
                SELECT
                    rh_c106_bono_alimentacion_calculo.pk_num_bono_alimentacion_calculo,
                    rh_c106_bono_alimentacion_calculo.fec_anio,
                    rh_c106_bono_alimentacion_calculo.ind_periodo,
                    rh_c106_bono_alimentacion_calculo.fk_rhc019_num_beneficio,
                    rh_c106_bono_alimentacion_calculo.fk_nmb001_num_tipo_nomina,
                    nm_b001_tipo_nomina.cod_tipo_nomina,
                    nm_b001_tipo_nomina.ind_nombre_nomina,
                    rh_c106_bono_alimentacion_calculo.fk_rhb001_num_empleado,
                    rh_b001_empleado.cod_empleado,
                    a003_persona.ind_cedula_documento,
                    CONCAT_WS(
                        ' ',
                        a003_persona.ind_nombre1,
                        a003_persona.ind_nombre2,
                        a003_persona.ind_apellido1,
                        a003_persona.ind_apellido2
                    ) AS empleado,
                    rh_c106_bono_alimentacion_calculo.num_monto_pagar,
                    rh_c106_bono_alimentacion_calculo.num_flag_verificado,
                    rh_c106_bono_alimentacion_calculo.num_flag_consolidado
                FROM
                    rh_c106_bono_alimentacion_calculo
                INNER JOIN rh_c019_beneficio_alimentacion ON rh_c019_beneficio_alimentacion.pk_num_beneficio = rh_c106_bono_alimentacion_calculo.fk_rhc019_num_beneficio
                INNER JOIN nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = rh_c106_bono_alimentacion_calculo.fk_nmb001_num_tipo_nomina
                INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c106_bono_alimentacion_calculo.fk_rhb001_num_empleado
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                WHERE
                    rh_c106_bono_alimentacion_calculo.fk_nmb001_num_tipo_nomina = $idTipoNomina
                AND rh_c106_bono_alimentacion_calculo.ind_periodo = '$periodo'                
    
                ");
                $consulta->setFetchMode(PDO::FETCH_ASSOC);
                return $consulta->fetchAll();
            }


        }//fin else registros








    }



}//fin clase
