<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Gestión de Jubilación. Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase contiene las consultas necesarias para gestionar las solicitud de jubilación por parte de los funcionarios
require_once RUTA_MODELO.'miscelaneoModelo.php';
class jubilacionModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();
    }

    // Método que permite obtener la dependencia, fecha de ingreso y el organismo al que pertenece el empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado =  $this->_db->query(
            "select a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, a.fk_a006_num_miscelaneo_detalle_sexo, b.num_sueldo_basico, a.fec_nacimiento, date_format(a.fec_nacimiento, '%d/%m/%Y') as fecha_nacimiento, date_format(b.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, b.fec_ingreso, d.ind_nombre_detalle, e.pk_num_puestos, e.ind_descripcion_cargo, f.ind_descripcion_empresa, g.ind_dependencia, g.pk_num_dependencia, f.pk_num_organismo, b.fk_rhc063_num_puestos_cargo from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, a005_miscelaneo_maestro as c, a006_miscelaneo_detalle as d, rh_c063_puestos as e, a001_organismo as f, a004_dependencia as g where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and c.cod_maestro='SEXO' and c.pk_num_miscelaneo_maestro=d.fk_a005_num_miscelaneo_maestro and a.fk_a006_num_miscelaneo_detalle_sexo=d.pk_num_miscelaneo_detalle and b.fk_rhc063_num_puestos_cargo=e.pk_num_puestos and b.fk_a004_num_dependencia=g.pk_num_dependencia and g.fk_a001_num_organismo=f.pk_num_organismo and b.fk_a001_num_organismo=f.pk_num_organismo and a.ind_estatus_persona=1"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    // Método encargado de obtener el empleado asociado a una cuenta de ususario
    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar lo empleados asociados a una dependencia
    public function metDependenciaEmpleado($pkNumDependencia)
    {
        $empleado =  $this->_db->query(
            "select a.pk_num_empleado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2 from rh_b001_empleado as a, a003_persona as b, rh_c076_empleado_organizacion as c where a.fk_a003_num_persona=b.pk_num_persona and c.fk_a004_num_dependencia=$pkNumDependencia and c.fk_rhb001_num_empleado=a.pk_num_empleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where fk_a001_num_organismo=$pkNumOrganismo"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select pk_num_dependencia, ind_dependencia from a004_dependencia where pk_num_dependencia=$pk_num_dependencia"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }

    // Método que permite listar empleados
    public function metListarEmpleado($pkNumDependencia, $metodo)
    {
        if($metodo==1){
            $empleado =  $this->_db->query(
                "select a.pk_num_empleado, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from vl_rh_persona_empleado as a, rh_b001_empleado as b, a003_persona as c, vl_rh_persona_empleado_datos_laborales as d where a.pk_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and d.pk_num_empleado=a.pk_num_empleado and d.fk_a004_num_dependencia=$pkNumDependencia and a.ind_estatus_empleado=1"
            );
        } else {
            $empleado =  $this->_db->query(
                "select a.pk_num_empleado, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from vl_rh_persona_empleado as a, rh_b001_empleado as b, a003_persona as c, vl_rh_persona_empleado_datos_laborales as d where a.pk_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and and d.pk_num_empleado=a.pk_num_empleado and a.ind_estatus_empleado=1"
            );
        }
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }
    

    // Método que permite listar los centros de costos asociado a una dependencia
    public function metListarCentroCosto($pkNumDependencia) 
    {
        $centroCosto =  $this->_db->query(
            "select pk_num_centro_costo, ind_descripcion_centro_costo from a023_centro_costo where fk_a004_num_dependencia=$pkNumDependencia"
        );
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }

    // Método que permite listar los tipos de nóminas
    public function metListarNomina()
    {
        $nomina =  $this->_db->query(
            "select pk_num_tipo_nomina, ind_nombre_nomina from nm_b001_tipo_nomina"
        );
        $nomina->setFetchMode(PDO::FETCH_ASSOC);
        return $nomina->fetchAll();
    }

    // Método que permite  listar a los empleados de acuerdo al buscador de trabajador al momento de registrar una nueva jubilación
    public function metBuscarTrabajador($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $tipoTrabajador, $estado_registro, $situacionTabajo, $fecha_inicio, $fecha_fin)
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and b.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and b.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if($situacionTabajo!=''){
            $filtro .= " and a.ind_estatus_empleado=$situacionTabajo";
        }
        if($centroCosto!=''){
            $filtro .= " and b.fk_a023_num_centro_costo=$centroCosto";
        }
        if($tipoNomina!=''){
            $filtro .= " and b.fk_nmb001_num_tipo_nomina=$tipoNomina";
        }
        if($tipoTrabajador!=''){
            $filtro .= " and b.fk_a006_num_miscelaneo_detalle_tipotrabajador=$tipoTrabajador";
        }
        if(($fecha_inicio!='')&&($fecha_fin!='')){
            $explodeInicio = explode("/", $fecha_inicio);
            $fechaInicio = $explodeInicio[2].'-'.$explodeInicio[1].'-'.$explodeInicio[0];
            $explodeFin = explode("/", $fecha_fin);
            $fechaFin = $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];
            $filtro .= " and b.fec_ingreso between '$fechaInicio' and '$fechaFin' ";
        }
        $buscarTrabajador = $this->_db->query(
            "select a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2 from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b where a.pk_num_empleado=b.pk_num_empleado $filtro"
        );
        $buscarTrabajador->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarTrabajador->fetchAll();
    }

    // Método que permite obtener el antecedente de servicio de un empleado
    public function metConsultarAntecedente($pkNumEmpleado)
    {
        $buscarAntecedente = $this->_db->query(
            "select a.fec_ingreso, date_format(a.fec_ingreso, '%d/%m/%Y') as fecha_ingreso, a.fec_egreso, date_format(a.fec_egreso, '%d/%m/%Y') as fecha_egreso, a.ind_empresa from vl_rh_empleado_experiencia_laboral as a,
a006_miscelaneo_detalle as b where a.pk_num_empleado=$pkNumEmpleado and a.fk_a006_num_miscelaneo_detalle_tipo_empresa=b.pk_num_miscelaneo_detalle and  b.cod_detalle='G'"
        );
        $buscarAntecedente->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarAntecedente->fetchAll();
    }

    // Método que permite consultar los sueldos base por periodos del empleado
    public function metConsultarSueldoBase($idEmpleado, $concepto)
    {
        $sueldoBase = $this->_db->query(
            " SELECT
        nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
        nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto,
        nm_b002_concepto.ind_descripcion,
        CONCAT(nm_c001_tipo_nomina_periodo.fec_anio,'-',nm_c001_tipo_nomina_periodo.fec_mes) AS periodo,
        nm_c001_tipo_nomina_periodo.fec_anio,
        nm_c001_tipo_nomina_periodo.fec_mes,
        SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS monto
        FROM
        nm_d005_tipo_nomina_empleado_concepto
        INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
        INNER JOIN nm_b002_concepto ON nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
        INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
        INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo = nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo
        WHERE
            nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado='$idEmpleado' AND
            nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto='$concepto' AND
            CONCAT(nm_c001_tipo_nomina_periodo.fec_anio,'-',nm_c001_tipo_nomina_periodo.fec_mes) < SUBSTRING(NOW(),1,7)
        GROUP BY CONCAT(nm_c001_tipo_nomina_periodo.fec_anio,'-',nm_c001_tipo_nomina_periodo.fec_mes) DESC, nm_c001_tipo_nomina_periodo.fec_anio, nm_c001_tipo_nomina_periodo.fec_mes LIMIT 24"
        );
       /* $sueldoBase = $this->_db->query(
            "select fec_anio, fec_mes, num_sueldo_base, num_total_asignaciones from nm_e002_prestaciones_sociales_calculo where fk_rhb001_num_empleado=$pkNumEmpleado order by pk_num_prestaciones_sociales_calculo desc limit 24"
        );*/
        $sueldoBase->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoBase->fetchAll();
    }

    // Método que permite mostrar el motivo del cese de un empleado
    public function metConsultarMotivoCese()
    {
        $consultarCese = $this->_db->query(
            "select pk_num_motivo_cese, ind_nombre_cese from rh_c032_motivo_cese"
        );
        $consultarCese->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarCese->fetchAll();
    }

    // Método que permite guardar la jubilacion
    public function metGuardarJubilacion($datosJubilacion, $datosMontoJubilacion, $datosOperacion)
    {
        $this->_db->beginTransaction();
        // Guardar Jubilacion
        $NuevaJubilacion = $this->_db->prepare(
            "insert into rh_b010_jubilacion values (null, :fk_rhb001_num_empleado, :fk_nmb001_num_tipo_nomina, :fk_a006_num_tipo_trabajador, :fk_rhc032_num_motivo_cese, :fk_a001_num_organismo, :fk_a004_num_dependencia, :fk_rhc063_num_puesto, :fec_ingreso, :fec_egreso, :ind_periodo, :num_edad, :num_anio_servicio, :num_anio_experiencia, :num_anio_total, :ind_observacion_cese, :fk_a018_num_seguridad_usuario, :fec_ultima_modificacion)"
        );

      $NuevaJubilacion->execute(array(
                ':fk_rhb001_num_empleado' => $datosJubilacion[0],
                ':fk_nmb001_num_tipo_nomina' => $datosJubilacion[1],
                ':fk_a006_num_tipo_trabajador' => $datosJubilacion[2],
                ':fk_rhc032_num_motivo_cese' => $datosJubilacion[3],
                ':fk_a001_num_organismo' => $datosJubilacion[4],
                ':fk_a004_num_dependencia' => $datosJubilacion[5],
                ':fk_rhc063_num_puesto' => $datosJubilacion[6],
                ':fec_ingreso' => $datosJubilacion[7],
                ':fec_egreso' => $datosJubilacion[8],
                ':ind_periodo' => $datosJubilacion[9],
                ':num_edad' => $datosJubilacion[10],
                ':num_anio_servicio' => $datosJubilacion[11],
                ':num_anio_experiencia' => $datosJubilacion[12],
                ':num_anio_total' => $datosJubilacion[13],
                ':ind_observacion_cese' => $datosJubilacion[14],
                ':fk_a018_num_seguridad_usuario' => $datosJubilacion[15],
                ':fec_ultima_modificacion' => $datosJubilacion[16]
            ));
        $pkNumJubilacion= $this->_db->lastInsertId();
        // Guardar montos
        $montoJubilacion = $this->_db->prepare(
            "insert into rh_c083_montos_jubilacion values (null, :num_ultimo_sueldo, :num_total_sueldo, :num_total_primas, :num_sueldo_base, :num_porcentaje, :num_coeficiente, :num_monto_jubilacion, :fk_rhb010_num_jubilacion)"
        );
            $montoJubilacion->execute(array(
                ':num_ultimo_sueldo' => $datosMontoJubilacion[0],
                ':num_total_sueldo' => $datosMontoJubilacion[1],
                ':num_total_primas' => $datosMontoJubilacion[2],
                ':num_sueldo_base' => $datosMontoJubilacion[3],
                ':num_porcentaje' => $datosMontoJubilacion[4],
                ':num_coeficiente'=> $datosMontoJubilacion[5],
                ':num_monto_jubilacion' => $datosMontoJubilacion[6],
                ':fk_rhb010_num_jubilacion'=> $pkNumJubilacion
            ));
        // Guardar Operacion
        $operacionJubilacion = $this->_db->prepare(
            "insert into rh_c085_operacion_jubilacion values (null, :fk_rhb010_num_jubilacion, :ind_estado, :ind_observacion, :fk_rhb001_num_empleado, :fk_rhc063_num_puesto, :fec_operacion, :num_estatus)"
        );
            $operacionJubilacion->execute(array(
                ':fk_rhb010_num_jubilacion' => $pkNumJubilacion,
                ':ind_estado' => 'PR',
                ':ind_observacion' => $datosOperacion[0],
                ':fk_rhb001_num_empleado' => $datosOperacion[1],
                ':fk_rhc063_num_puesto' => $datosOperacion[2],
                ':fec_operacion' => $datosOperacion[3],
                ':num_estatus' => '1'
            ));
        $this->_db->commit();
        return $pkNumJubilacion;
    }

    // Método que permite guardar la relación de sueldos de un funcionario (Sueldo base y primas por años de servicio)
    public function metGuardarRelacion($anioBase, $mesBase, $monto, $conceptoBase, $pkNumJubilacion)
    {
        $this->_db->beginTransaction();
        $montoJubilacion = $this->_db->prepare(
            "insert into rh_c084_relacion_sueldo_jubilacion values (null, :fk_rhb010_num_jubilacion, null, :num_anio_periodo, :ind_mes_periodo, :num_monto, :fk_nmb002_num_concepto)"
        );
        $montoJubilacion->execute(array(
            ':fk_rhb010_num_jubilacion' => $pkNumJubilacion,
            ':num_anio_periodo' => $anioBase,
            ':ind_mes_periodo' => $mesBase,
            ':num_monto' => $monto,
            ':fk_nmb002_num_concepto' => $conceptoBase

        ));
        $this->_db->commit();
    }

    // Método que permite listar las jubilaciones de un funcionario
    public function metConsultarJubilacion($pkNumJubilacion)
    {
        $consultarJubilacion = $this->_db->query(
            "select a.pk_num_jubilacion, a.num_ultimo_sueldo, a.num_edad, a.fec_ingreso, a.fec_egreso, a.ind_observacion_cese, a.num_total_sueldo, a.num_total_primas, sum(a.num_total_sueldo + a.num_total_primas) as total, a.num_porcentaje, a.num_sueldo_base, a.num_coeficiente, a.num_monto_jubilacion, a.fk_nmb001_num_tipo_nomina, a.fk_a006_num_tipo_trabajador, a.fk_rhc032_num_motivo_cese, c.ind_cedula_documento, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado, c.ind_cedula_documento, c.fec_nacimiento, d.ind_estado, e.ind_descripcion_empresa, f.ind_dependencia, g.ind_descripcion_cargo, i.ind_nombre_detalle from vl_rh_empleado_jubilacion as a, rh_b001_empleado as b, a003_persona as c, rh_c085_operacion_jubilacion as d, a001_organismo as e, a004_dependencia as f, rh_c063_puestos as g, a005_miscelaneo_maestro as h, a006_miscelaneo_detalle as i where a.pk_num_jubilacion=$pkNumJubilacion and a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and d.fk_rhb010_num_jubilacion=a.pk_num_jubilacion and d.num_estatus=1 and a.fk_a001_num_organismo=e.pk_num_organismo and a.fk_a004_num_dependencia=f.pk_num_dependencia and a.fk_rhc063_num_puesto=g.pk_num_puestos and h.cod_maestro='SEXO' and h.pk_num_miscelaneo_maestro=i.fk_a005_num_miscelaneo_maestro and c.fk_a006_num_miscelaneo_detalle_sexo=i.pk_num_miscelaneo_detalle group by a.num_ultimo_sueldo, a.num_total_sueldo, a.num_total_primas,  a.num_porcentaje, a.num_sueldo_base, a.num_coeficiente, a.num_monto_jubilacion,  d.ind_estado"
        );
        $consultarJubilacion->setFetchMode(PDO::FETCH_ASSOC);
        return $consultarJubilacion->fetch();
    }

    // Método que permite listar las jubilaciones
    public function metListarJubilacion($valor)
    {
        if($valor=='LI'){
            $filtro = '';
        } else {
            $filtro = " and d.ind_estado = '$valor'";
        }
        $listarJubilacion = $this->_db->query(
            "select a.pk_num_jubilacion, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2, b.pk_num_empleado, c.ind_cedula_documento, d.ind_estado from vl_rh_empleado_jubilacion as a, rh_b001_empleado as b, a003_persona as c, rh_c085_operacion_jubilacion as d where a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona and d.fk_rhb010_num_jubilacion=a.pk_num_jubilacion and d.num_estatus=1 $filtro"
        );
        $listarJubilacion->setFetchMode(PDO::FETCH_ASSOC);
        return $listarJubilacion->fetchAll();
    }

    // Método que permite consultar los estatus aprobatorios por los que pasa una jubilación
    public function metOperacionJubilacion($pkNumJubilacion, $estatus)
    {
        $operacionJubilacion = $this->_db->query(
            "select a.ind_estado, a.fec_operacion, a.ind_observacion, c.ind_nombre1, c.ind_nombre2, c.ind_apellido1, c.ind_apellido2 from rh_c085_operacion_jubilacion as a, rh_b001_empleado as b, a003_persona as c where a.fk_rhb010_num_jubilacion=$pkNumJubilacion and a.ind_estado='$estatus' and a.fk_rhb001_num_empleado=b.pk_num_empleado and b.fk_a003_num_persona=c.pk_num_persona"
        );
        $operacionJubilacion->setFetchMode(PDO::FETCH_ASSOC);
        return $operacionJubilacion->fetch();
    }

    // Método que permite cambiar el estatus aprobatorio de una jubilacion
    public function metCambiarEstatus($pkNumJubilacion, $valor, $ind_observacion, $funcionario, $cargoEmpleado, $fecha_hora)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update rh_c085_operacion_jubilacion set num_estatus=0 where fk_rhb010_num_jubilacion=$pkNumJubilacion"
        );
        $cambiarEstatus=$this->_db->prepare(
            "insert into rh_c085_operacion_jubilacion values (null, :fk_rhb010_num_jubilacion, :ind_estado, :ind_observacion, :fk_rhb001_num_empleado, :fk_rhc063_num_puesto, :fec_operacion, :num_estatus)"
        );
        $cambiarEstatus->execute(array(
           ':fk_rhb010_num_jubilacion' => $pkNumJubilacion,
           ':ind_estado' => $valor,
           ':ind_observacion' => $ind_observacion,
           ':fk_rhb001_num_empleado' => $funcionario,
           ':fk_rhc063_num_puesto' => $cargoEmpleado,
           ':fec_operacion'  => $fecha_hora,
           ':num_estatus' => 1
        ));
        $this->_db->commit(); 
    }

    // Método que permite buscar una jubilación
    public function metBuscarJubilacion($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $indCedulaDocumento, $txtEstatus) 
    {
        if ($pkNumOrganismo!='') {
            $filtro = " and a.fk_a001_num_organismo=$pkNumOrganismo";
        }
        if ($pkNumDependencia!='') {
            $filtro .= " and a.fk_a004_num_dependencia=$pkNumDependencia";
        }
        if ($pkNumEmpleado!='') {
            $filtro .= " and b.pk_num_empleado=$pkNumEmpleado";
        }
        if ($indCedulaDocumento!='') {
            $filtro .= " and b.ind_cedula_documento=$indCedulaDocumento";
        }
        if ($txtEstatus!='') {
            $filtro .= " and c.ind_estado='$txtEstatus'";
        }
        $buscarJubilacion = $this->_db->query(
            "select a.pk_num_jubilacion, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2, b.pk_num_empleado, b.ind_cedula_documento, c.ind_estado, a.num_monto_jubilacion, d.ind_dependencia from vl_rh_empleado_jubilacion as a, vl_rh_persona_empleado as b, rh_c085_operacion_jubilacion as c, a004_dependencia as d where a.fk_rhb001_num_empleado=b.pk_num_empleado and c.fk_rhb010_num_jubilacion=a.pk_num_jubilacion and c.num_estatus=1 and a.fk_a004_num_dependencia=d.pk_num_dependencia $filtro"
        );
        $buscarJubilacion->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarJubilacion->fetchAll();
    }

    // Método que permite editar una jubilación
    public function metEditarJubilacion($pkNumJubilacion, $tipoNomina, $tipoTrabajador, $motivoCese, $explicacionCese, $fechaCeseFormato, $usuario, $fecha_hora)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update rh_b010_jubilacion set fk_nmb001_num_tipo_nomina=$tipoNomina, fk_a006_num_tipo_trabajador=$tipoTrabajador, fk_rhc032_num_motivo_cese=$motivoCese, ind_observacion_cese='$explicacionCese', fec_egreso='$fechaCeseFormato', fk_a018_num_seguridad_usuario= $usuario, fec_ultima_modificacion='$fecha_hora' where pk_num_jubilacion=$pkNumJubilacion"
        );
        $this->_db->commit();
    }

    // Método que permite mostrar sueldo base y primas
    public function metMostrarConcepto($pkNumJubilacion, $conceptoBase)
    {
        $mostrarConcepto = $this->_db->query(
            "select num_anio_periodo, ind_mes_periodo, num_monto from rh_c084_relacion_sueldo_jubilacion where fk_rhb010_num_jubilacion=$pkNumJubilacion and fk_nmb002_num_concepto=$conceptoBase"
        );
        $mostrarConcepto->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarConcepto->fetchAll();
    }
	
	// Método que permite verificar si un empleado posee una jubilación registrada
    public function metComprobarEmpleado($pkNumEmpleado)
    {
        $comprobar = $this->_db->query(
            "select ind_estado from rh_c085_operacion_jubilacion where fk_rhb001_num_empleado=$pkNumEmpleado and num_estatus=1 order by pk_num_operacion_jubilacion desc limit 1"
        );
        $comprobar->setFetchMode(PDO::FETCH_ASSOC);
        return $comprobar->fetchAll();
    }

}// fin de la clase