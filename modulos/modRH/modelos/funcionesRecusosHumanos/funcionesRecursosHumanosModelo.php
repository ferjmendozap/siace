<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class funcionesRecursosHumanosModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');

    }

    #PERMITE OBTENER EL DIA DE LA SEMANA DE UNA FECHA
    public function metObtenerDiaSemana($fecha)
    {

        // primero creo un array para saber los días de la semana
        $dias = array(0, 1, 2, 3, 4, 5, 6);
        $dia = substr($fecha, 0, 2);
        $mes = substr($fecha, 3, 2);
        $anio = substr($fecha, 6, 4);
        // en la siguiente instrucción $pru toma el día de la semana, lunes, martes,
        $dato = strtoupper($dias[intval((date("w",mktime(0,0,0,$mes,$dia,$anio))))]);
        return $dato;

    }

    #PERMITE OBTENER DIAS HABILES
    public function metObtenerDiasHabiles($fecha_ini,$fecha_fin)
    {

        $dias_completos = $this->metObtenerDiasFecha($fecha_ini,$fecha_fin);

        $dias_feriados = $this->metObtenerDiasFeriados($fecha_ini,$fecha_fin);

        $dia_semana = $this->metObtenerDiaSemana($fecha_ini);

        $dias_habiles = 0;

        for ($i=0; $i<=$dias_completos; $i++) {

            if ($dia_semana >= 1 && $dia_semana <= 5) $dias_habiles++;

            $dia_semana++;

            if ($dia_semana == 7) $dia_semana = 0;

        }

        $dias_habiles -= $dias_feriados;

        return $dias_habiles;

    }

    #diferencia dias entre dos fechas
    public function metObtenerDiasFecha($fecha_ini,$fecha_fin)
    {

        list($dd, $md, $ad) = explode( '-', $fecha_ini);	$desde = "$ad-$md-$dd";

        list($dh, $mh, $ah) = explode( '-', $fecha_fin);	$hasta = "$ah-$mh-$dh";

        #diferencia de dias entre las fechas
        $con = $this->_db->query("SELECT DATEDIFF('$hasta', '$desde') as valor");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        $f = $con->fetch();

        return $f['valor'];

    }

    #PERMITE OBTENER DIAS FERIADOS SEGUN RANGO DE FECHAS
    public function metObtenerDiasFeriados($fecha_ini,$fecha_fin)
    {

        list($dia_desde, $mes_desde, $anio_desde) = explode('-', $fecha_ini); $DiaDesde = "$mes_desde-$dia_desde";
        list($dia_hasta, $mes_hasta, $anio_hasta) = explode('-', $fecha_fin); $DiaHasta = "$mes_hasta-$dia_hasta";

        $con = $this->_db->query("
                SELECT
                    *
                FROM
                    rh_c069_feriados
                WHERE
                    (num_flag_variable = 1 AND
				    (fec_anio = '$anio_desde' OR fec_anio = '$anio_hasta') AND
				    (fec_dia >= '$DiaDesde' AND fec_dia <= '$DiaHasta')) OR
				    (num_flag_variable = 0 AND fec_dia >= '$DiaDesde' AND fec_dia <= '$DiaHasta')
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $data = $con->fetchAll();

        $dias_feriados=0;

        for($i=0;$i<count($data);$i++){

            list($mes, $dia) = explode('-', $data[$i]['fec_dia']);

            if ($data[$i]['fec_anio'] == "") $anio = $anio_desde; else $anio = $data[$i]['fec_anio'];

            $fecha = "$dia-$mes-$anio";

            $dia_semana = $this->metObtenerDiaSemana($fecha);

            if ($dia_semana >= 1 && $dia_semana <= 5) $dias_feriados++;

            if ($anio_desde != $anio_hasta) {

                if ($data[$i]['fec_anio'] == "") $anio = $anio_hasta; else $anio = $data[$i]['fec_anio'];

                $fecha = "$dia-$mes-$anio";

                $dia_semana = $this->metObtenerDiaSemana($fecha);

                if ($dia_semana >= 1 && $dia_semana <= 5) $dias_feriados++;

            }

        }

        return $dias_feriados;

    }



    /*
     * DIFERENCIA ENTRE DOS HORAS SEGUN EMPLEADO Y HORARIO
     */
    public function metDiferenciaHorasEventos($empleado,$fecha,$hora_salida,$hora_entrada)
    {

        #transformo las horas a hora militar
        if(!empty($hora_salida)){
            $hs = strtotime($hora_salida);
            $hora_salida = date("H:i:s",$hs);
        }else{
            $hora_salida = '';
        }

        if(!empty($hora_entrada)){
            $he = strtotime($hora_entrada);
            $hora_entrada = date("H:i:s",$he);
        }else{
            $hora_entrada = '';
        }


        #busco el horario que tiene asignado el empleado
        $con1 = $this->_db->query("
                SELECT
                    fk_rhb007_num_horario
                FROM
                    rh_c005_empleado_laboral
                WHERE
                    fk_rhb001_num_empleado='$empleado'
               ");

        $con1->setFetchMode(PDO::FETCH_ASSOC);
        $data1 = $con1->fetch();
        $horario = $data1['fk_rhb007_num_horario'];

        #formato de fecha AMD - anio - mes -dia
        $f = explode("-",$fecha);
        $fecha = $f[2]."-".$f[1]."-".$f[0];

        #obtengo el dia de la semana para la fecha
        $dia = date("N", strtotime("".$fecha.""));

        if($horario != ''){

            #obtengo el detalle del horario
            $con2 = $this->_db->query("
                SELECT
                    *
                FROM
                    rh_c067_horario_detalle
                WHERE
                    fk_rhb007_num_horario='$horario' AND
                    num_dia='$dia' AND
                    num_flag_laborable=1
               ");

            $con2->setFetchMode(PDO::FETCH_ASSOC);
            $data2 = $con2->fetch();


            //	si tiene salida y no entrada
            if($hora_salida && !$hora_entrada){

                //	si la salida es en el primer turno
                if ($hora_salida >= $data2['fec_entrada1'] && $hora_salida < $data2['fec_salida1']) {
                    $turno1 = $this->metDiffHora($hora_salida, $data2['fec_salida1']);
                    $turno2 = $this->metDiffHora($data2['fec_entrada2'], $data2['fec_salida2']);
                }
                //	si la salida es en el segundo turno
                elseif ($hora_salida >= $data2['fec_entrada2'] && $hora_salida < $data2['fec_salida2']) {
                    $turno1 = "00:00:00";
                    $turno2 = $this->metDiffHora($hora_salida, $data2['fec_salida2']);

                }


            }


            //	si tiene salida y entrada
            elseif ($hora_salida && $hora_entrada) {
                //	si la salida y la entrada estan en el mismo turno
                if (($hora_salida >= $data2['fec_entrada1'] && $hora_salida < $data2['fec_salida1'] && $hora_entrada >= $data2['fec_entrada1'] && $hora_entrada <= $data2['fec_salida1']) || ($hora_salida >= $data2['fec_entrada2'] && $hora_salida < $data2['fec_salida2'] && $hora_entrada >= $data2['fec_entrada2'] && $hora_entrada <= $data2['fec_salida2'])) {
                    $turno1 = $this->metDiffHora($hora_salida, $hora_entrada);
                    $turno2 = "00:00:00";
                }
                //	si la salida es en el turno de la mañana y la entrada en el turno de la tarde
                elseif ($hora_salida >= $data2['fec_entrada1'] && $hora_salida < $data2['fec_salida1'] && $hora_entrada >= $data2['fec_entrada2'] && $hora_entrada <= $data2['fec_salida2']) {
                    $turno1 = $this->metDiffHora($hora_salida, $data2['fec_salida1']);
                    $turno2 = $this->metDiffHora($data2['fec_entrada2'], $hora_entrada);
                }

                else{
                    $turno1 = $this->metDiffHora($hora_salida, $data2['fec_salida1']);
                    $turno2 = $this->metDiffHora($data2['fec_entrada2'], $hora_entrada);
                }

            }

            //	si no tiene salida y si entrada
            elseif (!$hora_salida && $hora_entrada) {
                //	si la salida es en el primer turno
                if ($hora_entrada > $data2['fec_entrada1'] && $hora_entrada < $data2['fec_salida1']) {
                    $turno1 = $this->metDiffHora($data2['fec_entrada1'], $hora_entrada);
                    $turno2 = "00:00:00";
                }
                //	si la salida es en el segundo turno
                elseif ($hora_entrada > $data2['fec_entrada2'] && $hora_entrada < $data2['fec_salida2']) {
                    $turno1 = "00:00:00";
                    $turno2 = $this->metDiffHora($data2['fec_entrada2'], $hora_entrada);
                }


            }

            list($h1, $m1) = explode(":", $turno1);
            list($h2, $m2) = explode(":", $turno2);
            $totalh = $h1 + $h2;
            $totalm = $m1 + $m2;
            if ($totalm >= 60) {
                $hsumar = $totalm / 60;
                $totalh += $hsumar;
                $totalm = $totalm - (60 * $hsumar);
            }

            $total = $totalh.":".$totalm;

            return $total;



        }else{

            return $this->metDiffHora($hora_salida,$hora_entrada);

        }


    }


    /*
     * PERMITE CALCULAR LA DIFERENCIA QUE EXISTE ENTRE DOS HORAS
     */
    public function metDiffHora($Desde, $Hasta) {

        $con = $this->_db->query("
                SELECT TIMEDIFF('$Hasta', '$Desde') AS TotalHoras
               ");

        $con->setFetchMode(PDO::FETCH_ASSOC);
        $data = $con->fetch();

        return substr($data['TotalHoras'], 0, 5);

    }




}//fin clase
