<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class islrControlador extends Controlador
{
    private $atEmpleados;
    private $atIslr;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados     = $this->metCargarModelo('empleados','gestion');
        $this->atIslr   = $this->metCargarModelo('islr','gestion');
     }

    #Metodo Index del controlador listado de impoesto sobre la renta del empleado
    public function metIndex()
    {

    }

    public function metIslr()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO
        $complementosCss = array(
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min','bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idIslr = $this->metObtenerInt('idIslr');
        $idEmpleado      = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');


        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_porcentaje')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idIslr==0){//PARA INSERTAR REGISTRO

                $validacion['status']='creacion';

                #INSTANCIO EL METODO PARA REGISTRAR IMPUESTO SOBRE LA RENTA
                $id = $this->atIslr->metRegistrarIslr($idEmpleado,
                    $validacion['fec_anio'],
                    $validacion['ind_desde'],
                    $validacion['ind_hasta'],
                    $validacion['num_porcentaje']
                );
                $validacion['idIslr'] = $id;

                    echo json_encode($validacion);
                    exit;



            }else{//PARA MODIFICAR REGISTRO

                $validacion['status']='modificar';

                #INSTANCIO EL METODO PARA MODIFICAR IMPUESTO SOBRE LA RENTA
                $id = $this->atIslr->metModificarIslr($idIslr,
                    $validacion['fec_anio'],
                    $validacion['ind_desde'],
                    $validacion['ind_hasta'],
                    $validacion['num_porcentaje']
                );
                $validacion['idIslr'] = $idIslr;

                    echo json_encode($validacion);
                    exit;


            }

        }

        if($idIslr!=0){
            $db=$this->atIslr->metMostrarIslr($idIslr);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->assign('idIslr',$idIslr);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('form', 'modales');
    }


    public function metEliminarIslr()
    {
        $idIslr = $this->metObtenerInt('idIslr');
        if($idIslr!=0){
            $id=$this->atIslr->metEliminarIslr($idIslr);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'idIslr'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

}
