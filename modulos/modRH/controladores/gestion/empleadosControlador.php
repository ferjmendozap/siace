<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class empleadosControlador extends Controlador
{
    private $atEmpleados;
    private $atPaisModelo;
    private $atEstadoModelo;
    private $atMunicipioModelo;
    private $atCiudadModelo;
    private $atDependencias;
    private $atTipoNomina;
    private $atHorario;
    private $atOrganismo;
    private $atOperaciones;
    private $atIdUsuario;
    private $atPermisoModelo;
    private $atVacacionModelo;
    private $atGradoSalarial;
    private $atPasos;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados    = $this->metCargarModelo('empleados','gestion');
        $this->atPaisModelo   = $this->metCargarModelo('pais',false,'APP');
        $this->atEstadoModelo = $this->metCargarModelo('estado',false,'APP');
        $this->atMunicipioModelo = $this->metCargarModelo('municipio',false,'APP');
        $this->atCiudadModelo = $this->metCargarModelo('ciudad',false,'APP');
        $this->atDependencias = $this->metCargarModelo('dependencias','maestros');
        $this->atTipoNomina   = $this->metCargarModelo('tipoNomina','maestros','modNM');
        $this->atHorario      = $this->metCargarModelo('horarioLaboral','maestros');
        $this->atOrganismo    = $this->metCargarModelo('organismo','maestros','modCV');
        $this->atIdUsuario    = Session::metObtener('idUsuario');
        $this->atVacacionModelo = $this->metCargarModelo('vacaciones', 'gestion');
        $this->atGradoSalarial  = $this->metCargarModelo('gradoSalarial','maestros');
        $this->atPasos          = $this->metCargarModelo('pasos','maestros');

       }

    #Metodo Index del controlador Empleados.
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];

        $listadoOrganismo = $this->atEmpleados->metListarOrganismos();

        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);
        $pkNumDependencia = $datosEmpleado['pk_num_dependencia'];

        $listadoDependencia = $this->atDependencias->metListarDependenciaOrganismo($datosEmpleado['pk_num_organismo'], 1, 0);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        $this->atVista->assign('dependenciaEmpleado', $pkNumDependencia);
        // Organismo donde trabaja el empleado
        $empleado = array(
            'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
            'pk_num_dependencia' => $pkNumDependencia
        );

        $centroCosto = $this->atEmpleados->metListarCentroCosto();
        $tipoNomina = $this->atTipoNomina->metListarTipoNomina();
        $tipoTrabajador = $this->atEmpleados->metMostrarSelect('TIPOTRAB');

        #obtengo el valor del parametro HCMCONTROL para funcionalidad de hcm
        $hcmcontrol = Session::metObtener('HCMCONTROL');

        $this->atVista->assign('hcmcontrol', $hcmcontrol);
        $this->atVista->assign('listadoOrganismos', $listadoOrganismo);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('centroCosto', $centroCosto);
        $this->atVista->assign('nomina', $tipoNomina);
        $this->atVista->assign('tipoTrabajador', $tipoTrabajador);
        $this->atVista->assign('listadoEmpleados', $this->atEmpleados->metListarEmpleado('AP','listado','1','1'));
        $this->atVista->assign('n', 1);
        $this->atVista->metRenderizar('empleados');
    }

    public function metEmpleado()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/core/demo/DemoFormWizard','Aplicacion/appFunciones');
        $complementosCss = array(
            'wizard/wizardfa6c'
        );
        $complementoJs = array(
            'wizard/jquery.bootstrap.wizard.min'
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);



        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');
        $operacion = $this->metObtenerTexto('operacion');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');
            $formForm     = $this->metObtenerFormulas('form','formula');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum])) {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = '';

                    }
                }
            }
            if(!empty($formForm)) {
                foreach ($formForm as $tituloTxt => $valorTxt) {
                    if (!empty($formForm[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            #cuando se registra el empleado por primera vez
            if(strcmp($operacion,'registrar')==0){

                $validacion['status']='creacion';

                if(!isset($validacion['ind_estatus'])){
                    $validacion['ind_estatus']=0;
                }

                if(!isset($validacion['ind_estatus_persona'])){
                    $validacion['ind_estatus_persona']=0;
                }

                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodir'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodir']=0;
                }
                if(!isset($validacion['ind_direccion'])){
                    $validacion['ind_direccion']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono']=0;
                }
                if(!isset($validacion['ind_telefono'])){
                    $validacion['ind_telefono']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono1'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono1']=0;
                }
                if(!isset($validacion['ind_telefono1'])){
                    $validacion['ind_telefono1']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono2'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono2']=0;
                }
                if(!isset($validacion['ind_telefono2'])){
                    $validacion['ind_telefono2']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipolic'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipolic']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_claselic'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_claselic']=0;
                }
                if(!isset($validacion['fec_expiracion_licencia'])){
                    $validacion['fec_expiracion_licencia']=0;
                }
                if(!isset($validacion['cedRef1'])){
                    $validacion['cedRef1']=0;
                }
                if(!isset($validacion['cedRef2'])){
                    $validacion['cedRef2']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodirRef1'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef1']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodirRef2'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef2']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio1'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio1']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio2'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio2']=0;
                }
                if(!isset($validacion['pk_num_persona_direccion'])){
                    $validacion['pk_num_persona_direccion']=false;
                }
                if(!isset($validacion['pk_num_telefono'])){
                    $validacion['pk_num_telefono']=false;
                }
                if(!isset($validacion['pk_num_telefono1'])){
                    $validacion['pk_num_telefono1']=false;
                }
                if(!isset($validacion['pk_num_telefono2'])){
                    $validacion['pk_num_telefono2']=false;
                }
                if(!isset($validacion['pk_num_empleado_emergencia1'])){
                    $validacion['pk_num_empleado_emergencia1']=0;
                }
                if(!isset($validacion['pk_num_empleado_emergencia2'])){
                    $validacion['pk_num_empleado_emergencia2']=0;
                }
                if(!isset($validacion['pk_num_direccion_persona1'])){
                    $validacion['pk_num_direccion_persona1']=0;
                }
                if(!isset($validacion['pk_num_direccion_persona2'])){
                    $validacion['pk_num_direccion_persona2']=0;
                }
                if(!isset($validacion['pk_num_personalic'])){
                    $validacion['pk_num_personalic']=0;
                }
                if(!isset($validacion['pk_num_licencia'])){
                    $validacion['pk_num_licencia']=false;
                }
                if(!isset($validacion['aleatorio'])){
                    $validacion['aleatorio']='';
                }
                if(!isset($validacion['fk_a010_num_ciudad'])){
                    $validacion['fk_a010_num_ciudad']=false;
                }
                if(!isset($validacion['fk_a013_num_sector'])){
                    $validacion['fk_a013_num_sector']=false;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_gsanguineo']) || empty($validacion['fk_a006_num_miscelaneo_detalle_gsanguineo'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_gsanguineo']=NULL;
                }
                if(!isset($validacion['fk_a006_num_tipo_zona'])){
                    $validacion['fk_a006_num_tipo_zona']=false;
                }
                if(!isset($validacion['fk_a006_num_ubicacion_inmueble'])){
                    $validacion['fk_a006_num_ubicacion_inmueble']=false;
                }
                if(!isset($validacion['ind_ubicacion_inmueble'])){
                    $validacion['ind_ubicacion_inmueble']=false;
                }
                if(!isset($validacion['fk_a006_num_tipo_inmueble'])){
                    $validacion['fk_a006_num_tipo_inmueble']=false;
                }
                if(!isset($validacion['ind_tipo_inmueble'])){
                    $validacion['ind_tipo_inmueble']=false;
                }
                if(!isset($validacion['fk_a006_num_zona_residencial'])){
                    $validacion['fk_a006_num_zona_residencial']=false;
                }
                if(!isset($validacion['ind_zona_residencial'])){
                    $validacion['ind_zona_residencial']=false;
                }
                if(!isset($validacion['ind_inmueble'])){
                    $validacion['ind_inmueble']=false;
                }
                if(!isset($validacion['ind_punto_referencia'])){
                    $validacion['ind_punto_referencia']=false;
                }
                if(!isset($validacion['ind_zona_postal'])){
                    $validacion['ind_zona_postal']=false;
                }


                //arreglos de datos enviados por el formulario
                $datosIniciales = array(
                    $validacion['cedula'],
                    $validacion['persona'],
                    $validacion['nombre1'],
                    $validacion['nombre2'],
                    $validacion['apellido1'],
                    $validacion['apellido2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_sexo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_nacionalidad'],
                    $validacion['doc_fiscal'],
                    $validacion['email'],
                    $validacion['aleatorio']
                    //$validacion['Foto']
                );

                $nacimiento   = array(
                    $validacion['fk_a008_num_pais'],
                    $validacion['fk_a009_num_estado'],
                    $validacion['fk_a011_num_municipio'],
                    $validacion['fk_a010_num_ciudad'],
                    $validacion['fechaNacimiento'],
                    $validacion['lugarNacimiento'],
                );

                $domicilio    = array(
                    $validacion['fk_a008_num_pais2'],
                    $validacion['fk_a009_num_estado2'],
                    $validacion['fk_a011_num_municipio2'],
                    $validacion['fk_a010_num_ciudad2'],
                    $validacion['parroquia'],
                    $validacion['fk_a013_num_sector'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodir'],
                    $validacion['ind_direccion'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono'],
                    $validacion['ind_telefono'],
                    $validacion['pk_num_persona_direccion'],
                    $validacion['pk_num_telefono'],
                    $validacion['fk_a006_num_tipo_zona'],
                    $validacion['fk_a006_num_ubicacion_inmueble'],
                    $validacion['ind_ubicacion_inmueble'],
                    $validacion['fk_a006_num_tipo_inmueble'],
                    $validacion['ind_tipo_inmueble'],
                    $validacion['fk_a006_num_zona_residencial'],
                    $validacion['ind_zona_residencial'],
                    $validacion['ind_inmueble'],
                    $validacion['ind_punto_referencia'],
                    $validacion['ind_zona_postal']

                );
                $datos        = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_gsanguineo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_edocivil'],
                    #DATOS REFERENCIA EMERGENCIA 1
                    $validacion['cedRef1'],
                    $validacion['nombRef1'],
                    $validacion['apeRef1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef1'],
                    $validacion['dirRef1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono1'],
                    $validacion['ind_telefono1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_parentesco'],
                    #DATOS REFERENCIA EMERGENCIA 2
                    $validacion['cedRef2'],
                    $validacion['nombRef2'],
                    $validacion['apeRef2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef2'],
                    $validacion['dirRef2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono2'],
                    $validacion['ind_telefono2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_parentesco2'],
                    #DATOS LICENCIA
                    $validacion['fk_a006_num_miscelaneo_detalle_tipolic'],
                    $validacion['fk_a006_num_miscelaneo_detalle_claselic'],
                    $validacion['fec_expiracion_licencia'],
                    #IDS TELEFONOS DE EMERGENCIA
                    $validacion['pk_num_telefono1'],
                    $validacion['pk_num_telefono2'],
                    $validacion['pk_num_empleado_emergencia1'],
                    $validacion['pk_num_empleado_emergencia2'],
                    $validacion['pk_num_persona1'],
                    $validacion['pk_num_persona2'],
                    $validacion['pk_num_direccion_persona1'],
                    $validacion['pk_num_direccion_persona2'],
                    $validacion['pk_num_personalic'],
                    $validacion['pk_num_licencia'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio2'],
                    #MODIFICACION COMPLEMENTOS DIRECCION
                    $validacion['fk_a006_num_tipo_zona1'],
                    $validacion['fk_a006_num_ubicacion_inmueble1'],
                    $validacion['ind_ubicacion_inmueble1'],
                    $validacion['fk_a006_num_tipo_inmueble1'],
                    $validacion['ind_tipo_inmueble1'],
                    $validacion['fk_a006_num_zona_residencial1'],
                    $validacion['ind_zona_residencial1'],
                    $validacion['ind_inmueble1'],
                    $validacion['ind_punto_referencia1'],
                    $validacion['ind_zona_postal1'],

                    $validacion['fk_a006_num_tipo_zona2'],
                    $validacion['fk_a006_num_ubicacion_inmueble2'],
                    $validacion['ind_ubicacion_inmueble2'],
                    $validacion['fk_a006_num_tipo_inmueble2'],
                    $validacion['ind_tipo_inmueble2'],
                    $validacion['fk_a006_num_zona_residencial2'],
                    $validacion['ind_zona_residencial2'],
                    $validacion['ind_inmueble2'],
                    $validacion['ind_punto_referencia2'],
                    $validacion['ind_zona_postal2']

                );
                $organizacion = array(
                    $validacion['fk_a001_num_organismo'],
                    $validacion['fk_a004_num_dependencia'],
                    $validacion['fk_a023_num_centro_costo'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipopago'],
                    $validacion['num_tipo_trabajador']
                );
                $laborales    = array(
                    $validacion['fechaIngreso'],
                    $validacion['nroResolucion'],
                    $validacion['ind_estatus'],
                    $validacion['fk_a006_num_miscelaneo_detalle_grupoocupacional'],
                    $validacion['fk_rhc010_num_serie'],
                    $validacion['fk_rhc063_num_puestos_cargo'],
                    $validacion['fk_rhb007_num_horario'],
                    $validacion['num_sueldo_basico'],
                    $validacion['fk_a006_num_miscelaneo_detalle_paso']
                );


                $id = $this->atEmpleados->metRegistrarEmpleado($datosIniciales,$nacimiento,$domicilio,$datos,$organizacion,$laborales);

                $validacion['idEmpleado'] = $id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    echo json_encode($validacion);
                    exit;

                }else{
                    echo json_encode($validacion);
                    exit;
                }

            }

            #cuando se modifica el registro del empleado ya aprobado
            if(strcmp($operacion,'modificar')==0){

                $validacion['status']='modificacion';

                if(!isset($validacion['ind_estatus'])){
                    $validacion['ind_estatus']=0;
                }

                if(!isset($validacion['ind_estatus_persona'])){
                    $validacion['ind_estatus_persona']=0;
                }

                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodir'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodir']=0;
                }
                if(!isset($validacion['ind_direccion'])){
                    $validacion['ind_direccion']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono']=0;
                }
                if(!isset($validacion['ind_telefono'])){
                    $validacion['ind_telefono']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono1'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono1']=0;
                }
                if(!isset($validacion['ind_telefono1'])){
                    $validacion['ind_telefono1']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono2'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono2']=0;
                }
                if(!isset($validacion['ind_telefono2'])){
                    $validacion['ind_telefono2']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipolic'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipolic']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_claselic'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_claselic']=0;
                }
                if(!isset($validacion['fec_expiracion_licencia'])){
                    $validacion['fec_expiracion_licencia']=0;
                }
                if(!isset($validacion['cedRef1'])){
                    $validacion['cedRef1']=0;
                }
                if(!isset($validacion['cedRef2'])){
                    $validacion['cedRef2']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodirRef1'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef1']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodirRef2'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef2']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio1'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio1']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio2'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio2']=0;
                }
                if(!isset($validacion['pk_num_persona_direccion'])){
                    $validacion['pk_num_persona_direccion']=false;
                }
                if(!isset($validacion['pk_num_telefono'])){
                    $validacion['pk_num_telefono']=false;
                }
                if(!isset($validacion['pk_num_telefono1'])){
                    $validacion['pk_num_telefono1']=false;
                }
                if(!isset($validacion['pk_num_telefono2'])){
                    $validacion['pk_num_telefono2']=false;
                }
                if(!isset($validacion['pk_num_empleado_emergencia1'])){
                    $validacion['pk_num_empleado_emergencia1']=0;
                }
                if(!isset($validacion['pk_num_empleado_emergencia2'])){
                    $validacion['pk_num_empleado_emergencia2']=0;
                }
                if(!isset($validacion['pk_num_direccion_persona1'])){
                    $validacion['pk_num_direccion_persona1']=0;
                }
                if(!isset($validacion['pk_num_direccion_persona2'])){
                    $validacion['pk_num_direccion_persona2']=0;
                }
                if(!isset($validacion['pk_num_personalic'])){
                    $validacion['pk_num_personalic']=0;
                }
                if(!isset($validacion['pk_num_licencia'])){
                    $validacion['pk_num_licencia']=false;
                }
                if(!isset($validacion['aleatorio'])){
                    $validacion['aleatorio']='';
                }
                if(!isset($validacion['fk_a010_num_ciudad'])){
                    $validacion['fk_a010_num_ciudad']=false;
                }
                if(!isset($validacion['fk_a013_num_sector'])){
                    $validacion['fk_a013_num_sector']=false;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_gsanguineo']) || empty($validacion['fk_a006_num_miscelaneo_detalle_gsanguineo'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_gsanguineo']=NULL;
                }
                if(!isset($validacion['fk_a006_num_tipo_zona'])){
                    $validacion['fk_a006_num_tipo_zona']=false;
                }
                if(!isset($validacion['fk_a006_num_ubicacion_inmueble'])){
                    $validacion['fk_a006_num_ubicacion_inmueble']=false;
                }
                if(!isset($validacion['ind_ubicacion_inmueble'])){
                    $validacion['ind_ubicacion_inmueble']=false;
                }
                if(!isset($validacion['fk_a006_num_tipo_inmueble'])){
                    $validacion['fk_a006_num_tipo_inmueble']=false;
                }
                if(!isset($validacion['ind_tipo_inmueble'])){
                    $validacion['ind_tipo_inmueble']=false;
                }
                if(!isset($validacion['fk_a006_num_zona_residencial'])){
                    $validacion['fk_a006_num_zona_residencial']=false;
                }
                if(!isset($validacion['ind_zona_residencial'])){
                    $validacion['ind_zona_residencial']=false;
                }
                if(!isset($validacion['ind_inmueble'])){
                    $validacion['ind_inmueble']=false;
                }
                if(!isset($validacion['ind_punto_referencia'])){
                    $validacion['ind_punto_referencia']=false;
                }
                if(!isset($validacion['ind_zona_postal'])){
                    $validacion['ind_zona_postal']=false;
                }

                //arreglos de datos enviados por el formulario
                $datosIniciales = array(
                    $validacion['cedula'],
                    $validacion['persona'],
                    $validacion['nombre1'],
                    $validacion['nombre2'],
                    $validacion['apellido1'],
                    $validacion['apellido2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_sexo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_nacionalidad'],
                    $validacion['doc_fiscal'],
                    $validacion['email'],
                    $validacion['aleatorio']
                );

                $nacimiento   = array(
                    $validacion['fk_a008_num_pais'],
                    $validacion['fk_a009_num_estado'],
                    $validacion['fk_a011_num_municipio'],
                    $validacion['fk_a010_num_ciudad'],
                    $validacion['fechaNacimiento'],
                    $validacion['lugarNacimiento'],
                );

                $domicilio    = array(
                    $validacion['fk_a008_num_pais2'],
                    $validacion['fk_a009_num_estado2'],
                    $validacion['fk_a011_num_municipio2'],
                    $validacion['fk_a010_num_ciudad2'],
                    $validacion['parroquia'],
                    $validacion['fk_a013_num_sector'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodir'],
                    $validacion['ind_direccion'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono'],
                    $validacion['ind_telefono'],
                    $validacion['pk_num_persona_direccion'],
                    $validacion['pk_num_telefono'],
                    $validacion['fk_a006_num_tipo_zona'],
                    $validacion['fk_a006_num_ubicacion_inmueble'],
                    $validacion['ind_ubicacion_inmueble'],
                    $validacion['fk_a006_num_tipo_inmueble'],
                    $validacion['ind_tipo_inmueble'],
                    $validacion['fk_a006_num_zona_residencial'],
                    $validacion['ind_zona_residencial'],
                    $validacion['ind_inmueble'],
                    $validacion['ind_punto_referencia'],
                    $validacion['ind_zona_postal']
                );

                $datos        = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_gsanguineo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_edocivil'],
                    #DATOS REFERENCIA EMERGENCIA 1
                    $validacion['cedRef1'],
                    $validacion['nombRef1'],
                    $validacion['apeRef1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef1'],
                    $validacion['dirRef1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono1'],
                    $validacion['ind_telefono1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_parentesco'],
                    #DATOS REFERENCIA EMERGENCIA 2
                    $validacion['cedRef2'],
                    $validacion['nombRef2'],
                    $validacion['apeRef2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef2'],
                    $validacion['dirRef2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono2'],
                    $validacion['ind_telefono2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_parentesco2'],
                    #DATOS LICENCIA
                    $validacion['fk_a006_num_miscelaneo_detalle_tipolic'],
                    $validacion['fk_a006_num_miscelaneo_detalle_claselic'],
                    $validacion['fec_expiracion_licencia'],
                    #IDS TELEFONOS DE EMERGENCIA
                    $validacion['pk_num_telefono1'],
                    $validacion['pk_num_telefono2'],
                    $validacion['pk_num_empleado_emergencia1'],
                    $validacion['pk_num_empleado_emergencia2'],
                    $validacion['pk_num_persona1'],
                    $validacion['pk_num_persona2'],
                    $validacion['pk_num_direccion_persona1'],
                    $validacion['pk_num_direccion_persona2'],
                    $validacion['pk_num_personalic'],
                    $validacion['pk_num_licencia'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio2'],
                    #MODIFICACION COMPLEMENTOS DIRECCION
                    $validacion['fk_a006_num_tipo_zona1'],
                    $validacion['fk_a006_num_ubicacion_inmueble1'],
                    $validacion['ind_ubicacion_inmueble1'],
                    $validacion['fk_a006_num_tipo_inmueble1'],
                    $validacion['ind_tipo_inmueble1'],
                    $validacion['fk_a006_num_zona_residencial1'],
                    $validacion['ind_zona_residencial1'],
                    $validacion['ind_inmueble1'],
                    $validacion['ind_punto_referencia1'],
                    $validacion['ind_zona_postal1'],

                    $validacion['fk_a006_num_tipo_zona2'],
                    $validacion['fk_a006_num_ubicacion_inmueble2'],
                    $validacion['ind_ubicacion_inmueble2'],
                    $validacion['fk_a006_num_tipo_inmueble2'],
                    $validacion['ind_tipo_inmueble2'],
                    $validacion['fk_a006_num_zona_residencial2'],
                    $validacion['ind_zona_residencial2'],
                    $validacion['ind_inmueble2'],
                    $validacion['ind_punto_referencia2'],
                    $validacion['ind_zona_postal2']
                );
                $organizacion = array(
                    $validacion['fk_a001_num_organismo'],
                    $validacion['fk_a004_num_dependencia'],
                    $validacion['fk_a023_num_centro_costo'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipopago'],
                    $validacion['num_tipo_trabajador']
                );
                $laborales    = array(
                    $validacion['fechaIngreso'],
                    $validacion['nroResolucion'],
                    $validacion['ind_estatus'],
                    $validacion['fk_a006_num_miscelaneo_detalle_grupoocupacional'],
                    $validacion['fk_rhc010_num_serie'],
                    $validacion['fk_rhc063_num_puestos_cargo'],
                    $validacion['fk_rhb007_num_horario'],
                    $validacion['num_sueldo_basico'],
                    $validacion['fk_a006_num_miscelaneo_detalle_paso']
                );

                $id = $this->atEmpleados->metModificarEmpleado($idEmpleado,$datosIniciales,$nacimiento,$domicilio,$datos,$organizacion,$laborales);
                $validacion['idEmpleado'] = $id;
                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    echo json_encode($validacion);
                    exit;
                }
            }

            #cuando el registro esta en estado preparado y se modificar
            if(strcmp($operacion,'modificarp')==0) {


                if(!isset($validacion['ind_estatus'])){
                    $validacion['ind_estatus']=0;
                }

                if(!isset($validacion['ind_estatus_persona'])){
                    $validacion['ind_estatus_persona']=0;
                }

                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodir'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodir']=0;
                }
                if(!isset($validacion['ind_direccion'])){
                    $validacion['ind_direccion']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono']=0;
                }
                if(!isset($validacion['ind_telefono'])){
                    $validacion['ind_telefono']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono1'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono1']=0;
                }
                if(!isset($validacion['ind_telefono1'])){
                    $validacion['ind_telefono1']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono2'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono2']=0;
                }
                if(!isset($validacion['ind_telefono2'])){
                    $validacion['ind_telefono2']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipolic'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipolic']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_claselic'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_claselic']=0;
                }
                if(!isset($validacion['fec_expiracion_licencia'])){
                    $validacion['fec_expiracion_licencia']=0;
                }
                if(!isset($validacion['cedRef1'])){
                    $validacion['cedRef1']=0;
                }
                if(!isset($validacion['cedRef2'])){
                    $validacion['cedRef2']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodirRef1'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef1']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodirRef2'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef2']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio1'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio1']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio2'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio2']=0;
                }
                if(!isset($validacion['pk_num_persona_direccion'])){
                    $validacion['pk_num_persona_direccion']=false;
                }
                if(!isset($validacion['pk_num_telefono'])){
                    $validacion['pk_num_telefono']=false;
                }
                if(!isset($validacion['pk_num_telefono1'])){
                    $validacion['pk_num_telefono1']=false;
                }
                if(!isset($validacion['pk_num_telefono2'])){
                    $validacion['pk_num_telefono2']=false;
                }
                if(!isset($validacion['pk_num_empleado_emergencia1'])){
                    $validacion['pk_num_empleado_emergencia1']=0;
                }
                if(!isset($validacion['pk_num_empleado_emergencia2'])){
                    $validacion['pk_num_empleado_emergencia2']=0;
                }
                if(!isset($validacion['pk_num_direccion_persona1'])){
                    $validacion['pk_num_direccion_persona1']=0;
                }
                if(!isset($validacion['pk_num_direccion_persona2'])){
                    $validacion['pk_num_direccion_persona2']=0;
                }
                if(!isset($validacion['pk_num_personalic'])){
                    $validacion['pk_num_personalic']=0;
                }
                if(!isset($validacion['pk_num_licencia'])){
                    $validacion['pk_num_licencia']=false;
                }
                if(!isset($validacion['aleatorio'])){
                    $validacion['aleatorio']='';
                }
                if(!isset($validacion['fk_a010_num_ciudad'])){
                    $validacion['fk_a010_num_ciudad']=false;
                }
                if(!isset($validacion['fk_a013_num_sector'])){
                    $validacion['fk_a013_num_sector']=false;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_gsanguineo']) || empty($validacion['fk_a006_num_miscelaneo_detalle_gsanguineo'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_gsanguineo']=NULL;
                }
                if(!isset($validacion['fk_a006_num_tipo_zona'])){
                    $validacion['fk_a006_num_tipo_zona']=false;
                }
                if(!isset($validacion['fk_a006_num_ubicacion_inmueble'])){
                    $validacion['fk_a006_num_ubicacion_inmueble']=false;
                }
                if(!isset($validacion['ind_ubicacion_inmueble'])){
                    $validacion['ind_ubicacion_inmueble']=false;
                }
                if(!isset($validacion['fk_a006_num_tipo_inmueble'])){
                    $validacion['fk_a006_num_tipo_inmueble']=false;
                }
                if(!isset($validacion['ind_tipo_inmueble'])){
                    $validacion['ind_tipo_inmueble']=false;
                }
                if(!isset($validacion['fk_a006_num_zona_residencial'])){
                    $validacion['fk_a006_num_zona_residencial']=false;
                }
                if(!isset($validacion['ind_zona_residencial'])){
                    $validacion['ind_zona_residencial']=false;
                }
                if(!isset($validacion['ind_inmueble'])){
                    $validacion['ind_inmueble']=false;
                }
                if(!isset($validacion['ind_punto_referencia'])){
                    $validacion['ind_punto_referencia']=false;
                }
                if(!isset($validacion['ind_zona_postal'])){
                    $validacion['ind_zona_postal']=false;
                }

                //arreglos de datos enviados por el formulario
                $datosIniciales = array(
                    $validacion['cedula'],
                    $validacion['persona'],
                    $validacion['nombre1'],
                    $validacion['nombre2'],
                    $validacion['apellido1'],
                    $validacion['apellido2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_sexo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_nacionalidad'],
                    $validacion['doc_fiscal'],
                    $validacion['email'],
                    $validacion['aleatorio']
                );

                $nacimiento   = array(
                    $validacion['fk_a008_num_pais'],
                    $validacion['fk_a009_num_estado'],
                    $validacion['fk_a011_num_municipio'],
                    $validacion['fk_a010_num_ciudad'],
                    $validacion['fechaNacimiento'],
                    $validacion['lugarNacimiento'],
                );

                $domicilio    = array(
                    $validacion['fk_a008_num_pais2'],
                    $validacion['fk_a009_num_estado2'],
                    $validacion['fk_a011_num_municipio2'],
                    $validacion['fk_a010_num_ciudad2'],
                    $validacion['parroquia'],
                    $validacion['fk_a013_num_sector'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodir'],
                    $validacion['ind_direccion'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono'],
                    $validacion['ind_telefono'],
                    $validacion['pk_num_persona_direccion'],
                    $validacion['pk_num_telefono'],
                    $validacion['fk_a006_num_tipo_zona'],
                    $validacion['fk_a006_num_ubicacion_inmueble'],
                    $validacion['ind_ubicacion_inmueble'],
                    $validacion['fk_a006_num_tipo_inmueble'],
                    $validacion['ind_tipo_inmueble'],
                    $validacion['fk_a006_num_zona_residencial'],
                    $validacion['ind_zona_residencial'],
                    $validacion['ind_inmueble'],
                    $validacion['ind_punto_referencia'],
                    $validacion['ind_zona_postal']
                );

                $datos        = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_gsanguineo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_edocivil'],
                    #DATOS REFERENCIA EMERGENCIA 1
                    $validacion['cedRef1'],
                    $validacion['nombRef1'],
                    $validacion['apeRef1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef1'],
                    $validacion['dirRef1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono1'],
                    $validacion['ind_telefono1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_parentesco'],
                    #DATOS REFERENCIA EMERGENCIA 2
                    $validacion['cedRef2'],
                    $validacion['nombRef2'],
                    $validacion['apeRef2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodirRef2'],
                    $validacion['dirRef2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono2'],
                    $validacion['ind_telefono2'],
                    $validacion['fk_a006_num_miscelaneo_detalle_parentesco2'],
                    #DATOS LICENCIA
                    $validacion['fk_a006_num_miscelaneo_detalle_tipolic'],
                    $validacion['fk_a006_num_miscelaneo_detalle_claselic'],
                    $validacion['fec_expiracion_licencia'],
                    #IDS TELEFONOS DE EMERGENCIA
                    $validacion['pk_num_telefono1'],
                    $validacion['pk_num_telefono2'],
                    $validacion['pk_num_empleado_emergencia1'],
                    $validacion['pk_num_empleado_emergencia2'],
                    $validacion['pk_num_persona1'],
                    $validacion['pk_num_persona2'],
                    $validacion['pk_num_direccion_persona1'],
                    $validacion['pk_num_direccion_persona2'],
                    $validacion['pk_num_personalic'],
                    $validacion['pk_num_licencia'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio1'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio2'],
                    #MODIFICACION COMPLEMENTOS DIRECCION
                    $validacion['fk_a006_num_tipo_zona1'],
                    $validacion['fk_a006_num_ubicacion_inmueble1'],
                    $validacion['ind_ubicacion_inmueble1'],
                    $validacion['fk_a006_num_tipo_inmueble1'],
                    $validacion['ind_tipo_inmueble1'],
                    $validacion['fk_a006_num_zona_residencial1'],
                    $validacion['ind_zona_residencial1'],
                    $validacion['ind_inmueble1'],
                    $validacion['ind_punto_referencia1'],
                    $validacion['ind_zona_postal1'],

                    $validacion['fk_a006_num_tipo_zona2'],
                    $validacion['fk_a006_num_ubicacion_inmueble2'],
                    $validacion['ind_ubicacion_inmueble2'],
                    $validacion['fk_a006_num_tipo_inmueble2'],
                    $validacion['ind_tipo_inmueble2'],
                    $validacion['fk_a006_num_zona_residencial2'],
                    $validacion['ind_zona_residencial2'],
                    $validacion['ind_inmueble2'],
                    $validacion['ind_punto_referencia2'],
                    $validacion['ind_zona_postal2']
                );
                $organizacion = array(
                    $validacion['fk_a001_num_organismo'],
                    $validacion['fk_a004_num_dependencia'],
                    $validacion['fk_a023_num_centro_costo'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipopago'],
                    $validacion['num_tipo_trabajador']
                );
                $laborales    = array(
                    $validacion['fechaIngreso'],
                    $validacion['nroResolucion'],
                    $validacion['ind_estatus'],
                    $validacion['fk_a006_num_miscelaneo_detalle_grupoocupacional'],
                    $validacion['fk_rhc010_num_serie'],
                    $validacion['fk_rhc063_num_puestos_cargo'],
                    $validacion['fk_rhb007_num_horario'],
                    $validacion['num_sueldo_basico'],
                    $validacion['fk_a006_num_miscelaneo_detalle_paso']
                );

                $id = $this->atEmpleados->metModificarEmpleadoPreparado($idEmpleado,$datosIniciales,$nacimiento,$domicilio,$datos,$organizacion,$laborales);
                $validacion['idEmpleado'] = $id;
                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='modificacionp';
                    echo json_encode($validacion);
                    exit;
                }

            }

            #cuando se visualiza el registro
            if(strcmp($operacion,'ver')==0){

                $validacion['status']='ver';

            }

            #para revisar el registro del empleado
            if(strcmp($operacion,'revisar')==0){

                $datos = array(
                    $validacion['fechaIngreso'],
                    $validacion['nroResolucion'],
                    $validacion['fk_a001_num_organismo'],
                    $validacion['fk_a004_num_dependencia'],
                    $validacion['fk_a023_num_centro_costo'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['fk_rhc063_num_puestos_cargo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_paso']
                );

                $resultado = $this->atEmpleados->metOperacionesEmpleado($idEmpleado,'revisar',$datos);

                if(is_array($resultado)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $resultado;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='revisar';
                    $validacion['idEmpleado'] = $idEmpleado;
                    echo json_encode($validacion);
                    exit;
                }

            }

            #para aprobar el registro del empleado
            if(strcmp($operacion,'aprobar')==0){

                $datos = array(
                    $validacion['fechaIngreso'],
                    $validacion['nroResolucion'],
                    $validacion['fk_a001_num_organismo'],
                    $validacion['fk_a004_num_dependencia'],
                    $validacion['fk_a023_num_centro_costo'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['fk_rhc063_num_puestos_cargo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_paso']
                );

                $resultado = $this->atEmpleados->metOperacionesEmpleado($idEmpleado,'aprobar',$datos);

                if(is_array($resultado)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $resultado;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='aprobar';
                    $validacion['idEmpleado'] = $idEmpleado;
                    echo json_encode($validacion);
                    exit;
                }
            }



        }

        if($idEmpleado!=0){
            $db1=$this->atEmpleados->metMostrarEmpleado($idEmpleado,'PorIdEmpleado','PRINCIPALES',false);
            $db2=$this->atEmpleados->metMostrarEmpleado($idEmpleado,'PorIdEmpleado','NACIMIENTO',false);
            $db3=$this->atEmpleados->metMostrarEmpleado($idEmpleado,'PorIdEmpleado','DOMICILIO',false);
            $db4=$this->atEmpleados->metMostrarEmpleado($idEmpleado,'PorIdEmpleado','LABORALES',false);
            $db5=$this->atEmpleados->metMostrarEmpleado($idEmpleado,'PorIdEmpleado','TELEFONO',false);
            $db6=$this->atEmpleados->metMostrarEmpleado($idEmpleado,'PorIdEmpleado','EMERGENCIA',false);
            $db7=$this->atEmpleados->metMostrarEmpleado($idEmpleado,'PorIdEmpleado','DIRECCION',false);
            $db8=$this->atEmpleados->metMostrarEmpleado($idEmpleado,'PorIdEmpleado','TELEFONO_EMERGENCIA','GROUP BY ind_cedula_documento');
            $db9=$this->atEmpleados->metMostrarEmpleado($idEmpleado,'PorIdEmpleado','LICENCIA',false);
            $db10=$this->atEmpleados->metMostrarEmpleado($idEmpleado,'PorIdEmpleado','SUELDO',false);
            $this->atVista->assign('formDBprincipales',$db1);
            $this->atVista->assign('formDBnacimiento',$db2);
            $this->atVista->assign('formDBdomicilio',$db3);
            $this->atVista->assign('formDBlaborales',$db4);
            $this->atVista->assign('formDBtelefono',$db5);
            $this->atVista->assign('formDBemergencia',$db6);
            $this->atVista->assign('formDBdireccion',$db7);
            $this->atVista->assign('formDBlicencia',$db9);
            $this->atVista->assign('formDBlaboralesSueldo',$db10);
            $this->atVista->assign('numeroD', 1);
            $this->atVista->assign('numeroT', 1);
            $this->atVista->assign('numero1', 1);
            $this->atVista->assign('numero2', 1);
            $this->atVista->assign('numerol', 1);
            $this->atVista->assign('n', 0);
            $this->atVista->assign('nt', 0);
            $this->atVista->assign('nt1', 0);
            $this->atVista->assign('nt2', 0);
            $this->atVista->assign('nl', 0);

            if(isset($db8[0]['pk_num_persona'])){
                $tlfemrg1=$this->atEmpleados->metMostrarEmpleado($db8[0]['pk_num_persona'],'PorIdPersona','TELEFONO_EMERGENCIA');
                $this->atVista->assign('formDBtlfemerg1',$tlfemrg1);
            }
            if(isset($db8[1]['pk_num_persona'])){
                $tlfemrg2=$this->atEmpleados->metMostrarEmpleado($db8[1]['pk_num_persona'],'PorIdPersona','TELEFONO_EMERGENCIA');
                $this->atVista->assign('formDBtlfemerg2',$tlfemrg2);
            }



        }

        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        /*VALORES POR DEFECTO*/
        $NACIONALIDAD = Session::metObtener('DEFAULTNACION');
        $PAIS = Session::metObtener('DEFAULTPAIS');
        $ESTADO = Session::metObtener('DEFAULTESTADO');
        $MUNICIPIO = Session::metObtener('DEFAULTMUNICIPIO');
        $CIUDAD = Session::metObtener('DEFAULTCIUDAD');
        $PARROQUIA = Session::metObtener('DEFAULTPARROQUIA');
        $SECTOR = Session::metObtener('DEFAULTSECTOR');
        $ORGANISMO = Session::metObtener('DEFAULTORG');
        $TIPONOMINA = Session::metObtener('DEFAULTTIPNOM');
        $TIPOPAGO = Session::metObtener('DEFAULTTIPPAGO');
        $TIPOTRABA = Session::metObtener('DEFAULTTIPTRAB');
        $HORARIO = Session::metObtener('DEFAULTHOR');
        $this->atVista->assign('DefaultNacionalidad',$NACIONALIDAD);
        $this->atVista->assign('DefaultPais',$PAIS);
        $this->atVista->assign('DefaultEstado',$ESTADO);
        $this->atVista->assign('DefaultMunicipio',$MUNICIPIO);
        $this->atVista->assign('DefaultCiudad',$CIUDAD);
        $this->atVista->assign('DefaultParroquia',$PARROQUIA);
        $this->atVista->assign('DefaultSector',$SECTOR);
        $this->atVista->assign('DefaultOrg',$ORGANISMO);
        $this->atVista->assign('DefaultTipNom',$TIPONOMINA);
        $this->atVista->assign('DefaultTipPag',$TIPOPAGO);
        $this->atVista->assign('DefaultTipTrab',$TIPOTRABA);
        $this->atVista->assign('DefaultHor',$HORARIO);

        /*MISCELANEOS EN EL FORMULARIO*/
        $miscelaneoSexo    = $this->atEmpleados->metMostrarSelect('SEXO');
        $miscelaneoTipoDoc = $this->atEmpleados->metMostrarSelect('DOCUMENTOS');
        $miscelaneoNacion  = $this->atEmpleados->metMostrarSelect('NACION');
        $miscelaneoSangre     = $this->atEmpleados->metMostrarSelect('SANGRE');
        $miscelaneoDomicilio  = $this->atEmpleados->metMostrarSelect('SITDOM');
        $miscelaneoEdoCivil   = $this->atEmpleados->metMostrarSelect('EDOCIVIL');
        $miscelaneoParentesco = $this->atEmpleados->metMostrarSelect('PARENT');
        $miscelaneoLicencia   = $this->atEmpleados->metMostrarSelect('TIPOLIC');
        $miscelaneoTelefono   = $this->atEmpleados->metMostrarSelect('TIPOTELF');
        $miscelaneoDireccion  = $this->atEmpleados->metMostrarSelect('TIPODIR');
        $miscelaneoClaselic   = $this->atEmpleados->metMostrarSelect('CLASELIC');
        /*para direccion*/
        $miscelaneoUbicacionInm  = $this->atEmpleados->metMostrarSelect('UBICINMUE');
        $miscelaneoTipoZona  = $this->atEmpleados->metMostrarSelect('TIPOZONA');
        $miscelaneoTipoInmueble = $this->atEmpleados->metMostrarSelect('TIPOINMU');
        $miscelaneoZonaResidencial = $this->atEmpleados->metMostrarSelect('ZONARESID');
        $this->atVista->assign('Sexo',$miscelaneoSexo);
        $this->atVista->assign('TipoDoc',$miscelaneoTipoDoc);
        $this->atVista->assign('Nacion',$miscelaneoNacion);
        $this->atVista->assign('GrupoSanguineo',$miscelaneoSangre);
        $this->atVista->assign('SituacionDomicilio',$miscelaneoDomicilio);
        $this->atVista->assign('EdoCivil',$miscelaneoEdoCivil);
        $this->atVista->assign('Parentesco',$miscelaneoParentesco);
        $this->atVista->assign('TipoLic',$miscelaneoLicencia);
        $this->atVista->assign('TipoTelf',$miscelaneoTelefono);
        $this->atVista->assign('TipoDir',$miscelaneoDireccion);
        $this->atVista->assign('ClaseLic',$miscelaneoClaselic);
        $this->atVista->assign('UbicacionInmueble',$miscelaneoUbicacionInm);
        $this->atVista->assign('TipoZona',$miscelaneoTipoZona);
        $this->atVista->assign('TipoInmueble',$miscelaneoTipoInmueble);
        $this->atVista->assign('ZonaResidencial',$miscelaneoZonaResidencial);
        /*MISCELANEOS EN EL FORMULARIO*/

        /*PARA LISTAR SELECT PAISES, ESTADO, MUNICIPIO, CIUDAD*/
        $this->atVista->assign('listadoPais',$this->atPaisModelo->metListarPais(1));
        $this->atVista->assign('listadoEstado',$this->atEstadoModelo->metJsonEstado($PAIS));
        $this->atVista->assign('listadoMunicipio',$this->atMunicipioModelo->metJsonMunicipio($ESTADO));
        $this->atVista->assign('listadoCiudad',$this->atCiudadModelo->metJsonCiudad($ESTADO));
        $this->atVista->assign('listadoParroquia',$this->atEmpleados->metJsonParroquia($MUNICIPIO));
        $this->atVista->assign('listadoSector',$this->atEmpleados->metJsonSector($PARROQUIA));
        /*PARA LISTAR SELECT PAISES, ESTADO, MUNICIPIO, CIUDAD*/

        /*DATOS DE LA ORGANIZACION*/
        $this->atVista->assign('listadoOrganismos',$this->atEmpleados->metListarOrganismos());
        $this->atVista->assign('listadoDependencias',$this->atDependencias->metConsultarDependencias(true));
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNomina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoHorario',$this->atHorario->metConsultarHorarioLaboral());
        $miscelaneoTipoPago   = $this->atEmpleados->metMostrarSelect('TIPOPAGO');
        $this->atVista->assign('TipoPago',$miscelaneoTipoPago);
        /*DATOS DE LA ORGANIZACION*/

        /*DATOS LABORALES*/
        $this->atVista->assign('listadoGrupoOcupacional',$this->atEmpleados->metMostrarSelect('GRUPOCUP'));
        /*DATOS LABORALES*/

        #PARA LLENAR EL SELECT PASOS
        $Pasos = $this->atPasos->metConsultarPasos();
        $this->atVista->assign('Pasos',$Pasos);


        $this->atVista->assign('operacion',$operacion);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('form', 'modales');
    }


    // Método que permite calcular el tiempo de servicio de un empleado
    public function metTiempoServicio($fechaIngreso)
    {
        $fechaInicial = new DateTime($fechaIngreso);
        $actualidad = new DateTime();
        $interval = $fechaInicial->diff($actualidad);
        $datosServicio = array(
            'anio' => $interval->format('%y'),
            'mes' => $interval->format('%m'),
            'dia' => $interval->format('%d')
        );
        return $datosServicio;
    }

    // Método que permite calcular el tiempo de servicio de un empleado entre 2 fechas dadas
    public function metTiempoServicioFecha($fechaIngreso,$fechaEgreso,$retorno)
    {
        $fechaInicial = new DateTime($fechaIngreso);
        $actualidad = new DateTime($fechaEgreso);
        $interval = $fechaInicial->diff($actualidad);
        $datosServicio = array(
            'anio' => $interval->format('%y'),
            'mes' => $interval->format('%m'),
            'dia' => $interval->format('%d')
        );
        if(strcmp($retorno,'json')==0){
            echo json_encode($datosServicio);
        }else{
            return $datosServicio;
        }

    }

    // Verifico los dias de derecho a vacaciones que posee el empleado
    public function metDiasDerecho($fechaIngreso, $pkNumEmpleado)
    {
        // Se verifica el parámetro activo para el manejo de las vacaciones
        $tiempoServicio = $this->metTiempoServicio($fechaIngreso);
        $anioTotalPrevio = $tiempoServicio['anio'];

        // Consulto si el empleado tiene Antecedentes de servicio *********
        $antecedente = $this->atVacacionModelo->metConsultarAntecedente($pkNumEmpleado);
        $acumuladorAntecedente = 0;
        $acumuladoMeses = 0;
        foreach ($antecedente as $ant){
            $fechaIngreso = $ant['fec_ingreso'];
            $fechaEgreso = $ant['fec_egreso'];
            $fechaInicial = new DateTime($fechaIngreso);
            $fechaFinal = new DateTime($fechaEgreso);
            $interval = $fechaInicial->diff($fechaFinal);
            $servicio = $interval->format('%y');
            $acumuladoMeses = $acumuladoMeses+$interval->format('%m');
            $acumuladorAntecedente = $acumuladorAntecedente + $servicio;

        }
        $acumuladoMeses = $acumuladoMeses/12;
        // Fin antecedente de servicio
        $anioTotal = $anioTotalPrevio + $acumuladorAntecedente + (int)$acumuladoMeses;
        // Consulto la tabla de vacaciones dependendiendo del año total del funcionario
        $consultarNomina = $this->atVacacionModelo->metConsultarNomina($pkNumEmpleado);
        $pkNumTipoNomina = $consultarNomina['fk_nmb001_num_tipo_nomina'];
        $consultarTabla = $this->atVacacionModelo->metConsultarTabla($anioTotal, $pkNumTipoNomina);
        if($consultarTabla['num_dias_disfrute']>0){
            $derecho = $consultarTabla['num_dias_disfrute'];
            $diasAdicionales = $consultarTabla['num_dias_adicionales'];
            $diasDerecho = $consultarTabla['num_total_disfrutar'];
        } else {
            $derecho = 0;
            $diasAdicionales = 0;
            $diasDerecho = 0;
        }
        $dato = array(
            'dias_derecho' => $diasDerecho,
            'dia_adicional' => $diasAdicionales,
            'derecho' => $derecho,
            'anioTotal' => $anioTotal
        );
        return $dato;
    }

    #operaciones relacionadas con HCM
    public function metOperacionHCM()
    {
        $empleado = $this->metObtenerInt('idEmpleado');
        $nombres  = $this->metObtenerTexto('nombres');
        $cod_empleado = $this->metObtenerTexto('cod_empleado');

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('Aplicacion/appFunciones','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        //$this->atOperaciones = $this->metCargarModelo('Hcm','procesos');
       // $this->atVista->assign('listadoOperaciones', $this->atOperaciones->metListarOperacionesHcm($empleado));

        $this->atVista->assign('Empleado',$empleado);
        $this->atVista->assign('Nombres',$nombres);
        $this->atVista->assign('CodEmpleado',$cod_empleado);
        $this->atVista->metRenderizar('hcm', 'modales');

    }

    public function metJsonDataTablaExtensiones($empleado)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
            select * from vl_rh_hcm_extension
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    (
                      ind_descripcion LIKE '%$busqueda[value]%'
                    )
                    and pk_num_empleado='$empleado'
            ";
        }else{
            $sql .="
                WHERE pk_num_empleado='$empleado'
            ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('fec_anio','ind_descripcion','descripcion_p','num_monto_predeterminado','num_monto_final','descripcion_d','num_monto_disminucion_predeterminado','num_monto_operacion','num_monto_disminucion_final');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_extension';
        #construyo el listado de botones

        $campos['boton']['Editar'] = '
                <button class="modificarExt logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal2"
                        data-keyboard="false" data-backdrop="static" title="Editar" idExtension="'.$clavePrimaria.'" titulo="HCM Modificar de Extensión">
                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTablaListadoExt($sql,$campos,$clavePrimaria);

    }

    public function metDataTablaListadoExt($sql,$listado,$clavePrimaria,$datosExtraBotones = false,$flagSistema = false)
    {

        #Instancio la clase principal del modelo
        $modelo = new Modelo();
        #Accedo a la base de datos desde el controlador
        $db = $modelo->metAccesoControladorDB();
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        #Parametros enviados desde el datatable
        $pagina = $this->metObtenerFormulas('draw');
        $limiteTabla = $this->metObtenerFormulas('length');
        $inicio = $this->metObtenerFormulas('start');
        $columnas = $this->metObtenerFormulas('columns');
        $orden = $this->metObtenerFormulas('order');
        $busqueda = $this->metObtenerFormulas('search');
        $busqueda = $busqueda['value'];
        $tituloColumna = $columnas[$orden[0]['column']]['data'];
        $ordenColumna = $orden[0]['dir'];
        if ($inicio == 0) {
            $inicio = '0';
        }
        if ($pagina > 1) {
            $inicio = ($inicio / $limiteTabla) + 1;
        }
        if ($inicio != 0) {
            $primerResultado = $limiteTabla * ($inicio - 1);
        } else {
            $primerResultado = 0;
        }
        #concateno el orden de la columna
        $sql .="ORDER BY $tituloColumna $ordenColumna ";
        $resultado = $db->prepare($sql);
        $resultado->execute();
        $numeroTotalRegistros = $resultado->rowCount();
        $arrayResultado['recordsTotal'] = $numeroTotalRegistros;
        $arrayResultado['recordsFiltered'] = $numeroTotalRegistros;
        $arrayResultado['draw'] = $pagina;
        $arrayResultado['length'] = $limiteTabla;
        #concateno el Limite de la consulta
        $sql .= "LIMIT $primerResultado, $limiteTabla ";
        $resultados2 = $db->prepare($sql);
        $resultados2->execute();
        $detalle = array();
        foreach ($resultados2->fetchAll(PDO::FETCH_ASSOC) AS $i) {

            #valido
            $i['num_monto_predeterminado'] = number_format($i['num_monto_predeterminado'],2,',','.');
            $i['num_monto_final'] = number_format($i['num_monto_final'],2,',','.');
            $i['num_monto_disminucion_predeterminado'] = number_format($i['num_monto_disminucion_predeterminado'],2,',','.');
            $i['num_monto_operacion'] = number_format($i['num_monto_operacion'],2,',','.');
            $i['num_monto_disminucion_final'] = number_format($i['num_monto_disminucion_final'],2,',','.');


            #recorro los datos
            foreach ($listado AS $titulo => $valor){
                if(!is_array($valor)){
                    $detalle[$valor]=$i[$valor];
                }else{
                    $detalle['acciones'] = '';
                    foreach ($listado[$titulo] AS $titulo2 => $valor2){
                        if($titulo == 'boton'){

                            if(is_array($valor2)){
                                $valorArreglo = str_replace("$clavePrimaria", $i[$clavePrimaria], $valor2[0]);
                                $valorEval = str_replace("#botonEval", $valorArreglo, $valor2[1]);

                                eval($valorEval);
                            }else{
                                #sustituyo el parametro del texto de la clave primaria en el boton
                                # por el valor extraido de la base de datos
                                $valor2 = str_replace("$clavePrimaria", $i[$clavePrimaria], $valor2);
                            }
                            if($datosExtraBotones){
                                foreach ($datosExtraBotones AS $datoExtraBoton){
                                    $valor2 = str_replace("$datoExtraBoton", $i[$datoExtraBoton], $valor2);
                                }
                            }
                            $detalle['acciones'] .= "$valor2 ";
                        }
                    }
                }
            }
            $arrayResultado['data'][]=$detalle;
        }
        if (!isset($arrayResultado['data'])) {
            $arrayResultado['data'] = array();
        }
        echo json_encode($arrayResultado);

    }

    public function metHcmControl()
    {
        $js = array('Aplicacion/appFunciones','materialSiace/App','materialSiace/core/demo/DemoFormComponents');
        $complementoJs = array(
            'inputmask/mask'
        );
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atHcm = $this->metCargarModelo('Hcm','procesos');

        $empleado = $this->metObtenerInt('idEmpleado');
        $idExtension = $this->metObtenerInt('idExtension');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   =$this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_monto_predeterminado')==0) || (strcmp($tituloInt,'num_monto_disminucion_predeterminado')==0) || (strcmp($tituloInt,'num_monto_disponible')==0) || (strcmp($tituloInt,'num_monto_operacion')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idExtension===0){

                $datos = array(
                    $validacion['fec_anio'],
                    $validacion['fk_rhc068_ayuda_especifica'],
                    $validacion['fk_rhb008_ayuda_global'],
                    $empleado,
                    $validacion['num_monto_predeterminado'],
                    $validacion['fk_rhc068_ayuda_especifica_disminucion'],
                    $validacion['num_monto_disminucion_predeterminado'],
                    $validacion['num_monto_disponible'],
                    $validacion['num_monto_operacion']
                );

                $id = $this->atHcm->metRegistrarExtension($datos);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='creacion';
                    $validacion['idExtension'] = $id;
                    echo json_encode($validacion);
                    exit;
                }


            }else{

                $datos = array(
                    $validacion['fec_anio'],
                    $validacion['fk_rhc068_ayuda_especifica'],
                    $validacion['fk_rhb008_ayuda_global'],
                    $empleado,
                    $validacion['num_monto_predeterminado'],
                    $validacion['fk_rhc068_ayuda_especifica_disminucion'],
                    $validacion['num_monto_disminucion_predeterminado'],
                    $validacion['num_monto_disponible'],
                    $validacion['num_monto_operacion']
                );

                $id = $this->atHcm->metModificarExtension($idExtension,$datos);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='modificar';
                    $validacion['idExtension'] = $id;
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idExtension!=0){
            $db=$this->atHcm->metMostrarExtension($idExtension);
            $this->atVista->assign('formDB',$db);
        }


        $anio = date("Y");

        $this->atOperaciones1 = $this->metCargarModelo('beneficiosGlobalHcm','maestros');
        $beneficioGlobal = $this->atOperaciones1->metMostrarBeneficioGlobalAnio($anio);
        $this->atVista->assign('listadoBeneficioGlobal',$beneficioGlobal);
        /********************************************************************************/
        $this->atOperaciones2 = $this->metCargarModelo('beneficiosHcm','maestros');
        $beneficioAsignaciones = $this->atOperaciones2->metConsultarBeneficiosHcm();
        $this->atVista->assign('listadoAsignaciones',$beneficioAsignaciones);

        $this->atVista->assign('idEmpleado',$empleado);
        $this->atVista->assign('idExtension',$idExtension);
        $this->atVista->assign('anio',$anio);
        $this->atVista->metRenderizar('hcmcontrol', 'modales');


    }


    #permite levantar ventana de las distintas operaciones del empleado
    public function metInformacionEmpleado()
    {
        $info = $this->metObtenerTexto('info');
        $empleado = $this->metObtenerInt('empleado');
        $nombres  = $this->metObtenerTexto('nombres');

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        //verifico la ventana / operacion que se va a ejecutar para carga el modelo correspondiente

        #VACACIONES DEL EMPLEADO
        if(strcmp($info,'vacaciones')==0){

            /*$this->atOperaciones = $this->metCargarModelo('permiso','gestion');*/
            /*$this->atVista->assign('listadoPermisos', $this->atOperaciones->metListarPermisoUsuario($empleado));*/

            $pkNumEmpleado = $empleado;

            if($pkNumEmpleado!=0) {

                $empleadoDato = $this->atVacacionModelo->metEmpleado($pkNumEmpleado);
                $funcionario = $empleadoDato['ind_nombre1'] . ' ' . $empleadoDato['ind_nombre2'] . ' ' . $empleadoDato['ind_apellido1'] . ' ' . $empleadoDato['ind_apellido2'];
                $fechaActual = date('d/m/Y');
                $periodo = $this->atVacacionModelo->metListarPeriodo($pkNumEmpleado, 1);
                $contador = count($periodo);
                $diasPendientes = 0;
                foreach($periodo as $per){
                    $diasPendientes = $diasPendientes + $per['num_pendientes'];
                }
                // Calculo el tiempo de servicio
                $fechaIngreso = $empleadoDato['fec_ingreso'];
                $tiempoServicio = $this->metTiempoServicio($fechaIngreso);
                $anio = $tiempoServicio['anio'];
                $diasDerecho = $this->metDiasDerecho($fechaIngreso, $pkNumEmpleado);
                $derecho = $diasDerecho['derecho'];
                $adicional = $diasDerecho['dia_adicional'];
                // Antecedentes de Servicio
                $antecedente = $this->atVacacionModelo->metConsultarAntecedente($pkNumEmpleado);
                $acumuladorAntecedente = 0;
                $acumuladoMeses = 0;
                foreach ($antecedente as $ant) {
                    $fechaIngresoAnt = $ant['fec_ingreso'];
                    $fechaEgreso = $ant['fec_egreso'];
                    $fechaInicial = new DateTime($fechaIngresoAnt);
                    $fechaFinal = new DateTime($fechaEgreso);
                    $interval = $fechaInicial->diff($fechaFinal);
                    $servicio = $interval->format('%y');
                    $acumuladoMeses = $acumuladoMeses+$interval->format('%m');
                    $acumuladorAntecedente = $acumuladorAntecedente + $servicio;
                }
                $acumuladoMeses = $acumuladoMeses/12;

                //Fin
                $empleado = array(
                    'fecha_ingreso' => $fechaIngreso,
                    'fecha_actual' => $fechaActual,
                    'codigo_empleado' => $empleadoDato['pk_num_empleado'],
                    'funcionario' => $funcionario,
                    'tiempo_servicio' => $anio,
                    'derecho' => $derecho,
                    'dias_adicionales' => $adicional,
                    'contador' => $contador,
                    'antecedente' => ($acumuladorAntecedente+(int)$acumuladoMeses),
                    'pendientes' => $diasPendientes
                );
                $this->atVista->assign('periodo', $periodo);
            }

            $this->atVista->assign('empleado', $empleado);


        }

        if(strcmp($info,'permisos')==0){
            $this->atOperaciones = $this->metCargarModelo('permiso','gestion');
            $this->atVista->assign('listadoPermisos', $this->atOperaciones->metListarPermisoUsuario($empleado));
        }

        #PATRIMONIO
        if(strcmp($info,'patrimonio')==0){
            $this->atOperaciones = $this->metCargarModelo('patrimonio','gestion');
            $this->atVista->assign('listadoInmueble', $this->atOperaciones->metListarPatrimonio($empleado,'INMUEBLE'));
            $this->atVista->assign('listadoInversion', $this->atOperaciones->metListarPatrimonio($empleado,'INVERSION'));
            $this->atVista->assign('listadoVehiculo', $this->atOperaciones->metListarPatrimonio($empleado,'VEHICULO'));
            $this->atVista->assign('listadoCuenta', $this->atOperaciones->metListarPatrimonio($empleado,'CUENTAS'));
            $this->atVista->assign('listadoOtros', $this->atOperaciones->metListarPatrimonio($empleado,'OTROS'));
            $this->atVista->assign('montosPatrimonio', $this->atOperaciones->metConsultarTotalesPatrimonios($empleado));
        }
        #INSTRUCCION DEL EMPLEADO
        if(strcmp($info,'instruccion')==0){
            $this->atOperaciones = $this->metCargarModelo('instruccionEmpleado','gestion');
            $this->atVista->assign('listadoCarreras', $this->atOperaciones->metListarInstruccion($empleado,'CARRERAS'));
            $this->atVista->assign('listadoOtros', $this->atOperaciones->metListarInstruccion($empleado,'OTROSESTUDIOS'));
            $this->atVista->assign('listadoIdiomas', $this->atOperaciones->metListarInstruccion($empleado,'IDIOMAS'));
        }
        #DOCUMENTOS DEL EMPLEADO
        if(strcmp($info,'documentos')==0){
            $this->atOperaciones = $this->metCargarModelo('documentos','gestion');
            $this->atVista->assign('listadoDocumentos', $this->atOperaciones->metListarDocumentos($empleado));
        }
        #REFERENCIAS DEL EMPLEADO
        if(strcmp($info,'referencias')==0){
            $this->atOperaciones = $this->metCargarModelo('referencias','gestion');
            $this->atVista->assign('listadoReferencias', $this->atOperaciones->metListarReferencias($empleado));
        }
        #MERITOS Y DEMERITOS
        if(strcmp($info,'meritosdemeritos')==0){
            $this->atOperaciones = $this->metCargarModelo('meritosDemeritos','gestion');
            $this->atVista->assign('listadoMeritosDemeritos', $this->atOperaciones->metListarMeritosDemeritos($empleado));
        }
        #EXPERIENCIA LABORAL
        if(strcmp($info,'experiencia')==0){
            $this->atOperaciones = $this->metCargarModelo('experienciaLaboral','gestion');
            $this->atVista->assign('listadoExperienciaLaboral', $this->atOperaciones->metListarExperienciaLaboral($empleado));
        }
        #INFORMACION BANCARIA
        if(strcmp($info,'infobanco')==0){
            $this->atOperaciones = $this->metCargarModelo('informacionBancaria','gestion');
            $this->atVista->assign('listadoInformacionBancaria', $this->atOperaciones->metListarInformacionBancaria($empleado));
        }
        #CARGA FAMILIAR
        if(strcmp($info,'carga')==0){
            $this->atOperaciones = $this->metCargarModelo('cargaFamiliar','gestion');
            $this->atVista->assign('listadoCargaFamiliar', $this->atOperaciones->metListarCargaFamiliar($empleado));
        }
        #IMPUESTO SOBRE LA RENTA
        if(strcmp($info,'impuesto')==0){
            $this->atOperaciones = $this->metCargarModelo('islr','gestion');
            $this->atVista->assign('listadoIslr', $this->atOperaciones->metListarIslr($empleado));
        }
        #CONTROL E HISTORIAL DE NIVELACIONES DEL EMPLEADO
        if(strcmp($info,'ctrnivelaciones')==0){
            $this->atOperaciones = $this->metCargarModelo('nivelaciones','gestion');
            $this->atVista->assign('listadoHistorialNivelaciones', $this->atOperaciones->metListarHistorialNivelaciones($empleado));
        }
        #HISTORIAL DE MODIFICACIONES DEL EMPLEADO
        if(strcmp($info,'historial')==0){
            $this->atOperaciones = $this->metCargarModelo('historial','gestion');
            $this->atVista->assign('listadoHistorial', $this->atOperaciones->metListarHistorial($empleado));
        }

        $this->atVista->assign('Empleado',$empleado);
        $this->atVista->assign('Nombres',$nombres);
        $this->atVista->metRenderizar($info, 'modales');
    }

    #permite visualizar la informacion del registro del empleado (quien registro, reviso y aprobo)
    public function metInformacionRegistro()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');

        $datos = $this->atEmpleados->metInformacionRegistroEmpleado($idEmpleado);

        $this->atVista->assign('datos',$datos);
        $this->atVista->metRenderizar('informacion','modales');
    }

    #permite listar los empleados para su revision y aprobacion
    public function metListarEmpleado($estatus)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','Aplicacion/appFunciones');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('estatus',$estatus);
        $this->atVista->metRenderizar('listado');
    }

    #data para llenar listado de empleados por revisar y aprobar
    public function metJsonDataTablaEmpleados($estatus)
    {

        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol = Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
                SELECT
                rh_b001_empleado.pk_num_empleado,
                rh_b001_empleado.fk_a003_num_persona,
                a003_persona.ind_cedula_documento,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                CONCAT_WS(' ',a003_persona.ind_nombre1,a003_persona.ind_nombre2,a003_persona.ind_apellido1,a003_persona.ind_apellido2) AS nombre_empleado,
                rh_b001_empleado.ind_estado_aprobacion,
                rh_b001_empleado.num_estatus,
                a004_dependencia.ind_dependencia,
                rh_c063_puestos.ind_descripcion_cargo,
                a003_persona.ind_foto
                FROM
                rh_b001_empleado
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
                INNER JOIN a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
                INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
         ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .= "
                WHERE
                    (
                      a003_persona.ind_cedula_documento LIKE '%$busqueda[value]%' OR
                      a003_persona.ind_nombre1 LIKE '%$busqueda[value]%' OR
                      a003_persona.ind_nombre2 LIKE '%$busqueda[value]%' OR
                      a003_persona.ind_apellido1 LIKE '%$busqueda[value]%' OR
                      a003_persona.ind_apellido2 LIKE '%$busqueda[value]%'
                    )
                    and rh_b001_empleado.ind_estado_aprobacion='$estatus'

            ";
        } else {
            $sql .= "
                WHERE rh_b001_empleado.ind_estado_aprobacion='$estatus'
            ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_foto','ind_cedula_documento', 'nombre_empleado', 'ind_dependencia', 'ind_descripcion_cargo', 'ind_estado_aprobacion');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_empleado';
        #construyo el listado de botones

        $campos['boton']['Info'] = '
                    <button class="info logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" idEmpleado="' . $clavePrimaria . '"
                                                    descipcion="El Usuario a consultado operaciones del empleado" title="Información de Registro de Empleado" titulo="Información de Registro de Empleado">
                                                <i class="md md-people" style="color: #ffffff;"></i>
                    </button>
          ';

        if ($estatus == 'PR') {

            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" title="Editar Empleado" idEmpleado="' . $clavePrimaria . '"
                                                    descipcion="El Usuario a Modificado Registro de Empleado" titulo="Modificar Registro del Empleado">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                ';
        }


        if (in_array('RH-01-01-01-06-07-R',$rol) AND $estatus == 'PR') {
            $campos['boton']['Revisar'] = '
                    <button class="revisar logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"
                                                            data-keyboard="false" data-backdrop="static" idEmpleado="'.$clavePrimaria.'"
                                                            descipcion="El Usuario a revisado datos de registro del Empleado" title="Revisar Registro de Empleado" titulo="Revisar Datos de Registro del Empleado">
                                                        <i class="md md-done" style="color: #ffffff;"></i>
                                                    </button>
                ';
        } else {
            $campos['boton']['Revisar'] = false;
        }
        if (in_array('RH-01-01-01-09-A',$rol) AND $estatus == 'RV') {
            $campos['boton']['Aprobar'] = '
                    <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"
                                                            data-keyboard="false" data-backdrop="static" idEmpleado="'.$clavePrimaria.'"
                                                            descipcion="El Usuario a aprobado datos de registro del Empleado" title="Aprobar Registro de Empleado" titulo="Aprobar Datos de Registro del Empleado">
                                                        <i class="md md-done" style="color: #ffffff;"></i>
                                                    </button>
                ';
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        #hago el llamado de la datatabla del controlador
        $this->metDataTablaListado($sql,$campos,$clavePrimaria);

    }

    public function metDataTablaListado($sql,$listado,$clavePrimaria,$datosExtraBotones = false,$flagSistema = false)
    {

        #Instancio la clase principal del modelo
        $modelo = new Modelo();
        #Accedo a la base de datos desde el controlador
        $db = $modelo->metAccesoControladorDB();
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        #Parametros enviados desde el datatable
        $pagina = $this->metObtenerFormulas('draw');
        $limiteTabla = $this->metObtenerFormulas('length');
        $inicio = $this->metObtenerFormulas('start');
        $columnas = $this->metObtenerFormulas('columns');
        $orden = $this->metObtenerFormulas('order');
        $busqueda = $this->metObtenerFormulas('search');
        $busqueda = $busqueda['value'];
        $tituloColumna = $columnas[$orden[0]['column']]['data'];
        $ordenColumna = $orden[0]['dir'];
        if ($inicio == 0) {
            $inicio = '0';
        }
        if ($pagina > 1) {
            $inicio = ($inicio / $limiteTabla) + 1;
        }
        if ($inicio != 0) {
            $primerResultado = $limiteTabla * ($inicio - 1);
        } else {
            $primerResultado = 0;
        }
        #concateno el orden de la columna
        $sql .="ORDER BY $tituloColumna $ordenColumna ";
        $resultado = $db->prepare($sql);
        $resultado->execute();
        $numeroTotalRegistros = $resultado->rowCount();
        $arrayResultado['recordsTotal'] = $numeroTotalRegistros;
        $arrayResultado['recordsFiltered'] = $numeroTotalRegistros;
        $arrayResultado['draw'] = $pagina;
        $arrayResultado['length'] = $limiteTabla;
        #concateno el Limite de la consulta
        $sql .= "LIMIT $primerResultado, $limiteTabla ";
        $resultados2 = $db->prepare($sql);
        $resultados2->execute();
        $detalle = array();
        foreach ($resultados2->fetchAll(PDO::FETCH_ASSOC) AS $i) {

            #valido
            if(isset($i['ind_estado_aprobacion'])){
                if($i['ind_estado_aprobacion']=='PR'){
                    $estado = 'Preparado';
                }else if($i['ind_estado_aprobacion']=='RV'){
                    $estado = 'Revisado';
                }
                else if($i['ind_estado_aprobacion']=='AP'){
                    $estado = 'Aprobado';
                }else{
                    $estado = 'Anulado';
                }
                $i['ind_estado_aprobacion'] = $estado;
                
                if(isset($i['ind_foto'])){
                    $foto = $i['ind_foto'];
                    $i['ind_foto'] = '<img class="img-circle width-0" src="'.BASE_URL.'publico/imagenes/modRH/fotos/'.$foto.'"/>';
                }else{
                    $i['ind_foto'] = '<img class="img-circle width-0" src="'.BASE_URL.'publico/imagenes/modRH/fotos/foto.png"/>';
                }                
            }

            #recorro los datos
            foreach ($listado AS $titulo => $valor){
                if(!is_array($valor)){
                    $detalle[$valor]=$i[$valor];
                }else{
                    $detalle['acciones'] = '';
                    foreach ($listado[$titulo] AS $titulo2 => $valor2){
                        if($titulo == 'boton'){

                            if(is_array($valor2)){
                                $valorArreglo = str_replace("$clavePrimaria", $i[$clavePrimaria], $valor2[0]);
                                $valorEval = str_replace("#botonEval", $valorArreglo, $valor2[1]);

                                eval($valorEval);
                            }else{
                                #sustituyo el parametro del texto de la clave primaria en el boton
                                # por el valor extraido de la base de datos
                                $valor2 = str_replace("$clavePrimaria", $i[$clavePrimaria], $valor2);
                            }
                            if($datosExtraBotones){
                                foreach ($datosExtraBotones AS $datoExtraBoton){
                                    $valor2 = str_replace("$datoExtraBoton", $i[$datoExtraBoton], $valor2);
                                }
                            }
                            $detalle['acciones'] .= "$valor2 ";
                        }
                    }
                }
            }
            $arrayResultado['data'][]=$detalle;
        }
        if (!isset($arrayResultado['data'])) {
            $arrayResultado['data'] = array();
        }
        echo json_encode($arrayResultado);



    }

    #PERMITE ABRIR FORMULARIO PARA INDICAR EL MOTIVO DE ANULACIÓN
    public function metIndicarMotivoAnulacion()
    {
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js= array('materialSiace/App','Aplicacion/appFunciones','materialSiace/core/demo/DemoFormComponents');
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $estado_nuevo  = $this->metObtenerTexto('estado_nuevo');
        $estado_actual = $this->metObtenerTexto('estado_actual');
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->assign('estado_nuevo',$estado_nuevo);
        $this->atVista->assign('estado_actual',$estado_actual);
        $this->atVista->metRenderizar('anular', 'modales');
    }

    #PERMITE ANULAR OPERACION DEL EMPLEADO
    public function metAnularOperacion()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $estado = $this->metObtenerTexto('estado_nuevo');
        $motivo = $_POST['motivo'];
        $anular = $this->atEmpleados->metAnularOperacion($idEmpleado,$estado,$motivo);
        if(is_array($anular)){
            $validacion['status'] = 'error';
            $validacion['detalleERROR'] = $anular;
            echo json_encode($validacion);
            exit;
        }else{
            $validacion['status']='anular';
            $validacion['idEmpleado'] = $idEmpleado;
            echo json_encode($validacion);
            exit;
        }
    }


    #para armar json centro de costos
    public function metJsonCentroCosto()
    {

        $idDependencia = $this->metObtenerInt('idDependencia');
        $centro_costo  = $this->atEmpleados->metJsonCentroCosto($idDependencia);
        echo json_encode($centro_costo);
        exit;

    }

    #para armar json parroquia
    public function metJsonParroquia()
    {

        $idMunicipio = $this->metObtenerInt('idMunicipio');
        $parroquia  = $this->atEmpleados->metJsonParroquia($idMunicipio);
        echo json_encode($parroquia);
        exit;

    }

    #para armar json sector
    public function metJsonSector()
    {

        $idParroquia = $this->metObtenerInt('idParroquia');
        $sector  = $this->atEmpleados->metJsonSector($idParroquia);
        echo json_encode($sector);
        exit;

    }

    #permite obtener la categoria del cargo y el sueldo del cargo
    public function metObtenerSueldoBasico()
    {
        #obtengo el valor pasado del formulario
        $Cargo = $this->metObtenerInt('idCargo');
        #conecto con el modelo y consulto los datos
        $datos = $this->atEmpleados->metObtenerSueldoBasico($Cargo);
        #los codifico con json para mostrarlos en la vista

        $sueldo = number_format($datos['num_sueldo_basico'],2,',','.');

        $categoria = $datos['ind_nombre_detalle'];

        $paso = $datos['fk_a006_num_miscelaneo_detalle_paso'];

        $cod = $datos['cod_detalle'];

        $data = array(
            'ind_nombre_detalle' => $categoria,
            'num_sueldo_basico'  => $sueldo,
            'paso' => $paso,
            'cod_detalle' => $cod
        );

        echo json_encode($data);
    }

    #permite buscar una persona por cedula o cod persona
    public function metBuscarPersona()
    {
        #obtengo los valores pasado del formulario
        $parametro = $this->metObtenerTexto('parametro');
        $busqueda  = $this->metObtenerTexto('busqueda');

        $datos = $this->atEmpleados->metBuscarPersona($parametro,$busqueda);

        echo json_encode($datos);

    }


    #permite copiar subir una imagen del empleado temporalmente
    public function metCopiarImagenTmp()
    {
        #carpeta temporal donde se va a copiar la imagen
        $destino =  "publico/imagenes/modRH/tmp/";
        $tipo    = $this->metObtenerTexto('tipo');

        if(strcmp($tipo,'FOTO')==0){/*fotos del empleado*/

            if (isset($_FILES["Foto"]))
            {

                $partes  = explode("." ,$_FILES["Foto"]["name"]);
                $img_f   = $this->atIdUsuario."_tmp_".$partes[0].".".$partes[1];
                $ruta    = $destino.$img_f;

                if (!file_exists($ruta)){

                    $resultado = move_uploaded_file($_FILES["Foto"]["tmp_name"], $ruta);
                    chmod($destino.$img_f, 0777);
                    if ($resultado){
                        echo $img_f;
                    } else {
                        echo "ocurrio un error al mover el archivo.";
                    }

                }else{

                    echo $img_f;
                }

            }

        }

        if(strcmp($tipo,'DOCUMENTOS')==0){/*documentos del empleado*/

            $empleado = $this->metObtenerInt('idEmpleado');

            if (isset($_FILES["Foto"]))
            {

                $partes  = explode("." ,$_FILES["Foto"]["name"]);
                $img_f   = $this->atIdUsuario."_".$empleado."_tmp_doc_".$partes[0].".".$partes[1];
                $ruta    = $destino.$img_f;

                if (!file_exists($ruta)){

                    $resultado = move_uploaded_file($_FILES["Foto"]["tmp_name"], $ruta);
                    chmod($destino.$img_f, 0777);
                    if ($resultado){
                        echo $img_f;
                    } else {
                        echo "ocurrio un error al mover el archivo.";
                    }

                }else{

                    echo $img_f;
                }

            }

        }


    }

    /*
     * PERMITE MOSTRAR LA VENTANA CON EL LISTADO DE EMPLEADOS
     * PARA SU SELECCION
     */
    public function metConsultarEmpleado()
    {

        #identifica la accion que se va a realizar
        $proceso = $this->metObtenerTexto('proceso');
        $accion  = $this->metObtenerTexto('accion');
        $tipo_busqueda = $this->metObtenerTexto('tipo_busqueda');

        $usuario = Session::metObtener('idUsuario');

        $datosTrabajador = $this->atEmpleados->metBuscarTrabajador($tipo_busqueda);

        $this->atVista->assign('datosTrabajador', $datosTrabajador);
        $this->atVista->assign('proceso',$proceso);
        $this->atVista->assign('accion',$accion);
        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->metRenderizar('buscarEmpleado', 'modales');

    }


    /*
     * PERMITE MOSTRAR LA VENTANA CON EL LISTADO DE EMPLEADOS
     * PARA SU SELECCION
     */
    /*public function metConsultarEmpleado()
    {

        #identifica la accion que se va a realizar
        $proceso = $this->metObtenerTexto('proceso');
        $accion  = $this->metObtenerTexto('accion');

        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];

        $listadoOrganismo = $this->atOrganismo->metListarOrganismo();

        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);
        $pkNumDependencia = $datosEmpleado['pk_num_dependencia'];

        $listadoEmpleados = $this->atDependencias->metDependenciaEmpleado($pkNumDependencia);
        $this->atVista->assign('empleado', $listadoEmpleados);

        $listadoDependencia = $this->atDependencias->metListarDependenciaOrganismo($datosEmpleado['pk_num_organismo'], 1, 0);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        // Organismo donde trabaja el empleado
        $empleado = array(
            'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
            'pk_num_dependencia' => $pkNumDependencia
        );
        $datosTrabajador = $this->atEmpleados->metBuscarTrabajador($datosEmpleado['pk_num_organismo'], $pkNumDependencia, '', '', '', '1', '1', '', '');

        $centroCosto = $this->atEmpleados->metListarCentroCosto();
        $tipoNomina  = $this->atTipoNomina->metListarTipoNomina();
        $tipoTrabajador = $this->atEmpleados->metMostrarSelect('TIPOTRAB');
        // Envío a la vista
        $this->atVista->assign('datosTrabajador', $datosTrabajador);
        $this->atVista->assign('centroCosto', $centroCosto);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('nomina', $tipoNomina);
        $this->atVista->assign('tipoTrabajador', $tipoTrabajador);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('proceso',$proceso);
        $this->atVista->assign('accion',$accion);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));
        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->metRenderizar('buscarEmpleado', 'modales');

    }*/

    // Método que permite listar empleados de acuerdo a la busqueda
    public function metBuscarTrabajador()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $pkNumDependencia = $_POST['pk_num_dependencia'];
        $centroCosto = $_POST['centro_costo'];
        $tipoNomina = $_POST['tipo_nomina'];
        $tipoTrabajador = $_POST['tipo_trabajador'];
        $estado_registro = $_POST['estado_registro'];
        $situacionTrabajo = $_POST['situacion_trabajo'];
        $fechaInicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];

        $datosTrabajador = $this->atEmpleados->metBuscarTrabajadorFiltro($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $tipoTrabajador, $estado_registro, $situacionTrabajo, $fechaInicio, $fecha_fin);
        echo json_encode($datosTrabajador);
    }

    //apliar foto del empleado
    public function metAmpliarFoto()
    {
        $foto = $this->metObtenerTexto('foto');

        $this->atVista->assign('foto',$foto);
        $this->atVista->metRenderizar('foto_empleado', 'modales');
    }

    /**************************************************************/
    #ventana modal que permite visualizar el reporte
    public function metGenerarReportePermiso()
    {
        $idEmpleado = $_POST['idEmpleado'];

        $this->atVista->assign('idEmpleado', $idEmpleado);
        $this->atVista->metRenderizar('permisosPDF','modales');
    }

    public function metGenerarReportePermisoPDF()
    {

        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraPermiso','modRH');
        $pdf= new pdfListado('P','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $idEmpleado = $_GET['idEmpleado'];

        $this->atPermisoModelo = $this->metCargarModelo('permiso','gestion');

        $empleado = $this->atPermisoModelo->metListarPermisoUsuario($idEmpleado);

        $pdf->Ln(35);
        $pdf->SetDrawColor(255, 255, 255);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetFont('Arial', 'BU', 8);
        $pdf->Ln(5);
        $pdf->Cell(170, 5, 'EMPLEADO: '.utf8_decode($empleado[0]['ind_nombre1']." ".$empleado[0]['ind_nombre2']." ".$empleado[0]['ind_apellido1']." ".$empleado[0]['ind_apellido2']), 0, 0, 'L', 0);
        $pdf->Ln(5);

        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(240, 240, 240);
        $pdf->SetWidths(array(20, 56, 40, 31, 31, 18));
        $pdf->SetAligns(array('C', 'L', 'C', 'C', 'C', 'C'));
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Row(array('Permiso',
            'Motivo de Ausencia',
            'Tipo de Evento',
            'Desde',
            'Hasta',
            'Estado'));
        $pdf->Ln(1);


        foreach ($empleado as $reg){


            $pdf->SetDrawColor(255, 255, 255);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetWidths(array(20, 56, 40, 17, 14, 17, 14, 18));
            $pdf->SetAligns(array('C', 'L', 'C', 'C', 'C', 'C', 'C', 'C'));
            $pdf->SetFont('Arial', '', 8);
            $pdf->Row(array($reg['pk_num_permiso'],
                utf8_decode($reg['motivo_ausencia']),
                utf8_decode($reg['tipo_ausencia']),
                $reg['fecha_salida'],
                $pdf->formatHora12($reg['fec_hora_salida']),
                $reg['fecha_entrada'],
                $pdf->formatHora12($reg['fec_hora_entrada']),
                $reg['ind_estado']
            ));
            $pdf->Ln(1);


        }


        $pdf->Output();


    }



    public function metEliminarDireccion()
    {
        $id = $this->metObtenerInt('id');

        if($id!=0){
            $id=$this->atEmpleados->metEliminarDireccion($id);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'id'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metEliminarTelefono()
    {
        $id = $this->metObtenerInt('id');

        if($id!=0){
            $id=$this->atEmpleados->metEliminarTelefono($id);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'id'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metEliminarLicencia()
    {
        $id = $this->metObtenerInt('id');

        if($id!=0){
            $id=$this->atEmpleados->metEliminarLicencia($id);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'id'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

}
