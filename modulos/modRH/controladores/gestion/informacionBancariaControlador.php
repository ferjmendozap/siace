<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class informacionBancariaControlador extends Controlador
{
    private $atEmpleados;
    private $atInfoBancaria;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados     = $this->metCargarModelo('empleados','gestion');
        $this->atInfoBancaria  = $this->metCargarModelo('informacionBancaria','gestion');
     }

    #Metodo Index del controlador listado de carga familiar del empleado
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoEmpleados', $this->atEmpleados->metListarEmpleado('AP','listado'));
        $this->atVista->assign('n', 1);
        $this->atVista->metRenderizar('empleados');
    }

    public function metInformacionBancaria()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents');
        $complementoJs = array(
            'inputmask/mask',
        );
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idInformacionBancaria = $this->metObtenerInt('idInformacionBancaria');
        $idEmpleado      = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');


        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_sueldo_mensual')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idInformacionBancaria===0){//PARA INSERTAR REGISTRO

                $validacion['status']='creacion';

                if(!isset($validacion['num_flag_principal'])){
                    $validacion['num_flag_principal']=0;
                }
                #INSTANCIO EL METODO PARA REGISTRAR LA INFORMACION BANCARIA

               $id = $this->atInfoBancaria->metRegistrarInformacionBancaria($idEmpleado,
                    $validacion['fk_a006_num_miscelaneo_detalle_banco'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipocta'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipoapo'],
                    $validacion['ind_cuenta'],
                    '0.00',
                    $validacion['num_flag_principal']
                );
                $validacion['idInformacionBancaria'] = $id;

                if(is_array($id)){

                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atInfoBancaria->metMostrarInformacionBancaria($id);
                    $validacion['banco'] = $datos['banco'];
                    $validacion['tipocta'] = $datos['tipocta'];
                    $validacion['ind_cuenta'] = $datos['ind_cuenta'];
                    $validacion['tipoapo'] = $datos['tipoapo'];
                    echo json_encode($validacion);
                    exit;
                }


            }else{//PARA MODIFICAR REGISTRO

                $validacion['status']='modificar';

                if(!isset($validacion['num_flag_principal'])){
                    $validacion['num_flag_principal']=0;
                }
                #INSTANCIO EL METODO PARA MODIFICAR LA INFORMACION BANCARIA
                $id = $this->atInfoBancaria->metModificarInformacionBancaria($idInformacionBancaria,
                    $validacion['fk_a006_num_miscelaneo_detalle_banco'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipocta'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipoapo'],
                    $validacion['ind_cuenta'],
                    '0.00',
                    $validacion['num_flag_principal']
                );
                $validacion['idInformacionBancaria'] = $idInformacionBancaria;

                if(is_array($id)){

                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atInfoBancaria->metMostrarInformacionBancaria($id);
                    $validacion['banco'] = $datos['banco'];
                    $validacion['tipocta'] = $datos['tipocta'];
                    $validacion['ind_cuenta'] = $datos['ind_cuenta'];
                    $validacion['tipoapo'] = $datos['tipoapo'];
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idInformacionBancaria!=0){
            $db=$this->atInfoBancaria->metMostrarInformacionBancaria($idInformacionBancaria);
            $this->atVista->assign('formDB',$db);
        }

        $miscelaneoBanco   = $this->atEmpleados->metMostrarSelect('BANCOS');
        $miscelaneoTipoCta = $this->atEmpleados->metMostrarSelect('TIPOCTA');
        $miscelaneoTipoApo = $this->atEmpleados->metMostrarSelect('TIPOAPO');
        $this->atVista->assign('Banco',$miscelaneoBanco);
        $this->atVista->assign('TipoCta',$miscelaneoTipoCta);
        $this->atVista->assign('TipoApo',$miscelaneoTipoApo);

        $this->atVista->assign('idInformacionBancaria',$idInformacionBancaria);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('form', 'modales');
    }


    public function metEliminarInformacionBancaria()
    {
        $idInformacionBancaria = $this->metObtenerInt('idInformacionBancaria');
        if($idInformacionBancaria!=0){
            $id=$this->atInfoBancaria->metEliminarInformacionBancaria($idInformacionBancaria);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'idInformacionBancaria'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

}
