<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class instruccionEmpleadoControlador extends Controlador
{
    private $atEmpleados;
    private $atInstruccion;
    private $atNivelesGradoInst;
    private $atProfesiones;
    private $atCentroEstudio;
    private $atIdiomas;
    private $atCursos;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados   = $this->metCargarModelo('empleados','gestion');
        $this->atInstruccion = $this->metCargarModelo('instruccionEmpleado','gestion');
        $this->atNivelesGradoInst = $this->metCargarModelo('nivelGradoInstruccion','maestros');
        $this->atProfesiones = $this->metCargarModelo('profesiones','maestros');
        $this->atCentroEstudio = $this->metCargarModelo('centroEstudios','maestros');
        $this->atIdiomas = $this->metCargarModelo('idiomas','maestros');
        $this->atCursos  = $this->metCargarModelo('cursos','maestros');

     }

    #Metodo Index del controlador listado de carga familiar del empleado
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoEmpleados', $this->atEmpleados->metListarEmpleado('AP','listado'));
        $this->atVista->assign('n', 1);
        $this->atVista->metRenderizar('empleados');
    }


    public function metCarreras()
    {

        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents');
        $complementosCss = array(
            'select2/select201ef'
        );
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idInstruccionCarreras = $this->metObtenerInt('idInstruccionCarreras');
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();
            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt]=$valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idInstruccionCarreras===0){
                $validacion['status']='creacion';

                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_colegiatura']) || empty($validacion['fk_a006_num_miscelaneo_detalle_colegiatura'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_colegiatura']=NULL;
                }

                $datos = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_gradoinst'],
                    $validacion['fk_rhc064_num_nivel_grado'],
                    $validacion['fk_a006_num_miscelaneo_detalle_area'],
                    $validacion['fk_rhc057_num_profesion'],
                    $validacion['fk_rhc040_num_institucion'],
                    $validacion['fec_graduacion'],
                    $validacion['fec_ingreso'],
                    $validacion['fec_egreso'],
                    $validacion['fk_a006_num_miscelaneo_detalle_colegiatura'],
                    $validacion['ind_colegiatura'],
                    $validacion['txt_observaciones']
                );
                #INSTANCIO EL METODO PARA REGISTRAR LA CARRERA
                $id = $this->atInstruccion->metRegistrarInstruccion($idEmpleado,'CARRERAS',$datos);
                $validacion['idInstruccionCarreras'] = $id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atInstruccion->metMostrarInstruccion($id,'CARRERAS');
                    $validacion['grado_instruccion'] = $datos['grado_instruccion'];
                    $validacion['ind_nombre_profesion'] = $datos['ind_nombre_profesion'];
                    $validacion['ind_nombre_institucion'] = $datos['ind_nombre_institucion'];
                    echo json_encode($validacion);
                    exit;
                }

            }else{
                $validacion['status']='modificar';


                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_colegiatura']) || empty($validacion['fk_a006_num_miscelaneo_detalle_colegiatura'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_colegiatura']=NULL;
                }

                $datos = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_gradoinst'],
                    $validacion['fk_rhc064_num_nivel_grado'],
                    $validacion['fk_a006_num_miscelaneo_detalle_area'],
                    $validacion['fk_rhc057_num_profesion'],
                    $validacion['fk_rhc040_num_institucion'],
                    $validacion['fec_graduacion'],
                    $validacion['fec_ingreso'],
                    $validacion['fec_egreso'],
                    $validacion['fk_a006_num_miscelaneo_detalle_colegiatura'],
                    $validacion['ind_colegiatura'],
                    $validacion['txt_observaciones']
                );

                #INSTANCIO EL METODO PARA MODIFICAR LA CARRERA
                $id = $this->atInstruccion->metModificarInstruccion($idInstruccionCarreras,'CARRERAS',$datos);
                $validacion['idInstruccionCarreras'] = $idInstruccionCarreras;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atInstruccion->metMostrarInstruccion($id,'CARRERAS');
                    $validacion['grado_instruccion'] = $datos['grado_instruccion'];
                    $validacion['ind_nombre_profesion'] = $datos['ind_nombre_profesion'];
                    $validacion['ind_nombre_institucion'] = $datos['ind_nombre_institucion'];
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idInstruccionCarreras!=0){
            $db=$this->atInstruccion->metMostrarInstruccion($idInstruccionCarreras,'CARRERAS');
            $this->atVista->assign('formDB',$db);

            $NivelGrado = $this->atNivelesGradoInst->metMostrarNivelesGradoInst($db['fk_a006_num_miscelaneo_detalle_gradoinst']);
            $this->atVista->assign('NivelGrado',$NivelGrado);

            $Profesiones = $this->atProfesiones->metMostrarProfesionesGradoArea($db['fk_a006_num_miscelaneo_detalle_gradoinst'],$db['fk_a006_num_miscelaneo_detalle_area']);
            $this->atVista->assign('Profesiones',$Profesiones);
        }

        $miscelaneoGradoInst = $this->atEmpleados->metMostrarSelect('GRADOINST');
        $this->atVista->assign('listadoGradoInstruccion',$miscelaneoGradoInst);
        $miscelaneoArea = $this->atEmpleados ->metMostrarSelect('AREA');
        $this->atVista->assign('Area',$miscelaneoArea);
        $centros = $this->atCentroEstudio->metConsultarCentros();
        $this->atVista->assign('Centros',$centros);
        $miscelaneoColegiatura = $this->atEmpleados ->metMostrarSelect('COLEGIOS');
        $this->atVista->assign('Colegiatura',$miscelaneoColegiatura);

        $this->atVista->assign('idInstruccionCarreras',$idInstruccionCarreras);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('formCarreras', 'modales');
    }


    public function metOtrosEstudios()
    {
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents');
        $complementosCss = array(
            'select2/select201ef'
        );
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idInstruccionOtrosEstudios = $this->metObtenerInt('idInstruccionOtrosEstudios');
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();
            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt]=$valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if(!isset($validacion['fk_rhc048_num_curso']) || empty($validacion['fk_rhc048_num_curso'])){
                $validacion['status'] = 'errorCurso';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['fk_rhc040_num_institucion']) || empty($validacion['fk_rhc040_num_institucion'])){
                $validacion['status'] = 'errorInstitucion';
                echo json_encode($validacion);
                exit;
            }

            if($idInstruccionOtrosEstudios===0){
                $validacion['status']='creacion';

                $datos = array(
                    $validacion['fk_rhc048_num_curso'],
                    $validacion['fk_rhc040_num_institucion'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipocurso'],
                    $validacion['num_horas'],
                    $validacion['fec_desde'],
                    $validacion['fec_hasta'],
                    $validacion['periodo_cul_anio'],
                    $validacion['periodo_cul_mes'],
                    $validacion['txt_observaciones']
                );
                #INSTANCIO EL METODO PARA REGISTRAR EL OTROS ESTUDIOS
                $id = $this->atInstruccion->metRegistrarInstruccion($idEmpleado,'OTROSESTUDIOS',$datos);
                $validacion['idInstruccionOtrosEstudios'] = $id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atInstruccion->metMostrarInstruccion($id,'OTROSESTUDIOS');
                    $aux1 = explode("-",$datos['fec_desde']);
                    $aux2 = explode("-",$datos['fec_hasta']);
                    $validacion['ind_nombre_curso'] = $datos['ind_nombre_curso'];
                    $validacion['ind_nombre_institucion'] = $datos['ind_nombre_institucion'];
                    $validacion['num_horas'] = $datos['num_horas'];
                    $validacion['fec_desde'] = $aux1[2]."-".$aux1[1]."-".$aux1[0];
                    $validacion['fec_hasta'] = $aux2[2]."-".$aux2[1]."-".$aux2[0];
                    echo json_encode($validacion);
                    exit;
                }

            }else{
                $validacion['status']='modificar';

                $datos = array(
                    $validacion['fk_rhc048_num_curso'],
                    $validacion['fk_rhc040_num_institucion'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipocurso'],
                    $validacion['num_horas'],
                    $validacion['fec_desde'],
                    $validacion['fec_hasta'],
                    $validacion['periodo_cul_anio'],
                    $validacion['periodo_cul_mes'],
                    $validacion['txt_observaciones']
                );

                #INSTANCIO EL METODO PARA MODIFICAR OTROS ESTUDIOS
                $id = $this->atInstruccion->metModificarInstruccion($idInstruccionOtrosEstudios,'OTROSESTUDIOS',$datos);
                $validacion['idInstruccionOtrosEstudios'] = $idInstruccionOtrosEstudios;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atInstruccion->metMostrarInstruccion($id,'OTROSESTUDIOS');
                    $aux1 = explode("-",$datos['fec_desde']);
                    $aux2 = explode("-",$datos['fec_hasta']);
                    $validacion['ind_nombre_curso'] = $datos['ind_nombre_curso'];
                    $validacion['ind_nombre_institucion'] = $datos['ind_nombre_institucion'];
                    $validacion['num_horas'] = $datos['num_horas'];
                    $validacion['fec_desde'] = $aux1[2]."-".$aux1[1]."-".$aux1[0];
                    $validacion['fec_hasta'] = $aux2[2]."-".$aux2[1]."-".$aux2[0];
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idInstruccionOtrosEstudios!=0){
            $db=$this->atInstruccion->metMostrarInstruccion($idInstruccionOtrosEstudios,'OTROSESTUDIOS');
            $this->atVista->assign('formDB',$db);
            $aux = explode("-",$db['ind_periodo']);
            $this->atVista->assign('anio',$aux[0]);
            $this->atVista->assign('mes',$aux[1]);

        }

        $arrayAnio = array(
            '2016' => '2016',
            '2015' => '2015',
            '2014' => '2014',
            '2013' => '2013',
            '2012' => '2012',
            '2011' => '2011',
            '2010' => '2010',
        );
        $arrayMes = array(
            '12' => '12',
            '11' => '11',
            '10' => '10',
            '09' => '09',
            '08' => '08',
            '07' => '07',
            '06' => '06',
            '05' => '05',
            '04' => '04',
            '03' => '03',
            '02' => '02',
            '01' => '01',
        );
        $this->atVista->assign('arrayAnio',$arrayAnio);
        $this->atVista->assign('arrayMes',$arrayMes);

        $cursos = $this->atCursos->metConsultarCursos();
        $this->atVista->assign('Cursos',$cursos);
        $centros = $this->atCentroEstudio->metConsultarCentros();
        $this->atVista->assign('Centros',$centros);
        $tipoCurso = $this->atEmpleados->metMostrarSelect('TIPOCURSO');
        $this->atVista->assign('tipoCurso',$tipoCurso);

        $this->atVista->assign('idInstruccionOtrosEstudios',$idInstruccionOtrosEstudios);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('formOtrosEstudios', 'modales');
    }


    public function metIdiomas()
    {
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents');
        $complementosCss = array(
            'select2/select201ef'
        );
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idInstruccionIdiomas = $this->metObtenerInt('idInstruccionIdiomas');
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();
            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt]=$valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idInstruccionIdiomas===0){
                $validacion['status']='creacion';

                $datos = array(
                    $validacion['fk_rhc013_num_idioma'],
                    $validacion['fk_a006_num_miscelaneo_detalle_lectura'],
                    $validacion['fk_a006_num_miscelaneo_detalle_oral'],
                    $validacion['fk_a006_num_miscelaneo_detalle_escritura'],
                    $validacion['fk_a006_num_miscelaneo_detalle_general']
                );
                #INSTANCIO EL METODO PARA REGISTRAR EL IDIOMAS
                $id = $this->atInstruccion->metRegistrarInstruccion($idEmpleado,'IDIOMAS',$datos);
                $validacion['idInstruccionIdiomas'] = $id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atInstruccion->metMostrarInstruccion($id,'IDIOMAS');
                    $validacion['ind_nombre_idioma'] = $datos['ind_nombre_idioma'];
                    $validacion['lectura'] = $datos['lectura'];
                    $validacion['oral'] = $datos['oral'];
                    $validacion['escritura'] = $datos['escritura'];
                    $validacion['general'] = $datos['general'];
                    echo json_encode($validacion);
                    exit;
                }

            }else{
                $validacion['status']='modificar';

                $datos = array(
                    $validacion['fk_rhc013_num_idioma'],
                    $validacion['fk_a006_num_miscelaneo_detalle_lectura'],
                    $validacion['fk_a006_num_miscelaneo_detalle_oral'],
                    $validacion['fk_a006_num_miscelaneo_detalle_escritura'],
                    $validacion['fk_a006_num_miscelaneo_detalle_general']
                );

                #INSTANCIO EL METODO PARA MODIFICAR EL IDOMAS
                $id = $this->atInstruccion->metModificarInstruccion($idInstruccionIdiomas,'IDIOMAS',$datos);
                $validacion['idInstruccionIdiomas'] = $idInstruccionIdiomas;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atInstruccion->metMostrarInstruccion($id,'IDIOMAS');
                    $validacion['ind_nombre_idioma'] = $datos['ind_nombre_idioma'];
                    $validacion['lectura'] = $datos['lectura'];
                    $validacion['oral'] = $datos['oral'];
                    $validacion['escritura'] = $datos['escritura'];
                    $validacion['general'] = $datos['general'];
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idInstruccionIdiomas!=0){
            $db=$this->atInstruccion->metMostrarInstruccion($idInstruccionIdiomas,'IDIOMAS');
            $this->atVista->assign('formDB',$db);
        }

        $idiomas = $this->atIdiomas->metConsultarIdiomas();
        $this->atVista->assign('idiomas',$idiomas);
        $nivelConocimiento = $this->atEmpleados ->metMostrarSelect('NIVEL');
        $this->atVista->assign('nivel',$nivelConocimiento);

        $this->atVista->assign('idInstruccionIdiomas',$idInstruccionIdiomas);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->metRenderizar('formIdiomas', 'modales');

    }


    #para actualizar el select nivel de acuerdo al grado de instruccion
    public function metActualizarNivel()
    {
        #obtengo el valor pasado del formulario
        $GradoInstruccion = $this->metObtenerInt('GradoInstruccion');
        #conecto con el modelo y consulto los datos
        $datos = $this->atNivelesGradoInst->metMostrarNivelesGradoInst($GradoInstruccion);
        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html="";
        for($i=0;$i<sizeof($datos);$i++){
            $html.= "<option value=".$datos[$i]['pk_num_nivel_grado'].">".$datos[$i]['ind_nombre_nivel_grado']."</option>";
        }
        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;
    }

    #para actualizar el select profesion de acuerdo al grado de instruccion y el area de formacion
    public function metActualizarProfesion()
    {
        #obtengo el valor pasado del formulario
        $Area = $this->metObtenerInt('Area');
        $GradoInstruccion = $this->metObtenerInt('GradoInstruccion');
        #conecto con el modelo y consulto los datos
        $datos = $this->atProfesiones->metMostrarProfesionesGradoArea($GradoInstruccion,$Area);
        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html="";
        for($i=0;$i<sizeof($datos);$i++){
            $html.= "<option value=".$datos[$i]['pk_num_profesion'].">".$datos[$i]['ind_nombre_profesion']."</option>";
        }
        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;
    }

    public function metEliminarInstruccion()
    {

        $tipo = $this->metObtenerTexto('tipo');

        if(strcmp($tipo,'CARRERAS')==0){

            $idInstruccionCarreras = $this->metObtenerInt('idInstruccionCarreras');

            if($idInstruccionCarreras!=0){
                $id=$this->atInstruccion->metEliminarInstruccion($idInstruccionCarreras,'CARRERAS');
                if(is_array($id)){
                    $valido=array(
                        'status'=>'error',
                        'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                    );
                }else{
                    $valido=array(
                        'status'=>'OK',
                        'idInstruccionCarreras'=>$id
                    );
                }
            }
            echo json_encode($valido);
            exit;

        }
        if(strcmp($tipo,'OTROSESTUDIOS')==0){

            $idInstruccionOtrosEstudios = $this->metObtenerInt('idInstruccionOtrosEstudios');

            if($idInstruccionOtrosEstudios!=0){
                $id=$this->atInstruccion->metEliminarInstruccion($idInstruccionOtrosEstudios,'OTROSESTUDIOS');
                if(is_array($id)){
                    $valido=array(
                        'status'=>'error',
                        'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                    );
                }else{
                    $valido=array(
                        'status'=>'OK',
                        'idInstruccionOtrosEstudios'=>$id
                    );
                }
            }
            echo json_encode($valido);
            exit;

        }
        if(strcmp($tipo,'IDIOMAS')==0){

            $idInstruccionIdioma = $this->metObtenerInt('idInstruccionIdioma');

            if($idInstruccionIdioma!=0){
                $id=$this->atInstruccion->metEliminarInstruccion($idInstruccionIdioma,'IDIOMAS');
                if(is_array($id)){
                    $valido=array(
                        'status'=>'error',
                        'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                    );
                }else{
                    $valido=array(
                        'status'=>'OK',
                        'idInstruccionIdioma'=>$id
                    );
                }
            }
            echo json_encode($valido);
            exit;

        }

    }

}
