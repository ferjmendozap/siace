<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Gestión de Permisos / Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de permisos solicitados por funcionarios  
class permisoControlador extends Controlador
{
	private $atPermisoModelo;

	public function __construct() 
	{
		parent::__construct();
		Session::metAcceso();
		$this->atPermisoModelo = $this->metCargarModelo('permiso','gestion');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker'
		);
		$complementosJs = array('select2/select2.min','bootstrap-datepicker/bootstrap-datepicker','inputmask/jquery.inputmask.bundle.min');
		$js = array('materialSiace/core/demo/DemoTableDynamic', 'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);

		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$obtenerEmpleado = $this->atPermisoModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
		$empleado = array(
			'ind_nombre1' => $obtenerEmpleado['ind_nombre1'],
			'ind_nombre2' => $obtenerEmpleado['ind_nombre2'],
			'ind_apellido1' => $obtenerEmpleado['ind_apellido1'],
			'ind_apellido2' => $obtenerEmpleado['ind_apellido2'],
			'pk_num_empleado' => $obtenerEmpleado['pk_num_empleado'],
			'estado' => 'LI'
		);
		$datosEmpleado = $this->atPermisoModelo->metDependenciaEmpleado($pkNumEmpleado);
		$empleadoDato = array(
			'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
			'ind_descripcion_empresa' => $datosEmpleado['ind_descripcion_empresa'],
			'pk_num_dependencia' => $datosEmpleado['pk_num_dependencia'],
			'ind_dependencia' => $datosEmpleado['ind_dependencia'],
			'pk_num_empleado' => $datosEmpleado['pk_num_empleado']
		);
		$busqueda = array(
			'pk_num_permiso' => '',
			'fecha_inicio' => '',
			'ind_estado' => ''
		);
		$this->atVista->assign('busqueda', $busqueda);
		$this->atVista->assign('empleado', $empleado);
		$this->atVista->assign('emp', $empleadoDato);
		$this->atVista->assign('permisoFuncionario', $this->atPermisoModelo->metListarPermisoUsuario($pkNumEmpleado));
		$this->atVista->metRenderizar('listado');
	}

	// Método encargado de la solicitud de permiso directamente desde el menú principal
	public function metCrearPermiso()
	{

		$valido = $this->metObtenerInt('valido');
		$usuario = Session::metObtener('idUsuario');
		if (isset($valido) && $valido == 1) {
			// $this->metValidarToken();
			$pk_num_empleado = $_POST['pk_num_empleado'];
			$pk_num_empleado_aprueba = $_POST['pk_num_empleado_aprueba'];
			$motivo_ausencia = $_POST['motivo_ausencia'];
			$tipo_ausencia = $_POST['tipo_ausencia'];
			$fecha_inicio = $_POST['fecha_inicio'];
			$fecha_fin = $_POST['fecha_fin'];
			$hora_inicio = $_POST['hora_inicio'];
			$hora_fin = $_POST['hora_fin'];
			$horario_inicio = $_POST['horario_inicio'];
			$horario_fin = $_POST['horario_fin'];
			$periodoContable = date('Y-m');
			$fecha_hora = date('Y-m-d H:i:s');
			$remunerado = 1;
			$motivo = $_POST['ind_motivo'];
			// Calcular el tiempo total del permiso
			$obtenerTotal = $this->metCalcularTiempo($fecha_inicio, $fecha_fin, $hora_inicio, $hora_fin, $horario_inicio, $horario_fin, $pk_num_empleado, 2);
			$totalDias = $obtenerTotal[0];
			$totalHoras = $obtenerTotal[1];
			$totalMinutos = $obtenerTotal[2];
			// Dar Formato a las fechas
			//Fecha de Inicio
			$fechaExplodeInicio = explode("/", $fecha_inicio);
			$fechaInicio = $fechaExplodeInicio[2].'-'.$fechaExplodeInicio[1].'-'.$fechaExplodeInicio[0];
			// Fecha Fin
			$fechaExplodeFin = explode("/", $fecha_fin);
			$fechaFin = $fechaExplodeFin[2].'-'.$fechaExplodeFin[1].'-'.$fechaExplodeFin[0];
			// Obtengo la fecha de entrada
			$entrada = date('Y-m-d');
			// Transformar hora_inicio y la hora_fin en formato militar
			$horas = $this->metHoras($hora_inicio, $hora_fin, $horario_inicio, $horario_fin);
			$horaInicio = $horas[0];
			$horaFin = $horas[1];
			// Obtengo el horario del empleado
			$horarioLaboral = $this->atPermisoModelo->metConsultarHorario($pk_num_empleado);
			$pk_num_horario = $horarioLaboral['pk_num_horario'];
			// Obtengo la dependencia y el cargo del empleado
			$verDependencia = $this->atPermisoModelo->metDependenciaEmpleado($pk_num_empleado);
			$dependencia = $verDependencia['pk_num_dependencia'];
			$cargo = $verDependencia['pk_num_puestos'];
			// Guardar los datos
			$pkNumPermiso = $this->atPermisoModelo->metGuardarPermiso($pk_num_empleado, $pk_num_empleado_aprueba, $motivo_ausencia, $tipo_ausencia, $periodoContable, $fechaInicio, $fechaFin, $horaInicio, $horaFin, $motivo, $remunerado, $pk_num_horario, $entrada, $totalDias, $totalHoras, $totalMinutos, $dependencia, $cargo, $fecha_hora, $usuario);
            $permiso = $this->atPermisoModelo->metVerPermiso($pkNumPermiso, 1);
            $nombreCompleto = $permiso['ind_nombre1'].' '.$permiso['ind_nombre2'].' '.$permiso['ind_apellido1'].' '.$permiso['ind_apellido2'];
            $arrayPost = array(
                'pk_num_permiso' => $pkNumPermiso,
                'nombreCompleto' => $nombreCompleto,
                'tipo_ausencia' => $permiso['tipo_ausencia'],
                'motivo_ausencia' => $permiso['motivo_ausencia'],
                'fecha_ingreso' => $permiso['fecha_ingreso'],
                'ind_estado' => $permiso['ind_estado'],
				'fecha_registro' => $permiso['fecha_ingreso']
            );
			echo json_encode($arrayPost);
			exit;
		}
		$empleado = $this->atPermisoModelo-> metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$periodoContable = date('Y-m');
		$fechaIngreso = date('d/m/Y');
		$fechaPermiso = date('d/m/Y');
		$horaPermiso = date('h:i');
		$zonaHora = date('H');
		// Obtengo el horario del empleado
		$horarioLaboral = $this->atPermisoModelo->metConsultarHorario($pkNumEmpleado);
		$h_entrada1 = $horarioLaboral['fec_entrada1'];
		$h_salida1  = $horarioLaboral['fec_salida1'];
		$h_entrada2  = $horarioLaboral['fec_entrada2'];
		$h_salida2  = $horarioLaboral['fec_salida2'];
		if($h_entrada1!='' && $h_salida1!='' && $h_entrada2!='' && $h_salida2!=''){
			$entrada = $h_entrada1;
			$salida  = $h_salida2;
		}else{
			$entrada = $h_entrada1;
			$salida  = $h_salida1;
		}
		$array = $this->metHorarioEstandar($entrada,$salida);
		$entrada = $array[0].":".$array[1];
		$salida  = $array[2].":".$array[3];
		$horarioE = $array[4];
		$horarioS = $array[5];
		if($zonaHora>12) {
			$horario = 'PM';
		} else {
			$horario = 'AM';
		}
		$empleadoAprueba = $this->atPermisoModelo->metApruebaPermiso($pkNumEmpleado);
		$fk_a003_num_persona_responsable = $empleadoAprueba['fk_a003_num_persona_responsable'];
		$personaResponsable = $this->atPermisoModelo->metResponsable($fk_a003_num_persona_responsable);
		$responsable = $personaResponsable['pk_num_empleado'];
		$form = array(
			'status' => 'nuevo',
			'pk_num_empleado' => $pkNumEmpleado,
			'periodoContable' => $periodoContable,
			'fechaIngreso' => $fechaIngreso,
			'fechaPermiso' => $fechaPermiso,
			'horaPermiso' => $horaPermiso,
			'horaPermisoE' => $entrada,
			'horaPermisoS' => $salida,
			'horario' => $horario,
			'horarioE' => $horarioE,
			'horarioS' => $horarioS,
			'responsable' => $responsable
		);
		$empleado = $this->atPermisoModelo->metListarFuncionario();
		$this->atVista->assign('empleado', $empleado);
		$motivoAusencia = $this->atPermisoModelo->metMostrarSelect('MOTAUS');
		$this->atVista->assign('motivoAusencia', $motivoAusencia);
		$tipoAusencia = $this->atPermisoModelo->metMostrarSelect('TIPAUS');
		$this->atVista->assign('tipoAusencia', $tipoAusencia);
		$this->atVista->assign('form', $form);
		$this->atVista->metRenderizar('nuevoPermiso', 'modales');
	}

	// Método que permite calcular la diferencia entre dos horas dadas
	public function metRestarHoras($horaini, $horafin)
	{
		$horai=substr($horaini,0,2);
		$mini=substr($horaini,3,2);
		$segi=substr($horaini,6,2);

		$horaf=substr($horafin,0,2);
		$minf=substr($horafin,3,2);
		$segf=substr($horafin,6,2);

		$ini=((($horai*60)*60)+($mini*60)+$segi);
		$fin=((($horaf*60)*60)+($minf*60)+$segf);
		$dif=$fin-$ini;
		$difh=floor($dif/3600);
		$difm=floor(($dif-($difh*3600))/60);
		$difs=$dif-($difm*60)-($difh*3600);
		return date("H:i:s",mktime($difh,$difm,$difs));
	}

	// Método que permite transformar las horas de formato estándar a horario militar
	public function metHoras($horaInicioPrevio, $horaFinPrevio, $horarioInicio, $horarioFin)
	{
		// convierto las horas a horario militar
		// Hora Inicio
		$horaInicio = $horaInicioPrevio.$horarioInicio;
		$horaInicio = strtotime($horaInicio);
		$horaInicio = date("H:i:s", $horaInicio);
		// Hora Fin
		$horaFin = $horaFinPrevio.$horarioFin;
		$horaFin = strtotime($horaFin);
		$horaFin = date("H:i:s", $horaFin);
		$horas = array($horaInicio, $horaFin);
		return $horas;
	}

	// Método que permite recibir los parámetros para calcular el tiempo del permiso
	public function metCalcularTiempoPermiso()
	{
		$fechaInicioPrevia = $_POST['fechaInicio'];
		$fechaFinPrevia = $_POST['fechaFin'];
		$horaInicioPrevio = $_POST['horaInicio'];
		$horaFinPrevio = $_POST['horaFin'];
		$horarioInicio = $_POST['horarioInicio'];
		$horarioFin = $_POST['horarioFin'];
		$pkNumEmpleado = $_POST['pkNumEmpleado'];
		$this->metCalcularTiempo($fechaInicioPrevia, $fechaFinPrevia, $horaInicioPrevio, $horaFinPrevio, $horarioInicio, $horarioFin, $pkNumEmpleado, 1);
	}

	// Método que permite calcular el total de horas de un permiso basandose en el horario laboral que se tenga
	public function metObtenerTiempo($horaInicio, $horaFin, $horarioInicio, $horarioFin, $entrada1, $salida1, $entrada2, $salida2)
	{

		if((isset($entrada2)!='')&&(isset($salida2)!='')){
			// Comparo las horas para saber en que horario se encuentran
			if(($horarioInicio=='AM')&&($horarioFin=='AM')){
				if(($horaInicio <= $entrada1) && ($horaFin >= $salida1)){
					$diferenciaHorasPrevio = $this->metRestarHoras($entrada1, $salida1);
					$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
					$horaDiferencia = $diferenciaHoras[0];
					$minutoDiferencia = $diferenciaHoras[1];
				}
				// si la hora de inicio es menor o igual a la hora de entrada pero la hora de salida se encuentra dentro del horario laboral de la mañana.
				if(($horaInicio<=$entrada1)&&(($horaFin >= $entrada1)&&($horaFin <= $salida1))){
					$diferenciaHorasPrevio = $this->metRestarHoras($entrada1, $horaFin);
					$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
					$horaDiferencia = $diferenciaHoras[0];
					$minutoDiferencia = $diferenciaHoras[1];
				}
				// Si la hora de inicio se encuentra dentro del horario laboral de entrada pero la hora de salida sobrepasa la hora de salida de la mañana.
				if(($horaInicio>=$entrada1)&&(($horaFin>=$entrada1)&&($horaFin>=$salida1))){
					$diferenciaHorasPrevio = $this->metRestarHoras($horaInicio, $salida1);
					$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
					$horaDiferencia = $diferenciaHoras[0];
					$minutoDiferencia = $diferenciaHoras[1];
				}
				// Si la hora de entrada y salida se encuentra fuera del rango con horas muy cercanas
				if((($horaInicio<=$entrada1)&&($horaFin<=$entrada1))||(($horaInicio>=$salida1)&&($horaFin>=$salida1))){
					$horaDiferencia = 0;
					$minutoDiferencia = 0;
				}
				// Si las horas dadas estan dentro del horario laboral de la mañana
				if(($horaInicio>=$entrada1)&&($horaFin<=$salida1)){
					$diferenciaHorasPrevio = $this->metRestarHoras($horaInicio, $horaFin);
					$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
					$horaDiferencia = $diferenciaHoras[0];
					$minutoDiferencia = $diferenciaHoras[1];
				}
			}
			if(($horarioInicio=='PM')&&($horarioFin=='PM')){
				if(($horaInicio<=$entrada2)&&($horaFin>=$salida2)){
					// Calculo la sustracción entre horas
					$diferenciaHorasPrevio = $this->metRestarHoras($entrada2, $salida2);
					$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
					$horaDiferencia = $diferenciaHoras[0];
					$minutoDiferencia = $diferenciaHoras[1];
				}
				// si la hora de inicio es menor o igual a la hora de entrada pero la hora de salida se encuentra dentro del horario laboral de la tarde.
				if(($horaInicio<=$entrada2)&&(($horaFin>=$entrada2)&&($horaFin<=$salida2))){
					$diferenciaHorasPrevio = $this->metRestarHoras($entrada2, $horaFin);
					$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
					$horaDiferencia = $diferenciaHoras[0];
					$minutoDiferencia = $diferenciaHoras[1];
				}
				// Si la hora de inicio se encuentra dentro del horario laboral de entrada pero la hora de salida sobrepasa la hora de salida de la tarde.
				if(($horaInicio>=$entrada2)&&(($horaFin>=$entrada2)&&($horaFin>=$salida2))){
					$diferenciaHorasPrevio = $this->metRestarHoras($horaInicio, $salida2);
					$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
					$horaDiferencia = $diferenciaHoras[0];
					$minutoDiferencia = $diferenciaHoras[1];
				}
				// Si las horas de entrada y salida se encuentran fuera del rango con horas muy cercanas
				if((($horaInicio<=$entrada2)&&($horaFin<=$entrada2))||(($horaInicio>=$salida2)&&($horaFin>=$salida2))){
					$horaDiferencia = 0;
					$minutoDiferencia = 0;
				}
				// Si las horas dadas estan dentro del horario laboral de la mañana
				if(($horaInicio>=$entrada2)&&($horaFin<=$salida2)){
					$diferenciaHorasPrevio = $this->metRestarHoras($horaInicio, $horaFin);
					$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
					$horaDiferencia = $diferenciaHoras[0];
					$minutoDiferencia = $diferenciaHoras[1];
				}
			}
			if(($horarioInicio=='AM')&&($horarioFin=='PM')) {
				//Si las horas están fuera del horario laboral establecido
				if (($horaInicio <= $entrada1) && ($horaFin >= $salida2)) {
					//Bloque de la Mañana
					$diferenciaHorasManana = $this->metRestarHoras($entrada1, $salida1);
					$diferenciaManana = explode(":", $diferenciaHorasManana);
					$horaManana = $diferenciaManana[0];
					$minutoManana = $diferenciaManana[1];

					// Bloque de la Tarde
					$diferenciaHorasTarde = $this->metRestarHoras($entrada2, $salida2);
					$diferenciaTarde = explode(":", $diferenciaHorasTarde);
					$horaTarde = $diferenciaTarde[0];
					$minutoTarde = $diferenciaTarde[1];

					// Sumo las horas y los minutos
					$horaDiferencia = $horaManana + $horaTarde;
					$minutoDiferencia = $minutoManana + $minutoTarde;
					while ($minutoDiferencia >= 60) {
						if ($minutoDiferencia >= 60) {
							$horaDiferencia = $horaDiferencia + 1;
							$minutoDiferencia = $minutoDiferencia - 60;
						}
					}

				}
				//Si las horas están dentro del rango de horario establecido
				if ((($horaInicio >= $entrada1) && ($horaInicio <= $salida1)) && (($horaFin >= $entrada2) && ($horaFin <= $salida2))) {

					//Bloque de la Mañana
					$diferenciaHorasManana = $this->metRestarHoras($horaInicio, $salida1);
					$diferenciaManana = explode(":", $diferenciaHorasManana);
					$horaManana = $diferenciaManana[0];
					$minutoManana = $diferenciaManana[1];

					// Bloque de la Tarde
					$diferenciaHorasTarde = $this->metRestarHoras($entrada2, $horaFin);
					$diferenciaTarde = explode(":", $diferenciaHorasTarde);
					$horaTarde = $diferenciaTarde[0];
					$minutoTarde = $diferenciaTarde[1];

					// Sumo las horas y los minutos
					$horaDiferencia = $horaManana + $horaTarde;
					$minutoDiferencia = $minutoManana + $minutoTarde;
					while ($minutoDiferencia >= 60) {
						if ($minutoDiferencia >= 60) {
							$horaDiferencia = $horaDiferencia + 1;
							$minutoDiferencia = $minutoDiferencia - 60;
						}
					}
				}
				// Si la hora de inicio es menor a la hora de entrada del horario de la tarde y la hora final se encuentra dentro del horario laboral
				if ((($horaInicio >= $salida1) && ($horaInicio <= $entrada2)) && (($horaFin >= $entrada2) && ($horaFin <= $salida2))) {
					// Bloque de la Mañana
					$horaManana = 0;
					$minutoManana = 0;

					// Bloque de la Tarde
					$diferenciaHorasTarde = $this->metRestarHoras($entrada2, $horaFin);
					$diferenciaTarde = explode(":", $diferenciaHorasTarde);
					$horaTarde = $diferenciaTarde[0];
					$minutoTarde = $diferenciaTarde[1];

					// Sumo las horas y los minutos
					$horaDiferencia = $horaManana + $horaTarde;
					$minutoDiferencia = $minutoManana + $minutoTarde;
					while ($minutoDiferencia >= 60) {
						if ($minutoDiferencia >= 60) {
							$horaDiferencia = $horaDiferencia + 1;
							$minutoDiferencia = $minutoDiferencia - 60;
						}
					}
				}
				// Si la hora de inicio se encuentra dentro de la zona horaria de la mañana pero la hora final se encuentra fuera de la hora de salida de la tarde
				if ((($horaInicio >= $entrada1) && ($horaInicio <= $salida1)) && ($horaFin >= $salida2)) {
					//Bloque de la Mañana
					$diferenciaHorasManana = $this->metRestarHoras($horaInicio, $salida1);
					$diferenciaManana = explode(":", $diferenciaHorasManana);
					$horaManana = $diferenciaManana[0];
					$minutoManana = $diferenciaManana[1];

					// Bloque de la Tarde
					$diferenciaHorasTarde = $this->metRestarHoras($entrada2, $salida2);
					$diferenciaTarde = explode(":", $diferenciaHorasTarde);
					$horaTarde = $diferenciaTarde[0];
					$minutoTarde = $diferenciaTarde[1];

					// Sumo las horas y los minutos
					$horaDiferencia = $horaManana + $horaTarde;
					$minutoDiferencia = $minutoManana + $minutoTarde;
					while ($minutoDiferencia >= 60) {
						if ($minutoDiferencia >= 60) {
							$horaDiferencia = $horaDiferencia + 1;
							$minutoDiferencia = $minutoDiferencia - 60;
						}
					}
				}
				// Si la hora de inicio se encuentra dentro de la zona horaria de la mañana pero la hora final se encuentra en el horario de la tarde pero antes de la hora de entrada de la tarde
				if((($horaInicio>=$entrada1)&&($horaInicio<=$salida1))&&($horaFin<=$entrada2)){
					//Bloque de la Mañana
					$diferenciaHorasManana = $this->metRestarHoras($horaInicio, $salida1);
					$diferenciaManana = explode(":", $diferenciaHorasManana);
					$horaManana = $diferenciaManana[0];
					$minutoManana = $diferenciaManana[1];

					// Bloque de la Tarde
					$horaTarde = 0;
					$minutoTarde = 0;

					// Sumo las horas y los minutos
					$horaDiferencia = $horaManana + $horaTarde;
					$minutoDiferencia = $minutoManana + $minutoTarde;
					while($minutoDiferencia>=60){
						if($minutoDiferencia>=60){
							$horaDiferencia = $horaDiferencia + 1;
							$minutoDiferencia = $minutoDiferencia - 60;
						}
					}
				}
			}

		} else { // Si el horario es corrido
			//Si las horas de inicio y fin se encuentra dentro del horario laboral
			if(($horaInicio>=$entrada1)&&($horaFin<=$salida1)){
				$diferenciaHorasPrevio = $this->metRestarHoras($horaInicio, $horaFin);
				$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
				$horaDiferencia = $diferenciaHoras[0];
				$minutoDiferencia = $diferenciaHoras[1];
			}
			//Si las horas de inicio y fin se encuentran fuera del horario laboral
			if(($horaInicio<$entrada1)&&($horaFin>$salida1)){
				$diferenciaHorasPrevio = $this->metRestarHoras($entrada1, $salida1);
				$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
				$horaDiferencia = $diferenciaHoras[0];
				$minutoDiferencia = $diferenciaHoras[1];
			}
			//Si la hora de inicio es menor a la hora de entrada pero la hora final se encuentra dentro del horario laboral
			if(($horaInicio<=$entrada1)&&(($horaFin>=$entrada1)&&($horaFin<=$salida1))){
				$diferenciaHorasPrevio = $this->metRestarHoras($entrada1, $horaFin);
				$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
				$horaDiferencia = $diferenciaHoras[0];
				$minutoDiferencia = $diferenciaHoras[1];
			}
			//Si la fecha de entrada se encuentra dentro del horario laboral pero la hora final se encuentra fuera del horario laboral
			if((($horaInicio>=$entrada1)&&($horaInicio<=$salida1))&&($horaFin>=$salida1)){
				$diferenciaHorasPrevio = $this->metRestarHoras($horaInicio, $salida1);
				$diferenciaHoras = explode(":", $diferenciaHorasPrevio);
				$horaDiferencia = $diferenciaHoras[0];
				$minutoDiferencia = $diferenciaHoras[1];
			}
		}
		$datos = array($horaDiferencia, $minutoDiferencia);
		return $datos;
	}

	//Método que permite calcular el tiempo del permiso
	public function metCalcularTiempo($fechaInicioPrevia, $fechaFinPrevia, $horaInicioPrevio, $horaFinPrevio, $horarioInicio, $horarioFin, $pkNumEmpleado, $metodo)
	{
		//recibiendo los valores
		$fechaExplodeInicio = explode("/", $fechaInicioPrevia);
		$anioInicio = $fechaExplodeInicio[2];
		$mesInicio = $fechaExplodeInicio[1];
		$diaInicio = $fechaExplodeInicio[0];
		$fechaInicioTotal = $anioInicio.'-'.$mesInicio.'-'.$diaInicio;

		$fechaExplodeFin = explode("/", $fechaFinPrevia);
		$anioFin = $fechaExplodeFin[2];
		$mesFin = $fechaExplodeFin[1];
		$diaFin = $fechaExplodeFin[0];
		$fechaFinTotal = $anioFin.'-'.$mesFin.'-'.$diaFin;

		//calculo timestam de las dos fechas
		$timestamp1 = mktime(0,0,0,$mesInicio, $diaInicio, $anioInicio);
		$timestamp2 = mktime(4,12,0,$mesFin,$diaFin,$anioFin);
		//resto a una fecha la otra
		$segundos_diferencia = $timestamp1 - $timestamp2;
		//echo $segundos_diferencia;
		//convierto segundos en días
		$dias_diferencia = $segundos_diferencia / (60 * 60 * 24);
		//obtengo el valor absoulto de los días (quito el posible signo negativo)
		$dias_diferencia = abs($dias_diferencia);
		//quito los decimales a los días de diferencia
		$dias_diferencia = floor($dias_diferencia);
		$diasDiferencia = $dias_diferencia + 1; // Se le suma 1 para contar el dia de solicitud del permiso


		// Determino los dias no laborables (Sábado, Domingo y Feriados)
		for($i=$fechaInicioTotal;$i<=$fechaFinTotal;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){
			$diaFeriado = $this->atPermisoModelo->metFeriado($i);
			$feriado = $diaFeriado['feriado'];
			$fechaComprobar = date("w", strtotime($i));
			if(($fechaComprobar==0)||($fechaComprobar==6)||($feriado>0)){
				$diasDiferencia=  $diasDiferencia-1;
			}
		}

		// Obtengo la hora militar
		$horas = $this->metHoras($horaInicioPrevio, $horaFinPrevio, $horarioInicio, $horarioFin);
		$horaInicio = $horas[0];
		$horaFin = $horas[1];

		// Consulto el horario laboral que tiene el empleado
		$horarioLaboral = $this->atPermisoModelo->metConsultarHorario($pkNumEmpleado);
		$entrada1 = $horarioLaboral['fec_entrada1'];
		$salida1 = $horarioLaboral['fec_salida1'];
		$entrada2 = $horarioLaboral['fec_entrada2'];
		$salida2 = $horarioLaboral['fec_salida2'];
		// Tiempo total laborable de acuerdo al horario del empleado
		$diferenciaHorasHorario = $this->metRestarHoras($entrada1, $salida1);
		$diferenciaHorario1 = explode(":", $diferenciaHorasHorario);
		$horaHorario1 = $diferenciaHorario1[0];
		$minutoHorario1 = $diferenciaHorario1[1];
		$diferenciaHorasHorario2 = $this->metRestarHoras($entrada1, $salida1);
		$diferenciaHorario2 = explode(":", $diferenciaHorasHorario2);
		$horaHorario2 = $diferenciaHorario2[0];
		$minutoHorario2 = $diferenciaHorario2[1];
		$sumaHorasHorario = $horaHorario1 + $horaHorario2;
		$sumaMinutosHorario = $minutoHorario1 + $minutoHorario2;
		while($sumaMinutosHorario>=60){
			if($sumaMinutosHorario>=60){
				$sumaHorasHorario = $sumaHorasHorario + 1;
				$sumaMinutosHorario = $sumaMinutosHorario - 60;
			}
		}
		// Tiempo total del permiso respecto al horario
		$obtenerTiempo = $this->metObtenerTiempo($horaInicio, $horaFin, $horarioInicio, $horarioFin, $entrada1, $salida1, $entrada2, $salida2);
		$horaDiferencia = $obtenerTiempo[0];
		$minutoDiferencia = $obtenerTiempo[1];
		// Calculo las horas que se encuentran entre la diferencia de dias
		$horaAcumulador = 0;
		$minutoAcumulador = 0;
		for ($i=1; $i<=$diasDiferencia; $i++){
			$horaAcumulador = $horaAcumulador + $horaDiferencia;
			$minutoAcumulador = $minutoAcumulador + $minutoDiferencia;
		}

		while($minutoAcumulador>=60){
			if($minutoAcumulador>=60){
				$horaAcumulador = $horaAcumulador + 1;
				$minutoAcumulador = $minutoAcumulador - 60;
			}
		}
		//Array que contiene los datos obtenidos
		if(($diasDiferencia==1)&&($horaAcumulador<$sumaHorasHorario)){
			$diasDiferencia = 0;
		}

		if($metodo==1) {
			$datos = array(
				'diasDiferencia' => $diasDiferencia,
				'horaDiferencia' => $horaAcumulador,
				'minutoDiferencia' => $minutoAcumulador
			);
			echo json_encode($datos);
		} else {
			$datos = array($diasDiferencia, $horaAcumulador, $minutoAcumulador);
			return $datos;
		}
	}

	// Método que permite transformar las horas de horario militar a horario estándar
	public function metHorarioEstandar($horaSalida, $horaEntrada)
	{
		// Hora de salida
		$horaExplodeInicio = explode(":", $horaSalida);
		$horaInicio = $horaExplodeInicio[0];
		$minutoInicio = $horaExplodeInicio[1];

		// Hora de entrada
		$horaExplodeFin = explode(":", $horaEntrada);
		$horaFin = $horaExplodeFin[0];
		$minutoFin = $horaExplodeFin[1];

		// Convirtiendo a horario estándar
		// Hora Inicio
		if($horaInicio>12){
			$horaInicioPrevio = $horaInicio - 12;
			$hora_inicio = str_pad($horaInicioPrevio, 2, "0", STR_PAD_LEFT);
		}
		if($horaInicio<=12) {
			$hora_inicio = $horaInicio;
		}
		// Hora Fin
		if($horaFin>12){
			$horaFinPrevio = $horaFin - 12;
			$hora_fin = str_pad($horaFinPrevio, 2, "0", STR_PAD_LEFT);
		}
		if($horaFin<=12) {
			$hora_fin = $horaFin;
		}
		// Horario
		// Horario Inicio
		if($horaInicio>=12){
			$horarioInicio = 'PM';
		}
		if($horaInicio<12) {
			$horarioInicio = 'AM';
		}
		//Horario Fin
		if($horaFin>=12){
			$horariofin = 'PM';
		}
		if($horaFin<12) {
			$horariofin = 'AM';
		}
		// Los agrupo en un array
		$datoHorario = array($hora_inicio,$minutoInicio, $hora_fin, $minutoFin, $horarioInicio, $horariofin);
		return $datoHorario;
	}

	// Método que permite visualizar un permiso en específico
	public function metVerPermiso()
	{
		$pkNumPermiso = $this->metObtenerInt('pk_num_permiso');
		$flagFormulario = $this->metObtenerInt('flagFormulario');
		$estado = $this->metObtenerTexto('estado');
		$permiso = $this->atPermisoModelo->metVerPermiso($pkNumPermiso, 2);
		$this->atVista->assign('permiso', $permiso);
		$funcionarioAprueba = $this->atPermisoModelo->metFuncionarioAprueba($pkNumPermiso);
		$this->atVista->assign('funcionario', $funcionarioAprueba);
		// Transformo las horas a horario am-pm
		foreach ($permiso as $permiso){
			$horaSalida = $permiso['fec_hora_salida'];
			$horaEntrada = $permiso['fec_hora_entrada'];
		}
		$horario = $this->metHorarioEstandar($horaSalida, $horaEntrada);
		$datoHora = array (
			'horaInicio' => $horario[0],
			'minutoInicio' => $horario[1],
			'horaFin' => $horario[2],
			'minutoFin' => $horario[3],
			'horarioInicio' => $horario[4],
			'horarioFin' => $horario[5],
			'flagFormulario' => $flagFormulario
		);
		if($permiso['ind_estado']=='PR'){
			$estadoPrevio = 'PREPARADO';
		}
		if($permiso['ind_estado']=='AP'){
			$estadoPrevio = 'APROBADO';
		}
		if($permiso['ind_estado']=='VE'){
			$estadoPrevio = 'VERIFICADO';
		}
		if($permiso['ind_estado']=='AN'){
			$estadoPrevio = 'ANULADO';
		}
		// Aprueba permiso
		$funcionarioAprueba = $this->atPermisoModelo->metFuncionarioApruebaPermiso($pkNumPermiso);
		$pkNumEmpleadoAprueba = $funcionarioAprueba['pk_num_empleado'];
		$usuario = Session::metObtener('idUsuario');
		$obtenerEmpleado = $this->atPermisoModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
		$aprueba = array(
			'pkNumEmpleadoAprueba' => $pkNumEmpleadoAprueba,
			'pkNumEmpleado' => $pkNumEmpleado,
			'estado' => $estado
		);
		$estadoPermiso = array('ind_estado' => $estadoPrevio);
		$this->atVista->assign('estadoPermiso', $estadoPermiso);
		$this->atVista->assign('datoHora', $datoHora);
		$this->atVista->assign('aprueba', $aprueba);
		$verAcceso = $this->atPermisoModelo->metAcceso($pkNumPermiso, 2);
		$this->atVista->assign('acceso', $verAcceso);
		$this->atVista->metRenderizar('ver', 'modales');
	}

	// Método que permite generar el reporte con el listado de los permisos solicitados por un funcionario
	public function metListadoPermiso()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraPermiso','modRH');
		$pdf= new pdfGeneral('L','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$usuario = Session::metObtener('idUsuario');
		$obtenerEmpleado = $this->atPermisoModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
		$verPermiso = $this->atPermisoModelo->metListarPermisoUsuario($pkNumEmpleado);
		$pdf->SetWidths(array(25, 60, 40, 30, 20, 20, 66));
		foreach ($verPermiso as $verPermiso) {
			$pdf->Row(array(
				$verPermiso['pk_num_permiso'],
				utf8_decode($verPermiso['ind_nombre1'].' '.$verPermiso['ind_nombre2'].' '.$verPermiso['ind_apellido1'].' '.$verPermiso['ind_apellido2']),
				utf8_decode($verPermiso['motivo_ausencia']),
				utf8_decode($verPermiso['tipo_ausencia']),
				$verPermiso['fecha_salida'].'  '.$verPermiso['fecha_entrada'],
				$verPermiso['ind_estado'],
				utf8_decode($verPermiso['ind_motivo'])
			));
		}
		$pdf->Output();
	}

	// Método que permite editar un permiso
	public function metEditarPermiso()
	{
		$pkNumPermiso = $this->metObtenerInt('pk_num_permiso');
		$tipoListado = $this->metObtenerInt('tipoListado');
		$valido = $this->metObtenerInt('valido');
		if (isset($valido) && $valido == 1) {
			$pk_num_empleado = $_POST['idEmpleado'];
			$motivo_ausencia = $_POST['motivo_ausencia'];
			$tipo_ausencia = $_POST['tipo_ausencia'];
			$fecha_inicio = $_POST['fecha_inicio'];
			$fecha_fin = $_POST['fecha_fin'];
			$hora_inicio = $_POST['hora_inicio'];
			$hora_fin = $_POST['hora_fin'];
			$horario_inicio = $_POST['horario_inicio'];
			$horario_fin = $_POST['horario_fin'];
			$ind_motivo = $_POST['ind_motivo'];
			$pk_num_empleado_aprueba = $_POST['pk_num_empleado_aprueba'];
			$usuario = Session::metObtener('idUsuario');
			$fecha_hora = date('Y-m-d H:i:s');

			// Obtengo el total de tiempo del permiso
			$obtenerTotal = $this->metCalcularTiempo($fecha_inicio, $fecha_fin, $hora_inicio, $hora_fin, $horario_inicio, $horario_fin, $pk_num_empleado, 2);
			$totalDias = $obtenerTotal[0];
			$totalHoras = $obtenerTotal[1];
			$totalMinutos = $obtenerTotal[2];

			// Dar Formato a las fechas
			//Fecha de Inicio
			$fechaExplodeInicio = explode("/", $fecha_inicio);
			$fechaInicio = $fechaExplodeInicio[2].'-'.$fechaExplodeInicio[1].'-'.$fechaExplodeInicio[0];
			// Fecha Fin
			$fechaExplodeFin = explode("/", $fecha_fin);
			$fechaFin = $fechaExplodeFin[2].'-'.$fechaExplodeFin[1].'-'.$fechaExplodeFin[0];
			// Obtengo la fecha de entrada
			$entrada = date('Y-m-d');

			// Transformar hora_inicio y la hora_fin en formato militar
			$horas = $this->metHoras($hora_inicio, $hora_fin, $horario_inicio, $horario_fin);
			$horaInicio = $horas[0];
			$horaFin = $horas[1];

			$this->atPermisoModelo->metEditarPermiso($pkNumPermiso, $tipo_ausencia, $motivo_ausencia, $fechaInicio, $fechaFin, $horaInicio, $horaFin, $ind_motivo, $totalDias, $totalHoras, $totalMinutos, $usuario, $fecha_hora, $pk_num_empleado_aprueba);
			$permiso = $this->atPermisoModelo->metVerPermiso($pkNumPermiso, 1);
			$nombreCompleto = $permiso['ind_nombre1'].' '.$permiso['ind_nombre2'].' '.$permiso['ind_apellido1'].' '.$permiso['ind_apellido2'];
			$arrayPost = array(
				'status' => 'modificar',
				'pk_num_permiso' => $pkNumPermiso,
				'nombreCompleto' => $nombreCompleto,
				'tipo_ausencia' => $permiso['tipo_ausencia'],
				'motivo_ausencia' => $permiso['motivo_ausencia'],
				'fecha_ingreso' => $permiso['fecha_ingreso'],
				'ind_estado' => $permiso['ind_estado'],
				'tipoListado' => $tipoListado
			);
			echo json_encode($arrayPost);
			exit;
		}
		if (!empty($pkNumPermiso)) {
			$permiso = $this->atPermisoModelo->metVerPermiso($pkNumPermiso, 1);
			$pk_num_empleado_aprueba = $permiso['fk_rhb001_num_empleado_aprueba'];
			$aprueba = $this->atPermisoModelo->metAprueba($pk_num_empleado_aprueba);
			$nombreAprueba = $aprueba['ind_nombre1'].' '.$aprueba['ind_nombre2'].' '.$aprueba['ind_apellido1'].' '.$aprueba['ind_apellido2'];
			// Transformo las horas a horario am-pm
			$horaSalida = $permiso['fec_hora_salida'];
			$horaEntrada = $permiso['fec_hora_entrada'];
			$horario = $this->metHorarioEstandar($horaSalida, $horaEntrada);
			$datoHora = array (
				'horaInicio' => $horario[0],
				'minutoInicio' => $horario[1],
				'horaFin' => $horario[2],
				'minutoFin' => $horario[3],
				'horarioInicio' => $horario[4],
				'horarioFin' => $horario[5]
			);
			$this->atVista->assign('datoHora', $datoHora);
			$permiso = array(
				'pk_num_permiso' => $permiso['pk_num_permiso'],
				'pk_num_empleado' => $permiso['pk_num_empleado'],
				'motivo_ausencia' => $permiso['fk_a006_num_miscelaneo_motivo_ausencia'],
				'tipo_ausencia' => $permiso['fk_a006_num_miscelaneo_tipo_ausencia'],
				'ind_periodo_contable'=> $permiso['ind_periodo_contable'],
				'fecha_ingreso' => $permiso['fecha_ingreso'],
				'fecha_salida' => $permiso['fecha_salida'],
				'fecha_entrada' => $permiso['fecha_entrada'],
				'totalDia'=> $permiso['num_total_dia'],
				'totalHora' => $permiso['num_total_hora'],
				'totalMinuto' => $permiso['num_total_minuto'],
				'ind_motivo' => $permiso['ind_motivo'],
				'num_remuneracion' => $permiso['num_remuneracion'],
				'num_justificativo' => $permiso['num_justificativo'],
				'num_exento' => $permiso['num_exento'],
				'ind_nombre1' => $permiso['ind_nombre1'],
				'ind_nombre2' => $permiso['ind_nombre2'],
				'ind_apellido1' => $permiso['ind_apellido1'],
				'ind_apellido2' => $permiso['ind_apellido2'],
				'nombreAprueba' => $nombreAprueba,
				'pk_num_empleado_aprueba' => $pk_num_empleado_aprueba

			);
			$verAcceso = $this->atPermisoModelo->metAcceso($pkNumPermiso, 2);
			$this->atVista->assign('acceso', $verAcceso);
			$motivoAusencia = $this->atPermisoModelo->metMostrarSelect('MOTAUS');
			$this->atVista->assign('motivoAusencia', $motivoAusencia);
			$tipoAusencia = $this->atPermisoModelo->metMostrarSelect('TIPAUS');
			$this->atVista->assign('tipoAusencia', $tipoAusencia);
			$funcionarioResponsable = $this->atPermisoModelo->metListarResponsables();
			$this->atVista->assign('responsable', $funcionarioResponsable);
			$this->atVista->assign('permiso', $permiso);
            $empleado = $this->atPermisoModelo->metListarFuncionario();
            $this->atVista->assign('empleado', $empleado);
			$this->atVista->metRenderizar('editar', 'modales');
		}
	}

	// Método que permite visualizar un determinado permiso tipo reporte
	public function metReportePermiso()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraPermiso','modRH');
		$pdf= new pdfReporte('P','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		//$usuario = Session::metObtener('idUsuario');
		$pkNumPermiso = $_GET['pk_num_permiso'];
		$verPermiso = $this->atPermisoModelo->metVerPermiso($pkNumPermiso, 2);
		foreach ($verPermiso as $verPermiso) {
			//Recibo las variables y las convierto a mayúsculas
			$ind_nombre1 = mb_convert_case($verPermiso['ind_nombre1'], MB_CASE_UPPER, "UTF-8");
			$ind_nombre2 = mb_convert_case($verPermiso['ind_nombre2'], MB_CASE_UPPER, "UTF-8");
			$ind_apellido1 = mb_convert_case($verPermiso['ind_apellido1'], MB_CASE_UPPER, "UTF-8");
			$ind_apellido2 = mb_convert_case($verPermiso['ind_apellido2'], MB_CASE_UPPER, "UTF-8");
			$ind_descripcion_cargo = mb_convert_case($verPermiso['ind_descripcion_cargo'], MB_CASE_UPPER, "UTF-8");
			$ind_dependencia = mb_convert_case($verPermiso['ind_dependencia'], MB_CASE_UPPER, "UTF-8");
			$tipo_ausencia = mb_convert_case($verPermiso['tipo_ausencia'], MB_CASE_UPPER, "UTF-8");
			// Fin
			// Marca de agua en caso de que el permiso este anulado o pendiente
			$estatusPermiso = $verPermiso['ind_estado'];
			if(($estatusPermiso=='AN')||($estatusPermiso=='PR')){
				$pdf->SetFont('Arial','B',50);
				$pdf->SetTextColor(240, 240, 240);
				$pdf->RotatedText(35,190,'N O  A P R O B A D O',45);
				$pdf->SetTextColor(0, 0, 0);
			}
			// Fin
			$pdf->Ln();
			$pdf->Cell(197,2,'',0, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(197,5,utf8_decode('DATOS DEL FUNCIONARIO O TRABAJADOR'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(42,5,utf8_decode('CÉDULA:'),1, 0, 'C');
			$pdf->Cell(155,5,utf8_decode('NOMBRES Y APELLIDOS:'),1, 0, 'L');
			$cedulaPrevia = $verPermiso['ind_cedula_documento'];
			$cedula = number_format($cedulaPrevia,0,'.','.');
			$pdf->SetFont('Arial', '', 9);
			$pdf->Ln();
			$pdf->Cell(42,5, $cedula,1, 0, 'C');
			$pdf->Cell(155,5,utf8_decode($ind_nombre1.' '.$ind_nombre2.' '.$ind_apellido1.' '.$ind_apellido2),1, 0, 'L');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(155,5,utf8_decode('DENOMINACIÓN DEL CARGO:'),1, 0, 'L');
			$pdf->Cell(42,5, utf8_decode('CÓDIGO:'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', '', 9);
			$pdf->SetWidths(array(155, 42));
			$pdf->Row(array(
				utf8_decode($ind_descripcion_cargo), '                 '.str_pad($verPermiso['pk_num_empleado'], 6, "0", STR_PAD_LEFT)
			), 10);
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(197,5,utf8_decode('DEPENDENCIA:'),1, 0, 'L');
			$pdf->Ln();
			$pdf->SetFont('Arial','', 9);
			$pdf->Cell(197,10, utf8_decode($ind_dependencia),1, 0, 'L');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(98.5, 5,utf8_decode('FECHA:'),1, 0, 'C');
			$pdf->Cell(98.5, 5, utf8_decode('FIRMA:'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(98.5, 10, $verPermiso['fecha_ingreso'],1, 0, 'C');
			$pdf->Cell(98.5, 10, '',1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(197,5,utf8_decode('DATOS DE INCIDENCIA'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(49, 5,utf8_decode('TIPO:'),1, 0, 'C');
			$pdf->Cell(148, 5, utf8_decode('MOTIVO:'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(49, 15,utf8_decode($tipo_ausencia),1, 0, 'C');
			$pdf->SetWidths(array(148));
			$pdf->Row(array(
				utf8_decode(ucfirst($verPermiso['ind_motivo']))
			), 15);
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(197,5,utf8_decode('DURACIÓN'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(60, 5,utf8_decode('HORAS:'),1, 0, 'C');
			$pdf->Cell(85, 5, utf8_decode('DIAS:'),1, 0, 'C');
			$pdf->Cell(52, 5, utf8_decode('TIEMPO TOTAL:'),1, 0, 'C');
			$pdf->Ln();
			$pdf->Cell(20, 5,utf8_decode('DESDE:'),1, 0, 'C');
			$pdf->Cell(20, 5,utf8_decode('HASTA:'),1, 0, 'C');
			$pdf->Cell(20, 5,utf8_decode('TOTAL:'),1, 0, 'C');
			$pdf->Cell(28, 5,utf8_decode('DESDE:'),1, 0, 'C');
			$pdf->Cell(28, 5,utf8_decode('HASTA:'),1, 0, 'C');
			$pdf->Cell(29, 5,utf8_decode('TOTAL:'),1, 0, 'C');
			$pdf->Cell(26, 5,utf8_decode('DIAS:'),1, 0, 'C');
			$pdf->Cell(26, 5,utf8_decode('HORAS:'),1, 0, 'C');
			$pdf->Ln();
			// Formato de hora estándar
			$horaSalida = $verPermiso['fec_hora_salida'];
			$horaEntrada = $verPermiso['fec_hora_entrada'];
			$horario = $this->metHorarioEstandar($horaSalida, $horaEntrada);
			// Fin
			// Obtengo el horario laboral del empleado
			$obtenerHorario = $this->atPermisoModelo->metHorario($verPermiso['fk_rhb007_num_horario']);
			$entrada1 = $obtenerHorario['fec_entrada1'];
			$salida1 = $obtenerHorario['fec_salida1'];
			$entrada2 = $obtenerHorario['fec_entrada2'];
			$salida2 = $obtenerHorario['fec_salida2'];
			//Fin
			// Calculo las horas de diferencia
			$obtenerTiempo = $this->metObtenerTiempo($horaSalida, $horaEntrada, $horario[4], $horario[5], $entrada1, $salida1, $entrada2, $salida2);
			// Fin
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(20, 5, str_pad($horario[0], 2, "0", STR_PAD_LEFT).':'.$horario[1].' '.$horario[4],1, 0, 'C');
			$pdf->Cell(20, 5,str_pad($horario[2], 2, "0", STR_PAD_LEFT).':'.$horario[3].' '.$horario[5],1, 0, 'C');
			$pdf->Cell(20, 5,str_pad($obtenerTiempo[0], 2, "0", STR_PAD_LEFT).':'.str_pad($obtenerTiempo[1], 2, "0", STR_PAD_LEFT),1, 0, 'C');
			$pdf->Cell(28, 5,$verPermiso['fecha_salida'],1, 0, 'C');
			$pdf->Cell(28, 5,$verPermiso['fecha_entrada'],1, 0, 'C');
			$pdf->Cell(29, 5,$verPermiso['num_total_dia'],1, 0, 'C');
			$pdf->Cell(26, 5,$verPermiso['num_total_dia'],1, 0, 'C');
			$pdf->Cell(26, 5,str_pad($verPermiso['num_total_hora'], 2, "0", STR_PAD_LEFT).':'.str_pad($verPermiso['num_total_minuto'], 2, "0", STR_PAD_LEFT),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(197,5,utf8_decode('AUTORIZADO POR'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(98.5, 5,utf8_decode('NOMBRES Y APELLIDOS:'),1, 0, 'L');
			$pdf->Cell(98.5, 5, utf8_decode('CARGO:'),1, 0, 'L');
			$pdf->Ln();
			$pdf->SetFont('Arial', '', 9);
			$aprueba = $this->atPermisoModelo->metApruebaFuncionario($verPermiso['fk_rhb001_num_empleado_aprueba']);
			$nombreCompletoAprueba = $aprueba['ind_nombre1'].' '.$aprueba['ind_nombre2'].' '.$aprueba['ind_apellido1'].' '.$aprueba['ind_apellido2'];
			$ind_nombre_aprueba = mb_convert_case($nombreCompletoAprueba, MB_CASE_UPPER, "UTF-8");
			$cargoApruebaPrevio = $this->atPermisoModelo->metDependenciaEmpleado($verPermiso['fk_rhb001_num_empleado_aprueba']);
			$cargoAprueba = mb_convert_case($cargoApruebaPrevio['ind_descripcion_cargo'], MB_CASE_UPPER, "UTF-8");
			$pdf->SetWidths(array(98.5, 98.5));
			$pdf->Row(array(
				utf8_decode($ind_nombre_aprueba),
				utf8_decode($cargoAprueba)
			), 10);
			// Se obtiene la fecha de aprobacion
			$estatus = $this->atPermisoModelo->metVerEstatus($pkNumPermiso, 'AP');
			// Fin
			// Se obtiene la fecha de verificación y quien la realizó
			if($estatusPermiso=='VE'){
				$verEstatus = $this->atPermisoModelo->metVerEstatus($pkNumPermiso, 'VE');
				$nombreCompletoVerifica = $verEstatus['ind_nombre1'].' '.$verEstatus['ind_nombre2'].' '.$verEstatus['ind_apellido1'].' '.$verEstatus['ind_apellido2'];
				$ind_nombre_verifica = mb_convert_case($nombreCompletoVerifica, MB_CASE_UPPER, "UTF-8");
				$funcionarioVerifica = $verEstatus['pk_num_empleado'];
				$verificaPrevio = $this->atPermisoModelo->metDependenciaEmpleado($funcionarioVerifica);
				$cargoVerificaPrevio = $verificaPrevio['ind_descripcion_cargo'];
				$cargoVerifica = mb_convert_case($cargoVerificaPrevio, MB_CASE_UPPER, "UTF-8");
			}
			// Fin
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(98.5, 5,utf8_decode('FIRMA Y SELLO:'),1, 0, 'C');
			$pdf->Cell(98.5, 5, utf8_decode('FECHA'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(98.5, 10,'',1, 0, 'C');
			if($estatusPermiso=='PR'){
				$pdf->Cell(98.5, 10, '',1, 0, 'C');
			} else {
				$pdf->Cell(98.5, 10, $estatus['fecha_operacion'],1, 0, 'C');
			}
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(197,5,utf8_decode('OBSERVACIONES'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 9);
			$ind_observacion_aprobacion = mb_convert_case($verPermiso['ind_observacion_aprobacion'], MB_CASE_UPPER, "UTF-8");
			if(($estatusPermiso=='AP')||($estatusPermiso=='VE')){
				$pdf->SetWidths(array(197));
				$pdf->Row(array(
					utf8_decode($ind_observacion_aprobacion)
				), 15);
			} else {
				$pdf->SetWidths(array(197));
				$pdf->Row(array(
					''
				), 15);
			}

			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(197,5,utf8_decode('VERIFICADO POR'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(98.5, 5,utf8_decode('NOMBRES Y APELLIDOS:'),1, 0, 'L');
			$pdf->Cell(98.5, 5, utf8_decode('CARGO:'),1, 0, 'L');
			$pdf->Ln();
			$pdf->SetFont('Arial', '', 9);
			if($estatusPermiso=='VE'){
				$pdf->SetWidths(array(98.5, 98.5));
				$pdf->Row(array(
					utf8_decode($ind_nombre_verifica), utf8_decode($cargoVerifica)
				), 10);
			} else {
				$pdf->SetWidths(array(98.5, 98.5));
				$pdf->Row(array(
					'',''
				), 10);
			}
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(98.5, 5,utf8_decode('FIRMA Y SELLO:'),1, 0, 'C');
			$pdf->Cell(98.5, 5, utf8_decode('FECHA'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(98.5, 10,'',1, 0, 'C');
			if($estatusPermiso=='VE'){
				$pdf->Cell(98.5, 10, $verEstatus['fecha_operacion'],1, 0, 'C');
			} else {
				$pdf->Cell(98.5, 10, '',1, 0, 'C');
			}
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(197,5,utf8_decode('VERIFICACIÓN POSTERIOR'),1, 0, 'C');
			$pdf->Ln();
			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(145, 5,utf8_decode('OBSERVACIONES:'),1, 0, 'C');
			$pdf->Cell(52, 5, utf8_decode('ESTADO:'),1, 0, 'C');
			$pdf->Ln();
			$ind_observacion_verificacion = mb_convert_case($verPermiso['ind_observacion_verificacion'], MB_CASE_UPPER, "UTF-8");
			$num_justificativo = $verPermiso['num_justificativo'];
			if($num_justificativo==1){
				$estado = 'JUSTIFICADO';
			} else {
				$estado = 'INJUSTIFICADO';
			}
			if($estatusPermiso=='VE'){
				$pdf->SetWidths(array(145, 52));
				$pdf->Row(array(
					utf8_decode($ind_observacion_verificacion), $estado
				), 10);
			} else {
				$pdf->SetWidths(array(145, 52));
				$pdf->Row(array(
					'',''
				), 10);
			}

		}
		$pdf->Output();
	}

	// Este método se encarga de mostrar el listado de los empleados solicitantes de permisos
	public function metListadoAdministrador($estado=false)
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker'
		);
		$complementosJs = array('select2/select2.min','bootstrap-datepicker/bootstrap-datepicker','inputmask/jquery.inputmask.bundle.min');
		$js =  array('materialSiace/core/demo/DemoTableDynamic', 'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar=array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$obtenerEmpleado = $this->atPermisoModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
		$dependencia = $this->atPermisoModelo->metDependenciaEmpleado($pkNumEmpleado);
		$idDependencia = $dependencia['fk_a004_num_dependencia'];
		$verDependencia = $dependencia['ind_dependencia'];
		$dep = array('ind_dependencia' => $verDependencia);
		$this->atVista->assign('dep', $dep);
		// Si es la vista general
		if($estado=='GE'){
            $this->atVista->assign('permisoFuncionario', $this->atPermisoModelo->metListadoPermiso($idDependencia));
		} else {
            $this->atVista->assign('permisoFuncionario', $this->atPermisoModelo->metListadoPermiso($idDependencia, $estado));
		}

		$listadoOrganismo = $this->atPermisoModelo->metListarOrganismo(0, 1);
		$usuario = Session::metObtener('idUsuario');
		$obtenerEmpleado = $this->atPermisoModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
		$datosEmpleado = $this->atPermisoModelo->metDependenciaEmpleado($pkNumEmpleado);
		$empleadoDato = array(
			'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
			'ind_descripcion_empresa' => $datosEmpleado['ind_descripcion_empresa'],
			'estado' => $estado
		);
		$busqueda = array(
			'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
			'pk_num_dependencia' => $datosEmpleado['pk_num_dependencia'],
			'ind_descripcion_empresa' => $datosEmpleado['ind_descripcion_empresa'],
			'ind_dependencia' =>  $datosEmpleado['ind_dependencia']
		);
		$pkNumDependencia = $datosEmpleado['pk_num_dependencia'];
		$listadoEmpleados = $this->atPermisoModelo->metListarEmpleado($pkNumDependencia);
		$this->atVista->assign('empleado', $listadoEmpleados);
		$this->atVista->assign('busqueda', $busqueda);
		$this->atVista->assign('emp', $empleadoDato);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
		$listadoDependencia = $this->atPermisoModelo->metListarDependencia($datosEmpleado['pk_num_organismo'], 1, 0);
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->metRenderizar('listadoAdministrador');
	}

	// Método que permite gestionar los estatus del permiso
	public function metEstatus()
	{
		$pkNumPermiso = $_POST['pk_num_permiso'];
		$valor = $_POST['valor'];
		$fecha_hora = date('Y-m-d H:i:s');
		$usuario = Session::metObtener('idUsuario');
		$obtenerEmpleado = $this->atPermisoModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
		$estadoAnterior = $verPermiso = $this->atPermisoModelo->metVerPermiso($pkNumPermiso, 1);
		$valorAnterior = $estadoAnterior['ind_estado'];
		if($valor=='AP'){
			if(isset($_POST['ind_observacion_aprobacion']))
				$ind_observacion_aprobacion = $_POST['ind_observacion_aprobacion'];
			else
                $ind_observacion_aprobacion = NULL;
			$num_remunerado = $_POST['num_remunerado'];
			$num_justificativo = $_POST['num_justificativo'];
			$num_exento = $_POST['num_exento'];
			$this->atPermisoModelo->metPermisoEstatus($pkNumPermiso, $num_remunerado, $num_justificativo, $num_exento, $ind_observacion_aprobacion, $usuario, $fecha_hora, $valor);
		}
		if($valor=='VE'){
			$ind_observacion_verificacion = $_POST['ind_observacion_verificacion'];
			$this->atPermisoModelo->metPermisoEstatus($pkNumPermiso, 0, 0, 0, $ind_observacion_verificacion, $usuario, $fecha_hora, $valor);
		}
		$this->atPermisoModelo->metEstatus($valor, $pkNumPermiso, $fecha_hora, $pkNumEmpleado, $valorAnterior);
		$verPermiso = $this->atPermisoModelo->metVerPermiso($pkNumPermiso, 1);
		$nombreCompleto = $verPermiso['ind_nombre1'].' '.$verPermiso['ind_nombre2'].' '.$verPermiso['ind_apellido1'].' '.$verPermiso['ind_apellido2'];
		$estadoPrevio = $verPermiso['ind_estado'];
		if($estadoPrevio=='PR'){
			$indEstado = 'PREPARADO';
		}
		if($estadoPrevio=='AN'){
			$indEstado = 'ANULADO';
		}
		if($estadoPrevio=='AP'){
			$indEstado = 'APROBADO';
		}
		if($estadoPrevio=='VE'){
			$indEstado = 'VERIFICADO';
		}
		$permiso = array(
			'pk_num_permiso' => $pkNumPermiso,
			'nombreCompleto' => $nombreCompleto,
			'motivo_ausencia' => $verPermiso['motivo_ausencia'],
			'tipo_ausencia' => $verPermiso['tipo_ausencia'],
			'fecha_ingreso' => $verPermiso['fecha_ingreso'],
			'ind_estado' => $indEstado
		);
		echo json_encode($permiso);
		exit;
	}

	// Método que permite realizar la búsqueda de permisos mediante el filtro mostrado en la vista de listado
	public function metBuscarPermiso()
	{
		$pk_num_organismo = $_POST['pk_num_organismo'];
		$pk_num_dependencia = $_POST['pk_num_dependencia'];
		$pk_num_empleado = $_POST['pk_num_empleado'];
		$fechaInicio = $_POST['fecha_inicio'];
		$fechaFin = $_POST['fecha_fin'];
		$pk_num_permiso = $_POST['pk_num_permiso'];
		$ind_estado = $_POST['ind_estado'];

			$permisoFuncionario = $this->atPermisoModelo->metBuscarPermiso($pk_num_organismo, $pk_num_dependencia, $pk_num_empleado, $fechaInicio, $fechaFin, $pk_num_permiso, $ind_estado, 2);
			echo json_encode($permisoFuncionario);

	}

	public function metBuscarListar()
	{
			$pk_num_organismo = $_POST['pk_num_organismo'];
			$pk_num_dependencia = $_POST['pk_num_dependencia'];
			$pk_num_empleado = $_POST['pk_num_empleado'];

			if($pk_num_organismo!=''){
				$verOrganismo = $this->atPermisoModelo->metListarOrganismo($pk_num_organismo, 2);
				$organismo = $verOrganismo['ind_descripcion_empresa'];
			} else {
				$organismo = '';
			}

			if($pk_num_dependencia!=''){
				$verDependencia = $this->atPermisoModelo->metListarDependencia(0, 2, $pk_num_dependencia);
				$dependencia = $verDependencia['ind_dependencia'];
			} else {
				$dependencia = '';
			}

			if($pk_num_empleado!=''){
				$verEmpleado = $this->atPermisoModelo->metBuscarEmpleado($pk_num_empleado);
				$empleado = $verEmpleado['ind_nombre1'].' '.$verEmpleado['ind_apellido1'];
			} else {
				$empleado = '';
			}

			$permisoF =  array(
				'ind_descripcion_empresa' => $organismo,
				'dependencia' => $dependencia,
				'empleado' => $empleado

			);
			echo json_encode($permisoF);
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarDependencia()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$listarDependencia=$this->atPermisoModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia" onchange="cargarEmpleado(this.value)"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	public function metBuscarEmpleado()
	{
		$pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
		$listarEmpleado=$this->atPermisoModelo->metListarEmpleado($pkNumDependencia);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_empleado" id="pk_num_empleado"><option value="">&nbsp;</option>';
		foreach($listarEmpleado as $listarEmpleado){
			$a .='<option value="'.$listarEmpleado['pk_num_empleado'].'">'.$listarEmpleado['ind_nombre1'].' '.$listarEmpleado['ind_nombre2'].' '.$listarEmpleado['ind_apellido1'].' '.$listarEmpleado['ind_apellido2'].'</option>';
		}
		$a .= '</select><label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label></div>';
		echo $a;
	}

	// Método que permite emitir el reporte correspondiente a la búsqueda realizada mediante el filtro
	public function metBusquedaReporte()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$pk_num_organismo = $_GET['pk_num_organismo'];
		$pk_num_dependencia = $_GET['pk_num_dependencia'];
		$pk_num_empleado = $_GET['pk_num_empleado'];
		$fechaInicio = $_GET['fecha_inicio'];
		$fechaFin = $_GET['fecha_fin'];
		$pk_num_permiso = $_GET['pk_num_permiso'];
		$ind_estado = $_GET['ind_estado'];
		$this->metObtenerLibreria('cabeceraPermiso','modRH');
		$pdf= new pdfGeneral('L','mm','Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetWidths(array(25, 60, 40, 30, 20, 20, 66));
		$permisoFuncionario = $this->atPermisoModelo->metBuscarPermiso($pk_num_organismo, $pk_num_dependencia, $pk_num_empleado, $fechaInicio, $fechaFin, $pk_num_permiso, $ind_estado, 2);
		foreach($permisoFuncionario as $permiso){
			$pdf->Row(array(
				$permiso['pk_num_permiso'], utf8_decode($permiso['ind_nombre1'].' '.$permiso['ind_nombre2'].' '.$permiso['ind_apellido1'].' '.$permiso['ind_apellido2']), $permiso['motivo_ausencia'], $permiso['tipo_ausencia'], $permiso['fecha_registro'], $permiso['ind_estado'], utf8_decode(ucfirst($permiso['ind_motivo']))
			));
		}
		$pdf->Output();
	}

	// función que permite verificar las fechas recibidas
	public function metVerificarFechas()
	{
		$fechaAux = explode("/",$_POST['fecha_inicio']);
		$fechaInicio = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		$fechaAux = explode("/",$_POST['fecha_fin']);
		$fechaFin = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
		if(strtotime($fechaFin) >= strtotime($fechaInicio)) {
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	public function metListarDependencia()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$listarDependencia=$this->atPermisoModelo->metListadoDependencia($pkNumOrganismo, 1, 0);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// método que permite validar la busqueda de un empleado
	public function metValidarEmpleado()
	{
		$pkNumEmpleado = $this->metObtenerInt('pkNumEmpleado');
		$listarEmpleado=$this->atPermisoModelo->metValidarEmpleado();
		$a = '<select class="form-control dirty" id="pk_num_empleado_aprueba" name="pk_num_empleado_aprueba"><option value="">&nbsp;</option>';
		foreach($listarEmpleado as $listarEmpleado){
			if($listarEmpleado['pk_num_empleado']!=$pkNumEmpleado){
				$a .='<option value="'.$listarEmpleado['pk_num_empleado'].'">'.$listarEmpleado['ind_nombre1'].' '.$listarEmpleado['ind_nombre2'].' '.$listarEmpleado['ind_apellido1'].' '.$listarEmpleado['ind_apellido2'].'</option>';
			}
		}
		$a .= '</select><label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Aprueba</label>';
		echo $a;
	}
}
?>
