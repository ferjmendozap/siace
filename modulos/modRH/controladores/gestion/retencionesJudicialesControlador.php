<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class retencionesJudicialesControlador extends Controlador
{
    private $atEmpleados;
    private $atRetenciones;
    private $atOrganismo;
    private $atNomina;
    private $atPersona;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados    = $this->metCargarModelo('empleados','gestion');
        $this->atRetenciones  = $this->metCargarModelo('retencionesJudiciales','gestion');
        $this->atOrganismo    = $this->metCargarModelo('organismo','maestros','modCV');
        $this->atNomina       = $this->metCargarModelo('concepto','maestros','modNM');
        $this->atPersona      = $this->metCargarModelo('persona','maestros','modCV');

     }

    #Metodo Index del controlador listado de retenciones judiciales
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $EstadoRegistro = array('1'=>'ACTIVO','0'=>'INACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);
        $this->atVista->assign('EstadoPorDefecto','1');
        $listadoOrganismo = $this->atEmpleados->metListarOrganismos();
        $this->atVista->assign('listadoOrganismos', $listadoOrganismo);
        $this->atVista->assign('listadoRetenciones', $this->atRetenciones->metListarRetenciones());
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevaRetencion()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO
        $js = array('materialSiace/App','materialSiace/core/demo/DemoFormWizard','materialSiace/core/demo/DemoFormComponents');
        $complementosCss = array(
            'select2/select201ef','bootstrap-datepicker/datepicker','wizard/wizardfa6c'
        );
        $complementoJs = array(
            'select2/select2.min','wizard/jquery.bootstrap.wizard.min',
            'inputmask/mask','bootstrap-datepicker/bootstrap-datepicker',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        #OBTENGO EL ID DE LA RETENCION Y LA VARIABLE VALIDO PARA VALIDAR SI VAMOS A GUARDAR UN NUEVO REGISTRO O MODIFICARLO
        $idRetencion = $this->metObtenerInt('idRetencion');
        $valido  = $this->metObtenerInt('valido');
        $proceso = $this->metObtenerTexto('proceso');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   =$this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_sueldo_mensual')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idRetencion===0){//PARA INSERTAR REGISTRO



                $validacion['status']='creacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }

                //ARREGLO DE DATOS PARA ENVIAR AL MODELO PARA EL REGISTRO
                $datosGenerales = array(
                    $validacion['fk_a001_num_organismo'],
                    $validacion['fk_rhb001_num_empleado'],
                    $validacion['fk_a003_num_persona_demandante'],
                    $validacion['ind_expediente'],
                    $validacion['fec_fecha_resolucion'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tiporetencion'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipopago'],
                    $validacion['ind_juzgado'],
                    $validacion['txt_observaciones'],
                    $validacion['num_estatus']
                );

                $datosConceptos = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodescuento'],
                    $validacion['fk_nmb002_num_concepto'],
                    $validacion['ind_concepto'],
                    $validacion['num_descuento']
                );

                #INSTANCIO EL METODO PARA REGISTRAR LA CARGA DE FAMILIAR
                $id = $this->atRetenciones->metRegistrarRetenciones($datosGenerales,$datosConceptos);
                $validacion['idRetencion'] = $id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    echo json_encode($validacion);
                    exit;
                }



            }else{//PARA MODIFICAR REGISTRO

                $validacion['status']='modificar';
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }

                //ARREGLO DE DATOS PARA ENVIAR AL MODELO PARA EL REGISTRO
                $datosGenerales = array(
                    $validacion['fk_a001_num_organismo'],
                    $validacion['fk_rhb001_num_empleado'],
                    $validacion['fk_a003_num_persona_demandante'],
                    $validacion['ind_expediente'],
                    $validacion['fec_fecha_resolucion'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tiporetencion'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipopago'],
                    $validacion['ind_juzgado'],
                    $validacion['txt_observaciones'],
                    $validacion['num_estatus']
                );

                $datosConceptos = array(
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodescuento'],
                    $validacion['fk_nmb002_num_concepto'],
                    $validacion['ind_concepto'],
                    $validacion['num_descuento']
                );

                #INSTANCIO EL METODO PARA MODIFICAR LA RETENCION
                $id = $this->atRetenciones->metModificarRetenciones($idRetencion,$datosGenerales,$datosConceptos);
                $validacion['idRetencion'] = $idRetencion;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    echo json_encode($validacion);
                    exit;
                }


            }

        }

        if($idRetencion!=0){
            $db  = $this->atRetenciones->metMostrarRetenciones($idRetencion);
            $db1 = $this->atRetenciones->metMostrarRetencionesDetalle($idRetencion);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('formDBconceptos',$db1);
            $this->atVista->assign('n', 0);
            $this->atVista->assign('numero', 1);
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        #OBTENGO LISTADO DE ORGANISMOS
        $listadoOrganismo = $this->atEmpleados->metListarOrganismos();

        #OBTENGO MISCELANEOS TIPO RETENCION JUDICIAL Y TIPO PAGO RETENCION
        $miscelaneoTipoRetencion   = $this->atEmpleados->metMostrarSelect('RJUDICIAL');
        $miscelaneoTipoPago    = $this->atEmpleados->metMostrarSelect('TIPOPAGO');
        $miscelaneoTipoDesc    = $this->atEmpleados->metMostrarSelect('TIPODESC');

        #ASIGNO A LA VISTA LOS MISCELANEOS
        $this->atVista->assign('miscelaneoTipoRetencion',$miscelaneoTipoRetencion);
        $this->atVista->assign('miscelaneoTipoPago',$miscelaneoTipoPago);
        $this->atVista->assign('miscelaneoTipoDescuento',$miscelaneoTipoDesc);

        #ASIGNO A LA VISTA LISTADO DE ORGANISMO
        $this->atVista->assign('listadoOrganismos', $listadoOrganismo);

        $this->atVista->assign('n', 0);
        $this->atVista->assign('numero', 1);
        $this->atVista->assign('fecha_resolucion', date("d-m-Y"));#FECHA ACTUAL
        $this->atVista->assign('proceso',$proceso);
        $this->atVista->assign('idRetencion',$idRetencion);#ID DEL REGISTRO RETENCION
        $this->atVista->metRenderizar('form', 'modales');
    }

    #PERMITE LISTAR LOS CONCEPTOS DE NOMINA PARA PODER SELECCIONARIOS Y AGREGARLOS AL FORMULARIO
    public function metListarConceptos()
    {

        $linea = $this->metObtenerInt('linea');#para obtener el id de la linea a la que estamos haciendo click

        $conceptos = $this->atNomina->metListarConcepto(true);

        // Envío a la vista
        $this->atVista->assign('linea',$linea);
        $this->atVista->assign('conceptos',$conceptos);
        $this->atVista->metRenderizar('listaConceptos', 'modales');

    }


    #Metodo que permite obtener los datos del empleado
    public function metObtenerDatosEmpleado()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');

        $resultado = $this->atEmpleados->metObtenerDatosEmpleado($idEmpleado);

        //datos mpara mostrar en el formulario
        $datos = array(
            'id_empleado' => $idEmpleado,
            'nombreTrabajador' => $resultado['nombre_completo'],
            'id_dependencia' => $resultado['fk_a004_num_dependencia']
        );

        echo json_encode($datos);

    }

    #permite consultar listado de personas
    public function metConsultarPersona()
    {

        $personas = $this->atPersona->metListarPersona();
        $this->atVista->assign('personas',$personas);
        $this->atVista->metRenderizar('listaPersonas', 'modales');


    }

    #permite eliminar conceptos de la retencion ya registrada
    public function metEliminarConceptoDetalle()
    {
        $id = $this->metObtenerInt('id');

        if($id!=0){
            $id=$this->atRetenciones->metEliminarRetencionesDetalle($id);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'id'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


    /*PERMITE BUSCAR LAS RETENCIONES A TRAVES DEL FILTRO DE BUSQUEDA*/
    public function metBuscarRetenciones()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $buscar = mb_strtoupper($_POST['buscar'],'utf8');
        $estado_registro = $_POST['estado_registro'];
        $fechaInicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];


        $datosRetenciones = $this->atRetenciones->metBuscarRetenciones($pkNumOrganismo, $buscar, $estado_registro, $fechaInicio, $fecha_fin);
        echo json_encode($datosRetenciones);


    }


}
