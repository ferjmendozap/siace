<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class cargaFamiliarControlador extends Controlador
{
    private $atEmpleados;
    private $atCentroEstudio;
    private $atCargaFamiliar;
    private $atNivelGradoInstruccion;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados     = $this->metCargarModelo('empleados','gestion');
        $this->atCentroEstudio = $this->metCargarModelo('centroEstudios','maestros');
        $this->atCargaFamiliar = $this->metCargarModelo('cargaFamiliar','gestion');
        $this->atNivelGradoInstruccion = $this->metCargarModelo('nivelGradoInstruccion','maestros');

     }

    #Metodo Index del controlador listado de carga familiar del empleado
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoEmpleados', $this->atEmpleados->metListarEmpleado('AP','listado'));
        $this->atVista->assign('n', 1);
        $this->atVista->metRenderizar('empleados');
    }

    public function metCargaFamiliar()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/core/demo/DemoFormWizard','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents');
        $complementosCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef'
        );
        $complementoJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idCargaFamiliar = $this->metObtenerInt('idCargaFamiliar');
        $idEmpleado      = $this->metObtenerInt('idEmpleado');
        $idPersona       = $this->metObtenerInt('idPersona');
        $tipo = $this->metObtenerTexto('tipo');
        $valido  = $this->metObtenerInt('valido');


        if($valido==1) {

            $this->metValidarToken();

            $formTxt   =$this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_sueldo_mensual')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idCargaFamiliar===0){//PARA INSERTAR REGISTRO

                $validacion['status']='creacion';

                if(!isset($validacion['pk_num_telefono'])){
                    $validacion['pk_num_telefono']=false;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono']=false;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodir'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodir']=false;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio']=false;
                }
                if(!isset($validacion['ind_telefono'])){
                    $validacion['ind_telefono']=false;
                }
                if(!isset($validacion['apellido2'])){
                    $validacion['apellido2']=false;
                }
                if(!isset($validacion['nombre2'])){
                    $validacion['nombre2']=false;
                }
                if(!isset($validacion['aleatorio'])){
                    $validacion['aleatorio']='';
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_gradoinst'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_gradoinst']=false;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipoedu'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipoedu']=false;
                }
                if(!isset($validacion['fk_rhc040_num_institucion'])){
                    $validacion['fk_rhc040_num_institucion']=false;
                }
                if(!isset($validacion['num_flag_discapacidad'])){
                    $validacion['num_flag_discapacidad']=0;
                }
                if(!isset($validacion['num_flag_estudia'])){
                    $validacion['num_flag_estudia']=0;
                }
                if(!isset($validacion['num_flag_trabaja'])){
                    $validacion['num_flag_trabaja']=0;
                }
                if(!isset($validacion['fk_rhc064_nivel_grado_instruccion'])){
                    $validacion['fk_rhc064_nivel_grado_instruccion']=false;
                }
                if(!isset($validacion['fk_a006_num_tipo_zona'])){
                    $validacion['fk_a006_num_tipo_zona']=false;
                }
                if(!isset($validacion['fk_a006_num_ubicacion_inmueble'])){
                    $validacion['fk_a006_num_ubicacion_inmueble']=false;
                }
                if(!isset($validacion['ind_ubicacion_inmueble'])){
                    $validacion['ind_ubicacion_inmueble']=false;
                }
                if(!isset($validacion['ind_tipo_inmueble'])){
                    $validacion['ind_tipo_inmueble']=false;
                }
                if(!isset($validacion['fk_a006_num_zona_residencial'])){
                    $validacion['fk_a006_num_zona_residencial']=false;
                }
                if(!isset($validacion['ind_zona_residencial'])){
                    $validacion['ind_zona_residencial']=false;
                }
                if(!isset($validacion['ind_inmueble'])){
                    $validacion['ind_inmueble']=false;
                }
                if(!isset($validacion['ind_punto_referencia'])){
                    $validacion['ind_punto_referencia']=false;
                }
                if(!isset($validacion['ind_zona_postal'])){
                    $validacion['ind_zona_postal']=false;
                }    
                if(!isset($validacion['ind_direccion'])){
                    $validacion['ind_direccion']=false;
                }
                if(!isset($validacion['fk_a006_num_tipo_inmueble'])){
                    $validacion['fk_a006_num_tipo_inmueble']=false;
                }
                //ARREGLO DE DATOS PARA ENVIAR AL MODELO PARA EL REGISTRO
                $datos = array(
                    $validacion['ind_cedula_documento'],
                    $validacion['fk_a006_num_miscelaneo_detalle_sexo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_parentezco'],
                    $validacion['apellido1'],
                    $validacion['apellido2'],
                    $validacion['nombre1'],
                    $validacion['nombre2'],
                    $validacion['fec_nacimiento'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodir'],
                    $validacion['ind_direccion'],
                    $validacion['fk_a006_num_miscelaneo_det_gruposangre'],
                    $validacion['fk_a006_num_miscelaneo_detalle_edocivil'],
                    $validacion['pk_num_telefono'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono'],
                    $validacion['ind_telefono'],
                    $validacion['fk_a006_num_miscelaneo_detalle_gradoinst'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipoedu'],
                    $validacion['fk_rhc040_num_institucion'],
                    $validacion['ind_empresa'],
                    $validacion['txt_direccion_empresa'],
                    $validacion['num_sueldo_mensual'],
                    $validacion['aleatorio'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio'],
                    $validacion['num_flag_discapacidad'],
                    $validacion['num_flag_estudia'],
                    $validacion['num_flag_trabaja'],
                    $validacion['fk_rhc064_nivel_grado_instruccion'],
                    $validacion['fk_a006_num_tipo_zona'],
                    $validacion['fk_a006_num_ubicacion_inmueble'],
                    $validacion['ind_ubicacion_inmueble'],
                    $validacion['fk_a006_num_tipo_inmueble'],
                    $validacion['ind_tipo_inmueble'],
                    $validacion['fk_a006_num_zona_residencial'],
                    $validacion['ind_zona_residencial'],
                    $validacion['ind_inmueble'],
                    $validacion['ind_punto_referencia'],
                    $validacion['ind_zona_postal'],
                    $_POST['existe'],
                    $validacion['pk_num_persona']
                );
                
                #INSTANCIO EL METODO PARA REGISTRAR LA CARGA DE FAMILIAR
                $id = $this->atCargaFamiliar->metRegistrarCargaFamiliar($idEmpleado,$datos);
                $validacion['idCargaFamiliar'] = $id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    echo json_encode($validacion);
                    exit;
                }


            }else{//PARA MODIFICAR REGISTRO

                $validacion['status']='modificar';
                if(!isset($validacion['pk_num_telefono'])){
                    $validacion['pk_num_telefono']=false;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipodir'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodir']=false;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_domicilio'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio']=false;
                }
                if(!isset($validacion['ind_telefono'])){
                    $validacion['ind_telefono']=false;
                }
                if(!isset($validacion['apellido2'])){
                    $validacion['apellido2']=false;
                }
                if(!isset($validacion['nombre2'])){
                    $validacion['nombre2']=false;
                }
                if(!isset($validacion['pk_num_direccion_persona'])){
                    $validacion['pk_num_direccion_persona']=0;
                }
                if(!isset($validacion['pk_num_telefono'])){
                    $validacion['pk_num_telefono']=false;
                }
                if(!isset($validacion['aleatorio'])){
                    $validacion['aleatorio']='';
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_gradoinst'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_gradoinst']=false;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_tipoedu'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_tipoedu']=false;
                }
                if(!isset($validacion['fk_rhc040_num_institucion'])){
                    $validacion['fk_rhc040_num_institucion']=false;
                }
                if(!isset($validacion['num_flag_discapacidad'])){
                    $validacion['num_flag_discapacidad']=0;
                }
                if(!isset($validacion['num_flag_estudia'])){
                    $validacion['num_flag_estudia']=0;
                }
                if(!isset($validacion['num_flag_trabaja'])){
                    $validacion['num_flag_trabaja']=0;
                }
                if(!isset($validacion['fk_rhc064_nivel_grado_instruccion'])){
                    $validacion['fk_rhc064_nivel_grado_instruccion']=false;
                }
                if(!isset($validacion['fk_a006_num_tipo_zona'])){
                    $validacion['fk_a006_num_tipo_zona']=false;
                }
                if(!isset($validacion['fk_a006_num_ubicacion_inmueble'])){
                    $validacion['fk_a006_num_ubicacion_inmueble']=false;
                }
                if(!isset($validacion['ind_ubicacion_inmueble'])){
                    $validacion['ind_ubicacion_inmueble']=false;
                }
                if(!isset($validacion['ind_tipo_inmueble'])){
                    $validacion['ind_tipo_inmueble']=false;
                }
                if(!isset($validacion['fk_a006_num_zona_residencial'])){
                    $validacion['fk_a006_num_zona_residencial']=false;
                }
                if(!isset($validacion['ind_zona_residencial'])){
                    $validacion['ind_zona_residencial']=false;
                }
                if(!isset($validacion['ind_inmueble'])){
                    $validacion['ind_inmueble']=false;
                }
                if(!isset($validacion['ind_punto_referencia'])){
                    $validacion['ind_punto_referencia']=false;
                }
                if(!isset($validacion['ind_zona_postal'])){
                    $validacion['ind_zona_postal']=false;
                }    
                if(!isset($validacion['ind_direccion'])){
                    $validacion['ind_direccion']=false;
                }
                if(!isset($validacion['fk_a006_num_tipo_inmueble'])){
                    $validacion['fk_a006_num_tipo_inmueble']=false;
                }
                //ARREGLO DE DATOS PARA ENVIAR AL MODELO PARA LA MODIFICACION
                $datos = array(
                    $validacion['ind_cedula_documento'],
                    $validacion['fk_a006_num_miscelaneo_detalle_sexo'],
                    $validacion['fk_a006_num_miscelaneo_detalle_parentezco'],
                    $validacion['apellido1'],
                    $validacion['apellido2'],
                    $validacion['nombre1'],
                    $validacion['nombre2'],
                    $validacion['fec_nacimiento'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipodir'],
                    $validacion['ind_direccion'],
                    $validacion['fk_a006_num_miscelaneo_det_gruposangre'],
                    $validacion['fk_a006_num_miscelaneo_detalle_edocivil'],
                    $validacion['pk_num_telefono'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipo_telefono'],
                    $validacion['ind_telefono'],
                    $validacion['fk_a006_num_miscelaneo_detalle_gradoinst'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipoedu'],
                    $validacion['fk_rhc040_num_institucion'],
                    $validacion['ind_empresa'],
                    $validacion['txt_direccion_empresa'],
                    $validacion['num_sueldo_mensual'],
                    $validacion['pk_num_direccion_persona'],
                    $validacion['pk_num_telefono'],
                    $validacion['aleatorio'],
                    $validacion['fk_a006_num_miscelaneo_detalle_domicilio'],
                    $validacion['num_flag_discapacidad'],
                    $validacion['num_flag_estudia'],
                    $validacion['num_flag_trabaja'],
                    $validacion['fk_rhc064_nivel_grado_instruccion'],
                    $validacion['fk_a006_num_tipo_zona'],
                    $validacion['fk_a006_num_ubicacion_inmueble'],
                    $validacion['ind_ubicacion_inmueble'],
                    $validacion['fk_a006_num_tipo_inmueble'],
                    $validacion['ind_tipo_inmueble'],
                    $validacion['fk_a006_num_zona_residencial'],
                    $validacion['ind_zona_residencial'],
                    $validacion['ind_inmueble'],
                    $validacion['ind_punto_referencia'],
                    $validacion['ind_zona_postal']
                );
                #INSTANCIO EL METODO PARA MODIFICAR LA CARGA DE FAMILIAR
                $id = $this->atCargaFamiliar->metModificarCargaFamiliar($idCargaFamiliar,$idPersona,$idEmpleado,$datos);
                $validacion['idCargaFamiliar'] = $idCargaFamiliar;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    echo json_encode($validacion);
                    exit;
                }


            }

        }

        if($idCargaFamiliar!=0){
            $db=$this->atCargaFamiliar->metMostrarCargaFamiliar($idCargaFamiliar);
            $db1=$this->atCargaFamiliar->metMostrarDireccionCargaFamiliar($idCargaFamiliar);
            $db2=$this->atCargaFamiliar->metMostrarTelefonoCargaFamiliar($idCargaFamiliar);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('formDBdireccion',$db1);
            $this->atVista->assign('formDBtelefono',$db2);
            $this->atVista->assign('numero', 1);
            $this->atVista->assign('n', 0);
        }

        $miscelaneoTelefono   = $this->atEmpleados->metMostrarSelect('TIPOTELF');
        $miscelaneoSexo    = $this->atEmpleados->metMostrarSelect('SEXO');
        $miscelaneoParentesco = $this->atEmpleados->metMostrarSelect('PARENT');
        $miscelaneoDireccion  = $this->atEmpleados->metMostrarSelect('TIPODIR');
        $miscelaneoSangre     = $this->atEmpleados->metMostrarSelect('SANGRE');
        $miscelaneoEdoCivil   = $this->atEmpleados->metMostrarSelect('EDOCIVIL');
        $miscelaneoGradoInst  = $this->atEmpleados->metMostrarSelect('GRADOINST');
        $miscelaneoTipoEdu    = $this->atEmpleados->metMostrarSelect('TEDUCA');
        $miscelaneoDomicilio  = $this->atEmpleados->metMostrarSelect('SITDOM');
        $miscelaneoUbicacionInm  = $this->atEmpleados->metMostrarSelect('UBICINMUE');
        $miscelaneoTipoZona  = $this->atEmpleados->metMostrarSelect('TIPOZONA');
        $miscelaneoTipoInmueble = $this->atEmpleados->metMostrarSelect('TIPOINMU');
        $miscelaneoZonaResidencial = $this->atEmpleados->metMostrarSelect('ZONARESID');
        $this->atVista->assign('TipoTelf',$miscelaneoTelefono);
        $this->atVista->assign('Sexo',$miscelaneoSexo);
        $this->atVista->assign('Parentesco',$miscelaneoParentesco);
        $this->atVista->assign('GrupoSanguineo',$miscelaneoSangre);
        $this->atVista->assign('TipoDir',$miscelaneoDireccion);
        $this->atVista->assign('EdoCivil',$miscelaneoEdoCivil);
        $this->atVista->assign('GradoInst',$miscelaneoGradoInst);
        $this->atVista->assign('TipoEdu',$miscelaneoTipoEdu);
        $this->atVista->assign('SituacionDomicilio',$miscelaneoDomicilio);
        $this->atVista->assign('UbicacionInmueble',$miscelaneoUbicacionInm);
        $this->atVista->assign('TipoZona',$miscelaneoTipoZona);
        $this->atVista->assign('TipoInmueble',$miscelaneoTipoInmueble);
        $this->atVista->assign('ZonaResidencial',$miscelaneoZonaResidencial);

        $centros = $this->atCentroEstudio->metConsultarCentros();
        $this->atVista->assign('Centros',$centros);

        $nivelGradoInstruccion = $this->atNivelGradoInstruccion->metConsultarNivelGradoInstruccion();
        $this->atVista->assign('NivelGradoInstruccion',$nivelGradoInstruccion);


        $this->atVista->assign('idCargaFamiliar',$idCargaFamiliar);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->assign('tipo',$tipo);
        $this->atVista->metRenderizar('form', 'modales');
    }


    public function metVerificarCedulaPersona()
    {
        $cedula = $this->metObtenerTexto('cedula');
        $empleado = $this->metObtenerInt('empleado');
        $datos = $this->atCargaFamiliar->metVerificarCedulaPersona($cedula,$empleado);
        if(is_array($datos)){
            echo json_encode($datos);
        }else{
            echo json_encode($datos);
        }

    }
    
    public function metEliminarCargaFamiliar()
    {
        $idCargaFamiliar = $this->metObtenerInt('idCargaFamiliar');
        if($idCargaFamiliar!=0){
            $id=$this->atCargaFamiliar->metEliminarCargaFamiliar($idCargaFamiliar);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'idCargaFamiliar'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


    public function metGenerarCedula()
    {
        $empleado = $this->metObtenerInt('empleado');

        $ced = $this->atCargaFamiliar->metGenerarCedula($empleado);

        $cedula = array('cedula'=>$ced);

        echo json_encode($cedula);
    }


}
