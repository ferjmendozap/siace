<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Recursos Humanos
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Maikol Isava         | acc.desarrollo@cgesucre.gob.ve       | 0426-1814058
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class viaticosControlador extends Controlador
{
    private $atModeloViaticos;
    private $atIdUsuario;
    private $atDependencias;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atIdUsuario = Session::metObtener('idUsuario');
		$this->atModeloViaticos = $this->metCargarModelo('viaticos','gestion');
        $this->atDependencias = $this->metCargarModelo('dependencias','maestros');


    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array('materialSiace/App',
                'materialSiace/core/demo/DemoFormWizard',
                'materialSiace/core/demo/DemoFormComponents'

        );
        $unidadCombustible = Session::metObtener('CTRCOMBUSTIBLE');
        $datosDependencias  = $this->atModeloViaticos->metConsultarDepencias();
        $usuario  = $this->atModeloViaticos->metUsuario($this->atIdUsuario, 2);
        $datosEmpleados  = $this->atModeloViaticos->metConsultarEmpleados();
        $dependenciaSolicitante = $this->atModeloViaticos->metBuscarDependenciaEmpleado($usuario[0]['pk_num_empleado']);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('datosOrganismos',$this->atModeloViaticos->metConsultarOrganismos());
        $this->atVista->assign('idUsuario', $usuario[0]['pk_num_empleado']);
        $this->atVista->assign('usuario', $usuario);
        $this->atVista->assign('datosDependencias', $datosDependencias);
        $this->atVista->assign('datosEmpleados', $datosEmpleados);
        $this->atVista->assign('dependenciaSolicitante',$dependenciaSolicitante['fk_a004_num_dependencia']);
        $this->atVista->assign('unidadCombustible',$unidadCombustible);
        $this->atVista->metRenderizar('nuevaSolicitud','modales');

    }

    function metAgregarBeneficiarios()
    {

		$js = array('modRH/segundaTableDynamic');
		$this->atVista->metCargarJs($js);

        $organismo = $this->metObtenerInt('organismo');

		$listaEmpleados = $this->atModeloViaticos->metListarEmpleados('1',$organismo,'');
        $this->atVista->assign('datosDependencias', $this->atModeloViaticos->metConsultarDepencias());
        $this->atVista->assign('datosOrganismos',$this->atModeloViaticos->metConsultarOrganismos());
   	    $this->atVista->assign('listasEmpleados', $listaEmpleados);
		$this->atVista->metRenderizar('agregarBeneficiarios', 'modales');
		
    }

    function metFiltrarBeneficiarios()
    {
        $organismo = $this->metObtenerInt('organismo');
        $dependencia = $this->metObtenerInt('dependencia');

        if($dependencia == 0){
            $datos = $this->atModeloViaticos->metListarEmpleados('1',$organismo,'');//solo por organismo
        }else{
            $datos = $this->atModeloViaticos->metListarEmpleados('2',$organismo, $dependencia);//por dependencia
        }

        echo json_encode($datos);
    }

    function metRegistrarNuevaSolicitud(){
	
        $solicitante = $this->metObtenerInt('datosEmpleados');
        $fecha_ultima_modificacion = date('Y-m-d H:i:s');

        $fec_registro_viatico = date('Y-m-d');
        $anio = date('Y');

        $fec_salida = $_POST['fec_salida'];//se usa el post por motivos de los caracteres especiales
        $aux =explode("/", $fec_salida);
        $fec_salida = $aux[2]."-".$aux[1]."-".$aux[0];

        $fec_regreso = $_POST['fec_regreso'];
        $aux =explode("/", $fec_regreso);
        $fec_regreso = $aux[2]."-".$aux[1]."-".$aux[0];

        $num_total_dias = $this->metObtenerInt('num_total_dias');

        $ind_destino = $this->metObtenerTexto('ind_destino');
        $motivo = $this->metObtenerTexto('ind_motivo');
        $dependencia = $this->metObtenerInt('ind_dependencia');
        $organismo = $this->metObtenerTexto('ind_descripcion_empresa');
        $unidadCombustible = $this->metObtenerInt('num_unidad_combustible');
        if(!isset($unidadCombustible)){
            $unidadCombustible=0;
        }else{
            $unidadCombustible = $unidadCombustible;
        }
        $intDependencia = $this->atDependencias->metNumeroInteroDependencia($dependencia);
        $numeroInternoDependencia = $intDependencia['ind_codinterno'];

            $correlativo = $this->atModeloViaticos->metCorrelativoCodInternoViatico($dependencia,$anio);
            $trozo_correlativo = $correlativo['total'];

            $aux_correlativo = (int) ($trozo_correlativo+1);
            $aux_correlativo = (string) str_repeat("0", 3-strlen($aux_correlativo)).$aux_correlativo;

            $cod_interno = $numeroInternoDependencia."-".$aux_correlativo."-".$anio;
            $empleadoPreparado = Session::metObtener('idEmpleado');

                //Nota: en aprobado por por defecto es $this->atIdUsuario.

                $idViatico = $this->atModeloViaticos->metInsertarViatico($anio,
                    $fec_registro_viatico,
                    $solicitante,
                    $fec_salida,
                    $fec_regreso,
                    $num_total_dias,
                    $ind_destino,
                    '',
                    '',
                    $motivo,
                    $cod_interno,
                    $empleadoPreparado,
                    trim($dependencia),
                    $organismo,
                    'PR',
                    0,
                    NULL,$unidadCombustible);


                if($idViatico > 0){

                    $cad_beneficiarios = $_POST['id_beneficiarios'];
                    $aux_beneficiarios =explode("#", $cad_beneficiarios);
                    array_pop($aux_beneficiarios);//eliminamos el ultimo elemento del array (que esta en blanco)

                    $totalAux = count($aux_beneficiarios);

                    $cad_totales_dias_celdas = $_POST['totales_dias_celdas'];
                    $aux_totales_dias =explode("#", $cad_totales_dias_celdas);
                    array_pop($aux_totales_dias);

                    $cad_fechas_salidas_celdas = $_POST['fechas_salidas_celdas'];
                    $aux_fechas_salida =explode("#", $cad_fechas_salidas_celdas);
                    array_pop($aux_fechas_salida);

                    $cad_fechas_regresos_celdas = $_POST['fechas_regresos_celdas'];
                    $aux_fechas_regreso =explode("#", $cad_fechas_regresos_celdas);
                    array_pop($aux_fechas_regreso);


                    for($i=0; $i<$totalAux; $i++){


                        $aux =explode("/", $aux_fechas_salida[$i]);
                        $fec_salida = $aux[2]."-".$aux[1]."-".$aux[0];

                        $aux =explode("/", $aux_fechas_regreso[$i]);
                        $fec_regreso = $aux[2]."-".$aux[1]."-".$aux[0];

                        $idViaticoDetalle = $this->atModeloViaticos->metInsertarDetalleViatico(
                            $idViatico,
                            $aux_beneficiarios[$i],
                            $fec_salida,
                            $fec_regreso,
                            $aux_totales_dias[$i]
                        );

                        if($idViaticoDetalle <= 0){

                            break;

                        }

                    }

                    if($idViaticoDetalle <=0){

                        echo json_encode("error4: ".$idViatico."#".$fec_salida."#".$fec_regreso."#".$aux_totales_dias[$i]."#".$fecha_ultima_modificacion."#".$this->atIdUsuario);
                    }else{
                        echo json_encode("1");
                    }


                }else{

                    echo json_encode("error2: ".$idViatico);
                }




    }

    function metListarSolicitudes(){

         $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array('materialSiace/core/demo/DemoTableDynamic',
                'materialSiace/core/demo/DemoFormWizard',
                'materialSiace/core/demo/DemoFormComponents'

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
		
		$datosOrganismos = $this->atModeloViaticos->metConsultarOrganismos();
		$organismos = $datosOrganismos[0]["pk_num_organismo"];
		$datosDependencias =$this->atModeloViaticos->metConsultarDepencias();
        $this->atVista->assign('datosOrganismos', $datosOrganismos);
        $this->atVista->assign('datosDependencias', $datosDependencias);


        //****************************************
        $fecha_rang1 = date('Y-m-01');
        $fecha_rang2 = date('Y-m-d');

        $fecha_rang1b = date('01/m/Y');
        $fecha_rang2b = date('d/m/Y');

        $this->atVista->assign('fecha_rang1', $fecha_rang1b);
        $this->atVista->assign('fecha_rang2', $fecha_rang2b);
        //****************************************
        $listado = $this->atModeloViaticos->metListarViaticos($organismos, '', 'PR', $fecha_rang1, $fecha_rang2);

        //********************************************************
        for($i=0; $i<count($listado); $i++){

            $aux = explode("-",$listado[$i]['fec_registro_viatico']);
            $listado[$i]['fec_registro_viatico'] = $aux[2]."/".$aux[1]."/".$aux[0];

            $aux = explode("-",$listado[$i]['fec_salida']);
            $listado[$i]['fec_salida'] = $aux[2]."/".$aux[1]."/".$aux[0];

            $aux = explode("-",$listado[$i]['fec_ingreso']);
            $listado[$i]['fec_ingreso'] = $aux[2]."/".$aux[1]."/".$aux[0];

        }
        //********************************************************
        if(is_array($listado) || $listado == 0){
			
			 $this->atVista->assign('listaViaticos', $listado);
			
		}else{
				echo json_encode(-1);		
		}
		
        $this->atVista->metRenderizar('listarSolicitudes');
		
	
		
    }
	
	function metFiltrarListadoViaticos(){
	
        $organismo = $this->metObtenerInt('organismo');
        $dependencia = $this->metObtenerInt('dependencia');
		$estado = $this->metObtenerTexto('estado');
		
		$fecha_prepa1 = $_POST['fec_start'];
		$fecha_prepa2 = $_POST['fec_end'];
		
		if(!empty($fecha_prepa1)){
			$aux =explode("/", $fecha_prepa1);
			$fecha_prepa1 = $aux[2]."-".$aux[1]."-".$aux[0];
						
			$aux =explode("/", $fecha_prepa2);
			$fecha_prepa2 = $aux[2]."-".$aux[1]."-".$aux[0];
		}
		
		$listado = $this->atModeloViaticos->metListarViaticos($organismo, $dependencia, $estado, $fecha_prepa1, $fecha_prepa2);
        //*****************************************
        for($i=0; $i<count($listado); $i++){

            $aux = explode("-",$listado[$i]['fec_registro_viatico']);
            $listado[$i]['fec_registro_viatico'] = $aux[2]."/".$aux[1]."/".$aux[0];

            $aux = explode("-",$listado[$i]['fec_salida']);
            $listado[$i]['fec_salida'] = $aux[2]."/".$aux[1]."/".$aux[0];

            $aux = explode("-",$listado[$i]['fec_ingreso']);
            $listado[$i]['fec_ingreso'] = $aux[2]."/".$aux[1]."/".$aux[0];

        }
        //********************************************
        echo json_encode($listado);
		
		
		
    }
	
	function metEditarViatico(){


        $complementosCss = array(
            'wizard/wizardfa6c',

        );
        $complementoJs = array(

            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'


        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $datosEmpleados  = $this->atModeloViaticos->metConsultarEmpleados();
        $pk_num_viatico  = $this->metObtenerInt('pk_num_viatico');

        $viatico = $this->atModeloViaticos->metBuscarViatico($pk_num_viatico);

        //**************************
        $aux = explode("-",$viatico['fec_salida']);
        $viatico['fec_salida'] = $aux[2]."/".$aux[1]."/".$aux[0];

        $aux = explode("-",$viatico['fec_ingreso']);
        $viatico['fec_ingreso'] = $aux[2]."/".$aux[1]."/".$aux[0];
        //**************************

        $detallesViaticos = $this->atModeloViaticos->metBuscarDetalleViatico($pk_num_viatico);

        //**************************
        for($i=0; $i<count($detallesViaticos); $i++){

            $aux = explode("-",$detallesViaticos[$i]['fec_salida_detalle']);
            $detallesViaticos[$i]['fec_salida_detalle'] = $aux[2]."/".$aux[1]."/".$aux[0];

            $aux = explode("-",$detallesViaticos[$i]['fec_ingreso_detalle']);
            $detallesViaticos[$i]['fec_ingreso_detalle'] = $aux[2]."/".$aux[1]."/".$aux[0];
        }

        //**************************


        $preparadoPor = $this->atModeloViaticos->metConsultarEmpleadoUnico($viatico["fk_rhb001_num_empleado_preparado_por"]);

        $usuarioUltimaModificacion = $this->atModeloViaticos->metBuscarDatosUsuarioSeguridad($viatico["fk_a018_num_seguridad_usuario"]);
        $this->atVista->assign('datosUltimaModificacion', $usuarioUltimaModificacion);

		$this->atVista->assign('datosDependencias', $this->atModeloViaticos->metConsultarDepencias());
        $this->atVista->assign('datosOrganismos',$this->atModeloViaticos->metConsultarOrganismos());
   	    $this->atVista->assign('datosEmpleados', $datosEmpleados);

        $this->atVista->assign('datosViatico', $viatico);
        $this->atVista->assign('preparadoPor', $preparadoPor);
        $this->atVista->assign('idUsuario', $this->atIdUsuario);
        $this->atVista->assign('datosDetallesViaticos', $detallesViaticos);

        //********************************************
        if(strcmp($viatico['ind_estado'],"PR")==0){
            $cad_estado_titulo ='Preparado';
        }elseif(strcmp($viatico['ind_estado'],"AP")==0){
            $cad_estado_titulo ='Aprobado';
        }elseif(strcmp($viatico['ind_estado'],"AN")==0){
            $cad_estado_titulo ='Anulado';
        }
        $this->atVista->assign('titulo_form', "[Estatus: ".$cad_estado_titulo."]");
        //********************************************
		$this->atVista->metRenderizar("modificarViatico", 'modales');
				
	}

    function metAgregarBeneficiariosDetalle(){

        $js = array('modRH/segundaTableDynamic');
        $this->atVista->metCargarJs($js);

        $organismo = $this->metObtenerInt('organismo');

        $listaEmpleados = $this->atModeloViaticos->metListarEmpleados('1',$organismo,'');
        $this->atVista->assign('datosDependencias', $this->atModeloViaticos->metConsultarDepencias());
        $this->atVista->assign('datosOrganismos',$this->atModeloViaticos->metConsultarOrganismos());
        $this->atVista->assign('listasEmpleados', $listaEmpleados);
        $this->atVista->metRenderizar('agregarBeneficiariosDetalle', 'modales');
    }

    function metRegistrarModificacion(){

        //************************************

        $fec_ultima_modificacion = date('Y-m-d H:i:s');

        $fec_salida = $_POST['fec_salida'];
        $aux =explode("/", $fec_salida);
        $fec_salida = $aux[2]."-".$aux[1]."-".$aux[0];
        $fec_regreso = $_POST['fec_regreso'];
        $aux =explode("/", $fec_regreso);
        $fec_regreso = $aux[2]."-".$aux[1]."-".$aux[0];
        $num_total_dias = $this->metObtenerInt('num_total_dias');
        $ind_destino = $this->metObtenerTexto('ind_destino');
        $motivo = $this->metObtenerTexto('ind_motivo');
        $pk_viatico = $this->metObtenerInt('pk_viatico');

        $respuestaViatico = $this->atModeloViaticos->metModificarViatico($pk_viatico, $fec_salida, $fec_regreso, $num_total_dias, $ind_destino, $motivo, $fec_ultima_modificacion, $this->atIdUsuario );

        if($respuestaViatico){

            $respuestaDetalle = $this->atModeloViaticos->metEliminarDetalleViatico($pk_viatico);
            if($respuestaDetalle) {

                $cad_beneficiarios = $_POST['id_beneficiariosDetalle'];
                $aux_beneficiarios = explode("#", $cad_beneficiarios);
                array_pop($aux_beneficiarios);//eliminamos el ultimo elemento del array (que esta en blanco)

                $totalAux = count($aux_beneficiarios);

                $cad_totales_dias_celdas = $_POST['totales_dias_celdasDetalle'];
                $aux_totales_dias = explode("#", $cad_totales_dias_celdas);
                array_pop($aux_totales_dias);

                $cad_fechas_salidas_celdas = $_POST['fechas_salidas_celdasDetalle'];
                $aux_fechas_salida = explode("#", $cad_fechas_salidas_celdas);
                array_pop($aux_fechas_salida);

                $cad_fechas_regresos_celdas = $_POST['fechas_regresos_celdasDetalle'];
                $aux_fechas_regreso = explode("#", $cad_fechas_regresos_celdas);
                array_pop($aux_fechas_regreso);

                for($i=0; $i<$totalAux; $i++){


                    $aux =explode("/", $aux_fechas_salida[$i]);
                    $fec_salida = $aux[2]."-".$aux[1]."-".$aux[0];

                    $aux =explode("/", $aux_fechas_regreso[$i]);
                    $fec_regreso = $aux[2]."-".$aux[1]."-".$aux[0];

                    $idViaticoDetalle = $this->atModeloViaticos->metInsertarDetalleViatico(
                        $pk_viatico,
                        $aux_beneficiarios[$i],
                        $fec_salida,
                        $fec_regreso,
                        $aux_totales_dias[$i],
                        $fec_ultima_modificacion,
                        $this->atIdUsuario);

                    if($idViaticoDetalle <= 0){

                        break;

                    }

                }

                if($idViaticoDetalle <=0){

                    echo json_encode(-3);

                }else{

                    echo json_encode(1);
                }


            }else{
                echo json_encode(-2);
            }

        }else{

            echo json_encode(-1);
        }

    }

    function metAnularVerAprobarViatico(){

        $js = array('modRH/segundaTableDynamic');
        $this->atVista->metCargarJs($js);

        $datosEmpleados  = $this->atModeloViaticos->metConsultarEmpleados();
        $pk_num_viatico  = $this->metObtenerInt('pk_num_viatico');

        $viatico = $this->atModeloViaticos->metBuscarViatico($pk_num_viatico);

        //**************************
        $aux = explode("-",$viatico['fec_salida']);
        $viatico['fec_salida'] = $aux[2]."/".$aux[1]."/".$aux[0];

        $aux = explode("-",$viatico['fec_ingreso']);
        $viatico['fec_ingreso'] = $aux[2]."/".$aux[1]."/".$aux[0];
        //**************************

        $detallesViaticos = $this->atModeloViaticos->metBuscarDetalleViatico($pk_num_viatico);

        //**************************
        for($i=0; $i<count($detallesViaticos); $i++){

            $aux = explode("-",$detallesViaticos[$i]['fec_salida_detalle']);
            $detallesViaticos[$i]['fec_salida_detalle'] = $aux[2]."/".$aux[1]."/".$aux[0];

            $aux = explode("-",$detallesViaticos[$i]['fec_ingreso_detalle']);
            $detallesViaticos[$i]['fec_ingreso_detalle'] = $aux[2]."/".$aux[1]."/".$aux[0];
        }

        //**************************


        $preparadoPor = $this->atModeloViaticos->metConsultarEmpleadoUnico($viatico["fk_rhb001_num_empleado_preparado_por"]);

        $usuarioUltimaModificacion = $this->atModeloViaticos->metBuscarDatosUsuarioSeguridad($viatico["fk_a018_num_seguridad_usuario"]);
        $this->atVista->assign('datosUltimaModificacion', $usuarioUltimaModificacion);

        $this->atVista->assign('datosDependencias', $this->atModeloViaticos->metConsultarDepencias());
        $this->atVista->assign('datosOrganismos',$this->atModeloViaticos->metConsultarOrganismos());
        $this->atVista->assign('datosEmpleados', $datosEmpleados);

        $this->atVista->assign('datosViatico', $viatico);
        $this->atVista->assign('preparadoPor', $preparadoPor);
        $this->atVista->assign('idUsuario', $this->atIdUsuario);
        $this->atVista->assign('datosDetallesViaticos', $detallesViaticos);
        $this->atVista->assign('band_form', $_POST['band_form']);

        if($_POST['band_form']=="a"){
            $this->atVista->assign('titulo_form', "Anular Solicitud de Viaticos" );

        }elseif($_POST['band_form']=="v"){

            if(strcmp($viatico['ind_estado'],"PR")==0){
                $cad_estado_titulo ='Preparado';
            }elseif(strcmp($viatico['ind_estado'],"AP")==0){
                $cad_estado_titulo ='Aprobado';
            }elseif(strcmp($viatico['ind_estado'],"AN")==0){
                $cad_estado_titulo ='Anulado';
            }

            $this->atVista->assign('titulo_form', "Ver Solicitud de Viatico [Estatus: ".$cad_estado_titulo."]");

        }elseif($_POST['band_form'] == "p"){

            $this->atVista->assign('titulo_form', "Aprobar Solicitud de Viaticos");

        }
        $unidadCombustible = Session::metObtener('CTRCOMBUSTIBLE');
        $this->atVista->assign('unidadCombustible',$unidadCombustible);
        $this->atVista->metRenderizar('anularAprobarSolicitud', 'modales');


    }

    function metRegistrarAnulacion(){

        $pk_viatico_anular = $this->metObtenerInt('pk_viatico_anular');

        if(empty($_POST['ind_razon_rechazo'])){

            $razon_rechazo = '';

        }else{

            $razon_rechazo = $_POST['ind_razon_rechazo'];
        }



        $fec_ultima_modificacion = date('Y-m-d H:i:s');

        $respuesta = $this->atModeloViaticos->metActualizarEstadoViatico($pk_viatico_anular, 'AN', $razon_rechazo,'', $fec_ultima_modificacion, $this->atIdUsuario);

        if($respuesta){

            echo json_encode(1);

        }else{

            echo json_encode(-1);
        }



    }

    function metRegistrarAprobacion(){

        $pk_viatico_anular = $this->metObtenerInt('pk_viatico_anular');

        if(empty($_POST['ind_observacion'])){

            $ind_observacion = '';

        }else{

            $ind_observacion = $_POST['ind_observacion'];
        }

        $unidadCombustible = $this->metObtenerInt('num_unidad_combustible');
        if(!isset($unidadCombustible)){
            $unidadCombustible=0;
        }else{
            $unidadCombustible = $unidadCombustible;
        }

        $fec_ultima_modificacion = date('Y-m-d H:i:s');
        $empleadoAprobador = Session::metObtener('idEmpleado');

        $respuesta = $this->atModeloViaticos->metActualizarEstadoViatico($pk_viatico_anular, 'AP', '', $ind_observacion, $fec_ultima_modificacion,$empleadoAprobador,$unidadCombustible);

        if($respuesta){

            echo json_encode(1);

        }else{

            echo json_encode(-1);
        }



    }

    function metAprobarViaticos(){

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js = array('materialSiace/core/demo/DemoTableDynamic',
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $datosOrganismos = $this->atModeloViaticos->metConsultarOrganismos();
        $organismos = $datosOrganismos[0]["pk_num_organismo"];
        $datosDependencias =$this->atModeloViaticos->metConsultarDepencias();
        $this->atVista->assign('datosOrganismos', $datosOrganismos);
        $this->atVista->assign('datosDependencias', $datosDependencias);

        $listado = $this->atModeloViaticos->metListarViaticos($organismos, '', 'PR', '', '');

        //*****************************************
        for($i=0; $i<count($listado); $i++){

            $aux = explode("-",$listado[$i]['fec_registro_viatico']);
            $listado[$i]['fec_registro_viatico'] = $aux[2]."/".$aux[1]."/".$aux[0];

            $aux = explode("-",$listado[$i]['fec_salida']);
            $listado[$i]['fec_salida'] = $aux[2]."/".$aux[1]."/".$aux[0];

            $aux = explode("-",$listado[$i]['fec_ingreso']);
            $listado[$i]['fec_ingreso'] = $aux[2]."/".$aux[1]."/".$aux[0];

        }
        //*********************************************
        if(is_array($listado) || $listado == 0){

            $this->atVista->assign('listaViaticos', $listado);

        }else{
            echo json_encode(-1);
        }

        $this->atVista->metRenderizar('listarSolicitudesAprobar');

    }

    public function metReporteViatico()
    {
        $pk_num_viatico = $_GET['pk_num_viatico'];
        $viatico = $this->atModeloViaticos->metBuscarViatico($pk_num_viatico);
        if($viatico){

            ini_set('error_reporting', 'E_ALL & ~E_STRICT');
            $this->metObtenerLibreria('cabeceraViaticos','modRH');

            $unidadCombustible = Session::metObtener('CTRCOMBUSTIBLE');

            $pdf= new pdfViatico('P','mm','Letter');
            $pdf->AliasNbPages();
            $pdf->AddPage();

            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Arial', 'B', 8);
            //****************************************
            $cad_aprobado_por = '';
            $cad_cargo_aprobado = '';
            if ($viatico['ind_estado'] == "PR") {

                $cad_estado = 'PENDIENTE POR APROBAR';
                $cad_aprobado_por = 'PENDIENTE POR APROBAR';

            }elseif($viatico["ind_estado"] == "AN"){

                $cad_aprobado_por = 'ANULADO';
                $cad_estado = 'ANULADO';

            }else{
                $datosEmpAprobadoEmpleado = $this->atModeloViaticos->metBusquedaUnicaDatosEmpleados($viatico['fk_rhb001_num_empleado_aprobado_por']);
                $cad_aprobado_por = $datosEmpAprobadoEmpleado["ind_nombre1"]." ".$datosEmpAprobadoEmpleado["ind_nombre2"]." ".$datosEmpAprobadoEmpleado["ind_apellido1"]." ".$datosEmpAprobadoEmpleado["ind_apellido2"];
                $cad_cargo_aprobado = $datosEmpAprobadoEmpleado["ind_descripcion_cargo"];
                $cad_estado = 'APROBADO';
            }

            $pdf->SetFillColor(250, 250, 250);

            //******************************************
            $pdf->Cell(200, 5,utf8_decode('SOLICITUD VIATICO  N° '.$viatico["cod_interno"])." (".$cad_estado.")", 0, 1, 'C');
            $pdf->Ln(10);

            $pdf->Cell(0, 5,utf8_decode('C.I.:  '.$viatico["ind_cedula_documento"]), 0, 1, 'L');
            $pdf->Cell(0, 5,utf8_decode('Nombres y Apellidos:  '.$viatico["ind_nombre1"]." ".$viatico["ind_nombre2"]." ".$viatico["ind_apellido1"]." ".$viatico["ind_apellido2"]), 0, 1, 'L');
            $pdf->Cell(0, 5,utf8_decode('Dependencia:  '.$viatico["ind_dependencia"]), 0, 1, 'L');
            //**************************
            $aux = explode("-",$viatico['fec_salida']);
            $viatico['fec_salida'] = $aux[2]."/".$aux[1]."/".$aux[0];
            $aux = explode("-",$viatico['fec_ingreso']);
            $viatico['fec_ingreso'] = $aux[2]."/".$aux[1]."/".$aux[0];
            //**************************
            if($unidadCombustible==1){
                $pdf->Cell(0, 5,utf8_decode('Fecha Salidad:  '.$viatico["fec_salida"]."       Fecha Regreso:  ".$viatico["fec_ingreso"]."       Días:  ".$viatico["num_total_dias"]."       Unidad de Combustible: ".$viatico["num_unidad_combustible"]), 0, 1, 'L');
            }else{
                $pdf->Cell(0, 5,utf8_decode('Fecha Salidad:  '.$viatico["fec_salida"]."       Fecha Regreso:  ".$viatico["fec_ingreso"]."       Días:  ".$viatico["num_total_dias"]), 0, 1, 'L');
            }


            $pdf->Cell(14, 5,'Destino: ',0,0,'L',1);
            $pdf->SetFont('Arial', '', 8);
            $pdf->MultiCell(175, 5,utf8_decode($viatico["ind_destino"]), 0,'J');
            $pdf->Ln(2);
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(14, 5,'Motivo: ', 0,0,'L',1);
            $pdf->SetFont('Arial', '', 8);
            $pdf->MultiCell(0, 5,utf8_decode($viatico["ind_motivo"]), 0,'J');
            $pdf->Ln(2);
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(21, 5,utf8_decode('Observaciones: '),0,0,'L',1);
            $pdf->SetFont('Arial', '', 8);
            $pdf->MultiCell(0, 5,utf8_decode($viatico["ind_observacion"]), 0,'J');
            $pdf->Ln(5);

            //************************************************
            $detallesViaticos = $this->atModeloViaticos->metBuscarDetalleViatico($pk_num_viatico);

            //**************************
            for($i=0; $i<count($detallesViaticos); $i++){

                $aux = explode("-",$detallesViaticos[$i]['fec_salida_detalle']);
                $detallesViaticos[$i]['fec_salida_detalle'] = $aux[2]."/".$aux[1]."/".$aux[0];

                $aux = explode("-",$detallesViaticos[$i]['fec_ingreso_detalle']);
                $detallesViaticos[$i]['fec_ingreso_detalle'] = $aux[2]."/".$aux[1]."/".$aux[0];

                $nombreApellidoBene = $detallesViaticos[$i]["ind_nombre1"]." ".$detallesViaticos[$i]["ind_nombre2"]." ".$detallesViaticos[$i]["ind_apellido1"]." ".$detallesViaticos[$i]["ind_apellido2"];

                $datosBeneficiarios[$i][0] = $detallesViaticos[$i]['ind_cedula_documento'];
                $datosBeneficiarios[$i][1] = utf8_decode($nombreApellidoBene);
                $datosBeneficiarios[$i][2] = $detallesViaticos[$i]['fec_salida_detalle'];
                $datosBeneficiarios[$i][3] = $detallesViaticos[$i]['fec_ingreso_detalle'];
                $datosBeneficiarios[$i][4] = $detallesViaticos[$i]['num_total_dias_detalle'];

            }
            //****************************************************
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(0, 0,utf8_decode('BENEFICIARIOS'), 0,'J');
            $pdf->Ln(5);
            $header = array(utf8_decode('Cédula'), 'Nombres y apellidos', 'Fecha inicio', 'Fecha fin', utf8_decode('Total días'));
            $pdf->BasicTableViatico($header,$datosBeneficiarios);



            $preparadoEmpleado = $this->atModeloViaticos->metBusquedaUnicaDatosEmpleados($viatico["fk_rhb001_num_empleado_preparado_por"]);
            $cad_nombre_preparado = $preparadoEmpleado["ind_nombre1"]." ".$preparadoEmpleado["ind_nombre2"]." ".$preparadoEmpleado["ind_apellido1"]." ".$preparadoEmpleado["ind_apellido2"];


            $pdf->SetXY(10,225);
            //***************************
            $aux = explode("-",$viatico['fec_registro_viatico']);
            $fecha_registro = "Fecha: ".$aux[2]."/".$aux[1]."/".$aux[0];
            //***************************
            $pdf->Cell(100, 10,$fecha_registro, 0,'J');


            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetXY(10, 233);
            $pdf->Rect(10, 238, 95, 25, "D");
            $pdf->Rect(105, 238, 95, 25, "D");

            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(95, 5, 'PREPARADO POR', 1, 0, 'L');
            $pdf->Cell(95, 5, 'APROBADO POR', 1, 0, 'L');


            ##
            $pdf->SetFont('Arial', 'B', 7);
            $pdf->SetXY(10, 242); $pdf->MultiCell(95, 3, utf8_decode(strtoupper($cad_nombre_preparado)), 0, 'L');
            $pdf->SetXY(105, 242); $pdf->MultiCell(95, 3, utf8_decode(strtoupper($cad_aprobado_por)), 0, 'L');


            ##
            $pdf->SetFont('Arial', 'B', 6);
            $pdf->SetXY(10, 250); $pdf->MultiCell(95, 3, utf8_decode($preparadoEmpleado["ind_descripcion_cargo"]), 0, 'C');
            $pdf->SetXY(105, 250); $pdf->MultiCell(95, 3, utf8_decode($cad_cargo_aprobado), 0, 'C');


            $pdf->Output();

        }

    }





}
