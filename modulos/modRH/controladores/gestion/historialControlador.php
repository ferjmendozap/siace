<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Consumo de Red
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de historial del empleado
class historialControlador extends Controlador
{
	private $atHistorial;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
	}

	// Index del controlador
	public function metIndex()
	{

	}



	/*INICIO - Métodos que permite emitir el reporte correspondiente al historial del empleado*/
	public function metReporteHistorial($idEmpleado)
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');

		$this->metObtenerLibreria('cabeceraRH', 'modRH');
		$pdf = new pdfEmpleado('L', 'mm', 'Legal');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 5);
		$pdf->Header(1,'HISTORIAL DEL EMPLEADO');

		$this->atHistorial = $this->metCargarModelo('historial','gestion');

		$historial = $this->atHistorial->metListarHistorial($idEmpleado);
	
		foreach($historial as $datos){
			
			if($datos['ind_estatus']==1){
				$datos['ind_estatus']='A';
			}else{
				$datos['ind_estatus']='I';
			}

			$pdf->SetTextColor(0, 0, 0);
			$pdf->SetDrawColor(255, 255, 255);
			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetFont('Arial', '', 6);
			$pdf->Row(array($datos['fec_ingreso'],
				utf8_decode($datos['ind_dependencia']),
				utf8_decode($datos['ind_cargo']),
				utf8_decode($datos['ind_categoria_cargo']),
				number_format($datos['num_nivel_salarial'], 2, ',', '.'),
				utf8_decode($datos['ind_tipo_nomina']),
				utf8_decode($datos['ind_tipo_pago']),
				utf8_decode($datos['ind_tipo_trabajador']),
				$datos['ind_estatus'],
				utf8_decode($datos['ind_motivo_cese']),
				$datos['fec_egreso'],
				utf8_decode($datos['txt_obs_cese'])));
			
			
		}


		$pdf->Output();

	}

	/*FIN - Métodos que permite emitir el reporte correspondiente al historial del empleado*/


}
?>
