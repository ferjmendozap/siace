<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class nivelacionesControlador extends Controlador
{
    private $atEmpleados;
    private $atNivelaciones;
    private $atOrganismo;
    private $atDependencias;
    private $atTipoNimina;
    private $atCargos;
    private $atPasos;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados    = $this->metCargarModelo('empleados','gestion');
        $this->atNivelaciones = $this->metCargarModelo('nivelaciones','gestion');
        $this->atOrganismo    = $this->metCargarModelo('organismo','maestros','modCV');
        $this->atDependencias = $this->metCargarModelo('dependencias','maestros');
        $this->atTipoNimina   = $this->metCargarModelo('tipoNomina','maestros','modNM');
        $this->atCargos       = $this->metCargarModelo('cargos','maestros');
        $this->atPasos        = $this->metCargarModelo('pasos','maestros');
     }

    #Metodo Index del controlador
    public function metIndex()
    {


    }

    public function metNivelaciones()
    {


        $complementosCss = array(
            'select2/select201ef','bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min','bootstrap-datepicker/bootstrap-datepicker'
        );

        $js = array ('materialSiace/App','modRH/modRHFunciones','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idNivelacion = $this->metObtenerInt('idNivelacion');
        $idEmpleado   = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');


        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        if((strcmp($tituloInt,'sueldo')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    }else{
                        $validacion[$tituloInt]='';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idNivelacion===0){//PARA INSERTAR REGISTRO

                $validacion['status']='creacion';

                #INSTANCIO EL METODO PARA REGISTRAR LAS NIVELACIONES
                $id = $this->atNivelaciones->metRegistrarNivelacion($idEmpleado,
                    $validacion['fec_fecha_registro'],
                    $validacion['fk_a001_num_organismo'],
                    $validacion['fk_a004_num_dependencia'],
                    $validacion['fk_a023_num_centro_costo'],
                    $validacion['fk_rhc063_num_puestos'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipoaccion'],
                    $validacion['ind_motivo'],
                    $validacion['ind_documento'],
                    $validacion['fk_rhb001_num_empleado_responsable'],
                    $validacion['accion'],
                    $validacion['cargoAnterior'],
                    $validacion['dependenciaAnterior'],
                    $validacion['ind_estatus'],
                    $validacion['fk_a006_num_miscelaneo_detalle_paso'],
                    $validacion['sueldo'],
                    $validacion['txt_observaciones']
                );

                $validacion['idNivelacion'] = $id;

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $datos = $this->atNivelaciones->metMostrarHistorialNivelaciones($id);
                    if(!isset($datos['ind_tipo_accion'])){
                        $validacion['ind_tipo_accion'] = '';
                    }else{
                        $validacion['ind_tipo_accion'] = $datos['ind_tipo_accion'];
                    }
                    $validacion['ind_dependencia'] = $datos['ind_dependencia'];
                    $validacion['ind_cargo']       = $datos['ind_cargo'];
                    $validacion['ind_categoria_cargo'] = $datos['ind_categoria_cargo'];
                    $validacion['ind_tipo_nomina'] = $datos['ind_tipo_nomina'];
                    echo json_encode($validacion);
                    exit;
                }
            }
        }

        $miscelaneoTipoAccion = $this->atEmpleados->metMostrarSelect('TIPOACCION');
        $this->atVista->assign('TipoAccion',$miscelaneoTipoAccion);

        /*EMPLEADOS*/
        $empleados = $this->atEmpleados->metListarEmpleado('AP','operaciones');
        $this->atVista->assign('Empleado',$empleados);

        /*CONDICIONES ANTERIORES DEL EMPLEADO PARA LA NIVELACION*/
        $datos = $this->atNivelaciones->metConsultarDatosAnterioresNivelacion($idEmpleado);
        $this->atVista->assign('dataAnterior',$datos);
        $datosSueldo = $this->atNivelaciones->metConsultarDatosAnterioresNivelacionSueldo($idEmpleado);
        $this->atVista->assign('dataAnteriorSueldo',$datosSueldo);


        /*PARA MOSTRAR CATEGORIA DEL CARGO DE LAS CONDICONES ANTERIORES*/
        $cat = $this->atEmpleados->metObtenerSueldoBasico($datos['fk_rhc063_num_puestos_cargo']);
        $this->atVista->assign('categoria',$cat['ind_nombre_detalle']);

        #PARA LLENAR EL SELECT PASOS
        $Pasos = $this->atPasos->metConsultarPasos();
        $this->atVista->assign('Pasos',$Pasos);
        #************************************************************

        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoDependencias',$this->atDependencias->metConsultarDependencias());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoCargos',$this->atCargos->metConsultarCargos());
        $this->atVista->assign('listadoCentroCostos',$this->atNivelaciones->metListadoCentroCostos());

        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->assign('idNivelacion',$idNivelacion);
        $this->atVista->metRenderizar('form', 'modales');
    }

    /*INICIO - Métodos que permite emitir el reporte correspondiente a las nivelaciones del empleado*/
    public function metReporteHistorialNivelacion($idEmpleado)
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');

        $this->metObtenerLibreria('cabeceraRH', 'modRH');
        $pdf = new pdfEmpleado('L', 'mm', 'Legal');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 5);
        $pdf->Header(2,'HISTORIAL DE NIVELACIONES');

        $nivelaciones = $this->atNivelaciones->metListarHistorialNivelaciones($idEmpleado);

        foreach($nivelaciones as $datos){


            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetDrawColor(255, 255, 255);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetFont('Arial', '', 6);
            $pdf->SetAligns(array('C', 'L', 'L', 'L', 'L', 'R', 'L', 'L'));
            $pdf->Row(array($datos['fec_fecha_registro'],
                utf8_decode($datos['ind_tipo_accion']),
                utf8_decode($datos['ind_dependencia']),
                utf8_decode($datos['ind_cargo']),
                utf8_decode($datos['ind_categoria_cargo']),
                number_format($datos['num_nivel_salarial'], 2, ',', '.'),
                utf8_decode($datos['ind_tipo_nomina']),
                utf8_decode($datos['nombre_responsable'])));


        }


        $pdf->Output();

    }

    /*FIN - Métodos que permite emitir el reporte correspondiente a las nivelaciones del empleado*/



}
