<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class capacitacionControlador extends Controlador
{
    private $atEmpleados;
    private $atCapacitacion;
    private $atIdUsuario;
    private $atOrganismo;
    private $atCurso;
    private $atCentroEstudio;
    private $atPaisModelo;
    private $tiempo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados    = $this->metCargarModelo('empleados','gestion');
        $this->atCapacitacion = $this->metCargarModelo('capacitacion','gestion');
        $this->atIdUsuario    = Session::metObtener('idUsuario');
        $this->atOrganismo    = $this->metCargarModelo('organismo','maestros','modCV');
        $this->atCurso        = $this->metCargarModelo('cursos','maestros');
        $this->atCentroEstudio= $this->metCargarModelo('centroEstudios','maestros');
        $this->atPaisModelo   = $this->metCargarModelo('pais',false,'APP');

       }

    #Metodo Index del controlador Empleados.
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];

        $listadoOrganismo = $this->atEmpleados->metListarOrganismos();

        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);
        $pkNumDependencia = $datosEmpleado['pk_num_dependencia'];

        // Organismo donde trabaja el empleado
        $empleado = array(
            'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
            'pk_num_dependencia' => $pkNumDependencia
        );

        $EstadoRegistro = array('PE'=>'PENDIENTE','AP'=>'APROBADO','IN' => 'INICIADO', 'TE'=>'TERMINADO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);
        $this->atVista->assign('EstadoPorDefecto','PE');

        $this->atVista->assign('listadoOrganismos', $listadoOrganismo);
        $this->atVista->assign('listadoCursos', $this->atCurso->metConsultarCursos());
        $this->atVista->assign('listadoCentrosEstudios', $this->atCentroEstudio->metConsultarCentros());
        $this->atVista->assign('miscelaneoTipoCap',$this->atEmpleados->metMostrarSelect('TIPOCURSO'));
        $this->atVista->assign('miscelaneoModalidad',$this->atEmpleados->metMostrarSelect('MODACAPAC'));
        $this->atVista->assign('tipo','TODOS');
        $this->atVista->assign('listadoCapacitacion', $this->atCapacitacion->metListarCapacitacion('TODOS'));
        $this->atVista->metRenderizar('listado');

    }


    /*PERMITE BUSCAR LAS CAPACITACIONES A TRAVES DEL FILTRO DE BUSQUEDA*/
    public function metBuscarCapacitaciones()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $curso = $_POST['curso'];
        $institucion = $_POST['institucion'];
        $tipocap = $_POST['tipo_cap'];
        $modalidad = $_POST['modalidad'];
        $estado_registro = $_POST['estado_registro'];
        $fechaInicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];


        $datosCapacitaciones = $this->atCapacitacion->metBuscarCapacitaciones($pkNumOrganismo, $curso, $institucion, $tipocap, $modalidad, $estado_registro, $fechaInicio, $fecha_fin);
        echo json_encode($datosCapacitaciones);


    }

    public function metRegistrarCapacitacion()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO

        $complementosCss = array(
            'wizard/wizardfa6c','select2/select201ef',
            'bootstrap-datepicker/datepicker'

        );
        $complementoJs = array(
            'wizard/jquery.bootstrap.wizard.min','select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker','inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask','inputmask/jquery.maskedinput-1.2.2.min'
        );
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/core/demo/DemoFormWizard','materialSiace/App','materialSiace/core/demo/DemoFormComponents');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idCapacitacion = $this->metObtenerInt('idCapacitacion');
        $valido = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum])) {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = '';

                    }
                }
            }


            if($idCapacitacion===0){


                /*organizo los arrays de datos para el registro de la capacitacion*/

                if(!isset($validacion['flag_costos'])){
                    $validacion['flag_costos']=0;
                }

                $datosCapacitacion = array(
                    $validacion['fk_a001_num_organismo'],
                    date("Y"),
                    $validacion['fk_rhb001_empleado_solicitante'],
                    $validacion['fk_rhc048_num_curso'],
                    $validacion['fk_rhc040_centro_estudio'],
                    $validacion['fk_a010_num_ciudad'],
                    $validacion['fk_a006_num_miscelnaeo_detalle_tipocap'],
                    $validacion['fk_a006_num_miscelnaeo_detalle_modalidad'],
                    $validacion['fk_a006_num_miscelnaeo_detalle_origen'],
                    $validacion['ind_periodo'],
                    $validacion['num_vacantes'],
                    $validacion['num_participantes'],
                    $validacion['fec_fecha_inicio'],
                    $validacion['fec_fecha_termino'],
                    $validacion['ind_expositor'],
                    $validacion['ind_telefonos'],
                    $validacion['flag_costos'],
                    $validacion['txt_observaciones'],
                    $validacion['num_monto_estimado'],
                    $validacion['num_monto_asumido'],
                    $validacion['num_total_dias'],
                    $validacion['num_total_horas'],
                    'PE',
                    date("Y-m-d"),
                    $this->atIdUsuario,
                    '',
                    ''
                );
                $datosFundamentos = array(
                    $validacion['txt_fundamentacion_1'],
                    $validacion['txt_fundamentacion_2'],
                    $validacion['txt_fundamentacion_3'],
                    $validacion['txt_fundamentacion_4'],
                    $validacion['txt_fundamentacion_5'],
                    $validacion['txt_fundamentacion_6']
                );
                $datosParticipantes = array(
                    $validacion['id_empleados']
                );
                $datosHorario = array(
                    $validacion['fecha'],
                    $validacion['desde'],
                    $validacion['hasta'],
                    $validacion['totalH'],
                    $validacion['diasT'],
                    $validacion['horasT']
                );

                //echo json_encode($datosHorario);

                $id = $this->atCapacitacion->metRegistrarCapacitacion($datosCapacitacion,$datosFundamentos,$datosParticipantes,$datosHorario);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['idCapacitacion'] = $id;
                    $validacion['status'] = 'registrado';
                    echo json_encode($validacion);
                    exit;
                }



            }




        }

        $periodo = date("Y-m");
        $this->atVista->assign('periodo',$periodo);
        /*listado de organismo*/
        $this->atVista->assign('listadoOrganismos',$this->atEmpleados->metListarOrganismos());
        $this->atVista->assign('listadoCursos',$this->atCurso->metConsultarCursos());
        $this->atVista->assign('listadoCentros',$this->atCentroEstudio->metConsultarCentros());

        $this->atVista->assign('listadoPais',$this->atPaisModelo->metListarPais(1));

        /*miscelaneos*/
        $miscelaneoTipoCap    = $this->atEmpleados->metMostrarSelect('TIPOCURSO');
        $miscelaneoModalidad  = $this->atEmpleados->metMostrarSelect('MODACAPAC');
        $miscelaneoOrigen     = $this->atEmpleados->metMostrarSelect('ORIGCAP');

        $this->atVista->assign('miscelaneoTipoCap',$miscelaneoTipoCap);
        $this->atVista->assign('miscelaneoModalidad',$miscelaneoModalidad);
        $this->atVista->assign('miscelaneoOrigen',$miscelaneoOrigen);

        $this->atVista->assign('idCapacitacion',$idCapacitacion);
        $this->atVista->metRenderizar('form', 'modales');
    }


    #PERMITE VER FORMULARIO CON LOS DATOS DE LA CAPACITACION PARA APROBARLA
    public function metAprobarCapacitacionForm()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO

        $complementosCss = array(
            'wizard/wizardfa6c','select2/select201ef',
            'bootstrap-datepicker/datepicker'

        );
        $complementoJs = array(
            'wizard/jquery.bootstrap.wizard.min','select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker','inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask','inputmask/jquery.maskedinput-1.2.2.min'
        );
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/core/demo/DemoFormWizard','materialSiace/App','materialSiace/core/demo/DemoFormComponents');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idCapacitacion = $this->metObtenerInt('idCapacitacion');
        $valido = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum])) {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = '';

                    }
                }
            }


            if($idCapacitacion!=0){


                /*organizo los arrays de datos para el registro de la capacitacion*/

                if(!isset($validacion['flag_costos'])){
                    $validacion['flag_costos']=0;
                }

                $datosAprobacion = array(
                    $idCapacitacion,//id de la capacitacion
                    'AP',//estado de aprobacion
                    date("Y-m-d"),//fecha actual,
                    $validacion['num_vacantes'],
                    $validacion['num_participantes']
                );


                $id = $this->atCapacitacion->metAprobarCapacitacion($datosAprobacion);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['idCapacitacion'] = $id;
                    $validacion['status'] = 'aprobado';
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idCapacitacion!=0){


            $datosPrincipales = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'PR');
            $datosFundamentos = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'FU');
            $datosEmpleados   = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'EM');
            $datosHorarios    = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'HO');
            $datosUbicacion   = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'UB');

            $this->atVista->assign('formDBpr',$datosPrincipales);
            $this->atVista->assign('formDBfu',$datosFundamentos);
            $this->atVista->assign('formDBem',$datosEmpleados);
            $this->atVista->assign('formDBho',$datosHorarios);
            $this->atVista->assign('formDBub',$datosUbicacion);


        }

        $periodo = date("Y-m");
        $this->atVista->assign('periodo',$periodo);
        /*listado de organismo*/
        $this->atVista->assign('listadoOrganismos',$this->atEmpleados->metListarOrganismos());
        $this->atVista->assign('listadoCursos',$this->atCurso->metConsultarCursos());
        $this->atVista->assign('listadoCentros',$this->atCentroEstudio->metConsultarCentros());

        $this->atVista->assign('listadoPais',$this->atPaisModelo->metListarPais(1));

        /*miscelaneos*/
        $miscelaneoTipoCap    = $this->atEmpleados->metMostrarSelect('TIPOCURSO');
        $miscelaneoModalidad  = $this->atEmpleados->metMostrarSelect('MODACAPAC');
        $miscelaneoOrigen     = $this->atEmpleados->metMostrarSelect('ORIGCAP');

        $this->atVista->assign('miscelaneoTipoCap',$miscelaneoTipoCap);
        $this->atVista->assign('miscelaneoModalidad',$miscelaneoModalidad);
        $this->atVista->assign('miscelaneoOrigen',$miscelaneoOrigen);


        $this->atVista->assign('idCapacitacion',$idCapacitacion);
        $this->atVista->metRenderizar('formAprobar', 'modales');
    }


    #PERMITE VER FORMULARIO CON LOS DATOS DE LA CAPACITACION PARA MODIFICARLA
    public function metModificarCapacitacionForm()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO

        $complementosCss = array(
            'wizard/wizardfa6c','select2/select201ef',
            'bootstrap-datepicker/datepicker'

        );
        $complementoJs = array(
            'wizard/jquery.bootstrap.wizard.min','select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker','inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask','inputmask/jquery.maskedinput-1.2.2.min'
        );
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/core/demo/DemoFormWizard','materialSiace/App','materialSiace/core/demo/DemoFormComponents');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idCapacitacion = $this->metObtenerInt('idCapacitacion');
        $valido = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum])) {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = '';

                    }
                }
            }


            if($idCapacitacion!=0){


                /*organizo los arrays de datos para el registro de la capacitacion*/

                if(!isset($validacion['flag_costos'])){
                    $validacion['flag_costos']=0;
                }

                $datosCapacitacion = array(
                    $validacion['fk_a001_num_organismo'],
                    date("Y"),
                    $validacion['fk_rhb001_empleado_solicitante'],
                    $validacion['fk_rhc048_num_curso'],
                    $validacion['fk_rhc040_centro_estudio'],
                    $validacion['fk_a010_num_ciudad'],
                    $validacion['fk_a006_num_miscelnaeo_detalle_tipocap'],
                    $validacion['fk_a006_num_miscelnaeo_detalle_modalidad'],
                    $validacion['fk_a006_num_miscelnaeo_detalle_origen'],
                    $validacion['ind_periodo'],
                    $validacion['num_vacantes'],
                    $validacion['num_participantes'],
                    $validacion['fec_fecha_inicio'],
                    $validacion['fec_fecha_termino'],
                    $validacion['ind_expositor'],
                    $validacion['ind_telefonos'],
                    $validacion['flag_costos'],
                    $validacion['txt_observaciones'],
                    $validacion['num_monto_estimado'],
                    $validacion['num_monto_asumido'],
                    $validacion['num_total_dias'],
                    $validacion['num_total_horas'],
                    'PE',
                    date("Y-m-d"),
                    $this->atIdUsuario,
                    '',
                    ''
                );
                $datosFundamentos = array(
                    $validacion['txt_fundamentacion_1'],
                    $validacion['txt_fundamentacion_2'],
                    $validacion['txt_fundamentacion_3'],
                    $validacion['txt_fundamentacion_4'],
                    $validacion['txt_fundamentacion_5'],
                    $validacion['txt_fundamentacion_6']
                );
                $datosParticipantes = array(
                    $validacion['id_empleados']
                );
                $datosHorario = array(
                    $validacion['fecha'],
                    $validacion['desde'],
                    $validacion['hasta'],
                    $validacion['totalH'],
                    $validacion['diasT'],
                    $validacion['horasT']
                );

                $id = $this->atCapacitacion->metModificarCapacitacion($idCapacitacion,$datosCapacitacion,$datosFundamentos,$datosParticipantes,$datosHorario);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['idCapacitacion'] = $id;
                    $validacion['status'] = 'modificado';
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        

        if($idCapacitacion!=0){


            $datosPrincipales = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'PR');
            $datosFundamentos = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'FU');
            $datosEmpleados   = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'EM');
            $datosHorarios    = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'HO');
            $datosUbicacion   = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'UB');

            $this->atVista->assign('formDBpr',$datosPrincipales);
            $this->atVista->assign('formDBfu',$datosFundamentos);
            $this->atVista->assign('formDBem',$datosEmpleados);
            $this->atVista->assign('formDBho',$datosHorarios);
            $this->atVista->assign('formDBub',$datosUbicacion);

            $aux = '';

            foreach ($datosEmpleados as $emp){

                $aux = $aux.$emp['fk_rhb001_num_empleado']."#";

            }


            $this->atVista->assign('auxEmp',$aux);

        }

        $periodo = date("Y-m");
        $this->atVista->assign('periodo',$periodo);
        /*listado de organismo*/
        $this->atVista->assign('listadoOrganismos',$this->atEmpleados->metListarOrganismos());
        $this->atVista->assign('listadoCursos',$this->atCurso->metConsultarCursos());
        $this->atVista->assign('listadoCentros',$this->atCentroEstudio->metConsultarCentros());

        $this->atVista->assign('listadoPais',$this->atPaisModelo->metListarPais(1));

        /*miscelaneos*/
        $miscelaneoTipoCap    = $this->atEmpleados->metMostrarSelect('TIPOCURSO');
        $miscelaneoModalidad  = $this->atEmpleados->metMostrarSelect('MODACAPAC');
        $miscelaneoOrigen     = $this->atEmpleados->metMostrarSelect('ORIGCAP');

        $this->atVista->assign('miscelaneoTipoCap',$miscelaneoTipoCap);
        $this->atVista->assign('miscelaneoModalidad',$miscelaneoModalidad);
        $this->atVista->assign('miscelaneoOrigen',$miscelaneoOrigen);

        $this->atVista->assign('n',0);
        $this->atVista->assign('idCapacitacion',$idCapacitacion);
        $this->atVista->metRenderizar('formModificar', 'modales');
    }    
    

    public function metIniciarCapacitacionForm()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO

        $complementosCss = array(
            'wizard/wizardfa6c','select2/select201ef',
            'bootstrap-datepicker/datepicker'

        );
        $complementoJs = array(
            'wizard/jquery.bootstrap.wizard.min','select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker','inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask','inputmask/jquery.maskedinput-1.2.2.min'
        );
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/core/demo/DemoFormWizard','materialSiace/App','materialSiace/core/demo/DemoFormComponents');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idCapacitacion = $this->metObtenerInt('idCapacitacion');
        $tipo = $this->metObtenerTexto('tipo');
        $valido = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum])) {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = '';

                    }
                }
            }


            if($idCapacitacion!=0){


                /*organizo los arrays de datos para el registro*/


                $datosIniciar = array(
                    $idCapacitacion,//id de la capacitacion
                    'IN',//estado de aprobacion
                );


                $id = $this->atCapacitacion->metIniciarCapacitacion($datosIniciar);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['idCapacitacion'] = $id;
                    $validacion['status'] = 'iniciado';
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idCapacitacion!=0){


            $datosPrincipales = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'PR');
            $datosFundamentos = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'FU');
            $datosEmpleados   = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'EM');
            $datosHorarios    = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'HO');
            $datosUbicacion   = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'UB');

            $this->atVista->assign('formDBpr',$datosPrincipales);
            $this->atVista->assign('formDBfu',$datosFundamentos);
            $this->atVista->assign('formDBem',$datosEmpleados);
            $this->atVista->assign('formDBho',$datosHorarios);
            $this->atVista->assign('formDBub',$datosUbicacion);


        }

        $periodo = date("Y-m");
        $this->atVista->assign('periodo',$periodo);
        /*listado de organismo*/
        $this->atVista->assign('listadoOrganismos',$this->atEmpleados->metListarOrganismos());
        $this->atVista->assign('listadoCursos',$this->atCurso->metConsultarCursos());
        $this->atVista->assign('listadoCentros',$this->atCentroEstudio->metConsultarCentros());

        $this->atVista->assign('listadoPais',$this->atPaisModelo->metListarPais(1));

        /*miscelaneos*/
        $miscelaneoTipoCap    = $this->atEmpleados->metMostrarSelect('TIPOCURSO');
        $miscelaneoModalidad  = $this->atEmpleados->metMostrarSelect('MODACAPAC');
        $miscelaneoOrigen     = $this->atEmpleados->metMostrarSelect('ORIGCAP');

        $this->atVista->assign('miscelaneoTipoCap',$miscelaneoTipoCap);
        $this->atVista->assign('miscelaneoModalidad',$miscelaneoModalidad);
        $this->atVista->assign('miscelaneoOrigen',$miscelaneoOrigen);

        $this->atVista->assign('proceso','iniciar');
        $this->atVista->assign('idCapacitacion',$idCapacitacion);
        $this->atVista->assign('tipo',$tipo);
        $this->atVista->metRenderizar('formIniciarTerminar', 'modales');

    }


    public function metTerminarCapacitacionForm()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO

        $complementosCss = array(
            'wizard/wizardfa6c','select2/select201ef',
            'bootstrap-datepicker/datepicker'

        );
        $complementoJs = array(
            'wizard/jquery.bootstrap.wizard.min','select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker','inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask','inputmask/jquery.maskedinput-1.2.2.min'
        );
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/core/demo/DemoFormWizard','materialSiace/App','materialSiace/core/demo/DemoFormComponents');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idCapacitacion = $this->metObtenerInt('idCapacitacion');
        $tipo = $this->metObtenerTexto('tipo');
        $valido = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum])) {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = '';

                    }
                }
            }


            if($idCapacitacion!=0){


                /*organizo los arrays de datos para el registro de la capacitacion*/

                /*organizo los arrays de datos para el registro*/
                if(!isset($validacion['flag_aprobado'])){
                    $validacion['flag_aprobado']=0;
                }

                $datosTerminar = array(
                    $idCapacitacion,//id de la capacitacion
                    'TE',//estado de aprobacion
                    $validacion['empleado_cap'],
                    $validacion['num_dias_asistidos'],
                    $validacion['flag_aprobado'],
                    $validacion['ind_calificacion']
                );


                $id = $this->atCapacitacion->metTerminarCapacitacion($datosTerminar);

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['idCapacitacion'] = $id;
                    $validacion['status'] = 'terminado';
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        if($idCapacitacion!=0){


            $datosPrincipales = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'PR');
            $datosFundamentos = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'FU');
            $datosEmpleados   = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'EM');
            $datosHorarios    = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'HO');
            $datosUbicacion   = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'UB');

            $gastos = $this->atCapacitacion->metMostrarGastos($idCapacitacion);
            $listadoGastos = $this->atCapacitacion->metListadoGastos($idCapacitacion);

            $this->atVista->assign('formDBpr',$datosPrincipales);
            $this->atVista->assign('formDBfu',$datosFundamentos);
            $this->atVista->assign('formDBem',$datosEmpleados);
            $this->atVista->assign('formDBho',$datosHorarios);
            $this->atVista->assign('formDBub',$datosUbicacion);
            $this->atVista->assign('formDBGastos',$gastos);
            $this->atVista->assign('listadoGastos',$listadoGastos);


        }

        $periodo = date("Y-m");
        $this->atVista->assign('periodo',$periodo);
        /*listado de organismo*/
        $this->atVista->assign('listadoOrganismos',$this->atEmpleados->metListarOrganismos());
        $this->atVista->assign('listadoCursos',$this->atCurso->metConsultarCursos());
        $this->atVista->assign('listadoCentros',$this->atCentroEstudio->metConsultarCentros());

        $this->atVista->assign('listadoPais',$this->atPaisModelo->metListarPais(1));

        /*miscelaneos*/
        $miscelaneoTipoCap    = $this->atEmpleados->metMostrarSelect('TIPOCURSO');
        $miscelaneoModalidad  = $this->atEmpleados->metMostrarSelect('MODACAPAC');
        $miscelaneoOrigen     = $this->atEmpleados->metMostrarSelect('ORIGCAP');

        $this->atVista->assign('miscelaneoTipoCap',$miscelaneoTipoCap);
        $this->atVista->assign('miscelaneoModalidad',$miscelaneoModalidad);
        $this->atVista->assign('miscelaneoOrigen',$miscelaneoOrigen);

        $this->atVista->assign('proceso','terminar');
        $this->atVista->assign('tipo','TE');
        $this->atVista->assign('n',1);
        $this->atVista->assign('idCapacitacion',$idCapacitacion);
        $this->atVista->metRenderizar('formIniciarTerminar', 'modales');
    }



    public function metVerCapacitacionForm()
    {
        #ARCHIVO JAVASCRIPT CON FUNCIONES COMPLEMENTARIAS DEL MODULO

        $complementosCss = array(
            'wizard/wizardfa6c','select2/select201ef',
            'bootstrap-datepicker/datepicker'

        );
        $complementoJs = array(
            'wizard/jquery.bootstrap.wizard.min','select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker','inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask','inputmask/jquery.maskedinput-1.2.2.min'
        );
        $js = array('Aplicacion/appFunciones','modRH/modRHFunciones','materialSiace/core/demo/DemoFormWizard','materialSiace/App','materialSiace/core/demo/DemoFormComponents');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idCapacitacion = $this->metObtenerInt('idCapacitacion');
        $tipo = $this->metObtenerTexto('tipo');
        $valido = $this->metObtenerInt('valido');

        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum])) {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = '';

                    }
                }
            }


        }

        if($idCapacitacion!=0){


            $datosPrincipales = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'PR');
            $datosFundamentos = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'FU');
            $datosEmpleados   = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'EM');
            $datosHorarios    = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'HO');
            $datosUbicacion   = $this->atCapacitacion->metMostrarCapacitaciones($idCapacitacion,'UB');

            $gastos = $this->atCapacitacion->metMostrarGastos($idCapacitacion);
            $listadoGastos = $this->atCapacitacion->metListadoGastos($idCapacitacion);

            $this->atVista->assign('formDBpr',$datosPrincipales);
            $this->atVista->assign('formDBfu',$datosFundamentos);
            $this->atVista->assign('formDBem',$datosEmpleados);
            $this->atVista->assign('formDBho',$datosHorarios);
            $this->atVista->assign('formDBub',$datosUbicacion);
            $this->atVista->assign('formDBGastos',$gastos);
            $this->atVista->assign('listadoGastos',$listadoGastos);


        }

        $periodo = date("Y-m");
        $this->atVista->assign('periodo',$periodo);
        /*listado de organismo*/
        $this->atVista->assign('listadoOrganismos',$this->atEmpleados->metListarOrganismos());
        $this->atVista->assign('listadoCursos',$this->atCurso->metConsultarCursos());
        $this->atVista->assign('listadoCentros',$this->atCentroEstudio->metConsultarCentros());

        $this->atVista->assign('listadoPais',$this->atPaisModelo->metListarPais(1));

        /*miscelaneos*/
        $miscelaneoTipoCap    = $this->atEmpleados->metMostrarSelect('TIPOCURSO');
        $miscelaneoModalidad  = $this->atEmpleados->metMostrarSelect('MODACAPAC');
        $miscelaneoOrigen     = $this->atEmpleados->metMostrarSelect('ORIGCAP');

        $this->atVista->assign('miscelaneoTipoCap',$miscelaneoTipoCap);
        $this->atVista->assign('miscelaneoModalidad',$miscelaneoModalidad);
        $this->atVista->assign('miscelaneoOrigen',$miscelaneoOrigen);

        $this->atVista->assign('proceso','ver');
        $this->atVista->assign('tipo',$tipo);
        $this->atVista->assign('n',1);
        $this->atVista->assign('idCapacitacion',$idCapacitacion);
        $this->atVista->metRenderizar('formVer', 'modales');
    }



    #permite guardar los gastos generados en la capacitacion
    public function metGuardarGastos()
    {
        $capacitacion = $_POST['capacitacion'];
        $detalle = $_POST['detalle'];
        $fecha = $_POST['fecha'];
        $num_sub_total = $_POST['num_sub_total'];
        $num_impuestos = $_POST['num_impuestos'];
        $num_total = $_POST['num_total'];

        $op = $this->atCapacitacion->metGuardarGastos($capacitacion,$detalle,$fecha,$num_sub_total,$num_impuestos,$num_total);

        if(is_array($op)){
            $validacion['status'] = 'error';
            $validacion['detalleERROR'] = $op;
            echo json_encode($validacion);
            exit;
        }else{
            $validacion['idGasto'] = $op;
            $validacion['status'] = 'gasto_registrado';
            echo json_encode($validacion);
            exit;
        }


    }

    #permite eliminar los gastos generados en la capacitacion
    public function metEliminarGastos()
    {
        $capacitacion = $_POST['capacitacion'];
        $gasto  = $_POST['gasto'];

        if($capacitacion!=0){

            $id=$this->atCapacitacion->metEliminarGastos($capacitacion,$gasto);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'idGasto'=>$id
                );
            }

        }

        echo json_encode($valido);
        exit;

    }
    
    #permite obterner la sumatoria totao de los gastos de la capacitacion
    public function metMostrarGastos()
    {
        $capacitacion = $_POST['capacitacion'];

        if($capacitacion!=0){
            
            $gastos = $this->atCapacitacion->metMostrarGastos($capacitacion);
            
            
            $sub_total = $gastos['num_sub_total'];
            $impuestos = $gastos['num_impuestos'];
            $total = $gastos['num_total'];

            $data = array(
              'sub_total' => number_format($sub_total,2,",","."),
              'impuestos' => number_format($impuestos,2,",","."),
              'total' => number_format($total,2,",",".")

            );
            
            echo json_encode($data);


        }
        
        
    }



    #Metodo que permite obtener los datos del empleado
    public function metObtenerDatosEmpleado()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');

        $resultado = $this->atCapacitacion->metObtenerDatosEmpleado($idEmpleado);

        //datos mpara mostrar en el formulario
        $datos = array(
            'id_empleado' => $idEmpleado,
            'nombreTrabajador' => $resultado['nombre_completo'],
            'id_dependencia' => $resultado['fk_a004_num_dependencia']
        );

        echo json_encode($datos);

    }


    #permite graficar dadas las fecha inicio y termino los horarios
    public function metGraficarHorario()
    {
        $fecha_ini = $_POST['fecha_ini'];
        $fecha_fin = $_POST['fecha_fin'];

        $aux1 = explode("-",$fecha_ini);
        $aux2 = explode("-",$fecha_fin);

        $fecha1 = $aux1[2]."-".$aux1[1]."-".$aux1[0];
        $fecha2 = $aux2[2]."-".$aux2[1]."-".$aux2[0];

        $arreglo = array();

        for($i=$fecha1;$i<=$fecha2;$i = date("Y-m-d", strtotime($i ."+ 1 days"))){

            $w = explode("-",$i);

            $arreglo[] = $w[2]."-".$w[1]."-".$w[0];

        }

        echo json_encode($arreglo);


    }

    #hora en formato 12 horas
    public function metFormato12Horas($hora)
    {
        $h = date("g:i:s a",strtotime($hora));

        return $h;
    }

    #hora en formato 24 horas
    public function metFormato24Horas($hora)
    {
        $h = date("H:i:s",strtotime($hora));

        return $h;
    }

    #permite restar los horas para saber su diferencia
    public function metRestarHoras()
    {

        /*if(isset($hora_ini) && empty($hora_ini)){
            $horaini = $this->metFormato24Horas($_POST['hora_ini']);
        }else{
            $horaini= $this->metFormato24Horas($hora_ini);
        }
        if(isset($hora_fin) && empty($hora_fin)){
            $horafin = $this->metFormato24Horas($_POST['hora_fin']);
        }else{
            $horafin= $this->metFormato24Horas($hora_fin);
        }
        if(isset($operacion) && empty($operacion)){
            $operacion = $_POST['operacion'];
        }else{
            $operacion=$operacion;
        }*/
        $horaini = $this->metFormato24Horas($_POST['hora_ini']);
        $horafin = $this->metFormato24Horas($_POST['hora_fin']);
        $operacion = $_POST['operacion'];



        $horai=substr($horaini,0,2);

        $mini=substr($horaini,3,2);

        $segi=substr($horaini,6,2);



        $horaf=substr($horafin,0,2);

        $minf=substr($horafin,3,2);

        $segf=substr($horafin,6,2);



        $ini=((($horai*60)*60)+($mini*60)+$segi);

        $fin=((($horaf*60)*60)+($minf*60)+$segf);



        $dif=$fin-$ini;



        $difh=floor($dif/3600);

        $difm=floor(($dif-($difh*3600))/60);

        $difs=$dif-($difm*60)-($difh*3600);


        if(strcmp($operacion,'return')==0)
        {
            return date("H:i:s",mktime($difh,$difm,$difs));
        }

        if(strcmp($operacion,'json')==0)
        {
            echo json_encode(date("H:i:s",mktime($difh,$difm,$difs)));
        }


    }

    #permite sumar horas dado un arry de horas
    function metSumarHoras(){

        $totalH  = $_POST['totalH'];
        $horasT  = $_POST['horasT'];

        if(isset($horasT) || empty($horasT)){
            $horasT='00:00:00';
        }

        $horas = array($horasT,$totalH);

        $cant = count($horas);

        $tH='';
        $tM='';

        for($i=0 ;  $i < $cant ; $i++)
        {
            $w = explode(":",$horas[$i]);
            $auxH = $w[0];
            $auxM = $w[1];
            $tH = $tH + intval($auxH);
            $tM = $tM + intval($auxM);


            while($tM >= 60){

                if($tM >= 60){
                    $tH = $tH+1;
                    $tM = $tM-60;
                }

            }

            $t = date("H:i:s",mktime($tH,$tM,'00'));


        }

        echo json_encode($t);

    }

    #permite listar las capacitaciones pendiente por aprobar
    public function metAprobarCapacitacion()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $js= array('materialSiace/offcanvas','materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('tipo','PE');
        $this->atVista->assign('listadoCapacitacion', $this->atCapacitacion->metListarCapacitacion('PE'));
        $this->atVista->metRenderizar('listado');

    }

    #permite listar todas la capacitaciones que estan aprobadas
    public function metIniciarTerminarCapacitacion()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $js= array('materialSiace/offcanvas','materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('tipo','AP');
        $this->atVista->assign('listadoCapacitacion', $this->atCapacitacion->metListarCapacitacion('AP'));
        $this->atVista->metRenderizar('listado');
    }


    #permite eliminar en empleado de la capacitacion
    public function metEliminarEmpleadoCapacitacion()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $idCapacitacion = $this->metObtenerInt('idCapacitacion');

        $eliminar = $this->atCapacitacion->metEliminarEmpleadoCapacitacion($idCapacitacion,$idEmpleado);
        if(is_array($eliminar)){
            $valido=array(
                'status'=>'error',
                'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
            );
        }else{

            /*actualizo participantes*/
            $actualizar = $this->atCapacitacion->metActualizarParticipantesCapacitacion($idCapacitacion);

            $valido=array(
                'status'=>'OK',
                'idCapacitacion'=>$eliminar,
                'participantes'=>$actualizar
            );


        }
        echo json_encode($valido);
        exit;

    }


    #permite listar los empleados para su revision y aprobacion
    public function metListarEmpleado($estatus)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Empleados', $this->atEmpleados->metListarEmpleado($estatus,'operaciones'));
        $this->atVista->assign('estatus',$estatus);
        $this->atVista->metRenderizar('listado');
    }

    #para armar json centro de costos
    public function metJsonCentroCosto()
    {

        $idDependencia = $this->metObtenerInt('idDependencia');
        $centro_costo  = $this->atEmpleados->metJsonCentroCosto($idDependencia);
        echo json_encode($centro_costo);
        exit;

    }

    #para armar json parroquia
    public function metJsonParroquia()
    {

        $idMunicipio = $this->metObtenerInt('idMunicipio');
        $parroquia  = $this->atEmpleados->metJsonParroquia($idMunicipio);
        echo json_encode($parroquia);
        exit;

    }

    #para armar json sector
    public function metJsonSector()
    {

        $idParroquia = $this->metObtenerInt('idParroquia');
        $sector  = $this->atEmpleados->metJsonSector($idParroquia);
        echo json_encode($sector);
        exit;

    }

    #permite obtener la categoria del cargo y el sueldo del cargo
    public function metObtenerSueldoBasico()
    {
        #obtengo el valor pasado del formulario
        $Cargo = $this->metObtenerInt('idCargo');
        #conecto con el modelo y consulto los datos
        $datos = $this->atEmpleados->metObtenerSueldoBasico($Cargo);
        #los codifico con json para mostrarlos en la vista

        $sueldo = number_format($datos['num_sueldo_basico'],2,',','.');

        $categoria = $datos['ind_nombre_detalle'];

        $data = array(
            'ind_nombre_detalle' => $categoria,
            'num_sueldo_basico'  => $sueldo
        );

        echo json_encode($data);
    }

    #permite buscar una persona por cedula o cod persona
    public function metBuscarPersona()
    {
        #obtengo los valores pasado del formulario
        $parametro = $this->metObtenerTexto('parametro');
        $busqueda  = $this->metObtenerTexto('busqueda');

        $datos = $this->atEmpleados->metBuscarPersona($parametro,$busqueda);

        echo json_encode($datos);

    }


    #permite copiar subir una imagen del empleado temporalmente
    public function metCopiarImagenTmp()
    {
        #carpeta temporal donde se va a copiar la imagen
        $destino =  "publico/imagenes/modRH/tmp/";
        $tipo    = $this->metObtenerTexto('tipo');

        if(strcmp($tipo,'FOTO')==0){/*fotos del empleado*/

            if (isset($_FILES["Foto"]))
            {

                $partes  = explode("." ,$_FILES["Foto"]["name"]);
                $img_f   = $this->atIdUsuario."_tmp_".$partes[0].".".$partes[1];
                $ruta    = $destino.$img_f;

                if (!file_exists($ruta)){

                    $resultado = move_uploaded_file($_FILES["Foto"]["tmp_name"], $ruta);
                    chmod($destino.$img_f, 0777);
                    if ($resultado){
                        echo $img_f;
                    } else {
                        echo "ocurrio un error al mover el archivo.";
                    }

                }else{

                    echo $img_f;
                }

            }

        }

        if(strcmp($tipo,'DOCUMENTOS')==0){/*documentos del empleado*/

            $empleado = $this->metObtenerInt('idEmpleado');

            if (isset($_FILES["Foto"]))
            {

                $partes  = explode("." ,$_FILES["Foto"]["name"]);
                $img_f   = $this->atIdUsuario."_".$empleado."_tmp_doc_".$partes[0].".".$partes[1];
                $ruta    = $destino.$img_f;

                if (!file_exists($ruta)){

                    $resultado = move_uploaded_file($_FILES["Foto"]["tmp_name"], $ruta);
                    chmod($destino.$img_f, 0777);
                    if ($resultado){
                        echo $img_f;
                    } else {
                        echo "ocurrio un error al mover el archivo.";
                    }

                }else{

                    echo $img_f;
                }

            }

        }


    }




    /*
     * PERMITE MOSTRAR LA VENTANA CON EL LISTADO DE EMPLEADOS
     * PARA SU SELECCION
     */
    public function metConsultarEmpleado()
    {

        #identifica la accion que se va a realizar
        $proceso = $this->metObtenerTexto('proceso');
        $accion  = $this->metObtenerTexto('accion');

        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];

        $listadoOrganismo = $this->atOrganismo->metListarOrganismo();

        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);
        $pkNumDependencia = $datosEmpleado['pk_num_dependencia'];

        $listadoEmpleados = $this->atDependencias->metDependenciaEmpleado($pkNumDependencia);
        $this->atVista->assign('empleado', $listadoEmpleados);

        $listadoDependencia = $this->atDependencias->metListarDependenciaOrganismo($datosEmpleado['pk_num_organismo'], 1, 0);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        // Organismo donde trabaja el empleado
        $empleado = array(
            'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
            'pk_num_dependencia' => $pkNumDependencia
        );
        $datosTrabajador = $this->atEmpleados->metBuscarTrabajador($datosEmpleado['pk_num_organismo'], $pkNumDependencia, '', '', '', '1', '1', '', '');

        $centroCosto = $this->atEmpleados->metListarCentroCosto();
        $tipoNomina  = $this->atTipoNomina->metListarTipoNomina();
        $tipoTrabajador = $this->atEmpleados->metMostrarSelect('TIPOTRAB');
        // Envío a la vista
        $this->atVista->assign('datosTrabajador', $datosTrabajador);
        $this->atVista->assign('centroCosto', $centroCosto);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('nomina', $tipoNomina);
        $this->atVista->assign('tipoTrabajador', $tipoTrabajador);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('proceso',$proceso);
        $this->atVista->assign('accion',$accion);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));
        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->metRenderizar('buscarEmpleado', 'modales');

    }

    // Método que permite listar empleados de acuerdo a la busqueda
    public function metBuscarTrabajador()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $pkNumDependencia = $_POST['pk_num_dependencia'];
        $centroCosto = $_POST['centro_costo'];
        $tipoNomina = $_POST['tipo_nomina'];
        $tipoTrabajador = $_POST['tipo_trabajador'];
        $estado_registro = $_POST['estado_registro'];
        $situacionTrabajo = $_POST['situacion_trabajo'];
        $fechaInicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];

        $datosTrabajador = $this->atEmpleados->metBuscarTrabajador($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $tipoTrabajador, $estado_registro, $situacionTrabajo, $fechaInicio, $fecha_fin);
        echo json_encode($datosTrabajador);
    }

    //apliar foto del empleado
    public function metAmpliarFoto()
    {
        $foto = $this->metObtenerTexto('foto');

        $this->atVista->assign('foto',$foto);
        $this->atVista->metRenderizar('foto_empleado', 'modales');
    }

}
