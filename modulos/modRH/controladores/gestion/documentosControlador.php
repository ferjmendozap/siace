<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class documentosControlador extends Controlador
{
    private $atEmpleados;
    private $atDocumentos;
    private $atCargaFamiliar;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados   = $this->metCargarModelo('empleados','gestion');
        $this->atDocumentos  = $this->metCargarModelo('documentos','gestion');
        $this->atCargaFamiliar = $this->metCargarModelo('cargaFamiliar','gestion');
     }

    #Metodo Index del controlador
    public function metIndex()
    {

    }

    public function metDocumentos()
    {
        $js = array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $complementosCss = array(
            'select2/select201ef','bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'select2/select2.min','bootstrap-datepicker/bootstrap-datepicker'
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idDocumento = $this->metObtenerInt('idDocumento');
        $idEmpleado  = $this->metObtenerInt('idEmpleado');
        $valido  = $this->metObtenerInt('valido');


        if($valido==1) {

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt]=$valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idDocumento===0){//PARA INSERTAR REGISTRO

                $validacion['status']='creacion';

                if(!isset($validacion['num_flag_entregado'])){
                    $validacion['num_flag_entregado']=0;
                }

                if(!isset($validacion['num_flag_carga'])){
                    $validacion['num_flag_carga']=0;
                }

                if(!isset($validacion['fk_c015_num_carga_familiar'])){
                    $validacion['fk_c015_num_carga_familiar']=false;
                }
                #INSTANCIO EL METODO PARA REGISTRAR LOS DOCUMENTOS
                $id = $this->atDocumentos->metRegistrarDocumentos($idEmpleado,
                    $validacion['fk_a006_num_miscelaneo_detalle_documento'],
                    $validacion['num_flag_entregado'],
                    $validacion['num_flag_carga'],
                    $validacion['fec_entrega'],
                    $validacion['fec_vencimiento'],
                    $validacion['fk_c015_num_carga_familiar'],
                    $validacion['txt_observaciones'],
                    $validacion['aleatorio']/*FOTO DEL DOCUMENTO*/
                );
                $validacion['idDocumento'] = $id;

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $datos = $this->atDocumentos->metMostrarDocumentos($id);
                    $validacion['documento'] = $datos['documento'];
                    $validacion['ind_nombre1'] = $datos['ind_nombre1'];
                    $validacion['ind_nombre2'] = $datos['ind_nombre2'];
                    $validacion['ind_apellido1'] = $datos['ind_apellido1'];
                    $validacion['ind_apellido2'] = $datos['ind_apellido2'];
                    echo json_encode($validacion);
                    exit;
                }



            }else{//PARA MODIFICAR REGISTRO

                $validacion['status']='modificar';

                if(!isset($validacion['num_flag_entregado'])){
                    $validacion['num_flag_entregado']=0;
                }
                if(!isset($validacion['num_flag_carga'])){
                    $validacion['num_flag_carga']=0;
                }
                if(!isset($validacion['fk_c015_num_carga_familiar'])){
                    $validacion['fk_c015_num_carga_familiar']=false;
                }

                #INSTANCIO EL METODO PARA MODIFICAR LAS REFERENCIAS
                $id = $this->atDocumentos->metModificarDocumentos($idDocumento,$idEmpleado,
                    $validacion['fk_a006_num_miscelaneo_detalle_documento'],
                    $validacion['num_flag_entregado'],
                    $validacion['num_flag_carga'],
                    $validacion['fec_entrega'],
                    $validacion['fec_vencimiento'],
                    $validacion['fk_c015_num_carga_familiar'],
                    $validacion['txt_observaciones'],
                    $validacion['aleatorio']/*FOTO DEL DOCUMENTO*/
                );

                $validacion['idDocumento'] = $idDocumento;

                if(is_array($id)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $datos = $this->atDocumentos->metMostrarDocumentos($idDocumento);
                    $validacion['documento'] = $datos['documento'];
                    $validacion['ind_nombre1'] = $datos['ind_nombre1'];
                    $validacion['ind_nombre2'] = $datos['ind_nombre2'];
                    $validacion['ind_apellido1'] = $datos['ind_apellido1'];
                    $validacion['ind_apellido2'] = $datos['ind_apellido2'];
                    echo json_encode($validacion);
                    exit;
                }

            }
        }

        if($idDocumento!=0){
            $db=$this->atDocumentos->metMostrarDocumentos($idDocumento);
            $this->atVista->assign('formDB',$db);
        }

        $miscelaneoDocumentos = $this->atEmpleados->metMostrarSelect('DOCUMENTOS');
        $this->atVista->assign('Documentos',$miscelaneoDocumentos);

        $CargaFamiliar = $this->atCargaFamiliar->metListarCargaFamiliar($idEmpleado);
        $this->atVista->assign('CargaFamiliar',$CargaFamiliar);

        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->assign('idDocumento',$idDocumento);
        $this->atVista->metRenderizar('form', 'modales');
    }

    #permite eliminar los documentos del empleado
    public function metEliminarDocumentos()
    {
        $idDocumento= $this->metObtenerInt('idDocumento');
        if($idDocumento!=0){
            $id=$this->atDocumentos->metEliminarDocumentos($idDocumento);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'idDocumento'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


    public function metParentescoCargaFamiliar()
    {
        $idCargaFamiliar = $this->metObtenerInt('idCargaFamiliar');

        $datos = $this->atCargaFamiliar->metMostrarParentesco($idCargaFamiliar);

        echo json_encode($datos);
        exit;

    }



}
