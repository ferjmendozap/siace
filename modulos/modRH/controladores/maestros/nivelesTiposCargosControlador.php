<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class nivelesTiposCargosControlador extends Controlador
{
    private $atNivelesTiposCargosModelo;
    private $atTiposCargosModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atNivelesTiposCargosModelo = $this->metCargarModelo('nivelesTiposCargos','maestros');
        $this->atTiposCargosModelo        = $this->metCargarModelo('tiposCargos','maestros');
    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('listado');
    }


    public function metNuevoNivelesTiposCargos()
    {
        $idNivelesTiposCargos = $this->metObtenerInt('idNivelesTiposCargos');
        $valido               = $this->metObtenerInt('valido');

        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        if($valido==1){

            $this->metValidarToken();

            $formInt   = $this->metObtenerInt('form','int');
            $formTxt   = $this->metObtenerTexto('form','txt');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }



            if($idNivelesTiposCargos===0){


                $validacion['status']='creacion';
                $id = $this->atNivelesTiposCargosModelo->metRegistrarNivelesTipoCargo(
                    $validacion['fk_rhc006_num_cargo'], $validacion['num_nivel'], $validacion['ind_nombre_nivel']
                );
                $validacion['idNivelesTiposCargos']=$id;
                echo json_encode($validacion);
                exit;


            }else{


                $validacion['status']='modificacion';

                $this->atNivelesTiposCargosModelo->metModificarNivelesTipoCargo($idNivelesTiposCargos,
                    $validacion['fk_rhc006_num_cargo'], $validacion['num_nivel'], $validacion['ind_nombre_nivel']
                );
                $validacion['idNivelesTiposCargos']=$idNivelesTiposCargos;
                echo json_encode($validacion);
                exit;


            }

        }

        if($idNivelesTiposCargos!=0){
            $db=$this->atNivelesTiposCargosModelo->metMostrarNivelesTiposCargos($idNivelesTiposCargos);
            $this->atVista->assign('formDB',$db);
        }


        $tiposCargos = $this->atTiposCargosModelo->metConsultarTiposCargos();
        $this->atVista->assign('tiposCargos',$tiposCargos);
        $this->atVista->assign('idNivelesTiposCargos',$idNivelesTiposCargos);
        $this->atVista->metRenderizar('form','modales');
    }



    public function metVerificarNivel(){

        $cargo = $this->metObtenerInt('cargo');

        $nivel = $this->atNivelesTiposCargosModelo->metUltimoNivelCargo($cargo);

        echo json_encode($nivel);

    }

    #PERMITE ELIMINAR NIVELES DE TIPOS DE CARGO
    public function metEliminarNivelTipoCargo()
    {
        $idNivelesTiposCargos = $this->metObtenerInt('idNivelesTiposCargos');

        $this->atNivelesTiposCargosModelo->metEliminarNivelesTipoCargo($idNivelesTiposCargos);
        $array = array(
            'status' => 'OK',
            'idNivelesTiposCargos' => $idNivelesTiposCargos
        );

        echo json_encode($array);
    }

    #PERMITE LISTAR LOS NIVELES TIPOS DE CARGOS A TRAVES DEL DATATABLES (JSON)
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
             SELECT
                rh_c009_nivel.pk_num_nivel,
                rh_c009_nivel.fk_rhc006_num_cargo,
                rh_c009_nivel.num_nivel,
                rh_c009_nivel.ind_nombre_nivel,
                rh_c006_tipo_cargo.pk_num_cargo,
                rh_c006_tipo_cargo.ind_nombre_cargo
             FROM
                rh_c009_nivel
             INNER JOIN rh_c006_tipo_cargo ON rh_c006_tipo_cargo.pk_num_cargo = rh_c009_nivel.fk_rhc006_num_cargo
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    (
                      ind_nombre_cargo LIKE '%$busqueda[value]%' OR
                      ind_nombre_nivel LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('num_nivel','ind_nombre_nivel','ind_nombre_cargo');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_nivel';
        #construyo el listado de botones
        if (in_array('RH-01-04-01-07-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" title="Editar" idNivelesTiposCargos="'.$clavePrimaria.'" descipcion="El Usuario a Modificado un Nivel de cargo" titulo="Modificar Nivel de Cargo">
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('RH-01-04-01-08-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idNivelesTiposCargos="'.$clavePrimaria.'"  boton="si, Eliminar"
                            descipcion="El usuario a eliminado un Nivel del cargo" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Nivel de Cargo!!">
                            <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }


}
