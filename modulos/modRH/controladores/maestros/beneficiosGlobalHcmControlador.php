<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class beneficiosGlobalHcmControlador extends Controlador
{
    private $atBeneficiosGlobalHcm;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atBeneficiosGlobalHcm  = $this->metCargarModelo('beneficiosGlobalHcm','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        //$this->atVista->assign('BeneficioGlobal', $this->atBeneficiosGlobalHcm->metConsultarBeneficioGlobal());
        $this->atVista->metRenderizar('listado');


    }

    public function metBeneficioGlobalHcm()
    {
        $idBeneficioGlobal = $this->metObtenerInt('idBeneficioGlobal');
        $valido            = $this->metObtenerInt('valido');

        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min'
        );

        $js = array ('materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents','Aplicacion/appFunciones'
        );

        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    if((strcmp($tituloInt,'num_limite')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }




            if($idBeneficioGlobal===0){


                $id = $this->atBeneficiosGlobalHcm->metRegistrarBeneficioGlobal(
                    $validacion['ind_descripcion'],
                    $validacion['num_limite'],
                    $validacion['fec_anio']
                );

                //print_r($id);
                if(is_array($id)){
                    if (in_array("Duplicate entry '".$validacion['fec_anio']."' for key 'fec_anio'", $id)) {
                        $validacion['status'] = 'duplicado';
                    }else{
                        $validacion['status'] = 'error';
                    }
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='creacion';
                    $validacion['num_limite'] = number_format($validacion['num_limite'],2,',','.');
                    $validacion['idBeneficioGlobal'] = $id;
                    echo json_encode($validacion);
                    exit;
                }

            }else{


                $id = $this->atBeneficiosGlobalHcm->metModificarBeneficioGlobal($idBeneficioGlobal,
                    $validacion['ind_descripcion'],
                    $validacion['num_limite']
                );

                if(is_array($id)){
                    if (in_array('Duplicate entry', $id)) {
                        $validacion['status'] = 'duplicado';
                    }else{
                        $validacion['status'] = 'error';
                    }
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='modificacion';
                    $validacion['num_limite'] = number_format($validacion['num_limite'],2,',','.');
                    $validacion['idBeneficioGlobal']=$idBeneficioGlobal;
                    echo json_encode($validacion);
                    exit;
                }



            }

        }

        if($idBeneficioGlobal!=0){
            $db=$this->atBeneficiosGlobalHcm->metMostrarBeneficioGlobal($idBeneficioGlobal);
            $this->atVista->assign('formDB',$db);
        }else{
            $this->atVista->assign('anio',date("Y"));
        }

        $this->atVista->assign('idBeneficioGlobal',$idBeneficioGlobal);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar beneficio global
    public function metEliminarBeneficioGlobal()
    {
        $idBeneficioGlobal = $this->metObtenerInt('idBeneficioGlobal');

        $this->atBeneficiosGlobalHcm->metEliminarBeneficioGlobal($idBeneficioGlobal);

        $array = array(
            'status'  => 'OK',
            'idBeneficioGlobal' => $idBeneficioGlobal
        );

        echo json_encode($array);
    }


    #PERMITE LISTAR LOS BENFICIOS A TRAVES DEL DATATABLES (JSON)
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
            SELECT
                 *
                FROM
                rh_b008_ayuda_global
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    (
                      ind_descripcion LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_ayuda_global','fec_anio','ind_descripcion','num_limite');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_ayuda_global';
        #construyo el listado de botones
        if (in_array('RH-01-04-07-03-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idBeneficioGlobal="'.$clavePrimaria.'"
                                                        descipcion="El Usuario a Modificado un Beneficio Global" titulo="Modificar Benificio Global">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('RH-01-04-07-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" title="Eliminar" idBeneficioGlobal="'.$clavePrimaria.'"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado Beneficio Global" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Beneficio Global!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTablaListado($sql,$campos,$clavePrimaria);
    }


    public function metDataTablaListado($sql,$listado,$clavePrimaria,$datosExtraBotones = false,$flagSistema = false)
    {

        #Instancio la clase principal del modelo
        $modelo = new Modelo();
        #Accedo a la base de datos desde el controlador
        $db = $modelo->metAccesoControladorDB();
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        #Parametros enviados desde el datatable
        $pagina = $this->metObtenerFormulas('draw');
        $limiteTabla = $this->metObtenerFormulas('length');
        $inicio = $this->metObtenerFormulas('start');
        $columnas = $this->metObtenerFormulas('columns');
        $orden = $this->metObtenerFormulas('order');
        $busqueda = $this->metObtenerFormulas('search');
        $busqueda = $busqueda['value'];
        $tituloColumna = $columnas[$orden[0]['column']]['data'];
        $ordenColumna = $orden[0]['dir'];
        if ($inicio == 0) {
            $inicio = '0';
        }
        if ($pagina > 1) {
            $inicio = ($inicio / $limiteTabla) + 1;
        }
        if ($inicio != 0) {
            $primerResultado = $limiteTabla * ($inicio - 1);
        } else {
            $primerResultado = 0;
        }
        #concateno el orden de la columna
        $sql .="ORDER BY $tituloColumna $ordenColumna ";
        $resultado = $db->prepare($sql);
        $resultado->execute();
        $numeroTotalRegistros = $resultado->rowCount();
        $arrayResultado['recordsTotal'] = $numeroTotalRegistros;
        $arrayResultado['recordsFiltered'] = $numeroTotalRegistros;
        $arrayResultado['draw'] = $pagina;
        $arrayResultado['length'] = $limiteTabla;
        #concateno el Limite de la consulta
        $sql .= "LIMIT $primerResultado, $limiteTabla ";
        $resultados2 = $db->prepare($sql);
        $resultados2->execute();
        $detalle = array();
        foreach ($resultados2->fetchAll(PDO::FETCH_ASSOC) AS $i) {

            $i['num_limite'] = number_format($i['num_limite'],2,',','.');

            #recorro los datos
            foreach ($listado AS $titulo => $valor){
                if(!is_array($valor)){
                    $detalle[$valor]=$i[$valor];
                }else{
                    $detalle['acciones'] = '';
                    foreach ($listado[$titulo] AS $titulo2 => $valor2){
                        if($titulo == 'boton'){

                            if(is_array($valor2)){
                                $valorArreglo = str_replace("$clavePrimaria", $i[$clavePrimaria], $valor2[0]);
                                $valorEval = str_replace("#botonEval", $valorArreglo, $valor2[1]);

                                eval($valorEval);
                            }else{
                                #sustituyo el parametro del texto de la clave primaria en el boton
                                # por el valor extraido de la base de datos
                                $valor2 = str_replace("$clavePrimaria", $i[$clavePrimaria], $valor2);
                            }
                            if($datosExtraBotones){
                                foreach ($datosExtraBotones AS $datoExtraBoton){
                                    $valor2 = str_replace("$datoExtraBoton", $i[$datoExtraBoton], $valor2);
                                }
                            }
                            $detalle['acciones'] .= "$valor2 ";
                        }
                    }
                }
            }
            $arrayResultado['data'][]=$detalle;
        }
        if (!isset($arrayResultado['data'])) {
            $arrayResultado['data'] = array();
        }
        echo json_encode($arrayResultado);

    }



}
