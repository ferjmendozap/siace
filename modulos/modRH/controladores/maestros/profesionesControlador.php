<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class profesionesControlador extends Controlador
{
    private $atProfesiones;
    private $atGradoInstruccion;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atProfesiones      = $this->metCargarModelo('profesiones','maestros');
        $this->atGradoInstruccion = $this->metCargarModelo('gradoInstruccion','maestros');
    }

    #Metodo Index del controlador profesiones
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('enc','');
        $this->atVista->assign('Profesiones', $this->atProfesiones->metConsultarProfesiones());
        $this->atVista->metRenderizar('listado');


    }

    public function metNuevaProfesion()
    {
        $idProfesion = $this->metObtenerInt('idProfesion');
        $valido      = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');
            $formInt      = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($formAlphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='';

                }
            }

            if($idProfesion===0){

                $validacion['status']='creacion';
                $id = $this->atProfesiones->metRegistrarProfesion(
                      $validacion['fk_a006_num_miscelaneo_detalle_areaformacion'],
                      $validacion['fk_a006_num_miscelaneo_detalle_gradoinst'],
                      $validacion['ind_nombre_profesion'],
                      $validacion['num_estatus']
                );

                $validacion['idProfesion'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }

                $this->atProfesiones->metModificarProfesion($idProfesion,
                      $validacion['fk_a006_num_miscelaneo_detalle_areaformacion'],
                      $validacion['fk_a006_num_miscelaneo_detalle_gradoinst'],
                      $validacion['ind_nombre_profesion'],
                      $validacion['num_estatus']
                );
                $validacion['idProfesion']=$idProfesion;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idProfesion!=0){
            $db=$this->atProfesiones->metMostrarProfesion($idProfesion);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        #PARA LLENAR SELECT GRADO DE INSTRUCCION
        $GradoInstruccion = $this->atGradoInstruccion -> metConsultarGradoInstruccion();
        $this->atVista->assign('GradoInstruccion',$GradoInstruccion);

        #PARA LLENAR SELECT AREA
        $Area = $this->atProfesiones ->metMostrarSelect('AREA');
        $this->atVista->assign('Area',$Area);


        $this->atVista->assign('idProfesion',$idProfesion);
        $this->atVista->metRenderizar('form','modales');


    }

    #para eliminar una profesion
    public function metEliminarProfesion()
    {
        $idProfesion = $this->metObtenerInt('idProfesion');

        $this->atProfesiones->metEliminarProfesion($idProfesion);

        $array = array(
            'status'  => 'OK',
            'idProfesion' => $idProfesion
        );

        echo json_encode($array);
    }

}
