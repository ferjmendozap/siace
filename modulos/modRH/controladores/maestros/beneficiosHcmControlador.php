<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class beneficiosHcmControlador extends Controlador
{
    private $atBeneficiosHcm;
    private $atBeneficiosGlobalHcm;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atBeneficiosHcm  = $this->metCargarModelo('beneficiosHcm','maestros');
        $this->atBeneficiosGlobalHcm  = $this->metCargarModelo('beneficiosGlobalHcm','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('BeneficiosHcm', $this->atBeneficiosHcm->metConsultarBeneficiosHcm());
        $this->atVista->metRenderizar('listado');


    }

    public function metBeneficioHcm()
    {
        $idBeneficioHcm = $this->metObtenerInt('idBeneficioHcm');
        $valido         = $this->metObtenerInt('valido');

        #parametro para funcionalidad de HCM
        $HCMCONTTROL = Session::metObtener('HCMCONTROL');

        $complementosCss = array(
            'select2/select201ef'
        );
        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min',
            'select2/select2.min'
        );

        $js = array ('materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents'
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_limite_esp')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';
                    }
                }
            }


            if($idBeneficioHcm===0){


                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['flag_extensible'])){
                    $validacion['flag_extensible']=0;
                }


                #verificar los beneficios con el mosnto limite
                $verificar = $this->atBeneficiosHcm->metVerificarLimite($validacion['fk_rhb008_num_ayuda_global'],$validacion['num_limite_esp']);

                if($verificar==1){

                    $id = $this->atBeneficiosHcm->metRegistrarBeneficioHcm(
                        $validacion['fk_rhb008_num_ayuda_global'],
                        $validacion['ind_descripcion_especifica'],
                        $validacion['num_limite_esp'],
                        $validacion['num_empleado_apro'],
                        $validacion['num_estatus'],
                        $validacion['flag_extensible']
                    );
                    if(is_array($id)){
                        $validacion['status'] = 'error';
                        $validacion['detalleERROR'] = $id;
                        echo json_encode($validacion);
                        exit;
                    }else{
                        $validacion['status']='creacion';
                        $validacion['num_limite_esp'] = number_format($validacion['num_limite_esp'],2,',','.');
                        $validacion['idBeneficioHcm'] = $id;
                        echo json_encode($validacion);
                        exit;
                    }

                }else{
                    $aux = explode("#",$verificar);
                    $total = number_format($aux[0],2,',','.');
                    $limite = number_format($aux[1],2,',','.');
                    $validacion['status'] = 'errorLimite';
                    $validacion['mensaje'] = 'El Total de asignaciones por Beneficio ('.$total.') no puede ser Mayor que la Asignación Global ('.$limite.')';
                    echo json_encode($validacion);
                    exit;
                }





            }else{


                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['flag_extensible'])){
                    $validacion['flag_extensible']=0;
                }

                #verificar los beneficios con el monto limite
                $verificar = $this->atBeneficiosHcm->metVerificarLimite($validacion['fk_rhb008_num_ayuda_global'],$validacion['num_limite_esp'],$idBeneficioHcm);

                if($verificar==1) {

                    $id = $this->atBeneficiosHcm->metModificarBeneficioHcm($idBeneficioHcm,
                        $validacion['fk_rhb008_num_ayuda_global'],
                        $validacion['ind_descripcion_especifica'],
                        $validacion['num_limite_esp'],
                        $validacion['num_empleado_apro'],
                        $validacion['num_estatus'],
                        $validacion['flag_extensible']
                    );
                    if (is_array($id)) {
                        $validacion['status'] = 'error';
                        $validacion['detalleERROR'] = $id;
                        echo json_encode($validacion);
                        exit;
                    } else {
                        $validacion['status']='modificacion';
                        $validacion['num_limite_esp'] = number_format($validacion['num_limite_esp'], 2, ',', '.');
                        $validacion['idBeneficioHcm'] = $idBeneficioHcm;
                        echo json_encode($validacion);
                        exit;
                    }

                }else{
                    $aux = explode("#",$verificar);
                    $total = number_format($aux[0],2,',','.');
                    $limite = number_format($aux[1],2,',','.');
                    $validacion['status'] = 'errorLimite';
                    $validacion['mensaje'] = 'El Total de asignaciones por Beneficio ('.$total.') no puede ser Mayor que la Asignación Global ('.$limite.')';
                    echo json_encode($validacion);
                    exit;
                }

            }


        }

        if($idBeneficioHcm!=0){
            $db=$this->atBeneficiosHcm->metMostrarBeneficioHcm($idBeneficioHcm);
            $this->atVista->assign('formDB',$db);
        }

        #ASIGNACION GLOBAL (PARA LLENAR SELECT)
        $Global = $this->atBeneficiosGlobalHcm->metConsultarBeneficioGlobal();
        $this->atVista->assign('Global',$Global);
        #ASIGNACION GLOBAL (PARA LLENAR SELECT)

        #PARA LISTAR EMPLEADOS
        $Empleados = $this->atBeneficiosHcm->metListarEmpleados();
        $this->atVista->assign('Empleado',$Empleados);
        #PARA LISTAR EMPLEADOS

        $this->atVista->assign('control',$HCMCONTTROL);
        $this->atVista->assign('idBeneficioHcm',$idBeneficioHcm);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar beneficio hcm
    public function metEliminarBeneficioHcm()
    {
        $idBeneficioHcm = $this->metObtenerInt('idBeneficioHcm');

        $this->atBeneficiosHcm->metEliminarBeneficioHCM($idBeneficioHcm);

        $array = array(
            'status'  => 'OK',
            'idBeneficioHcm' => $idBeneficioHcm
        );

        echo json_encode($array);
    }


    #para actualizar el select asignaciones extensibles
    public function metListarAsignacionesExtensibles()
    {
        #obtengo el valor pasado del formulario
        $idBeneficioGlobal = $this->metObtenerInt('idBeneficioGlobal');

        #conecto con el modelo y consulto los datos
        $datos = $this->atBeneficiosHcm->metMostrarAsignacionesExtensibles($idBeneficioGlobal);

        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html="<option value=''>Seleccione</option>";
        for($i=0;$i<sizeof($datos);$i++){
            $html.= "<option value=".$datos[$i]['pk_num_ayuda_especifica'].">".$datos[$i]['ind_descripcion_especifica']."</option>";
        }
        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;

    }


    public function metMontoAsignado()
    {
        #obtengo el valor pasado del formulario
        $idAsignacion = $this->metObtenerInt('idAsignacion');

        #conecto con el modelo y consulto los datos
        $datos = $this->atBeneficiosHcm->metMontoAsignado($idAsignacion);

        $monto_asignado = number_format($datos['num_limite_esp'],2,',','.');

        #los codifico con json para mostrarlos en la vista
        echo json_encode($monto_asignado);
        exit;

    }

}
