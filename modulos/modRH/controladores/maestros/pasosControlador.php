<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class pasosControlador extends Controlador
{
    private $atPasos;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atPasos = $this->metCargarModelo('pasos','maestros');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('idMaestro', $this->atPasos->metObtenerMaestro());
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevoPaso()
    {
        $idPasos = $this->metObtenerInt('idPasos');
        $valido             = $this->metObtenerInt('valido');
        $maestro            = $this->metObtenerInt('maestro');

        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        if($valido==1){

            $this->metValidarToken();

            $formTxt = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            if($idPasos===0){


                $validacion['status']='creacion';
                $id = $this->atPasos->metRegistrarPasos(
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_detalle'],
                    $validacion['idMaestro']
                );

                $validacion['idPasos']=$id;
                echo json_encode($validacion);
                exit;


            }else{


                $validacion['status']='modificacion';

                $this->atPasos->metModificarPasos($idPasos,
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_detalle']
                );
                $validacion['idPasos']=$idPasos;
                echo json_encode($validacion);
                exit;


            }


        }

        if($idPasos!=0){
            $db = $this->atPasos->metMostrarPasos($idPasos);
            $this->atVista->assign('formDB',$db);
        }

        $this->atVista->assign('idPasos',$idPasos);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->metRenderizar('form','modales');

    }

    public function metEliminarPasos()
    {
        $idPasos = $this->metObtenerInt('idPasos');
        $this->atPasos->metEliminarPasos($idPasos);
        $array = array(
            'status' => 'OK',
            'idPasos' => $idPasos
        );

        echo json_encode($array);
    }


    #PERMITE LISTAR PASOS A TRAVES DEL DATATABLES (JSON)
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
             SELECT
              a006.pk_num_miscelaneo_detalle,
              a006.ind_nombre_detalle,
              a006.cod_detalle,
              a006.num_estatus,
              a006.fk_a005_num_miscelaneo_maestro
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE a005.cod_maestro='PASOS'
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    (
                      a006.ind_nombre_detalle LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_miscelaneo_detalle','ind_nombre_detalle');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_miscelaneo_detalle';
        #construyo el listado de botones
        if (in_array('RH-01-04-01-23-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" title="Editar" idPasos="'.$clavePrimaria.'" descipcion="El Usuario a Modificado un Paso" titulo="Modificar Pasos">
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('RH-01-04-01-24-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idPasos="'.$clavePrimaria.'"  boton="si, Eliminar"
                            descipcion="El usuario a eliminado un Paso" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Paso!!">
                            <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }

}
