<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class centroEstudiosControlador extends Controlador
{
    private $atCentroEstudios;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atCentroEstudios = $this->metCargarModelo('centroEstudios','maestros');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Centros', $this->atCentroEstudios->metConsultarCentros());
        $this->atVista->metRenderizar('listado');


    }

    public function metNuevoCentroEstudio()
    {
        $idCentro = $this->metObtenerInt('idCentro');
        $valido   = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt      =$this->metObtenerTexto('form','txt');
            $formInt      = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');


            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }

            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($formAlphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='';

                }
            }

            if($idCentro===0){

                $validacion['status']='creacion';

                if(!isset($validacion['ind_flag_estudio'])){
                    $validacion['ind_flag_estudio']=0;
                }
                if(!isset($validacion['ind_flag_curso'])){
                    $validacion['ind_flag_curso']=0;
                }

                $id = $this->atCentroEstudios->metRegistrarCentro(
                    $validacion['ind_nombre_institucion'],
                    $validacion['ind_ubicacion'],
                    $validacion['ind_flag_estudio'],
                    $validacion['ind_flag_curso'],
                    $validacion['num_estatus']
                );

                $validacion['idCentro'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['ind_flag_estudio'])){
                    $validacion['ind_flag_estudio']=0;
                }
                if(!isset($validacion['ind_flag_curso'])){
                    $validacion['ind_flag_curso']=0;
                }

                $this->atCentroEstudios->metModificarCentro($idCentro,
                    $validacion['ind_nombre_institucion'],
                    $validacion['ind_ubicacion'],
                    $validacion['ind_flag_estudio'],
                    $validacion['ind_flag_curso'],
                    $validacion['num_estatus']
                );
                $validacion['idCentro']=$idCentro;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idCentro!=0){
            $db = $this->atCentroEstudios->metMostrarCentro($idCentro);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        $this->atVista->assign('idCentro',$idCentro);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar un centro de estudio
    public function metEliminarCentro()
    {
        $idCentro = $this->metObtenerInt('idCentro');

        $this->atCentroEstudios ->metEliminarCentro($idCentro);
        $array = array(
            'status'  => 'OK',
            'idCentro' => $idCentro
        );

        echo json_encode($array);
    }

}
