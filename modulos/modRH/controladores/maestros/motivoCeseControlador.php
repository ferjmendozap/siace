<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class motivoCeseControlador extends Controlador
{
    private $atMotivoCese;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atMotivoCese  = $this->metCargarModelo('motivoCese','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('MotivoCese', $this->atMotivoCese->metConsultarMotivoCese());
        $this->atVista->metRenderizar('listado');


    }

    public function metNuevoMotivoCese()
    {
        $idMotivoCese = $this->metObtenerInt('idMotivoCese');
        $valido       = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }


            if($idMotivoCese===0){

                $validacion['status']='creacion';

                if(!isset($validacion['num_flag_falta_grave'])){
                    $validacion['num_flag_falta_grave']=0;
                }
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }

                $id = $this->atMotivoCese->metRegistrarMotivoCese(
                    $validacion['ind_nombre_cese'],
                    $validacion['num_flag_falta_grave'],
                    $validacion['num_estatus']
                );

                $validacion['idMotivoCese'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['num_flag_falta_grave'])){
                    $validacion['num_flag_falta_grave']=0;
                }

                $this->atMotivoCese->metModificarMotivoCese($idMotivoCese,
                    $validacion['ind_nombre_cese'],
                    $validacion['num_flag_falta_grave'],
                    $validacion['num_estatus']
                );
                $validacion['idMotivoCese']=$idMotivoCese;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idMotivoCese!=0){
            $db=$this->atMotivoCese->metMostrarMotivoCese($idMotivoCese);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        $this->atVista->assign('idMotivoCese',$idMotivoCese);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar motivo cese
    public function metEliminarMotivoCese()
    {
        $idMotivoCese = $this->metObtenerInt('idMotivoCese');

        $this->atMotivoCese->metEliminarMotivoCese($idMotivoCese);

        $array = array(
            'status'  => 'OK',
            'idMotivoCese' => $idMotivoCese
        );

        echo json_encode($array);
    }

}
