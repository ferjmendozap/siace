<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class dependenciasControlador extends Controlador
{
    private $atDependencias;
    private $atCargos;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atDependencias    = $this->metCargarModelo('dependencias','maestros');

    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );

        $js = array ('materialSiace/core/demo/DemoTableDynamic');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('enc','');
        $this->atVista->assign('Dependencias', $this->atDependencias->metConsultarDependencias());
        $this->atVista->metRenderizar('listado');
    }

    public function metDependencias()
    {
        $idDependencia = $this->metObtenerInt('idDependencia');
        $valido        = $this->metObtenerInt('valido');

        $complementosCss = array(
            'select2/select201ef',
        );
        $complementoJs = array(
            'select2/select2.min',
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idDependencia===0){

                $validacion['status']='creacion';
                if(!isset($validacion['num_flag_responsable'])){
                    $validacion['num_flag_responsable']=0;
                }
                if(!isset($validacion['num_flag_controlfiscal'])){
                    $validacion['num_flag_controlfiscal']=0;
                }
                if(!isset($validacion['num_flag_principal'])){
                    $validacion['num_flag_principal']=0;
                }
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(empty($validacion['fk_a003_num_persona_responsable'])){
                    $validacion['fk_a003_num_persona_responsable']=NULL;
                }


                $id = $this->atDependencias->metRegistrarDependencias(
                    $validacion['fk_a001_num_organismo'], $validacion['ind_dependencia'], $validacion['ind_codpadre'],
                    $validacion['ind_codinterno'],
                    $validacion['ind_nivel'],
                    $validacion['num_piso'],
                    $validacion['txt_abreviacion'],
                    $validacion['fk_a003_num_persona_responsable'],
                    $validacion['num_flag_responsable'],
                    $validacion['num_flag_controlfiscal'],
                    $validacion['num_flag_principal'],
                    $validacion['num_estatus']

                );

                $validacion['idDependencia']=$id;
                echo json_encode($validacion);
                exit;

            }else{

                $validacion['status']='modificacion';
                if(!isset($validacion['num_flag_responsable'])){
                    $validacion['num_flag_responsable']=0;
                }
                if(!isset($validacion['num_flag_controlfiscal'])){
                    $validacion['num_flag_controlfiscal']=0;
                }
                if(!isset($validacion['num_flag_principal'])){
                    $validacion['num_flag_principal']=0;
                }
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(empty($validacion['fk_a003_num_persona_responsable'])){
                    $validacion['fk_a003_num_persona_responsable']=NULL;
                }
                $this->atDependencias->metModificarDependencias($idDependencia,
                    $validacion['fk_a001_num_organismo'], $validacion['ind_dependencia'], $validacion['ind_codpadre'],
                    $validacion['ind_codinterno'],
                    $validacion['ind_nivel'],
                    $validacion['num_piso'],
                    $validacion['txt_abreviacion'],
                    $validacion['fk_a003_num_persona_responsable'],
                    $validacion['num_flag_responsable'],
                    $validacion['num_flag_controlfiscal'],
                    $validacion['num_flag_principal'],
                    $validacion['num_estatus']
                );

                $validacion['idDependencia']=$idDependencia;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idDependencia!=0){
            $db=$this->atDependencias->metMostrarDependencias($idDependencia);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        #select organismos
        $organismos = $this->atDependencias->metMostrarOrganismosSelect();
        $this->atVista->assign('Organismos',$organismos);

        #select dependencias
        $dependencia = $this->atDependencias->metConsultarDependencias();
        $this->atVista->assign('Dependencia',$dependencia);

        #PARA LISTAR EMPLEADOS
        $Empleados = $this->atDependencias->metListarEmpleados();
        $this->atVista->assign('Empleado',$Empleados);

        $this->atVista->assign('idDependencia',$idDependencia);
        $this->atVista->metRenderizar('form','modales');

    }

    public function metEliminarDependencias()
    {
        $idDependencia = $this->metObtenerInt('idDependencia');
        $this->atDependencias->metEliminarDependencias($idDependencia);
        $array = array(
            'status' => 'OK',
            'idDependencia' => $idDependencia
        );

        echo json_encode($array);
    }

    #para actualizar el nivel segun la dependencia seleccionada
    public function metVerificarNivel()
    {
        #obtengo el valor pasado del formulario
        $Dep = $this->metObtenerInt('dep');
        #conecto con el modelo y consulto los datos
        $datos = $this->atDependencias->metVerificarNivelDependencia($Dep);

        $nivel = count($datos)+1;

        #los codifico con json para mostrarlos en la vista
        echo json_encode($nivel);
        exit;

    }

    #para obtener el cargo del empleado
    public function metObtenerCargoEmpleado()
    {
        $this->atCargos = $this->metCargarModelo('cargos','maestros');
        #obtengo el valor pasado del formulario
        $persona = $this->metObtenerInt('empleado');
        #conecto con el modelo y consulto los datos
        $datos = $this->atCargos->metObtenerCargoEmpleado($persona);
        #los codifico con json para mostrarlos en la vista
        echo json_encode($datos);

    }


}
