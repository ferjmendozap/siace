<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class horarioLaboralControlador extends Controlador
{
    private $atHorario;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atHorario  = $this->metCargarModelo('horarioLaboral','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'

        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Horario', $this->atHorario->metConsultarHorarioLaboral());
        $this->atVista->metRenderizar('listado');


    }

    public function metNuevoHorarioLaboral()
    {
        $idHorario  = $this->metObtenerInt('idHorario');
        $valido     = $this->metObtenerInt('valido');

        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min'
        );

       $js = array ('Aplicacion/appFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents'
        );

        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        if($valido==1){

            $this->metValidarToken();

            $formTxt      = $this->metObtenerTexto('form','txt');
            $formInt      = $this->metObtenerInt('form','int');
            $formForm     = $this->metObtenerFormulas('form','formulas');


            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if(!empty($formForm)) {
                foreach ($formForm as $tituloTxt => $valorTxt) {
                    if (!empty($formForm[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }


            if($idHorario===0){

                $validacion['status']='creacion';

                if(!isset($validacion['num_flag_corrido'])){
                    $validacion['num_flag_corrido']=0;
                }
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['num_flag_laborable'])){
                    $validacion['num_flag_laborable']=0;
                }

                $id = $this->atHorario->metRegistrarHorarioLaboral(
                    $validacion['ind_descripcion'],
                    $validacion['num_flag_corrido'],
                    $validacion['ind_resolucion'],
                    $validacion['fec_resolucion'],
                    $validacion['dia'],
                    $validacion['num_flag_laborable'],
                    $validacion['fec_entrada1'],
                    $validacion['fec_salida1'],
                    $validacion['fec_entrada2'],
                    $validacion['fec_salida2'],
                    $validacion['num_estatus']
                );

                if($validacion['num_flag_corrido']==0){
                    $validacion['num_flag_corrido']='No';
                }else{
                    $validacion['num_flag_corrido']='Si';
                }

                $validacion['idHorario'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['num_flag_corrido'])){
                    $validacion['num_flag_corrido']=0;
                }
                if(!isset($validacion['num_flag_laborable'])){
                    $validacion['num_flag_laborable']=0;
                }
                $this->atHorario->metModificarHorarioLaboral($idHorario,
                    $validacion['ind_descripcion'],
                    $validacion['num_flag_corrido'],
                    $validacion['ind_resolucion'],
                    $validacion['fec_resolucion'],
                    $validacion['dia'],
                    $validacion['num_flag_laborable'],
                    $validacion['fec_entrada1'],
                    $validacion['fec_salida1'],
                    $validacion['fec_entrada2'],
                    $validacion['fec_salida2'],
                    $validacion['pk_num_horario_detalle'],
                    $validacion['num_estatus']
                );

                if($validacion['num_flag_corrido']==0){
                    $validacion['num_flag_corrido']='No';
                }else{
                    $validacion['num_flag_corrido']='Si';
                }

                $validacion['idHorario']=$idHorario;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idHorario!=0){
            //horario general
            $db1=$this->atHorario->metMostrarHorarioGeneral($idHorario);
            $this->atVista->assign('formDB1',$db1);
            $this->atVista->assign('numero',1);
            $this->atVista->assign('n',0);
            //horario detalle
            $db2=$this->atHorario->metMostrarHorarioDetalle($idHorario);
            $this->atVista->assign('formDB2',$db2);
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        $semana = $this->atHorario->metDiasSemana();
        $this->atVista->assign('Dias',$semana);
        $this->atVista->assign('num',1);
        $this->atVista->assign('n',0);
        $this->atVista->assign('idHorario',$idHorario);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar hgorario laboral
    public function metEliminarHorarioLaboral()
    {
        $idHorario = $this->metObtenerInt('idHorario');

        $id = $this->atHorario->metEliminarHorarioLaboral($idHorario);

        if($id==1){
            $array = array(
                'status'  => 'OK',
                'idHorario' => $idHorario
            );
        }else{
            $array = array(
                'status'  => 'ERROR',
                'mensaje' => 'No se puede Eliminar el Horario por Integridad Referencial, posiblemente este siendo usado. Consulte con el Administrador del Sistema'
            );
        }



        echo json_encode($array);
    }

}
