<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class gradoInstruccionControlador extends Controlador
{
    private $atGradoInstruccion;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atGradoInstruccion = $this->metCargarModelo('gradoInstruccion','maestros');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('GradoInstruccion', $this->atGradoInstruccion->metConsultarGradoInstruccion());
        $this->atVista->assign('idMaestro', $this->atGradoInstruccion->metObtenerMaestro());
        $this->atVista->metRenderizar('listado');


    }

    public function metNuevoGradoInstruccion()
    {
        $idGradoInstruccion = $this->metObtenerInt('idGradoInstruccion');
        $valido             = $this->metObtenerInt('valido');
        $maestro            = $this->metObtenerInt('maestro');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }


            if($idGradoInstruccion===0){

                $validacion['status']='creacion';
                $id = $this->atGradoInstruccion->metRegistrarGradoInstruccion(
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_instruccion'],
                    $validacion['idMaestro']
                );

                $validacion['idGradoInstruccion'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['ind_estatus'])){
                    $validacion['ind_estatus']=0;
                }

                $this->atGradoInstruccion->metModificarGradoInstruccion($idGradoInstruccion,
                    $validacion['cod_detalle'],
                    $validacion['ind_nombre_instruccion']
                );
                $validacion['idGradoInstruccion']=$idGradoInstruccion;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idGradoInstruccion!=0){
            $db=$this->atGradoInstruccion->metMostrarGradoInstruccion($idGradoInstruccion);
            $this->atVista->assign('formDB',$db);

        }

        $this->atVista->assign('idGradoInstruccion',$idGradoInstruccion);
        $this->atVista->assign('idMaestro',$maestro);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar un grado de instruccion
    public function metEliminarGradoInstruccion()
    {
        $idGradoInstruccion = $this->metObtenerInt('idGradoInstruccion');
        $this->atGradoInstruccion ->metEliminarGradoInstruccion($idGradoInstruccion);
        $array = array(
            'status'  => 'OK',
            'idGradoInstruccion' => $idGradoInstruccion
        );

        echo json_encode($array);
    }

}
