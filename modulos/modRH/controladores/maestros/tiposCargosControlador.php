<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class tiposCargosControlador extends Controlador
{
    private $atTiposCargosModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atTiposCargosModelo = $this->metCargarModelo('tiposCargos','maestros');
    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    #PERMITE REGISTRAR Y/O MODIFICAR UN TIPO DE CARGO
    public function metNuevoTipoCargo()
    {
        $idTiposCargos = $this->metObtenerInt('idTiposCargos');
        $valido        = $this->metObtenerInt('valido');

        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        if($valido==1){

            $this->metValidarToken();

            $formTxt=$this->metObtenerTexto('form','txt');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');


            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                        $validacion[$tituloTxt]='';

                }
            }

            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($formAlphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='';

                }
            }


            if($idTiposCargos===0){


                $validacion['status']='creacion';
                $id = $this->atTiposCargosModelo->metRegistrarTipoCargo(
                    $validacion['ind_nombre_cargo'], $validacion['txt_descripcion'], $validacion['txt_funcion']
                    );

                $validacion['idTiposCargos']=$id;
                echo json_encode($validacion);
                exit;


            }else{


                $validacion['status']='modificacion';

                $this->atTiposCargosModelo->metModificarTipoCargo($idTiposCargos,
                    $validacion['ind_nombre_cargo'], $validacion['txt_descripcion'], $validacion['txt_funcion']
                );
                $validacion['idTiposCargos']=$idTiposCargos;
                echo json_encode($validacion);
                exit;


            }

        }

            if($idTiposCargos!=0){
                $db=$this->atTiposCargosModelo->metMostrarTiposCargos($idTiposCargos);
                $this->atVista->assign('formDB',$db);
            }

            $this->atVista->assign('idTiposCargos',$idTiposCargos);
            $this->atVista->metRenderizar('form','modales');





    }

    #PERMITE ELIMINAR UN TIPO DE CARGO
    public function metEliminarTipoCargo()
    {
        $idTiposCargos = $this->metObtenerInt('idTiposCargos');

        $this->atTiposCargosModelo->metEliminarTipoCargo($idTiposCargos);
        $array = array(
            'status' => 'OK',
            'idTiposCargos' => $idTiposCargos
        );

        echo json_encode($array);
    }

    #PERMITE LISTAR LOS TIPOS DE CARGOS A TRAVES DEL DATATABLES (JSON)
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
            SELECT
                *
              FROM
                rh_c006_tipo_cargo
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    (
                      ind_nombre_cargo LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_cargo','ind_nombre_cargo');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_cargo';
        #construyo el listado de botones
        if (in_array('RH-01-04-01-03-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" title="Editar" idTiposCargos="'.$clavePrimaria.'" descripcion="El Usuario a Modificado un Tipo de Cargo" titulo="Modificar Tipo de Cargo">
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('RH-01-04-01-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idTiposCargos="'.$clavePrimaria.'" boton="si, Eliminar"
                            descripcion="El usuario a eliminado un Tipo de Cargo" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Cargo!!">
                        <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }

}
