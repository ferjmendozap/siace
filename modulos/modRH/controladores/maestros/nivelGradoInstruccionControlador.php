<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class nivelGradoInstruccionControlador extends Controlador
{
    private $atNivelGradoInstruccion;
    private $atGradoInstruccion;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atNivelGradoInstruccion = $this->metCargarModelo('nivelGradoInstruccion','maestros');
        $this->atGradoInstruccion      = $this->metCargarModelo('gradoInstruccion','maestros');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('enc','');
        $this->atVista->assign('NivelGradoInstruccion', $this->atNivelGradoInstruccion->metConsultarNivelGradoInstruccion());
        $this->atVista->metRenderizar('listado');


    }

    public function metNuevoNivelGradoInstruccion()
    {
        $idNivelGradoInstruccion = $this->metObtenerInt('idNivelGradoInstruccion');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }


            if($idNivelGradoInstruccion===0){

                $validacion['status']='creacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['num_flag_utiles'])){
                    $validacion['num_flag_utiles']=0;
                }

                $id = $this->atNivelGradoInstruccion->metRegistrarNivelGradoInstruccion(
                    $validacion['fk_a006_num_miscelaneo_detalle_gradoinst'],
                    $validacion['ind_nombre_nivel_grado'],
                    $validacion['num_estatus'],
                    $validacion['num_flag_utiles']
                );

                $validacion['idNivelGradoInstruccion'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['num_flag_utiles'])){
                    $validacion['num_flag_utiles']=0;
                }

                $this->atNivelGradoInstruccion->metModificarNivelGradoInstruccion($idNivelGradoInstruccion,
                    $validacion['fk_a006_num_miscelaneo_detalle_gradoinst'],
                    $validacion['ind_nombre_nivel_grado'],
                    $validacion['num_estatus'],
                    $validacion['num_flag_utiles']
                );
                $validacion['idNivelGradoInstruccion']=$idNivelGradoInstruccion;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idNivelGradoInstruccion!=0){
            $db=$this->atNivelGradoInstruccion->metMostrarNivelGradoInstruccion($idNivelGradoInstruccion);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        $GradoInstruccion = $this->atGradoInstruccion->metConsultarGradoInstruccion();
        $this->atVista->assign('GradoInstruccion',$GradoInstruccion);

        $this->atVista->assign('idNivelGradoInstruccion',$idNivelGradoInstruccion);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar NIVEL GRADO DE INSTRUCCION
    public function metEliminarNivelGradoInstruccion()
    {
        $idNivelGradoInstruccion = $this->metObtenerInt('idNivelGradoInstruccion');
        $this->atNivelGradoInstruccion ->metEliminarNivelGradoInstruccion($idNivelGradoInstruccion);
        $array = array(
            'status'  => 'OK',
            'idNivelGradoInstruccion' => $idNivelGradoInstruccion
        );

        echo json_encode($array);
    }


    public function metMostrarNivelesGradoInst()
    {
        $gradoInst = $this->metObtenerInt('GradoInst');

        $datos =  $this->atNivelGradoInstruccion->metMostrarNivelesGradoInst($gradoInst);

        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html="";
        for($i=0;$i<sizeof($datos);$i++){
            $html.= "<option value=".$datos[$i]['pk_num_nivel_grado'].">".$datos[$i]['ind_nombre_nivel_grado']."</option>";
        }

        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;
    }


}
