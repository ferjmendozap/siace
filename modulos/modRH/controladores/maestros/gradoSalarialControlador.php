<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class gradoSalarialControlador extends Controlador
{
    private $atGradoSalarial;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atGradoSalarial    = $this->metCargarModelo('gradoSalarial','maestros');

    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );

        $js = array ('materialSiace/core/demo/DemoTableDynamic','Aplicacion/appFunciones');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        //$this->atVista->assign('GradoSalarial', $this->atGradoSalarial->metConsultarGradoSalarial());
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevoGradoSalarial()
    {
        $idGradoSalarial = $this->metObtenerInt('idGradoSalarial');
        $valido          = $this->metObtenerInt('valido');


        $complementoJs = array(
            'inputmask/mask','inputmask/jquery.inputmask.bundle.min'
        );

        $js = array ('materialSiace/App',
                'materialSiace/core/demo/DemoFormComponents','Aplicacion/appFunciones'
        );

        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');


            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    if((strcmp($tituloInt,'num_sueldo_minimo')==0) || (strcmp($tituloInt,'num_sueldo_maximo')==0) || (strcmp($tituloInt,'num_sueldo_promedio')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }



            if($idGradoSalarial===0){

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['flag_pasos'])){
                    $validacion['flag_pasos']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_paso'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_paso']=false;
                }

                $validacion['status']='creacion';
                $id = $this->atGradoSalarial->metRegistrarGradoSalarial(
                    $validacion['fk_a006_num_miscelaneo_detalle'], $validacion['ind_grado'], $validacion['ind_nombre_grado'],
                    $validacion['num_sueldo_minimo'],
                    $validacion['num_sueldo_maximo'],
                    $validacion['num_sueldo_promedio'],
                    $validacion['num_estatus'],
                    $validacion['fk_a006_num_miscelaneo_detalle_paso'],
                    $validacion['flag_pasos']

                );

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atGradoSalarial->metMostrarGradoSalarial($id);
                    $validacion['idGradoSalarial']=$datos['pk_num_grado'];
                    $validacion['num_sueldo_promedio'] = number_format($datos['num_sueldo_promedio'],2,',','.');
                    $validacion['flag_pasos'] = $datos['flag_pasos'];
                    echo json_encode($validacion);
                    exit;
                }


            }else{


                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['flag_pasos'])){
                    $validacion['flag_pasos']=0;
                }
                if(!isset($validacion['fk_a006_num_miscelaneo_detalle_paso'])){
                    $validacion['fk_a006_num_miscelaneo_detalle_paso']=false;
                }
                if(!isset($validacion['pk_num_grados_pasos'])){
                    $validacion['pk_num_grados_pasos']=false;
                }

                $id = $this->atGradoSalarial->metModificarGradoSalarial($idGradoSalarial,
                    $validacion['fk_a006_num_miscelaneo_detalle'], $validacion['ind_grado'], $validacion['ind_nombre_grado'],
                    $validacion['num_sueldo_minimo'], $validacion['num_sueldo_maximo'], $validacion['num_sueldo_promedio'],
                    $validacion['num_estatus'],$validacion['fk_a006_num_miscelaneo_detalle_paso'],$validacion['pk_num_grados_pasos'],
                    $validacion['flag_pasos']
                );

                $validacion['idGradoSalarial']=$idGradoSalarial;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atGradoSalarial->metMostrarGradoSalarial($id);
                    $validacion['idGradoSalarial']=$datos['pk_num_grado'];
                    $validacion['num_sueldo_promedio'] = number_format($datos['num_sueldo_promedio'],2,',','.');
                    $validacion['flag_pasos'] = $datos['flag_pasos'];
                    echo json_encode($validacion);
                    exit;
                }




            }


        }

        if($idGradoSalarial!=0){
            $db=$this->atGradoSalarial->metMostrarGradoSalarial($idGradoSalarial);
            $this->atVista->assign('formDB',$db);

            if($db['flag_pasos']==1){
                $dbpasos = $this->atGradoSalarial->metVisualizarPasos($idGradoSalarial);
                $this->atVista->assign('formDBpasos',$dbpasos);
            }

            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }


        #PARA LLENAR SELECT AREA
        $miscelaneoDetalle = $this->atGradoSalarial->metMostrarSelect('CATCARGO');
        $this->atVista->assign('Categoria',$miscelaneoDetalle);

        $miscelaneoPasos = $this->atGradoSalarial->metMostrarSelect('PASOS');
        $this->atVista->assign('Pasos',$miscelaneoPasos);



        $this->atVista->assign('numero', 1);
        $this->atVista->assign('n', 0);
        $this->atVista->assign('idGradoSalarial',$idGradoSalarial);
        $this->atVista->metRenderizar('form','modales');

    }



    public function metJsonDataTablaGradoSalarial()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol = Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
                SELECT
                rh_c007_grado_salarial.pk_num_grado,
                rh_c007_grado_salarial.fk_a006_num_miscelaneo_detalle,
                a006_miscelaneo_detalle.ind_nombre_detalle,
                rh_c007_grado_salarial.ind_grado,
                rh_c007_grado_salarial.ind_nombre_grado,
                rh_c007_grado_salarial.num_sueldo_minimo,
                rh_c007_grado_salarial.num_sueldo_maximo,
                rh_c007_grado_salarial.num_sueldo_promedio,
                rh_c007_grado_salarial.num_estatus,
                rh_c007_grado_salarial.flag_pasos
                FROM
                a006_miscelaneo_detalle
                INNER JOIN rh_c007_grado_salarial ON rh_c007_grado_salarial.fk_a006_num_miscelaneo_detalle = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
         ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .= "
                WHERE
                    (
                      a006_miscelaneo_detalle.ind_nombre_detalle LIKE '%$busqueda[value]%' OR
                      rh_c007_grado_salarial.ind_grado LIKE '%$busqueda[value]%' OR
                      rh_c007_grado_salarial.ind_nombre_grado LIKE '%$busqueda[value]%'
                    )


            ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_grado', 'ind_nombre_detalle', 'ind_grado', 'ind_nombre_grado', 'num_sueldo_promedio');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_grado';
        #construyo el listado de botones

        if (in_array('RH-01-04-01-19-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" title="Editar" idGradoSalarial="'.$clavePrimaria.'"
                                                        descipcion="El Usuario a Modificado un Grado Salarial" titulo="Modificar Grado Salarial">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('RH-01-04-01-20-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idGradoSalarial="'.$clavePrimaria.'"  boton="si, Eliminar"
                                                        descipcion="El usuario a eliminado un Grado Salarial" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Grado Salarial!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador
        $this->metDataTablaListado($sql,$campos,$clavePrimaria);
    }


    public function metDataTablaListado($sql,$listado,$clavePrimaria,$datosExtraBotones = false,$flagSistema = false)
    {

        #Instancio la clase principal del modelo
        $modelo = new Modelo();
        #Accedo a la base de datos desde el controlador
        $db = $modelo->metAccesoControladorDB();
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        #Parametros enviados desde el datatable
        $pagina = $this->metObtenerFormulas('draw');
        $limiteTabla = $this->metObtenerFormulas('length');
        $inicio = $this->metObtenerFormulas('start');
        $columnas = $this->metObtenerFormulas('columns');
        $orden = $this->metObtenerFormulas('order');
        $busqueda = $this->metObtenerFormulas('search');
        $busqueda = $busqueda['value'];
        $tituloColumna = $columnas[$orden[0]['column']]['data'];
        $ordenColumna = $orden[0]['dir'];
        if ($inicio == 0) {
            $inicio = '0';
        }
        if ($pagina > 1) {
            $inicio = ($inicio / $limiteTabla) + 1;
        }
        if ($inicio != 0) {
            $primerResultado = $limiteTabla * ($inicio - 1);
        } else {
            $primerResultado = 0;
        }
        #concateno el orden de la columna
        $sql .="ORDER BY $tituloColumna $ordenColumna ";
        $resultado = $db->prepare($sql);
        $resultado->execute();
        $numeroTotalRegistros = $resultado->rowCount();
        $arrayResultado['recordsTotal'] = $numeroTotalRegistros;
        $arrayResultado['recordsFiltered'] = $numeroTotalRegistros;
        $arrayResultado['draw'] = $pagina;
        $arrayResultado['length'] = $limiteTabla;
        #concateno el Limite de la consulta
        $sql .= "LIMIT $primerResultado, $limiteTabla ";
        $resultados2 = $db->prepare($sql);
        $resultados2->execute();
        $detalle = array();
        foreach ($resultados2->fetchAll(PDO::FETCH_ASSOC) AS $i) {

            #valido
            if(isset($i['flag_pasos'])){
                if($i['flag_pasos']==1){
                    $valor = '<button class="verPasos btn btn-xs ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                        title="Visualizar Pasos" titulo="Visualizar Pasos" idGradoSalarial="'.$i['pk_num_grado'].'" nombre="'.$i['ind_nombre_grado'].'" id="verPasos" >
                                                    <i class="md md-search"></i> Pasos
                                                </button>';
                }else{
                    $valor = number_format($i['num_sueldo_promedio'],2,',','.');
                }

                $i['num_sueldo_promedio'] = $valor;
            }

            #recorro los datos
            foreach ($listado AS $titulo => $valor){
                if(!is_array($valor)){
                    $detalle[$valor]=$i[$valor];
                }else{
                    $detalle['acciones'] = '';
                    foreach ($listado[$titulo] AS $titulo2 => $valor2){
                        if($titulo == 'boton'){

                            if(is_array($valor2)){
                                $valorArreglo = str_replace("$clavePrimaria", $i[$clavePrimaria], $valor2[0]);
                                $valorEval = str_replace("#botonEval", $valorArreglo, $valor2[1]);

                                eval($valorEval);
                            }else{
                                #sustituyo el parametro del texto de la clave primaria en el boton
                                # por el valor extraido de la base de datos
                                $valor2 = str_replace("$clavePrimaria", $i[$clavePrimaria], $valor2);
                            }
                            if($datosExtraBotones){
                                foreach ($datosExtraBotones AS $datoExtraBoton){
                                    $valor2 = str_replace("$datoExtraBoton", $i[$datoExtraBoton], $valor2);
                                }
                            }
                            $detalle['acciones'] .= "$valor2 ";
                        }
                    }
                }
            }
            $arrayResultado['data'][]=$detalle;
        }
        if (!isset($arrayResultado['data'])) {
            $arrayResultado['data'] = array();
        }
        echo json_encode($arrayResultado);

    }


    public function metVisualizarPasos()
    {
        $idGradoSalarial = $this->metObtenerInt('idGradoSalarial');
        $nombre = $_POST['nombre'];

        $datos = $this->atGradoSalarial->metVisualizarPasos($idGradoSalarial);

        $this->atVista->assign('Pasos',$datos);
        $this->atVista->assign('nombre_grado',$nombre);
        $this->atVista->metRenderizar('pasos','modales');
    }

    public function metEliminarGradoSalarial()
    {
        $idGradoSalarial = $this->metObtenerInt('idGradoSalarial');
        $this->atGradoSalarial->metEliminarGradoSalarial($idGradoSalarial);
        $array = array(
            'status' => 'OK',
            'idGradoSalarial' => $idGradoSalarial
        );

        echo json_encode($array);
    }


}
