<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class idiomasControlador extends Controlador
{
    private $atIdiomas;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atIdiomas  = $this->metCargarModelo('idiomas','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Idiomas', $this->atIdiomas->metConsultarIdiomas());
        $this->atVista->metRenderizar('listado');


    }

    public function metNuevoIdioma()
    {
        $idIdioma = $this->metObtenerInt('idIdioma');
        $valido   = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   =$this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }


            if($idIdioma===0){

                $validacion['status']='creacion';

               $id = $this->atIdiomas->metRegistrarIdiomas(
                    $validacion['ind_nombre_idioma'],
                    $validacion['ind_nombre_ext'],
                    $validacion['num_estatus']
                );

                $validacion['idIdioma'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }

                $this->atIdiomas->metModificarIdiomas($idIdioma,
                    $validacion['ind_nombre_idioma'],
                    $validacion['ind_nombre_ext'],
                    $validacion['num_estatus']
                );
                $validacion['idIdioma']=$idIdioma;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idIdioma!=0){
            $db=$this->atIdiomas->metMostrarIdiomas($idIdioma);
            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        $this->atVista->assign('idIdioma',$idIdioma);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar un idioma
    public function metEliminarIdioma()
    {
        $idIdioma = $this->metObtenerInt('idIdioma');

        $this->atIdiomas ->metEliminarIdioma($idIdioma);

        $array = array(
            'status'  => 'OK',
            'idIdioma' => $idIdioma
        );

        echo json_encode($array);
    }

}
