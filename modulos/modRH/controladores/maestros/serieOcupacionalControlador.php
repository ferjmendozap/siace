<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class serieOcupacionalControlador extends Controlador
{
    private $atSerieOcupacional;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atSerieOcupacional = $this->metCargarModelo('serieOcupacional','maestros');
        $this->atGrupoOcupacional = $this->metCargarModelo('grupoOcupacional','maestros');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        /*$this->atVista->assign('SerieOcupacional', $this->atSerieOcupacional->metConsultarSerieOcupacional());*/
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevaSerieOcupacional()
    {
        $idSerieOcupacional = $this->metObtenerInt('idSerieOcupacional');
        $valido             = $this->metObtenerInt('valido');

        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        if($valido==1){

            $this->metValidarToken();

            $formTxt = $this->metObtenerTexto('form','txt');
            $formInt = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }

            if($idSerieOcupacional===0){

                $validacion['status']='creacion';
                $id = $this->atSerieOcupacional->metRegistrarSerieOcupacional(
                    $validacion['fk_a006_num_miscelaneo_detalle_grupoocupacional'],
                    $validacion['ind_nombre_serie']
                );
                $validacion['idSerieOcupacional']=$id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atSerieOcupacional->metMostrarSerieOcupacional($id);
                    $validacion['ind_nombre_detalle'] = $datos['ind_nombre_detalle'];
                    $validacion['ind_nombre_serie'] = $datos['ind_nombre_serie'];
                    $validacion['idSerieOcupacional'] = $id;
                    echo json_encode($validacion);
                    exit;
                }


            }else{

                $validacion['status']='modificacion';

                $id = $this->atSerieOcupacional->metModificarSerieOcupacional($idSerieOcupacional,
                    $validacion['fk_a006_num_miscelaneo_detalle_grupoocupacional'],
                    $validacion['ind_nombre_serie']
                );

                $validacion['idSerieOcupacional']=$idSerieOcupacional;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atSerieOcupacional->metMostrarSerieOcupacional($id);
                    $validacion['ind_nombre_detalle'] = $datos['ind_nombre_detalle'];
                    $validacion['ind_nombre_serie'] = $datos['ind_nombre_serie'];
                    $validacion['idSerieOcupacional'] = $id;
                    echo json_encode($validacion);
                    exit;
                }


            }

        }

        if($idSerieOcupacional!=0){
            $db = $this->atSerieOcupacional->metMostrarSerieOcupacional($idSerieOcupacional);
            $this->atVista->assign('formDB',$db);
        }

        $GrupoOcupacional = $this->atGrupoOcupacional->metConsultarGrupoOcupacional();
        $this->atVista->assign('GrupoOcupacional',$GrupoOcupacional);
        $this->atVista->assign('idSerieOcupacional',$idSerieOcupacional);
        $this->atVista->metRenderizar('form','modales');

    }

    public function metEliminarSerieOcupacional()
    {
        $idSerieOcupacional = $this->metObtenerInt('idSerieOcupacional');
        $this->atSerieOcupacional->metEliminarSerieOcupacional($idSerieOcupacional);
        $array = array(
            'status' => 'OK',
            'idSerieOcupacional' => $idSerieOcupacional
        );

        echo json_encode($array);
    }


    public function metJsonSerieOcupacional()
    {

        $idGrupoOcupacional = $this->metObtenerInt('idGrupoOcupacional');
        $serie  = $this->atSerieOcupacional->metJsonSerieOcupacional($idGrupoOcupacional);
        echo json_encode($serie);
        exit;

    }

    #PERMITE LISTAR SERIE OCUPACIONAL A TRAVES DEL DATATABLES (JSON)
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
            SELECT
                rh_c010_serie.pk_num_serie,
                rh_c010_serie.ind_nombre_serie,
                rh_c010_serie.fk_a006_num_miscelaneo_detalle_grupoocupacional,
                a006_miscelaneo_detalle.pk_num_miscelaneo_detalle,
                a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
                rh_c010_serie
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c010_serie.fk_a006_num_miscelaneo_detalle_grupoocupacional
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    (
                      rh_c010_serie.ind_nombre_serie LIKE '%$busqueda[value]%' OR
                      a006_miscelaneo_detalle.ind_nombre_detalle LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_serie','ind_nombre_detalle','ind_nombre_serie');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_serie';
        #construyo el listado de botones
        if (in_array('RH-01-04-01-15-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" title="Editar" idSerieOcupacional="'.$clavePrimaria.'" descipcion="El Usuario a Modificado una Seria Ocupacional" titulo="Modificar Serie Ocupacional">
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('RH-01-04-01-16-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idSerieOcupacional="'.$clavePrimaria.'"  boton="si, Eliminar"
                            descipcion="El usuario a eliminado una Serie Ocupacional" title="Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Serie Ocupacional!!">
                            <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }

}
