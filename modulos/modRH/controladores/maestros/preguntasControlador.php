<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class preguntasControlador extends Controlador
{
    private $atPreguntas;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atPreguntas  = $this->metCargarModelo('preguntas','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $enc = null;
        $this->atVista->assign('enc',$enc);

        $this->atVista->assign('Preguntas', $this->atPreguntas->metConsultarPreguntas());
        $this->atVista->metRenderizar('listado');


    }

    public function metPreguntas()
    {
        $idPregunta = $this->metObtenerInt('idPregunta');
        $valido     = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }


            if($idPregunta===0){

                $validacion['status']='creacion';
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                $id = $this->atPreguntas->metRegistrarPreguntas(
                    $validacion['ind_descripcion_preguntas'],
                    $validacion['fk_a006_num_miscelaneo_detalle_area'],
                    $validacion['num_valor_minimo'],
                    $validacion['num_valor_maximo'],
                    $validacion['num_estatus']
                );
                $validacion['idPregunta'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                $this->atPreguntas->metModificarPreguntas($idPregunta,
                    $validacion['ind_descripcion_preguntas'],
                    $validacion['fk_a006_num_miscelaneo_detalle_area'],
                    $validacion['num_valor_minimo'],
                    $validacion['num_valor_maximo'],
                    $validacion['num_estatus']
                );
                $validacion['idPregunta']=$idPregunta;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idPregunta!=0){
            $db=$this->atPreguntas->metMostrarPreguntas($idPregunta);
            $this->atVista->assign('formDB',$db);
        }

        #PARA LLENAR SELECT AREA
        $Area = $this->atPreguntas ->metMostrarSelect('AREACLIMA');
        $this->atVista->assign('Area',$Area);

        $this->atVista->assign('idPregunta',$idPregunta);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar preguntas
    public function metEliminarPreguntas()
    {
        $idPregunta = $this->metObtenerInt('idPregunta');
        $this->atPreguntas->metEliminarPreguntas($idPregunta);
        $array = array(
            'status'  => 'OK',
            'idPregunta' => $idPregunta
        );
        echo json_encode($array);
    }

}
