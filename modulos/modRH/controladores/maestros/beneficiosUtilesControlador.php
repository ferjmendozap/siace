<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class beneficiosUtilesControlador extends Controlador
{
    private $atBeneficiosUtiles;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atBeneficiosUtiles  = $this->metCargarModelo('beneficiosUtiles','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('BenefeciosUtiles', $this->atBeneficiosUtiles->metConsultarBeneficiosUtiles());
        $this->atVista->metRenderizar('listado');


    }

    public function metBeneficiosUtiles()
    {
        $idBeneficiosUtiles = $this->metObtenerInt('idBeneficiosUtiles');
        $valido             = $this->metObtenerInt('valido');

        $complementoJs = array(
            'inputmask/jquery.inputmask.bundle.min'
        );

        $js = array ('Aplicacion/appFunciones','materialSiace/App',
            'materialSiace/core/demo/DemoFormComponents'
        );

        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_monto_asignado')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }


            if($idBeneficiosUtiles===0){

                $validacion['status']='creacion';

                $id = $this->atBeneficiosUtiles->metRegistrarBeneficiosUtiles(
                    $validacion['ind_descripcion_beneficio'],
                    $validacion['ind_periodo_ini'],
                    $validacion['ind_periodo_fin'],
                    $validacion['num_monto_asignado']
                );

                $validacion['idBeneficiosUtiles'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                $this->atBeneficiosUtiles->metModificarBeneficiosUtiles($idBeneficiosUtiles,
                    $validacion['ind_descripcion_beneficio'],
                    $validacion['ind_periodo_ini'],
                    $validacion['ind_periodo_fin'],
                    $validacion['num_monto_asignado']
                );
                $validacion['idBeneficiosUtiles']=$idBeneficiosUtiles;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idBeneficiosUtiles!=0){
            $db=$this->atBeneficiosUtiles->metMostrarBeneficiosUtiles($idBeneficiosUtiles);
            $this->atVista->assign('formDB',$db);
        }

        $aux  = date("Y");
        $anio = $aux+1;
        $this->atVista->assign('desde',$anio);
        $this->atVista->assign('idBeneficiosUtiles',$idBeneficiosUtiles);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar beneficios utiles
    public function metEliminarBeneficiosUtiles()
    {
        $idBeneficiosUtiles = $this->metObtenerInt('idBeneficiosUtiles');

        $this->atBeneficiosUtiles->metEliminarBeneficiosUtiles($idBeneficiosUtiles);

        $array = array(
            'status'  => 'OK',
            'idBeneficiosUtiles' => $idBeneficiosUtiles
        );

        echo json_encode($array);
    }

}
