<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class plantillasControlador extends Controlador
{
    private $atPlantillas;
    private $atPreguntas;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atPlantillas = $this->metCargarModelo('plantillas','maestros');
        $this->atPreguntas  = $this->metCargarModelo('preguntas','maestros');

    }

    #Metodo Index del controlador
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Plantillas', $this->atPlantillas->metConsultarPlantillas());
        $this->atVista->metRenderizar('listado');


    }

    public function metPlantillas()
    {
        $idPlantilla = $this->metObtenerInt('idPlantilla');
        $valido      = $this->metObtenerInt('valido');

        $enc = null;
        $this->atVista->assign('enc',$enc);

        $complementosCss = array(
            'multi-select/multi-select555c',
        );
        $complementosJs = array(
            'multi-select/jquery.multi-select',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }


            if($idPlantilla===0){

                $validacion['status']='creacion';
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['preguntas'])){
                    $validacion['preguntas']=NULL;
                }
                $id = $this->atPlantillas->metRegistrarPlantillas(
                    $validacion['ind_descripcion_plantilla'],
                    $validacion['preguntas'],
                    $validacion['num_estatus']
                );
                $validacion['idPlantilla'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';
                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }
                if(!isset($validacion['preguntas'])){
                    $validacion['preguntas']=NULL;
                }
                $this->atPlantillas->metModificarPlantillas($idPlantilla,
                    $validacion['ind_descripcion_plantilla'],
                    $validacion['preguntas'],
                    $validacion['num_estatus']
                );
                $validacion['idPlantilla']=$idPlantilla;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idPlantilla!=0){
            $db = $this->atPlantillas->metMostrarPlantillas($idPlantilla);
            $dbPreguntas = $this->atPlantillas->metMostrarPlantillasPreguntas($idPlantilla);
            if($dbPreguntas){
                for ($i = 0; $i < count($dbPreguntas); $i++) {
                    $dbPre[] = $dbPreguntas[$i]['fk_rhc074_num_preguntas'];
                }
            }else{
                    $dbPre=null;
            }

            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('formDBPreguntas',$dbPre);
        }

        $this->atVista->assign('listaPreguntas',$this->atPreguntas->metConsultarPreguntas());
        $this->atVista->assign('idPlantilla',$idPlantilla);
        $this->atVista->metRenderizar('form','modales');


    }


    #para eliminar plantilla
    public function metEliminarPlantillas()
    {
        $idPlantilla = $this->metObtenerInt('idPlantilla');

        $this->atPlantillas->metEliminarPlantillas($idPlantilla);

        $array = array(
            'status'  => 'OK',
            'idPlantilla' => $idPlantilla
        );

        echo json_encode($array);
    }

}
