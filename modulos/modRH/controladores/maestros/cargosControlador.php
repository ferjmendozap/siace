<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class cargosControlador extends Controlador
{
    private $atCargos;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atCargos                   = $this->metCargarModelo('cargos','maestros');
        $this->atTiposCargosModelo        = $this->metCargarModelo('tiposCargos','maestros');//se carga el modelo tipos de cargos para llenar el select
        $this->atNivelesTiposCargosModelo = $this->metCargarModelo('nivelesTiposCargos','maestros');//se carga el modelo niveles tipos cargo para llenar el select
        $this->atGrupoOcupacional         = $this->metCargarModelo('grupoOcupacional','maestros');
        $this->atSerieOcupacional         = $this->metCargarModelo('serieOcupacional','maestros');
        $this->atGradoSalarial            = $this->metCargarModelo('gradoSalarial','maestros');
        $this->atPasos                    = $this->metCargarModelo('pasos','maestros');
    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Cargos', $this->atCargos->metConsultarCargos());
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevoCargo()
    {
        $idCargo = $this->metObtenerInt('idCargo');
        $valido  = $this->metObtenerInt('valido');

        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        if($valido==1){

            $this->metValidarToken();

            $formTxt   =$this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form','alphaNum');


            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    if((strcmp($tituloInt,'num_sueldo_basico')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';

                }
            }

            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if(!empty($formAlphaNum[$tituloAlphaNum])){
                    $validacion[$tituloAlphaNum]=$valorAlphaNum;
                }else{
                    $validacion[$tituloAlphaNum]='';

                }
            }

            if($idCargo===0){

                $validacion['status']='creacion';
                $id = $this->atCargos->metRegistrarCargo(
                    $validacion['fk_rhc006_num_cargo'], $validacion['fk_rhc009_num_nivel'], $validacion['fk_a006_num_miscelaneo_detalle_grupoocupacional'],
                    $validacion['fk_rhc010_num_serie'], $validacion['ind_descripcion_cargo'], $validacion['txt_descripcion_gen'],
                    $validacion['fk_a006_num_miscelaneo_detalle_categoria'],$validacion['fk_rhc007_num_grado'],$validacion['fk_a006_num_miscelaneo_detalle_paso'],$validacion['num_sueldo_basico'],
                    $validacion['num_estatus']
                );


                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atCargos->metMostrarCargo($id);
                    $validacion['grupo_ocupacional'] = $datos['grupo_ocupacional'];
                    $validacion['ind_nombre_serie'] = $datos['ind_nombre_serie'];
                    $validacion['ind_descripcion_cargo'] = $datos['ind_descripcion_cargo'];
                    $validacion['idCargo'] = $id;
                    echo json_encode($validacion);
                    exit;
                }



            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['num_estatus'])){
                    $validacion['num_estatus']=0;
                }

                $id = $this->atCargos->metModificarCargo($idCargo,
                    $validacion['fk_rhc006_num_cargo'], $validacion['fk_rhc009_num_nivel'], $validacion['fk_a006_num_miscelaneo_detalle_grupoocupacional'],
                    $validacion['fk_rhc010_num_serie'], $validacion['ind_descripcion_cargo'], $validacion['txt_descripcion_gen'],
                    $validacion['fk_a006_num_miscelaneo_detalle_categoria'],$validacion['fk_rhc007_num_grado'],$validacion['fk_a006_num_miscelaneo_detalle_paso'],$validacion['num_sueldo_basico'],
                    $validacion['num_estatus']
                );

                $validacion['idCargo'] = $id;

                if(is_array($id)){

                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $id;
                    echo json_encode($validacion);
                    exit;

                }else{
                    $datos = $this->atCargos->metMostrarCargo($id);
                    $validacion['grupo_ocupacional'] = $datos['grupo_ocupacional'];
                    $validacion['ind_nombre_serie'] = $datos['ind_nombre_serie'];
                    $validacion['ind_descripcion_cargo'] = $datos['ind_descripcion_cargo'];
                    echo json_encode($validacion);
                    exit;
                }


            }


        }

        if($idCargo!=0){
            $db=$this->atCargos->metMostrarCargo($idCargo);
            $this->atVista->assign('formDB',$db);

            #PARA LLENAR EL SELECT GRADO SALARIAL SEGUN CATEGORIA DEL CARGO
            $Grado =  $this->atGradoSalarial->metMostrarGrado($db['fk_a006_num_miscelaneo_detalle_categoria']);
            $this->atVista->assign('Grado',$Grado);
            #************************************************************
            #PARA LLENAR EL SELECT DE NIVEL SEGUN TIPO DE CARGO
            $Nivel = $this->atNivelesTiposCargosModelo->metMostrarNivelesTipos($db['fk_rhc006_num_cargo']);
            $this->atVista->assign('Nivel',$Nivel);
            #************************************************************
            #PARA LLENAR EL SELECT SERIE OCUPACIONAL SEGUN GRUPO OCUPACIONAL
            $Serie = $this->atSerieOcupacional->metMostrarSerie($db['fk_a006_num_miscelaneo_detalle_grupoocupacional']);
            $this->atVista->assign('Serie',$Serie);
            #************************************************************
            $this->atVista->assign('default', 0);
        }else{
            $this->atVista->assign('default', 1);
        }

        #PARA LLENAR EL SELECT DE CATEGORIA DE CARGOS
        $Categoria = $this->atCargos->metMostrarSelect('CATCARGO');
        $this->atVista->assign('Categoria',$Categoria);
        #************************************************************

        #PARA LLENAR EL SELECT DE TIPOS DE CARGO
        $TipoCargo = $this->atTiposCargosModelo->metConsultarTiposCargos();
        $this->atVista->assign('TipoCargo',$TipoCargo);
        #************************************************************

        #PARA LLENAR EL SELECT GRUPO OCUPACIONAL
        $GrupoOcupacional = $this->atGrupoOcupacional->metConsultarGrupoOcupacional();
        $this->atVista->assign('GrupoOcupacional',$GrupoOcupacional);
        #************************************************************

        #PARA LLENAR EL SELECT PASOS
        $Pasos = $this->atPasos->metConsultarPasos();
        $this->atVista->assign('Pasos',$Pasos);
        #************************************************************

        $this->atVista->assign('idCargo',$idCargo);
        $this->atVista->metRenderizar('form','modales');


    }

    #para actualizar el select nivel de acuerdo al tipo de cargo
    public function metActualizarNivel()
    {
        #obtengo el valor pasado del formulario
        $TipoCargo = $this->metObtenerInt('TipoCargo');

        #conecto con el modelo y consulto los datos
        $datos = $this->atNivelesTiposCargosModelo->metMostrarNivelesTipos($TipoCargo);

        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html="";
        for($i=0;$i<sizeof($datos);$i++){
            $html.= "<option value=".$datos[$i]['pk_num_nivel'].">".$datos[$i]['ind_nombre_nivel']."</option>";
        }

        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;

    }


    #para actualizar el select SERIE OCUPACIONAL de acuerdo al grupo ocupacional seleccionado
    public function metActualizarSerie()
    {
        #obtengo el valor pasado del formulario
        $GrupoOcupacional = $this->metObtenerInt('GrupoOcupacional');

        #conecto con el modelo y consulto los datos
        $datos = $this->atSerieOcupacional->metMostrarSerie($GrupoOcupacional);

        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html="";
        for($i=0;$i<sizeof($datos);$i++){
            $html.= "<option value=".$datos[$i]['pk_num_serie'].">".$datos[$i]['ind_nombre_serie']."</option>";
        }

        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;

    }

    #para actualizar el select GRADO DEL CARGO de acuerdo a la categoria seleccionada
    public function metActualizarGrado()
    {
        #obtengo el valor pasado del formulario
        $Categoria = $this->metObtenerInt('Categoria');

        #conecto con el modelo y consulto los datos
        $datos = $this->atGradoSalarial->metMostrarGrado($Categoria);

        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html='<option value="">&nbsp;</option>';
        for($i=0;$i<sizeof($datos);$i++){
            $html.= "<option value=".$datos[$i]['pk_num_grado'].">".$datos[$i]['ind_nombre_grado']."</option>";
        }

        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;

    }

    #para mostrar el sueldo segun el grado seleccionado
    public function metActualizarSueldo()
    {
        #obtengo el valor pasado del formulario
        $Grado = $this->metObtenerInt('Grado');


        //verifico si el grado salarial tiene o no pasos
        $pasos = $this->atGradoSalarial->metVerificarPasos($Grado);
        $lpasos = $this->atPasos->metConsultarPasos();

        if($pasos['total']==0){//no tiene pasos

            $datos = $this->atGradoSalarial->metMostrarGradoSalarial($Grado);
            $sueldo = number_format($datos['num_sueldo_maximo'],2,',','.');
            $data = array(
                'P' => 0,
                'sueldo' => $sueldo,
                'pasos' => $lpasos
            );
            echo json_encode($data);

        }else{
            //tiene pasos
            //obtengo los pasos y armo el select con los pasos
            $datos = $this->atGradoSalarial->metPasosGradoSalarial($Grado);

            $data = array(
                'P' => 1,
                'pasos' => $datos
            );

            #los codifico con json para mostrarlos en la vista
            echo json_encode($data);
            exit;

        }

    }

    #para mostrar el sueldo segun el grado seleccionado
    public function metActualizarSueldoPasos()
    {
        #obtengo el valor pasado del formulario
        $Grado = $this->metObtenerInt('Grado');
        $Paso  = $this->metObtenerInt('Paso');


        $datos = $this->atGradoSalarial->metMostrarPasosSueldo($Grado,$Paso);

        $sueldo = number_format($datos['num_sueldo_maximo'],2,',','.');

        echo json_encode($sueldo);


    }

    #para mostrar el sueldo segun el grado seleccionado
    public function metActualizarSueldoCargoPasos()
    {
        #obtengo el valor pasado del formulario
        $Cargo = $this->metObtenerInt('Cargo');
        $Paso  = $this->metObtenerInt('Paso');

        $datos = $this->atGradoSalarial->metMostrarPasosCargoSueldo($Cargo,$Paso);

        $sueldo = number_format($datos['num_sueldo_maximo'],2,',','.');

        echo json_encode($sueldo);


    }

    #para eliminar un cargo o puestos
    public function metEliminarCargo()
    {
        $idCargo = $this->metObtenerInt('idCargo');
        $this->atCargos ->metEliminarCargo($idCargo);
        $array = array(
            'status'  => 'OK',
            'idCargo' => $idCargo
        );

        echo json_encode($array);
    }

    public function metJsonCargos()
    {

        $idGrupoOcupacional = $this->metObtenerInt('idGrupoOcupacional');
        $idSerieOcupacional = $this->metObtenerInt('idSerieOcupacional');
        $cargo  = $this->atCargos->metJsonCargos($idGrupoOcupacional,$idSerieOcupacional);
        echo json_encode($cargo);
        exit;

    }

    #PERMITE LISTAR LOS CARGOS A TRAVES DEL DATATABLES (JSON)
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
            SELECT
            rh_c063_puestos.pk_num_puestos,
            rh_c063_puestos.fk_rhc006_num_cargo,
            rh_c063_puestos.fk_rhc009_num_nivel,
            rh_c063_puestos.fk_a006_num_miscelaneo_detalle_grupoocupacional,
            rh_c063_puestos.fk_rhc010_num_serie,
            rh_c063_puestos.ind_descripcion_cargo,
            rh_c063_puestos.txt_descripcion_gen,
            rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria,
            rh_c063_puestos.fk_rhc007_num_grado,
            rh_c063_puestos.fk_a006_num_miscelaneo_detalle_paso,
            rh_c063_puestos.num_sueldo_basico,
            rh_c063_puestos.num_estatus,
            rh_c006_tipo_cargo.ind_nombre_cargo,
            rh_c009_nivel.ind_nombre_nivel,
            rh_c010_serie.ind_nombre_serie,
            misc1.ind_nombre_detalle AS grupo_ocupacional,
            rh_c007_grado_salarial.ind_nombre_grado,
            misc2.ind_nombre_detalle AS categoria
            FROM
            rh_c063_puestos
            INNER JOIN rh_c006_tipo_cargo ON rh_c063_puestos.fk_rhc006_num_cargo = rh_c006_tipo_cargo.pk_num_cargo
            INNER JOIN rh_c007_grado_salarial ON rh_c063_puestos.fk_rhc007_num_grado = rh_c007_grado_salarial.pk_num_grado
            INNER JOIN a006_miscelaneo_detalle as misc1 ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_grupoocupacional = misc1.pk_num_miscelaneo_detalle
            INNER JOIN rh_c009_nivel ON rh_c063_puestos.fk_rhc009_num_nivel = rh_c009_nivel.pk_num_nivel
            INNER JOIN rh_c010_serie ON rh_c063_puestos.fk_rhc010_num_serie = rh_c010_serie.pk_num_serie
            INNER JOIN a006_miscelaneo_detalle as misc2 ON rh_c063_puestos.fk_a006_num_miscelaneo_detalle_categoria = misc2.pk_num_miscelaneo_detalle
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                WHERE
                    (
                      misc1.ind_nombre_detalle LIKE '%$busqueda[value]%' OR
                      rh_c010_serie.ind_nombre_serie LIKE '%$busqueda[value]%' OR
                      rh_c063_puestos.ind_descripcion_cargo LIKE '%$busqueda[value]%'
                    )
            ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_puestos','grupo_ocupacional','ind_nombre_serie','ind_descripcion_cargo');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_puestos';
        #construyo el listado de botones
        if (in_array('RH-01-04-01-27-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" title="Editar" idCargo="'.$clavePrimaria.'" descipcion="El Usuario a Modificado un Cargo" titulo="Modificar Cargo">
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('RH-01-04-01-28-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" title="Eliminar" idCargo="'.$clavePrimaria.'"  boton="si, Eliminar"
                            descipcion="El usuario a eliminado un Cargo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Cargo!!">
                            <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }

}
