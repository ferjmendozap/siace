<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class pruebaControlador extends Controlador
{
    private $atPrueba;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atPrueba  = $this->metCargarModelo('prueba','maestros');

    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
		
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('Datos', $this->atPrueba->metConsultarDatos());
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevoDato()
    {
        $idPrueba = $this->metObtenerInt('idPrueba');
        $valido   = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='';
                }
            }

            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if(!empty($formTxt[$tituloTxt])){
                    $validacion[$tituloTxt]=$valorTxt;
                }else{
                    $validacion[$tituloTxt]='';
                }
            }


            if($idPrueba===0){

                $validacion['status']='creacion';

                $id = $this->atPrueba->metRegistrarDatos(
                    $validacion['nombres'],
                    $validacion['direccion']
                );

                $validacion['idPrueba'] = $id;
                echo json_encode($validacion);
                exit;


            }else{

                $validacion['status']='modificacion';

                if(!isset($validacion['ind_estatus'])){
                    $validacion['ind_estatus']=0;
                }

                $this->atCargos->metModificarCargo($idCargo,
                    $validacion['fk_rhc006_num_cargo'], $validacion['fk_rhc009_num_nivel'], $validacion['fk_rhc008_num_grupo'],
                    $validacion['fk_rhc010_num_serie'], $validacion['ind_descripcion_cargo'], $validacion['txt_descripcion_gen'],
                    $validacion['fk_a006_num_miscelaneo_detalle'],$validacion['fk_rhc007_num_grado'],$validacion['num_sueldo_basico'],
                    $validacion['ind_estatus']
                );
                $validacion['idCargo']=$idCargo;
                echo json_encode($validacion);
                exit;

            }


        }

        if($idCargo!=0){
            $db=$this->atCargos->metMostrarCargo($idCargo);
            $this->atVista->assign('formDB',$db);

            #PARA LLENAR EL SELECT GRADO SALARIAL SEGUN CATEGORIA DEL CARGO
            $Grado =  $this->atGradoSalarial->metMostrarGrado($db['fk_a006_num_miscelaneo_detalle']);
            $this->atVista->assign('Grado',$Grado);
            #************************************************************
            #PARA LLENAR EL SELECT DE NIVEL SEGUN TIPO DE CARGO
            $Nivel = $this->atNivelesTiposCargosModelo->metMostrarNivelesTipos($db['fk_rhc006_num_cargo']);
            $this->atVista->assign('Nivel',$Nivel);
            #************************************************************
            #PARA LLENAR EL SELECT SERIE OCUPACIONAL SEGUN GRUPO OCUPACIONAL
            $Serie = $this->atSerieOcupacional->metMostrarSerie($db['fk_rhc008_num_grupo']);
            $this->atVista->assign('Serie',$Serie);
            #************************************************************
        }

        #PARA LLENAR EL SELECT DE CATEGORIA DE CARGOS
        $Categoria = $this->atCargos->metMostrarSelect('CATCARGO');
        $this->atVista->assign('Categoria',$Categoria);
        #************************************************************

        #PARA LLENAR EL SELECT DE TIPOS DE CARGO
        $TipoCargo = $this->atTiposCargosModelo->metConsultarTiposCargos();
        $this->atVista->assign('TipoCargo',$TipoCargo);
        #************************************************************

        #PARA LLENAR EL SELECT GRUPO OCUPACIONAL
        $GrupoOcupacional = $this->atGrupoOcupacional->metConsultarGrupoOcupacional();
        $this->atVista->assign('GrupoOcupacional',$GrupoOcupacional);
        #************************************************************

        $this->atVista->assign('idCargo',$idCargo);
        $this->atVista->metRenderizar('form','modales');


    }



    #para eliminar un cargo o puestos
    public function metEliminarDatos()
    {
        $idPrueba = $this->metObtenerInt('idPrueba');

        $this->atPrueba ->metEliminarDatos($idPrueba);

        $array = array(
            'status'  => 'OK',
            'idPrueba' => $idPrueba
        );

        echo json_encode($array);
    }



}
