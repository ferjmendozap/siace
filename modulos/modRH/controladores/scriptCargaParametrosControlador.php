<?php

require_once 'datosCarga/dataBasicaInicialParametros.php';


class scriptCargaParametrosControlador extends Controlador
{

    use dataBasicaInicialParametros;

    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCarga = $this->metCargarModelo('scriptCarga');
    }

    public function metIndex()
    {
        echo "INICIANDO<br>";

        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE PARAMETROS<br>';

        $this->atScriptCarga->metCargarParametros($this->metDataParametros());


        echo "TERMINADO";
    }



    public function metRecursosHumanos()
    {

        echo "MAESTROS -> TIPOS DE CARGOS<br>";
        $this->atScriptCarga->metCargarMaestroTipoCargo($this->metDataTipoCargo());

        echo "MAESTROS -> NIVEL TIPOS DE CARGO<br>";
        $this->atScriptCarga->metCargarMaestroNivelTipoCargo($this->metDataNivelTiposCargo());

        echo "MAESTROS -> SERIE OCUPACIONAL<br>";
        $this->atScriptCarga->metCargarMaestroSerieOcupacional($this->metDataSerieOcupacional());

        echo "MAESTROS -> GRADO SALARIAL<br>";
        $this->atScriptCarga->metCargarMaestroGradoSalarial($this->metDataGradoSalarial());

        echo "MAESTROS -> CARGOS<br>";
        $this->atScriptCarga->metCargarMaestroCargos($this->metDataCargos());

        echo "MAESTROS -> NIVEL GRADOS DE INSTRUCCION<br>";
        $this->atScriptCarga->metCargarMaestroNivelGradoInstruccion($this->metDataNivelGradoInstruccion());

        echo "MAESTROS -> PROFESIONES<br>";
        $this->atScriptCarga->metCargarMaestroProfesiones($this->metDataProfesiones());

        echo "MAESTROS -> CENTROS DE ESTUDIOS<br>";
        $this->atScriptCarga->metCargarMaestroCentrosEstudios($this->metDataCentrosEstudios());

        echo "MAESTROS -> CURSOS<br>";
        $this->atScriptCarga->metCargarMaestroCursos($this->metDataCursos());

        echo "MAESTROS -> IDIOMAS<br>";
        $this->atScriptCarga->metCargarMaestroIdiomas($this->metDataIdiomas());

        echo "MAESTROS -> TIPOS DE CONTRATO<br>";
        $this->atScriptCarga->metCargarMaestroTiposContratos($this->metDataTipoContrato());

        echo "MAESTROS -> MOTIVOS DE CESE<br>";
        $this->atScriptCarga->metCargarMaestroMotivoCese($this->metDataMotivoCese());

        echo "MAESTROS -> HORARIO LABORAL<br>";
        $this->atScriptCarga->metCargarMaestroHorarioLaboral($this->metDataHorarioLaboral());
        $this->atScriptCarga->metCargarMaestroHorarioLaboralDetalle($this->metDataHorarioLaboralDetalle());

        echo "ORGANISMO<br>";
        $this->atScriptCarga->metCargarOrganismo($this->metDataOrganismo());

        echo "MAESTROS -> DEPENDENCIAS<br>";
        $this->atScriptCarga->metCargarDependencias($this->metDataDependencias());

        echo "CENTROS DE COSTOS<br>";
        $this->atScriptCarga->metCargarCentroCostoGrupo($this->metDataCentroCostoGrupo());
        $this->atScriptCarga->metCargarCentroCostoSubGrupo($this->metDataCentroCostoSubGrupo());
        $this->atScriptCarga->metCargarCentroCosto($this->metDataCentroCosto());

    }


}
