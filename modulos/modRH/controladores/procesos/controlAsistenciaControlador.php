<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class controlAsistenciaControlador extends Controlador
{

    private $atIdUsuario;
    private $atControlAsistencia;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atControlAsistencia = $this->metCargarModelo('controlAsistencia','procesos');


    }

    public function metIndex()
    {
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('transferir');
    }

    #PERMITE TRANSFERIR Y PROCESAR EL ARCHIVO DE ASISTENCIAS
    public function metTransferirAsistencia()
    {
        $tipo    = $this->metObtenerTexto('tipo');
        $destino = "publico/";

        error_reporting(E_ALL);
        set_time_limit(0);

        #cuando se seleciona el archivo
        if(strcmp($tipo,'TRANSFERIR')==0){

            if (isset($_FILES["archivo"]))
            {
                if (is_uploaded_file($_FILES['archivo']['tmp_name'])) {
                    copy($_FILES['archivo']['tmp_name'], $destino.$_FILES['archivo']['name']);
                    $subio = true;
                }
                else{
                    $subio = false;
                }

            }


        }

        #proceso el archivo (boton transferir eventos)
        if(strcmp($tipo,'PROCESAR')==0){

            $archivo = $this->metObtenerTexto('archivo');
            $tipoarc = explode(".",$archivo);

            if(file_exists($destino.$archivo)){
                #proceso el archivo
                $this->metObtenerLibreria('IOFactory', 'PHPExcel/Classes/PHPExcel');

                #dependiendo de la extension del archivo inicio el objeto
                if(strcmp($tipoarc[1],'xls')==0 || strcmp($tipoarc[1],'xlsx')==0){
                    $objReader = new PHPExcel_Reader_Excel5();
                }
                if(strcmp($tipoarc[1],'ods')==0){
                    $objReader = new PHPExcel_Reader_OOCalc();
                }

                $objPHPExcel = $objReader->load('publico/'.$archivo);

                //Asigno la hoja de calculo activa
                $objPHPExcel->setActiveSheetIndex(0);
                //Obtengo el numero de filas del archivo
                $numRows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();


                for ($i = 1; $i <= $numRows; $i++) {

                    //valido la fecha y hora del archivo antes de guardar en la bd
                    $fecha = $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue();
                    $hora  = $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue();

                    if(($this->metValidarFecha($fecha)==1) && ($this->metValidarHora($hora)==1)){


                        $informacion[] = array(
                            'fecha' => $objPHPExcel->getActiveSheet()->getCell('A'.$i)->getCalculatedValue(),
                            'hora' => $objPHPExcel->getActiveSheet()->getCell('B'.$i)->getCalculatedValue(),
                            'evento' => $objPHPExcel->getActiveSheet()->getCell('C'.$i)->getCalculatedValue(),
                            'cedula' => $objPHPExcel->getActiveSheet()->getCell('D'.$i)->getCalculatedValue()
                        );
                        $w=1;

                    }else{

                        $w = 0;//ERROR EN ARCHIVO

                    }


                }


                #si hay un error en el archivo
                if($w==0){
                    echo json_encode(2);
                }else{


                    #transfiero los eventos
                    $eventos = $this->atControlAsistencia->metTransferirEventos($informacion);

                    if($eventos==1){

                        unlink('publico/'.$archivo);

                        #procesar los eventos
                        $asistencia = $this->atControlAsistencia->metControlAsistencia();

                        if($asistencia==1){
                            echo json_encode($asistencia);
                        }else{
                            echo json_encode($asistencia);
                        }

                    }else{

                        echo json_encode($eventos);

                    }

                }

            }

        }

    }

    #eventos por procesar
    public function metEventosPorProcesar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );

        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App');
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('listado');
    }

    #data eventos por procesar
    public function metDataPorProcesar()
    {


        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #capturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
            SELECT
                    rh_c105_controlasistencia.pk_num_controlasistencia,
                    rh_c105_controlasistencia.fk_rhb001_num_empleado,
                    mp.pk_num_persona,
                    rh_c105_controlasistencia.ind_cedula_empleado,
                    rh_c105_controlasistencia.fec_fecha,
                    rh_c105_controlasistencia.fec_hora,
                    rh_c105_controlasistencia.fec_fecha_format,
                    rh_c105_controlasistencia.fec_hora_format,
                    rh_c105_controlasistencia.ind_evento,
                    rh_c105_controlasistencia.ind_estado,
                    CONCAT_WS(' ',mp.ind_nombre1,mp.ind_nombre2,mp.ind_apellido1,mp.ind_apellido2) AS nombre_empleado,
                    mp.ind_nombre1, mp.ind_nombre2,mp.ind_apellido1,mp.ind_apellido2
                FROM
                    rh_c105_controlasistencia
                INNER JOIN rh_b001_empleado AS me ON me.pk_num_empleado = rh_c105_controlasistencia.fk_rhb001_num_empleado
                INNER JOIN a003_persona AS mp ON mp.pk_num_persona = me.fk_a003_num_persona
                WHERE rh_c105_controlasistencia.ind_estado='S'
        ";
        if ($busqueda['value']) {
            #concateno la busqueda si existe, Nota: esto es obligatorio
            $sql .="
                      and
                      ind_cedula_empleado LIKE '%$busqueda[value]%' OR
                      ind_nombre1 LIKE '%$busqueda[value]%'

            ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_cedula_empleado','nombre_empleado','fec_fecha','fec_hora','ind_evento');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_controlasistencia';

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);


    }

    public function metProcesarEventos()
    {

        $procesar = $this->atControlAsistencia->metProcesarEventos();

        if($procesar==1){
            echo json_encode($procesar);
        }else{
            echo json_encode($procesar);
        }

    }

    #para validar formato correcto de la fecha en el archivo
    public function metValidarFecha($fecha)
    {
        if(preg_match('/^\d{1,2}\-\d{1,2}\-\d{4}$/', $fecha)==1){
            return 1;
        }
        elseif(preg_match('/^\d{1,2}\/\d{1,2}\/\d{4}$/', $fecha)==1){
            return 1;
        }
        else{
            return 0;
        }

    }

    #para validar formato correcto de la hora en el archivo
    public function metValidarHora($fecha)
    {
        if(preg_match('/^\d{2}\:\d{2}$/', $fecha)==1){
            return 1;
        }
        else{
            return 0;
        }

    }



}