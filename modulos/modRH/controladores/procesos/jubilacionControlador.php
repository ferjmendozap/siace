<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Gestión de JUbilación. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de jubilación solicitada por el empleado 
class jubilacionControlador extends Controlador
{
    private $atJubilacionModelo;

    public function __construct() 
    {
        parent::__construct();
        Session::metAcceso();
        $this->atJubilacionModelo = $this->metCargarModelo('jubilacion', 'procesos');
    }


    // Index del controlador
    public function metIndex()
    {

    }

    // Index del controlador
    public function metListarJubilacion($valor)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'dropzone//downloads/css/dropzone'
        );
        $js = array('materialSiace/core/demo/DemoTableDynamic');
        $datepickerJs[] = 'bootstrap-datepicker/bootstrap-datepicker';
        $dropzoneJs[]= 'dropzone/downloads/dropzone.min';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($datepickerJs);
        $this->atVista->metCargarJsComplemento($dropzoneJs);
        $this->atVista->metCargarJs($js);
        $validarjs= array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validarjs);
        // Datos para el Filtro de búsqueda
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atJubilacionModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
        $listadoOrganismo = $this->atJubilacionModelo->metListarOrganismo(0, 1);
        $datosEmpleado = $this->atJubilacionModelo->metEmpleado($pkNumEmpleado);
        $pkNumDependencia = $datosEmpleado['pk_num_dependencia'];
        $listadoEmpleados = $this->atJubilacionModelo->metDependenciaEmpleado($pkNumDependencia);
        $this->atVista->assign('empleado', $listadoEmpleados);
        $listadoDependencia = $this->atJubilacionModelo->metListarDependencia($datosEmpleado['pk_num_organismo'], 1, 0);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        // Organismo donde trabaja el empleado
        $empleado = array(
            'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
            'estado' => $valor
        );
        $this->atVista->assign('emp', $empleado);
        // Fin del filtro
        $listarJubilacion = $this->atJubilacionModelo->metListarJubilacion($valor);
        $this->atVista->assign('listarJubilacion', $listarJubilacion);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->metRenderizar('listado');
    }

    // Método que permite crear una nueva jubilación
    public function metNuevaJubilacion()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
        );
        $complementosJs = array(
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
        );
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $usuario = Session::metObtener('idUsuario');
        $valido = $this->metObtenerInt('valido');
        if (isset($valido) && $valido == 1) {
            $this->metValidarToken();
            $pkNumEmpleado = $_POST['pkNumEmpleado'];
            $pkNumOrganismo = $_POST['num_organismo'];
            $pkNumDependencia = $_POST['num_dependencia'];
            $cargo = $_POST['cargo'];
            $sueldoActual = $_POST['salario'];
            $edad = $_POST['edad'];
            $fechaIngreso = $_POST['fecha_ingreso'];
            $anioServicio = $_POST['anioServicio'];
            $anioExperiencia = $_POST['anioExperiencia'];
            $anioTotal = $_POST['anioTotal'];
            $sueldo = $_POST['sueldo'];
            $prima = $_POST['prima'];
            $total = $_POST['total'];
            $porcentaje = $_POST['porcentaje'];
            $coeficiente = $_POST['coeficiente'];
            $baseJubilacion = $_POST['baseJubilacion'];
            $montoJubilacion = $_POST['jubilacion'];
            $observacionPreparado = $_POST['observacion_preparado'];
            $tipoNomina = $_POST['tipo_nomina'];
            $tipoTrabajador = $_POST['tipo_trabajador'];
            // $estado= $_POST['estadoInactivo'];
            $motivoCese = $_POST['motivo_cese'];
            $fechaCese = $_POST['fecha_cese'];
            $explicacionCese = $_POST['explicacion_cese'];
            $usuario = Session::metObtener('idUsuario');
            $fecha_hora = date('Y-m-d H:i:s');
            $periodo = date('Y-m');
            $explodeIngreso = explode("/", $fechaIngreso);
            $fechaIngresoFormato = $explodeIngreso[2].'-'.$explodeIngreso[1].'-'.$explodeIngreso[0];
            $explodeCese = explode("/", $fechaCese);
            $fechaCeseFormato = $explodeCese[2].'-'.$explodeCese[1].'-'.$explodeCese[0];
            // Agrupo los datos comunes

            $datosJubilacion = array(
                $pkNumEmpleado,
                $tipoNomina,
                $tipoTrabajador,
                $motivoCese,
                $pkNumOrganismo,
                $pkNumDependencia,
                $cargo,
                $fechaIngresoFormato,
                $fechaCeseFormato,
                $periodo,
                $edad,
                $anioServicio,
                $anioExperiencia,
                $anioTotal,
                $explicacionCese,
                $usuario,
                $fecha_hora
            );
            $datosMontoJubilacion = array(
                $sueldoActual,
                $sueldo,
                $prima,
                $baseJubilacion,
                $porcentaje,
                $coeficiente,
                $montoJubilacion
            );
            // Obtengo los datos de Operacion
            $empleado = $this->atJubilacionModelo->metUsuario($usuario, 1);
            $funcionario = $empleado['pk_num_empleado'];
            $datosEmpleado = $this->atJubilacionModelo->metEmpleado($pkNumEmpleado);
            $datosOperacion = array(
                $observacionPreparado,
                $funcionario,
                $datosEmpleado['pk_num_puestos'],
                $fecha_hora,
            );
            $pkNumJubilacion = $this->atJubilacionModelo->metGuardarJubilacion($datosJubilacion, $datosMontoJubilacion, $datosOperacion);
            // Guardamos la relacion de sueldos
            // Sueldo Base
            $conceptoBase = Session::metObtener('CODSUELDOBAS');
            $consultarSueldo = $this->atJubilacionModelo->metConsultarSueldoBase($pkNumEmpleado, $conceptoBase);
            foreach ($consultarSueldo as $base){
                $periodo = $base['periodo'];
                $explodeBase = explode("-", $periodo);
                $anioBase = $explodeBase[0];
                $mesBase = $explodeBase[1];
                $monto = $base['monto'];
                $this->atJubilacionModelo->metGuardarRelacion($anioBase, $mesBase, $monto, $conceptoBase, $pkNumJubilacion);
            }
            // Prima por años de servicio
            $conceptoPrima = Session::metObtener('CODPRIANIOSERV');
            $consultarPrima = $this->atJubilacionModelo->metConsultarSueldoBase($pkNumEmpleado, $conceptoPrima);
            foreach ($consultarPrima as $prima){
                $periodo = $prima['periodo'];
                $explodeBase = explode("-", $periodo);
                $anioBase = $explodeBase[0];
                $mesBase = $explodeBase[1];
                $monto = $prima['monto'];
                $this->atJubilacionModelo->metGuardarRelacion($anioBase, $mesBase, $monto, $conceptoPrima, $pkNumJubilacion);
            }
            $consultarJubilacion = $this->atJubilacionModelo->metConsultarJubilacion($pkNumJubilacion);
            $jubilacion = array(
                'pk_num_jubilacion' => $consultarJubilacion['pk_num_jubilacion'],
                'pk_num_empleado' => $consultarJubilacion['pk_num_empleado'],
                'ind_cedula_documento' => $consultarJubilacion['ind_cedula_documento'],
                'ind_nombre1' => $consultarJubilacion['ind_nombre1'],
                'ind_nombre2' => $consultarJubilacion['ind_nombre2'],
                'ind_apellido1' => $consultarJubilacion['ind_apellido1'],
                'ind_apellido2' => $consultarJubilacion['ind_apellido2'],
                'ind_estado' => $consultarJubilacion['ind_estado']
            );
            echo json_encode($jubilacion);
            exit;
        }
        $empleado = $this->atJubilacionModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
        $datosEmpleado = $this->atJubilacionModelo->metEmpleado($pkNumEmpleado);
        $fechaActual = date('d/m/Y');
        $datosEmpleado = array(
            'pk_num_empleado' => $datosEmpleado['pk_num_empleado'],
            'ind_nombre1' => $datosEmpleado['ind_nombre1'],
            'ind_nombre2' => $datosEmpleado['ind_nombre2'],
            'ind_apellido1' => $datosEmpleado['ind_apellido1'],
            'ind_apellido2' => $datosEmpleado['ind_apellido2'],
            'fecha_actual' => $fechaActual
        );
        $tipoNomina = $this->atJubilacionModelo->metListarNomina();
        $tipoTrabajador = $this->atJubilacionModelo->metMostrarSelect('TIPOTRAB');
        $motivoCese = $this->atJubilacionModelo->metConsultarMotivoCese();
        $this->atVista->assign('emp', $datosEmpleado);
        $this->atVista->assign('tipoNomina', $tipoNomina);
        $this->atVista->assign('tipoTrabajador', $tipoTrabajador);
        $this->atVista->assign('motivoCese', $motivoCese);
        $this->atVista->metRenderizar('nuevaJubilacion', 'modales');
    }

    // Método que permite listar las dependencias del organismo
    public function metBuscarDependencia()
    {
        $pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
        $listarDependencia=$this->atJubilacionModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia" onchange="cargarEmpleado(this.value)"> <option value="" disabled selected>&nbsp;</option>';
        foreach($listarDependencia as $listarDependencia){
            $a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
        }
        $a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
        echo $a;
    }

    // Método que permite listar los empleados de una dependencia particular
    public function metBuscarEmpleado()
    {
        $pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
        $listarEmpleado=$this->atJubilacionModelo->metListarEmpleado($pkNumDependencia, 1);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_empleado" id="pk_num_empleado"><option value="">&nbsp;</option>';
        foreach($listarEmpleado as $listarEmpleado){
            $a .='<option value="'.$listarEmpleado['pk_num_empleado'].'">'.$listarEmpleado['ind_nombre1'].' '.$listarEmpleado['ind_nombre2'].' '.$listarEmpleado['ind_apellido1'].' '.$listarEmpleado['ind_apellido2'].'</option>';
        }
        $a .= '</select><label for="pk_num_empleado"><i class="glyphicon glyphicon-user"></i> Empleado</label></div>';
        echo $a;
    }

    // Método que permite buscar un empleado desde el wizard
    public function metConsultarEmpleado()
    {
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atJubilacionModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
        $listadoOrganismo = $this->atJubilacionModelo->metListarOrganismo(0, 1);
        $datosEmpleado = $this->atJubilacionModelo->metEmpleado($pkNumEmpleado);
        $pkNumDependencia = $datosEmpleado['pk_num_dependencia'];
        $listadoEmpleados = $this->atJubilacionModelo->metDependenciaEmpleado($pkNumDependencia);
        $this->atVista->assign('empleado', $listadoEmpleados);
        $listadoDependencia = $this->atJubilacionModelo->metListarDependencia($datosEmpleado['pk_num_organismo'], 1, 0);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        // Organismo donde trabaja el empleado
        $empleado = array(
            'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
            'pk_num_dependencia' => $pkNumDependencia
        );
        $datosTrabajador = $this->atJubilacionModelo->metBuscarTrabajador($datosEmpleado['pk_num_organismo'], $pkNumDependencia, '', '', '', '', 1, '', '');

        $centroCosto = $this->atJubilacionModelo->metListarCentroCosto($pkNumDependencia);
        $tipoNomina = $this->atJubilacionModelo->metListarNomina();
        $tipoTrabajador = $this->atJubilacionModelo->metMostrarSelect('TIPOTRAB');
        // Envío a la vista
        $this->atVista->assign('datosTrabajador', $datosTrabajador);
        $this->atVista->assign('centroCosto', $centroCosto);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('nomina', $tipoNomina);
        $this->atVista->assign('tipoTrabajador', $tipoTrabajador);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->metRenderizar('buscarEmpleado', 'modales');
    }

    // Método que permite listar empleados de acuerdo a la busqueda en nueva jubilación
    public function metBuscarTrabajador()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $pkNumDependencia = $_POST['pk_num_dependencia'];
        $centroCosto = $_POST['centro_costo'];
        $tipoNomina = $_POST['tipo_nomina_buscar'];
        $tipoTrabajador = $_POST['tipo_trabajador_buscar'];
        $estado_registro = $_POST['estado_registro'];
        $situacionTabajo = $_POST['situacion_trabajo'];
        $fecha_inicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $datosTrabajador = $this->atJubilacionModelo->metBuscarTrabajador($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $tipoTrabajador, $estado_registro, $situacionTabajo, $fecha_inicio, $fecha_fin);
        echo json_encode($datosTrabajador);
    }

    // Método que permite cargar a la solicitud el empleado seleccionado
    public function metCargarEmpleado()
    {
        $pkNumEmpleado = $_POST['pk_num_empleado'];
        $empleado = $this->atJubilacionModelo->metEmpleado($pkNumEmpleado);
        echo json_encode($empleado);
    }

    // Método que permite calcular el tiempo de servicio de un empleado
    public function metTiempoServicio($fechaIngreso, $fechaEgreso)
    {
        $fechaInicial = new DateTime($fechaIngreso);
        $fechaFinal = new DateTime($fechaEgreso);
        $interval = $fechaInicial->diff($fechaFinal);
        $datosServicio = array(
            'anio' => $interval->format('%y'),
            'mes' => $interval->format('%m'),
            'dia' => $interval->format('%d')
        );
        return $datosServicio;
    }

    // Método que permite calcular el tiempo de servicio de un empleadp
    public function metCalcularServicio()
    {
        $pkNumEmpleado = $_POST['pk_num_empleado'];
        $fechaEgreso = date('Y-m-d');
        $servicio = $this->atJubilacionModelo->metEmpleado($pkNumEmpleado);
        $nacimiento = $servicio['fec_nacimiento'];
        $sexo = $servicio['ind_nombre_detalle'];
        $edadNacimiento= $this->metTiempoServicio($nacimiento, $fechaEgreso);
        $edad = $edadNacimiento['anio'];
        $fechaIngreso = $servicio['fec_ingreso'];
        $tiempoServicio = $this->metTiempoServicio($fechaIngreso, $fechaEgreso);
        $anioServicio = $tiempoServicio['anio'];
        $antecedente = $this->atJubilacionModelo->metConsultarAntecedente($pkNumEmpleado);
        $acumuladorServicio = 0;
        foreach($antecedente as $ant){
            $fechaIngresoAnt = $ant['fec_ingreso'];
            $fechaEgresoAnt = $ant['fec_egreso'];
            $tiempoServicioAnt = $this->metTiempoServicio($fechaIngresoAnt, $fechaEgresoAnt);
            $serviciotiempo = $tiempoServicioAnt['anio'];
            $acumuladorServicio = $acumuladorServicio + $serviciotiempo;
        }
        $totalServicio = $acumuladorServicio + $anioServicio;
        $requisitos = $this->metRevisarRequisitos($totalServicio, $edad, $sexo);
        $datosServicio = array(
            'requisito' => $requisitos
        );
        echo json_encode($datosServicio);
    }



    // Método que permite verificar si el empleado cumple con los requisitos necesarios para solicitar la jubilacion
    public function metRevisarRequisitos($totalServicio, $edad, $sexo)
    {
        if($sexo=='MASCULINO'){
            $edadMasculino = Session::metObtener('EDADJUBM');
            if(($totalServicio>=25)&&($edad>=$edadMasculino)){
                $requisito = 1;
            } else {
                $requisito = 0;
            }
        } else {
            $edadFemenino = Session::metObtener('EDADJUBF');
            if(($totalServicio>=25)&&($edad>=$edadFemenino)){
                $requisito = 1;
            } else {
                $requisito = 0;
            }
        }
        return $requisito;
    }

    // Método que muestra el listado de antecedentes de servicio
    public function metGenerarServicio()
    {
        $pkNumEmpleado = $_POST['pk_num_empleado'];
       $this->metAntecedenteServicio($pkNumEmpleado, 1);
    }

    public function metAntecedenteServicio($pkNumEmpleado, $metodo)
    {
        $antecedente = $this->atJubilacionModelo->metConsultarAntecedente($pkNumEmpleado);
        $i = 0;
        $acumuladorServicio = 0;
        $contadorServicio = count($antecedente);
        if($contadorServicio>0){
            foreach($antecedente as $ant){
                $fechaIngresoAnt = $ant['fec_ingreso'];
                $fechaEgresoAnt = $ant['fec_egreso'];

                $fechaIngreso = $ant['fecha_ingreso'];
                $fechaEgreso = $ant['fecha_egreso'];

                $tiempoServicioAnt = $this->metTiempoServicio($fechaIngresoAnt, $fechaEgresoAnt);
                $anio = $tiempoServicioAnt['anio'];
                $mes = $tiempoServicioAnt['mes'];
                $dia = $tiempoServicioAnt['dia'];
                $acumuladorServicio = $acumuladorServicio + $anio;
                $empresa = $ant['ind_empresa'];
                $datosServicio[] = array (
                    'fechaIngreso' => $fechaIngreso,
                    'fechaEgreso' => $fechaEgreso,
                    'tiempoServicio' => $acumuladorServicio,
                    'empresa' => $empresa,
                    'dia' => $dia,
                    'mes'=> $mes,
                    'anio' => $anio
                );
                $i++;
            }
        } else {
            $datosServicio =array();
        }

        if($metodo==1){
            echo json_encode($datosServicio);
        } else {
            return $datosServicio;
        }
    }

    // Método que permite calcular el tiempo de servicio total
    public function metCalcularServicioTotal()
    {
        $pkNumEmpleado = $_POST['pk_num_empleado'];
        $this->metServicioTotal($pkNumEmpleado, 1);
    }

    public function metServicioTotal($pkNumEmpleado, $metodo)
    {
        $antecedente = $this->atJubilacionModelo->metConsultarAntecedente($pkNumEmpleado);
        $acumuladorAnio = 0;
        $acumuladorMes = 0;
        $acumuladorDia = 0;
        foreach($antecedente as $ant) {
            $fechaIngresoAnt = $ant['fec_ingreso'];
            $fechaEgresoAnt = $ant['fec_egreso'];
            $tiempoServicioAnt = $this->metTiempoServicio($fechaIngresoAnt, $fechaEgresoAnt);
            $anio = $tiempoServicioAnt['anio'];
            $mes = $tiempoServicioAnt['mes'];
            $dia = $tiempoServicioAnt['dia'];
            $acumuladorAnio = $acumuladorAnio + $anio;
            $acumuladorMes = $acumuladorMes + $mes;
            $acumuladorDia = $acumuladorDia + $dia;
        }
        // Calculo el total de antecedentes de servicio en la institucion
        $servicioEmpleado = $this->atJubilacionModelo->metEmpleado($pkNumEmpleado);
        $fechaIngreso = $servicioEmpleado['fec_ingreso'];
        $fechaActual = date('Y-m-d');
        $tiempoServicioInst = $this->metTiempoServicio($fechaIngreso, $fechaActual);
        $diaInst = $tiempoServicioInst['dia'];
        $mesInst = $tiempoServicioInst['mes'];
        $anioInst = $tiempoServicioInst['anio'];
        // Obtengo el total de servicio en cuanto a antecedente
        $totalAntecedente = $this->metObtenerServicio($acumuladorDia, $acumuladorMes, $acumuladorAnio);
        $diaAnt = $totalAntecedente['dia'];
        $mesAnt = $totalAntecedente['mes'];
        $anioAnt = $totalAntecedente['anio'];
        // Tiempo total de Servicio
        $diaTotalPrevio = $diaInst + $diaAnt;
        $mesTotalPrevio = $mesInst + $mesAnt;
        $anioTotalPrevio = $anioInst + $anioAnt;
        $total = $this->metObtenerServicio($diaTotalPrevio, $mesTotalPrevio, $anioTotalPrevio);
        $diaTotal = $total['dia'];
        $mesTotal = $total['mes'];
        $anioTotal = $total['anio'];
        // Si en el cálculo el mes es mayor que 8 meses entonces se toma como un año
        if ($mesTotal >= 8) {
            $anioTotal++;
            $mesTotal = 0;
            $diaTotal = 0;
        }
        // Array
        $datosServicio = array (
            'dia' => $diaAnt,
            'mes' => $mesAnt,
            'anio' => $anioAnt,
            'diaInst' => $diaInst,
            'mesInst' => $mesInst,
            'anioInst' => $anioInst,
            'diaTotal' => $diaTotal,
            'mesTotal' => $mesTotal,
            'anioTotal' => $anioTotal
        );
        if($metodo==1){
            echo json_encode($datosServicio);
        } else {
            return $datosServicio;
        }

    }

    // Método que se encarga de obtener el total de antecedentes
    public function metObtenerServicio($acumuladorDia, $acumuladorMes, $acumuladorAnio)
    {

        $_Dias_A_Meses = intval($acumuladorDia / 30);
        $acumuladorMes += $_Dias_A_Meses;
        $_Meses_A_Anios = intval($acumuladorMes / 12);
        $acumuladorAnio += $_Meses_A_Anios;
        $Dias  = $acumuladorDia - (intval($acumuladorDia / 30) * 30);
        $Meses = $acumuladorMes - (intval($acumuladorMes / 12) * 12);
        $Anios = $acumuladorAnio;
        $totalAntecedente = array(
            'anio' => $Anios,
            'mes' => $Meses,
            'dia' => $Dias
        );
        return $totalAntecedente;
    }

    // Método que permite generar la relación de sueldo de los ultimos 2 años del empleado
    public function metConsultarRelacionBase()
    {
        $pkNumEmpleado = $_POST['pk_num_empleado'];
        $idConcepto = Session::metObtener('CODSUELDOBAS');
        $consultarSueldo = $this->atJubilacionModelo->metConsultarSueldoBase($pkNumEmpleado, $idConcepto);
        echo json_encode($consultarSueldo);

    }

    // Método que permite generar la relación de primas por años de servicio de los ultimos 2 años del empleado
    public function metConsultarRelacionPrima()
    {
        $pkNumEmpleado = $_POST['pk_num_empleado'];
        $idConcepto = Session::metObtener('CODPRIANIOSERV');
        $consultarSueldo = $this->atJubilacionModelo->metConsultarSueldoBase($pkNumEmpleado, $idConcepto);
        echo json_encode($consultarSueldo);

    }

    // Método que permite calcular la jubilacion
    public function metCalcularJubilacion()
    {
        $pkNumEmpleado = $_POST['pk_num_empleado'];
        // Tiempo de servicio
        $antecedente = $this->atJubilacionModelo->metConsultarAntecedente($pkNumEmpleado);
        $acumuladorAnio = 0;
        $acumuladorMes = 0;
        $acumuladorDia = 0;
        foreach($antecedente as $ant) {
            $fechaIngresoAnt = $ant['fec_ingreso'];
            $fechaEgresoAnt = $ant['fec_egreso'];
            $tiempoServicioAnt = $this->metTiempoServicio($fechaIngresoAnt, $fechaEgresoAnt);
            $anio = $tiempoServicioAnt['anio'];
            $mes = $tiempoServicioAnt['mes'];
            $dia = $tiempoServicioAnt['dia'];
            $acumuladorAnio = $acumuladorAnio + $anio;
            $acumuladorMes = $acumuladorMes + $mes;
            $acumuladorDia = $acumuladorDia + $dia;
        }
        // Calculo el total de antecedentes de servicio en la institucion
        $servicioEmpleado = $this->atJubilacionModelo->metEmpleado($pkNumEmpleado);
        $fechaIngreso = $servicioEmpleado['fec_ingreso'];
        $fechaActual = date('Y-m-d');
        $tiempoServicioInst = $this->metTiempoServicio($fechaIngreso, $fechaActual);
        $diaInst = $tiempoServicioInst['dia'];
        $mesInst = $tiempoServicioInst['mes'];
        $anioInst = $tiempoServicioInst['anio'];
        // Obtengo el total de servicio en cuanto a antecedente
        $totalAntecedente = $this->metObtenerServicio($acumuladorDia, $acumuladorMes, $acumuladorAnio);
        $diaAnt = $totalAntecedente['dia'];
        $mesAnt = $totalAntecedente['mes'];
        $anioAnt = $totalAntecedente['anio'];
        // Tiempo total de Servicio
        $diaTotalPrevio = $diaInst + $diaAnt;
        $mesTotalPrevio = $mesInst + $mesAnt;
        $anioTotalPrevio = $anioInst + $anioAnt;
        $total = $this->metObtenerServicio($diaTotalPrevio, $mesTotalPrevio, $anioTotalPrevio);
        $mesTotal = $total['mes'];
        $anioTotal = $total['anio'];
        // Si en el cálculo el mes es mayor que 8 meses entonces se toma como un año
        if ($mesTotal >= 8) {
            $anioTotal++;
        }
        // Total de Sueldo Base y Total de prima por antecedente de servicio
        $conceptoBase = Session::metObtener('CODSUELDOBAS');
        $totalBase = $this->atJubilacionModelo->metConsultarSueldoBase($pkNumEmpleado, $conceptoBase);
        $acumuladorSueldoBase = 0;
        foreach($totalBase as $totalB){
            $acumuladorSueldoBase = $acumuladorSueldoBase + $totalB['monto'];
        }
        $conceptoPrima = Session::metObtener('CODPRIANIOSERV');
        $totalPrima = $this->atJubilacionModelo->metConsultarSueldoBase($pkNumEmpleado, $conceptoPrima);
        $acumuladorSueldoPrima = 0;
        foreach($totalPrima as $totalP){
            $acumuladorSueldoPrima =  $acumuladorSueldoPrima + $totalP['monto'];
        }
        // Calculos
        $obtenerParametro = Session::metObtener('COEPORCJUB');
        $total = $acumuladorSueldoBase + $acumuladorSueldoPrima;
        $sueldoBase = $total / 24;
        $porcentaje = $anioTotal * $obtenerParametro;
        $coeficiente = $porcentaje / 100;
        $montoJubilacion = $sueldoBase * $coeficiente;
        // Formato
        $totalSueldo = number_format($acumuladorSueldoBase, 2, ",", ".");
        $totalPrima =  number_format($acumuladorSueldoPrima, 2, ",", ".");
        $baseJubilacion = number_format($sueldoBase, 2, ",", ".");
        $montoJubilacionMostrar = number_format($montoJubilacion, 2, ",", ".");
        $total = number_format($total, 2, ",", ".");
        $datosJubilacion = array(
            'totalSueldoOriginal' => $acumuladorSueldoBase,
            'totalPrimaOriginal' => $acumuladorSueldoPrima,
            'baseJubilacionOriginal' => $sueldoBase,
            'porcentajeOriginal' => $porcentaje,
            'montoJubilacionOriginal' => $montoJubilacion,
            'totalOriginal' => $total,
            'coeficienteOriginal' => $coeficiente,
            'totalSueldo' => $totalSueldo,
            'totalPrima' => $totalPrima,
            'baseJubilacion' => $baseJubilacion,
            'porcentaje' => $porcentaje,
            'montoJubilacion' => $montoJubilacionMostrar,
            'total' => $total,
            'coeficiente' => $coeficiente
        );
        echo json_encode($datosJubilacion);
    }

    // Método que permite visualizar la jubilacion
    public function metVerJubilacion()
    {
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
        );
        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementosCss = array(
            'wizard/wizardfa6c'
        );
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $pkNumJubilacion = $_POST['pk_num_jubilacion'];
        $valorFormulario = $_POST['valorFormulario'];
        $this->metCargarJubilación($pkNumJubilacion, 'ver', $valorFormulario);
    }

    // Método que permite cargar los datos de la jubilacion
    public function metCargarJubilación($pkNumJubilacion, $vista, $valorFormulario)
    {
        $consultarJubilacion = $this->atJubilacionModelo->metConsultarJubilacion($pkNumJubilacion);
        $pkNumEmpleado = $consultarJubilacion['pk_num_empleado'];
        $this->atVista->assign('jub', $consultarJubilacion);
        //Antecedentes de servicio
        $datosServicio =  $this->metAntecedenteServicio($pkNumEmpleado, 2);
        $this->atVista->assign('servicio', $datosServicio);

        if (empty($datosServicio)) {
            $valorServicio = 0;
        } else {
            $valorServicio = 1;
        }
        $variableServicio = array('valorServicio' => $valorServicio);
        $this->atVista->assign('variableServicio', $variableServicio);
        // Servicio Total
        $datosServicio = $this->metServicioTotal($pkNumEmpleado, 2);
        $this->atVista->assign('total', $datosServicio);
        $conceptoBase = Session::metObtener('CODSUELDOBAS');
        $mostrarSueldoBase = $this->atJubilacionModelo->metMostrarConcepto($pkNumJubilacion, $conceptoBase);
        $this->atVista->assign('sueldoBase', $mostrarSueldoBase);
        $conceptoPrima = Session::metObtener('CODPRIANIOSERV');
        $mostrarSueldoPrima = $this->atJubilacionModelo->metMostrarConcepto($pkNumJubilacion, $conceptoPrima);
        $this->atVista->assign('sueldoPrima', $mostrarSueldoPrima);
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atJubilacionModelo->metUsuario($usuario, 1);
        $funcionario = $empleado['pk_num_empleado'];
        $datosEmpleado = $this->atJubilacionModelo->metEmpleado($funcionario);
        $empleadoNombre = $datosEmpleado['ind_nombre1'].' '.$datosEmpleado['ind_nombre2'].' '.$datosEmpleado['ind_apellido1'].' '.$datosEmpleado['ind_apellido2'];
        $nombre = array(
            'nombreEmpleado' => $empleadoNombre,
            'fechaActual' => date('d/m/Y')
        );
        $this->atVista->assign('nombre', $nombre);
        // Estatus aprobatorio
        $estatusPrevio = 'PR';
        $estatusPreparado = $this->atJubilacionModelo->metOperacionJubilacion($pkNumJubilacion, $estatusPrevio);
        $procesadoPor = $estatusPreparado['ind_nombre1'].' '.$estatusPreparado['ind_nombre2'].' '.$estatusPreparado['ind_apellido1'].' '.$estatusPreparado['ind_apellido2'];
        $estatusJubilacionPreparado = array(
            'ind_estado'  => 'PREPARADO',
            'fecha_operacion_preparado' => $estatusPreparado['fec_operacion'],
            'procesadoPor' => $procesadoPor,
            'ind_observacion' => $estatusPreparado['ind_observacion']
        );
        $this->atVista->assign('estatusPreparado', $estatusJubilacionPreparado);
        // Estatus conformado
        $estatusPrevio = 'CO';
        $estatusConformado = $this->atJubilacionModelo->metOperacionJubilacion($pkNumJubilacion, $estatusPrevio);
        $procesadoConformado = $estatusConformado['ind_nombre1'].' '.$estatusConformado['ind_nombre2'].' '.$estatusConformado['ind_apellido1'].' '.$estatusConformado['ind_apellido2'];
        $estatusJubilacionConformado = array(
            'ind_estado'  => 'CONFORMADO',
            'fecha_operacion_conformado' => $estatusConformado['fec_operacion'],
            'procesadoPor' => $procesadoConformado,
            'ind_observacion' => $estatusConformado['ind_observacion']
        );
        $this->atVista->assign('estatusConformado', $estatusJubilacionConformado);
        // Estatus Aprobado
        $estatusPrevio = 'AP';
        $estatusAprobado = $this->atJubilacionModelo->metOperacionJubilacion($pkNumJubilacion, $estatusPrevio);
        $procesadoAprobado = $estatusAprobado['ind_nombre1'].' '.$estatusAprobado['ind_nombre2'].' '.$estatusAprobado['ind_apellido1'].' '.$estatusAprobado['ind_apellido2'];
        $estatusJubilacionAprobado = array(
            'ind_estado'  => 'APROBADO',
            'fecha_operacion_aprobado' => $estatusAprobado['fec_operacion'],
            'procesadoPor' => $procesadoAprobado,
            'ind_observacion' => $estatusAprobado['ind_observacion']
        );
        $this->atVista->assign('estatusAprobado', $estatusJubilacionAprobado);
        $tipoNomina = $this->atJubilacionModelo->metListarNomina();
        $tipoTrabajador = $this->atJubilacionModelo->metMostrarSelect('TIPOTRAB');
        $motivoCese = $this->atJubilacionModelo->metConsultarMotivoCese();
        $this->atVista->assign('tipoNomina', $tipoNomina);
        $this->atVista->assign('tipoTrabajador', $tipoTrabajador);
        $this->atVista->assign('motivoCese', $motivoCese);
        $fechaActual = date('d/m/Y');
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atJubilacionModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
        $datosEmpleado = $this->atJubilacionModelo->metEmpleado($pkNumEmpleado);
        $datosUsuario = array(
            'nombreUsuario' => $datosEmpleado['ind_nombre1'].' '.$datosEmpleado['ind_apellido1'],
            'fechaActual' => $fechaActual,
            'valorFormulario' => $valorFormulario
        );
        $this->atVista->assign('datosUsuario',  $datosUsuario);
        $this->atVista->metRenderizar($vista, 'modales');
    }

    // Método que permite establecer los estatus aprobatorios de una jubilacion
    public function metEstatus()
    {
        $pkNumJubilacion = $_POST['pk_num_jubilacion'];
        $valor = $_POST['valor'];
        $ind_observacion = $_POST['ind_observacion'];
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atJubilacionModelo->metUsuario($usuario, 1);
        $funcionario = $empleado['pk_num_empleado'];
        $cargo = $this->atJubilacionModelo->metEmpleado($funcionario);
        $cargoEmpleado = $cargo['pk_num_puestos'];
        $fecha_hora = date('Y-m-d H:i:s');
        $this->atJubilacionModelo->metCambiarEstatus($pkNumJubilacion, $valor, $ind_observacion, $funcionario, $cargoEmpleado, $fecha_hora);
        $consultarJubilacion = $this->atJubilacionModelo->metConsultarJubilacion($pkNumJubilacion);
        $jubilacion = array(
            'pk_num_jubilacion' => $consultarJubilacion['pk_num_jubilacion'],
            'pk_num_empleado' => $consultarJubilacion['pk_num_empleado'],
            'ind_cedula_documento' => $consultarJubilacion['ind_cedula_documento'],
            'ind_nombre1' => $consultarJubilacion['ind_nombre1'],
            'ind_nombre2' => $consultarJubilacion['ind_nombre2'],
            'ind_apellido1' => $consultarJubilacion['ind_apellido1'],
            'ind_apellido2' => $consultarJubilacion['ind_apellido2'],
            'ind_estado' => $consultarJubilacion['ind_estado']
        );
        echo json_encode($jubilacion);
    }

    // Método que permite buscar mediante el filtro del listado principal
    public function metBuscarJubilacion()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $pkNumDependencia = $_POST['pk_num_dependencia'];
        $pkNumEmpleado = $_POST['pk_num_empleado'];
        $indCedulaDocumento = $_POST['ind_cedula_documento'];
        $txtEstatus = $_POST['ind_estado'];
        $buscarJubilacion = $this->atJubilacionModelo->metBuscarJubilacion($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $indCedulaDocumento, $txtEstatus);
        echo json_encode($buscarJubilacion);
    }

    // Método que permite generar un reporte con los datos de la búsqueda realizada
    public function metGenerarReporteJubilacion()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $pkNumOrganismo = $_GET['pk_num_organismo'];
        $pkNumDependencia = $_GET['pk_num_dependencia'];
        $pkNumEmpleado = $_GET['pk_num_empleado'];
        $indCedulaDocumento = $_GET['ind_cedula_documento'];
        $txtEstatus = $_GET['ind_estado'];
        $this->metObtenerLibreria('cabeceraJubilacion', 'modRH');
        $pdf = new pdfGeneral('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 2);
        $buscarJubilacion = $this->atJubilacionModelo->metBuscarJubilacion($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $indCedulaDocumento, $txtEstatus);
        $pdf->SetWidths(array(25, 60, 30, 86, 30, 30));
        foreach ($buscarJubilacion as $jubilacion){
            $funcionario = $jubilacion['ind_nombre1'].' '.$jubilacion['ind_nombre2'].' '.$jubilacion['ind_apellido1'].' '.$jubilacion['ind_apellido2'];
            $montoJubilacionMostrar = number_format($jubilacion['num_monto_jubilacion'], 2, ",", ".");
            $cedula = number_format($jubilacion['ind_cedula_documento'], 0, "", ".");
            $pdf->Row(array(
                $jubilacion['pk_num_jubilacion'],
                utf8_decode($jubilacion['ind_dependencia']),
                $jubilacion['pk_num_empleado'],
                utf8_decode($funcionario),
                $cedula,
                $montoJubilacionMostrar
            ), 5);
        }
        $pdf->Output();
    }

    public function metEditarJubilacion()
    {
        $pkNumJubilacion = $_POST['pk_num_jubilacion'];
        $valido = $this->metObtenerInt('valido');
        if (isset($valido) && $valido == 1) {
            $tipoNomina = $_POST['tipo_nomina'];
            $tipoTrabajador = $_POST['tipo_trabajador'];
            $motivoCese = $_POST['motivo_cese'];
            $fechaCese = $_POST['fecha_cese'];
            $explicacionCese = $_POST['explicacion_cese'];
            $usuario = Session::metObtener('idUsuario');
            $fecha_hora = date('Y-m-d H:i:s');
            $explodeCese = explode("/", $fechaCese);
            $fechaCeseFormato = $explodeCese[2].'-'.$explodeCese[1].'-'.$explodeCese[0];
            $this->atJubilacionModelo->metEditarJubilacion($pkNumJubilacion, $tipoNomina, $tipoTrabajador, $motivoCese, $explicacionCese, $fechaCeseFormato, $usuario, $fecha_hora);
            $consultarJubilacion = $this->atJubilacionModelo->metConsultarJubilacion($pkNumJubilacion);
            $jubilacion = array(
                'pk_num_jubilacion' => $consultarJubilacion['pk_num_jubilacion'],
                'pk_num_empleado' => $consultarJubilacion['pk_num_empleado'],
                'ind_cedula_documento' => $consultarJubilacion['ind_cedula_documento'],
                'ind_nombre1' => $consultarJubilacion['ind_nombre1'],
                'ind_nombre2' => $consultarJubilacion['ind_nombre2'],
                'ind_apellido1' => $consultarJubilacion['ind_apellido1'],
                'ind_apellido2' => $consultarJubilacion['ind_apellido2'],
                'ind_estado' => $consultarJubilacion['ind_estado']
            );
            echo json_encode($jubilacion);
            exit;
        }
        if (!empty($pkNumJubilacion)) {
            $js = array(
                'materialSiace/core/demo/DemoFormWizard',
                'materialSiace/core/demo/DemoFormComponents',
            );
            $complementosJs = array(
                'wizard/jquery.bootstrap.wizard.min'
            );
            $complementosCss = array(
                'wizard/wizardfa6c'
            );
            $valorFormulario = $_POST['valorFormulario'];
            $this->atVista->metCargarJs($js);
            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarCssComplemento($complementosCss);
            $tipoNomina = $this->atJubilacionModelo->metListarNomina();
            $tipoTrabajador = $this->atJubilacionModelo->metMostrarSelect('TIPOTRAB');
            $motivoCese = $this->atJubilacionModelo->metConsultarMotivoCese();
            $this->atVista->assign('tipoNomina', $tipoNomina);
            $this->atVista->assign('tipoTrabajador', $tipoTrabajador);
            $this->atVista->assign('motivoCese', $motivoCese);
            $this->metCargarJubilación($pkNumJubilacion, 'editar', $valorFormulario);
        }
    }

    // Método que permite listar el tiempo de servicio en otras instituciones
    public function metConsultarServicio($pkNumEmpleado)
    {
        $antecedente = $this->atJubilacionModelo->metConsultarAntecedente($pkNumEmpleado);
        $i = 0;
        $acumuladorServicio = 0;
        foreach($antecedente as $ant){
            $fechaIngresoAnt = $ant['fec_ingreso'];
            $fechaEgresoAnt = $ant['fec_egreso'];

            $fechaIngreso = $ant['fecha_ingreso'];
            $fechaEgreso = $ant['fecha_egreso'];

            $tiempoServicioAnt = $this->metTiempoServicio($fechaIngresoAnt, $fechaEgresoAnt);
            $anio = $tiempoServicioAnt['anio'];
            $mes = $tiempoServicioAnt['mes'];
            $dia = $tiempoServicioAnt['dia'];
            $acumuladorServicio = $acumuladorServicio + $anio;
            $empresa = $ant['ind_empresa'];
            $datosServicio[] = array (
                'fechaIngreso' => $fechaIngreso,
                'fechaEgreso' => $fechaEgreso,
                'tiempoServicio' => $acumuladorServicio,
                'empresa' => $empresa,
                'dia' => $dia,
                'mes'=> $mes,
                'anio' => $anio
            );
            $i++;
        }
        return $datosServicio;
    }

    // Método que permite generar un reporte con los datos de la búsqueda realizada
    public function metReporteJubilacion()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $pkNumJubilacion = $_GET['pk_num_jubilacion'];
        $this->metObtenerLibreria('cabeceraJubilacion', 'modRH');
        $pdf = new pdfReporteDetallado('P', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 2);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetTextColor(0, 0, 0);
        $jubilacion = $this->atJubilacionModelo->metConsultarJubilacion($pkNumJubilacion);
        $pdf->SetFillColor(235, 235, 235);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(190, 8, utf8_decode('I. DATOS DE JUBILACIÓN'), 1, 1, 'L', 1);
        $pdf->SetFillColor(255, 255, 255);

        $y = $pdf->GetY();
        $pdf->Rect(10, $y, 90, 10, "DF");
        $pdf->Rect(100, $y, 25, 10, "DF");
        $pdf->Rect(125, $y, 25, 10, "DF");
        $pdf->Rect(150, $y, 25, 10, "DF");
        $pdf->Rect(175, $y, 25, 10, "DF");
        $pdf->SetY($y);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(90, 4, 'Apellidos y Nombres', 0, 0, 'L');
        $pdf->Cell(25, 4, 'Nro. Doc.', 0, 0, 'C');
        $pdf->Cell(25, 4, 'Fecha Nac.', 0, 0, 'C');
        $pdf->Cell(25, 4, 'Sexo', 0, 0, 'C');
        $pdf->Cell(25, 4, 'Edad', 0, 1, 'C');
        $pdf->SetY($y+4);
        $pdf->SetFont('Arial', '', 8);
        $explodeNacimiento = explode("-", $jubilacion['fec_nacimiento']);
        $fechaNacimiento = $explodeNacimiento[2].'/'.$explodeNacimiento[1].'/'.$explodeNacimiento[0];
        $pdf->Cell(90, 5, utf8_decode($jubilacion['ind_apellido1'].' '.$jubilacion['ind_apellido2'].' '.$jubilacion['ind_nombre1'].' '.$jubilacion['ind_nombre2']), 0, 0, 'L');
        $pdf->Cell(25, 5, $jubilacion['ind_cedula_documento'], 0, 0, 'C');
        $pdf->Cell(25, 5, $fechaNacimiento , 0, 0, 'C');
        $pdf->Cell(25, 5, $jubilacion['ind_nombre_detalle'], 0, 0, 'C');
        $pdf->Cell(25, 5,$jubilacion['num_edad'], 0, 1, 'C');
        $y = $pdf->GetY();
        $pdf->Rect(10, $y, 155, 10, "DF");
        $pdf->Rect(165, $y, 35, 10, "DF");
        $pdf->SetY($y);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(155, 4, 'Dependencia', 0, 0, 'L');
        $pdf->Cell(35, 4, 'Fecha de Ingreso', 0, 1, 'C');
        $pdf->SetY($y+4);
        $pdf->SetFont('Arial', '', 8);
        $explodeIngreso = explode("-", $jubilacion['fec_ingreso']);
        $fechaIngreso = $explodeIngreso[2].'/'.$explodeIngreso[1].'/'.$explodeIngreso[0];
        $pdf->Cell(155, 5, utf8_decode($jubilacion['ind_dependencia']), 0, 0, 'L');
        $pdf->Cell(35, 5, $fechaIngreso, 0, 1, 'C');

        $y = $pdf->GetY();
        $pdf->Rect(10, $y, 155, 10, "DF");
        $pdf->Rect(165, $y, 35, 10, "DF");
        $pdf->SetY($y);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(155, 4, 'Cargo Actual', 0, 0, 'L');
        $pdf->Cell(35, 4, 'Nivel Salarial', 0, 1, 'C');
        $pdf->SetY($y+4);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(155, 5, utf8_decode($jubilacion['ind_descripcion_cargo']), 0, 0, 'L');
        $pdf->Cell(35, 5, number_format($jubilacion['num_ultimo_sueldo'], 2, ',', '.'), 0, 1, 'C');
        $pdf->Ln(10);
        //	Imprimo el tiempo de servicio...
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetTextColor(0, 0, 0);

        $pdf->SetFillColor(235, 235, 235);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(190, 8, 'II. TIEMPO DE SERVICIO', 1, 1, 'L', 1);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetWidths(array(105, 20, 20, 15, 15, 15));
        $pdf->SetAligns(array('L', 'C', 'C', 'C', 'C', 'C'));
        $pdf->Row(array('Organismo', 'Ingreso', 'Egreso', utf8_decode('Años'), 'Meses', 'Dias'));
        $pdf->SetFont('Arial', '', 8);
        $pkNumEmpleado= $jubilacion['pk_num_empleado'];
        $servicio = $this->metConsultarServicio($pkNumEmpleado);
        foreach ($servicio as $serv){
            $pdf->Row(array($serv['empresa'], $serv['fechaIngreso'], $serv['fechaEgreso'], $serv['anio'], $serv['mes'], $serv['dia']));
        }
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetWidths(array(145, 15, 15, 15));
        $pdf->SetAligns(array('R', 'C', 'C', 'C', 'C', 'C'));
        $total = $this->metServicioTotal($pkNumEmpleado, 2);
        $pdf->Row(array('SUB-TOTAL ANTIGUEDAD', $total['anio'], $total['mes'], $total['dia']));
        $pdf->Row(array('TOTAL ANTIGUEDAD', $total['anioTotal'], $total['mesTotal'], $total['diaTotal']));
        $pdf->Ln(10);
//	---------------------------------------------------

//	Imprimo Caculo de Jubilacion...
        $pdf->SetFillColor(235, 235, 235);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(190, 8, utf8_decode('III. CÁLCULO DE JUBILACIÓN'), 1, 1, 'L', 1);
        $pdf->SetFillColor(255, 255, 255);

        $y = $pdf->GetY();
        $pdf->Rect(10, $y, 60, 10, "DF");
        $pdf->Rect(70, $y, 60, 10, "DF");
        $pdf->Rect(130, $y, 70, 10, "DF");
        $pdf->SetY($y);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(60, 4, 'Coeficiente', 0, 0, 'C');
        $pdf->Cell(60, 4, 'Porcentaje', 0, 0, 'C');
        $pdf->Cell(70, 4, utf8_decode('Salario Jubilación'), 0, 1, 'C');
        $pdf->SetY($y+4);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(60, 5, $jubilacion['num_coeficiente'], 0, 0, 'C');
        $pdf->Cell(60, 5, $jubilacion['num_porcentaje'], 0, 0, 'C');
        $pdf->Cell(70, 5, number_format($jubilacion['num_monto_jubilacion'], 2, ',', '.'), 0, 1, 'C');
// Estatus aprobatorio
        $estatusPrevio = 'PR';
        $estatusPreparado = $this->atJubilacionModelo->metOperacionJubilacion($pkNumJubilacion, $estatusPrevio);
        $procesadoPor = $estatusPreparado['ind_nombre1'].' '.$estatusPreparado['ind_nombre2'].' '.$estatusPreparado['ind_apellido1'].' '.$estatusPreparado['ind_apellido2'];
        // Estatus Aprobado
        $estatusPrevio = 'AP';
        $estatusAprobado = $this->atJubilacionModelo->metOperacionJubilacion($pkNumJubilacion, $estatusPrevio);
        $procesadoAprobado = $estatusAprobado['ind_nombre1'].' '.$estatusAprobado['ind_nombre2'].' '.$estatusAprobado['ind_apellido1'].' '.$estatusAprobado['ind_apellido2'];
        $y = $pdf->GetY();
        $pdf->Rect(10, $y, 190, 15, "DF");
        $pdf->SetY($y);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(190, 4, 'Observaciones', 0, 1, 'L');
        $pdf->SetY($y+4);
        $pdf->SetFont('Arial', '', 8);
        $pdf->MultiCell(190, 5, $estatusAprobado['ind_observacion'], 0, 'L');
        $pdf->Ln(15);
//	---------------------------------------------------

//	Imprimo funcionario responsable...
        $pdf->SetFillColor(235, 235, 235);
        $pdf->SetFont('Arial', 'B', 9);
        $pdf->Cell(190, 8, utf8_decode('IV. FUNCIONARIO RESPONSABLE DE LA INFORMACIÓN'), 1, 1, 'L', 1);
        $pdf->SetFillColor(255, 255, 255);
        $y = $pdf->GetY();
        $pdf->Rect(10, $y, 60, 15, "DF");
        $pdf->Rect(70, $y, 60, 15, "DF");
        $pdf->Rect(130, $y, 70, 15, "DF");
        $pdf->SetY($y);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(60, 4, 'Procesado Por', 0, 0, 'L');
        $pdf->Cell(60, 4, 'Aprobado Por', 0, 0, 'L');
        $pdf->Cell(70, 4, utf8_decode('Dirección de Recursos Humanos'), 0, 1, 'L');
        $pdf->SetY($y+4);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(60, 5,utf8_decode($procesadoPor), 0, 0, 'L');
        $pdf->Cell(60, 5, utf8_decode($procesadoAprobado), 0, 0, 'L');
        $pdf->Cell(70, 5, '', 0, 0, 'L');
        $pdf->Ln(10);
        $pdf->Output();
    }
	
	// Método que permite verificar si un empleado ya tiene una jubilacion
	public function metComprobarEmpleado()
	{
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$empleadoComprobar = $this->atJubilacionModelo->metComprobarEmpleado($pkNumEmpleado);
        $contador = count($empleadoComprobar);
        if($contador>0){
            foreach($empleadoComprobar as $emp){
                $estatus = $emp['ind_estado'];
            }
            if($estatus=='AN'){
                $valor = 0;
            } else {
                $valor = 1;
            }
        } else {
            $valor = 0;
        }
        $valorComprobar = array(
            "valor" => $valor
        );
		echo json_encode($valorComprobar);	
	}

    // Método que permite listar las dependencias del organismo mediante el listado de busqueda
    public function metBuscarDependenciaListado()
    {
        $pkNumOrganismo = $this->metObtenerInt('listado_organismo');
        $listarDependencia=$this->atJubilacionModelo->metListarDependencia($pkNumOrganismo, 1, 0); 
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="listado_dependencia" id="listado_dependencia" onchange="cargarCentroListado(this.value)"> <option value="" disabled selected>&nbsp;</option>';
        foreach($listarDependencia as $listarDependencia){
            $a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
        }
        $a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
        echo $a;
    }

    // Método que permite listar los centros de costos asociados a una dependencia
    public function metBuscarCentroListado()
    {
        $pkNumDependencia = $this->metObtenerInt('listado_dependencia');
        $listarCentroCosto=$this->atJubilacionModelo->metListarCentroCosto($pkNumDependencia);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="centro_costo" id="centro_costo"> <option value="" disabled selected>&nbsp;</option>';
        foreach($listarCentroCosto as $listarCentroCosto){
            $a .='<option value="'.$listarCentroCosto['pk_num_centro_costo'].'">'.$listarCentroCosto['ind_descripcion_centro_costo'].'</option>';
        }
        $a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Centro Costo</label></div>';
        echo $a;
    }
}
