<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-02-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/
class ceseReingresoControlador extends Controlador
{
    private $atEmpleados;
    private $atIdUsuario;
    private $atCeseReingreso;
    private $atOrganismo;
    private $atDependencias;
    private $atTipoNimina;
    private $atMotivoCese;
    private $atCargo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados    = $this->metCargarModelo('empleados','gestion');
        $this->atCeseReingreso= $this->metCargarModelo('ceseReingreso','procesos');
        $this->atOrganismo    = $this->metCargarModelo('organismo','maestros','modCV');
        $this->atDependencias = $this->metCargarModelo('dependencias','maestros');
        $this->atMotivoCese   = $this->metCargarModelo('motivoCese','maestros');
        $this->atTipoNimina   = $this->metCargarModelo('tipoNomina','maestros','modNM');
        $this->atCargo        = $this->metCargarModelo('cargos','maestros');
        $this->atIdUsuario    = Session::metObtener('idUsuario');

    }

    #Metodo Index del controlador ceseReingreso.
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoDependencias',$this->atDependencias->metConsultarDependencias());
        $Empleados = $this->atEmpleados->metListarEmpleado('AP','operaciones','1','1');
        $this->atVista->assign('Empleado',$Empleados);
        $EstadoRegistro = array('PR'=>'PREPARADO','CO'=>'CONFORMADO','AP'=>'APROBADO','AN'=>'ANULADO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $this->atVista->assign('proceso','Listado');
        $this->atVista->assign('listadoCeseReingreso', $this->atCeseReingreso->metListarCeseReingreso('PR'));
        $this->atVista->metRenderizar('listado');
    }


    public function metRegistrar()
    {
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idCeseReingreso = $this->metObtenerInt('idCeseReingreso');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1) {

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_sueldo_actual')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if ($validacion['fk_rhb001_num_empleado']==''){
                $validacion['fk_rhb001_num_empleado']='noEmpleado';
            }

            if(!isset($validacion['txt_observacion_pre'])){
                $validacion['txt_observacion_pre']=NULL;
            }

            if (in_array('noEmpleado', $validacion)) {
                $validacion['status'] = 'noEmpleado';
                echo json_encode($validacion);
                exit;
            }

            #array de datos
            $datos = array(
                $validacion['fk_rhb001_num_empleado'],
                $validacion['fk_a006_num_miscelaneo_detalle_tipocr'],
                $validacion['fk_a001_num_organismo'],
                $validacion['fk_a004_num_dependencia'],
                $validacion['ind_dependencia'],
                $validacion['fk_rhc063_num_puestos'],
                $validacion['ind_cargo'],
                $validacion['num_sueldo_actual'],
                $validacion['ind_periodo'],
                $validacion['fec_fecha'],
                $validacion['num_anio_servicio'],
                $validacion['num_edad'],
                $validacion['fec_fecha_ingreso'],
                $validacion['fk_nmb001_num_tipo_nomina'],
                $validacion['fk_a006_num_miscelaneo_detalle_tipotrab'],
                $validacion['fk_rhc032_num_motivo_cese'],
                $validacion['fec_fecha_egreso'],
                $validacion['txt_observacion_egreso'],
                $validacion['num_situacion_trabajo'],
                $validacion['ind_resolucion'],
                $validacion['txt_observacion_pre']
            );


            $id = $this->atCeseReingreso->metRegistrarCeseReingreso($datos);

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='registrar';
                echo json_encode($validacion);
                exit;
            }

        }

        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $usuario = Session::metObtener('idUsuario');
        $obtenerEmpleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);

        $this->atVista->assign('estadoEmpleado',$datosEmpleado);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual

        //para llenar select empleados
        $Empleados = $this->atEmpleados->metListarEmpleado('AP','operaciones','1','1');
        $this->atVista->assign('Empleado',$Empleados);
        $miscTipoCR = $this->atEmpleados->metMostrarSelect('TIPOCR');
        $this->atVista->assign('TipoCR',$miscTipoCR);
        $miscSexo = $this->atEmpleados->metMostrarSelect('SEXO');
        $this->atVista->assign('Sexo',$miscSexo);
        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoDependencias',$this->atDependencias->metConsultarDependencias());
        $this->atVista->assign('listadoCargo',$this->atCargo->metConsultarCargos());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        $this->atVista->assign('idCeseReingreso',$idCeseReingreso);
        $this->atVista->assign('proceso','Nuevo');
        $this->atVista->metRenderizar('form','modales');
    }


    public function metModificar()
    {
        /**********************************************************************/
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idCeseReingreso = $this->metObtenerInt('idCeseReingreso');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1) {

            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if ((strcmp($tituloInt, 'num_sueldo_actual') == 0)) {
                            $validacion[$tituloInt] = str_replace(",", ".", preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        } else {
                            $validacion[$tituloInt] = $valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if (!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }


            if($idCeseReingreso != 0){


                if(strcmp($validacion['motivo'],'C')==0){

                    $datos = array(
                        $validacion['motivo'],
                        $validacion['fk_nmb001_num_tipo_nomina'],
                        $validacion['fk_a006_num_miscelaneo_detalle_tipotrab'],
                        $validacion['fk_rhc032_num_motivo_cese'],
                        $validacion['fec_fecha_egreso'],
                        $validacion['txt_observacion_egreso'],
                        $validacion['num_situacion_trabajo'],
                        $validacion['ind_resolucion'],
                        $validacion['txt_observacion_pre'],
                        $validacion['motivo']
                    );

                }
                if(strcmp($validacion['motivo'],'R')==0){

                    $datos = array(
                        $validacion['motivo'],
                        $validacion['fk_a001_num_organismo'],
                        $validacion['fk_a004_num_dependencia'],
                        $validacion['ind_dependencia'],
                        $validacion['fk_rhc063_num_puestos'],
                        $validacion['ind_cargo'],
                        $validacion['fk_nmb001_num_tipo_nomina'],
                        $validacion['fk_a006_num_miscelaneo_detalle_tipotrab'],
                        $validacion['num_situacion_trabajo'],
                        $validacion['ind_resolucion'],
                        $validacion['txt_observacion_pre']

                    );

                }


                $resultado = $this->atCeseReingreso->metModificarCeseReingreso($idCeseReingreso,$datos);

                if(is_array($resultado)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $resultado;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='modificar';
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        /***********************************************************************/

        #PARA LLENAR DATOS EN EL FORMULARIOS
        $datosBD = $this->atCeseReingreso->metMostrarCeseReingreso($idCeseReingreso);
        $this->atVista->assign('formDB',$datosBD);
        $datosOperacionesPR = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);

        /**********************************************************************/

        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual

        //para llenar select empleados
        $Empleados = $this->atEmpleados->metListarEmpleado('AP','operaciones');
        $this->atVista->assign('Empleado',$Empleados);
        $miscTipoCR = $this->atEmpleados->metMostrarSelect('TIPOCR');
        $this->atVista->assign('TipoCR',$miscTipoCR);
        $miscSexo = $this->atEmpleados->metMostrarSelect('SEXO');
        $this->atVista->assign('Sexo',$miscSexo);
        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoDependencias',$this->atDependencias->metConsultarDependencias());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());
        $this->atVista->assign('listadoCargo',$this->atCargo->metConsultarCargos());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);


        #para verificar el tipo de operacion si es CESE o REINGRESO
        foreach($miscTipoCR as $d){

            if($d['pk_num_miscelaneo_detalle'] == $datosBD['fk_a006_num_miscelaneo_detalle_tipocr']){
                $misc = $d['cod_detalle'];
            }

        }

        $this->atVista->assign('T',$misc);

        $this->atVista->assign('idCeseReingreso',$idCeseReingreso);
        $this->atVista->assign('proceso','Modificar');
        $this->atVista->metRenderizar('form','modales');

    }

    public function metVer()
    {
        /**********************************************************************/
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idCeseReingreso = $this->metObtenerInt('idCeseReingreso');

        /***********************************************************************/
        $datosBD = $this->atCeseReingreso->metMostrarCeseReingreso($idCeseReingreso);
        $this->atVista->assign('formDB',$datosBD);
        $datosOperacionesPR = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesCO = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);
        /**********************************************************************/

        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual

        //para llenar select empleados
        $Empleados = $this->atEmpleados->metListarEmpleado('AP','operaciones');
        $this->atVista->assign('Empleado',$Empleados);
        $miscTipoCR = $this->atEmpleados->metMostrarSelect('TIPOCR');
        $this->atVista->assign('TipoCR',$miscTipoCR);
        $miscSexo = $this->atEmpleados->metMostrarSelect('SEXO');
        $this->atVista->assign('Sexo',$miscSexo);
        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoDependencias',$this->atDependencias->metConsultarDependencias());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());
        $this->atVista->assign('listadoCargo',$this->atCargo->metConsultarCargos());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        $this->atVista->assign('idCeseReingreso',$idCeseReingreso);
        $this->atVista->assign('proceso','Ver');
        $this->atVista->metRenderizar('form','modales');

    }

    public function metAnular()
    {
        /**********************************************************************/
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idCeseReingreso = $this->metObtenerInt('idCeseReingreso');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if ((strcmp($tituloInt, 'num_sueldo_actual') == 0)) {
                            $validacion[$tituloInt] = str_replace(",", ".", preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        } else {
                            $validacion[$tituloInt] = $valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if (!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idCeseReingreso != 0){


                $resultado = $this->atCeseReingreso->metAnularCeseReingreso($idCeseReingreso,$validacion['txt_observacion_anulado']);

                if(is_array($resultado)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $resultado;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='anular';
                    echo json_encode($validacion);
                    exit;
                }

            }
        }

        /***********************************************************************/
        $datosBD = $this->atCeseReingreso->metMostrarCeseReingreso($idCeseReingreso);
        $this->atVista->assign('formDB',$datosBD);

        $datosOperacionesPR = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesCO = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);

        /**********************************************************************/



        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual

        //para llenar select empleados
        $Empleados = $this->atEmpleados->metListarEmpleado('AP','operaciones');
        $this->atVista->assign('Empleado',$Empleados);
        $miscTipoCR = $this->atEmpleados->metMostrarSelect('TIPOCR');
        $this->atVista->assign('TipoCR',$miscTipoCR);
        $miscSexo = $this->atEmpleados->metMostrarSelect('SEXO');
        $this->atVista->assign('Sexo',$miscSexo);
        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoDependencias',$this->atDependencias->metConsultarDependencias());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());
        $this->atVista->assign('listadoCargo',$this->atCargo->metConsultarCargos());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        $this->atVista->assign('idCeseReingreso',$idCeseReingreso);
        $this->atVista->assign('proceso','Anular');
        $this->atVista->metRenderizar('form','modales');

    }


    public function metConformar()
    {
        /**********************************************************************/
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idCeseReingreso = $this->metObtenerInt('idCeseReingreso');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if ((strcmp($tituloInt, 'num_sueldo_actual') == 0)) {
                            $validacion[$tituloInt] = str_replace(",", ".", preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        } else {
                            $validacion[$tituloInt] = $valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if (!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idCeseReingreso != 0){


                $resultado = $this->atCeseReingreso->metConformarCeseReingreso($idCeseReingreso,$validacion['txt_observacion_con']);

                if(is_array($resultado)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $resultado;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='conformado';
                    echo json_encode($validacion);
                    exit;
                }
            }
        }

        /***********************************************************************/
        $datosBD = $this->atCeseReingreso->metMostrarCeseReingreso($idCeseReingreso);
        $this->atVista->assign('formDB',$datosBD);

        $datosOperacionesPR = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesCO = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);

        /**********************************************************************/


        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual

        //para llenar select empleados
        $Empleados = $this->atEmpleados->metListarEmpleado('AP','operaciones');
        $this->atVista->assign('Empleado',$Empleados);
        $miscTipoCR = $this->atEmpleados->metMostrarSelect('TIPOCR');
        $this->atVista->assign('TipoCR',$miscTipoCR);
        $miscSexo = $this->atEmpleados->metMostrarSelect('SEXO');
        $this->atVista->assign('Sexo',$miscSexo);
        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoDependencias',$this->atDependencias->metConsultarDependencias());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());
        $this->atVista->assign('listadoCargo',$this->atCargo->metConsultarCargos());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        $this->atVista->assign('idCeseReingreso',$idCeseReingreso);
        $this->atVista->assign('proceso','Conformar');
        $this->atVista->metRenderizar('form','modales');

    }


    public function metAprobar()
    {
        /**********************************************************************/
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idCeseReingreso = $this->metObtenerInt('idCeseReingreso');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if ((strcmp($tituloInt, 'num_sueldo_actual') == 0)) {
                            $validacion[$tituloInt] = str_replace(",", ".", preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        } else {
                            $validacion[$tituloInt] = $valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if (!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idCeseReingreso != 0){

                #array de datos
                $datos = array(
                    $validacion['motivo'],
                    $validacion['fk_rhb001_num_empleado'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipocr'],
                    $validacion['fk_a001_num_organismo'],
                    $validacion['fk_a004_num_dependencia'],
                    $validacion['ind_dependencia'],
                    $validacion['fk_rhc063_num_puestos'],
                    $validacion['ind_cargo'],
                    $validacion['num_sueldo_actual'],
                    $validacion['ind_periodo'],
                    $validacion['fec_fecha'],
                    $validacion['num_anio_servicio'],
                    $validacion['num_edad'],
                    $validacion['fec_fecha_ingreso'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipotrab'],
                    $validacion['fk_rhc032_num_motivo_cese'],
                    $validacion['fec_fecha_egreso'],
                    $validacion['txt_observacion_egreso'],
                    $validacion['num_situacion_trabajo'],
                    $validacion['ind_resolucion']
                );


                $resultado = $this->atCeseReingreso->metAprobarCeseReingreso($idCeseReingreso,$validacion['txt_observacion_apro'],$datos);

                if(is_array($resultado)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $resultado;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='aprobado';
                    echo json_encode($validacion);
                    exit;
                }
            }
        }

        /***********************************************************************/
        $datosBD = $this->atCeseReingreso->metMostrarCeseReingreso($idCeseReingreso);
        $this->atVista->assign('formDB',$datosBD);

        $datosOperacionesPR = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesCO = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atCeseReingreso->metMostrarCeseReingresoOperaciones($idCeseReingreso,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);

        /**********************************************************************/


        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual

        //para llenar select empleados
        $Empleados = $this->atEmpleados->metListarEmpleado('AP','operaciones');
        $this->atVista->assign('Empleado',$Empleados);
        $miscTipoCR = $this->atEmpleados->metMostrarSelect('TIPOCR');
        $this->atVista->assign('TipoCR',$miscTipoCR);
        $miscSexo = $this->atEmpleados->metMostrarSelect('SEXO');
        $this->atVista->assign('Sexo',$miscSexo);
        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoDependencias',$this->atDependencias->metConsultarDependencias());
        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());
        $this->atVista->assign('listadoCargo',$this->atCargo->metConsultarCargos());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        #para verificar el tipo de operacion si es CESE o REINGRESO
        foreach($miscTipoCR as $d){

            if($d['pk_num_miscelaneo_detalle'] == $datosBD['fk_a006_num_miscelaneo_detalle_tipocr']){
                $misc = $d['cod_detalle'];
            }

        }

        $this->atVista->assign('T',$misc);

        $this->atVista->assign('idCeseReingreso',$idCeseReingreso);
        $this->atVista->assign('proceso','Aprobar');
        $this->atVista->metRenderizar('form','modales');

    }


    public function metVerificarSolicitudes()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');

        $fechaActual = date("Y-m-d");

        $verificar = $this->atCeseReingreso->metVerificarSolicitudes($idEmpleado);

        if($verificar==0){

            $data = array('id' => $idEmpleado, 'status' => 'SI');

            echo json_encode($data);

        }else{

            foreach($verificar as $reg){

                if(strcmp($reg['cod_detalle'],'C')==0){

                    if($reg['fec_operacion'] == $fechaActual){

                        $data = array('id' => $idEmpleado, 'status' => 'NO-C');
                        echo json_encode($data);

                    }
                    else if(($reg['ind_estado']=='PR') || ($reg['ind_estado']=='CO')){

                        $data = array('id' => $idEmpleado, 'status' => 'NO-C');
                        echo json_encode($data);

                    }
                    else if(($reg['fec_operacion'] > $fechaActual) && ($reg['ind_estado']=='AP')){

                        $data = array('id' => $idEmpleado, 'status' => 'SI');
                        echo json_encode($data);
                    }

                }
                else{

                    if($reg['fec_operacion']==$fechaActual){

                        $data = array('id' => $idEmpleado, 'status' => 'NO-R');
                        echo json_encode($data);

                    }
                    else if(($reg['ind_estado']=='PR') || ($reg['ind_estado']=='CO')){

                        $data = array('id' => $idEmpleado, 'status' => 'NO-R');
                        echo json_encode($data);

                    }
                    else if(($reg['fec_operacion'] > $fechaActual) && ($reg['ind_estado']=='AP')){

                        $data = array('id' => $idEmpleado, 'status' => 'SI');
                        echo json_encode($data);
                    }

                }

            }

        }

    }


    // Método que permite buscar un empleado desde el wizard
    public function metConsultarEmpleado()
    {

        $proceso = $this->metObtenerTexto('proceso');

        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];

        $listadoOrganismo = $this->atOrganismo->metListarOrganismo();

        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);
        $pkNumDependencia = $datosEmpleado['pk_num_dependencia'];

        $listadoEmpleados = $this->atDependencias->metDependenciaEmpleado($pkNumDependencia);
        $this->atVista->assign('empleado', $listadoEmpleados);

        $listadoDependencia = $this->atDependencias->metListarDependenciaOrganismo($datosEmpleado['pk_num_organismo'], 1, 0);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        // Organismo donde trabaja el empleado
        $empleado = array(
            'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
            'pk_num_dependencia' => $pkNumDependencia
        );
        $datosTrabajador = $this->atCeseReingreso->metBuscarTrabajador($datosEmpleado['pk_num_organismo'], $pkNumDependencia, '', '', '', '', '', '', '');

        $centroCosto = $this->atCeseReingreso->metListarCentroCosto();
        $tipoNomina = $this->atTipoNimina->metListarTipoNomina();
        $tipoTrabajador = $this->atEmpleados->metMostrarSelect('TIPOTRAB');
        // Envío a la vista
        $this->atVista->assign('datosTrabajador', $datosTrabajador);
        $this->atVista->assign('centroCosto', $centroCosto);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('nomina', $tipoNomina);
        $this->atVista->assign('tipoTrabajador', $tipoTrabajador);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('proceso',$proceso);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));
        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->metRenderizar('buscarEmpleado', 'modales');

    }

    #PERMITE OBTENER LOS DATOS DEL EMPLEADO
    public function metObtenerDatosEmpleado()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');

        $resultado = $this->atCeseReingreso->metObtenerDatosEmpleado($idEmpleado);

        $sueldo = number_format($resultado['num_sueldo_basico'],2,",",".");
        $f_n    = explode("-",$resultado['fec_nacimiento']);
        $f_i    = explode("-",$resultado['fec_ingreso']);
        $f_a    = date("d-m-Y");

        #edad y años de servicio
        list($edad, $EdadMeses, $EdadDias) = $this->metCalcularEdadAnioServicio($f_n[2]."-".$f_n[1]."-".$f_n[0],$f_a);
        list($anio_serv, $MesesServicio, $DiasServicio) = $this->metCalcularEdadAnioServicio($f_i[2]."-".$f_i[1]."-".$f_i[0],$f_a);


        #consulto miscelaneos
        $miscTipoCR = $this->atEmpleados->metMostrarSelect('TIPOCR');

        #situacion de trabajo
        if($resultado['estatus_empleado']==1){//activo:cese
            $T = 'C';

            foreach($miscTipoCR as $d){

                if(strcmp($d['cod_detalle'],$T)==0){
                    $misc = $d['pk_num_miscelaneo_detalle'];
                }

            }


        }else{//inactivo:reingreso
            $T= 'R';

            foreach($miscTipoCR as $d){
                if(strcmp($d['cod_detalle'],$T)==0){
                    $misc = $d['pk_num_miscelaneo_detalle'];
                }
            }

        }

        //datos mpara mostrar en el formulario
        $datos = array(
             'id_empleado' => $idEmpleado,
             'nombreTrabajador' => $resultado['nombre_completo'],
             'num_sueldo_basico' => $sueldo,
             'fec_nacimiento' => $f_n[2]."-".$f_n[1]."-".$f_n[0],
             'fec_ingreso' => $f_i[2]."-".$f_i[1]."-".$f_i[0],
             'ind_cedula_documento' => $resultado['ind_cedula_documento'],
             'fk_a001_num_organismo' => $resultado['fk_a001_num_organismo'],
             'fk_a004_num_dependencia' => $resultado['fk_a004_num_dependencia'],
             'ind_dependencia' => $resultado['dependencia'],
             'fk_a006_num_miscelaneo_detalle_sexo' => $resultado['fk_a006_num_miscelaneo_detalle_sexo'],
             'fk_rhc063_num_puestos_cargo' => $resultado['fk_rhc063_num_puestos_cargo'],
             'cargo' => $resultado['cargo'],
             'edad' => $edad,
             'anio_serv' => $anio_serv,
             'fk_a006_num_miscelaneo_detalle_tipocr' => $misc,
             'nomina' => $resultado['fk_nmb001_num_tipo_nomina'],
             'tipo_trabajador' => $resultado['fk_a006_num_miscelaneo_detalle_tipotrabajador'],
             'estatus' => $resultado['estatus_empleado'],
             'tipo' => $T
        );

        echo json_encode($datos);

    }


    public function metListadoConformar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('proceso','Conformar');
        $this->atVista->assign('listadoCeseReingreso', $this->atCeseReingreso->metListarCeseReingreso('PR'));
        $this->atVista->metRenderizar('listado');
    }


    public function metListadoAprobar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('proceso','Aprobar');
        $this->atVista->assign('listadoCeseReingreso', $this->atCeseReingreso->metListarCeseReingreso('CO'));
        $this->atVista->metRenderizar('listado');
    }

    public function metCalcularEdadAnioServicio($_DESDE,$_HASTA)
    {
        $error = 0;
        $listo = 0;
        if ((strlen($_DESDE)) < 10) $error = 1;
        else {
            list($d, $m, $a) = explode("-", $_HASTA);
            $diaActual = $d;
            $mesActual = $m;
            $annioActual = $a;
            ##
            list($d, $m, $a) = explode("-", $_DESDE);
            $dia = intval($d);
            $mes = intval($m);
            $annio = intval($a);
            $dias = 0;
            $meses = 0;
            $annios = 0;
            ##
            if ($annio > $annioActual || ($annio == $annioActual && $mes > $mesActual) || ($annio == $annioActual && $mes == $mesActual && $dia > $diaActual)) $error = 2;
            else {
                $annios = $annioActual - $annio;
                $meses = $mesActual - $mes;
                $dias = $diaActual - $dia;
                ##
                if ($dias < 0) { $meses--; $dias = 30 + $dias; }
                if ($meses < 0) { $annios--; $meses = 12 + $meses; }
                ##
                if ($dias >= 30) { $meses++; $dias = 0; }
                if ($meses >= 12) { $annios++; $meses = 0; }
                ##
                return array($annios, $meses, $dias);
            }
        }
        if ($error!=0) return array("", "", "");
    }



    public function metBusquedaFiltro()
    {

        $organismo   = $this->metObtenerInt('organismo');
        $dependencia = $this->metObtenerInt('dependencia');
        $estatus_persona  = $this->metObtenerInt('estatus_persona');
        $estatus_empleado = $this->metObtenerInt('estatus_empleado');

        $datos = $this->atEmpleados->metListarEmpleadoOrganizacion($organismo,$dependencia,$estatus_persona,$estatus_empleado);

        echo json_encode($datos);


    }

    public function metBusquedaFiltroListado()
    {
        $organismo   = $this->metObtenerInt('organismo');
        $dependencia = $this->metObtenerInt('dependencia');
        $estatus  = $this->metObtenerTexto('estatus');
        $empleado = $this->metObtenerInt('empleado');

        $datos = $this->atCeseReingreso->metListarCeseReingresoFiltro($organismo,$dependencia,$estatus,$empleado);

        echo json_encode($datos);

    }




}
