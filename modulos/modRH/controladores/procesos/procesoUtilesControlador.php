<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class procesoUtilesControlador extends Controlador
{

    private $atIdUsuario;
    private $atProcesoUtiles;
    private $atBeneficioUtiles;
    private $atEmpleados;
    private $atCargaFamiliar;
    private $atProveedor;
    private $atDependencias;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atProcesoUtiles   = $this->metCargarModelo('procesoUtiles','procesos');
        $this->atBeneficioUtiles = $this->metCargarModelo('beneficiosUtiles','maestros');
        $this->atEmpleados       = $this->metCargarModelo('empleados','gestion');
        $this->atProveedor       = $this->metCargarModelo('proveedor','maestros','modLG');

    }

    public function metIndex()
    {

    }

    /*permite cargar el formuario para registrar los requisitos para optar por el beneficio*/
    public function metNuevoRequisito()
    {

        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idRequisito = $this->metObtenerInt('idRequisito');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if((strcmp($tituloInt,'monto_t')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            $fecha = date("Y-m-d");

            #array de datos
            $datos = array(
                $validacion['fk_rhc056_num_beneficio_utiles'],
                $validacion['fk_rhb001_num_empleado'],
                $fecha,
                $validacion['d_requisitos'],
                $validacion['t_requisitos'],
                $validacion['aleatorio']
            );

            $id = $this->atProcesoUtiles->metRegistrarRequisitosUtiles($datos);

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='registrado';
                echo json_encode($validacion);
                exit;
            }

        }

        $periodos = $this->atBeneficioUtiles->metConsultarBeneficiosUtiles();
        $this->atVista->assign('periodos',$periodos);

        $this->atVista->assign('aleatorio',rand(0,9999999999));
        $this->atVista->assign('idRequisito',$idRequisito);
        $this->atVista->assign('proceso','Nuevo');
        $this->atVista->metRenderizar('ingresarRequisitos','modales');

    }

    #Metodo que permite obtener los datos del empleado
    public function metObtenerDatosEmpleado()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');

        $resultado = $this->atProcesoUtiles->metObtenerDatosEmpleado($idEmpleado);

          //datos mpara mostrar en el formulario
        $datos = array(
            'id_empleado' => $idEmpleado,
            'nombreTrabajador' => $resultado['nombre_completo'],
        );

        echo json_encode($datos);

    }

    /*permite consultar la carga familiar unica que puede optar por el beneficio de utiles*/
    public function metConsultarCargaFamiliar()
    {
        $empleado = $this->metObtenerInt('pk_num_empleado');
        $this->atCargaFamiliar = $this->metCargarModelo('cargaFamiliar','gestion');
        $beneficiarios = $this->atCargaFamiliar->metCargaFamiliarEmpleadoRequisitos($empleado);
        echo json_encode($beneficiarios);
    }

    /*permite consultar la carga familiar unica que puede optar por el beneficio de utiles para armar el selec para el registro de beneficio*/
    public function metConsultarCargaFamiliarHijos()
    {
        $empleado = $this->metObtenerInt('pk_num_empleado');
        $this->atCargaFamiliar = $this->metCargarModelo('cargaFamiliar','gestion');
        $beneficiarios = $this->atCargaFamiliar->metCargaFamiliarEmpleadoRequisitos($empleado);

        #recibo los datos , los recorro con un for y los almaceno en una variable
        $html="<option value=''>Seleccione...</option>";
        for($i=0;$i<sizeof($beneficiarios);$i++){
            $html.= "<option value='".$beneficiarios[$i]['pk_num_carga']."-".$beneficiarios[$i]['nombre']."'>".$beneficiarios[$i]['nombre']."</option>";
        }
        #los codifico con json para mostrarlos en la vista
        echo json_encode($html);
        exit;
    }

    /*permite abrir formulario para ingresar los requisitos entregados por beneficiarios*/
    public function metIngresarRequisitos()
    {

        $empleado = $this->metObtenerInt('idEmpleado');
        $cargaFam = $this->metObtenerInt('idCargaFamiliar');

        $miscelaneo = $this->atEmpleados->metMostrarSelect('REQUTILES');
        $this->atVista->assign('tipo_requisito',$miscelaneo);

        $this->atVista->assign('id_carga_familiar',$cargaFam);
        $this->atVista->assign('id_empleado',$empleado);

        $this->atVista->metRenderizar('requisitos','modales');

    }


    /*permite cargar el formuario para registrar un nuevo beneficio*/
    public function metNuevoBeneficio()
    {

        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idBeneficio = $this->metObtenerInt('idBeneficio');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if((strcmp($tituloInt,'monto_t')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            $fecha = date("Y-m-d");

            if(!isset($validacion['lg_b022_num_proveedor']) || empty($validacion['lg_b022_num_proveedor'])){
                $validacion['lg_b022_num_proveedor']=NULL;
            }

            #array de datos
            $datos = array(
                $validacion['num_orden'],
                $validacion['fk_a006_num_miscelaneo_detalle_asignacion'],
                $validacion['fk_rhc056_num_beneficio_utiles'],
                $validacion['fk_rhb001_num_empleado'],
                $fecha,
                $validacion['lg_b022_num_proveedor'],
                $validacion['monto_t'],
                'PR',
                $validacion['carga'],
                $validacion['monto_a'],
                $validacion['txt_observacion_pre']
            );

            $id = $this->atProcesoUtiles->metRegistrarBeneficioUtiles($datos);

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='registrado';
                $datos = $this->atProcesoUtiles->metMostrarBeneficioUtiles($id);
                $validacion['ind_descripcion_beneficio'] = $datos['ind_descripcion_beneficio'];
                $validacion['nombre_empleado'] = $datos['nombre_empleado'];
                $validacion['num_monto'] = number_format($datos['num_monto'],2,".",",");
                $validacion['cod_detalle'] = $datos['cod_detalle'];
                $validacion['ind_nombre_detalle'] = $datos['ind_nombre_detalle'];
                $validacion['ind_estado'] = $datos['ind_estado'];
                $validacion['idBeneficio'] = $id;
                echo json_encode($validacion);
                exit;
            }

        }

        $miscelaneoTipoAsignacion = $this->atEmpleados->metMostrarSelect('TIPOASGUTI');
        $this->atVista->assign('tipoAsignacion',$miscelaneoTipoAsignacion);
        $periodos = $this->atBeneficioUtiles->metConsultarBeneficiosUtiles();
        $this->atVista->assign('periodos',$periodos);

        $proveedor = $this->atProveedor->metListarProveedor();
        $this->atVista->assign('proveedores',$proveedor);

        $ultimaOrden = $this->atProcesoUtiles->metUltimaOrden();
        $this->atVista->assign('orden',$ultimaOrden);

        $this->atVista->assign('aleatorio',rand(0,9999999999));
        $this->atVista->assign('proceso','NuevoBeneficio');
        $this->atVista->metRenderizar('nuevoBeneficio','modales');

    }

    /*permite listar los beneficios de utiles generados*/
    public function metListarBeneficiosUtiles()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'dropzone/downloads/css/dropzone'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'dropzone/downloads/dropzone.min'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoBeneficio', $this->atProcesoUtiles->metListarBeneficioUtiles());
        $this->atVista->metRenderizar('listado');
    }


    /*permite listar las asignaciones de utiles pendientes*/
    public function metListarAsignacionPendientes()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'dropzone/downloads/css/dropzone'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'dropzone/downloads/dropzone.min'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoBeneficioPendientes', $this->atProcesoUtiles->metListarAsignacionesPendientes());
        $this->atVista->metRenderizar('listadoAsignacionesPendientes');
    }

    /*permite listar las asignaciones de utiles*/
    public function metListarAsignacionUtiles()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'dropzone/downloads/css/dropzone'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'dropzone/downloads/dropzone.min'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoAsignacionesUtiles', $this->atProcesoUtiles->metListarAsignacionesUtiles());
        $this->atVista->metRenderizar('listadoAsignaciones');
    }


    /*permite mostrar los datos en formulario para visualizarlos*/
    public function metVer()
    {

        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idBeneficio = $this->metObtenerInt('idBeneficio');
        $valido  = $this->metObtenerInt('valido');


        $datosBeneficio = $this->atProcesoUtiles->metMostrarBeneficioUtiles($idBeneficio);
        $this->atVista->assign('formDB',$datosBeneficio);

        $datosBeneficiario = $this->atProcesoUtiles->metMostrarBeneficiarios($idBeneficio);
        $this->atVista->assign('formDBbeneficiarios',$datosBeneficiario);

        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesRV = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'RV');
        $this->atVista->assign('formDBoperacionesRV',$datosOperacionesRV);
        $datosOperacionesCO = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);


        $miscelaneoTipoAsignacion = $this->atEmpleados->metMostrarSelect('TIPOASGUTI');
        $this->atVista->assign('tipoAsignacion',$miscelaneoTipoAsignacion);
        $periodos = $this->atBeneficioUtiles->metConsultarBeneficiosUtiles();
        $this->atVista->assign('periodos',$periodos);

        $proveedor = $this->atProveedor->metListarProveedor();
        $this->atVista->assign('proveedores',$proveedor);

        $this->atVista->assign('idBeneficio',$idBeneficio);
        $this->atVista->assign('proceso','VerBeneficio');
        $this->atVista->metRenderizar('verBeneficio','modales');


    }

    /*permite mostrar los datos en formulario para modificarlos*/
    public function metModificar()
    {
        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idBeneficio = $this->metObtenerInt('idBeneficio');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if((strcmp($tituloInt,'monto_t')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            $fecha = date("Y-m-d");

            if(!isset($validacion['lg_b022_num_proveedor']) || empty($validacion['lg_b022_num_proveedor'])){
                $validacion['lg_b022_num_proveedor']=NULL;
            }

            #array de datos
            $datos = array(
                $validacion['num_orden'],
                $validacion['fk_a006_num_miscelaneo_detalle_asignacion'],
                $validacion['fk_rhc056_num_beneficio_utiles'],
                $validacion['fk_rhb001_num_empleado'],
                $fecha,
                $validacion['lg_b022_num_proveedor'],
                $validacion['monto_t'],
                'PR',
                $validacion['carga'],
                $validacion['monto_a'],
                $validacion['txt_observacion_pre']

            );

            $id = $this->atProcesoUtiles->metModificarBeneficioUtiles($idBeneficio,$datos);
            $validacion['idBeneficio'] = $idBeneficio;

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='modificado';
                $datos = $this->atProcesoUtiles->metMostrarBeneficioUtiles($id);
                $validacion['ind_descripcion_beneficio'] = $datos['ind_descripcion_beneficio'];
                $validacion['nombre_empleado'] = $datos['nombre_empleado'];
                $validacion['num_monto'] = number_format($datos['num_monto'],2,",",".");
                $validacion['cod_detalle'] = $datos['cod_detalle'];
                $validacion['ind_nombre_detalle'] = $datos['ind_nombre_detalle'];
                $validacion['ind_estado'] = $datos['ind_estado'];
                echo json_encode($validacion);
                exit;
            }

        }


        $datosBeneficio = $this->atProcesoUtiles->metMostrarBeneficioUtiles($idBeneficio);
        $this->atVista->assign('formDB',$datosBeneficio);

        $datosBeneficiario = $this->atProcesoUtiles->metMostrarBeneficiarios($idBeneficio);
        $this->atVista->assign('formDBbeneficiarios',$datosBeneficiario);


        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);

        $beneficiarios = '';
        $montos = '';
        foreach($datosBeneficiario as $ben){
            $beneficiarios = $beneficiarios.$ben['fk_c015_num_carga_familiar']."#";
            $montos = $montos.$ben['num_monto_asignado']."#";
        }
        $this->atVista->assign('beneficiarios',$beneficiarios);
        $this->atVista->assign('montos',$montos);

        $miscelaneoTipoAsignacion = $this->atEmpleados->metMostrarSelect('TIPOASGUTI');
        $this->atVista->assign('tipoAsignacion',$miscelaneoTipoAsignacion);
        $periodos = $this->atBeneficioUtiles->metConsultarBeneficiosUtiles();
        $this->atVista->assign('periodos',$periodos);

        $proveedor = $this->atProveedor->metListarProveedor();
        $this->atVista->assign('proveedores',$proveedor);

        $this->atVista->assign('idBeneficio',$idBeneficio);
        $this->atVista->assign('proceso','ModificarBeneficio');
        $this->atVista->metRenderizar('modBeneficio','modales');
    }


    /*permite mostrar los datos en formulario para hacer la Revision*/
    public function metRevisar()
    {
        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idBeneficio = $this->metObtenerInt('idBeneficio');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if((strcmp($tituloInt,'monto_t')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            $fecha = date("Y-m-d");

            $id = $this->atProcesoUtiles->metRevisarBeneficioUtiles($idBeneficio,$validacion['txt_observacion_rev']);
            $validacion['idBeneficio'] = $idBeneficio;

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='revisado';
                $datos = $this->atProcesoUtiles->metMostrarBeneficioUtiles($id);
                $validacion['ind_descripcion_beneficio'] = $datos['ind_descripcion_beneficio'];
                $validacion['nombre_empleado'] = $datos['nombre_empleado'];
                $validacion['num_monto'] = number_format($datos['num_monto'],2,",",".");
                $validacion['cod_detalle'] = $datos['cod_detalle'];
                $validacion['ind_nombre_detalle'] = $datos['ind_nombre_detalle'];
                $validacion['ind_estado'] = $datos['ind_estado'];
                echo json_encode($validacion);
                exit;
            }

        }


        $datosBeneficio = $this->atProcesoUtiles->metMostrarBeneficioUtiles($idBeneficio);
        $this->atVista->assign('formDB',$datosBeneficio);

        $datosBeneficiario = $this->atProcesoUtiles->metMostrarBeneficiarios($idBeneficio);
        $this->atVista->assign('formDBbeneficiarios',$datosBeneficiario);


        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);

        $beneficiarios = '';
        $montos = '';
        foreach($datosBeneficiario as $ben){
            $beneficiarios = $beneficiarios.$ben['fk_c015_num_carga_familiar']."#";
            $montos = $montos.$ben['num_monto_asignado']."#";
        }
        $this->atVista->assign('beneficiarios',$beneficiarios);
        $this->atVista->assign('montos',$montos);

        $miscelaneoTipoAsignacion = $this->atEmpleados->metMostrarSelect('TIPOASGUTI');
        $this->atVista->assign('tipoAsignacion',$miscelaneoTipoAsignacion);
        $periodos = $this->atBeneficioUtiles->metConsultarBeneficiosUtiles();
        $this->atVista->assign('periodos',$periodos);

        $proveedor = $this->atProveedor->metListarProveedor();
        $this->atVista->assign('proveedores',$proveedor);

        $this->atVista->assign('idBeneficio',$idBeneficio);
        $this->atVista->assign('proceso','RevisarBeneficio');
        $this->atVista->metRenderizar('operacionesBeneficio','modales');
    }

    /*permite mostrar los datos en formulario para hacer la Conformacion*/
    public function metConformar()
    {
        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idBeneficio = $this->metObtenerInt('idBeneficio');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if((strcmp($tituloInt,'monto_t')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            $fecha = date("Y-m-d");

            $id = $this->atProcesoUtiles->metConformarBeneficioUtiles($idBeneficio,$validacion['txt_observacion_con']);
            $validacion['idBeneficio'] = $idBeneficio;

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='conformado';
                $datos = $this->atProcesoUtiles->metMostrarBeneficioUtiles($id);
                $validacion['ind_descripcion_beneficio'] = $datos['ind_descripcion_beneficio'];
                $validacion['nombre_empleado'] = $datos['nombre_empleado'];
                $validacion['num_monto'] = number_format($datos['num_monto'],2,",",".");
                $validacion['cod_detalle'] = $datos['cod_detalle'];
                $validacion['ind_nombre_detalle'] = $datos['ind_nombre_detalle'];
                $validacion['ind_estado'] = $datos['ind_estado'];
                echo json_encode($validacion);
                exit;
            }

        }


        $datosBeneficio = $this->atProcesoUtiles->metMostrarBeneficioUtiles($idBeneficio);
        $this->atVista->assign('formDB',$datosBeneficio);

        $datosBeneficiario = $this->atProcesoUtiles->metMostrarBeneficiarios($idBeneficio);
        $this->atVista->assign('formDBbeneficiarios',$datosBeneficiario);

        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);

        $datosOperacionesRV = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'RV');
        $this->atVista->assign('formDBoperacionesRV',$datosOperacionesRV);

        $beneficiarios = '';
        $montos = '';
        foreach($datosBeneficiario as $ben){
            $beneficiarios = $beneficiarios.$ben['fk_c015_num_carga_familiar']."#";
            $montos = $montos.$ben['num_monto_asignado']."#";
        }
        $this->atVista->assign('beneficiarios',$beneficiarios);
        $this->atVista->assign('montos',$montos);

        $miscelaneoTipoAsignacion = $this->atEmpleados->metMostrarSelect('TIPOASGUTI');
        $this->atVista->assign('tipoAsignacion',$miscelaneoTipoAsignacion);
        $periodos = $this->atBeneficioUtiles->metConsultarBeneficiosUtiles();
        $this->atVista->assign('periodos',$periodos);

        $proveedor = $this->atProveedor->metListarProveedor();
        $this->atVista->assign('proveedores',$proveedor);

        $this->atVista->assign('idBeneficio',$idBeneficio);
        $this->atVista->assign('proceso','ConformarBeneficio');
        $this->atVista->metRenderizar('operacionesBeneficio','modales');

    }

    /*permite mostrar los datos en formulario para hacer la Aprobacion*/
    public function metAprobar()
    {
        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
        );
        $complementoJs = array(
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idBeneficio = $this->metObtenerInt('idBeneficio');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $this->metValidarToken();

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if((strcmp($tituloInt,'monto_t')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            $fecha = date("Y-m-d");

            $id = $this->atProcesoUtiles->metAprobarBeneficioUtiles($idBeneficio,$validacion['txt_observacion_apr']);
            $validacion['idBeneficio'] = $idBeneficio;

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='aprobado';
                $datos = $this->atProcesoUtiles->metMostrarBeneficioUtiles($id);
                $validacion['ind_descripcion_beneficio'] = $datos['ind_descripcion_beneficio'];
                $validacion['nombre_empleado'] = $datos['nombre_empleado'];
                $validacion['num_monto'] = number_format($datos['num_monto'],2,",",".");
                $validacion['cod_detalle'] = $datos['cod_detalle'];
                $validacion['ind_nombre_detalle'] = $datos['ind_nombre_detalle'];
                $validacion['ind_estado'] = $datos['ind_estado'];
                echo json_encode($validacion);
                exit;
            }

        }


        $datosBeneficio = $this->atProcesoUtiles->metMostrarBeneficioUtiles($idBeneficio);
        $this->atVista->assign('formDB',$datosBeneficio);

        $datosBeneficiario = $this->atProcesoUtiles->metMostrarBeneficiarios($idBeneficio);
        $this->atVista->assign('formDBbeneficiarios',$datosBeneficiario);

        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);

        $datosOperacionesRV = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'RV');
        $this->atVista->assign('formDBoperacionesRV',$datosOperacionesRV);

        $datosOperacionesCO = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);

        $beneficiarios = '';
        $montos = '';
        foreach($datosBeneficiario as $ben){
            $beneficiarios = $beneficiarios.$ben['fk_c015_num_carga_familiar']."#";
            $montos = $montos.$ben['num_monto_asignado']."#";
        }
        $this->atVista->assign('beneficiarios',$beneficiarios);
        $this->atVista->assign('montos',$montos);

        $miscelaneoTipoAsignacion = $this->atEmpleados->metMostrarSelect('TIPOASGUTI');
        $this->atVista->assign('tipoAsignacion',$miscelaneoTipoAsignacion);
        $periodos = $this->atBeneficioUtiles->metConsultarBeneficiosUtiles();
        $this->atVista->assign('periodos',$periodos);

        $proveedor = $this->atProveedor->metListarProveedor();
        $this->atVista->assign('proveedores',$proveedor);

        $this->atVista->assign('idBeneficio',$idBeneficio);
        $this->atVista->assign('proceso','AprobarBeneficio');
        $this->atVista->metRenderizar('operacionesBeneficio','modales');

    }


    public function metInformacionRegistro()
    {
        $idBeneficio = $this->metObtenerInt('idBeneficio');

        /*$informacion = $this->atProcesoUtiles->metConsultarInformacionRegistro($idBeneficio);
        $this->atVista->assign('informacion',$informacion);*/

        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesRV = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'RV');
        $this->atVista->assign('formDBoperacionesRV',$datosOperacionesRV);
        $datosOperacionesCO = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);

        $this->atVista->metRenderizar('informacion','modales');

    }

    public function metInformacionRegistroAsignaciones()
    {
        $idAsignacion = $this->metObtenerInt('idAsignacion');

        /*$informacion = $this->atProcesoUtiles->metConsultarInformacionRegistro($idBeneficio);
        $this->atVista->assign('informacion',$informacion);*/

        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesRV = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'RV');
        $this->atVista->assign('formDBoperacionesRV',$datosOperacionesRV);
        $datosOperacionesAP = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);

        $this->atVista->metRenderizar('informacion','modales');

    }



    #permite eliminar un beneficiario hijo
    public function metEliminarBeneficario()
    {
        $idBeneficio  = $this->metObtenerInt('idBeneficio');
        $beneficiario = $this->metObtenerInt('idCargaFamiliar');

        if($idBeneficio!=0 && $beneficiario!=0){
            $id=$this->atProcesoUtiles->metEliminarBeneficiario($idBeneficio,$beneficiario);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Error al eliminar registro. Consulte con el Administrador del Sistema'
                );
            }else{
                $valido=array(
                    'status'=>'OK',
                    'idBeneficio'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }


    /*permite cargar el formuario para registrar la asigancion de utiles escolares de un determinado beneficio*/
    public function metNuevaAsignacion()
    {

        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idAsignacion = $this->metObtenerInt('idAsignacion');
        $idBeneficio  = $this->metObtenerInt('idBeneficio');
        $idBeneficioUtiles  = $this->metObtenerInt('idBeneficioUtiles');
        $beneficio    = $this->metObtenerTexto('beneficio');
        $idEmpleado   = $this->metObtenerInt('idEmpleado');
        $nombEmpleado = $this->metObtenerTexto('nombEmpleado');
        $idCargaFamiliar = $this->metObtenerInt('idCargaFamiliar');
        $nombHijo = $this->metObtenerTexto('nombHijo');
        $idNivelGradoInstruccion = $this->metObtenerInt('idNivelGradoInstruccion');
        $nivelGradoInstruccion = $this->metObtenerTexto('nivelGradoInstruccion');
        $orden = $this->metObtenerTexto('orden');

        $valido  = $this->metObtenerInt('valido');

        if($valido==1)
        {

            $this->metValidarToken();


            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if((strcmp($tituloInt,'exento')==0) || (strcmp($tituloInt,'bi_g')==0) || (strcmp($tituloInt,'iva_g')==0) || (strcmp($tituloInt,'total')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            $anio = date("Y");

            #array de datos
            $datos = array(
                $idBeneficio,
                $validacion['fk_rhb001_num_empleado'],
                $validacion['fk_rhc015_num_carga_familiar'],
                $validacion['fk_rhc064_nivel_grado_instruccion'],
                $validacion['fk_rhc056_num_beneficio_utiles'],
                $anio,
                $validacion['num_factura'],
                $validacion['fec_fecha_factura'],
                $validacion['exento'],
                $validacion['bi_g'],
                $validacion['iva_g'],
                $validacion['total'],
                'PR',
                $validacion['txt_observacion_pre']
            );

            $detalle = array(
                $_POST['idItem'],
                $_POST['cantidadItem'],
                $_POST['descripcionItem'],
                $_POST['montoItem'],
                $_POST['gravableItem']
            );


            $id = $this->atProcesoUtiles->metRegistrarAsignacionUtiles($datos,$detalle);

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='registrado';
                echo json_encode($validacion);
                exit;
            }

        }

        /*asigno variables a la vista*/
        $this->atVista->assign('idBeneficio',$idBeneficio);
        $this->atVista->assign('idBeneficioUtiles',$idBeneficioUtiles);
        $this->atVista->assign('beneficio',$beneficio);
        $this->atVista->assign('idEmpleado',$idEmpleado);
        $this->atVista->assign('nombEmpleado',$nombEmpleado);
        $this->atVista->assign('nombHijo',$nombHijo);
        $this->atVista->assign('idCargaFamiliar',$idCargaFamiliar);
        $this->atVista->assign('nombHijo',$nombHijo);
        $this->atVista->assign('idNivelGradoInstruccion',$idNivelGradoInstruccion);
        $this->atVista->assign('nivelGradoInstruccion',$nivelGradoInstruccion);
        $this->atVista->assign('orden',$orden);

        $this->atVista->assign('idAsignacion',$idAsignacion);
        $this->atVista->assign('proceso','NuevaAsignacion');
        $this->atVista->metRenderizar('nuevaAsignacion','modales');

    }

    #permite modificar la asignacion de utiles
    public function metModificarAsignacion()
    {

        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);




        $idAsignacion = $this->metObtenerInt('idAsignacion');
        $valido  = $this->metObtenerInt('valido');

        $datosAsignacion = $this->atProcesoUtiles->metMostrarAsignacionUtiles($idAsignacion);
        $this->atVista->assign('formDB',$datosAsignacion);

        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);


        if($valido==1)
        {

            $this->metValidarToken();


            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if((strcmp($tituloInt,'exento')==0) || (strcmp($tituloInt,'bi_g')==0) || (strcmp($tituloInt,'iva_g')==0) || (strcmp($tituloInt,'total')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            $anio = date("Y");

            #array de datos
            $datos = array(
                $idAsignacion,
                $validacion['fk_rhb001_num_empleado'],
                $validacion['fk_rhc015_num_carga_familiar'],
                $validacion['fk_rhc064_nivel_grado_instruccion'],
                $validacion['fk_rhc056_num_beneficio_utiles'],
                $anio,
                $validacion['num_factura'],
                $validacion['fec_fecha_factura'],
                $validacion['exento'],
                $validacion['bi_g'],
                $validacion['iva_g'],
                $validacion['total'],
                'PR',
                $validacion['txt_observacion_pre']
            );

            $detalle = array(
                $_POST['idItem'],
                $_POST['cantidadItem'],
                $_POST['descripcionItem'],
                $_POST['montoItem'],
                $_POST['gravableItem']
            );

            $id = $this->atProcesoUtiles->metModificarAsignacionUtiles($idAsignacion,$datos,$detalle);

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='modificado';
                echo json_encode($validacion);
                exit;
            }

        }

        /*asigno variables a la vista*/
        $this->atVista->assign('idAsignacion',$idAsignacion);
        $this->atVista->assign('proceso','ModificarAsignacion');
        $this->atVista->metRenderizar('modAsignacion','modales');

    }


    #permite revisar la asignacion de utiles
    public function metRevisarAsignacion()
    {

        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);




        $idAsignacion = $this->metObtenerInt('idAsignacion');
        $valido  = $this->metObtenerInt('valido');

        $datosAsignacion = $this->atProcesoUtiles->metMostrarAsignacionUtiles($idAsignacion);
        $this->atVista->assign('formDB',$datosAsignacion);

        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);


        if($valido==1)
        {

            $this->metValidarToken();


            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if((strcmp($tituloInt,'exento')==0) || (strcmp($tituloInt,'bi_g')==0) || (strcmp($tituloInt,'iva_g')==0) || (strcmp($tituloInt,'total')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            $fecha = date("Y-m-d");

            $id = $this->atProcesoUtiles->metRevisarAsignacionUtiles($idAsignacion,$validacion['txt_observacion_rev']);
            $validacion['idAsignacion'] = $idAsignacion;

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='revisado';
                $datos = $this->atProcesoUtiles->metMostrarAsignacionUtiles($idAsignacion);
                $validacion['num_factura'] = $datos['num_factura'];
                $validacion['ind_descripcion_beneficio'] = $datos['ind_descripcion_beneficio'];
                $validacion['nombre_funcionario'] = $datos['nombre_funcionario'];
                $validacion['nombre_carga_familiar'] = $datos['nombre_hijo'];
                $validacion['num_monto'] = number_format($datos['num_monto'],2,',','.');
                echo json_encode($validacion);
                exit;
            }

        }

        /*asigno variables a la vista*/
        $this->atVista->assign('idAsignacion',$idAsignacion);
        $this->atVista->assign('proceso','RevisarAsignacion');
        $this->atVista->metRenderizar('operacionesBeneficioAsignacion','modales');

    }


    #permite revisar la asignacion de utiles
    public function metAprobarAsignacion()
    {

        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $idAsignacion = $this->metObtenerInt('idAsignacion');
        $valido  = $this->metObtenerInt('valido');

        $datosAsignacion = $this->atProcesoUtiles->metMostrarAsignacionUtiles($idAsignacion);
        $this->atVista->assign('formDB',$datosAsignacion);

        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);

        $datosOperacionesRV = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'RV');
        $this->atVista->assign('formDBoperacionesRV',$datosOperacionesRV);

        if($valido==1)
        {

            $this->metValidarToken();


            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if((strcmp($tituloInt,'exento')==0) || (strcmp($tituloInt,'bi_g')==0) || (strcmp($tituloInt,'iva_g')==0) || (strcmp($tituloInt,'total')==0)){
                        $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                    }else{
                        $validacion[$tituloInt]=$valorInt;
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            $fecha = date("Y-m-d");

            $id = $this->atProcesoUtiles->metAprobarAsignacionUtiles($idAsignacion,$validacion['txt_observacion_apr']);
            $validacion['idAsignacion'] = $idAsignacion;

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='aprobado';
                $datos = $this->atProcesoUtiles->metMostrarAsignacionUtiles($idAsignacion);
                $validacion['num_factura'] = $datos['num_factura'];
                $validacion['ind_descripcion_beneficio'] = $datos['ind_descripcion_beneficio'];
                $validacion['nombre_funcionario'] = $datos['nombre_funcionario'];
                $validacion['nombre_carga_familiar'] = $datos['nombre_hijo'];
                $validacion['num_monto'] = number_format($datos['num_monto'],2,',','.');
                echo json_encode($validacion);
                exit;
            }

        }

        /*asigno variables a la vista*/
        $this->atVista->assign('idAsignacion',$idAsignacion);
        $this->atVista->assign('proceso','AprobarAsignacion');
        $this->atVista->metRenderizar('operacionesBeneficioAsignacion','modales');

    }


    #permite buscar los items del datte de la asignacion
    public function metBuscarAsignacionDetalle()
    {
        $idAsignacion = $this->metObtenerInt('idAsignacion');

        $datosAsignacionDetalle = $this->atProcesoUtiles->metMostrarAsignacionUtilesDetalle($idAsignacion);

        echo json_encode($datosAsignacionDetalle);


    }

    #permite visualizar el formulario con los datos de la asignacion para visualizarla
    public function metVerAsignacion()
    {
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'inputmask/mask'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idAsignacion = $this->metObtenerInt('idAsignacion');

        $datosAsignacion = $this->atProcesoUtiles->metMostrarAsignacionUtiles($idAsignacion);
        $this->atVista->assign('formDB',$datosAsignacion);

        $datosAsignacionDetalle = $this->atProcesoUtiles->metMostrarAsignacionUtilesDetalle($idAsignacion);
        $this->atVista->assign('formDBdetalle',$datosAsignacionDetalle);


        $datosOperacionesPR = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesRV = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'RV');
        $this->atVista->assign('formDBoperacionesRV',$datosOperacionesRV);
        $datosOperacionesAP = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);



        $this->atVista->assign('idAsignacion',$idAsignacion);
        $this->atVista->assign('proceso','VerAsignacion');
        $this->atVista->metRenderizar('verAsignacion','modales');



    }


    #permite consultar utiles
    public function metConsultarUtiles()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'

        );

        $js= array('materialSiace/App');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        #identifica la accion que se va a realizar
        $proceso = $this->metObtenerTexto('proceso');
        $accion  = $this->metObtenerTexto('accion');

        $this->atUtilesEscolares  = $this->metCargarModelo('utilesEscolares','maestros');
        $this->atVista->assign('Utiles', $this->atUtilesEscolares->metConsultarUtiles());
        $this->atVista->assign('idMaestro', $this->atUtilesEscolares->metObtenerMaestro());

        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));
        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->metRenderizar('listaUtiles', 'modales');

    }

    /*PERMITE ANULAR UNA ASIGNACION DE UTILES (REVERSO) */
    public function metAnularAsignacion()
    {
        $idBeneficio  = $this->metObtenerInt('idBeneficio');
        $idAsignacion = $this->metObtenerInt('idAsignacion');

        $anular = $this->atProcesoUtiles->metAnularAsignacion($idBeneficio,$idAsignacion);

        if(is_array($anular)){
            $validacion['status'] = 'error';
            $validacion['detalleERROR'] = $anular;
            echo json_encode($validacion);
            exit;
        }else{
            $validacion['status']='anulado';
            echo json_encode($validacion);
            exit;
        }



    }


    //Método que permite cargar las imágenes del archivo digitalizado
    public function metCargarImagen()
    {

        $tipoCarga = $this->metObtenerTexto('tipoCarga');

        $path_tmp = ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'requisitosUtiles'.DS.'tmp'.DS;

        if(strcmp($tipoCarga,'tmp')==0){

            $aleatorio = $this->metObtenerTexto('aleatorio');

            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

                if (isset($_POST["delete"]) && $_POST["delete"] == true) {
                    $name = $_POST["filename"];
                    $path = ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'requisitosUtiles'.DS.'tmp'.DS.$aleatorio.DS;
                    $archivos = scandir($path_tmp.DS.$aleatorio);
                    if (file_exists($path.$name)) {
                        unlink($path.$name);
                        echo json_encode(array("res" => true));
                    } else {
                        echo json_encode(array("res" => false));
                    }
                } else { // para guardar las imagenes
                    //se obtienen los datos de la imagen
                    $file = $_FILES["file"]["name"];
                    $filetype = $_FILES["file"]["type"];
                    $filesize = $_FILES["file"]["size"];

                    if (!is_dir($path_tmp.DS.$aleatorio)){ // si no es un directorio, entonces se crea
                        mkdir($path_tmp.DS.$aleatorio, 0777);
                        chmod($path_tmp.DS.$aleatorio,0777);
                        //se hace el renombrado de la imagen
                        $archivos = scandir($path_tmp.DS.$aleatorio);
                        $cuenta = 1; // contador de imagenes
                        for ($i = 2; $i < count($archivos); $i++) {
                            $ext = explode(".", $archivos[$i]);
                            if ($ext[1] == 'jpg' || $ext[1] == 'png' || $ext[1] == 'gif' || $ext[1] == 'jpeg')
                                $cuenta += 1;
                        }
                    }else{
                        //se hace el renombrado de la imagen
                        $archivos = scandir($path_tmp.DS.$aleatorio);
                        $cuenta = 1; // contador de imagenes del documento
                        for ($i = 2; $i < count($archivos); $i++) {
                            $ext = explode(".", $archivos[$i]);
                            if ($ext[1] == 'jpg' || $ext[1] == 'png' || $ext[1] == 'gif' || $ext[1] == 'jpeg')
                                $cuenta += 1;
                        }
                    }
                    $path_parts = pathinfo("/$file");
                    $extension = NULL;
                    $extension = $path_parts['extension'];
                    $nom_imagen = NULL;
                    /*el nombre de la imagen estara compuesto de la siguiente manera
                    * nombreOriginalImagen_idempleado_$idpersona_$periodo_tmp_$numero_cant_imagen.ext
                    */
                    //$nom_imagen = $_FILES["file"]["name"]."_".$empleado."_".$persona."_".$periodo."_tmp_".$cuenta.".".$extension;
                    $nom_imagen = $_FILES["file"]["name"];
                    if ($file && move_uploaded_file($_FILES["file"]["tmp_name"], $path_tmp.DS.$aleatorio.DS.$nom_imagen)) {
                        chmod($path_tmp.DS.$aleatorio.DS.$nom_imagen,0777);
                    }

                }

            }

        }

    }



    /*INICIO - Método que permite emitir la orden del Beneficio de Utiles*/
    public function metOrdenBeneficioUtiles($idBeneficio)
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');

        $this->metObtenerLibreria('cabeceraRH', 'modRH');
        $this->metObtenerLibreria('montoLetra');
        $pdf = new pdfOrdenUtiles('L','mm',array(216,140));
        $pdf->Open();
        $pdf->AddPage();

        #DATOS CABECERA DEL BENEFICIO
        $datos_c = $this->atProcesoUtiles->metMostrarBeneficioUtiles($idBeneficio);

        #DATOS BENEFICIARIOS DEL BENEFICIO
        $datos_b = $this->atProcesoUtiles->metMostrarBeneficiarios($idBeneficio);

        #CONSULTO LA DEPENDENCIA DEL EMPLEADO
        $this->atDependencias = $this->metCargarModelo('dependencias','maestros');

        $dependencia = $this->atDependencias->metEmpleadoDependencia($datos_c['fk_rhb001_num_empleado']);

        foreach($datos_b as $carga){

            $pdf->SetFont('Courier', 'B', 12);
            $pdf->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
            $pdf->SetXY(33, 10);$pdf->Cell(130,5,utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
            $pdf->Cell(15,5,'Fecha: '.date('d/m/Y'),0,0,'');
            $pdf->SetXY(33, 15);$pdf->Cell(130,5, utf8_decode('Dirección de Recursos Humanos'), 0, 0, 'L');
            $pdf->Cell(15,5,utf8_decode('Año: '.date('Y')),0,1,'L');
            $pdf->SetXY(33, 20);$pdf->Cell(130,5, 'Beneficio de Utiles Escolares', 0, 0, 'L');$pdf->Cell(15,5,date('h:i a'),0,0,'L');
            $pdf->SetXY(33, 25);$pdf->Cell(130,5, utf8_decode('Institución: '.$datos_c['proveedor']), 0, 0, 'L');
            $pdf->Cell(15,5,utf8_decode('Nro. Beneficio: '.$idBeneficio),0,1,'L');

            $pdf->Ln();
            $monto = number_format($datos_c['num_monto'],2,',','.');
            $pdf->Ln(5);
            $pdf->SetFont('Courier', 'B', 12);
            $objMontoLetra = new EnLetras();
            #Monto
            $pdf->Cell(200, 5, 'Monto (Bs.): '.$monto, 0, 1, 'L');
            $pdf->SetFont('Courier', 'B', 9);
            $pdf->Cell(200, 5, $objMontoLetra->ValorEnLetras($monto,'BOLIVARES CON') , 0, 1, 'L');
            $pdf->Cell(200, 5, utf8_decode('Atención: '), 0, 1, 'L');
            $pdf->SetFont('Courier', 'B', 10);
            $pdf->Cell(200, 5, '<< A G R A D E Z C O     A T E N D E R     A >>', 0, 1, 'C');

            $pdf->Ln(10);
            $pdf->SetFont('Courier', 'B', 9);
            $pdf->Cell(260, 5, utf8_decode('Ciudadano (a): '.$carga['nombre_hijo']), 0, 1, 'L');
            $pdf->Cell(260, 5, utf8_decode('Parentesco: HIJO del FUNCIONARIO - '.$datos_c['ind_cedula_documento'].' - '.$datos_c['nombre_empleado']), 0, 1, 'L');
            $pdf->Cell(260, 5, utf8_decode('Adscrito al Departamento de: '.$dependencia['ind_dependencia']), 0, 1, 'L');
            $pdf->Cell(260, 5, utf8_decode('Quien quiere de sus servicios en lo referente a: (VER ANEXO)'), 0, 1, 'L');


            $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Courier', 'B', 8);
            $pdf->SetXY(10, 75);
            $pdf->Cell(186, 20, '', 0, 1, 'J');
            $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
            $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
            $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
            $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
            $pdf->SetXY(10, 109);

            #PARA OBTENER LOS DATOS DE LA PERSONA QUE REALIZO EL REGISTRO (PREPARADO)
            $datos_p = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'PR');

            #DIRECTOR DE RECURSOS HUMANOS
            $codInternoRH = Session::metObtener('RHDEP');//codigo interno Recursos Humanos
            $d_rh = $this->atDependencias->metResponsableDependencia($codInternoRH);
            #DIRECTOR DE ADMINISTRACION
            $codInternoAD = Session::metObtener('ADDEP');//Codigo interno Administracion
            $d_ad = $this->atDependencias->metResponsableDependencia($codInternoAD);

            $pdf->Cell(50, 5, 'Elaborado Por:', 0, 0, 'C', 0);
            $pdf->Cell(50, 5, 'Dir. RRHH:', 0, 0, 'C', 0);
            $pdf->Cell(50, 5, 'Control Interno:', 0, 0, 'C', 0);
            $pdf->Cell(50, 5, utf8_decode('Dir. Administración:'), 0, 0, 'C', 0);
            $pdf->Ln();
            $pdf->Cell(50, 1, utf8_decode($datos_p[0]['nombre_operacion']), 0, 0, 'C', 0);
            $pdf->Cell(50, 1, utf8_decode($d_rh['responsable']), 0, 0, 'C', 0);
            $pdf->Cell(50, 1, '',0, 0, 'C', 0);
            $pdf->Cell(50, 1, utf8_decode($d_ad['responsable']), 0, 0, 'C', 0);

        }

        $pdf->Output();

    }
    /*FIN - Método que permite emitir la orden del Beneficio de Utiles*/


    /*INICIO - Método que permite imprimir el  Beneficio de Utiles cuando es por asignacion directa*/
    public function metImprimirBeneficioUtiles($idBeneficio)
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');

        $this->metObtenerLibreria('cabeceraRH', 'modRH');
        $pdf = new pdfOrdenUtiles('P','mm','Letter');
        $pdf->Open();
        $pdf->AddPage();

        #DATOS CABECERA DEL BENEFICIO
        $datos_c = $this->atProcesoUtiles->metMostrarBeneficioUtiles($idBeneficio);

        #DATOS BENEFICIARIOS DEL BENEFICIO
        $datos_b = $this->atProcesoUtiles->metMostrarBeneficiarios($idBeneficio);

        #CONSULTO LA DEPENDENCIA DEL EMPLEADO
        $this->atDependencias = $this->metCargarModelo('dependencias','maestros');

        $dependencia = $this->atDependencias->metEmpleadoDependencia($datos_c['fk_rhb001_num_empleado']);

        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $pdf->SetXY(33, 10);$pdf->Cell(130,5,utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $pdf->Cell(15,5,'Fecha: '.date('d/m/Y'),0,0,'');
        $pdf->SetXY(33, 15);$pdf->Cell(130,5, utf8_decode('Dirección de Recursos Humanos'), 0, 0, 'L');
        $pdf->Cell(15,5,utf8_decode('Año: '.date('Y')),0,1,'L');
        $pdf->SetXY(33, 20);$pdf->Cell(130,5, 'Beneficio de Utiles Escolares', 0, 0, 'L');$pdf->Cell(15,5,date('h:i a'),0,0,'L');
        $pdf->SetXY(33, 25);$pdf->Cell(15,5,utf8_decode('Nro. Beneficio: '.$idBeneficio),0,1,'L');

        $aux = explode("-",$datos_c['fec_fecha']);


        $pdf->Ln(10);
        $pdf->Cell(200, 5, utf8_decode('BENEFICIO DE ÚTILES ESCOLARES Y UNIFROMES'), 0, 1, 'C');
        $pdf->Ln(5);$pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(100, 5, utf8_decode('Periodo: '.$aux[0]."-".$aux[1]), 1, 0, 'L');
        $pdf->Cell(100, 5, utf8_decode('Fecha de Emisión: '.$aux[2]."-".$aux[1]."-".$aux[0]), 1, 0, 'L');
        $pdf->Ln(20);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(200, 5, utf8_decode('FUNCIONARIO'),0, 1, 'C');
        $pdf->Ln(5);$pdf->SetFont('Arial', 'B', 10);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFillColor(200,220,255);
        $pdf->Cell(100, 5, utf8_decode('NOMBRES Y APELLIDOS'), 1, 0, 'C',true);
        $pdf->Cell(100, 5, utf8_decode('CEDULA DE IDENTIDAD'), 1, 1, 'C',true);
        $pdf->Cell(100, 5, utf8_decode($datos_c['nombre_empleado']), 1, 0, 'C');
        $pdf->Cell(100, 5, utf8_decode($datos_c['ind_cedula_documento']), 1, 1, 'C');
        $pdf->Cell(200, 5, utf8_decode('UBICACIÓN ADMINISTRATIVA'), 1, 1, 'C',true);
        $pdf->Cell(200, 5, utf8_decode($datos_c['ind_dependencia']), 1, 1, 'C');
        $pdf->Ln(20);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(200, 5, utf8_decode('BENEFICIARIOS'),0, 1, 'C');
        $pdf->Ln(5);$pdf->SetFont('Arial', 'B', 10);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFillColor(200,220,255);
        $pdf->Cell(150, 5, utf8_decode('NOMBRES Y APELLIDOS'), 1, 0, 'C',true);
        $pdf->Cell(50, 5, utf8_decode('MONTO'), 1, 1, 'C',true);
        $pdf->SetFont('Arial', 'B', 10);
        $total=0;

        foreach($datos_b as $carga){

            $total = $total + $carga['num_monto_asignado'];

            $pdf->Cell(150, 5, utf8_decode($carga['nombre_hijo']), 1, 0, 'C');
            $pdf->Cell(50, 5, utf8_decode(number_format($carga['num_monto_asignado'],2,',','.')), 1, 1, 'R');

        }

        $pdf->Cell(150, 5, 'TOTAL ', 1, 0, 'R');
        $pdf->Cell(50, 5, number_format($total,2,',','.'), 1, 1, 'R');


        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetXY(10, 210);
        $pdf->Cell(186, 20, '', 0, 1, 'J');
        $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
        $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
        $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
        $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
        $pdf->SetXY(10, 244);

        #PARA OBTENER LOS DATOS DE LA PERSONA QUE REALIZO EL REGISTRO (PREPARADO)
        $datos_p = $this->atProcesoUtiles->metMostrarBeneficiosOperaciones($idBeneficio,'PR');

        #DIRECTOR DE RECURSOS HUMANOS
        $codInternoRH = Session::metObtener('RHDEP');//codigo interno Recursos Humanos
        $d_rh = $this->atDependencias->metResponsableDependencia($codInternoRH);
        #DIRECTOR DE ADMINISTRACION
        $codInternoAD = Session::metObtener('ADDEP');//Codigo interno Administracion
        $d_ad = $this->atDependencias->metResponsableDependencia($codInternoAD);

        $pdf->Cell(50, 5, 'Elaborado Por:', 0, 0, 'C', 0);
        $pdf->Cell(50, 5, 'Dir. RRHH:', 0, 0, 'C', 0);
        $pdf->Cell(50, 5, 'Control Interno:', 0, 0, 'C', 0);
        $pdf->Cell(50, 5, utf8_decode('Dir. Administración:'), 0, 0, 'C', 0);
        $pdf->Ln();
        $pdf->Cell(50, 1, utf8_decode($datos_p[0]['nombre_operacion']), 0, 0, 'C', 0);
        $pdf->Cell(50, 1, utf8_decode($d_rh['responsable']), 0, 0, 'C', 0);
        $pdf->Cell(50, 1, '',0, 0, 'C', 0);
        $pdf->Cell(50, 1, utf8_decode($d_ad['responsable']), 0, 0, 'C', 0);


        $pdf->Output();
    }
    /*FIN - Método que permite imprimir el  Beneficio de Utiles cuando es por asignacion directa*/


    /*INICIO - Método que permite imprimir la asignacion de  Utiles*/
    public function metImprimirAsignacionUtiles($idAsignacion)
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');

        $this->metObtenerLibreria('cabeceraRH', 'modRH');
        $pdf = new pdfOrdenUtiles('P','mm','Letter');
        $pdf->Open();
        $pdf->AddPage();

        #DATOS CABECERA DE LA ASIGNACION
        $datos_c = $this->atProcesoUtiles->metMostrarAsignacionUtiles($idAsignacion);

        #DATOS DETALLE DE LA ASIGNACION
        $datos_b = $this->atProcesoUtiles->metMostrarAsignacionUtilesDetalle($idAsignacion);

        #CONSULTO LA DEPENDENCIA DEL EMPLEADO
        $this->atDependencias = $this->metCargarModelo('dependencias','maestros');

        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $pdf->SetXY(33, 10);$pdf->Cell(130,5,utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $pdf->Cell(15,5,'Fecha: '.date('d/m/Y'),0,0,'');
        $pdf->SetXY(33, 15);$pdf->Cell(130,5, utf8_decode('Dirección de Recursos Humanos'), 0, 0, 'L');
        $pdf->Cell(15,5,utf8_decode('Año: '.date('Y')),0,1,'L');
        $pdf->SetXY(33, 20);$pdf->Cell(130,5, utf8_decode('Asignación de Utiles Escolares'), 0, 0, 'L');$pdf->Cell(15,5,date('h:i a'),0,0,'L');
        $pdf->SetXY(33, 25);$pdf->Cell(15,5,utf8_decode('Nro. Asignación: '.$idAsignacion),0,1,'L');

        $aux = explode("-",$datos_c['fec_fecha']);


        $pdf->Ln(20);
        $pdf->Cell(200, 5, utf8_decode('ASIGNACIÓN DE ÚTILES ESCOLARES'), 0, 1, 'C');
        $pdf->Ln(5);$pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(100, 5, utf8_decode('Periodo: '.$aux[0]."-".$aux[1]), 1, 0, 'L');
        $pdf->Cell(100, 5, utf8_decode('Fecha de Emisión: '.$aux[2]."-".$aux[1]."-".$aux[0]), 1, 0, 'L');
        $pdf->Ln(25);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(200, 5, utf8_decode('FUNCIONARIO'),0, 1, 'C');
        $pdf->Ln(5);$pdf->SetFont('Arial', 'B', 10);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFillColor(200,220,255);
        $pdf->Cell(100, 5, utf8_decode('NOMBRES Y APELLIDOS'), 1, 0, 'C',true);
        $pdf->Cell(100, 5, utf8_decode('CEDULA DE IDENTIDAD'), 1, 1, 'C',true);
        $pdf->Cell(100, 5, utf8_decode($datos_c['nombre_funcionario']), 1, 0, 'C');
        $pdf->Cell(100, 5, utf8_decode($datos_c['ind_cedula_documento']), 1, 1, 'C');
        $pdf->Cell(200, 5, utf8_decode('UBICACIÓN ADMINISTRATIVA'), 1, 1, 'C',true);
        $pdf->Cell(200, 5, utf8_decode($datos_c['ind_dependencia']), 1, 1, 'C');
        $pdf->Ln(25);
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(200, 5, utf8_decode('BENEFICIARIOS'),0, 1, 'C');
        $pdf->Ln(5);$pdf->SetFont('Arial', 'B', 10);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFillColor(200,220,255);
        $pdf->Cell(200, 5, utf8_decode('NOMBRES Y APELLIDOS'), 1, 1, 'C',true);
        $pdf->Cell(200, 5, utf8_decode($datos_c['nombre_hijo']), 1, 0, 'C');
        $pdf->SetFont('Arial', 'B', 10);

        $pdf->AddPage();


        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Image(ROOT.'publico'.DS.'imagenes'.DS.'modRH'.DS.'logo.png', 10, 10, 22);
        $pdf->SetXY(33, 10);$pdf->Cell(130,5,utf8_decode('CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $pdf->Cell(15,5,'Fecha: '.date('d/m/Y'),0,0,'');
        $pdf->SetXY(33, 15);$pdf->Cell(130,5, utf8_decode('Dirección de Recursos Humanos'), 0, 0, 'L');
        $pdf->Cell(15,5,utf8_decode('Año: '.date('Y')),0,1,'L');
        $pdf->SetXY(33, 20);$pdf->Cell(130,5, utf8_decode('Asignación de Utiles Escolares'), 0, 0, 'L');$pdf->Cell(15,5,date('h:i a'),0,0,'L');
        $pdf->SetXY(33, 25);$pdf->Cell(15,5,utf8_decode('Nro. Asignación: '.$idAsignacion),0,1,'L');

        $pdf->Ln(15);
        $pdf->Cell(200, 5, utf8_decode('DETALLE DE LA ASIGNACIÓN'), 0, 1, 'C');

        $pdf->Ln(5);$pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(100, 5, utf8_decode('Factura: '.$aux[0]."-".$aux[1]), 1, 0, 'L');
        $pdf->Cell(100, 5, utf8_decode('Fecha de Emisión: '.$aux[2]."-".$aux[1]."-".$aux[0]), 1, 1, 'L');

        $pdf->Ln(5);
        $pdf->SetDrawColor(0,0,0);
        $pdf->SetFillColor(200,220,255);
        $pdf->Cell(20, 5, '#', 1, 0, 'C',true);
        $pdf->Cell(130, 5, 'Utiles', 1, 0, 'L',true);
        $pdf->Cell(20, 5, 'Cantidad', 1, 0, 'C',true);
        $pdf->Cell(30, 5, 'Monto', 1, 1, 'R',true);

        $total=0;
        $i = 1;
        foreach($datos_b as $detalle){
            $pdf->SetFont('Arial', '', 7);
            $pdf->Cell(20, 5, $i, 1, 0, 'C');
            $pdf->Cell(130, 5, utf8_decode($detalle['ind_descripcion_utiles']), 1, 0, 'L');
            $pdf->Cell(20, 5, utf8_decode($detalle['num_cantidad']), 1, 0, 'C');
            $pdf->Cell(30, 5, number_format($detalle['nom_monto'],2,',','.'), 1, 1, 'R');
            $i++;
        }

        $pdf->Ln(5);
        $pdf->Cell(50, 5, 'Exento: '.number_format($datos_c['num_excento'],2,',','.'), 1, 0, 'C');
        $pdf->Cell(50, 5, 'BI G 12%: '.number_format($datos_c['num_base_imponible'],2,',','.'), 1, 0, 'C');
        $pdf->Cell(50, 5, 'IVA G 12%: '.number_format($datos_c['num_iva'],2,',','.'), 1, 0, 'C');
        $pdf->Cell(50, 5, 'TOTAL: '.number_format($datos_c['num_monto'],2,',','.'), 1, 1, 'C');



        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetXY(10, 210);
        $pdf->Cell(186, 20, '', 0, 1, 'J');
        $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
        $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
        $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
        $pdf->Cell(50, 15, '', 1, 0, 'C', 0);
        $pdf->SetXY(10, 244);


        #PARA OBTENER LOS DATOS DE LA PERSONA QUE REALIZO EL REGISTRO (PREPARADO)
        $datos_p = $this->atProcesoUtiles->metMostrarAsignacionOperaciones($idAsignacion,'PR');

        #DIRECTOR DE RECURSOS HUMANOS
        $codInternoRH = Session::metObtener('RHDEP');//codigo interno Recursos Humanos
        $d_rh = $this->atDependencias->metResponsableDependencia($codInternoRH);
        #DIRECTOR DE ADMINISTRACION
        $codInternoAD = Session::metObtener('ADDEP');//Codigo interno Administracion
        $d_ad = $this->atDependencias->metResponsableDependencia($codInternoAD);

        $pdf->Cell(50, 5, 'Elaborado Por:', 0, 0, 'C', 0);
        $pdf->Cell(50, 5, 'Dir. RRHH:', 0, 0, 'C', 0);
        $pdf->Cell(50, 5, 'Control Interno:', 0, 0, 'C', 0);
        $pdf->Cell(50, 5, utf8_decode('Dir. Administración:'), 0, 0, 'C', 0);
        $pdf->Ln();
        $pdf->Cell(50, 1,utf8_decode($datos_p[0]['nombre_operacion']), 0, 0, 'C', 0);
        $pdf->Cell(50, 1, utf8_decode($d_rh['responsable']), 0, 0, 'C', 0);
        $pdf->Cell(50, 1, '',0, 0, 'C', 0);
        $pdf->Cell(50, 1, utf8_decode($d_ad['responsable']), 0, 0, 'C', 0);


        $pdf->Output();
    }
    /*FIN - Método que permite imprimir la asignacion de  Utiles*/


}