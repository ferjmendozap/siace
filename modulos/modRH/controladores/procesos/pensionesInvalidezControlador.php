<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: RECURSOS HUMANOS
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.ait.programador1@cgesucre.gob.ve  | 0424-8942068
 * | 2 |
 * |_____________________________________________________________________________________
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-02-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************/
class pensionesInvalidezControlador extends Controlador
{
    private $atEmpleados;
    private $atIdUsuario;
    private $atPensionesInvalidez;
    private $atOrganismo;
    private $atDependencias;
    private $atTipoNimina;
    private $atMotivoCese;
    private $atCargo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEmpleados    = $this->metCargarModelo('empleados','gestion');
        $this->atPensionesInvalidez = $this->metCargarModelo('pensionesInvalidez','procesos');
        $this->atOrganismo    = $this->metCargarModelo('organismo','maestros','modCV');
        $this->atDependencias = $this->metCargarModelo('dependencias','maestros');
        $this->atMotivoCese   = $this->metCargarModelo('motivoCese','maestros');
        $this->atTipoNimina   = $this->metCargarModelo('tipoNomina','maestros','modNM');
        $this->atCargo        = $this->metCargarModelo('cargos','maestros');
        $this->atIdUsuario    = Session::metObtener('idUsuario');

    }

    #Metodo Index del controlador ceseReingreso.
    public function metIndex()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listadoOrganismos',$this->atOrganismo->metListarOrganismo());
        $this->atVista->assign('listadoDependencias',$this->atDependencias->metConsultarDependencias());
        $Empleados = $this->atEmpleados->metListarEmpleado('AP','operaciones','1','1');
        $this->atVista->assign('Empleado',$Empleados);
        $EstadoRegistro = array('PR'=>'PREPARADO','CO'=>'CONFORMADO','AP'=>'APROBADO','AN'=>'ANULADO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $this->atVista->assign('proceso','Listado');
        $this->atVista->assign('listadoPensionesInvalidez', $this->atPensionesInvalidez->metListarPensionesInvalidez('PR'));
        $this->atVista->metRenderizar('listado');
    }


    public function metRegistrar()
    {
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idPensionInvalidez = $this->metObtenerInt('idPensionInvalidez');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1) {

            $formTxt   = $this->metObtenerTexto('form','txt');
            $formInt   = $this->metObtenerInt('form','int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_ultimo_sueldo')==0) || (strcmp($tituloInt,'num_monto_pension')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }


            if(!isset($validacion['txt_observacion_pre'])){
                $validacion['txt_observacion_pre']=NULL;
            }
            if(empty($validacion['fk_rhc032_num_motivo_cese'])){
                $validacion['fk_rhc032_num_motivo_cese']=NULL;
            }

            #array de datos
            $datos = array(
                /*datos para tabla rh_b003_pension*/
                $validacion['fk_rhb001_num_empleado'],
                $validacion['fk_a001_num_organismo'],
                $validacion['ind_organismo'],
                $validacion['fk_a004_num_dependencia'],
                $validacion['ind_dependencia'],
                $validacion['fk_rhc063_num_puestos'],
                $validacion['ind_cargo'],
                $validacion['num_ultimo_sueldo'],
                $validacion['num_anio_servicio'],
                $validacion['num_edad'],
                $validacion['fec_fecha'],
                $validacion['fec_fecha_ingreso'],
                $validacion['fk_a006_num_miscelaneo_detalle_tipopension'],
                $validacion['ind_detalle_tipopension'],
                $validacion['fk_a006_num_miscelaneo_detalle_motivopension'],
                $validacion['ind_detalle_motivopension'],
                /*datos para tabla rh_c034_invalidez*/
                $validacion['num_monto_pension'],
                $validacion['ind_periodo'],
                $validacion['fk_nmb001_num_tipo_nomina'],
                $validacion['fk_a006_num_miscelaneo_detalle_tipotrab'],
                $validacion['num_situacion_trabajo'],
                $validacion['fk_rhc032_num_motivo_cese'],
                $validacion['fec_fecha_egreso'],
                $validacion['txt_observacion_egreso'],
                $validacion['ind_resolucion'],
                $validacion['txt_observacion_pre']
            );


            $id = $this->atPensionesInvalidez->metRegistrarPensionInvalidez($datos);

            if(is_array($id)){
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;
            }else{
                $validacion['status']='registrar';
                echo json_encode($validacion);
                exit;
            }

        }

        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $usuario = Session::metObtener('idUsuario');
        $obtenerEmpleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);

        $this->atVista->assign('estadoEmpleado',$datosEmpleado);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual


        $miscMotivoPension = $this->atEmpleados->metMostrarSelect('MOTIPEN');
        $this->atVista->assign('MotivoPension',$miscMotivoPension);

        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        $this->atVista->assign('idPensionInvalidez',$idPensionInvalidez);
        $this->atVista->assign('proceso','Nuevo');
        $this->atVista->metRenderizar('nuevaPension','modales');
    }


    // Método que permite buscar un empleado desde el wizard
    public function metConsultarEmpleado()
    {

        $proceso = $this->metObtenerTexto('proceso');

        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];

        $listadoOrganismo = $this->atOrganismo->metListarOrganismo();

        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);
        $pkNumDependencia = $datosEmpleado['pk_num_dependencia'];

        $listadoEmpleados = $this->atDependencias->metDependenciaEmpleado($pkNumDependencia);
        $this->atVista->assign('empleado', $listadoEmpleados);

        $listadoDependencia = $this->atDependencias->metListarDependenciaOrganismo($datosEmpleado['pk_num_organismo'], 1, 0);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        // Organismo donde trabaja el empleado
        $empleado = array(
            'pk_num_organismo' => $datosEmpleado['pk_num_organismo'],
            'pk_num_dependencia' => $pkNumDependencia
        );
        $datosTrabajador = $this->atPensionesInvalidez->metBuscarTrabajador($datosEmpleado['pk_num_organismo'], $pkNumDependencia, '', '', '', '', '', '', '');

        $centroCosto = $this->atPensionesInvalidez->metListarCentroCosto();
        $tipoNomina = $this->atTipoNimina->metListarTipoNomina();
        $tipoTrabajador = $this->atEmpleados->metMostrarSelect('TIPOTRAB');
        // Envío a la vista
        $this->atVista->assign('datosTrabajador', $datosTrabajador);
        $this->atVista->assign('centroCosto', $centroCosto);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('nomina', $tipoNomina);
        $this->atVista->assign('tipoTrabajador', $tipoTrabajador);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('proceso',$proceso);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));
        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->metRenderizar('buscarEmpleado', 'modales');

    }

    // Método que permite listar empleados de acuerdo a la busqueda en nueva pension
    public function metBuscarTrabajador()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $pkNumDependencia = $_POST['pk_num_dependencia'];
        $centroCosto = $_POST['centro_costo'];
        $tipoNomina = $_POST['tipo_nomina'];
        $tipoTrabajador = $_POST['tipo_trabajador'];
        $estado_registro = $_POST['estado_registro'];
        $situacionTabajo = $_POST['situacion_trabajo'];
        $fechaInicio = $_POST['fecha_inicio'];
        $fecha_fin = $_POST['fecha_fin'];
        $datosTrabajador = $this->atPensionesInvalidez->metBuscarTrabajador($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $tipoTrabajador, $estado_registro, $situacionTabajo, $fechaInicio, $fecha_fin);
        echo json_encode($datosTrabajador);
    }

    // Método que permite cargar a la solicitud el empleado seleccionado
    public function metCargarEmpleado()
    {
        $pkNumEmpleado = $_POST['pk_num_empleado'];
        $empleado = $this->atJubilacionModelo->metEmpleado($pkNumEmpleado);
        echo json_encode($empleado);
    }


    public function metModificar()
    {
        /**********************************************************************/
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idPensionInvalidez = $this->metObtenerInt('idPensionInvalidez');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1) {

            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_ultimo_sueldo')==0) || (strcmp($tituloInt,'num_monto_pension')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }


            if($idPensionInvalidez != 0){

                if(!isset($validacion['txt_observacion_pre'])){
                    $validacion['txt_observacion_pre']=NULL;
                }
                if(empty($validacion['fk_rhc032_num_motivo_cese'])){
                    $validacion['fk_rhc032_num_motivo_cese']="NULL";
                }

                #array de datos
                $datos = array(
                    /*datos para tabla rh_b003_pension*/
                    $validacion['fk_rhb001_num_empleado'],
                    $validacion['fk_a001_num_organismo'],
                    $validacion['ind_organismo'],
                    $validacion['fk_a004_num_dependencia'],
                    $validacion['ind_dependencia'],
                    $validacion['fk_rhc063_num_puestos'],
                    $validacion['ind_cargo'],
                    $validacion['num_ultimo_sueldo'],
                    $validacion['num_anio_servicio'],
                    $validacion['num_edad'],
                    $validacion['fec_fecha'],
                    $validacion['fec_fecha_ingreso'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipopension'],
                    $validacion['ind_detalle_tipopension'],
                    $validacion['fk_a006_num_miscelaneo_detalle_motivopension'],
                    $validacion['ind_detalle_motivopension'],
                    /*datos para tabla rh_c034_invalidez*/
                    $validacion['num_monto_pension'],
                    $validacion['ind_periodo'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipotrab'],
                    $validacion['num_situacion_trabajo'],
                    $validacion['fk_rhc032_num_motivo_cese'],
                    $validacion['fec_fecha_egreso'],
                    $validacion['txt_observacion_egreso'],
                    $validacion['ind_resolucion'],
                    $validacion['txt_observacion_pre']
                );

                $resultado = $this->atPensionesInvalidez->metModificarPensionesInvalidez($idPensionInvalidez,$datos);

                if(is_array($resultado)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $resultado;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='modificar';
                    echo json_encode($validacion);
                    exit;
                }

            }

        }

        /***********************************************************************/
        $datosBD = $this->atPensionesInvalidez->metMostrarPernsionesInvalidez($idPensionInvalidez);
        $this->atVista->assign('formDB',$datosBD);
        $datosOperacionesPR = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesCO = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);
        /**********************************************************************/


        /*************************************************************************/
        $resultado = $this->atPensionesInvalidez->metObtenerDatosEmpleado($datosBD['fk_rhb001_num_empleado']);

        $f_n    = explode("-",$resultado['fec_nacimiento']);
        $f_i    = explode("-",$resultado['fec_ingreso']);
        $f_a    = date("d-m-Y");
        #edad y años de servicio
        list($edad, $EdadMeses, $EdadDias) = $this->metCalcularEdadAnioServicio($f_n[2]."-".$f_n[1]."-".$f_n[0],$f_a);
        list($anio_serv, $MesesServicio, $DiasServicio) = $this->metCalcularEdadAnioServicio($f_i[2]."-".$f_i[1]."-".$f_i[0],$f_a);

        $this->atVista->assign('edad',$edad);
        $this->atVista->assign('anio_servicio',$anio_serv);
        $this->atVista->assign('fec_ingreso',$resultado['fec_ingreso']);
        /*************************************************************************/

        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $usuario = Session::metObtener('idUsuario');
        $obtenerEmpleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);

        $this->atVista->assign('estadoEmpleado',$datosEmpleado);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual


        $miscMotivoPension = $this->atEmpleados->metMostrarSelect('MOTIPEN');
        $this->atVista->assign('MotivoPension',$miscMotivoPension);

        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        $this->atVista->assign('idPensionInvalidez',$idPensionInvalidez);
        $this->atVista->assign('proceso','Modificar');
        $this->atVista->metRenderizar('nuevaPension','modales');

    }

    public function metVer()
    {
        /**********************************************************************/
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idPensionInvalidez = $this->metObtenerInt('idPensionInvalidez');

        /***********************************************************************/
        $datosBD = $this->atPensionesInvalidez->metMostrarPernsionesInvalidez($idPensionInvalidez);
        $this->atVista->assign('formDB',$datosBD);
        $datosOperacionesPR = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesCO = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);
        /**********************************************************************/


        /*************************************************************************/
        $resultado = $this->atPensionesInvalidez->metObtenerDatosEmpleado($datosBD['fk_rhb001_num_empleado']);

        $f_n    = explode("-",$resultado['fec_nacimiento']);
        $f_i    = explode("-",$resultado['fec_ingreso']);
        $f_a    = date("d-m-Y");
        #edad y años de servicio
        list($edad, $EdadMeses, $EdadDias) = $this->metCalcularEdadAnioServicio($f_n[2]."-".$f_n[1]."-".$f_n[0],$f_a);
        list($anio_serv, $MesesServicio, $DiasServicio) = $this->metCalcularEdadAnioServicio($f_i[2]."-".$f_i[1]."-".$f_i[0],$f_a);

        $this->atVista->assign('edad',$edad);
        $this->atVista->assign('anio_servicio',$anio_serv);
        $this->atVista->assign('fec_ingreso',$resultado['fec_ingreso']);
        /*************************************************************************/




        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $usuario = Session::metObtener('idUsuario');
        $obtenerEmpleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);

        $this->atVista->assign('estadoEmpleado',$datosEmpleado);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual


        $miscMotivoPension = $this->atEmpleados->metMostrarSelect('MOTIPEN');
        $this->atVista->assign('MotivoPension',$miscMotivoPension);

        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        $this->atVista->assign('idPensionInvalidez',$idPensionInvalidez);
        $this->atVista->assign('proceso','Ver');
        $this->atVista->metRenderizar('nuevaPension','modales');

    }

    public function metAnular()
    {
        /**********************************************************************/
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idPensionInvalidez = $this->metObtenerInt('idPensionInvalidez');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_ultimo_sueldo')==0) || (strcmp($tituloInt,'num_monto_pension')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if (!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idPensionInvalidez != 0){


                $resultado = $this->atPensionesInvalidez->metAnularPensionesInvalidez($idPensionInvalidez,$validacion['txt_observacion_anulado']);

                if(is_array($resultado)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $resultado;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='anular';
                    echo json_encode($validacion);
                    exit;
                }

            }
        }

        /***********************************************************************/
        $datosBD = $this->atPensionesInvalidez->metMostrarPernsionesInvalidez($idPensionInvalidez);
        $this->atVista->assign('formDB',$datosBD);
        $datosOperacionesPR = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesCO = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);
        /**********************************************************************/


        /*************************************************************************/
        $resultado = $this->atPensionesInvalidez->metObtenerDatosEmpleado($datosBD['fk_rhb001_num_empleado']);

        $f_n    = explode("-",$resultado['fec_nacimiento']);
        $f_i    = explode("-",$resultado['fec_ingreso']);
        $f_a    = date("d-m-Y");
        #edad y años de servicio
        list($edad, $EdadMeses, $EdadDias) = $this->metCalcularEdadAnioServicio($f_n[2]."-".$f_n[1]."-".$f_n[0],$f_a);
        list($anio_serv, $MesesServicio, $DiasServicio) = $this->metCalcularEdadAnioServicio($f_i[2]."-".$f_i[1]."-".$f_i[0],$f_a);

        $this->atVista->assign('edad',$edad);
        $this->atVista->assign('anio_servicio',$anio_serv);
        $this->atVista->assign('fec_ingreso',$resultado['fec_ingreso']);
        /*************************************************************************/

        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $usuario = Session::metObtener('idUsuario');
        $obtenerEmpleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);

        $this->atVista->assign('estadoEmpleado',$datosEmpleado);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual


        $miscMotivoPension = $this->atEmpleados->metMostrarSelect('MOTIPEN');
        $this->atVista->assign('MotivoPension',$miscMotivoPension);

        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        $this->atVista->assign('idPensionInvalidez',$idPensionInvalidez);
        $this->atVista->assign('proceso','Anular');
        $this->atVista->metRenderizar('nuevaPension','modales');

    }


    public function metConformar()
    {
        /**********************************************************************/
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idPensionInvalidez = $this->metObtenerInt('idPensionInvalidez');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_ultimo_sueldo')==0) || (strcmp($tituloInt,'num_monto_pension')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if (!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idPensionInvalidez != 0){


                $resultado = $this->atPensionesInvalidez->metConformarPensionesInvalidez($idPensionInvalidez,$validacion['txt_observacion_con']);

                if(is_array($resultado)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $resultado;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='conformado';
                    echo json_encode($validacion);
                    exit;
                }
            }
        }

        /***********************************************************************/
        $datosBD = $this->atPensionesInvalidez->metMostrarPernsionesInvalidez($idPensionInvalidez);
        $this->atVista->assign('formDB',$datosBD);
        $datosOperacionesPR = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesCO = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);
        /**********************************************************************/


        /*************************************************************************/
        $resultado = $this->atPensionesInvalidez->metObtenerDatosEmpleado($datosBD['fk_rhb001_num_empleado']);

        $f_n    = explode("-",$resultado['fec_nacimiento']);
        $f_i    = explode("-",$resultado['fec_ingreso']);
        $f_a    = date("d-m-Y");
        #edad y años de servicio
        list($edad, $EdadMeses, $EdadDias) = $this->metCalcularEdadAnioServicio($f_n[2]."-".$f_n[1]."-".$f_n[0],$f_a);
        list($anio_serv, $MesesServicio, $DiasServicio) = $this->metCalcularEdadAnioServicio($f_i[2]."-".$f_i[1]."-".$f_i[0],$f_a);

        $this->atVista->assign('edad',$edad);
        $this->atVista->assign('anio_servicio',$anio_serv);
        $this->atVista->assign('fec_ingreso',$resultado['fec_ingreso']);
        /*************************************************************************/

        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $usuario = Session::metObtener('idUsuario');
        $obtenerEmpleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);

        $this->atVista->assign('estadoEmpleado',$datosEmpleado);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual


        $miscMotivoPension = $this->atEmpleados->metMostrarSelect('MOTIPEN');
        $this->atVista->assign('MotivoPension',$miscMotivoPension);

        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        $this->atVista->assign('idPensionInvalidez',$idPensionInvalidez);
        $this->atVista->assign('proceso','Conformar');
        $this->atVista->metRenderizar('nuevaPension','modales');

    }


    public function metAprobar()
    {
        /**********************************************************************/
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','materialSiace/core/demo/DemoFormWizard');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);


        $idPensionInvalidez = $this->metObtenerInt('idPensionInvalidez');
        $valido  = $this->metObtenerInt('valido');

        if($valido==1){

            $formTxt = $this->metObtenerTexto('form', 'txt');
            $formInt = $this->metObtenerInt('form', 'int');

            if(!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        if((strcmp($tituloInt,'num_ultimo_sueldo')==0) || (strcmp($tituloInt,'num_monto_pension')==0)){
                            $validacion[$tituloInt]=str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $valorInt));
                        }else{
                            $validacion[$tituloInt]=$valorInt;
                        }
                    } else {
                        $validacion[$tituloInt] = '';
                    }
                }
            }
            if (!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt])) {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = '';

                    }
                }
            }

            if($idPensionInvalidez != 0){

                #array de datos
                $datos = array(
                    /*datos para tabla rh_b003_pension*/
                    $validacion['fk_rhb001_num_empleado'],
                    $validacion['fk_nmb001_num_tipo_nomina'],
                    $validacion['fk_a006_num_miscelaneo_detalle_tipotrab'],
                    $validacion['num_situacion_trabajo'],
                    $validacion['fk_rhc032_num_motivo_cese'],
                    $validacion['fec_fecha_egreso'],
                    $validacion['txt_observacion_egreso'],
                    $validacion['ind_resolucion'],
                    $validacion['num_ultimo_sueldo'],
                    $validacion['num_monto_pension'],
                    $validacion['fk_a001_num_organismo'],
                    $validacion['fk_a004_num_dependencia'],
                    $validacion['fk_rhc063_num_puestos']
                );

                $resultado = $this->atPensionesInvalidez->metAprobarPensionesInvalidez($idPensionInvalidez,$validacion['txt_observacion_apro'],$datos);

                if(is_array($resultado)){
                    $validacion['status'] = 'error';
                    $validacion['detalleERROR'] = $resultado;
                    echo json_encode($validacion);
                    exit;
                }else{
                    $validacion['status']='aprobado';
                    echo json_encode($validacion);
                    exit;
                }
            }
        }

        /***********************************************************************/
        $datosBD = $this->atPensionesInvalidez->metMostrarPernsionesInvalidez($idPensionInvalidez);
        $this->atVista->assign('formDB',$datosBD);
        $datosOperacionesPR = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'PR');
        $this->atVista->assign('formDBoperacionesPR',$datosOperacionesPR);
        $datosOperacionesCO = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'CO');
        $this->atVista->assign('formDBoperacionesCO',$datosOperacionesCO);
        $datosOperacionesAP = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'AP');
        $this->atVista->assign('formDBoperacionesAP',$datosOperacionesAP);
        $datosOperacionesAN = $this->atPensionesInvalidez->metMostrarPensionesInvalidezOperaciones($idPensionInvalidez,'AN');
        $this->atVista->assign('formDBoperacionesAN',$datosOperacionesAN);
        /**********************************************************************/


        /*************************************************************************/
        $resultado = $this->atPensionesInvalidez->metObtenerDatosEmpleado($datosBD['fk_rhb001_num_empleado']);

        $f_n    = explode("-",$resultado['fec_nacimiento']);
        $f_i    = explode("-",$resultado['fec_ingreso']);
        $f_a    = date("d-m-Y");
        #edad y años de servicio
        list($edad, $EdadMeses, $EdadDias) = $this->metCalcularEdadAnioServicio($f_n[2]."-".$f_n[1]."-".$f_n[0],$f_a);
        list($anio_serv, $MesesServicio, $DiasServicio) = $this->metCalcularEdadAnioServicio($f_i[2]."-".$f_i[1]."-".$f_i[0],$f_a);

        $this->atVista->assign('edad',$edad);
        $this->atVista->assign('anio_servicio',$anio_serv);
        $this->atVista->assign('fec_ingreso',$resultado['fec_ingreso']);
        /*************************************************************************/

        $EstadoRegistro = array('0'=>'NO ACTIVO','1'=>'ACTIVO');
        $this->atVista->assign('EstadoRegistro',$EstadoRegistro);

        $usuario = Session::metObtener('idUsuario');
        $obtenerEmpleado = $this->atEmpleados->metUsuario($usuario, 1);
        $pkNumEmpleado = $obtenerEmpleado['pk_num_empleado'];
        $datosEmpleado = $this->atEmpleados->metEstadoEmpleado($pkNumEmpleado);

        $this->atVista->assign('estadoEmpleado',$datosEmpleado);

        $periodo = date("Y-m");#periodo actual
        $fecha   = date("d-m-Y");#fecha actual


        $miscMotivoPension = $this->atEmpleados->metMostrarSelect('MOTIPEN');
        $this->atVista->assign('MotivoPension',$miscMotivoPension);

        $this->atVista->assign('listadoTipoNomina',$this->atTipoNimina->metListarTipoNomina());
        $this->atVista->assign('listadoTipoTrabajador',$this->atEmpleados->metMostrarSelect('TIPOTRAB'));
        $this->atVista->assign('listadoMotivoCese',$this->atMotivoCese->metConsultarMotivoCese());

        $this->atVista->assign('idUsuario',$this->atIdUsuario);
        $this->atVista->assign('nombreUsuario',Session::metObtener('nombreUsuario'));

        $this->atVista->assign('periodo',$periodo);
        $this->atVista->assign('fecha',$fecha);

        $this->atVista->assign('idPensionInvalidez',$idPensionInvalidez);
        $this->atVista->assign('proceso','Aprobar');
        $this->atVista->metRenderizar('nuevaPension','modales');

    }




    #PERMITE OBTENER LOS DATOS DEL EMPLEADO
    public function metObtenerDatosEmpleado()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');

        $resultado = $this->atPensionesInvalidez->metObtenerDatosEmpleado($idEmpleado);

        $sueldo = number_format($resultado['num_sueldo_basico'],2,",",".");
        $f_n    = explode("-",$resultado['fec_nacimiento']);
        $f_i    = explode("-",$resultado['fec_ingreso']);
        $f_a    = date("d-m-Y");

        #edad y años de servicio
        list($edad, $EdadMeses, $EdadDias) = $this->metCalcularEdadAnioServicio($f_n[2]."-".$f_n[1]."-".$f_n[0],$f_a);
        list($anio_serv, $MesesServicio, $DiasServicio) = $this->metCalcularEdadAnioServicio($f_i[2]."-".$f_i[1]."-".$f_i[0],$f_a);


        #consulto miscelaneos
        $miscMotivoPen= $this->atEmpleados->metMostrarSelect('MOTIPEN');
        $miscTipoPen= $this->atEmpleados->metMostrarSelect('TIPOPEN');

        foreach($miscMotivoPen as $d1){

            $T = 'INV';

            if(strcmp($d1['cod_detalle'],$T)==0){
                $id_motivo_pension = $d1['pk_num_miscelaneo_detalle'];
                $motivo_pension = $d1['cod_detalle'];
            }

        }

        foreach($miscTipoPen as $d2){

            $T = 'INV';

            if(strcmp($d2['cod_detalle'],$T)==0){
                $id_tipo_pension = $d2['pk_num_miscelaneo_detalle'];
                $tipo_pension = $d2['cod_detalle'];
            }

        }

        //datos mpara mostrar en el formulario
        $datos = array(
             'id_empleado' => $idEmpleado,
             'nombreTrabajador' => $resultado['nombre_completo'],
             'num_sueldo_basico' => $sueldo,
             'fec_nacimiento' => $f_n[2]."-".$f_n[1]."-".$f_n[0],
             'fec_ingreso' => $f_i[2]."-".$f_i[1]."-".$f_i[0],
             'ind_cedula_documento' => $resultado['ind_cedula_documento'],
             'fk_a001_num_organismo' => $resultado['fk_a001_num_organismo'],
             'organismo' => $resultado['organismo'],
             'fk_a004_num_dependencia' => $resultado['fk_a004_num_dependencia'],
             'ind_dependencia' => $resultado['dependencia'],
             'fk_a006_num_miscelaneo_detalle_sexo' => $resultado['fk_a006_num_miscelaneo_detalle_sexo'],
             'sexo' => $resultado['sexo'],
             'fk_rhc063_num_puestos_cargo' => $resultado['fk_rhc063_num_puestos_cargo'],
             'cargo' => $resultado['cargo'],
             'edad' => $edad,
             'anio_serv' => $anio_serv,
             'fk_a006_num_miscelaneo_detalle_motivopension' => $id_motivo_pension,
             'motivo_pension' => $motivo_pension,
             'fk_a006_num_miscelaneo_detalle_tipopension' => $id_tipo_pension,
             'tipo_pension' => $tipo_pension,
             'nomina' => $resultado['fk_nmb001_num_tipo_nomina'],
             'tipo_trabajador' => $resultado['fk_a006_num_miscelaneo_detalle_tipotrabajador'],
             'estatus' => $resultado['estatus_empleado'],
             'tipo' => $T,

        );

        echo json_encode($datos);

    }

    /**
     * PARA VERIFICAR LOS REQUISITOS PARA OPTAR A LA PENSION POR INVALIDEZ
     * VALIDA EL TIEMPO DEL FUNCIONARIO EN LA INSTITUCION Y EL MONTO ASIGNADO A LA PENSION
     **/
    public function metVerificarRequisitos()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $tipo = $this->metObtenerTexto('tipo');
        $f_a       = date("d-m-Y");



        if(strcmp($tipo,'MONTO')==0){

            $PENPORMIN  = Session::metObtener('PENPORMIN');
            $PENPORMAX  = Session::metObtener('PENPORMAX');
            $monto_pension = trim(str_replace(",",".",preg_replace('/[^0-9 ,]/i','', $this->metObtenerTexto('monto'))));
            $sueldo_actual = trim(str_replace(",",".",preg_replace('/[^0-9 ,]/i','', $this->metObtenerTexto('sueldo'))));

            $MontoMin = $sueldo_actual * $PENPORMIN / 100;
            $MontoMax = $sueldo_actual *  $PENPORMAX / 100;

            if ($monto_pension < $MontoMin || $monto_pension > $MontoMax){
                $requisito = array(
                    'requisito' => '0',
                    'PENPORMIN' => $PENPORMIN,
                    'PENPORMAX' => $PENPORMAX
                );
            }else{
                $requisito = array(
                    'requisito' => '1'
                );
            }

            echo json_encode($requisito);

        }
        if(strcmp($tipo,'REQUISITOS')==0){

            $fecha_nac = $this->metObtenerTexto('fecha_nac');
            $fecha_ing = $this->metObtenerTexto('fecha_ing');
            $PENSERVMIN = Session::metObtener('PENSERVMIN');

            list($anio_serv, $MesesServicio, $DiasServicio) = $this->metCalcularEdadAnioServicio($fecha_ing,$f_a);

            if ($anio_serv >= $PENSERVMIN){

                $requisito = array(
                    'requisito' => '1'
                );


            } else {
                $requisito = array(
                    'requisito' => '0'
                );

            }
            echo json_encode($requisito);
        }

    }


    public function metListadoConformar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('proceso','Conformar');
        $this->atVista->assign('listadoPensionesInvalidez', $this->atPensionesInvalidez->metListarPensionesInvalidez('PR'));
        $this->atVista->metRenderizar('listado');
    }


    public function metListadoAprobar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $js= array('materialSiace/core/demo/DemoTableDynamic','materialSiace/App','materialSiace/core/demo/DemoFormComponents');

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('proceso','Aprobar');
        $this->atVista->assign('listadoPensionesInvalidez', $this->atPensionesInvalidez->metListarPensionesInvalidez('CO'));
        $this->atVista->metRenderizar('listado');
    }

    public function metCalcularEdadAnioServicio($_DESDE,$_HASTA)
    {
        $error = 0;
        $listo = 0;
        if ((strlen($_DESDE)) < 10) $error = 1;
        else {
            list($d, $m, $a) = explode("-", $_HASTA);
            $diaActual = $d;
            $mesActual = $m;
            $annioActual = $a;
            ##
            list($d, $m, $a) = explode("-", $_DESDE);
            $dia = intval($d);
            $mes = intval($m);
            $annio = intval($a);
            $dias = 0;
            $meses = 0;
            $annios = 0;
            ##
            if ($annio > $annioActual || ($annio == $annioActual && $mes > $mesActual) || ($annio == $annioActual && $mes == $mesActual && $dia > $diaActual)) $error = 2;
            else {
                $annios = $annioActual - $annio;
                $meses = $mesActual - $mes;
                $dias = $diaActual - $dia;
                ##
                if ($dias < 0) { $meses--; $dias = 30 + $dias; }
                if ($meses < 0) { $annios--; $meses = 12 + $meses; }
                ##
                if ($dias >= 30) { $meses++; $dias = 0; }
                if ($meses >= 12) { $annios++; $meses = 0; }
                ##
                return array($annios, $meses, $dias);
            }
        }
        if ($error!=0) return array("", "", "");
    }



    public function metBusquedaFiltro()
    {

        $organismo   = $this->metObtenerInt('organismo');
        $dependencia = $this->metObtenerInt('dependencia');
        $estatus_persona  = $this->metObtenerInt('estatus_persona');
        $estatus_empleado = $this->metObtenerInt('estatus_empleado');

        $datos = $this->atEmpleados->metListarEmpleadoOrganizacion($organismo,$dependencia,$estatus_persona,$estatus_empleado);

        echo json_encode($datos);


    }

    public function metBusquedaFiltroListado()
    {
        $organismo   = $this->metObtenerInt('organismo');
        $dependencia = $this->metObtenerInt('dependencia');
        $estatus  = $this->metObtenerTexto('estatus');

        $datos = $this->atPensionesInvalidez->metListarPensionesInvalidezFiltro($organismo,$dependencia,$estatus);

        echo json_encode($datos);

    }




}
