<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de gestionar reporte acerca del control de asistencia de los funcionarios
class controlAsistenciaControlador extends Controlador
{
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('controlAsistencia', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo
		);
		$tipoNomina = $this->atReporteModelo->metListarNomina();
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
		$listadoEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, '', '', '', '', '');
		$listarCargos = $this->atReporteModelo->metListarCargos();
		$tipoAusencia = $this->atReporteModelo->metMostrarSelect('TIPAUS');
		$this->atVista->assign('nomina', $tipoNomina);
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
		$this->atVista->assign('datos', $listadoEmpleado);
		$this->atVista->assign('listarCargos', $listarCargos);
		$this->atVista->assign('tipoAusencia', $tipoAusencia);
		$this->atVista->metRenderizar('listadoAsistencia');
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarDependencia()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$listarDependencia=$this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="">&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// Método que permite listar los empleados por dependencia y organismo
	public function metGenerarAsistencia()
	{
		$pkNumOrganismo = $_POST['pk_num_organismo'];
		$pkNumDependencia = $_POST['pk_num_dependencia'];
		$tipoNomina = $_POST['tipo_nomina'];
		$indCedulaDocumento = $_POST['ind_cedula_documento'];
		$cargo = $_POST['cargo'];
		$edoReg = $_POST['edo_reg'];
		$sitTrab = $_POST['sit_trab'];
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$ordenar = $_POST['ordenar'];
		$fechaInicio = $_POST['fechaInicio'];
		$fechaFin = $_POST['fechaFin'];
		$tipoAusencia = $_POST['tipo_ausencia'];
		$datos = array(
			'pk_num_organismo' => $pkNumOrganismo,
			'pk_num_dependencia' => $pkNumDependencia,
			'tipoNomina' => $tipoNomina,
			'indCedulaDocumento' => $indCedulaDocumento,
			'cargo' => $cargo,
			'edoReg' => $edoReg,
			'sitTrab' => $sitTrab,
			'pkNumEmpleado' => $pkNumEmpleado,
			'ordenar' => $ordenar,
			'fechaInicio' => $fechaInicio,
			'fechaFin' => $fechaFin,
			'tipoAusencia' => $tipoAusencia
		);
		$this->atVista->assign('datos', $datos);
		$this->atVista->metRenderizar('generarAsistencia', 'modales');
	}

	// Método que permite generar el control de asistencias a la institución
	public function metGenerarControlAsistencia()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfControlAsistencia('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);

		// Recibo las variables
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$tipoNomina = $_GET['tipoNomina'];
		$indCedulaDocumento = $_GET['indCedulaDocumento'];
		$cargo = $_GET['cargo'];
		$edoReg = $_GET['edo_reg'];
		$sitTrab = $_GET['sit_trab'];
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$ordenar = $_GET['ordenar'];
		$fechaInicioPrevia = $_GET['fechaInicio'];
		$fechaFinPrevia = $_GET['fechaFin'];
		$tipoAusencia = $_GET['tipo_ausencia'];


		// Formato de Fechas
		if ($fechaInicioPrevia != '') {
			$explodeInicio = explode('/', $fechaInicioPrevia);
			$fechaInicio = $explodeInicio[2] . '-' . $explodeInicio[1] . '-' . $explodeInicio[0];
		} else {
			$fechaInicio = '';
		}
		if ($fechaFinPrevia != '') {
			$explodeFin = explode('/', $fechaFinPrevia);
			$fechaFin = $explodeFin[2] . '-' . $explodeFin[1] . '-' . $explodeFin[0];
		} else {
			$fechaFin = '';
		}

		if($pkNumDependencia!=''){
			$listarDependencia = $this->atReporteModelo->metListarDependencia('', 2, $pkNumDependencia);
		} else {
			$listarDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, '');
		}

		$pdf->SetWidths(array(195));
		$pdf->SetAligns(array('L'));
		$pdf->SetFont('Arial', '', 8);
		$pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255);

		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(195, 4, 'De: '.$fechaInicioPrevia.' a '.$fechaFinPrevia, 1, 1, 'C');
		$pdf->Ln(5);

		$pdf->SetFont('Arial', 'B', 9);

		foreach($listarDependencia as $dep) {

			$pkNumDependencia = $dep['pk_num_dependencia'];

			// Se requiere saber si existen empleados con esas especificaciones
			$listarAsistencias = $this->atReporteModelo->metListarAsistencias($pkNumOrganismo, $pkNumDependencia, $tipoNomina, $indCedulaDocumento, $cargo, $edoReg, $sitTrab, $pkNumEmpleado, $fechaInicio, $fechaFin);

			$contador = count($listarAsistencias);

			if($contador>0) {
				$pdf->Row(array(
					utf8_decode($dep['ind_dependencia'])
				));
				$listarEmpleados = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $indCedulaDocumento, $ordenar);

				foreach($listarEmpleados as $emp) {

					// Tomo el id de cada empleado
					$pkNumEmpleado = $emp['pk_num_empleado'];
					// Se detallan las asistencias
					$listarAsistencias = $this->atReporteModelo->metListarAsistencias($pkNumOrganismo, $pkNumDependencia, $tipoNomina, $indCedulaDocumento, $cargo, $edoReg, $sitTrab, $pkNumEmpleado, $fechaInicio, $fechaFin);
					$contadorAsistencias = count($listarAsistencias);

					if($contadorAsistencias>0) {
						$pdf->SETX(15);
						$pdf->SetWidths(array(20, 80, 90));
						$pdf->SetAligns(array('L', 'L', 'L'));
						$pdf->SetFont('Arial', 'B', 8);
						$pdf->Row(array(
							number_format($emp['ind_cedula_documento'], 0, '', '.'),
							utf8_decode($emp['ind_nombre1'].' '.$emp['ind_nombre2'].' '.$emp['ind_apellido1'].' '.$emp['ind_apellido2']),
							utf8_decode($emp['ind_descripcion_cargo']),
						));
						// Cargo la tabla
						$pdf->Ln();
						$pdf->SetFillColor(255, 255, 255);
						$pdf->SetDrawColor(0, 0, 0);
						$pdf->SetFont('Arial', 'B', 8);
						$pdf->SETX(15);
						$pdf->Cell(30, 4, 'Fecha', 1, 0, 'C');
						$pdf->Cell(30, 4, 'Hora', 1, 0, 'C');
						$pdf->Cell(50, 4, 'Evento', 1, 0, 'C');
						$pdf->Cell(80, 4, 'firma', 1, 1, 'C');
						$pdf->Ln();
						$pdf->SetFillColor(255, 255, 255);
						$pdf->SetDrawColor(255, 255, 255);
						$pdf->SetWidths(array(30, 30, 50, 80));
						$pdf->SetAligns(array('C', 'C', 'C', 'C'));
						$pdf->SetFont('Arial', '', 8);
						$contar = 0;

						foreach ($listarAsistencias as $asistencias) {
							$pdf->SETX(15);
							$contar++;
							if ($contar % 2 == 0) {
								$pdf->SetFillColor(240, 240, 240);
								$pdf->SetDrawColor(240, 240, 240);
							} else {
								$pdf->SetFillColor(255, 255, 255);
								$pdf->SetDrawColor(255, 255, 255);
							}
							// Formato de Horas. Transformacion al formato estandar
							$fecha = $asistencias['fec_fecha'];
							$hora  = $asistencias['fec_hora'];
							$evento = $asistencias['ind_evento'];
							$pdf->Row(array(
								$fecha,
								$hora,
								$evento,
								'____________________________________________'
							));

						}
						$pdf->Ln();
						$pdf->SetFillColor(255, 255, 255);
						$pdf->SetDrawColor(255, 255, 255);
						$pdf->SETX(15);
						$pdf->SetFont('Arial', 'B', 8);


					}



				}

			}

		}

		$pdf->Output();



	}

	// Método que permite transformar las horas de horario militar a horario estándar
	public function metHorarioEstandar($horaSalida, $horaEntrada)
	{
		// Hora de salida
		$horaExplodeInicio = explode(":", $horaSalida);
		$horaInicio = $horaExplodeInicio[0];
		$minutoInicio = $horaExplodeInicio[1];

		// Hora de entrada
		$horaExplodeFin = explode(":", $horaEntrada);
		$horaFin = $horaExplodeFin[0];
		$minutoFin = $horaExplodeFin[1];

		// Convirtiendo a horario estándar
		// Hora Inicio
		if($horaInicio>12){
			$horaInicioPrevio = $horaInicio - 12;
			$hora_inicio = str_pad($horaInicioPrevio, 2, "0", STR_PAD_LEFT);
		}
		if($horaInicio<12) {
			$hora_inicio = $horaInicio;
		}
		// Hora Fin
		if($horaFin>12){
			$horaFinPrevio = $horaFin - 12;
			$hora_fin = str_pad($horaFinPrevio, 2, "0", STR_PAD_LEFT);
		}
		if($horaFin<12) {
			$hora_fin = $horaFin;
		}
		// Horario
		// Horario Inicio
		if($horaInicio>=12){
			$horarioInicio = 'PM';
		}
		if($horaInicio<12) {
			$horarioInicio = 'AM';
		}
		//Horario Fin
		if($horaFin>=12){
			$horariofin = 'PM';
		}
		if($horaFin<12) {
			$horariofin = 'AM';
		}
		// Los agrupo en un array
		$datoHorario = array($hora_inicio,$minutoInicio, $hora_fin, $minutoFin, $horarioInicio, $horariofin);
		return $datoHorario;
	}

	// Método que permite calcular la edad de una persona
	public function metCalcularFechaEdad($edadPrevia)
	{
		$fecha = date('Y-m-d');
		$nuevafecha = strtotime ( '-'.$edadPrevia.' year' , strtotime ( $fecha ) ) ;
		$nuevafechaTotal = date ( 'Y-m-d' , $nuevafecha );
		return $nuevafechaTotal;
	}

	// Método que permite listar las inasistencias por medio de permisos otorgados
	public function metGenerarControlPermiso()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfControlPermiso('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		// Recibo las variables
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$tipoNomina = $_GET['tipoNomina'];
		$indCedulaDocumento = $_GET['indCedulaDocumento'];
		$cargo = $_GET['cargo'];
		$edoReg = $_GET['edo_reg'];
		$sitTrab = $_GET['sit_trab'];
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$ordenar = $_GET['ordenar'];
		$fechaInicioPrevia = $_GET['fechaInicio'];
		$fechaFinPrevia = $_GET['fechaFin'];
		$tipoAusencia = $_GET['tipo_ausencia'];
		// Formato de Fechas
		if ($fechaInicioPrevia != '') {
			$explodeInicio = explode('/', $fechaInicioPrevia);
			$fechaInicio = $explodeInicio[2] . '-' . $explodeInicio[1] . '-' . $explodeInicio[0];
		} else {
			$fechaInicio = '';
		}
		if ($fechaFinPrevia != '') {
			$explodeFin = explode('/', $fechaFinPrevia);
			$fechaFin = $explodeFin[2] . '-' . $explodeFin[1] . '-' . $explodeFin[0];
		} else {
			$fechaFin = '';
		}
		if($pkNumDependencia!=''){
			$listarDependencia = $this->atReporteModelo->metListarDependencia('', 2, $pkNumDependencia);
		} else {
			$listarDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, '');
		}

		// Fin de Variables
		//***************************Inicio del PDF ***************************************************
		$pdf->SetWidths(array(195));
		$pdf->SetAligns(array('L'));
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255);

		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(195, 4, 'De: '.$fechaInicioPrevia.' a '.$fechaFinPrevia, 1, 1, 'C');
		$pdf->Ln(5);

		$pdf->SetFont('Arial', 'B', 9);

		foreach($listarDependencia as $dep) {
			$pkNumDependencia = $dep['pk_num_dependencia'];

			// Se requiere saber si existen empleados con esas especificaciones
			$listarPermiso = $this->atReporteModelo->metListarPermiso($pkNumOrganismo, $pkNumDependencia, $tipoNomina, $indCedulaDocumento, $cargo, $edoReg, $sitTrab, $pkNumEmpleado, $fechaInicio, $fechaFin, $tipoAusencia);

			$contador = count($listarPermiso);


			if($contador>0) {
				$pdf->Row(array(
					utf8_decode($dep['ind_dependencia'])
				));
				$listarEmpleados = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $indCedulaDocumento, $ordenar);
				foreach($listarEmpleados as $emp) {
					// Tomo el id de cada empleado
					$pkNumEmpleado = $emp['pk_num_empleado'];
					// Se detallan los empleados especificos
					$listadoPermiso = $this->atReporteModelo->metListarPermiso($pkNumOrganismo, $pkNumDependencia, $tipoNomina, $indCedulaDocumento, $cargo, $edoReg, $sitTrab, $pkNumEmpleado, $fechaInicio, $fechaFin, $tipoAusencia);
					// cuento a los empleados que tienen permiso
					$contadorPermiso = count($listadoPermiso);
					if($contadorPermiso>0) {
						// Cargo al Empleado
						$pdf->SETX(15);
						$pdf->SetWidths(array(20, 20, 70, 80));
						$pdf->SetAligns(array('L', 'L', 'L'));
						$pdf->SetFont('Arial', 'B', 8);
						$pdf->Row(array(
							utf8_decode($emp['pk_num_empleado']),
							number_format($emp['ind_cedula_documento'], 0, '', '.'),
							utf8_decode($emp['ind_nombre1'].' '.$emp['ind_nombre2'].' '.$emp['ind_apellido1'].' '.$emp['ind_apellido2']),
							utf8_decode($emp['ind_descripcion_cargo']),
						));
						// Cargo la tabla
						$pdf->Ln();
						$pdf->SetFillColor(255, 255, 255);
						$pdf->SetDrawColor(0, 0, 0);
						$pdf->SetFont('Arial', 'B', 8);
						$pdf->SETX(20);
						$pdf->Cell(30, 4, 'Tipo de Falta', 1, 0, 'C');
						$pdf->Cell(60, 4, 'Tipo de Permiso', 1, 0, 'C');
						$pdf->Cell(35, 4, 'Fecha', 1, 0, 'C');
						$pdf->Cell(20, 4, 'Hora', 1, 0, 'C');
						$pdf->Cell(15, 4, 'Dias', 1, 0, 'C');
						$pdf->Cell(15, 4, 'Horas', 1, 1, 'C');
						$pdf->Ln();
						$pdf->SetFillColor(255, 255, 255);
						$pdf->SetDrawColor(255, 255, 255);
						$pdf->SetWidths(array(30, 60, 35, 20, 15, 15));
						$pdf->SetAligns(array('L', 'L', 'C', 'C', 'C', 'C'));
						$pdf->SetFont('Arial', '', 8);
						$contar = 0;
						$acumuladorDia = 0;
						$acumuladorHora = 0;
						$acumuladorMinuto = 0;
						foreach ($listadoPermiso as $permiso) {
							$pdf->SETX(20);
							$contar++;
							if ($contar % 2 == 0) {
								$pdf->SetFillColor(240, 240, 240);
								$pdf->SetDrawColor(240, 240, 240);
							} else {
								$pdf->SetFillColor(255, 255, 255);
								$pdf->SetDrawColor(255, 255, 255);
							}
							// Formato de Horas. Transformacion al formato estandar
							$fecHoraSalida = $permiso['fec_hora_salida'];
							$fecHoraEntrada = $permiso['fec_hora_entrada'];
							$horaHorario = $this->metHorarioEstandar($fecHoraSalida, $fecHoraEntrada);
							$dia = $permiso['num_total_dia'];
							$acumuladorDia = $acumuladorDia + $dia;
							$hora = $permiso['num_total_hora'];
							$minuto = $permiso['num_total_minuto'];
							$pdf->Row(array(
								utf8_decode($permiso['tipo_ausencia']),
								utf8_decode($permiso['motivo_ausencia']),
								$permiso['fecha_salida'].' - '.$permiso['fecha_entrada'],
								$horaHorario[0].':'.$horaHorario[1].' - '.$horaHorario[2].':'.$horaHorario[3],
								$dia,
								str_pad($hora, 2, "0", STR_PAD_LEFT).':'.str_pad($permiso['num_total_minuto'], 2, "0", STR_PAD_LEFT).':00'
							));
							$acumuladorHora = $acumuladorHora + $hora;
							$acumuladorMinuto = $acumuladorMinuto + $minuto;
						}
						$pdf->SetFillColor(255, 255, 255);
						$pdf->SetDrawColor(255, 255, 255);
						$pdf->SETX(20);
						$pdf->SetFont('Arial', 'B', 8);
						while($acumuladorMinuto>=60){
							if($acumuladorMinuto>=60){
								$acumuladorHora = $acumuladorHora + 1;
								$acumuladorMinuto = $acumuladorMinuto - 60;
							}
						}
						$pdf->Row(array(
							'',
							'',
							'',
							'',
							$dia,
							str_pad($acumuladorHora, 2, "0", STR_PAD_LEFT).':'.str_pad($acumuladorMinuto, 2, "0", STR_PAD_LEFT).':00'
						));
						$pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255);
						$pdf->Ln();
					}
				}
				$pdf->SetFont('Arial', 'B', 9);
				$pdf->SetWidths(array(195));
				$pdf->SetAligns(array('L'));
				$pdf->Ln();
			}
		}
		$pdf->Output();
		//***************************Fin del PDF ***************************************************
	}
}
