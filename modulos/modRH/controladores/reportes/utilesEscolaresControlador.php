<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de HCM. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class utilesEscolaresControlador extends Controlador
{
    private $atReporteModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atReporteModelo = $this->metCargarModelo('utilesEscolares', 'reportes');
    }

    // Index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
            'multi-select/multi-select555c'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'multi-select/jquery.multi-select'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atReporteModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
        $datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
        $pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
        $empleado = array(
            'pk_num_organismo' => $pkNumOrganismo
        );
        $listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $listarEmpleado = $this->atReporteModelo->metListarEmpleados($pkNumOrganismo, '');
        $listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $listarNomina = $this->atReporteModelo->metListarNomina('');
        $listarPeriodo = $this->atReporteModelo->metMostrarListadoUtiles($pkNumOrganismo);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('listarEmpleado', $listarEmpleado);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        $this->atVista->assign('listarNomina', $listarNomina);
        $this->atVista->assign('listarPeriodo', $listarPeriodo);
        $this->atVista->metRenderizar('reporteGeneralUtiles');
    }

    // Index del controlador
    public function metGenerarReporteDetallado()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
            'multi-select/multi-select555c'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'multi-select/jquery.multi-select'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atReporteModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
        $datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
        $pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
        $empleado = array(
            'pk_num_organismo' => $pkNumOrganismo
        );
        $listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $listarEmpleado = $this->atReporteModelo->metListarEmpleados($pkNumOrganismo, '');
        $listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $listarNomina = $this->atReporteModelo->metListarNomina('');
        $listarPeriodo = $this->atReporteModelo->metMostrarListadoUtiles($pkNumOrganismo);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('listarEmpleado', $listarEmpleado);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        $this->atVista->assign('listarNomina', $listarNomina);
        $this->atVista->assign('listarPeriodo', $listarPeriodo);
        $this->atVista->metRenderizar('reporteDetalladoUtiles');
    }

    // Método que permite listar las dependencias del organismo
    public function metBuscarDependencia()
    {
        $pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
        $listarDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="">&nbsp;</option>';
        foreach ($listarDependencia as $listarDependencia) {
            $a .= '<option value="' . $listarDependencia['pk_num_dependencia'] . '">' . $listarDependencia['ind_dependencia'] . '</option>';
        }
        $a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
        echo $a;
    }

    // Método que permite listar los empleados de una dependencia
    public function metBuscarEmpleado()
    {
        $pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
        $listarEmpleado = $this->atReporteModelo->metListarEmpleados('', $pkNumDependencia);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_empleado" id="pk_num_empleado"> <option value="">&nbsp;</option>';
        foreach ($listarEmpleado as $empleado) {
            $a .= '<option value="' . $empleado['pk_num_empleado'] . '">' . $empleado['ind_nombre1'] . '  '.$empleado['ind_nombre2'].'  '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2'].'</option>';
        }
        $a .= '</select><label for="pk_num_empleado"><i class="glyphicon glyphicon-briefcase"></i> Empleado</label></div>';
        echo $a;
    }

    // Método que permite listar los empleados de una dependencia
    public function metBuscarPeriodo()
    {
        $pkNumTipoNomina = $this->metObtenerInt('pk_num_tipo_nomina');
        $pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
        $listarPeriodo = $this->atReporteModelo->metListarUtiles($pkNumOrganismo, '', '', $pkNumTipoNomina, '');
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="periodoLista" id="periodoLista">';
        foreach ($listarPeriodo as $periodo) {
            $a .= '<option value="' . $periodo['periodo'] . '">' . $periodo['periodo'] .'</option>';
        }
        $a .= '</select><label for="periodo"><i class="glyphicon glyphicon-briefcase"></i> Periodo</label></div>';
        echo $a;
    }

    // Método que permite generar el reporte general de útiles Escolares
    public function metGenerarReporteGeneral()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraReportes', 'modRH');
        $pdf = new pdfUtilesGeneral('P', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        // Recibo las variables
        $pkNumOrganismo = $_GET['pk_num_organismo'];
        $pkNumDependencia = $_GET['pk_num_dependencia'];
        $pkNumEmpleado = $_GET['pk_num_empleado'];
        $pkNumTipoNomina = $_GET['pk_num_tipo_nomina'];
        $periodo = $_GET['periodo'];
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(195, 5, utf8_decode('REPORTE GENERAL DE ÚTILES'), 0, 0, 'C', 0);
        $pdf->Ln();
        if($pkNumDependencia!=''){
            $dependencia = $this->atReporteModelo->metListarDependencia('', 2, $pkNumDependencia);
            $pdf->Cell(180, 5, utf8_decode('DEPENDENCIA: '.$dependencia['ind_dependencia']), 0, 1, 'L');
        }
        if($pkNumTipoNomina!=''){
            $nomina = $this->atReporteModelo->metListarNomina($pkNumTipoNomina);
            $pdf->Cell(65, 5, utf8_decode('NÓMINA: '.$nomina['ind_nombre_nomina']), 0, 1, 'L');
        }
        $pdf->Cell(65, 5, 'PERIODO: '.$periodo, 0, 0, 'L');
        $pdf->Ln();
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(229, 229, 229);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->Cell(15, 5, utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(45, 5, utf8_decode('CÉDULA'), 1, 0, 'C', 1);
        $pdf->Cell(100, 5, utf8_decode('EMPLEADO'), 1, 0, 'C', 1);
        $pdf->Cell(35, 5, utf8_decode('MONTO'), 1, 0, 'C', 1);
        $i = 1;
        $acumuladorMonto = 0;
        $pdf->SetWidths(array(15, 45, 100, 35));
        $pdf->SetAligns(array('C', 'C', 'L','R'));
        $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(0, 0, 0);$pdf->SetTextColor(0, 0, 0);
        $pdf->Ln();
        $listarUtiles = $this->atReporteModelo->metListarUtiles($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $pkNumTipoNomina, $periodo);
        foreach($listarUtiles as $listar){
            $pdf->Row(array(
                $i,
                number_format($listar['ind_cedula_documento'], 0, '', '.'),
                utf8_decode($listar['ind_nombre1'].' '.$listar['ind_nombre2'].' '.$listar['ind_apellido1'].' '.$listar['ind_apellido2']),
                number_format($listar['monto'], 2, ',', '.')
            ));
            $acumuladorMonto = $acumuladorMonto + $listar['monto'];
            $i++;
        }
        $pdf->Cell(160, 5, 'TOTAL', 1, 0, 'R', 1);
        $pdf->Cell(35, 5,  number_format($acumuladorMonto, 2, ',', '.'), 1, 0, 'R', 1);
        $pdf->Output();
    }

    // Método que permite generar el reporte general de útiles Escolares
    public function metGenerarUtilesDetallado()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraReportes', 'modRH');
        $pdf = new pdfUtilesGeneral('P', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        // Recibo las variables
        $pkNumOrganismo = $_GET['pk_num_organismo'];
        $pkNumDependencia = $_GET['pk_num_dependencia'];
        $pkNumEmpleado = $_GET['pk_num_empleado'];
        $pkNumTipoNomina = $_GET['pk_num_tipo_nomina'];
        $periodo = $_GET['periodo'];
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(195, 5, utf8_decode('REPORTE DETALLADO DE ÚTILES'), 0, 0, 'C', 0);
        $pdf->Ln();
        if($pkNumDependencia!=''){
            $dependencia = $this->atReporteModelo->metListarDependencia('', 2, $pkNumDependencia);
            $pdf->Cell(180, 5, utf8_decode('DEPENDENCIA: '.$dependencia['ind_dependencia']), 0, 1, 'L');
        }
        if($pkNumTipoNomina!=''){
            $nomina = $this->atReporteModelo->metListarNomina($pkNumTipoNomina);
            $pdf->Cell(65, 5, utf8_decode('NÓMINA: '.$nomina['ind_nombre_nomina']), 0, 1, 'L');
        }
        $pdf->Cell(65, 5, 'PERIODO: '.$periodo, 0, 0, 'L');
        $pdf->Ln();
        $pdf->Cell(15, 5, utf8_decode('N°'), 0, 0, 'C', 0);
        $pdf->Cell(25, 5, utf8_decode('CÉDULA'), 0, 0, 'C', 0);
        $pdf->Cell(100, 5, utf8_decode('EMPLEADO/HIJO'), 0, 0, 'L', 0);
        $pdf->Cell(20, 5, utf8_decode('FECHA'), 0, 0, 'C', 0);
        $pdf->Cell(35, 5, utf8_decode('MONTO'), 0, 0, 'C', 0);
        $pdf->Ln();
        $i = 0;
        $acumuladorMonto = 0;
        $pdf->SetWidths(array(15, 25, 100, 20, 35));
        $pdf->SetAligns(array('C', 'C', 'L', 'C','R'));
        $pdf->SetFont('Arial', '', 8);
        $listarUtiles = $this->atReporteModelo->metListarUtilesDetallado($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $pkNumTipoNomina, $periodo);
        foreach($listarUtiles as $listar){
            $i++;
            $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);
            $pkNumUtilesBeneficio = $listar['pk_num_utiles_beneficio'];
            $pdf->Row(array(
                $i,
                number_format($listar['ind_cedula_documento'], 0, '', '.'),
                utf8_decode($listar['ind_nombre1'].' '.$listar['ind_nombre2'].' '.$listar['ind_apellido1'].' '.$listar['ind_apellido2']),
                $listar['fecha'],
                number_format($listar['num_monto'], 2, ',', '.')
            ));
            $listarBeneficiario = $this->atReporteModelo->metListarBeneficiario($pkNumUtilesBeneficio);
            $pdf->SetWidths(array(15, 25, 100, 20, 35));
            $pdf->SetAligns(array('C', 'C', 'C', 'C','L'));
            $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255);
            foreach($listarBeneficiario as $beneficiario){
                $pdf->Row(array(
                    '',
                    number_format($beneficiario['ind_cedula_documento'], 0, '', '.'),
                    utf8_decode($beneficiario['ind_nombre1'].' '.$beneficiario['ind_nombre2'].' '.$beneficiario['ind_apellido1'].' '.$beneficiario['ind_apellido2']),
                    '',
                    number_format($beneficiario['num_monto_asignado'], 2, ',', '.')
                ));
            }
            $pdf->SetAligns(array('C', 'C', 'L', 'C','R'));
            $acumuladorMonto = $acumuladorMonto + $listar['num_monto'];
        }
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(195, 5, 'TOTAL  '.number_format($acumuladorMonto, 2, ',', '.'), 0, 0, 'R', 0);
        $pdf->Output();
    }
}