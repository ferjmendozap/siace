<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class disfruteVacacionesControlador extends Controlador
{
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('disfruteVacaciones', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo
		);
		$tipoNomina = $this->atReporteModelo->metListarNomina();
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
		$listadoEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, '', '', '', '', '');
		$listadoEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, '', '', '', '', '');
		$this->atVista->assign('nomina', $tipoNomina);
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
		$this->atVista->assign('datos', $listadoEmpleado);
		$this->atVista->metRenderizar('listadoDisfruteVacaciones');
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarDependencia()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$listarDependencia=$this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia" onchange="cargarEmpleado(this.value)"> <option value="">&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// Método que permite listar los empleados del organismo
	public function metBuscarEmpleado()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
		$listarEmpleado=$this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, '', '', '', '');
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_empleado" id="pk_num_empleado"> <option value="">&nbsp;</option>';
		foreach($listarEmpleado as $empleado){
			$a .='<option value="'.$empleado['pk_num_empleado'].'">'.$empleado['ind_nombre1'].' '.$empleado['ind_nombre2'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2'].'</option>';
		}
		$a .= '</select><label for="pk_num_empleado"><i class="glyphicon glyphicon-briefcase"></i> Empleado</label></div>';
		echo $a;
	}

	// Método que permite listar los empleados por dependencia y organismo
	public function metGenerarVacacion()
	{
		$pkNumOrganismo = $_POST['pk_num_organismo'];
		$pkNumDependencia = $_POST['pk_num_dependencia'];
		$tipoNomina = $_POST['tipo_nomina'];
		$sitTrab = $_POST['sit_trab'];
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$periodo = $_POST['periodo'];
		$empleado = $_POST['empleado'];
		$datos = array(
			'pk_num_organismo' => $pkNumOrganismo,
			'pk_num_dependencia' => $pkNumDependencia,
			'tipoNomina' => $tipoNomina,
			'sitTrab' => $sitTrab,
			'pkNumEmpleado' => $pkNumEmpleado,
			'periodo' => $periodo,
			'empleado' => $empleado
		);
		$this->atVista->assign('datos', $datos);
		$this->atVista->metRenderizar('generarDisfruteVacaciones', 'modales');
	}

	// Método que permite generar el reporte de vacaciones de todos los funcionarios asociados a la búsqueda
	public function metGenerarUtilizacion()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfUtilizacionVacaciones('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$tipoNomina = $_GET['tipo_nomina'];
		$sitTrab = $_GET['sit_trab'];
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$periodo = $_GET['periodo'];
		$empleado = $_GET['empleado'];
		$listarEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $sitTrab, $empleado, $tipoNomina);
        $id = 0;
		foreach($listarEmpleado as $empleado){
			$pdf->SetWidths(array(15, 75));
			$pdf->SetAligns(array('C', 'L'));
			$pdf->SetFont('Arial', 'B', 8);
			$pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255);
			$pkNumEmpleado = $empleado['pk_num_empleado'];
			$listarUtilizacionPeriodo = $this->atReporteModelo->metConsultarUtilizacionPeriodo($pkNumEmpleado);
			$contador = count($listarUtilizacionPeriodo);

			if($contador>0) {
			    $id = $id + 1;
				$pdf->Row(array(
                    $id,
					utf8_decode($empleado['ind_nombre1'] . ' ' . $empleado['ind_nombre2'] . ' ' . $empleado['ind_apellido1'] . ' ' . $empleado['ind_apellido2']),
				));
				$pdf->SetWidths(array(30, 30, 30, 30, 30));
				$pdf->SetAligns(array('C', 'C', 'C', 'C', 'C'));
				$pdf->SetFont('Arial', '', 8);
				foreach($listarUtilizacionPeriodo as $periodoUtilizacion){
					$pdf->SETX(30);
					$anioAnterior = $periodoUtilizacion['num_anio']-1;
					$pdf->Row(array(
						$anioAnterior.' - '.$periodoUtilizacion['num_anio'],
						$periodoUtilizacion['num_dias_derecho'],
						$periodoUtilizacion['num_dias_gozados'],
						$periodoUtilizacion['num_dias_interrumpidos'],
						$periodoUtilizacion['num_total_utilizados']
					));
					if($periodo!=''){
						$pkNumPeriodo = $periodoUtilizacion['pk_num_periodo'];
						$listarDetallePeriodo = $this->atReporteModelo->metConsultarDetalle($pkNumEmpleado, $pkNumPeriodo);
						$pdf->SetWidths(array(30, 30, 30, 60));
						$pdf->SetAligns(array('C', 'C', 'C', 'C'));
						$pdf->SetFont('Arial', 'B', 8);
						foreach($listarDetallePeriodo as $detalle){
							$pdf->SETX(50);
							$pdf->Row(array(
								$detalle['ind_nombre_detalle'],
								$detalle['num_dias_detalle'],
								$detalle['fecha_inicial'],
								$detalle['fecha_final'],
							));
						}
					}
				}
			}
		}
		$pdf->Output();
	}

	// Método que permite generar el reporte de vacaciones pendientes de todos los funcionarios asociados a la búsqueda
	public function metGenerarPendiente()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfUtilizacionPendiente('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$tipoNomina = $_GET['tipo_nomina'];
		$sitTrab = $_GET['sit_trab'];
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$periodo = $_GET['periodo'];
		$empleado = $_GET['empleado'];
		$listarEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $sitTrab, $empleado, $tipoNomina);
		$id = 0;
		foreach($listarEmpleado as $empleado){
			$pdf->SetWidths(array(15, 75));
			$pdf->SetAligns(array('C', 'L'));
			$pdf->SetFont('Arial', 'B', 8);
			$pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255);
			$pkNumEmpleado = $empleado['pk_num_empleado'];
			$listarUtilizacionPeriodo = $this->atReporteModelo->metConsultarUtilizacionPeriodo2($pkNumEmpleado);
			$contador = count($listarUtilizacionPeriodo);

			if($contador>0) {
                $id = $id + 1;
				$pdf->Row(array(
                    $id,
					utf8_decode($empleado['ind_nombre1'] . ' ' . $empleado['ind_nombre2'] . ' ' . $empleado['ind_apellido1'] . ' ' . $empleado['ind_apellido2']),
				));
				$pdf->SetWidths(array(30, 30, 30, 30, 30, 30));
				$pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C'));
				$pdf->SetFont('Arial', '', 8);
				$acumuladorPendiente = 0;
				foreach($listarUtilizacionPeriodo as $periodoUtilizacion){
                    if($periodoUtilizacion['num_pendientes'] !=0){
                        $pdf->SETX(25);
                        $pendiente = $periodoUtilizacion['num_pendientes'];
                        $acumuladorPendiente = $acumuladorPendiente+$pendiente;
                        $anioAnterior = $periodoUtilizacion['num_anio']-1;
                        $pdf->Row(array(
                            $anioAnterior.' - '.$periodoUtilizacion['num_anio'],
                            $periodoUtilizacion['num_dias_derecho'],
                            $periodoUtilizacion['num_dias_gozados'],
                            $periodoUtilizacion['num_dias_interrumpidos'],
                            $periodoUtilizacion['num_total_utilizados'],
                            $periodoUtilizacion['num_pendientes']
                        ));
                    }

					if($periodo!=''){
						$pkNumPeriodo = $periodoUtilizacion['pk_num_periodo'];
						$listarDetallePeriodo = $this->atReporteModelo->metConsultarDetalle($pkNumEmpleado, $pkNumPeriodo);
						$pdf->SetWidths(array(30, 30, 30, 60));
						$pdf->SetAligns(array('C', 'C', 'C', 'C'));
						$pdf->SetFont('Arial', 'B', 8);
						foreach($listarDetallePeriodo as $detalle){
							$pdf->SETX(25);
							$pdf->Row(array(
								$detalle['ind_nombre_detalle'],
								$detalle['num_dias_detalle'],
								$detalle['fecha_inicial'],
								$detalle['fecha_final'],
							));

						}
					}
				}
                $pdf->SetWidths(array(323));
                $pdf->SetAligns(array('C'));
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Row(array(
                    'TOTAL DIAS PENDIENTES:'.$acumuladorPendiente
                ));
                $pdf->Ln(5);

			}

		}
		$pdf->Output();
	}

	// Método que muestra el resumen de vacaciones por periodos de los funcionarios
	public function metGenerarResumenVacacion()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfResumenVacacion('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$tipoNomina = $_GET['tipo_nomina'];
		$sitTrab = $_GET['sit_trab'];
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$periodo = $_GET['periodo'];
		$empleado = $_GET['empleado'];
		$listarEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $sitTrab, $empleado, $tipoNomina);
        $id = 0;
		foreach($listarEmpleado as $empleado){
			$pdf->SetWidths(array(16, 70, 18, 16, 18, 22, 18, 18));
			$pdf->SetAligns(array('C', 'L', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
			$pdf->SetFont('Arial', '', 7);
			$pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255);
			$pkNumEmpleado = $empleado['pk_num_empleado'];
			$calcularPeriodo = $this->atReporteModelo->metCalcularPeriodo($pkNumEmpleado);

			if($calcularPeriodo['dias_derecho']>0){
				$id = $id + 1;
			    $pdf->Row(array(
					$id,
					utf8_decode($calcularPeriodo['ind_nombre1'] . ' ' . $calcularPeriodo['ind_nombre2'] . ' ' . $calcularPeriodo['ind_apellido1'] . ' ' . $calcularPeriodo['ind_apellido2']),
					$calcularPeriodo['fecha_ingreso'],
					$calcularPeriodo['dias_derecho'],
					$calcularPeriodo['dias_gozados'],
					$calcularPeriodo['dias_interrumpidos'],
					$calcularPeriodo['total_utilizado'],
					$calcularPeriodo['pendiente']
				));
				if($periodo!=''){
					$pdf->SetWidths(array(16, 70, 18, 16, 18, 22, 18, 18));
					$pdf->SetAligns(array('C', 'L', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
					$pdf->SetFont('Arial', 'B', 7);
					$listarUtilizacionPeriodo = $this->atReporteModelo->metConsultarUtilizacionPeriodo($pkNumEmpleado);
					foreach($listarUtilizacionPeriodo as $periodoUtilizacion){
						$anioAnterior = $periodoUtilizacion['num_anio']-1;
						$pendiente = $periodoUtilizacion['num_pendientes'];
						$pdf->Row(array(
							'',
							'',
							$anioAnterior.' - '.$periodoUtilizacion['num_anio'],
							$periodoUtilizacion['num_dias_derecho'],
							$periodoUtilizacion['num_dias_gozados'],
							$periodoUtilizacion['num_dias_interrumpidos'],
							$periodoUtilizacion['num_total_utilizados'],
							$pendiente
						));
					}
				}
			}
		}
		$pdf->Output();
	}

	// Método que permite generar el resumen general de vacaciones del funcionario
	public function metGenerarResumenGeneral()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfResumenGeneralVacacion('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$tipoNomina = $_GET['tipo_nomina'];
		$sitTrab = $_GET['sit_trab'];
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$periodo = $_GET['periodo'];
		$empleado = $_GET['empleado'];
		$listarEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $sitTrab, $empleado, $tipoNomina);
		$id = 0;
		foreach($listarEmpleado as $listar) {
			$pkNumEmpleado = $listar['pk_num_empleado'];
			$consultarPeriodo = $this->atReporteModelo->metConsultarPeriodo($pkNumEmpleado);
			$contador = count($consultarPeriodo);
			$pdf->SetFont('Arial', '', 7);
			if($contador>0){
			    $id = $id + 1;
				$pdf->SetFont('Arial', 'B', 7);
				$pdf->Cell(10, 5, $id, 0, 0, 'L');
				$pdf->Cell(180, 5, utf8_decode($listar['ind_nombre1'].' '.$listar['ind_nombre2'].' '.$listar['ind_apellido1'].' '.$listar['ind_apellido2']), 0, 0, 'L');
				$pdf->Ln();
				foreach ($consultarPeriodo as $periodo){
					$anioAnterior = $periodo['num_anio']-1;
					$pdf->SETX(25);
					$pdf->Cell(38, 5, $anioAnterior.' - '.$periodo['num_anio'], 0, 0, 'C');
					$pdf->Cell(20, 5, str_pad($periodo['num_mes'], 2, "0", STR_PAD_LEFT), 0, 0, 'C');
					$pdf->Cell(20, 5, str_pad($periodo['num_dias_derecho'], 2, "0", STR_PAD_LEFT), 0, 0, 'C');
					$pdf->Cell(22, 5, str_pad($periodo['total_dias'], 2, "0", STR_PAD_LEFT), 0, 0, 'C');
					$pdf->Cell(25, 5, str_pad($periodo['num_dias_interrumpidos'], 2, "0", STR_PAD_LEFT), 0, 0, 'C');
					$pdf->Cell(25, 5, str_pad($periodo['num_total_utilizados'], 2, "0", STR_PAD_LEFT), 0, 0, 'C');
					$pdf->Cell(25, 5, str_pad($periodo['num_pendientes'], 2, "0", STR_PAD_LEFT), 0, 0, 'C');
					$pdf->Ln();
					// Muestro la utilización
					if($periodo!=''){
						$pkNumPeriodo = $periodo['pk_num_periodo'];
						$consultarUtilizacion = $this->atReporteModelo->metMostrarUtilizacion($pkNumPeriodo);
						$contadorUtilizacion = count($consultarUtilizacion);
						if($contadorUtilizacion>0){
							$pdf->SetFont('Arial', 'B', 7);
							foreach ($consultarUtilizacion as $utilizacion){
								$pdf->SETX(63);
								$pdf->Cell(20, 5, utf8_decode($utilizacion['ind_nombre_detalle']), 0, 0, 'L');
								$pdf->Cell(20, 5, utf8_decode($utilizacion['num_dias_utiles'].' días'), 0, 0, 'R');
								$pdf->Cell(45, 5, 'Del '.$utilizacion['fecha_inicio'].' Al '.$utilizacion['fecha_fin'], 0, 0, 'R');
								$pdf->Ln();
							}
						} else {
							$pdf->Cell(190, 5, 'NO POSEE UTILIZACIÓN', 0, 1, 'C');
						}
					}
				}
			}

		}
		$pdf->Output();
	}
}
