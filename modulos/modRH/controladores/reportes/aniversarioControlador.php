<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class aniversarioControlador extends Controlador
{
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('aniversario', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo
		);
		$tipoTrabajador = $this->atReporteModelo->metMostrarSelect('TIPOTRAB');
		$tipoNomina = $this->atReporteModelo->metListarNomina();
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $listadoEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, '', '', '', '', '', '', '', '', '');
        $centroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, 0);
		$this->atVista->assign('tipoTrabajador', $tipoTrabajador);
		$this->atVista->assign('nomina', $tipoNomina);
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('datos', $listadoEmpleado);
        $this->atVista->assign('centroCosto', $centroCosto);
		$this->atVista->metRenderizar('listadoAniversario');
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarDependencia()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$listarDependencia=$this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="">&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarCentroCosto()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
		$listarCentroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, $pkNumDependencia);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="centro_costo" id="centro_costo"><option value="0" disabled selected>&nbsp;</option>';
		foreach($listarCentroCosto as $centroCosto){
			$a .='<option value="'.$centroCosto['pk_num_centro_costo'].'">'.$centroCosto['ind_descripcion_centro_costo'].'</option>';
		}
		$a .= '</select><label for="pk_num_centro_costo"><i class="glyphicon glyphicon-briefcase"></i> Centro Costo</label></div>';
		echo $a;
	}

	public function metObtenerMes($mes)
	{
		switch($mes){
			case 1:
				$nombreMes = 'ENERO';
				break;
			case 2:
				$nombreMes = 'FEBRERO';
				break;
			case 3:
				$nombreMes = 'MARZO';
				break;
			case 4:
				$nombreMes = 'ABRIL';
				break;
			case 5:
				$nombreMes = 'MAYO';
				break;
			case 6:
				$nombreMes = 'JUNIO';
				break;
			case 7:
				$nombreMes = 'JULIO';
				break;
			case 8:
				$nombreMes = 'AGOSTO';
				break;
			case 9:
				$nombreMes = 'SEPTIEMBRE';
				break;
			case 10:
				$nombreMes = 'OCTUBRE';
				break;
			case 11:
				$nombreMes = 'NOVIEMBRE';
				break;
			case 12:
				$nombreMes = 'DICIEMBRE';
				break;
		}
		return $nombreMes;
	}

	// Método que permite calcular el tiempo de servicio de un empleado
	public function metTiempoServicio($fechaIngreso, $fechaEgreso)
	{
		$fechaInicial = new DateTime($fechaIngreso);
		$fechaFinal = new DateTime($fechaEgreso);
		$interval = $fechaInicial->diff($fechaFinal);
		$datosServicio = array(
			'anio' => $interval->format('%y'),
			'mes' => $interval->format('%m'),
			'dia' => $interval->format('%d')
		);
		return $datosServicio;
	}

	// Método que se encarga de obtener el total de antecedentes
	public function metObtenerServicio($acumuladorDia, $acumuladorMes, $acumuladorAnio)
	{

		$_Dias_A_Meses = intval($acumuladorDia / 30);
		$acumuladorMes += $_Dias_A_Meses;
		$_Meses_A_Anios = intval($acumuladorMes / 12);
		$acumuladorAnio += $_Meses_A_Anios;
		$Dias  = $acumuladorDia - (intval($acumuladorDia / 30) * 30);
		$Meses = $acumuladorMes - (intval($acumuladorMes / 12) * 12);
		$Anios = $acumuladorAnio;
		$totalAntecedente = array(
			'anio' => $Anios,
			'mes' => $Meses,
			'dia' => $Dias
		);
		return $totalAntecedente;
	}

	// Método que permite calcular la edad de una persona
	public function metCalcularFechaEdad($edadPrevia)
	{
		$fecha = date('Y-m-d');
		$nuevafecha = strtotime ( '-'.$edadPrevia.' year' , strtotime ( $fecha ) ) ;
		$nuevafechaTotal = date ( 'Y-m-d' , $nuevafecha );
		return $nuevafechaTotal;
	}

	// Método que permite listar los empleados por dependencia y organismo
	public function metGenerarAniversario()
	{
		$pkNumOrganismo = $_POST['pk_num_organismo'];
		$pkNumDependencia = $_POST['pk_num_dependencia'];
		$centro_costo = $_POST['centro_costo'];
		$mesIngreso = $_POST['mes_ingreso'];
		$anio1 = $_POST['anio1'];
		$anio2 = $_POST['anio2'];
		$edoReg = $_POST['edo_reg'];
		$sitTrab = $_POST['sit_trab'];
		$tipoNomina = $_POST['tipo_nomina'];
		$tipoTrabajador = $_POST['tipo_trabajador'];
		$datos = array(
			'pk_num_organismo' => $pkNumOrganismo,
			'pk_num_dependencia' => $pkNumDependencia,
			'centro_costo' => $centro_costo,
			'mes_ingreso' => $mesIngreso,
			'anio1' => $anio1,
			'anio2' => $anio2,
			'edoReg' => $edoReg,
			'sitTrab' => $sitTrab,
			'tipoNomina' => $tipoNomina,
			'tipoTrabajador' => $tipoTrabajador
		);
		$this->atVista->assign('datos', $datos);
		$this->atVista->metRenderizar('generarAniversario', 'modales');
	}

	// Método que permite
	public function metGenerarReporteAniversario()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfAniversario('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$centroCosto = $_GET['centro_costo'];
		$mesIngreso = $_GET['mes_ingreso'];
		$anio1 = $_GET['anio1'];
		$anio2 = $_GET['anio2'];
		$edoReg = $_GET['edoReg'];
		$sitTrab = $_GET['sitTrab'];
		$tipoNomina = $_GET['tipo_nomina'];
		$tipoTrabajador = $_GET['tipo_trabajador'];
		if(($anio1!='')&&($anio2!='')){
			$obtenerFecha1 = $this->metCalcularFechaEdad($anio1);
			$obtenerFecha2 = $this->metCalcularFechaEdad($anio2);
		} else {
			$obtenerFecha1 = '';
			$obtenerFecha2 = '';
		}
		$pdf->SetWidths(array(20, 75, 57, 25, 13));
		$pdf->SetAligns(array('C', 'L', 'L', 'C','C'));
		$pdf->SetFont('Arial', '', 8);
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
		if($mesIngreso>0){
			$valor = $mesIngreso;
		} else {
			$valor = 12;
			$mesIngreso = 1;
		}
		$i = 0;
		for($i=$mesIngreso; $i<=$valor; $i++){
			if($i==$mesIngreso){
				$mostrarMes = $this->metObtenerMes($mesIngreso);
			} else {
				$mostrarMes = $this->metObtenerMes($i);
			}
			$mesIngresoNuevo = str_pad($i, 2, '0', STR_PAD_LEFT);
			$empleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $centroCosto, $mesIngresoNuevo, $obtenerFecha1, $obtenerFecha2, $edoReg, $sitTrab, $tipoNomina, $tipoTrabajador);
			$contadorEmpleado = count($empleado);
			$contar = 0;
			foreach($empleado as $emp){
				// Calcular tiempo de servicio en la institución
				$acumuladorDia = 0;
				$acumuladorMes = 0;
				$acumuladorAnio = 0;
				$tiempoActual = $this->atReporteModelo->metEmpleadoNivelación($emp['pk_num_empleado']);
				foreach($tiempoActual as $tiempo){
					$fechaInicio = $tiempo['fec_fecha_registro'];
					$fechaFin = $tiempo['fec_fecha_hasta'];
					if($fechaFin=='0000-00-00'){
						$fechaFin = date('Y-m-d');
					}
					$tiempoPrevio = $this->metTiempoServicio($fechaInicio, $fechaFin);
					$acumuladorDia = $acumuladorDia + $tiempoPrevio['dia'];
					$acumuladorMes = $acumuladorMes + $tiempoPrevio['mes'];
					$acumuladorAnio = $acumuladorAnio + $tiempoPrevio['anio'];
				}
				$totalOrganismo = $this->metObtenerServicio($acumuladorDia, $acumuladorMes, $acumuladorAnio);
				$tiempo = $totalOrganismo['anio'];
				if($tiempo>0){
					if($contar==0){
						$pdf->SetFont('Arial', 'B', 8);
						$pdf->Cell(15, 5, $mostrarMes, 0, 0, 'L');
						$pdf->SetFont('Arial', '', 8);
						$pdf->Ln();
					}
					$i++;
					if ($i % 2 == 0) { $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);  }
					else { $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255); }
					$pdf->Row(array(
						number_format($emp['ind_cedula_documento'],0 , ' , ' ,  '.'),
						utf8_decode($emp['ind_nombre1'].' '.$emp['ind_nombre2'].' '.$emp['ind_apellido1'].' '.$emp['ind_apellido2']),
						utf8_decode($emp['ind_descripcion_cargo']),
						$emp['fecha_ingreso'],
						$tiempo
					));
					$contar++;
				}
				if($contar==$contadorEmpleado){
					$pdf->SetFont('Arial', 'B', 8);
					$pdf->Cell(80, 5, 'Nro. de Trabajadores: '.$contar, 0, 0, 'R');
					$pdf->Ln();
					$pdf->SetFont('Arial', '', 8);
				}
			}
		}
		$pdf->Output();
	}

	public function metGenerarReporteDemostrativo()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfAniversarioDemostrativo('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$centroCosto = $_GET['centro_costo'];
		$anio1 = $_GET['anio1'];
		$anio2 = $_GET['anio2'];
		$edoReg = $_GET['edoReg'];
		$sitTrab = $_GET['sitTrab'];
		$tipoNomina = $_GET['tipo_nomina'];
		$tipoTrabajador = $_GET['tipo_trabajador'];
		if(($anio1!='')&&($anio2!='')){
			$obtenerFecha1 = $this->metCalcularFechaEdad($anio1);
			$obtenerFecha2 = $this->metCalcularFechaEdad($anio2);
		} else {
			$obtenerFecha1 = '';
			$obtenerFecha2 = '';
		}
		$pdf->SetWidths(array(20, 56, 47, 14, 11, 8, 8, 8, 8, 8, 8, 8));
		$pdf->SetAligns(array('C', 'L', 'L', 'C','C', 'C','C', 'C','C','C','C'));
		$pdf->SetFont('Arial', '', 6);
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
		$empleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $centroCosto, '', $obtenerFecha1, $obtenerFecha2, $edoReg, $sitTrab, $tipoNomina, $tipoTrabajador);
		$i = 0;
		foreach($empleado as $emp){
			// Calcular tiempo de servicio en la institución
			$acumuladorDia = 0;
			$acumuladorMes = 0;
			$acumuladorAnio = 0;
			$tiempoActual = $this->atReporteModelo->metEmpleadoNivelación($emp['pk_num_empleado']);
			foreach($tiempoActual as $tiempo){
				$fechaInicio = $tiempo['fec_fecha_registro'];
				$fechaFin = $tiempo['fec_fecha_hasta'];
				if($fechaFin=='0000-00-00'){
					$fechaFin = date('Y-m-d');
				}
				$tiempoPrevio = $this->metTiempoServicio($fechaInicio, $fechaFin);
				$acumuladorDia = $acumuladorDia + $tiempoPrevio['dia'];
				$acumuladorMes = $acumuladorMes + $tiempoPrevio['mes'];
				$acumuladorAnio = $acumuladorAnio + $tiempoPrevio['anio'];
			}
			$totalOrganismo = $this->metObtenerServicio($acumuladorDia, $acumuladorMes, $acumuladorAnio);
			$tiempo = $totalOrganismo['anio'];
			$x5=''; $x10=''; $x15=''; $x20=''; $x25=''; $x30='';
			if($tiempo>0){
				// Marco con X
				if($tiempo>=5){
					$x5 = 'x';
				}
				if($tiempo>=10){
					$x10 = 'x';
				}
				if($tiempo>=15){
					$x15 = 'x';
				}
				if($tiempo>=20){
					$x20 = 'x';
				}
				if($tiempo>=25){
					$x25 = 'x';
				}
				if($tiempo>=30){
					$x30 = 'x';
				}
				$i++;
				if ($i % 2 == 0) { $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);  }
				else { $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255); }
				$pdf->Row(array(
					number_format($emp['ind_cedula_documento'],0 , ' , ' ,  '.'),
					utf8_decode($emp['ind_nombre1'].' '.$emp['ind_nombre2'].' '.$emp['ind_apellido1'].' '.$emp['ind_apellido2']),
					utf8_decode($emp['ind_descripcion_cargo']),
					$emp['fecha_ingreso'],
					$tiempo,
					$x5,
					$x10,
					$x15,
					$x20,
					$x25,
					$x30
				));
			}
		}
		$pdf->Output();
	}

	public function metGenerarReporteJubilacion()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfAniversarioJubilacion('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$centroCosto = $_GET['centro_costo'];
		$anio1 = $_GET['anio1'];
		$anio2 = $_GET['anio2'];
		$edoReg = $_GET['edoReg'];
		$sitTrab = $_GET['sitTrab'];
		$tipoNomina = $_GET['tipo_nomina'];
		$tipoTrabajador = $_GET['tipo_trabajador'];
		if(($anio1!='')&&($anio2!='')){
			$obtenerFecha1 = $this->metCalcularFechaEdad($anio1);
			$obtenerFecha2 = $this->metCalcularFechaEdad($anio2);
		} else {
			$obtenerFecha1 = '';
			$obtenerFecha2 = '';
		}
		$pdf->SetWidths(array(20, 65, 54, 14, 11, 11, 13, 11));
		$pdf->SetAligns(array('C', 'L', 'L', 'C', 'C', 'C', 'C', 'C'));
		$pdf->SetFont('Arial', '', 6);
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
		$fechaActual = date('Y-m-d');
		$empleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $centroCosto, '', $obtenerFecha1, $obtenerFecha2, $edoReg, $sitTrab, $tipoNomina, $tipoTrabajador);
		$i = 0;
		foreach($empleado as $emp){
			$pkNumEmpleado = $emp['pk_num_empleado'];
			// Calcular edad
			$fechaNacimiento = $emp['fec_nacimiento'];
			$edad = $this->metTiempoServicio($fechaNacimiento, $fechaActual);
			// Calcular tiempo de servicio en la institución
			$acumuladorDia = 0;
			$acumuladorMes = 0;
			$acumuladorAnio = 0;
			$tiempoActual = $this->atReporteModelo->metEmpleadoNivelación($pkNumEmpleado);
			foreach($tiempoActual as $tiempo){
				$fechaInicio = $tiempo['fec_fecha_registro'];
				$fechaFin = $tiempo['fec_fecha_hasta'];
				if($fechaFin=='0000-00-00'){
					$fechaFin = date('Y-m-d');
				}
				$tiempoPrevio = $this->metTiempoServicio($fechaInicio, $fechaFin);
				$acumuladorDia = $acumuladorDia + $tiempoPrevio['dia'];
				$acumuladorMes = $acumuladorMes + $tiempoPrevio['mes'];
				$acumuladorAnio = $acumuladorAnio + $tiempoPrevio['anio'];
			}
			$totalOrganismo = $this->metObtenerServicio($acumuladorDia, $acumuladorMes, $acumuladorAnio);
			$anioInst = $totalOrganismo['anio'];
			// Calcular tiempo de servicio en otras instituciones
			$contadorDia = 0;
			$contadorMes = 0;
			$contadorAnio = 0;
			$antecedente = $this->atReporteModelo->metConsultarAntecedente($pkNumEmpleado);
			foreach($antecedente as $ant){
				$fechaI = $ant['fec_ingreso'];
				$fechaF = $ant['fec_egreso'];
				$tiempoGubernamental = $this->metTiempoServicio($fechaI, $fechaF);
				$contadorDia = $contadorDia + $tiempoGubernamental['dia'];
				$contadorMes = $contadorMes + $tiempoGubernamental['mes'];
				$contadorAnio = $contadorAnio + $tiempoGubernamental['anio'];
			}
			$totalGubernamental = $this->metObtenerServicio($contadorDia, $contadorMes, $contadorAnio);
			$anioGub = $totalGubernamental['anio'];
			// TIEMPO TOTAL
			$tiempo = $anioInst + $anioGub;
			if($tiempo>0) {
				$i++;
				if ($i % 2 == 0) { $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);  }
				else { $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255); }
				$pdf->Row(array(
					number_format($emp['ind_cedula_documento'], 0, ' , ', '.'),
					utf8_decode($emp['ind_nombre1'] . ' ' . $emp['ind_nombre2'] . ' ' . $emp['ind_apellido1'] . ' ' . $emp['ind_apellido2']),
					utf8_decode($emp['ind_descripcion_cargo']),
					$emp['fecha_ingreso'],
					$edad['anio'],
					$anioInst,
					$anioGub,
					$tiempo
				));
			}
		}
		$pdf->Output();
	}
}
