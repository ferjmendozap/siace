<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de HCM. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class hcmControlador extends Controlador
{
    private $atReporteModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atReporteModelo = $this->metCargarModelo('hcm', 'reportes');
    }

    // Index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
            'multi-select/multi-select555c'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'multi-select/jquery.multi-select'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );

        $this->atVista->metCargarJsComplemento($validar);
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atReporteModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
        $datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
        $pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
        $empleado = array(
            'pk_num_organismo' => $pkNumOrganismo
        );
        $ayudaGlobal = $this->atReporteModelo->metObtenerAyudaMedica(1);
        $listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $listarEmpleado = $this->atReporteModelo->metListarEmpleados($pkNumOrganismo, '', '');
        $listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('listarEmpleado', $listarEmpleado);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('ayudaMedica', $ayudaGlobal);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        $this->atVista->metRenderizar('consumoFuncionario');
    }

    // Método que permite cargar el consumo de hcm por institución
    public function metObtenerConsumoInstitucion()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
            'multi-select/multi-select555c'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'multi-select/jquery.multi-select'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atReporteModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
        $datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
        $pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
        $empleado = array(
            'pk_num_organismo' => $pkNumOrganismo
        );
        $ayudaGlobal = $this->atReporteModelo->metObtenerAyudaMedica(1);
        $listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $listarEmpleado = $this->atReporteModelo->metListarEmpleados($pkNumOrganismo, '', '');
        $listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $listarInstitucion = $this->atReporteModelo->metListarInstitucion($pkNumOrganismo, '', '', '');
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('listarEmpleado', $listarEmpleado);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('ayudaMedica', $ayudaGlobal);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        $this->atVista->assign('listarInstitucion', $listarInstitucion);
        $this->atVista->metRenderizar('consumoInstitucion');
    }

    // Método que permite cargar el consumo de hcm por partidas
    public function metObtenerConsumoPartidas()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
            'multi-select/multi-select555c'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'multi-select/jquery.multi-select'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atReporteModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
        $datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
        $pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
        $empleado = array(
            'pk_num_organismo' => $pkNumOrganismo
        );
        $ayudaGlobal = $this->atReporteModelo->metObtenerAyudaMedica(2);
        $pkNumAyudaGlobal = $ayudaGlobal['pk_num_ayuda_global'];
        $ayudaMedicaGlobal = $this->atReporteModelo->metObtenerAyudaMedica(1);
        $this->atVista->assign('ayudaMedica', $ayudaMedicaGlobal);
        if($pkNumAyudaGlobal==''){
            $pkNumAyudaGlobal = 0;
        } 
        // Obtengo la ultima ayuda medica
        $listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $listarEmpleado = $this->atReporteModelo->metListarEmpleados($pkNumOrganismo, '', '');
        $listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $listarAyuda = $this->atReporteModelo->metObtenerAyudaEspecifica($pkNumAyudaGlobal);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('listarEmpleado', $listarEmpleado);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);

        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        $this->atVista->assign('listarAyuda', $listarAyuda);
        $this->atVista->metRenderizar('consumoPartidas');
    }

    // Método que permite listar las dependencias del organismo
    public function metBuscarDependencia()
    {
        $pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
        $listarDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="">&nbsp;</option>';
        foreach ($listarDependencia as $listarDependencia) {
            $a .= '<option value="' . $listarDependencia['pk_num_dependencia'] . '">' . $listarDependencia['ind_dependencia'] . '</option>';
        }
        $a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
        echo $a;
    }

    // Método que permite listar la ayuda especifica de proveniente de un beneficio
    public function metBuscarAyudaEspecifica()
    {
        $pkNumAyudaGlobal = $this->metObtenerInt('pk_num_ayuda_global');
        $listarAyuda = $this->atReporteModelo->metObtenerAyudaEspecifica($pkNumAyudaGlobal);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_ayuda_especifica" id="pk_num_ayuda_especifica"> <option value="">&nbsp;</option>';
        foreach ($listarAyuda as $listar) {
            $a .= '<option value="' . $listar['pk_num_ayuda_especifica'] . '">' . $listar['ind_descripcion_especifica'] . '</option>';
        }
        $a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Partidas</label></div>';
        echo $a;
    }

    // Método que permite realizar la busqueda de los empleados
    public function metListarEmpleado()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $pkNumDependencia = $_POST['pk_num_dependencia'];
        $pkNumAyudaGlobal = $_POST['pk_num_ayuda_global'];
        $listadoEmpleados = $this->atReporteModelo->metListarEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumAyudaGlobal);
        echo json_encode($listadoEmpleados);
    }

    // Método que permite generar el consumo de hcm por funcionario
    public function metGenerarConsumoFuncionario()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraReportes', 'modRH');
        $pdf = new pdfFuncionarioHcm('P', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        //Recibo los valores
        $pkNumEmpleado = $_GET['pk_num_empleado'];
        $pkNumAyudaGlobal = $_GET['pk_num_ayuda_global'];
        $empleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(22, 5, 'FUNCIONARIO: ', 0, 0, 'L');
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(130, 5, utf8_decode($empleado['ind_nombre1'].' '.$empleado['ind_nombre2'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2']), 0, 1, 'L');
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(22, 5, utf8_decode('CÉDULA: '), 0, 0, 'L');
        $pdf->SetFont('Arial', '', 8);
        $pdf->Cell(130, 5, number_format($empleado['ind_cedula_documento'], 0, '', '.'), 0, 1, 'L');
        // Obtengo el listado de ayudas disponibles

        $obtenerAyudaEspecifica = $this->atReporteModelo->metObtenerAyudaEspecifica($pkNumAyudaGlobal);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(15, 5, utf8_decode('N°'), 0, 0, 'C');
        $pdf->Cell(70, 5, utf8_decode('T. Servicio'), 0, 0, 'C');
        $pdf->Cell(20, 5, utf8_decode('Límite'), 0, 0, 'C');
        $pdf->Cell(22, 5, utf8_decode('N° de Solicitud'), 0, 0, 'C');
        $pdf->Cell(18, 5, utf8_decode('Fecha'), 0, 0, 'C');
        $pdf->Cell(25, 5, utf8_decode('Monto Solicitado'), 0, 0, 'C');
        $pdf->Cell(25, 5, utf8_decode('Monto Disponible'), 0, 0, 'C');
        $pdf->SetDrawColor(0, 0, 0); $pdf->Rect(10, 49, 195, 0.2);
        $pdf->Ln();
        $pdf->SetWidths(array(15, 70, 20, 22, 18, 25, 25));
        $pdf->SetAligns(array('C', 'L', 'R', 'C', 'C', 'C', 'C', 'C'));
        $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);
        $pdf->SetFont('Arial', '', 8);
        $i = 1;
        $acumuladorTotal = 0;
        $totalMontoSolicitado = 0;
        $montoDisponible = 0;
        foreach($obtenerAyudaEspecifica as $ayuda){
            $pkNumAyudaEspecifica = $ayuda['pk_num_ayuda_especifica'];
            $numLimiteAyuda = $ayuda['num_limite_esp'];
            $acumuladorTotal = $acumuladorTotal + $numLimiteAyuda;
            $pdf->Row(array(
                '',
                utf8_decode($ayuda['ind_descripcion_especifica']),
                number_format($numLimiteAyuda, 2, ',', '.'),
                '',
                '',
                '',
                ''
            ));
            // Listo las solicitudes del empleado en base a ese beneficio
            $listadobeneficio = $this->atReporteModelo->metObtenerAyudaFuncionario($pkNumAyudaGlobal, $pkNumAyudaEspecifica, $pkNumEmpleado);
            $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255);
            $pdf->SetWidths(array(15, 70, 20, 22, 18, 25, 25));
            $pdf->SetAligns(array('C', 'L', 'R', 'C', 'C', 'C', 'R', 'R'));
            $montoSolicitado = 0;
            foreach($listadobeneficio as $beneficio){
                $numLimiteAyuda = $numLimiteAyuda - $beneficio['num_monto'];
                // Muestro los resultados
                $pdf->Row(array(
                    $i++,
                    '',
                    '',
                    $beneficio['pk_beneficio_medico'],
                    $beneficio['fecha_solicitud'],
                    number_format($beneficio['num_monto'], 2, ',', '.'),
                    number_format($numLimiteAyuda, 2, ',', '.')
                ));
                $montoSolicitado = $montoSolicitado + $beneficio['num_monto'];
            }
            $totalMontoSolicitado = $totalMontoSolicitado + $montoSolicitado;
            $montoDisponible = $acumuladorTotal - $totalMontoSolicitado;
            $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);
        }
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(15, 5, '', 0, 0, 'C');
        $pdf->Cell(70, 5, utf8_decode('Monto Total'), 0, 0, 'L');
        $pdf->Cell(20, 5, number_format($acumuladorTotal, 2, ',', '.'), 0, 0, 'R');
        $pdf->Cell(22, 5, '', 0, 0, 'C');
        $pdf->Cell(18, 5, '', 0, 0, 'C');
        $pdf->Cell(20, 5, number_format($totalMontoSolicitado, 2, ',', '.'), 0, 0, 'R');
        $pdf->Cell(30, 5, number_format($montoDisponible, 2, ',', '.'), 0, 0, 'R');
        $pdf->Ln(); $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(100, 5, 'REALIZADO POR', 0, 0, 'C');
        $pdf->Cell(100, 5, 'APROBADO POR', 0, 0, 'C');
        $pdf->Output();
    }


    // Método que permite generar el resumen consolodado de eventos
    public function metGenerarConsumoInstitucion()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraReportes', 'modRH');
        $pdf = new pdfHcmInstitucion('P', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        $pkNumOrganismo = $_GET['pk_num_organismo'];
        $pkNumDependencia = $_GET['pk_num_dependencia'];
        $pkNumAyudaGlobal = $_GET['pk_num_ayuda_global'];
        $pkNumInstitucion = $_GET['pk_num_institucion'];
        $listarInstitucion = $this->atReporteModelo->metListarInstitucion($pkNumOrganismo, $pkNumDependencia, $pkNumAyudaGlobal, $pkNumInstitucion);
        $ayudaGlobal = $this->atReporteModelo->metObtenerAyudaGlobal($pkNumAyudaGlobal);
        if($pkNumDependencia!=''){
            $dependencia = $this->atReporteModelo->metListarDependencia('', 2, $pkNumDependencia);
            $nombreDependencia = $dependencia['ind_dependencia'];
        } else {
            $nombreDependencia = '';
        }
        $pdf->hcm($nombreDependencia, $ayudaGlobal['ind_descripcion']);
        $pdf->SetWidths(array(25, 130, 40));
        $pdf->SetAligns(array('C', 'L', 'R'));
        $j = 0;
        $pdf->SetFont('Arial', '', 8);
        foreach($listarInstitucion as $listar){
            if ($j % 2 == 0) { $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);  }
            else { $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255); }
            $pdf->Row(array(
                $listar['casos'],
                utf8_decode($listar['ind_nombre_institucion']),
                number_format($listar['total'], 2, ',', '.')
            ));
            $j++;
        }
        $pdf->Output();
    }

    // Método que permite generar el resumen consolodado de eventos
    public function metGenerarConsumoPartidas()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraReportes', 'modRH');
        $pdf = new pdfHcmPartidas('P', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        $pkNumOrganismo = $_GET['pk_num_organismo'];
        $pkNumDependencia = $_GET['pk_num_dependencia'];
        $pkNumAyudaGlobal = $_GET['pk_num_ayuda_global'];
        $pkNumAyudaEspecifica = $_GET['pk_num_ayuda_especifica'];
        $pdf->SetWidths(array(155, 40));
        $pdf->SetAligns(array('L', 'R'));
        $j = 0;
        $pdf->SetFont('Arial', '', 8);
        $listarAyuda = $this->atReporteModelo->metObtenerPartidas($pkNumOrganismo, $pkNumDependencia, $pkNumAyudaGlobal, $pkNumAyudaEspecifica);
        foreach($listarAyuda as $listar){
            if ($j % 2 == 0) { $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);  }
            else { $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255); }
            $pdf->Row(array(
                utf8_decode($listar['ind_descripcion_especifica']),
                number_format($listar['total'], 2, ',', '.')
            ));
            $j++;
        }
        $pdf->Output();
    }
}