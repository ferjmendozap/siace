<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_Modulo.'modCP'.DS.'controladores'.DS.'trait'.DS.'funcionesTrait.php';
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class constanciaTrabajoControlador extends Controlador
{
    use funcionesTrait;
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('constanciaTrabajo', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
        $js = array('materialSiace/core/demo/DemoTableDynamic',  'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo
		);
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
		$tipoNomina = $this->atReporteModelo->metListarNomina();
		$tipoTrabajador = $this->atReporteModelo->metMostrarSelect('TIPOTRAB');
		$listadoEmpleado = $this->atReporteModelo->metConsultarEmpleados();
		// Cargar a la vista
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
		$this->atVista->assign('nomina', $tipoNomina);
		$this->atVista->assign('tipoTrabajador', $tipoTrabajador);
		$this->atVista->assign('listadoEmpleado', $listadoEmpleado);
        // Cargo el listado de empleados
        $empleados = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, '', '', '', '', '', '', '', '', '', '', '', '', '');
        $this->atVista->assign('datos', $empleados);
		$this->atVista->metRenderizar('listadoEmpleado');
	}

    // Método que permite calcular la edad de una persona
    public function metCalcularEdad($edadPrevia)
    {
        $fecha = date('Y-m-d');
        $nuevafecha = strtotime ( '-'.$edadPrevia.' year' , strtotime ( $fecha ) ) ;
        $nuevafechaTotal = date ( 'Y-m-d' , $nuevafecha );
        return $nuevafechaTotal;
    }

    // Método que permite crear el listado de empleados
    public function metListarEmpleados()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $pkNumDependencia = $_POST['pk_num_dependencia'];
        $tipoNomina = $_POST['tipo_nomina'];
        $tipoTrabajador = $_POST['tipo_trabajador'];
        $cedula = $_POST['cedula'];
        $empleado = $_POST['empleado'];
        $rango = $_POST['rango'];
        $edadPrevia = $_POST['edad'];
        $edo_reg = $_POST['edo_reg'];
        $sit_trab = $_POST['sit_trab'];
        $ordenar = $_POST['ordenar'];
        $buscar = $_POST['buscar'];
        $fechaInicioPrevio = $_POST['fechaInicio'];
        $fechaFinPrevio = $_POST['fechaFin'];
        // Cambio el formato de las fechas
        if(($fechaInicioPrevio!='')&&($fechaFinPrevio!='')) {
            $explodeInicio = explode('/', $fechaInicioPrevio);
            $fechaInicio = $explodeInicio[2].'-'.$explodeInicio[1].'-'.$explodeInicio[0];
            $explodeFin = explode('/', $fechaFinPrevio);
            $fechaFin = $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];
        } else {
            $fechaInicio = '';
            $fechaFin = '';
        }
        // Calculo la fecha tentativa para realizar la busqueda por edad
        if(($edadPrevia!='')&&($rango!='')){
            $fechaEdad = $this->metCalcularEdad($edadPrevia);
            $rangoEdad = $rango;
        } else {
            $fechaEdad = '';
            $rangoEdad = '';
        }

        $listadoEmpleados = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $tipoNomina, $tipoTrabajador, $cedula, $empleado, $rangoEdad, $fechaEdad, $edo_reg, $sit_trab, $ordenar, $fechaInicio, $fechaFin, $buscar);
        echo json_encode($listadoEmpleados);
    }

    // FUNCIONES DE CONVERSION DE NUMEROS A LETRAS.
    public function metNumLetras($num, $fem = true, $dec = true) {
//if (strlen($num) > 14) die("El número introducido es demasiado grande");
        $matuni[2]  = "dos";
        $matuni[3]  = "tres";
        $matuni[4]  = "cuatro";
        $matuni[5]  = "cinco";
        $matuni[6]  = "seis";
        $matuni[7]  = "siete";
        $matuni[8]  = "ocho";
        $matuni[9]  = "nueve";
        $matuni[10] = "diez";
        $matuni[11] = "once";
        $matuni[12] = "doce";
        $matuni[13] = "trece";
        $matuni[14] = "catorce";
        $matuni[15] = "quince";
        $matuni[16] = "dieciseis";
        $matuni[17] = "diecisiete";
        $matuni[18] = "dieciocho";
        $matuni[19] = "diecinueve";
        $matuni[20] = "veinte";
        $matunisub[2] = "dos";
        $matunisub[3] = "tres";
        $matunisub[4] = "cuatro";
        $matunisub[5] = "quin";
        $matunisub[6] = "seis";
        $matunisub[7] = "sete";
        $matunisub[8] = "ocho";
        $matunisub[9] = "nove";
        $matdec[2] = "veint";
        $matdec[3] = "treinta";
        $matdec[4] = "cuarenta";
        $matdec[5] = "cincuenta";
        $matdec[6] = "sesenta";
        $matdec[7] = "setenta";
        $matdec[8] = "ochenta";
        $matdec[9] = "noventa";
        $matsub[3]  = "mill";
        $matsub[5]  = "bill";
        $matsub[7]  = "mill";
        $matsub[9]  = "trill";
        $matsub[11] = "mill";
        $matsub[13] = "bill";
        $matsub[15] = "mill";
        $matmil[4]  = "millones";
        $matmil[6]  = "billones";
        $matmil[7]  = "de billones";
        $matmil[8]  = "millones de billones";
        $matmil[10] = "trillones";
        $matmil[11] = "de trillones";
        $matmil[12] = "millones de trillones";
        $matmil[13] = "de trillones";
        $matmil[14] = "billones de trillones";
        $matmil[15] = "de billones de trillones";
        $matmil[16] = "millones de billones de trillones";
        $num = trim((string)@$num);
        if ($num[0] == "-") {
            $neg = "menos ";
            $num = substr($num, 1);
        }else
            $neg = "";
        while ($num[0] == "0") $num = substr($num, 1);
        if ($num[0] < "1" or $num[0] > 9) $num = "0" . $num;
        $zeros = true;
        $punt = false;
        $ent = "";
        $fra = "";
        for ($c = 0; $c < strlen($num); $c++) {
            $n = $num[$c];
            if (! (strpos(".,´´`", $n) === false)) {
                if ($punt) break;
                else{
                    $punt = true;
                    continue;
                }
            }elseif (! (strpos("0123456789", $n) === false)) {
                if ($punt) {
                    if ($n != "0") $zeros = false;
                    $fra .= $n;
                }else
                    $ent .= $n;
            }else
                break;
        }

        $ent = "     " . $ent;

        if ($dec and $fra and ! $zeros) {
            $fin = " coma";
            for ($n = 0; $n < strlen($fra); $n++) {
                if (($s = $fra[$n]) == "0")
                    $fin .= " cero";
                elseif ($s == "1")
                    $fin .= $fem ? " una" : " un";
                else
                    $fin .= " " . $matuni[$s];
            }
        }else
            $fin = "";
        if ((int)$ent === 0) return "Cero " . $fin;
        $tex = "";
        $sub = 0;
        $mils = 0;
        $neutro = false;

        while ( ($num = substr($ent, -3)) != "   ") {

            $ent = substr($ent, 0, -3);
            if (++$sub < 3 and $fem) {
                $matuni[1] = "una";
                $subcent = "as";
            }else{
                //$matuni[1] = $neutro ? "un" : "uno";
                $matuni[1] = $neutro ? "un" : "un";
                $subcent = "os";
            }
            $t = "";
            $n2 = substr($num, 1);
            if ($n2 == "00") {
            }elseif ($n2 < 21)
                $t = " " . $matuni[(int)$n2];
            elseif ($n2 < 30) {
                $n3 = $num[2];
                if ($n3 != 0) $t = "i" . $matuni[$n3];
                $n2 = $num[1];
                $t = " " . $matdec[$n2] . $t;
            }else{
                $n3 = $num[2];
                if ($n3 != 0) $t = " y " . $matuni[$n3];
                $n2 = $num[1];
                $t = " " . $matdec[$n2] . $t;
            }

            $n = $num[0];
            if ($n == 1) {
                if ($num == 100) $t = " cien" . $t; else $t = " ciento" . $t;
            }elseif ($n == 5){
                $t = " " . $matunisub[$n] . "ient" . $subcent . $t;
            }elseif ($n != 0){
                $t = " " . $matunisub[$n] . "cient" . $subcent . $t;
            }

            if ($sub == 1) {
            }elseif (! isset($matsub[$sub])) {
                if ($num == 1) {
                    $t = " mil";
                }elseif ($num > 1){
                    $t .= " mil";
                }
            }elseif ($num == 1) {
                $t .= " " . $matsub[$sub] . utf8_encode("ón");
            }elseif ($num > 1){
                $t .= " " . $matsub[$sub] . "ones";
            }
            if ($num == "000") $mils ++;
            elseif ($mils != 0) {
                if (isset($matmil[$sub])) $t .= " " . $matmil[$sub];
                $mils = 0;
            }
            $neutro = true;
            $tex = $t . $tex;
        }
        $tex = $neg . substr($tex, 1) . $fin;
        return $tex;
    }

    public function metConvertirLetras($numero, $tipo)
    {

        $explodeNumero = explode(".", $numero);
        $e = $explodeNumero[0];
        $d = $explodeNumero[1];

        if ($tipo == "moneda"){
            return $this->numtoletras($numero);
        } else if ($tipo == "decimal"){
            return $this->numtoletras($numero);
        } else if ($tipo == "entero") {
            return $this->metNumLetras($e, false, false);
        }
    }

    public function metConsultarMes($mes) {
        $nombre[1]="Enero";
        $nombre[2]="Febrero";
        $nombre[3]="Marzo";
        $nombre[4]="Abril";
        $nombre[5]="Mayo";
        $nombre[6]="Junio";
        $nombre[7]="Julio";
        $nombre[8]="Agosto";
        $nombre[9]="Septiembre";
        $nombre[10]="Octubre";
        $nombre[11]="Noviembre";
        $nombre[12]="Diciembre";
        return $nombre[$mes];
    }

	// Funcion que genera el PDF correspondiente al reporte de empleados
	public function metGenerarConstanciaTrabajo()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfConstanciaTrabajo('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
        $pdf->SetMargins(25, 10, 30);
		$pdf->SetAutoPageBreak(true, 2);
        // Consulto la data
        $pkNumEmpleado = $_GET['pk_num_empleado'];
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atReporteModelo->metUsuario($usuario, 1);
        $pkNumEmpleadoLogin = $empleado['pk_num_empleado'];
        $datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleadoLogin);
        $pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
        $parametroDependencia = Session::metObtener('RHDEP');
        $responsable = $this->atReporteModelo->metConsultarResponsableRH($pkNumOrganismo, $parametroDependencia);

        // Datos del responsable
        $sexoResponsable = $responsable['ind_nombre_detalle'];
        $empresa = $responsable['ind_descripcion_empresa'];
        $nombreResponsable = $responsable['ind_nombre1'].' '.$responsable['ind_nombre2'].' '.$responsable['ind_apellido1'].' '.$responsable['ind_apellido2'];
        $cedulaResponsable = $responsable['ind_cedula_documento'];
        $empleadoDirector = $responsable['pk_num_empleado'];
        $nacionalidadDirector = $this->atReporteModelo->metConsultarNacionalidad($empleadoDirector);
        $nacionalidadD = $nacionalidadDirector['nacionalidad'];
        $letraDirector = substr($nacionalidadD, 0, 1);
        if ($sexoResponsable=="MASCULINO"){
            $responsable = 'El suscrito Director';
            $cargo = 'DIRECTOR DE RECURSOS HUMANOS';
        }  else {
            $responsable = 'La suscrita Directora';
            $cargo = 'DIRECTORA DE RECURSOS HUMANOS';
        }
        // Conversión a letras
        $dia_actual=date("d");
        $mes_actual=date("m");
        $anio_actual=date("Y");
        $dia_letras = $this->metConvertirLetras($dia_actual, "entero");
        $dia_actual = ("$dia_letras ($dia_actual)");
        $anio_letras = $this->metConvertirLetras($anio_actual, "entero");
        $anio_actual = ("$anio_letras ($anio_actual)");
        $m = (int) $mes_actual;

        $mes_actual = $this->metConsultarMes($m);
        // Fin

        // Datos del Empleado
        $funcionario = $this->atReporteModelo->metEmpleado($pkNumEmpleado);

        $nombreCompleto = $funcionario['ind_nombre1'].' '.$funcionario['ind_nombre2'].' '.$funcionario['ind_apellido1'].' '.$funcionario['ind_apellido2'];
        $sexo = $funcionario['sexo'];
        if ($sexo=="MASCULINO"){
            $referencia = ' el funcionario';
        }  else {
            $referencia = ' la funcionaria';
        }
        $nacionalidad = $this->atReporteModelo->metConsultarNacionalidad($pkNumEmpleado);
        $nacionalidadFuncionario = $nacionalidad['nacionalidad'];
        $letraNacionalidad = substr($nacionalidadFuncionario, 0, 1);
        $cedula = $funcionario['ind_cedula_documento'];
        $fechaIngreso = str_replace("-","/",$funcionario['fecha_ingreso']);
        $cargoFuncionario = $funcionario['ind_descripcion_cargo'];
        $anioActual = date("Y");
        $mesActual = date("m");
        if($mes_actual==01){
            $mesPeriodo = 12;
            $anioPeriodo = $anioActual-1;
        } else {
            $mesPeriodoPrevio = $mesActual-1;
            $mesPeriodo = str_pad((int) $mesPeriodoPrevio,2,"0",STR_PAD_LEFT);
            $anioPeriodo = $anioActual;
        }
        // Sueldo Funcionario
        $mesAntPeriodo = $mesPeriodo - 1;
        if($mesAntPeriodo < 10){
            $mesAntPeriodo = '0'.$mesAntPeriodo;
        }
        $sueldos = $this->atReporteModelo->metConsultarSueldo($pkNumEmpleado, $mesPeriodo, $anioPeriodo, $mesAntPeriodo);
        $sueldoBase = $sueldos['num_sueldo_base']+$sueldos['num_sueldo_retro'];
        $primas = $sueldos['num_total_asignaciones']+$sueldos['asignaciones_retro'];
        $total = $sueldoBase + $primas;
        // Sueldo Base
        $sueldo = "(Bs. ".number_format($sueldoBase, 2, ',', '.').")";
        //$sueldo = "(Bs. $sueldo)";
        $sueldo_letras = $this->metConvertirLetras($sueldoBase, "moneda");
        $sueldo_basico = (strtoupper("$sueldo_letras")." $sueldo");

        // Primas
        $primasSueldo ="(Bs. ".number_format($primas, 2, ',', '.').")";
        //$primasSueldo = "(Bs. $primasSueldo)";
        $primas_letras = $this->metConvertirLetras($primas, "moneda");
        $sueldoPrimaTotal = (strtoupper(" $primas_letras")." $primasSueldo");

        // Total
        $sueldoTotal = "(Bs. ".number_format($total, 2, ',', '.').")";
        //$sueldoTotal = "(Bs.  $sueldoTotal)";
        $total_letras = $this->metConvertirLetras($total, "moneda");
        $totalSalario = (strtoupper(" $total_letras")."  $sueldoTotal");

        // Se genera la constancia
        $parrafo1 = ("".$responsable." de Recursos Humanos de la ".$empresa.", ".$nombreResponsable.", titular de la cédula de identidad ".$letraDirector."-".number_format($cedulaResponsable,0,',', '.').", según Resolución Nº DC-80-2017 de fecha 01 de Mayo de 2017, hace constar que ".$referencia." ").strtoupper(trim($nombreCompleto)).(", titular de la cédula de identidad ").$letraNacionalidad."-".number_format($cedula, 0, '', '.').(", presta sus servicios en este Organismo de Control Fiscal, desde el ". $fechaIngreso.", Actualmente desempeña el cargo de ". $cargoFuncionario." de este Organismo Contralor, devengando una remuneracion salarial básica mensual de ".$sueldo_basico .", más primas por la cantidad de ".$sueldoPrimaTotal."; totalizando una remuneración normal de".$totalSalario.".");

        $parrafo2 = ("Constancia que se expide a petición de la parte interesada, en Cumaná, Estado Sucre, a los ".$dia_actual." día(s) del mes de ".$mes_actual." del ".$anio_actual.".");

        $pie1 = ("LCDO. ".$nombreResponsable."");
        $pie2 = ("".$cargo."");
        $pie3 = ("Resolución Nº DC-80-2017 de la contraloría del estado Sucre de fecha 01/05/2017");
        $pie4 = ("");


        $pdf->SetFont('Arial', '', 12);
        $pdf->setX(24);
        $pdf->MultiCell(172, 6, utf8_decode($parrafo1), 0, 'J');
        $pdf->Ln(5);
        $pdf->MultiCell(170, 6, utf8_decode($parrafo2), 0, 'J');
        $pdf->Ln(5);
        $pdf->MultiCell(170, 6,'', 0, 'J');

        $pdf->SetFont('Arial', 'B', 12);
        $pdf->SetXY(25, 170); $pdf->Cell(170, 10, utf8_decode($pie1), 0, 1, 'C');
        $pdf->SetXY(25, 175); $pdf->Cell(170, 10, utf8_decode($pie2), 0, 1, 'C');

        $pdf->SetFont('Arial', '', 8);
        $pdf->SetXY(25, 180); $pdf->Cell(170, 10, utf8_decode($pie3), 0, 1, 'C');
        $pdf->SetXY(25, 184); $pdf->Cell(170, 10, utf8_decode($pie4), 0, 1, 'C');
        $pdf->SetXY(25, 188); $pdf->Cell(170, 10, '', 0, 1, 'C');

        $pdf->SetFont('Arial', 'B', 9);
        $pdf->SetXY(25, 190); $pdf->Cell(170, 10,'' /*utf8_decode($field_param['ValorParam'])*/, 0, 1, 'L');

        $pie6 = ("Dirección: Avenida Arismendi, Edificio Palacio Legislativo, Cumaná, estado Sucre, Teléfonos: (0293) 4320708, 4323658. Directo: Tele Fax 4323447");
        $pie7 = ("Rif: G-20001224-2 Correo Electrónico: contraloria.estado.sucre@cgesucre.gob.ve / Página web: www.cgesucre.gob.ve");
        
        $pdf->SetFont('Arial', 'I', 8);
        $pdf->SetTextColor(100, 100, 100);
        $pdf->SetXY(25, 252); $pdf->Cell(170, 10, utf8_decode($pie6), 0, 1, 'C');
        $pdf->SetXY(25, 256); $pdf->Cell(170, 10, utf8_decode($pie7), 0, 1, 'C');

        $pdf->Output();
    }
}
