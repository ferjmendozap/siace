<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class permisoControlador extends Controlador
{
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('permiso', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js = array('materialSiace/core/demo/DemoTableDynamic',  'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$pkNumDependencia = $datosEmpleado['fk_a004_num_dependencia'];
		$listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo,
			'pk_num_dependencia' => $pkNumDependencia
		);
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
		$listadoEmpleado = $this->atReporteModelo->metListarEmpleado($pkNumDependencia);
		$listadoPermiso = $this->atReporteModelo->metListarPermiso($pkNumOrganismo, $pkNumDependencia, '', '', '', '', '');
		$tipoAusencia = $this->atReporteModelo->metMostrarSelect('TIPAUS');
		// Cargar a la vista
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->assign('listadoEmpleado', $listadoEmpleado);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->assign('listadoPermiso', $listadoPermiso);
		$this->atVista->assign('tipoAusencia', $tipoAusencia);
        // Cargo el listado de empleados

		$this->atVista->metRenderizar('listadoPermiso');
	}

    // Método que permite listar las dependencias del organismo
    public function metBuscarDependencia()
    {
        $pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
        $listarDependencia=$this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="">&nbsp;</option>';
        foreach($listarDependencia as $listarDependencia){
            $a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
        }
        $a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
        echo $a;
    }

    // Método que permite listar a los empleados de una dependencia
    public function metBuscarEmpleado()
    {
        $pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
        $listarEmpleado = $this->atReporteModelo->metListarEmpleado($pkNumDependencia);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_empleado" id="pk_num_empleado"> <option value="">&nbsp;</option>';
        foreach($listarEmpleado as $empleado){
            $a .='<option value="'.$empleado['pk_num_empleado'].'">'.$empleado['ind_nombre1'].' '. $empleado['ind_nombre1'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2'].'</option>';
        }
        $a .= '</select><label for="pk_num_empleado"><i class="glyphicon glyphicon-briefcase"></i> Empleado</label></div>';
        echo $a;
    }

	// Método que permite listar un permiso
	public function metListarPermiso()
	{
		$pkNumOrganismo = $_POST['pk_num_organismo'];
		$pkNumDependencia = $_POST['pk_num_dependencia'];
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$numPermiso = $_POST['num_permiso'];
		$tipoAusencia = $_POST['tipo_ausencia'];
		$fechaInicioPrevia = $_POST['fechaInicio'];
		$fechaFinPrevia = $_POST['fechaFin'];
		if($fechaInicioPrevia!=''){
			$explodeInicio = explode('/', $fechaInicioPrevia);
			$fechaInicio = $explodeInicio[2].'-'.$explodeInicio[1].'-'.$explodeInicio[0];
		} else {
			$fechaInicio = '';
		}
		if($fechaFinPrevia!=''){
			$explodeFin = explode('/', $fechaFinPrevia);
			$fechaFin = $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];
		} else {
			$fechaFin = '';
		}
		$listarPermiso = $this->atReporteModelo->metListarPermiso($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $numPermiso, $fechaInicio, $fechaFin, $tipoAusencia);
		echo json_encode($listarPermiso);
	}

	// Método que permite transformar las horas de horario militar a horario estándar
	public function metHorarioEstandar($horaSalida, $horaEntrada)
	{
		// Hora de salida
		$horaExplodeInicio = explode(":", $horaSalida);
		$horaInicio = $horaExplodeInicio[0];
		$minutoInicio = $horaExplodeInicio[1];

		// Hora de entrada
		$horaExplodeFin = explode(":", $horaEntrada);
		$horaFin = $horaExplodeFin[0];
		$minutoFin = $horaExplodeFin[1];

		// Convirtiendo a horario estándar
		// Hora Inicio
		if($horaInicio>12){
			$horaInicioPrevio = $horaInicio - 12;
			$hora_inicio = str_pad($horaInicioPrevio, 2, "0", STR_PAD_LEFT);
		}
		if($horaInicio<12) {
			$hora_inicio = $horaInicio;
		}
		// Hora Fin
		if($horaFin>12){
			$horaFinPrevio = $horaFin - 12;
			$hora_fin = str_pad($horaFinPrevio, 2, "0", STR_PAD_LEFT);
		}
		if($horaFin<12) {
			$hora_fin = $horaFin;
		}
		// Horario
		// Horario Inicio
		if($horaInicio>=12){
			$horarioInicio = 'PM';
		}
		if($horaInicio<12) {
			$horarioInicio = 'AM';
		}
		//Horario Fin
		if($horaFin>=12){
			$horariofin = 'PM';
		}
		if($horaFin<12) {
			$horariofin = 'AM';
		}
		// Los agrupo en un array
		$datoHorario = array($hora_inicio,$minutoInicio, $hora_fin, $minutoFin, $horarioInicio, $horariofin);
		return $datoHorario;
	}

	// Método que permite visualizar un permiso en específico
	public function metVerPermiso()
	{
		$pkNumPermiso = $this->metObtenerInt('pk_num_permiso');
		$flagFormulario = $this->metObtenerInt('flagFormulario');
		$permiso = $this->atReporteModelo->metVerPermiso($pkNumPermiso, 2);
		$this->atVista->assign('permiso', $permiso);
		$funcionarioAprueba = $this->atReporteModelo->metFuncionarioAprueba($pkNumPermiso);
		$this->atVista->assign('funcionario', $funcionarioAprueba);
		// Transformo las horas a horario am-pm
		foreach ($permiso as $permiso){
			$horaSalida = $permiso['fec_hora_salida'];
			$horaEntrada = $permiso['fec_hora_entrada'];
		}
		$horario = $this->metHorarioEstandar($horaSalida, $horaEntrada);
		$datoHora = array (
			'horaInicio' => $horario[0],
			'minutoInicio' => $horario[1],
			'horaFin' => $horario[2],
			'minutoFin' => $horario[3],
			'horarioInicio' => $horario[4],
			'horarioFin' => $horario[5],
			'flagFormulario' => $flagFormulario
		);
		
		$this->atVista->assign('datoHora', $datoHora);
		$verAcceso = $this->atReporteModelo->metAcceso($pkNumPermiso, 2);
		$this->atVista->assign('acceso', $verAcceso);
		$this->atVista->metRenderizar('ver', 'modales');
	}

	// Método que permite generar el reporte de pdf de los permisos
	public function metGenerarDiarioPermiso()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfPermisoEmpleado('L', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$empleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$nombre = $empleado['ind_nombre1'].' '.$empleado['ind_nombre2'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2'];
		$cargo = $empleado['ind_descripcion_cargo'];
		$dependencia = $empleado['ind_dependencia'];
		$pdf->Empleado($nombre, $cargo, $dependencia);
		$pdf->Header(1);
		$listadoPermiso = $this->atReporteModelo->metListarPermiso('', '', $pkNumEmpleado, '', '', '', '');
		$pdf->SetWidths(array(10, 20, 20, 18, 18, 15, 15, 23, 35, 38, 45));
		$pdf->SetAligns(array('C', 'C', 'C', 'C','C', 'C', 'C', 'C', 'L'));
		$pdf->SetFont('Arial', '', 8);
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
		$contar = 0;
		foreach ($listadoPermiso as $permiso){
			$contar++;
			if ($contar % 2 == 0) { $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);  }
			else { $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255); }
			$pdf->Row(array(
				$contar,
				$permiso['pk_num_permiso'],
				$permiso['fecha_ingreso'],
				$permiso['fecha_salida'],
				$permiso['fecha_entrada'],
				str_pad($permiso['num_total_hora'], 2, "0", STR_PAD_LEFT).':'.str_pad($permiso['num_total_minuto'], 2, "0", STR_PAD_LEFT).':00',
				$permiso['num_total_dia'],
				$permiso['txt_estatus'],
				$permiso['tipo_ausencia'],
				$permiso['motivo_ausencia'],
				utf8_decode($permiso['ind_motivo'])
			));
		}
		$pdf->Output();
	}

	// Método que permite generar el reporte de permisos de todos los funcionarios asociados a la búsqueda
	public function metGenerarReportePermiso()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfReportePermiso('L', 'mm', 'Legal');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_POST['pk_num_organismo'];
		$pkNumDependencia = $_POST['pk_num_dependencia'];
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$numPermiso = $_POST['num_permiso'];
		$tipoAusencia = $_POST['tipo_ausencia'];
		$fechaInicioPrevia = $_POST['fechaInicio'];
		$fechaFinPrevia = $_POST['fechaFin'];
		if($fechaInicioPrevia!=''){
			$explodeInicio = explode('/', $fechaInicioPrevia);
			$fechaInicio = $explodeInicio[2].'-'.$explodeInicio[1].'-'.$explodeInicio[0];
		} else {
			$fechaInicio = '';
		}
		if($fechaFinPrevia!=''){
			$explodeFin = explode('/', $fechaFinPrevia);
			$fechaFin = $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];
		} else {
			$fechaFin = '';
		}
		$listarPermiso = $this->atReporteModelo->metListarPermiso($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $numPermiso, $fechaInicio, $fechaFin, $tipoAusencia);
		$contar = 0;
		$pdf->SetWidths(array(15, 75, 30, 19, 19, 16, 12, 25, 40, 76));
		$pdf->SetAligns(array('C', 'L', 'C', 'C', 'C','C', 'C', 'C', 'L', 'L'));
		$pdf->SetFont('Arial', '', 9);
		foreach($listarPermiso as $permiso){
			$contar++;
			if ($contar % 2 == 0) { $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);  }
			else { $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255); }
			$pdf->Row(array(
				$contar,
				utf8_decode($permiso['ind_nombre1'].' '.$permiso['ind_nombre2'].' '.$permiso['ind_apellido1'].' '.$permiso['ind_apellido2']),
				$permiso['fecha_ingreso'],
				$permiso['fecha_salida'].'  H: '.$permiso['fec_hora_salida'],
				$permiso['fecha_entrada'].'  H: '.$permiso['fec_hora_entrada'],
				str_pad($permiso['num_total_hora'], 2, "0", STR_PAD_LEFT).':'.str_pad($permiso['num_total_minuto'], 2, "0", STR_PAD_LEFT).':00',
				$permiso['num_total_dia'],
				$permiso['txt_estatus'],
				$permiso['tipo_ausencia'],
				utf8_decode($permiso['ind_motivo'])
			));
		}
		$pdf->Output();
	}
}
