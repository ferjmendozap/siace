<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Resumen de Eventos. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class resumenEventoControlador extends Controlador
{
    private $atReporteModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atReporteModelo = $this->metCargarModelo('resumenEvento', 'reportes');
        $this->atFeriado = $this->metCargarModelo('diasFeriados', 'maestros');
        $this->atBonoAlimentacion  = $this->metCargarModelo('bonoAlimentacion','procesos');
    }

    // Index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker',
            'multi-select/multi-select555c'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'multi-select/jquery.multi-select'
        );
        $js = array('materialSiace/core/demo/DemoTableDynamic',  'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $usuario = Session::metObtener('idUsuario');
        $empleado = $this->atReporteModelo->metUsuario($usuario, 1);
        $pkNumEmpleado = $empleado['pk_num_empleado'];
        $datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
        $pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
        $listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $empleado = array(
            'pk_num_organismo' => $pkNumOrganismo
        );
        $tipoNomina = $this->atReporteModelo->metListarNomina();
        $listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $listadoEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, '', '', '', '', '');
        $listarCargos = $this->atReporteModelo->metListarCargos();
        $listarPeriodo = $this->atReporteModelo->metListarPeriodo();
        $this->atVista->assign('nomina', $tipoNomina);
        $this->atVista->assign('listadoDependencia', $listadoDependencia);
        $this->atVista->assign('emp', $empleado);
        $this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('datos', $listadoEmpleado);
        $this->atVista->assign('listarCargos', $listarCargos);
        $this->atVista->assign('listarPeriodo', $listarPeriodo);
        $this->atVista->metRenderizar('listadoResumen');
    }

    // Método que permite listar las dependencias del organismo
    public function metBuscarDependencia()
    {
        $pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
        $listarDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="">&nbsp;</option>';
        foreach ($listarDependencia as $listarDependencia) {
            $a .= '<option value="' . $listarDependencia['pk_num_dependencia'] . '">' . $listarDependencia['ind_dependencia'] . '</option>';
        }
        $a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
        echo $a;
    }

    // Método que permite listar las dependencias del organismo
    public function metBuscarCentroCosto()
    {
        $pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
        $pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
        $listarCentroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, $pkNumDependencia);
        $a = '<div class="form-group floating-label"><select class="form-control dirty" name="centro_costo" id="centro_costo"><option value="">&nbsp;</option>';
        foreach ($listarCentroCosto as $centroCosto) {
            $a .= '<option value="' . $centroCosto['pk_num_centro_costo'] . '">' . $centroCosto['ind_descripcion_centro_costo'] . '</option>';
        }
        $a .= '</select><label for="pk_num_centro_costo"><i class="glyphicon glyphicon-briefcase"></i> Centro Costo</label></div>';
        echo $a;
    }

    // Método que permite listar las dependencias del organismo
    public function metBuscarMesPeriodo()
    {
        $fecAnio = $this->metObtenerInt('fec_anio');
        $listarMesPeriodo = $this->atReporteModelo->metListarMesPeriodo($fecAnio);
        $a = '<div class="form-group"><select class="form-control dirty" name="mes_periodo" id="mes_periodo"><option value="">&nbsp;</option>';
        foreach ($listarMesPeriodo as $mesPeriodo) {
            $explode = explode("-", $mesPeriodo['ind_periodo']);
            $mes = $explode[1];
            $a .= '<option value="' . $mes . '">Mes: ' . $mes . ' - Nomina: '.$mesPeriodo['ind_nombre_nomina'].'</option>';
        }
        $a .= '</select><label for="pk_num_beneficio"><i class="glyphicon glyphicon-briefcase"></i></label></div>';
        echo $a;
    }

    // Método que permite calcular la diferencia entre dos horas dadas
    public function metRestarHoras($horaPreviaInicio, $horaPreviaFin)
    {
        $explodeInicio = explode(":", $horaPreviaInicio);
        $horaInicio = $explodeInicio[0];
        $minutoInicio = $explodeInicio[1];

        $explodeFin = explode(":", $horaPreviaFin);
        $horaFin = $explodeFin[0];
        $minutoFin = $explodeFin[1];

        if ($minutoInicio == $minutoFin) {
            $horaTotal = $horaFin - $horaInicio;
            $total = $horaTotal . ':00';
        }
        if ($minutoInicio < $minutoFin) {
            $horaTotal = $horaFin - $horaInicio;
            $totalMinuto = $minutoFin - $minutoInicio;
            $total = $horaTotal . ':' . $totalMinuto;
        }
        if ($minutoInicio > $minutoFin) {
            $minutoPrevio = $minutoInicio - $minutoFin;
            $minutoAnterior = 60 - $minutoPrevio;
            $horaF = $horaFin - 1;
            $horaTotal = $horaF - $horaInicio;
            $total = $horaTotal . ':' . $minutoAnterior;
        }

        return $total;
    }

    // Método que permite generar la semana a partir de dos fechas
    public function metGenerarSemanas($fecha_inicial, $fecha_final)
    {
        $semanas = array();
        //transformar las fechas al formato que utiliza la funcion DateTime()
        $aux = explode("/", $fecha_inicial);
        $fecha_inicial_formateada = ($aux[2] + 0) . '-' . ($aux[1] + 0) . '-' . ($aux[0] + 0);
        $aux = explode("/", $fecha_final);
        $fecha_final_formateada = ($aux[2] + 0) . '-' . ($aux[1] + 0) . '-' . ($aux[0] + 0);
        //obtener el numero de dias entre las dos fechas
        $datetime1 = new DateTime($fecha_inicial_formateada.' 00:00:00');
        $datetime2 = new DateTime($fecha_final_formateada.' 24:00:00');
        $interval = $datetime1->diff($datetime2);

        $dias = $interval->format('%a');

        //comenzar a contar las semanas
        $fecha_i = new DateTime($fecha_inicial_formateada.' 00:00:00');

        $comienzo = date("d/m/Y", $fecha_i->getTimestamp());

        $indice = 1;
#        echo 'dias es: '.$dias;
        for ($i = 1; $i <= $dias; $i++) {
            if (date("N", $fecha_i->getTimestamp()) == 7 OR $i == $dias) //aqui N significa el indice del día de la semana (1 es lunes, 7 es domingo)
            {
                $semanas[$indice]['inicio'] = $comienzo;
                $semanas[$indice]['final'] = date("d/m/Y", $fecha_i->getTimestamp());
                $fecha_i->add(new DateInterval('P1D'));
                $comienzo = date("d/m/Y", $fecha_i->getTimestamp());
                $indice = $indice + 1;
            } else {
                $fecha_i->add(new DateInterval('P1D'));
            }
        }
        return $semanas;
    }

    // Método que permite listar los empleados por dependencia y organismo
    public function metGenerarResumen()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $pkNumDependencia = $_POST['pk_num_dependencia'];
        $centroCosto = $_POST['centro_costo'];
        $tipoNomina = $_POST['tipo_nomina'];
        $cargo = $_POST['cargo'];
        $edoReg = $_POST['edo_reg'];
        $sitTrab = $_POST['sit_trab'];
        $pkNumEmpleado = $_POST['pk_num_empleado'];
        $periodo = $_POST['periodo'];
        $mes_periodo = $_POST['mes_periodo'];
        $datos = array(
            'pk_num_organismo' => $pkNumOrganismo,
            'pk_num_dependencia' => $pkNumDependencia,
            'centro_costo' => $centroCosto,
            'tipoNomina' => $tipoNomina,
            'cargo' => $cargo,
            'edoReg' => $edoReg,
            'sitTrab' => $sitTrab,
            'pkNumEmpleado' => $pkNumEmpleado,
            'periodo' => $periodo,
            'mes_periodo' => $mes_periodo
        );
        $this->atVista->assign('datos', $datos);
        $this->atVista->metRenderizar('generarResumen', 'modales');
    }

    // Método que permite operar horas y minutos
    public function metObtenerTiempo($acumuladorTotalHora, $acumuladorTotalMinuto)
    {
        while ($acumuladorTotalMinuto >= 60) {
            if ($acumuladorTotalMinuto >= 60) {
                $acumuladorTotalHora = $acumuladorTotalHora + 1;
                $acumuladorTotalMinuto = $acumuladorTotalMinuto - 60;
            }
        }
        $tiempo = array(
            'hora' => $acumuladorTotalHora,
            'minuto' => $acumuladorTotalMinuto
        );
        return $tiempo;
    }

    // Permite calcular los dias a pagar
    public function metCalcularDiasPagar($fechaISemana, $fechaFSemana)
    {
        $diaInactivo = 0;
        $totalDias = 0;
        for ($i = $fechaISemana; $i <= $fechaFSemana; $i = date("Y-m-d", strtotime($i . "+ 1 days"))) {
            $fechaComprobar = date("w", strtotime($i));
            if (($fechaComprobar == 0) || ($fechaComprobar == 6)) {
                $diaInactivo = $diaInactivo + 1;
            }
            $totalDias++;
        }
        $datos = array(
            'diaInactivo' => $diaInactivo,
            'total' => $totalDias
        );
        return $datos;
    }

    // Calcular las horas Hábiles del periodo
    public function metCalcularHoras($horasDiarias, $diasHabilesPeriodo)
    {
        $explode = explode(":", $horasDiarias);
        $hora = $explode[0];
        $minuto = $explode[1];
        $horaAcumulador = $hora * $diasHabilesPeriodo;
        $minutoAcumulador = $minuto * $diasHabilesPeriodo;
        while ($minutoAcumulador >= 60) {
            if ($minutoAcumulador >= 60) {
                $horaAcumulador = $horaAcumulador + 1;
                $minutoAcumulador = $minutoAcumulador - 60;
            }
        }
        $horasDatos = array(
            'hora' => $horaAcumulador,
            'minuto' => $minutoAcumulador
        );
        return $horasDatos;
    }

    // Calcular horas
    public function metObtenerHoras($horaAcumulador, $minutoAcumulador)
    {
        while ($minutoAcumulador >= 60) {
            if ($minutoAcumulador >= 60) {
                $horaAcumulador = $horaAcumulador + 1;
                $minutoAcumulador = $minutoAcumulador - 60;
            }
        }
        $horasDatos = array(
            'hora' => $horaAcumulador,
            'minuto' => $minutoAcumulador
        );
        return $horasDatos;
    }

    // Método que permite obtener los dias feriados e inactivos entre dos fechas
    public function metObtenerFeriados($fechaISemana, $fechaFSemana)
    {
        $acumuladorInactivo = 0;
        $acumuladorFeriado = 0;
        for ($i = $fechaISemana; $i <= $fechaFSemana; $i = date("Y-m-d", strtotime($i . "+ 1 days"))) {
            $diaFeriado = $this->atReporteModelo->metFeriado($i);
            $feriado = $diaFeriado['feriado'];
            $fechaComprobar = date("w", strtotime($i));
            if (($fechaComprobar == 0) || ($fechaComprobar == 6)) {
                $acumuladorInactivo = $acumuladorInactivo + 1;
            }
            if ($feriado > 0) {
                $acumuladorFeriado = $acumuladorFeriado + 1;
            }
        }
        $datosSemana = array(
            'feriado' => $acumuladorFeriado,
            'inactivo' => $acumuladorInactivo
        );
        return $datosSemana;
    }

    // Método que permite generar el Resumen Semanal de Eventos
    public function metGenerarResumenSemanal()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraReportes', 'modRH');
        $pdf = new pdfResumenEventos('P', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        //Recibo los valores
        $pkNumOrganismo = $_GET['pk_num_organismo'];
        $pkNumDependencia = $_GET['pk_num_dependencia'];
        $centroCosto = $_GET['centro_costo'];
        $tipoNomina = $_GET['tipoNomina'];
        $cargo = $_GET['cargo'];
        $edoReg = $_GET['edo_reg'];
        $sitTrab = $_GET['sit_trab'];
        $pkNumEmpleado = $_GET['pkNumEmpleado'];
        $periodo = $_GET['periodo'];
        $mes_periodo = $_GET['mes_periodo'];
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(195, 5, 'RESUMEN SEMANAL', 0, 0, 'C');
        $pdf->Ln();
        // Verifico las fechas y el periodo
        if (($periodo == '') && ($mes_periodo == '')) {
            $obtenerBeneficio = $this->atReporteModelo->metObtenerUltimoBeneficio($pkNumOrganismo);
            $pdf->Cell(195, 5, utf8_decode('GENERAL'), 0, 1, 'L');
            $pdf->Ln();
            $fechaInicio = $obtenerBeneficio['fecha_inicio'];
            $fechaFin = $obtenerBeneficio['fecha_fin'];
            $pdf->Cell(60, 5, 'Beneficio del ' . $fechaInicio . ' al ' . $fechaFin, 0, 1, 'L');
            $pdf->Ln();
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(229, 229, 229);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetWidths(array(27, 25, 22, 25, 25, 25, 28));
            $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->SETX(28);
            $pdf->Row(array(
                'HORAS DIARIAS',
                'HORAS SEMANALES',
                utf8_decode('DIAS HÁBILES'),
                'DIAS FERIADOS',
                'DIAS INACTIVOS',
                'VALOR POR DIA',
            ));
            $pdf->SetWidths(array(27, 25, 22, 25, 25, 25, 28));
            $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
            $pdf->SetFont('Arial', '', 9);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetDrawColor(255, 255, 255);
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SETX(28);
            // Calcular los dias hábiles e inactivos del periodo
            $fechaInicioPeriodo = $obtenerBeneficio['fec_inicio_periodo'];
            $fechaFinPeriodo = $obtenerBeneficio['fec_fin_periodo'];
            $datos = $this->metCalcularDiasPagar($fechaInicioPeriodo, $fechaFinPeriodo);
            $diasTotal = $datos['total'] - $datos['diaInactivo'];
            $diasHabilesPeriodo = $diasTotal - $obtenerBeneficio['num_total_feriados'];
            $pdf->SetFont('Arial', '', 8);
            $pdf->Row(array(
                $obtenerBeneficio['fec_horas_diarias'],
                $obtenerBeneficio['fec_horas_semanales'],
                $diasHabilesPeriodo,
                $obtenerBeneficio['num_total_feriados'],
                $datos['diaInactivo'],
                number_format($obtenerBeneficio['num_valor_diario'], 2, ',', '.'),
            ));
        } else {
            $obtenerPeriodo = $this->atReporteModelo->metObtenerPeriodo($periodo, $mes_periodo);
            $pdf->Cell(60, 5, 'Beneficio del ' . $obtenerPeriodo['fechaI'] . ' al ' . $obtenerPeriodo['fechaF'], 0, 1, 'L');
            $pdf->Ln();
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(229, 229, 229);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetWidths(array(27, 25, 22, 25, 25, 25, 28));
            $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->SETX(28);
            $pdf->Row(array(
                'HORAS DIARIAS',
                'HORAS SEMANALES',
                utf8_decode('DIAS HÁBILES'),
                'DIAS FERIADOS',
                'DIAS INACTIVOS',
                'VALOR POR DIA',
            ));
            $pdf->SetWidths(array(27, 25, 22, 25, 25, 25, 28));
            $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
            $pdf->SetFont('Arial', '', 9);
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SETX(28);
            // Calcular los dias hábiles e inactivos del periodo
            $fechaInicioPeriodo = $obtenerPeriodo['fec_inicio_periodo'];
            $fechaFinPeriodo = $obtenerPeriodo['fec_fin_periodo'];
            $datos = $this->metCalcularDiasPagar($fechaInicioPeriodo, $fechaFinPeriodo);
            $diasTotal = $datos['total'] - $datos['diaInactivo'];
            $diasHabilesPeriodo = $diasTotal - $obtenerPeriodo['num_total_feriados'];
            $pdf->Row(array(
                $obtenerPeriodo['fec_horas_diarias'],
                $obtenerPeriodo['fec_horas_semanales'],
                $diasHabilesPeriodo,
                $obtenerPeriodo['num_total_feriados'],
                $datos['diaInactivo'],
                number_format($obtenerPeriodo['num_valor_diario'], 2, ',', '.'),
            ));
            $pdf->Ln();
            $semana = $this->metGenerarSemanas($obtenerPeriodo['fechaI'], $obtenerPeriodo['fechaF']);
            for ($i = 1; $i <= count($semana); $i++) {
                $fechaInicioSemana = $semana[$i]['inicio'];
                $fechaFinSemana = $semana[$i]['final'];
                $explodeInicio = explode('/', $fechaInicioSemana);
                $fechaISemana = $explodeInicio[2] . '-' . $explodeInicio[1] . '-' . $explodeInicio[0];
                $explodeFin = explode('/', $fechaFinSemana);
                $fechaFSemana = $explodeFin[2] . '-' . $explodeFin[1] . '-' . $explodeFin[0];
                $pdf->SetFont('Arial', 'B', 9);
                $pdf->Cell(60, 5, 'Semana del ' . $fechaInicioSemana . ' al ' . $fechaFinSemana, 0, 1, 'L');
                $pdf->SetFont('Arial', '', 9);
                $empleado = $this->atReporteModelo->metListarEmpleado($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $cargo, $edoReg, $sitTrab, $pkNumEmpleado, $fechaISemana, $fechaFSemana);
                $contador = count($empleado);
                if ($contador > 0) {
                    foreach ($empleado as $emp) {
                        $pkNumEmpleado = $emp['pk_num_empleado'];
                        $pdf->Cell(10, 5, $pkNumEmpleado, 0, 0, 'L');
                        $pdf->Cell(80, 5, utf8_decode($emp['ind_nombre1'] . ' ' . $emp['ind_nombre2'] . ' ' . $emp['ind_apellido1'] . ' ' . $emp['ind_apellido2']), 0, 1, 'L');
                        $pdf->SETX(20);
                        $pdf->SetDrawColor(0, 0, 0);
                        $pdf->SetFillColor(229, 229, 229);
                        $pdf->SetTextColor(0, 0, 0);
                        $pdf->SetFont('Arial', 'B', 8);
                        $pdf->SetDrawColor(0, 0, 0);
                        $pdf->SetFillColor(229, 229, 229);
                        $pdf->SetTextColor(0, 0, 0);
                        $pdf->SetWidths(array(17, 16, 16, 25, 30, 30, 43));
                        $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
                        $pdf->SetFont('Arial', 'B', 8);
                        $pdf->SETX(28);
                        $pdf->Row(array(
                            'FECHA',
                            'SALIDA',
                            'ENTRADA',
                            'TOTAL HORAS',
                            'TIPO DE EVENTO',
                            'MOTIVO',
                            'OBSERVACIONES'
                        ));
                        $eventoEmpleado = $this->atReporteModelo->metEventoEmpleado($pkNumEmpleado, $fechaISemana, $fechaFSemana);
                        $pdf->SetDrawColor(0, 0, 0);
                        $pdf->SetFillColor(255, 255, 255);
                        $pdf->SetTextColor(0, 0, 0);
                        $acumuladorTotalHora = 0;
                        $acumuladorTotalMinuto = 0;
                        $diasDescuento = 0;
                        $pdf->SetFont('Arial', '', 8);
                        foreach ($eventoEmpleado as $evento) {
                            $pdf->SETX(28);
                            $pdf->Row(array(
                                $evento['fecha'],
                                $evento['fec_hora_salida'],
                                $evento['fec_hora_entrada'],
                                $evento['fec_total_horas'],
                                $evento['tipo_ausencia'],
                                $evento['motivo_ausencia'],
                                $evento['txt_observaciones'],
                            ));
                            $diasDescuento++;
                            // Acumuladores del cuadro preliminar
                            // Horas faltantes
                            $explodeHoras = explode(":", $evento['fec_total_horas']);
                            $horaTotal = $explodeHoras[0];
                            $minutoTotal = $explodeHoras[1];
                            $acumuladorTotalHora = $acumuladorTotalHora + $horaTotal;
                            $acumuladorTotalMinuto = $acumuladorTotalMinuto + $minutoTotal;
                        }
                        // Horas faltantes
                        $tiempoFaltante = $this->metObtenerTiempo($acumuladorTotalHora, $acumuladorTotalMinuto);
                        // Valor a descontar
                        $valorDescontar = $obtenerPeriodo['num_valor_diario'] * $diasDescuento;
                        $pdf->SetDrawColor(0, 0, 0);
                        $pdf->SetFillColor(229, 229, 229);
                        $pdf->SetTextColor(0, 0, 0);
                        $pdf->SetWidths(array(30, 25, 25, 22, 22, 25, 28));
                        $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
                        $pdf->SetFont('Arial', 'B', 8);
                        $pdf->Ln();
                        $pdf->SETX(28);
                        $pdf->Row(array(
                            'HORAS FALTANTES',
                            'DIAS INACTIVOS',
                            'DIAS FERIADOS',
                            'DIAS DE DESCUENTO',
                            'DIAS A PAGAR',
                            'VALOR A DESCONTAR',
                            'VALOR A PAGAR'
                        ));
                        $feriados = $this->metObtenerFeriados($fechaISemana, $fechaFSemana);
                        $totalDias = $this->metCalcularDiasPagar($fechaISemana, $fechaFSemana);
                        $total = $totalDias['total'] - $totalDias['diaInactivo'];
                        $diaPagar = $total - $diasDescuento;
                        $diaPagarTotal = $diaPagar + $feriados['inactivo'] + $feriados['feriado'];
                        $totalPagar = $diaPagarTotal * $obtenerPeriodo['num_valor_diario'];
                        $pdf->SetDrawColor(0, 0, 0);
                        $pdf->SetFillColor(255, 255, 255);
                        $pdf->SetTextColor(0, 0, 0);
                        $pdf->SetFont('Arial', '', 8);
                        $pdf->SETX(28);
                        $pdf->Row(array(
                            str_pad($tiempoFaltante['hora'], 2, "0", STR_PAD_LEFT) . ':' . str_pad($tiempoFaltante['minuto'], 2, "0", STR_PAD_LEFT),
                            $feriados['inactivo'],
                            $feriados['feriado'],
                            $diasDescuento,
                            $diaPagarTotal,
                            number_format($valorDescontar, 2, ',', '.'),
                            number_format($totalPagar, 2, ',', '.')
                        ));
                    }
                } else {
                    $pdf->SETX(20);
                    $pdf->Cell(100, 5, 'No se encuentran eventos registrados para esta semana', 0, 1, 'L');
                }
            }
        }
        $pdf->Output();
    }

    // Método que permite generar el Resumen Mensual de Eventos
    public function metGenerarResumenMensual()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraReportes', 'modRH');
        $pdf = new pdfResumenEventos('P', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        //Recibo los valores
        $pkNumOrganismo = $_GET['pk_num_organismo'];
        $pkNumDependencia = $_GET['pk_num_dependencia'];
        $centroCosto = $_GET['centro_costo'];
        $tipoNomina = $_GET['tipoNomina'];
        $cargo = $_GET['cargo'];
        $edoReg = $_GET['edo_reg'];
        $sitTrab = $_GET['sit_trab'];
        $pkNumEmpleado = $_GET['pkNumEmpleado'];
        $periodo = $_GET['periodo'];
        $mes_periodo = $_GET['mes_periodo'];
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(195, 5, 'RESUMEN MENSUAL', 0, 0, 'C');
        $pdf->Ln();
        // Verifico las fechas y el periodo
        if (($periodo == '') && ($mes_periodo == '')) {
            $obtenerBeneficio = $this->atReporteModelo->metObtenerUltimoBeneficio($pkNumOrganismo);
            $fechaInicio = $obtenerBeneficio['fecha_inicio'];
            $fechaFin = $obtenerBeneficio['fecha_fin'];
            $pdf->Cell(195, 5, 'Periodo del ' . $fechaInicio . ' al ' . $fechaFin, 0, 1, 'C');
            $pdf->Ln();
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(229, 229, 229);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetWidths(array(30, 25, 25, 25, 25, 30, 34));
            $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Row(array(
                'HORAS DIARIAS',
                'HORAS MENSUALES',
                utf8_decode('DIAS HÁBILES'),
                'DIAS FERIADOS',
                'DIAS INACTIVOS',
                'VALOR POR DIA',
                'VALOR MENSUAL'
            ));
            $pdf->SetWidths(array(30, 25, 25, 25, 25, 30, 34));
            $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
            $pdf->SetFont('Arial', '', 9);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetDrawColor(255, 255, 255);
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetTextColor(0, 0, 0);
            // Calcular los dias hábiles e inactivos del periodo
            $fechaInicioPeriodo = $obtenerBeneficio['fec_inicio_periodo'];
            $fechaFinPeriodo = $obtenerBeneficio['fec_fin_periodo'];
            $datos = $this->metCalcularDiasPagar($fechaInicioPeriodo, $fechaFinPeriodo);
            $diasTotal = $datos['total'] - $datos['diaInactivo'];
            $diasHabilesPeriodo = $diasTotal - $obtenerBeneficio['num_total_feriados'];
            $horas = $this->metCalcularHoras($obtenerBeneficio['fec_horas_diarias'], $diasHabilesPeriodo);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Row(array(
                $obtenerBeneficio['fec_horas_diarias'],
                $horas['hora'] . ':' . $horas['minuto'] . ':00',
                $diasHabilesPeriodo,
                $obtenerBeneficio['num_total_feriados'],
                $datos['diaInactivo'],
                number_format($obtenerBeneficio['num_valor_diario'], 2, ',', '.'),
                number_format($obtenerBeneficio['num_valor_mes'], 2, ',', '.')
            ));
        } else {
            $obtenerPeriodo = $this->atReporteModelo->metObtenerPeriodo($periodo, $mes_periodo);
            $pdf->Cell(195, 5, 'Periodo del ' . $obtenerPeriodo['fechaI'] . ' al ' . $obtenerPeriodo['fechaF'], 0, 1, 'C');
            $pdf->Ln();
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(229, 229, 229);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetWidths(array(30, 25, 25, 25, 25, 30, 34));
            $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Row(array(
                'HORAS DIARIAS',
                'HORAS MENSUALES',
                utf8_decode('DIAS HÁBILES'),
                'DIAS FERIADOS',
                'DIAS INACTIVOS',
                'VALOR POR DIA',
                'VALOR MENSUAL'
            ));
            $pdf->SetWidths(array(30, 25, 25, 25, 25, 30, 34));
            $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
            $pdf->SetFont('Arial', '', 9);
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetTextColor(0, 0, 0);
            // Calcular los dias hábiles e inactivos del periodo
            $fechaInicioPeriodo = $obtenerPeriodo['fec_inicio_periodo'];
            $fechaFinPeriodo = $obtenerPeriodo['fec_fin_periodo'];
            $datos = $this->metCalcularDiasPagar($fechaInicioPeriodo, $fechaFinPeriodo);
            $diasTotal = $datos['total'] - $datos['diaInactivo'];
            $diasHabilesPeriodo = $diasTotal - $obtenerPeriodo['num_total_feriados'];
            // Horas Mensuales
            $horas = $this->metCalcularHoras($obtenerPeriodo['fec_horas_diarias'], $diasHabilesPeriodo);
            $pdf->Row(array(
                $obtenerPeriodo['fec_horas_diarias'],
                $horas['hora'] . ':' . $horas['minuto'] . ':00',
                $diasHabilesPeriodo,
                $obtenerPeriodo['num_total_feriados'],
                $datos['diaInactivo'],
                number_format($obtenerPeriodo['num_valor_diario'], 2, ',', '.'),
                number_format($obtenerPeriodo['num_valor_mes'], 2, ',', '.')
            ));
            $pdf->Ln();
            $periodoTotal = $periodo.'-'.$mes_periodo;
            $empleado = $this->atReporteModelo->metConsultarEmpleado($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $cargo, $edoReg, $sitTrab, $pkNumEmpleado, $periodoTotal);
            $contador = count($empleado);
            if($contador>0) {
                foreach ($empleado as $emp) {
                    $pdf->SetFont('Arial', 'B', 9);
                    $pkNumEmpleado = $emp['pk_num_empleado'];
                    $pdf->Cell(10, 5, $pkNumEmpleado, 0, 0, 'L');
                    $pdf->Cell(80, 5, utf8_decode($emp['ind_nombre1'] . ' ' . $emp['ind_nombre2'] . ' ' . $emp['ind_apellido1'] . ' ' . $emp['ind_apellido2']), 0, 1, 'L');
                    $pdf->SetDrawColor(0, 0, 0);
                    $pdf->SetFillColor(229, 229, 229);
                    $pdf->SetTextColor(0, 0, 0);
                    $pdf->SetWidths(array(22, 22, 22, 25, 25, 22, 28, 28));
                    $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Row(array(
                        'HORAS FALTANTES',
                        'HORAS TRABAJADAS',
                        'DIAS FERIADOS',
                        'DIAS INACTIVOS',
                        'DIAS DESCUENTO',
                        'DIAS A PAGAR',
                        'VALOR A DESCONTAR',
                        'VALOR A PAGAR'
                    ));
                    // Acumuladores totales
                    $horaFaltanteAcumulador = 0;
                    $minutoFaltanteAcumulador = 0;
                    $horaTrabajadaAcumulador = 0;
                    $minutoTrabajadoAcumulador = 0;
                    $diasFeriadosAcumulador = 0;
                    $diasInactivosAcumulador = 0;
                    $diasDescuentoAcumulador = 0;
                    $diasPagarAcumulador = 0;
                    $valorDescontarAcumulador = 0;
                    $valorPagarAcumulador = 0;
                    $semana = $this->metGenerarSemanas($obtenerPeriodo['fechaI'], $obtenerPeriodo['fechaF']);
                    for ($i = 1; $i <= count($semana); $i++) {
                        $fechaInicioSemana = $semana[$i]['inicio'];
                        $fechaFinSemana = $semana[$i]['final'];
                        $explodeInicio = explode('/', $fechaInicioSemana);
                        $fechaISemana = $explodeInicio[2] . '-' . $explodeInicio[1] . '-' . $explodeInicio[0];
                        $explodeFin = explode('/', $fechaFinSemana);
                        $fechaFSemana = $explodeFin[2] . '-' . $explodeFin[1] . '-' . $explodeFin[0];
                        $pdf->SetFont('Arial', 'B', 9);
                        $pdf->Cell(195, 5, 'Semana del ' . $fechaInicioSemana . ' al ' . $fechaFinSemana, 0, 1, 'C');
                        $pdf->SetFont('Arial', '', 9);
                        $pdf->SetWidths(array(22, 22, 22, 25, 25, 22, 28, 28));
                        $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
                        $pdf->SetDrawColor(0, 0, 0);
                        $pdf->SetFillColor(255, 255, 255);
                        $pdf->SetTextColor(0, 0, 0);
                        // Verifico si el empleado tuvo algun evento esa semana
                        $eventoSemanal = $this->atReporteModelo->metEventoEmpleado($pkNumEmpleado, $fechaISemana, $fechaFSemana);
                        $contadorEvento = count($eventoSemanal);
                        if($contadorEvento>0){
                            // Acumuladores
                            $acumuladorHora = 0;
                            $acumuladorMinuto = 0;
                            $acumuladorDescuento = 0;
                            foreach ($eventoSemanal as $ev) {
                                // Hora Faltante
                                $explodeHora = explode(":", $ev['fec_total_horas']);
                                $hora = $explodeHora[0];
                                $minuto = $explodeHora[1];
                                $acumuladorHora = $acumuladorHora + $hora;
                                $acumuladorMinuto = $acumuladorMinuto + $minuto;
                                // Dias de descuento
                                $acumuladorDescuento++;
                            }
                            // Hora Faltante
                            $horaFaltante = $this->metObtenerHoras($acumuladorHora, $acumuladorMinuto);
                            // Feriados e Inactivos
                            $feriados = $this->metObtenerFeriados($fechaISemana, $fechaFSemana);
                            // Dias a pagar
                            $diasPagar = $this->metCalcularDiasPagar($fechaISemana, $fechaFSemana);
                            $diasTotal = $diasPagar['total']-$acumuladorDescuento;
                            $diasHabilesPeriodo = $diasPagar['total'] - $feriados['inactivo'];
                            // Monto a pagar
                            $valorDescontar = $obtenerPeriodo['num_valor_diario'] * $acumuladorDescuento;
                            $valorPagar = $obtenerPeriodo['num_valor_diario'] * $diasTotal;
                            // Horas Faltantes
                            $horaInicioFalta = $horaFaltante['hora'];
                            $minutoInicioFalta = $horaFaltante['minuto'];
                            $horaInicio = str_pad($horaInicioFalta, 2, "0", STR_PAD_LEFT) . ':' . str_pad($minutoInicioFalta, 2, "0", STR_PAD_LEFT) . ':00';
                        } else {
                            $horasSemanalesPeriodo = $obtenerPeriodo['fec_horas_semanales'];
                            $horasDiariasPeriodo = $obtenerPeriodo['fec_horas_diarias'];
                            // Feriados e Inactivos
                            $feriados = $this->metObtenerFeriados($fechaISemana, $fechaFSemana);
                            // Calcular horas trabajadas

                            $horaInicioFalta = 0;
                            $minutoInicioFalta = 0;
                            // Dias a pagar
                            $diasPagar = $this->metCalcularDiasPagar($fechaISemana, $fechaFSemana);
                            $diasHabilesPeriodo = $diasPagar['total'] - $feriados['inactivo'];
                            $diasTotal = $diasHabilesPeriodo + $feriados['inactivo'] + $feriados['feriado'];
                            // Descuento
                            $acumuladorDescuento = 0;
                            // Monto a pagar
                            $valorDescontar = 0;
                            $valorPagar = $obtenerPeriodo['num_valor_diario'] * $diasTotal;
                        }
                        // Calcular horas trabajadas
                        // horas en base a dias pagados
                        $explodeHoras = explode(":", $obtenerPeriodo['fec_horas_diarias']);
                        $horaInicioSemana = $explodeHoras[0];
                        $horaMinutoSemana = $explodeHoras[1];
                        $horaIPagado = $horaInicioSemana * $diasHabilesPeriodo;
                        $minutoIPagado = $horaMinutoSemana * $diasHabilesPeriodo;
                        $obtenerHoras = $this->metObtenerHoras($horaIPagado, $minutoIPagado);
                        $horasTrabajadas = $obtenerHoras['hora'].':'.$obtenerHoras['minuto'];
                        $pdf->Row(array(
                            str_pad($horaInicioFalta, 2, "0", STR_PAD_LEFT) . ':' . str_pad($minutoInicioFalta, 2, "0", STR_PAD_LEFT),
                            str_pad($obtenerHoras['hora'], 2, "0", STR_PAD_LEFT) . ':' . str_pad($obtenerHoras['minuto'], 2, "0", STR_PAD_LEFT),
                            $feriados['feriado'],
                            $feriados['inactivo'],
                            $acumuladorDescuento,
                            $diasTotal,
                            number_format($valorDescontar, 2, ',', '.'),
                            number_format($valorPagar, 2, ',', '.')
                        ));
                        // Calculando los totales
                        // Total de horas faltantes
                        $horaFaltanteAcumulador = $horaFaltanteAcumulador + $horaInicioFalta;
                        $minutoFaltanteAcumulador = $minutoFaltanteAcumulador + $minutoInicioFalta;
                        // Horas Trabajadas
                        $explodeHorasTrabajadas = explode(":", $horasTrabajadas);
                        $horaTrabajadaPrevia = $explodeHorasTrabajadas[0];
                        $minutoTrabajadoPrevio = $explodeHorasTrabajadas[1];
                        $horaTrabajadaAcumulador = $horaTrabajadaAcumulador + $horaTrabajadaPrevia;
                        $minutoTrabajadoAcumulador = $minutoTrabajadoAcumulador + $minutoTrabajadoPrevio;
                        // Dias Feriados
                        $diasFeriadosAcumulador = $diasFeriadosAcumulador + $feriados['feriado'];
                        // Dias Inactivos
                        $diasInactivosAcumulador = $diasInactivosAcumulador + $feriados['inactivo'];
                        // Dias de Descuento
                        $diasDescuentoAcumulador = $diasDescuentoAcumulador + $acumuladorDescuento;
                        // Dias a pagar
                        $diasPagarAcumulador = $diasPagarAcumulador + $diasTotal;
                        // Valor a descontar
                        $valorDescontarAcumulador = $valorDescontarAcumulador + $valorDescontar;
                        // Valor a pagar
                        $valorPagarAcumulador = $valorPagarAcumulador + $valorPagar;
                    }
                    // Total del empleado durante el mes
                    $totalHoraFaltante = $this->metObtenerHoras($horaFaltanteAcumulador, $minutoFaltanteAcumulador);
                    $totalHoraTrabajada = $this->metObtenerHoras($horaTrabajadaAcumulador, $minutoTrabajadoAcumulador);
                    $pdf->Ln();
                    $pdf->SetFont('Arial', 'B', 9);
                    $pdf->Row(array(
                        str_pad($totalHoraFaltante['hora'], 2, "0", STR_PAD_LEFT) . ':' . str_pad($totalHoraFaltante['minuto'], 2, "0", STR_PAD_LEFT),
                        str_pad($totalHoraTrabajada['hora'], 2, "0", STR_PAD_LEFT) . ':' . str_pad($totalHoraTrabajada['minuto'], 2, "0", STR_PAD_LEFT),
                        $diasFeriadosAcumulador,
                        $diasInactivosAcumulador,
                        $diasDescuentoAcumulador,
                        $diasPagarAcumulador,
                        number_format($valorDescontarAcumulador, 2, ',', '.'),
                        number_format($valorPagarAcumulador, 2, ',', '.')
                    ));
                    $pdf->Ln();
                }

            } else {
                $pdf->SETX(20);
                $pdf->Cell(185, 5, 'NO SE ENCUENTRAN BENEFICIOS REGISTRADOS', 0, 1, 'C');
            }
        }
        $pdf->Output();
    }


    // Método que permite generar el reporte Resumen General de Eventos
    public function metGenerarResumenGeneral()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraReportes', 'modRH');
        $pdf = new pdfResumenEventos('P', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        //Recibo los valores
        $pkNumOrganismo = $_GET['pk_num_organismo'];
        $pkNumDependencia = $_GET['pk_num_dependencia'];
        $centroCosto = $_GET['centro_costo'];
        $tipoNomina = $_GET['tipoNomina'];
        $cargo = $_GET['cargo'];
        $edoReg = $_GET['edo_reg'];
        $sitTrab = $_GET['sit_trab'];
        $pkNumEmpleado = $_GET['pkNumEmpleado'];
        $periodo = $_GET['periodo'];
        $mes_periodo = $_GET['mes_periodo'];
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(195, 5, 'RESUMEN GENERAL', 0, 0, 'C');
        // Obtengo la fecha de inicio y fin del periodo
        if (($periodo == '') && ($mes_periodo == '')){
            $obtenerBeneficio = $this->atReporteModelo->metObtenerUltimoBeneficio($pkNumOrganismo);
            $explodePeriodo = explode("-", $obtenerBeneficio['fec_inicio_periodo']);
            $periodoTotal = $explodePeriodo[0].'-'.$explodePeriodo[1];
            $periodo = $explodePeriodo[0];
            $mes_periodo = $explodePeriodo[1];
        } else {
            $periodoTotal = $periodo.'-'.$mes_periodo;
        }
        $fechaPeriodo = $this->atReporteModelo->metObtenerPeriodo($periodo, $mes_periodo);
        $pdf->Ln();
        $pdf->Cell(195, 5, 'Periodo del ' . $fechaPeriodo['fechaI'] . ' al ' . $fechaPeriodo['fechaF'], 0, 1, 'C');
        $pdf->Ln();
        $listarEmpleado = $this->atReporteModelo->metConsultarEmpleado($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $cargo, $edoReg, $sitTrab, $pkNumEmpleado, $periodoTotal);
        $contador = count($listarEmpleado);
        if($contador>0) {
            $pdf->SetFont('Arial', 'B', 8);
            foreach($listarEmpleado as $empleado) {
                $pdf->Cell(10, 5, $empleado['pk_num_empleado'], 0, 0, 'L');
                $pdf->Cell(80, 5, utf8_decode($empleado['ind_nombre1'] . ' ' . $empleado['ind_nombre2'] . ' ' . $empleado['ind_apellido1'] . ' ' . $empleado['ind_apellido2']), 0, 0, 'L');
                $pkNumEmpleado = $empleado['pk_num_empleado'];
                $pkNumBeneficio = $empleado['pk_num_beneficio'];
                $fechaInicioPeriodo = $empleado['fec_inicio_periodo'];
                $fechaFinPeriodo = $empleado['fec_fin_periodo'];
                $pdf->Ln();
                // Listo el detalle
                $beneficio = $this->atReporteModelo->metConsultarBeneficioDetalle($pkNumEmpleado, $pkNumBeneficio);
                $contador = 0;
                $pdf->SETX(20);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetFillColor(200, 200, 200);
                for ($i = $fechaInicioPeriodo; $i <= $fechaFinPeriodo; $i = date("Y-m-d", strtotime($i . "+ 1 days"))) {
                    $explodeFecha = explode("-", $i);
                    $numFecha = $explodeFecha[2];
                    $pdf->Cell(6, 5, $numFecha, 1, 0, 'C', 1);
                    $contador++;
                    // En caso de colocar un periodo mayor a 30 dias por equivocación
                    if ($contador == 30) {
                        break;
                    }
                }
                // Relleno las casillas
                $pdf->Ln();
                $pdf->SETX(20);
                // Acumuladores
                $diaTrabajadoAcumulador = 0;
                $diasInactivoAcumulador = 0;
                $diasFeriadoAcumulador = 0;
                $diasHabilesAcumulador = 0;
                $diasDescuentoAcumulador = 0;
                $diasPagarAcumulador = 0;
                for ($j = 1; $j <= 30; $j++) {
                    $valor = 'num_dia' . $j;
                    if ($beneficio[$valor] == 'X') {
                        $pdf->SetTextColor(0, 200, 80);
                        $diaTrabajadoAcumulador++;
                    }
                    if ($beneficio[$valor] == 'F') {
                        $pdf->SetTextColor(0, 0, 120);
                        $diasFeriadoAcumulador++;
                    }
                    if ($beneficio[$valor] == 'D') {
                        $pdf->SetTextColor(255, 0, 0);
                        $diasDescuentoAcumulador++;
                    }
                    if ($beneficio[$valor] == 'I') {
                        $diasInactivoAcumulador++;
                    }
                    if (($beneficio[$valor] != 'I') && ($beneficio[$valor] != 'F')) {
                        $diasHabilesAcumulador++;
                    }
                    if (($beneficio[$valor] == 'X') || ($beneficio[$valor] == 'F')) {
                        $diasPagarAcumulador++;
                    }
                    $pdf->Cell(6, 5, $beneficio[$valor], 1, 0, 'C');
                    $pdf->SetTextColor(0, 0, 0);
                }
                $pdf->Ln();
                $pdf->Ln();
                $pdf->SETX(20);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetFillColor(229, 229, 229);
                $pdf->SetTextColor(0, 0, 0);
                $pdf->SetWidths(array(18, 18, 22, 22, 25, 22, 28, 28));
                $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Row(array(
                    'TOTAL',
                    utf8_decode('DIAS HÁBILES'),
                    'DIAS FERIADOS',
                    'DIAS INACTIVOS',
                    'DIAS TRABAJADOS',
                    'DIAS DESCUENTO',
                    'VALOR A DESCONTAR',
                    'VALOR A PAGAR'
                ));
                $pdf->SETX(20);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetFillColor(255, 255, 255);
                $pdf->SetTextColor(0, 0, 0);
                $consultarPeriodo = $this->atReporteModelo->metObtenerPeriodo($periodo, $mes_periodo);
                $diasDescontar = $diasDescuentoAcumulador * $consultarPeriodo['num_valor_diario'];
                $diasPagar = $consultarPeriodo['num_valor_mes']-$diasDescontar;
                $pdf->Row(array(
                    $contador,
                    $diasHabilesAcumulador,
                    $diasFeriadoAcumulador,
                    $diasInactivoAcumulador,
                    $diaTrabajadoAcumulador,
                    $diasDescuentoAcumulador,
                    number_format($diasDescontar, 2, ',', '.'),
                    number_format($diasPagar, 2, ',', '.')
                ));
                $pdf->Ln();
                $pdf->Ln();
            }
        }
        else {
            $pdf->SETX(20);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(185, 5, 'NO SE ENCUENTRAN BENEFICIOS REGISTRADOS', 0, 1, 'C');
        }
        $pdf->Output();
    }


    public function metGenerarDiasHabiles($mes,$anio)
    {
        #obtengo el numero de dias del mes
        $dias_mes = cal_days_in_month(CAL_GREGORIAN, $mes, $anio);
        #armo el array de fechas
        $cont = 0;
        for($i = 1; $i <= $dias_mes; $i++) {
            $dia = date("N", strtotime("".$anio."-".$mes."-".$i.""));
            if($dia==6 || $dia==7){
                //no lo agrego al array por que sabado o domingo
            }else{
                $cont++;
            }
        }

        return $cont;
    }


    // Método que permite generar el resumen consolodado de eventos
    public function metGenerarResumenConsolidado()
    {
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('cabeceraReportes', 'modRH');
        $pdf = new pdfResumenEventosConsolidado('L', 'mm', 'A4');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        //Recibo los valores
        $pkNumOrganismo = $_GET['pk_num_organismo'];
        $pkNumDependencia = $_GET['pk_num_dependencia'];
        $centroCosto = $_GET['centro_costo'];
        $tipoNomina = $_GET['tipoNomina'];
        $cargo = $_GET['cargo'];
        $edoReg = $_GET['edo_reg'];
        $sitTrab = $_GET['sit_trab'];
        $pkNumEmpleado = $_GET['pkNumEmpleado'];
        $periodo = $_GET['periodo'];
        $mes_periodo = $_GET['mes_periodo'];
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(258, 5, 'CONSOLIDADO MENSUAL', 0, 0, 'C');
        // Obtengo la fecha de inicio y fin del periodo
        if (($periodo == '') && ($mes_periodo == '')){
            $obtenerBeneficio = $this->atReporteModelo->metObtenerUltimoBeneficio($pkNumOrganismo);
            $explodePeriodo = explode("-", $obtenerBeneficio['fec_inicio_periodo']);
            $periodoTotal = $explodePeriodo[0].'-'.$explodePeriodo[1];
            $periodo = $explodePeriodo[0];
            $mes_periodo = $explodePeriodo[1];
        } else {
            $periodoTotal = $periodo.'-'.$mes_periodo;
        }
        $datoPeriodo = $this->atReporteModelo->metObtenerPeriodo($periodo, $mes_periodo,$tipoNomina);
        $pdf->Ln();
        $pdf->Cell(258, 5, 'Periodo del ' . $datoPeriodo['fechaI'] . ' al ' . $datoPeriodo['fechaF'], 0, 1, 'C');
        $pdf->Ln();

        // Datos del Periodo
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->SETX(9);
        if($datoPeriodo['ind_descripcion_descuento']==null){

        }else{
            $pdf->Cell(200, 5, '* Se aplicara descuento por concepto de: '.$datoPeriodo['ind_descripcion_descuento'], 0, 1, 'L');
        }
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(229, 229, 229);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetWidths(array(30, 26, 25, 25, 25, 25, 30, 34));
        $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Row(array(
            'HORAS DIARIAS',
            'HORAS MENSUALES',
            utf8_decode('DIAS HÁBILES'),
            'DIAS FERIADOS',
            'DIAS INACTIVOS',
            'DESCUENTOS',
            'VALOR POR DIA',
            'VALOR MENSUAL'
        ));
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(255, 255, 255);
        $pdf->SetTextColor(0, 0, 0);
        // Calcular los dias inactivos del periodo
        $fechaInicioPeriodo = $datoPeriodo['fec_inicio_periodo'];
        $fechaFinPeriodo = $datoPeriodo['fec_fin_periodo'];
        $diasInactivos = $this->metObtenerFeriados($fechaInicioPeriodo, $fechaFinPeriodo);
        $pdf->SETX(10);
        $pdf->Row(array(
            $datoPeriodo['fec_horas_diarias'],
            $datoPeriodo['fec_horas_semanales'],
            $this->metGenerarDiasHabiles($mes_periodo,$periodo) - $datoPeriodo['num_total_feriados'],
            $datoPeriodo['num_total_feriados'],
            $diasInactivos['inactivo'],
            number_format($datoPeriodo['num_monto_descuento'], 2, ',', '.'),
            number_format($datoPeriodo['num_valor_diario'], 2, ',', '.'),
            number_format($datoPeriodo['num_valor_mes'], 2, ',', '.')
        ));


        // Listado de Empleados
        $listarEmpleado = $this->atReporteModelo->metConsultarEmpleado($pkNumOrganismo, $pkNumDependencia, $centroCosto, $tipoNomina, $cargo, $edoReg, $sitTrab, $pkNumEmpleado, $periodoTotal);
        $contador = count($listarEmpleado);
        if($contador>0) {
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(229, 229, 229);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetWidths(array(55, 21, 70, 18, 18, 18, 18, 18, 22,19));
            $pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C','C'));
            $pdf->SetFont('Arial', 'B', 7);
            $pdf->Ln();
            $semana = $this->metGenerarSemanas($datoPeriodo['fechaI'], $datoPeriodo['fechaF']);
            $n_sem = count($semana);
            if($n_sem==4){
                $pdf->Row(array('EMPLEADO','DOCUMENTO','CARGO','SEM 1','SEM 2','SEM 3','SEM 4','DESCUENTO','TOTAL'));
            }
            if($n_sem==5){
                $pdf->Row(array('EMPLEADO','DOCUMENTO','CARGO','SEM 1','SEM 2','SEM 3','SEM 4','SEM 5','DESCUENTO','TOTAL'));

            }
            if($n_sem==6){
                $pdf->Row(array('EMPLEADO','DOCUMENTO','CARGO','SEM 1','SEM 2','SEM 3','SEM 4','SEM 5','SEM 6','DESCUENTO','TOTAL'));

            }
            if($n_sem==7){
                $pdf->Row(array('EMPLEADO','DOCUMENTO','CARGO','SEM 1','SEM 2','SEM 3','SEM 4','SEM 5','SEM 6','SEM 7','DESCUENTO','TOTAL'));

            }

            $pdf->SetFont('Arial', '', 8);
            $pdf->SetDrawColor(0, 0, 0);
            $pdf->SetFillColor(255, 255, 255);
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetAligns(array('L', 'R', 'L', 'C', 'C', 'C', 'C', 'C', 'C','C'));
            $i = 0;
            $max_dias_pago = Session::metObtener('MAXDIASBONO');

            if(strcmp(Session::metObtener('UTPAGOMES'),'SI')==0){
                //solo 30 dias
                $dias_pago = $max_dias_pago;
            }else if(strcmp(Session::metObtener('UTPAGOMES'),'NO')==0){
                $dias_pago = $this->atFeriado->metObtenerDiasHabiles($fechaInicioPeriodo,$fechaFinPeriodo);
            }
            $modalidad = Session::metObtener('MODDIASBONO');

            if($modalidad==1){
                $max_dias_pago = Session::metObtener('MAXDIASBONO');
                $porc = Session::metObtener('UTPORC') / 100;
                $v_diario  = $this->atBonoAlimentacion->metObtenerUT(Session::metObtener('UTANIO'));
                $dias_periodo = $max_dias_pago;
                $aux = $porc * $v_diario['ind_valor'];
                $mes = $aux * $dias_periodo;
                $valor_dia = $mes / $dias_periodo;
                $valor_mes = $valor_dia * $dias_pago;
                $valor_semanal = $valor_mes / $n_sem;
            }

            foreach ($listarEmpleado as $empleado) {
                $pkNumEmpleado = $empleado['pk_num_empleado'];
                $i++;
                if ($i % 2 == 0) {
                    $pdf->SetFillColor(240, 240, 240);
                    $pdf->SetDrawColor(0, 0, 0);
                } else {
                    $pdf->SetFillColor(255, 255, 255);
                    $pdf->SetDrawColor(0, 0, 0);
                }
                //obtengo monto descuento aplicado
                $monto_descuento = $this->atReporteModelo->metMontoDescuentoEmpleadoBeneficio($pkNumEmpleado,$datoPeriodo['pk_num_beneficio']);
                // Calcular montos semanales
                $acumuladorTotal = 0;
                $semana = $this->metGenerarSemanas($datoPeriodo['fechaI'], $datoPeriodo['fechaF']);
                for ($i = 1; $i <= count($semana); $i++) {

                    if($modalidad==1){
                        //INDICA QUE SE VA A TOMAR OBLIGATORIAMENTE LOS 30 DIAS DE PAGO QUE INDICA LA LEY PARAMETRO (MAXDIASBONO)
                        $fechaInicioSemana = $semana[$i]['inicio'];
                        $fechaFinSemana = $semana[$i]['final'];
                        $explodeInicio = explode('/', $fechaInicioSemana);
                        $fechaISemana = $explodeInicio[2] . '-' . $explodeInicio[1] . '-' . $explodeInicio[0];
                        $explodeFin = explode('/', $fechaFinSemana);
                        $fechaFSemana = $explodeFin[2] . '-' . $explodeFin[1] . '-' . $explodeFin[0];
                        $eventoSemanal = $this->atReporteModelo->metEventoEmpleado($pkNumEmpleado, $fechaISemana, $fechaFSemana);
                        $acumuladorDescuento = count($eventoSemanal);
                        // Monto a pagar
                        $descuento = $acumuladorDescuento * $valor_dia;
                        $valorPagar = $valor_semanal - $descuento;
                        $montoPagar[$i] = $valorPagar;
                        $acumuladorTotal = $acumuladorTotal + $valorPagar;

                    }else{
                        $fechaInicioSemana = $semana[$i]['inicio'];
                        $fechaFinSemana = $semana[$i]['final'];
                        $explodeInicio = explode('/', $fechaInicioSemana);
                        $fechaISemana = $explodeInicio[2] . '-' . $explodeInicio[1] . '-' . $explodeInicio[0];
                        $explodeFin = explode('/', $fechaFinSemana);
                        $fechaFSemana = $explodeFin[2] . '-' . $explodeFin[1] . '-' . $explodeFin[0];
                        $eventoSemanal = $this->atReporteModelo->metEventoEmpleado($pkNumEmpleado, $fechaISemana, $fechaFSemana);
                        $acumuladorDescuento = count($eventoSemanal);
                        // Feriados e Inactivos
                        $feriados = $this->metObtenerFeriados($fechaISemana, $fechaFSemana);
                        // Dias a pagar
                        $diasPagar = $this->metCalcularDiasPagar($fechaISemana, $fechaFSemana);
                        $diasTotal = $diasPagar['total'] - $acumuladorDescuento;
                        // Monto a pagar
                        $descuento = $acumuladorDescuento * $datoPeriodo['num_valor_diario'];
                        $valorPagar = $datoPeriodo['num_valor_diario'] * $diasTotal;
                        $montoPagar[$i] = $valorPagar;
                        $acumuladorTotal = $acumuladorTotal + $valorPagar;
                    }

                }
                $pdf->SetFont('Arial', '', 7);

                if($n_sem==4){
                    $pdf->Row(array(
                        utf8_decode($empleado['ind_nombre1'] . ' ' . $empleado['ind_nombre2'] . ' ' . $empleado['ind_apellido1'] . ' ' . $empleado['ind_apellido2']),
                        number_format($empleado['ind_cedula_documento'], 0, '', '.'),
                        utf8_decode($empleado['ind_descripcion_cargo']),
                        number_format($montoPagar[1], 2, ',', '.'),
                        number_format($montoPagar[2], 2, ',', '.'),
                        number_format($montoPagar[3], 2, ',', '.'),
                        number_format($montoPagar[4], 2, ',', '.'),
                        number_format($monto_descuento, 2, ',', '.'),
                        number_format($acumuladorTotal-$monto_descuento, 2, ',', '.')
                    ));
                }
                if($n_sem==5){
                    $pdf->Row(array(
                        utf8_decode($empleado['ind_nombre1'] . ' ' . $empleado['ind_nombre2'] . ' ' . $empleado['ind_apellido1'] . ' ' . $empleado['ind_apellido2']),
                        number_format($empleado['ind_cedula_documento'], 0, '', '.'),
                        utf8_decode($empleado['ind_descripcion_cargo']),
                        number_format($montoPagar[1], 2, ',', '.'),
                        number_format($montoPagar[2], 2, ',', '.'),
                        number_format($montoPagar[3], 2, ',', '.'),
                        number_format($montoPagar[4], 2, ',', '.'),
                        number_format($montoPagar[5], 2, ',', '.'),
                        number_format($monto_descuento, 2, ',', '.'),
                        number_format($acumuladorTotal-$monto_descuento, 2, ',', '.')
                    ));

                }
                if($n_sem==6){
                    $pdf->Row(array(
                        utf8_decode($empleado['ind_nombre1'] . ' ' . $empleado['ind_nombre2'] . ' ' . $empleado['ind_apellido1'] . ' ' . $empleado['ind_apellido2']),
                        number_format($empleado['ind_cedula_documento'], 0, '', '.'),
                        utf8_decode($empleado['ind_descripcion_cargo']),
                        number_format($montoPagar[1], 2, ',', '.'),
                        number_format($montoPagar[2], 2, ',', '.'),
                        number_format($montoPagar[3], 2, ',', '.'),
                        number_format($montoPagar[4], 2, ',', '.'),
                        number_format($montoPagar[5], 2, ',', '.'),
                        number_format($montoPagar[6], 2, ',', '.'),
                        number_format($monto_descuento, 2, ',', '.'),
                        number_format($acumuladorTotal-$monto_descuento, 2, ',', '.')
                    ));

                }
                if($n_sem==7){
                    $pdf->Row(array(
                        utf8_decode($empleado['ind_nombre1'] . ' ' . $empleado['ind_nombre2'] . ' ' . $empleado['ind_apellido1'] . ' ' . $empleado['ind_apellido2']),
                        number_format($empleado['ind_cedula_documento'], 0, '', '.'),
                        utf8_decode($empleado['ind_descripcion_cargo']),
                        number_format($montoPagar[1], 2, ',', '.'),
                        number_format($montoPagar[2], 2, ',', '.'),
                        number_format($montoPagar[3], 2, ',', '.'),
                        number_format($montoPagar[4], 2, ',', '.'),
                        number_format($montoPagar[5], 2, ',', '.'),
                        number_format($montoPagar[6], 2, ',', '.'),
                        number_format($montoPagar[7], 2, ',', '.'),
                        number_format($monto_descuento, 2, ',', '.'),
                        number_format($acumuladorTotal-$monto_descuento, 2, ',', '.')
                    ));
                }

            }
        } else {
            $pdf->Ln();
            $pdf->SETX(20);
            $pdf->SetFont('Arial', '', 8);
            $pdf->Cell(245, 5, 'NO SE ENCUENTRAN BENEFICIOS REGISTRADOS', 0, 1, 'C');
        }
        $pdf->Output();
    }
}