<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class antecedenteServicioControlador extends Controlador
{
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('antecedenteServicio', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo
		);
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $listadoEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, '', '', '', '', '', '', '', '', '', '');
        $centroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, 0);
		$tipoEntidad = $this->atReporteModelo->metMostrarSelect('TIPOENTE');
		$areaExperiencia = $this->atReporteModelo->metMostrarSelect('AREAEXP');
		$motivoCese = $this->atReporteModelo->metConsultarMotivoCese();
		// Cargar a la vista
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('datos', $listadoEmpleado);
        $this->atVista->assign('centroCosto', $centroCosto);
		$this->atVista->assign('tipoEntidad', $tipoEntidad);
		$this->atVista->assign('areaExperiencia', $areaExperiencia);
		$this->atVista->assign('motivoCese', $motivoCese);
		$this->atVista->metRenderizar('listadoAntecedente');
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarDependencia()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$listarDependencia=$this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="" disabled selected>&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarCentroCosto()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
		$listarCentroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, $pkNumDependencia);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="centro_costo" id="centro_costo"><option value="0" disabled selected>&nbsp;</option>';
		foreach($listarCentroCosto as $centroCosto){
			$a .='<option value="'.$centroCosto['pk_num_centro_costo'].'">'.$centroCosto['ind_descripcion_centro_costo'].'</option>';
		}
		$a .= '</select><label for="pk_num_centro_costo"><i class="glyphicon glyphicon-briefcase"></i> Centro Costo</label></div>';
		echo $a;
	}

	// Método que permite listar los empleados por dependencia y organismo
    public function metListarEmpleado()
    {
        $pkNumOrganismo = $_POST['pk_num_organismo'];
        $pkNumDependencia = $_POST['pk_num_dependencia'];
		$centroCosto = $_POST['centro_costo'];
		$edo_reg = $_POST['edo_reg'];
		$sit_trab = $_POST['sit_trab'];
		$pkNumEmpleado = $_POST['pk_num_empleado'];
		$tipo_entidad = $_POST['tipo_entidad'];
		$areaExperiencia = $_POST['areaExperiencia'];
		$motivoCese = $_POST['motivo_cese'];
		$fechaInicioPrevia = $_POST['fechaInicio'];
		$fechaFinPrevia = $_POST['fechaFin'];
		if(($fechaInicioPrevia!='')&&($fechaFinPrevia!='')){
			// Formato de Fechas
			$fechaExplodeInicio = explode("/", $fechaInicioPrevia);
			$fechaInicio = $fechaExplodeInicio['2'] . '-' . $fechaExplodeInicio['1'] . '-' . $fechaExplodeInicio['0'];
			$fechaExplodeFin = explode("/", $fechaFinPrevia);
			$fechaFin = $fechaExplodeFin['2'] . '-' . $fechaExplodeFin['1'] . '-' . $fechaExplodeFin['0'];
		} else {
			$fechaInicio = '';
			$fechaFin = '';
		}

        $listadoEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $centroCosto, $edo_reg, $sit_trab, $pkNumEmpleado, $tipo_entidad, $areaExperiencia, $motivoCese, $fechaInicio, $fechaFin);
        echo json_encode($listadoEmpleado);
    }

    // Método que permite listar los empleados por dependencia y organismo
    public function metGenerarAntecedente()
    {
		$pk_num_empleado = $_POST['pk_num_empleado'];
		$empleado = array(
			'pkNumEmpleado' => $pk_num_empleado
		);
		$this->atVista->assign('empleado', $empleado);
        $this->atVista->metRenderizar('generarAntecedente', 'modales');
    }

	// Método que permite transformar la hora militar en horario estándar
	public function metConsultarHorario($horaSalida, $horaEntrada)
	{
// Hora de salida
		$horaExplodeInicio = explode(":", $horaSalida);
		$horaInicio = $horaExplodeInicio[0];
		$minutoInicio = $horaExplodeInicio[1];

		// Hora de entrada
		$horaExplodeFin = explode(":", $horaEntrada);
		$horaFin = $horaExplodeFin[0];
		$minutoFin = $horaExplodeFin[1];

		// Convirtiendo a horario estándar
		// Hora Inicio
		if($horaInicio>12){
			$horaInicioPrevio = $horaInicio - 12;
			$hora_inicio = str_pad($horaInicioPrevio, 2, "0", STR_PAD_LEFT);
		}
		if($horaInicio<12) {
			$hora_inicio = $horaInicio;
		}
		// Hora Fin
		if($horaFin>12){
			$horaFinPrevio = $horaFin - 12;
			$hora_fin = str_pad($horaFinPrevio, 2, "0", STR_PAD_LEFT);
		}
		if($horaFin<12) {
			$hora_fin = $horaFin;
		}
		// Horario
		// Horario Inicio
		if($horaInicio>=12){
			$horarioInicio = 'PM';
		}
		if($horaInicio<12) {
			$horarioInicio = 'AM';
		}
		//Horario Fin
		if($horaFin>=12){
			$horariofin = 'PM';
		}
		if($horaFin<12) {
			$horariofin = 'AM';
		}
		// Los agrupo en un array
		$datoHorario = array($hora_inicio,$minutoInicio, $hora_fin, $minutoFin, $horarioInicio, $horariofin);
		return $datoHorario;
	}

	// Método que permite calcular las horas semanales trabajadas
	public function metCalcularTiempo($horaini, $horafin)
	{
		$horai=substr($horaini,0,2);
		$mini=substr($horaini,3,2);
		$segi=substr($horaini,6,2);

		$horaf=substr($horafin,0,2);
		$minf=substr($horafin,3,2);
		$segf=substr($horafin,6,2);

		$ini=((($horai*60)*60)+($mini*60)+$segi);
		$fin=((($horaf*60)*60)+($minf*60)+$segf);
		$dif=$fin-$ini;
		$difh=floor($dif/3600);
		$difm=floor(($dif-($difh*3600))/60);
		$difs=$dif-($difm*60)-($difh*3600);
		return date("H:i:s",mktime($difh,$difm,$difs));
	}

	// Método que permite calcular el tiempo de servicio de un empleado
	public function metTiempoServicio($fechaIngreso, $fechaEgreso)
	{
		$fechaInicial = new DateTime($fechaIngreso);
		$fechaFinal = new DateTime($fechaEgreso);
		$interval = $fechaInicial->diff($fechaFinal);
		$datosServicio = array(
			'anio' => $interval->format('%y'),
			'mes' => $interval->format('%m'),
			'dia' => $interval->format('%d')
		);
		return $datosServicio;
	}

	// Método que se encarga de obtener el total de antecedentes
	public function metObtenerServicio($acumuladorDia, $acumuladorMes, $acumuladorAnio)
	{

		$_Dias_A_Meses = intval($acumuladorDia / 30);
		$acumuladorMes += $_Dias_A_Meses;
		$_Meses_A_Anios = intval($acumuladorMes / 12);
		$acumuladorAnio += $_Meses_A_Anios;
		$Dias  = $acumuladorDia - (intval($acumuladorDia / 30) * 30);
		$Meses = $acumuladorMes - (intval($acumuladorMes / 12) * 12);
		$Anios = $acumuladorAnio;
		$totalAntecedente = array(
			'anio' => $Anios,
			'mes' => $Meses,
			'dia' => $Dias
		);
		return $totalAntecedente;
	}

    // Funcion que genera el PDF correspondiente al reporte de antecedentes de servicio en la institucion
	public function metAntecendenteOrganismo()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfAntecedente('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 2);
		##	colores
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->SetTextColor(0, 0, 0);
		##	cuadro
		$pdf->Rect(10, 55, 195, 208, 'D');
		##
		$pdf->Ln();
		$pdf->SetFillColor(200, 200, 200);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(195, 5, utf8_decode('DATOS PERSONALES'), 1, 0, 'C', 1);
		// Consulto los datos personales
		$empleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pdf->Ln();
		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetWidths(array(150, 45));
		$pdf->SetAligns(array('L', 'C'));
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Row(array('APELLIDOS Y NOMBRES',
			'CEDULA DE IDENTIDAD'));
		$pdf->SetFont('Arial', '', 10);
		$pdf->Row(array(utf8_decode($empleado['ind_nombre1'].' '.$empleado['ind_nombre2'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2']),
			number_format($empleado['ind_cedula_documento'], 0, '', '.')));
		##
		$pdf->SetFillColor(200, 200, 200);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(195, 5, utf8_decode('ACTUACIÓN EN EL ORGANISMO'), 1, 0, 'C', 1);
		//Actuación en el organismo
		$nivelacionInicial = $this->atReporteModelo->metEmpleadoNivelación($pkNumEmpleado, 1);
		$nivelacionFinal = $this->atReporteModelo->metEmpleadoNivelación($pkNumEmpleado, 2);
		$horario = $this->atReporteModelo->metConsultarHorario($pkNumEmpleado);
		$salida = $horario['fec_salida1'];
		$inicio =  $horario['fec_entrada1'];
		$consultarHorario1 = $this->metConsultarHorario($salida, $inicio);
		$horaInicio1 = $consultarHorario1[2];
		$minutoInicio1 = $consultarHorario1[3];
		$horaFin1 = $consultarHorario1[0];
		$minutoFin1 = $consultarHorario1[1];
		$horarioInicio1 = $consultarHorario1[4];
		$horarioFin1 = $consultarHorario1[5];
		$salida2 = $horario['fec_salida2'];
		$inicio2 = $horario['fec_entrada2'];
		$consultarHorario2 = $this->metConsultarHorario($salida2, $inicio2);
		$horaInicio2 = $consultarHorario2[2];
		$minutoInicio2 = $consultarHorario2[3];
		$horaFin2 = $consultarHorario2[0];
		$minutoFin2 = $consultarHorario2[1];
		$horarioInicio2 = $consultarHorario2[4];
		$horarioFin2 = $consultarHorario2[5];
		$calcularTiempoInicio = $this->metCalcularTiempo($inicio, $salida);
		$diferenciaHoras1 = explode(":", $calcularTiempoInicio);
		$horaDiferencia1 = $diferenciaHoras1[0];
		$minutoDiferencia1 = $diferenciaHoras1[1];
		$calcularTiempoFin = $this->metCalcularTiempo($inicio2,  $salida2);
		$diferenciaHoras2 = explode(":", $calcularTiempoFin);
		$horaDiferencia2 = $diferenciaHoras2[0];
		$minutoDiferencia2 = $diferenciaHoras2[1];
		// Sumo las horas y los minutos
		$horaDiferencia = $horaDiferencia1 + $horaDiferencia2;
		$minutoDiferencia = $minutoDiferencia1 + $minutoDiferencia2;
		while ($minutoDiferencia >= 60) {
			if ($minutoDiferencia >= 60) {
				$horaDiferencia = $horaDiferencia + 1;
				$minutoDiferencia = $minutoDiferencia - 60;
			}
		}
		$hora = $horaDiferencia.','.$minutoDiferencia;
		$horaFloat = floatval($hora);
		$horaTotal = $horaFloat * 5;
		$explodeHora = explode(",", $horaTotal);
		$horaPrimero = $explodeHora[0];
		$minutoPrimero = $explodeHora[1];
		// Calculo las horas semanalaes
		while ($minutoPrimero >= 60) {
			if ($minutoPrimero >= 60) {
				$horaPrimero = $horaPrimero + 1;
				$minutoPrimero = $minutoPrimero - 60;
			}
		}
		$pdf->Ln();
		$pdf->SetFillColor(245, 245, 245);
		$pdf->SetWidths(array(97.5, 97.5));
		$pdf->SetAligns(array('C', 'C'));
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Row(array('INGRESO',
			'EGRESO'));
		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetWidths(array(31.5, 66, 31.5, 66));
		$pdf->SetAligns(array('C', 'L', 'C', 'L'));
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Row(array('FECHA',
			utf8_decode('TÍTULO DEL CARGO'),
			'FECHA',
			utf8_decode('TÍTULO DEL CARGO')));
		$pdf->SetWidths(array(10, 10, 11.5));
		$pdf->SetAligns(array('C', 'C', 'C'));
		$pdf->SetFont('Arial', 'B', 10);
		$y = $pdf->GetY();
		$pdf->SetXY(10, $y);
		$pdf->Row(array('DIA',
			'MES',
			utf8_decode('AÑO')));
		$pdf->SetXY(107.5, $y);
		$pdf->Row(array('DIA',
			'MES',
			utf8_decode('AÑO')));
		$pdf->SetFont('Arial', '', 10);
		$y = $pdf->GetY();
		$pdf->SetXY(10, $y);
		// Datos de Ingreso
		$fechaInicial = $nivelacionInicial['fec_fecha_registro'];
		$explodeFecha = explode("-", $fechaInicial);
		$diaInicial = $explodeFecha[2];
		$mesInicial = $explodeFecha[1];
		$anioInicial = $explodeFecha[0];
		$fechaFinal = $nivelacionInicial['fec_fecha_hasta'];
		$explodeFecha = explode("-", $fechaFinal);
		$diaFinal = $explodeFecha[2];
		$mesFinal = $explodeFecha[1];
		$anioFinal = $explodeFecha[0];
		$pdf->Row(array($diaInicial,$mesInicial,$anioInicial));
		$pdf->SetXY(41.5, $y-5);
		$pdf->MultiCell(66, 5, utf8_decode($nivelacionInicial['ind_cargo']), 0, 'L');
		$pdf->SetXY(107.5, $y);
		$pdf->Row(array($diaFinal,$mesFinal,$anioFinal));
		$pdf->SetXY(139, $y-5);
		$pdf->MultiCell(66, 5, utf8_decode($nivelacionFinal['ind_cargo']), 0, 'L');
		$pdf->SetY(91);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(20, 5, utf8_decode('Código: '), 1, 0, 'L');
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(11.5, 5, $empleado['pk_num_empleado'], 1, 0, 'C');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(40, 5, utf8_decode('Grado: '), 1, 0, 'C');
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(26, 5, $nivelacionInicial['ind_grado'], 1, 0, 'C');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(20, 5, utf8_decode('Código: '), 1, 0, 'L');
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(11.5, 5, $empleado['pk_num_empleado'], 1, 0, 'C');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(40, 5, utf8_decode('Grado: '), 1, 0, 'C');
		$pdf->SetFont('Arial','', 10);
		$pdf->Cell(26, 5, $nivelacionFinal['ind_grado'], 1, 0, 'C');
		##
		$pdf->Ln();
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->SetWidths(array(31.5, 40, 26, 31.5, 40, 26));
		$pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C'));
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Row(array('Tipo de Nombramiento',
			'Horario de Trabajo',
			'Horas Semanales',
			'Tipo de Nombramiento',
			'Horario de Trabajo',
			'Horas Semanales'));
		$pdf->SetFont('Arial', '', 10);
		$y = $pdf->GetY();
		$pdf->SetXY(10, $y);
		$pdf->Row(array(
			'',
			$horaInicio1.':'.$minutoInicio1.' '.$horarioInicio1.' a '.$horaFin1.':'.$minutoFin1.' '.$horarioFin1.' '.$horaInicio2.':'.$minutoInicio2.' '.$horarioInicio2.' a '.$horaFin2.':'.$minutoFin2.' '.$horarioFin2,
			$horaPrimero,
			'',
			$horaInicio1.':'.$minutoInicio1.' '.$horarioInicio1.' a '.$horaFin1.':'.$minutoFin1.' '.$horarioFin1.' '.$horaInicio2.':'.$minutoInicio2.' '.$horarioInicio2.' a '.$horaFin2.':'.$minutoFin2.' '.$horarioFin2,
			$horaPrimero,
		));
		##
		//Sueldo
		$sueldo = $this->atReporteModelo->metConsultarNomina($pkNumEmpleado, 1);
		$salario = $this->atReporteModelo->metConsultarNomina($pkNumEmpleado, 2);

		$pdf->SetFont('Arial', '', 10);
		$y = $pdf->GetY() + 2;
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->Line(107.50, $y-2, 107.50, $y+20);
		$pdf->SetXY(15, $y);
		$pdf->Cell(35, 5, utf8_decode('SUELDO BÁSICO'), 0, 0, 'L');
		$pdf->Cell(52.5, 5, number_format($sueldo['num_sueldo_base'], 2, ',', '.'), 0, 0, 'R');
		$pdf->SetXY(112.5, $y);
		$pdf->Cell(35, 5, utf8_decode('SUELDO BÁSICO'), 0, 0, 'L');
		$pdf->Cell(52.5, 5, number_format($salario['num_sueldo_base'], 2, ',', '.'), 0, 0, 'R');
		$pdf->SetXY(15, $y+6);
		$pdf->Cell(35, 5, utf8_decode('PRIMAS'), 0, 0, 'L');
		$pdf->Cell(52.5, 5, number_format($sueldo['num_total_asignaciones'], 2, ',', '.'), 0, 0, 'R');
		$pdf->SetXY(112.5, $y+6);
		$pdf->Cell(35, 5, utf8_decode('PRIMAS'), 0, 0, 'L');
		$pdf->Cell(52.5, 5, number_format($salario['num_total_asignaciones'], 2, ',', '.'), 0, 0, 'R');
		$pdf->SetXY(15, $y+12);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(35, 5, utf8_decode('TOTAL'), 0, 0, 'L');
		$pdf->Cell(52.5, 5, number_format($sueldo['num_sueldo_normal'], 2, ',', '.'), 0, 0, 'R');
		$pdf->SetXY(112.5, $y+12);
		$pdf->Cell(35, 5, utf8_decode('TOTAL'), 0, 0, 'L');
		$pdf->Cell(52.5, 5, number_format($salario['num_sueldo_normal'], 2, ',', '.'), 0, 0, 'R');
		##
		$pdf->Ln(8);
		$pdf->SetFillColor(200, 200, 200);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(97.5, 5, utf8_decode('TIPO DE EGRESO'), 1, 0, 'C', 1);
		$pdf->Cell(97.5, 5, utf8_decode('PAGO DE PRESTACIONES SOCIALES'), 1, 0, 'C', 1);
		$pdf->Ln();
		$pdf->SetFillColor(245, 245, 245);
		$pdf->SetWidths(array(97.5, 97.5));
		$pdf->SetAligns(array('C', 'L'));
		$pdf->SetFont('Arial', '', 10);
		$pdf->Row(array(utf8_decode(''),
			'               Si                                    No'));
##
		$pdf->SetFillColor(200, 200, 200);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(97.5, 5, utf8_decode('FUNDAMENTO LEGAL'), 1, 0, 'C', 1);
		$pdf->Cell(97.5, 5, utf8_decode('TIEMPO DE SERVICIO'), 1, 0, 'C', 1);
		$pdf->Ln();
		// Calcular Tiempo de Servicio en la institución
		$fechaI = $nivelacionInicial['fec_fecha_registro'];
		$fechaF = $nivelacionFinal['fec_fecha_hasta'];
		if($fechaF=='0000-00-00'){
			$fechaF = date('Y-m-d');
		}
		$antecedenteServicio = $this->metTiempoServicio($fechaI, $fechaF);
		$anioServicio = $antecedenteServicio['anio'];
		$mesServicio = $antecedenteServicio['mes'];
		$diaServicio = $antecedenteServicio['dia'];

		$TiempoServicio = "  AÑOS: $anioServicio                    MESES: $mesServicio                    DIAS: $diaServicio";
		$pdf->SetFillColor(245, 245, 245);
		$pdf->SetWidths(array(97.5, 97.5));
		$pdf->SetAligns(array('J', 'C'));
		$pdf->SetFont('Arial', '', 10);
		$basamentoLegal = 'ARTICULO 21 LEY DEL ESTATUTO DE LA FUNCIÓN PÚBLICA PUBLICADA  EN GACETA OFICIAL DE LA REPÚBLICA BOLIVARIANA DE VENEZUELA NRO 37.522 DE FECHA 06-09-2002.';
		$pdf->Row(array(utf8_decode($basamentoLegal),
			utf8_decode($TiempoServicio)));
		##
		$pdf->SetFillColor(200, 200, 200);
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(195, 5, utf8_decode('OBSERVACIONES'), 1, 0, 'C', 1);
		$pdf->Ln();
		$pdf->SetFillColor(245, 245, 245);

		$pdf->SetFont('Arial', '', 10);
		$sexo = $empleado['ind_nombre_detalle'];
		if($sexo=='MASCULINO'){
			$inicio = 'El CIUDADANO';
		} else {
			$inicio = 'LA CIUDADANA';
		}
		$nacionalidad = $this->atReporteModelo->metConsultarNacionalidad($pkNumEmpleado);
		$nacionalidadPrevia = $nacionalidad['nacionalidad'];
		$letraNacionalidad = substr($nacionalidadPrevia, 0, 1);

		$pdf->SetAligns(array('J'));
		$pdf->SetFont('Arial', '', 10);
		$pdf->SetWidths(array(195));

		if($nivelacionFinal['ind_cargo']=='') {
			$pdf->Row(array(
				utf8_decode($inicio.' '.$empleado['ind_nombre1'].' '.$empleado['ind_nombre2'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2'].', TITULAR DE LA CÉDULA DE IDENTIDAD '.$letraNacionalidad.'-'.number_format($empleado['ind_cedula_documento'], 0, '', '.').' PRESTÓ SUS SERVICIOS EN ESTE ÓRGANO DE CONTROL FISCAL DESDE EL DÍA '.$diaInicial.'-'.$mesInicial.'-'.$anioInicial.' HASTA EL DÍA '.$diaFinal.'-'.$mesFinal.'-'.$anioFinal.' COMO (EL EMPLEADO NO TIENE NIVELACIONES REGISTRADAS)')
			));
		} else {
			$pdf->Row(array(
				utf8_decode($inicio.' '.$empleado['ind_nombre1'].' '.$empleado['ind_nombre2'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2'].', TITULAR DE LA CÉDULA DE IDENTIDAD '.$letraNacionalidad.'-'.number_format($empleado['ind_cedula_documento'], 0, '', '.').' PRESTÓ SUS SERVICIOS EN ESTE ÓRGANO DE CONTROL FISCAL DESDE EL DÍA '.$diaInicial.'-'.$mesInicial.'-'.$anioInicial.' HASTA EL DÍA '.$diaFinal.'-'.$mesFinal.'-'.$anioFinal.' COMO '.$nivelacionFinal['ind_cargo'].'.')
			));
		}

		$pdf->SetFillColor(200, 200, 200);
		$pdf->Cell(195, 5, utf8_decode('CONFORMACIÓN Y APROBACIÓN'), 1, 0, 'C', 1);
		$pdf->Ln();
		$pdf->SetFillColor(255, 255, 255);
		$pdf->Cell(65, 5, utf8_decode('REVISADO POR'), 1, 0, 'C');
		$pdf->Cell(65, 5, utf8_decode('CONFORMADO POR'), 1, 0, 'C');
		$pdf->Cell(65, 5, utf8_decode('APROBADO POR'), 1, 0, 'C');
		$pdf->Rect(75, 203, 65, 60, 'D');
		##
		$pdf->SetDrawColor(255, 255, 255);
		$pdf->SetFont('Arial', '', 8);
		$pdf->SetWidths(array(63));
		$pdf->SetAligns(array('C'));
		$pdf->SetXY(11, 209);
		$pdf->Row(array(''));
		$pdf->SetX(11);
		$pdf->Row(array(''));
		$pdf->SetXY(76, 209);
		$pdf->Row(array(''));
		$pdf->SetX(76);
		$pdf->Row(array(utf8_decode('')));
		$pdf->SetXY(141, 209);
		$pdf->Row(array(utf8_decode('')));
		$pdf->SetX(141);
		$pdf->Row(array(utf8_decode('')));
		##
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->SetXY(11, 250);
		$pdf->Cell(65, 5, utf8_decode('Firma: '), 0, 0, 'L');
		$pdf->SetXY(76, 250);
		$pdf->Cell(65, 5, utf8_decode('Firma: '), 0, 0, 'L');
		$pdf->SetXY(141, 250);
		$pdf->Cell(65, 5, utf8_decode('Firma: '), 0, 0, 'L');
		$pdf->SetXY(11, 258);
		$pdf->Cell(65, 5, utf8_decode('Fecha: '.date('d-m-Y')), 0, 0, 'L');
		$pdf->SetXY(76, 258);
		$pdf->Cell(65, 5, utf8_decode('Fecha: '.date('d-m-Y')), 0, 0, 'L');
		$pdf->SetXY(141, 258);
		$pdf->Cell(65, 5, utf8_decode('Fecha: '.date('d-m-Y')), 0, 0, 'L');
		$pdf->Output();
	}

	// Funcion que genera el PDF correspondiente al reporte de antecedentes de servicio en la institucion
	public function metReporteExperienciaLaboral()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfExperienciaLaboral('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 2);
		$antecedente = $this->atReporteModelo->metConsultarAntecedente($pkNumEmpleado);
		$total = count($antecedente);
		$pdf->SetFont('Arial', '', 8);
		$acumuladorDia = 0;
		$acumuladorMes = 0;
		$acumuladorAnio = 0;
		$empleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$nombreCompleto = $empleado['ind_nombre1'].' '.$empleado['ind_nombre2'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2'];
		$pdf->Header($pkNumEmpleado, $nombreCompleto);
		if($total>0){
			$pdf->SetWidths(array(96, 21, 21, 20, 20, 20));
			$pdf->SetAligns(array('L','C', 'C', 'C', 'C','C'));
			$pdf->SetDrawColor(0, 0, 0);
			$pdf->SetFillColor(255, 255, 255);
			$pdf->SetTextColor(0, 0, 0);
			foreach($antecedente as $ant){
				$fechaIngreso = $ant['fec_ingreso'];
				$fechaEgreso = $ant['fec_egreso'];
				$tiempoServicio = $this->metTiempoServicio($fechaIngreso, $fechaEgreso);
				$anioAnt = $tiempoServicio['anio'];
				$mesAnt = $tiempoServicio['mes'];
				$diaAnt = $tiempoServicio['dia'];
				$acumuladorAnio = $acumuladorAnio + $anioAnt;
				$acumuladorMes = $acumuladorMes + $mesAnt;
				$acumuladorDia = $acumuladorDia + $diaAnt;
				$pdf->Row(array(
					utf8_decode($ant['ind_empresa']), $ant['fecha_ingreso'], $ant['fecha_egreso'], $anioAnt, $mesAnt, $diaAnt
				));
			}
			$tiempoAntecedente = $this->metObtenerServicio($acumuladorDia, $acumuladorMes, $acumuladorAnio);
			$diaTiempo = $tiempoAntecedente['dia'];
			$mesTiempo = $tiempoAntecedente['mes'];
			$anioTiempo = $tiempoAntecedente['anio'];
		} else {
			$pdf->SetWidths(array(198));
			$pdf->SetAligns(array('L'));
			$pdf->Row(array(
				'El empleado no tiene antecedentes'
			));
			$diaTiempo = 0;
			$mesTiempo = 0;
			$anioTiempo = 0;
		}
		// Tiempo de Servicio en el Organismo
		$nivelacionInicial = $this->atReporteModelo->metEmpleadoNivelación($pkNumEmpleado, 1);
		$nivelacionFinal = $this->atReporteModelo->metEmpleadoNivelación($pkNumEmpleado, 2);
		$fechaIngresoOrganismo = $nivelacionInicial['fec_fecha_registro'];
		$fechaEgresoOrganismo = $nivelacionFinal['fec_fecha_hasta'];
		if($fechaEgresoOrganismo=='0000-00-00'){
			$fechaEgresoOrganismo = date('Y-m-d');
		}
		$tiempoServicioOrganismo = $this->metTiempoServicio($fechaIngresoOrganismo, $fechaEgresoOrganismo);
		$anioServicioOrganismo = $tiempoServicioOrganismo['anio'];
		$mesServicioOrganismo = $tiempoServicioOrganismo['mes'];
		$diaServicioOrganismo = $tiempoServicioOrganismo['dia'];
		// Tiempo Total de Servicio
		$AnioTotalServicio = $anioTiempo + $anioServicioOrganismo;
		$mesTotalServicio = $mesTiempo + $mesServicioOrganismo;
		$diaTotalServicio = $diaTiempo + $diaServicioOrganismo;
		$totales = $this->metObtenerServicio( $diaTotalServicio, $mesTotalServicio, $AnioTotalServicio);
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(138, 5,'Antecedentes: ',0, 0, 'R');
		$pdf->Cell(20, 5,$anioTiempo,1, 0, 'C');
		$pdf->Cell(20, 5,$mesTiempo,1, 0, 'C');
		$pdf->Cell(20, 5,$diaTiempo,1, 0, 'C');
		$pdf->Ln();
		$pdf->Cell(138, 5,'En el Organismo: ',0, 0, 'R');
		$pdf->Cell(20, 5,$anioServicioOrganismo,1, 0, 'C');
		$pdf->Cell(20, 5,$mesServicioOrganismo,1, 0, 'C');
		$pdf->Cell(20, 5,$diaServicioOrganismo,1, 0, 'C');
		$pdf->Ln();
		$pdf->Cell(138, 5,'Tiempo de Servicio: ',0, 0, 'R');
		$pdf->Cell(20, 5,$totales['anio'],1, 0, 'C');
		$pdf->Cell(20, 5,$totales['mes'],1, 0, 'C');
		$pdf->Cell(20, 5,$totales['dia'],1, 0, 'C');
		$pdf->Output();
	}

	// Funcion que genera el PDF correspondiente al reporte de antecedentes de servicio en la institucion
	public function metReporteConsolidado()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfConsolidado('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 2);
		$pdf->Ln();
		$empleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$nombreCompleto = $empleado['ind_nombre1'].' '.$empleado['ind_nombre2'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2'];
		$antecedente = $this->atReporteModelo->metConsultarAntecedente($pkNumEmpleado);
		$total = count($antecedente);
		$acumuladorDia = 0;
		$acumuladorMes = 0;
		$acumuladorAnio = 0;
		if($total>0){
			foreach($antecedente as $ant){
				$fechaIngreso = $ant['fec_ingreso'];
				$fechaEgreso = $ant['fec_egreso'];
				$tiempoServicio = $this->metTiempoServicio($fechaIngreso, $fechaEgreso);
				$anioAnt = $tiempoServicio['anio'];
				$mesAnt = $tiempoServicio['mes'];
				$diaAnt = $tiempoServicio['dia'];
				$acumuladorAnio = $acumuladorAnio + $anioAnt;
				$acumuladorMes = $acumuladorMes + $mesAnt;
				$acumuladorDia = $acumuladorDia + $diaAnt;
			}
			$tiempoAntecedente = $this->metObtenerServicio($acumuladorDia, $acumuladorMes, $acumuladorAnio);
			$diaTiempo = $tiempoAntecedente['dia'];
			$mesTiempo = $tiempoAntecedente['mes'];
			$anioTiempo = $tiempoAntecedente['anio'];
		} else {
			$diaTiempo = 0;
			$mesTiempo = 0;
			$anioTiempo = 0;
		}
		// Tiempo de Servicio en el Organismo
		$nivelacionInicial = $this->atReporteModelo->metEmpleadoNivelación($pkNumEmpleado, 1);
		$nivelacionFinal = $this->atReporteModelo->metEmpleadoNivelación($pkNumEmpleado, 2);
		$fechaIngresoOrganismo = $nivelacionInicial['fec_fecha_registro'];
		$fechaEgresoOrganismo = $nivelacionFinal['fec_fecha_hasta'];
		if($fechaEgresoOrganismo=='0000-00-00'){
			$fechaEgresoOrganismo = date('Y-m-d');
		}
		$tiempoServicioOrganismo = $this->metTiempoServicio($fechaIngresoOrganismo, $fechaEgresoOrganismo);
		$anioServicioOrganismo = $tiempoServicioOrganismo['anio'];
		$mesServicioOrganismo = $tiempoServicioOrganismo['mes'];
		$diaServicioOrganismo = $tiempoServicioOrganismo['dia'];
		// Tiempo Total de Servicio
		$AnioTotalServicio = $anioTiempo + $anioServicioOrganismo;
		$mesTotalServicio = $mesTiempo + $mesServicioOrganismo;
		$diaTotalServicio = $diaTiempo + $diaServicioOrganismo;
		$totales = $this->metObtenerServicio( $diaTotalServicio, $mesTotalServicio, $AnioTotalServicio);
		$pdf->SetWidths(array(20, 88, 10, 10, 10, 10, 10, 10, 10, 10, 10));
		$pdf->SetAligns(array('L', 'L', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->SetFillColor(255, 255, 255);
		$pdf->SetTextColor(0, 0, 0);
		$pdf->Row(array(
			$pkNumEmpleado, utf8_decode($nombreCompleto), $anioTiempo, $mesTiempo, $diaTiempo, $diaServicioOrganismo, $mesServicioOrganismo, $anioServicioOrganismo, $totales['anio'], $totales['mes'], $totales['dia']
		));
		$pdf->Output();
	}
}
