<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class ceseReingresoControlador extends Controlador
{
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('ceseReingreso', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo
		);
		$tipoCese = $this->atReporteModelo->metMostrarSelect('TIPOCR');
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
        $listadoEmpleado = $this->atReporteModelo->metBuscarEmpleados($pkNumOrganismo);
        $centroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, 0);
		// Cargar a la vista
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
        $this->atVista->assign('datos', $listadoEmpleado);
        $this->atVista->assign('centroCosto', $centroCosto);
		$this->atVista->assign('tipoCese', $tipoCese);
		$this->atVista->metRenderizar('listadoCeseReingreso');
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarDependencia()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$listarDependencia=$this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="">&nbsp;</option>';
		foreach($listarDependencia as $listarDependencia){
			$a .='<option value="'.$listarDependencia['pk_num_dependencia'].'">'.$listarDependencia['ind_dependencia'].'</option>';
		}
		$a .= '</select><label for="pk_num_dependencia"><i class="glyphicon glyphicon-briefcase"></i> Dependencia</label></div>';
		echo $a;
	}

	// Método que permite listar las dependencias del organismo
	public function metBuscarCentroCosto()
	{
		$pkNumOrganismo = $this->metObtenerInt('pk_num_organismo');
		$pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
		$listarCentroCosto = $this->atReporteModelo->metListarCentroCosto($pkNumOrganismo, $pkNumDependencia);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="centro_costo" id="centro_costo"><option value="" disabled selected>&nbsp;</option>';
		foreach($listarCentroCosto as $centroCosto){
			$a .='<option value="'.$centroCosto['pk_num_centro_costo'].'">'.$centroCosto['ind_descripcion_centro_costo'].'</option>';
		}
		$a .= '</select><label for="pk_num_centro_costo"><i class="glyphicon glyphicon-briefcase"></i> Centro Costo</label></div>';
		echo $a;
	}

	// Método que permite
	public function metGenerarReporteCese()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfReporteCese('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$centroCosto = $_GET['centro_costo'];
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$tipo = $_GET['tipo'];
		$fechaInicioPrevia = $_GET['fechaInicio'];
		$fechaFinPrevia = $_GET['fechaFin'];
		// Formato de Fechas
		if(($fechaInicioPrevia!='')&&($fechaFinPrevia!='')){
			$explodeInicio = explode('/', $fechaInicioPrevia);
			$fechaInicio = $explodeInicio[2].'-'.$explodeInicio[1].'-'.$explodeInicio[0];
			$explodeFin = explode('/', $fechaFinPrevia);
			$fechaFin = $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];
		} else {
			$fechaInicio = '';
			$fechaFin = '';
		}
		$pdf->SetWidths(array(10, 20, 20, 107, 20, 20));
		$pdf->SetAligns(array('C', 'C', 'C', 'L','C','C'));
		$pdf->SetFont('Arial', '', 8);
		$pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
		$determinarTipo = $this->atReporteModelo->metListarCeseReingreso($tipo);
		$nombreTipo = $determinarTipo['ind_nombre_detalle'];
		if($nombreTipo=='CESE'){
			$valor = 1;
		} else {
			$valor = 2;
		}
		$listadoEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $centroCosto, $pkNumEmpleado, $tipo, $fechaInicio, $fechaFin, $valor);
		$contador = 1;
		$i = 0;
		foreach($listadoEmpleado as $emp){
			$i++;
			if ($i % 2 == 0) { $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);  }
			else { $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255); }
			if($valor==1){
				$fecha = $emp['fecha_egreso'];
			} else {
				$fecha = $emp['fecha_ingreso'];
			}
			$pdf->Row(array(
				$contador,
				$emp['pk_num_empleado_cese_reingreso'],
				$emp['pk_num_empleado'],
				utf8_decode($emp['ind_nombre1'].' '.$emp['ind_nombre2'].' '.$emp['ind_apellido1'].' '.$emp['ind_apellido2']),
				utf8_decode($emp['ind_nombre_detalle']),
				$fecha
			));
			$contador++;
		}
		$pdf->Output();
	}
}
