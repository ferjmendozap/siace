<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class empleadoReporteControlador extends Controlador
{
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('empleadoReporte', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js = array('materialSiace/core/demo/DemoTableDynamic',  'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo
		);
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
		$tipoNomina = $this->atReporteModelo->metListarNomina();
		$tipoTrabajador = $this->atReporteModelo->metMostrarSelect('TIPOTRAB');
		$listadoEmpleado = $this->atReporteModelo->metConsultarEmpleados();
        $centroCosto = $this->atReporteModelo->metListarCentroCosto();
		// Cargar a la vista
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
		$this->atVista->assign('nomina', $tipoNomina);
		$this->atVista->assign('tipoTrabajador', $tipoTrabajador);
		$this->atVista->assign('listadoEmpleado', $listadoEmpleado);
        $this->atVista->assign('centroCosto', $centroCosto);
		$this->atVista->metRenderizar('listadoEmpleado');
	}

	// Funcion que genera el PDF correspondiente al reporte de empleados
	public function metGenerarReporteEmpleados()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$tipoNomina = $_GET['tipo_nomina'];
		$tipoTrabajador = $_GET['tipo_trabajador'];
		$edo_reg = $_GET['edo_reg'];
		$sit_trab = $_GET['sit_trab'];
		$buscar = $_GET['buscar'];
		$fechaInicioPrevio = $_GET['fechaInicio'];
		$fechaFinPrevio = $_GET['fechaFin'];
		$centroCosto = $_GET['centro_costo'];
		// Cambio el formato de las fechas
		if(($fechaInicioPrevio!='')&&($fechaFinPrevio!='')) {
			$explodeInicio = explode('/', $fechaInicioPrevio);
			$fechaInicio = $explodeInicio[2].'-'.$explodeInicio[1].'-'.$explodeInicio[0];
			$explodeFin = explode('/', $fechaFinPrevio);
			$fechaFin = $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];
		} else {
			$fechaInicio = '';
			$fechaFin = '';
		}
		
		$listadoEmpleados = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $tipoNomina, $tipoTrabajador,  $edo_reg, $sit_trab, $fechaInicio, $fechaFin, $buscar, $centroCosto);
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfEmpleado('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 5);
		$pdf->Header(1);
		$pdf->SetWidths(array(10, 58, 15, 50, 55, 10));
		$pdf->SetDrawColor(0, 0, 0);
		$pdf->SetFillColor(255,255,255);
		$pdf->SetTextColor(0, 0, 0);
		foreach ($listadoEmpleados as $listado){
			$nombreCompleto = $listado['ind_nombre1'].' '.$listado['ind_nombre2'].' '.$listado['ind_apellido1'].' '.$listado['ind_apellido2'];
			$estadoPrevio = $listado['ind_estatus'];
			if($estadoPrevio==1){
				$estado = 'Activo';
			} else {
				$estado = 'Inactivo';
			}
			$pdf->Row(array(
				$listado['pk_num_empleado'],
				utf8_decode($nombreCompleto),
				$listado['ind_cedula_documento'],
				$listado['ind_descripcion_cargo'],
				utf8_decode($listado['ind_dependencia']),
				$estado
			), 5);
		}
		$pdf->Output();

	}
}
