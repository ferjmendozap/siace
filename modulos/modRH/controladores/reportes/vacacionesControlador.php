<?php
/*****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Reporte de Empleado. Recursos Humanos
 * PROGRAMADORES._________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |__________________________________________________________________________________
 *****************************************************************************************/
// Clase encargada de la gestión de vacaciones solicitadas por funcionarios
class vacacionesControlador extends Controlador
{
	private $atReporteModelo;

	public function __construct()
	{
		parent::__construct();
		Session::metAcceso();
		$this->atReporteModelo = $this->metCargarModelo('vacaciones', 'reportes');
	}

	// Index del controlador
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
			'select2/select201ef',
			'bootstrap-datepicker/datepicker',
			'multi-select/multi-select555c'
		);
		$complementosJs = array(
			'select2/select2.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'multi-select/jquery.multi-select'
		);
		$js = array('materialSiace/core/demo/DemoTableDynamic',  'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$validar = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$this->atVista->metCargarJsComplemento($validar);
		$usuario = Session::metObtener('idUsuario');
		$empleado = $this->atReporteModelo->metUsuario($usuario, 1);
		$pkNumEmpleado = $empleado['pk_num_empleado'];
		$datosEmpleado = $this->atReporteModelo->metEmpleado($pkNumEmpleado);
		$pkNumOrganismo = $datosEmpleado['fk_a001_num_organismo'];
		$listadoDependencia = $this->atReporteModelo->metListarDependencia($pkNumOrganismo, 1, 0);
		$empleado = array(
			'pk_num_organismo' => $pkNumOrganismo
		);
		$tipoNomina = $this->atReporteModelo->metListarNomina();
		$listadoOrganismo = $this->atReporteModelo->metListarOrganismo($pkNumOrganismo, 1);
		$listarEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, '', '', '', '', '');
		$listadoEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, '', '', '', '', '');
		$this->atVista->assign('nomina', $tipoNomina);
		$this->atVista->assign('listadoDependencia', $listadoDependencia);
		$this->atVista->assign('emp', $empleado);
		$this->atVista->assign('listadoOrganismo', $listadoOrganismo);
		$this->atVista->assign('datos', $listadoEmpleado);
		$this->atVista->assign('listadoEmpleado', $listarEmpleado);
		$this->atVista->metRenderizar('listadoVacacion');
	}

	// Método que permite listar los empleados del organismo
	public function metBuscarEmpleado()
	{
		$pkNumDependencia = $this->metObtenerInt('pk_num_dependencia');
		$listarEmpleado=$this->atReporteModelo->metListadoEmpleados('', $pkNumDependencia);
		$a = '<div class="form-group floating-label"><select class="form-control dirty" name="pk_num_dependencia" id="pk_num_dependencia"> <option value="">&nbsp;</option>';
		foreach($listarEmpleado as $empleado){
			$a .='<option value="'.$empleado['pk_num_empleado'].'">'.$empleado['ind_nombre1'].' '.$empleado['ind_nombre2'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2'].'</option>';
		}
		$a .= '</select><label for="pk_num_empleado"><i class="glyphicon glyphicon-briefcase"></i> Empleado</label></div>';
		echo $a;
	}

// Método que permite generar el reporte de permisos de todos los funcionarios asociados a la búsqueda
	public function metGenerarListadoVacacion()
	{
		ini_set('error_reporting', 'E_ALL & ~E_STRICT');
		$this->metObtenerLibreria('cabeceraReportes', 'modRH');
		$pdf = new pdfListadoVacaciones('P', 'mm', 'Letter');
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$pdf->SetAutoPageBreak(true, 10);
		$pkNumOrganismo = $_GET['pk_num_organismo'];
		$pkNumDependencia = $_GET['pk_num_dependencia'];
		$pkNumEmpleado = $_GET['pk_num_empleado'];
		$sitTrab = $_GET['sit_trab'];
		$empleado = $_GET['empleado'];
		$fechaInicioPrevia = $_GET['fechaInicio'];
		$fechaFinPrevia = $_GET['fechaFin'];
		$periodo = $_GET['periodo'];
		$estado = $_GET['estado'];
		if($fechaInicioPrevia!=''){
			$explodeInicio = explode('/', $fechaInicioPrevia);
			$fechaInicio = $explodeInicio[2].'-'.$explodeInicio[1].'-'.$explodeInicio[0];
		} else {
			$fechaInicio = '';
		}
		if($fechaFinPrevia!=''){
			$explodeFin = explode('/', $fechaFinPrevia);
			$fechaFin = $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];
		} else {
			$fechaFin = '';
		}

		$listarEmpleado = $this->atReporteModelo->metListadoEmpleados($pkNumOrganismo, $pkNumDependencia, $pkNumEmpleado, $sitTrab, $empleado);
		foreach($listarEmpleado as $empleado){
			$pdf->SetWidths(array(15, 75));
			$pdf->SetAligns(array('C', 'L'));
			$pdf->SetFont('Arial', 'B', 8);
			$pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255);
			$pkNumEmpleado = $empleado['pk_num_empleado'];
			$listarVacacion = $this->atReporteModelo->metListadoVacacion($pkNumEmpleado, $fechaInicio, $fechaFin, $estado);
			$contador = count($listarVacacion);
			if($contador>0){
				$pdf->Row(array(
					$pkNumEmpleado,
					utf8_decode($empleado['ind_nombre1'].' '.$empleado['ind_nombre2'].' '.$empleado['ind_apellido1'].' '.$empleado['ind_apellido2']),
				));
				$contar = 0;
				foreach($listarVacacion as $vacacion){
					$pdf->SETX(20);
					$contar++;
					if ($contar % 2 == 0) { $pdf->SetFillColor(240, 240, 240); $pdf->SetDrawColor(240, 240, 240);  }
					else { $pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255); }
					$pdf->SetWidths(array(30, 15, 30, 30, 30, 15, 30));
					$pdf->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C', 'C'));
					$pdf->SetFont('Arial', '', 8);
					$pkNumSolicitudVacacion = $vacacion['pk_num_solicitud_vacacion'];
					$letraVacacion = substr($vacacion['ind_nombre_detalle'],0,1);
					$pdf->Row(array(
						$vacacion['num_anio'].'-'.$pkNumSolicitudVacacion,
						$letraVacacion,
						$vacacion['fecha_solicitud'],
						$vacacion['fecha_salida'],
						$vacacion['fecha_termino'],
						$vacacion['num_dias_solicitados'],
						$vacacion['fecha_incorporacion'],
					));
					if($periodo!=''){
						$listarDetalle = $this->atReporteModelo->metMostrarDetalleVacacion($pkNumSolicitudVacacion);
						$pdf->SetWidths(array( 30, 30, 30, 15, 30));
						$pdf->SetAligns(array('C', 'C', 'C', 'C', 'C'));
						$pdf->SetFont('Arial', 'B', 8);
						$pdf->SetFillColor(255, 255, 255); $pdf->SetDrawColor(255, 255, 255);
						foreach($listarDetalle as $detalle){
							$numAnioAnterior = $detalle['num_anio']-1;
							$pdf->SETX(65);
							$pdf->Row(array(
								$numAnioAnterior.' - '.$detalle['num_anio'],
								$detalle['fecha_inicial'],
								$detalle['fecha_final'],
								$detalle['num_dias_detalle'],
								$detalle['fecha_incorporacion']
							));
						}
					}
				}
			}
			$pdf->Ln();
		}
		$pdf->Output();
	}
}
