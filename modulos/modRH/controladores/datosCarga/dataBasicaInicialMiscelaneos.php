<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
trait dataBasicaInicialMiscelaneos
{




    public function metMiscelaneosRH()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Miscelaneos RH<br>';
        $a005_miscelaneo_maestro = array(
            array('ind_nombre_maestro' => 'TIPO TRABAJADOR', 'ind_descripcion' => 'TIPO DE TRABAJADOR ', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOTRAB',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ACTIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'INACTIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'JUBILADO', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'PENSIONADO POR INVALIDEZ', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'PENSIONADO SOBREVIVIENTE', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'CONTRATADO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'GRUPO OCUPACIONAL ', 'ind_descripcion' => 'GRUPO OCUPACIONAL DEL CARGO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'GRUPOCUP',
                'Det' => array(
                    array('cod_detalle' => 'TEC', 'ind_nombre_detalle' => 'TÉCNICO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'ADMI', 'ind_nombre_detalle' => 'ADMINISTRATIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PRO', 'ind_nombre_detalle' => 'PROFESIONAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GRH', 'ind_nombre_detalle' => 'GRUPO DE RECURSOS HUMANOS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDT', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN TÉCNICA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GPPE', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN DE CONTROL DE LOS PODERES PÚBLICOS ESTADALES', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDCD', 'ind_nombre_detalle' => 'GRUPO DIRECCIÒN DE CONTROL DE LA ADMINISTRACIÓN ESTADAL DESCENTRALALIZADA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'DDC', 'ind_nombre_detalle' => 'GRUPO DESPACHO CONTRALOR', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GUAI', 'ind_nombre_detalle' => 'GRUPO UNIDAD DE AUDITORIA INTERNA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GOAC', 'ind_nombre_detalle' => 'GRUPO OFICINA DE ATENCIÓN AL CIUDADANO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDG', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN GENERAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GADM', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN DE ADMINISTRACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDSJ', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN DE SERVICIOS JURÍDICOS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDRA', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN DE DETERMINACIÓN DE RESPONSABILIDADES', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDSG', 'ind_nombre_detalle' => 'GRUPO DE SERVICIOS GENERALES', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDCP', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE CONTROL PREVIO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDVT', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN TECNICA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDVF', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE FISCALIZACIÓN E INVESTIGACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GUAA', 'ind_nombre_detalle' => 'GRUPO UNIDAD AVERIGUACIONES ADMINISTRATIVA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDE', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE EXAMEN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GSC', 'ind_nombre_detalle' => 'GRUPO SALA DE CONTROL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDCE', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE CENTRALIZACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GJP', 'ind_nombre_detalle' => 'GRUPO DE JEFATURA DE PERSONAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDFI', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE FISCALIZACIÓN E INSPECCIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDI', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN INSPECCIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDAP', 'ind_nombre_detalle' => 'GRUPO DE DIVISION DE ADMINISTRACION Y CONTROL DE PERSONAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GSA', 'ind_nombre_detalle' => 'GRUPO SALA DE AUDITORIA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDCO', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE CONTROL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDBI', 'ind_nombre_detalle' => 'GRUPO DIVISIÓN DE BIENES Y MATERIAS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GDSP', 'ind_nombre_detalle' => 'GRUPO DIRECCIÓN DE SISTEMAS Y PROCEDIMIENTOS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'DA', 'ind_nombre_detalle' => 'DEPARTAMENTO DE ARCHIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'GSB', 'ind_nombre_detalle' => 'GRUPO SALA DE BIENES', 'num_estatus' => '1'),
                    array('cod_detalle' => 'DIRE', 'ind_nombre_detalle' => 'GRUPO DIRECTORES', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'SEXO DE LA PERSONA', 'ind_descripcion' => 'SEXO DE LA PERSONA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'SEXO',
                'Det' => array(
                    array('cod_detalle' => 'M', 'ind_nombre_detalle' => 'MASCULINO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'F', 'ind_nombre_detalle' => 'FEMENINO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'NACIONALIDADES', 'ind_descripcion' => 'NACIONALIDADES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'NACION',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'VENEZOLANO(A)', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'EXTRANJERO(A)', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'SITUACION DOMICILIO', 'ind_descripcion' => 'TIPO SITUACION DOMICILIO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'SITDOM',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ALQUILER', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'PROPIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'COMPARTIDA', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'CON FAMILIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'EN RESIDENCIA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'GRUPOS SANGUINEOS', 'ind_descripcion' => 'TIPOS DE GRUPOS SANGUINEOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'SANGRE',
                'Det' => array(
                    array('cod_detalle' => 'A', 'ind_nombre_detalle' => 'GRUPO A NEGATIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'A', 'ind_nombre_detalle' => 'GRUPO A POSITIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'B', 'ind_nombre_detalle' => 'GRUPO B NEGATIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'B', 'ind_nombre_detalle' => 'GRUPO B POSITIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'O', 'ind_nombre_detalle' => 'GRUPO O NEGATIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'O', 'ind_nombre_detalle' => 'GRUPO O POSITIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AB', 'ind_nombre_detalle' => 'GRUPO AB NEGATIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AB', 'ind_nombre_detalle' => 'GRUPO AB POSITIVO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'PARENTESCO', 'ind_descripcion' => 'PARENTESCO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'PARENT',
                'Det' => array(
                    array('cod_detalle' => 'Am', 'ind_nombre_detalle' => 'AMIGO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CB', 'ind_nombre_detalle' => 'CONCUBINO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => 'ES', 'ind_nombre_detalle' => 'ESPOSO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => 'HE', 'ind_nombre_detalle' => 'HERMANO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => 'HI', 'ind_nombre_detalle' => 'HIJO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'MA', 'ind_nombre_detalle' => 'MADRE', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PA', 'ind_nombre_detalle' => 'PADRE', 'num_estatus' => '1'),
                    array('cod_detalle' => 'SB', 'ind_nombre_detalle' => 'SOBRINO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'SU', 'ind_nombre_detalle' => 'SUEGRO (A)', 'num_estatus' => '1'),
                    array('cod_detalle' => 'TI', 'ind_nombre_detalle' => 'TIO (A)', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO LICENCIA', 'ind_descripcion' => 'TIPO LICENCIA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOLIC',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'PRIMERA', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'SEGUNDA', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'TERCERA', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'CUARTA', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'QUINTA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'CATEGORIA CARGO', 'ind_descripcion' => 'CATEGORIA CARGO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CATCARGO',
                'Det' => array(
                    array('cod_detalle' => 'EMPL', 'ind_nombre_detalle' => 'EMPLEADO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'OBRE', 'ind_nombre_detalle' => 'OBRERO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'GRADO INSTRUCCIÓN', 'ind_descripcion' => 'GRADO INSTRUCCIÓN PERSONA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'GRADOINST',
                'Det' => array(
                    array('cod_detalle' => 'PRI', 'ind_nombre_detalle' => 'PRIMARIA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'SEC', 'ind_nombre_detalle' => 'SECUNDARIA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'BAC', 'ind_nombre_detalle' => 'BACHILLER', 'num_estatus' => '1'),
                    array('cod_detalle' => 'TSU', 'ind_nombre_detalle' => 'TÉCNICO SUPERIOR UNIVERSITARIO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'UNI', 'ind_nombre_detalle' => 'UNIVERSITARIO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'POS', 'ind_nombre_detalle' => 'POSTGRADO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'MAG', 'ind_nombre_detalle' => 'MAGISTER', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PRE', 'ind_nombre_detalle' => 'PREESCOLAR', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'AREA  DE FORMACIÓN', 'ind_descripcion' => 'TIPOS DE AREA DE FORMACIÓN', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AREA',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'CONTABLES ADMINISTRATIVAS Y ECONOMICAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'COMPUTACIÓN E INFORMÁTICA', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'RELACIONES INDUSTRIALES Y RRHH', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'CONSTRUCCION Y DISEÑO', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'NDUSTRIA MECANICA Y ELECTRICIDAD', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'PREGUNTAS DEL CLIMA LABORAL', 'ind_descripcion' => 'AREA CLIMA LABORAL', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AREACLIMA',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ITEMS', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'LIDERAZGO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'GRADOS ESTUDIOS', 'ind_descripcion' => 'GRADOS ESTUDIOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'GRADOEST',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'PRIMER GRADO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'SEGUNDO GRADO ', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'UTILES ESCOLARES', 'ind_descripcion' => 'UTILES ESCOLARES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'UTILESESC',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'LAPIZ', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CAS', 'ind_nombre_detalle' => 'CUADERNOS', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO TELEFONO', 'ind_descripcion' => 'TIPO TELEFONO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOTELF',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'CASA', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'CELULAR', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'FAX', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE DIRECCIÓN', 'ind_descripcion' => 'TIPO DE DIRECCIÓN', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPODIR',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'DIRECCIÓN FISCAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'DIRECCIÓN RESIDENCIAL', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'CLASE DE LICENCIA', 'ind_descripcion' => 'CLASE DE LICENCIA DE CONDUCIR', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CLASELIC',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'VEHICULO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'MOTO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE EDUCACION', 'ind_descripcion' => 'TIPO DE EDUCACION', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TEDUCA',
                'Det' => array(
                    array('cod_detalle' => 'PR', 'ind_nombre_detalle' => 'PRIVADA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PU', 'ind_nombre_detalle' => 'PUBLICA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'AREA DE EXPERIENCIA', 'ind_descripcion' => 'AREA DE EXPERIENCIA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'AREAEXP',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ADMINISTRACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'FISCALAUDITORIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'TRANSPORTE', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'CONTROL DE CUENTAS BANCARIAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'COMPRAS Y LOGISTICA', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'CREDITOS Y COBRANZAS', 'num_estatus' => '1'),
                    array('cod_detalle' => '07', 'ind_nombre_detalle' => 'COMPUTACION E INFORMATICA', 'num_estatus' => '1'),
                    array('cod_detalle' => '08', 'ind_nombre_detalle' => 'SERVICIOS GENERALES', 'num_estatus' => '1'),
                    array('cod_detalle' => '09', 'ind_nombre_detalle' => 'ADMINISTRACIÓN DE RECURSOS HUMANOS', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'JUDICIAL', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE CUENTAS BANCARIAS', 'ind_descripcion' => 'TIPO DE CUENTAS BANCARIAS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOCTA',
                'Det' => array(
                    array('cod_detalle' => 'AH', 'ind_nombre_detalle' => 'AHORRO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CO', 'ind_nombre_detalle' => 'CORRIENTE', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE APORTE', 'ind_descripcion' => 'TIPO DE APORTE', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOAPO',
                'Det' => array(
                    array('cod_detalle' => 'FI', 'ind_nombre_detalle' => 'FIDEICOMISO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PN', 'ind_nombre_detalle' => 'PAGO DE NOMINA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPOS DE DOCUMENTOS PERSONALES DEL EMPLEADO', 'ind_descripcion' => 'TIPOS DE DOCUMENTOS PERSONALES DEL EMPLEADO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'DOCUMENTOS',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'COPIA CEDULA IDENTIDAD', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'PARTIDA DE NACIMIENTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'ACTA DE MATRIMONIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'CARTA DE RESIDENCIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'CERTIFICADO DE SALUD', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'RIF', 'num_estatus' => '1'),
                    array('cod_detalle' => '07', 'ind_nombre_detalle' => 'CONSTANCIA DE BUENA CONDUCTA', 'num_estatus' => '1'),
                    array('cod_detalle' => '08', 'ind_nombre_detalle' => 'COPIA DE TITULO DE BACHILLER', 'num_estatus' => '1'),
                    array('cod_detalle' => '09', 'ind_nombre_detalle' => 'COPIA DEL TITULO UNIVERSITARIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'COPIA CERTIFICADOS CURSOS', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE ENTIDAD', 'ind_descripcion' => 'TIPO DE ENTIDAD', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOENTE',
                'Det' => array(
                    array('cod_detalle' => 'P', 'ind_nombre_detalle' => 'PRIVADA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'G', 'ind_nombre_detalle' => 'GUBERNAMENTAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'O', 'ind_nombre_detalle' => 'OTRA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'MERITOS DEL EMPLEADOS', 'ind_descripcion' => 'MERITOS DEL EMPLEADOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MERITO',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'AGRADECIMIENTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'FELICITACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'SOBRESALIENTE', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'NOTABLE', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'HONOR AL MERITO', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'ANIVERSARIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '07', 'ind_nombre_detalle' => 'RECONOCIMIENTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '08', 'ind_nombre_detalle' => 'BOTON DE HONOR', 'num_estatus' => '1'),
                    array('cod_detalle' => '09', 'ind_nombre_detalle' => 'ORDEN MERITO AL TRABAJO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'DEMERITOS DEL EMPLEADO', 'ind_descripcion' => 'DEMERITOS DEL EMPLEADO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'DEMERITO',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'AMONESTACION', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'FALTA GRAVE', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'LLAMADA DE ATENCION', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'SUSPENSION', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO MERITO DEMERITO', 'ind_descripcion' => 'TIPO MERITO DEMERITO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MERDEM',
                'Det' => array(
                    array('cod_detalle' => 'ME', 'ind_nombre_detalle' => 'MERITO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'DE', 'ind_nombre_detalle' => 'DEMERITO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'COLORES', 'ind_descripcion' => 'COLRES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'COLOR',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'AMARRILLO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'AZUL', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'ROJO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE REFERENCIAS ', 'ind_descripcion' => 'TIPO DE REFERENCIAS DEL EMPLEADO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOREF',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'LABORAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'PERSONAL', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'NIVEL DE CONOCIMIENTOS', 'ind_descripcion' => 'NIVEL DE CONOCIMIENTOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'NIVEL',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'BASICO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'INTERMEDIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'AVANZADO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPOS DE CURSOS', 'ind_descripcion' => 'TIPOS DE CURSOS', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOCURSO',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'CURSO', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'TALLER', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'CONFERENCIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'SIMPOSIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'CONGRESO', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'SEMINARIO', 'num_estatus' => '1'),
                    array('cod_detalle' => '07', 'ind_nombre_detalle' => 'CHARLA', 'num_estatus' => '1'),
                    array('cod_detalle' => '08', 'ind_nombre_detalle' => 'ENCUENTRO', 'num_estatus' => '1'),
                    array('cod_detalle' => '09', 'ind_nombre_detalle' => 'PROGRAMA DE FORMACIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'ENTRENAMIENTO', 'num_estatus' => '1'),
                    array('cod_detalle' => '11', 'ind_nombre_detalle' => 'FORO', 'num_estatus' => '1'),
                    array('cod_detalle' => '12', 'ind_nombre_detalle' => 'JORNADA', 'num_estatus' => '1'),
                    array('cod_detalle' => '13', 'ind_nombre_detalle' => 'INDUCCIÓN', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'COLEGIOS DE PROFESIONALES', 'ind_descripcion' => 'COLEGIOS DE PROFESIONALES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'COLEGIOS',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'CONTADORES', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'MEDICOS', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'INGENIEROS', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'ABOGADOS', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'ECONOMISTAS', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'MOTIVO DE AUSENCIA', 'ind_descripcion' => 'Motivo de ausencia para solicitar permisos en Recursos Humanos', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MOTAUS',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Matrimonio', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Enfermedad', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Accidente', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Fallecimiento de familiar', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Comparecencia ante la autoridad', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Licencia de Paternidad', 'num_estatus' => '1'),
                    array('cod_detalle' => '7', 'ind_nombre_detalle' => 'Prenatal', 'num_estatus' => '1'),
                    array('cod_detalle' => '8', 'ind_nombre_detalle' => 'Postnatal', 'num_estatus' => '1'),
                    array('cod_detalle' => '9', 'ind_nombre_detalle' => 'Permiso de lactancia', 'num_estatus' => '1'),
                    array('cod_detalle' => '10', 'ind_nombre_detalle' => 'Asistencia a seminario conferencia o congreso', 'num_estatus' => '1'),
                    array('cod_detalle' => '11', 'ind_nombre_detalle' => 'Beca', 'num_estatus' => '1'),
                    array('cod_detalle' => '12', 'ind_nombre_detalle' => 'Enfermedad de familiar', 'num_estatus' => '1'),
                    array('cod_detalle' => '13', 'ind_nombre_detalle' => 'Accidente de familiar', 'num_estatus' => '1'),
                    array('cod_detalle' => '14', 'ind_nombre_detalle' => 'Estudios', 'num_estatus' => '1'),
                    array('cod_detalle' => '15', 'ind_nombre_detalle' => 'Exámenes', 'num_estatus' => '1'),
                    array('cod_detalle' => '16', 'ind_nombre_detalle' => 'Diligencia', 'num_estatus' => '1'),
                    array('cod_detalle' => '17', 'ind_nombre_detalle' => 'Actividades Deportivas', 'num_estatus' => '1'),
                    array('cod_detalle' => '18', 'ind_nombre_detalle' => 'Asistencia a cursos', 'num_estatus' => '1'),
                    array('cod_detalle' => '19', 'ind_nombre_detalle' => 'Otro', 'num_estatus' => '1'),
                    array('cod_detalle' => '20', 'ind_nombre_detalle' => 'Auditoría', 'num_estatus' => '1'),
                    array('cod_detalle' => '21', 'ind_nombre_detalle' => 'Diligencia CES', 'num_estatus' => '1'),
                    array('cod_detalle' => '22', 'ind_nombre_detalle' => 'Cita médica', 'num_estatus' => '1'),
                    array('cod_detalle' => '23', 'ind_nombre_detalle' => 'Reposo Médico', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE AUSENCIA', 'ind_descripcion' => 'Tipo de ausencia para solicitar permisos en Recursos Humanos', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPAUS',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Inasistencia', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Entrada Tarde ', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Salida Temporal', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Salida Temprana', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Permiso', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE ACCIONES', 'ind_descripcion' => 'TIPO DE ACCION DE CONTROL DE NIVELACIONES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOACCION',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'AJUSTE SALARIAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'AJUSTE DE ESCALA', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'CAMBIO DE POSICIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'MERITO', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'PROMOCIÓN', 'num_estatus' => '1'),
                    array('cod_detalle' => '06', 'ind_nombre_detalle' => 'MOVIMIENTO ENTRE DEPENDENCIAS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'ET', 'ind_nombre_detalle' => 'ENCARGADURIA TEMPORAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CP', 'ind_nombre_detalle' => 'CAMBIO DE PASO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO CESE REINGRESO', 'ind_descripcion' => 'TIPO CESE REINGRESO', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOCR',
                'Det' => array(
                    array('cod_detalle' => 'C', 'ind_nombre_detalle' => 'CESE', 'num_estatus' => '1'),
                    array('cod_detalle' => 'R', 'ind_nombre_detalle' => 'REINGRESO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE SOLICITUD HCM', 'ind_descripcion' => 'tipos de solicitud en el proceso HCM', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'SOLHCM',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Reembolso', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Emisión', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'ESTADO HCM', 'ind_descripcion' => 'Estadoss de las solicitudes de beneficio hcm', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'ESTADOHCM',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Revisado', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Aprobado', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Anulado', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Modificado', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE VACACIÓN', 'ind_descripcion' => 'Tipo de vacación ', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPVAC',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'GOCE', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'INTERRUPCIÓN', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'RAMAS MEDICINA', 'ind_descripcion' => 'Ramas de la medicina', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'RAMAS',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Oftalmología', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Hematología', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Cardiología', 'num_estatus' => '1'),
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Enviado a Revisión', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Revisado', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Conformado', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Aprobado', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Anulado', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'CLASE INSTITUCIÓN', 'ind_descripcion' => 'Clase de institución', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'CLASEINST',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Pública', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Privada', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Sin fines de lucro', 'num_estatus' => '1'),
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Modificado', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Enviado a Revisión', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Revisado', 'num_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Conformado', 'num_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Aprobado', 'num_estatus' => '1'),
                    array('cod_detalle' => '7', 'ind_nombre_detalle' => 'Anulado', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE INSTITUCIÓN', 'ind_descripcion' => 'Tipo de institución HCM', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOINST',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Hospital', 'num_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Clínica', 'num_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Ambulatorio', 'num_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'CDI', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'MOTIVO DE PENSIÓN', 'ind_descripcion' => 'MOTIVO DE PENSIÓN DE LA PERSONA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MOTIPEN',
                'Det' => array(
                    array('cod_detalle' => 'INV', 'ind_nombre_detalle' => 'INVALIDEZ', 'num_estatus' => '1'),
                    array('cod_detalle' => 'INC', 'ind_nombre_detalle' => 'INCAPACIDAD', 'num_estatus' => '1'),
                    array('cod_detalle' => 'FA', 'ind_nombre_detalle' => 'FALLECIMIENTO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE PENSIÓN', 'ind_descripcion' => 'TIPO DE PENSIÓN DE LA PERSONA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOPEN',
                'Det' => array(
                    array('cod_detalle' => 'INV', 'ind_nombre_detalle' => 'INVALIDEZ', 'num_estatus' => '1'),
                    array('cod_detalle' => 'SOB', 'ind_nombre_detalle' => 'SOBREVIVIENTE', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'REQUISITOS UTILES', 'ind_descripcion' => 'REQUISITOS PARA OPTAR UTILES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'REQUTILES',
                'Det' => array(
                    array('cod_detalle' => 'CE', 'ind_nombre_detalle' => 'CONSTANCIA DE ESTUDIOS', 'num_estatus' => '1'),
                    array('cod_detalle' => 'CI', 'ind_nombre_detalle' => 'CONSTANCIA DE INSCRIPCIÓN', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO ASIGNACIÓN UTILES', 'ind_descripcion' => 'TIPO DE ASIGNACION DE UTILES BENEFICIO DE UTILES ESCOLARES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOASGUTI',
                'Det' => array(
                    array('cod_detalle' => 'AD', 'ind_nombre_detalle' => 'ASIGNACIÓN DIRECTA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'AF', 'ind_nombre_detalle' => 'ASIGNACIÓN POR FACTURAS', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE PAGO NOMINA', 'ind_descripcion' => 'TIPO DE PAGO NOMINA', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPOPAGO',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'ABONO EN CUENTA', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'CHEQUE', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'EFECTIVO', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'TRANSFERENCIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'CHEQUE GERENCIA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'MODALIDAD DE LA CAPACITACIONES', 'ind_descripcion' => 'MODALIDAD DE LA CAPACITACIONES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MODACAPAC',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'PRESENCIAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '02', 'ind_nombre_detalle' => 'SEMIPRESENCIAL', 'num_estatus' => '1'),
                    array('cod_detalle' => '03', 'ind_nombre_detalle' => 'A DISTANCIA', 'num_estatus' => '1'),
                    array('cod_detalle' => '04', 'ind_nombre_detalle' => 'INTERNET', 'num_estatus' => '1'),
                    array('cod_detalle' => '05', 'ind_nombre_detalle' => 'OTROS', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'ORIGEN DE LA CAPACITACION', 'ind_descripcion' => 'ORIGEN DE LAS CAPACITACIONES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'ORIGCAP',
                'Det' => array(
                    array('cod_detalle' => 'EXT', 'ind_nombre_detalle' => 'EXTERNO', 'num_estatus' => '1'),
                    array('cod_detalle' => 'INT', 'ind_nombre_detalle' => 'INTERNO', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'PASOS GRADO SALARIAL', 'ind_descripcion' => 'PASOS GRADO SALARIAL', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'PASOS',
                'Det' => array(
                    array('cod_detalle' => 'NA', 'ind_nombre_detalle' => 'NO APLICA', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PA', 'ind_nombre_detalle' => 'PASO A', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PB', 'ind_nombre_detalle' => 'PASO B', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PC', 'ind_nombre_detalle' => 'PASO C', 'num_estatus' => '1'),
                    array('cod_detalle' => 'PD', 'ind_nombre_detalle' => 'PASO D', 'num_estatus' => '1'),
                    array('cod_detalle' => '2E23', 'ind_nombre_detalle' => 'PASO E', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE RETENCIONES JUDICIALES', 'ind_descripcion' => 'TIPO DE RETENCIONES JUDICIALES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'RJUDICIAL',
                'Det' => array(
                    array('cod_detalle' => '01', 'ind_nombre_detalle' => 'OBLIGACIÓN ALIMENTARIA', 'num_estatus' => '1'),
                )
            ),
            array('ind_nombre_maestro' => 'TIPO DE DESCUENTOS', 'ind_descripcion' => 'TIPO DE DESCUENTOS RETENCIONES JUDICIALES', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '11', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'TIPODESC',
                'Det' => array(
                    array('cod_detalle' => 'P', 'ind_nombre_detalle' => 'PORCENTUAL', 'num_estatus' => '1'),
                    array('cod_detalle' => 'M', 'ind_nombre_detalle' => 'MONTO', 'num_estatus' => '1'),
                )
            )
        );
        return $a005_miscelaneo_maestro;
    }


    public function metDataMiscelaneos()
    {
        $a005_miscelaneo_maestro = array_merge(
            $this->metMiscelaneosRH()
        );
        return $a005_miscelaneo_maestro;
    }
}
