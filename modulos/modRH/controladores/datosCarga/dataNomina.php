<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
trait dataNomina
{
    public function metDataProcesos()
    {
        $nm_b003_tipo_proceso = array(
            array('cod_proceso' => 'PRQ', 'ind_nombre_proceso' => 'PRIMERA QUINCENA', 'num_flag_adelanto' => '0', 'ind_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'FIN', 'ind_nombre_proceso' => 'FIN DE MES', 'num_flag_adelanto' => '0', 'ind_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'GPS', 'ind_nombre_proceso' => 'GARANTIA PRESTACIONES SOCIALES', 'num_flag_adelanto' => '0', 'ind_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'GPA', 'ind_nombre_proceso' => 'GARANTIA PRESTACIONES SOCIALES DIAS ADICIONALES', 'num_flag_adelanto' => '0', 'ind_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'RTA', 'ind_nombre_proceso' => 'RETROACTIVO', 'num_flag_adelanto' => '0', 'ind_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'RPS', 'ind_nombre_proceso' => 'RETROACTIVO GARANTIA PRESTACIONES SOCIALES', 'num_flag_adelanto' => '0', 'ind_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1'),
            array('cod_proceso' => 'RPA', 'ind_nombre_proceso' => 'RETROACTIVO GARANTIA PRESTACIONES SOCIALES DIAS ADICIONALES', 'num_flag_adelanto' => '0', 'ind_estatus' => '1', 'fk_a018_num_seguridad_usuario' => '1')
        );
        return $nm_b003_tipo_proceso;
    }

    public function metDataConceptos()
    {
        $nm_b002_concepto = array(
            array('cod_concepto' => '0001','ind_descripcion' => 'SUELDO BASICO','ind_impresion' => 'SUELDO BASICO','num_orden_planilla' => '1','ind_formula' => '	$this->atMonto = $this->metSueldoBasico();
	$this->atCantidad = 1;','ind_abrebiatura' => 'SB','num_flag_automatico' => '1','num_flag_bono' => '0','num_flag_incidencia' => '0','fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I'),
            array('cod_concepto' => '0002','ind_descripcion' => 'DIFERENCIA DE SUELDO BASICO','ind_impresion' => 'DIFERENCIA DE SUELDO BASICO','num_orden_planilla' => '1','ind_formula' => '	$this->atMonto = $this->metDiferenciaSueldoBasico();
	$this->atCantidad = 1;','ind_abrebiatura' => 'DSB','num_flag_automatico' => '1','num_flag_bono' => '0','num_flag_incidencia' => '0','fk_a018_num_seguridad_usuario' => '1','cod_detalle' => 'I')
        );
        return $nm_b002_concepto;
    }

}
