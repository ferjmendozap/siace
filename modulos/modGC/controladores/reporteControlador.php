<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/


class reporteControlador extends Controlador
{
    private $atReporteModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atReporteModelo = $this->metCargarModelo('reporte');
        $this->atProyectoContratoModelo = $this->metCargarModelo('proyectoContrato');
    }



    public function metIndex()
    {

    }


    public function metProyectosPdf()
    {

        $proveedor=intval($_GET['proveedor']);
        $desde= $_GET['desde'] ;
        $hasta= $_GET['hasta'] ;
        $estatus= $_GET['estatus'] ;
        $anio= $_GET['anio'] ;



        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');

        $this->metObtenerLibreria('cabeceraReporte','modGC');
        $pdf=new pdfReporteProyectoContrato('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('CORRELATIVO'), 1, 0, 'C', 1);
        $pdf->Cell(70, 5, 'PROVEEDOR', 1, 0, 'C', 1);
        $pdf->Cell(55, 5, 'OBJETO DEL CONTRATO', 1, 0, 'C', 1);
        $pdf->Cell(18, 5, 'MONTO', 1, 0, 'C', 1);
        $pdf->Cell(28, 5, 'ESTADO', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'DESDE', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'HASTA', 1, 1, 'C', 1);



        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
        $consulta= $this->atReporteModelo-> metGetProyectoPDF($proveedor,$desde,$hasta,$estatus,$anio);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)
        {
         //   $consulta[$i]['num_monto_contrato']= money_format('%.2n', $consulta[$i]['num_monto_contrato']);

            $pdf->SetWidths(array(5,25,70,55,18,28 ,25,25));
            $pdf->SetAligns(array('C' ,'C','C','C','C','C','C' ,'C'));
            $pdf->Cell(5,5); //
            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['ind_correlativo']),
                utf8_decode($consulta[$i]['proveedor']),
                utf8_decode($consulta[$i]['ind_objeto_contrato']),
                utf8_decode($consulta[$i]['num_monto_contrato']),
                utf8_decode($consulta[$i]['ind_nombre_detalle']),
                utf8_decode($consulta[$i]['fec_desde']),
            utf8_decode( $consulta[$i]['fec_hasta'])
            ));
        }
        $pdf->Output();

    }

    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }


    public function metContratosReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );


        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );


        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);


        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());


        $this->atVista->metRenderizar('ListarContratosReporte' );


    }

    public function metContratosDetallesReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
            'materialSiace/App'
        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );


        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );


        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);


        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());


        $this->atVista->metRenderizar('ListarContratosDetallesReporte' );


    }

    public function metProyectoContratosReporte()
    {

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
        );


        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );


        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);



        $this->atVista->assign('_EstatusPost', $this->atProyectoContratoModelo->metGetEstatus());

        $this->atVista->metRenderizar('ListarProyectoContratosReporte' );


    }

    public function metContratosReportePdf()
    {


        $proveedor=intval($_GET['proveedor']);
        $desde= $_GET['desde'] ;
        $hasta= $_GET['hasta'] ;

        $anio= $_GET['anio'] ;

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
        $formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }

        if(!empty($formAlphaNum)) {
            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                    $validacion[$tituloAlphaNum] = $valorAlphaNum;
                } else {
                    $validacion[$tituloAlphaNum] = 'error';
                }
            }
        }

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraReporte','modGC');
        $pdf=new pdfReporteContrato('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);
        $pdf->Cell(5, 5,utf8_decode('N°'), 1, 0, 'C', 1);
        $pdf->Cell(25, 5, utf8_decode('CORRELATIVO'), 1, 0, 'C', 1);
        $pdf->Cell(80, 5, 'PROVEEDOR', 1, 0, 'C', 1);
        $pdf->Cell(60, 5, 'OBJETO DEL CONTRATO', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'MONTO', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'DESDE', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'HASTA', 1, 1, 'C', 1);


        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
        $consulta= $this->_ReporteModelo-> metGetReporteContrato($proveedor,$desde,$hasta, $anio);

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)
        {
          //  $consulta[$i]['num_monto_contrato']= money_format('%.2n', $consulta[$i]['num_monto_contrato']);

            $pdf->SetWidths(array(5,25,80,60,25,25,25 ));
            $pdf->SetAligns(array('C' ,'C','C','C','C','C','C' ));
            $pdf->Cell(5,5); //
            $pdf->Row(array($i+1,
                utf8_decode($consulta[$i]['ind_correlativo_contrato']),
                utf8_decode($consulta[$i]['ind_nombre1']),
                utf8_decode($consulta[$i]['ind_objeto_contrato']),
                utf8_decode($consulta[$i]['num_monto_contrato']),
                utf8_decode($consulta[$i]['fec_desde']),
                utf8_decode($consulta[$i]['fec_hasta'])
            ));
        }



        $pdf->Output();

    }

    public function metContratosDetallesReportePdf()
    {


        $proveedor=intval($_GET['proveedor']);
        $desde= $_GET['desde'] ;
        $hasta= $_GET['hasta'] ;

        $anio= $_GET['anio'] ;

        #Recibiendo las variables del formulario que utiliza la clase vehículo

        $formInt=$this->metObtenerInt('form','int');

        $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
        $formTxt=$this->metObtenerTexto('form','txt');

        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }

        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }

        if(!empty($formAlphaNum)) {
            foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                    $validacion[$tituloAlphaNum] = $valorAlphaNum;
                } else {
                    $validacion[$tituloAlphaNum] = 'error';
                }
            }
        }

        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraReporte','modGC');
        $pdf=new pdfReporteContrato('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(200, 200, 200); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(5, 5);

        $pdf->Cell(25, 5, utf8_decode('CORRELATIVO'), 1, 0, 'C', 1);
        $pdf->Cell(60, 5, 'OBJETO DEL CONTRATO', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'PARTIDA', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'APROBADO', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'AJUSTADO', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'COMPROMETIDO', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'CAUSADO', 1, 0, 'C', 1);
        $pdf->Cell(25, 5, 'PAGADO', 1, 1 ,'C', 1);


        $pdf->SetFont('Arial', '', 7);

        $this->_ReporteModelo = $this->metCargarModelo('reporte');
        $consulta= $this->_ReporteModelo-> metGetReporteContrato($proveedor,$desde,$hasta, $anio);




        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(900, 900, 900); $pdf->SetTextColor(0, 0, 0);

        for($i=0; $i< count($consulta); $i++)
        {

            $respuesta=$this->atProyectoContratoModelo->metPresupuestoContrato($consulta[$i]['pk_num_registro_contrato']);




            $pdf->SetWidths(array( 25,60,25,25,25,25,25,25 ));
            $pdf->SetAligns(array('C' ,'C','C','C','C','C','C' ,'C','C'));


            for($x=0;$x<count($respuesta);$x++){
                $pdf->Cell(5,5); //

                if(!$respuesta[$x]['Comprometido']){
                    $respuesta[$x]['Comprometido']="0.00";
                }
                if(!$respuesta[$x]['Causado']){
                    $respuesta[$x]['Causado']="0.00";
                }
                if(!$respuesta[$x]['Pagado']){
                    $respuesta[$x]['Pagado']="0.00";
                }
                if($x>0){


                    $pdf->Row(array(
                        utf8_decode(""),
                        utf8_decode(""),
                        utf8_decode($respuesta[$x]['ind_partida']),
                        utf8_decode(number_format($consulta[$i]['num_monto_contrato'], 2, '.', '')),
                        utf8_decode(number_format($consulta[$i]['num_monto_contrato'], 2, '.', '')),
                        utf8_decode(number_format($respuesta[$x]['Comprometido'], 2, '.', '')),
                        utf8_decode(number_format($respuesta[$x]['Causado'], 2, '.', '')),
                        utf8_decode(number_format($respuesta[$x]['Pagado'], 2, '.', ''))

                    ));

                }else{

                    $pdf->Row(array(
                        utf8_decode($consulta[$i]['ind_correlativo_contrato']),
                        utf8_decode($consulta[$i]['ind_nombre1']),
                        utf8_decode($respuesta[$x]['ind_partida']),
                        utf8_decode(number_format($consulta[$i]['num_monto_contrato'], 2, '.', '')),
                        utf8_decode(number_format($consulta[$i]['num_monto_contrato'], 2, '.', '')),
                        utf8_decode(number_format($respuesta[$x]['Comprometido'], 2, '.', '')),
                        utf8_decode(number_format($respuesta[$x]['Causado'], 2, '.', '')),
                        utf8_decode(number_format($respuesta[$x]['Pagado'], 2, '.', ''))

                    ));

                }



            }


        }



        $pdf->Output();

    }


    public function metProyectoContratoPdf()
    {
        $id=intval($_GET['idProyecto']);
        $consulta= $this->atReporteModelo-> metGetProyectoId($id);


        $titulo=strtoupper($consulta[0]['tipoContrato']);
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('cabeceraReporte','modGC');
        $pdf=new pdfReporteGestion('P','mm','A4');
        $pdf->AliasNbPages();
        $pdf->AddPage();


        $pdf->SetFont('Arial', 'B', 8);
        if($consulta[0]['fk_a006_num_estado_contrato']==5){
            $pdf->Cell(0, 0, utf8_decode($consulta[0]['ind_correlativo_contrato']), 0, 0, 'R', 0);
        }else{
            $pdf->Cell(0, 0, utf8_decode($consulta[0]['ind_correlativo']), 0, 0, 'R', 0);
        }

        $pdf->Ln(10);
        $pdf->Cell(0, 0, utf8_decode($titulo), 0, 0, 'C', 0);
        $pdf->Ln(3);
        $pdf->SetFont('Arial', '', 8);
        $pdf->MultiCell(0,5,strip_tags(html_entity_decode(utf8_decode($consulta[0]['ind_cuerpo']))),0,'J');
        $pdf->Cell(87,32, " ________________________ " ,0, 0, 'C', 0);
        $pdf->Cell(120,35, " ________________________ " ,0, 0, 'C', 0);
        $pdf->Ln(4);
        $pdf->Cell(87,34, "EL CONTRATANTE ", 0, 0, 'C', 0);
        $pdf->Cell(120,35, "EL CONTRATADO ", 0, 0, 'C', 0);
        $pdf->Output();

    }


}

