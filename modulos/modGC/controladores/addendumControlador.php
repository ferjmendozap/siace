<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/

class addendumControlador extends Controlador
{
    private $atAddendumModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atAddendumModelo = $this->metCargarModelo('addendum');
        $this->atContratoModelo = $this->metCargarModelo('contrato');
        $this->atProyectoContratoModelo = $this->metCargarModelo('proyectoContrato');
    }
	


    #Metodo Index del controlador Contrato
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );


        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_addendumPost',$this->atAddendumModelo->metGetAddendums(0));

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarAddendum');
    }
    public function metRevisarAddendum()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );

        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_addendumPost',$this->atAddendumModelo->metGetAddendums(1));

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarAddendumRevisar');
    }

    public function metAprobarAddendum()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );

        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }

        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_addendumPost',$this->atAddendumModelo->metGetAddendums(3));

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarAddendumAprobar');
    }


    public function metConsultarContrato()
    {


     $idContrato=$this->metObtenerInt('idContrato');
     $consulta = $this->atAddendumModelo->metGetContratoAddendumId($idContrato);
     $datos=[];
     $datos['objeto'] = $consulta[0]['ind_objeto_contrato'];
     $datos['monto']=$consulta[0]['num_monto_contrato'];
     $datos['correlativo']=$consulta[0]['ind_correlativo_contrato'];


     echo json_encode($datos);
     exit;

    }


    public function metModificarAddendum()
    {

        $id=$this->metObtenerInt('id');
        $valido=$this->metObtenerInt('valido');

        $Accion=array( "accion" => "modificar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if($valido==1) {


            $Estatus=$this->metObtenerAlphaNumerico('Estatus');
            $idContrato=$this->metObtenerInt('Contrato');
            $idComprometido=$this->metObtenerInt('idComprometido');
            if($Estatus=='PR'){
                $NEstatus='RV';
                $this->atAddendumModelo->metUpdateAddendum($idComprometido,$NEstatus);
            }else if($Estatus=='RV'){
                $NEstatus='AP';
                $this->atAddendumModelo->metUpdateAddendum($idComprometido,$NEstatus);
                $respuesta=$this->atProyectoContratoModelo->metGetPresupuestoDet($idContrato);
                for($i=0;$i < count($respuesta);$i++){

                    $this->atProyectoContratoModelo->metContratoComprometer ( $respuesta[$i]['num_monto'],$idContrato,$respuesta[$i]['pk_num_presupuesto_det']);


                }
                 $this->atProyectoContratoModelo->metUpdateContratoPresupuesto($idContrato);

            }



                $respuesta = $this->atAddendumModelo->metGetContratoAddendumId($idContrato);
                $datos=[];
                $datos['status'] = 'modificar';
                $datos['idComprometido']=$idComprometido;
                $datos['Estatus']=$NEstatus;
                $datos['Correlativo']=$respuesta[0]['ind_correlativo_contrato'];
                $datos['Objeto']=$respuesta[0]['ind_objeto_contrato'];
                $datos['Monto']=$respuesta[0]['num_monto_contrato'];

                echo json_encode($datos);
                exit;





        }

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->assign('_addendumBDPost',$this->atAddendumModelo->metGetContratoAddendumId($id));

        $this->atVista->assign('_addendumPost',$this->atAddendumModelo->metGetcontratoAddendum());

        $this->atVista->assign('_Acciones', $Accion);
        $this->atVista->metRenderizar('RegistrarAddendum',"modales");




    }

    public function metRegistrarAddendum()
    {


        $valido=$this->metObtenerInt('valido');
        $id_valor=$this->metObtenerInt('idPost');

        $_Accion=$this->metObtenerAlphaNumerico('Accion');

        $Accion=array( "accion" => $_Accion);

        // asignamos a la variable moviento el valor que le corresponde en el miscelaneo en este caso el 1, de  preparado.
        $movimiento="3";
        $Estatus="2";

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
        );

        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'ckeditor/ckeditor',
            'ckeditor/adapters/jquery',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            '../javascript/modGC/inputValidacion/valido',


        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',

        );

        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            $formMonto=$this->metObtenerMonto('form','monto');

            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


            $desde=$this->metFormatoFecha($_POST['desde']);
            $desde=trim($desde." 00:00:00");

            $hasta=$this->metFormatoFecha($_POST['hasta']);
            $hasta=trim($hasta." 00:00:00");

            $entrada=$this->metFormatoFecha($_POST['entrada']);
            $entrada=trim($entrada." 00:00:00");

            $Correlativo=1;


            if($validacion['accion']=='revisar'){

                $validacion['contrato']=str_replace(",", ".", $validacion['contrato']);
                $this->atProyectoContratoModelo->metUpdateProyectoEstatus($validacion['idAplicable'],$validacion['objeto'],$validacion['contrato'],$validacion['disponibilidad'],$validacion['idSolicitado'],$validacion['idTipoContrato'],$validacion['formaPago'],$validacion['idDocumento'],$validacion['CodRepresentante'],htmlspecialchars_encode($validacion['cuerpo']),$Estatus,$validacion['idRegistro'],$movimiento,$validacion['guia']);

            }


        }else {

            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarJs($js);

            $this->atVista->metCargarCssComplemento($complementosCss);

            $this->atVista->assign('_FormaPagoPost', $this->atProyectoContratoModelo->atMiscelaneoModelo->metMostrarSelect('TIPOPAGO'));

            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());
            $consulta=$this->atProyectoContratoModelo->metGetProyecto($id_valor);
            $this->atVista->assign('_ProyectoContratoPost',$consulta);
            $selectRif=explode("-",strtolower ($consulta[0]['ind_documento_fiscal']));

            $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());
            $this->atVista->assign('_PartidasSeleccionadasPost',$this->atProyectoContratoModelo->metObtenerPartidasSeleccionadas($id_valor));
            $this->atVista->assign('_AplicablePost',$this->atProyectoContratoModelo->metMostrarSelectAplicable($consulta[0]['fk_gcc003_num_tipo_contrato']));
            $this->atVista->assign('_TipoContratoPost', $this->atProyectoContratoModelo->metMostrarSelectTipo($consulta[0]['fk_a004_dependencia']));
            $this->atVista->metRenderizar('RegistrarAddendum');

        }

    }

    public function metAnularAddendum()
    {

        $id_valor=$this->metObtenerInt('id');

        $estatus=$this->atAddendumModelo->metGetAddendumEstatusId($id_valor);

        if($estatus['fk_a006_num_estado_contrato']==1){
            $this->atAddendumModelo->metAnularAddendum($id_valor,'6');

        }else if ($estatus['fk_a006_num_estado_contrato']>2&&$estatus['fk_a006_num_estado_contrato']<6){
            $estatus['fk_a006_num_estado_contrato']=$estatus['fk_a006_num_estado_contrato']-2;
            $this->atAddendumModelo->metAnularAddendum($id_valor,$estatus['fk_a006_num_estado_contrato']);
        }else if($estatus['fk_a006_num_estado_contrato']==6){

            $this->atProyectoContratoModelo->metEliminarProyecto($id_valor);

        }


    }



    public function metSubirAddendum()
    {

        $valido=$this->metObtenerInt('valido');



        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');


            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }

            if($validacion['estatus']==1){

                $validacion['estatus']=3;

            }else if ($validacion['estatus']==3){

                $validacion['estatus']=5;

            }


            // asignamos a la variable moviento el valor que le corresponde en el miscelaneo en este método le sumaremos 1 al estatus.
            $movimiento=$validacion['estatus']+1;


            $Contrato=$this->atAddendumModelo->metGetRelacionContrato($validacion['idRegistro']);

            $guia=$this->atProyectoContratoModelo->metConsultarAddendums(date('Y'), $Contrato['fk_gcb001_addendum']);
            $guia++;


            // EN CASO DE APROBACIÓN
            if($validacion['estatus']==5) {

                $CorrelativoContrato = $this->metCorrelativoAddendum($Contrato['fk_gcb001_addendum'] ,$guia);

            }else {

                $CorrelativoContrato ="0";

            }

            $this->atProyectoContratoModelo->metUpdateProyectoEstatus($validacion['idRegistro'],$validacion['estatus'],$movimiento,$validacion['cuerpo'],$CorrelativoContrato);





            $MontoAjustado=$this->atAddendumModelo->metConsultarPartidas($validacion['idRegistro']);
            $MontoOriginal=$this->atAddendumModelo->metConsultarPartidas($Contrato['fk_gcb001_addendum']);

            for ($i = 0; $i < count($MontoOriginal) ; $i++) {


                if($MontoOriginal[$i]['num_monto_ajustado']<$MontoAjustado[$i]['num_monto_ajustado']){

                    $this->atAddendumModelo->metUpdatePartidas( $MontoOriginal[$i]['pk_num_partidas'],$MontoAjustado[$i]['num_monto_ajustado']);
                }

            }
            echo json_encode($validacion['idRegistro']);
            exit;

        }

    }

    protected function metCorrelativoAddendum($solicitado,$id)
    {
        $Correlativo="A-0".$solicitado."-".str_pad($id, '3', '0', STR_PAD_LEFT)."-".date('Y');


        return $Correlativo;

    }


    #Metodo parac cargar el listado de los addendums relacionados con un contrato
    public function metListarAddendums()
    {

        $id_contrato=$_POST['idContrato'];
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',


        );


        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);


        $this->atVista->assign('addendums', $this->atAddendumModelo->metGetContratoAddendumsId($id_contrato));
        $this->atVista->assign('_id_contrato', $id_contrato);

        $this->atVista->metRenderizar('ListarAddendumsGC','modales');
    }



}
