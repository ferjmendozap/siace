<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/

class proyectoContratoControlador extends Controlador
{
    private $atProyectoContratoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atProyectoContratoModelo = $this->metCargarModelo('proyectoContrato');
    }
	


    #Metodo Index del controlador proyectoContrato
    public function metIndex()
    {


    }

    #Metodo parac cargar el listado de los Proyecto de Contrato
    public function metListarProyectoContrato()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'ckeditor/ckeditor',
            'ckeditor/adapters/jquery',

         );

        $this->atVista->assign('_EstatusPost', $this->atProyectoContratoModelo->metGetEstatus());
        
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_ProyectoContratoPost', $this->atProyectoContratoModelo->metGetProyectoContrato('0'));

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarProyectoContrato');
    }



    // Método que permite listar empleados de acuerdo a la busqueda
    public function metFiltroProyecto()
    {

        $filtro_estatus = intval($_POST['filtro_estatus']);



        $filtro_dependencia = $_POST['filtro_dependencia'];

        $filtro_proveedor = $_POST['filtro_proveedor'];

        $resultados= $this->atProyectoContratoModelo->metFiltroProyecto($filtro_estatus,$filtro_dependencia,$filtro_proveedor);

        echo json_encode($resultados);

    }



    #Metodo parac cargar el listado de los Proyecto de Contrato por redactar
    public function metListarRedaccionProyecto()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'ckeditor/ckeditor',
            'ckeditor/adapters/jquery',

        );



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_ProyectoContratoPost',$this->atProyectoContratoModelo->metGetProyectoContrato('1'));

        $this->atVista->metRenderizar('ListarRedaccionProyecto');
    }


    #Metodo parac cargar el listado de las obligaciones generadas por un proyecto de contrato
    public function metListarProyectoObligacion()
    {

        $id_contrato=$_POST['idContrato'];
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',


        );


        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);

        $valor=$this->atProyectoContratoModelo->metListarObligacion($id_contrato);
        $this->atVista->assign('_ProyectoContratoPost',  $valor);



        if(isset($valor[0]['fk_cpd001_obligacion'])){

            $i=0;
            $TotalComprometido=0;
            $TotalCausado=0;
            $TotalPagado=0;


            while (isset($valor[$i]['fk_cpd001_obligacion'])){


                $Comprometido=$this->atProyectoContratoModelo->metMontosObligacion ($valor[$i]['fk_cpd001_obligacion'],'CO');
                $Causado=$this->atProyectoContratoModelo->metMontosObligacion ($valor[$i]['fk_cpd001_obligacion'],'CA');
                $Pagado=$this->atProyectoContratoModelo->metMontosObligacion ($valor[$i]['fk_cpd001_obligacion'],'PA');

                if(isset($Comprometido[0]['suma'])){

                    $TotalComprometido=$TotalComprometido+$Comprometido[0]['suma'];

                }
                if(isset($Causado[0]['suma'])){

                    $TotalCausado=$TotalCausado+$Causado[0]['suma'];

                }
                if(isset($Pagado[0]['suma'])){

                    $TotalPagado=$TotalPagado+$Pagado[0]['suma'];

                }

                $i++;


            }

            $Montos=array( "Comprometido" =>  $TotalComprometido ,"Causado" => $TotalCausado ,"Pagado" => $TotalPagado );
            
        }else{ $Montos=array( "Comprometido" => 0 ,"Causado" => 0 ,"Pagado" => 0 );}

        $this->atVista->assign('Montos_',$Montos);
        $this->atVista->assign('_id_contrato', $id_contrato);
        $this->atVista->metRenderizar('ListarObligacionesGC','modales');
    }


    public function metListarOrdenes()
    {

        $id_contrato=$_POST['idContrato'];
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',


        );


        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);

        $valor=$this->atProyectoContratoModelo->metListarOrdenContratos($id_contrato);
        $this->atVista->assign('_ProyectoContratoPost',  $valor);

        $this->atVista->assign('ordenes',$valor);
        $this->atVista->assign('_id_contrato', $id_contrato);
        $this->atVista->metRenderizar('ListarOrdenesGC','modales');
    }


    #Metodo parac cargar el listado de los Proyecto de Contrato por revisar
    public function metListarRevisionProyecto()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',


        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_ProyectoContratoPost',$this->atProyectoContratoModelo->metGetProyectoContrato('2'));

        $this->atVista->metRenderizar('ListarRevisionProyecto');
    }
    #Metodo parac cargar el listado de los Proyecto de Contrato por  conformar
    public function metListarConformacionProyecto()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_ProyectoContratoPost',$this->atProyectoContratoModelo->metGetProyectoContrato('3'));

        $this->atVista->metRenderizar('ListarConformacionProyecto');
    }
    #Metodo parac cargar el listado de los Proyecto de Contrato por revisar
    public function metListarAprobacionProyecto()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',


        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
        );



        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('_ProyectoContratoPost',$this->atProyectoContratoModelo->metGetProyectoContrato('4'));

        $this->atVista->metRenderizar('ListarAprobacionProyecto');
    }

    #Metodo para cargar el listado de las partidas relacionadas con los contratos
    public function metListarPartidasContrato()
    {
        $idPartida=$_POST['id'];
        $idPartidas=array( "Partida" => $idPartida);
        $this->atVista->assign('_PartidasPost',$this->atProyectoContratoModelo->metListarPartida('error',date('Y'),'error'));
        $this->atVista->assign('_PartidasSeleccionadasPost',$idPartidas);
        $this->atVista->metRenderizar('ListarPartidasContrato','modales');
    }


    public function metActualizarTipoContrato()
    {

        $idSolicitado=$this->metObtenerInt('idSolicitado');

        $consulta= $this->atProyectoContratoModelo->metMostrarSelectTipo($idSolicitado);
        $resultado="<option value=''>Seleccione</option>";
        for($i=0;$i<count($consulta);$i++){
            $resultado.= "<option value=".$consulta[$i]['pk_num_tipo_contrato'].">".$consulta[$i]['ind_descripcion']."</option>";
        }



        echo json_encode($resultado);
        exit;

    }

    public function metActualizarAplicable()
    {

        $idTipoContrato=$this->metObtenerInt('idTipoContrato');

        $consulta= $this->atProyectoContratoModelo->metMostrarSelectAplicable($idTipoContrato);
        $resultado="<option value=''>Seleccione</option>";
        for($i=0;$i<sizeof($consulta);$i++){
            $resultado.= "<option value=".$consulta[$i]['pk_num_aplicable_contrato'].">".$consulta[$i]['ind_descripcion']."</option>";
        }



        echo json_encode($resultado);
        exit;

    }

    public function metVisualizarNombre()
    {

        $idTipoContrato=$this->metObtenerAlphaNumerico('idTipoDocumento');
        $idDocumento=$this->metObtenerAlphaNumerico('idDocumento');


        $consulta= $this->atProyectoContratoModelo->metMostrarNombre($idDocumento,$idTipoContrato);


        echo json_encode($consulta);
        exit;

    }
    public function metBuscarRepresentante()
    {


        $idDocumento=$this->metObtenerAlphaNumerico('idDocumento');


        $consulta= $this->atProyectoContratoModelo->metBuscarRepresentante($idDocumento);


        echo json_encode($consulta);
        exit;

    }

    public function metRegistrarProyecto()
    {

        $NumeroPartidas = $this->metObtenerInt('NPartidasSeleccionadas');
        $valido=$this->metObtenerInt('valido');
        $addendum=$this->metObtenerInt('idPost');
        $movimiento="1";


        $Accion=array( "accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
        );

        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'ckeditor/ckeditor',
            'ckeditor/adapters/jquery',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            '../javascript/modGC/inputValidacion/valido',


        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',

        );
        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            $formMonto=$this->metObtenerMonto('form','monto');
            $validacion['accion']=$this->metObtenerAlphaNumerico('accion');


            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


           $desde=$this->metFormatoFecha($_POST['desde']);
            $desde=trim($desde." 00:00:00");

            $hasta=$this->metFormatoFecha($_POST['hasta']);
            $hasta=trim($hasta." 00:00:00");

            $entrada=$this->metFormatoFecha($_POST['entrada']);
            $entrada=trim($entrada." 00:00:00");





        $validacion['contrato']=str_replace(",", ".", $validacion['contrato']);
        // Consultamos los proyectos registrados por año y dependencia para asignar correctamente un correlativo
        $guia=$this->atProyectoContratoModelo->metConsultarProyectoContratos(date('Y'),$validacion['idSolicitado']);
        $guia++;

        if($validacion['objeto']=="error"){


            $validacion=[];
            $validacion['status'] = 'error';
            $validacion['detalleERROR'] = " Campos objeto sin contenido";
            echo json_encode($validacion);
            exit;


        }
        // Ingresamos el proyecto a la bd y obtenemos su id
        $id=$this->atProyectoContratoModelo->metSetProyecto($validacion['idSolicitado'],$validacion['idTipoContrato'],$validacion['idAplicable'],$validacion['objeto'],$validacion['contrato'],$validacion['idDocumento'],$validacion['CodRepresentante'],$desde,$hasta,$entrada,$validacion['formaPago'],$validacion['lugarPago'], $validacion['obligacionesContrante'],$validacion['obligacionesContratado'],$validacion['observacion'],$movimiento,$validacion['guia'],intval($validacion['addendum']),intval($validacion['tipo_servicio']));

            if(is_array($id)){

                $validacion=[];
                $validacion['status'] = 'error';
                $validacion['detalleERROR'] = $id;
                echo json_encode($validacion);
                exit;

            }else{

            
         // Creamos el correlativo
         $Correlativo=$this->metCorrelativoProyecto($validacion['idSolicitado'],$guia);
         // Asignamos el correlativo
         $this->atProyectoContratoModelo->metAsignarCorrelativo($Correlativo,$id);


         $this->atProyectoContratoModelo->metEliminarPartidas ($id);

         if ($NumeroPartidas != 0) {
             for ($i = 1; $i <= $NumeroPartidas ; $i++) {

                 if( isset($validacion['monto'.$i])){
                     $respuesta=$this->atProyectoContratoModelo->metGetPresupuestoDetPartida($validacion['partidaDet'.$i]);
                     $validacion['monto'.$i]=str_replace(",", ".", $validacion['monto'.$i]);
                     $validacion['Ajustado'.$i]=str_replace(",", ".", $validacion['Ajustado'.$i]);
                    if($validacion['accion']=="registrar"){

                        $this->atProyectoContratoModelo->metAsignarPartidas( $validacion['partidaDet'.$i], $validacion['monto'.$i],$validacion['monto'.$i], $respuesta['pk_num_presupuesto_det'],$respuesta['fk_cbb004_num_plan_cuenta_pub20'],$respuesta['fk_cbb004_num_plan_cuenta_onco'],$id);

                    }else{

                        $this->atProyectoContratoModelo->metAsignarPartidas( $validacion['partidaDet'.$i], $validacion['monto'.$i],$validacion['Ajustado'.$i], $respuesta['pk_num_presupuesto_det'],$respuesta['fk_cbb004_num_plan_cuenta_pub20'],$respuesta['fk_cbb004_num_plan_cuenta_onco'],$id);

                    }

                 }

             }
         }

                $consulta=$this->atProyectoContratoModelo->metGetProyecto($id);
                $datos=[];
                $datos['status'] = 'creacion';
                $datos['idProyecto'] = $id;
                $datos['proveedor']=$consulta[0]['proveedor'];
                $datos['objeto']=$validacion['objeto'];
                $datos['estatus']=$consulta[0]['ind_nombre_detalle'];
                $datos['monto']=$validacion['contrato'];
                $datos['estado']=$consulta[0]['fk_a006_num_estado_contrato'];
                echo json_encode($datos);
                exit;

            }

        }else {

                $this->atVista->metCargarJsComplemento($complementosJs);
                $this->atVista->metCargarJs($js);

                $this->atVista->metCargarCssComplemento($complementosCss);
                $this->atVista->assign('_FormaPagoPost', $this->atProyectoContratoModelo->atMiscelaneoModelo->metMostrarSelect('TIPOPAGO'));
                $this->atVista->assign('listadoServicio', $this->atProyectoContratoModelo->atObligacionModelo->atServicioModelo->metServiciosListar());
            if(intval($addendum>0)){


                $Accion=array( "accion" => "addendum",);
                $this->atVista->assign('_Acciones', $Accion);
                $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());
                $consulta=$this->atProyectoContratoModelo->metGetProyecto($addendum);
                $this->atVista->assign('_ProyectoContratoPost',$consulta);


                $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());
                $this->atVista->assign('_PartidasSeleccionadasPost',$this->atProyectoContratoModelo->metObtenerPartidasSeleccionadas($addendum));
                $this->atVista->assign('_AplicablePost',$this->atProyectoContratoModelo->metMostrarSelectAplicable($consulta[0]['fk_gcc003_num_tipo_contrato']));
                $this->atVista->assign('_TipoContratoPost', $this->atProyectoContratoModelo->metMostrarSelectTipo($consulta[0]['fk_a004_dependencia']));

                $this->atVista->assign('_Addendum', $addendum);
                $this->atVista->metRenderizar('RegistrarRedaccionProyecto' );

            }else{
                $this->atVista->assign('_Acciones', $Accion);
                $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());
                $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

                $this->atVista->metRenderizar('RegistrarProyectoContrato' );


            }


        }

    }

    public function metRedactarProyecto()
    {
        $valido=$this->metObtenerInt('valido');
        $id_valor=$this->metObtenerInt('idPost');
        $_Accion=$this->metObtenerAlphaNumerico('Accion');

        $Accion=array( "accion" => $_Accion);

        // asignamos a la variable moviento el valor que le corresponde en el miscelaneo en este caso el 1, de  preparado.
        $movimiento="3";
        $Estatus="2";

        $js = array(
            'materialSiace/core/demo/DemoFormWizard'
        );

        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'ckeditor/ckeditor',
            'ckeditor/adapters/jquery',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            '../javascript/modGC/inputValidacion/valido',


        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
        );

        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            $formMonto=$this->metObtenerMonto('form','monto');

            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }


            $desde=$this->metFormatoFecha($_POST['desde']);
            $desde=trim($desde." 00:00:00");

            $hasta=$this->metFormatoFecha($_POST['hasta']);
            $hasta=trim($hasta." 00:00:00");

            $entrada=$this->metFormatoFecha($_POST['entrada']);
            $entrada=trim($entrada." 00:00:00");

            $Correlativo=1;


            if($validacion['accion']=='revisar'){
                
                $validacion['contrato']=str_replace(",", ".", $validacion['contrato']);
                $this->atProyectoContratoModelo->metUpdateProyectoEstatus($validacion['idAplicable'],$validacion['objeto'],$validacion['contrato'],$validacion['disponibilidad'],$validacion['idSolicitado'],$validacion['idTipoContrato'],$validacion['formaPago'],$validacion['idDocumento'],$validacion['CodRepresentante'],htmlspecialchars_encode($validacion['cuerpo']),$Estatus,$validacion['idRegistro'],$movimiento,$validacion['guia']);

            }
            
            
        }else {

            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarJs($js);

            $this->atVista->metCargarCssComplemento($complementosCss);

            $this->atVista->assign('_FormaPagoPost', $this->atProyectoContratoModelo->atMiscelaneoModelo->metMostrarSelect('TIPOPAGO'));
            $this->atVista->assign('listadoServicio', $this->atProyectoContratoModelo->atObligacionModelo->atServicioModelo->metServiciosListar());
            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());
            $consulta=$this->atProyectoContratoModelo->metGetProyecto($id_valor);
            $this->atVista->assign('_ProyectoContratoPost',$consulta);


            $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

            $partidas=$this->atProyectoContratoModelo->metObtenerPartidasSeleccionadas($id_valor);

            for ($i=0;$i<count($partidas);$i++){
                $tmp= $this->atProyectoContratoModelo-> metBuscarIdPartida($partidas[$i]['ind_partida']);
                $partidas[$i]['pk_num_partida_presupuestaria']=$tmp['pk_num_partida_presupuestaria'];

            }
            $this->atVista->assign('_PartidasSeleccionadasPost',$partidas);
            $this->atVista->assign('_AplicablePost',$this->atProyectoContratoModelo->metMostrarSelectAplicable($consulta[0]['fk_gcc003_num_tipo_contrato']));
            $this->atVista->assign('_TipoContratoPost', $this->atProyectoContratoModelo->metMostrarSelectTipo($consulta[0]['fk_a004_dependencia']));
            $this->atVista->metRenderizar('RegistrarRedaccionProyecto');

        }

    }





    public function metModificarProyecto()
    {

        $NumeroPartidas = $this->metObtenerInt('NPartidasSeleccionadas');
        $valido=$this->metObtenerInt('valido');

        $id_valor=$this->metObtenerInt('idPost');

        $accion=$this->metObtenerAlphaNumerico('accion');

        $Accion=array( "accion" => $accion,);
        // asignamos a la variable moviento el valor que le corresponde en el miscelaneo
        $movimiento="2";
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
        );

        $complementosJs = array(
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            '../javascript/modGC/inputValidacion/valido',

        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
        );

        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            $formMonto=$this->metObtenerMonto('form','monto');

            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }

           $consulta=$this->atProyectoContratoModelo->metGetProyecto($validacion['idRegistro']);



           if($consulta[0]['fk_gcc004_num_aplicable_contrato']!=$validacion['idAplicable']
           || $consulta[0]['ind_objeto_contrato']!=$validacion['objeto']                   || $consulta[0]['num_monto_contrato']!=$validacion['contrato']

           || $consulta[0]['fk_gcc003_num_tipo_contrato']!=$validacion['idTipoContrato']   || $consulta[0]['fk_a006_num_forma_pago']!=$validacion['formaPago']
           || $consulta[0]['fk_a003_proveedor']!=$validacion['idDocumento']                || $consulta[0]['fk_a003_representante']!=$validacion['CodRepresentante']
           || $consulta[0]['num_guia_contrato']!=$validacion['guia']
           ){

               $validacion['contrato']=str_replace(",", ".", $validacion['contrato']);



           $this->atProyectoContratoModelo->metUpdateProyecto($validacion['idAplicable'],$validacion['objeto'],$validacion['contrato'] ,$validacion['idSolicitado'],$validacion['idTipoContrato'],$validacion['formaPago'],$validacion['fk_a003_proveedor'],$validacion['CodRepresentante'],$validacion['cuerpo'],$validacion['idRegistro'],$movimiento,$validacion['guia']);


           }
           if($consulta[0]['fec_desde']!=$_POST['desde']  || $consulta[0]['fec_hasta']!=$_POST['hasta']  || $consulta[0]['fec_entrada']!=$_POST['entrada']
           || $consulta[0]['ind_obligaciones_contratante']!=$validacion['obligacionesContrante'] || $consulta[0]['ind_obligaciones_contratado']!=$validacion['obligacionesContratado']
           || $consulta[0]['ind_observacion']!=$validacion['observacion'] || $consulta[0]['ind_lugar_pago']!=$validacion['lugarPago'])
            {
              $desde=$this->metFormatoFecha($_POST['desde']);
              $desde=trim($desde." 00:00:00");

              $hasta=$this->metFormatoFecha($_POST['hasta']);
              $hasta=trim($hasta." 00:00:00");

              $entrada=$this->metFormatoFecha($_POST['entrada']);
              $entrada=trim($entrada." 00:00:00");

              $this->atProyectoContratoModelo->metUpdateProyectoDetalle($desde,$hasta,$entrada,$validacion['obligacionesContrante'],$validacion['obligacionesContratado'],$validacion['observacion'],$validacion['lugarPago'],$validacion['idRegistro']);
            }


            $this->atProyectoContratoModelo->metEliminarPartidas ($validacion['idRegistro']);

            if ($NumeroPartidas != 0) {


                for ($i = 1; $i <= $NumeroPartidas ; $i++) {

                    if( isset($validacion['monto'.$i])){

                        $respuesta=$this->atProyectoContratoModelo->metGetPresupuestoDetPartida($validacion['partidaDet'.$i]);
                        $validacion['monto'.$i]=str_replace(",", ".", $validacion['monto'.$i]);
                        $validacion['Ajustado'.$i]=str_replace(",", ".", $validacion['Ajustado'.$i]);
                        $this->atProyectoContratoModelo->metAsignarPartidas( $validacion['partidaDet'.$i], $validacion['monto'.$i],$validacion['Ajustado'.$i], $respuesta['pk_num_presupuesto_det'],$respuesta['fk_cbb004_num_plan_cuenta_pub20'],$respuesta['fk_cbb004_num_plan_cuenta_onco'],$validacion['idRegistro']);


                    }


                }
            }

            $datos=[];
            $datos['idProyecto']=$validacion['idRegistro'];
            $datos['proveedor']=$consulta[0]['proveedor'];
            $datos['objeto']=$validacion['objeto'];
            $datos['estatus']=$consulta[0]['ind_nombre_detalle'];
            $datos['monto']=$validacion['contrato'];
            $datos['estado']=$consulta[0]['fk_a006_num_estado_contrato'];
            echo json_encode($datos);
            exit;

        }else {

            $this->atVista->metCargarJs($js);
            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarCssComplemento($complementosCss);

            $this->atVista->assign('_FormaPagoPost', $this->atProyectoContratoModelo->atMiscelaneoModelo->metMostrarSelect('TIPOPAGO'));

            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());
            $consulta=$this->atProyectoContratoModelo->metGetProyecto($id_valor);
            $this->atVista->assign('_ProyectoContratoPost',$consulta);

            $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());
            $this->atVista->assign('_PartidasSeleccionadasPost',$this->atProyectoContratoModelo->metObtenerPartidasSeleccionadas($id_valor));



            $this->atVista->assign('_AplicablePost',$this->atProyectoContratoModelo->metMostrarSelectAplicable($consulta[0]['fk_gcc003_num_tipo_contrato']));
            $this->atVista->assign('_TipoContratoPost', $this->atProyectoContratoModelo->metMostrarSelectTipo ($consulta[0]['fk_a004_dependencia']));
            $this->atVista->metRenderizar('RegistrarProyectoContrato',"modales");

            }

    }


    public function metSubirProyecto()
    {


        $valido=$this->metObtenerInt('valido');

        $id_valor=$this->metObtenerInt('idPost');

        $Accion=array( "accion" => "modificar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents',
        );

        $complementosJs = array(
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            '../javascript/modGC/inputValidacion/valido',

        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
        );

        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');
            $formAlphaNum=$this->metObtenerAlphaNumerico('form','alphaNum');
            $formTxt=$this->metObtenerTexto('form','txt');
            $formMonto=$this->metObtenerMonto('form','monto');

            if(!empty($formMonto)){
                foreach ($formMonto as $tituloInt => $valorInt) {
                    if(!empty($formMonto[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }
            if(!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }

            // subimos un nivel estatus
            $validacion['estatus']++;

            // asignamos a la variable moviento el valor que le corresponde en el miscelaneo en este método le sumaremos 1 al estatus.
            $movimiento=$validacion['estatus']+1;


            $guia=$this->atProyectoContratoModelo->metConsultarContratos(date('Y'),$validacion['idSolicitado']);
            $guia++;

            
            // EN CASO DE APROBACIÓN
            if($validacion['estatus']==5) {
                $CorrelativoContrato = $this->metCorrelativoContrato($validacion['idSolicitado'] ,$guia);

                }else {

                $CorrelativoContrato ="0";

                }


            $this->atProyectoContratoModelo->metUpdateProyectoEstatus($validacion['idRegistro'],$validacion['estatus'],$movimiento,$validacion['cuerpo'],$CorrelativoContrato);


            echo json_encode($validacion['idRegistro']);
            exit;

        }else {


            $this->atVista->metCargarJs($js);
            $this->atVista->metCargarJsComplemento($complementosJs);
            $this->atVista->metCargarCssComplemento($complementosCss);

            $this->_MiscelaneoModelo= $this->metCargarModelo('miscelaneo', false ,'APP');
            # Asignamos el resultado del llamado a la variable _ClaseVehiculoPost.
            $this->atVista->assign('_FormaPagoPost', $this->_MiscelaneoModelo->metMostrarSelect('FPAGO'));

            $this->atVista->assign('_Acciones', $Accion);
            $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());
            $consulta=$this->atProyectoContratoModelo->metGetProyecto($id_valor);
            $this->atVista->assign('_ProyectoContratoPost',$consulta);


            $this->atVista->assign('_AplicablePost',$this->atProyectoContratoModelo->metMostrarSelectAplicable($consulta[0]['fk_gcc003_num_tipo_contrato']));
            $this->atVista->assign('_TipoContratoPost', $this->atProyectoContratoModelo->metMostrarSelectTipo($consulta[0]['fk_a004_dependencia']));
            $this->atVista->metRenderizar('RegistrarProyectoContrato',"modales");

        }

    }

    public function metMovimientos()
    {
        $id_valor=$this->metObtenerInt('idPost');

        $this->atVista->assign('_MovimientosPost', $this->atProyectoContratoModelo->metGetMovimientos($id_valor));
        $this->atVista->metRenderizar('ListarMovimientos',"modales");
    }

    public function metPresupuestoContrato()
    {
        $id=$this->metObtenerInt('id');

        $respuesta=$this->atProyectoContratoModelo->metPresupuestoContrato($id);
        $datos=[];
        for($i=0;$i<count($respuesta);$i++){
            $datos[$i]['ind_partida']=$respuesta[$i]['ind_partida'];
            $datos[$i]['Comprometido']=$respuesta[$i]['Comprometido'];
            $datos[$i]['Causado']=$respuesta[$i]['Causado'];
            $datos[$i]['Pagado']=$respuesta[$i]['Pagado'];
        }



        echo json_encode($datos);
        exit;
    }


    protected function metObtenerMonto($clave,$posicion=false)
    {
        if((isset($_POST[$clave]) && !empty($_POST[$clave]))){
            if(is_array($_POST[$clave])){
                if($posicion) {
                    if (isset($_POST[$clave][$posicion]) && !empty($_POST[$clave][$posicion])) {
                        foreach ($_POST[$clave][$posicion] as $titulo => $valor) {
                            if(is_array($_POST[$clave][$posicion][$titulo])){
                                foreach($_POST[$clave][$posicion][$titulo] as $titulo1 => $valor1){
                                    $_POST[$clave][$posicion][$titulo][$titulo1] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$posicion][$titulo][$titulo1]));
                                }
                            }else{
                                if(isset($_POST[$clave][$posicion][$titulo]) && !empty($_POST[$clave][$posicion][$titulo])) {
                                    $_POST[$clave][$posicion][$titulo] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$posicion][$titulo]));
                                }
                            }
                        }
                        return $_POST[$clave][$posicion];
                    }
                }else{
                    if (isset($_POST[$clave])) {
                        foreach ($_POST[$clave] as $titulo => $valor) {
                            $_POST[$clave][$titulo] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave][$titulo]));
                        }
                        return $_POST[$clave];
                    }
                }
            }else{
                $_POST[$clave] = (string)htmlspecialchars(preg_replace('/[^0-9 ,]/i', '', $_POST[$clave]));
                return $_POST[$clave];
            }
        }
    }


    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }


    public function metAnularProyecto()
    {

    $id_valor=$this->metObtenerInt('idPost');

    $resultado=$this->atProyectoContratoModelo->metGetEstatusId($id_valor);


         if($resultado[0]['fk_a006_num_estado_contrato']==1){

            $estatus="6";

            $this->atProyectoContratoModelo->metBajar($id_valor,$estatus);

            $array = array(
                'status' => 'OK',
                'idProyecto' => $id_valor
            );

            echo json_encode($array);
            
         }else if($resultado[0]['fk_a006_num_estado_contrato']==6){



                $this->atProyectoContratoModelo->metEliminar($id_valor);

                $array = array(
                    'status' => 'OK',
                    'idProyecto' => $id_valor
                );

                echo json_encode($array);

         }else{

                 $this->atProyectoContratoModelo->metBajar($id_valor,($resultado[0]['fk_a006_num_estado_contrato']-1));

                 $array = array(
                     'status' => 'OK',
                     'idProyecto' => $id_valor
                 );

                 echo json_encode($array);

         }
    }



    protected function metCorrelativoProyecto($solicitado,$id)
    {

        $Correlativo="P-0".$solicitado."-".str_pad($id, '3', '0', STR_PAD_LEFT)."-".date('Y');


        return $Correlativo;

    }

    protected function metCorrelativoContrato($solicitado,$id)
    {

        $Correlativo="C-0".$solicitado."-".str_pad($id,'3', '0', STR_PAD_LEFT)."-".date('Y');


        return $Correlativo;

    }




    public function metNuevaObligacion()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $cajaChica = $this->metObtenerInt('cajaChica');
        $idObligacion = $this->metObtenerInt('idObligacion');
        $idOrden = $this->metObtenerInt('idOrden');
        $estado = $this->metObtenerAlphaNumerico('estado');
        $motivoAnulacion = $this->metObtenerAlphaNumerico('motivoAnulacion');
        $idContrato = $this->metObtenerInt('idContrato');
        $Disponible = $this->metObtenerInt('Disponible');
        if ($valido == 1) {
            if($estado=='RE' || $estado=='CO' || $estado=='AP' || $estado=='RECHAZO' || $estado=='AN' || $estado == 'IM'){
                if($estado=='RECHAZO'){
                    $id=$this->atProyectoContratoModelo->atObligacionModelo->metRechazarObligacion($idObligacion,$estado);
                    $validacion['Mensaje'] = array('titulo'=>'Rechazado','contenido'=>'La Obligacion fue rechazada Satisfactoriamente');
                }elseif($estado=='AN'){
                    if($motivoAnulacion==''){
                        $validacion['status']='error';
                        $validacion['ind_motivo_anulacion']='error';
                        echo json_encode($validacion);
                        exit;
                    }
                    $id=$this->atProyectoContratoModelo->atObligacionModelo->metRechazarObligacion($idObligacion,$estado,$motivoAnulacion,$idOrden);
                    $validacion['Mensaje'] = array('titulo'=>'Anulado','contenido'=>'La Obligacion fue anulada Satisfactoriamente');
                }elseif($estado=='RE'){
                    $id=$this->atProyectoContratoModelo->atObligacionModelo->metRevisarObligacion($idObligacion);
                    $validacion['Mensaje'] = array('titulo'=>'Revisada','contenido'=>'La Obligacion fue revisada Satisfactoriamente');
                    $validacion['fk_prc002_num_presupuesto_det'] = $id;
                }elseif($estado=='CO'){
                    $id=$this->atProyectoContratoModelo->atObligacionModelo->metConformarObligacion($idObligacion, Session::metObtener('CONTABILIDAD'));
                    $generaVoucherProvision = $this->atProyectoContratoModelo->atObligacionModelo->metConsultaObligacion($idObligacion);
                    $validacion['num_flag_presupuesto'] = $generaVoucherProvision['num_flag_provision'];
                    $validacion['Mensaje'] = array('titulo'=>'Conformada','contenido'=>'La Obligacion fue conformada Satisfactoriamente');
                    $validacion['contabilidad'] = Session::metObtener('CONTABILIDAD');
                }elseif($estado=='AP'){
                    $resultado=$this->atProyectoContratoModelo->atObligacionModelo->metAprobarObligacion($idObligacion, Session::metObtener('CONTABILIDAD'));
                    $generaVoucherProvision = $this->atProyectoContratoModelo->atObligacionModelo->metConsultaObligacion($idObligacion);
                    if (is_array($resultado)) {
                        $validacion['status'] = 'errorSQL';
                        echo json_encode($validacion);
                        exit;
                    }
                    $id=str_getcsv($resultado,'-');
                    $validacion['num_flag_presupuesto'] = $generaVoucherProvision['num_flag_provision'];
                    $validacion['Mensaje'] = array('titulo'=>'APROBADA','contenido'=>'La Obligacion fue aprobada Satisfactoriamente se ha generado el pago N°'.$id[1]);
                    $validacion['contabilidad'] = Session::metObtener('CONTABILIDAD');
                    $id=$id[0];
                }

                if (is_array($id)) {
                    foreach ($validacion as $titulo => $valor) {
                        if(is_array($validacion[$titulo])) {
                            if (strpos($id[2], $titulo)) {
                                $validacion[$titulo] = 'error';
                            }
                        }
                    }
                    $validacion['status'] = 'errorSQL';
                    echo json_encode($validacion);
                    exit;
                }
                $validacion['idObligacion'] = $id;
                $validacion['status'] = 'OK';
                echo json_encode($validacion);
                exit;
            }
            $excepcion = array(
                'num_flag_caja_chica', 'num_flag_pago_individual', 'num_flag_obligacion_auto',
                'num_flag_diferido', 'num_flag_pago_diferido', 'num_flag_afecto_IGV',
                'num_flag_compromiso', 'num_flag_presupuesto', 'num_flag_obligacion_directa',
                'num_monto_descuento', 'fk_cpb015_num_impuesto', 'cant',
                'ind_secuencia', 'num_monto_afecto', 'num_monto_impuesto',
                'num_monto_no_afecto', 'num_monto_impuesto_otros', 'num_monto_obligacion',
                'nomtoTotalPagar','ind_comentarios_adicional', 'ind_motivo_anulacion', 'ind_tipo_descuento',
                'num_monto_adelanto','num_monto_pago_parcial'
            );
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum', $excepcion);
            $txt = $this->metValidarFormArrayDatos('form', 'txt', $excepcion);
            $ind = $this->metValidarFormArrayDatos('form', 'int', $excepcion);

            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = $ind;
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif ($alphaNum != null && $txt != null && $ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif ($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            } else {
                $validacion = array_merge($ind, $txt, $alphaNum);
            }
            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }
            if (!isset($validacion['num_flag_caja_chica'])) {
                $validacion['num_flag_caja_chica'] = 0;
            }
            if (!isset($validacion['num_flag_pago_individual'])) {
                $validacion['num_flag_pago_individual'] = 0;
            }
            if (!isset($validacion['num_flag_obligacion_auto'])) {
                $validacion['num_flag_obligacion_auto'] = 0;
            }
            if (!isset($validacion['num_flag_diferido'])) {
                $validacion['num_flag_diferido'] = 0;
            }
            if (!isset($validacion['num_flag_pago_diferido'])) {
                $validacion['num_flag_pago_diferido'] = 0;
            }
            if (!isset($validacion['num_flag_afecto_IGV'])) {
                $validacion['num_flag_afecto_IGV'] = 0;
            }
            if (!isset($validacion['num_flag_compromiso'])) {
                $validacion['num_flag_compromiso'] = 0;
            }
            if (!isset($validacion['num_flag_presupuesto'])) {
                $validacion['num_flag_presupuesto'] = 0;
            }
            if (!isset($validacion['num_flag_adelanto'])) {
                $validacion['num_flag_adelanto'] = 0;
            }
            if (!isset($validacion['num_flag_obligacion_directa'])) {
                $validacion['num_flag_obligacion_directa'] = 0;
            }
            if ($idObligacion == 0) {
                if(isset($cajaChica) and $cajaChica==1){

                    $idCajaChica = $this->metObtenerInt('idCajaChica');
                }else{

                    $idCajaChica = '';
                }
                $procedencia = 'GC';
                $validacion['status'] = 'nuevo';



                $id=$this->atProyectoContratoModelo->metNuevaObligacion($validacion,$procedencia);

                $this->atProyectoContratoModelo->metRelacionarContrato($idContrato,$id);

                $validacion['idObligacion'] = $id;
                if(isset($cajaChica) and $cajaChica==1){
                    $actualizarCC = $this->atProyectoContratoModelo->atObligacionModelo->metActualizarCajaChica($idCajaChica,$validacion['idObligacion']);
                }

                $idNuevo = $this->atProyectoContratoModelo->atObligacionModelo->metObligacionListar(false,false,$validacion['idObligacion']);
            } else {
                $validacion['status'] = 'modificar';
                $id=$this->atProyectoContratoModelo->atObligacionModelo->metModificarObligacion($idObligacion,$validacion,'CP');
                $validacion['idObligacion'] = $id;
                $idNuevo = $this->atProyectoContratoModelo->atObligacionModelo->metObligacionListar(false,false,$idObligacion);
            }
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $titulo)) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $idNuevo['status'] = $validacion['status'];
            echo json_encode($idNuevo);
            exit;
        }


        if ($idObligacion != 0) {



            $this->atVista->assign('obligacionBD', $this->atProyectoContratoModelo->atObligacionModelo->metConsultaObligacion($idObligacion));
            $this->atVista->assign('impuestoBD', $this->atProyectoContratoModelo->atObligacionModelo->metMostrarImpuesto($idObligacion));
            $this->atVista->assign('documentoBD', $this->atProyectoContratoModelo->atObligacionModelo->metMostrarDocumentoObligacion($idObligacion));
            $this->atVista->assign('partidaBD', $this->atProyectoContratoModelo->atObligacionModelo->metMostrarPartidas($idObligacion));
            $this->atVista->assign('trDocumentos', 1);

            if($estado!='modificar'){
                $this->atVista->assign('ver', 1);
            }

        }
        else if ($idContrato != 0){

            $listarPartidas = $this->atProyectoContratoModelo->metObtenerPartidasCargadas($idContrato);

            $acumuladorMonto=0;

            foreach ( $listarPartidas as $listar ) {

                $acumuladorMonto = $acumuladorMonto + $listar['num_monto'];

            }

            $contadorPartidas = count($listarPartidas);



            $this->atVista->assign('partidaBD', $listarPartidas);
            $this->atVista->assign('contadorPartida', $contadorPartidas);



            $respuesta=$this->atProyectoContratoModelo->metGetIdProveedor($idContrato);
            $respuesta['num_monto_obligacion']=$acumuladorMonto;
            $this->atVista->assign('obligacionBD', $respuesta );


            $this->atVista->assign('listadoDocumento', $this->atProyectoContratoModelo->atObligacionModelo->metListarDocumentosPersona($respuesta['fk_a003_num_persona_proveedor']));
            $existe= $this->atProyectoContratoModelo->atObligacionModelo->metListarServicioPersona($respuesta['fk_a003_num_persona_proveedor']);
            $this->atVista->assign('listadoServicio', $this->atProyectoContratoModelo->atObligacionModelo->metListarServicioPersona($respuesta['fk_a003_num_persona_proveedor']));

            if( empty($existe)){

                $this->atVista->assign('listadoDocumento', $this->atProyectoContratoModelo->atObligacionModelo->metDocumentoListar());
                $this->atVista->assign('listadoServicio', $this->atProyectoContratoModelo->atObligacionModelo->atServicioModelo->metServiciosListar());
            }



                $this->atVista->assign('Bloquear', $respuesta['num_guia_contrato']);


        }

        if($estado!=''){
            $this->atVista->assign('estado', $estado);
        }

        //$this->atVista->assign('listadoDocumento', $this->atProyectoContratoModelo->atObligacionModelo->metDocumentoListar());
        //$this->atVista->assign('listadoServicio', $this->atProyectoContratoModelo->atObligacionModelo->atServicioModelo->metServiciosListar());

        $this->atVista->assign('listadoCuentas', $this->atProyectoContratoModelo->atObligacionModelo->atCuentasModelo->metCuentaListar());
        $this->atVista->assign('listadoTipoPago', $this->atProyectoContratoModelo->atObligacionModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
        $this->atVista->assign('tipoDescuento', $this->atProyectoContratoModelo->atObligacionModelo->atMiscelaneoModelo->metMostrarSelect('TIPODESC'));
        $this->atVista->assign('parametroRevisar', Session::metObtener('REVOBLIG'));
        $this->atVista->assign('idContrato', $idContrato);
        $this->atVista->assign('Disponible', $Disponible);
        $this->atVista->assign('idObligacion', $idObligacion);
        $this->atVista->metRenderizar('NuevaObligacion', 'modales');


    }

    public function metPersona($persona, $idCampo = false)
    {

        $this->atVista->assign('lista', $this->atProyectoContratoModelo->metListaProveedorRepresentante());
        $this->atVista->assign('listaPersona', $persona);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoPersona', 'modales');
    }

    public function metVerificarDisponibilidadPartida()
    {
        $monto = $this->metObtenerMonto('monto');
        $pkNumPartida = $this->metObtenerInt('idPartida');
        $partida = $this->atProyectoContratoModelo->metBuscarPartidaCuenta($pkNumPartida);
        $calculo = (($partida['num_monto_compromiso'] +  $monto) * 100) / $partida['num_monto_ajustado'];
        $arrayPorcentaje = array(
            'porcentaje' => round($calculo, 2)
        );
        echo json_encode($arrayPorcentaje);
    }


    public function metCalcularIva()
    {

        $iva = $this->metObtenerInt('iva');

        $factor = $this->atProyectoContratoModelo->atObligacionModelo->metBuscarIva($iva);

        echo json_encode($factor);
    }

}
