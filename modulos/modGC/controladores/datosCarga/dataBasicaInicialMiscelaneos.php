<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
trait dataBasicaInicialMiscelaneos
{

    public function metMiscelaneosGC()
    {
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--->>Modulo GC<br>';
        $a005_miscelaneo_maestro = array( array('ind_nombre_maestro' => 'Estatus Contratos GC', 'ind_descripcion' => 'Estatus de los Contratos', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '16', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'PROESTATUS',
            'Det' => array(
                array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado', 'ind_estatus' => '1'),
                array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Enviado a Revisión', 'ind_estatus' => '1'),
                array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Revisado', 'ind_estatus' => '1'),
                array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Conformado', 'ind_estatus' => '1'),
                array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Aprobado', 'ind_estatus' => '1'),
                array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Anulado', 'ind_estatus' => '1'),
            )
        ),
            array('ind_nombre_maestro' => 'Movimiento GC', 'ind_descripcion' => 'Movimientos en Gestión de Contratos', 'num_estatus' => '1', 'fk_a015_num_seguridad_aplicacion' => '16', 'fk_a018_num_seguridad_usuario' => '1', 'cod_maestro' => 'MOVIGC',
                'Det' => array(
                    array('cod_detalle' => '1', 'ind_nombre_detalle' => 'Preparado', 'ind_estatus' => '1'),
                    array('cod_detalle' => '2', 'ind_nombre_detalle' => 'Modificado', 'ind_estatus' => '1'),
                    array('cod_detalle' => '3', 'ind_nombre_detalle' => 'Enviado a Revisión', 'ind_estatus' => '1'),
                    array('cod_detalle' => '4', 'ind_nombre_detalle' => 'Revisado', 'ind_estatus' => '1'),
                    array('cod_detalle' => '5', 'ind_nombre_detalle' => 'Conformado', 'ind_estatus' => '1'),
                    array('cod_detalle' => '6', 'ind_nombre_detalle' => 'Aprobado', 'ind_estatus' => '1'),
                    array('cod_detalle' => '7', 'ind_nombre_detalle' => 'Anulado', 'ind_estatus' => '1'),
                )
            )
        );
        return $a005_miscelaneo_maestro;
    }



    public function metDataMiscelaneos()
    {
        $a005_miscelaneo_maestro = array_merge(

            $this->metMiscelaneosGC()
        );
        return $a005_miscelaneo_maestro;
    }


}
