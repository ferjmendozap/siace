<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          José Pereda                      |                                    |         0424-8040078           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        02-05-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

trait dataGestionContrato
{



    public function metDataTipoContrato()
    {
        $gc_c003_tipo_contrato = array(
            array('ind_descripcion' => 'Contrato de Servicios por Honorarios Profesionales','fk_a004_dependencia' => '3','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1'),
            array('ind_descripcion' => 'Contrato Marco','fk_a004_dependencia' => '2','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1'),
            array('ind_descripcion' => 'Contrato de Prestación de Servicios Profesion','fk_a004_dependencia' => '2','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1')
        );

        return $gc_c003_tipo_contrato ;

    }

    public function metDataAplicableContrato()
    {
        $gc_c004_aplicable_contrato = array(
            array('ind_descripcion' => 'Honorarios Profesionales','fk_gcc003_num_tipo_contrato' => '1','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1'),
            array('ind_descripcion' => 'Tiempo Determinado','fk_gcc003_num_tipo_contrato' => '1','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1'),
            array('ind_descripcion' => 'honorarios Profesionales ','fk_gcc003_num_tipo_contrato' => '2','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1'),
            array('ind_descripcion' => 'tiempo Determinado ','fk_gcc003_num_tipo_contrato' => '2','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1'),
            array('ind_descripcion' => 'Bienes','fk_gcc003_num_tipo_contrato' => '3','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1'),
            array('ind_descripcion' => 'Servicios','fk_gcc003_num_tipo_contrato' => '3','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1'),
            array('ind_descripcion' => 'Ejecución de Obras','fk_gcc003_num_tipo_contrato' => '3','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1'),
            array('ind_descripcion' => 'Servicios Profesionales','fk_gcc003_num_tipo_contrato' => '4','fec_ultima_modificacion' => '2015-10-07 00:00:00','fk_a018_num_seguridad_usuario' => '1')
        );

        return $gc_c004_aplicable_contrato ;

    }


}
