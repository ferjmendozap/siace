<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/

class contratoControlador extends Controlador
{
    private $atContratoModelo;
    private $atRequerimientoModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atContratoModelo = $this->metCargarModelo('contrato');
        $this->atProyectoContratoModelo = $this->metCargarModelo('proyectoContrato');


    }
	


    #Metodo Index del controlador Contrato
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );





        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_contratoPost',$this->atContratoModelo->metGetContrato());

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarContrato');
    }

    public function metContratosPresupuesto()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';

        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );



        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_contratoPost',$this->atContratoModelo->metGetContratoPresupuesto());

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarContratoPresupuesto');
    }


    public function metObligacionesContrato()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );



        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $Contratos=$this->atContratoModelo->metGetContratoObligaciones();

        for($x=0;$x<count($Contratos);$x++){

            $respuesta=$this->atProyectoContratoModelo->metPresupuestoContrato($Contratos[$x]['pk_num_registro_contrato']);

            $total=0;

            for($i=0;$i<count($respuesta);$i++){
                $total=($total+$respuesta[$i]['Pagado']);
            }

            $Contratos[$x]['Disponible']=$Contratos[$x]['num_monto_contrato']-$total;

        }



        $this->atVista->assign('_contratoPost',$Contratos );


        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarContratoObligacion');
    }

    public function metOrdenesContrato()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );



        $filtro=$this->metObtenerInt('filtro');

        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_contratoPost',$this->atContratoModelo->metGetcontratoOrdenes());

        $this->atVista->metCargarJsComplemento($complementosJs);

        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('ListarOrdenesContrato');
    }

    public function metPagosContrato()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );



        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_contratoPost',$this->atContratoModelo->metGetContrato());
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);




        $this->atVista->metRenderizar('ListarPagoContrato');
    }

    public function metAddendum()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',

        );



        $filtro=$this->metObtenerInt('filtro');
        if($filtro==1) {
            $formInt = $this->metObtenerInt('form', 'int');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
        }
        $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

        $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

        $this->atVista->assign('_contratoPost',$this->atContratoModelo->metGetContrato());
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);




        $this->atVista->metRenderizar('ListarAddendum');
    }

    public function metComprometerContrato ()
    {

        $id=$this->metObtenerInt('id');

        $respuesta=$this->atProyectoContratoModelo->metGetPresupuestoDet($id);


        $id_registro=$this->atProyectoContratoModelo->metContratoComprometer ( $respuesta[0]['num_monto'],$id,$respuesta[0]['pk_num_presupuesto_det']);

        $this->atProyectoContratoModelo->metUpdateContratoPresupuesto($id);

        $validacion=[];

        if(is_array($id_registro)){

            $validacion['status'] = 'error';
            $validacion['detalleERROR'] = $id;


            echo json_encode($validacion);
            exit;

        }else{

            $validacion['status'] = 'registro';
            $validacion['monto'] = $respuesta[0]['num_monto'];
            $validacion['detalle'] = $respuesta[0]['pk_num_presupuesto_det'];
            $validacion['id'] = $id;
            echo json_encode($validacion);
            exit;

        }

    }

    public function metOrdenCompraGC()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idOrden = $this->metObtenerInt('idOrden');
        $estado = "Listado de";
        $idContrato = $this->metObtenerInt('idContrato');
        $ver = $this->metObtenerInt('ver');
        $int = $this->metObtenerInt('form','int');
        if ($valido == 1) {
            $this->metValidarToken();

            if (isset($int['mAfecto']) and $int['mAfecto']!=0){
                $ExcceccionInt = array('mNoAfecto','impuestos','oCargos');
            } else {
                $ExcceccionInt = array('mAfecto','impuestos','oCargos');
            }
            $ExcceccionAlphaNum = array('comentario','rechazo');

            $ind = $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
            $txt = $this->metValidarFormArrayDatos('form','txt');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum',$ExcceccionAlphaNum);

            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $txt != null) {
                $validacion = $txt;
            } elseif ($alphaNum == null && $ind != null && $txt != null) {
                $validacion = array_merge($txt, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null) {
                $validacion = array_merge($txt, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $txt);
            }

            if (!isset($validacion['sec'])){
                $validacion['status']="errorIC";
                echo json_encode($validacion);
                exit;
            }

            $orden=$this->atContratoModelo->atOrden->atRequerimientoModelo->metContar('lg_b019_orden',date('y'),'fec_anio');
            $orden['cantidad'] = $orden['cantidad'] +1;
            $orden=$this->rellenarCeros($orden['cantidad'],10);
            $validacion['orden']=$orden;
            $predeterminado=$this->atContratoModelo->atOrden->atRequerimientoModelo->metBuscarPredeterminado();
            $validacion['cCosto'] = $predeterminado['fk_a023_num_centro_costo'];
            $contador=1;
            if($ver!=1) {
                foreach ($validacion['cant'] as $key=>$value) {

                    if(!isset($validacion['exonerado'][$key])){
                        $validacion['exonerado'][$key]="0";
                    }

                    if($validacion['cant'][$key]=="error"){
                        $validacion['status'] = 'errorCantidades';
                        echo json_encode($validacion);
                        exit;
                    }

                    if($validacion['precio'][$key]=="error"){
                        $validacion['precio'][$key]="error";
                        $validacion['status'] = 'errorPrecio';
                        echo json_encode($validacion);
                        exit;
                    }
                    if($idOrden==0){
                        $idOrden="0";
                    }

                    if($validacion['descPorc'][$key]=="error"){
                        $validacion['descPorc'][$key]="0";
                    }

                    if($validacion['descFijo'][$key]=="error"){
                        $validacion['descFijo'][$key]="0";
                    }

                }
            }

            /*
            while ($contador<=$validacion['sec'] AND $ver!=1) {
                $contador=$contador+1;
            }
*/

            $contador=1;
            foreach ($validacion['montoPartida'] as $key=>$value) {
                #$key es el idPartida
                #value es el monto

                $idPresupuesto = $this->atContratoModelo->atOrden->metBuscarPresupuestoDetalle($key);

                if(!$idPresupuesto){
                    $validacion['idPartida'.$key] = 'error';
                    $validacion['status'] = 'errorPresupuesto';
                    echo json_encode($validacion);
                    exit;
                }
                if($idPresupuesto['num_monto_ajustado']-$idPresupuesto['num_monto_compromiso']<$value){
                    $validacion['cod_partida'] = $idPresupuesto['cod_partida'];
                    $validacion['status'] = 'errorPresupuesto2';
                    $validacion['idPartida'.$key] = 'error';
                    echo json_encode($validacion);
                    exit;
                }
                if ($estado=='revisar' and $idOrden!=null){

                    $id3=$this->atContratoModelo->atOrden->metCrearEstadoDistribucion(
                        $idPresupuesto['pk_num_presupuesto_det'],$key,
                        $idPresupuesto['fk_cbb004_num_plan_cuenta_onco'],
                        $idPresupuesto['fk_cbb004_num_plan_cuenta_pub20'],
                        $contador,$idOrden,$value);
                    $validacion['id3']=$id3;
                }
                $contador=$contador+1;
            }

            $id3="";
            $validacion['estado']=$estado;
            $validacion['idOrden']=$idOrden;
            if ($estado=='revisar' and $idOrden!=null){
                $estatus='RV';
                $empleado='fk_rhb001_num_empleado_revisado_por';
                $campoFecha="fec_revision";
                $estatusDetalle="PR";
                $validacion['status']='modificar';
            } elseif ($estado=='conformar' and $idOrden!=null){
                $estatus='CN';
                $empleado=false;
                $campoFecha=false;
                $estatusDetalle='PR';
                $validacion['status']='modificar';
            } elseif ($estado=='aprobar' and $idOrden!=null){
                $estatus='AP';
                $empleado='fk_rhb001_num_empleado_aprobado_por';
                $campoFecha="fec_aprobacion";
                $estatusDetalle='PE';
                $validacion['status']='modificar';
            } elseif ($estado=='anular' and $idOrden!=null){
                $estatus='AN';
                $empleado=false;
                $campoFecha=false;
                $estatusDetalle='PR';

                $validacion['status']='ok';
            } elseif ($estado=='reverzar' and $idOrden!=null){
                $estatus='PR';
                $empleado=false;
                $campoFecha=false;
                $estatusDetalle='PR';
                $id3=$this->atContratoModelo->atOrden->metEliminarEstadoDistribucion($idOrden);
                $validacion['status']='ok';
            }
            if(isset($estatus)){
                $id=$this->atContratoModelo->atOrden->metActualizarOrden($estatus,$idOrden,$empleado,$campoFecha);
                $id2=$this->atContratoModelo->atOrden->metActualizarOrdenDetalle($estatusDetalle,$idOrden);
                $validacion['idactualizar']=$id;
                $validacion['idactualizadetalle']=$id2;
                $validacion['id3']=$id3;
                $validacion['ind_estado']=$estatus;
            }

            if(!isset($validacion['comentario'])){
                $validacion['comentario']=false;
            }
            if(!isset($validacion['rechazo'])){
                $validacion['rechazo']=false;
            }

            if($estado!="Listado de"){
                echo json_encode($validacion);
                exit;
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            $validacion['item'] = null;
            $validacion['comm'] = null;
            if($validacion['tipo']=='item'){
                $validacion['item'] = $validacion['num'];
            } else {
                $validacion['comm'] = $validacion['num'];
            }


            if(!isset($validacion['idOrdenDetalle'])) {
                $validacion['idOrdenDetalle'] = null;
            }

            if  ($idOrden == "0") {
                $validacion['status']='nuevo';
                $id=$this->atContratoModelo->atOrden->metNuevaOrden(
                    $validacion
                );

                $this->atContratoModelo->metRelacionarOrdenContrato($validacion['idContrato'],$id);

            } else {
                $validacion['status']='modificar';
                $id=$this->atContratoModelo->atOrden->metModificarOrden(
                    $validacion['dependencia'],$validacion['centro_costo'],$validacion['clasificacion'],$validacion['almacen'],
                    $validacion['proveedor'],
                    $validacion['tipo_servicio'],$validacion['mBruto'],$validacion['impuestos'],
                    $validacion['mTotal'],$validacion['mPendiente'],$validacion['mAfecto'],
                    $validacion['mNoAfecto'],$validacion['formaPago'],$validacion['plazo'],
                    $validacion['entregar'],$validacion['direccion'],$validacion['comentario'],
                    $validacion['descripcion'],$validacion['cuenta'],
                    $validacion['partida'],null,
                    $validacion['sec'],$validacion['cant'],$validacion['precio'],
                    $validacion['montoPU'],$validacion['descPorc'],$validacion['descFijo'],
                    $validacion['exonerado'],$validacion['total'],$validacion['cuentaDetalle'],
                    $validacion['partidaDetalle'],$validacion['unidad'],$validacion['cCosto'],
                    $idOrden,$validacion['idOrdenDetalle'],$validacion['item'],$validacion['comm']
                );
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['id'] = $id;
                echo json_encode($validacion);
                exit;
            }

            $validacion['idOrden']=$id;
            echo json_encode($validacion);
            exit;
        }

        if($idOrden!=0){
            $orden = $this->atContratoModelo->atOrden->metMostrarOrden($idOrden);
            $this->atVista->assign('formBD',$orden);
            $this->atVista->assign('estadoOrden',$this->estadoNombre($orden['estadoOS']));
            $this->atVista->assign('formBDD',$this->atContratoModelo->atOrden->metMostrarOrdenDetalle($idOrden));
            $this->atVista->assign('idOrden',$idOrden);
            $this->atVista->assign('cotizaciones',$this->atContratoModelo->atOrden->metBuscarCotizaciones($idOrden));
            $this->atVista->assign('requerimientos',$this->atContratoModelo->atOrden->metBuscarRequerimientos($idOrden));
        }

        $this->atVista->assign('idContrato',$idContrato);
        $this->atVista->assign('ver',$ver);
        $this->atVista->assign('predeterminado',$this->atContratoModelo->atOrden->atRequerimientoModelo->metBuscarPredeterminado());
        $this->atVista->assign('partidaCuenta',$this->atContratoModelo->atOrden->metBuscarPartidaCuenta('403.18.01.00'));
        $this->atVista->assign('almacen',$this->atContratoModelo->atOrden->atRequerimientoModelo->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('clas',$this->atContratoModelo->atOrden->atRequerimientoModelo->atClasificacionModelo->metListarClasificacion(false,'REQUERIMIENTO DE AUTO REPOSICION'));
        $this->atVista->assign('formaPago',$this->atContratoModelo->atOrden->atMiscelaneoModelo->metMostrarSelect('FDPLG'));
        $this->atVista->assign('tipoServicio',$this->atContratoModelo->atOrden->metListarTipoServicio());
        $this->atVista->assign('dependencia',$this->atContratoModelo->atOrden->metListarDependencias());
        $this->atVista->assign('estado',"Listado de");
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function rellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    }


}
