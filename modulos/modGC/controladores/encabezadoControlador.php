<?php
/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/

class encabezadoControlador extends Controlador
{
    private $atEncabezadoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEncabezadoModelo = $this->metCargarModelo('encabezado');
        $this->atProyectoContratoModelo= $this->metCargarModelo('proyectoContrato');

    }
	


    #Metodo Index del controlador proyectoContrato
    public function metIndex()
    {
    }

    #Metodo parac cargar el listado de los Proyecto de Contrato
    public function metListarEncabezado()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
        );

        $js [] ='materialSiace/core/demo/DemoTableDynamic';
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'ckeditor/ckeditor',
            'ckeditor/adapters/jquery',

        );


        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);


        $this->atVista->assign('_EncabezadoPost',$this->atEncabezadoModelo->metGetEncabezados('0'));


        $this->atVista->metRenderizar('ListarEncabezado');
    }



    #Metodo parac cargar el listado de los Proyecto de Contrato
    public function metRegistrarEncabezado()
    {
        $valido=$this->metObtenerInt('valido');

        $id_valor=$this->metObtenerInt('idPost');
        $accion =$this->metObtenerAlphaNumerico('Accion');
        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
 
        );

        $complementosJs = array (
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'ckeditor/ckeditor',
            'ckeditor/adapters/jquery',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
            'select2/select201ef',
        );

        if($valido==1) {

            $formInt=$this->metObtenerInt('form','int');

            $formTxt=$this->metObtenerTexto('form','txt');

            if(!empty($formInt)){
                foreach ($formInt as $tituloInt => $valorInt) {
                    if(!empty($formInt[$tituloInt])){
                        $validacion[$tituloInt]=$valorInt;
                    }else{
                        $validacion[$tituloInt]='error';
                    }
                }
            }

            if(!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }

 			if($validacion['cuerpo']=="error"){
                    $validacion['cuerpo']="";
                }

            if($accion!="registrar"){
  	 
                $this->atEncabezadoModelo->metUpdateEncabezado($validacion['cuerpo'],$validacion['idTipoContrato'],$id_valor);
                $resultado=$this->atEncabezadoModelo->metGetEncabezadosId($id_valor);
                echo json_encode($resultado);
                exit;



            }else{
                	
                $id=$this->atEncabezadoModelo->metRegistrarEncabezado($validacion['cuerpo'],$validacion['idTipoContrato']);

                $resultado=$this->atEncabezadoModelo->metGetEncabezadosId($id);
                echo json_encode($resultado);
                exit;
            }

        }else {

            $this->atVista->assign('_ProveedorPost',  $this->atProyectoContratoModelo->metGetProveedores());

            $this->atVista->assign('_DependenciaPost', $this->atProyectoContratoModelo->metGetDependencias());

            $this->atVista->metCargarJsComplemento($complementosJs);

            $this->atVista->metCargarCssComplemento($complementosCss);

            $this->atVista->metCargarJs($js);

            if($accion=="modificar"){

                $this->atVista->assign('_EncabezadoPost', $this->atEncabezadoModelo->metGetEncabezadosId($id_valor));

                $this->atVista->metRenderizar('ModificarEncabezado','modales');

            }else{

                $this->atVista->metRenderizar('RegistrarEncabezado','modales');

            }



        }


    }



    protected function metFormatoFecha($fecha)
    {
        $resultado=explode("/",$fecha);
        $resultado=$resultado[2]."-".$resultado[1]."-".$resultado[0];
        return $resultado;

    }
    public function metEliminarProyectoContrato()
    {
/*
        $id_valor=$this->metObtenerInt('idPost');

        $this->atAccesorioModelo->deleteAccesorio($id_valor);


*/

    }

    public function metEliminarEncabezado()
    {
        $idEncabezado = $this->metObtenerInt('idEncabezado');
        $this->atEncabezadoModelo->metEliminarEncabezado($idEncabezado);
        $array = array(
            'status' => 'OK',
            'idEncabezado' => $idEncabezado
        );

        echo json_encode($array);
    }




}
