<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class encabezadoModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener("idUsuario");
    }

    #metodo para obtener todos los registros guardados de los post
    public function metGetEncabezados()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT gc_b003_predeterminado.*,gc_c003_tipo_contrato.ind_descripcion,a004_dependencia.ind_dependencia

        FROM gc_b003_predeterminado
        left join gc_c003_tipo_contrato on (gc_c003_tipo_contrato.pk_num_tipo_contrato=gc_b003_predeterminado.fk_c003_tipo_contrato )
        left join a004_dependencia on (a004_dependencia.pk_num_dependencia=gc_c003_tipo_contrato.fk_a004_dependencia)
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para obtener todos los registros guardados de un id
    public function metGetEncabezadosId($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost =  $this->_db->query("SELECT 
        gc_b003_predeterminado.pk_num_predeterminado as idEncabezado,
        gc_b003_predeterminado.ind_encabezado,
        gc_b003_predeterminado.fk_c003_tipo_contrato,
        gc_b003_predeterminado.pk_num_predeterminado,
        gc_c003_tipo_contrato.fk_a004_dependencia,
        gc_c003_tipo_contrato.ind_descripcion as tipoContrato,
        a004_dependencia.ind_dependencia 
        FROM gc_b003_predeterminado
        left join gc_c003_tipo_contrato on (gc_c003_tipo_contrato.pk_num_tipo_contrato=gc_b003_predeterminado.fk_c003_tipo_contrato)
        left join a004_dependencia on (a004_dependencia.pk_num_dependencia=gc_c003_tipo_contrato.fk_a004_dependencia)
        WHERE pk_num_predeterminado='".$id."'

        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metUpdateEncabezado($cuerpo,$tipo,$id)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar=$this->_db->prepare(
            "UPDATE gc_b003_predeterminado
              SET
              ind_encabezado=:ind_encabezado,
              fk_c003_tipo_contrato=:fk_c003_tipo_contrato,
              fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
              fec_ultima_modificacion=:fec_ultima_modificacion
              WHERE pk_num_predeterminado='".$id."'"
        );
        $modificar->execute(array(
            ':ind_encabezado' => $cuerpo,
            ':fk_c003_tipo_contrato' => $tipo,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),

        ));

        #commit — Consigna una transacción
        $this->_db->commit();




        return 1;
    }

    public function metRegistrarEncabezado($cuerpo,$tipo )
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $registrar=$this->_db->prepare(
            "INSERT INTO gc_b003_predeterminado (
             ind_encabezado,
             fk_c003_tipo_contrato,
             fec_ultima_modificacion,
             fk_a018_num_seguridad_usuario
             )
             values (
             :ind_encabezado,
             :fk_c003_tipo_contrato,
             :fec_ultima_modificacion,
             :fk_a018_num_seguridad_usuario

             )"
        );
        $registrar->execute(array(
            ':ind_encabezado' => $cuerpo,
            ':fk_c003_tipo_contrato' => $tipo,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuario,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),

        ));


        $idPost= $this->_db->lastInsertId();

        #commit — Consigna una transacción
        $this->_db->commit();

        return $idPost;

    }
    public function metEliminarEncabezado($idEncabezado)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();

        #ejecuto la consulta a la base de datos
        $this->_db->query(
            "delete from gc_b003_predeterminado where pk_num_predeterminado ='$idEncabezado'"
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }


}

