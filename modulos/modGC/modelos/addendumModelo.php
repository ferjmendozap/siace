<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';


class addendumModelo extends miscelaneoModelo
{
    public function __construct()
    {
        parent::__construct();

        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atIdUsuarioModelo = Session::metObtener("idUsuario");
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }



    public function metGetcontratoAddendum( )
    {




        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT gc_b001_registro_contrato.*,gc_b002_registro_contrato_detalle.*, a003_persona.ind_nombre1,a003_persona.ind_apellido1,a003_persona.ind_documento_fiscal,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle,
         a006_miscelaneo_detalle.ind_nombre_detalle
        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
 
        left join a003_persona on (a003_persona.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor )
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )

 

        
        where  fk_a006_num_estado_contrato='5' AND num_estado_presupuesto=0 AND gc_b001_registro_contrato.pk_num_registro_contrato NOT IN (SELECT fk_gcb001_num_registro_contrato FROM  gc_b005_comprometido WHERE ind_estado!='AN')     "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metGetContratoAddendumId($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
        SELECT
        gc_b001_registro_contrato.pk_num_registro_contrato,
        gc_b001_registro_contrato.ind_objeto_contrato,
        gc_b001_registro_contrato.ind_correlativo_contrato,
        gc_b001_registro_contrato.num_monto_contrato,
        gc_b005_comprometido.*,
        preparado.fk_a003_num_persona,persona_preparado.ind_nombre1 as nombre_preparado,persona_preparado.ind_apellido1 as apellido_preparado,
        revisado.fk_a003_num_persona,persona_revisado.ind_nombre1 as nombre_revisado,persona_revisado.ind_apellido1 as apellido_revisado,
        aprobado.fk_a003_num_persona,persona_aprobado.ind_nombre1 as nombre_aprobado,persona_aprobado.ind_apellido1 as apellido_aprobado
        
        FROM gc_b001_registro_contrato
        
        left join  gc_b005_comprometido on (gc_b005_comprometido.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
              
        left join rh_b001_empleado as preparado on (preparado.pk_num_empleado=gc_b005_comprometido.fk_rhb001_num_empleado_prepara)
        
        left join a003_persona as persona_preparado on (persona_preparado.pk_num_persona=preparado.fk_a003_num_persona)
        
        left join rh_b001_empleado as revisado on (revisado.pk_num_empleado=gc_b005_comprometido.fk_rhb001_num_empleado_revisa)
        
        left join a003_persona as persona_revisado on (persona_revisado.pk_num_persona=revisado.fk_a003_num_persona)
       
        left join rh_b001_empleado as aprobado on (aprobado.pk_num_empleado=gc_b005_comprometido.fk_rhb001_num_empleado_aprueba)
        
        left join a003_persona as persona_aprobado on (persona_aprobado.pk_num_persona=aprobado.fk_a003_num_persona)
       
        where  gc_b001_registro_contrato.pk_num_registro_contrato='".$id."'
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metGetAddendumEstatusId($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
        SELECT
        gc_b001_registro_contrato.fk_a006_num_estado_contrato
       
        
        FROM gc_b001_registro_contrato
 
       
        where  gc_b001_registro_contrato.pk_num_registro_contrato='".$id."'
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetch();

    }


    public function metGetContratoAddendumsId($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
        SELECT
        gc_b001_registro_contrato.pk_num_registro_contrato,
        gc_b001_registro_contrato.ind_objeto_contrato,
        gc_b001_registro_contrato.fk_a006_num_estado_contrato,
        gc_b001_registro_contrato.num_monto_contrato,
        gc_b001_registro_contrato.fk_gcc004_num_aplicable_contrato,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle,
        a006_miscelaneo_detalle.ind_nombre_detalle,
        gc_b001_registro_contrato.ind_correlativo_contrato
        
        FROM gc_b001_registro_contrato
        
 
        left join a005_miscelaneo_maestro on  (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )
       
        where  gc_b001_registro_contrato.fk_gcb001_addendum='".$id."'
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }



    public function metAnularAddendum( $id,$estatus)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        if($estatus!=0){

            $this->_db->query(
                " UPDATE gc_b001_registro_contrato SET fk_a006_num_estado_contrato=$estatus WHERE  pk_num_registro_contrato=".$id." "
            );

        }

        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metUpdateAddendum( $id,$estatus)
    {

        if($estatus=="RV"){
            $complemento=" , fk_rhb001_num_empleado_revisa=".$this->atIdEmpleado." , fec_revisado='".date("Y-m-d H:i:s")."' ";
        }else if($estatus=="AP"){

            $complemento=" , fk_rhb001_num_empleado_aprueba=".$this->atIdEmpleado." , fec_aprobado='".date("Y-m-d H:i:s")."' ";
        }else{
            $complemento="";
        }
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            " UPDATE gc_b005_comprometido SET ind_estado='".$estatus."' $complemento  WHERE  pk_num_comprometido=".$id." "
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }

    public function metGetAddendums($estatus)
    {


        if($estatus>0){
            $complemento=" AND gc_b001_registro_contrato.fk_a006_num_estado_contrato=".$estatus."";
        }else{
            $complemento="  ";
        }


        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
        SELECT gc_b001_registro_contrato.ind_correlativo_contrato, gc_b001_registro_contrato.pk_num_registro_contrato, gc_b001_registro_contrato.ind_objeto_contrato, gc_b001_registro_contrato.fk_a006_num_estado_contrato, gc_b001_registro_contrato.num_monto_contrato ,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle, a006_miscelaneo_detalle.ind_nombre_detalle FROM gc_b001_registro_contrato 
 
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS') 
        left join a006_miscelaneo_detalle on (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato ) 
        left join  gc_b001_registro_contrato as addendum on (addendum.pk_num_registro_contrato=gc_b001_registro_contrato.fk_gcb001_addendum)   
        WHERE  gc_b001_registro_contrato.fk_gcb001_addendum!=0 $complemento
        "
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metGetRelacionContrato($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
        
        SELECT fk_gcb001_addendum FROM gc_b001_registro_contrato
             
        WHERE pk_num_registro_contrato='".$id."' LIMIT 0,1
        
        "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetch();

    }


    public function metConsultarPartidas($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
        
        SELECT pk_num_partidas, ind_partida,num_monto_ajustado FROM gc_b004_partidas_contrato
             
        WHERE fk_gcb001_registro_contacto='".$id."' 
        
        "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metSetAddendum($idContrato)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "INSERT INTO `gc_b005_comprometido`(  `fk_gcb001_num_registro_contrato`, `ind_estado`, `fk_rhb001_num_empleado_prepara`) 
             VALUES (:fk_gcb001_num_registro_contrato,:ind_estado,:empleado )"
        );

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            ':fk_gcb001_num_registro_contrato' => $idContrato,
            ':ind_estado' => 'PR',
            ':empleado' =>$this->atIdEmpleado

        ));

        $idPost= $this->_db->lastInsertId();

        $error = $NuevoPost->errorInfo();


        if(!empty($error[1]) && !empty($error[2])){

            $this->_db->rollBack();
            return $error;
        }else{

            $this->_db->commit();

            return $idPost;

        }


    }


    public function metUpdatePartidas( $id,$monto)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            " UPDATE gc_b004_partidas_contrato SET num_monto_ajustado='".$monto."'   WHERE  pk_num_partidas=".$id." "
        );

        #commit — Consigna una transacción
        $this->_db->commit();
    }



}



