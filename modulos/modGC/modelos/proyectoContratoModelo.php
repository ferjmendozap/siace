<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO SUCRE
 * MODULO:Parque Automotor
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | José Pereda.         |dt.ait.programador2@cgesucre.gob.ve   | 0424-8040078
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once RUTA_MODELO.'miscelaneoModelo.php';

require_once RUTA_Modulo . 'modCP' . DS . 'modelos' . DS .'obligacionModelo.php';
require_once RUTA_Modulo . 'modCP' . DS . 'modelos' . DS .'trait'.DS.'consultasTrait.php';

class proyectoContratoModelo extends miscelaneoModelo
{
    use consultasTrait;

    public function __construct()
    {
        parent::__construct();
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atObligacionModelo = new obligacionModelo();
        $this->atIdUsuarioModelo = Session::metObtener("idUsuario");
    }

    #metodo para obtener los datos de los proyectos de contratos a mostrar en los listados
    public function metGetproyectoContrato($id)
    {
        if (intval($id) > 0) {
            $complemento = " AND  fk_a006_num_estado_contrato='" . $id . "'  ";
        } else {
            $complemento = "AND  fk_a006_num_estado_contrato!='5'  ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT gc_b001_registro_contrato.*,gc_b002_registro_contrato_detalle.* ,a003_persona.ind_nombre1 ,a003_persona.ind_apellido1 ,a003_persona.ind_documento_fiscal,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle,
         a006_miscelaneo_detalle.ind_nombre_detalle
        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
        left join a003_persona on (a003_persona.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor )
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )
        where  fk_gcb001_addendum=0  $complemento    
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }


    public function metFiltroProyecto($id, $dependencia, $proveedor)
    {
        if ($id > 0) {
            $complemento = " AND  fk_a006_num_estado_contrato='" . $id . "'  ";
        } else {
            $complemento = "AND  fk_a006_num_estado_contrato!='5'  ";
        }

        if ($dependencia > 0) {
            $complemento2 = " AND  fk_a004_dependencia='" . $dependencia . "'  ";
        } else {
            $complemento2 = "  ";
        }

        if ($proveedor > 0) {
            $complemento3 = " AND  fk_a003_proveedor='" . $proveedor . "'  ";
        } else {
            $complemento3 = "  ";
        }


        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT gc_b001_registro_contrato.*,gc_b002_registro_contrato_detalle.*,a003_persona.ind_nombre1,a003_persona.ind_documento_fiscal,a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,a006_miscelaneo_detalle.cod_detalle,
         a006_miscelaneo_detalle.ind_nombre_detalle
        FROM gc_b001_registro_contrato
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)

        left join a003_persona on (a003_persona.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor )
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )
        where   1 $complemento $complemento2 $complemento3 
        "
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    #metodo para obtener todos los estatus de los proyectos de contrato
    public function metGetEstatus()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT
         a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
         a006_miscelaneo_detalle.cod_detalle,
         a006_miscelaneo_detalle.ind_nombre_detalle

        FROM a005_miscelaneo_maestro

        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro  )

        WHERE a005_miscelaneo_maestro.cod_maestro='PROESTATUS'
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metGetIdProveedor($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
        SELECT  
        gc_b001_registro_contrato.num_guia_contrato,
        gc_b001_registro_contrato.num_monto_contrato,
        gc_b001_registro_contrato.fk_a003_proveedor as fk_a003_num_persona_proveedor,
        gc_b001_registro_contrato.fk_a003_representante as fk_a003_num_persona_proveedor_a_pagar,
        CONCAT(proveedor.ind_nombre1,' ',proveedor.ind_apellido1) AS proveedor,
        proveedor.ind_documento_fiscal AS documentoFiscal,
        CONCAT(proveedorPagar.ind_nombre1,' ',proveedorPagar.ind_apellido1) AS proveedorPagar
        
        FROM `gc_b001_registro_contrato`
               
        INNER JOIN a003_persona AS proveedor ON  (proveedor.pk_num_persona = gc_b001_registro_contrato.fk_a003_proveedor)
       
        INNER JOIN a003_persona AS proveedorPagar on proveedorPagar.pk_num_persona = gc_b001_registro_contrato.fk_a003_proveedor
       
        WHERE  `pk_num_registro_contrato`='" . $id . "'
         
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetch();

    }

    #metodo para obtener todos los registros  de los movimientos que ha tenido los proyectos de contrato
    public function metGetMovimientos($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
        SELECT
        gc_c005_bitacora.*,
        a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
        a006_miscelaneo_detalle.cod_detalle,
        a006_miscelaneo_detalle.ind_nombre_detalle,
        rh_b001_empleado.fk_a003_num_persona,
        a003_persona.ind_nombre1,
        a003_persona.ind_apellido1

        FROM gc_c005_bitacora

        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='MOVIGC')

        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND   a006_miscelaneo_detalle.cod_detalle= gc_c005_bitacora.fk_a006_miscelaneo  )
        left join rh_b001_empleado on (rh_b001_empleado.pk_num_empleado=gc_c005_bitacora.fk_a018_num_seguridad_usuario)

        left join a003_persona on (a003_persona.pk_num_persona=rh_b001_empleado.fk_a003_num_persona)

        WHERE fk_gcb001_num_registro_contrato='" . $id . "'
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metGetproyectoContratoEstatus($Estatus)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT
        gc_b001_registro_contrato.pk_num_registro_contrato,
        gc_b001_registro_contrato.fk_a003_proveedor,
        gc_b001_registro_contrato.ind_objeto_contrato,
        gc_b001_registro_contrato.fk_a006_num_estado_contrato,

        FORMAT( gc_b001_registro_contrato.num_monto_contrato,2) as num_monto_contrato,
        a003_persona.ind_nombre1

        FROM gc_b001_registro_contrato
        left join a003_persona on (a003_persona.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor)
        where fk_a006_num_estado_contrato='$Estatus'
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    #metodo para obtener los datos a mostrar en el formulario de proyecto de contrato
    public function metGetproyecto($id)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT
        gc_b001_registro_contrato.pk_num_registro_contrato,
        gc_b001_registro_contrato.num_guia_contrato,
        gc_b001_registro_contrato.fk_cpb017_tipo_servicio,
        gc_b001_registro_contrato.ind_objeto_contrato,
        gc_b001_registro_contrato.num_monto_contrato,
        gc_b001_registro_contrato.ind_cuerpo,
        gc_b001_registro_contrato.fk_a006_num_estado_contrato,
        gc_b001_registro_contrato.fk_a004_dependencia,
        gc_b001_registro_contrato.fk_gcc004_num_aplicable_contrato,
        gc_b001_registro_contrato.fk_gcc003_num_tipo_contrato,
        gc_b001_registro_contrato.fk_a006_num_forma_pago,
        gc_b003_predeterminado.ind_encabezado,
        gc_b001_registro_contrato.fk_a003_proveedor,
        gc_b001_registro_contrato.fk_a003_representante,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_desde,  '%d/%m/%Y') as  fec_desde,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_hasta,  '%d/%m/%Y') as  fec_hasta,
        DATE_FORMAT(gc_b002_registro_contrato_detalle.fec_entrada,'%d/%m/%Y') as  fec_entrada,

        gc_b002_registro_contrato_detalle.ind_obligaciones_contratante,
        gc_b002_registro_contrato_detalle.ind_obligaciones_contratado,
        gc_b002_registro_contrato_detalle.ind_observacion,
        gc_b002_registro_contrato_detalle.ind_lugar_pago,
        proveedor.ind_nombre1 as proveedor,proveedor.ind_documento_fiscal ,
        a005_miscelaneo_maestro.pk_num_miscelaneo_maestro,
        a006_miscelaneo_detalle.cod_detalle,
        a006_miscelaneo_detalle.ind_nombre_detalle,
       
        representante.ind_apellido1,representante.ind_nombre1,
        representante.ind_cedula_documento
        
        FROM gc_b001_registro_contrato
        
        left join gc_b002_registro_contrato_detalle on (gc_b002_registro_contrato_detalle.fk_gcb001_num_registro_contrato=gc_b001_registro_contrato.pk_num_registro_contrato)
 
        left join a003_persona as proveedor on (proveedor.pk_num_persona=gc_b001_registro_contrato.fk_a003_proveedor )
        left join a003_persona as representante on (representante.pk_num_persona=gc_b001_registro_contrato.fk_a003_representante)
        
        left join a005_miscelaneo_maestro on (a005_miscelaneo_maestro.cod_maestro='PROESTATUS')
        left join a006_miscelaneo_detalle on  (a006_miscelaneo_detalle.fk_a005_num_miscelaneo_maestro=a005_miscelaneo_maestro.pk_num_miscelaneo_maestro AND  a006_miscelaneo_detalle.cod_detalle=gc_b001_registro_contrato.fk_a006_num_estado_contrato )

        left join gc_b003_predeterminado on (gc_b003_predeterminado.fk_c003_tipo_contrato=gc_b001_registro_contrato.fk_gcc003_num_tipo_contrato)

        where pk_num_registro_contrato='" . $id . "' "
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    #metodo para obtener las dependencias
    public function metGetDependencias()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT pk_num_dependencia,ind_dependencia FROM  a004_dependencia WHERE ind_dependencia='DIRECCION DE RECURSOS HUMANOS' OR  ind_dependencia='DIRECCIÓN DE ADMINISTRACIÓN'  "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metListaProveedorRepresentante()
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("SELECT  `a003_persona`.`pk_num_persona`,lg_b022_proveedor.fk_a003_num_persona_representante,  `a003_persona`.`ind_nombre1`,  `a003_persona`.`ind_apellido1`, `a003_persona`.`ind_cedula_documento`,`a003_persona`.`ind_documento_fiscal`,  `proveedor`.`ind_nombre1` as representante1,  `proveedor`.`ind_apellido1` as representnte2,proveedor.ind_cedula_documento as doc_representante, lg_b022_proveedor.fk_a003_num_persona_proveedor,lg_b022_proveedor.pk_num_proveedor

                                        FROM `a003_persona`
                                        left join lg_b022_proveedor on (lg_b022_proveedor.fk_a003_num_persona_proveedor=a003_persona.pk_num_persona)
                                        left join a003_persona as proveedor on (proveedor.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_representante)

                                        group by  a003_persona.pk_num_persona ,lg_b022_proveedor.fk_a003_num_persona_representante ,lg_b022_proveedor.pk_num_proveedor
                                        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metGetPresupuestoDet($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
                SELECT
        pr_b004_presupuesto.pk_num_presupuesto,
        pr_c002_presupuesto_det.pk_num_presupuesto_det,
        gc_b004_partidas_contrato.num_monto,
        gc_b004_partidas_contrato.ind_partida,
        pr_b002_partida_presupuestaria.ind_denominacion,
        gc_b004_partidas_contrato.fec_anio ,
        pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria 
        
        FROM `gc_b004_partidas_contrato`
        
         left join pr_b002_partida_presupuestaria on (pr_b002_partida_presupuestaria.cod_partida=gc_b004_partidas_contrato.ind_partida)
        
        left join pr_b004_presupuesto on (pr_b004_presupuesto.fec_anio='" . date('Y') . "')
        
        left join pr_c002_presupuesto_det on (pr_c002_presupuesto_det.fk_prb002_num_partida_presupuestaria=pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria AND pr_b004_presupuesto.pk_num_presupuesto=pr_c002_presupuesto_det.fk_prb004_num_presupuesto)
        
        
        WHERE `fk_gcb001_registro_contacto`='" . $id . "'
      
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metGetPresupuestoDetPartida($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query("
               SELECT
        pr_b004_presupuesto.pk_num_presupuesto,
        pr_c002_presupuesto_det.pk_num_presupuesto_det,
        pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_pub20,
        pr_b002_partida_presupuestaria.fk_cbb004_num_plan_cuenta_onco 
         
        
        FROM pr_b002_partida_presupuestaria
        

        
        left join pr_b004_presupuesto on (pr_b004_presupuesto.fec_anio='2017')
        
        left join pr_c002_presupuesto_det on (pr_c002_presupuesto_det.fk_prb002_num_partida_presupuestaria=pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria AND pr_b004_presupuesto.pk_num_presupuesto=pr_c002_presupuesto_det.fk_prb004_num_presupuesto)
          
        
        WHERE pr_b002_partida_presupuestaria.cod_partida='" . $id . "'
      
        "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetch();

    }

    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrarSelectTipo($id)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT * FROM `gc_c003_tipo_contrato` where fk_a004_dependencia='" . $id . "'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para mostrar las filas en una consulta y asi determinar el numero de correlativo
    public function metObtenerCorrelativo($id)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT * FROM `gc_c003_tipo_contrato` where fk_a004_dependencia='" . $id . "'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para mostrar las partidas relacionadas con el proyecto de contrato
    public function metObtenerPartidasSeleccionadas($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT gc_b004_partidas_contrato.*,pr_b002_partida_presupuestaria.ind_denominacion FROM `gc_b004_partidas_contrato`
             left join pr_b002_partida_presupuestaria on (pr_b002_partida_presupuestaria.cod_partida=gc_b004_partidas_contrato.ind_partida)
             where fk_gcb001_registro_contacto='" . $id . "'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para obtener el id pk de la  partida
    public function metObtenerIdPartida($partida, $detalle)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT gc_b004_partidas_contrato.*,pr_b002_partida_presupuestaria.ind_denominacion FROM `gc_b004_partidas_contrato`
             left join pr_b002_partida_presupuestaria on (pr_b002_partida_presupuestaria.cod_partida=gc_b004_partidas_contrato.ind_partida)
             where fk_gcb001_registro_contacto='" . $id . "'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    #metodo para mostrar todos los registros con referencia a un pk
    public function metMostrarSelectAplicable($id)
    {

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT pk_num_aplicable_contrato,ind_descripcion FROM `gc_c004_aplicable_contrato` where fk_gcc003_num_tipo_contrato='" . $id . "'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    #metodo para mostrar ingresar nombre de la persona apartir de la cedula recogida
    public function metMostrarNombre($documento, $tipo)
    {
        if ($tipo < 3) {
            $complemento = " AND a003_persona.ind_cedula_documento='" . $documento . "' ";
        } else {
            if ($tipo == 3) {
                $documento = "J-" . $documento;
            } else {
                $documento = "G-" . $documento;
            }
            $complemento = " AND a003_persona.ind_documento_fiscal='" . $documento . "'    ";
        }

        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "SELECT pk_num_persona,ind_nombre1,ind_apellido1 FROM a003_persona

             LEFT JOIN lg_b022_proveedor on (lg_b022_proveedor.fk_a003_num_persona_proveedor=a003_persona.pk_num_persona)

             where 1 $complemento  "
        );


        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }


    public function metBuscarRepresentante($id)
    {


        $pruebaPost = $this->_db->query(
            "SELECT lg_b022_proveedor.fk_a003_num_persona_representante	,lg_b022_proveedor.pk_num_proveedor, a003_persona.pk_num_persona,a003_persona.ind_nombre1, a003_persona.ind_apellido1, a003_persona.ind_cedula_documento FROM `lg_b022_proveedor`
             inner join a003_persona on (a003_persona.pk_num_persona=lg_b022_proveedor.fk_a003_num_persona_representante)
             where  lg_b022_proveedor.pk_num_proveedor='" . $id . "'"
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }


    public function metPresupuestoContrato($id)
    {


        $pruebaPost = $this->_db->query(
            "SELECT     pr_d008_estado_distribucion.num_monto  AS Comprometido,
             pr_Causado.num_monto  AS Causado,
              pr_Pagado.num_monto  AS Pagado ,
             gc_b004_partidas_contrato.ind_partida
             
             FROM  gc_b004_partidas_contrato 
              
             LEFT JOIN pr_d008_estado_distribucion ON (pr_d008_estado_distribucion.fk_gcb001_num_contrato=gc_b004_partidas_contrato.fk_gcb001_registro_contacto AND pr_d008_estado_distribucion.ind_tipo_distribucion='CO' AND gc_b004_partidas_contrato.fk_prc002_num_presupuesto_det=pr_d008_estado_distribucion.fk_prc002_num_presupuesto_det)
             
             LEFT JOIN pr_d008_estado_distribucion AS pr_Causado ON (pr_Causado.fk_gcb001_num_contrato=gc_b004_partidas_contrato.fk_gcb001_registro_contacto AND pr_Causado.ind_tipo_distribucion='CA'
                                                                    AND gc_b004_partidas_contrato.fk_prc002_num_presupuesto_det=pr_Causado.fk_prc002_num_presupuesto_det
                                                                    )
              
              
             LEFT JOIN pr_d008_estado_distribucion AS pr_Pagado ON (pr_Pagado.fk_gcb001_num_contrato=gc_b004_partidas_contrato.fk_gcb001_registro_contacto AND pr_Pagado.ind_tipo_distribucion='PA'
                                                                       AND gc_b004_partidas_contrato.fk_prc002_num_presupuesto_det=pr_Pagado.fk_prc002_num_presupuesto_det
                                              
                                                                   )
             
             
             WHERE gc_b004_partidas_contrato.fk_gcb001_registro_contacto='" . $id . "'
 "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metPresupuestoContratoTotal($id)
    {


        $pruebaPost = $this->_db->query(
            "SELECT gc_b001_registro_contrato.pk_num_registro_contrato ,SUM(pr_d008_estado_distribucion.num_monto) AS Comprometido,
             SUM(pr_Causado.num_monto) AS Causado,
             SUM(pr_Pagado.num_monto) AS Pagado 
             
             
             FROM gc_b001_registro_contrato 
             
              
             LEFT JOIN pr_d008_estado_distribucion ON (pr_d008_estado_distribucion.fk_gcb001_num_contrato=gc_b001_registro_contrato.pk_num_registro_contrato AND pr_d008_estado_distribucion.ind_tipo_distribucion='CO')
             
             LEFT JOIN pr_d008_estado_distribucion AS pr_Causado ON (pr_Causado.fk_gcb001_num_contrato=gc_b001_registro_contrato.pk_num_registro_contrato AND pr_Causado.ind_tipo_distribucion='CA')
              
              
             LEFT JOIN pr_d008_estado_distribucion AS pr_Pagado ON (pr_Pagado.fk_gcb001_num_contrato=gc_b001_registro_contrato.pk_num_registro_contrato AND pr_Pagado.ind_tipo_distribucion='PA')
             
             
             WHERE gc_b001_registro_contrato.`pk_num_registro_contrato`='" . $id . "'
 "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }


    public function metGetProveedores()
    {


        $pruebaPost = $this->_db->query(
            "SELECT lg_b022_proveedor.pk_num_proveedor, a003_persona.ind_nombre1 FROM `a003_persona`
             inner join lg_b022_proveedor on (lg_b022_proveedor.fk_a003_num_persona_proveedor=a003_persona.pk_num_persona)
            "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }


    public function metGetEstatusId($id)
    {


        $pruebaPost = $this->_db->query(
            "SELECT fk_a006_num_estado_contrato FROM `gc_b001_registro_contrato`
             WHERE pk_num_registro_contrato='$id' 
            "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }

    public function metSetProyecto($SolicitadoPor, $Tipo, $Aplicable, $Objeto, $Monto, $idProveedor, $idRepresentante, $desde, $hasta, $entrada, $formaPago, $lugarPago, $obligacionesContrante, $obligacionesContratado, $observacion, $movimiento, $guia, $addendum,$tipo_servicio)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost = $this->_db->prepare(
            "insert into gc_b001_registro_contrato (
             ind_objeto_contrato,
             num_monto_contrato,
             fk_gcc004_num_aplicable_contrato,
             fk_a006_num_estado_contrato,
             fec_ultima_modificacion,
             fk_a018_num_seguridad_usuario,
             fk_a004_dependencia,
             fk_gcc003_num_tipo_contrato,
             fk_a006_num_forma_pago,
             num_anio,
             fk_a003_proveedor,
             fk_a003_representante,
             num_guia_contrato,
             fk_gcb001_addendum,
             fk_cpb017_tipo_servicio
             )
             values(
            :ind_objeto_contrato,
            :num_monto_contrato,
            :fk_gcc004_num_aplicable_contrato,
            :fk_a006_num_estado_contrato,
            :fec_ultima_modificacion,
            :fk_a018_num_seguridad_usuario,
            :fk_a004_dependencia,
            :fk_gcc003_num_tipo_contrato,
            :fk_a006_num_forma_pago,
            :num_anio,
            :fk_a003_proveedor,
            :fk_a003_representante,
            :num_guia_contrato,
            :fk_gcb001_addendum,
            :fk_cpb017_tipo_servicio
            )"
        );


        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(

            ':ind_objeto_contrato' => $Objeto,
            ':num_monto_contrato' => $Monto,
            ':fk_gcc004_num_aplicable_contrato' => $Aplicable,
            ':fk_a006_num_estado_contrato' => '1',
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuarioModelo,
            ':fk_a004_dependencia' => $SolicitadoPor,
            ':fk_gcc003_num_tipo_contrato' => $Tipo,
            ':num_anio' => date('Y'),
            ':fk_a006_num_forma_pago' => $formaPago,
            ':fk_a003_proveedor' => $idProveedor,
            ':fk_a003_representante' => $idRepresentante,
            ':num_guia_contrato' => $guia,
            ':fk_gcb001_addendum' => $addendum,
            ':fk_cpb017_tipo_servicio'=> $tipo_servicio,

        ));
        $error = $NuevoPost->errorInfo();
        $id = $this->_db->lastInsertId();


        $consulta = $this->_db->prepare(
            "insert into gc_b002_registro_contrato_detalle (
             fk_gcb001_num_registro_contrato,
             fec_desde,
             fec_hasta,
             fec_entrada,
             ind_obligaciones_contratante,
             ind_obligaciones_contratado,
             ind_observacion,
             ind_lugar_pago,
             fk_a018_num_seguridad_usuario,
             fec_ultima_modificacion

             )
             values (
             :fk_gcb001_num_registro_contrato,
             :fec_desde,
             :fec_hasta,
             :fec_entrada,
             :ind_obligaciones_contratante,
             :ind_obligaciones_contratado,
             :ind_observacion,
             :ind_lugar_pago,
             :fk_a018_num_seguridad_usuario,
             :fec_ultima_modificacion
             )"
        );


        $consulta->execute(array(
            ':fk_gcb001_num_registro_contrato' => $id,
            ':fec_desde' => $desde,
            ':fec_hasta' => $hasta,
            ':fec_entrada' => $entrada,
            ':ind_obligaciones_contratante' => $obligacionesContrante,
            ':ind_obligaciones_contratado' => $obligacionesContratado,
            ':ind_observacion' => $observacion,
            ':ind_lugar_pago' => $lugarPago,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuarioModelo,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),


        ));

        $error2 = $consulta->errorInfo();


        $consulta = $this->_db->prepare(
            "insert into gc_c005_bitacora (
             fk_a006_miscelaneo,

             fk_gcb001_num_registro_contrato,
             fec_ultima_modificacion,
             fk_a018_num_seguridad_usuario


             )
             values (
             :fk_a006_miscelaneo,

             :fk_gcb001_num_registro_contrato,
             :fec_ultima_modificacion,
             :fk_a018_num_seguridad_usuario

             )"
        );

        $consulta->execute(array(
            ':fk_a006_miscelaneo' => $movimiento,
            ':fk_gcb001_num_registro_contrato' => $id,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuarioModelo
        ));


        $error3 = $consulta->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else if (!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        } else if (!empty($error3[1]) && !empty($error3[2])) {
            $this->_db->rollBack();
            return $error3;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $id;
        }


    }


    public function metContratoComprometer($monto, $idContrato, $detalle)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $consulta = $this->_db->prepare(
            "insert into pr_d008_estado_distribucion (
             fec_periodo,
             num_monto,
             fk_prc002_num_presupuesto_det,
             fk_gcb001_num_contrato

             )
             values (
             :fec_periodo,
             :num_monto,
             :fk_prc002_num_presupuesto_det,
             :fk_gcb001_num_contrato

             )"
        );

        $consulta->execute(array(
            ':fec_periodo' => date('Y-m-d'),
            ':num_monto' => $monto,
            ':fk_prc002_num_presupuesto_det' => $detalle,
            ':fk_gcb001_num_contrato' => $idContrato
        ));


        $error = $consulta->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #metodo para modificar el registro del post
    public function metUpdateProyecto($Aplicable, $objeto, $num_monto, $dependencia, $TipoContrato, $formaPago, $CodProveedor, $CodRepresentante, $cuerpo, $idRegistro, $movimiento, $guia)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar = $this->_db->prepare(
            "UPDATE gc_b001_registro_contrato
              SET
              fk_gcc004_num_aplicable_contrato=:fk_gcc004_num_aplicable_contrato,
              ind_objeto_contrato=:ind_objeto_contrato,
              num_monto_contrato=:num_monto_contrato,

              fk_a004_dependencia=:fk_a004_dependencia,
              fk_gcc003_num_tipo_contrato=:fk_gcc003_num_tipo_contrato,
              fk_a006_num_forma_pago=:fk_a006_num_forma_pago,
              fk_a003_proveedor=:fk_a003_proveedor,
              fk_a003_representante=:fk_a003_representante,
 	          fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
 	          ind_cuerpo=:ind_cuerpo,
 	          fec_ultima_modificacion=:fec_ultima_modificacion,
 	          num_guia_contrato=:num_guia_contrato

              WHERE pk_num_registro_contrato='" . $idRegistro . "'"
        );


        $modificar->execute(array(
            ':fk_gcc004_num_aplicable_contrato' => $Aplicable,
            ':ind_objeto_contrato' => $objeto,
            ':num_monto_contrato' => $num_monto,

            ':fk_a004_dependencia' => $dependencia,
            ':fk_gcc003_num_tipo_contrato' => $TipoContrato,
            ':fk_a006_num_forma_pago' => $formaPago,
            ':fk_a003_proveedor' => $CodProveedor,
            ':fk_a003_representante' => $CodRepresentante,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuarioModelo,
            ':ind_cuerpo' => $cuerpo,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':num_guia_contrato' => $guia
        ));


        $consulta = $this->_db->prepare(
            "insert into gc_c005_bitacora (
             fk_a006_miscelaneo,
             fk_gcb001_num_registro_contrato,
             fec_ultima_modificacion,
             fk_a018_num_seguridad_usuario
             )
             values (
             :fk_a006_miscelaneo,
             :fk_gcb001_num_registro_contrato,
             :fec_ultima_modificacion,
             :fk_a018_num_seguridad_usuario

             )"
        );

        $consulta->execute(array(
            ':fk_a006_miscelaneo' => $movimiento,
            ':fk_gcb001_num_registro_contrato' => $idRegistro,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuarioModelo
        ));


        #commit — Consigna una transacción
        $this->_db->commit();


        return 1;
    }

    #metodo para modificar el registro del post
    public function metUpdateProyectoEstatus($idRegistro, $Estatus, $movimiento, $cuerpo = false, $correlativo = false)
    {
        if ($Estatus == 2) {

            $complemento = ", ind_cuerpo='" . $cuerpo . "'";
        } else if ($Estatus == 5) {

            $complemento = ", ind_correlativo_contrato='" . $correlativo . "' ";
        } else {

            $complemento = '';
        }

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar = $this->_db->prepare(
            "UPDATE gc_b001_registro_contrato
              SET
      
 	          fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
               
 	          fk_a006_num_estado_contrato=:fk_a006_num_estado_contrato,
 	          fec_ultima_modificacion=:fec_ultima_modificacion
              $complemento
              WHERE pk_num_registro_contrato='" . $idRegistro . "'"
        );
        $modificar->execute(array(

            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuarioModelo,
            ':fk_a006_num_estado_contrato' => $Estatus,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
        ));


        $consulta = $this->_db->prepare(
            "insert into gc_c005_bitacora (
             fk_a006_miscelaneo,

             fk_gcb001_num_registro_contrato,
             fec_ultima_modificacion,
             fk_a018_num_seguridad_usuario


             )
             values (
             :fk_a006_miscelaneo,

             :fk_gcb001_num_registro_contrato,
             :fec_ultima_modificacion,
             :fk_a018_num_seguridad_usuario

             )"
        );

        $consulta->execute(array(
            ':fk_a006_miscelaneo' => $movimiento,
            ':fk_gcb001_num_registro_contrato' => $idRegistro,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuarioModelo
        ));


        #commit — Consigna una transacción
        $this->_db->commit();


        return 1;
    }

    public function metEliminarPartidas($id)
    {
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "delete from `gc_b004_partidas_contrato` " .
            "where fk_gcb001_registro_contacto= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
        return 1;
    }

    public function metEliminar($id)
    {
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            " DELETE FROM `gc_b001_registro_contrato` WHERE `pk_num_registro_contrato`='" . $id . "'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
        return 1;
    }

    public function metEliminarProyecto($id)
    {
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $this->_db->query(
            "gc_b001_registro_contrato` WHERE pk_num_registro_contacto= '$id'"
        );
        #commit — Consigna una transacción
        $this->_db->commit();
        return 1;
    }


    public function metAsignarPartidas($partida, $monto, $monto_ajustado, $detalle, $pub20, $oncop, $id)
    {

        $this->_db->beginTransaction();

        $consulta = $this->_db->prepare(
            "insert into gc_b004_partidas_contrato (
             ind_partida,

             num_monto,
             num_monto_ajustado,
             fk_gcb001_registro_contacto,
             fk_prc002_num_presupuesto_det,
             fec_anio,
             fk_cbb004_num_cuenta_pub20,
             fk_cbb004_num_cuenta_onco
             )
             values (
             :ind_partida,
             :num_monto,
             :num_monto_ajustado,
             :fk_gcb001_registro_contacto,
             :fk_prc002_num_presupuesto_det,
             :fec_anio,
             :fk_cbb004_num_cuenta_pub20,
             :fk_cbb004_num_cuenta_onco

             )"
        );


        $consulta->execute(array(
            ':ind_partida' => $partida,
            ':num_monto' => $monto,
            ':num_monto_ajustado' => $monto_ajustado,
            ':fk_gcb001_registro_contacto' => $id,
            ':fk_prc002_num_presupuesto_det' => $detalle,
            ':fec_anio' => date("Y"),
            ':fk_cbb004_num_cuenta_pub20' => $pub20,
            ':fk_cbb004_num_cuenta_onco' => $oncop

        ));


        #commit — Consigna una transacción
        $this->_db->commit();


        return 1;


    }

    #función agregada mientras se desarrolla la propia en el modPR
    public function metListarPartida($idpresupuesto, $fec_anio, $cod_partida)
    {
        if ($idpresupuesto != "error") {
            $complemento1 = " AND pbp.pk_num_presupuesto='" . $idpresupuesto . "'";
        } else {
            $complemento1 = " ";
        }


        if ($fec_anio != "error") {
            $complemento2 = " AND pbp.fec_anio='" . $fec_anio . "'";
        } else {
            $complemento2 = " ";
        }


        if ($cod_partida != "error") {
            $complemento3 = " AND pnpp.cod_partida like '%" . $cod_partida . "%'";
        } else {
            $complemento3 = " ";
        }


        #ejecuto la consulta a la base de datos
        $partida = $this->_db->query("

		  SELECT pbp.ind_cod_presupuesto,pcpd.num_monto_aprobado, pnpp.cod_partida, pnpp.pk_num_partida_presupuestaria,pnpp.ind_denominacion,pcpd.*, @num:=pcpd.pk_num_presupuesto_det,


            (SELECT sum(gc_b004_partidas_contrato.num_monto) as total FROM gc_b004_partidas_contrato
             WHERE gc_b004_partidas_contrato.ind_partida=pnpp.cod_partida AND fec_anio='" . $fec_anio . "')as monto_pre,

			(SELECT
                SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
            INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  ind_tipo_distribucion='CO' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
            AS compromiso_dist,

			(SELECT
                SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
            INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  ind_tipo_distribucion='CA' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
            AS causado_dist,

			(SELECT
                SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
             INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
             WHERE  ind_tipo_distribucion='PA' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=@num)
             AS pagado_dist

	      FROM pr_c002_presupuesto_det pcpd
			    INNER JOIN pr_b004_presupuesto pbp ON (pbp.pk_num_presupuesto=pcpd.fk_prb004_num_presupuesto)
			    INNER JOIN pr_b002_partida_presupuestaria pnpp ON (pcpd.fk_prb002_num_partida_presupuestaria=pnpp.pk_num_partida_presupuestaria)
	      WHERE 1 $complemento1 $complemento2  $complemento3 ORDER BY cod_partida ASC"

        );


        $partida->setFetchMode(PDO::FETCH_ASSOC);
        return $partida->fetchAll();
    }


    public function metPreComprometidoPartida($partida, $anio)
    {


        $pruebaPost = $this->_db->query(
            "SELECT sum(num_monto) as total FROM `gc_b004_partidas_contrato`
             WHERE ind_partida='" . $partida . "' AND fec_anio='" . $anio . "'
            "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();

    }


    public function metUpdateProyectoDetalle($desde, $hasta, $entrada, $obligacionesContrante, $obligacionesContratado, $observacion, $lugar_pago, $id)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar = $this->_db->prepare(
            "UPDATE gc_b002_registro_contrato_detalle
              SET
              fec_desde=:fec_desde,
              fec_hasta=:fec_hasta,
              fec_entrada=:fec_entrada,
              ind_obligaciones_contratante=:ind_obligaciones_contratante,
              ind_obligaciones_contratado=:ind_obligaciones_contratado,
              ind_observacion=:ind_observacion,
              ind_lugar_pago=:ind_lugar_pago,
 	          fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
 	          fec_ultima_modificacion=:fec_ultima_modificacion

              WHERE fk_gcb001_num_registro_contrato='" . $id . "'"
        );


        $modificar->execute(array(
            ':fec_desde' => $desde,
            ':fec_hasta' => $hasta,
            ':fec_entrada' => $entrada,
            ':ind_obligaciones_contratante' => $obligacionesContrante,
            ':ind_obligaciones_contratado' => $obligacionesContratado,
            ':ind_observacion' => $observacion,
            ':ind_lugar_pago' => $lugar_pago,
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuarioModelo,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s'),

        ));


        #commit — Consigna una transacción
        $this->_db->commit();


        return 1;
    }


    #metodo para modificar el registro del post
    public function metBajar($id, $estatus)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar = $this->_db->prepare(
            "UPDATE gc_b001_registro_contrato
              SET
              fk_a006_num_estado_contrato=:fk_a006_num_estado_contrato,
   	          fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
 	          fec_ultima_modificacion=:fec_ultima_modificacion

              WHERE pk_num_registro_contrato='" . $id . "'"
        );
        $modificar->execute(array(
            ':fk_a006_num_estado_contrato' => ($estatus),
            ':fk_a018_num_seguridad_usuario' => $this->atIdUsuarioModelo,
            ':fec_ultima_modificacion' => date('Y-m-d H:i:s')
        ));


        #commit — Consigna una transacción
        $this->_db->commit();


        return 1;
    }

    #metodo para modificar el registro del post
    public function metAsignarCorrelativo($Correlativo, $id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar = $this->_db->prepare(
            "UPDATE gc_b001_registro_contrato
              SET
              ind_correlativo=:ind_correlativo

              WHERE pk_num_registro_contrato='" . $id . "'"
        );
        $modificar->execute(array(
            ':ind_correlativo' => $Correlativo,

        ));

        #commit — Consigna una transacción
        $this->_db->commit();

        return 1;
    }

    #metodo para modificar el registro del post
    public function metUpdateContratoPresupuesto($id)
    {

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $modificar = $this->_db->prepare(
            "UPDATE gc_b001_registro_contrato
              SET
              num_estado_presupuesto=:num_estado_presupuesto

              WHERE pk_num_registro_contrato='" . $id . "'"
        );
        $modificar->execute(array(
            ':num_estado_presupuesto' => 1,

        ));

        #commit — Consigna una transacción
        $this->_db->commit();

        return 1;
    }

    #metodo para modificar el registro del post
    public function metRelacionarContrato($idContrato, $idObligacion)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia

        $consulta = $this->_db->prepare(
            "insert into gc_c001_relacion_obligacion (
             fk_gcb001_registro_contrato,
             fk_cpd001_obligacion
             )
             values (
             :fk_gcb001_registro_contrato,
             :fk_cpd001_obligacion
             )"
        );

        $consulta->execute(array(
            ':fk_gcb001_registro_contrato' => $idContrato,
            ':fk_cpd001_obligacion' => $idObligacion,

        ));

        #commit — Consigna una transacción
        $this->_db->commit();


        return 1;
    }


    public function metListarObligacion($id)
    {

        $obligacionListar = $this->_db->query("
            SELECT
                gc_c001_relacion_obligacion.fk_gcb001_registro_contrato as pk_num_registro_contrato,
                gc_c001_relacion_obligacion.fk_cpd001_obligacion,
                cp_d001_obligacion.pk_num_obligacion,
                cp_d001_obligacion.num_monto_obligacion,
                cp_d001_obligacion.fec_documento,
                cp_d001_obligacion.ind_nro_factura,
                gc_b001_registro_contrato.num_monto_contrato,

                cp_d001_obligacion.ind_estado AS estado,
                cp_b002_tipo_documento.cod_tipo_documento,

                (CASE
                        WHEN cp_d001_obligacion.ind_estado='PR' THEN 'PREPARADO'
                        WHEN cp_d001_obligacion.ind_estado='RE' THEN 'REVISADO'
                        WHEN cp_d001_obligacion.ind_estado='CO' THEN 'CONFORMADO'
                        WHEN cp_d001_obligacion.ind_estado='AP' THEN 'APROBADO'
                        WHEN cp_d001_obligacion.ind_estado='AN' THEN 'ANULADO'
                END) AS ind_estado, 
                CONCAT(
                    IF(a003_persona.ind_nombre1,'',a003_persona.ind_nombre1),
                    ' ',
                    IF(a003_persona.ind_apellido1,'',a003_persona.ind_apellido1)
                ) AS fk_a003_num_persona_proveedor
            FROM
               gc_c001_relacion_obligacion

              INNER JOIN cp_d001_obligacion on  cp_d001_obligacion.pk_num_obligacion = gc_c001_relacion_obligacion.fk_cpd001_obligacion
              INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
              INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor = a003_persona.pk_num_persona
              INNER JOIN gc_b001_registro_contrato ON (gc_b001_registro_contrato.pk_num_registro_contrato=gc_c001_relacion_obligacion.fk_gcb001_registro_contrato)


            WHERE

            fk_gcb001_registro_contrato = $id

            ORDER BY cp_d001_obligacion.fec_documento
        ");


        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();


    }


    public function metListarOrdenContratos($idContrato)
    {

        $requerimientoPost = $this->_db->query("
             SELECT gc_c002_relacion_orden.fk_lgb019_orden,
                    orden.*,
                                    orden.ind_estado AS estatus,
                                    persona.*,
                                    concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS provNom,
                                    almacen.ind_descripcion AS nomAlmacen,
                                    perceptivo.*,
                                    (SELECT SUM(oc1.num_monto_total)
                                    FROM lg_b019_orden oc1
                                    WHERE oc1.fk_lgb022_num_proveedor = orden.fk_lgb022_num_proveedor
                                    GROUP BY fk_lgb022_num_proveedor) AS TotalProveedor
                    
                    
                    FROM `gc_c002_relacion_orden`
                    
                    left join lg_b019_orden as orden on (orden.pk_num_orden=gc_c002_relacion_orden.fk_lgb019_orden)
                    LEFT JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
                                LEFT JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                                LEFT JOIN lg_b014_almacen AS almacen ON orden.fk_lgb014_num_almacen = almacen.pk_num_almacen
                                LEFT JOIN lg_b020_control_perceptivo AS perceptivo ON orden.pk_num_orden = perceptivo.fk_lgb019_num_orden
                        
                                 
                    WHERE fk_gcb001_registro_contrato='$idContrato '
                    
                    GROUP BY orden.pk_num_orden
             ORDER BY orden.pk_num_orden DESC
        
         
             ");

        $requerimientoPost->setFetchMode(PDO::FETCH_ASSOC);

        return $requerimientoPost->fetchAll();

    }


    public function metMontosObligacion($id, $tipo)
    {

        $monto = $this->_db->query("
           SELECT sum( num_monto ) as suma

           FROM pr_d008_estado_distribucion

           WHERE ind_tipo_distribucion='" . $tipo . "' AND  fk_cpd001_num_obligacion='$id'


        ");


        $monto->setFetchMode(PDO::FETCH_ASSOC);
        return $monto->fetchAll();
    }


    public function metConsultarProyectoContratos($anio, $dependencia)
    {


        $pruebaPost = $this->_db->query(
            "SELECT `pk_num_registro_contrato`  FROM `gc_b001_registro_contrato` WHERE num_anio='" . $anio . "' AND fk_a004_dependencia='" . $dependencia . "'
            "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->rowCount();

    }

    public function metContarObligacion($id)
    {


        $Post = $this->_db->query(
            "SELECT `pk_num_relacion`  FROM `gc_c001_relacion_obligacion` WHERE fk_gcb001_registro_contrato='" . $id . " '
            "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $Post->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $Post->rowCount();

    }

    public function metMostrarObligacion($id)
    {


        $Post = $this->_db->query(
            "SELECT
             gc_c001_relacion_obligacion.fk_cpd001_obligacion,
             cp_d013_obligacion_cuentas.fk_prb002_num_partida_presupuestaria,
             pr_c002_presupuesto_det.pk_num_presupuesto_det

             FROM `gc_c001_relacion_obligacion`
             left join cp_d013_obligacion_cuentas on (cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion=gc_c001_relacion_obligacion.fk_cpd001_obligacion)
             left join pr_c002_presupuesto_det on (pr_c002_presupuesto_det.fk_prb002_num_partida_presupuestaria=cp_d013_obligacion_cuentas.fk_prb002_num_partida_presupuestaria)

             WHERE gc_c001_relacion_obligacion.fk_gcb001_registro_contrato='" . $id . "'
            "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $Post->setFetchMode(PDO::FETCH_ASSOC);
        return $Post->fetchAll();

    }

    public function metConsultarContratos($anio, $dependencia)
    {

        $pruebaPost = $this->_db->query(
            "SELECT `pk_num_registro_contrato`  FROM `gc_b001_registro_contrato` WHERE num_anio='" . $anio . "' AND fk_a004_dependencia='" . $dependencia . "' AND fk_a006_num_estado_contrato='5'
            "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->rowCount();

    }

    public function metConsultarAddendums($anio, $id)
    {

        $pruebaPost = $this->_db->query(
            "SELECT `pk_num_registro_contrato`  FROM `gc_b001_registro_contrato` WHERE num_anio='" . $anio . "' AND fk_gcb001_addendum='" . $id . "' AND fk_a006_num_estado_contrato='5'
            "
        );

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->rowCount();

    }

    public function metConsultarDistribucion($id)
    {
        $pruebaPost = $this->_db->query(
            "SELECT `pk_num_estado_distribucion`  FROM `pr_d008_estado_distribucion` WHERE fk_cpd001_num_obligacion='$id'
            "
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->rowCount();

    }

    public function metNuevaObligacion($form, $procedencia, $distribucionManual = 1)
    {
        $this->_db->beginTransaction();
        $secuencia = $this->metSecuencia('cp_d001_obligacion', 'ind_nro_registro');
        $procedencia = 'GC';
        $NuevoRegistro = $this->_db->prepare("
            INSERT INTO
                cp_d001_obligacion
            SET
                fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                fk_a003_num_persona_proveedor_a_pagar=:fk_a003_num_persona_proveedor_a_pagar,
                fk_a023_num_centro_de_costo=:fk_a023_num_centro_de_costo,
                fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                fk_a006_num_miscelaneo_tipo_pago=:fk_a006_num_miscelaneo_tipo_pago,
                fk_cpb014_num_cuenta=:fk_cpb014_num_cuenta,
                fk_a018_num_seguridad_usuario='$this->atIdUsuarioModelo',
                fk_rhb001_num_empleado_ingresa='$this->atIdEmpleado',
                fec_programada=:fec_programada,
                fec_factura=:fec_factura,
                fec_recepcion=:fec_recepcion,
                fec_vencimiento=:fec_vencimiento,
                fec_registro=:fec_registro,
                fec_anio=YEAR(NOW()),
                fec_mes=LPAD( MONTH (NOW()),2,'0'),
                fec_documento=NOW(),
                fec_preparacion=NOW(),
                fec_ultima_modificacion=NOW(),
                ind_nro_registro = LPAD( '" . $secuencia['secuencia'] . "',6,'0'),
                ind_nro_control=:ind_nro_control,
                ind_nro_factura=:ind_nro_factura,
                ind_comentarios=:ind_comentarios,
                ind_comentarios_adicional=:ind_comentarios_adicional,
                ind_tipo_descuento=:ind_tipo_descuento,
                ind_tipo_procedencia=:ind_tipo_procedencia,
                ind_estado='PR',
                num_flag_afecto_IGV=:num_flag_afecto_IGV,
                num_flag_diferido=:num_flag_diferido,
                num_flag_pago_diferido=:num_flag_pago_diferido,
                num_flag_compromiso=:num_flag_compromiso,
                num_flag_presupuesto=:num_flag_presupuesto,
                num_flag_obligacion_auto=:num_flag_obligacion_auto,
                num_flag_obligacion_directa=:num_flag_obligacion_directa,
                num_flag_caja_chica=:num_flag_caja_chica,
                num_flag_pago_individual=:num_flag_pago_individual,
                num_flag_generar_pago=:num_flag_generar_pago,
                num_flag_cont_pendiente_pub_20=:num_flag_cont_pendiente_pub_20,
                num_flag_verificado=:num_flag_verificado,
                num_flag_distribucion_manual=:num_flag_distribucion_manual,
                num_flag_adelanto=:num_flag_adelanto,
                num_monto_impuesto_otros=:num_monto_impuesto_otros,
                num_monto_no_afecto=:num_monto_no_afecto,
                num_monto_afecto=:num_monto_afecto,
                num_monto_adelanto=:num_monto_adelanto,
                num_monto_impuesto=:num_monto_impuesto,
                num_monto_pago_parcial=:num_monto_pago_parcial,
                num_monto_descuento=:num_monto_descuento,
                num_monto_obligacion=:num_monto_obligacion,
                num_proceso_secuencia=:num_proceso_secuencia,
                num_contabilizacion_pendiente=:num_contabilizacion_pendiente
        ");


        $NuevoRegistro->execute(array(
            'fk_a003_num_persona_proveedor' => $form['fk_a003_num_persona_proveedor'],
            'fk_a003_num_persona_proveedor_a_pagar' => $form['fk_a003_num_persona_proveedor_a_pagar'],
            'fk_a023_num_centro_de_costo' => $form['fk_a023_num_centro_de_costo'],
            'fk_cpb002_num_tipo_documento' => $form['fk_cpb002_num_tipo_documento'],
            'fk_cpb017_num_tipo_servicio' => $form['fk_cpb017_num_tipo_servicio'],
            'fk_a006_num_miscelaneo_tipo_pago' => $form['fk_a006_num_miscelaneo_tipo_pago'],
            'fk_cpb014_num_cuenta' => $form['fk_cpb014_num_cuenta'],
            'fec_programada' => $form['fec_programada'],
            'fec_factura' => $form['fec_factura'],
            'fec_recepcion' => $form['fec_recepcion'],
            'fec_vencimiento' => $form['fec_vencimiento'],
            'fec_registro' => $form['fec_registro'],
            'ind_nro_control' => $form['ind_nro_control'],
            'ind_nro_factura' => $form['ind_nro_factura'],
            'ind_comentarios' => $form['ind_comentarios'],
            'ind_comentarios_adicional' => $form['ind_comentarios_adicional'],
            'ind_tipo_descuento' => $form['ind_tipo_descuento'],
            'ind_tipo_procedencia' => $procedencia,#  de donde viene
            'num_flag_afecto_IGV' => $form['num_flag_afecto_IGV'],
            'num_flag_diferido' => $form['num_flag_diferido'],
            'num_flag_pago_diferido' => $form['num_flag_pago_diferido'],
            'num_flag_compromiso' => $form['num_flag_compromiso'],
            'num_flag_presupuesto' => $form['num_flag_presupuesto'],
            'num_flag_obligacion_auto' => $form['num_flag_obligacion_auto'],
            'num_flag_obligacion_directa' => $form['num_flag_obligacion_directa'],
            'num_flag_caja_chica' => $form['num_flag_caja_chica'],
            'num_flag_pago_individual' => $form['num_flag_pago_individual'],
            'num_flag_generar_pago' => 0,
            'num_flag_distribucion_manual' => $distribucionManual,#no se que hace
            'num_flag_adelanto' => 0,#no se que hace
            'num_flag_verificado' => 0,#no se que hace
            'num_flag_cont_pendiente_pub_20' => 0,
            'num_contabilizacion_pendiente' => 0,
            'num_proceso_secuencia' => 1,#no se que hace
            'num_monto_obligacion' => $form['num_monto_obligacion'],
            'num_monto_impuesto_otros' => $form['num_monto_impuesto_otros'],
            'num_monto_no_afecto' => $form['num_monto_no_afecto'],
            'num_monto_afecto' => $form['num_monto_afecto'],
            'num_monto_adelanto' => $form['num_monto_adelanto'],
            'num_monto_impuesto' => $form['num_monto_impuesto'],
            'num_monto_pago_parcial' => $form['num_monto_pago_parcial'],
            'num_monto_descuento' => $form['num_monto_descuento']
        ));
        $idObligacion = $this->_db->lastInsertId();

        #PARTIDAS partidaCuenta
        $registrarCuentas = $this->_db->prepare("
            INSERT INTO
                cp_d013_obligacion_cuentas
            SET
                fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                fk_a018_num_seguridad_usuario='$this->atIdUsuarioModelo',
                fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                fk_cbb004_num_cuenta=:fk_cbb004_num_cuenta,
                fk_cbb004_num_cuenta_pub20=:fk_cbb004_num_cuenta_pub20,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                fec_ultima_modificacion=NOW(),
                ind_descripcion=:ind_descripcion,
                num_flag_no_afecto=:num_flag_no_afecto,
                num_monto=:num_monto,
                num_secuencia=:num_secuencia
        ");
        $partidaCuenta = $form['partidaCuenta'];
        $secuencia = 1;
        foreach ($partidaCuenta['ind_secuencia'] AS $i) {
            $ids = $partidaCuenta[$i];
            if ($ids['ind_descripcion'] == '') {
                $ids['ind_descripcion'] = $form['ind_comentarios'];
            }
            if (!isset($ids['num_flag_no_afecto'])) {
                $ids['num_flag_no_afecto'] = 0;
            }
            if ($ids['fk_cbb004_num_cuenta'] == '') {
                $ids['fk_cbb004_num_cuenta'] = null;
            }
            if ($ids['fk_cbb004_num_cuenta_pub20'] == '') {
                $ids['fk_cbb004_num_cuenta_pub20'] = null;
            }
            if ($ids['fk_prb002_num_partida_presupuestaria'] == '') {
                $ids['fk_prb002_num_partida_presupuestaria'] = null;
            }
            $registrarCuentas->execute(array(
                'fk_a003_num_persona_proveedor' => $ids['fk_a003_num_persona_proveedor'],
                'fk_a023_num_centro_costo' => $ids['fk_a023_num_centro_costo'],
                'fk_cbb004_num_cuenta' => $ids['fk_cbb004_num_cuenta'],
                'fk_cbb004_num_cuenta_pub20' => $ids['fk_cbb004_num_cuenta_pub20'],
                'fk_prb002_num_partida_presupuestaria' => $ids['fk_prb002_num_partida_presupuestaria'],
                'fk_cpd001_num_obligacion' => $idObligacion,
                'ind_descripcion' => $ids['ind_descripcion'],
                'num_flag_no_afecto' => $ids['num_flag_no_afecto'],
                'num_monto' => $ids['num_monto'],
                'num_secuencia' => $secuencia
            ));


            $secuencia++;
        }

        #Impuestos impuestosRetenciones
        $registrarImpuestos = $this->_db->prepare("
            INSERT INTO
                cp_d012_obligacion_impuesto
            SET
                fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
                fk_a018_num_seguridad_usuario='$this->atIdUsuarioModelo',
                fk_cbb004_num_cuenta=:fk_cbb004_num_cuenta,
                fk_cbb004_num_cuenta_pub20=:fk_cbb004_num_cuenta_pub20,
                fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
                fk_nmb002_num_concepto=:fk_nmb002_num_concepto,
                fk_cpb015_num_impuesto=:fk_cpb015_num_impuesto,
                fec_ultima_modificacion=NOW(),
                ind_secuencia=:ind_secuencia,
                num_monto_impuesto=:num_monto_impuesto,
                num_monto_afecto=:num_monto_afecto
        ");

        if (!isset($form['impuestosRetenciones'])) {
            $impuestoRetenciones = false;
        } else {
            $impuestoRetenciones = $form['impuestosRetenciones'];
        }

        $secuencia = 1;
        if ($impuestoRetenciones) {
            foreach ($impuestoRetenciones['ind_secuencia'] AS $i) {
                $ids = $impuestoRetenciones[$i];
                if (isset($ids['fk_nmb002_num_concepto'])) {
                    $ids['fk_cpb015_num_impuesto'] = null;
                } else {
                    $ids['fk_nmb002_num_concepto'] = null;
                }

                if ($ids['fk_cbb004_num_cuenta'] == '') {
                    $ids['fk_cbb004_num_cuenta'] = null;
                }
                if ($ids['fk_cbb004_num_cuenta_pub20'] == '') {
                    $ids['fk_cbb004_num_cuenta_pub20'] = null;
                }

                $registrarImpuestos->execute(array(
                    'fk_a003_num_persona_proveedor' => $form['fk_a003_num_persona_proveedor'],
                    'fk_cbb004_num_cuenta' => $ids['fk_cbb004_num_cuenta'],
                    'fk_cbb004_num_cuenta_pub20' => $ids['fk_cbb004_num_cuenta_pub20'],
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'fk_nmb002_num_concepto' => $ids['fk_nmb002_num_concepto'],
                    'fk_cpb015_num_impuesto' => $ids['fk_cpb015_num_impuesto'],
                    'ind_secuencia' => $secuencia,
                    'num_monto_impuesto' => $ids['num_monto_impuesto'],
                    'num_monto_afecto' => $ids['num_monto_afecto']
                ));
                $secuencia++;
            }
        }

        #Documentos
        if (!isset($form['documentos'])) {
            $documentos = false;
        } else {
            $documentos = $form['documentos'];
        }

        $registrarDocumentos = $this->_db->prepare("
            INSERT INTO
                cp_d002_documento_obligacion
            SET
              fk_a018_num_seguridad_usuario='$this->atIdUsuarioModelo',
              fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
              fk_lgb019_num_orden=:fk_lgb019_num_orden,
              fec_ultima_modificacion=NOW()
        ");
        if ($documentos) {
            foreach ($documentos['ind_secuencia'] AS $i) {
                $ids = $documentos[$i];
                $registrarDocumentos->execute(array(
                    'fk_cpd001_num_obligacion' => $idObligacion,
                    'fk_lgb019_num_orden' => $ids['fk_lgb019_num_orden'],
                ));
            }
        }

        $fallaTansaccion = $NuevoRegistro->errorInfo();
        $fallaTansaccion2 = $registrarCuentas->errorInfo();
        if ($impuestoRetenciones) {
            $fallaTansaccion3 = $registrarImpuestos->errorInfo();
        } else {
            $fallaTansaccion3 = array(1 => null, 2 => null);
        }
        if ($documentos) {
            $fallaTansaccion4 = $registrarDocumentos->errorInfo();
        } else {
            $fallaTansaccion4 = array(1 => null, 2 => null);
        }

        if ((!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } elseif ((!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion2;
        } elseif ((!empty($fallaTansaccion3[1]) && !empty($fallaTansaccion3[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion3;
        } elseif ((!empty($fallaTansaccion4[1]) && !empty($fallaTansaccion4[2]))) {
            $this->_db->rollBack();
            return $fallaTansaccion4;
        } else {
            $this->_db->commit();
            return $idObligacion;
        }
    }

    #metodo para mostrar las partidas relacionadas cuando se aprobó el proyecto de contrato
    public function metObtenerPartidasCargadas($id)
    {
        #ejecuto la consulta a la base de datos
        $pruebaPost = $this->_db->query(
            "select gc_b004_partidas_contrato.*,
              pr_b002_partida_presupuestaria.ind_denominacion as descripcionPartida,
              pr_b002_partida_presupuestaria.cod_partida as codigoPartida,
              cb_b004_plan_cuenta.ind_descripcion as descripcionCuenta,
              cb_b004_plan_cuenta.cod_cuenta as codigoCuenta,
              cb_b004_plan_cuenta20.ind_descripcion as descripcionCuenta20,
              cb_b004_plan_cuenta20.cod_cuenta as codigoCuenta20,
              gc_b004_partidas_contrato.fk_cbb004_num_cuenta_onco as fk_cbb004_num_cuenta,
              pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria as fk_prb002_num_partida_presupuestaria
              from gc_b004_partidas_contrato
              left join pr_b002_partida_presupuestaria on (pr_b002_partida_presupuestaria.cod_partida=gc_b004_partidas_contrato.ind_partida)                left join cb_b004_plan_cuenta on cb_b004_plan_cuenta.pk_num_cuenta = gc_b004_partidas_contrato.fk_cbb004_num_cuenta_onco
              left join cb_b004_plan_cuenta as  cb_b004_plan_cuenta20 on  cb_b004_plan_cuenta20.pk_num_cuenta = gc_b004_partidas_contrato.fk_cbb004_num_cuenta_pub20
              where fk_gcb001_registro_contacto='" . $id . "' order by gc_b004_partidas_contrato.pk_num_partidas asc");

        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.

        $pruebaPost->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $pruebaPost->fetchAll();
    }

    public function metBuscarPartidaCuenta($idPartida)
    {
        $año = date('Y');
        $persona = $this->_db->query("
            SELECT
              pr_b002_partida_presupuestaria.*,
              pr_c002_presupuesto_det.num_monto_compromiso,
              pr_c002_presupuesto_det.num_monto_ajustado,
              pr_c002_presupuesto_det.pk_num_presupuesto_det
            FROM
              pr_b002_partida_presupuestaria
            INNER JOIN pr_c002_presupuesto_det ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = pr_c002_presupuesto_det.fk_prb002_num_partida_presupuestaria
            INNER JOIN pr_b004_presupuesto ON pr_b004_presupuesto.pk_num_presupuesto = pr_c002_presupuesto_det.fk_prb004_num_presupuesto
            WHERE
              pk_num_partida_presupuestaria='$idPartida' AND
              pr_b004_presupuesto.fec_anio='$año' AND
              pr_b004_presupuesto.ind_estado='AP'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }


    public function metBuscarIdPartida($codPartida)
    {

        $persona = $this->_db->query("
            SELECT pk_num_partida_presupuestaria FROM `pr_b002_partida_presupuestaria` WHERE `cod_partida`='" . $codPartida . "' 
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);

        return $persona->fetch();
    }

}

