<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary"> LISTADO DE ENCABEZADOS </h2></header>
    </div>
    <div class="col-md-12">
        <h5>Listado de los encabezados relacionados con los proyectos de contrato.</h5>
    </div><!--end .col -->

</div><!--end .card -->


<div class="form-group   col-lg-12 ">

    <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
</div>

</div><!--end .card -->

<section class="style-default-bright">




    <!-- Listado -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-actionbar">

            </div>
        </div><!--end .col -->
    </div><!--end .row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">N° </th>
                        <th class="sort-alpha col-sm-2"> Dependencia </th>
                        <th class="sort-alpha col-sm-5"> Tipo de Contrato</th>


                        <th class=" col-sm-1">Editar</th>
                        <th class="sort-alpha col-sm-1"> Eliminar</th>


                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_EncabezadoPost}
                        <tr id="idEncabezado{$post.pk_num_predeterminado}" class="gradeA">
                            <td>{$post.pk_num_predeterminado}</td>
                            <td>{$post.ind_dependencia}</td>
                            <td>{$post.ind_descripcion}</td>
                            <td>{if in_array('GC-01-01-01-02-M',$_Parametros.perfil)}
                                    <button class="modificar   btn ink-reaction btn-raised btn-xs btn-primary" titulo="Modificar Encabezado del {$post.ind_descripcion}" descipcion="El Usuario a Modificado la redacción de un Proyecto de Contrato" idModificar="{$post.pk_num_predeterminado}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                    </button>
                                {/if}
                            </td>
                            <td>{if in_array('GC-01-01-01-02-M',$_Parametros.perfil)}
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"    idEncabezado="{$post.pk_num_predeterminado}" mensaje="Estas seguro que desea eliminar la el post!!" titulo="Estas Seguro?" descipcion="El usuario a eliminado un post" boton="si, Eliminar"  >
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>
                                {/if}
                            </td>


                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>

                        <th colspan="5">

                            {if in_array('GC-01-01-01-02-M',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info nuevo" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                        descripcion="el Usuario a registrado vehículo"  titulo="Registrar Encabezado" id="nuevo" >
                                    <i class="md md-create"></i>  Nuevo Encabezado &nbsp;&nbsp;&nbsp;
                                </button>
                            {/if}


                        </th>

                    </tr>
                    </tfoot>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->


    <script type="text/javascript">
        $(document).ready(function() {

            $('#modalAncho').css( "width", "85%" );

            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                var $url='{$_Parametros.url}modGC/encabezadoCONTROL/RegistrarEncabezadoMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idModificar'), Accion:"modificar"},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#nuevo').click( function () {

                var $url='{$_Parametros.url}modGC/encabezadoCONTROL/RegistrarEncabezadoMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost:0, Accion:"registrar"},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });


            $('#datatable1 tbody').on( 'click', '.eliminar', function () {

                var idEncabezado=$(this).attr('idEncabezado');
                swal({
                    title: $(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){
                    var $url='{$_Parametros.url}modGC/encabezadoCONTROL/EliminarEncabezadoMET';
                    $.post($url, { idEncabezado: idEncabezado},function($dato){
                        if($dato['status']=='OK'){
                            $(document.getElementById('idEncabezado'+$dato['idEncabezado'])).html('');
                            swal("Eliminado!", "Encabezado ha sido eliminado.", "success");
                            $('#cerrar').click();
                        }
                    },'json');
                });
            });


        });
    </script>
    </div>