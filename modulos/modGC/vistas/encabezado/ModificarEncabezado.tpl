<form action="" id="formAjax"class="form   form-validation" role="form"  >
    <input type="hidden" value="1" name="valido"  id="valido" />
    <input type="hidden" value="{$_EncabezadoPost[0]['fk_c003_tipo_contrato']}" name="tipoContrato"  id="tipoContrato" />
    <input type="hidden"  value="{if isset($_EncabezadoPost[0]['pk_num_predeterminado'])}{$_EncabezadoPost[0]['pk_num_predeterminado']}{/if}" name="idPost"  />







    <div class="col-lg-12">

    <div class="col-lg-7 form-group ">


            <select  required="" class="form-control    " name="form[int][idSolicitado]" id="idSolicitado" aria-required="true">
                <option value="">&nbsp;</option>

                {foreach item=tipo from=$_DependenciaPost}

                    {if isset($_EncabezadoPost[0]['fk_a004_dependencia'])}
                        {if $tipo.pk_num_dependencia==$_EncabezadoPost[0]['fk_a004_dependencia']}
                            <option selected value="{$tipo.pk_num_dependencia}">{$tipo.ind_dependencia}  </option>
                        {else}
                            <option value="{$tipo.pk_num_dependencia}">{$tipo.ind_dependencia}  </option>
                        {/if}
                    {else}
                        <option value="{$tipo.pk_num_dependencia}">{$tipo.ind_dependencia}  </option>
                    {/if}
                {/foreach}


            </select>
            <label for="rangelength2">Solicitado Por </label>


        </div>

        <div class="col-lg-7 form-group ">



                <select   required="" class="form-control" name="form[int][idTipoContrato]" id="idTipoContrato" aria-required="true">
                    <option value="">&nbsp;</option>

                    {foreach item=tipo from=$_TipoContratoPost}

                        {if isset($_EncabezadoPost[0]['fk_c003_tipo_contrato'])}

                            {if $tipo.pk_num_tipo_contrato==$_EncabezadoPost[0]['fk_c003_tipo_contrato']}
                                <option selected value="{$tipo.pk_num_tipo_contrato}"> {$tipo.ind_descripcion} </option>
                            {else}
                                <option value="{$tipo.pk_num_tipo_contrato}"> {$tipo.ind_descripcion}</option>
                            {/if}
                        {else}
                            <option value="{$tipo.pk_num_tipo_contrato}"> {$tipo.ind_descripcion} </option>
                        {/if}
                    {/foreach}


                </select>
                <label for="rangelength2"> Tipo de Contrato </label>




        </div>


        <div class="col-lg-12 form-group">


            <textarea name="form[txt][cuerpo]" id="ckeditor">{if isset($_EncabezadoPost[0]['ind_encabezado'])}{$_EncabezadoPost[0]['ind_encabezado']}{/if}</textarea>


        </div>
    </div>

<div class="modal-footer">

    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El Registro a sido cancelado" data-dismiss="modal">Cancelar</button>

    <button type="submit" id="boton" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" type="button">Guardar</button>



</div>
    </form>
        </div>





<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();


                $.post("{$_Parametros.url}modGC/encabezadoCONTROL/RegistrarEncabezadoMET", datos, function (dato) {
                    $(document.getElementById('idEncabezado'+dato[0]['idEncabezado'])).html( '<td>'+dato[0]['idEncabezado']+'</td>' +

                            '<td>'+dato[0]['ind_dependencia']+'</td>' +
                            '<td>'+dato[0]['tipoContrato']+'</td>' +
                            '<td   class="sort-alpha col-sm-1" >' +
                            '{if in_array('GC-01-01-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idModificar="'+dato[0]['idEncabezado']+'"' +
                            'descipcion="El Usuario a Modificado un Encabezado" titulo="Modificar Encabezado">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if} '+
                            '{if in_array('GC-01-01-01-02-M',$_Parametros.perfil)}<td   class="sort-alpha col-sm-1" ><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEncabezado="'+dato[0]['idEncabezado']+'"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado encabezado" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar encabezado?" >' +
                            '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                            '</td>' +
                            '</tr>');
                }, 'json');
                swal("Encabezado Modificado!", "El encabezado ha sido modificado satisfactoriamente!", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
                //  window.setTimeout('location.reload()', 3000);

        }
    });


    $(document).ready(function() {



        $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/ActualizarTipoContratoMET", { idSolicitado: $('#idSolicitado').val() }, function (data) {
            var json = data,
                    obj = JSON.parse(json);
            $("#idTipoContrato").html(obj);
            $("#idTipoContrato").val($("#tipoContrato").val())
        });


        $("#formAjax").validate();

        $( '#ckeditor' ).ckeditor();


        $("#idSolicitado").change(function (){

            $("#idSolicitado option:selected").each(function () {

                $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/ActualizarTipoContratoMET", { idSolicitado: $('#idSolicitado').val() }, function (data) {
                    var json = data,
                            obj = JSON.parse(json);
                    $("#idTipoContrato").html(obj);
                });
            });
        });



    });


</script>