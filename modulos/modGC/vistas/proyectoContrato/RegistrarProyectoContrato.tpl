<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2016       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
<!-- BEGIN VALIDATION FORM WIZARD -->
 

    <div class="card">

            <div class="card-body ">
                <div id="rootwizard2" class="form-wizard form-wizard-horizontal">
                 <form  id="formAjax"class="form   form-validation" role="form"   >



                <input type="hidden" value="1" name="valido"  id="valido" />
                     <input type="hidden" value="{if isset($_Addendum)}{$_Addendum}{/if}" name="form[int][addendum]" />
                <input type="hidden" value="0" name="NPartidasSeleccionadas"  id="NPartidasSeleccionadas" />

                <input type="hidden" value="0"   id="Factor" />
                <div class="form-wizard-nav">
                    <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
                    <ul class="nav nav-justified">

                        <li class="active"><a href="#step1" data-toggle="tab">
                                <span class="step">1</span> <span class="title">GENERAL </span></a></li>
                        <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title"> DETALLES   </span></a></li>

                        <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title"> OBLIGACIONES </span></a></li>
                        <li><a href="#step4" data-toggle="tab"><span class="step">4</span> <span class="title"> PARTIDAS PRESUPUESTARIAS </span></a></li>
                        <li><a href="#step5" data-toggle="tab"><span class="step">5</span> <span class="title"> DETALLES PRESUPUESTARIOS</span></a></li>

                    </ul>
                </div><!--end .form-wizard-nav -->
                <div class="tab-content clearfix">
                    <div class="tab-pane active" id="step1">
                        <div class="col-lg-6">
                            <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"  name="accion"  id="accion" />
                            <div class="col-lg-12">


                                <div class="form-group ">
                                    <select  required="" class="form-control" name="form[int][idSolicitado]" id="idSolicitado" aria-required="true">
                                        <option value="">&nbsp;</option>

                                        {foreach item=tipo from=$_DependenciaPost}

                                            {if isset($_ProyectoContratoPost[0]['fk_a004_dependencia'])}
                                                {if $tipo.pk_num_dependencia==$_ProyectoContratoPost[0]['fk_a004_dependencia']}
                                                    <option selected value="{$tipo.pk_num_dependencia}">{$tipo.ind_dependencia}  </option>
                                                {else}
                                                    <option value="{$tipo.pk_num_dependencia}">{$tipo.ind_dependencia}  </option>
                                                {/if}
                                            {else}
                                                <option value="{$tipo.pk_num_dependencia}">{$tipo.ind_dependencia}  </option>
                                            {/if}
                                        {/foreach}


                                    </select>
                                    <label for="rangelength2">Solicitado Por </label>

                                </div>

                            </div>


                            <div class="col-lg-12">



                                <div class="form-group ">
                                    <select   required="" class="form-control" name="form[int][idTipoContrato]" id="idTipoContrato" aria-required="true">
                                        <option value="">&nbsp;</option>

                                        {foreach item=tipo from=$_TipoContratoPost}

                                            {if isset($_ProyectoContratoPost[0]['fk_gcc003_num_tipo_contrato'])}

                                                {if $tipo.pk_num_tipo_contrato==$_ProyectoContratoPost[0]['fk_gcc003_num_tipo_contrato']}
                                                    <option selected value="{$tipo.pk_num_tipo_contrato}"> {$tipo.ind_descripcion} </option>
                                                {else}
                                                    <option value="{$tipo.pk_num_tipo_contrato}"> {$tipo.ind_descripcion}</option>
                                                {/if}
                                            {else}
                                                <option value="{$tipo.pk_num_tipo_contrato}"> {$tipo.ind_descripcion} </option>
                                            {/if}
                                        {/foreach}


                                    </select>
                                    <label for="rangelength2"> Tipo de Contrato </label>

                                </div>

                            </div>

                            <div class="col-lg-12">


                                <div class="form-group ">
                                    <select   required="" class="form-control" name="form[int][idAplicable]" id="idAplicable" aria-required="true">
                                        <option value="">&nbsp;</option>

                                        {foreach item=tipo from=$_AplicablePost}

                                            {if isset($_ProyectoContratoPost[0]['fk_gcc004_num_aplicable_contrato'])}

                                                {if $tipo.pk_num_aplicable_contrato==$_ProyectoContratoPost[0]['fk_gcc004_num_aplicable_contrato']}
                                                    <option selected value="{$tipo.pk_num_aplicable_contrato}">{$tipo.ind_descripcion} </option>
                                                {else}
                                                    <option value="{$tipo.pk_num_aplicable_contrato}">{$tipo.ind_descripcion} </option>
                                                {/if}
                                            {else}
                                                <option value="{$tipo.pk_num_aplicable_contrato}">{$tipo.ind_descripcion} </option>
                                            {/if}
                                        {/foreach}


                                    </select>
                                    <label for="rangelength2"> Aplicable a </label>

                                </div>

                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">

                                    <input type="text"   required="" value="{if isset($_ProyectoContratoPost[0]['ind_objeto_contrato'])}{$_ProyectoContratoPost[0]['ind_objeto_contrato']}{/if}" name="form[txt][objeto]" data-rule-rangelength="[2, 50]"    id="objeto"  class="form-control alpha"  >

                                    <label for="rangelength2"> Objeto del Contrato </label>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="form-group">

                                    <input type="text" required="" value="{if isset($_ProyectoContratoPost[0]['num_monto_contrato'])} {$_ProyectoContratoPost[0]['num_monto_contrato']|number_format:2:",":"."}  {/if}"name="form[monto][contrato]"  id="Monto"   class="form-control Monto"  >

                                    <label for="rangelength2"> Monto Bs.</label>

                                </div>
                            </div>


                        </div>




                        <div class="col-lg-6">

                            <div class="col-lg-9">

                                <div class="col-sm-12">
                                    <div class="col-sm-10 form-group ">

                                        <input type="text" class="form-control"
                                               id="nombreProveedor"
                                               value="{if isset($_ChoferBD[0]['proveedor'])}{$_ChoferBD[0]['proveedor']}  {/if}"
                                               readonly >

                                        <input type="hidden" class="form-control"
                                               id="codProveedor" name="form[int][idDocumento]"
                                               value="{if isset($_ProyectoContratoPost[0]['fk_a003_proveedor'])}{$_ProyectoContratoPost[0]['fk_a003_proveedor']}{/if}"
                                        >

                                        <input type="hidden" value="{if isset($_ProyectoContratoPost[0]['fk_a003_representante'])}{$_ProyectoContratoPost[0]['fk_a003_representante']}{/if}" name="form[int][CodRepresentante]"  id="CodRepresentante" />

                                        <label for="rangelength2"> Proveedor </label>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group floating-label">
                                            <button
                                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                    type="button"
                                                    data-toggle="modal"
                                                    data-target="#formModal2"
                                                    titulo="Listado de Personas"
                                                    {if isset($ver) and $ver==1}disabled{/if}
                                                    url="{$_Parametros.url}modGC/proyectoContratoCONTROL/personaMET/persona/">
                                                <i class="md md-search"></i>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <input type="hidden" value="{if isset($_ProyectoContratoPost[0]['fk_a003_proveedor'])}{$_ProyectoContratoPost[0]['fk_a003_proveedor']}{/if}" name="form[int][CodProveedor]"  id="CodProveedor" />

                            <input type="hidden" value="{if isset($_ProyectoContratoPost[0]['pk_num_registro_contrato'])}{$_ProyectoContratoPost[0]['pk_num_registro_contrato']}{/if}" name="form[int][idRegistro]"  id="idRegistro" />

                            <div class="col-lg-9">

                                <div class="form-group">


                                    <input type="text" required="" value="{if isset($_ProyectoContratoPost[0]['ind_cedula_documento'])}{$_ProyectoContratoPost[0]['ind_cedula_documento']}{/if}"name="form[alphaNum][idDocumentoRepresentante]"    id="idDocumentoRepresentante" class="form-control alpha"  >



                                    <label for="rangelength2"> Documento del Representante</label>
                                </div>
                            </div>



                            <div class="col-lg-12">
                                <div class="form-group">

                                    <input type="text" required="" value="{if isset($_ProyectoContratoPost[0]['ind_nombre1'])}{$_ProyectoContratoPost[0]['ind_nombre1']} {$_ProyectoContratoPost[0]['ind_apellido1']}{/if}" name="form[alphaNum][Representante]"   id="Representante" class="form-control"  >

                                    <label for="rangelength2"> Representante </label>

                                </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="form-group" id="fk_cpb017_num_tipo_servicioError">
                                    <label for="fk_cpb017_num_tipo_servicio">
                                        Tipo de Servicio:
                                    </label>
                                    <select name="form[int][tipo_servicio]"
                                            class="form-control select2-list select2" required
                                            data-placeholder="Seleccione el Tipo de Servicio"
                                            {if isset($ver) and $ver==1}disabled{/if}
                                            id="fk_cpb017_num_tipo_servicio">
                                        {foreach item=proceso from=$listadoServicio}
                                            {if isset($obligacionBD.fk_cpb017_num_tipo_servicio) and $obligacionBD.fk_cpb017_num_tipo_servicio == $proceso.pk_num_tipo_servico }
                                                <option value="{$proceso.pk_num_tipo_servico}"
                                                        selected>{$proceso.ind_descripcion}</option>
                                            {else}
                                                <option value="{$proceso.pk_num_tipo_servico}">{$proceso.ind_descripcion}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                </div>
                            </div>




                        </div>


                    </div><!--fin #step1 -->
                    <div class="tab-pane" id="step2">
                        <div class="col-lg-6">



                            <div class="col-lg-12">
                                <div class="form-group">

                                    <div id="fechas" class="input-daterange input-group" >
                                        <div class="input-group-content">
                                            <input required="" class="form-control " readonly  data-keyboard="false"  type="text" id="desde" name="desde" value="{if isset($_ProyectoContratoPost[0]['fec_desde'])}{$_ProyectoContratoPost[0]['fec_desde']}{/if}">
                                            <label>Vigencia desde:</label>
                                        </div>

                                        <span class="input-group-addon">hasta</span>
                                        <div class="input-group-content">
                                            <input required="" class="form-control "  readonly  type="text"  id="hasta"  name="hasta" value="{if isset($_ProyectoContratoPost[0]['fec_hasta'])}{$_ProyectoContratoPost[0]['fec_hasta']}{/if}">
                                            <div class="form-control-line"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class=" col-lg-12">
                                <div class="form-group">
                                    <input type="text" required="" readonly id="entrada"  name="entrada" class="form-control fechas2" value="{if isset($_ProyectoContratoPost[0]['fec_entrada'])}{$_ProyectoContratoPost[0]['fec_entrada']}{/if}">
                                    <label>Fecha de Entrada</label>
                                </div>
                            </div>


                            <div class="col-lg-12">


                                <div class="form-group ">
                                    <select  required="" class="form-control" name="form[int][formaPago]" id="formaPago" aria-required="true">
                                        <option value="">&nbsp;</option>

                                        {foreach item=tipo from=$_FormaPagoPost}

                                            {if isset($_ProyectoContratoPost[0]['fk_a006_num_forma_pago'])}

                                                {if $tipo.pk_num_miscelaneo_detalle==$_ProyectoContratoPost[0]['fk_a006_num_forma_pago']}
                                                    <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                                                {else}
                                                    <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                                                {/if}
                                            {else}
                                                <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}  </option>
                                            {/if}
                                        {/foreach}


                                    </select>
                                    <label for="rangelength2"> Forma de Pago </label>

                                </div>
                            </div>

                            <div class="col-lg-12">

                                <div class="form-group">
                                    <input required="" class="form-control alpha" type="text"  name="form[txt][lugarPago]" id="lugarPago"  value="{if isset($_ProyectoContratoPost[0]['ind_lugar_pago'])}{$_ProyectoContratoPost[0]['ind_lugar_pago']}{/if}"/>
                                    <label for="textarea1"> Lugar de Pago </label>
                                </div>

                            </div>

                        </div>

                    </div><!--fin #step2 -->

                    <div class="tab-pane" id="step3">
                        <div class="col-lg-6">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea placeholder=""  required=""  rows="3" class="form-control" id="obligacionesContrante" name="form[txt][obligacionesContrante]" >{if isset($_ProyectoContratoPost[0]['ind_obligaciones_contratante'])}{$_ProyectoContratoPost[0]['ind_obligaciones_contratante']}{/if}</textarea>
                                    <label for="textarea1">Obligaciones del Órgano Contrante</label>
                                </div>
                            </div>



                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea placeholder=""  required=""  rows="3" class="form-control" id="obligacionesContratado" name="form[txt][obligacionesContratado]" >{if isset($_ProyectoContratoPost[0]['ind_obligaciones_contratado'])}{$_ProyectoContratoPost[0]['ind_obligaciones_contratado']}{/if}</textarea>
                                    <label for="textarea1">Obligaciones del Contratado</label>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea placeholder="" required=""   rows="3" class="form-control" id="observacion" name="form[txt][observacion]" >{if isset($_ProyectoContratoPost[0]['ind_observacion'])}{$_ProyectoContratoPost[0]['ind_observacion']}{/if}</textarea>
                                    <label for="textarea1">Observaciones</label>
                                </div>
                            </div>

                        </div>


                    </div><!--fin #step3 -->
                    <div class="tab-pane" id="step4">

                        <div class="col-sm-12  "  >

                            <button  type="button" class="ver  btn ink-reaction btn-raised   btn-primary" titulo="Ver Partidas Presupuestarias" descipcion="El Usuario ha visualizado las partidas presupuestarias"   data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal">
                                SELECCIONAR PARTIDA PRESUPUESTARIA
                            </button>

                        </div>
                        &nbsp;&nbsp;
                         <div class="col-sm-12 ">
                                    <div class="card">
                                        <div align="center" class="card-head card-head-xs style-primary">
                                            <header>Partidas Presupuestarias</header>
                                        </div>
                                             <div style="padding: 10px;" >
                                            <table id="partidasSeleccionadas" style="width: 100%;display:block;" class="table table-striped table-hover">
                                                {$i=1}
                                                <tr  >
                                                    <th width="200">Partida </th>
                                                    <th width="950">Descripción</th>
                                                    <th width="140">Aprobado</th>
                                                    <th width="140">Ajustado</th>
                                                    <th width="140">Comprometido</th>
                                                    <th width="140">Causado</th>
                                                    <th width="140">Pagado</th>
                                                    <th width="140">Borrar</th>
                                                </tr>
                                                {if isset($_PartidasSeleccionadasPost)}
                                                    {foreach item=tipo from=$_PartidasSeleccionadasPost}

                                                        <tr id="id{$i}" style="font-size: 12px;">
                                                            <th width="200"> <input name="form[txt][partidaDet{$i}]"
                                                                                    class="form-control  text-medium " style="font-size: 12px;"  readonly type="text" value="{$tipo.ind_partida}"/></th>
                                                            <th width="950"> <input name="form[txt][descripcion{$i}]" class="form-control   text-medium " readonly style="font-size: 12px;"  type="text" value="{$tipo.ind_denominacion}"/></th>
                                                            <th width="170"> <input name="form[txt][monto{$i}]" class="form-control    text-medium " style="font-size: 12px;"  type="text" value="{$tipo.num_monto|number_format:2:",":""}"/> </th>
                                                            <th width="170"> <input name="form[txt][Ajustado{$i}]" class="form-control    text-medium " style="font-size: 12px;"  type="text" value="0"/> </th>
                                                            <th width="170"> <input name="form[txt][Comprometido{$i}]" class="form-control    text-medium " style="font-size: 12px;"  type="text" value="0"/> </th>
                                                            <th width="170"> <input name="form[txt][Causado{$i}]" class="form-control    text-medium " style="font-size: 12px;"  type="text" value="0"/> </th>
                                                            <th width="170"> <input name="form[txt][Pagado{$i}]" class="form-control    text-medium " style="font-size: 12px;"  type="text" value="0"/> </th>
                                                            <td> <button class=" btn ink-reaction btn-raised btn-xs btn-danger" type="button" onclick="Borrando({$i});" >
                                                                    <i class="md md-delete" style="color: #ffffff;"></i></button> </td>
                                                        </tr>

                                                        {$i=$i+1}

                                                    {/foreach}
                                                {/if}
                                                <tbody id="distribucionPresupuestaria" style="height: 170px;display: inline-block;width: 100%;overflow: auto;">



                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
							<span id="cky-error" class="help-block" style="color: #FF0000"></span>




                        </div>
                    </div><!--fin #step4 -->
                        <div class="tab-pane" id="step5">
                            <h4>Acción a realizar de aprobarse el proyecto.</h4>
                                <div class="col-lg-12  ">
                                    <div class="radio  ">
                                        <div class="form-group">
                                        <label><input checked type="radio"  value="1" name="form[int][guia]">Comprometer el monto total del contrato.</label>

                                            <small> El monto del contrato será comprometido  mediante este módulo.</small>

                                        </div>
                                    </div>
                                    <div class="radio">
                                        <div class="form-group">
                                            <label><input type="radio" value="2" name="form[int][guia]">Comprometer y causar el monto total del contrato.</label>

                                            <small> El monto del contrato será comprometido y causado  mediante una obligación  a realizar en el menu "Listar Obligaciones Contrato " mediante este módulo.</small>

                                        </div>
                                    </div>

                                    <div class="radio  ">

                                        <div class="form-group">
                                            <label><input type="radio" value="3" name="form[int][guia]">Afectar presupuesto por ordenes de servicio.</label>

                                            <small> El monto del contrato será afectado presupuestariamente por ordenes de servicio.</small>

                                        </div>

                                    </div>
                                    </div>




                        </div><!--fin #step5 -->

                        <span class="clearfix"></span>


                            {if isset($_Acciones.accion)}
                                {if $_Acciones.accion!="Ver"}
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

                            <button  class="btn btn-primary ink-reaction btn-raised" id="boton"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                        </div>   {/if}
                            {/if}

                    </div>
            </form>




        </div>

                <span class="clearfix"></span>
                <ul class="pager wizard">

                    <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>

                    <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>

                </ul>
        </div>
    </div>




 <script type="text/javascript">


     function Borrando (id) {

         $('#id'+id).remove();
          var Limite=$('#Monto').val().replace(/[_.]/gi,'');
          Limite=Limite.replace(",",".");
          var Facturas=$('#NPartidasSeleccionadas').val();
          var i=0;
          var Total=0;

          for( i; i<=Facturas;i++){

          var tmp=$('#idTmp'+i).val();

          if(tmp!=undefined){

          Total=parseFloat(Total+parseFloat((tmp.replace(",","."))));
          }

          }


          if(parseFloat(Total)<=parseFloat(Limite) ){
          $('#cky-error').html('');
          $('#boton').show('fast');
          }else{
          $('#boton').hide('fast');
          $('#cky-error').html("  Ha sobrepasado con la suma total , el monto especificado en el primer paso de este formulario.");

          }


     }

     $("#formAjax").submit(function(e){
         e.preventDefault();
     });

    $('#boton').click(function () {

        var datos = $("#formAjax" ).serialize();

        var accion = $("#accion").val();


         if(accion=="registrar" ) {

             if($('#formAjax').find(':input').prop('disabled', false)){

                 $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/RegistrarProyectoMET", datos).done(function (dato) {
                     var resultado = JSON.parse(dato);
                     if(resultado ['status']!='error'){


                         if(resultado['estado']==1){

                             var botonModificar ='<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Modificar Proyecto de Contrato"  titulo="Modificar Proyecto de Contrato"   idModificar="'+resultado['idProyecto']+'" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"><i class="fa fa-edit" style="color: #ffffff;"></i>  </button>';

                         }else{

                             var botonModificar ='';
                         }

                         if(resultado['estado']!=6){

                             var botonAnular ='<button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAnular="'+resultado['idProyecto']+'" mensaje="¿Estás seguro que desea anular el proyecto de contrato y las obligaciones relacionadas al proyecto?" title="¿Estás Seguro que desea anular?" titulo="¿Estás Seguro que desea anular?" descipcion="El usuario anuló un proyecto de contrato" boton="si, Eliminar" ><i class="md md-delete" style="color: #ffffff;"></i></button>';

                         }else{

                             var botonAnular ='';
                         }


                         $(document.getElementById('datatable1')).append('<tr  id="idProyecto'+resultado['idProyecto']+'">' +
                             '<td class="sort-alpha col-sm-1">'+resultado['idProyecto']+'</td>' +
                             '<td class="sort-alpha col-sm-4">'+resultado['proveedor']+'</td>' +
                             '<td class="sort-alpha col-sm-2">'+resultado['objeto']+'</td>' +
                             '<td class="sort-alpha col-sm-2">'+resultado['estatus']+'</td>' +

                             '<td class="sort-alpha col-sm-2"> Bs. '+new Intl.NumberFormat("de-DE").format(resultado["monto"])+'</td>'+

                             '<td  class="sort-alpha col-sm-1" >' +
                             '  <button class="movimientos logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Bitácora de Movimiento" titulo="Bitácora de Movimiento"  id="'+resultado['idProyecto']+'" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal"><i class="md md-perm-identity" style="color: #ffffff;"></i></button>'+
                             '  <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Ver Proyecto de Contrato" titulo="Ver Proyecto de Contrato"   idVer="'+resultado['idProyecto']+'" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"> <i class="md md-remove-red-eye" style="color: #ffffff;"></i> </button>' +
                             '<i class="fa fa-edit" style="color: #ffffff;"></i></button>'+

                             botonModificar+' '+
                             botonAnular+' </td>');
                         swal("Registro Modificado!", "Proyecto fue modificado satisfactoriamente.", "success");
                         $(document.getElementById('cerrarModal')).click();
                         $(document.getElementById('ContenidoModal')).html('');


                         swal({
                             title: 'Exito',
                             text: 'El proyecto de contrato ha sido agregado satisfactoriamente!!!.',
                             type: "success",
                             showCancelButton: false,
                             confirmButtonColor: "#DD6B55",
                             confirmButtonText: 'Aceptar',
                             closeOnConfirm: true
                         }, function(){





                         });
                     }else{

                         swal({
                             title: 'Error',
                             text: 'Hubo un error en los datos suministrados.',
                             type: "error",
                             showCancelButton: false,
                             confirmButtonColor: "#DD6B55",
                             confirmButtonText: 'Aceptar',
                             closeOnConfirm: true
                         });

                     }


                 }, 'json');
             }





            }else {


         $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/ModificarProyectoMET", datos, function (dato) {
         }, 'json');
         swal("Registro Modificado!","El proyecto de contrato ha sido modificado satisfactoriamente.", "success");
         $(document.getElementById('cerrarModal')).click();
         $(document.getElementById('ContenidoModal')).html('');

         }


    });






    $(document).ready(function() {

        $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/CalcularIvaMET",{ iva: $("#fk_cpb017_num_tipo_servicio").val()}, function (dato) {


            if(dato!=""){

                if(dato[0]['tipo']=="IVA"){
                    $("#Factor").val(dato[0]['num_factor_porcentaje']);
                }else{
                    $("#Factor").val('0');
                }

            }else{
                $("#Factor").val('0');
            }



        },'json');

        $('#desde').datepicker({ format: 'dd/mm/yyyy', language:'es' });
        $('#hasta').datepicker({ format: 'dd/mm/yyyy', language:'es' });
        $('#entrada').datepicker({ format: 'dd/mm/yyyy', language:'es' });
        $("#formAjax").keypress(function(e){
            if(e.which == 13){
                return false;
            }
        });

        $('#select2').select2();
        $('.fechas2').datepicker({ format: 'dd/mm/yyyy' });
        $('#fechas').datepicker({ format: 'dd/mm/yyyy' });
        $('.Monto').inputmask(' 999.999.999,99 BS', { numericInput: true });
        $('#NPartidasSeleccionadas').val({$i});
        $( '#ckeditor' ).ckeditor();


        $("#idSolicitado").change(function (){

            $("#idSolicitado option:selected").each(function () {

                $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/ActualizarTipoContratoMET", { idSolicitado: $('#idSolicitado').val() }, function (data) {
                    var json = data,
                            obj = JSON.parse(json);
                    $("#idTipoContrato").html(obj);
                });
            });
        });

        $("#idTipoContrato").change(function () {

            $("#idTipoContrato option:selected").each(function () {

                $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/ActualizarAplicableMET", { idTipoContrato: $('#idTipoContrato').val() }, function(data){
                    var json = data,
                            obj = JSON.parse(json);
                    $("#idAplicable").html(obj);
                });
            });
        });



        $("#idDocumento").change(function () {

            $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/BuscarRepresentanteMET", { idDocumento: $('#idDocumento').val() }, function(data){

                var json = data,
                        obj = JSON.parse(json);

                if(obj[0]!=undefined) {
                    $("#idDocumentoRepresentante").val(obj[0]['ind_cedula_documento']);
                    $("#CodRepresentante").val(obj[0]['pk_num_persona']);
                    $("#Representante").val(obj[0]['ind_nombre1']+" "+obj[0]['ind_apellido1']);
                    $(".persona").hide();
                }
            });

        });


        $("#idDocumentoRepresentante").change(function () {

            $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/VisualizarNombreMET", { idTipoDocumento: $('#idTipoDocumentoRepresentante').val(),idDocumento: $('#idDocumentoRepresentante').val() }, function(data){

                var json = data,
                        obj = JSON.parse(json);


                if(obj[0]!=undefined) {

                    $("#CodRepresentante").val(obj[0]['pk_num_persona']);
                    $("#Representante").val(obj[0]['ind_nombre1']+" "+obj[0]['ind_apellido1']);
                    $(".persona").hide();


                }else{

                    $("#Representante").val('');
                    $("#formAjax").validate().element("#Representante");
                    $(".persona").show();

                }

            });

        });


        $('.ver').click(function () {
            var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/ListarPartidasContratoMET';
            $('#modalAncho2').css( "width", "80%" );
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url,{ id:  $('#NPartidasSeleccionadas').val()},function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });


        $('.proveedor').click(function () {

            $('#modalAncho').css( "width", "60%" );
            var $url='{$_Parametros.url}modLG/maestros/proveedorCONTROL/crearModificarProveedorMET';

            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPost: $(this).attr('idModificar')},function($dato){
                $('#ContenidoModal').html($dato);

            });
        });

	 
    });

    function verificarDisponibilidad(monto, idPartida, posicion){

        $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/VerificarDisponibilidadPartidaMET",{ monto: monto, idPartida: idPartida }, function (dato) {
            var porcentaje = dato['porcentaje'];
            if(porcentaje <= 60){
                var color = "green";
            }
            if(porcentaje >= 61 && porcentaje <= 99){
                var color = "yellow";
            }
            if(porcentaje > 99){
                var color = "red";
            }
            $("#id"+posicion).css("background-color", color);

        },'json');

    }


     $("#fk_cpb017_num_tipo_servicio").on('change',function(){



         $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/CalcularIvaMET",{ iva: $("#fk_cpb017_num_tipo_servicio").val()}, function (dato) {


             if(dato!=""){

                 if(dato[0]['tipo']=="IVA"){
                     $("#Factor").val(dato[0]['num_factor_porcentaje']);
                 }else{
                     $("#Factor").val('0');
                 }

             }else{
                 $("#Factor").val('0');
             }



         },'json');

     });


 	function LimiteProyecto (valor,id){
		 
     	var Limite=$('#Monto').val().replace(/[_.]/gi,'');
        Limite=Limite.replace(",",".");
        var Facturas=$('#NPartidasSeleccionadas').val();
        var i=0;
        var Total=0;
        var iva=$("#Factor").val();
        var acumulado=0;


            if(parseFloat(valor) <= parseFloat(Limite)){
                $('#cky-error').html('');
                $('#boton').show('fast');

                for( i; i<=Facturas;i++){

                    var tmp=$('#idTmp'+i).val();

                    if(tmp!=undefined){

                        var idPartida = $("#idPartida"+i).val();

                        if($("#codigoPartida"+i).val()!='403.18.01.00'){
                            Total=parseFloat(Total+parseFloat((tmp.replace(",","."))));
                            acumulado=acumulado+(parseFloat(iva)*parseFloat((tmp.replace(",","."))))/100;
                        }
                        verificarDisponibilidad(parseFloat((tmp.replace(",","."))), idPartida, i);
                    }

                }

                Total=Total+acumulado;

                if(parseFloat(Total)<parseFloat(Limite) ){
                    $('#boton').hide('fast');
                    $('#cky-error').html(' El Monto ingresado es menor al indicado en la pestaña General');
                    $(".iva_").val(acumulado);

                }else if (parseFloat(Total)>parseFloat(Limite) ){
                    $('#boton').hide('fast');

                    swal({
                        title: "Ha sobrepasado el monto!.",
                        text: "Ha sobrepasado en la suma total , el monto especificado en el primer paso de este formulario.",
                        timer: 50000000,
                        showConfirmButton: true
                    });
                    $('#idTmp'+id).val('0,00');
                    $(".iva_").val(0);

                }else if (parseFloat(Total)==parseFloat(Limite) ){

                    $(".iva_").val(acumulado);
                    $('#boton').show('fast');

                }


            }else{
                $('#boton').hide('fast');
                $('#idTmp'+id).val('0,00');

                swal({
                    title: "Ha sobrepasado el monto!.",
                    text: "Ha sobrepasado el monto especificado en el primer paso de este formulario.",
                    timer: 50000000,
                    showConfirmButton: true
                });

            }

	 }
	
	 
	 function NumCheck(e, field) {

         key = e.keyCode ? e.keyCode : e.which

         // backspace

         if (key == 8) return true

         // 0-9

         if (key > 47 && key < 58) {

             if (field.value == "") return true

             regexp = /.[0-9]{2}$/

             return !(regexp.test(field.value))

         }

         // .

         if (key == 46) {

             if (field.value == "") return false

             regexp = /^[0-9]+$/

             return regexp.test(field.value)

         }

         // other key

         return false

     }


     $('.accionModal').click(function () {

         accionModal(this,'url')
     });

     function accionModal(id,attr){

         $('#formModalLabel2').html($(id).attr('titulo'));
         $.post($(id).attr(attr), {
             cargar: 0,

             tr: $("#"+$(id).attr('idTabla')+" > tbody > tr").length + 1
         }, function ($dato) {
             $('#ContenidoModal2').html($dato);
         });
     }


 </script>