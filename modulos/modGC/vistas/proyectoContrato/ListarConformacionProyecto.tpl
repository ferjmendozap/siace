<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary"> LISTADO DE PROYECTOS DE CONTRATOS POR CONFORMAR </h2></header>
    </div>
    <div class="col-md-12">
        <h5>Listado de los proyectos de contratos por conformar.</h5>
    </div><!--end .col -->

</div><!--end .card -->


<div class="form-group   col-lg-12 ">

    <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
</div>

</div><!--end .card -->

<section class="style-default-bright">




    <!-- Listado -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-actionbar">

            </div>
        </div><!--end .col -->
    </div><!--end .row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">N° </th>
                        <th class="sort-alpha col-sm-2">Proveedor </th>
                        <th class="sort-alpha col-sm-3">Objeto del Contrato</th>
                        <th class="sort-alpha col-sm-2">Monto</th>
                        <th class="sort-alpha col-sm-2">Acciones</th>



                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_ProyectoContratoPost}
                        <tr id="idProyecto{$post.pk_num_registro_contrato}" class="gradeA">
                            <td>{$post.pk_num_registro_contrato}</td>
                            <td>{$post.ind_nombre1}</td>
                            <td>{$post.ind_objeto_contrato}</td>
                            <td>Bs. {$post.num_monto_contrato|number_format:2:",":"."} </td>



                                <td>
                                    <button  title="Bitácora de Movimiento"  class="movimientos logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Ver Proyecto de Contrato"  id="{$post.pk_num_registro_contrato}" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal">
                                        <i class="md md-perm-identity" style="color: #ffffff;"></i>
                                    </button>





                                    {if in_array('GC-01-01-01-09-C',$_Parametros.perfil)}

                                            <button class="enviar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Conformar Proyecto de Contrato"  titulo="Conformar Proyecto de Contrato"   idEnviar="{$post.pk_num_registro_contrato}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                                <i class="md md-done" style="color: #ffffff;"></i>
                                            </button>

                                    {/if}

                                    <button data-toggle="modal" data-target="#formModal"  class="pdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  title="Ver Pdf"  idPdf="{$post.pk_num_registro_contrato}"   descipcion="El Usuario a visualizado el proyecto de contrato N° {$post.pk_num_registro_contrato}" >
                                        <i class="md md-description" style="color: #ffffff;"></i>
                                    </button>


                                    {if in_array('GC-01-01-01-05-A',$_Parametros.perfil)}

                                            <button title="Anular Proyecto" class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAnular="{$post.pk_num_registro_contrato}" mensaje="¿Estás seguro que desea anular el proyecto de contrato y las obligaciones relacionadas al proyecto?" titulo="¿Estás Seguro que desea anular?" descipcion="El usuario anuló un proyecto de contrato" boton="si, Eliminar" idmenu="7">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>

                                    {/if}
                                </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->




    </div><!--end .row -->


    <script type="text/javascript">
        $(document).ready(function() {



            $('#datatable1 tbody').on( 'click', '.obligacion', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/ListarProyectoObligacionMET';
                $('#modalAncho').css( "width", "75%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idContrato: $(this).attr('idEnviar') },function($dato){
                    $('#ContenidoModal').html($dato);
                });

            });



            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/RedactarProyectoMET';
                $('#modalAncho').css( "width", "95%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idModificar'), Accion:"redactar"},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
            $('#datatable1 tbody').on( 'click', '.movimientos', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/MovimientosMET';


                $('#formModalLabel2').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('id')},function($dato){
                    $('#ContenidoModal2').html($dato);
                });
                $('#modalAncho2').css( "width", "55%" );
            });

            $('#datatable1 tbody').on( 'click', '.pdf', function () {

                $('#modalAncho').css( "width", "95%" );

                var url = '{$_Parametros.url}modGC/reporteCONTROL/ProyectoContratoPdfMET/?idProyecto='+$(this).attr('idPdf');

                $('#ContenidoModal').html('<iframe src="'+url+'" width="100%" height="950"></iframe>');

            });



            $('#datatable1 tbody').on( 'click', '.enviar', function () {

                var $idContrato=$(this).attr('idEnviar');
                $('#formModalLabel').html($(this).attr('titulo'));

                       var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/RedactarProyectoMET';

                       $('#modalAncho').css( "width", "95%" );

                    $.post($url,{ idPost:$idContrato, Accion:"conformar"},function($dato) {
                    $('#ContenidoModal').html($dato);
                    });




            });

            $('#datatable1 tbody').on( 'click', '.anular', function () {

                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/AnularProyectoContratoMET';
                var idPost=$(this).attr('idAnular');

                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){

                    var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/AnularProyectoMET';


                    $.post($url,{ idPost: idPost},function($dato){
                        swal("Anulado!", "El proyecto fue anulado correctamente.", "success");
                    });

                    //  window.setTimeout('location.reload()', 2000);
                });
            });







        });
    </script>
    </div>