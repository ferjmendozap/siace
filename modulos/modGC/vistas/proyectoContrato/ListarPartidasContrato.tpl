<section class="style-default-bright">

    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>

                            <th>Partida</th>
                            <th>Denominacion</th>
                            <th>T.Presupuestado</th>
                            <th>T.Ajustado</th>
                            <th>T.Comprometido</th>
                            <th>T.Disponible</th>
                            <th>Montos PreComprometidos por Contratos</th>



                        </tr>
                        </thead>
                        <tbody>

                        {foreach item=partida from=$_PartidasPost}
                            <tr id="idPartida{$partida.cod_partida}" onclick="Seleccionar('{$partida.cod_partida}','{$partida.ind_denominacion}','{$_PartidasSeleccionadasPost.Partida}', '{$partida.pk_num_partida_presupuestaria}')">

                                <td><label>{$partida.cod_partida}</label></td>
                                <td><label>{$partida.ind_denominacion}</label></td>
                                <td><label>{number_format($partida.num_monto_aprobado,2,',','.') } </label></td>
                                <td><label>{number_format($partida.num_monto_ajustado,2,',','.') } </label></td>
                                <td><label>{number_format($partida.compromiso_dist,2,',','.') } </label></td>
                                <td><label>{number_format(($partida.num_monto_ajustado-$partida.compromiso_dist) ,2,',','.') } </label></td>
                                <td><label>{number_format(( $partida.monto_pre) ,2,',','.') } </label></td>
                            </tr>

                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div><button type="button" class="btn btn-default ink-reaction btn-raised  "   data-dismiss="modal">Cerrar </button>


</section>

<script type="text/javascript">


    function Seleccionar (data,data2,i, idPartida) {
        i++;
        $('#partidasSeleccionadas  ').append('<tr id="id'+i+'" style="font-size: 12px;">' +
                '                <th width="200"> <input id="codigoPartida'+i+'" name="form[txt][partidaDet'+ i +']"'+
                'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="'+data+'"/><input type="hidden" id="idPartida'+i+'" value="'+idPartida+'"/>'+
                '</th>' +
                '        <th width="950"> <input name="form[txt][descripcion'+ i +']"'+
                'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="'+data2+'"/></th>' +
                '<th width="170"> <input id="idTmp'+i+'" onkeypress="return NumCheck(event, this)" onchange=" LimiteProyecto(this.value,'+i+')" name="form[txt][monto'+ i +']"'+
                'class="form-control Monto  text-medium " style="font-size: 12px;"     type="text" value="0,00"/> </th>' +

                '                <th width="200"> <input id="AjTmp'+i+'" onchange=" LimiteProyecto(this.value,'+i+')" name="form[txt][Ajustado'+ i +']"'+
                'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="0,00"/></th>' +

                '                <th width="200"> <input readonly name="form[txt][Comprometido'+ i +']"'+
                'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="0,00"/></th>' +

                '                <th width="200"> <input readonly name="form[txt][Causado'+ i +']"'+
                'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="0,00"/></th>' +

                '                <th width="200"> <input readonly name="form[txt][Pagado'+ i +']"'+
                'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="0,00"/></th>' +

                '<td> <button class=" btn ink-reaction btn-raised btn-xs btn-danger" type="button" onclick="Borrando(\'' + i + '\');" >' +
                '                <i class="md md-delete" style="color: #ffffff;"></i></button> </td>' +
                '        </tr>');

            if($(".iva_").val()==undefined){
                i++;
                $('#partidasSeleccionadas  ').append('<tr id="id'+i+'" style="font-size: 12px;">' +
                    '                <th width="200"> <input id="codigoPartida'+i+'" name="form[txt][partidaDet'+ i +']"'+
                    'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="403.18.01.00"/><input type="hidden" id="idPartida'+i+'" value="'+idPartida+'"/>'+
                    '</th>' +
                    '        <th width="950"> <input name="form[txt][descripcion'+ i +']"'+
                    'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="IMPUESTO AL VALOR AGREGADO"/></th>' +
                    '<th width="170"> <input id="idTmp'+i+'" readonly  onkeypress="return NumCheck(event, this)" onchange=" LimiteProyecto(this.value,'+i+')" name="form[txt][monto'+ i +']"'+
                    'class="form-control Monto iva_ text-medium " style="font-size: 12px;"     type="text" value="0,00"/> </th>' +

                    '                <th width="200"> <input id="AjTmp'+i+'" readonly onchange=" LimiteProyecto(this.value,'+i+')" name="form[txt][Ajustado'+ i +']"'+
                    'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="0,00"/></th>' +

                    '                <th width="200"> <input readonly name="form[txt][Comprometido'+ i +']"'+
                    'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="0,00"/></th>' +

                    '                <th width="200"> <input readonly name="form[txt][Causado'+ i +']"'+
                    'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="0,00"/></th>' +

                    '                <th width="200"> <input readonly name="form[txt][Pagado'+ i +']"'+
                    'class="form-control Monto  text-medium " style="font-size: 12px;"  type="text" value="0,00"/></th>' +

                    '<td> <button class=" btn ink-reaction btn-raised btn-xs btn-danger" type="button" onclick="Borrando(\'' + i + '\');" >' +
                    '                <i class="md md-delete" style="color: #ffffff;"></i></button> </td>' +
                    '        </tr>');

            }

        $('#NPartidasSeleccionadas').val(i);
        $(document.getElementById('cerrarModal2')).click();


    }


</script>