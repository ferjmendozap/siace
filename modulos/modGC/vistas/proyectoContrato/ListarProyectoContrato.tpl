<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">LISTAR PROYECTO DE CONTRATOS </h2></header>
    </div>
    <div class="col-md-12">
        <h5>Listado de los proyectos de contratos.</h5>
    </div><!--end .col -->

</div><!--end .card -->


</div><!--end .card -->

<section class="style-default-bright">


    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <div class="btn-group pull-right ">
                    <a data-toggle="offcanvas" title="Filtro" class="  btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"> <i class="fa fa-filter"></i> </a>
                </div><br>
                <br/>
                <table id="datatable1" class="table table-striped table-hover">

                    <thead>
                    <tr>

                        <th class="sort-alpha col-sm-1">N° </th>
                        <th class="sort-alpha col-sm-3">Proveedor </th>
                        <th class="sort-alpha col-sm-2">Objeto del Contrato</th>
                        <th class="sort-alpha col-sm-2">Estatus</th>
                        <th class="sort-alpha col-sm-2">Monto</th>
                        <th class="sort-alpha col-sm-4">Acciones</th>


                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_ProyectoContratoPost}

                        <tr id="idProyecto{$post.pk_num_registro_contrato}" class="gradeA">
                            <td>{$post.pk_num_registro_contrato}</td>
                            <td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
                            <td>{$post.ind_objeto_contrato}</td>
                            <td>{$post.ind_nombre_detalle}</td>
                            <td>Bs. {$post.num_monto_contrato|number_format:2:",":"."}     </td>


                            <td>


                              <button type="button" class="movimientos logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Bitácora de Movimiento" titulo="Bitácora de Movimiento"  id="{$post.pk_num_registro_contrato}" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal">

                                        <i class="md md-perm-identity" style="color: #ffffff;"></i>
                               </button>

                               <button type="button" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Ver Proyecto de Contrato" titulo="Ver Proyecto de Contrato"   idVer="{$post.pk_num_registro_contrato}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">

                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                               </button>

                                    {if in_array('GC-01-01-01-04-P',$_Parametros.perfil)}


                                        <button data-toggle="modal" data-target="#formModal"  class="pdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  title="Ver Pdf"  idPdf="{$post.pk_num_registro_contrato}"   descipcion="El Usuario a visualizado el proyecto de contrato N° {$post.pk_num_registro_contrato}" >
                                            <i class="md md-description" style="color: #ffffff;"></i>
                                        </button>


                                    {/if}



                            {if in_array('GC-01-01-01-02-M',$_Parametros.perfil)&&($post.fk_a006_num_estado_contrato=='1')}

                                <button type="button" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Modificar Proyecto de Contrato"  titulo="Modificar Proyecto de Contrato" descipcion="El Usuario a Modificado un Proyecto de Contrato" idModificar="{$post.pk_num_registro_contrato}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">

                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                </button>

                            {/if}




                                <button type="button" class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAnular="{$post.pk_num_registro_contrato}" mensaje="¿Estás seguro que desea anular el proyecto de contrato y las obligaciones relacionadas al proyecto?" titulo="¿Estás Seguro que desea anular?" descipcion="El usuario anuló un proyecto de contrato" boton="si, Eliminar" >

                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                </button>


                            </td>
                        </tr>
                    {/foreach}
                    </tbody>



                </table>
                <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                        descripcion="Registro de Proyecto de Contrato"  titulo="Registrar Proyecto de Contrato" id="nuevo" >
                    <i class="md md-create"></i> Registrar Proyecto de Contrato &nbsp;&nbsp;
                </button>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->
    <!-- filtro -->
    <div class="offcanvas">
        <div id="offcanvas-filtro" class="offcanvas-pane width-9">
            <div class="offcanvas-head">
                <header>Filtro</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                </div>
            </div>

            <div class="offcanvas-body">

                    <input type="hidden" name="filtro" value="1">
                    <div class="row">

                       <div class="col-sm-12 ">
                            <div class="form-group ">
                                <select   id="filtro_estatus" class="form-control">
                                    <option value="">&nbsp;</option>

                                    {foreach item=app from=$_EstatusPost}

                                        {if isset($_EstatusSelect)}

                                            {if $app.cod_detalle==$_EstatusSelect}
                                                <option selected value="{$app.cod_detalle}">{$app.ind_nombre_detalle} </option>
                                            {else}
                                                <option value="{$app.cod_detalle}">{$app.ind_nombre_detalle} </option>
                                            {/if}

                                        {else}
                                            <option value="{$app.cod_detalle}">{$app.ind_nombre_detalle} </option>

                                        {/if}

                                    {/foreach}

                                </select>
                                <label for="rangelength2">  Estatus de la Solicitud</label>
                            </div>
                        </div>

                        <div class="col-sm-12  ">
                            <div class="form-group ">
                            <select  class="form-control" id="filtro_dependencia">
                                <option value="0"> Todas </option>
                                {foreach item=tipo from=$_DependenciaPost}
                                    {if isset($_DependenciaSelect)}


                                    {if ($tipo.pk_num_dependencia==$_DependenciaSelect)}

                                        <option selected  value="{$tipo.pk_num_dependencia}"> {$tipo.ind_dependencia}</option>

                                    {else}
                                        <option value="{$tipo.pk_num_dependencia}"> {$tipo.ind_dependencia}</option>
                                    {/if}

                                    {else}
                                        <option value="{$tipo.pk_num_dependencia}"> {$tipo.ind_dependencia}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="select1">Dependencias</label>
                        </div>
                        </div>
                        <div class="col-sm-12  ">
                            <div class="form-group ">
                            <select   class="form-control" id="filtro_proveedor">
                                <option value="0"> Todos </option>
                                {foreach item=tipo from=$_ProveedorPost}

                                    {if isset($_ProveedorSelect)}
                                    {if ($tipo.pk_num_proveedor==$_ProveedorSelect)}
                                    <option selected value="{$tipo.pk_num_proveedor}">{$tipo.ind_nombre1} </option>
                                    {else}
                                    <option   value="{$tipo.pk_num_proveedor}">{$tipo.ind_nombre1} </option>
                                    {/if}
                                    {else}
                                        <option  value="{$tipo.pk_num_proveedor}">{$tipo.ind_nombre1} </option>
                                    {/if}

                                {/foreach}
                            </select>
                            <label for="select1"> Proveedor </label>
                        </div>
                        </div>



                        </div>




                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button  type="button" onclick="Filtro()" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                                Filtrar
                            </button>
                        </div>
                    </div>





            </div>
        </div>
    </div>



    </div><!--end .row -->


    <script type="text/javascript">
        $(document).ready(function() {

            var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/RegistrarProyectoMET';
            $('#nuevo').click(function(){
                $('#modalAncho').css( "width", "95%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                 $.post($url,{ idMenu:0 },function($dato){
                    $('#ContenidoModal').html($dato);
                 });
            });



            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/RedactarProyectoMET';
                $('#modalAncho').css( "width", "95%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idModificar'), Accion:"modificar"},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });


            $('#datatable1 tbody').on( 'click', '.ver', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/RedactarProyectoMET';
                $('#modalAncho').css( "width", "95%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idVer'),accion:"Ver"},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.pdf', function () {

                $('#modalAncho').css( "width", "95%" );

                var url = '{$_Parametros.url}modGC/reporteCONTROL/ProyectoContratoPdfMET/?idProyecto='+$(this).attr('idPdf');

                $('#ContenidoModal').html('<iframe src="'+url+'" width="100%" height="950"></iframe>');

            });


            $('#datatable1 tbody').on( 'click', '.obligacion', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/ListarProyectoObligacionMET';
                $('#modalAncho').css( "width", "95%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idModificar'),accion:"Modificar"},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });




            $('#datatable1 tbody').on( 'click', '.movimientos', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/MovimientosMET';


                $('#formModalLabel2').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('id')},function($dato){
                    $('#ContenidoModal2').html($dato);
                });
                $('#modalAncho2').css( "width", "55%" );
            });

            $('#datatable1 tbody').on( 'click', '.anular', function () {


                var idPost=$(this).attr('idAnular');

                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){
                    var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/AnularProyectoMET';
                    $.post($url,{ idPost: idPost},function(dato){

                        if(dato['status']=='OK'){
                            $(document.getElementById('idProyecto'+idPost)).html('');
                            swal("Anulado!", "El proyecto fue anulado correctamente.", "success");
                            $('#cerrar').click();
                        }



                    },'json');


                });
            });






        });




        // Buscador
        function Filtro()
        {

         var filtro_proveedor = $("#filtro_proveedor").val();
         var filtro_dependencia = $("#filtro_dependencia").val();
         var filtro_estatus = $("#filtro_estatus").val();

         var url_listar='{$_Parametros.url}modGC/proyectoContratoCONTROL/FiltroProyectoMET';
         $.post(url_listar,{ filtro_proveedor: filtro_proveedor, filtro_dependencia: filtro_dependencia, filtro_estatus: filtro_estatus  },function(respuesta_post) {


         var tabla_listado = $('#datatable1').DataTable();
         tabla_listado.clear().draw();

    if(respuesta_post != -1) {
         for(var i=0; i<respuesta_post.length; i++) {



             if(respuesta_post[i].fk_a006_num_estado_contrato==1){

                 var botonModificar ='<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Modificar Proyecto de Contrato"  titulo="Modificar Proyecto de Contrato"   idModificar="'+respuesta_post[i].pk_num_registro_contrato+'" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"><i class="fa fa-edit" style="color: #ffffff;"></i>  </button>';

             }else{

                 var botonModificar ='';
             }

             if(respuesta_post[i].fk_a006_num_estado_contrato!=6){

                 var botonAnular ='<button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAnular="'+respuesta_post[i].pk_num_registro_contrato+'" mensaje="¿Estás seguro que desea anular el proyecto de contrato y las obligaciones relacionadas al proyecto?" title="¿Estás Seguro que desea anular?" titulo="¿Estás Seguro que desea anular?" descipcion="El usuario anuló un proyecto de contrato" boton="si, Eliminar" ><i class="md md-delete" style="color: #ffffff;"></i></button>';

             }else{

                 var botonAnular ='';
             }


             var botonCargar = '<button type="button" pk_num_empleado = "' + respuesta_post[i].num_monto_contrato + '" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Cargar empleado"><i class="glyphicon glyphicon-download"></i></button>';

         var botonBitacora ='<button class="movimientos logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Bitácora de Movimiento" titulo="Bitácora de Movimiento"   id="'+respuesta_post[i].pk_num_registro_contrato+'" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal"><i class="md md-perm-identity" style="color: #ffffff;"></i></button>';
         var botonVer ='<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  title="Ver Proyecto de Contrato" titulo="Ver Proyecto de Contrato"   idVer="'+respuesta_post[i].pk_num_registro_contrato+'"  data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"><i class="md md-remove-red-eye" style="color: #ffffff;"></i></button>';
         var botonPDF =' <form id="filtro" target="_blank"  action="{$_Parametros.url}modGC/reporteCONTROL/ProyectoContratoPdfMET"   class="form" role="form" method="post">'+
                       '<input type="hidden" name="idProyecto" value="'+respuesta_post[i].pk_num_registro_contrato+'" >'+
                       '<button class="pdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"   idPdf="'+respuesta_post[i].pk_num_registro_contrato+'"   descipcion="El Usuario a visualizado el proyecto de contrato N° '+respuesta_post[i].pk_num_registro_contrato+'" >'+
                       '<i class="md md-description" style="color: #ffffff;"></i></button></form>';


         tabla_listado.row.add([
             respuesta_post[i].pk_num_registro_contrato,
             respuesta_post[i].ind_nombre1,
             respuesta_post[i].ind_objeto_contrato,
             respuesta_post[i].ind_nombre_detalle,

             "Bs. "+new Intl.NumberFormat("de-DE").format(respuesta_post[i].num_monto_contrato),
             botonBitacora,
             botonVer,
             botonPDF,
             botonModificar,
             botonAnular
         ]).draw()
         .nodes()
         .to$()
         }
         }


         },'json');

        }
    </script>
    </div>