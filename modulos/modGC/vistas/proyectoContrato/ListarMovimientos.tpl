<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: GESTIÓN DE CONTRATOS
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->


<section class="style-default-bright">




    <!-- Listado -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-actionbar">

            </div>
        </div><!--end .col -->
    </div><!--end .row -->
    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">Acción </th>
                        <th class="sort-alpha col-sm-4">Usuario </th>
                        <th class="sort-alpha col-sm-4">Fecha</th>



                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_MovimientosPost}

                        <tr id="idPost{$post.pk_num_bitacora}" class="gradeA">
                            <td>{$post.ind_nombre_detalle}</td>
                            <td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
                            <td>{$post.fec_ultima_modificacion}</td>
</tr>
                    {/foreach}
                    </tbody>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->
