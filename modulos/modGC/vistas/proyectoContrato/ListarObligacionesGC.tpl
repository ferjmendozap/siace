<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: GESTIÓN DE CONTRATOS
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
     <section class="style-default-bright">




        <!-- Listado -->
        <div class="row">
            <div class="col-md-12">
                <div class="card-actionbar">

                </div>
            </div><!--end .col -->
        </div><!--end .row -->

        <div class="row">

            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <form id="filtro"  action="{$_Parametros.url}modPA/solicitudCONTROL/ListarSolicitudMET"  class="form" role="form" method="post">
                            <thead>
                            <tr>

                            <th class="sort-alpha col-sm-1">N° </th>
                            <th class="sort-alpha col-sm-6">Proveedor </th>
                            <th class="sort-alpha col-sm-2">Nro Documento</th>
                            <th class="sort-alpha col-sm-2">Monto </th>
                            <th class="sort-alpha col-sm-2">Fec de Documento</th>
                            <th class="sort-alpha col-sm-2">Estado</th>

                            <th class="sort-alpha col-sm-2">Ver Obligación </th>


                        </tr>
                        </thead>
                        <tbody>

                        {foreach item=post from=$_ProyectoContratoPost}

                            <tr id="idPost{$post.pk_num_obligacion}" class="gradeA">
                                <td> {$post.pk_num_obligacion} </td>
                                <td>{$post.fk_a003_num_persona_proveedor}</td>

                                <td> {$post.ind_nro_factura}</td>
                                <td>   {$post.num_monto_obligacion|number_format:2:",":"."}</td>


                                    <td>
                                        {$post.fec_documento}

                                    </td>


                                    <td>
                                        {$post.ind_estado}
                                    </td>





                                {if in_array('GC-01-01-01-01-V',$_Parametros.perfil)}
                                    <td>
                                        <button class="ver_obligacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Ver Obligación" descipcion="El Usuario a Visualizado un Proyecto de Contrato  " idObligacion="{$post.fk_cpd001_obligacion}" data-backdrop="false" data-keyboard="false" data-target="#formModal"   >
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                    </td>
                                {/if}



                            </tr>
                        {/foreach}
                        </tbody>

                    </form>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->


         <!-- fin listado -->
         <script>


             $('#nuevo').click(function(){
                 $('#modalAncho').css( "width", "100%" );

                 var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/NuevaObligacionMET';

                 $('#formModalLabel').html($(this).attr('titulo'));
                 $.post($url,{ idContrato: $(this).attr('idCrear')},function($dato){
                     $('#ContenidoModal').html($dato);

                 });

             });

             $('#datatable1 tbody').on( 'click', '.ver_obligacion', function () {
                 $('#modalAncho').css( "width", "100%" );

                 var $url='{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/nuevaObligacionMET';

                 $('#formModalLabel').html($(this).attr('titulo'));
                 $.post($url,{ idObligacion: $(this).attr('idObligacion'),ver:1},function($dato){
                     $('#ContenidoModal').html($dato);

                 });

             });




         </script>

