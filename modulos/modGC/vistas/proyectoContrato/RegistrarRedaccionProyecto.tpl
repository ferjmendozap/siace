<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2016       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
<!-- BEGIN VALIDATION FORM WIZARD -->


<div id="rootwizard2" class="form-wizard form-wizard-horizontal">
    <form action="" id="formAjax"class="form   form-validation" role="form"  >
        <input type="hidden" value="0" name="NPartidasSeleccionadas"  id="NPartidasSeleccionadas" />
        <input type="hidden" value="0"   id="Factor" />

        <input type="hidden" value="1" name="valido"  id="valido" />
        <input type="hidden" value="{if isset($_ProyectoContratoPost[0]['fk_a006_num_estado_contrato'])}{$_ProyectoContratoPost[0]['fk_a006_num_estado_contrato']}{/if}" name="form[int][estatus]"  id="form[int][estatus]" />
        <div class="form-wizard-nav">
            <div class="progress"><div class="progress-bar progress-bar-primary"></div></div>
            <ul class="nav nav-justified">

                <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span class="title">GENERAL</span></a></li>
                <li><a href="#step2" data-toggle="tab"><span class="step">2</span> <span class="title"> DETALLES   </span></a></li>
                <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title"> INFORMACIÓN DEL REPRESENTANTE  </span></a></li>
                <li><a href="#step4" data-toggle="tab"><span class="step">4</span> <span class="title">  PARTIDAS PRESUPUESTARIAS</span></a></li>
                <li><a href="#step5" data-toggle="tab"><span class="step">5</span> <span class="title"> DETALLES PRESUPUESTARIOS</span></a></li>

            </ul>





        </div><!--end .form-wizard-nav -->
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="step1">
                <div class="col-lg-6">
                    <input type="hidden" value="{if isset($_Acciones.accion)}{$_Acciones.accion}{/if}"  name="accion"  id="accion" />
                    <div class="col-lg-12">


                        <div class="form-group ">
                            <select   disabled required="" class="form-control" name="form[int][idSolicitado]" id="idSolicitado" aria-required="true">
                                <option value="">&nbsp;</option>

                                {foreach item=tipo from=$_DependenciaPost}

                                    {if isset($_ProyectoContratoPost[0]['fk_a004_dependencia'])}
                                        {if $tipo.pk_num_dependencia==$_ProyectoContratoPost[0]['fk_a004_dependencia']}
                                            <option selected value="{$tipo.pk_num_dependencia}">{$tipo.ind_dependencia}  </option>
                                        {else}
                                            <option value="{$tipo.pk_num_dependencia}">{$tipo.ind_dependencia}  </option>
                                        {/if}
                                    {else}
                                        <option value="{$tipo.pk_num_dependencia}">{$tipo.ind_dependencia}  </option>
                                    {/if}
                                {/foreach}


                            </select>
                            <label for="rangelength2">Solicitado Por </label>

                        </div>

                    </div>


                    <div class="col-lg-12">



                        <div class="form-group ">
                            <select  disabled required="" class="form-control" name="form[int][idTipoContrato]" id="idTipoContrato" aria-required="true">
                                <option value="">&nbsp;</option>

                                {foreach item=tipo from=$_TipoContratoPost}

                                    {if isset($_ProyectoContratoPost[0]['fk_gcc003_num_tipo_contrato'])}

                                        {if $tipo.pk_num_tipo_contrato==$_ProyectoContratoPost[0]['fk_gcc003_num_tipo_contrato']}
                                            <option selected value="{$tipo.pk_num_tipo_contrato}"> {$tipo.ind_descripcion} </option>
                                        {else}
                                            <option value="{$tipo.pk_num_tipo_contrato}"> {$tipo.ind_descripcion}</option>
                                        {/if}
                                    {else}
                                        <option value="{$tipo.pk_num_tipo_contrato}"> {$tipo.ind_descripcion} </option>
                                    {/if}
                                {/foreach}


                            </select>
                            <label for="rangelength2"> Tipo de Contrato </label>

                        </div>

                    </div>

                    <div class="col-lg-12">


                        <div class="form-group ">
                            <select  disabled required="" class="form-control" name="form[int][idAplicable]" id="idAplicable" aria-required="true">
                                <option value="">&nbsp;</option>

                                {foreach item=tipo from=$_AplicablePost}

                                    {if isset($_ProyectoContratoPost[0]['fk_gcc004_num_aplicable_contrato'])}

                                        {if $tipo.pk_num_aplicable_contrato==$_ProyectoContratoPost[0]['fk_gcc004_num_aplicable_contrato']}
                                            <option selected value="{$tipo.pk_num_aplicable_contrato}">{$tipo.ind_descripcion} </option>
                                        {else}
                                            <option value="{$tipo.pk_num_aplicable_contrato}">{$tipo.ind_descripcion} </option>
                                        {/if}
                                    {else}
                                        <option value="{$tipo.pk_num_aplicable_contrato}">{$tipo.ind_descripcion} </option>
                                    {/if}
                                {/foreach}


                            </select>
                            <label for="rangelength2"> Aplicable a </label>

                        </div>

                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">
                            {if isset($_Addendum)}
                                <input type="hidden" value="{$_ProyectoContratoPost[0]['pk_num_registro_contrato']}" name="form[int][addendum]" />
                                <input   type="text"   required="" value="" name="form[txt][objeto]" data-rule-rangelength="[2, 50]"    id="objeto"  class="form-control alpha"  >
                                <label for="rangelength2"> Objeto del Addendum </label>

                            {/if}


                            {if !isset($_Addendum)}
                                <input type="hidden" value="0" name="form[int][addendum]" />
                                <input disabled type="text"   required="" value="{if isset($_ProyectoContratoPost[0]['ind_objeto_contrato'])}{$_ProyectoContratoPost[0]['ind_objeto_contrato']}{/if}" name="form[txt][objeto]" data-rule-rangelength="[2, 50]"    id="objeto"  class="form-control alpha"  >
                                <label for="rangelength2"> Objeto del Contrato </label>
                            {/if}
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group">

                            <input   {if !isset($_Addendum)}  disabled {/if} type="text" required="" value="{if isset($_ProyectoContratoPost[0]['num_monto_contrato'])} {$_ProyectoContratoPost[0]['num_monto_contrato']|number_format:2:",":"."}  {/if}"name="form[monto][contrato]"  id="Monto"   class="form-control Monto"  >

                            <label for="rangelength2"> Monto Bs.</label>

                        </div>
                    </div>


                </div>




                <div class="col-lg-6">

                    <div class="col-lg-9">

                        <div class="col-sm-12">
                            <div class="col-sm-10 form-group ">

                                <input disabled type="text" class="form-control"
                                       id="nombreProveedor"
                                       value="{if isset($_ProyectoContratoPost[0]['proveedor'])}{$_ProyectoContratoPost[0]['proveedor']}  {/if}"
                                       readonly >

                                <input type="hidden" class="form-control"
                                       id="codProveedor" name="form[int][fk_a003_proveedor]"
                                       value="{if isset($_ProyectoContratoPost[0]['fk_a003_proveedor'])}{$_ProyectoContratoPost[0]['fk_a003_proveedor']}{/if}"
                                >

                                <input type="hidden" class="form-control"
                                       id="codProveedor" name="form[int][idDocumento]"
                                       value="{if isset($_ProyectoContratoPost[0]['fk_a003_proveedor'])}{$_ProyectoContratoPost[0]['fk_a003_proveedor']}{/if}"
                                >

                                <input type="hidden" value="{if isset($_ProyectoContratoPost[0]['fk_a003_representante'])}{$_ProyectoContratoPost[0]['fk_a003_representante']}{/if}" name="form[int][CodRepresentante]"  id="CodRepresentante" />

                                <label for="rangelength2"> Proveedor </label>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group floating-label">
                                    <button disabled
                                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                            type="button"
                                            data-toggle="modal"
                                            data-target="#formModal2"
                                            titulo="Listado de Personas"
                                            {if isset($ver) and $ver==1}disabled{/if}
                                            url="{$_Parametros.url}modGC/proyectoContratoCONTROL/personaMET/persona/">
                                        <i class="md md-search"></i>
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <input type="hidden" value="{if isset($_ProyectoContratoPost[0]['fk_a003_proveedor'])}{$_ProyectoContratoPost[0]['fk_a003_proveedor']}{/if}" name="form[int][CodProveedor]"  id="CodProveedor" />

                    <input type="hidden" value="{if isset($_ProyectoContratoPost[0]['pk_num_registro_contrato'])}{$_ProyectoContratoPost[0]['pk_num_registro_contrato']}{/if}" name="form[int][idRegistro]"  id="idRegistro" />

                    <div class="col-lg-9">

                        <div class="form-group">


                            <input disabled type="text" required="" value="{if isset($_ProyectoContratoPost[0]['ind_cedula_documento'])}{$_ProyectoContratoPost[0]['ind_cedula_documento']}{/if}"name="form[alphaNum][idDocumentoRepresentante]"    id="idDocumentoRepresentante" class="form-control alpha"  >



                            <label for="rangelength2"> Documento del Representante</label>
                        </div>
                    </div>



                    <div class="col-lg-12">
                        <div class="form-group">

                            <input disabled type="text" required="" value="{if isset($_ProyectoContratoPost[0]['ind_nombre1'])}{$_ProyectoContratoPost[0]['ind_nombre1']} {$_ProyectoContratoPost[0]['ind_apellido1']}{/if}" name="form[alphaNum][Representante]"   id="Representante" class="form-control"  >

                            <label for="rangelength2"> Representante </label>

                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="form-group" id="fk_cpb017_num_tipo_servicioError">
                            <label for="fk_cpb017_num_tipo_servicio">
                                Tipo de Servicio:
                            </label>
                            <select name="form[int][tipo_servicio]"
                                    class="form-control select2-list select2" required
                                    data-placeholder="Seleccione el Tipo de Servicio"
                                    {if isset($ver) and $ver==1}disabled{/if}
                                    id="fk_cpb017_num_tipo_servicio">
                                {foreach item=proceso from=$listadoServicio}
                                    {if isset($_ProyectoContratoPost[0]['fk_cpb017_tipo_servicio']) and $_ProyectoContratoPost[0]['fk_cpb017_tipo_servicio'] == $proceso.pk_num_tipo_servico }
                                        <option value="{$proceso.pk_num_tipo_servico}"
                                                selected>{$proceso.ind_descripcion}</option>
                                    {else}
                                        <option value="{$proceso.pk_num_tipo_servico}">{$proceso.ind_descripcion}</option>
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                    </div>

                </div>



            </div><!--fin #step1 -->
            <div class="tab-pane" id="step2">
                <div class="col-lg-6">



                    <div class="col-lg-12">
                        <div class="form-group">

                            <div id="fechas" class="input-daterange input-group" >
                                <div class="input-group-content">
                                    <input  required="" class="form-control " readonly  data-keyboard="false"  type="text" id="desde" name="desde" value="{if isset($_ProyectoContratoPost[0]['fec_desde'])}{$_ProyectoContratoPost[0]['fec_desde']}{/if}">
                                    <label>Vigencia desde:</label>
                                </div>

                                <span class="input-group-addon">hasta</span>
                                <div class="input-group-content">
                                    <input  required="" class="form-control "  readonly  type="text"  id="hasta"  name="hasta" value="{if isset($_ProyectoContratoPost[0]['fec_hasta'])}{$_ProyectoContratoPost[0]['fec_hasta']}{/if}">
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class=" col-lg-12">
                        <div class="form-group">
                            <input  type="text" required="" readonly id="entrada"  name="entrada" class="form-control fechas2" value="{if isset($_ProyectoContratoPost[0]['fec_entrada'])}{$_ProyectoContratoPost[0]['fec_entrada']}{/if}">
                            <label>Fecha de Entrada</label>
                        </div>
                    </div>


                    <div class="col-lg-12">


                        <div class="form-group ">
                            <select disabled required="" class="form-control" name="form[int][formaPago]" id="formaPago" aria-required="true">
                                <option value="">&nbsp;</option>

                                {foreach item=tipo from=$_FormaPagoPost}

                                    {if isset($_ProyectoContratoPost[0]['fk_a006_num_forma_pago'])}

                                        {if $tipo.pk_num_miscelaneo_detalle==$_ProyectoContratoPost[0]['fk_a006_num_forma_pago']}
                                            <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                                        {else}
                                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                                        {/if}
                                    {else}
                                        <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}  </option>
                                    {/if}
                                {/foreach}


                            </select>
                            <label for="rangelength2"> Forma de Pago </label>

                        </div>
                    </div>

                    <div class="col-lg-12">

                        <div class="form-group">
                            <input disabled required="" class="form-control alpha" type="text"  name="form[txt][lugarPago]" id="lugarPago"  value="{if isset($_ProyectoContratoPost[0]['ind_lugar_pago'])}{$_ProyectoContratoPost[0]['ind_lugar_pago']}{/if}"/>
                            <label for="textarea1"> Lugar de Pago </label>
                        </div>

                    </div>

                </div>


            </div><!--fin #step2 -->
            <div class="tab-pane" id="step3">
                <div class="col-lg-6">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <textarea placeholder=""  required=""  rows="3" class="form-control" id="obligacionesContrante" name="form[txt][obligacionesContrante]" >{if isset($_ProyectoContratoPost[0]['ind_obligaciones_contratante'])}{$_ProyectoContratoPost[0]['ind_obligaciones_contratante']}{/if}</textarea>
                            <label for="textarea1">Obligaciones del Órgano Contrante</label>
                        </div>
                    </div>



                    <div class="col-lg-12">
                        <div class="form-group">
                            <textarea  placeholder=""  required=""  rows="3" class="form-control" id="obligacionesContratado" name="form[txt][obligacionesContratado]" >{if isset($_ProyectoContratoPost[0]['ind_obligaciones_contratado'])}{$_ProyectoContratoPost[0]['ind_obligaciones_contratado']}{/if}</textarea>
                            <label for="textarea1">Obligaciones del Contratado</label>
                        </div>
                    </div>


                    <div class="col-lg-12">
                        <div class="form-group">
                            <textarea  placeholder="" required=""   rows="3" class="form-control" id="observacion" name="form[txt][observacion]" >{if isset($_ProyectoContratoPost[0]['ind_observacion'])}{$_ProyectoContratoPost[0]['ind_observacion']}{/if}</textarea>
                            <label for="textarea1">Observaciones</label>
                        </div>
                    </div>

                </div>
            </div><!--fin #step3 -->


            <div class="tab-pane" id="step4">

                <div class="col-sm-12  "  >

                    <button {if isset($_ProyectoContratoPost[0]['fk_a006_num_estado_contrato'])}{if  $_ProyectoContratoPost[0]['fk_a006_num_estado_contrato']!='1'}  disabled {/if} {/if} type="button" class="ver  btn ink-reaction btn-raised  btn-primary" titulo="Registrar Proveedor" descipcion="El Usuario ha visualizado las partidas presupuestarias" idModificar="{$post.pk_num_registro_contrato}" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal">
                        SELECCIONAR PARTIDA PRESUPUESTARIA
                    </button>

                </div>
                &nbsp;&nbsp;
                <div class="col-sm-12 ">
                    <div class="card">
                        <div align="center" class="card-head card-head-xs style-primary">
                            <header>Partidas Presupuestarias</header>
                        </div>
                        <div style="padding: 10px;" >
                            <table id="partidasSeleccionadas" style="width: 100%;display:block;" class="table table-striped table-hover">
                                {$i=1}
                                <tr  >
                                    <th width="200">Partida </th>
                                    <th width="950">Descripción</th>
                                    <th width="140">Aprobado</th>
                                    <th width="140">Ajustado</th>
                                    <th width="140">Comprometido</th>
                                    <th width="140">Causado</th>
                                    <th width="140">Pagado</th>
                                    <th width="140">Borrar</th>
                                </tr>
                                {if isset($_PartidasSeleccionadasPost)}
                                    {foreach item=tipo from=$_PartidasSeleccionadasPost}

                                        <tr id="id{$i}" style="font-size: 12px;">
                                            <input id="idPartida{$i}" value="{$tipo.pk_num_partida_presupuestaria}" type="hidden">
                                            <th width="200"> <input name="form[txt][partidaDet{$i}]" class="form-control  text-medium " style="font-size: 12px;"  readonly  id="codigoPartida{$i}"  type="text" value="{$tipo.ind_partida}"/></th>
                                            <th width="950"> <input disabled name="form[txt][descripcion{$i}]" class="form-control   text-medium " readonly style="font-size: 12px;"  type="text" value="{$tipo.ind_denominacion}"/></th>
                                            <th width="170"> <input  id="idTmp{$i}"  name="form[txt][monto{$i}]"   onkeypress="return NumCheck(event, this)"  onchange=" LimiteProyecto(this.value,{$i})"    class="form-control {if $tipo.ind_partida=="403.18.01.00"} iva_{/if}    text-medium " style="font-size: 12px;"  type="text" value="{$tipo.num_monto|number_format:2:",":""}"/> </th>
                                            <th width="170"> <input  name="form[txt][Ajustado{$i}]" readonly onchange=" LimiteProyecto(this.value,{$i})"  id="AjTmp{$i}" class="form-control    {if $tipo.ind_partida=="403.18.01.00"} iva_{/if}  text-medium " style="font-size: 12px;"  type="text" value="{$tipo.num_monto_ajustado|number_format:2:",":""}"/> </th>
                                            <th width="170"> <input disabled id="Comprometido{$i}" readonly  name="form[txt][Comprometido{$i}]" class="form-control    text-medium " style="font-size: 12px;"  type="text" value="0"/> </th>
                                            <th width="170"> <input disabled id="Causado{$i}"   readonly  name="form[txt][Causado{$i}]" class="form-control    text-medium " style="font-size: 12px;"  type="text" value="0"/> </th>
                                            <th width="170"> <input disabled id="Pagado{$i}" readonly  name="form[txt][Pagado{$i}]" class="form-control    text-medium " style="font-size: 12px;"  type="text" value="0"/> </th>
                                            <td> <button class=" btn ink-reaction btn-raised btn-xs btn-danger" type="button" onclick="Borrando({$i});" >
                                                    <i class="md md-delete" style="color: #ffffff;"></i></button> </td>
                                        </tr>

                                        {$i=$i+1}

                                    {/foreach}
                                {/if}
                                <tbody id="distribucionPresupuestaria" style="height: 170px;display: inline-block;width: 100%;overflow: auto;">

                                </tbody>
                            </table>

                        </div>
                    </div>


                    <span id="cky-error" class="help-block" style="color: #FF0000">  </span>




                </div>









            </div>

            <div class="tab-pane" id="step5">
                <h4>Acción a realizar de aprobarse el proyecto.</h4>
                <div class="col-lg-12  ">
                    <div class="radio  ">
                        <div class="form-group">
                            <label><input {if $_ProyectoContratoPost[0]['num_guia_contrato']==1} checked{/if} type="radio"  value="1" name="form[int][guia]">Comprometer el monto total del contrato.</label>

                            <small> El monto del contrato será comprometido  mediante este módulo.</small>

                        </div>
                    </div>
                    <div class="radio">
                        <div class="form-group">
                            <label><input {if $_ProyectoContratoPost[0]['num_guia_contrato']==2} checked{/if} type="radio" value="2" name="form[int][guia]">Comprometer y causar el monto total del contrato.</label>

                            <small> El monto del contrato será comprometido y causado  mediante una obligación  a realizar en el menu "Listar Obligaciones Contrato " mediante este módulo.</small>

                        </div>
                    </div>

                    <div class="radio  ">

                        <div class="form-group">
                            <label><input {if $_ProyectoContratoPost[0]['num_guia_contrato']==3} checked{/if}  type="radio" value="3" name="form[int][guia]">Afectar presupuesto por ordenes de servicio.</label>

                            <small> El monto del contrato será afectado presupuestariamente por ordenes de servicio.</small>

                        </div>

                    </div>
                </div>




            </div><!--fin #step5 -->
            <span class="clearfix"></span>
            <div class="modal-footer">


                <button type="button" class="btn cancelar btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>


                {if $_Acciones.accion=="redactar" || $_Acciones.accion=="addendum"}
                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="boton_accion"><span class="glyphicon glyphicon-floppy-disk"></span> Guardar</button>
                {/if}
                {if $_Acciones.accion=="PorRevisar"}
                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="boton_accion"><span class="glyphicon glyphicon-floppy-disk"></span> Mandar a Revisión</button>
                {/if}
                {if $_Acciones.accion=="revisar"}
                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="boton_accion"><span class="glyphicon glyphicon-floppy-disk"></span> Revisado</button>
                {/if}
                {if $_Acciones.accion=="conformar"}
                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="boton_accion"><span class="glyphicon glyphicon-floppy-disk"></span> Conformado</button>
                {/if}
                {if $_Acciones.accion=="aprobar"}
                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="boton_accion"><span class="glyphicon glyphicon-floppy-disk"></span> Aprobado</button>
                {/if}
                {if $_Acciones.accion=="modificar"}
                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="boton_accion"><span class="glyphicon glyphicon-floppy-disk"></span> Modificar</button>
                {/if}

                {if $_Acciones.accion=="RevisarAddendum"}
                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="boton_accion"><span class="glyphicon glyphicon-floppy-disk"></span> Revisar Addendum </button>
                {/if}

                {if $_Acciones.accion=="AprobarAddendum"}
                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="boton_accion"><span class="glyphicon glyphicon-floppy-disk"></span> Aprobar Addendum </button>
                {/if}


            </div>
    </form>



    <div class="col-lg-12">
        <div class="form-group">
                        <textarea name="form[txt][cuerpo]" id="ckeditor">

                            {if (trim($_ProyectoContratoPost[0]['ind_cuerpo'])!="error"&&($_ProyectoContratoPost[0]['ind_cuerpo']!="En la "))}{$_ProyectoContratoPost[0]['ind_cuerpo']}
                            {else}
                                {$_ProyectoContratoPost[0]['ind_encabezado']}
                            {/if}
                         </textarea>
        </div>
    </div>


    <span class="clearfix"></span>
    <ul class="pager wizard">

        <li class="previous"><a class="btn-raised"       href="javascript:void(0);">Previo</a></li>

        <li class="next"><a class="btn-raised"           href="javascript:void(0);">Siguiente</a></li>

    </ul>
</div>





<script type="text/javascript">




    $.validator.setDefaults({

        submitHandler: function() {

            $("#formAjax :input").prop("disabled", false);

            var datos = $("#formAjax" ).serialize();

            var accion = $("#accion").val();

            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });


            if(accion=="PorRevisar"){
                var titulo =" Enviado a Revisión";
                var descripcion ="El proyecto de contrato ha sido enviado para su revisión.";
            }else if (accion=="revisar"){
                var titulo ="Proyecto Revisado";
                var descripcion ="El proyecto de contrato ha sido revisado.";
            }else if (accion=="conformar"){
                var titulo =" Proyecto Conformado";
                var descripcion ="El proyecto de contrato ha sido conformado.";
            }else if (accion=="aprobar"){
                var titulo =" Proyecto Aprobado";
                var descripcion ="El proyecto de contrato ha sido aprobado.";
            }
            if(accion=="modificar") {
                $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/ModificarProyectoMET", datos, function (dato) {

                    $(document.getElementById('idProyecto'+dato['idProyecto'])).html('<td class="sort-alpha col-sm-1">'+dato['idProyecto']+'</td>' +
                        '<td class="sort-alpha col-sm-4">'+dato['proveedor']+'</td>' +
                        '<td class="sort-alpha col-sm-2">'+dato['objeto']+'</td>' +
                        '<td class="sort-alpha col-sm-2">'+dato['estatus']+'</td>' +

                        '<td class="sort-alpha col-sm-2"> Bs. '+new Intl.NumberFormat("de-DE").format(dato["monto"])+'</td>'+

                        '<td  class="sort-alpha col-sm-1" >' +
                        '  <button class="movimientos logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Bitácora de Movimiento" titulo="Bitácora de Movimiento"  id="'+dato['idProyecto']+'" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal"><i class="md md-perm-identity" style="color: #ffffff;"></i></button>'+
                        '<td  class="sort-alpha col-sm-1" >' +
                        ' <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Ver Proyecto de Contrato" titulo="Ver Proyecto de Contrato"   idVer="'+dato['idProyecto']+'" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"> <i class="md md-remove-red-eye" style="color: #ffffff;"></i> </button>' +
                        '<i class="fa fa-edit" style="color: #ffffff;"></i></button></td>'+
                        '<td  class="sort-alpha col-sm-1" >' +
                        ' <form id="filtro" target="_blank"  action="{$_Parametros.url}modGC/reporteCONTROL/ProyectoContratoPdfMET"   class="form" role="form" method="post">'+
                        '<input type="hidden" name="idProyecto" value="'+dato['idProyecto']+'" >'+
                        '<button class="pdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"   idPdf="'+dato['idProyecto']+'"   descipcion="El Usuario a visualizado el proyecto de contrato N° '+dato['idProyecto']+'" >'+
                        '<i class="md md-description" style="color: #ffffff;"></i></button></form></td> '+
                        '<td  class="sort-alpha col-sm-1" >' +
                        botonModificar+' </td> '+
                        '<td  class="sort-alpha col-sm-1" >' +
                        botonAnular+' </td> ');
                    swal("Registro Modificado!", "Proyecto fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                }, 'json');
                swal("Proyecto Modificado!", "El proyecto de contrato ha sido modificado.", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');

            }
            else if (accion=="redactar") {


                $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/ModificarProyectoMET", datos, function (dato) {

                    var botonBitacora ='<button class="movimientos logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Bitácora de Movimiento" titulo="Bitácora de Movimiento"   id="'+respuesta_post[i].pk_num_registro_contrato+'" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal"><i class="md md-perm-identity" style="color: #ffffff;"></i></button>';

                    if(dato['estado']==1){

                        var botonModificar ='<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Modificar Proyecto de Contrato"  titulo="Modificar Proyecto de Contrato"   idModificar="'+dato['idProyecto']+'" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"><i class="fa fa-edit" style="color: #ffffff;"></i>  </button>';

                    }else{

                        var botonModificar ='';
                    }

                    if(dato['estado']!=6){

                        var botonAnular ='<button class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAnular="'+dato['idProyecto']+'" mensaje="¿Estás seguro que desea anular el proyecto de contrato y las obligaciones relacionadas al proyecto?" title="¿Estás Seguro que desea anular?" titulo="¿Estás Seguro que desea anular?" descipcion="El usuario anuló un proyecto de contrato" boton="si, Eliminar" ><i class="md md-delete" style="color: #ffffff;"></i></button>';

                    }else{

                        var botonAnular ='';
                    }

                    $(document.getElementById('idProyecto'+dato['idProyecto'])).html('<td class="sort-alpha col-sm-1">'+dato['idProyecto']+'</td>' +
                        '<td class="sort-alpha col-sm-4">'+dato['proveedor']+'</td>' +
                        '<td class="sort-alpha col-sm-2">'+dato['objeto']+'</td>' +


                        '<td class="sort-alpha col-sm-2"> Bs. '+new Intl.NumberFormat("de-DE").format(dato["monto"])+'</td>'+

                        '<td  class="sort-alpha col-sm-1" >' +
                        botonBitacora+
                        '<td  class="sort-alpha col-sm-1" >' +
                        '<button class="redactar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Redactar Proyecto de Contrato" descipcion="El Usuario a Modificado la redacción de un Proyecto de Contrato" idModificar="'+dato['idProyecto']+'" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"><i class="fa fa-edit" style="color: #ffffff;"></i></button>'+
                        '<td  class="sort-alpha col-sm-1" >' +
                        '  <button class="enviar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" titulo="Enviar Proyecto de Contrato" descipcion="El Usuario a Modificado la redacción de un Proyecto de Contrato" idEnviar="'+dato['idProyecto']+'" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"><i class="md md-done" style="color: #ffffff;"></i></button>'+

                        '<td  class="sort-alpha col-sm-1" >' +
                        ' <form id="filtro" target="_blank"  action="{$_Parametros.url}modGC/reporteCONTROL/ProyectoContratoPdfMET"   class="form" role="form" method="post">'+
                        '<input type="hidden" name="idProyecto" value="'+dato['idProyecto']+'" >'+
                        '<button class="pdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"   idPdf="'+dato['idProyecto']+'"   descipcion="El Usuario a visualizado el proyecto de contrato N° '+dato['idProyecto']+'" >'+
                        '<i class="md md-description" style="color: #ffffff;"></i></button></form></td> '+

                        '<td  class="sort-alpha col-sm-1" >' +
                        botonAnular+' </td> ');
                    swal("Registro Modificado!", "Proyecto fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                }, 'json');






            }
            else if (accion=="addendum") {

                if($('#formAjax').find(':input').prop('disabled', false)){

                    $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/RegistrarProyectoMET", datos, function (dato) {

                        if(dato['status']=='error') {
                            swal("Error", " Hay campos del formulario por completar", "error");
                        }else{
                            $(document.getElementById('idAddendum'+dato)).html('');
                            swal("Addendum Agregado!", "El Addendum del contrato ha sido agregado.", "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');

                        }



                    }, 'json');

                }


            }
            else if (accion=="RevisarAddendum") {

                if($('#formAjax').find(':input').prop('disabled', false)){

                    $.post("{$_Parametros.url}modGC/addendumCONTROL/SubirAddendumMET", datos, function (dato) {

                        if(dato['status']=='error') {
                            swal("Error", " Hay campos del formulario por completar", "error");
                        }else{
                            $(document.getElementById('idAddendum'+dato)).html('');
                            swal("Addendum Agregado!", "El Addendum del contrato ha sido revisado.", "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');

                        }

                    }, 'json');

                }

            }
            else if (accion=="AprobarAddendum") {

                if($('#formAjax').find(':input').prop('disabled', false)){

                    $.post("{$_Parametros.url}modGC/addendumCONTROL/SubirAddendumMET", datos, function (dato) {

                        if(dato['status']=='error') {
                            swal("Error", " Hay campos del formulario por completar", "error");
                        }else{
                            $(document.getElementById('idAddendum'+dato)).html('');
                            swal("Addendum Agregado!", "El Addendum del contrato ha sido aprobado.", "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');

                        }

                    }, 'json');

                }

            }
            else{
                $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/SubirProyectoMET", datos, function (dato) {

                    $(document.getElementById('idProyecto'+dato)).html('');

                }, 'json');
                swal(titulo, descripcion, "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');

            }



        }
    });
    function Borrando (id) {

        $('#id'+id).remove();
        var Limite=$('#Monto').val().replace(/[_.]/gi,'');
        Limite=Limite.replace(",",".");
        var Facturas=$('#NPartidasSeleccionadas').val();
        var i=0;
        var Total=0;

        for( i; i<=Facturas;i++){

            var tmp=$('#AjTmp'+i).val();

            if(tmp!=undefined){

                Total=parseFloat(Total+parseFloat((tmp.replace(",","."))));
            }

        }


        if(parseFloat(Total)<=parseFloat(Limite) ){
            $('#cky-error').html('');
            $('#boton_accion').show('fast');
        }else{
            $('#boton_accion').hide('fast');
            $('#cky-error').html("  Ha sobrepasado con la suma total , el monto especificado en el primer paso de este formulario.");

        }


    }

    $(document).ready(function() {


        $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/CalcularIvaMET",{ iva: $("#fk_cpb017_num_tipo_servicio").val() }, function (dato) {

            $("#Factor").val(dato[0]['num_factor_porcentaje']);

            if($('#NPartidasSeleccionadas').val()>1){


            }
        },'json');



        $("#formAjax").keypress(function(e){
            if(e.which == 13){
                return false;
            }
        });

        $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/PresupuestoContratoMET", { id:$("#idRegistro").val() }, function (dato) {

            for(var i=0; i< dato.length ; i++){

                if(dato[i]['Comprometido']==null){
                    $("#Comprometido"+(i+1)).val(0);
                }else{
                    $("#Comprometido"+(i+1)).val(dato[i]['Comprometido']);
                }

                if(dato[i]['Causado']==null){
                    $("#Causado"+(i+1)).val(0);
                }else{
                    $("#Causado"+(i+1)).val(dato[i]['Causado']);
                }

                if(dato[i]['Pagado']==null){
                    $("#Pagado"+(i+1)).val(0);
                }else{
                    $("#Pagado"+(i+1)).val(dato[i]['Pagado']);
                }

            }

        }, 'json');

        $('.ver').click(function () {
            var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/ListarPartidasContratoMET';
            $('#modalAncho2').css( "width", "80%" );
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($url,{ id:  $('#NPartidasSeleccionadas').val()},function($dato){
                $('#ContenidoModal2').html($dato);
            });
        });

        $("#formAjax").validate();
        $('#select2').select2();
        $('.fechas2').datepicker({ format: 'dd/mm/yyyy' });
        $('#fechas').datepicker( { format: 'dd/mm/yyyy' });
        $('.Monto').inputmask(' 999.999.999,99 BS', { numericInput: true });
        $( '#ckeditor' ).ckeditor();
        $('#NPartidasSeleccionadas').val({$i});
        $("#idSolicitado").change(function (){

            $("#idSolicitado option:selected").each(function () {

                $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/ActualizarTipoContratoMET", { idSolicitado: $('#idSolicitado').val() }, function (data) {
                    var json = data,
                        obj = JSON.parse(json);
                    $("#idTipoContrato").html(obj);
                });
            });
        });

        $("#idTipoContrato").change(function () {

            $("#idTipoContrato option:selected").each(function () {

                $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/ActualizarAplicableMET", { idTipoContrato: $('#idTipoContrato').val() }, function(data){
                    var json = data,
                        obj = JSON.parse(json);
                    $("#idAplicable").html(obj);
                });
            });
        });



        $("#idDocumento").change(function () {

            $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/BuscarRepresentanteMET", { idDocumento: $('#idDocumento').val() }, function(data){

                var json = data,
                    obj = JSON.parse(json);

                if(obj[0]!=undefined) {
                    $("#idDocumentoRepresentante").val(obj[0]['ind_cedula_documento']);
                    $("#CodRepresentante").val(obj[0]['pk_num_persona']);
                    $("#Representante").val(obj[0]['ind_nombre1']+" "+obj[0]['ind_apellido1']);
                    $(".persona").hide();
                }
            });

        });




        $("#idDocumentoRepresentante").change(function () {

            $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/VisualizarNombreMET", { idTipoDocumento: $('#idTipoDocumentoRepresentante').val(),idDocumento: $('#idDocumentoRepresentante').val() }, function(data){

                var json = data,
                    obj = JSON.parse(json);
                if(obj[0]!=undefined) {

                    $("#CodRepresentante").val(obj[0]['pk_num_persona']);
                    $("#Representante").val(obj[0]['ind_nombre1']+" "+obj[0]['ind_apellido1']);
                    $(".persona").hide();

                }else{

                    $("#Representante").val('');
                    $("#formAjax").validate().element("#Representante");
                    $(".persona").show();

                }
            });

        });

        $('.proveedor').click(function () {
            var $url='{$_Parametros.url}modLG/maestros/proveedorCONTROL/CrearModificarProveedorMET';

            $('#formModalLabel3').html($(this).attr('titulo'));
            $.post($url,{ idPost: $(this).attr('idModificar')},function($dato){
                $('#ContenidoModal3').html($dato);
                $('#modalAncho3').css( "width", "85%" );
            });
        });


        if($("#accion").val()=='modificar') {

            $('#formAjax').find(':input').prop('disabled', false)

            var Limite = $('#Monto').val().replace(/[_.]/gi, '');
            Limite = Limite.replace(",", ".");
            var Facturas = $('#NPartidasSeleccionadas').val();
            var i = 0;
            var Total = 0;

            for (i; i <= Facturas; i++) {

                var tmp = $('#AjTmp' + i).val();

                if (tmp != undefined) {
                    Total = parseFloat(Total + parseFloat((tmp.replace(",", "."))));
                    var idPartida = $("#idPartida" + i).val();
                    verificarDisponibilidad(parseFloat((tmp.replace(",", "."))), idPartida, i);
                }

            }


            if (parseFloat(Total) != parseFloat(Limite)) {
                $('#boton_accion').hide('fast');
                $('#cky-error').html(' El Monto ingresado es menor al indicado en la pestaña General');
            }


        }
    });


    function verificarDisponibilidad(monto, idPartida, posicion){
        $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/VerificarDisponibilidadPartidaMET",{ monto: monto, idPartida: idPartida }, function (dato) {
            var porcentaje = dato['porcentaje'];
            if(porcentaje <= 60){
                var color = "green";
            }
            if(porcentaje >= 61 && porcentaje <= 99){
                var color = "yellow";
            }
            if(porcentaje > 99){
                var color = "red";
            }
            $("#id"+posicion).css("background-color", color);
        },'json');

    }

    $("#fk_cpb017_num_tipo_servicio").on('change',function(){



        $.post("{$_Parametros.url}modGC/proyectoContratoCONTROL/CalcularIvaMET",{ iva: $("#fk_cpb017_num_tipo_servicio").val()}, function (dato) {


            if(dato!=""){

                if(dato[0]['tipo']=="IVA"){
                    $("#Factor").val(dato[0]['num_factor_porcentaje']);
                }else{
                    $("#Factor").val('0');
                }

            }else{
                $("#Factor").val('0');
            }



        },'json');

    });

    function LimiteProyecto (valor,id){


        var Limite=$('#Monto').val().replace(/[_.]/gi,'');
        Limite=Limite.replace(",",".");
        var Facturas=$('#NPartidasSeleccionadas').val();
        var i=0;
        var Total=0;
        var iva=$("#Factor").val();
        var acumulado=0;

        if(parseFloat(valor) <= parseFloat(Limite)){
            $('#cky-error').html('');
            $('#boton').show('fast');

            $('#AjTmp'+id).val(valor);

            for( i; i<=Facturas;i++){

                var tmp=$('#AjTmp'+i).val();

                if(tmp!=undefined){

                    var idPartida = $("#idPartida"+i).val();

                    if($("#codigoPartida"+i).val()!='403.18.01.00'){

                        Total=parseFloat(Total+parseFloat((tmp.replace(",","."))));
                        acumulado=acumulado+(parseFloat(iva)*parseFloat((tmp.replace(",","."))))/100;

                    }

                    verificarDisponibilidad(parseFloat((tmp.replace(",","."))), idPartida, i);

                }

            }

            Total=Total+acumulado;

            if(parseFloat(Total)<parseFloat(Limite) ){
                $('#boton').hide('fast');
                $('#cky-error').html(' El Monto ingresado es menor al indicado en la pestaña General');
                $(".iva_").val(acumulado);

            }else if (parseFloat(Total)>parseFloat(Limite) ){
                $('#boton').hide('fast');

                swal({
                    title: "Ha sobrepasado el monto!.",
                    text: "Ha sobrepasado en la suma total , el monto especificado en el primer paso de este formulario.",
                    timer: 50000000,
                    showConfirmButton: true
                });
                $('#idTmp'+id).val('0,00');
                $('#AjTmp'+id).val('0,00');
                $(".iva_").val(0);

            }else if (parseFloat(Total)==parseFloat(Limite) ){

                $(".iva_").val(acumulado);
                $('#boton').show('fast');

            }


        }else{
            $('#boton').hide('fast');
            $('#idTmp'+id).val('0,00');
            $('#AjTmp'+id).val('0,00');
            swal({
                title: "Ha sobrepasado el monto!.",
                text: "Ha sobrepasado el monto especificado en el primer paso de este formulario.",
                timer: 50000000,
                showConfirmButton: true
            });

        }



    }



    function NumCheck(e, field) {

        key = e.keyCode ? e.keyCode : e.which

        // backspace

        if (key == 8) return true

        // 0-9

        if (key > 47 && key < 58) {

            if (field.value == "") return true

            regexp = /.[0-9]{2}$/

            return !(regexp.test(field.value))

        }

        // .

        if (key == 46) {

            if (field.value == "") return false

            regexp = /^[0-9]+$/

            return regexp.test(field.value)

        }

        // other key

        return false

    }

    $('.accionModal').click(function () {

        accionModal(this,'url')
    });

    function accionModal(id,attr){

        $('#formModalLabel2').html($(id).attr('titulo'));
        $.post($(id).attr(attr), {
            cargar: 0,

            tr: $("#"+$(id).attr('idTabla')+" > tbody > tr").length + 1
        }, function ($dato) {
            $('#ContenidoModal2').html($dato);
        });
    }

</script>