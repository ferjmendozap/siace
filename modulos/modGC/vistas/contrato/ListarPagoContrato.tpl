<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">LISTAR CONTRATOS</h2></header>
    </div>
    <div class="col-md-12">
        <h5>Listado de lasordenes de pago relacionados con los contratos .</h5>
    </div><!--end .col -->

</div><!--end .card -->


<div class="form-group   col-lg-12 ">

    <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
</div>

</div><!--end .card -->

<section class="style-default-bright">



    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <div class="btn-group pull-right ">
                    <a data-toggle="offcanvas" title="Filtro" class="  btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"> <i class="fa fa-filter"></i> </a>
                </div><br>
                <br/>
                <table id="datatable1" class="table table-striped table-hover">

                    <thead>
                    <tr>
                        <th class="sort-alpha col-sm-1">N° </th>

                        <th class="sort-alpha col-sm-4">Proveedor </th>

                        <th class="sort-alpha col-sm-2">Objeto del Contrato</th>

                        <th class="sort-alpha col-sm-2">Monto</th>

                        {if in_array('GC-01-01-01-03-B',$_Parametros.perfil)} <th class=" col-sm-1">Bitácora</th> {/if}

                        {if in_array('GC-01-01-01-04-P',$_Parametros.perfil)} <th class=" col-sm-1">Ver Contrato</th>      {/if}

                        {if in_array('GC-01-01-01-11-O',$_Parametros.perfil)} <th class=" col-sm-1">Listar Ordenes</th>    {/if}

                        {if in_array('GC-01-01-01-11-O',$_Parametros.perfil)} <th class=" col-sm-1">Nueva Orden de Pago</th>    {/if}



                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_contratoPost}

                        <tr id="idPost{$post.pk_num_registro_contrato}" class="gradeA">
                            <td>{$post.pk_num_registro_contrato}</td>
                            <td>{$post.ind_nombre1}</td>
                            <td>{$post.ind_objeto_contrato}</td>

                            <td>Bs. {$post.num_monto_contrato|number_format:2:",":""}     </td>
                            {if in_array('GC-01-01-01-03-B',$_Parametros.perfil)}

                            <td>
                                    <button class="movimientos logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Bitácora de Movimientos" titulo="Bitácora de Movimientos" descipcion="El Usuario a Visualizado un Proyecto de Contrato  " id="{$post.pk_num_registro_contrato}" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal">
                                        <i class="md md-perm-identity" style="color: #ffffff;"></i>
                                    </button>

                            </td>
                            {/if}


                            {if in_array('GC-01-01-01-04-P',$_Parametros.perfil)}

                            <td>
                                <form id="filtro"  action="{$_Parametros.url}modGC/reporteCONTROL/ProyectoContratoPdfMET"  target="_blank" class="form" role="form" method="post">
                                    <input type="hidden" name="idProyecto" value="{$post.pk_num_registro_contrato}"/>
                                    <button class="pdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Ver Pdf"  idPdf="{$post.pk_num_registro_contrato}"   descipcion="El Usuario a visualizado el proyecto de contrato N° {$post.pk_num_registro_contrato}" >
                                        <i class="md md-description" style="color: #ffffff;"></i>
                                    </button>
                                </form>
                            </td>

                            {/if}



                            {if in_array('GC-01-01-01-11-O',$_Parametros.perfil)}
                                <td>{if ($post.fk_a006_num_estado_contrato!='6')}
                                    <button class="obligacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  title="Ver Obligaciones de GC" titulo="Ver Obligaciones de GC"   idObligacion="{$post.pk_num_registro_contrato}" data-backdrop="false" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                            <i class="md md-assignment-late" style="color: #ffffff;"></i>
                                        </button>{/if}

                                </td>
                            {/if}

                            {if in_array('GC-01-01-01-11-O',$_Parametros.perfil)}
                                <td>{if ($post.fk_a006_num_estado_contrato!='6')}
                                    <button class="obligacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"  title="Ver Obligaciones de GC" titulo="Ver Obligaciones de GC"   idObligacion="{$post.pk_num_registro_contrato}" data-backdrop="false" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                            <i class="md md-assignment-late" style="color: #ffffff;"></i>
                                        </button>{/if}

                                </td>
                            {/if}


                        </tr>
                    {/foreach}
                    </tbody>
 </form>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->
    <!-- filtro -->
    <div class="offcanvas">
        <div id="offcanvas-filtro" class="offcanvas-pane width-9">
            <div class="offcanvas-head">
                <header>Filtro</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                </div>
            </div>

            <div class="offcanvas-body">
                <form id="filtro"   class="form" role="form" method="post">
                    <input type="hidden" name="filtro" value="1">
                    <div class="row">

                        <div class="col-sm-12  ">
                            <div class="form-group ">
                                <select  class="form-control" id="filtro_dependencia">
                                    <option value="0"> Todas </option>
                                    {foreach item=tipo from=$_DependenciaPost}
                                        {if isset($_DependenciaSelect)}


                                            {if ($tipo.pk_num_dependencia==$_DependenciaSelect)}

                                                <option selected  value="{$tipo.pk_num_dependencia}"> {$tipo.ind_dependencia}</option>

                                            {else}
                                                <option value="{$tipo.pk_num_dependencia}"> {$tipo.ind_dependencia}</option>
                                            {/if}

                                        {else}
                                            <option value="{$tipo.pk_num_dependencia}"> {$tipo.ind_dependencia}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="select1">Dependencias</label>
                            </div>
                        </div>
                        <div class="col-sm-12  ">
                            <div class="form-group ">
                                <select   class="form-control" id="filtro_proveedor">
                                    <option value="0"> Todos </option>
                                    {foreach item=tipo from=$_ProveedorPost}

                                        {if isset($_ProveedorSelect)}
                                            {if ($tipo.pk_num_proveedor==$_ProveedorSelect)}
                                                <option selected value="{$tipo.pk_num_proveedor}">{$tipo.ind_nombre1} </option>
                                            {else}
                                                <option   value="{$tipo.pk_num_proveedor}">{$tipo.ind_nombre1} </option>
                                            {/if}
                                        {else}
                                            <option  value="{$tipo.pk_num_proveedor}">{$tipo.ind_nombre1} </option>
                                        {/if}

                                    {/foreach}
                                </select>
                                <label for="select1"> Proveedor </label>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button  type="button" onclick="Filtro()" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                                Filtrar
                            </button>
                        </div>
                    </div>




                </form>
            </div>
        </div>
    </div>



    </div><!--end .row -->


    <script type="text/javascript">
        $(document).ready(function() {
            var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/RegistrarProyectoMET';
            $('#nuevo').click(function(){
                $('#formModalLabel').html($(this).attr('titulo'));
                 $.post($url,{ idMenu:0 },function($dato){
                    $('#ContenidoModal').html($dato);
                 });



            });

            $('#modalAncho').css( "width", "85%" );

            $('#datatable1 tbody').on( 'click', '.modificar', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/ModificarProyectoMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idModificar'),accion:"Modificar"},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });



            $('#datatable1 tbody').on( 'click', '.obligacion', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/ListarProyectoObligacionMET';

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idContrato: $(this).attr('idObligacion'),accion:"Modificar"},function($dato){
                    $('#ContenidoModal').html($dato);
                });

            });


            $('#datatable1 tbody').on( 'click', '.nueva_obligacion', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/NuevaObligacionMET';
                    $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idContrato: $(this).attr('idContrato')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });



            $('#datatable1 tbody').on( 'click', '.movimientos', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/MovimientosMET';


                $('#formModalLabel2').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('id')},function($dato){
                    $('#ContenidoModal2').html($dato);
                });
                $('#modalAncho2').css( "width", "55%" );
            });

            $('#datatable1 tbody').on( 'click', '.anular', function () {

                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/AnularProyectoContratoMET';
                var idPost=$(this).attr('idAnular');

                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){

                    var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/AnularProyectoMET';


                    $.post($url,{ idPost: idPost},function($dato){
                        swal("Anulado!", "El proyecto fue anulado correctamente.", "success");
                    });

                  //  window.setTimeout('location.reload()', 2000);
                });
            });





        });


        // Buscador
        function Filtro()
        {

            var filtro_proveedor = $("#filtro_proveedor").val();
            var filtro_dependencia = $("#filtro_dependencia").val();
            var filtro_estatus = 5;

            var url_listar='{$_Parametros.url}modGC/proyectoContratoCONTROL/FiltroProyectoMET';
            $.post(url_listar,{ filtro_proveedor: filtro_proveedor, filtro_dependencia: filtro_dependencia, filtro_estatus: filtro_estatus  },function(respuesta_post) {


                var tabla_listado = $('#datatable1').DataTable();
                tabla_listado.clear().draw();

                if(respuesta_post != -1) {
                    for(var i=0; i<respuesta_post.length; i++) {
                        var botonCargar = '<button type="button" pk_num_empleado = "' + respuesta_post[i].num_monto_contrato + '" class="cargar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descripcion="El usuario ha buscado un empleado" title="Cargar empleado"><i class="glyphicon glyphicon-download"></i></button>';

                        var botonBitacora ='<button class="movimientos logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Bitácora de Movimiento" titulo="Bitácora de Movimiento"   id="'+respuesta_post[i].pk_num_registro_contrato+'" data-backdrop="static" data-keyboard="false" data-target="#formModal2" data-toggle="modal"><i class="md md-perm-identity" style="color: #ffffff;"></i></button>';

                        if(respuesta_post[i].fk_a006_num_estado_contrato!=6){

                            var botonListarObligaciones ='<button class="obligacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Ver Obligaciones de GC" titulo="Ver Obligaciones de GC"   idObligacion="'+respuesta_post[i].pk_num_registro_contrato+'" data-backdrop="false" data-keyboard="false" data-target="#formModal" data-toggle="modal"><i class="md md-assignment-late" style="color: #ffffff;"></i></button>';
                            var botonNuevaObligacion =   '<button class="nueva_obligacion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Nueva Obligaciones de GC" titulo="Nueva Obligaciones de GC"  idContrato="'+respuesta_post[i].pk_num_registro_contrato+'" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal"><i class="md md-create" style="color: #ffffff;"></i>  </button>';
                        }else{

                            var botonListarObligaciones ='';
                            var botonNuevaObligacion ='';
                        }




                        var botonPDF =' <form id="filtro" target="_blank"  action="{$_Parametros.url}modGC/reporteCONTROL/ProyectoContratoPdfMET"   class="form" role="form" method="post">'+
                                '<input type="hidden" name="idProyecto" value="'+respuesta_post[i].pk_num_registro_contrato+'" >'+
                                '<button class="pdf logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"   title="Ver Pdf"  idPdf="'+respuesta_post[i].pk_num_registro_contrato+'"   descipcion="El Usuario a visualizado el proyecto de contrato N° '+respuesta_post[i].pk_num_registro_contrato+'" >'+
                                '<i class="md md-description" style="color: #ffffff;"></i></button></form>';
                        tabla_listado.row.add([

                            respuesta_post[i].pk_num_registro_contrato,
                            respuesta_post[i].ind_nombre1,
                            respuesta_post[i].ind_objeto_contrato,


                            "Bs. "+new Intl.NumberFormat("de-DE").format(respuesta_post[i].num_monto_contrato),
                            botonBitacora,

                            botonPDF,
                            botonListarObligaciones ,
                            botonNuevaObligacion
                        ]).draw()
                                .nodes()
                                .to$()
                    }
                }


            },'json');

        }



    </script>
    </div>