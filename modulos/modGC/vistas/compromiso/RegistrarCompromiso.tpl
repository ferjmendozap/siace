<form action="" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido"  id="valido" />
        <input type="hidden"  value="{if isset($_compromisoBDPost[0]['ind_correlativo_contrato'])}{$_compromisoBDPost[0]['ind_correlativo_contrato']}{/if}" name="Correlativo"  id="Correlativo" />
        <input type="hidden"  value="{if isset($_compromisoBDPost[0]['pk_num_registro_contrato'])}{$_compromisoBDPost[0]['pk_num_registro_contrato']}{/if}" name="Contrato" />
        <input type="hidden"  value="{if isset($_compromisoBDPost[0]['ind_estado'])}{$_compromisoBDPost[0]['ind_estado']}{/if}" name="Estatus" />
        <input type="hidden"  value="{if isset($_compromisoBDPost[0]['pk_num_comprometido'])}{$_compromisoBDPost[0]['pk_num_comprometido']}{/if}" name="idComprometido" />
                                <div class="col-lg-6">

                                    <input type="hidden" value="{$_Acciones.accion}"    id="accion" />
                                        <div class="col-lg-12">
                                            <div class="form-group">   {if !isset($_compromisoBDPost[0]['pk_num_comprometido'])}
                                                <select  required="" class="form-control" name="idContrato" id="idContrato" aria-required="true">
                                                    <option value="">&nbsp;</option>



                                                    {foreach item=tipo from=$_compromisoPost}

                                                        {if isset($_compromisoBDPost[0]['pk_num_comprometido'])}

                                                            {if $tipo.pk_num_registro_contrato==$_compromisoBDPost[0]['pk_num_comprometido']}
                                                                <option selected value="{$tipo.pk_num_registro_contrato}">{$tipo.ind_correlativo_contrato}    </option>
                                                            {else}
                                                                <option value="{$tipo.pk_num_registro_contrato}">{$tipo.ind_correlativo_contrato}  </option>
                                                            {/if}
                                                        {else}
                                                            <option value="{$tipo.pk_num_registro_contrato}">  {$tipo.ind_correlativo_contrato}  </option>
                                                        {/if}
                                                    {/foreach}




                                                </select>
                                                <label for="rangelength2"> Contratos Aprobados </label>
                                                {/if}
                                            </div>
                                        </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="col-lg-12 form-group ">

                                            <input readonly maxlength="100" value="{if isset($_compromisoBDPost[0]['ind_objeto_contrato'])}{$_compromisoBDPost[0]['ind_objeto_contrato']}{/if}" type="text" class="form-control" id="Objeto"  data-rule-rangelength="[2, 100]" required>
                                            <label for="rangelength2">Objeto del Contrato</label>

                                    </div>

                                    <div class="col-lg-12 form-group ">

                                        <input readonly maxlength="100" value="{if isset($_compromisoBDPost[0]['num_monto_contrato'])}{$_compromisoBDPost[0]['num_monto_contrato']}{/if}" type="text" class="form-control" id="Monto"  data-rule-rangelength="[2, 100]" required>
                                        <label for="rangelength2">Monto del Contrato</label>

                                    </div>

                                    <div class="col-lg-12   ">
                                        <div class="col-lg-6 form-group">
                                        <input readonly maxlength="100" value="{if isset($_compromisoBDPost[0]['nombre_preparado'])}{$_compromisoBDPost[0]['nombre_preparado']} {$_compromisoBDPost[0]['apellido_preparado']} {/if}" type="text" class="form-control"     >
                                        <label for="rangelength2">Preparado por</label>
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <input readonly maxlength="100" value="{if isset($_compromisoBDPost[0]['fec_preparado'])}{$_compromisoBDPost[0]['fec_preparado']}{/if}" type="text" class="form-control" id="AccesorioVehiculo"    >
                                            <label for="rangelength2">Fecha </label>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 form-group ">

                                        <div class="col-lg-6 form-group">
                                            <input readonly maxlength="100" value="{if isset($_compromisoBDPost[0]['nombre_revisado'])}{$_compromisoBDPost[0]['nombre_revisado']} {$_compromisoBDPost[0]['apellido_revisado']} {/if}" type="text" class="form-control"     >
                                            <label for="rangelength2">Revisado por</label>
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <input readonly  maxlength="100" value="{if isset($_compromisoBDPost[0]['fec_revisado'])}{$_compromisoBDPost[0]['fec_revisado']}{/if}" type="text" class="form-control"  >
                                            <label for="rangelength2">Fecha </label>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 form-group ">

                                        <div class="col-lg-6 form-group">
                                            <input readonly maxlength="100" value="{if isset($_compromisoBDPost[0]['nombre_aprobado'])}{$_compromisoBDPost[0]['nombre_aprobado']} {$_compromisoBDPost[0]['apellido_aprobado']} {/if}" type="text" class="form-control"     >
                                            <label for="rangelength2">Aprobado por</label>
                                        </div>
                                        <div class="col-lg-6 form-group">
                                            <input readonly maxlength="100" value="{if isset($_compromisoBDPost[0]['fec_aprobado'])}{$_compromisoBDPost[0]['fec_aprobado']}{/if}" type="text" class="form-control" >
                                            <label for="rangelength2">Fecha </label>
                                        </div>

                                    </div>




                                </div>
        <span class="clearfix"></span>

        <div class="modal-footer">

            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha cancelado registro de la entrada de documento" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>

            <button type="submit" class="btn btn-primary ink-reaction btn-raised"  ><span class="glyphicon glyphicon-floppy-disk"></span>Comprometer</button>


        </div>

    </div>
</form>



<script type="text/javascript">

    $.validator.setDefaults({
        submitHandler: function() {


            var datos = $("#formAjax" ).serialize();



            var accion = $("#accion").val();

            if(accion=="registrar") {

                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });

          $.post("{$_Parametros.url}modGC/compromisoCONTROL/RegistrarCompromisoMET", datos, function (dato) {


              if(dato['status']=='error') {


                  swal("Error", "No se pudo realizar operacion. Verifique que los valores sean los correctos o Consulte con el Administrador del Sistema.", "error");

              }else{
                      $(document.getElementById('datatable1')).append('<tr  id="idComprometido'+dato['idComprometido']+'">' +
                      '<td>'+dato['idComprometido']+'</td>' +
                      '<td> '+dato['Correlativo']+'</td>' +
                      '<td> '+dato['Objeto']+'</td>' +
                      '<td> '+dato['Monto']+'</td>' +
                      '<td> '+dato['Estatus']+'</td>' +
                      '<td   class="sort-alpha col-sm-1" >' +
                      '{if in_array('GC-01-01-01-01-V',$_Parametros.perfil)}<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                      'data-keyboard="false" data-backdrop="static" idVer="'+dato['idContrato']+'"' +
                      'titulo="Ver Contrato">' +
                      '<i class="md md-remove-red-eye" style="color: #ffffff;"></i></button> {/if}&nbsp;&nbsp;&nbsp;&nbsp;' +
                      '{if in_array('GC-01-01-01-01-V',$_Parametros.perfil)} <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idComprometido="'+dato['idComprometido']+'"  boton="si, Eliminar"' +
                      'descipcion="El usuario a eliminado un Accesorio" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Accesorio!!">' +
                      '<i class="md md-delete" style="color: #ffffff;"></i></button>{/if}' +
                      '</td>' +
                      '</tr>');

                        swal("Compromiso  Agregado!", "Compromiso ha sido agregado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                  }

                }, 'json');



            }else {
                $.post("{$_Parametros.url}modGC/compromisoCONTROL/ModificarCompromisoMET", datos, function (dato) {

                    $(document.getElementById('idComprometido'+dato['idComprometido'])).html('');
                    if(dato['Estatus']=="RV"){
                        swal("Compromiso Revisado!", "El compromiso ha sido revisado satisfactoriamente.", "success");
                    }else if (dato['Estatus']=="AP"){
                        swal("Compromiso Aprobado!", "El compromiso ha sido aprobado satisfactoriamente y el monto ha sido comprometido en el sistema.", "success");
                    }

                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }, 'json');


            }



        }
    });


    $(document).ready(function() {


        $("#formAjax").validate();


    });

    $("#idContrato").change(function () {

        $("#idContrato option:selected").each(function () {

            $.post("{$_Parametros.url}modGC/compromisoCONTROL/ConsultarContratoMET", { idContrato: $('#idContrato').val() }, function(dato){
                var datos= JSON.parse(dato)
                $("#Objeto").val(datos['objeto']);
                $("#Monto").val(datos['monto']);
                $("#Correlativo").val(datos['correlativo']);



            });
        });
    });



</script>