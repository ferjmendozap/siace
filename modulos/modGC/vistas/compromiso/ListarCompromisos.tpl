<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div id="body1">
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">LISTAR COMPROMISOS</h2></header>
    </div>
    <div class="col-md-12">
        <h5>Listado de los compromisos realizados por este módulo.</h5>
    </div><!--end .col -->

</div><!--end .card -->


<div class="form-group   col-lg-12 ">

    <div class="col-lg-11 "></div>	<a class="btn btn-flat btn-accent ink-reaction span8" href="javascript:void(0);">filtrar</a>
</div>

</div><!--end .card -->

<section class="style-default-bright">



    <div class="row">

        <div class="col-lg-12">
            <div class="table-responsive">
                <div class="btn-group pull-right ">
                    <a data-toggle="offcanvas" title="Filtro" class="  btn ink-reaction btn-floating-action btn-sm btn-primary" href="#offcanvas-filtro"> <i class="fa fa-filter"></i> </a>
                </div><br>
                <br/>
                <table id="datatable1" class="table table-striped table-hover">

                    <thead>
                    <tr>

                        <th class="sort-alpha col-sm-1">N° </th>

                        <th class="sort-alpha col-sm-2">Contrato </th>

                        <th class="sort-alpha col-sm-4">Objeto del Contrato</th>

                        <th class="sort-alpha col-sm-2">Monto</th>

                        <th class="sort-alpha col-sm-2">Estatus</th>

                        <th class="sort-alpha col-sm-2">Acciones</th>



                    </tr>
                    </thead>
                    <tbody>

                    {foreach item=post from=$_compromisoPost}

                        <tr id="idCompromiso{$post.pk_num_comprometido}" class="gradeA">
                            <td>{$post.pk_num_comprometido}</td>
                            <td>{$post.ind_correlativo_contrato}</td>
                            <td>{$post.ind_objeto_contrato}</td>

                            <td>Bs. {$post.num_monto_contrato|number_format:2:",":""}     </td>
                            {if in_array('GC-01-01-01-03-B',$_Parametros.perfil)}

                                <td>{$post.ind_estado}</td>

                            {/if}

                            <td>
                            {if in_array('GC-01-01-01-01-V',$_Parametros.perfil)}

                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" title="Ver Contrato" titulo="Ver Contrato"   idVer="{$post.pk_num_registro_contrato}" data-backdrop="static" data-keyboard="false" data-target="#formModal" data-toggle="modal">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>

                            {/if}
                            {if in_array('GC-01-01-01-01-V',$_Parametros.perfil)}

                                    <button title='Anular Compromiso'class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idContrato="{$post.pk_num_registro_contrato}" idCompromiso="{$post.pk_num_comprometido}" estado="{$post.ind_estado}"  mensaje="Estas seguro que desea anular la solicitud de compromiso" titulo="Estas Seguro?" descipcion="El usuario a eliminado un post" boton="si, Eliminar" idmenu="7">
                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                    </button>

                            {/if}

                                </td>



                        </tr>
                    {/foreach}
                    </tbody>
 </form>
                </table>
            </div><!--end .table-responsive -->
        </div><!--end .col -->
    </div><!--end .row -->
    <!-- fin listado -->
    <!-- filtro -->
    <div class="offcanvas">
        <div id="offcanvas-filtro" class="offcanvas-pane width-9">
            <div class="offcanvas-head">
                <header>Filtro</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                </div>
            </div>

            <div class="offcanvas-body">
                <form id="filtro"   class="form" role="form" method="post">
                    <input type="hidden" name="filtro" value="1">
                    <div class="row">

                        <div class="col-sm-12  ">
                            <div class="form-group ">
                                <select  class="form-control" id="filtro_dependencia">
                                    <option value="0"> Todas </option>
                                    {foreach item=tipo from=$_DependenciaPost}
                                        {if isset($_DependenciaSelect)}


                                            {if ($tipo.pk_num_dependencia==$_DependenciaSelect)}

                                                <option selected  value="{$tipo.pk_num_dependencia}"> {$tipo.ind_dependencia}</option>

                                            {else}
                                                <option value="{$tipo.pk_num_dependencia}"> {$tipo.ind_dependencia}</option>
                                            {/if}

                                        {else}
                                            <option value="{$tipo.pk_num_dependencia}"> {$tipo.ind_dependencia}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <label for="select1">Dependencias</label>
                            </div>
                        </div>
                        <div class="col-sm-12  ">
                            <div class="form-group ">
                                <select   class="form-control" id="filtro_proveedor">
                                    <option value="0"> Todos </option>
                                    {foreach item=tipo from=$_ProveedorPost}

                                        {if isset($_ProveedorSelect)}
                                            {if ($tipo.pk_num_proveedor==$_ProveedorSelect)}
                                                <option selected value="{$tipo.pk_num_proveedor}">{$tipo.ind_nombre1} </option>
                                            {else}
                                                <option   value="{$tipo.pk_num_proveedor}">{$tipo.ind_nombre1} </option>
                                            {/if}
                                        {else}
                                            <option  value="{$tipo.pk_num_proveedor}">{$tipo.ind_nombre1} </option>
                                        {/if}

                                    {/foreach}
                                </select>
                                <label for="select1"> Proveedor </label>
                            </div>
                        </div>


                    </div>

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button  type="button" onclick="Filtro()" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                                Filtrar
                            </button>
                        </div>
                    </div>




                </form>
            </div>
        </div>
    </div>
    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
            descripcion="Registro de Compromiso"  titulo="Registrar Compromiso" id="nuevo" >
        <i class="md md-create"></i> Registrar Compromiso&nbsp;&nbsp;
    </button>


    </div><!--end .row -->


        <script type="text/javascript">
        $(document).ready(function() {


            $('#nuevo').click(function () {
                var $url = '{$_Parametros.url}modGC/compromisoCONTROL/RegistrarCompromisoMET';
                $('#modalAncho').css("width", "75%");
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url, { id: 0}, function ($dato) {
                    $('#ContenidoModal').html($dato);
                });

            });

            $('#datatable1 tbody').on( 'click', '.ver', function () {
                var $url='{$_Parametros.url}modGC/proyectoContratoCONTROL/RedactarProyectoMET';
                $('#modalAncho').css( "width", "95%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idPost: $(this).attr('idVer'),accion:"Ver"},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });

            $('#datatable1 tbody').on( 'click', '.anular', function () {

                var idPost=$(this).attr('idCompromiso');
                var estado=$(this).attr('estado');
                var idContrato=$(this).attr('idContrato');
                swal({
                    title:$(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){

                    var $url='{$_Parametros.url}modGC/compromisoCONTROL/AnularCompromisoMET';
                    $(document.getElementById('idCompromiso'+idPost)).html('');

                    $.post($url,{ id: idPost, estado: estado, idContrato: idContrato   },function($dato){
                        swal("Anulado!", "el registro fue anulado satisfactoriamente.", "success");
                    });


                });
            });


        });
    </script>
    </div>