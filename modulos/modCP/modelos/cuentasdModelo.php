<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO:Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once 'cuentasModelo.php';
class cuentasdModelo extends Modelo
{
    private $atIdUsuario;
    public $atCuentasModelo;
    public $atMiscelaneoModelo;

    public function __construct()
    {
        parent::__construct();

        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atCuentasModelo = new cuentasModelo();
        $this->atMiscelaneoModelo = new miscelaneoModelo();

    }

    public function metCuentasdListar($idCuentad = false)
    {
        if($idCuentad){
            $where = "WHERE cp_b007_cuenta_bancaria_defecto.pk_num_cuenta_bancaria_defecto = '$idCuentad'";
        }else{
            $where = "";
        }
        $transaccionListar = $this->_db->query(
            "SELECT
              cp_b007_cuenta_bancaria_defecto.*,
	          cp_b014_cuenta_bancaria.ind_num_cuenta,
              a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
	          cp_b007_cuenta_bancaria_defecto
	        INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_b007_cuenta_bancaria_defecto.fk_cpb014_num_cuenta_bancaria
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b007_cuenta_bancaria_defecto.fk_a006_num_miscelaneo_detalle_tipo_pago
            $where
            ORDER BY pk_num_cuenta_bancaria_defecto ASC"
        );
        //var_dump($transaccionListar);
        $transaccionListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idCuentad){
            return $transaccionListar->fetch();
        }else{
            return $transaccionListar->fetchAll();
        }


    }
    public function metCuentasdConsultaDetalle($idCuentad)
    {
        $consultaCuentad = $this->_db->query(
            "SELECT
              cp_b007_cuenta_bancaria_defecto.*,
	          cp_b014_cuenta_bancaria.*,
              a006_miscelaneo_detalle.*
            FROM
	          cp_b007_cuenta_bancaria_defecto
	        INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_b007_cuenta_bancaria_defecto.fk_cpb014_num_cuenta_bancaria
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b007_cuenta_bancaria_defecto.fk_a006_num_miscelaneo_detalle_tipo_pago

            WHERE
             pk_num_cuenta_bancaria_defecto = $idCuentad"
        );
        $consultaCuentad->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaCuentad->fetch();
    }

    public function metNuevaCuentad($cuenta,$tipoPago)
    {
        $this->_db->beginTransaction();
        $nuevaTransaccion = $this->_db->prepare(
            "INSERT INTO
             cp_b007_cuenta_bancaria_defecto
              SET
               fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
               fk_a006_num_miscelaneo_detalle_tipo_pago=:fk_a006_num_miscelaneo_detalle_tipo_pago,
               int_numero_ultimo=:int_numero_ultimo,
               fec_ultima_modificacion=NOW(),
               fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             ");
        $nuevaTransaccion->execute(array(
            'fk_cpb014_num_cuenta_bancaria' => $cuenta,
            'fk_a006_num_miscelaneo_detalle_tipo_pago' => $tipoPago,
            'int_numero_ultimo' => '1'
        ));

        $fallaTansaccion = $nuevaTransaccion->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarCuenta($idCuentad,$cuenta,$tipoPago)
    {
        $this->_db->beginTransaction();
        $nuevaTransaccion = $this->_db->prepare(
            "UPDATE
             cp_b007_cuenta_bancaria_defecto
              SET
               fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
               fk_a006_num_miscelaneo_detalle_tipo_pago=:fk_a006_num_miscelaneo_detalle_tipo_pago,
               int_numero_ultimo=:int_numero_ultimo,
               fec_ultima_modificacion=NOW(),
               fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE
              pk_num_cuenta_bancaria_defecto = $idCuentad
             ");
        $nuevaTransaccion->execute(array(
            'fk_cpb014_num_cuenta_bancaria' => $cuenta,
            'fk_a006_num_miscelaneo_detalle_tipo_pago' => $tipoPago,
            'int_numero_ultimo' => '1'
        ));

        $fallaTansaccion = $nuevaTransaccion->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metEliminarCuentad($idCuentad){

        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
            DELETE FROM
            cp_b007_cuenta_bancaria_defecto
             WHERE
            pk_num_cuenta_bancaria_defecto=:pk_num_cuenta_bancaria_defecto
              ");
        $elimar->execute(array(
            'pk_num_cuenta_bancaria_defecto'=>$idCuentad
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCuentad;
        }
    }

}