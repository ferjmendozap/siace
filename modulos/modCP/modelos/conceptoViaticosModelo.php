<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modNM' . DS . 'modelos' . DS . 'maestros' . DS . 'tipoNominaModelo.php';

class conceptoViaticosModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atTipoNominaModelo;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
        $this->atTipoNominaModelo=new tipoNominaModelo();
    }

    public function metListarConceptoViatico($idConceptoViatico = false)
    {
        if($idConceptoViatico){
            $where = "WHERE cp_b009_concepto_gasto_viatico.pk_num_concepto_gasto_viatico = '$idConceptoViatico'";
        }else{
            $where = "";
        }
        $Listar = $this->_db->query(
            "SELECT
              cp_b009_concepto_gasto_viatico.*,
              categoria.ind_nombre_detalle AS categoria,
              articulo.ind_nombre_detalle AS articulo
             FROM
             cp_b009_concepto_gasto_viatico
             INNER JOIN a006_miscelaneo_detalle AS categoria ON categoria.pk_num_miscelaneo_detalle = cp_b009_concepto_gasto_viatico.fk_a006_num_miscelaneo_detalle_categoria
             INNER JOIN a006_miscelaneo_detalle AS articulo ON articulo.pk_num_miscelaneo_detalle = cp_b009_concepto_gasto_viatico.fk_a006_num_miscelaneo_detalle_articulo
             $where
             ");
        $Listar->setFetchMode(PDO::FETCH_ASSOC);
        if($idConceptoViatico) {
            return $Listar->fetch();
        }else{
            return $Listar->fetchAll();
        }

    }

    public function metConsultarConceptoViatico($idConceptoViatico)
    {
        $Listar = $this->_db->query("
            SELECT
              cp_b009_concepto_gasto_viatico.*,
              a018_seguridad_usuario.ind_usuario,
              categoria.ind_nombre_detalle AS categoria,
              articulo.ind_nombre_detalle AS articulo,
              cuenta.cod_cuenta AS cuenta,
              cuentaPub20.cod_cuenta AS cuentaPub20,
              pr_b002_partida_presupuestaria.cod_partida

             FROM
             cp_b009_concepto_gasto_viatico
             INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_b009_concepto_gasto_viatico.fk_a018_num_seguridad_usuario
             INNER JOIN a006_miscelaneo_detalle AS categoria ON categoria.pk_num_miscelaneo_detalle = cp_b009_concepto_gasto_viatico.fk_a006_num_miscelaneo_detalle_categoria
             INNER JOIN a006_miscelaneo_detalle AS articulo ON articulo.pk_num_miscelaneo_detalle = cp_b009_concepto_gasto_viatico.fk_a006_num_miscelaneo_detalle_articulo
             INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_b009_concepto_gasto_viatico.fk_prb002_num_partida_presupuestaria
             INNER JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = cp_b009_concepto_gasto_viatico.fk_cbb004_num_plan_cuenta
             INNER JOIN cb_b004_plan_cuenta AS cuentaPub20 ON cuentaPub20.pk_num_cuenta = cp_b009_concepto_gasto_viatico.fk_cbb004_num_plan_cuenta_pub20
             WHERE
             pk_num_concepto_gasto_viatico='$idConceptoViatico'

             ");
        $Listar->setFetchMode(PDO::FETCH_ASSOC);
        return $Listar->fetch();
    }

    public function metNuevoConceptoViatico($form)
    {
        $this->_db->beginTransaction();
        $registrar = $this->_db->prepare("
            INSERT INTO
              cp_b009_concepto_gasto_viatico
            SET
              cod_concepto=:cod_concepto,
              ind_descripcion=:ind_descripcion,
              fk_b008_num_unidad_viatico=:fk_b008_num_unidad_viatico,
              fk_a006_num_miscelaneo_detalle_categoria=:fk_a006_num_miscelaneo_detalle_categoria,
              fk_a006_num_miscelaneo_detalle_articulo=:fk_a006_num_miscelaneo_detalle_articulo,
              fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
              fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
              fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
              num_numeral=:num_numeral,
              num_valor_ut=:num_valor_ut,
              num_flag_monto=:num_flag_monto,
              num_flag_cantidad=:num_flag_cantidad,
              num_flag_unidad_combustible=:num_flag_unidad_combustible,
              num_monto=:num_monto,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
             fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             ");
        $registrar->execute(array(
            'cod_concepto' => $form['cod_concepto'],
            'ind_descripcion' => $form['ind_descripcion'],
            'fk_b008_num_unidad_viatico' => $form['fk_b008_num_unidad_viatico'],
            'fk_a006_num_miscelaneo_detalle_categoria' => $form['fk_a006_num_miscelaneo_detalle_categoria'],
            'fk_a006_num_miscelaneo_detalle_articulo' => $form['fk_a006_num_miscelaneo_detalle_articulo'],
            'fk_cbb004_num_plan_cuenta' => $form['fk_cbb004_num_plan_cuenta'],
            'fk_cbb004_num_plan_cuenta_pub20' => $form['fk_cbb004_num_plan_cuenta_pub20'],
            'fk_prb002_num_partida_presupuestaria' => $form['fk_prb002_num_partida_presupuestaria'],
            'num_numeral' => $form['num_numeral'],
            'num_valor_ut' => $form['num_valor_ut'],
            'num_flag_monto' => $form['num_flag_monto'],
            'num_flag_cantidad' => $form['num_flag_cantidad'],
            'num_flag_unidad_combustible' => $form['num_flag_unidad_combustible'],
            'num_monto' => $form['num_monto'],
            'num_estatus' => $form['num_estatus']
        ));

        $fallaTansaccion = $registrar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarConceptoViatico($idConceptoViatico,$form)
    {
        $this->_db->beginTransaction();
        $modificar = $this->_db->prepare("
            UPDATE
              cp_b009_concepto_gasto_viatico
            SET
              ind_descripcion=:ind_descripcion,
              fk_b008_num_unidad_viatico=:fk_b008_num_unidad_viatico,
              fk_a006_num_miscelaneo_detalle_categoria=:fk_a006_num_miscelaneo_detalle_categoria,
              fk_a006_num_miscelaneo_detalle_articulo=:fk_a006_num_miscelaneo_detalle_articulo,
              fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
              fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
              fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
              num_numeral=:num_numeral,
              num_valor_ut=:num_valor_ut,
              num_flag_monto=:num_flag_monto,
              num_flag_cantidad=:num_flag_cantidad,
              num_flag_unidad_combustible=:num_flag_unidad_combustible,
              num_monto=:num_monto,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
             fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             WHERE
             pk_num_concepto_gasto_viatico='$idConceptoViatico'
             ");
        $modificar->execute(array(
            'ind_descripcion' => $form['ind_descripcion'],
            'fk_b008_num_unidad_viatico' => $form['fk_b008_num_unidad_viatico'],
            'fk_a006_num_miscelaneo_detalle_categoria' => $form['fk_a006_num_miscelaneo_detalle_categoria'],
            'fk_a006_num_miscelaneo_detalle_articulo' => $form['fk_a006_num_miscelaneo_detalle_articulo'],
            'fk_cbb004_num_plan_cuenta' => $form['fk_cbb004_num_plan_cuenta'],
            'fk_cbb004_num_plan_cuenta_pub20' => $form['fk_cbb004_num_plan_cuenta_pub20'],
            'fk_prb002_num_partida_presupuestaria' => $form['fk_prb002_num_partida_presupuestaria'],
            'num_numeral' => $form['num_numeral'],
            'num_valor_ut' => $form['num_valor_ut'],
            'num_flag_monto' => $form['num_flag_monto'],
            'num_flag_cantidad' => $form['num_flag_cantidad'],
            'num_flag_unidad_combustible' => $form['num_flag_unidad_combustible'],
            'num_monto' => $form['num_monto'],
            'num_estatus' => $form['num_estatus']
        ));

        $fallaTansaccion = $modificar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metEliminarConceptoViatico($idConceptoViatico)
    {
        $this->_db->beginTransaction();
        $elimar = $this->_db->prepare("
                DELETE FROM
                  cp_b009_concepto_gasto_viatico
                WHERE
                  pk_num_concepto_gasto_viatico=:pk_num_concepto_gasto_viatico
            ");
        $elimar->execute(array(
            'pk_num_concepto_gasto_viatico' => $idConceptoViatico
        ));
        $error = $elimar->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idConceptoViatico;
        }
    }
}
