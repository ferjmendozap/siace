<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class unidadCombustibleModelo extends Modelo
{
    private $atIdUsuario;
    public $atIdEmpleado;


    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metUnidadCombustibleListar($estado = false, $idUnidadCombustible = false)
    {
        $where = "";
        if ($estado) {
            $where .= "AND ind_estado='$estado'";
        }
        if($idUnidadCombustible){
            $where .= "AND cp_b021_unidad_combustible.pk_num_unidad_combustible = '$idUnidadCombustible'";
        }
        $Listar = $this->_db->query(
            "SELECT * FROM  cp_b021_unidad_combustible
            WHERE 1
            $where
            ORDER BY pk_num_unidad_combustible DESC LIMIT 1
        ");
        $Listar->setFetchMode(PDO::FETCH_ASSOC);
        if($idUnidadCombustible){
            return $Listar->fetch();
        }
        else if ($estado) {
            return $Listar->fetch();
        }
        else{
            return $Listar->fetchAll();
        }
    }

    public function metBuscarUnidadCombustible()
    {
        $listar = $this->_db->query(
            "SELECT
                num_unidad_combustible AS ind_valor
             FROM
                cp_b021_unidad_combustible
             WHERE
             (SELECT MAX(num_unidad_combustible) FROM cp_b021_unidad_combustible)
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metConsultaUnidadCombustibleModelo($idUnidadCombustible)
    {
        $consultar = $this->_db->query("
            SELECT
               cp_b021_unidad_combustible.*,
               a018_seguridad_usuario.ind_usuario,
               (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_b021_unidad_combustible ON rh_b001_empleado.pk_num_empleado = cp_b021_unidad_combustible.fk_rhb001_num_empleado_preparado
                    WHERE
                        pk_num_unidad_combustible = '$idUnidadCombustible'
                ) AS EMPLEADO_PREPARA,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_b021_unidad_combustible ON rh_b001_empleado.pk_num_empleado = cp_b021_unidad_combustible.fk_rhb001_num_empleado_aprobado
                    WHERE
                        pk_num_unidad_combustible = '$idUnidadCombustible'
                ) AS EMPLEADO_APRUEBA
             FROM
               cp_b021_unidad_combustible
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_b021_unidad_combustible.fk_a018_num_seguridad_usuario
            WHERE
            pk_num_unidad_combustible='$idUnidadCombustible'
             ");
        $consultar->setFetchMode(PDO::FETCH_ASSOC);
        return $consultar->fetch();
    }

    public function metNuevaUnidadCombustible($resolucion,$periodo,$numCombustible,$descripcion)
    {
        $this->_db->beginTransaction();
        $registrar = $this->_db->prepare("
            INSERT INTO
              cp_b021_unidad_combustible
            SET
              ind_num_resolucion=:ind_num_resolucion,
              ind_periodo=:ind_periodo,
              num_unidad_combustible=:num_unidad_combustible,
              ind_estado='PR',
              ind_descripcion=:ind_descripcion,
              fec_preparado=NOW(),
              fk_rhb001_num_empleado_preparado='$this->atIdEmpleado',
              fec_ultima_modificacion=NOW(),
             fk_a018_num_seguridad_usuario='$this->atIdUsuario'
             ");
        $registrar->execute(array(
            'ind_num_resolucion' => $resolucion,
            'ind_periodo' => $periodo,
            'num_unidad_combustible' => $numCombustible,
            'ind_descripcion' => $descripcion
        ));

        $fallaTansaccion = $registrar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarUnidadCombustible($idUnidadCombustible,$resolucion,$periodo,$numCombustible,$descripcion)
    {
        $this->_db->beginTransaction();
        $registrar = $this->_db->prepare("
            UPDATE
              cp_b021_unidad_combustible
            SET
              ind_num_resolucion=:ind_num_resolucion,
              ind_periodo=:ind_periodo,
              num_unidad_combustible=:num_unidad_combustible,
              ind_estado='PR',
              ind_descripcion=:ind_descripcion,
              fec_preparado=NOW(),
              fk_rhb001_num_empleado_preparado='$this->atIdEmpleado',
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE
              pk_num_unidad_combustible='$idUnidadCombustible'
             ");
        $registrar->execute(array(
            'ind_num_resolucion' => $resolucion,
            'ind_periodo' => $periodo,
            'num_unidad_combustible' => $numCombustible,
            'ind_descripcion' => $descripcion
        ));

        $fallaTansaccion = $registrar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metAprobatUnidadCombustible($idUnidadCombustible)
    {
        $this->_db->beginTransaction();
        $registrar = $this->_db->prepare("
            UPDATE
              cp_b021_unidad_combustible
            SET
              ind_estado='AP',
              fec_aprobado=NOW(),
              fk_rhb001_num_empleado_aprobado='$this->atIdEmpleado'
            WHERE
              pk_num_unidad_combustible='$idUnidadCombustible'
             ");
        $registrar->execute(array(
            'pk_num_unidad_combustible' => $idUnidadCombustible
        ));

        $fallaTansaccion = $registrar->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $this->_db->commit();
            return $idUnidadCombustible;
        }
    }
}