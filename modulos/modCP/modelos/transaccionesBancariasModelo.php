<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once 'cuentasModelo.php';
//require_once 'transaccionModelo.php';
require_once 'documentoModelo.php';
require_once 'documentoTransaccionModelo.php';
require_once 'obligacionModelo.php';
require_once 'trait' . DS . 'updateTrait.php';
require_once 'trait' .DS. 'consultasTrait.php';
require_once RUTA_Modulo . 'modCB' . DS . 'modelos' . DS . 'LibroContableModelo.php';
require_once RUTA_Modulo . 'modCB' . DS . 'modelos' . DS . 'ListaVoucherModelo.php';

class transaccionesBancariasModelo extends Modelo
{
    use consultasTrait;
    use updateTrait;
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atCuentasModelo;
//    public $atTransaccionModelo;
    public $atDocumentosModelo;
    public $atDocumentosTransaccionModelo;
    public $atObligacionModelo;
    public $atLibroContableModelo;
    public $atListaVoucherModelo;


    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atCuentasModelo = new cuentasModelo();
//        $this->atTransaccionModelo = new transaccionModelo();
        $this->atDocumentosModelo = new documentoModelo();
        $this->atDocumentosTransaccionModelo = new documentoTransaccionModelo();
        $this->atObligacionModelo = new obligacionModelo();
        $this->atLibroContableModelo = new LibroContableModelo();
        $this->atListaVoucherModelo = new ListaVoucherModelo();
    }
    public function metEmpleado()
    {
        $listar = $this->_db->query(
            "SELECT
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreEmpleado
            FROM
            rh_b001_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona

            WHERE
            rh_b001_empleado.pk_num_empleado='$this->atIdEmpleado'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metListaTransacciones()
    {
        $listar = $this->_db->query(
            "SELECT
            cp_b006_banco_tipo_transaccion.*,
            cb_b004_plan_cuenta.cod_cuenta,
            a006_miscelaneo_detalle.cod_detalle
            FROM
            cp_b006_banco_tipo_transaccion
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion

            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metTransacciones($estado=false, $idTransaccion = false)
    {
        if($estado){
            if($estado == 'AP'){
                $where="WHERE (cp_d011_banco_transaccion.ind_estado='$estado' OR cp_d011_banco_transaccion.ind_estado='CO') AND cp_d011_banco_transaccion.num_flag_genera_voucher ='1'";
            }else{
                $where="WHERE cp_d011_banco_transaccion.ind_estado='$estado'";
            }
        }elseif($idTransaccion){
            $where = "WHERE cp_d011_banco_transaccion.pk_num_banco_transaccion = '$idTransaccion'";
        }else{
            $where = "";
        }
        $listar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
              cp_b006_banco_tipo_transaccion.ind_descripcion AS decripcionTransaccion,
              cp_b014_cuenta_bancaria.ind_num_cuenta AS cuentaBancaria,
              cp_d010_pago.fk_cbb001_num_voucher_pago,
              cp_d010_pago.ind_num_pago,
               (CASE
                            WHEN cp_d011_banco_transaccion.ind_estado = 'PR' THEN 'PENDIENTE'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'RE' THEN 'REVISADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'AP' THEN 'ACTUALIZADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'CO' THEN 'CONTABILIZADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'AN' THEN 'ANULADO'
              END) AS ind_estado
            FROM
              cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            LEFT JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
            LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            LEFT JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta or cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            $where
            ORDER BY cp_d011_banco_transaccion.ind_num_transaccion ASC
            ");
       //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        if($idTransaccion){
            return $listar->fetch();
        }else{
            return $listar->fetchAll();
        }
    }

    public function metSaldoBanco($idCuenta = false)
    {
        if($idCuenta){
            $where = "AND cp_b014_cuenta_bancaria.pk_num_cuenta=$idCuenta";
            $groupBy = '';
        }else{
            $where = '';
            $groupBy = "GROUP BY cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria, cp_b014_cuenta_bancaria.ind_num_cuenta";
        }
        $listar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta AS cuentaBancaria,
              cp_b014_cuenta_bancaria.ind_descripcion AS ind_descripcion,
              a006_miscelaneo_detalle.ind_nombre_detalle AS banco,
              SUM(cp_d011_banco_transaccion.num_monto) AS Monto,
              cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
              cp_b006_banco_tipo_transaccion.ind_descripcion AS decripcionTransaccion,
              tipoTransaccion.cod_detalle AS tipoTransaccion
            FROM
              cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN a006_miscelaneo_detalle AS tipoTransaccion ON tipoTransaccion.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
            LEFT JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
            LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            LEFT JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta or cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco
            LEFT JOIN cp_d006_cuenta_bancaria_balance ON cp_d006_cuenta_bancaria_balance.pk_num_cuenta_balance = cp_b014_cuenta_bancaria.pk_num_cuenta

            WHERE
            cp_d011_banco_transaccion.ind_estado = 'AP' or cp_d011_banco_transaccion.ind_estado = 'CO'
            $where
            $groupBy
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metSaldoBancoDetalle($idCuenta)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta AS cuentaBancaria,
              cp_b014_cuenta_bancaria.ind_descripcion AS ind_descripcion,
              a006_miscelaneo_detalle.ind_nombre_detalle AS banco,
              SUM(cp_d011_banco_transaccion.num_monto) AS Monto

            FROM
              cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            LEFT JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
            LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            LEFT JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta or cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco
            LEFT JOIN cp_d006_cuenta_bancaria_balance ON cp_d006_cuenta_bancaria_balance.pk_num_cuenta_balance = cp_b014_cuenta_bancaria.pk_num_cuenta

            WHERE
            cp_d011_banco_transaccion.ind_estado!='PE' and cp_b014_cuenta_bancaria.pk_num_cuenta=$idCuenta
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metConsultarTransacciones($idTransaccion)
    {
        $consultar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
              cp_b006_banco_tipo_transaccion.ind_descripcion AS decripcionTransaccion,
              cp_b014_cuenta_bancaria.ind_num_cuenta AS cuentaBancaria,
              cp_d010_pago.fk_cbb001_num_voucher_pago,
              cp_d010_pago.ind_num_pago,
              cp_b002_tipo_documento.num_flag_provision,
              cp_b002_tipo_documento.num_flag_obligacion,
              (CASE
                            WHEN cp_d011_banco_transaccion.ind_estado = 'PR' THEN 'PENDIENTE'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'RE' THEN 'REVISADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'AP' THEN 'ACTUALIZADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'CO' THEN 'CONTABILIZADO'
              END) AS ind_estado_comp,
              (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d011_banco_transaccion ON cp_d011_banco_transaccion.fk_rhb001_num_empleado_crea = rh_b001_empleado.pk_num_empleado
                        WHERE
                        pk_num_banco_transaccion = '$idTransaccion'
                ) AS EMPLEADO_PREPARA,
            a018_seguridad_usuario.ind_usuario
            FROM
              cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            LEFT JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
            LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            LEFT JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta
            LEFT JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_d011_banco_transaccion.fk_a018_num_seguridad_usuario
            INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d011_banco_transaccion.fk_cpb002_num_tipo_documento

            WHERE
            cp_d011_banco_transaccion.pk_num_banco_transaccion = '$idTransaccion'
            ");
        //var_dump($listar);
        $consultar->setFetchMode(PDO::FETCH_ASSOC);
        return $consultar->fetch();
    }

    public function metDispFinanciera($idTransaccion)
    {
        $listar = $this->_db->query(
            "SELECT
                cp_d011_banco_transaccion.*,
             SUM(cp_d011_banco_transaccion.num_monto) AS montoTotal,
                (cp_d006_cuenta_bancaria_balance.num_saldo_actual) AS montoActual
             FROM
                cp_d011_banco_transaccion
             INNER JOIN cp_d006_cuenta_bancaria_balance ON cp_d006_cuenta_bancaria_balance.fk_cpb014_pk_num_cuenta_bancaria = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
             WHERE
                cp_d011_banco_transaccion.pk_num_banco_transaccion = '$idTransaccion'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metConsultarTransaccionesDetalle($idTransaccion)
    {
        $consultar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
              cp_b006_banco_tipo_transaccion.ind_descripcion AS decripcionTransaccion,
              a006_miscelaneo_detalle.cod_detalle,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta AS cuentaBancaria,
              cp_d010_pago.fk_cbb001_num_voucher_pago,
              cp_d010_pago.ind_num_pago,
              pr_b002_partida_presupuestaria.cod_partida,
              cb_b001_voucher.ind_voucher,
              (CASE
                            WHEN cp_d011_banco_transaccion.ind_estado = 'PR' THEN 'PENDIENTE'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'AP' THEN 'ACTUALIZADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'CO' THEN 'CONTABILIZADO'
              END) AS ind_estado,
            a018_seguridad_usuario.ind_usuario,
            (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d011_banco_transaccion ON rh_b001_empleado.pk_num_empleado = cp_d011_banco_transaccion.fk_rhb001_num_empleado_crea
                    WHERE
                        pk_num_banco_transaccion = '$idTransaccion'
                ) AS EMPLEADO_PREPARA
            FROM
              cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
            LEFT JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
            LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            LEFT JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta or cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_d011_banco_transaccion.fk_a018_num_seguridad_usuario
            LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_d011_banco_transaccion.fk_prb002_num_partida_presupuestaria
            LEFT JOIN cb_b001_voucher ON cb_b001_voucher.pk_num_voucher_mast = cp_d010_pago.fk_cbb001_num_voucher_pago
            WHERE
            cp_d011_banco_transaccion.pk_num_banco_transaccion = '$idTransaccion'
            ");
        //var_dump($consultar);
        $consultar->setFetchMode(PDO::FETCH_ASSOC);
        return $consultar->fetchAll();
    }

    public function metConsultarTransaccionesImprimir($idTransaccion)
    {
        $consultar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
              cp_b006_banco_tipo_transaccion.ind_descripcion AS decripcionTransaccion,
              a006_miscelaneo_detalle.cod_detalle,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta AS cuentaBancaria,
              cp_d010_pago.fk_cbb001_num_voucher_pago,
              cp_d010_pago.ind_num_pago,
              (CASE
                            WHEN cp_d011_banco_transaccion.ind_estado = 'PR' THEN 'PENDIENTE'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'AP' THEN 'ACTUALIZADO'
                            WHEN cp_d011_banco_transaccion.ind_estado = 'CO' THEN 'CONTABILIZADO'
              END) AS ind_estado,
            a018_seguridad_usuario.ind_usuario,
            (
                    SELECT
                        CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) 
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d011_banco_transaccion ON rh_b001_empleado.pk_num_empleado = cp_d011_banco_transaccion.fk_rhb001_num_empleado_crea
                    WHERE
                        pk_num_banco_transaccion = '$idTransaccion'
                ) AS EMPLEADO_PREPARA,
                (
                  SELECT
                    rh_c063_puestos.ind_descripcion_cargo
                  FROM
                    rh_b001_empleado
                  INNER JOIN
                    a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                  INNER JOIN
                    cp_d011_banco_transaccion ON rh_b001_empleado.pk_num_empleado = cp_d011_banco_transaccion.fk_rhb001_num_empleado_crea
                   INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado 
                    INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
                  WHERE
                    pk_num_banco_transaccion = '$idTransaccion'
                ) AS CARGO_PREPARA,
                (
                    SELECT
                        CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) 
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d011_banco_transaccion ON rh_b001_empleado.pk_num_empleado = cp_d011_banco_transaccion.fk_rhb001_num_empleado_revisa
                    WHERE
                        pk_num_banco_transaccion = '$idTransaccion'
                ) AS EMPLEADO_REVISA,
                (
                  SELECT
                    rh_c063_puestos.ind_descripcion_cargo
                  FROM
                    rh_b001_empleado
                  INNER JOIN
                    a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                  INNER JOIN cp_d011_banco_transaccion ON rh_b001_empleado.pk_num_empleado = cp_d011_banco_transaccion.fk_rhb001_num_empleado_revisa
                  INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado 
                  INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
                  WHERE
                    pk_num_banco_transaccion = '$idTransaccion'
                ) AS CARGO_REVISA,
                (
                     SELECT
                      CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1)
                        FROM
                      cb_b001_voucher
                    INNER JOIN  a003_persona ON a003_persona.pk_num_persona = cb_b001_voucher.fk_a003_num_preparado_por
                    INNER JOIN rh_b001_empleado ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                    INNER JOIN cp_d011_banco_transaccion ON cb_b001_voucher.pk_num_voucher_mast = cp_d011_banco_transaccion.fk_cbb001_num_voucher
                      WHERE
                      pk_num_banco_transaccion = '$idTransaccion'
                ) AS EMPLEADO_CONFORMA,
                (
                     SELECT
                      rh_c063_puestos.ind_descripcion_cargo
                        FROM
                      cb_b001_voucher
                    INNER JOIN  a003_persona ON a003_persona.pk_num_persona = cb_b001_voucher.fk_a003_num_preparado_por
                    INNER JOIN rh_b001_empleado ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
                    INNER JOIN cp_d011_banco_transaccion ON cb_b001_voucher.pk_num_voucher_mast = cp_d011_banco_transaccion.fk_cbb001_num_voucher
                    INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado 
                    INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
                      WHERE
                      pk_num_banco_transaccion = '$idTransaccion'
                ) AS CARGO_CONFORMA,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d011_banco_transaccion ON rh_b001_empleado.pk_num_empleado = cp_d011_banco_transaccion.fk_rhb001_num_empleado_aprueba
                    WHERE
                        pk_num_banco_transaccion = '$idTransaccion'
                ) AS EMPLEADO_APRUEBA,
                (
                  SELECT
                    rh_c063_puestos.ind_descripcion_cargo
                  FROM
                    rh_b001_empleado
                  INNER JOIN
                    a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                  INNER JOIN cp_d011_banco_transaccion ON rh_b001_empleado.pk_num_empleado = cp_d011_banco_transaccion.fk_rhb001_num_empleado_aprueba
                  INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado 
                  INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
                  WHERE
                    pk_num_banco_transaccion = '$idTransaccion'
                ) AS CARGO_APRUEBA
                
            FROM
              cp_d011_banco_transaccion
            INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.fk_cpb006_num_banco_tipo_transaccion
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
            LEFT JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d011_banco_transaccion.fk_cpd010_num_pago
            LEFT JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
            LEFT JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta or cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d011_banco_transaccion.fk_cpb014_num_cuenta_bancaria
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_d011_banco_transaccion.fk_a018_num_seguridad_usuario
            WHERE
            cp_d011_banco_transaccion.pk_num_banco_transaccion = '$idTransaccion'
            ");
        //var_dump($consultar);
        $consultar->setFetchMode(PDO::FETCH_ASSOC);
        return $consultar->fetch();
    }

    public function metNuevaTransaccionBancaria($form)
    {
        $this->_db->beginTransaction();
        $secuencia=$this->metSecuencia('cp_d011_banco_transaccion','ind_num_transaccion');

        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
             cp_d011_banco_transaccion
              SET
               fk_cpb006_num_banco_tipo_transaccion=:fk_cpb006_num_banco_tipo_transaccion,
               fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
               fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
               ind_num_transaccion= '" . $secuencia['secuencia'] . "',
               ind_periodo_contable=:ind_periodo_contable,
               ind_estado=:ind_estado,
               fec_anio=YEAR(NOW()),
               fec_transaccion=:fec_transaccion,
               ind_num_documento_referencia=:ind_num_documento_referencia,
               num_monto=:num_monto,
               num_secuencia=:num_secuencia,
               num_flag_genera_voucher=:num_flag_genera_voucher,
               num_flag_presupuesto=:num_flag_presupuesto,
               num_flag_automatico=:num_flag_automatico,
               txt_comentarios=:txt_comentarios,
               ind_mes=LPAD( MONTH (NOW()),2,'0'),
               fec_preparacion=NOW(),
               fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
               fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
               fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
               fk_rhb001_num_empleado_crea='$this->atIdEmpleado',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
               ");
        $detalle=$form['codDetalle'];
        $detalleA=$form['codDetalleAlphaNum'];
        $secuencia=1;
        foreach($detalle['num_secuencia'] AS $i) {
            $ids=$detalle[$i];
            $idsA=$detalleA[$i];
            if($ids['fk_prb002_num_partida_presupuestaria'] != null){
                $partida = $ids['fk_prb002_num_partida_presupuestaria'];
            }else{
                $partida = null;
            }

            $tipoTransaccion=$this->metTipoTransaccion($ids['fk_cpb006_num_banco_tipo_transaccion']);
            if($tipoTransaccion['cod_detalle'] == 'E'){
                $monto = $ids['num_monto'] * (-1);
            }else{
                $monto = $ids['num_monto'];
            }
            $NuevoRegistro->execute(array(
                'fk_cpb006_num_banco_tipo_transaccion' => $ids['fk_cpb006_num_banco_tipo_transaccion'],
                'fk_cpb002_num_tipo_documento' => $ids['fk_cpb002_num_tipo_documento'],
                'fk_a023_num_centro_costo' => $ids['fk_a023_num_centro_costo'],
                'fk_cpb014_num_cuenta_bancaria' => $ids['fk_cpb014_num_cuenta'],
                'ind_periodo_contable' => $form['ind_periodo_contable'],
                'ind_estado' => 'PR',
                'fec_transaccion' => $form['fec_transaccion'],
                'ind_num_documento_referencia' => $idsA['ind_num_documento_referencia'],
                'num_monto' => $monto,
                'num_secuencia'=>$secuencia,
                'txt_comentarios' => $form['txt_comentarios'],
                'num_flag_genera_voucher' => 1,
                'num_flag_presupuesto' => $form['num_flag_presupuesto'],
                'num_flag_automatico' => 0,
                'fk_a003_num_persona_proveedor' => $ids['fk_a003_num_persona_proveedor'],
                'fk_prb002_num_partida_presupuestaria' => $partida
            ));
            $secuencia++;
        }
        $idTransaccion = $this->_db->lastInsertId();
        $fallaTansaccion = $NuevoRegistro->errorInfo();


        if(
            (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) ||
            (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2]))
        ){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idTransaccion;
        }
    }

    public function metModificarTransaccionBancaria($idTransaccion,$form)
    {
        $this->_db->beginTransaction();
            $NuevoRegistro=$this->_db->prepare("
            UPDATE
             cp_d011_banco_transaccion
              SET
               fk_cpb006_num_banco_tipo_transaccion=:fk_cpb006_num_banco_tipo_transaccion,
               fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
               fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
               ind_periodo_contable=:ind_periodo_contable,
               ind_estado=:ind_estado,
               fec_anio=YEAR(NOW()),
               fec_transaccion=:fec_transaccion,
               num_monto=:num_monto,
               num_secuencia=:num_secuencia,
               num_flag_presupuesto=:num_flag_presupuesto,
               num_flag_automatico=:num_flag_automatico,
               txt_comentarios=:txt_comentarios,
               ind_mes=LPAD( MONTH (NOW()),2,'0'),
               fec_preparacion=NOW(),
               fk_cpb014_num_cuenta_bancaria=:fk_cpb014_num_cuenta_bancaria,
               fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor,
               fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
               fk_rhb001_num_empleado_crea='$this->atIdEmpleado',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
              WHERE
              pk_num_banco_transaccion='$idTransaccion'
             ");
            $detalle=$form['codDetalle'];
            $secuencia=1;
            foreach($detalle['num_secuencia'] AS $i) {
                $ids=$detalle[$i];
                if($ids['fk_prb002_num_partida_presupuestaria'] != null){
                    $partida = $ids['fk_prb002_num_partida_presupuestaria'];
                }else{
                    $partida = null;
                }
                $tipoTransaccion=$this->metTipoTransaccion($ids['fk_cpb006_num_banco_tipo_transaccion']);
                if($tipoTransaccion['cod_detalle'] == 'E'){
                    $monto = $ids['num_monto'] * (-1);
                }else{
                    $monto = $ids['num_monto'];
                }
            $NuevoRegistro->execute(array(
                'fk_cpb006_num_banco_tipo_transaccion' => $ids['fk_cpb006_num_banco_tipo_transaccion'],
                'fk_cpb002_num_tipo_documento' => $ids['fk_cpb002_num_tipo_documento'],
                'fk_a023_num_centro_costo' => $ids['fk_a023_num_centro_costo'],
                'fk_cpb014_num_cuenta_bancaria' => $ids['fk_cpb014_num_cuenta'],
                'ind_periodo_contable' => $form['ind_periodo_contable'],
                'ind_estado' => $form['ind_estado'],
                'fec_transaccion' => $form['fec_transaccion'],
                'num_monto' => $monto,
                'num_secuencia'=>$secuencia,
                'txt_comentarios' => $form['txt_comentarios'],
                'num_flag_presupuesto' => $form['num_flag_presupuesto'],
                'num_flag_automatico' => 0,
                'fk_a003_num_persona_proveedor' => $ids['fk_a003_num_persona_proveedor'],
                'fk_prb002_num_partida_presupuestaria' => $partida
            ));
            $secuencia++;
        }
        $idTransaccion = $this->_db->lastInsertId();

        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(
            (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2]))
        ){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idTransaccion;
        }
    }

    public function metRevisarTransaccionBancaria($idTransaccion)
    {
        $this->_db->beginTransaction();
        $revisar=$this->_db->prepare("
            UPDATE
             cp_d011_banco_transaccion
              SET
              ind_estado='RE',
              fk_rhb001_num_empleado_revisa='$this->atIdEmpleado',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_revisado=NOW(),
              fec_ultima_modificacion=NOW()
              WHERE
              pk_num_banco_transaccion=:pk_num_banco_transaccion
             ");
        $revisar->execute(array(

                'pk_num_banco_transaccion' => $idTransaccion
            ));
        $error = $revisar->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idTransaccion;
        }
    }

    public function metAprobarTransaccionBancaria($idTransaccion)
    {
        $this->_db->beginTransaction();
        $revisar=$this->_db->prepare("
            UPDATE
             cp_d011_banco_transaccion
              SET
              ind_estado=:ind_estado,
              fk_rhb001_num_empleado_aprueba='$this->atIdEmpleado',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_aprobado=NOW(),
              fec_ultima_modificacion=NOW()
              WHERE
              pk_num_banco_transaccion='$idTransaccion'
             ");
        $revisar->execute(array(

            'ind_estado' => 'AP'
        ));
        $error = $revisar->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idTransaccion;
        }
    }

    public function metAnularTransaccionBancaria($idTransaccion, $motivoAnulacion)
    {
        $this->_db->beginTransaction();
        $anular=$this->_db->prepare("
            UPDATE
             cp_d011_banco_transaccion
              SET
              ind_estado=:ind_estado,
              txt_comentario_anulacion=:txt_comentario_anulacion,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
              WHERE
              pk_num_banco_transaccion='$idTransaccion'
             ");
        $anular->execute(array(

            'ind_estado' => 'PR',
            'txt_comentario_anulacion' => $motivoAnulacion
        ));
/*
        $transaccion = $this->metConsultarTransacciones($idTransaccion);
        $secuencia=$this->metSecuencia('cp_d011_banco_transaccion','ind_num_transaccion');
        $bancoTransaccion = $this->_db->prepare("
            INSERT INTO
                cp_d011_banco_transaccion
            SET
                fk_cpb006_num_banco_tipo_transaccion =:fk_cpb006_num_banco_tipo_transaccion,
                fk_cpb002_num_tipo_documento =:fk_cpb002_num_tipo_documento,
                fk_a023_num_centro_costo =:fk_a023_num_centro_costo,
                fk_rhb001_num_empleado_crea = '$this->atIdEmpleado',
                ind_num_transaccion =:ind_num_transaccion,
                ind_periodo_contable =:ind_periodo_contable,
                ind_estado = 'AN',
                fec_anio = NOW(),
                fec_transaccion=NOW(),
                fec_ultima_modificacion=NOW(),
                num_monto=:num_monto,
                num_secuencia=:num_secuencia,
                num_flag_presupuesto =:num_flag_presupuesto,
                txt_comentarios =:txt_comentarios,
                ind_mes =LPAD( MONTH (NOW()),2,'0'),
                fec_preparacion=NOW(),
                fk_a003_num_persona_proveedor =:fk_a003_num_persona_proveedor,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
        ");
        $secuencia = $this->metSecuencia('cp_d011_banco_transaccion', 'ind_num_transaccion');
        $tipoTransaccion = $this->metBuscarTipoTransaccion('',$transaccion['fk_cpb006_num_banco_tipo_transaccion']);
        if($tipoTransaccion['cod_detalle']=='E'){
            $signo=-1;
        }else{
            $signo=1;
        }
        $bancoTransaccion->execute(array(
            'fk_cpb006_num_banco_tipo_transaccion' => $transaccion['fk_cpb006_num_banco_tipo_transaccion'],
            'fk_cpb002_num_tipo_documento' => $transaccion['fk_cpb002_num_tipo_documento'],
            'fk_a023_num_centro_costo' => $transaccion['fk_a023_num_centro_costo'],
            'ind_num_transaccion' => $secuencia['secuencia'],
            'ind_periodo_contable' => date('Y-m'),
            'num_monto' => $transaccion['num_monto']*$signo,
            'num_secuencia' => 1,
            'num_flag_presupuesto' => $transaccion['num_flag_presupuesto'],
            'txt_comentarios' => $transaccion['txt_comentarios'],
            'fk_a003_num_persona_proveedor' => $transaccion['fk_a003_num_persona_proveedor']
        ));*/
        $error1 = $anular->errorInfo();
        //$error2 = $bancoTransaccion->errorInfo();
        if ((!empty($error1[1]) && !empty($error1[2]))) {
            $this->_db->rollBack();
            return $error1;
        } elseif (!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        } else {
            $this->_db->commit();
            return $idTransaccion;
        }
    }

    public function metRechazarTransaccionBancaria($idTransaccion, $motivoAnulacion)
    {
        $this->_db->beginTransaction();
        $anular=$this->_db->prepare("
            UPDATE
             cp_d011_banco_transaccion
              SET
              ind_estado=:ind_estado,
              txt_comentario_anulacion=:txt_comentario_anulacion,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
              WHERE
              pk_num_banco_transaccion='$idTransaccion'
             ");
        $anular->execute(array(

            'ind_estado' => 'PR',
            'txt_comentario_anulacion' => $motivoAnulacion
        ));
        $error1 = $anular->errorInfo();
        //$error2 = $bancoTransaccion->errorInfo();
        if ((!empty($error1[1]) && !empty($error1[2]))) {
            $this->_db->rollBack();
            return $error1;
        } elseif (!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        } else {
            $this->_db->commit();
            return $idTransaccion;
        }
    }

    public function metMostrarPartidas($idBancoTransaccion)
    {
        $consultaImpuesto = $this->_db->query(
            "SELECT
              pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria,
              cp_d011_banco_transaccion.num_monto

             FROM
              cp_d011_banco_transaccion
             INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_d011_banco_transaccion.fk_prb002_num_partida_presupuestaria
          WHERE
             cp_d011_banco_transaccion.pk_num_banco_transaccion = '$idBancoTransaccion'
       ");
        $consultaImpuesto->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaImpuesto->fetchAll();
    }

    public function metBuscarDistribucion($idBancoTransaccion,$tipo)
    {
        $listar = $this->_db->query(
            "SELECT
                pk_num_estado_distribucion,
                num_monto,
                fk_prc002_num_presupuesto_det,
                fk_cpd011_num_banco_transaccion
              FROM
                pr_d008_estado_distribucion
              WHERE
                fk_cpd011_num_banco_transaccion='$idBancoTransaccion' AND
                ind_estado='AC' AND
                ind_tipo_distribucion='$tipo'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metPresupuesto($idBancoTransaccion)
    {
        $this->_db->beginTransaction();
        $afectarPresupuestoCO = $this->_db->prepare("
        INSERT INTO
                pr_d008_estado_distribucion
            SET
                fec_periodo=NOW() ,
                num_monto=:num_monto,
                ind_tipo_distribucion='CO',
                ind_estado='AC',
                fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,
                fk_cpd011_num_banco_transaccion=:fk_cpd011_num_banco_transaccion
        ");
        $partidas = $this->metMostrarPartidas($idBancoTransaccion);
        foreach ($partidas AS $partida) {
            $idPresupuestoDet = $this->atObligacionModelo->metBuscarPartidaCuenta($partida['pk_num_partida_presupuestaria']);
            if ($partida['pk_num_partida_presupuestaria'] != null) {
                $afectarPresupuestoCO->execute(array(
                    'fk_cpd011_num_banco_transaccion' => $idBancoTransaccion,
                    'num_monto' => $partida['num_monto'],
                    'fk_prc002_num_presupuesto_det' => $idPresupuestoDet['pk_num_presupuesto_det']
                ));
            }
        }
        $afectarPresupuestoCA = $this->_db->prepare("
            INSERT INTO
                pr_d008_estado_distribucion
            SET
                fec_periodo=NOW(),
                num_monto=:num_monto,
                ind_tipo_distribucion='CA',
                ind_estado='AC',
                fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,
                fk_prd008_num_estado_distribucion=:fk_prd008_num_estado_distribucion,
                fk_cpd011_num_banco_transaccion=:fk_cpd011_num_banco_transaccion
        ");
        $distribucion = $this->metBuscarDistribucion($idBancoTransaccion,'CO');
        foreach ($distribucion AS $dist) {
            $afectarPresupuestoCA->execute(array(
                'fk_cpd011_num_banco_transaccion' => $idBancoTransaccion,
                'num_monto' => $dist['num_monto'],
                'fk_prc002_num_presupuesto_det' => $dist['fk_prc002_num_presupuesto_det'],
                'fk_prd008_num_estado_distribucion' => $dist['pk_num_estado_distribucion']
            ));
        }
        $afectarPresupuestoPA = $this->_db->prepare("
            INSERT INTO
                pr_d008_estado_distribucion
            SET
                fec_periodo=NOW(),
                num_monto=:num_monto,
                ind_tipo_distribucion='PA',
                ind_estado='AC',
                fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,
                fk_prd008_num_estado_distribucion=:fk_prd008_num_estado_distribucion,
                fk_cpd011_num_banco_transaccion=:fk_cpd011_num_banco_transaccion
        ");
        $distribucion = $this->metBuscarDistribucion($idBancoTransaccion,'CA');
        foreach ($distribucion AS $dist) {
            $afectarPresupuestoPA->execute(array(
                'fk_cpd011_num_banco_transaccion' => $idBancoTransaccion,
                'num_monto' => $dist['num_monto'],
                'fk_prc002_num_presupuesto_det' => $dist['fk_prc002_num_presupuesto_det'],
                'fk_prd008_num_estado_distribucion' => $dist['pk_num_estado_distribucion']
            ));
        }
        $fallaTansaccion = $afectarPresupuestoCA->errorInfo();
        $fallaTansaccion2 = $afectarPresupuestoCA->errorInfo();
        $fallaTansaccion3 = $afectarPresupuestoPA->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } elseif (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion2;
        } else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $partidas;
        }

    }

    //vouchers
    public function metBuscarTransacciones($id)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d011_banco_transaccion.*,
              cb_c003_tipo_voucher.cod_voucher,
              cp_b006_banco_tipo_transaccion.cod_tipo_transaccion,
              a006_miscelaneo_detalle.cod_detalle,
              cp_b002_tipo_documento.fk_cbc003_num_tipo_voucher,
              cp_b002_tipo_documento.cod_tipo_documento,
              CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS proveedor
             FROM
              cp_d011_banco_transaccion
             INNER JOIN cp_b006_banco_tipo_transaccion ON cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = cp_d011_banco_transaccion.	fk_cpb006_num_banco_tipo_transaccion
             LEFT JOIN cb_c003_tipo_voucher on cb_c003_tipo_voucher.pk_num_voucher = cp_b006_banco_tipo_transaccion.fk_cbc003_tipo_voucher
             INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
             INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d011_banco_transaccion.fk_cpb002_num_tipo_documento
             INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d011_banco_transaccion.fk_a003_num_persona_proveedor
             WHERE
              cp_d011_banco_transaccion.ind_estado = 'PR'
              and cp_d011_banco_transaccion.num_flag_genera_voucher = 1
              AND cp_d011_banco_transaccion.pk_num_banco_transaccion = '$id'

            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metDetalleVoucher($idVoucherPago)
    {
        $listar = $this->_db->query(
            "SELECT cb_c001_voucher_det.*,
                cb_b004_plan_cuenta.cod_cuenta,
                a003_persona.pk_num_persona,
                CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS proveedor
            FROM
                cb_c001_voucher_det
            INNER JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cb_c001_voucher_det.fk_cbb004_num_cuenta
            INNER JOIN cb_b001_voucher ON cb_b001_voucher.pk_num_voucher_mast = cb_c001_voucher_det.fk_cbb001_num_voucher_mast
           INNER JOIN cp_d011_banco_transaccion ON cp_d011_banco_transaccion.fk_cbb001_num_voucher = cb_b001_voucher.pk_num_voucher_mast
           INNER JOIN a003_persona ON a003_persona.pk_num_persona = cp_d011_banco_transaccion.fk_a003_num_persona_proveedor
            WHERE
            cb_c001_voucher_det.fk_cbb001_num_voucher_mast = '$idVoucherPago'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metListarTipoDocumento($tipoTransaccion)
    {
        $documento = $this->_db->query("
            SELECT
              cp_b002_tipo_documento.*
            FROM
              cp_b002_tipo_documento
            WHERE
              cp_b002_tipo_documento.fk_a006_miscelaneo_detalle_tipo_transaccion = '$tipoTransaccion'
        ");
        $documento->setFetchMode(PDO::FETCH_ASSOC);
        return $documento->fetchAll();
    }

    public function metTransaccionListar($idTransaccion = false)
    {
        if($idTransaccion){
            $where = "WHERE cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = '$idTransaccion'";
        }else{
            $where = "";
        }
        $transaccionListar =  $this->_db->query(
            "SELECT
              cp_b006_banco_tipo_transaccion.*,
              a006_miscelaneo_detalle.ind_nombre_detalle,
              cb_b004_plan_cuenta.cod_cuenta
            FROM
            cp_b006_banco_tipo_transaccion
             INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
             LEFT JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
              $where
             ORDER BY cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion ASC"
        );
        //var_dump($transaccionListar);
        $transaccionListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idTransaccion){
            return $transaccionListar->fetch();
        }else{
            return $transaccionListar->fetchAll();
        }

    }
}
