<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';

class documentoTransaccionModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();

        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
    }

    public function metDocumentoListar($idDocumento = false)
    {
        if($idDocumento){
            $and = "AND d.pk_num_tipo_documento = '$idDocumento'";
        }else{
            $and = "";
        }
        $documentoListar =  $this->_db->query(
            "SELECT
            d.*,
            a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
            cp_b002_tipo_documento AS d
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = d.fk_a006_miscelaneo_detalle_tipo_transaccion
            WHERE d.num_flag_obligacion = 0 $and
            ORDER BY cod_tipo_documento ASC
          ");
        //var_dump($documentoListar);
        $documentoListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idDocumento){
            return $documentoListar->fetch();
        }else{
            return $documentoListar->fetchAll();
        }
    }

    public function metDocumentoConsultaDetalle($idDocumento)
    {
        $consultaDocumento =  $this->_db->query(
            "SELECT
                d.*,
                a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
                cp_b002_tipo_documento AS d
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = d.fk_a006_miscelaneo_detalle_tipo_transaccion
            WHERE
                d.pk_num_tipo_documento='$idDocumento'
              "
        );
        $consultaDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaDocumento->fetch();
    }

    public function metNuevoDocumento($form)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
              cp_b002_tipo_documento
              SET
                cod_tipo_documento=:cod_tipo_documento,
                ind_descripcion=:ind_descripcion,
                fk_a006_miscelaneo_detalle_tipo_transaccion=:fk_a006_miscelaneo_detalle_tipo_transaccion,
                num_flag_obligacion=:num_flag_obligacion,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
               ");
        $NuevoRegistro->execute(array(
            ':cod_tipo_documento' => $form['cod_tipo_documento'],
            ':ind_descripcion' => $form['ind_descripcion'],
            ':fk_a006_miscelaneo_detalle_tipo_transaccion' => $form['fk_a006_miscelaneo_detalle_tipo_transaccion'],
            ':num_flag_obligacion' => 0,
            ':num_estatus' => $form['num_estatus']

        ));

        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarDocumento($idDocumento, $form)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              cp_b002_tipo_documento
              SET
                ind_descripcion=:ind_descripcion,
                fk_a006_miscelaneo_detalle_tipo_transaccion=:fk_a006_miscelaneo_detalle_tipo_transaccion,
                num_estatus=:num_estatus,
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                WHERE
                pk_num_tipo_documento='$idDocumento'
               ");
        $NuevoRegistro->execute(array(
            ':ind_descripcion' => $form['ind_descripcion'],
            ':fk_a006_miscelaneo_detalle_tipo_transaccion' => $form['fk_a006_miscelaneo_detalle_tipo_transaccion'],
            ':num_estatus' => $form['num_estatus']

        ));

        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }
}
