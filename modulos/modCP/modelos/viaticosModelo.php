<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once 'listarCajaChicaModelo.php';
require_once 'conceptoViaticosModelo.php';
require_once RUTA_MODELO.'miscelaneoModelo.php';

class viaticosModelo extends Modelo
{
    private $atIdUsuario;
    public $atListarCajaChicaModelo;
    public $atConceptoViaticosModelo;
    public $atMiscelaneoModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atListarCajaChicaModelo = new listarCajaChicaModelo();
        $this->atConceptoViaticosModelo = new conceptoViaticosModelo();
        $this->atMiscelaneoModelo=new miscelaneoModelo();

    }

    public function metSecuencia($campo,$tabla)
    {
        $secuencia = $this->_db->query(
            "SELECT
                  IF(
                        MAX($campo),
                        MAX($campo)+1,
                        1
                  ) AS secuencia
            FROM $tabla"
        );
        $secuencia->setFetchMode(PDO::FETCH_ASSOC);
        return $secuencia->fetch();
    }

    public function metBuscarDependenciaCC($persona)
    {
        $dependencia = $this->_db->query("
          	SELECT
              a004_dependencia.ind_dependencia,
              a004_dependencia.pk_num_dependencia,
              a023_centro_costo.pk_num_centro_costo,
              a023_centro_costo.ind_descripcion_centro_costo
            FROM
              rh_c076_empleado_organizacion
              INNER JOIN rh_b001_empleado ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
              INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
              INNER JOIN a004_dependencia ON a004_dependencia.pk_num_dependencia = rh_c076_empleado_organizacion.fk_a004_num_dependencia
              INNER JOIN a023_centro_costo ON a023_centro_costo.pk_num_centro_costo = rh_c076_empleado_organizacion.fk_a023_num_centro_costo
            WHERE
              a003_persona.pk_num_persona = '$persona'
          ");
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetch();
    }

    public function metBuscarCargo($persona)
    {
        $dependencia = $this->_db->query("
          	SELECT
                cargo.ind_nombre_cargo
            FROM
              rh_b001_empleado AS empleado
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
              INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
              INNER JOIN rh_c006_tipo_cargo AS cargo ON cargo.pk_num_cargo = puesto.fk_rhc006_num_cargo
            WHERE
                persona.pk_num_persona = '$persona'
          ");
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetch();
    }

    public function metBuscarUnidadTrib()
    {
        $listar = $this->_db->query(
            "SELECT
                ind_valor
             FROM
                cp_b018_unidad_tributaria
              INNER JOIN a006_miscelaneo_detalle on cp_b018_unidad_tributaria.fk_a006_num_miscelaneo_detalle_clasificacion_unidad=a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
              WHERE a006_miscelaneo_detalle.cod_detalle='UT' AND
                (SELECT MAX(ind_anio) FROM cp_b018_unidad_tributaria)
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metListarSolicitudViaticos()
    {
        $listar = $this->_db->query(
            "SELECT
              rh_c045_viatico.pk_num_viatico,
              rh_c045_viatico.cod_interno,
              rh_c045_viatico.fec_salida,
              rh_c045_viatico.fec_ingreso,
              rh_c045_viatico.num_total_dias,
              rh_c045_viatico.ind_motivo,
              rh_c045_viatico.ind_destino,
              rh_c045_viatico.fec_registro_viatico,
              cp_b011_viaticos.pk_num_viaticos,
              a003_persona.pk_num_persona AS pkSolicitante,
              CONCAT(a003_persona.ind_nombre1, ' ', a003_persona.ind_apellido1) AS solicitante
            FROM
              rh_c045_viatico
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c045_viatico.fk_rhb001_num_empleado_solicita
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            LEFT JOIN cp_b011_viaticos ON cp_b011_viaticos.fk_rhc045_num_viatico = rh_c045_viatico.pk_num_viatico
            WHERE
            rh_c045_viatico.ind_estado = 'AP'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metListarViaticos($estado)
    {
        if($estado == 'GE'){
            $estadoConsulta = "AP";
            $cantidad= "(SELECT COUNT(cp_c003_viatico_detalle.pk_num_viatico_detalle) FROM cp_c003_viatico_detalle
                        INNER JOIN cp_b011_viaticos ON cp_b011_viaticos.pk_num_viaticos = cp_c003_viatico_detalle.fk_cpb011_num_viatico
                        WHERE cp_c003_viatico_detalle.ind_estado = 'PR' ) AS cantidad,";
        }else if($estado == 'AP'){
            $estadoConsulta = "RE";
            $cantidad = "";
        }else if($estado == 'RE'){
            $estadoConsulta = "PR";
            $cantidad = "";
        }else{
            $estadoConsulta = 'PR';
            $cantidad = "";
        }
        $listar = $this->_db->query(
            "SELECT
              cp_b011_viaticos.*,
              rh_c045_viatico.pk_num_viatico,
              rh_c045_viatico.cod_interno,
              rh_c045_viatico.fec_registro_viatico,
              a003_persona.pk_num_persona AS pkSolicitante,
              CONCAT(a003_persona.ind_nombre1, ' ', a003_persona.ind_apellido1) AS solicitante,
              a018_seguridad_usuario.ind_usuario,
              $cantidad
              (CASE
                        WHEN cp_b011_viaticos.ind_estado='PR' THEN 'PREPARADO'
                        WHEN cp_b011_viaticos.ind_estado='RE' THEN 'REVISADO'
                        WHEN cp_b011_viaticos.ind_estado='AP' THEN 'APROBADO'
                        WHEN cp_b011_viaticos.ind_estado='AN' THEN 'ANULADO'
                END) AS ind_estado
            FROM
              cp_b011_viaticos
            LEFT JOIN rh_c045_viatico ON rh_c045_viatico.pk_num_viatico =cp_b011_viaticos.fk_rhc045_num_viatico
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c045_viatico.fk_rhb001_num_empleado_solicita
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_b011_viaticos.fk_a018_num_seguridad_usuario
            WHERE
              cp_b011_viaticos.ind_estado = '$estadoConsulta'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metConsultarViaticos($idViatico)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_b011_viaticos.*,
              rh_c045_viatico.cod_interno,
              rh_c045_viatico.fec_registro_viatico,
              a003_persona.pk_num_persona AS pkSolicitante,
              CONCAT(a003_persona.ind_nombre1, ' ', a003_persona.ind_apellido1) AS solicitante,
              a018_seguridad_usuario.ind_usuario,
              (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_b011_viaticos ON rh_b001_empleado.pk_num_empleado = cp_b011_viaticos.fk_rhb001_num_empleado_ingresa
                    WHERE
                        pk_num_viaticos = '$idViatico'
                ) AS EMPLEADO_INGRESA,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_b011_viaticos ON rh_b001_empleado.pk_num_empleado = cp_b011_viaticos.fk_rhb001_num_empleado_aprueba
                    WHERE
                        pk_num_viaticos = '$idViatico'
                ) AS EMPLEADO_APRUEBA,
              (CASE
                        WHEN cp_b011_viaticos.ind_estado='PR' THEN 'PREPARADO'
                        WHEN cp_b011_viaticos.ind_estado='AP' THEN 'APROBADO'
                END) AS ind_estado
            FROM
              cp_b011_viaticos
            LEFT JOIN rh_c045_viatico ON rh_c045_viatico.pk_num_viatico =cp_b011_viaticos.fk_rhc045_num_viatico
            INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c045_viatico.fk_rhb001_num_empleado_solicita
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_b011_viaticos.fk_a018_num_seguridad_usuario
            WHERE
            pk_num_viaticos = '$idViatico'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metConsultarDetalleViaticos($idViatico, $idBeneficiario = false)
    {
        if($idBeneficiario){
            $and = "AND cp_c003_viatico_detalle.fk_a003_persona_beneficiario ='$idBeneficiario'";
        }else{
            $and = "";
        }
        $listar = $this->_db->query(
            "SELECT
              cp_c003_viatico_detalle.*,
              cp_b009_concepto_gasto_viatico.ind_descripcion,
              cp_b009_concepto_gasto_viatico.num_valor_ut,
              CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS nombreBeneficiario,
              a003_persona.ind_documento_fiscal,
              a006_miscelaneo_detalle.ind_nombre_detalle,
              cp_b011_viaticos.ind_num_viatico,
              cp_c003_viatico_detalle.num_monto_total,
              cp_b009_concepto_gasto_viatico.ind_descripcion as concepto,
              cuenta.pk_num_cuenta AS pk_num_cuenta,
              cuenta.cod_cuenta AS cod_cuenta,
              cuenta.ind_descripcion AS nombreCuenta,
              cuenta20.pk_num_cuenta AS pk_num_cuenta20,
              cuenta20.cod_cuenta AS cod_cuenta20,
              cuenta20.ind_descripcion AS nombreCuenta20,
              pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria,
              pr_b002_partida_presupuestaria.cod_partida,
              pr_b002_partida_presupuestaria.ind_denominacion,
              cp_d001_obligacion.ind_nro_registro,
              cp_d001_obligacion.ind_nro_control
            FROM
              cp_c003_viatico_detalle
            INNER JOIN cp_b011_viaticos ON cp_b011_viaticos.pk_num_viaticos = cp_c003_viatico_detalle.fk_cpb011_num_viatico
            INNER JOIN cp_b009_concepto_gasto_viatico ON cp_b009_concepto_gasto_viatico.pk_num_concepto_gasto_viatico = cp_c003_viatico_detalle.fk_cpb009_num_concepto_gasto_viatico
            LEFT JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_c003_viatico_detalle.fk_cpd001_num_obligacion
            LEFT JOIN rh_b001_empleado ON rh_b001_empleado.fk_a003_num_persona = cp_c003_viatico_detalle.fk_a003_persona_beneficiario
            LEFT JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            LEFT JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c076_empleado_organizacion.fk_a006_num_miscelaneo_detalle_tipopago
            LEFT JOIN a003_persona ON a003_persona.pk_num_persona = cp_c003_viatico_detalle.fk_a003_persona_beneficiario
            LEFT JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = cp_c003_viatico_detalle.fk_cbb004_num_plan_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS cuenta20 ON cuenta20.pk_num_cuenta = cp_c003_viatico_detalle.fk_cbb004_num_plan_cuenta_pub20
            LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_c003_viatico_detalle.fk_prb002_num_partida_presupuestaria
            WHERE
              fk_cpb011_num_viatico = '$idViatico' $and
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();

    }

    public function metConsultarDetalleContableViat($idViatico)
    {
        $listar = $this->_db->query(
            "SELECT
              SUM(cp_c003_viatico_detalle.num_monto_total) AS monto,
              cuenta.pk_num_cuenta AS pk_num_cuenta,
              cuenta.cod_cuenta AS cod_cuenta,
              cuenta.ind_descripcion AS nombreCuenta,
              cuenta20.pk_num_cuenta AS pk_num_cuenta20,
              cuenta20.cod_cuenta AS cod_cuenta20,
              cuenta20.ind_descripcion AS nombreCuenta20,
              pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria,
              pr_b002_partida_presupuestaria.cod_partida,
              pr_b002_partida_presupuestaria.ind_denominacion
            FROM
              cp_c003_viatico_detalle
            INNER JOIN cp_b011_viaticos ON cp_b011_viaticos.pk_num_viaticos = cp_c003_viatico_detalle.fk_cpb011_num_viatico
            INNER JOIN cp_b009_concepto_gasto_viatico ON cp_b009_concepto_gasto_viatico.pk_num_concepto_gasto_viatico = cp_c003_viatico_detalle.fk_cpb009_num_concepto_gasto_viatico
            LEFT JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = cp_c003_viatico_detalle.fk_cbb004_num_plan_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS cuenta20 ON cuenta20.pk_num_cuenta = cp_c003_viatico_detalle.fk_cbb004_num_plan_cuenta_pub20
            LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_c003_viatico_detalle.fk_prb002_num_partida_presupuestaria
            WHERE
              fk_cpb011_num_viatico = '$idViatico'

              GROUP BY cp_c003_viatico_detalle.fk_prb002_num_partida_presupuestaria
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();

    }

    public function metConsultarSolicitudViaticos($idSolViatico)
    {
        $listar = $this->_db->query(
            "SELECT
              rh_c045_viatico.pk_num_viatico,
              rh_c045_viatico.cod_interno,
              rh_c045_viatico.fec_salida,
              rh_c045_viatico.fec_ingreso,
              rh_c045_viatico.num_total_dias,
              rh_c045_viatico.ind_motivo,
              rh_c045_viatico.ind_destino,
              rh_c045_viatico.ind_observacion,
              rh_c045_viatico.num_unidad_combustible,
              rh_c045_viatico.fec_registro_viatico,
              (
                    SELECT
                        rh_b001_empleado.pk_num_empleado
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN rh_c045_viatico ON rh_b001_empleado.pk_num_empleado = rh_c045_viatico.fk_rhb001_num_empleado_solicita
                    WHERE
                        rh_c045_viatico.pk_num_viatico = '$idSolViatico'
                ) AS pkSolicitante,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN rh_c045_viatico ON rh_b001_empleado.pk_num_empleado = rh_c045_viatico.fk_rhb001_num_empleado_solicita
                    WHERE
                        rh_c045_viatico.pk_num_viatico = '$idSolViatico'
                ) AS solicitante,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN rh_c045_viatico ON rh_b001_empleado.pk_num_empleado = rh_c045_viatico.fk_rhb001_num_empleado_preparado_por
                    WHERE
                        rh_c045_viatico.pk_num_viatico = '$idSolViatico'
                ) AS EMPLEADO_PREPARA,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN rh_c045_viatico ON rh_b001_empleado.pk_num_empleado = rh_c045_viatico.fk_rhb001_num_empleado_aprobado_por
                    WHERE
                        rh_c045_viatico.pk_num_viatico = '$idSolViatico'
                ) AS EMPLEADO_APRUEBA
            FROM
              rh_c045_viatico
            WHERE
            rh_c045_viatico.pk_num_viatico = '$idSolViatico'
        ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metConsultarDetalleSolViaticos($idSolViatico, $idBeneficiario = false)
    {
        if($idBeneficiario){
            $and = "and a003_persona.pk_num_persona = '$idBeneficiario'";
        }else{
            $and = "";
        }
        $listar = $this->_db->query(
        "SELECT
            rh_c004_detalle_viatico.*,
            rh_c004_detalle_viatico.fk_rhb001_num_empleado_beneficiario,
            a003_persona.pk_num_persona,
            CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1) AS beneficiario
         FROM
            rh_c004_detalle_viatico
         INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = rh_c004_detalle_viatico.fk_rhb001_num_empleado_beneficiario
         INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona

           WHERE
            fk_rhc045_num_viatico = '$idSolViatico'
            $and
        ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        if($idBeneficiario){
            return $listar->fetch();
        }else{
            return $listar->fetchAll();
        }
    }

    public function metNuevoViatico($idSolViatico,$form)
    {
        $this->_db->beginTransaction();
        $secuencia=$this->metSecuencia('ind_num_viatico','cp_b011_viaticos');
        $NuevoRegistro=$this->_db->prepare(
            "INSERT INTO
             cp_b011_viaticos
              SET
               fk_rhc045_num_viatico='$idSolViatico',
               ind_num_viatico = LPAD( '".$secuencia['secuencia']."',4,'0'),
               ind_descripcion=:ind_descripcion,
               ind_periodo=:ind_periodo,
               ind_destino=:ind_destino,
               ind_motivo=:ind_motivo,
               ind_estado='PR',
               fec_salida=:fec_salida,
               fec_regreso=:fec_regreso,
               fec_preparado=NOW(),
               fk_rhb001_num_empleado_solicita=:fk_rhb001_num_empleado_solicita,
               fk_rhb001_num_empleado_ingresa='$this->atIdUsuario',
               fk_a018_num_seguridad_usuario='$this->atIdUsuario',
               fec_ultima_modificacion=NOW()
               ");
        $NuevoRegistro->execute(array(
            'fk_rhb001_num_empleado_solicita' => $form['pkSolicitante'],
            'ind_descripcion' => $form['ind_descripcion'],
            'ind_periodo' => $form['ind_periodo'],
            'ind_destino' => $form['ind_destino'],
            'ind_motivo' => $form['ind_motivo'],
            'fec_salida' => $form['fec_salida'],
            'fec_regreso' => $form['fec_regreso']
        ));
        $idViatico = $this->_db->lastInsertId();

        #CONCEPTOS viaticos detalles
        $registrarConcepto = $this->_db->prepare("
            INSERT INTO
                cp_c003_viatico_detalle
            SET
                fk_cpb011_num_viatico=:fk_cpb011_num_viatico,
                ind_estado=:ind_estado,
                fk_a003_persona_beneficiario=:fk_a003_persona_beneficiario,
                fk_cpb009_num_concepto_gasto_viatico=:fk_cpb009_num_concepto_gasto_viatico,
                num_monto_viatico=:num_monto_viatico,
                num_dia=:num_dia,
                num_monto_total=:num_monto_total,
                ind_secuencia=:ind_secuencia,
                num_unidad_viatico=:num_unidad_viatico,
                num_uv=:num_uv,
                fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
        ");
        $conceptoViatico = $form['conceptoViatico'];
        foreach ($conceptoViatico['fk_a003_persona_beneficiario'] AS $i) {
            $secuencia = 1;


            foreach($conceptoViatico[$i]['fk_cpb009_num_concepto_gasto_viatico'] as $ii) {
                $valor = $conceptoViatico[$i][$ii];
                $registrarConcepto->execute(array(
                    'fk_cpb011_num_viatico' => $idViatico,
                    'ind_estado' => 'PR',
                    'fk_a003_persona_beneficiario' => $i,
                    'fk_cpb009_num_concepto_gasto_viatico' => $ii,
                    'num_monto_viatico' => $valor['num_monto_viatico'],
                    'num_dia' => $valor['num_dia'],
                    'num_uv' => $valor['num_uv'],
                    'num_monto_total' => $valor['num_monto_total'],
                    'num_unidad_viatico' => $valor['num_unidad_viatico'],
                    'ind_secuencia' => $secuencia,
                    'fk_cbb004_num_plan_cuenta' => $valor['fk_cbb004_num_plan_cuenta'],
                    'fk_cbb004_num_plan_cuenta_pub20' => $valor['fk_cbb004_num_plan_cuenta_pub20'],
                    'fk_prb002_num_partida_presupuestaria' => $valor['fk_prb002_num_partida_presupuestaria']
                ));
            $secuencia++;
            }
        }

        $fallaTansaccion = $NuevoRegistro->errorInfo();


        if(
            (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) ||
            (!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2]))
        ){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idViatico;
        }
    }

    public function metModificarViatico($idViatico,$form)
    {
        $this->_db->beginTransaction();
        $conceptoViatico = $form['conceptoViatico'];
        foreach ($conceptoViatico['fk_a003_persona_beneficiario'] AS $i) {
            $secuencia = 1;
            foreach($conceptoViatico[$i]['fk_cpb009_num_concepto_gasto_viatico'] as $ii) {
        #CONCEPTOS viaticos detalles
        if(isset($ids['pk_num_viatico_detalle'])){
            $titulo='UPDATE';
            $where="WHERE fk_a003_persona_beneficiario ='".$conceptoViatico['fk_a003_persona_beneficiario']."' ";
        }else{
            $titulo='INSERT INTO';
            $where="";
        }
        $registrarConcepto = $this->_db->prepare("
            $titulo
                cp_c003_viatico_detalle
                SET
                    fk_cpb011_num_viatico=:fk_cpb011_num_viatico,
                    ind_estado=:ind_estado,
                    fk_a003_persona_beneficiario=:fk_a003_persona_beneficiario,
                    fk_cpb009_num_concepto_gasto_viatico=:fk_cpb009_num_concepto_gasto_viatico,
                    num_monto_viatico=:num_monto_viatico,
                    num_dia=:num_dia,
                    num_monto_total=:num_monto_total,
                    ind_secuencia=:ind_secuencia,
                    num_unidad_viatico=:num_unidad_viatico,
                    num_uv=:num_uv,
                    fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                    fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
                    fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
                $where
            ");
                $valor = $conceptoViatico[$i][$ii];

                $registrarConcepto->execute(array(
                    'fk_cpb011_num_viatico' => $idViatico,
                    'ind_estado' => 'PR',
                    'fk_a003_persona_beneficiario' => $i,
                    'fk_cpb009_num_concepto_gasto_viatico' => $ii,
                    'num_monto_viatico' => $valor['num_monto_viatico'],
                    'num_dia' => $valor['num_dia'],
                    'num_monto_total' => $valor['num_monto_total'],
                    'ind_secuencia' => $secuencia,
                    'num_uv' => $valor['num_uv'],
                    'num_unidad_viatico' => $valor['num_unidad_viatico'],
                    'fk_cbb004_num_plan_cuenta' => $valor['fk_cbb004_num_plan_cuenta'],
                    'fk_cbb004_num_plan_cuenta_pub20' => $valor['fk_cbb004_num_plan_cuenta_pub20'],
                    'fk_prb002_num_partida_presupuestaria' => $valor['fk_prb002_num_partida_presupuestaria']
                ));
                $secuencia++;
            }
        }

        $error = $registrarConcepto->errorInfo();
        if(
            (!empty($error[1]) && !empty($error[2]))
        ){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idViatico;
        }
    }

    public function metRevisarViatico($idViatico)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
            UPDATE
                cp_b011_viaticos
            SET
              fk_rhb001_num_empleado_revisa='$this->atIdUsuario',
              fec_revisado=NOW(),
              ind_estado='RE'
            WHERE
              pk_num_viaticos=:pk_num_viaticos
        ");
        $registro->execute(array(
            ':pk_num_viaticos' => $idViatico
        ));
        $fallaTansaccion = $registro->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metAprobarViatico($idViatico)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
            UPDATE
                cp_b011_viaticos
            SET
              fk_rhb001_num_empleado_aprueba='$this->atIdUsuario',
              fec_aprobado=NOW(),
              ind_estado='AP'
            WHERE
              pk_num_viaticos=:pk_num_viaticos
        ");
        $registro->execute(array(
            ':pk_num_viaticos' => $idViatico
        ));
        $fallaTansaccion = $registro->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metAnularViatico($idViatico)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
            UPDATE
                cp_b011_viaticos
            SET
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              ind_estado='AN'
            WHERE
              pk_num_viaticos=:pk_num_viaticos
        ");
        $registro->execute(array(
            ':pk_num_viaticos' => $idViatico
        ));
        $fallaTansaccion = $registro->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metRechazarViatico($idViatico)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
            UPDATE
                cp_b011_viaticos
            SET
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              ind_estado='PR'
            WHERE
              pk_num_viaticos=:pk_num_viaticos
        ");
        $registro->execute(array(
            ':pk_num_viaticos' => $idViatico
        ));
        $fallaTansaccion = $registro->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metBuscarConcepto($idConcepto)
    {
        $listar = $this->_db->query(
            "SELECT
                *
             FROM
                cp_b009_concepto_gasto_viatico
             WHERE
                pk_num_concepto_gasto_viatico = '$idConcepto'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metBeneficiariosViatico($idViatico)
    {
        $consulta = $this->_db->prepare("
        SELECT
        rh_b001_empleado.pk_num_empleado,
        a003_persona.pk_num_persona,
        a003_persona.ind_cedula_documento,
        a003_persona.ind_nombre1,
        a003_persona.ind_nombre2,
        a003_persona.ind_apellido1,
        a003_persona.ind_apellido2
        FROM
        rh_b001_empleado
        INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
        INNER JOIN cp_c003_viatico_detalle ON cp_c003_viatico_detalle.fk_a003_persona_beneficiario = a003_persona.pk_num_persona
        WHERE
        cp_c003_viatico_detalle.fk_cpb011_num_viatico = '$idViatico'
        GROUP BY
        pk_num_persona
        ");
        $consulta->execute();
        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    public function metDatosCabeceraViatico($idViatico,$idPersona)
    {
      $consulta = $this->_db->prepare("
      SELECT
      cp_b011_viaticos.pk_num_viaticos,
      cp_b011_viaticos.ind_num_viatico,
      cp_b011_viaticos.ind_descripcion,
      cp_b011_viaticos.ind_periodo,
      cp_b011_viaticos.ind_destino,
      cp_b011_viaticos.ind_observacion_apro,
      cp_b011_viaticos.ind_observacion_rech,
      cp_b011_viaticos.ind_razon_rechazo,
      cp_b011_viaticos.ind_motivo,
      cp_b011_viaticos.ind_estado,
      cp_b011_viaticos.fec_salida,
      cp_b011_viaticos.fec_regreso,
      a003_persona.ind_cedula_documento,
      CONCAT_WS(
      		' ',
      		a003_persona.ind_nombre1,
      		a003_persona.ind_nombre2,
      		a003_persona.ind_apellido1,
      		a003_persona.ind_apellido2
      	) AS empleado,
      cp_c003_viatico_detalle.fk_a003_persona_beneficiario,
      a004_dependencia.ind_dependencia
      FROM
      cp_c003_viatico_detalle
      INNER JOIN cp_b011_viaticos ON cp_c003_viatico_detalle.fk_cpb011_num_viatico = cp_b011_viaticos.pk_num_viaticos
      INNER JOIN a003_persona ON cp_c003_viatico_detalle.fk_a003_persona_beneficiario = a003_persona.pk_num_persona
      INNER JOIN rh_b001_empleado ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
      INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
      INNER JOIN a004_dependencia ON rh_c076_empleado_organizacion.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
        WHERE
        cp_b011_viaticos.pk_num_viaticos = '$idViatico'
        AND cp_c003_viatico_detalle.fk_a003_persona_beneficiario = '$idPersona'
        GROUP BY
        cp_b011_viaticos.pk_num_viaticos,
        cp_c003_viatico_detalle.fk_a003_persona_beneficiario
      ");
      $consulta->execute();
      return $consulta->fetch(PDO::FETCH_ASSOC);
    }

    public function metGastosViatico($idViatico,$idPersona)
    {
      $consulta = $this->_db->prepare("
      SELECT
          cp_c003_viatico_detalle.pk_num_viatico_detalle,
          cp_c003_viatico_detalle.ind_estado,
          cp_c003_viatico_detalle.ind_secuencia,
          cp_c003_viatico_detalle.num_unidad_viatico,
          cp_c003_viatico_detalle.num_uv,
          cp_c003_viatico_detalle.num_monto_viatico,
          cp_c003_viatico_detalle.num_dia,
          cp_c003_viatico_detalle.num_monto_total,
          cp_c003_viatico_detalle.fk_cpb011_num_viatico,
          cp_c003_viatico_detalle.fk_cpd001_num_obligacion,
          cp_c003_viatico_detalle.fk_prb002_num_partida_presupuestaria,
          cp_c003_viatico_detalle.fk_cpb009_num_concepto_gasto_viatico,
          cp_c003_viatico_detalle.fk_a003_persona_beneficiario,
          cp_c003_viatico_detalle.fk_cbb004_num_plan_cuenta,
          cp_c003_viatico_detalle.fk_cbb004_num_plan_cuenta_pub20,
          cp_b009_concepto_gasto_viatico.ind_descripcion,
          cp_b009_concepto_gasto_viatico.ind_reporte
      FROM
          cp_c003_viatico_detalle
      INNER JOIN cp_b009_concepto_gasto_viatico ON cp_c003_viatico_detalle.fk_cpb009_num_concepto_gasto_viatico = cp_b009_concepto_gasto_viatico.pk_num_concepto_gasto_viatico
      WHERE cp_c003_viatico_detalle.fk_cpb011_num_viatico = '$idViatico' and cp_c003_viatico_detalle.fk_a003_persona_beneficiario='$idPersona'
      ");
      $consulta->execute();
      return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    public function metFirmas($idViatico,$tipo)
    {

      if(strcmp($tipo,'PR')==0){

          $consulta = $this->_db->prepare("
          SELECT
          	a003_persona.ind_cedula_documento,
            CONCAT_WS(' ',a003_persona.ind_nombre1,a003_persona.ind_nombre2,a003_persona.ind_apellido1,a003_persona.ind_apellido2) AS empleado,
          	rh_c063_puestos.ind_descripcion_cargo as cargo
          FROM
          	cp_b011_viaticos
          INNER JOIN rh_b001_empleado ON cp_b011_viaticos.fk_rhb001_num_empleado_ingresa = rh_b001_empleado.pk_num_empleado
          INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
          INNER JOIN rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado
          INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
          where cp_b011_viaticos.pk_num_viaticos='$idViatico'
          ");
          $consulta->execute();

      }

      if(strcmp($tipo,'RV')==0){

          $consulta = $this->_db->prepare("
          SELECT
            a003_persona.ind_cedula_documento,
            CONCAT_WS(' ',a003_persona.ind_nombre1,a003_persona.ind_nombre2,a003_persona.ind_apellido1,a003_persona.ind_apellido2) AS empleado,
            rh_c063_puestos.ind_descripcion_cargo as cargo
          FROM
            cp_b011_viaticos
          INNER JOIN rh_b001_empleado ON cp_b011_viaticos.fk_rhb001_num_empleado_revisa = rh_b001_empleado.pk_num_empleado
          INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
          INNER JOIN rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado
          INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
          where cp_b011_viaticos.pk_num_viaticos='$idViatico'
          ");
          $consulta->execute();

      }

      if(strcmp($tipo,'AP')==0){

          $consulta = $this->_db->prepare("
          SELECT
            a003_persona.ind_cedula_documento,
            CONCAT_WS(' ',a003_persona.ind_nombre1,a003_persona.ind_nombre2,a003_persona.ind_apellido1,a003_persona.ind_apellido2) AS empleado,
            rh_c063_puestos.ind_descripcion_cargo AS cargo
          FROM
            cp_b011_viaticos
          INNER JOIN rh_b001_empleado ON cp_b011_viaticos.fk_rhb001_num_empleado_aprueba = rh_b001_empleado.pk_num_empleado
          INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
          INNER JOIN rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado
          INNER JOIN rh_c063_puestos ON rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo = rh_c063_puestos.pk_num_puestos
          where cp_b011_viaticos.pk_num_viaticos='$idViatico'
          ");
          $consulta->execute();

      }

      return $consulta->fetch(PDO::FETCH_ASSOC);


    }

}
