<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO:Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Deivis Millan   | d.millan@contraloriamonagas.gob.ve    | 0426-9372700
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class voucherModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metVoucherListar()
    {
        $voucherListar = $this->_db->query(
            "SELECT * FROM  cp_b016_tipo_voucher
             ORDER BY cod_voucher ASC"
        );
        $voucherListar->setFetchMode(PDO::FETCH_ASSOC);
        return $voucherListar->fetchAll();
    }

    public function metVoucherVerificar($indDescripcion)
    {
        $consultaVoucher = $this->_db->query(
            "SELECT COUNT(cod_voucher) AS numero
              FROM cp_b016_tipo_voucher
              WHERE ind_descripcion='$indDescripcion'"
        );
        $consultaVoucher->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaVoucher->fetch();
    }

    function nuevoCodigo($tabla, $campo)
    {
        $consultaVoucher = $this->_db->query(
            "select max($campo) AS $campo
                            FROM $tabla");
        $consultaVoucher->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaVoucher->fetch();
    }

    public function metVoucherConsultaDetalle($idVoucher)
    {
        $consultaVoucher = $this->_db->query(
            "SELECT
              cp_b016_tipo_voucher.*,
              a018.ind_usuario
             FROM
              cp_b016_tipo_voucher
             INNER JOIN
              a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cp_b016_tipo_voucher.fk_a018_num_seguridad_usuario
             WHERE
             cp_b016_tipo_voucher.pk_num_voucher_mast = $idVoucher"
        );
        $consultaVoucher->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaVoucher->fetch();
    }

    public function metNuevoVoucher($codigo, $descripcion, $flagManual, $estado)
    {
        $this->_db->beginTransaction();
        $nuevoVoucher = $this->_db->prepare(
            "INSERT INTO
             cp_b016_tipo_voucher
              SET
               cod_voucher=:cod_voucher,
               ind_descripcion=:ind_descripcion,
               num_flag_manual=:num_flag_manual,
               num_estatus=:num_estatus,
               fec_ultima_modificacion=NOW(),
               fk_a018_num_seguridad_usuario='$this->atIdUsuario'
               ");
        $nuevoVoucher->execute(array(
            ':cod_voucher' => $codigo,
            ':ind_descripcion' => $descripcion,
            ':num_flag_manual' => $flagManual,
            ':num_estatus' => $estado
        ));
        $fallaTansaccion = $nuevoVoucher->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarVoucher($descripcion, $flagManual, $estado, $idVoucher)
    {
        $this->_db->beginTransaction();
        $modificarVoucher = $this->_db->prepare("
            UPDATE
              cp_b016_tipo_voucher
            SET
              ind_descripcion=:ind_descripcion,
              num_flag_manual=:num_flag_manual,
              num_estatus=:num_estatus,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE
              pk_num_voucher_mast='$idVoucher'
        ");
        $modificarVoucher->execute(array(
            'ind_descripcion' => $descripcion,
            'num_flag_manual' => $flagManual,
            'num_estatus' => $estado
           ));
        $fallaTansaccion = $modificarVoucher->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {

            $this->_db->commit();
        }
    }

    public function metEliminarVoucher($idVoucher){
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
           DELETE FROM cp_b016_tipo_voucher
           WHERE pk_num_voucher_mast=:pk_num_voucher_mast
            ");
        $elimar->execute(array(
            'pk_num_voucher_mast'=>$idVoucher
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idVoucher;
        }
    }
}