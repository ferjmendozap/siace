<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/

require_once 'documentoModelo.php';
require_once 'obligacionModelo.php';
require_once 'cuentasModelo.php';
require_once 'trait'.DS.'consultasTrait.php';
require_once 'trait'.DS.'updateTrait.php';
require_once RUTA_MODELO . 'miscelaneoModelo.php';


class ordenPagoModelo extends Modelo
{
    use consultasTrait;
    use updateTrait;
    private $atIdUsuario;
    private $atIdEmpleado;
    public $atDocumentosModelo;
    public $atMiscelaneoModelo;
    public $atCuentasModelo;
    public $atObligacionModelo;


    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atDocumentosModelo = new documentoModelo();
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atCuentasModelo = new cuentasModelo();
        $this->atObligacionModelo = new obligacionModelo();
    }

    public function metListarOrdenPago($estado = false)
    {
        if ($estado) {
            $where = "WHERE cp_d009_orden_pago.ind_estado = 'PE' or cp_d009_orden_pago.ind_estado = 'PP' ";
        } else {
            $where = "";
        }
        $listar = $this->_db->query(
            "SELECT
              cp_d009_orden_pago.*,
              cp_d009_orden_pago.ind_estado AS estado,
              cp_d001_obligacion.fec_documento,
              CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d001_obligacion.num_monto_obligacion, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d001_obligacion.num_monto_obligacion,2), 2), '.', -1)
              ) AS num_monto_obligacion,
              (CASE
                        WHEN cp_d009_orden_pago.ind_estado='PP' THEN 'PAGO PARCIAL'
                        WHEN cp_d009_orden_pago.ind_estado='PE' THEN 'PENDIENTE'
                        WHEN cp_d009_orden_pago.ind_estado='GE' THEN 'GENERADO'
                        WHEN cp_d009_orden_pago.ind_estado='PA' THEN 'PAGADO'
                        WHEN cp_d009_orden_pago.ind_estado='AN' THEN 'ANULADO'
              END) AS ind_estado,
              CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS fk_a003_num_persona_proveedor_a_pagar,
              a003_persona.ind_documento_fiscal

            FROM
              cp_d009_orden_pago
              INNER JOIN cp_d001_obligacion ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
              INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar = a003_persona.pk_num_persona
            $where
            ORDER BY ind_num_orden DESC 
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metListarRetencionesImpuesto($idOrdenPago)
    {
        $listar = $this->_db->query(
            "(SELECT
              cp_b015_impuesto.cod_impuesto AS CODIGO,
              ROUND(cp_b015_impuesto.num_factor_porcentaje,2) AS PORCENTAJE,
              cp_b015_impuesto.txt_descripcion AS DESCRIPCION,
              ROUND(cp_d012_obligacion_impuesto.num_monto_impuesto,2) AS num_monto_impuesto
            FROM
              cp_d009_orden_pago
              INNER JOIN cp_d001_obligacion ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
              INNER JOIN cp_d012_obligacion_impuesto ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
              INNER JOIN cp_b015_impuesto ON cp_d012_obligacion_impuesto.fk_cpb015_num_impuesto = cp_b015_impuesto.pk_num_impuesto
            WHERE
              pk_num_orden_pago='$idOrdenPago'
            )UNION(
            SELECT
              nm_b002_concepto.cod_concepto AS CODIGO,
              '' AS PORCENTAJE,
              nm_b002_concepto.ind_impresion AS DESCRIPCION,
              ROUND(cp_d012_obligacion_impuesto.num_monto_impuesto,2) AS num_monto_impuesto
            FROM
              cp_d009_orden_pago
              INNER JOIN cp_d001_obligacion ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
              INNER JOIN cp_d012_obligacion_impuesto ON cp_d001_obligacion.pk_num_obligacion = cp_d012_obligacion_impuesto.fk_cpd001_num_obligacion
              INNER JOIN nm_b002_concepto ON cp_d012_obligacion_impuesto.fk_nmb002_num_concepto = nm_b002_concepto.pk_num_concepto
            WHERE
              pk_num_orden_pago='$idOrdenPago'
            )
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metListarPartidasPresupuesto($idOrdenPago)
    {
        $listar = $this->_db->query(
            "SELECT
              pr_b002_partida_presupuestaria.cod_partida,
              pr_b002_partida_presupuestaria.ind_denominacion,
              cp_d013_obligacion_cuentas.num_monto
            FROM
              cp_d009_orden_pago
              INNER JOIN cp_d001_obligacion ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
              INNER JOIN cp_d013_obligacion_cuentas ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
              INNER JOIN pr_b002_partida_presupuestaria ON cp_d013_obligacion_cuentas.fk_prb002_num_partida_presupuestaria = pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria
            WHERE
              pk_num_orden_pago='$idOrdenPago'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metConsultarOrdenPago($idOrdenPago)
    {
        $listar = $this->_db->query(
            "SELECT
                cp_d009_orden_pago.*,
                cp_d001_obligacion.fk_a023_num_centro_de_costo AS centroCosto,
                a018_seguridad_usuario.ind_usuario,
                cp_b014_cuenta_bancaria.pk_num_cuenta,
                cp_b002_tipo_documento.num_flag_provision,
                cp_b002_tipo_documento.cod_tipo_documento,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d001_obligacion ON rh_b001_empleado.pk_num_empleado = cp_d001_obligacion.fk_rhb001_num_empleado_conforma
                        INNER JOIN cp_d009_orden_pago ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
                    WHERE
                        pk_num_orden_pago = '$idOrdenPago'
                ) AS EMPLEADO_CONFORMA,
                (
                    SELECT
                      puesto.ind_descripcion_cargo
                    FROM
                      rh_b001_empleado 
                    INNER JOIN a003_persona AS persona ON persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                    INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                    INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
                    INNER JOIN rh_c006_tipo_cargo AS cargo ON cargo.pk_num_cargo = puesto.fk_rhc006_num_cargo
                    INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.fk_rhb001_num_empleado_conforma = rh_b001_empleado.pk_num_empleado
                    INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                    WHERE
                      pk_num_orden_pago = '$idOrdenPago'
                ) AS CARGO_CONFORMA,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN cp_d001_obligacion ON rh_b001_empleado.pk_num_empleado = cp_d001_obligacion.fk_rhb001_num_empleado_aprueba
                        INNER JOIN cp_d009_orden_pago ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
                    WHERE
                        pk_num_orden_pago = '$idOrdenPago'
                ) AS EMPLEADO_APRUEBA,
                (
                    SELECT
                      puesto.ind_descripcion_cargo
                    FROM
                      rh_b001_empleado 
                    INNER JOIN a003_persona AS persona ON persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                    INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                    INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
                    INNER JOIN rh_c006_tipo_cargo AS cargo ON cargo.pk_num_cargo = puesto.fk_rhc006_num_cargo
                    INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.fk_rhb001_num_empleado_aprueba = rh_b001_empleado.pk_num_empleado
                    INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
                    WHERE
                      pk_num_orden_pago = '$idOrdenPago'
                ) AS CARGO_APRUEBA,
                cp_d001_obligacion.fec_conformado,
                cp_d001_obligacion.fec_aprobado,
                (CASE
                        WHEN cp_d009_orden_pago.ind_estado='PE' THEN 'PENDIENTE'
                        WHEN cp_d009_orden_pago.ind_estado='GE' THEN 'GENERADO'
                        WHEN cp_d009_orden_pago.ind_estado='PA' THEN 'PAGADO'
                        WHEN cp_d009_orden_pago.ind_estado='AN' THEN 'ANULADO'
                END) AS ind_estado,
                cp_d001_obligacion.num_monto_obligacion,
                cp_d001_obligacion.num_monto_adelanto,
                cp_d001_obligacion.num_monto_pago_parcial,
                cp_d001_obligacion.num_monto_impuesto_otros,
                cp_d001_obligacion.num_monto_impuesto,
                cp_d001_obligacion.num_monto_afecto,
                cp_d001_obligacion.num_monto_no_afecto,
                cp_d001_obligacion.ind_comentarios,
                cp_d001_obligacion.fec_documento,
                CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS fk_a003_num_persona_proveedor_a_pagar,
                CONCAT_WS(' ',a003_persona_proveedor.ind_nombre1, a003_persona_proveedor.ind_apellido1) AS fk_a003_num_persona_proveedor,
                a003_persona.pk_num_persona,
                a003_persona.ind_documento_fiscal AS ind_documento_fiscal_pagar,
                a003_persona_proveedor.ind_documento_fiscal,
                a006_miscelaneo_detalle_tipo_pago.ind_nombre_detalle AS a006_miscelaneo_detalle_tipo_pago,
                cp_b014_cuenta_bancaria.ind_num_cuenta,
                cp_d009_orden_pago.fk_cpd001_num_obligacion,
                cp_d009_orden_pago.ind_num_orden,
                date_format(cp_d009_orden_pago.fec_orden_pago,'%d-%m-%Y') AS fec_orden_pago,
                a006_miscelaneo_detalle_banco.ind_nombre_detalle AS a006_miscelaneo_detalle_banco,
                cp_d001_obligacion.num_monto_descuento,
                cp_d001_obligacion.ind_tipo_descuento,
                cp_d001_obligacion.pk_num_obligacion

            FROM
                cp_d009_orden_pago
                INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
                INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cp_d009_orden_pago.fk_a018_num_seguridad_usuario
                INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar = a003_persona.pk_num_persona
                INNER JOIN a003_persona AS a003_persona_proveedor ON cp_d001_obligacion.fk_a003_num_persona_proveedor = a003_persona_proveedor.pk_num_persona
                INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_tipo_pago ON cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = a006_miscelaneo_detalle_tipo_pago.pk_num_miscelaneo_detalle
                INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
                INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_banco ON cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco = a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle
                INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento

            WHERE
              cp_d009_orden_pago.pk_num_orden_pago = '$idOrdenPago'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metConsultarOrdenPagoPP($idOrdenPago)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d009_orden_pago.*,
              cp_d010_pago.pk_num_pago,
              cp_d010_pago.num_monto_pago,
              cp_d010_pago.ind_num_pago,
              cp_d010_pago.fec_pago,
              (CASE
                    WHEN cp_d010_pago.ind_estado='GE' THEN 'GENERADO'
                    WHEN cp_d010_pago.ind_estado='IM' THEN 'IMPRESO'
                    WHEN cp_d010_pago.ind_estado='AN' THEN 'ANULADO'
              END) AS ind_estado_pago

            FROM
              cp_d009_orden_pago
            INNER JOIN cp_d010_pago ON cp_d010_pago.fk_cpd009_num_orden_pago = cp_d009_orden_pago.pk_num_orden_pago
            WHERE
              cp_d009_orden_pago.pk_num_orden_pago = '$idOrdenPago'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metConsultarPagoPP($idOrdenPago,$partida)
    {
        $listar = $this->_db->query(  "SELECT 
            SUM(num_monto) AS num_monto 
            FROM cp_d015_orden_pago_parcial
            INNER JOIN cp_d010_pago ON cp_d010_pago.pk_num_pago = cp_d015_orden_pago_parcial.fk_cpd010_num_pago
            WHERE 
            cp_d015_orden_pago_parcial.fk_cpd009_num_orden_pago = '$idOrdenPago' 
            AND fk_prb002_num_partida_presupuestaria = '$partida' 
            AND cp_d010_pago.ind_estado != 'AN'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metConsultarOrdenPagoCuenta($idOrdenPago)
    {
        $listar = $this->_db->query(
            "SELECT
              SUM(cp_d001_obligacion.num_monto_obligacion) AS montoTotal,
              cp_d009_orden_pago.*,
              cp_d009_orden_pago.ind_estado AS estado,
              cp_d001_obligacion.ind_nro_control,
              cp_d001_obligacion.fec_documento,

              (CASE
                        WHEN cp_d009_orden_pago.ind_estado='PE' THEN 'PENDIENTE'
                        WHEN cp_d009_orden_pago.ind_estado='GE' THEN 'GENERADO'
                        WHEN cp_d009_orden_pago.ind_estado='PA' THEN 'PAGADO'
                        WHEN cp_d009_orden_pago.ind_estado='AN' THEN 'ANULADO'
              END) AS ind_estado,
              CONCAT(
                    IF(a003_persona.ind_nombre1,'',a003_persona.ind_nombre1),
                    ' ',
                    IF(a003_persona.ind_apellido1,'',a003_persona.ind_apellido1)
              ) AS fk_a003_num_persona_proveedor_a_pagar,
              CONCAT(
                    IF(a003_persona_proveedor.ind_nombre1,'',a003_persona_proveedor.ind_nombre1),
                    ' ',
                    IF(a003_persona_proveedor.ind_apellido1,'',a003_persona_proveedor.ind_apellido1)
              ) AS fk_a003_num_persona_proveedor,
              a003_persona.ind_documento_fiscal,
              a006_miscelaneo_detalle_banco.ind_nombre_detalle AS a006_miscelaneo_detalle_banco,
              a006_miscelaneo_detalle_tipo_pago.ind_nombre_detalle AS a006_miscelaneo_detalle_tipo_pago,
              cp_b014_cuenta_bancaria.pk_num_cuenta,
              cp_b014_cuenta_bancaria.ind_num_cuenta

            FROM
              cp_d009_orden_pago
              INNER JOIN cp_d001_obligacion ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
              INNER JOIN cp_b014_cuenta_bancaria ON cp_d001_obligacion.fk_cpb014_num_cuenta = cp_b014_cuenta_bancaria.pk_num_cuenta
              INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar = a003_persona.pk_num_persona
              INNER JOIN a003_persona AS a003_persona_proveedor ON cp_d001_obligacion.fk_a003_num_persona_proveedor = a003_persona_proveedor.pk_num_persona
              INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_banco ON cp_b014_cuenta_bancaria.fk_a006_num_miscelaneo_detalle_banco = a006_miscelaneo_detalle_banco.pk_num_miscelaneo_detalle
              INNER JOIN a006_miscelaneo_detalle AS a006_miscelaneo_detalle_tipo_pago ON cp_d001_obligacion.fk_a006_num_miscelaneo_tipo_pago = a006_miscelaneo_detalle_tipo_pago.pk_num_miscelaneo_detalle
              INNER JOIN cp_b002_tipo_documento ON cp_b002_tipo_documento.pk_num_tipo_documento = cp_d001_obligacion.fk_cpb002_num_tipo_documento

            WHERE
            cp_d009_orden_pago.pk_num_orden_pago = '$idOrdenPago'
            ");
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metConsultarOrdenPagoPartida($idOrdenPago)
    {
        $listar = $this->_db->query("
            SELECT
              cp_d013_obligacion_cuentas.*,
              cuenta.pk_num_cuenta AS pkCuenta,
              cuenta.cod_cuenta AS cod_cuenta,
              cuenta20.pk_num_cuenta AS pkCuenta20,
              cuenta20.cod_cuenta AS cod_cuenta20,
              pr_b002_partida_presupuestaria.cod_partida,
              pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria AS pkPartida
            FROM
              cp_d013_obligacion_cuentas
            INNER JOIN
              cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d013_obligacion_cuentas.fk_cpd001_num_obligacion
            INNER JOIN
              cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
            LEFT JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS cuenta20 ON cuenta20.pk_num_cuenta = cp_d013_obligacion_cuentas.fk_cbb004_num_cuenta_pub20
            LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria = cp_d013_obligacion_cuentas.fk_prb002_num_partida_presupuestaria
            WHERE
              cp_d009_orden_pago.pk_num_orden_pago = '$idOrdenPago'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetchAll();
    }

    public function metConsultarOrdenPagoImpuesto($idOrdenPago)
    {
        $codImpuestoIva = Session::metObtener('IVGCODIGO');
        $listar = $this->_db->query("
            SELECT
              cp_d001_obligacion.num_monto_impuesto as num_monto,
             (
                      SELECT
                        cb_b004_plan_cuenta.cod_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS cod_cuenta20,
                    (
                      SELECT
                        cb_b004_plan_cuenta.pk_num_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta_pub20 = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS pkCuenta20,
                    (
                      SELECT
                        cb_b004_plan_cuenta.cod_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS cod_cuenta,
                    (
                      SELECT
                        cb_b004_plan_cuenta.pk_num_cuenta
                      FROM
                        cp_b015_impuesto
                        INNER JOIN cb_b004_plan_cuenta ON cp_b015_impuesto.fk_cbb004_num_plan_cuenta = cb_b004_plan_cuenta.pk_num_cuenta
                      WHERE
                        cod_impuesto='$codImpuestoIva'
                    ) AS pkCuenta
                        
            FROM
              cp_d001_obligacion
            INNER JOIN
              cp_d009_orden_pago ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
            WHERE
              cp_d009_orden_pago.pk_num_orden_pago = '$idOrdenPago'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metAnularOrdenPago($idOrdenPago, $motivo)
    {
        $this->_db->beginTransaction();
        $ordenPago = $this->metConsultarOrdenPago($idOrdenPago);
        $obligacion = $this->atObligacionModelo->metConsultaObligacion($ordenPago['fk_cpd001_num_obligacion']);

        $anularOrdenPago=$this->metAnularRechazarOrdenPago($idOrdenPago,$motivo);
        $reversarObligacion=$this->metAnularRechazarObligacion($ordenPago['fk_cpd001_num_obligacion'],false,'orden');
        if(Session::metObtener('CONTABILIDAD') == 'ONCOP'){
            $anularEstadoDistribucion=$this->metAnularEstadoDistribucion($ordenPago['fk_cpd001_num_obligacion'], 'CA', false,true);
        }else{
            $anularEstadoDistribucion='';
        }
        if ($anularOrdenPago) {
            $this->_db->rollBack();
            return $anularOrdenPago;
        } elseif ($reversarObligacion) {
            $this->_db->rollBack();
            return $reversarObligacion;
        } elseif ($anularEstadoDistribucion) {
            $this->_db->rollBack();
            return $anularEstadoDistribucion;
        } else {
            $this->_db->commit();
            return $idOrdenPago;
        }
    }

    public function metGenerarOrdenPago($idOrdenPago)
    {
        $this->_db->beginTransaction();
        $generarOrdenPago = $this->_db->prepare("
            UPDATE
                cp_d009_orden_pago
            SET
                fec_generado=NOW(),
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_rhb001_num_empleado_genera='$this->atIdEmpleado',
                ind_estado='GE'
            WHERE
              pk_num_orden_pago=:pk_num_orden_pago
        ");
        $generarOrdenPago->execute(array(
            'pk_num_orden_pago' => $idOrdenPago
        ));

        $secuencia = $this->metSecuencia('cp_d010_pago', 'ind_num_pago');
        $registrarPago = $this->_db->prepare("
            INSERT INTO
                cp_d010_pago
            SET
                fk_cpd009_num_orden_pago='$idOrdenPago',
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_rhb001_num_empleado_crea='$this->atIdEmpleado',
                ind_num_pago=LPAD( '" . $secuencia['secuencia'] . "',6,'0'),
                ind_estado='GE',
                fec_anio=YEAR(NOW()),
                fec_mes= LPAD( MONTH (NOW()),2,'0'),
                fec_pago=NOW(),
                fec_ultima_modificacion=NOW(),
                num_monto_pago=:num_monto_pago,
                num_monto_retenido=:num_monto_retenido
        ");
        $ordenPago = $this->metConsultarOrdenPago($idOrdenPago);
        $registrarPago->execute(array(
            'num_monto_pago' => $ordenPago['num_monto_obligacion'],
            'num_monto_retenido' => $ordenPago['num_monto_impuesto_otros']
        ));

        $error1 = $generarOrdenPago->errorInfo();
        $error2 = $registrarPago->errorInfo();

        if ((!empty($error1[1]) && !empty($error1[2]))) {
            $this->_db->rollBack();
            return $error1;
        } elseif ((!empty($error2[1]) && !empty($error2[2]))) {
            $this->_db->rollBack();
            return $error2;
        } else {
            $this->_db->commit();
            return $idOrdenPago . '-' . str_pad($secuencia['secuencia'], 6, "0", STR_PAD_LEFT);;
        }
    }

    public function metGenerarOrdenPagoParcial($idOrdenPago,$montoPagado,$estado)
    {
        $this->_db->beginTransaction();
        $generarOrdenPago = $this->_db->prepare("
            UPDATE
                cp_d009_orden_pago
            SET
                fec_generado=NOW(),
                fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_rhb001_num_empleado_genera='$this->atIdEmpleado',
                ind_estado='$estado'
            WHERE
              pk_num_orden_pago=:pk_num_orden_pago
        ");
        $generarOrdenPago->execute(array(
            'pk_num_orden_pago' => $idOrdenPago
        ));

        $secuencia = $this->metSecuencia('cp_d010_pago', 'ind_num_pago');
        $registrarPago = $this->_db->prepare("
            INSERT INTO
                cp_d010_pago
            SET
                fk_cpd009_num_orden_pago='$idOrdenPago',
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fk_rhb001_num_empleado_crea='$this->atIdEmpleado',
                ind_num_pago=LPAD( '" . $secuencia['secuencia'] . "',6,'0'),
                ind_estado='GE',
                fec_anio=YEAR(NOW()),
                fec_mes= LPAD( MONTH (NOW()),2,'0'),
                fec_pago=NOW(),
                fec_ultima_modificacion=NOW(),
                num_monto_pago=:num_monto_pago,
                num_monto_retenido=:num_monto_retenido
        ");
        $ordenPago = $this->metConsultarOrdenPago($idOrdenPago);
        $registrarPago->execute(array(
            'num_monto_pago' => $montoPagado,
            'num_monto_retenido' => $ordenPago['num_monto_impuesto_otros']
        ));
        $idPago = $this->_db->lastInsertId();
        $error1 = $generarOrdenPago->errorInfo();
        $error2 = $registrarPago->errorInfo();

        if ((!empty($error1[1]) && !empty($error1[2]))) {
            $this->_db->rollBack();
            return $error1;
        } elseif ((!empty($error2[1]) && !empty($error2[2]))) {
            $this->_db->rollBack();
            return $error2;
        } else {
            $this->_db->commit();
            return $idOrdenPago . '-' . str_pad($secuencia['secuencia'], 6, "0", STR_PAD_LEFT).'-'.$idPago;
        }
    }

    public function metDetallePagoParcial($idOrdenPago,$form,$idPago)
    {
        $this->_db->beginTransaction();
        $registrarPagoParcial = $this->_db->prepare("
            INSERT INTO
                cp_d015_orden_pago_parcial
            SET
                fk_cpd009_num_orden_pago='$idOrdenPago',
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_cbb004_num_cuenta=:fk_cbb004_num_cuenta,
                fk_cbb004_num_cuenta_pub20=:fk_cbb004_num_cuenta_pub20,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                num_monto=:num_monto,
                num_secuencia_pago=:num_secuencia_pago,
                fk_cpd010_num_pago=:fk_cpd010_num_pago,
                num_estatus=:num_estatus
        ");
        $detalle=$form['detalle'];
        $secuencia=1;
        foreach($detalle['num_secuencia'] AS $i) {
            $ids=$detalle[$i];
            $registrarPagoParcial->execute(array(
                'fk_cbb004_num_cuenta' => $ids['fk_cbb004_num_cuenta'],
                'fk_cbb004_num_cuenta_pub20' => $ids['fk_cbb004_num_cuenta_pub20'],
                'fk_prb002_num_partida_presupuestaria' => $ids['fk_prb002_num_partida_presupuestaria'],
                'num_monto' => $ids['num_monto'],
                'num_secuencia_pago' => $form['num_secuencia_pago'],
                'fk_cpd010_num_pago' => $idPago,
                'num_estatus' => 0
            ));
            $secuencia++;
        }
        $idPagoParcial = $this->_db->lastInsertId();

        $error1 = $registrarPagoParcial->errorInfo();

        if ((!empty($error1[1]) && !empty($error1[2]))) {
            $this->_db->rollBack();
            return $error1;
        } else {
            $this->_db->commit();
            return $idPagoParcial;
        }
    }

    public function metDispFinanciera($idOrdenPago)
    {
        $listar = $this->_db->query(
            "SELECT
              cp_d009_orden_pago.*,
              SUM(cp_d001_obligacion.num_monto_obligacion) AS montoTotal,
              (cp_d006_cuenta_bancaria_balance.num_saldo_actual) AS montoActual
            FROM
              cp_d009_orden_pago
              INNER JOIN cp_d001_obligacion ON cp_d009_orden_pago.fk_cpd001_num_obligacion = cp_d001_obligacion.pk_num_obligacion
              INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor_a_pagar = a003_persona.pk_num_persona
              INNER JOIN cp_d006_cuenta_bancaria_balance ON cp_d006_cuenta_bancaria_balance.fk_cpb014_pk_num_cuenta_bancaria = cp_d001_obligacion.fk_cpb014_num_cuenta
            WHERE
              cp_d009_orden_pago.pk_num_orden_pago = '$idOrdenPago'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metPagoPendiente($idCuenta)
    {
        $listar = $this->_db->query(
            "SELECT
             SUM(cp_d010_pago.num_monto_pago) AS montoPendiente

            FROM
              cp_d010_pago
             INNER JOIN cp_d009_orden_pago ON cp_d009_orden_pago.pk_num_orden_pago = cp_d010_pago.fk_cpd009_num_orden_pago
             INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
             INNER JOIN cp_b014_cuenta_bancaria ON cp_b014_cuenta_bancaria.pk_num_cuenta = cp_d001_obligacion.fk_cpb014_num_cuenta

            WHERE
             cp_d010_pago.ind_estado = 'GE' and
             cp_b014_cuenta_bancaria.pk_num_cuenta = '$idCuenta'
            ");
        //var_dump($listar);
        $listar->setFetchMode(PDO::FETCH_ASSOC);
        return $listar->fetch();
    }

    public function metAnularVoucher($idVoucherPago)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "UPDATE
              cb_b001_voucher
              SET
                fk_a003_num_anulado_por='$this->atIdUsuario',
                fec_fecha_anulacion=NOW(),
                txt_motivo_anulacion='Anular',
                ind_estatus='AN'
                WHERE
                pk_num_voucher_mast=:pk_num_voucher_mast
               ");
        $NuevoRegistro->execute(array(
            ':pk_num_voucher_mast' => $idVoucherPago
        ));
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $idRegistro= $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }


    }

    public function metBuscarIva($idOrdenPago){
        $persona = $this->_db->query("
          SELECT
             cp_d001_obligacion.num_monto_impuesto
          FROM
            cp_d009_orden_pago
          INNER JOIN cp_d001_obligacion ON cp_d001_obligacion.pk_num_obligacion = cp_d009_orden_pago.fk_cpd001_num_obligacion
          WHERE
            cp_d009_orden_pago.pk_num_orden_pago = '$idOrdenPago'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();


    }
}
