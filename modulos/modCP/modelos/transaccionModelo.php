<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO:Prueba
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_MODELO.'miscelaneoModelo.php';
require_once 'impuestoModelo.php';
require_once RUTA_Modulo . 'modCB' . DS . 'modelos' . DS . 'TipoVoucherModelo.php';

class transaccionModelo extends Modelo
{
    private $atIdUsuario;
    public $atMiscelaneoModelo;
    public $atImpuestoModelo;
    public $atVoucherModelo;


    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
        $this->atImpuestoModelo=new impuestoModelo();
        $this->atTipoVoucherModelo = new TipoVoucherModelo();
    }
    public function metTransaccionListar($idTransaccion = false)
    {
        if($idTransaccion){
            $where = "WHERE cp_b006_banco_tipo_transaccion.pk_num_banco_tipo_transaccion = '$idTransaccion'";
        }else{
            $where = "";
        }
        $transaccionListar =  $this->_db->query(
            "SELECT
              cp_b006_banco_tipo_transaccion.*,
              a006_miscelaneo_detalle.ind_nombre_detalle,
              cb_b004_plan_cuenta.cod_cuenta
            FROM
            cp_b006_banco_tipo_transaccion
             INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
             LEFT JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
              $where
             ORDER BY cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion ASC"
        );
        //var_dump($transaccionListar);
        $transaccionListar->setFetchMode(PDO::FETCH_ASSOC);
        if($idTransaccion){
            return $transaccionListar->fetch();
        }else{
            return $transaccionListar->fetchAll();
        }

    }
    public function metTransaccionConsultaDetalle($idTransaccion)
    {
        $consultaTransaccion = $this->_db->query(
            "SELECT
            cp_b006_banco_tipo_transaccion.*,
            a018.ind_usuario,
            a006_miscelaneo_detalle.ind_nombre_detalle,
            cb_b004_plan_cuenta.cod_cuenta,
            cb_b004_plan_cuentaPub20.cod_cuenta AS cod_cuentaPub20
            FROM
            cp_b006_banco_tipo_transaccion
            INNER JOIN a018_seguridad_usuario AS a018 ON a018.pk_num_seguridad_usuario = cp_b006_banco_tipo_transaccion.fk_a018_num_seguridad_usuario
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = cp_b006_banco_tipo_transaccion.fk_a006_miscelaneo_detalle_tipo_transaccion
            LEFT JOIN cb_b004_plan_cuenta ON cb_b004_plan_cuenta.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS cb_b004_plan_cuentaPub20 ON cb_b004_plan_cuentaPub20.pk_num_cuenta = cp_b006_banco_tipo_transaccion.fk_cbb004_num_plan_cuenta_pub20
            WHERE
            pk_num_banco_tipo_transaccion = $idTransaccion"
        );
        $consultaTransaccion->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaTransaccion->fetch();
    }
    public function metNuevaTransaccionB($form)
    {
        $this->_db->beginTransaction();
        $nuevaTransaccion = $this->_db->prepare(
            "INSERT INTO
             cp_b006_banco_tipo_transaccion
              SET
               cod_tipo_transaccion=:cod_tipo_transaccion,
               ind_descripcion=:ind_descripcion,
               fk_a006_miscelaneo_detalle_tipo_transaccion=:fk_a006_miscelaneo_detalle_tipo_transaccion,
               num_flag_voucher=:num_flag_voucher,
               fk_cbc003_tipo_voucher=:fk_cbc003_tipo_voucher,
               fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
               fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
               num_flag_transaccion=:num_flag_transaccion,
               num_estatus=:num_estatus,
               fec_ultima_modificacion=NOW(),
               fk_a018_num_seguridad_usuario='$this->atIdUsuario'
               ");
        $nuevaTransaccion->execute(array(
            'cod_tipo_transaccion' => $form['cod_tipo_transaccion'],
            'ind_descripcion' => $form['ind_descripcion'],
            'fk_a006_miscelaneo_detalle_tipo_transaccion' => $form['fk_a006_miscelaneo_detalle_tipo_transaccion'],
            'num_flag_voucher' => $form['num_flag_voucher'],
            'fk_cbc003_tipo_voucher' => $form['fk_cbc003_tipo_voucher'],
            'fk_cbb004_num_plan_cuenta' => $form['fk_cbb004_num_plan_cuenta'],
            'fk_cbb004_num_plan_cuenta_pub20' => $form['fk_cbb004_num_plan_cuenta_pub20'],
            'num_flag_transaccion' => $form['num_flag_transaccion'],
            'num_estatus' => $form['num_estatus'],
        ));
        $fallaTansaccion = $nuevaTransaccion->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }
    public function metModificarTransaccionB($idTransaccion, $form)
    {
        $this->_db->beginTransaction();
        $modificarTransaccion = $this->_db->prepare("
            UPDATE
              cp_b006_banco_tipo_transaccion
            SET
              ind_descripcion=:ind_descripcion,
               fk_a006_miscelaneo_detalle_tipo_transaccion=:fk_a006_miscelaneo_detalle_tipo_transaccion,
               num_flag_voucher=:num_flag_voucher,
               fk_cbc003_tipo_voucher=:fk_cbc003_tipo_voucher,
               fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
               fk_cbb004_num_plan_cuenta_pub20=:fk_cbb004_num_plan_cuenta_pub20,
               num_flag_transaccion=:num_flag_transaccion,
               num_estatus=:num_estatus,
               fec_ultima_modificacion=NOW(),
               fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE
              pk_num_banco_tipo_transaccion='$idTransaccion'
        ");
        $modificarTransaccion->execute(array(
            'ind_descripcion' => $form['ind_descripcion'],
            'fk_a006_miscelaneo_detalle_tipo_transaccion' => $form['fk_a006_miscelaneo_detalle_tipo_transaccion'],
            'num_flag_voucher' => $form['num_flag_voucher'],
            'fk_cbc003_tipo_voucher' => $form['fk_cbc003_tipo_voucher'],
            'fk_cbb004_num_plan_cuenta' => $form['fk_cbb004_num_plan_cuenta'],
            'fk_cbb004_num_plan_cuenta_pub20' => $form['fk_cbb004_num_plan_cuenta_pub20'],
            'num_flag_transaccion' => $form['num_flag_transaccion'],
            'num_estatus' => $form['num_estatus'],
        ));
        $fallaTansaccion = $modificarTransaccion->errorInfo();

        if (!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])) {
            $this->_db->rollBack();
            return $fallaTansaccion;
        } else {

            $this->_db->commit();
        }
    }

    public function metEliminarTransaccion($idTransaccion){
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
           DELETE FROM cp_b006_banco_tipo_transaccion
           WHERE pk_num_banco_tipo_transaccion=:pk_num_banco_tipo_transaccion
            ");
        $elimar->execute(array(
            'pk_num_banco_tipo_transaccion'=>$idTransaccion
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idTransaccion;
        }
    }


}
