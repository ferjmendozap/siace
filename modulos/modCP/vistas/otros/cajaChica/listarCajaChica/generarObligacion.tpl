<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form action="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/nuevaObligacionMET" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="cajaChica" name="tipo"/>
                            <input type="hidden" value="{$idObligacion}" id="idObligacion" name="idObligacion"/>
                            <input type="hidden" value="{$idCajaChica}" id="idCajaChica" name="idCajaChica"/>

                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#informacionGeneral" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span></a></li>
                                    <li><a href="#informacionMonetaria" data-toggle="tab"><span class="step">2</span> <span class="title">INFORMACIÓN MONETARIA</span></a></li>
                                    <li><a href="#cuentaContablePresupuestaria" data-toggle="tab"><span class="step">3</span> <span class="title">DIST. CONTABLE Y PRESUP. </span></a></li>
                                    <li><a href="#resumenCuentaContablePresupuestaria" id="ajaxDisponibilidad" data-toggle="tab"><span class="step">4</span> <span class="title"><BR>RESU.CONTABLE Y PRESUP.</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="informacionGeneral">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">Información del Proveedor</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fk_a003_num_persona_proveedor"
                                                                       class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       name="form[int][fk_a003_num_persona_proveedor]"
                                                                       class="form-control" id="codigoProveedor"
                                                                       value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.fk_a003_num_persona_proveedor}{/if}"
                                                                       readonly size="9%">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control"
                                                                           id="nombreProveedor"
                                                                               value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.proveedor}{/if}"
                                                                           disabled>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group floating-label">
                                                                        <button
                                                                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                                type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#formModal2"
                                                                                titulo="Listado de Personas"
                                                                                {if isset($ver) and $ver==1}disabled{/if}
                                                                                url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                                                            <i class="md md-search"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fk_a003_num_persona_proveedor"
                                                                       class="control-label" style="margin-top: 10px;"> Pagar a:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       name="form[int][fk_a003_num_persona_proveedor_a_pagar]"
                                                                       value="{if isset($obligacionBD.fk_a003_num_persona_proveedor_a_pagar)}{$obligacionBD.fk_a003_num_persona_proveedor_a_pagar}{/if}"
                                                                       class="form-control"
                                                                       id="codigoProveedorPagar" readonly size="9%">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control"
                                                                           id="nombreProveedorPagar"
                                                                           value="{if isset($obligacionBD.fk_a003_num_persona_proveedor_a_pagar)}{$obligacionBD.proveedorPagar}{/if}"
                                                                           disabled>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group floating-label">
                                                                        <button
                                                                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                                type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#formModal2"
                                                                                titulo="Listado de Personas"
                                                                                {if isset($ver) and $ver==1}disabled{/if}
                                                                                url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/pagarA/">
                                                                            <i class="md md-search"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fk_a003_num_persona_proveedor"
                                                                       class="control-label" style="margin-top: 10px;"> RIF:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text" class="form-control"
                                                                       id="documentoProveedor"
                                                                       value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.documentoFiscal}{/if}"
                                                                       disabled>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control"
                                                                           id="nombreProveedor2"
                                                                           value="{if isset($obligacionBD.fk_a003_num_persona_proveedor)}{$obligacionBD.proveedor}{/if}"
                                                                           disabled>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-7">
                                                                <div class="col-sm-5 text-right">
                                                                    <label for="fk_a023_num_centro_de_costo"
                                                                           class="control-label"  style="margin-top: 10px;">Centro de
                                                                        Costo:</label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group" id="fk_a023_num_centro_de_costoError">
                                                                        <input type="text" class="form-control" value="{if isset($obligacionBD.fk_a023_num_centro_de_costo)}{$obligacionBD.fk_a023_num_centro_de_costo}{/if}" name="form[int][fk_a023_num_centro_de_costo]" id="fk_a023_num_centro_de_costo" readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group floating-label">
                                                                        <button
                                                                                type="button"
                                                                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                                data-toggle="modal"
                                                                                data-target="#formModal2"
                                                                                titulo="Listado Centro de Costo"
                                                                                {if isset($ver) and $ver==1}disabled{/if}
                                                                                url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET">
                                                                            <i class="md md-search"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-5" style="margin-left: -40px;">
                                                                <div class="col-sm-5">
                                                                    <label class="control-label"  style="margin-top: 10px;">Días de Pago:</label>
                                                                </div>
                                                                <div class="col-sm-5">
                                                                    <input type="text" class="form-control"
                                                                           id="dias"
                                                                           {if isset($ver) and $ver==1}disabled{/if}>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fk_cpb002_num_tipo_documento"
                                                                       class="control-label" style="margin-top: 10px;"> Tipo de Documento:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group"
                                                                     id="fk_cpb002_num_tipo_documentoError" style="margin-top: -10px;">
                                                                    <select name="form[int][fk_cpb002_num_tipo_documento]"
                                                                            class="form-control select2" id="fk_cpb002_num_tipo_documento"
                                                                            data-placeholder="Seleccione el Tipo de Documento"
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=proceso from=$listadoDocumento}
                                                                            {if isset($obligacionBD.fk_cpb002_num_tipo_documento) and $obligacionBD.fk_cpb002_num_tipo_documento == $proceso.pk_num_tipo_documento }
                                                                                <option value="{$proceso.pk_num_tipo_documento}"
                                                                                        selected>{$proceso.ind_descripcion}</option>
                                                                            {else}
                                                                                <option value="{$proceso.pk_num_tipo_documento}">{$proceso.ind_descripcion}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-7">
                                                                <div class="col-sm-5 text-right">
                                                                    <label for="ind_nro_registro"
                                                                           class="control-label" style="margin-top: 10px;"> Nro.
                                                                        Registro:</label>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group" id="ind_nro_registroError">
                                                                        <label for="ind_nro_registro"><i class="fa fa-list-alt"></i></label>
                                                                        <input type="text" class="form-control" value="{if isset($obligacionBD.ind_nro_registro)}{$obligacionBD.ind_nro_registro}{/if}" name="form[int][ind_nro_registro]" id="ind_nro_registro" disabled>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-7">
                                                                <div class="col-sm-3 text-right">
                                                                    <label for="ind_nro_control" class="control-label" style="margin-top: 10px;">
                                                                        Nro. Control:</label>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <div class="form-group" id="ind_nro_controlError">
                                                                        <label for="ind_nro_control"><i class="fa fa-list-alt"></i></label>
                                                                        <input type="text" class="form-control" value="{if isset($obligacionBD.ind_nro_control)}{$obligacionBD.ind_nro_control}{/if}" name="form[alphaNum][ind_nro_control]" id="ind_nro_control" {if isset($ver) and $ver==1}readonly{/if}>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-7">
                                                                <div class="col-sm-5 text-right">
                                                                    <label for="ind_nro_factura" class="control-label" style="margin-top: 10px;">
                                                                        Nro. Factura:</label>
                                                                </div>
                                                                <div class="col-sm-7">
                                                                    <div class="form-group" id="ind_nro_facturaError">
                                                                        <label for="ind_nro_factura"><i class="fa fa-list-alt"></i></label>
                                                                        <input type="text" class="form-control" value="{if isset($obligacionBD.ind_nro_factura)}{$obligacionBD.ind_nro_factura}{/if}" name="form[alphaNum][ind_nro_factura]" id="ind_nro_factura" {if isset($ver) and $ver==1}readonly{/if}>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="ind_estado" class="control-label" style="margin-top: 10px;">
                                                                    Estado:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <input type="text" id="ind_estado" class="form-control"
                                                                       style="font-weight:bold;"
                                                                       readonly {if !isset($obligacionBD.ind_estado)} placeholder="En Preparacion" {else} placeholder="En Revision" {/if} >
                                                                <label for="ind_estado" class="control-label"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-7">
                                                                <div class="col-sm-5 text-right">
                                                                    <label for="fec_factura" class="control-label" style="margin-top: 10px;">
                                                                        Fecha Factura:</label>
                                                                </div>
                                                                <div class="col-sm-7" style="margin-top: -10px;">
                                                                    <div class="form-group control-width-normal">
                                                                        <div class="input-group date" id="fec_factura">
                                                                            <div class="input-group-content text-center">
                                                                                <input type="text"
                                                                                       name="form[txt][fec_factura]"
                                                                                       id="datepicker" class="form-control"
                                                                                       value="{if isset($obligacionBD.fec_factura)}{$obligacionBD.fec_factura}{else}{date('Y-m-d')}{/if}"
                                                                                       readonly >
                                                                            </div>
                                                                        <span class="input-group-addon"><i
                                                                                    class="fa fa-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary">
                                                    <header>Fechas del Documento</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-4">
                                                            <div class="form-group control-width-normal">
                                                                <div class="input-group date" id="demo-date">
                                                                    <div class="input-group-content">
                                                                        <label>Fecha Registro:</label>
                                                                        <input type="text"
                                                                               name="form[txt][fec_registro]"
                                                                               id="fec_registro"
                                                                               class="form-control"
                                                                               {if isset($ver) and $ver==1}readonly{/if}
                                                                               value="{if isset($obligacionBD.fec_registro)}{$obligacionBD.fec_registro}{else}{date('Y-m-d')}{/if}">
                                                                    </div>
                                                                    <span class="input-group-addon"><i
                                                                                class="fa fa-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group control-width-normal">
                                                                <div class="input-group date"
                                                                     id="demo-date-emision">
                                                                    <div class="input-group-content">
                                                                        <label>Fecha Emisión</label>
                                                                        <input type="text"
                                                                               name="form[txt][FechaEmisions]"
                                                                               class="form-control"
                                                                               {if isset($ver) and $ver==1}disabled{/if}
                                                                               value="{if isset($obligacionBD.FechaEmisions)}{$obligacionBD.FechaEmisions}{else}{date('Y-m-d')}{/if}">
                                                                    </div>
                                                                    <span class="input-group-addon"><i
                                                                                class="fa fa-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group control-width-normal">
                                                                <div class="input-group date"
                                                                     id="demo-date-recepcion">
                                                                    <div class="input-group-content">
                                                                        <label>Fecha Recepción</label>
                                                                        <input type="text"
                                                                               name="form[txt][fec_recepcion]"
                                                                               id="fec_recepcion"
                                                                               class="form-control"
                                                                               {if isset($ver) and $ver==1}readonly{/if}
                                                                               value="{if isset($obligacionBD.fec_recepcion)}{$obligacionBD.fec_recepcion}{else}{date('Y-m-d')}{/if}">
                                                                    </div>
                                                                    <span class="input-group-addon"><i
                                                                                class="fa fa-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group control-width-normal">
                                                                <div class="input-group date"
                                                                     id="demo-date-vencimiento">
                                                                    <div class="input-group-content">
                                                                        <label>Fecha Vencimiento</label>
                                                                        <input type="text"
                                                                               name="form[txt][fec_vencimiento]"
                                                                               id="fec_vencimiento"
                                                                               class="form-control"
                                                                               {if isset($ver) and $ver==1}readonly{/if}
                                                                               value="{if isset($obligacionBD.fec_vencimiento)}{$obligacionBD.fec_vencimiento}{else}{date('Y-m-d')}{/if}">
                                                                    </div>
                                                                    <span class="input-group-addon"><i
                                                                                class="fa fa-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group control-width-normal">
                                                                <div class="input-group date" id="demo-date-pago">
                                                                    <div class="input-group-content">
                                                                        <label>Fecha Prog. Pago:</label>
                                                                        <input type="text"
                                                                               name="form[txt][fec_programada]"
                                                                               id="fec_programada"
                                                                               class="form-control"
                                                                               {if isset($ver) and $ver==1}readonly{/if}
                                                                               value="{if isset($obligacionBD.fec_programada)}{$obligacionBD.fec_programada}{else}{date('Y-m-d')}{/if}">
                                                                    </div>
                                                                    <span class="input-group-addon"><i
                                                                                class="fa fa-calendar"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group floating-label" id="ind_comentariosError">
                                                            <textarea id="ind_comentarios" name="form[alphaNum][ind_comentarios]" class="form-control" rows="2" placeholder="" {if isset($ver) and $ver==1}readonly{/if}>{if isset($obligacionBD.ind_comentarios)}{$obligacionBD.ind_comentarios}{/if}</textarea>
                                                            <label for="ind_comentarios"><i class="md md-comment"></i> Glosa del Voucher:</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group floating-label" id="ind_comentarios_adicionalError">
                                                            <textarea id="ind_comentarios_adicional" name="form[alphaNum][ind_comentarios_adicional]" class="form-control" rows="2" placeholder="" {if isset($ver) and $ver==1}readonly{/if}>{if isset($obligacionBD.ind_comentarios_adicional)}{$obligacionBD.ind_comentarios_adicional}{/if}</textarea>
                                                            <label for="ind_comentarios_adicional"><i class="md md-comment"></i> comentarios Adicionales:</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12" >
                                                        <div class="form-group floating-label" id="ind_motivo_anulacionError">
                                                            <textarea id="ind_motivo_anulacion" name="form[alphaNum][ind_motivo_anulacion]" class="form-control" rows="2" placeholder="" >{if isset($obligacionBD.ind_motivo_anulacion)}{$obligacionBD.ind_motivo_anulacion}{/if}</textarea>
                                                            <label for="ind_motivo_anulacion"><i class="md md-comment"></i> Razon Anulación:</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary">
                                                    <header>Información Adicional</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="fk_cpb017_num_tipo_servicioError">
                                                                <label for="fk_cpb017_num_tipo_servicio">
                                                                    Tipo de Servicio:
                                                                </label>
                                                                <select name="form[int][fk_cpb017_num_tipo_servicio]"
                                                                        class="form-control select2-list select2" required
                                                                        data-placeholder="Seleccione el Tipo de Servicio"
                                                                        {if isset($ver) and $ver==1}readonly{/if}
                                                                        id="fk_cpb017_num_tipo_servicio">
                                                                    <option value="">Seleccione..</option>
                                                                    {foreach item=proceso from=$listadoServicio}
                                                                        {if isset($obligacionBD.fk_cpb017_num_tipo_servicio) and $obligacionBD.fk_cpb017_num_tipo_servicio == $proceso.pk_num_tipo_servico }
                                                                            <option value="{$proceso.pk_num_tipo_servico}"
                                                                                    selected>{$proceso.ind_descripcion}</option>
                                                                        {else}
                                                                            <option value="{$proceso.pk_num_tipo_servico}">{$proceso.ind_descripcion}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="fk_a006_num_miscelaneo_tipo_pagoError">
                                                                <label for="fk_a006_num_miscelaneo_tipo_pago">
                                                                    Tipo de Pago:
                                                                </label>
                                                                <select name="form[int][fk_a006_num_miscelaneo_tipo_pago]"
                                                                        class="form-control select2-list select2" required
                                                                        {if isset($ver) and $ver==1}readonly{/if}
                                                                        data-placeholder="Seleccione Tipo de Pago">
                                                                    <option value="">Seleccione...</option>
                                                                    {foreach item=tipoPago from=$listadoTipoPago}
                                                                        {if $obligacionBD.fk_a006_num_miscelaneo_tipo_pago == $tipoPago.pk_num_miscelaneo_detalle}
                                                                            <option value="{$tipoPago.pk_num_miscelaneo_detalle}"
                                                                                    selected>{$tipoPago.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$tipoPago.pk_num_miscelaneo_detalle}">{$tipoPago.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_ingresaError">
                                                                <input type="text" class="form-control" value="{if isset($obligacionBD.fk_rhb001_num_empleado_ingresa)}{$obligacionBD.EMPLEADO_INGRESA}{/if}" name="form[int][fk_rhb001_num_empleado_ingresa]" disabled id="fk_rhb001_num_empleado_ingresa">
                                                                <label for="fk_rhb001_num_empleado_ingresa"><i class="md md-person"></i> Ingresado Por</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fec_preparacionError">
                                                                <input type="text" class="form-control" value="{if isset($obligacionBD.fec_preparacion)}{$obligacionBD.fec_preparacion}{/if}" disabled name="form[alphaNum][fec_preparacion]" id="fec_preparacion">
                                                                <label for="fec_preparacion"><i class="md md-today"></i> Fecha</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {if $parametroRevisar == 'S'}
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="fk_rhb001_num_empleado_revisaError">
                                                                    <input type="text" class="form-control" value="{if isset($obligacionBD.fk_rhb001_num_empleado_revisa)}{$obligacionBD.EMPLEADO_REVISA}{/if}" name="form[int][fk_rhb001_num_empleado_revisa]" disabled id="fk_rhb001_num_empleado_revisa">
                                                                    <label for="fk_rhb001_num_empleado_revisa"><i class="md md-person"></i> Revisado Por</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="fec_revisionError">
                                                                    <input type="text" class="form-control" value="{if isset($obligacionBD.fec_revision)}{$obligacionBD.fec_revision}{/if}" disabled name="form[alphaNum][fec_revision]" id="fec_revision">
                                                                    <label for="fec_revision"><i class="md md-today"></i> Fecha</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    {/if}
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_conformaError">
                                                                <input type="text" class="form-control" value="{if isset($obligacionBD.fk_rhb001_num_empleado_conforma)}{$obligacionBD.EMPLEADO_CONFORMA}{/if}" name="form[int][fk_rhb001_num_empleado_conforma]" disabled id="fk_rhb001_num_empleado_conforma">
                                                                <label for="fk_rhb001_num_empleado_conforma"><i class="md md-person"></i> Conformado Por</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fec_revisionError">
                                                                <input type="text" class="form-control" value="{if isset($obligacionBD.fec_revision)}{$obligacionBD.fec_revision}{/if}" disabled name="form[alphaNum][fec_revision]" id="fec_revision">
                                                                <label for="fec_revision"><i class="md md-today"></i> Fecha</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                                                                <input type="text" class="form-control" value="{if isset($obligacionBD.fk_rhb001_num_empleado_aprueba)}{$obligacionBD.EMPLEADO_APRUEBA}{/if}" name="form[int][fk_rhb001_num_empleado_aprueba]" disabled id="fk_rhb001_num_empleado_aprueba">
                                                                <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Aprobado Por</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fec_aprobadoError">
                                                                <input type="text" class="form-control" value="{if isset($obligacionBD.fec_aprobado)}{$obligacionBD.fec_aprobado}{/if}" disabled name="form[alphaNum][fec_aprobado]" id="fec_aprobado">
                                                                <label for="fec_aprobado"><i class="md md-today"></i> Fecha</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_anulaError">
                                                                <input type="text" class="form-control" value="{if isset($obligacionBD.fk_rhb001_num_empleado_anula)}{$obligacionBD.EMPLEADO_ANULA}{/if}" name="form[int][fk_rhb001_num_empleado_anula]" disabled id="fk_rhb001_num_empleado_anula">
                                                                <label for="fk_rhb001_num_empleado_anula"><i class="md md-person"></i> Anulado Por</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fec_anulacionError">
                                                                <input type="text" class="form-control" value="{if isset($obligacionBD.fec_anulacion)}{$obligacionBD.fec_anulacion}{/if}" disabled name="form[alphaNum][fec_anulacion]" id="fec_anulacion">
                                                                <label for="fec_anulacion"><i class="md md-today"></i> Fecha</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="informacionMonetaria">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header>Información Monetaria</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-3 text-right">
                                                                    <label for="docInterno"
                                                                           class="control-label" style="margin-top: 10px;"> Ref. Doc. Interno:</label>
                                                                </div>
                                                                <div class="col-sm-6" style="margin-top: -10px;">
                                                                    <div class="form-group floating-label" id="docInternoError">
                                                                        <input type="text" name="form[alphaNum][docInterno]"  id="docInterno" class="form-control" size="50%" disabled>
                                                                        <label for="docInterno" class="control-label"></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-3 text-right">
                                                                    <label for="fk_cpb002_num_tipo_documento"
                                                                           class="control-label" style="margin-top: 10px;"> Cuenta Bancaria:</label>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group"
                                                                         id="fk_cpb014_num_cuentaError" style="margin-top: -10px;">
                                                                        <select name="form[int][fk_cpb014_num_cuenta]" class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria" {if isset($ver) and $ver==1}readonly{/if}>
                                                                            <option value="">Seleccione Cuenta Bancaria</option>}
                                                                            {foreach item=proceso from=$listadoCuentas}
                                                                                {if isset($obligacionBD.fk_cpb014_num_cuenta) and $obligacionBD.fk_cpb014_num_cuenta == $proceso.pk_num_cuenta }
                                                                                    <option value="{$proceso.pk_num_cuenta}" selected >{$proceso.ind_num_cuenta}</option>
                                                                                {else}
                                                                                    <option value="{$proceso.pk_num_cuenta}">{$proceso.ind_num_cuenta}</option>
                                                                                {/if}
                                                                            {/foreach}
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-offset-3 col-sm-9" style="margin-top: -25px;">
                                                                <div class="form-group floating-label" id="num_flag_caja_chicaError">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" {if isset($obligacionBD.num_flag_caja_chica) and $obligacionBD.num_flag_caja_chica==1} checked{/if} value="1" name="form[int][num_flag_caja_chica]" {if isset($ver) and $ver==1}disabled{/if}>
                                                                            <span>Pago con Caja Chica </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-offset-3 col-sm-9" style="margin-top: -25px;">
                                                                <div class="form-group floating-label" id="num_flag_pago_individual Error">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" {if isset($obligacionBD.num_flag_pago_individual) and $obligacionBD.num_flag_pago_individual==1} checked{/if} value="1" name="form[int][num_flag_pago_individual]" {if isset($ver) and $ver==1}disabled{/if}>
                                                                            <span>Preparar Pago Individual </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-offset-3 col-sm-9" style="margin-top: -25px;">
                                                                <div class="form-group floating-label" id="num_flag_obligacion_autoError">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox"  checked value="1" name="form[int][num_flag_obligacion_auto]" {if isset($ver) and $ver==1}disabled{/if}>
                                                                            <span>Preparar Pago (Automatico) </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-offset-3 col-sm-9" style="margin-top: -25px;">
                                                                <div class="form-group floating-label" id="num_flag_diferidoError">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" {if isset($obligacionBD.num_flag_diferido) and $obligacionBD.num_flag_diferido==1} checked{/if} value="1" name="form[int][num_flag_diferido]" {if isset($ver) and $ver==1}disabled{/if}>
                                                                            <span>Diferir el Pago </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-offset-3 col-sm-9" style="margin-top: -25px;">
                                                                <div class="form-group floating-label" id="num_flag_pago_diferidoError">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" {if isset($obligacionBD.num_flag_pago_diferido) and $obligacionBD.num_flag_pago_diferido==1} checked{/if} value="1" name="form[int][num_flag_pago_diferido]" {if isset($ver) and $ver==1}disabled{/if}>
                                                                            <span>Considerarlo como Diferido </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-offset-3 col-sm-9" style="margin-top: -25px;">
                                                                <div class="form-group floating-label" id="num_flag_afecto_IGVlError">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" {if isset($obligacionBD.num_flag_afecto_IGV) and $obligacionBD.num_flag_afecto_IGV==1} checked{/if} value="1" name="form[int][num_flag_afecto_IGV]" {if isset($ver) and $ver==1}disabled{/if}>
                                                                            <span>Afecto a Defraccion del IGV </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-offset-3 col-sm-9" style="margin-top: -25px;">
                                                                <div class="form-group floating-label" id="num_flag_compromisoError">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" onclick="activarBoton('1')" {if isset($obligacionBD.num_flag_compromiso) and $obligacionBD.num_flag_compromiso==1} checked{/if} value="1" id="num_flag_compromiso" name="form[int][num_flag_compromiso]" {if isset($ver) and $ver==1}disabled{/if}>
                                                                            <span>Refiere Compromiso </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-offset-3 col-sm-9" style="margin-top: -25px;">
                                                                <div class="form-group floating-label" id="num_flag_presupuestoError">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" {if isset($obligacionBD.num_flag_presupuesto) and $obligacionBD.num_flag_presupuesto==1} checked{/if} value="1" name="form[int][num_flag_presupuesto]" id="num_flag_presupuesto" {if isset($ver) and $ver==1}disabled{/if}>
                                                                            <span>Afecta Presupuesto </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-offset-3 col-sm-9" style="margin-top: -25px;">
                                                                <div class="form-group floating-label" id="num_flag_obligacion_directaError">
                                                                    <div class="checkbox checkbox-styled">
                                                                        <label>
                                                                            <input type="checkbox" onclick="activarBoton('2')" {if isset($obligacionBD.num_flag_obligacion_directa) and $obligacionBD.num_flag_obligacion_directa==1} checked{/if} id="num_flag_obligacion_directa" name="form[int][num_flag_obligacion_directa]" value="1" {if isset($ver) and $ver==1}disabled{/if}>
                                                                            <span>Pago Directo </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <br>
                                                            <br>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4 text-right">
                                                                    <label for="ind_tipo_descuento" class="control-label" style="margin-top: 10px;"> Tipo Descuento:</label>
                                                                </div>
                                                                <div class="col-sm-5">
                                                                    <div class="form-group" style="margin-top: -18px;"
                                                                         id="ind_tipo_descuentoError">
                                                                        <select id="ind_tipo_descuento" name="form[alphaNum][ind_tipo_descuento]" class="form-control select2"
                                                                                {if isset($ver) and $ver==1}readonly{/if} data-placeholder="Seleccione el Tipo de Pago">

                                                                            {if isset($obligacionBD.ind_tipo_descuento) and $obligacionBD.ind_tipo_descuento=='P'}
                                                                                {$s1='selected'}  {$s2=''}
                                                                            {elseif isset($obligacionBD.ind_tipo_descuento) and $obligacionBD.ind_tipo_descuento=='N'}
                                                                                {$s2='selected'}  {$s1=''}
                                                                            {else}
                                                                                {$s2=''}  {$s1=''}
                                                                            {/if}
                                                                            <option value="">Tipo de descuento</option>
                                                                            <option value="P" {$s1}>Proveedor</option>
                                                                            <option value="N" {$s2}>Nomina</option>



                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-2">
                                                                    <input type="text"
                                                                           name="form[int][num_monto_descuento]"
                                                                           id="num_monto_descuento"
                                                                           class="form-control" size="15"
                                                                           value="{if isset($obligacionBD.num_monto_descuento)}{$obligacionBD.num_monto_descuento}{/if}"
                                                                           placeholder="Monto Descuento"
                                                                           style="display: {if isset($obligacionBD.ind_tipo_descuento)} line {else} none{/if} ">
                                                                </div>

                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4 text-right">
                                                                    <label for="num_monto_afecto" class="control-label" style="margin-top: 10px;"> Monto Afecto</label>
                                                                </div>
                                                                <div class="col-sm-4 col-sm-offset-1">
                                                                    <input type="text" class="form-control"  name="form[int][num_monto_afecto]" id="num_monto_afecto" value="{if isset($obligacionBD.num_monto_afecto)}{$obligacionBD.num_monto_afecto}{else}0.000000{/if}" readonly >
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4 text-right">
                                                                    <label for="num_monto_no_afecto" class="control-label" style="margin-top: 10px;"> Monto No Afeccto:</label>
                                                                </div>
                                                                <div class="col-sm-4 col-sm-offset-1">
                                                                    <input type="text" name="form[int][num_monto_no_afecto]"  id="num_monto_no_afecto" class="form-control" value="{if isset($obligacionBD.num_monto_no_afecto)}{$obligacionBD.num_monto_no_afecto}{else}0.000000{/if}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4 text-right">
                                                                    <label for="num_monto_impuesto" class="control-label" style="margin-top: 10px;"> Impuesto:</label>
                                                                </div>
                                                                <div class="col-sm-4 col-sm-offset-1">
                                                                    <input type="text" name="form[int][num_monto_impuesto]"  id="num_monto_impuesto" class="form-control" value="{if isset($obligacionBD.num_monto_impuesto)}{$obligacionBD.num_monto_impuesto}{else}0.000000{/if}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4 text-right">
                                                                    <label for="num_monto_impuesto_otros" class="control-label" style="margin-top: 10px;"> Otros Impuestos/Retenciones:</label>
                                                                </div>
                                                                <div class="col-sm-4 col-sm-offset-1">
                                                                    <input type="text" name="form[int][num_monto_impuesto_otros]"  id="num_monto_impuesto_otros" class="form-control" value="{if isset($obligacionBD.num_monto_impuesto_otros)}{$obligacionBD.num_monto_impuesto_otros}{else}0.000000{/if}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4 text-right">
                                                                    <label for="num_monto_obligacion" class="control-label" style="margin-top: 10px;"> Total Obligacion:</label>
                                                                </div>
                                                                <div class="col-sm-4 col-sm-offset-1">
                                                                    <input type="text" style="font-weight:bold;" name="form[int][num_monto_obligacion]"  id="num_monto_obligacion" class="form-control" value="{if isset($obligacionBD.num_monto_obligacion)}{$obligacionBD.num_monto_obligacion}{else}0.000000{/if}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4 text-right">
                                                                    <label for="num_monto_adelanto" class="control-label" style="margin-top: 10px;"> Adelanto:</label>
                                                                </div>
                                                                <div class="col-sm-4 col-sm-offset-1">
                                                                    <input type="text" name="form[int][num_monto_adelanto]"  id="num_monto_adelanto" class="form-control" value="{if isset($obligacionBD.num_monto_adelanto)}{$obligacionBD.num_monto_adelanto}{else}0.000000{/if}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4 text-right">
                                                                    <label for="nomtoTotalPagar" class="control-label" style="margin-top: 10px;"> Total a Pagar:</label>
                                                                </div>
                                                                <div class="col-sm-4 col-sm-offset-1">
                                                                    <input type="text" style="font-weight:bold;" name="form[int][nomtoTotalPagar]"  id="nomtoTotalPagar" class="form-control" value="{if isset($obligacionBD.num_monto_obligacion)}{$obligacionBD.num_monto_obligacion-$obligacionBD.num_monto_adelanto}{else}0.00{/if}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4 text-right">
                                                                    <label for="num_monto_pago_parcial" class="control-label" style="margin-top: 10px;"> Pagos parciales:</label>
                                                                </div>
                                                                <div class="col-sm-4 col-sm-offset-1">
                                                                    <input type="text" style="font-weight:bold;" name="form[int][num_monto_pago_parcial]"  id="num_monto_pago_parcial" class="form-control" value="{if isset($obligacionBD.num_monto_pago_parcial)}{$obligacionBD.num_monto_pago_parcial}{else}0.000000{/if}" readonly>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-4 text-right">
                                                                    <label for="docInterno" class="control-label" style="margin-top: 10px;"> Saldo Pendiente:</label>
                                                                </div>
                                                                <div class="col-sm-4 col-sm-offset-1">
                                                                    <input type="text" style="font-weight:bold;" name="form[int][docInterno]"  id="docInterno" class="form-control" value="0.00" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="cuentaContablePresupuestaria">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="card">
                                                    <div class="card-head card-head-xs style-primary" align="center">
                                                        <header>Retenciones/Impuestos</header>
                                                    </div>
                                                    <div class="card-body">
                                                        <div id="imput_nuevo"></div>
                                                        {if isset($obligacionBD) && $obligacionBD.ind_tipo_procedencia=='NM'}
                                                            <table class="table table-striped no-margin" id="contenidoTabla" style="width: 100%;display:block;">
                                                                <thead style="display: inline-block;width: 100%;">
                                                                <tr>
                                                                    <th width="35" class="text-center">#</th>
                                                                    <th width="390" class="text-center">Concepto</th>
                                                                    <th width="100" class="text-center">Monto</th>
                                                                    <th width="35" class="text-center"> </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="impuesto" style="height: 170px;display: inline-block;width: 100%;overflow: auto;">
                                                                {if isset($impuestoBD)}
                                                                    {foreach item=i from=$impuestoBD}
                                                                        <tr id="impuesto{$i.ind_secuencia}">
                                                                            <input type="hidden" value="{$i.ind_secuencia}" name="form[int][impuestosRetenciones][ind_secuencia][{$i.ind_secuencia}]">
                                                                            <input type="hidden" value="{$i.pk_num_obligacion_impuesto}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][pk_num_obligacion_impuesto]">
                                                                            <input type="hidden" value="{$i.0}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][num_monto_afecto]">
                                                                            <input type="hidden" value="{$i.0}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][num_monto_impuesto]">
                                                                            <input type="hidden" value="{$i.fk_cbb004_num_cuenta}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][fk_cbb004_num_cuenta]">
                                                                            <input type="hidden" value="{$i.fk_cbb004_num_cuenta_pub20}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][fk_cbb004_num_cuenta_pub20]">
                                                                            <input type="hidden" value="{$i.fk_nmb002_num_concepto}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][fk_nmb002_num_concepto]">

                                                                            <td width="35"><input type="text" style="font-size:11px;" class="form-control text-center"  value="{$i.ind_secuencia}" readonly></td>
                                                                            <td width="390"><input type="text" style="font-size:11px;" class="form-control text-center"  value="{$i.ind_descripcion}" readonly></td>
                                                                            <td width="100"><input type="text" style="font-size:11px;" class="form-control text-center muntoImpuesto" value="{$i.num_monto_afecto}" readonly></td>
                                                                            <td width="35" id="impuesto{$i.ind_secuencia}" width="10px" class="text-center" style="vertical-align: middle;">
                                                                                <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$i.ind_secuencia}" {if isset($ver) and $ver==1}disabled{/if}>
                                                                                    <i class="md md-delete"></i>
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                    {/foreach}
                                                                {/if}
                                                                </tbody>
                                                                <tfoot  style="display: inline-block;width: 100%;">
                                                                <tr>
                                                                    <th width="135" colspan="2" class="text-center"></th>
                                                                    <th width="290" class="text-center"  colspan="2">
                                                                        <button
                                                                                disabled="disabled"
                                                                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                                type="button"
                                                                                title="Buscar Impuesto"
                                                                                titulo="Buscar Impuesto"
                                                                                id="nuevoImpuesto"
                                                                                idTabla="contenidoTabla"
                                                                                data-toggle="modal" data-target="#formModal2"
                                                                                url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/impuestoMET">
                                                                            <i class="md md-add"></i> AGREGAR
                                                                        </button>
                                                                    </th>
                                                                    <th width="135" class="text-center"  colspan="2">
                                                                        <label>Total: </label>
                                                                        <label id="totalImpuesto" style="width: 70%;" class="text-right" ivaPorcentaje="0" ivaMonto="{if isset($obligacionBD.num_monto_impuesto)} {$obligacionBD.num_monto_impuesto} {else} 0 {/if}" monto="{if isset($obligacionBD.num_monto_impuesto_otros)} {$obligacionBD.num_monto_impuesto_otros} {else} 0 {/if}">{if isset($obligacionBD.num_monto_impuesto_otros)} {$obligacionBD.num_monto_impuesto_otros} {else} 0 {/if}</label>
                                                                    </th>
                                                                </tr>
                                                                </tfoot>
                                                            </table>
                                                        {else}
                                                            <table class="table table-striped no-margin" id="contenidoTabla" style="width: 100%;display:block;">
                                                                <thead style="display: inline-block;width: 100%;">
                                                                <tr>
                                                                    <th width="35" class="text-center">#</th>
                                                                    <th width="190" class="text-center">Impuesto</th>
                                                                    <th width="100" class="text-center">Monto Afecto</th>
                                                                    <th width="100" class="text-center">Factor %</th>
                                                                    <th width="100" class="text-center">Monto</th>
                                                                    <th width="35" class="text-center"> </th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="impuesto" style="height: 170px;display: inline-block;width: 100%;overflow: auto;">
                                                                {if isset($impuestoBD)}
                                                                    {foreach item=i from=$impuestoBD}
                                                                        <tr id="impuesto{$i.ind_secuencia}">
                                                                            <input type="hidden" value="{$i.ind_secuencia}" name="form[int][impuestosRetenciones][ind_secuencia][{$i.ind_secuencia}]">
                                                                            <input type="hidden" value="{$i.pk_num_obligacion_impuesto}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][pk_num_obligacion_impuesto]">
                                                                            <input type="hidden" value="{$i.num_monto_afecto}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][num_monto_afecto]">
                                                                            <input type="hidden" value="{$i.num_monto_impuesto}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][num_monto_impuesto]">
                                                                            <input type="hidden" value="{$i.fk_cbb004_num_cuenta}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][fk_cbb004_num_cuenta]">
                                                                            <input type="hidden" value="{$i.fk_cbb004_num_cuenta_pub20}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][fk_cbb004_num_cuenta_pub20]">
                                                                            <input type="hidden" value="{$i.fk_cpb015_num_impuesto}" name="form[int][impuestosRetenciones][{$i.ind_secuencia}][fk_cpb015_num_impuesto]">
                                                                            <td width="35"><input type="text" style="font-size:11px;" class="form-control text-center" value="{$i.ind_secuencia}" readonly ></td>
                                                                            <td width="190"><input type="text" style="font-size:11px;" class="form-control text-center"  value="{$i.txt_descripcion}" readonly></td>
                                                                            <td width="100"><input type="text" style="font-size:11px;" class="form-control text-center"  value="{$i.num_monto_afecto}" readonly></td>
                                                                            <td width="100"><input type="text" style="font-size:11px;" class="form-control text-center"  value="{$i.num_factor_porcentaje}" readonly></td>
                                                                            <td width="100"><input type="text" style="font-size:11px;" class="form-control text-center muntoImpuesto" value="{$i.num_monto_impuesto}" readonly></td>
                                                                            <td width="35" id="impuesto{$i.ind_secuencia}" width="10px" class="text-center" style="vertical-align: middle;">
                                                                                <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$i.ind_secuencia}" {if isset($ver) and $ver==1}disabled{/if}>
                                                                                    <i class="md md-delete"></i>
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                    {/foreach}
                                                                {/if}
                                                                </tbody>
                                                                <tfoot  style="display: inline-block;width: 100%;">
                                                                <tr>
                                                                    <th width="135" colspan="2" class="text-center"></th>
                                                                    <th width="290" class="text-center"  colspan="2">
                                                                        <button
                                                                                disabled="disabled"
                                                                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                                type="button"
                                                                                title="Buscar Impuesto"
                                                                                titulo="Buscar Impuesto"
                                                                                id="nuevoImpuesto"
                                                                                idTabla="contenidoTabla"
                                                                                data-toggle="modal" data-target="#formModal2"
                                                                                url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/impuestoMET">
                                                                            <i class="md md-add"></i> AGREGAR
                                                                        </button>
                                                                    </th>
                                                                    <th width="135" class="text-center"  colspan="2">
                                                                        <label>Total: </label>
                                                                        <label id="totalImpuesto" style="width: 70%;" class="text-right" ivaPorcentaje="0" ivaMonto="{if isset($obligacionBD.num_monto_impuesto)} {$obligacionBD.num_monto_impuesto} {else} 0 {/if}" monto="{if isset($obligacionBD.num_monto_impuesto_otros)} {$obligacionBD.num_monto_impuesto_otros} {else} 0 {/if}">{if isset($obligacionBD.num_monto_impuesto_otros)} {$obligacionBD.num_monto_impuesto_otros} {else} 0 {/if}</label>
                                                                    </th>
                                                                </tr>
                                                                </tfoot>
                                                            </table>
                                                        {/if}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="card">
                                                    <div class="card-head card-head-xs style-primary" align="center">
                                                        <header>Documentos Relacionados</header>
                                                    </div>
                                                    <div class="card-body">
                                                        <div id="imput_nuevo"></div>
                                                        <table class="table table-striped no-margin" id="contenidoTablaDoc" style="display: inline-block;width: 1400px; overflow: auto;">
                                                            <thead style="display: inline-block;width: 1400px;">
                                                            <tr>
                                                                <th width="35" class="text-center">#</th>
                                                                <th width="150" class="text-center">Clasificacion</th>
                                                                <th width="150" class="text-center">Doc. Referencia </th>
                                                                <th width="150" class="text-center">Fecha</th>
                                                                <th width="120" class="text-center">Monto Total</th>
                                                                <th width="120" class="text-center">Monto Afecto</th>
                                                                <th width="120" class="text-center">Impuesto</th>
                                                                <th width="120" class="text-center">Monto No Afecto</th>
                                                                <th width="450" class="text-center">Comentarios</th>
                                                                <th width="35" class="text-center"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="documentos" style="height: 170px; display: inline-block;width: 1400px;">
                                                            {if isset($documentoBD)}
                                                                {foreach item=i from=$documentoBD}
                                                                    {if $i.fk_lgb014_num_almacen==2}
                                                                        {$clasificacion = 'O. Compra'}
                                                                    {else}
                                                                        {$clasificacion = 'O. Servicio'}
                                                                    {/if}
                                                                    <tr id="idDocumento{$i.pk_num_orden}">
                                                                        <input type="hidden" value="{$trDocumentos}" name="form[int][documentos][ind_secuencia][{$trDocumentos}]">
                                                                        <input type="hidden" id="codDocumento{$trDocumentos}idOrden" value="{$i.pk_num_orden}" name="form[int][documentos][{$trDocumentos}][fk_lgb019_num_orden]">
                                                                        <td width="35"><input type="text" class="form-control text-center" value="{$trDocumentos}" name="form[int][ind_secuencia{$trDocumentos}]" disabled="disabled" ></td>
                                                                        <td width="150"><input type="text" class="form-control text-center" id="clasificacion" disabled="disabled" value="{$clasificacion}"  ></td>
                                                                        <td width="150"><input type="text" class="form-control text-center" id="nroDoc" value="DOC-REF" readonly></td>
                                                                        <td width="150"><input type="text" class="form-control text-center" id="fecha" value="{$i.fec_creacion}" readonly></td>
                                                                        <td width="120"><input type="text" class="form-control text-center" id="num_monto_total" value="{$i.num_monto_total}" readonly></td>
                                                                        <td width="120"><input type="text" class="form-control text-center" id="num_monto_afecto" value="{$i.num_monto_afecto}" readonly></td>
                                                                        <td width="120"><input type="text" class="form-control text-center" id="monto_impuesto" value="monto_impuesto" readonly></td>
                                                                        <td width="120"><input type="text" class="form-control text-center" id="num_monto_no_afecto" value="{$i.num_monto_no_afecto}" readonly></td>
                                                                        <td width="450"><input type="text" class="form-control text-center" id="comentario" value="{$i.ind_comentario}" readonly ></td>
                                                                        <td id="documentos{$trDocumentos}" width="35" class="text-center" style="vertical-align: middle;">
                                                                            <input type="hidden" value="{$trDocumentos}" name="form[int][cant]">
                                                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$trDocumentos}" {if isset($ver) and $ver==1}disabled{/if}>
                                                                                <i class="md md-delete"></i></button></td>
                                                                    </tr>
                                                                {/foreach}
                                                            {/if}

                                                            </tbody>
                                                        </table>
                                                        <div class="col-sm-12" align="center">
                                                            <button
                                                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                    type="button"
                                                                    titulo="Buscar Documento"
                                                                    id="nuevoDocumento"
                                                                    idTabla="contenidoTablaDoc"
                                                                    data-toggle="modal" data-target="#formModal2"
                                                                    url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/documentosMET/listadoDoc/">
                                                                <i class="md md-add"></i> AGREGAR
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-head card-head-xs style-primary" align="center">
                                                        <header>Distribucion</header>
                                                    </div>
                                                    <div class="card-body" style="padding: 4px;">
                                                        <div id="imput_nuevo"></div>

                                                        <table id="partidasCuentas" class="table table-striped table-hover" style="width: 100%;display:block;">
                                                            <thead style="display: inline-block;width: 100%;">
                                                            <tr>
                                                                <th width="35">#</th>
                                                                <th class="text-center" width="400">Partida </th>
                                                                <th class="text-center" width="400">Cta. Contable</th>
                                                                <th class="text-center" width="35">C.Costo</th>
                                                                <th class="text-center" width="50">Persona</th>
                                                                <th class="text-center" width="35">No Afec.</th>
                                                                <th class="text-center" width="150">Monto</th>
                                                                <th class="text-center" width="60"> Acciones </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody style="height: 200px;display: inline-block;width: 100%;overflow: auto;" id="tbodyCuentas">
                                                            {if isset($partidaBD)}
                                                                {foreach item=i from=$partidaBD}
                                                                    <tr id="codCuentas{$i.num_secuencia}">
                                                                        <input type="hidden" class="ajaxPartidas" value="{$i.num_secuencia}" name="form[int][partidaCuenta][ind_secuencia][{$i.num_secuencia}]">
                                                                        <input type="hidden" class="ajaxPartidas" id="codCuentas{$i.num_secuencia}ID" value="{$i.pk_num_obligacion_cuentas}" name="form[int][partidaCuenta][{$i.num_secuencia}][pk_num_obligacion_cuentas]">
                                                                        <input type="hidden" class="ajaxPartidas" id="codCuentas{$i.num_secuencia}CUENTA" value="{$i.fk_cbb004_num_cuenta}" name="form[int][partidaCuenta][{$i.num_secuencia}][fk_cbb004_num_cuenta]">
                                                                        <input type="hidden" class="ajaxPartidas" id="codCuentas{$i.num_secuencia}CUENTA20" value="{$i.fk_cbb004_num_cuenta_pub20}" name="form[int][partidaCuenta][{$i.num_secuencia}][fk_cbb004_num_cuenta_pub20]">
                                                                        <input type="hidden" class="ajaxPartidas" id="codCuentas{$i.num_secuencia}PARTIDA" value="{$i.fk_prb002_num_partida_presupuestaria}" name="form[int][partidaCuenta][{$i.num_secuencia}][fk_prb002_num_partida_presupuestaria]">
                                                                        <input type="hidden" class="ajaxPartidas" id="codCuentas{$i.num_secuencia}CC" value="{$i.fk_a023_num_centro_costo}" name="form[int][partidaCuenta][{$i.num_secuencia}][fk_a023_num_centro_costo]">
                                                                        <input type="hidden" class="ajaxPartidas" id="codCuentas{$i.num_secuencia}PERSONA" value="{$i.fk_a003_num_persona_proveedor}" name="form[int][partidaCuenta][{$i.num_secuencia}][fk_a003_num_persona_proveedor]">
                                                                        <input type="hidden" class="ajaxPartidas" id="codCuentas{$i.num_secuencia}DESCRIPCION" value="{$i.ind_descripcion}" name="form[int][partidaCuenta][{$i.num_secuencia}][ind_descripcion]">
                                                                        <td width="35" style="vertical-align: middle;"><input type="text"  class="form-control text-center" value="{$i.num_secuencia}" readonly ></td>
                                                                        <td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$i.num_secuencia}PARTIDA_C" value="{$i.codigoPartida}" readonly></td>
                                                                        <td width="250" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$i.num_secuencia}PARTIDA_D" value="{$i.descripcionPartida}" readonly></td>
                                                                        <td width="150">
                                                                            <input type="text" class="form-control text-center" id="codCuentas{$i.num_secuencia}CUENTA_C" value="{$i.codigoCuenta}" readonly>
                                                                            <input type="text" class="form-control text-center" id="codCuentas{$i.num_secuencia}CUENTA20_C" value="{$i.codigoCuenta20}" readonly>
                                                                        </td>
                                                                        <td width="250">
                                                                            <input type="text" class="form-control text-center" id="codCuentas{$i.num_secuencia}CUENTA_D" value="{$i.descripcionCuenta}" readonly>
                                                                            <input type="text" class="form-control text-center" id="codCuentas{$i.num_secuencia}CUENTA20_D" value="{$i.descripcionCuenta20}" readonly>
                                                                        </td>
                                                                        <td width="65" style="vertical-align: middle;">
                                                                            <input type="text" class="form-control text-center accionModal" id="codCuentas{$i.num_secuencia}CC_C" value="{$i.fk_a023_num_centro_costo}"
                                                                                   data-toggle="modal"
                                                                                   data-target="#formModal2"
                                                                                   titulo="Listado Centro de Costo"
                                                                                   enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET/partida/codCuentas{$i.num_secuencia}" readonly>
                                                                        </td>
                                                                        <td width="60" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$i.num_secuencia}PERSONA_C" value="{$i.fk_a003_num_persona_proveedor}" readonly></td>
                                                                        <td width="40" style="vertical-align: middle;">
                                                                            <div class="checkbox checkbox-styled">
                                                                                <label>
                                                                                    <input type="checkbox" id="codCuentas{$i.num_secuencia}FLAG_NO_AFECTO" name="form[int][partidaCuenta][{$i.num_secuencia}][num_flag_no_afecto]" {if isset($i.num_flag_no_afecto) && $i.num_flag_no_afecto==1} checked {/if} {if isset($ver) and $ver==1}disabled{/if} value="1">
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                        </td>
                                                                        <td width="160" style="vertical-align: middle;"><input type="text" class="form-control text-right montoDistribucion ajaxPartidas" idTR="codCuentas{$i.num_secuencia}" value="{if isset($i.num_monto)} {$i.num_monto} {/if}" {if isset($ver) and $ver==1}readonly{/if} name="form[int][partidaCuenta][{$i.num_secuencia}][num_monto]"></td>
                                                                        <td width="70" style="vertical-align: middle;">
                                                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$i.num_secuencia}" {if isset($ver) and $ver==1}disabled{/if}><i class="md md-delete"></i></button>
                                                                        </td>
                                                                    </tr>
                                                                {/foreach}
                                                            {/if}
                                                            </tbody>
                                                            <tfoot>
                                                            <tr>
                                                                <th width="35"></th>
                                                                <th class="text-center" width="400"></th>
                                                                <th class="text-center" width="400"></th>
                                                                <th class="text-center" width="35"></th>
                                                                <th class="text-center" width="50"></th>
                                                                <th class="text-center" width="35"></th>
                                                                <th width="150"><label>Total: </label><label id="totalPartidaCuenta" style="width: 76%;" class="text-right" monto="0" montoAfecto="{if isset($obligacionBD.num_monto_afecto)}{$obligacionBD.num_monto_afecto}{else}0.000000{/if}" montoNoAfecto="{if isset($obligacionBD.num_monto_no_afecto)}{$obligacionBD.num_monto_no_afecto}{else}0.000000{/if}">{if isset($obligacionBD.num_monto_afecto)}{$obligacionBD.num_monto_afecto+$obligacionBD.num_monto_no_afecto}{else}0.000000{/if}</label></th>
                                                                <th class="text-center" width="60"></th>
                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                        <div class="col-sm-12" align="center">
                                                            <button
                                                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                    type="button"
                                                                    data-toggle="modal" data-target="#formModal2"
                                                                    titulo="SELECCIONE LA PARTIDA PRESUPUESTARIA"
                                                                    idTabla="partidasCuentas"
                                                                    url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/partidasCuentasMET/Partidas"
                                                                    {if isset($ver) and $ver==1}disabled{/if} >
                                                                <i class="md md-add"></i> AGREGAR PARTIDAS PRESUPUESTARIAS
                                                            </button>
                                                            <button
                                                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                    type="button"
                                                                    idTabla="partidasCuentas"
                                                                    id="CuentasContables"
                                                                    data-toggle="modal" data-target="#formModal2"
                                                                    titulo="SELECCIONE LA CUENTA CONTABLE"
                                                                    url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/partidasCuentasMET/Cuentas"
                                                                    {if isset($ver) and $ver==1}disabled{/if} >
                                                                <i class="md md-add"></i> AGREGAR CUENTA CONTABLE
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="resumenCuentaContablePresupuestaria">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary" align="center">
                                                    <header>Distribución Contable</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <table id="distribucionContableTabla" class="table table-striped table-hover" style="width: 100%;display:block;">
                                                        <thead style="display: inline-block;width: 100%;">
                                                        <tr>
                                                            <th width="200">Cuenta </th>
                                                            <th width="900">Descripcion</th>
                                                            <th width="140">Monto</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody style="height: 170px;display: inline-block;width: 100%;overflow: auto;" id="distribucionContable">

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary" align="center">
                                                    <header>Distribución Presupuestaria</header>
                                                </div>
                                                <div class="col-sm-12" align="center" style="background-color:#C7C7C7;"">
                                                <div class="col-sm-8">
                                                    <table style="width:100%;">
                                                        <tr>
                                                            <td width="35"><div style="background-color: red; width:25px; height:20px;"></div></td>
                                                            <td align="left">Sin disponibilidad presupuestaria</td>
                                                            <td width="35"><div style="background-color:green; width:25px; height:20px;"></div></td>
                                                            <td align="left">Disponibilidad presupuestaria</td>
                                                            <td width="35"><div style="background-color:yellow; width:25px; height:20px;"></div></td>
                                                            <td align="left">Disponibilidad presupuestaria (Tiene ordenes pendientes)</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-sm-4 text-right">
                                                    <button
                                                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                            type="button"
                                                            title="Disponibilidad Presupuestaria"
                                                            titulo="Disponibilidad Presupuestaria de Obligación"
                                                            id="dispPresupuestaria"
                                                            idTabla="contenidoTabla"
                                                            data-toggle="modal" data-target="#formModal2"
                                                            url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/presupuestoMET">
                                                        <i class="fa fa-bar-chart"></i> DISPONIBILIDAD PRESUPUESTARIA
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body" style="padding: 4px;">
                                            <table id="distribucionPresupuestariaTabla" class="table table-striped table-hover" style="width: 100%;display:block;">
                                                <thead style="display: inline-block;width: 100%;">
                                                <tr>
                                                    <th width="200">Partida </th>
                                                    <th width="900">Descripción</th>
                                                    <th width="140">Monto</th>
                                                </tr>
                                                </thead>
                                                <tbody style="height: 170px;display: inline-block;width: 100%;overflow: auto;" id="distribucionPresupuestaria">



                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                    <ul class="pager wizard">
                        <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                        <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                        <li class="next last"><a class="btn-raised" href="javascript:void(0);">&Uacute;ltimo</a></li>
                        <li class="next"><a class="btn-raised" href="javascript:void(0);">Pr&oacute;ximo</a></li>
                    </ul>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-2">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group floating-label">
                                        <input type="text" disabled class="form-control disabled"
                                               value="{if isset($obligacionBD.ind_usuario)}{$obligacionBD.ind_usuario}{/if}"
                                               id="ind_usuario">
                                        <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12">
                                <div class="form-group floating-label" id="cod_tipo_nominaError">
                                    <input type="text" disabled class="form-control disabled"
                                           value="{if isset($obligacionBD.fec_ultima_modificacion)}{$obligacionBD.fec_ultima_modificacion}{/if}"
                                           id="fec_ultima_modificacion">
                                    <label for="cod_tipo_nomina"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<span class="clearfix"></span>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal "
            descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
        <i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
    </button>
    <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">
            <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>

</div>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();
        /// Complementos
        $('.select2').select2({ allowClear: true });
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });

        // ancho de la Modal
        $('#modalAncho').css("width", "100%");

        //Guardado y modales
        $('#accion').click(function () {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                var arrayCheck = '';
                var arrayMostrarOrden = ['fk_a003_num_persona_proveedor', 'cod_tipo_documento', 'ind_nro_factura', 'fec_documento', 'num_monto_obligacion', 'ind_estado'];
                if (dato['status'] == 'error') {
                    app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                } else if (dato['status'] == 'errorSQL') {
                    app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                } else if (dato['status'] == 'modificar') {
                    app.metActualizarRegistroTabla(dato, dato['idObligacion'], 'idObligacion', arrayCheck, arrayMostrarOrden, 'La Obligacion fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                } else if (dato['status'] == 'nuevo') {
                    $(document.getElementById('cerrarModal')).click();
                    swal('Registro Exitoso','La Obligacion fue guardada satisfactoriamente.', 'success');
                    $(document.getElementById('idCajaChica' + dato['idCajaChica'])).remove();
                }
            }, 'json');
        });

        /// alarmas de Cuentas Por Pagar
        $('#ajaxDisponibilidad').click(function () {
            var iva = $('#num_monto_impuesto').val();
            if( $("#partidasCuentas > tbody > tr").length > 0){
                //limpio
                $('#distribucionContable').remove();
                $('#distribucionContableTabla').append(
                        '<tbody style="height: 170px; display: inline-block; width: 100%; overflow: auto;" id="distribucionContable">'+

                        '</tbody>'
                );

                $('#distribucionPresupuestaria').remove();
                $('#distribucionPresupuestariaTabla').append(
                        '<tbody style="height: 170px ; display: inline-block; width: 100%; overflow: auto;" id="distribucionPresupuestaria">'+

                        '</tbody>'
                );
                $.post('{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/alarmaMET/'+iva,
                        $('.ajaxPartidas').serialize()
                        , function (dato) {
                            if(dato['cuentas']){
                                var cuentas = dato['cuentas'];
                                for (var cuenta in cuentas) {
                                    $("#cuentaContable"+cuenta).remove();
                                    $('#distribucionContable').append(
                                            '<tr id="cuentaContable'+cuenta+'">' +
                                            '<td width="200">' +
                                            '<input type="text" style="font-size:11px;" class="form-control text-center"  value="'+cuentas[cuenta]['cuenta']+'" readonly>' +
                                            '<input type="text" style="font-size:11px;" class="form-control text-center"  value="'+cuentas[cuenta]['cuenta20']+'" readonly>' +
                                            '</td>' +
                                            '<td width="900">' +
                                            '<input type="text" style="font-size:11px;" class="form-control text-center"  value="'+cuentas[cuenta]['descripcion']+'" readonly>' +
                                            '<input type="text" style="font-size:11px;" class="form-control text-center"  value="'+cuentas[cuenta]['descripcion20']+'" readonly>' +
                                            '</td>' +
                                            '<td width="140" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center"  value="'+cuentas[cuenta]['monto']+'" readonly></td>' +
                                            '</tr>'
                                    );
                                }
                            }
                            if(dato['partidas']){
                                var partidas = dato['partidas'];
                                $('#distribucionPresupuestaria').html('');
                                for (var partida in partidas) {
                                    if(partidas[partida]['porcentaje'] <= 60){
                                        var color = "green";
                                    }else if(partidas[partida]['porcentaje'] >= 61 && partidas[partida]['porcentaje'] <= 99){
                                        var color = "yellow";
                                    }else{
                                        var color = "red";
                                    }
                                    $('#distribucionPresupuestaria').append(
                                            '<tr id="partidaPresupuestaria'+partida+'" style="background-color: '+color+'">' +
                                            '<td width="200">' +
                                            '<input type="text" style="font-size:11px;" class="form-control text-center"  value="'+partidas[partida]['partida']+'" readonly>' +
                                            '</td>' +
                                            '<td width="900">' +
                                            '<input type="text" style="font-size:11px;" class="form-control text-center"  value="'+partidas[partida]['descripcion']+'" readonly>' +
                                            '</td>' +
                                            '<td width="140" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center"  value="'+partidas[partida]['monto']+'" readonly></td>' +
                                            '</tr>'
                                    );
                                }
                            }
                        },'json');
            }else{
                $('#distribucionContable').remove();
                $('#distribucionContableTabla').append(
                        '<tbody style="height: 170px; display: inline-block; width: 100%; overflow: auto;" id="distribucionContable">'+

                        '</tbody>'
                );

                $('#distribucionPresupuestaria').remove();
                $('#distribucionPresupuestariaTabla').append(
                        '<tbody style="height: 170px ; display: inline-block; width: 100%; overflow: auto;" id="distribucionPresupuestaria">'+

                        '</tbody>'
                );

            }
        });



    });
</script>