<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <div id="mensajeAutorizacion" ></div>
                    {if !isset($autorizacion.fk_rhb001_num_empleado)}

                        <div class="card-head card-head-xs btn-danger text-center">
                            <header class="text-center">El Beneficiario Actual no Tiene
                                Monto Autorizado para Crear una Reposición de Caja Chica </header>
                        </div>

                    {/if}
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">

                        <form action="{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/nuevaCajaChicaMET" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idCajaChica}" id="idCajaChica" name="idCajaChica"/>
                            <input type="hidden" value="{$cuenta.cod_cuenta}" id="cod_cuenta"/>
                            <input type="hidden" value="{$cuenta.ind_descripcion}" id="ind_descripcion" />
                            <input type="hidden" value="{$cuenta20.cod_cuenta}" id="cod_cuenta20" />
                            <input type="hidden" value="{$cuenta20.ind_descripcion}" id="ind_descripcion20"/>
                            <input type="hidden" value="{$partida.cod_partida}" id="cod_partida"/>
                            <input type="hidden" value="{$partida.ind_denominacion}" id="ind_denominacion"/>


                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#informacionGeneral" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span></a></li>
                                    <li><a href="#detalle" data-toggle="tab"><span class="step">2</span> <span class="title">DETALLES</span></a></li>
                                    <li><a href="#distribucion" data-toggle="tab" id="ajaxDistribucion"><span class="step">3</span> <span class="title">DISTRIBUCION </span></a></li>
                                    <li><a href="#resumenCuentaContablePresupuestaria" data-toggle="tab"><span class="step">4</span> <span class="title"><BR>RESU.CONTABLE Y PRESUP.</span></a></li>

                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="informacionGeneral">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary text-center">
                                                    <header class="text-center">Información General</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="ind_num_caja_chica"
                                                                       class="control-label" style="margin-top: 10px;"> Caja Chica:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="ind_num_caja_chica"
                                                                       class="form-control"
                                                                       value="{if isset($cajaChicaBD.ind_num_caja_chica)}{$cajaChicaBD.ind_num_caja_chica}{/if}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="col-sm-5">
                                                                    <input type="text" class="form-control"
                                                                           id="fec_anio"
                                                                           style="font-weight:bold;"
                                                                           value="{if isset($cajaChicaBD.fec_anio)}{$cajaChicaBD.fec_anio}{else}{date('Y')}{/if}"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fk_rhb001_num_empleado_beneficiario"
                                                                       class="control-label" style="margin-top: 10px;">Beneficiario:</label>
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <div class="col-sm-10">
                                                                    <input type="hidden"
                                                                           name="form[int][fk_rhb001_num_empleado_beneficiario]"
                                                                           id="fk_rhb001_num_empleado_beneficiario"
                                                                           value="{if isset($cajaChicaBD.fk_rhb001_num_empleado_beneficiario)}{$cajaChicaBD.fk_rhb001_num_empleado_beneficiario}{else}{$empleado.pk_num_empleado}{/if}">

                                                                    <input type="text" class="form-control"
                                                                           id="nombreBenef"
                                                                           value="{if isset($cajaChicaBD.fk_rhb001_num_empleado_beneficiario)}{$cajaChicaBD.nombre_empleado}{else}{$empleado.nombreEmpleado}{/if}"
                                                                            {if $ver==1} disabled {/if}>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group floating-label">
                                                                        <button
                                                                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                                type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#formModal2"
                                                                                titulo="Listado de Personas"
                                                                                url="{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/empleadoMET/beneficiario/"
                                                                                {if $ver==1} disabled {/if}>
                                                                            <i class="md md-search"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fk_a004_num_dependencia"
                                                                       class="control-label" style="margin-top: 10px;"> Dependencia:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group"
                                                                     id="fk_a004_num_dependenciaError" style="margin-top: -10px;">
                                                                    <select name="form[int][fk_a004_num_dependencia]"
                                                                            class="form-control select2"
                                                                            data-placeholder="Seleccione la dependencia" {if $ver==1} disabled {/if}>
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=dep from=$dependencia}
                                                                            {if isset($cajaChicaBD.fk_a004_num_dependencia) and $cajaChicaBD.fk_a004_num_dependencia == $dep.pk_num_dependencia }
                                                                                <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                                                            {else}
                                                                                <option value="{$dep.pk_num_dependencia}" >{$dep.ind_dependencia}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fk_rhb001_num_empleado_persona_pagar"
                                                                       class="control-label" style="margin-top: 10px;">Cheque a Nombre:</label>
                                                            </div>
                                                            <div class="col-sm-9">
                                                                <div class="col-sm-10">
                                                                    <input type="hidden"
                                                                           name="form[int][fk_rhb001_num_empleado_persona_pagar]"
                                                                           id="fk_rhb001_num_empleado_persona_pagar"
                                                                           value="{if isset($cajaChicaBD.fk_rhb001_num_empleado_persona_pagar)}{$cajaChicaBD.fk_rhb001_num_empleado_persona_pagar}{else}{$empleado.pk_num_persona}{/if}">

                                                                    <input type="text" class="form-control"
                                                                           id="beneficiarioPagar"
                                                                           value="{if isset($cajaChicaBD.fk_rhb001_num_empleado_persona_pagar)}{$cajaChicaBD.nombrePagar}{else}{$empleado.nombreEmpleado}{/if}"
                                                                            {if $ver==1} disabled {/if}>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group floating-label">
                                                                        <button
                                                                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                                type="button"
                                                                                data-toggle="modal"
                                                                                data-target="#formModal2"
                                                                                titulo="Listado de Personas"
                                                                                url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/personaCajaCpagar"
                                                                                {if $ver==1} disabled {/if}>
                                                                            <i class="md md-search"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="obligacion"
                                                                       class="control-label" style="margin-top: 10px;"> Obligacion:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text" class="form-control"
                                                                       id="documentoProveedor"
                                                                       value="{if isset($cajaChicaBD.cod_tipo_documento)}{$cajaChicaBD.cod_tipo_documento}{/if}"
                                                                       disabled="disabled">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-8">
                                                                    <input type="text" class="form-control"
                                                                           id="nombreProveedor2"
                                                                           value="{if isset($cajaChicaBD.ind_nro_factura)}{$cajaChicaBD.ind_nro_factura}{/if}"
                                                                           disabled="disabled">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="fk_cpb004_num_clasificacion_gastos"
                                                                       class="control-label" style="margin-top: 10px;"> Clasificacion:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group"
                                                                     id="fk_cpb004_num_clasificacion_gastosError" style="margin-top: -10px;">
                                                                    <select name="form[int][fk_cpb004_num_clasificacion_gastos]"
                                                                            class="form-control select2"
                                                                            data-placeholder="Seleccione la Clasificacion de Gastos"  {if $ver==1} disabled {/if}>
                                                                        <option value="">Seleccione..</option>
                                                                        {foreach item=clasif from=$clasifGasto}
                                                                            {if isset($cajaChicaBD.fk_cpb004_num_clasificacion_gastos) and $cajaChicaBD.fk_cpb004_num_clasificacion_gastos == $clasif.pk_num_clasificacion_gastos }
                                                                                <option value="{$clasif.pk_num_clasificacion_gastos}" selected>{$clasif.ind_descripcion}</option>
                                                                            {else}
                                                                                <option value="{$clasif.pk_num_clasificacion_gastos}">{$clasif.ind_descripcion}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-12">
                                                                <div class="col-sm-3 text-right">
                                                                    <label for="docInterno"
                                                                           class="control-label" style="margin-top: 10px;"> Nro. Doc. Interno:</label>
                                                                </div>
                                                                <div class="col-sm-6" style="margin-top: -10px;">
                                                                    <div class="form-group floating-label" id="docInternoError">
                                                                        <input type="text" name="form[alphaNum][docInterno]"  id="docInterno" class="form-control" value="{if isset($cajaChicaBD.ind_nro_factura)}{$cajaChicaBD.ind_nro_factura}{/if}" disabled>
                                                                        <label for="docInterno" class="control-label"></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="tipoPago"
                                                                       class="control-label" style="margin-top: 10px;"> Tipo de Pago:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group"
                                                                     id="fk_a006_num_miscelaneo_detalle_tipo_pagoError" style="margin-top: -10px;">
                                                                    <select name="form[int][fk_a006_num_miscelaneo_detalle_tipo_pago]"
                                                                            class="form-control select2-list select2" required
                                                                            data-placeholder="Seleccione Tipo de Pago"  {if $ver==1} disabled {/if}>
                                                                        <option value="">Seleccione...</option>
                                                                        {foreach item=tipoPago from=$listadoTipoPago}
                                                                            {if isset($cajaChicaBD.fk_a006_num_miscelaneo_detalle_tipo_pago) and $cajaChicaBD.fk_a006_num_miscelaneo_detalle_tipo_pago == $tipoPago.pk_num_miscelaneo_detalle}
                                                                                <option value="{$tipoPago.pk_num_miscelaneo_detalle}" selected>{$tipoPago.ind_nombre_detalle}</option>
                                                                            {else}
                                                                                <option value="{$tipoPago.pk_num_miscelaneo_detalle}">{$tipoPago.ind_nombre_detalle}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-8">
                                                                <div class="col-sm-6 text-right">
                                                                   <label for="fk_a023_num_centro_costo"
                                                                          class="control-label"  style="margin-top: 10px;">Centro de
                                                                      Costo:</label>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <input type="hidden"
                                                                           name="form[int][fk_a023_num_centro_costo]"
                                                                           id="fk_a023_num_centro_de_costo"
                                                                           value="{if isset($cajaChicaBD.fk_a023_num_centro_costo)}{$cajaChicaBD.fk_a023_num_centro_costo}{/if}">
                                                                    <input type="text"
                                                                           class="form-control"
                                                                           id="centroCosto"
                                                                           value="{if isset($cajaChicaBD.fk_a023_num_centro_costo)}{$cajaChicaBD.fk_a023_num_centro_costo}{/if}"
                                                                           readonly>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group floating-label">
                                                                        <button
                                                                                type="button"
                                                                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                                data-toggle="modal"
                                                                                data-target="#formModal2"
                                                                                titulo="Listado Centro de Costo"
                                                                                url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET/cajaChicaCC"
                                                                                {if $ver==1} disabled {/if}>
                                                                            <i class="md md-search"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="num_monto_total"
                                                                       class="control-label" style="margin-top: 10px;"> Monto a Reembolsar:</label>
                                                            </div>
                                                            <div class="col-sm-6" style="margin-top: -10px;">
                                                                <div class="form-group floating-label" id="num_monto_totalError">
                                                                    <input type="text" name="form[int][num_monto_total]"  id="num_monto_total" value="{if isset($cajaChicaBD.num_monto_total)}{$cajaChicaBD.num_monto_total}{/if}" class="form-control" size="50%" {if $ver==1} disabled {/if}>
                                                                    <label for="num_monto_total" class="control-label"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="ind_estado" class="control-label" style="margin-top: 10px;">
                                                                    Estado:</label>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" id="ind_estado" class="form-control"
                                                                       style="font-weight:bold;"
                                                                       readonly {if isset($cajaChicaBD.ind_estado) and $cajaChicaBD.ind_estado=='AP'} placeholder="Aprobado"
                                                                                {elseif isset($cajaChicaBD.ind_estado) and $cajaChicaBD.ind_estado=='AN'} placeholder="Anulado"
                                                                                {else} placeholder="En Preparacion"
                                                                                {/if} >
                                                                <label for="ind_estado" class="control-label"></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="montoAutorizado"
                                                                       class="control-label" style="margin-top: 10px;"> Monto Autorizado:</label>
                                                            </div>
                                                            <div class="col-sm-6" style="margin-top: -10px;">
                                                                <div class="form-group floating-label" id="montoAutorizadoError">
                                                                    <input type="text" class="form-control" size="50%" disabled
                                                                           id="montoAutorizado"
                                                                            value="{if $autorizacion.num_monto_autorizado}{$autorizacion.num_monto_autorizado}{/if}">
                                                                    <label for="montoAutorizado" class="control-label"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_preparaError">
                                                                <input type="text" class="form-control" value="{if isset($cajaChicaBD.fk_rhb001_num_empleado_prepara)}{$cajaChicaBD.EMPLEADO_PREPARA}{/if}" name="form[int][fk_rhb001_num_empleado_prepara]" disabled id="fk_rhb001_num_empleado_prepara">
                                                                <label for="fk_rhb001_num_empleado_prepara"><i class="md md-person"></i> Preparado Por</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fec_preparacionError">
                                                                <input type="text" class="form-control" value="{if isset($cajaChicaBD.fec_preparacion)}{$cajaChicaBD.fec_preparacion}{/if}" disabled name="form[alphaNum][fec_preparacion]" id="fec_preparacion">
                                                                <label for="fec_preparacion"><i class="md md-today"></i> Fecha</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                                                                <input type="text" class="form-control" value="{if isset($cajaChicaBD.fk_rhb001_num_empleado_aprueba)}{$cajaChicaBD.EMPLEADO_APRUEBA}{/if}" name="form[int][fk_rhb001_num_empleado_aprueba]" disabled id="fk_rhb001_num_empleado_aprueba">
                                                                <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Aprobado Por</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="fec_aprobacionError">
                                                                <input type="text" class="form-control" value="{if isset($cajaChicaBD.fec_aprobacion)}{$cajaChicaBD.fec_aprobacion}{/if}" disabled name="form[alphaNum][fec_aprobacion]" id="fec_aprobacion">
                                                                <label for="fec_aprobacion"><i class="md md-today"></i> Fecha</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group floating-label" id="ind_descripcionError">
                                                            <textarea id="ind_descripcion" name="form[alphaNum][ind_descripcion]" class="form-control" rows="2" placeholder=""  {if $ver==1} disabled {/if} >{if isset($cajaChicaBD.ind_descripcion)}{$cajaChicaBD.ind_descripcion}{/if}</textarea>
                                                            <label for="ind_descripcion"><i class="md md-comment"></i> Descripcion:</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="form-group floating-label" id="ind_razon_rechazoError">
                                                            <textarea id="ind_razon_rechazo" name="form[alphaNum][ind_razon_rechazo]" class="form-control" rows="2" placeholder="" >{if isset($cajaChicaBD.ind_razon_rechazo)}{$cajaChicaBD.ind_razon_rechazo}{/if}</textarea>
                                                            <label for="ind_razon_rechazo"><i class="md md-comment"></i> Razon Rechazo:</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="detalle">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card-body" style="padding: 4px;">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12">
                                                        <div class="card">
                                                            <div class="card-head card-head-xs style-primary" align="center">
                                                                <header>Detalles de la Reposición</header>
                                                            </div>
                                                            <tr class="card-body" style="padding: 4px;">
                                                                <div id="imput_nuevo"></div>

                                                                <table id="reposicion" class="table table-striped table-hover" style="width: 200%;display:block;overflow: auto;">
                                                                    <thead style="display: inline-block; width: 200%;">
                                                                    <tr>
                                                                        <th width="50">#</th>
                                                                        <th class="text-center" colspan="2" width="150">Fecha Documento</th>
                                                                        <th class="text-center" width="300">Concepto</th>
                                                                        <th class="text-center" width="400">Descripción</th>
                                                                        <th class="text-center" width="150">Monto Pagado</th>
                                                                        <th class="text-center" width="300">Tipo Impuesto</th>
                                                                        <th class="text-center" width="300">Tipo Servicio</th>
                                                                        <th class="text-center" width="150">Monto Afecto</th>
                                                                        <th class="text-center" width="150">Monto No Afecto</th>
                                                                        <th class="text-center" width="150">Monto Impuesto</th>
                                                                        <th class="text-center" width="150">Monto Retención</th>
                                                                        <th class="text-center" width="100">Documento</th>
                                                                        <th class="text-center" width="120"></th>
                                                                        <th class="text-center" width="150">Factura</th>
                                                                        <th class="text-center" width="150">R.I.F.</th>
                                                                        <th class="text-center" width="300">Persona</th>
                                                                        <th class="text-center" width="80"> Acciones</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody style="height: 300px;display: inline-block;width: 200%;overflow: auto;" id="tbodyReposicion" >
                                                                    {if isset($reposicion)}
                                                                    {foreach item=i from=$reposicion}
                                                                        <tr id="codReposicion{$i.num_secuencia}">
                                                                        <input type="hidden" class="ajaxReposicion" value="{$i.num_secuencia}" name="form[int][detalleReposicion][num_secuencia][{$i.num_secuencia}]">
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}CUENTA" value="{$i.fk_cbb004_num_plan_cuenta}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_cbb004_num_plan_cuenta]">
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}CUENTA20" value="{$i.fk_cbb004_num_plan_cuentaPub20}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_cbb004_num_plan_cuentaPub20]">
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}PARTIDA" value="{$i.fk_prb002_num_partida_presupuestaria}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_prb002_num_partida_presupuestaria]">
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}CONCEPTO" value="{$i.fk_cpb005_num_concepto_gasto}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_cpb005_num_concepto_gasto]" >
                                                                        <input type="hidden" id="codReposicion{$i.num_secuencia}pkPersona" value="{$i.pk_num_persona}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_rhb001_num_empleado_proveedor]">
                                                                        <input type="hidden" value="{$i.pk_num_caja_chica_detalle}" name="form[int][detalleReposicion][{$i.num_secuencia}][pk_num_caja_chica_detalle]" >
                                                                        <input type="hidden" value="{$i.regimen}" id="regimen" >
                                                                        <input type="hidden" value="{$i.ivaPorcentaje}" id="ivaPorcentaje" >

                                                                        <td width="50" style="vertical-align: middle;"><input type="text"  class="form-control text-center" value="{$i.num_secuencia}" readonly ></td>
                                                                        <td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codReposicion{$i.num_secuencia}fec_doc" value="{$i.fec_documento}" readonly></td>

                                                                        <td width="300">
                                                                            <input type="text" class="form-control text-center concepto accionModal2" id="codReposicion{$i.num_secuencia}concepto" value="{$i.nombreConcepto}" readonly
                                                                            data-toggle="modal"
                                                                            data-target="#formModal2"
                                                                            titulo="Listado de Concepto"
                                                                            enlace="{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/conceptoMET/conceptoDet/{$i.num_secuencia}" >
                                                                        </>

                                                                        <td width="400" style="vertical-align: middle;"><textarea id="codReposicion{$i.num_secuencia}descripcion" class="form-control ajaxReposicion" rows="2" name="form[txt][detallesReposicion][{$i.num_secuencia}][ind_descripcion]"{if $ver==1}disabled{/if}>{$i.descripcionDetalle}</textarea></td>
                                                                        <td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-right montoPagado ajaxReposicion" idTR="codReposicion{$i.num_secuencia}" id="codReposicion{$i.num_secuencia}montoPagado" value="{$i.num_monto_pagado}" name="form[int][detalleReposicion][{$i.num_secuencia}][num_monto_pagado]" {if $ver==1}disabled{/if}></td>
                                                                        <td width="300"><select class="form-control regimen select2-list select2" id="codReposicion{$i.num_secuencia}tipoImpuesto" idTR="codReposicion{$i.num_secuencia}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_a006_num_miscelaneo_regimen_fiscal]"
                                                                            enlace="{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/listaServicioMET/codReposicion{$i.num_secuencia}tipoServicio" {if $ver==1}disabled{/if}>
                                                                            <option value="">Seleccione...</option>
                                                                            {foreach item=reg from=$listadoRegimen}
                                                                                {if isset($i.fk_a006_num_miscelaneo_regimen_fiscal) and $i.fk_a006_num_miscelaneo_regimen_fiscal == $reg.pk_num_miscelaneo_detalle}
                                                                                    <option value="{$reg.pk_num_miscelaneo_detalle}" selected>{$reg.ind_nombre_detalle}</option>
                                                                                {else}
                                                                                    <option value="{$reg.pk_num_miscelaneo_detalle}">{$reg.ind_nombre_detalle}</option>
                                                                                {/if}
                                                                            {/foreach}
                                                                            </select></td>
                                                                        <td width="300"><select class="form-control select2-list select2 servicio" id="codReposicion{$i.num_secuencia}tipoServicio" idTR="codReposicion{$i.num_secuencia}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_cpb017_num_tipo_servicio]" >
                                                                                <option value="">Seleccione..</option>
                                                                            {foreach item=serv from=$servicios}
                                                                                {if isset($i.fk_cpb017_num_tipo_servicio) and $i.fk_cpb017_num_tipo_servicio == $serv.pk_num_tipo_servico}
                                                                                <option value="{$serv.pk_num_tipo_servico}" selected>{$serv.ind_descripcion}</option>
                                                                                {/if}
                                                                            {/foreach}
                                                                                </select></td>
                                                                        <td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-right montoAfecto" idTR="codReposicion{$i.num_secuencia}" id="codReposicion{$i.num_secuencia}montoAfecto" value="{$i.num_monto_afecto}" name="form[int][detalleReposicion][{$i.num_secuencia}][num_monto_afecto]" {if $ver==1}disabled{/if}></td>
                                                                        <td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-right montoNoAfecto" idTR="codReposicion{$i.num_secuencia}" id="codReposicion{$i.num_secuencia}montoNoAfecto" value="{$i.num_monto_no_afecto}" name="form[int][detalleReposicion][{$i.num_secuencia}][num_monto_no_afecto]" {if $ver==1}disabled{/if}></td>
                                                                        <td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-right montoImpuesto" id="codReposicion{$i.num_secuencia}montoImpuesto" value="{$i.num_monto_impuesto}" name="form[int][detalleReposicion][{$i.num_secuencia}][num_monto_impuesto]" readonly></td>
                                                                        <td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-right" id="codReposicion{$i.num_secuencia}montoRetencion" value="{$i.num_monto_retencion}" name="form[int][detalleReposicion][{$i.num_secuencia}][num_monto_retencion]" readonly></td>
                                                                        <td width="100"><select class="form-control select2-list select2" id="codReposicion{$i.num_secuencia}fk_cpb002_num_tipo_documento" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_cpb002_num_tipo_documento]" {if $ver==1}disabled{/if}>
                                                                                <option value="">Seleccione...</option>
                                                                                {foreach item=doc from=$documento}
                                                                                    {if isset($i.fk_cpb002_num_tipo_documento) and $i.fk_cpb002_num_tipo_documento == $doc.pk_num_tipo_documento}
                                                                                        <option value="{$doc.pk_num_tipo_documento}" selected>{$doc.ind_descripcion}</option>
                                                                                    {else}
                                                                                        <option value="{$doc.pk_num_tipo_documento}">{$doc.ind_descripcion}</option>
                                                                                    {/if}
                                                                                {/foreach}
                                                                                </select>
                                                                            <td width="120" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codReposicion{$i.num_secuencia}num_documento" value="{$i.num_documento}" name="form[int][detalleReposicion][{$i.num_secuencia}][num_documento]" {if $ver==1}disabled{/if}>
                                                                            </td>
                                                                        <td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codReposicion{$i.num_secuencia}ind_num_recibo" value="{$i.ind_num_recibo}" name="form[int][detalleReposicion][{$i.num_secuencia}][ind_num_recibo]" {if $ver==1}disabled{/if}></td>
                                                                        <td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codReposicion{$i.num_secuencia}rif" value="{$i.ind_documento_fiscal}" name="form[int][detalleReposicion][{$i.num_secuencia}][ind_doc_fiscal]" {if $ver==1}disabled{/if}></td>
                                                                        <td width="350" style="vertical-align: middle;">
                                                                            <input type="text" class="form-control text-center accionModal2" id="codReposicion{$i.num_secuencia}persona" value="{if isset($i.pk_num_persona)}{$i.persona}{/if}"
                                                                            data-toggle="modal"
                                                                            data-target="#formModal2"
                                                                            titulo="Listado de Persona"
                                                                            enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/personaConcepto/codReposicion{$i.num_secuencia}"  {if $ver==1}disabled{/if}>
                                                                            </td>
                                                                        <td width="120" style="vertical-align: middle;">
                                                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$i.num_secuencia}"  {if $ver==1}disabled{/if}><i class="md md-delete"></i></button>
                                                                        </td>
                                                                        </tr>

                                                                    {/foreach}
                                                                    {/if}
                                                                    </tbody>
                                                                </table>

                                                                <div class="col-sm-12" align="center">

                                                                    <label id="totalImpuesto" class="text-right" montoAfecto="0" ivaPorcentaje="0" signo="1" regimen="" regimenServ="" imponible="" ></label>

                                                                    <td width="10px">
                                                                    <button
                                                                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                            type="button"
                                                                            id="agregarDetalleR"
                                                                            restriccion="CC"
                                                                            mensaje="Debe Seleccionar un Centro de Costo"
                                                                            titulo="SELECCIONE CONCEPTO DE GASTO"
                                                                            idTabla="reposicion"
                                                                            url="{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/conceptoMET/concepto"
                                                                            {if $ver==1} disabled {/if}>
                                                                        <i class="md md-add"></i> AGREGAR CONCEPTO
                                                                    </button>
                                                                </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="distribucion">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-head card-head-xs style-primary" align="center">
                                                        <header>Distribucion</header>
                                                    </div>
                                                    <div class="card-body" style="padding: 4px;">
                                                        <div id="imput_nuevo"></div>

                                                        <table id="concepto" class="table table-striped table-hover" style="width: 100%;display:block;">
                                                            <thead style="display: inline-block;width: 100%;">
                                                                <tr>
                                                                    <th width="35">#</th>
                                                                    <th class="text-right" colspan="2" width="100">Partida </th>
                                                                    <th class="text-right" colspan="2" width="300"></th>
                                                                    <th class="text-right" colspan="2" width="100">Cta. Contable</th>
                                                                    <th class="text-right" colspan="2" width="300"></th>
                                                                    <th class="text-right" width="35">C.Costo</th>
                                                                    <th class="text-center" width="150">Monto</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody style="height: 300px;display: inline-block;width: 100%;overflow: auto;" id="distribucionContable">
                                                                {if isset($reposicion)}
                                                                {foreach item=i from=$reposicion}
                                                                    <tr id="cuentaContablea{$i.num_secuencia}">

                                                                        <input type="hidden" class="ajaxReposicion" value="{$i.num_secuencia}" name="form[int][detalleReposicion][num_secuencia][{$i.num_secuencia}]">
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}CUENTA" value="{$i.fk_cbb004_num_plan_cuenta}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_cbb004_num_plan_cuenta]">
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}CUENTA20" value="{$i.fk_cbb004_num_plan_cuentaPub20}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_cbb004_num_plan_cuentaPub20]">
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}PARTIDA" value="{$i.fk_prb002_num_partida_presupuestaria}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_prb002_num_partida_presupuestaria]">
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}COD_PARTIDA" value="{$i.cod_partida}" >
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}NOMBRE_PARTIDA" value="{$i.ind_denominacion}" >
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}COD_CUENTA" value="{$i.cod_cuenta}" >
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}COD_CUENTA20" value="{$i.cod_cuenta20}" >
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}NOMBRE_CUENTA" value="{$i.nombreCuenta}" >
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}NOMBRE_CUENTA20" value="{$i.nombreCuenta20}" >
                                                                        <input type="hidden" class="ajaxReposicion" id="codReposicion{$i.num_secuencia}CONCEPTO" value="{$i.fk_cpb005_num_concepto_gasto}" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_cpb005_num_concepto_gasto]" >
                                                                        <!--input type="hidden" id="codReposicion{$i.num_secuencia}pkPersona" name="form[int][detalleReposicion][{$i.num_secuencia}][fk_rhb001_num_empleado_proveedor]"-->

                                                                        <td width="35" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center"  value="{$i.num_secuencia}" readonly></td>
                                                                        <td width="100" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="codPartida" value="{$i.cod_partida}" readonly></td>
                                                                        <td width="300" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="nombrePartida" value="{$i.ind_denominacion}" readonly></td>
                                                                        <td width="100">
                                                                            <input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta" value="{$i.cod_cuenta}" readonly>
                                                                            <input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta" value="{$i.cod_cuenta20}" readonly>
                                                                        </td>
                                                                        <td width="300">
                                                                            <input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="{$i.nombreCuenta}" readonly>
                                                                            <input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="{$i.nombreCuenta20}" readonly>
                                                                        </td>
                                                                        <td width="35" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="centroCosto" value="{$i.fk_a023_num_centro_costo}" readonly></td>
                                                                        <td width="150" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="" value="{$i.monto}" readonly></td>
                                                                    </tr>
                                                                {/foreach}
                                                                    {if $i.num_monto_impuesto > 0}
                                                                        <tr id="cuentaContableImpuesto">
                                                                            <td width="35" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center"  value="" readonly></td>
                                                                            <td width="100" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="codPartida" value="{$partida.cod_partida}" readonly></td>
                                                                            <td width="300" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="nombrePartida" value="{$partida.ind_denominacion}" readonly></td>
                                                                            <td width="100">
                                                                                <input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta" value="{$cuenta.cod_cuenta}" readonly>
                                                                                <input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta20" value="{$cuenta20.cod_cuenta}" readonly>
                                                                            </td>
                                                                            <td width="300">
                                                                                <input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="{$cuenta.ind_descripcion}" readonly>
                                                                                <input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta20" value="{$cuenta20.ind_descripcion}" readonly>
                                                                            </td>
                                                                            <td width="35" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="centroCosto" value="" readonly></td>
                                                                            <td width="150" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoImp" value="{$i.num_monto_impuesto}" readonly></td>
                                                                        </tr>
                                                                    {/if}
                                                                {/if}

n$2

                                                            </tbody>
                                                            <tfoot>
                                                            <tr>
                                                                <th width="35"></th>
                                                                <th class="text-right" colspan="2" width="100"></th>
                                                                <th class="text-right" colspan="2" width="300"></th>
                                                                <th class="text-right" colspan="2" width="100"></th>
                                                                <th class="text-right" colspan="2" width="300"></th>
                                                                <th class="text-right" width="35"></th>
                                                                <th width="150"><label>Total: </label><label id="total" style="width: 76%;" class="text-right">{if isset($cajaChicaBD.num_monto_total)}{$cajaChicaBD.num_monto_total}{/if}</label></th>

                                                            </tr>

                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="resumenCuentaContablePresupuestaria">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary" align="center">
                                                    <header>Distribución Contable</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <table id="" class="table table-striped table-hover" style="width: 100%;display:block;">
                                                        <thead style="display: inline-block;width: 100%;">
                                                        <tr>
                                                            <th width="200">Cuenta </th>
                                                            <th width="900">Descripcion</th>
                                                            <th width="140">Monto</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody style="height: 170px;display: inline-block;width: 100%;overflow: auto;" id="distribucionContableCuentas">
                                                        {if isset($reposicion)}
                                                            {foreach item=i from=$reposicion}
                                                               <tr id="cuentaContableCuentas{$i.num_secuencia}">
                                                                   <td width="200">
                                                                       <input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta" value="{$i.cod_cuenta}" readonly>
                                                                       <input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta20" value="{$i.cod_cuenta20}" readonly>
                                                                   </td>
                                                                   <td width="900">
                                                                       <input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="{$i.nombreCuenta}" readonly>
                                                                       <input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta20" value="{$i.nombreCuenta20}" readonly>
                                                                   </td>
                                                                   <td width="150" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfecto" value="{$i.monto}" readonly></td>
                                                               </tr>
                                                            {/foreach}
                                                            {if $i.num_monto_impuesto > 0}
                                                                <tr id="cuentaContableCuentasImpuesto">
                                                                    <td width="200">
                                                                        <input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta" value="{$cuenta.cod_cuenta}" readonly>
                                                                        <input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta20" value="{$cuenta20.cod_cuenta}" readonly>
                                                                    </td>
                                                                    <td width="900">
                                                                        <input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="{$cuenta.ind_descripcion}" readonly>
                                                                        <input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta20" value="{$cuenta20.ind_descripcion}" readonly>
                                                                    </td>
                                                                    <td width="150" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfecto" value="{$i.num_monto_impuesto}" readonly></td>
                                                                </tr>
                                                            {/if}
                                                        {/if}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head card-head-xs style-primary" align="center">
                                                    <header>Distribución Presupuestaria</header>
                                                </div>
                                                <div class="card-body" style="padding: 4px;">
                                                    <table id="datatable" class="table table-striped table-hover" style="width: 100%;display:block;" >
                                                        <thead style="display: inline-block;width: 100%;">
                                                        <tr>
                                                            <th width="200">Partida </th>
                                                            <th width="900">Descripción</th>
                                                            <th width="140">Monto</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody style="height: 170px;display: inline-block;width: 100%;overflow: auto;" id="distribucionContablePartidas">
                                                        {if isset($reposicion)}
                                                            {foreach item=i from=$reposicion}
                                                                <tr id="cuentaContablePartidas{$i.num_secuencia}">
                                                                    <td width="200"><input type="text" style="font-size:11px;" class="form-control text-center" id="codPartida" value="{$i.cod_partida}" readonly></td>
                                                                    <td width="900"><input type="text" style="font-size:11px;" class="form-control text-center" id="nombrePartida" value="{$i.ind_denominacion}" readonly></td>
                                                                    <td width="150"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfecto" value="{$i.monto}" readonly></td>
                                                                </tr>
                                                            {/foreach}
                                                            {if $i.num_monto_impuesto > 0}
                                                                <tr id="cuentaContablePartidasImpuesto">
                                                                    <td width="200"><input type="text" style="font-size:11px;" class="form-control text-center" id="codPartida" value="{$partida.cod_partida}" readonly></td>
                                                                    <td width="900"><input type="text" style="font-size:11px;" class="form-control text-center" id="nombrePartida" value="{$partida.ind_denominacion}" readonly></td>
                                                                    <td width="150"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfecto" value="{$i.num_monto_impuesto}" readonly></td>
                                                                </tr>
                                                            {/if}
                                                        {/if}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <ul class="pager wizard">
                                    <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                    <li class="next last"><a class="btn-raised" href="javascript:void(0);">Ultimo</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Próximo</a></li>
                                </ul>
                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group floating-label">
                                                    <input type="text" disabled class="form-control disabled"
                                                           value="{if isset($cajaChicaBD.ind_usuario)}{$cajaChicaBD.ind_usuario}{/if}"
                                                           id="ind_usuario">
                                                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="col-sm-12">
                                            <div class="form-group floating-label" id="cod_tipo_nominaError">
                                                <input type="text" disabled class="form-control disabled"
                                                       value="{if isset($cajaChicaBD.fec_ultima_modificacion)}{$cajaChicaBD.fec_ultima_modificacion}{/if}"
                                                       id="fec_ultima_modificacion">
                                                <label for="cod_tipo_nomina"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal "
            descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
        <i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
    </button>

    {if isset($estado)}
        {if $estado == 'PR'}
            <button type="button" class="btn btn-primary accionesEstado logsUsuarioModal" id="PR">
                <i class="icm icm-rating3"></i>&nbsp;Aprobar</button>
        {elseif $estado == 'AN'}
            <button type="button" class="btn btn-danger accionesEstado logsUsuarioModal" id="AN">
                <i class="icm icm-blocked"></i>&nbsp;Anular</button>
        {/if}
    {else}
        {if isset($autorizacion.fk_rhb001_num_empleado)}
            {if $ver!=1}
                <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">
                    {if $idCajaChica!=0}
                        <i class="fa fa-edit"></i>&nbsp;Modificar
                    {else}
                        <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                    {/if}
            {/if}
        {/if}
    {/if}
</div>

<script type="text/javascript">
    // funciones

    function calculoMonto(idTR,campo=false, valor=false) {
        if (valor == 0 ) {
            limpiarMontosCajaChica(idTR);
        } else {
            if (campo == 'montoPagado') {
                calculoMontoPagado(idTR);

            }
            if (campo == 'montoAfecto') {
                calculoMontoAfecto(idTR);
            }
        }
    }

    function calculoMontoPagado(idTR){
        var montoPagado = 0;
        var montoAfecto = 0;
        var montoNoAfecto = 0;
        var montoImpuesto = 0;
        var montoRetencion = 0;
        //iva
        var ivaPorcentaje = parseFloat($('#totalImpuesto').attr('ivaPorcentaje'));
        var regimen=$('#totalImpuesto').attr('regimen');
        if(regimen == ''){
            regimen = $('#regimen').val();
            ivaPorcentaje = $('#ivaPorcentaje').val();
        }
        var regimenServ=$('#totalImpuesto').attr('regimenServ');
        var imponible=$('#totalImpuesto').attr('imponible');
        var signoImpuesto=$('#totalImpuesto').attr('signo');
        var valor = $('#'+idTR+'montoPagado').val();

//        $('.montoPagado').each(function( titulo, valor ) {
            if (valor != '') {
                if (parseFloat(valor)) {
                    if (regimen == 'I' || regimen == 'R') {
                        montoPagado = parseFloat(valor);
                        montoAfecto =  montoPagado /  ((ivaPorcentaje / 100)+1);
                        montoImpuesto = parseFloat(montoAfecto) * (ivaPorcentaje / 100);

                        if (signoImpuesto == 2) {
                            montoImpuesto *= (-1);
                        }
                    }
                    if(imponible=='N'){
                        montoRetencion = montoAfecto * (ivaPorcentaje / 100);
                    }
                    else if(imponible=='I'){
                        montoRetencion = montoImpuesto * (ivaPorcentaje / 100);
                    }
                    if(signoImpuesto == 2){
                        montoRetencion *= (-1);
                    }
                    /*if(regimenServ == 'N'){
                        montoAfecto = 0;
                        montoPagado = parseFloat($(this).val());
                        montoNoAfecto = montoPagado;
                    }*/
                }
            }

        $(document.getElementById(idTR +'montoAfecto')).attr('value',montoAfecto.toFixed(2));
        $(document.getElementById(idTR +'montoNoAfecto')).attr('value',montoNoAfecto.toFixed(2));
        $(document.getElementById(idTR +'montoImpuesto')).attr('value',montoImpuesto.toFixed(2));

    }

    function calculoMontoAfecto(idTR){

       var montoPagado = 0;
       var montoAfecto = 0;
       var montoNoAfecto = 0;
       var montoImpuesto = 0;
       var montoRetencion = 0;
       var ivaPorcentaje = parseFloat($('#totalImpuesto').attr('ivaPorcentaje'));
       var signoImpuesto=$('#totalImpuesto').attr('signo');
       var imponible=$('#totalImpuesto').attr('imponible');
       var regimen=$('#totalImpuesto').attr('regimen');

       $('.montoAfecto').each(function( titulo, valor ){
           if($(this).val()!=''){

               montoAfecto = parseFloat($(this).val());
               montoImpuesto = parseFloat(montoAfecto) * (ivaPorcentaje / 100);

               //porcentaje de la retencion
               var idServ = $(document.getElementById($(this).attr('idTR') +'tipoServicio')).val();
               $.post('{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/impuestoMET/RETENCION', {
                   idTipoServicio: idServ
               }, function (dato) {
                   if(dato){
                      var retencionPorcentaje = dato['num_factor_porcentaje'];
                      var signoRetencion = dato['ind_signo'];
                   }
                   montoRetencion = montoImpuesto * (retencionPorcentaje / 100);
                   //alert('montoRetencion'+montoRetencion);
                   if(signoRetencion == 2){
                       montoRetencion *= (-1);
                   }
                   montoPagado = montoAfecto + montoImpuesto + montoRetencion;
                  // alert('montoPagado',montoPagado);
                   $(document.getElementById(idTR +'montoRetencion')).attr('value',montoRetencion);
                   $(document.getElementById(idTR +'montoPagado')).attr('value',montoPagado);
               },'json');

           }

       });
      // $(document.getElementById(idTR +'montoPagado')).attr('value',montoPagado);
       $(document.getElementById(idTR +'montoAfecto')).attr('value',montoAfecto.toFixed(2));
       $(document.getElementById(idTR +'montoNoAfecto')).attr('value',montoNoAfecto.toFixed(2));
       $(document.getElementById(idTR +'montoImpuesto')).attr('value',montoImpuesto.toFixed(2));
      // $(document.getElementById(idTR +'montoRetencion')).attr('value',montoRetencion);
    }

    function limpiarMontosCajaChica(idTR){

        var montoPagado = 0;
        var montoAfecto = 0;
        var montoNoAfecto = 0;
        var montoImpuesto = 0;
        var montoRetencion = 0;

        $(document.getElementById(idTR +'montoPagado')).attr('value',montoPagado);
        $(document.getElementById(idTR +'montoAfecto')).attr('value',montoAfecto);
        $(document.getElementById(idTR +'montoNoAfecto')).attr('value',montoNoAfecto);
        $(document.getElementById(idTR +'montoImpuesto')).attr('value',montoImpuesto);
        $(document.getElementById(idTR +'montoRetencion')).attr('value',montoRetencion);

    }

    function distribucionCajaChica(){
        var numTrMax = $("#reposicion > tbody > tr").length;
        var conceptoAn;
        if( numTrMax > 0) {
            var total = 0;
            var impuesto = 0;

            for(var i = 1; i <= numTrMax ; i++){
                if(i>1){
                var an= i-1;
                    conceptoAn = $('#cuentaContable'+an+' #codReposicion'+ an + 'CONCEPTO').val();
                }

                var codPartida = $('#cuentaContable'+i+' #codReposicion' + i + 'COD_PARTIDA').val();
                var nombrePartida = $('#cuentaContable'+i+' #codReposicion'+ i + 'NOMBRE_PARTIDA').val();
                var codCuenta = $('#cuentaContable'+i+' #codReposicion'+ i + 'COD_CUENTA').val();
                var codCuenta20 = $('#cuentaContable'+i+' #codReposicion'+ i + 'COD_CUENTA20').val();
                var nombreCuenta = $('#cuentaContable'+i+' #codReposicion'+ i + 'NOMBRE_CUENTA').val();
                var nombreCuenta20 = $('#cuentaContable'+i+' #codReposicion'+ i + 'NOMBRE_CUENTA20').val();
                var centroCosto = $('#fk_a023_num_centro_de_costo').val();
                var montoAfecto = $('#codReposicion'+i+'montoAfecto').val();
                var montoPagado = $('#codReposicion'+i+'montoPagado').val();
                var montoImpuesto = $('#codReposicion'+ i + 'montoImpuesto').val();

                var concepto = $('#cuentaContable'+i+' #codReposicion'+ i + 'CONCEPTO').val();

                total = parseFloat(total) + parseFloat(montoPagado);
                impuesto = parseFloat(impuesto) + parseFloat(montoImpuesto);


//                $('#total').attr('value', total.toFixed(2));
                $('#num_monto_total').attr('value', total.toFixed(2));
                $('#total').html(total.toFixed(2));


                if(conceptoAn == concepto){
                    montoAfecto = parseFloat(montoAfecto) + parseFloat(montoAfecto);
                    $('#montoAfecto').attr('value', montoAfecto);
                    $('#montoAfectoCuenta').attr('value', montoAfecto);
                    $('#montoAfectoPart').attr('value', montoAfecto);
                }else{
                    $('tbody #cuentaContable'+i).html(
                        '<td style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center"  value="'+i+'" readonly></td>' +
                        '<td style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-left" id="codReposicion' + i + 'COD_PARTIDA" value="'+codPartida+'" readonly></td>' +
                        '<td style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-left" id="codReposicion' + i + 'NOMBRE_PARTIDA" value="'+nombrePartida+'" readonly></td>' +
                        '<td >' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codReposicion' + i + 'COD_CUENTA" value="'+codCuenta+'" readonly>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codReposicion' + i + 'COD_CUENTA20" value="'+codCuenta20+'" readonly>' +
                        '</td>' +
                        '<td>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codReposicion' + i + 'NOMBRE_CUENTA" value="'+nombreCuenta+'" readonly>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codReposicion' + i + 'NOMBRE_CUENTA20" value="'+nombreCuenta20+'" readonly>' +
                        '</td>' +
                        '<td style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="'+centroCosto+'" value="'+centroCosto+'" readonly></td>' +
                        '<td style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfecto" value="'+montoAfecto+'" readonly></td>'
                    );

                    $("#cuentaContableCuentas"+i).remove();
                    $("#cuentaContableCuentas"+i).append(
                        '<tr id="cuentaContableCuentas'+i+'">' +
                        '<td width="200">' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta" value="'+codCuenta+'" readonly>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta20" value="'+codCuenta20+'" readonly>' +
                        '</td>' +
                        '<td width="900">' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="'+nombreCuenta+'" readonly>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta20" value="'+nombreCuenta20+'" readonly>' +
                        '</td>' +
                        '<td width="150" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfectoCuenta" value="'+montoAfecto+'" readonly></td>' +
                        '</tr>'
                    );
                    $("#cuentaContablePartidas"+i).remove();
                    $("#cuentaContablePartidas"+i).append(
                        '<tr id="cuentaContablePartidas'+i+'">' +
                        '<td width="200"><input type="text" style="font-size:11px;" class="form-control text-center" id="codPartida" value="'+codPartida+'" readonly></td>' +
                        '<td width="900"><input type="text" style="font-size:11px;" class="form-control text-center" id="nombrePartida" value="'+nombrePartida+'" readonly></td>' +
                        '<td width="150"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfectoPart" value="'+montoAfecto+'" readonly></td>' +
                        '</tr>'
                    );
                }

            }


            if(impuesto > 0) {
                var codCuentaIva = document.getElementById('cod_cuenta').value;
                var codCuentaIva20 = document.getElementById('cod_cuenta20').value;
                var descripcion = document.getElementById('ind_descripcion').value;
                var descripcion20 = document.getElementById('ind_descripcion20').value;
                var codPartida = document.getElementById('cod_partida').value;
                var denominacion = document.getElementById('ind_denominacion').value;

                $("#cuentaContableImpuesto").remove();
                $(document.getElementById('distribucionContable')).append(
                        '<tr id="cuentaContableImpuesto">' +
                        '<td width="35" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center"  value="" readonly></td>' +
                        '<td width="100" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-left" id="codPartida" value="'+codPartida+'" readonly></td>' +
                        '<td width="300" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-left" id="nombrePartida" value="'+denominacion+'" readonly></td>' +
                        '<td width="100">' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta" value="'+codCuentaIva+'" readonly>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta20" value="'+codCuentaIva20+'" readonly>' +
                        '</td>' +
                        '<td width="300">' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="'+descripcion+'" readonly>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="'+descripcion20+'" readonly>' +
                        '</td>' +
                        '<td width="35" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="centroCosto" value="" readonly></td>' +
                        '<td width="150" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoImp" value="' + impuesto + '" readonly></td>' +
                        '</tr>'
                );
                $("#cuentaContableCuentasImpuesto").remove();
                $(document.getElementById('distribucionContableCuentas')).append(
                        '<tr id="cuentaContableCuentasImpuesto">' +
                        '<td width="200">' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta" value="'+codCuentaIva+'" readonly>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta20" value="'+codCuentaIva20+'" readonly>' +
                        '</td>' +
                        '<td width="900">' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="'+descripcion+'" readonly>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="'+descripcion20+'" readonly>' +
                        '</td>' +
                        '<td width="150" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfecto" value="'+impuesto+'" readonly></td>' +
                        '</tr>'
                );
                $("#cuentaContablePartidasImpuesto").remove();
                $(document.getElementById('distribucionContablePartidas')).append(
                        '<tr id="cuentaContablePartidasImpuesto">' +
                        '<td width="200"><input type="text" style="font-size:11px;" class="form-control text-center" id="codPartida" value="'+codPartida+'" readonly></td>' +
                        '<td width="900"><input type="text" style="font-size:11px;" class="form-control text-center" id="nombrePartida" value="'+denominacion+'" readonly></td>' +
                        '<td width="150"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfecto" value="'+impuesto+'" readonly></td>' +
                        '</tr>'
                );
            }else{
                $("#cuentaContableImpuesto").remove();
                $("#cuentaContableCuentasImpuesto").remove();
                $("#cuentaContablePartidasImpuesto").remove();
            }
        }
    }

</script>

<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();

        /// Complementos
        $('.select2').select2({ allowClear: true});

        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });

        // ancho de la Modal
        $('#modalAncho').css("width", "85%");

        //Guardado y modales
        $('#accion').click(function () {
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                var arrayCheck = ['ind_estado'];
                var arrayMostrarOrden = ['ind_num_caja_chica', 'ind_descripcion', '	num_monto_total', 'fec_preparacion', 'fec_aprobacion', 'ind_estado', 'fk_a004_num_dependencia', 'fk_a023_num_centro_costo', '', 'fk_rhb001_num_empleado_beneficiario'];
                if (dato['status'] == 'error') {
                    app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                } else if (dato['status'] == 'errorSQL') {
                    app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                } else if (dato['status'] == 'errorDetalleReposicion') {
                    swal('Error','¡ERROR: Debe ingresar los detalles de la reposición de la caja chica','error');
                } else if (dato['status'] == 'montoExcedido') {
                    swal('Error','Disculpa. El Monto a Pagar excede el Monto Autorizado','error');
                }else if (dato['status'] == 'modificar') {
                    app.metActualizarRegistroTabla(dato, dato['idCajaChica'], 'idCajaChica', arrayCheck, arrayMostrarOrden, 'La Caja Chica fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                } else if (dato['status'] == 'nuevo') {
                    app.metNuevoRegistroTabla(dato, dato['idCajaChica'], 'idCajaChica', arrayCheck, arrayMostrarOrden, 'La Caja Chica fue guardada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });


        $('.accionModal').click(function () {
            if ($(this).attr('restriccion') == 'CC') {
                var ccosto = document.getElementById("fk_a023_num_centro_de_costo").value;
                if (ccosto == "") {
                    swal("Error!", 'Debe seleccionar el centro de costo por defecto', "error");

                } else {
                    $('#formModal2').modal({
                        show: 'false'
                    });
                    $.post($(this).attr('url'), {
                        cargar: 0,
                        tr: $("#" + $(this).attr('idTabla') + " > tbody > tr").length + 1
                    }, function ($dato) {
                        $('#ContenidoModal2').html($dato);
                    });
                }
            } else {
                $.post($(this).attr('url'), {
                    cargar: 0,
                    tr: $("#" + $(this).attr('idTabla') + " > tbody > tr").length + 1
                }, function ($dato) {
                    $('#ContenidoModal2').html($dato);
                });
            }
            $('#formModalLabel2').html($(this).attr('titulo'));
            $('#modalAncho2').css("width", "60%");
        });

        $('#tbodyReposicion').on('click', '.delete', function () {
            var campo = $(this).attr('id');
            $('#codReposicion' + campo).remove();
        });

        function accionModal(id, attr) {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(id).attr('titulo'));
            $.post($(id).attr(attr), {
                cargar: 0,
                tr: $("#" + $(id).attr('idTabla') + " > tbody > tr").length + 1
            }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        }

        {if isset($estado)}
        $('.accionesEstado').click(function () {
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post(
                    $("#formAjax").attr("action"),
                    { idCajaChica: $('#idCajaChica').val(), estado: $(this).attr('id'), motivoAnulacion: $('#ind_razon_rechazo').val(), valido: 1 },
                    function(dato){
                        if (dato['status'] == 'errorSQL') {
                            app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                        }else if (dato['status'] == 'error') {
                            app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son Obligatorios');
                        }else if (dato['status'] == 'OK') {
                            if (dato['Mensaje']['titulo'] == 'APROBADA') {
                                $(document.getElementById('idCajaChica' + dato['idCajaChica'])).remove();
                            }
                            swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        }
                    },'JSON');
        });
        {/if}

        $('#reposicion').on('click', 'tbody tr .accionModal2', function () {
            accionModal(this, 'enlace');
        });

        $('#tbodyReposicion').on('change', 'tr td .regimen', function () {
            var idTr = $(this).attr('idTR');
            limpiarMontosCajaChica($(this).attr('idTR'));
            var valor = $(this).val();
            var url = $(this).attr('enlace');
            $.post(url, {
                        idRegimen: $(this).val()
                    },
                    function (dato) {
                        console.log('dato');
                        if (valor != '') {
                            if (dato) {
                                $('#' + dato['idSelect']).html('');
                                var id = dato['id'];
                                for (var i = 0; i < id.length; i++) {
                                    $('#' + dato['idSelect']).append('<option value="' + id[i]['pk_num_tipo_servico'] + '">' + id[i]['ind_descripcion'] + '</option>');
                                }
                            }
                        } else {
                            $('#' + dato['idSelect']).html('');
                            $('#' + dato['idSelect']).append('');
                        }
                        //aqui
                        for (var i = 0; i < id.length; i++) {
                            var regimen = id[i]['cod_detalle'];
                            // alert(regimen);
                        }

                        if (regimen == 'M') {
                            document.getElementById(idTr + 'montoPagado').disabled = true;
                        } else if (regimen == 'N') {
                            document.getElementById(idTr + 'montoPagado').disabled = false;
                            document.getElementById(idTr + 'montoAfecto').disabled = true;
                            document.getElementById(idTr + 'montoNoAfecto').disabled = true;
                            document.getElementById(idTr + 'montoImpuesto').disabled = true;
                            document.getElementById(idTr + 'montoRetencion').disabled = true;
                        } else {
                            document.getElementById(idTr + 'montoPagado').disabled = false;
                            document.getElementById(idTr + 'montoAfecto').disabled = false;
                            document.getElementById(idTr + 'montoNoAfecto').disabled = false;
                            document.getElementById(idTr + 'montoImpuesto').disabled = false;
                            document.getElementById(idTr + 'montoRetencion').disabled = false;
                        }
                    }, 'json');
        });

        $('#tbodyReposicion').on('change', 'tr td .servicio', function () {
            limpiarMontosCajaChica($(this).attr('idTR'));
            $.post('{$_Parametros.url}modCP/otros/cajaChica/listarCajaChicaCONTROL/impuestoMET/IVA', {
                idTipoServicio: $(this).val()
            }, function (dato) {
                if (dato) {
                    if (dato['num_factor_porcentaje']) {
                        $('#totalImpuesto').attr('ivaporcentaje', dato['num_factor_porcentaje']);
                        $('#totalImpuesto').attr('signo', dato['ind_signo']);
                        $('#totalImpuesto').attr('regimen', dato['regimen']);
                        $('#totalImpuesto').attr('imponible', dato['imponible']);
                    }
                }
            }, 'json');
        });
        $('#tbodyReposicion').on('change', 'tr td .montoPagado', function () {
            calculoMonto($(this).attr('idTR'), 'montoPagado', $(this).val());
            distribucionCajaChica($(this).attr('idTR'));
        });
        $('#tbodyReposicion').on('change', 'tr td .montoAfecto', function () {
            calculoMonto($(this).attr('idTR'), 'montoAfecto', $(this).val());
            distribucionCajaChica($(this).attr('idTR'));
        });

        $('#tbodyReposicion').on('change', 'tr td .montoNoAfecto', function () {
            var idTr = $(this).attr('idTR');
            var montoNoAfecto = $(this).val();
            $(document.getElementById(idTr +'montoPagado')).attr('value',montoNoAfecto);
        });

        $('#ajaxDistribucion').click(function () {
            distribucionCajaChica();
        });
        $('#siguiente').click(function () {
            distribucionCajaChica();
        });

        $('#tbodyReposicion').on('click', 'tr td .delete', function () {
            var idTr = $(this).attr('id');
            var total = 0

            $('#total').html(total);
            $("#cuentaContable"+idTr).remove();
            $("#cuentaContableCuentas"+idTr).remove();
            $("#cuentaContablePartidas"+idTr).remove();

            $("#cuentaContableImpuesto").remove();
            $("#cuentaContableCuentasImpuesto").remove();
            $("#cuentaContablePartidasImpuesto").remove();
        });


    });

</script>