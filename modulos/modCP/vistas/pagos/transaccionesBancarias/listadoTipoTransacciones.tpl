<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Tipo de Transacciones Bancarias</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="75" scope="col">Tipo</th>
                            <th scope="col">Descripción</th>
                            <th width="50" scope="col">Vou.</th>
                            <th width="100" scope="col">Cuenta</th>
                            <th width="100" scope="col">Estado</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr id="idTransaccion{$i.pk_num_banco_tipo_transaccion}">
                                <input type="hidden" value="{$i.pk_num_banco_tipo_transaccion}" class="transaccion"
                                       transaccion="{$i.pk_num_banco_tipo_transaccion}"
                                       codigo="{$i.cod_tipo_transaccion}"
                                       tipoT="{$i.cod_detalle}"
                                       descripcion="{$i.ind_descripcion}"
                                       tipoTransaccion="{$i.fk_a006_miscelaneo_detalle_tipo_transaccion}"
                                        >

                                <td><label>{$i.cod_tipo_transaccion}</label></td>
                                <td><label>{$i.ind_descripcion}</label></td>
                                <td><label><i class="{if $i.num_flag_voucher==1}md md-check{else}md md-not-interested{/if}"></i></label></td>
                                <td><label>{$i.cod_cuenta}</label></td>
                                <td><label><i class="{if $i.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');
            if('{$listaTransaccion}' == 'transacciones'){
                $(document.getElementById('{$idCampo}codTransaccion')).val(input.attr('codigo'));
                $(document.getElementById('{$idCampo}tipoTransaccion')).val(input.attr('tipoT'));
                $(document.getElementById('{$idCampo}nombreTransaccion')).val(input.attr('descripcion'));
                $(document.getElementById('{$idCampo}fk_cpb006_num_banco_tipo_transaccion')).val(input.attr('transaccion'));
            }
            //TIPO DE DOCUMENTO - busco los tipos de documentos para el tipo de transaccion seleccionada
            $.post('{$_Parametros.url}modCP/pagos/transaccionesBancariasCONTROL/listasDocumentoTransaccionMET', {
                tipoTransaccion: input.attr('tipoTransaccion')
            }, function (dato) {
                if(dato.length > 0){
                    var nombre = '{$idCampo}fk_cpb002_num_tipo_documento';
                    $('#detalleTransaccion #'+nombre).html('');
                    //muestro el listado de los tipos de documentos del proveedor
                    for(var i=0; i < dato.length; i++){
                        $('#detalleTransaccion #'+nombre).append('<option value="'+dato[i]['pk_num_tipo_documento']+'">'+dato[i]['ind_descripcion']+'</option>');
                        //selecciono el primero de la lista por defecto
                        $('#detalleTransaccion #'+nombre+' .select2-chosen').html(dato[0]['ind_descripcion']);
                    }
                }
            },'json');

            $('#cerrarModal2').click();

        });
    });
</script>
