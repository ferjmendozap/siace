<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                        <form action="{$_Parametros.url}modCP/pagos/transaccionesBancariasCONTROL/accionesMET/'" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idCuenta}" id="idCuenta" name="idCuenta"/>

                            <div class="tab-content clearfix">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" style="padding: 4px;">
                                                    <div class="card-head card-head-xs style-primary text-center">
                                                        <header class="text-center">Informacion de la Cuenta Bancaria</header>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="pk_num_tipo_documento"
                                                                       class="control-label" style="margin-top: 10px;"> Cta. Bancaria:</label>
                                                            </div>
                                                            <div class="col-sm-7">
                                                                <div class="form-group"
                                                                     id="pk_num_tipo_documentoError" style="margin-top: -10px;">
                                                                    <select class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria"  disabled >
                                                                        <option value="">Seleccione Cuenta Bancaria</option>}
                                                                        {foreach item=proceso from=$listadoCuentas}
                                                                            {if isset($dataBD.pk_num_cuenta) and $dataBD.pk_num_cuenta == $proceso.pk_num_cuenta }
                                                                                <option value="{$proceso.pk_num_cuenta}" selected >{$proceso.ind_num_cuenta}</option>
                                                                            {else}
                                                                                <option value="{$proceso.pk_num_cuenta}">{$proceso.ind_num_cuenta}</option>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="col-sm-3 text-right">
                                                                <label for="pago"
                                                                       class="control-label" style="margin-top: 10px;"> Saldo a la Fecha:</label>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="fecha"
                                                                       class="form-control"
                                                                       value="{date('Y-m-d')}"
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <input type="text"
                                                                       id="saldo"
                                                                       class="form-control"
                                                                       value=""
                                                                       disabled
                                                                       readonly size="9%">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-body" style="padding: 4px;">
                                                        <div class="card-head card-head-xs style-primary text-center">
                                                            <header class="text-center">Criterio de Selección para Transaciones</header>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-3 text-right">
                                                                    <label for="pk_num_tipo_documento"
                                                                           class="control-label" style="margin-top: 10px;"> Tipo Transacción:</label>
                                                                </div>
                                                                <div class="col-sm-2" align="center">
                                                                    <div class="checkbox checkbox-styled" style="vertical-align: middle;">
                                                                        <label>
                                                                            <input type="checkbox" value="1" id="checkTipoTransaccion" onclick="activarBoton()" >

                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group" id="pk_num_banco_tipo_transaccionError" style="margin-top: -10px;">
                                                                        <select class="form-control select2" data-placeholder="Seleccione Tipo de Transaación" id="pk_num_banco_tipo_transaccion" disabled>
                                                                            <option value="">Seleccione Tipo de Transaación</option>}
                                                                            {foreach item=transaccion from=$listadoTipoTrans}
                                                                                <option value="{$transaccion.pk_num_banco_tipo_transaccion}">{$transaccion.ind_descripcion}</option>
                                                                            {/foreach}
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="col-sm-3 text-right">
                                                                    <label for="pago"
                                                                           class="control-label" style="margin-top: 10px;"> Fecha:</label>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                     <input type="text" class="form-control text-center date"
                                                                           id="desde"
                                                                           style="text-align: center"
                                                                           value=""
                                                                            placeholder="Desde"
                                                                           readonly>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <input type="text" class="form-control text-center date"
                                                                           id="hasta"
                                                                           style="text-align: center"
                                                                           value=""
                                                                           placeholder="Hasta"
                                                                           readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div align="center">
                                                            <button type="button" class="btn btn-primary accionesEstado" id="BUSCAR">Buscar</button>
                                                        </div>
                                                    </div>
                                                </div>



                                            <div class="col-sm-12">
                                                <div class="card-body">
                                                    <div class="table-responsive">
                                                        <table id="datatable1" class="table table-striped table-hover">
                                                            <thead>
                                                                <th scope="col" width="75">Fecha</th>
                                                                <th scope="col" width="75">Nro. Transacción</th>
                                                                <th scope="col" width="275">Tipo</th>
                                                                <th scope="col" width="100">Abonos</th>
                                                                <th scope="col" width="100">Cargos</th>
                                                                <th scope="col" width="75">Periodo</th>
                                                                <th scope="col" width="150">Cheque #</th>
                                                                <th scope="col" width="100">Monto</th>
                                                                <th scope="col" width="175">Referencia Banco</th>
                                                                <th scope="col">Comentarios</th>
                                                            </thead>
                                                            <tbody>
                                                            {foreach item=i from=$dataDetalleBD}
                                                                {$totalAbonos = 0}
                                                                {$totalCargos = 0}
                                                                {if ($i.tipoTransaccion == 'I')}
                                                                    {$abono = $i.Monto}
                                                                    {$cargo = 0}
                                                                {else}
                                                                    {$abono = 0}
                                                                    {$cargo = $i.Monto}
                                                                {/if}
                                                                <tr id="idCuenta{$i.pk_num_cuenta}">
                                                                    <td>{$i.fec_transaccion}</td>
                                                                    <td>{$i.ind_num_transaccion}</td>
                                                                    <td>{$i.decripcionTransaccion}</td>
                                                                    <td>{$abono}</td>
                                                                    <td>{$cargo}</td>
                                                                    <td>{$i.ind_periodo_contable}</td>
                                                                    <td>-</td>
                                                                    <td>{$i.Monto}</td>
                                                                    <td>-</td>
                                                                    <td>{$i.txt_comentarios}</td>
                                                                </tr>
                                                                {$totalAbonos = $totalAbonos + $abono}
                                                                {$totalCargos = $totalCargos + $cargo}
                                                            {/foreach}
                                                            </tbody>
                                                            <tfoot>
                                                            <tr><td colspan="10">&nbsp;</td></tr>
                                                            <tr>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th>{$totalAbonos}</th>
                                                                <th>{$totalCargos}</th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                                <th></th>
                                                            </tr>
                                                            </tfoot>

                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>

                                </div>
                </div>

                        </form>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal"
            descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">Cancelar
    </button>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

        var app = new AppFunciones();
        app.metWizard();
        /// Complementos
        $('.select2').select2({ allowClear: true});
        // anular accion del Formulario
        $("#formAjax").submit(function () {
            return false;
        });

        // ancho de la Modal
        $('#modalAncho').css("width", "80%");

        {if isset($estado)}
        $('.accionesEstado').click(function () {
            $.post(
                    $("#formAjax").attr("action"),
                    { idCuenta: $('#idCuenta').val(), estado: $(this).attr('id'), motivo: $('#txt_comentario_anulacion').val()},
                    function(dato){
                        if (dato['status'] == 'errorSQL') {
                            app.metValidarError(dato, 'Disculpa. Contacta Con la Direccion Tecnica porque hay un ERROR interno');
                        } else if (dato['status'] == 'OK') {
                            $(document.getElementById('idCuenta'+dato['idCuenta'])).remove();
                            swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                            $(document.getElementById('cerrarModal')).click();
                            $(document.getElementById('ContenidoModal')).html('');
                        }
                    },'JSON');
        });
        {/if}
    });
    function activarBoton() {
        if (checkTipoTransaccion.checked == true) {
            document.getElementById("pk_num_banco_tipo_transaccion").disabled = false;
        } else {
            document.getElementById("pk_num_banco_tipo_transaccion").disabled = true;
        }
    }
</script>