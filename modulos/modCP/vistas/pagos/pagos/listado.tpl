
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">LISTA DE PAGOS</h2>
    </div>
    <div class="section-body">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>CTA. BANCARIA</th>
                            <th>PREPAGO</th>
                            <th>PAGAR A</th>
                            <th>MONTO</th>
                            <th>FECHA</th>
                            <th>ESTADO</th>
                            <th>N° CHEQUE</th>
                            <th>TIPO PAGO</th>
                            <th>VOUCHER</th>
                            <th>ACCIONES</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$dataBD}
                            <tr>
                                <td width="100">{$i.ind_num_cuenta}</td>
                                <td width="60">{$i.ind_num_pago}</td>
                                <td width="160">{$i.fk_a003_num_persona_proveedor_a_pagar}</td>
                                <td width="100" class="text-danger text-bold text-right">{$i.num_monto_pago}</td>
                                <td width="60">{$i.fec_pago}</td>
                                <td width="60">{$i.ind_estado}</td>
                                <td width="80">{$i.ind_num_cheque}</td>
                                <td width="150">{$i.a006_miscelaneo_detalle_tipo_pago}</td>
                                <td>{$i.ind_anio}-{$i.ind_mes}  {$i.ind_voucher}</td>
                                <td width="120">
                                    {if in_array('CP-01-03-04-02-A',$_Parametros.perfil)}
                                    <button class="acciones btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" id="AN" idPago="{$i.pk_num_pago}" title="Anular"
                                            descipcion="El Usuario ha ANULADO un pago" titulo="<i class='fa fa-edit'></i> Anular Pago">
                                        <i class="icm icm-blocked" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                    {if in_array('CP-01-03-04-04-VV',$_Parametros.perfil)}
                                        {if ($i.ind_voucher != NULL)}
                                            <button class="acciones btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" id="VOUCHER" idVoucherPago="{$i.fk_cbb001_num_voucher_pago}" title="VER VOUCHER"
                                                    descipcion="El Usuario esta viendo un vouchers" titulo="<i class='fa fa-file'></i> Ver Vouchers de Pago">
                                                <i class="fa fa-file" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    {/if}
                                    {if in_array('CP-01-03-04-01-V',$_Parametros.perfil)}
                                    <button class="acciones logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static"  id="VER" idPago="{$i.pk_num_pago}" title="Consultar"
                                            descipcion="El Usuario esta viendo un pago" titulo="<i class='md md-remove-red-eye'></i> Consultar Pago">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                    {if in_array('CP-01-03-04-03-IS',$_Parametros.perfil)}
                                    <button class="acciones btn ink-reaction btn-raised btn-xs btn-success" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" id="IM" idPago="{$i.pk_num_pago}" title="Imprimir"
                                            descipcion="El Usuario entro a IMPRIMIR SUSTENTO" titulo="<i class='md md-print'></i> IMPRIMIR SUSTENTO">
                                        <i class="md md-print" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/pagos/pagosCONTROL/accionesPagoMET';

        $('#datatable1 tbody').on( 'click', '.acciones', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idPago: $(this).attr('idPago'),idVoucherPago:$(this).attr('idVoucherPago'), estado: $(this).attr('id') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });
</script>
