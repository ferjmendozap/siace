
<div class="modal-body">
    <div class="row">
        <div class="card">
            <form action="{$_Parametros.url}modCP/pagos/imprimirTransferirCONTROL/registrarVouchersMET" id="formAjax2" class="form floating-label form-validate" role="form" method="post" novalidate="novalidate">
                <input type="hidden" name="form[int][ind_mes]" value="{date('m')}"/>
                <input type="hidden" name="form[int][ind_anio]" value="{date('Y')}"/>
                <input type="hidden" name="form[int][fk_a001_num_organismo]" value="{$organismo.pk_num_organismo}"/>
                <input type="hidden" name="form[int][fk_cbb005_num_contabilidades]" value="{$contabilidad}"/>
                <input type="hidden" id="estado" value="{if isset($estado)}{$estado}{/if}"/>
                <input type="hidden" name="idPago" value="{$idPago}"/>
                <input type="hidden" name="idOrdenPago" value="{$idOrdenPago}"/>
                <input type="hidden" name="tipo" value="anulacion"/>
                <input type="hidden" name="tipoVoucher" value="{$tipoVoucher}"/>
                <div class="card-body">
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table id="datatable1" class="table table-striped table-hover style-primary">
                                <thead>
                                <tr>
                                    <th>PERIODO</th>
                                    <th>VOUCHER</th>
                                    <th>FECHA</th>
                                    <th>STATUS</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table id="datatable1" class="table table-striped table-hover style-primary">
                                <thead>
                                <tr>
                                    <th>LINEA</th>
                                    <th>ERRORES</th>
                                    <th>PERIODO</th>
                                    <th>VOUCHER</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group floating-label" id="descripcionError">
                                <input type="text" class="form-control" id="descripcion" name="txt_titulo_voucher" value="{$datosVoucher.txt_titulo_voucher}">
                                <label for="descripcion"><i class="md md-person"></i> Descripci&oacute;n</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group floating-label date" id="fechaError">
                                <input type="text" class="form-control" id="fecha" name="form[txt][fec_fecha_voucher]" value="{$datosVoucher.fec_fecha_voucher}">
                                <label for="fecha"><i class="md md-today"></i> Fecha</label>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="col-sm-2">
                                <input name="form[int][fk_cbc003_num_voucher]" type="hidden" class="form-control" value="{$codVoucher.pk_num_voucher}">
                                <input style="text-align: center" type="text" class="form-control" value="{$codVoucher.cod_voucher}">
                            </div>
                            <div class="col-sm-4">
                                <input style="text-align: center" type="text" class="form-control" value="{$datosVoucher.ind_nro_voucher}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group floating-label date" id="libroError">
                                <select name="form[int][fk_cbb002_num_libro_contable]" class="form-control select2" data-placeholder="Seleccione Libro Contable">
                                    {foreach item=i from=$libroContable}
                                        <option value="{$i.pk_num_libro_contable}">{$i.ind_descripcion}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group floating-label date" id="preparadoPorError">
                                <input type="text" class="form-control" id="preparadoPor" value="{$_Parametros.nombreUsuario}">
                                <label for="preparadoPor"><i class="md md-today"></i> Preparado por:</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group floating-label date" id="aprobadoPorError">
                                <input type="text" class="form-control" id="aprobadoPor" value="{$_Parametros.nombreUsuario}">
                                <label for="aprobadoPor"><i class="md md-today"></i> Aprobado por:</label>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <button type="button" class="btn btn-default logsUsuarioModal "
                                descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
                            <i class="icm icm-close"></i>&nbsp;Cerrar
                        </button>
                    </div>
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr class="style-primary">
                                    <th>#</th>
                                    <th>FECHA</th>
                                    <th>CUENTA</th>
                                    <th>DESCRIPCION</th>
                                    <th>PERSONA</th>
                                    <th>DOCUMENTO</th>
                                    <th>C. COSTO</th>
                                    <th>DEBE</th>
                                    <th>HABER</th>
                                </tr>
                                </thead>
                                <tbody>
                                {$c=0}
                                {if $datoArreglo}
                                    {$debitos=0} {$creditos=0}
                                    {foreach item=i from=$datoArreglo.Debe}
                                        {$c=$c+1}
                                        <input type="hidden" name="form[int][detalle_fk_cbb004_num_cuenta][]" id="detalle_fk_cbb004_num_cuenta{$c}" value="{$i.pk_num_cuenta}">
                                        <input type="hidden" name="form[alphaNum][detalle_ind_descripcion][]" id="detalle_ind_descripcion{$c}" value="{$i.ind_descripcion}">
                                        <input type="hidden" name="form[int][detalle_fk_a003_num_persona][]" id="detalle_fk_a003_num_persona{$c}" value="{$i.pk_num_persona}">
                                        <input type="hidden" name="form[int][detalle_fk_a023_num_centro_costo][]" id="detalle_fk_a023_num_centro_costo{$c}" value="{$i.fk_a023_num_centro_costo}">
                                        <input type="hidden" name="form[int][detalle_num_debe][]" id="detalle_num_debe{$c}" value="{number_format($i.MontoVoucher,'2',',','.')}">
                                        <input type="hidden" name="form[int][detalle_num_haber][]" id="detalle_num_haber{$c}" value="0">
                                        <tr>
                                            <td>{$c}</td>
                                            <td>{$datosVoucher.fec_fecha_voucher}</td>
                                            <td>{$i.cod_cuenta}</td>
                                            <td>{$i.ind_descripcion}</td>
                                            <td>{$i.proveedor}</td>
                                            <td>{$datosVoucher.fk_cbc003_num_voucher}-{{$datosVoucher.fk_cbc003_num_voucher}}</td>
                                            <td>{$centroCosto['pk_num_centro_costo']}</td>
                                            <td>{number_format($i.MontoVoucher,'2',',','.')}</td>
                                            <td  style="color: red"></td>
                                        </tr>
                                        {$debitos=$debitos+$i.MontoVoucher}
                                    {/foreach}
                                    {foreach item=i from=$datoArreglo.Haber}
                                        {$c=$c+1}
                                        <input type="hidden" name="form[int][detalle_fk_cbb004_num_cuenta][]" id="detalle_fk_cbb004_num_cuenta{$c}" value="{$i.pk_num_cuenta}">
                                        <input type="hidden" name="form[alphaNum][detalle_ind_descripcion][]" id="detalle_ind_descripcion{$c}" value="{$i.ind_descripcion}">
                                        <input type="hidden" name="form[int][detalle_fk_a003_num_persona][]" id="detalle_fk_a003_num_persona{$c}" value="{$i.pk_num_persona}">
                                        <input type="hidden" name="form[int][detalle_fk_a023_num_centro_costo][]" id="detalle_fk_a023_num_centro_costo{$c}" value="{$i.fk_a023_num_centro_costo}">
                                        <input type="hidden" name="form[int][detalle_num_haber][]" id="detalle_num_haber{$c}" value="{number_format($i.MontoVoucher,'2',',','.')}">
                                        <input type="hidden" name="form[int][detalle_num_debe][]" id="detalle_num_debe{$c}" value="0">

                                        <tr>
                                            <td>{$c}</td>
                                            <td>{$datosVoucher.fec_fecha_voucher}</td>
                                            <td>{$i.cod_cuenta}</td>
                                            <td>{$i.ind_descripcion}</td>
                                            <td>{$i.proveedor}</td>
                                            <td>{$datosVoucher.fk_cbc003_num_voucher}-{{$datosVoucher.fk_cbc003_num_voucher}}</td>
                                            <td>{$centroCosto['pk_num_centro_costo']}</td>
                                            <td> </td>
                                            <td  style="color: red">{number_format($i.MontoVoucher,'2',',','.')}</td>
                                        </tr>
                                        {$creditos=$creditos+$i.MontoVoucher}
                                    {/foreach}
                                {/if}
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th colspan="3">
                                        Nro de Lineas: {$c}
                                    </th>
                                    <th colspan="2">

                                    </th>
                                    <th colspan="2" style="font-weight:bold; font-size:16px;">
                                        Total:
                                    </th>
                                    <th colspan="1">
                                        <input type="text" class="form-control" id="creditos" value="{number_format($debitos,'2',',','.')}" style="font-weight:bold; font-size:18px;" readonly>
                                    </th>
                                    <th colspan="1">
                                        <input type="text" class="form-control" id="debitos" value="{number_format($creditos,'2',',','.')}" style="font-weight:bold; font-size:18px; color: red"" readonly>
                                    </th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        /// Complementos
        var app = new  AppFunciones();

        // anular accion del Formulario
        $("#formAjax2").submit(function(){
            return false;
        });
        var estado=$('#estado').val();
        //Guardado y modales
        if(estado){

        }else{
            $.post($("#formAjax2").attr("action"), $("#formAjax2").serialize(), function (dato) {
                if (dato['status'] == 'error') {
                    app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                } else if (dato['status'] == 'errorSQL') {
                    app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                } else if (dato['status'] == 'ok2') {

                }
            }, 'json');
        }
        });
</script>