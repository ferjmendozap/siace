<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">LASTADO DE VIATICOS PROCESADOS</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#Sol.</th>
                            <th>Solicitante</th>
                            <th>Fecha Registro</th>
                            <th>Fecha Salida</th>
                            <th>Fecha Regreso</th>
                            <th>Destino</th>
                            <th>Motivo</th>
                            <th class="col-sm-1">Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$solicitudBD}
                                <tr id="idSolViatico{$sol.pk_num_viatico}">
                                    <td>{$i.cod_interno}</td>
                                    <td>{$i.solicitante}</td>
                                    <td>{$i.fec_registro_viatico|date_format:"d-m-Y"}</td>
                                    <td>{$i.fec_salida|date_format:"d-m-Y"}</td>
                                    <td>{$i.fec_salida|date_format:"d-m-Y"}</td>
                                    <td>{$i.ind_destino}</td>
                                    <td>{$i.ind_motivo}</td>
                                    <td>
                                          <button title='Imprimir Calculo Viatico' data-toggle="modal" data-target="#formModal"
                                                  data-keyboard="false" data-backdrop="static"
                                                  class='imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                                                  idViatico="{$i.pk_num_viaticos}" idSolViatico="{$i.pk_num_viatico}" titulo='Calculo de Viatico'>
                                              <i class='md md-print'></i>
                                          </button>
                                    </td>
                                </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        $('#datatable1 tbody').on( 'click', '.imprimir', function () {
            var idViatico = $(this).attr('idViatico');
            var idSolViatico = $(this).attr('idSolViatico');
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html('<iframe frameborder="0" src="{$_Parametros.url}modCP/viaticos/viaticosCONTROL/ImprimirMET/'+idViatico+'/'+idSolViatico+'/"  width="100%" height="540px"></iframe>');

        });



    });
</script>
