<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">{if $estado =='RE'}REVISAR VIATICOS {elseif $estado == 'AP'}APROBAR VIATICOS {elseif $estado == 'GE'}GENERAR OBLIGACION DE VIATICOS {else}LISTADO DE SOLICITUDES DE VIATICOS{/if}</h2>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#Sol.</th>
                            <th>Solicitante</th>
                            <th>Fecha Registro</th>
                            <th>Fecha Salida</th>
                            <th>Fecha Regreso</th>
                            <th>Destino</th>
                            <th>Motivo</th>
                            <th class="col-sm-1">Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$solicitudBD}
                            {if $estado == 'GE'}
                                {if $i.cantidad != 0}
                                    <tr id="idSolViatico{$sol.pk_num_viatico}">
                                        <td>{$i.cod_interno}</td>
                                        <td>{$i.solicitante}</td>
                                        <td>{$i.fec_registro_viatico|date_format:"d-m-Y"}</td>
                                        <td>{$i.fec_salida|date_format:"d-m-Y"}</td>
                                        <td>{$i.fec_salida|date_format:"d-m-Y"}</td>
                                        <td>{$i.ind_destino}</td>
                                        <td>{$i.ind_motivo}</td>
                                        <td>
                                            {if $estado == 'GE'}
                                                <button class="accion btn ink-reaction btn-raised btn-xs btn-primary-light" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" id="GE" idViatico="{$i.pk_num_viaticos}" idSolViatico="{$i.pk_num_viatico}"  title="Generar Obligación"
                                                        descipcion="El Usuario ha GENERADO una Obligación" titulo="<i class='icm icm-spinner12'></i> Generar Obligación de Beneficiario de Viaticos">
                                                    <i class="icm icm-spinner12" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                            {if in_array('CP-01-06-02-01-P',$_Parametros.perfil)}
                                                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" idViatico="{$i.pk_num_viaticos}" idSolViatico="{$i.pk_num_viatico}" title="Consultar"
                                                        descipcion="El Usuario esta viendo un Viático" titulo="<i class='md md-remove-red-eye'></i> Consultar Viático">
                                                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}

                                        </td>
                                    </tr>
                                {/if}
                                {else}
                                <tr id="idSolViatico{$sol.pk_num_viatico}">
                                    <td>{$i.cod_interno}</td>
                                    <td>{$i.solicitante}</td>
                                    <td>{$i.fec_registro_viatico|date_format:"d-m-Y"}</td>
                                    <td>{$i.fec_salida|date_format:"d-m-Y"}</td>
                                    <td>{$i.fec_salida|date_format:"d-m-Y"}</td>
                                    <td>{$i.ind_destino}</td>
                                    <td>{$i.ind_motivo}</td>
                                    <td>

                                        {if !isset($i.pk_num_viaticos)}
                                            {if in_array('CP-01-06-02-01-P',$_Parametros.perfil)}
                                                <button class="procesar btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" id="PROCESAR" idSolViatico="{$i.pk_num_viatico}" title="PROCESAR"
                                                        descipcion="El Usuario ha procesado un viatico" titulo="<i class='fa fa-edit'></i> Procesar Viatico">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        {else}
                                            {if isset($estado) and $estado == ''}
                                                <button class="accion Obligacion btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" data-backdrop="static" id="modificar" idViatico="{$i.pk_num_viaticos}" idSolViatico="{$i.pk_num_viatico}" title="Editar"
                                                        descipcion="El Usuario ha Modificado un Viatico" titulo="<i class='fa fa-edit'></i> Editar Viatico">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        {/if}
                                        {if isset($estado) and $estado == 'RE'}
                                            {if in_array('CP-01-06-02-01-P',$_Parametros.perfil)}
                                                <button class="accion btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" id="PR" data-backdrop="static" idViatico="{$i.pk_num_viaticos}" idSolViatico="{$i.pk_num_viatico}" title="Revisar"
                                                        descipcion="El Usuario ha Revisado una Viatico" titulo="<i class='icm icm-rating'></i> Revisar Viatico">
                                                    <i class="icm icm-rating" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        {elseif isset($estado) and $estado == 'AP'}

                                            {if in_array('CP-01-06-02-01-P',$_Parametros.perfil)}
                                               <button class="accion btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                        data-keyboard="false" id="RE" data-backdrop="static" idSolViatico="{$i.pk_num_viatico}" idViatico="{$i.pk_num_viaticos}" title="Aprobar"
                                                        descipcion="El Usuario ha Aprobado una Viatico" titulo="<i class='icm icm-rating3'></i> Aprobar Viatico">
                                                    <i class="icm icm-rating3" style="color: #ffffff;"></i>
                                                </button>
                                            {/if}
                                        {/if}
                                        {if in_array('CP-01-06-02-01-P',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idViatico="{$i.pk_num_viaticos}" idSolViatico="{$i.pk_num_viatico}" title="Consultar"
                                                    descipcion="El Usuario esta viendo un Viático" titulo="<i class='md md-remove-red-eye'></i> Consultar Viático">
                                                <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                          <!--<button title='Imprimir Calculo Viatico' data-toggle="modal" data-target="#formModal"
                                                  data-keyboard="false" data-backdrop="static"
                                                  class='imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                                                  idViatico="{$i.pk_num_viaticos}" idSolViatico="{$i.pk_num_viatico}" titulo='Calculo de Viatico'>
                                              <i class='md md-print'></i>
                                          </button>-->
                                    </td>
                                </tr>
                            {/if}
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/viaticos/viaticosCONTROL/accionesViaticoMET';

        $('#datatable1 tbody').on( 'click', '.procesar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idSolViatico: $(this).attr('idSolViatico'), estado: $(this).attr('id') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.accion', function () {
            var estado = $(this).attr('id');  var ver = 2;
            if(estado != 'modificar'){  ver = 1;    }
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idViatico: $(this).attr('idViatico'), idSolViatico: $(this).attr('idSolViatico'), ver:ver, estado: estado },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idViatico: $(this).attr('idViatico'), idSolViatico: $(this).attr('idSolViatico'), ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.imprimir', function () {
            var idViatico = $(this).attr('idViatico');
            var idSolViatico = $(this).attr('idSolViatico');
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html('<iframe frameborder="0" src="{$_Parametros.url}modCP/viaticos/viaticosCONTROL/ImprimirMET/'+idViatico+'/'+idSolViatico+'/"  width="100%" height="540px"></iframe>');

        });


    });
</script>
