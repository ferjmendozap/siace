<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Conceptos de Viáticos</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Concepto</th>
                            <th>Descripción</th>
                            <th>Categoría</th>
                            <th>Valor U.T</th>
                            <th>Estado</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$dataBD}
                            {$monto = $i.num_valor_ut * $i.num_monto}
                            <tr id="idConceptoViatico{$i.pk_num_concepto_gasto_viatico}">
                                <input type="hidden" value="{$i.pk_num_concepto_gasto_viatico}"
                                       pkConcepto = "{$i.pk_num_concepto_gasto_viatico}"
                                       descripcion = "{$i.ind_descripcion}"
                                       unidadViatico = "{number_format($i.num_valor_ut,2, '.', '')}"
                                       unidadTrib = "{number_format($i.num_monto,2, '.', '')}"
                                       monto = "{number_format($monto,2, '.', '')}"
                                       montoC = "{$monto}"
                                       flagMonto = "{$i.num_flag_monto}"
                                       flagCantidad = "{$i.num_flag_cantidad}"
                                       flagCombustible = "{$i.num_flag_unidad_combustible}"
                                       idBeneficiario ="{$idBeneficiario}"
                                       pkCuenta = "{$i.fk_cbb004_num_plan_cuenta}"
                                       pkCuenta20 = "{$i.fk_cbb004_num_plan_cuenta_pub20}"
                                       pkPartida = "{$i.fk_prb002_num_partida_presupuestaria}"
                                       >
                                <td>{$i.cod_concepto }</td>
                                <td>{$i.ind_descripcion}</td>
                                <td>{$i.categoria}</td>
                                <td>{$i.num_valor_ut}</td>
                                <td><i class="{if $i.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {

            var input = $(this).find('input');
            if('{$parametro}' == 'conceptos') {
                var sec = $('#contenidoTabla tr').length + 1;
                var idBeneficiario = input.attr('idBeneficiario');
                var pkConcepto = input.attr('pkConcepto');
                var flagMonto = input.attr('flagMonto');
                var flagCantidad = input.attr('flagCantidad');
                var flagCombustible = input.attr('flagCombustible');
                var pkCuenta = input.attr('pkCuenta');
                var pkCuenta20 = input.attr('pkCuenta20');
                var pkPartida = input.attr('pkPartida');
                var montoC = input.attr('montoC');
                var unidadTrib = input.attr('unidadTrib');

                if(flagCombustible==1){
                    montoC = parseFloat($("#num_unidad_combustible").val()) * parseFloat(unidadTrib);
                    var unidadViatico = $("#num_unidad_combustible").val();
                    var monto = montoC;
                }else{
                    montoC = montoC;
                    var unidadViatico = input.attr('unidadViatico');
                    var monto = input.attr('monto');
                }

                $(document.getElementById('contenidoTabla')).append(
                        '<tr id="codConcepto' + sec + '">' +
                        '<input type="hidden" class="concepto" idTr="codConcepto' + sec + '" id="codConcepto' + sec + 'pkConcepto" value="' + input.attr('pkConcepto') + '" > ' +
                        '<input type="hidden" id="codConcepto'+sec+'num_monto_viatico" value="' + input.attr('pkConcepto') + '" > ' +
                        '<input type="hidden" id="codConcepto'+sec+'pkCuenta" value="' + pkCuenta + '" > ' +
                        '<input type="hidden" id="codConcepto'+sec+'pkCuenta20" value="' + pkCuenta20 + '" > ' +
                        '<input type="hidden" id="codConcepto'+sec+'pkPartida" value="' + pkPartida + '" > ' +
                        '<input type="hidden" id="codConcepto'+sec+'montoC" value="' + montoC + '" > ' +
                        '<input type="hidden" id="codConcepto'+sec+'unidadTributaria" value="' + unidadTrib + '" > ' +
                        '<input type="hidden" id="codConcepto'+sec+'unidadViatico" value="' + unidadViatico + '" > ' +
                        '<td width="10"><input type="text" class="form-control text-center" value="' + sec + '" readonly></td>' +
                        '<td width="70"><input type="text" class="form-control text-center" id="codConcepto' + sec + 'concepto" value="' + input.attr('descripcion') + '" readonly></td>' +
                        '<td width="20"><input type="text" class="form-control text-center" id="codConcepto' + sec + 'unidadViatico" value="' + unidadViatico + '"></td>' +
                        '<td width="20"><input type="text" class="form-control text-center" id="codConcepto' + sec + 'unidadTrib" value="' + input.attr('unidadTrib') + '"></td>' +
                        '<td width="20"><input type="text" class="form-control text-center monto" id="codConcepto'+sec+'monto'+idBeneficiario+'" idBenef="' + input.attr('idBeneficiario') + '" idTr="codConcepto' + sec + '" value="' + monto + '" ></td>' +
                        '<td width="20"><input type="text" class="form-control text-center dias" id="codConcepto'+sec+'dias'+idBeneficiario+'" idBenef="' + input.attr('idBeneficiario') + '" idTr="codConcepto' + sec + '" value="0" ></td>' +
                        '<td width="20"><input type="text" class="form-control text-center montoTotal" id="codConcepto'+sec+'montoTotal'+idBeneficiario+'" value="" readonly></td>' +
                        '<td width="15" style="text-align: center">' +
                        '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="' + sec + '"><i class="md md-delete"></i></button>' +
                        '</td>' +
                        '</tr>'
                );
                if(flagMonto == 0 && flagCantidad == 1){
                    $('#codConcepto'+sec+'monto'+idBeneficiario).attr('disabled',true);
                    $('#codConcepto'+sec+'dias'+idBeneficiario).attr('disabled',false);
                }else if(flagMonto == 1 && flagCantidad == 0){
                    $('#codConcepto'+sec+'monto'+idBeneficiario).attr('disabled',false);
                    $('#codConcepto'+sec+'dias'+idBeneficiario).attr('disabled',true);
                }else if(flagMonto == 0 && flagCantidad == 0){
                    $('#codConcepto'+sec+'monto'+idBeneficiario).disabled = true;
                    $('#codConcepto'+sec+'dias'+idBeneficiario).disabled = true;
                }
                /*
                $("#cuentaContableCuentas").remove();
                $(document.getElementById('distribucionContableCuentas')).append(
                        '<tr id="cuentaContableCuentas">' +
                        '<td width="200">' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta" value="'+pkCuenta+'" readonly>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="codCuenta20" value="'+pkCuenta+'" readonly>' +
                        '</td>' +
                        '<td width="900">' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta" value="'+pkCuenta+'" readonly>' +
                        '<input type="text" style="font-size:11px;" class="form-control text-center" id="nombreCuenta20" value="'+pkCuenta+'" readonly>' +
                        '</td>' +
                        '<td width="150" style="vertical-align: middle;"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfectoCuenta" value="'+pkCuenta+'" readonly></td>' +
                        '</tr>'
                );

                 $(document.getElementById('distribucionContablePartidas')).append(
                 '<tr id="cuentaContablePartidas">' +
                 '<td width="200"><input type="text" style="font-size:11px;" class="form-control text-center" id="codPartida" value="" readonly></td>' +
                 '<td width="900"><input type="text" style="font-size:11px;" class="form-control text-center" id="nombrePartida" value="" readonly></td>' +
                 '<td width="150"><input type="text" style="font-size:11px;" class="form-control text-center" id="montoAfectoPart" value="" readonly></td>' +
                 '</tr>'
                 );*/

            }
            $('#cerrarModal3').click();

            $('#contenidoTabla').on('click', '.delete', function () {
                var campo = $(this).attr('id');
                $('#codConcepto' + campo).remove();
                actualizarMonto();
            });

            $('#contenidoTabla').on('change', '.dias', function () {
                var idBeneficiario = $(this).attr('idBenef');
                var campo = $(this).attr('idTr');
                    var dias = $(this).val();
                    var monto = $('#'+campo+'montoC').val();
                    var total = dias * parseFloat(monto);
                $('#'+campo+'montoTotal'+idBeneficiario).attr('value',total.toFixed(2));
                actualizarMonto();

            });

            $('#contenidoTabla').on('change', '.monto', function () {
                var idBeneficiario = $(this).attr('idBenef');
                var campo = $(this).attr('idTr');
                var monto = $('#'+campo+'monto'+idBeneficiario).val();
                $('#'+campo+'montoTotal'+idBeneficiario).attr('value',monto);
                actualizarMonto();

            });

            function actualizarMonto(){

                var montoTotal = 0;
                $('.montoTotal').each(function( titulo, valor ){
                    var monto =$(this).val();

                    montoTotal = parseFloat(montoTotal) + parseFloat(monto);
                });
                $('#monto').attr('value',montoTotal.toFixed(2));
                $('#monto').html(montoTotal.toFixed(2));

                $('#totalViatico').attr('value',montoTotal.toFixed(2));
                $('#totalViatico').html(montoTotal.toFixed(2));

            }
        });
    });
</script>
