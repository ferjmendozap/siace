
<div class="modal-body">
    <div class="row">
        <div class="card">
            <form action="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/registrarVouchersMET" id="formAjax" class="form floating-label form-validate" role="form" method="post" novalidate="novalidate">
                <input type="hidden" name="form[int][ind_mes]" value="{date('m')}"/>
                <input type="hidden" name="form[int][ind_anio]" value="{date('Y')}"/>
                <input type="hidden" name="form[int][fk_a001_num_organismo]" value="{$organismo.pk_num_organismo}"/>
                <input type="hidden" name="form[int][fk_cbb005_num_contabilidades]" value="{$contabilidad}"/>
                <input type="hidden" name="idObligacion" value="{$idObligacion}"/>
                <input type="hidden" name="voucherContab" value="{$voucherContab}"/>
                <div class="card-body">
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table id="datatable1" class="table table-striped table-hover style-primary">
                                <thead>
                                <tr>
                                    <th>PERIODO</th>
                                    <th>VOUCHER</th>
                                    <th>FECHA</th>
                                    <th>STATUS</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                    <td>-</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="table-responsive">
                            <table id="datatable1" class="table table-striped table-hover style-primary">
                                <thead>
                                <tr>
                                    <th>LINEA</th>
                                    <th>ERRORES</th>
                                    <th>PERIODO</th>
                                    <th>VOUCHER</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    {if "$debitos" != "$creditos"}
                                        <td colspan="4" style="color: red">
                                            ERROR. LOS DEBITOS Y CREDITOS NO SON IGUALES!
                                        </td>
                                    {else}
                                        <td colspan="4" style="color: red">
                                        </td>
                                    {/if}
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group floating-label" id="descripcionError">
                                <input type="text" class="form-control" id="descripcion" name="txt_titulo_voucher" value="{$datosOblig.ind_comentarios}">
                                <label for="descripcion"><i class="md md-person"></i> Descripci&oacute;n</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group floating-label date" id="fechaError">
                                <input type="text" class="form-control" id="fecha" name="form[txt][fec_fecha_voucher]" value="{$fechaVocuher}">
                                <label for="fecha"><i class="md md-today"></i> Fecha</label>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group floating-label" id="voucherError">
                                <select name="form[int][fk_cbc003_num_voucher]" class="form-control select2" data-placeholder="Seleccione Voucher">
                                        {foreach item=i from=$voucher}
                                            {if isset($codVoucher) and $codVoucher == $i.pk_num_voucher }
                                                <option value="{$i.pk_num_voucher}" selected>{$i.ind_descripcion}</option>
                                            {/if}
                                        {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group floating-label date" id="libroError">
                                <select name="form[int][fk_cbb002_num_libro_contable]" class="form-control select2" data-placeholder="Seleccione Libro Contable">
                                    {foreach item=i from=$libroContable}
                                        <option value="{$i.pk_num_libro_contable}">{$i.ind_descripcion}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group floating-label date" id="preparadoPorError">
                                <input type="text" class="form-control" id="preparadoPor" value="{$_Parametros.nombreUsuario}">
                                <label for="preparadoPor"><i class="md md-today"></i> Preparado por:</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group floating-label date" id="aprobadoPorError">
                                <input type="text" class="form-control" id="aprobadoPor" value="{$_Parametros.nombreUsuario}">
                                <label for="aprobadoPor"><i class="md md-today"></i> Aprobado por:</label>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="card-body">
                    <div class="text-right">
                        <button type="button" class="btn btn-default logsUsuarioModal "
                                descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
                            <i class="icm icm-close"></i>&nbsp;Rechazar
                        </button>
                        &nbsp;&nbsp;
                        {if "$debitos" == "$creditos"}
                            <button type="button" class="btn btn-primary-light logsUsuarioModal" id="accion">
                                <i class="md md-done"></i>&nbsp;Aceptar</button>
                        {/if}
                    </div>
                    <div class="table-responsive">
                        <table id="datatable1" class="table table-striped table-hover">
                            <thead>
                            <tr class="style-primary">
                                <th>#</th>
                                <th>FECHA</th>
                                <th>CUENTA</th>
                                <th>DESCRIPCION</th>
                                <th>PERSONA</th>
                                <th>DOCUMENTO</th>
                                <th>C. COSTO</th>
                                <th>DEBE</th>
                                <th>HABER</th>
                            </tr>
                            </thead>
                            <tbody>
                            {$c=0}
                            {if $datoArreglo}
                                {$debitos=0} {$creditos=0}
                                {foreach item=i from=$datoArreglo.Debe}
                                    {$c=$c+1}
                                    <input type="hidden" name="form[int][detalle_fk_cbb004_num_cuenta][]" id="detalle_fk_cbb004_num_cuenta{$c}" value="{$i.pk_num_cuenta}">
                                    <input type="hidden" name="form[int][detalle_num_debe][]" id="detalle_num_debe{$c}" value="{number_format($i.MontoVoucher,'2',',','.')}">
                                    <input type="hidden" name="form[alphaNum][detalle_ind_descripcion][]" id="detalle_ind_descripcion{$c}" value="{$i.ind_descripcion}">
                                    <input type="hidden" name="form[int][detalle_fk_a003_num_persona][]" id="detalle_fk_a003_num_persona{$c}" value="{$datosOblig.fk_a003_num_persona_proveedor}">
                                    <input type="hidden" name="form[int][detalle_fk_a023_num_centro_costo][]" id="detalle_fk_a023_num_centro_costo{$c}" value="1">
                                    <input type="hidden" name="form[int][detalle_num_haber][]" id="detalle_num_haber{$c}" value="0">
                                    <tr>
                                        <td>{$c}</td>
                                        <td>{$datosOblig.fec_pago}</td>
                                        <td>{$i.cod_cuenta}</td>
                                        <td>{$i.ind_descripcion}</td>
                                        <td>{$datosOblig.proveedor}</td>
                                        <td>OP  {{$datosOblig.ind_num_orden}}</td>
                                        <td>{$centroCosto['pk_num_centro_costo']}</td>
                                        <td>{number_format($i.MontoVoucher,'2',',','.')}</td>
                                        <td  style="color: red"></td>
                                    </tr>
                                    {$debitos=$debitos+$i.MontoVoucher}
                                {/foreach}
                                {foreach item=i from=$datoArreglo.Haber}
                                {$c=$c+1}
                                    <input type="hidden" name="form[int][detalle_fk_cbb004_num_cuenta][]" id="detalle_fk_cbb004_num_cuenta{$c}" value="{$i.pk_num_cuenta}">
                                    <input type="hidden" name="form[alphaNum][detalle_ind_descripcion][]" id="detalle_ind_descripcion{$c}" value="{$i.ind_descripcion}">
                                    <input type="hidden" name="form[int][detalle_fk_a003_num_persona][]" id="detalle_fk_a003_num_persona{$c}" value="{$datosOblig.fk_a003_num_persona_proveedor}">
                                    <input type="hidden" name="form[int][detalle_fk_a023_num_centro_costo][]" id="detalle_fk_a023_num_centro_costo{$c}" value="1">
                                    <input type="hidden" name="form[int][detalle_num_debe][]" id="detalle_num_debe{$c}" value="0">
                                    <input type="hidden" name="form[int][detalle_num_haber][]" id="detalle_num_haber{$c}" value="{number_format($i.MontoVoucher,'2',',','.')}">

                                    <tr>
                                        <td>{$c}</td>
                                        <td>{$datosOblig.fec_pago}</td>
                                        <td>{$i.cod_cuenta}</td>
                                        <td>{$i.ind_descripcion}</td>
                                        <td>{$datosOblig.proveedor}</td>
                                        <td>OP  {{$datosOblig.ind_num_orden}}</td>
                                        <td>{$centroCosto['pk_num_centro_costo']}</td>
                                        <td> </td>
                                        <td  style="color: red">{number_format($i.MontoVoucher,'2',',','.')}</td>
                                    </tr>
                                    {$creditos=$creditos+$i.MontoVoucher}
                            {/foreach}
                            {/if}
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3">
                                        Nro de Lineas: {$c}
                                    </th>
                                    <th colspan="2">
                                        Tiene un descuento de <input type="text" class="form-control" id="lineas" value="{$datosOblig.num_monto_descuento}" style="font-weight:bold; font-size:12px;" readonly>

                                    </th>
                                    <th colspan="2" style="font-weight:bold; font-size:16px;">
                                        Total:
                                    </th>
                                    <th colspan="1">
                                        <input type="text" class="form-control" id="creditos" value="{number_format($debitos,'2',',','.')}" style="font-weight:bold; font-size:18px;" readonly>
                                    </th>
                                    <th colspan="1">
                                        <input type="text" class="form-control" id="debitos" value="{number_format($creditos,'2',',','.')}" style="font-weight:bold; font-size:18px; color: red"" readonly>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        /// Complementos
        var app = new  AppFunciones();

        // anular accion del Formulario
        $("#formAjax").submit(function(){
            return false;
        });

        //Guardado y modales
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='no-periodo'){
                    swal('Error','EL periodo contable no se encuentra abierto.', 'error');
                }else if (dato['status'] == 'ok2') {
                    $(document.getElementById('cerrarModal2')).click();
                    swal('Registro Exitoso','El voucher fue registrado satisfactoriamente.', 'success');

                }
            },'json');
        });
    });
</script>