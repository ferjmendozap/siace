<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de  {if $titulo == 'PartidasTrans' or $titulo == 'PartidasConcepto'} Partidas {else} {$titulo}{/if}</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    {if $titulo == 'Partidas' or $titulo == 'PartidasTrans' or $titulo == 'PartidasConcepto'}
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>{if $titulo == 'PartidasTrans' or $titulo == 'PartidasConcepto'} Partidas {else} {$titulo}{/if}</th>
                                <th>Descripcion</th>
                            </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr id="cod{if isset($i.pk_num_partida_presupuestaria)}{$i.pk_num_partida_presupuestaria}{else}{$i.pk_num_cuenta}{/if}">
                                <input type="hidden" value="{if isset($i.pk_num_partida_presupuestaria)}{$i.pk_num_partida_presupuestaria}{else}{$i.pk_num_cuenta}{/if}"
                                       class="registro"
                                       idPartida="{if isset($i.pk_num_partida_presupuestaria)}{$i.pk_num_partida_presupuestaria}{/if}"
                                       idCuenta="{if isset($i.pk_num_cuentaOnco)}{$i.pk_num_cuentaOnco}{/if}"
                                       descripcionPartida="{if isset($i.pk_num_partida_presupuestaria)}{$i.ind_denominacion}{/if}"
                                       descripcionCuenta="{if isset($i.pk_num_cuentaOnco)}{$i.ind_descripcionOnco}{/if}"
                                       codPartida="{if isset($i.pk_num_partida_presupuestaria)}{$i.cod_partida}{/if}"
                                       codCuenta="{if isset($i.pk_num_cuentaOnco)}{$i.cod_cuentaOnco}{/if}"

                                       idCuenta20="{if isset($i.pk_num_cuenta20)}{$i.pk_num_cuenta20}{/if}"
                                       descripcionCuenta20="{if isset($i.pk_num_cuenta20)}{$i.ind_descripcion20}{/if}"
                                       codCuenta20="{if isset($i.pk_num_cuenta20)}{$i.cod_cuenta20}{/if}">
                                <td>{if isset($i.pk_num_partida_presupuestaria)}{$i.cod_partida}{else}{$i.cod_cuenta}{/if}</td>
                                <td>{if isset($i.pk_num_partida_presupuestaria)}{$i.ind_denominacion}{else}{$i.ind_descripcion}{/if}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                    {else}
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <div class="section-header">
                                    <h2 class="text-primary">Oncop </h2>
                                </div>
                                <table id="datatable2" class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>{$titulo}</th>
                                    <th>Descripcion</th>
                                </tr>
                                </thead>
                                <tbody>
                                {foreach item=i from=$listaOnco}
                                    <tr id="cod{$i.pk_num_cuenta}">
                                        <input type="hidden" value="{$i.pk_num_cuenta}"
                                               class="registro"
                                               idCuenta="{$i.pk_num_cuenta}"
                                               descripcionCuenta="{$i.ind_descripcion}"
                                               codCuenta="{$i.cod_cuenta}">
                                        <td>{$i.cod_cuenta}</td>
                                        <td>{$i.ind_descripcion}</td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>
                            </div>
                            <div class="col-sm-6">
                                <div class="section-header">
                                    <h2 class="text-primary">Publicación 20 </h2>
                                </div>
                                <table id="datatable3" class="table table-striped table-hover">
                                    <thead>
                                    <tr>
                                        <th>{$titulo}</th>
                                        <th>Descripcion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {foreach item=i from=$listaPub20}
                                        <tr id="cod{if isset($i.pk_num_partida_presupuestaria)}{$i.pk_num_partida_presupuestaria}{else}{$i.pk_num_cuenta}{/if}">
                                            <input type="hidden" value="{$i.pk_num_cuenta}"
                                                   class="registro"
                                                   idCuenta="{$i.pk_num_cuenta}"
                                                   descripcionCuenta="{$i.ind_descripcion}"
                                                   codCuenta="{$i.cod_cuenta}">
                                            <td>{$i.cod_cuenta}</td>
                                            <td>{$i.ind_descripcion}</td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable3').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');
            if('{$titulo}' == 'PartidasTrans'){
                $(document.getElementById('{$idCampo}partida')).val(input.attr('idPartida'));
                $(document.getElementById('{$idCampo}codigo')).val(input.attr('codPartida'));


            }else if('{$titulo}' == 'PartidasConcepto'){
                $(document.getElementById('pkPartida')).val(input.attr('idPartida'));
                $(document.getElementById('codigo')).val(input.attr('codPartida'));


            }else if('{$titulo}' == 'Cuentas') {
                var cc = $(document.getElementById('fk_a023_num_centro_de_costo')).val();
                var persona = $(document.getElementById('codigoProveedor1')).val();
                $(document.getElementById('partidasCuentas')).append(
                        '<tr id="codCuentas{$tr}">' +
                        '<input type="hidden" class="ajaxPartidas" value="{$tr}" name="form[int][partidaCuenta][ind_secuencia][{$tr}]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}CUENTA" value="' + input.attr('idCuenta') + '" name="form[int][partidaCuenta][{$tr}][fk_cbb004_num_cuenta]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}CUENTA20" value="' + input.attr('idCuenta20') + '" name="form[int][partidaCuenta][{$tr}][fk_cbb004_num_cuenta_pub20]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}PARTIDA" value="' + input.attr('idPartida') + '" name="form[int][partidaCuenta][{$tr}][fk_prb002_num_partida_presupuestaria]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}CC" value="' + cc + '" name="form[int][partidaCuenta][{$tr}][fk_a023_num_centro_costo]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}PERSONA" value="' + persona + '" name="form[int][partidaCuenta][{$tr}][fk_a003_num_persona_proveedor]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}DESCRIPCION" value="" name="form[int][partidaCuenta][{$tr}][ind_descripcion]"> ' +
                        '<td width="35" style="vertical-align: middle;"><input type="text"  class="form-control text-center" value="{$tr}" readonly ></td>' +

                        '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$tr}PARTIDA_C" value="" readonly></td>' +
                        '<td width="250" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$tr}PARTIDA_D" value="" readonly></td>' +
                        '<td width="150">' +
                        '<input type="text" class="form-control text-center" id="codCuentas{$tr}CUENTA_C" value="' + input.attr('codCuenta') + '" readonly>' +
                        '</td>' +
                        '<td width="250">' +
                        '<input type="text" class="form-control text-center" id="codCuentas{$tr}CUENTA_D" value="' + input.attr('descripcionCuenta') + '" readonly>' +
                        '</td>' +
                        '<td width="65" style="vertical-align: middle;">' +
                        '<input type="text" class="form-control text-center accionModal" id="codCuentas{$tr}CC_C" value="' + cc + '" ' +
                        'data-toggle="modal"' +
                        'data-target="#formModal2"' +
                        'titulo="Listado Centro de Costo"' +
                        'enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET/partida/codCuentas{$tr}" readonly>' +
                        '</td>' +
                        '<td width="60" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$tr}PERSONA_C" value="' + persona + '" readonly></td>' +
                        '<td width="40" style="vertical-align: middle;">' +
                        '<div class="checkbox checkbox-styled">' +
                        '<label>' +
                        '<input type="checkbox" id="codCuentas{$tr}FLAG_NO_AFECTO" name="form[int][partidaCuenta][{$tr}][num_flag_no_afecto]" value="1">' +
                        '<span></span>' +
                        '</label>' +
                        '</div>' +
                        '</td>' +
                        '<td width="160" style="vertical-align: middle;"><input type="text" class="form-control text-right montoDistribucion ajaxPartidas" idTR="codCuentas{$tr}" value="0" name="form[int][partidaCuenta][{$tr}][num_monto]"></td>' +
                        '<td width="70" style="vertical-align: middle;">' +
                        '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$tr}"><i class="md md-delete"></i></button>' +
                        '<button class="btn ink-reaction btn-raised btn-xs btn-warning visualizar" id="{$tr}"><i class="md md-remove-red-eye"></i></button>' +
                        '</td>' +
                        '</tr>'
                );
            }else {
                var cc = $(document.getElementById('fk_a023_num_centro_de_costo')).val();
                var persona = $(document.getElementById('codigoProveedor1')).val();
                $(document.getElementById('partidasCuentas')).append(
                        '<tr id="codCuentas{$tr}">' +
                        '<input type="hidden" class="ajaxPartidas" value="{$tr}" name="form[int][partidaCuenta][ind_secuencia][{$tr}]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}CUENTA" value="' + input.attr('idCuenta') + '" name="form[int][partidaCuenta][{$tr}][fk_cbb004_num_cuenta]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}CUENTA20" value="' + input.attr('idCuenta20') + '" name="form[int][partidaCuenta][{$tr}][fk_cbb004_num_cuenta_pub20]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}PARTIDA" value="' + input.attr('idPartida') + '" name="form[int][partidaCuenta][{$tr}][fk_prb002_num_partida_presupuestaria]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}CC" value="' + cc + '" name="form[int][partidaCuenta][{$tr}][fk_a023_num_centro_costo]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}PERSONA" value="' + persona + '" name="form[int][partidaCuenta][{$tr}][fk_a003_num_persona_proveedor]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}DESCRIPCION" value="" name="form[int][partidaCuenta][{$tr}][ind_descripcion]"> ' +
                        '<td width="35" style="vertical-align: middle;"><input type="text"  class="form-control text-center" value="{$tr}" readonly ></td>' +
                        '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$tr}PARTIDA_C" value="' + input.attr('codPartida') + '" readonly></td>' +
                        '<td width="250" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$tr}PARTIDA_D" value="' + input.attr('descripcionPartida') + '" readonly></td>' +
                        '<td width="150">' +
                        '<input type="text" class="form-control text-center" id="codCuentas{$tr}CUENTA_C" value="' + input.attr('codCuenta') + '" readonly>' +
                        '<input type="text" class="form-control text-center" id="codCuentas{$tr}CUENTA20_C" value="' + input.attr('codCuenta20') + '" readonly>' +
                        '</td>' +
                        '<td width="250">' +
                        '<input type="text" class="form-control text-center" id="codCuentas{$tr}CUENTA_D" value="' + input.attr('descripcionCuenta') + '" readonly>' +
                        '<input type="text" class="form-control text-center" id="codCuentas{$tr}CUENTA20_D" value="' + input.attr('descripcionCuenta20') + '" readonly>' +
                        '</td>' +
                        '<td width="65" style="vertical-align: middle;">' +
                        '<input type="text" class="form-control text-center accionModal" id="codCuentas{$tr}CC_C" value="' + cc + '" ' +
                        'data-toggle="modal"' +
                        'data-target="#formModal2"' +
                        'titulo="Listado Centro de Costo"' +
                        'enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET/partida/codCuentas{$tr}" readonly>' +
                        '</td>' +
                        '<td width="60" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$tr}PERSONA_C" value="' + persona + '" readonly></td>' +
                        '<td width="40" style="vertical-align: middle;">' +
                        '<div class="checkbox checkbox-styled">' +
                        '<label>' +
                        '<input type="checkbox" id="codCuentas{$tr}FLAG_NO_AFECTO" name="form[int][partidaCuenta][{$tr}][num_flag_no_afecto]" value="1">' +
                        '<span></span>' +
                        '</label>' +
                        '</div>' +
                        '</td>' +
                        '<td width="160" style="vertical-align: middle;"><input type="text" class="form-control text-right montoDistribucion ajaxPartidas" idTR="codCuentas{$tr}" value="0" name="form[int][partidaCuenta][{$tr}][num_monto]"></td>' +
                        '<td width="70" style="vertical-align: middle;">' +
                        '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$tr}"><i class="md md-delete"></i></button>' +
                        '<button class="btn ink-reaction btn-raised btn-xs btn-warning visualizar" id="{$tr}"><i class="md md-remove-red-eye"></i></button>' +
                        '</td>' +
                        '</tr>'
                );
            }

            $('#cerrarModal2').click();
        });

        $('#datatable3').on('click', 'tbody tr', function () {
            var input = $(this).find('input');
            if('{$titulo}' ==  'Cuentas') {
                var cc = $(document.getElementById('fk_a023_num_centro_de_costo')).val();
                var persona = $(document.getElementById('codigoProveedor1')).val();
                $(document.getElementById('partidasCuentas')).append(
                        '<tr id="codCuentas{$tr}">' +
                        '<input type="hidden" class="ajaxPartidas" value="{$tr}" name="form[int][partidaCuenta][ind_secuencia][{$tr}]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}CUENTA" value="' + input.attr('idCuenta') + '" name="form[int][partidaCuenta][{$tr}][fk_cbb004_num_cuenta]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}CUENTA20" value="' + input.attr('idCuenta20') + '" name="form[int][partidaCuenta][{$tr}][fk_cbb004_num_cuenta_pub20]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}PARTIDA" value="' + input.attr('idPartida') + '" name="form[int][partidaCuenta][{$tr}][fk_prb002_num_partida_presupuestaria]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}CC" value="' + cc + '" name="form[int][partidaCuenta][{$tr}][fk_a023_num_centro_costo]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}PERSONA" value="' + persona + '" name="form[int][partidaCuenta][{$tr}][fk_a003_num_persona_proveedor]"> ' +
                        '<input type="hidden" class="ajaxPartidas" id="codCuentas{$tr}DESCRIPCION" value="" name="form[int][partidaCuenta][{$tr}][ind_descripcion]"> ' +
                        '<td width="35" style="vertical-align: middle;"><input type="text"  class="form-control text-center" value="{$tr}" readonly ></td>' +

                        '<td width="150" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$tr}PARTIDA_C" value="" readonly></td>' +
                        '<td width="250" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$tr}PARTIDA_D" value="" readonly></td>' +
                        '<td width="150">' +
                        '<input type="text" class="form-control text-center" id="codCuentas{$tr}CUENTA_C" value="' + input.attr('codCuenta') + '" readonly>' +
                        '</td>' +
                        '<td width="250">' +
                        '<input type="text" class="form-control text-center" id="codCuentas{$tr}CUENTA_D" value="' + input.attr('descripcionCuenta') + '" readonly>' +
                        '</td>' +
                        '<td width="65" style="vertical-align: middle;">' +
                        '<input type="text" class="form-control text-center accionModal" id="codCuentas{$tr}CC_C" value="' + cc + '" ' +
                        'data-toggle="modal"' +
                        'data-target="#formModal2"' +
                        'titulo="Listado Centro de Costo"' +
                        'enlace="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/centroCostoMET/partida/codCuentas{$tr}" readonly>' +
                        '</td>' +
                        '<td width="60" style="vertical-align: middle;"><input type="text" class="form-control text-center" id="codCuentas{$tr}PERSONA_C" value="' + persona + '" readonly></td>' +
                        '<td width="40" style="vertical-align: middle;">' +
                        '<div class="checkbox checkbox-styled">' +
                        '<label>' +
                        '<input type="checkbox" id="codCuentas{$tr}FLAG_NO_AFECTO" name="form[int][partidaCuenta][{$tr}][num_flag_no_afecto]" value="1">' +
                        '<span></span>' +
                        '</label>' +
                        '</div>' +
                        '</td>' +
                        '<td width="160" style="vertical-align: middle;"><input type="text" class="form-control text-right montoDistribucion ajaxPartidas" idTR="codCuentas{$tr}" value="0" name="form[int][partidaCuenta][{$tr}][num_monto]"></td>' +
                        '<td width="70" style="vertical-align: middle;">' +
                        '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$tr}"><i class="md md-delete"></i></button>' +
                        '<button class="btn ink-reaction btn-raised btn-xs btn-warning visualizar" id="{$tr}"><i class="md md-remove-red-eye"></i></button>' +
                        '</td>' +
                        '</tr>'
                );
            }

            $('#cerrarModal2').click();
        });
    });
</script>