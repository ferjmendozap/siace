<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Personas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Persona</th>
                            <th>Descripcion</th>
                            <th>Emp.</th>
                            <th>Pro.</th>
                            <th>Otr.</th>
                            <th>Nro. Documento</th>
                            <th>Doc. Fiscal</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=persona from=$lista}
                            <tr id="idPersona{$persona.pk_num_persona}">
                                <input type="hidden" value="{$persona.pk_num_persona}" class="persona"
                                       proveedor="{$persona.pk_num_persona}"
                                       nombre="{$persona.ind_nombre1} {$persona.ind_nombre2} {$persona.ind_apellido1} {$persona.ind_apellido2}"
                                       documentoProv="{$persona.ind_documento_fiscal}"
                                       tipoPersona="{$persona.ind_tipo_persona}"
                                        >

                                <td><label>{$persona.pk_num_persona}</label></td>
                                <td><label>{$persona.ind_nombre1} {$persona.ind_nombre2} {$persona.ind_apellido1} {$persona.ind_apellido2}</label></td>
                                <td><i class="{if $persona.cod=='EMP'}md md-check{/if}"></i></td>
                                <td><i class="{if $persona.cod=='PROV'}md md-check{/if}"></i></td>
                                <td><i class="{if $persona.cod=='PART'}md md-check{/if}"></i></td>
                                <td><label>{$persona.ind_cedula_documento}</label></td>
                                <td><label>{$persona.ind_documento_fiscal}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');

            {if $listaPersona == 'personaLista'}
                $(document.getElementById('codigoProveedorLista')).val(input.attr('proveedor'));
                $(document.getElementById('nombreProveedorLista')).val(input.attr('nombre'));
            {/if}

            {if $listaPersona == 'persona'}
            $(document.getElementById('tipoProveedor')).val(input.attr('tipoPersona'));
            $(document.getElementById('codigoProveedor1')).val(input.attr('proveedor'));
            $(document.getElementById('nombreProveedor1')).val(input.attr('nombre'));
            $(document.getElementById('nombreProveedor2')).val(input.attr('nombre'));
            $(document.getElementById('documentoProveedor')).val(input.attr('documentoProv'));
            $(document.getElementById('codigoProveedorPagar')).val(input.attr('proveedor'));
            $(document.getElementById('nombreProveedorPagar')).val(input.attr('nombre'));

            //TIPO DE DOCUMENTO - busco los tipos de documentos para el proveedor seleccionado
            $.post('{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/listasDocumentoServicioMET/documento', {
                idPersona: input.attr('proveedor')
            }, function (dato) {
                if(dato.length > 0){
                    $('#fk_cpb002_num_tipo_documento').html('');
                    //muestro el listado de los tipos de documentos del proveedor
                    for(var i=0; i < dato.length; i++){
                        $('#fk_cpb002_num_tipo_documento').append('<option value="'+dato[i]['pk_num_tipo_documento']+'">'+dato[i]['ind_descripcion']+'</option>');
                        //selecciono el primero de la lista por defecto
                        $('#s2id_fk_cpb002_num_tipo_documento .select2-chosen').html(dato[0]['ind_descripcion']);
                    }
                }
            },'json');

            //TIPO DE SERVICIO - busco los tipos de servicio para el proveedor seleccionado
            $.post('{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/listasDocumentoServicioMET/servicio', {
                idPersona: input.attr('proveedor')
            }, function (dato) {
                if(dato.length > 0) {
                    $('#fk_cpb017_num_tipo_servicio').html('');
                    //muestro el listado de los tipo de servicio del proveedor
                    for (var i = 0; i < dato.length; i++) {
                        $('#fk_cpb017_num_tipo_servicio').append('<option value="' + dato[i]['pk_num_tipo_servico'] + '">' + dato[i]['ind_descripcion'] + '</option>');
                        //selecciono el primero de la lista por defecto
                        $('#s2id_fk_cpb017_num_tipo_servicio .select2-chosen').html(dato[0]['ind_descripcion']);
                        var tipoServicio = dato[0]['pk_num_tipo_servico'];
                    }
                }else {
                    var tipoServicio = $('#fk_cpb017_num_tipo_servicio').val();
                }
                    //ejecuto funcion para actualizar los monto de impuestos y retenciones de acuerdo al tipo de servicio
                    $.post('{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/impuestoMET/IVA', {
                        idTipoServicio: tipoServicio
                    }, function (dato) {
                        var sec = dato.length;
                        var tr = 1;
                        for (var i = 0; i <sec; i++) {
                            var descripcion = dato[i]['txt_descripcion'];
                            var factor = dato[i]['num_factor_porcentaje'];
                            var porcentaje = parseFloat(factor)/100;
                            var imponible = dato[i]['cod_detalle'];
                            if (dato) {
                                if (dato[i]['cod_detalle'] == 'N' && dato[i]['tipo'] == 'IVA') {
                                    $('#totalImpuesto').attr('ivaporcentaje', dato[i]['num_factor_porcentaje']);
                                }
                                if (dato[i]['cod_impuesto'] != 'I01' && dato[i]['cod_impuesto'] != 'I02') {
                                    $(document.getElementById('impuesto'+tr)).remove();
                                    if(tr < sec) {
                                        $(document.getElementById('impuesto' + sec)).remove();
                                    }
                                    $(document.getElementById('contenidoTabla')).append(
                                            '<tr id="impuesto'+tr+'">' +
                                            '<input type="hidden" value="'+tr+'" name="form[int][impuestosRetenciones][ind_secuencia]['+tr+']">' +
                                            '<input type="hidden" value="0" id="num_monto_afecto'+tr+'" name="form[int][impuestosRetenciones]['+tr+'][num_monto_afecto]">' +
                                            '<input type="hidden" value="0" id="num_monto_impuesto'+tr+'" name="form[int][impuestosRetenciones]['+tr+'][num_monto_impuesto]">' +
                                            '<input type="hidden" value="'+imponible+'" id="imponible'+tr+'">' +
                                            '<input type="hidden" value="'+porcentaje+'" id="porcentaje'+tr+'">' +
                                            '<input type="hidden" value="'+dato[i]['fk_cbb004_num_plan_cuenta']+'" name="form[int][impuestosRetenciones]['+tr+'][fk_cbb004_num_cuenta]">' +
                                            '<input type="hidden" value="'+dato[i]['fk_cbb004_num_plan_cuenta_pub20']+'" name="form[int][impuestosRetenciones]['+tr+'][fk_cbb004_num_cuenta_pub20]">' +
                                            '<input type="hidden" value="'+dato[i]['pk_num_impuesto']+'" name="form[int][impuestosRetenciones]['+tr+'][fk_cpb015_num_impuesto]">' +
                                            '<td width="35"><input type="text" style="font-size:11px;" class="form-control text-center" value="'+tr+'" readonly ></td>' +
                                            '<td width="190"><input type="text" style="font-size:11px;" class="form-control text-center"  value="'+descripcion+'" readonly></td>' +
                                            '<td width="100"><input type="text" style="font-size:11px;" class="form-control text-center" id="monto'+tr+'"  value="0" readonly></td>' +
                                            '<td width="100"><input type="text" style="font-size:11px;" class="form-control text-center"  value="'+factor+'" readonly></td>' +
                                            '<td width="100"><input type="text" style="font-size:11px;" class="form-control text-center muntoImpuesto" id="montoTotal'+tr+'" value="0" readonly></td>'+
                                            '<td width="35" id="impuesto'+tr+'" width="10px" class="text-center" style="vertical-align: middle;">' +
                                            '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="'+tr+'">' +
                                            '<i class="md md-delete"></i></button></td>' +
                                            '</tr>'
                                    );
                                    tr = tr+1;
                                }
                            }
                        }
                    },'json');
            },'json');

            //TIPO DE PAGO - busco el tipo de pago para el proveedor seleccionado
            $.post('{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/listasDocumentoServicioMET/tipoPago', {
                idPersona: input.attr('proveedor')
            }, function (dato) {
                if(dato){
                    var tipoPago = $('#fk_a006_num_miscelaneo_tipo_pago').val();
                    var tipoPagoProv = dato['pk_num_miscelaneo_detalle'];
                    //selecciono el tipo de pago del proveedor en la lista de obligaciones
                    if(tipoPagoProv != tipoPago){
                        $('#fk_a006_num_miscelaneo_tipo_pago option[value="'+tipoPagoProv+'"]').attr('selected',true);
                        $('#s2id_fk_a006_num_miscelaneo_tipo_pago .select2-chosen').html($('#fk_a006_num_miscelaneo_tipo_pago option:selected').text());
                    }
                }
            },'json');
            $('#cerrarModal2').click();

            {else}
            $(document.getElementById('codigoProveedorPagar')).val(input.attr('proveedor'));
            $(document.getElementById('nombreProveedorPagar')).val(input.attr('nombre'));

            $('#cerrarModal2').click();

            if('{$listaPersona}' == 'personaCajaC') {
                $(document.getElementById('fk_rhb001_num_empleado_beneficiario')).val(input.attr('proveedor'));
                $(document.getElementById('beneficiario')).val(input.attr('nombre'));
                $(document.getElementById('fk_rhb001_num_empleado_persona_pagar')).val(input.attr('proveedor'));
                $(document.getElementById('beneficiarioPagar')).val(input.attr('nombre'));
            }
            if('{$listaPersona}' == 'personaCajaCpagar'){
                $(document.getElementById('fk_rhb001_num_empleado_persona_pagar')).val(input.attr('proveedor'));
                $(document.getElementById('beneficiarioPagar')).val(input.attr('nombre'));
            }
            if('{$listaPersona}' == 'personaConcepto'){
                $(document.getElementById('{$idCampo}pkPersona')).val(input.attr('proveedor'));
                $(document.getElementById('{$idCampo}persona')).val(input.attr('nombre'));
                $(document.getElementById('{$idCampo}rif')).val(input.attr('documentoProv'));
            }
            if('{$listaPersona}' == 'transacciones'){
                $(document.getElementById('{$idCampo}persona')).val(input.attr('proveedor'));
            }
            if('{$listaPersona}' == 'ingresadoPor'){
                $(document.getElementById('codigoIngresadoPor')).val(input.attr('proveedor'));
                $(document.getElementById('nombreIngresadoPor')).val(input.attr('nombre'));
            }
            {/if}


        });
    });
</script>
