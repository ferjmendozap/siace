<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">LISTA DE OBLIGACIONES </h2>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-2">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkProveedor">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text"
                                               class="form-control" id="codigoProveedor"
                                               value=""
                                               readonly size="9%">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="nombreProveedor"
                                                   value=""
                                                   disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        id="botonPersona"
                                                        disabled="disabled"
                                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkIngresadoPor">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="ingresadoPor"
                                               class="control-label" style="margin-top: 10px;"> Ingresado por:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text"
                                               class="form-control" id="codigoIngresadoPor"
                                               value=""
                                               readonly size="9%">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="nombreIngresadoPor"
                                                   value=""
                                                   disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        id="botonPersonaIngresado"
                                                        disabled="disabled"
                                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/ingresadoPor/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkDoc">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="cuenta"
                                               class="control-label" style="margin-top: 10px;"> Tipo Documento:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="doc"
                                                data-placeholder="Seleccione Tipo de Documento">
                                            <option value="">Seleccione...</option>}
                                            {foreach item=proceso from=$listadoDocumento}
                                                <option value="{$proceso.pk_num_tipo_documento}">{$proceso.ind_descripcion}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFechaDoc">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Fecha Doc:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="hasta"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Fecha Pago:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="hasta"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               disabled="disabled"
                                               readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkDoc" checked class="checked">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="cuenta"
                                               class="control-label" style="margin-top: 10px;"> Estado:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="doc"
                                                data-placeholder="Seleccione Tipo de Documento">
                                            <option value="">Seleccione...</option>}
                                            {foreach item=proceso from=$listadoDocumento}
                                                <option value="{$proceso.pk_num_tipo_documento}">{$proceso.ind_descripcion}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkPeriodo" checked>
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Periodo:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control text-center datePeriodo"
                                               id="periodo"
                                               style="text-align: center"
                                               value="{date('Y-m')}"
                                        readonly>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Fecha Aprobación:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               disabled="disabled">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="hasta"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               disabled="disabled">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <div class="col-sm-12">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkDoc">
                                                <span>Totales por Persona</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">

                                </div>

                                <div class="col-sm-4">

                                </div>
                            </div>

                            <div align="center">
                                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                                        >BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>


                    <form action="{$_Parametros.url}modCP/reportes/obligacionesReportesCONTROL/" id="formAjax"
                          class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body" id="resultado" style="height: 560px" >

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           var app = new AppFunciones();
           app.metWizard();

           /// Complementos
           $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});
           $('.datePeriodo').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es'});


           $('.accionModal').click(function () {
               $('#formModalLabel2').html($(this).attr('titulo'));
               $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                   $('#ContenidoModal2').html($dato);
               });
           });

           //busqueda segun el filtro
           $('.buscar').click(function () {

               var url = '{$_Parametros.url}modCP/reportes/obligacionesCONTROL/imprimirListaObligacionesMET';
               $('#resultado').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

           });

           //habilitar y deshabilitar campos del filtro
           $('.checked').click(function () {
               if(this.checked=!this.checked){
                   this.checked
               }
           });

           $('#checkProveedor').click(function () {
               if(this.checked){
                   $('#botonPersona').attr('disabled', false);
               }else{
                   $('#botonPersona').attr('disabled', true);
                   $(document.getElementById('codigoProveedor')).val("");
                   $(document.getElementById('nombreProveedor')).val("");

               }
           });
           $('#checkIngresadoPor').click(function () {
               if(this.checked){
                   $('#botonPersonaIngresado').attr('disabled', false);
               }else{
                   $('#botonPersonaIngresado').attr('disabled', true);
                   $(document.getElementById('codigoIngresadoPor')).val("");
                   $(document.getElementById('nombreIngresadoPor')).val("");

               }
           });
           $('#checkDoc').click(function () {
               if($('#doc').attr('disabled') =='disabled') {
                   $('#doc').attr('disabled', false);
               }else{
                   $('#doc').attr('disabled', true);
                   $(document.getElementById('doc')).val("");
               }
           });
           $('#checkFechaDoc').click(function () {
               if(this.checked){
                   $('#desde').attr('disabled', false);
                   $('#hasta').attr('disabled', false);
               }else{
                   $('#desde').attr('disabled', true);
                   $(document.getElementById('desde')).val("");
                   $('#hasta').attr('disabled', true);
                   $(document.getElementById('hasta')).val("");

               }
           });
           $('#checkPeriodo').click(function () {
               if(this.checked){
                   $('#periodo').attr('disabled', false);
               }else{
                   $('#periodo').attr('disabled', true);
                   $(document.getElementById('periodo')).val("");

               }
           });
       });

</script>