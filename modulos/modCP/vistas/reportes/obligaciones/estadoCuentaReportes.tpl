<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">ESTADO DE CUENTA </h2>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <div class="col-sm-6">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkProveedor" class="checked" checked>
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="fk_a003_num_persona_proveedor"
                                               class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text"
                                               class="form-control" id="codigoProveedor"
                                               value=""
                                               readonly size="9%">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="nombreProveedor"
                                                   value=""
                                                   disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        id="botonPersona"
                                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkDoc">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="cuenta"
                                               class="control-label" style="margin-top: 10px;"> Tipo Documento:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="doc"
                                                data-placeholder="Seleccione Tipo de Documento">
                                            <option value="">Seleccione...</option>}
                                            {foreach item=proceso from=$listadoDocumento}
                                                <option value="{$proceso.pk_num_tipo_documento}">{$proceso.ind_descripcion}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha" class="checked" checked>
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Fecha:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control text-center date"
                                               id="fecha"
                                               style="text-align: center"
                                               value="{date("Y-m-d")}"
                                               readonly>
                                    </div>
                                </div>
                            </div>

                            <div align="center">
                                <button class="buscarRetencion logsUsuario btn ink-reaction btn-raised btn-primary"
                                        id="buscar" idTipo="fecha">BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form action="" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#uno" data-toggle="tab" id="unoPDF" idTipo="fecha" class="buscarRetencion"><span class="step">1</span> <span class="title" style="font-weight:bold;">FORMATO STANDARD: A UNA FECHA</span></a></li>
                                    <li><a href="#dos" data-toggle="tab" id="dosPDF" idTipo="rango" class="buscarRetencion"><span class="step">2</span> <span class="title" style="font-weight:bold;">POR RANGO DE FECHA</span></a></li>
                                </ul>
                            </div>
                            <div id="rangoFecha" class="col-sm-12" style="background: #aeb3b9; display: none">
                                <div class="col-sm-6">
                                    <div class="col-sm-2">
                                        <label for="desde"
                                               class="control-label" style="font-weight:bold; margin-top: 10px;"> Desde:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               value="{date("Y-m")}-01"
                                               readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="uno">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="unoLista" style="height: 560px" >

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="dos">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="dosLista" style="height: 560px">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <ul class="pager wizard">
                                <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Último</a></li>
                            </ul>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           var app = new AppFunciones();
           app.metWizard();
           /// Complementos
           $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

           ///acciones
           $('.accionModal').click(function () {
               $('#formModalLabel2').html($(this).attr('titulo'));
               $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                   $('#ContenidoModal2').html($dato);
               });
           });

           //busqueda segun el filtro
           $('.buscarRetencion').click(function () {
               var proveedor = $('#codigoProveedor').val();
               var hasta = $('#fecha').val();
               var tipoDoc = $('#doc').val();
               var tipo = $(this).attr('idTipo');
               $('#buscar').attr('idTipo', tipo);

               if (tipo == 'fecha') {
                   $('#rangoFecha').css("display", "none");
               }else{
                   $('#rangoFecha').css("display", "inline");
               }

               if(proveedor == ""){
                   swal("Error!", "Debe seleccionar un Proveedor", "error");
               }else {
                   if (tipo == 'fecha') {
                       var url = '{$_Parametros.url}modCP/reportes/obligacionesCONTROL/imprimirEstadoCuentaMET/?proveedor=' + proveedor + '&' + 'hasta=' + hasta + '&' + 'tipoDoc=' + tipoDoc;
                       $('#unoLista').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

                   }else{
                       var desde = $('#desde').val();
                       var url = '{$_Parametros.url}modCP/reportes/obligacionesCONTROL/imprimirEstadoCuentaMET/?proveedor=' + proveedor + '&' + 'hasta=' + hasta + '&' + 'desde=' + desde + '&' + 'tipoDoc=' + tipoDoc;
                       $('#dosLista').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

                   }
               }
           });

           //habilitar y deshabilitar campos del filtro
           $('.checked').click(function () {
               if(this.checked=!this.checked){
                   this.checked
               }
           });

           $('#checkDoc').click(function () {
               if(this.checked){
                   $('#doc').attr('disabled', false);
               }else{
                   $('#doc').attr('disabled', true);
                   $(document.getElementById('doc')).val("");
               }
           });

       });

</script>