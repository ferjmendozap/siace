<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">DOCUMENTOS EMITIDOS / CHEQUES GIRADOS</h2>
                    </div>
                    <div class="card">
                        <div class="card-body" id="filtro">
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkTipoPago" checked="checked" class="checked">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="tipoPago"
                                               class="control-label" style="margin-top: 10px;"> Tipo Pago:</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control select2-list select2" required
                                                id="tipoPago"
                                                data-placeholder="Seleccione Tipo de Pago">
                                            {foreach item=tipoPago from=$listadoTipoPago}
                                                <option value="{$tipoPago.pk_num_miscelaneo_detalle}">{$tipoPago.ind_nombre_detalle}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha" checked="checked" class="checked">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Fecha:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               style="text-align: center"
                                               readonly
                                               value="{date('Y-m')}-01"
                                               placeholder="Desde">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="hasta"
                                               style="text-align: center"
                                               readonly
                                               value="{date('Y-m-d')}"
                                               placeholder="Hasta">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkCuenta">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="cuenta"
                                               class="control-label" style="margin-top: 10px;"> Cta. Bancaria:</label>
                                    </div>
                                    <div class="col-sm-7">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="cuenta"
                                                data-placeholder="Seleccione Cuenta Bancaria">
                                            <option value="">Seleccione...</option>}
                                            {foreach item=proceso from=$listadoCuentas}
                                                <option value="{$proceso.pk_num_cuenta}">{$proceso.ind_num_cuenta}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div align="center">
                                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                                        >BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body" id="resultado" style="height: 560px" >

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           var app = new AppFunciones();
           app.metWizard();

           /// Complementos
           $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

           $('.accionModal').click(function () {
               $('#formModalLabel2').html($(this).attr('titulo'));
               $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                   $('#ContenidoModal2').html($dato);
               });
           });

           //busqueda segun el filtro
           $('.buscar').click(function () {

               var cuenta = $('#cuenta').val();
               var tipoPago = $('#tipoPago').val();
               var desde = $('#desde').val();
               var hasta = $('#hasta').val();

               var url = '{$_Parametros.url}modCP/reportes/obligacionesCONTROL/imprimirDocumentosEmitidosMET/?cuenta=' + cuenta + '&' + 'tipoPago=' + tipoPago + '&' + 'desde=' + desde + '&' + 'hasta=' + hasta;
               $('#resultado').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

           });

           //habilitar y deshabilitar campos del filtro
           $('.checked').click(function () {
               if(this.checked=!this.checked){
                   this.checked
               }
           });

           $('#checkCuenta').click(function () {
               if(this.checked){
                   $('#cuenta').attr('disabled', false);
               }else{
                   $('#cuenta').attr('disabled', true);
                   $(document.getElementById('cuenta')).val("");

               }
           });

       });

</script>