<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">REPORTE DE BANCOS </h2>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkBanco">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Banco:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group" id="pk_num_banco_tipo_transaccionError" style="margin-top: -10px;">
                                            <select name="form[int][fk_a006_num_miscelaneo_detalle_banco]"
                                                    class="form-control select2-list select2" required
                                                    data-placeholder="Seleccione el Tipo de Banco"
                                                    id="BANCO"
                                                    disabled="disabled">
                                                <option value="">Seleccione el Banco</option>
                                                {foreach item=banco from=$selectBANCOS}
                                                    <option value="{$banco.pk_num_miscelaneo_detalle}" >{$banco.ind_nombre_detalle}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="col-sm-3 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Cta. Bancaria:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group"
                                             id="pk_num_tipo_documentoError" style="margin-top: -10px;">
                                            <select class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria"
                                                    id="CUENTA"
                                                    disabled="disabled">
                                                <option value="">Seleccione Cuenta Bancaria</option>}
                                                <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" checked class="checked">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3" >
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Fecha:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               style="text-align: center"
                                               value="{date('Y-m')}-01"
                                               placeholder="Desde"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="hasta"
                                               style="text-align: center"
                                               value="{date('Y-m-d')}"
                                               placeholder="Hasta"
                                               readonly>
                                    </div>
                                </div>
                            </div>
                            <div align="center">
                                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                                        id="buscar" idCuenta="" >BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>

                    <form action="{$_Parametros.url}modCP/pagos/chequesCONTROL" id="formAjax"
                          class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-body" id="resultado" style="height: 560px" >

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        /// Complementos
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

        $("#BANCO").change(function(){
            var url = '{$_Parametros.url}modCP/procesos/procesosCONTROL/cuentasBancariasMET/';
            var idBanco = $(this).val();
            $.post(url,{  idBanco: idBanco },
                    function (dato) {
                        if (dato) {
                            $('#CUENTA').html('');
                            $('#CUENTA').append('<option value="">Seleccione...</option>');
                            var id = dato['id'];
                            for (var i = 0; i < id.length; i++) {
                                $('#CUENTA').append('<option value="' + id[i]['pk_num_cuenta'] + '" selected>' + id[i]['ind_num_cuenta'] + '</option>');
                            }
                        }
                    }, 'json');
        });

        $('#buscar').click(function () {
            var idCuenta = $('#CUENTA').val();
            var desde = $('#desde').val();
            var hasta = $('#hasta').val();

            var url = '{$_Parametros.url}modCP/reportes/bancosCONTROL/imprimirLibroBancoMET/?idCuenta='+idCuenta+'&'+'desde='+desde+'&'+'hasta='+hasta;
            $('#resultado').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

        });

        //habilitar y deshabilitar campos del filtro
        $('.checked').click(function () {
            if(this.checked=!this.checked){
                this.checked
            }
        });
        $('#checkBanco').click(function () {
            if($('#BANCO').attr('disabled') == 'disabled') {
                $('#BANCO').attr('disabled', false);
                $('#CUENTA').attr('disabled', false);
            }else{
                $('#BANCO').attr('disabled', true);
                $(document.getElementById('BANCO')).val("");
                $('#CUENTA').attr('disabled', true);
                $(document.getElementById('CUENTA')).val("");

            }
        });
    });

</script>