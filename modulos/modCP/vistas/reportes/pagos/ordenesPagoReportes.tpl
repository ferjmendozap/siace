<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">LISTA DE ORDENES DE PAGO </h2>
                    </div>
                    <div class="card">
                        <div class="card-body" id="filtro">
                            <div class="col-sm-12">
                                <div class="col-sm-5">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkProveedor">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="proveedor"
                                               class="control-label" style="margin-top: 10px;"> Proveedor:</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <input type="text"
                                               class="form-control" id="codigoProveedor"
                                               value=""
                                               readonly size="9%">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control"
                                                   id="nombreProveedor"
                                                   value=""
                                                   disabled>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group floating-label">
                                                <button
                                                        class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                        type="button"
                                                        data-toggle="modal"
                                                        data-target="#formModal2"
                                                        titulo="Listado de Personas"
                                                        id="botonPersona"
                                                        disabled="disabled"
                                                        url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/personaMET/persona/">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-1">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkDoc">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="cuenta"
                                               class="control-label" style="margin-top: 10px;"> Tipo Documento:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control select2"
                                                disabled="disabled"
                                                id="doc"
                                                data-placeholder="Seleccione Tipo de Documento">
                                            <option value="">Seleccione...</option>}
                                            {foreach item=proceso from=$listadoDocumento}
                                                <option value="{$proceso.pk_num_tipo_documento}">{$proceso.ind_descripcion}</option>
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="nroDoc">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Nro Documento:</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control text-center"
                                               id="nroDocumento"
                                               style="text-align: center"
                                               value=""
                                               disabled="disabled">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="col-sm-5">
                                    <div class="col-sm-1 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkBanco">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Banco:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group" id="pk_num_banco_tipo_transaccionError" style="margin-top: -10px;">
                                            <select name="form[int][fk_a006_num_miscelaneo_detalle_banco]"
                                                    class="form-control select2-list select2" required
                                                    data-placeholder="Seleccione el Tipo de Banco"
                                                    id="BANCO"
                                                    disabled="disabled">
                                                <option value="">Seleccione el Banco</option>
                                                {foreach item=banco from=$selectBANCOS}
                                                    <option value="{$banco.pk_num_miscelaneo_detalle}" >{$banco.ind_nombre_detalle}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="col-sm-3">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Cta. Bancaria:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group"
                                             id="pk_num_tipo_documentoError" style="margin-top: -10px;">
                                            <select class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria"
                                                    id="CUENTA"
                                                    disabled="disabled">
                                                <option value="">Seleccione Cuenta Bancaria</option>}
                                                <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="col-sm-2 text-right">
                                        <div class="checkbox checkbox-styled">
                                            <label>
                                                <input type="checkbox" value="1" id="checkFecha">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> F. Orden:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="desde"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               disabled="disabled">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center date"
                                               id="hasta"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               disabled="disabled">
                                    </div>
                                </div>

                            </div>

                            <div align="center">
                                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                                        id="buscar" idTipo="presupuesto">BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form action="{$_Parametros.url}modCP/reportes/pagosCONTROL" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#uno" data-toggle="tab" id="unoPDF" idTipo="presupuesto" class="buscar"><span class="step">1</span> <span class="title" style="font-weight:bold;">ORDENES CON DETALLE PRESUPUESTARIO</span></a></li>
                                    <li><a href="#dos" data-toggle="tab" id="dosPDF" idTipo="contable" class="buscar"><span class="step">2</span> <span class="title" style="font-weight:bold;">ORDENES CON DETALLE CONTABLE</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active" id="uno">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-body" id="unoLista" style="height: 560px" >

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <div class="tab-pane" id="dos">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-body" id="dosLista" style="height: 560px">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="pager wizard">
                            <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                            <li class="next"><a class="btn-raised" href="javascript:void(0);" id="siguiente">Último</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();

        /// Complementos
        $('.date').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});

        $('.buscar').click(function () {
            var tipo = $(this).attr('idTipo');
            $('#buscar').attr('idTipo', tipo);

            var proveedor = $('#codigoProveedor').val();
            var documento = $('#doc').val();
            var cuenta = $('#CUENTA').val();
            var fdesde = $('#desde').val();
            var fhasta = $('#hasta').val();

            if(tipo == 'presupuesto'){
                var url = '{$_Parametros.url}modCP/reportes/pagosCONTROL/imprimirDetallePresupuestarioContableMET/?proveedor=' + proveedor +'&'+'documento=' + documento +'&'+'cuenta=' + cuenta +'&'+'fdesde=' + fdesde +'&'+'fhasta=' + fhasta;
                $('#unoLista').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');

            }else{
                var url = '{$_Parametros.url}modCP/reportes/pagosCONTROL/imprimirDetallePresupuestarioContableMET?proveedor=' + proveedor +'&'+'documento=' + documento +'&'+'cuenta=' + cuenta +'&'+'fdesde=' + fdesde +'&'+'fhasta=' + fhasta;
                $('#dosLista').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');
            }
        });

        ///acciones
        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        $("#BANCO").change(function(){
            var url = '{$_Parametros.url}modCP/procesos/procesosCONTROL/cuentasBancariasMET/';
            var idBanco = $(this).val();
            $.post(url,{  idBanco: idBanco },
                    function (dato) {
                        if (dato) {
                            $('#CUENTA').html('');
                            $('#CUENTA').append('<option value="">Seleccione...</option>');
                            var id = dato['id'];
                            for (var i = 0; i < id.length; i++) {
                                $('#CUENTA').append('<option value="' + id[i]['pk_num_cuenta'] + '" selected>' + id[i]['ind_num_cuenta'] + '</option>');
                            }
                        }
                    }, 'json');
        });

        //habilitar y deshabilitar campos del filtro
        $('#checkProveedor').click(function () {
            if(this.checked){
                $('#botonPersona').attr('disabled', false);
            }else{
                $('#botonPersona').attr('disabled', true);
                $(document.getElementById('codigoProveedor')).val("");
                $(document.getElementById('nombreProveedor')).val("");

            }
        });
        $('#checkDoc').click(function () {
            if(this.checked){
                $('#doc').attr('disabled', false);
            }else{
                $('#doc').attr('disabled', true);
                $(document.getElementById('doc')).val("");
            }
        });
        $('#nroDoc').click(function () {
            if(this.checked){
                $('#nroDocumento').attr('disabled', false);
            }else{
                $('#nroDocumento').attr('disabled', true);
                $(document.getElementById('nroDocumento')).val("");
            }
        });
        $('#checkBanco').click(function () {
            if(this.checked){
                $('#BANCO').attr('disabled', false);
                $('#CUENTA').attr('disabled', false);
            }else{
                $('#BANCO').attr('disabled', true);
                $(document.getElementById('BANCO')).val("");
                $('#CUENTA').attr('disabled', true);
                $(document.getElementById('CUENTA')).val("");

            }
        });
        $('#checkFecha').click(function () {
            if(this.checked){
                $('#desde').attr('disabled', false);
                $('#hasta').attr('disabled', false);
            }else{
                $('#desde').attr('disabled', true);
                $(document.getElementById('desde')).val("");
                $('#hasta').attr('disabled', true);
                $(document.getElementById('hasta')).val("");

            }
        });

    });

</script>