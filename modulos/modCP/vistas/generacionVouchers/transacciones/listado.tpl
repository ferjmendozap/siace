
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">GENERACIÓN DE VOUCHERS PARA TRANSACCIONES BANCARIAS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>NUMERO</th>
                            <th>FECHA</th>
                            <th>COMENTARIO</th>
                            <th>DOC. REFERENCIA BANCO</th>
                            <th>NRO. DOCUMENTO</th>
                            <th>ESTADO</th>
                            <th>ACCIONES</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$dataBD}
                            <tr id="idPago">
                                <td>{$i.ind_num_transaccion}</td>
                                <td>{$i.fec_transaccion}</td>
                                <td>{$i.txt_comentarios}</td>
                                <td>{$i.ind_num_documento_referencia}</td>
                                <td>{$i.ind_num_documento_referencia}</td>
                                <td>{$i.ind_estado}</td>
                                <td>
                                    <button class="acciones btn ink-reaction btn-raised btn-xs btn-primary-light" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" id="GE" idTransaccion="{$i.pk_num_banco_transaccion}" title="Generar Vouchers"
                                            descipcion="El Usuario ha GENERADO un orden pago" titulo="<i class='icm icm-spinner12'></i> Generar Vouchers de Transaccion">
                                        <i class="icm icm-spinner12" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div></section>


<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/generacionVouchers/transaccionesCONTROL/generarVouchersMET';

        $('#datatable1 tbody').on( 'click', '.acciones', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idTransaccion: $(this).attr('idTransaccion')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

    });
</script>