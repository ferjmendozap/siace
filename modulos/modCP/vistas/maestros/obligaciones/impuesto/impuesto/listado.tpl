
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">IMPUESTOS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Descripcion</th>
                                <th>Tipo</th>
                                <th>Regimen Fiscal</th>
                                <th>%</th>
                                <th>Provision</th>
                                <th>Imponible</th>
                                <th>Signo</th>
                                <th>Estatus</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            {if in_array('CP-01-07-01-02-03-N',$_Parametros.perfil)}
                            <th colspan="10"><button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                    descipcion="el Usuario ha creado un post de impuesto" data-toggle="modal"
                                                    data-target="#formModal" titulo="<i class='md md-create'></i>&nbsp;Registrar Nuevo Impuesto" id="nuevo"
                                                    data-keyboard="false" data-backdrop="static">
                                    <i class="md md-create"></i>&nbsp;Nuevo Impuesto
                                </button></th>
                            {/if}
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/nuevoImpuestoMET';

        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson',
            "{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/jsonDataTablaMET",
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "cod_impuesto" },
                { "data": "txt_descripcion" },
                { "data": "Tipo" },
                { "data": "RegimenFiscal" },
                { "data": "num_factor_porcentaje"},
                { "data": "Provision"},
                { "data": "nombreImponible"},
                { "orderable": false,"data": "ind_signo"},
                { "orderable": false,"data": "num_estatus"},
                { "orderable": false,"data": "acciones"}
            ]
        );
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idImpuesto:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {

                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ idImpuesto: $(this).attr('idImpuesto')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idImpuesto: $(this).attr('idImpuesto') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

    });
</script>