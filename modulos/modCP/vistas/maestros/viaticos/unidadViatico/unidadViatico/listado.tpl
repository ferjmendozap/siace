
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">{if $estado == 'PR'}APROBAR {/if}UNIDAD DE VIATICO</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Periodo</th>
                            <th>Num. Resolucion</th>
                            <th>Tipo Viatico</th>
                            <th>Monto</th>
                            <th>Estado</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=unidad from=$unidadViaBD}
                            <tr id="idUnidadViatico{$unidad.pk_num_unidad_viatico}">
                                <td>{$unidad.ind_periodo}</td>
                                <td>{$unidad.ind_num_resolucion}</td>
                                <td>{$unidad.ind_nombre_detalle}</td>
                                <td>{$unidad.num_unidad_viatico|number_format:2:',':'.'}</td>
                                <td>{$unidad.ind_estado}</td>

                                <td>
                                    {if $estado == 'PR'}
                                        {if in_array('CP-01-07-04-01-02',$_Parametros.perfil)}
                                            <button class="accion btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" id="PR" ver="1" idUnidadViatico="{$unidad.pk_num_unidad_viatico}" title="Aprobar"
                                                    descipcion="El Usuario ha Aprobado una Unidad de Viatico" titulo="<i class='icm icm-rating3'></i> Aprobar Unidad de Viatico">
                                                <i class="icm icm-rating3" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    {else}
                                        {if $unidad.ind_estado == 'PR'}
                                            {if in_array('CP-01-07-04-01-01-01-M',$_Parametros.perfil)}
                                            <button class="modificar btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idUnidadViatico="{$unidad.pk_num_unidad_viatico}" title="Editar"
                                                    descipcion="El Usuario a Modificado una Unidad de Viatico" titulo="<i class='fa fa-edit'></i> Editar Unidad de Viatico">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                            {/if}
                                        {/if}
                                    {/if}
                                    {if in_array('CP-01-07-04-01-01-02-V',$_Parametros.perfil)}
                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static" idUnidadViatico="{$unidad.pk_num_unidad_viatico}" title="Consultar"
                                            descipcion="El Usuario esta viendo una Unidad de Viatico" titulo="<i class='md md-remove-red-eye'></i> Consultar Unidad de Viatico">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="5">
                                {if $estado !='PR'}
                                    {if in_array('CP-01-07-04-01-01-03-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                            descipcion="el Usuario ha creado un post de Unidad de Viatico"
                                            data-toggle="modal" data-target="#formModal"
                                            titulo="<i class='md md-create'></i>&nbsp;Registrar Unidad de Viatico" id="nuevo"
                                            data-keyboard="false"
                                            data-backdrop="static"><i class="md md-create"></i>&nbsp;Nueva Unidad de Viatico
                                    </button>
                                    {/if}
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/viaticos/unidadViatico/unidadViaticoCONTROL/nuevaUnidadViaticoMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idUnidadViatico:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idUnidadViatico: $(this).attr('idUnidadViatico') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idUnidadViatico: $(this).attr('idUnidadViatico')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.accion', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idUnidadViatico: $(this).attr('idUnidadViatico'), estado: $(this).attr('id'), ver:$(this).attr('ver') },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

    });
</script>
