<form action="{$_Parametros.url}modCP/maestros/viaticos/conceptoViaticos/conceptoViaticosCONTROL/nuevoConceptoViaticoMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idConceptoViatico}" name="idConceptoViatico"/>

    <div class="modal-body">
        <div class="col-sm-12">
            <div class="col-sm-3">
                <div class="form-group " id="cod_conceptoError">
                    <input type="text" class="form-control" maxlength="4" name="form[alphaNum][cod_concepto]" id="cod_impuesto" {if isset($dataBD.cod_concepto)} value="{$dataBD.cod_concepto}" disabled {/if} >
                    <label for="cod_concepto"><i class="md md-border-color"></i> Codigo</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group " id="ind_descripcionError">
                <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion"  value="{if isset($dataBD.ind_descripcion)}{$dataBD.ind_descripcion}{/if}" {if $ver==1 } disabled{/if}>
                <label for="ind_descripcion"><i class="md md-border-color"></i> Descripcion</label>
            </div>
        </div>
        <div class="col-sm-12">
          <div class="col-sm-9">
              <div class="form-group " id="fk_b008_num_unidad_viaticoError" style="margin-top: -10px;">
                  <select id="fk_b008_num_unidad_viatico" name="form[int][fk_b008_num_unidad_viatico]" url="{$_Parametros.url}modCP/maestros/viaticos/conceptoViaticos/conceptoViaticosCONTROL/ConsultarMontoUnidadMET"
                          class="form-control select2" {if $ver==1} disabled {/if}>
                      <option value=""></option>
                      {foreach item=i from=$listadoUnidadViatico}
                          {if isset($dataBD.fk_b008_num_unidad_viatico) and $dataBD.fk_b008_num_unidad_viatico == $i.pk_num_unidad_viatico}
                              <option value="{$i.pk_num_unidad_viatico}" selected >{$i.ind_nombre_detalle}</option>
                          {else}
                              <option value="{$i.pk_num_unidad_viatico}">{$i.ind_nombre_detalle}</option>
                          {/if}
                      {/foreach}
                  </select>
                  <label for="fk_b008_num_unidad_viatico"> Unidad de Viatico:</label>
              </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group " id="num_montoError">
                <input type="text" class="form-control input-sm" name="form[formula][num_monto]" id="num_monto"  value="{if isset($dataBD.num_monto)}{$dataBD.num_monto|number_format:2:'.':''}{else}0.00{/if}" {if $ver==1 } disabled{/if}>
                <label for="num_monto">Monto</label>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group " id="fk_a006_num_miscelaneo_detalle_categoriaError" style="margin-top: -10px;">
                    <select id="fk_a006_num_miscelaneo_detalle_categoria" name="form[int][fk_a006_num_miscelaneo_detalle_categoria]"
                            class="form-control select2" {if $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=i from=$listadoCategoria}
                            {if isset($dataBD.fk_a006_num_miscelaneo_detalle_categoria) and $dataBD.fk_a006_num_miscelaneo_detalle_categoria == $i.pk_num_miscelaneo_detalle }
                                <option value="{$i.pk_num_miscelaneo_detalle}" selected >{$i.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_a006_num_miscelaneo_detalle_flag_provision"> Categoría:</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group " id="fk_a006_num_miscelaneo_detalle_articuloError" style="margin-top: -10px;">
                    <select id="fk_a006_num_miscelaneo_detalle_articulo" name="form[int][fk_a006_num_miscelaneo_detalle_articulo]"
                            class="form-control select2" {if $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=i from=$listadoArticulo}
                            {if isset($dataBD.fk_a006_num_miscelaneo_detalle_articulo) and $dataBD.fk_a006_num_miscelaneo_detalle_articulo == $i.pk_num_miscelaneo_detalle}
                                <option value="{$i.pk_num_miscelaneo_detalle}" selected >{$i.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="fk_a006_num_miscelaneo_detalle_articulo"> Artículo:</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6">
                <div class="form-group " id="num_numeralError">
                    <input type="text" class="form-control" maxlength="3" name="form[alphaNum][num_numeral]" id="num_numeral" value="{if isset($dataBD.num_numeral)}{$dataBD.num_numeral}{/if}" {if $ver==1} disabled {/if}>
                    <label for="num_numeral">Numeral:</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group " id="num_valor_utError">
                    <input type="text" class="form-control" maxlength="3" name="form[int][num_valor_ut]" id="num_valor_ut" {if isset($dataBD.num_valor_ut)} value="{$dataBD.num_valor_ut|number_format:2:'.':''}"{/if} {if $ver==1} disabled {/if}>
                    <label for="num_valor_ut">Valor U.T:</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-8">
                <div class="form-group" id="fk_prb002_num_partida_presupuestariaError">
                    <input type="text" class="form-control" value="{if isset($dataBD.fk_prb002_num_partida_presupuestaria)}{$dataBD.cod_partida}{/if}" id="codigo" disabled>
                    <input type="hidden" name="form[int][fk_prb002_num_partida_presupuestaria]" id="pkPartida" value="{if isset($dataBD.fk_prb002_num_partida_presupuestaria)}{$dataBD.fk_prb002_num_partida_presupuestaria}{/if}">
                    <label for="fk_prb002_num_partida_presupuestaria">Partida</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group ">
                    <button
                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                            type="button"
                            data-toggle="modal" data-target="#formModal2"
                            titulo="Listado de Plan Cuenta"
                            url="{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/partidasCuentasMET/PartidasConcepto/"
                            {if $ver==1} disabled {/if}>
                        <i class="md md-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-8">
                <div class="form-group" id="fk_cbb004_num_plan_cuentaError">
                    <input type="text" class="form-control" value="{if isset($dataBD.fk_cbb004_num_plan_cuenta)}{$dataBD.cuenta}{/if}" id="indCuenta" disabled>
                    <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta]" id="fk_cbb004_num_plan_cuenta" value="{if isset($dataBD.fk_cbb004_num_plan_cuenta)}{$dataBD.fk_cbb004_num_plan_cuenta}{/if}">
                    <label for="fk_cbb004_num_plan_cuenta">Cuenta Contable</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group ">
                    <button
                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                            type="button"
                            data-toggle="modal" data-target="#formModal2"
                            titulo="Listado de Plan Cuenta"
                            url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuenta/"
                            {if $ver==1} disabled {/if}>
                        <i class="md md-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-8">
                <div class="form-group" id="fk_cbb004_num_plan_cuenta_pub20Error">
                    <input type="text" class="form-control"   value="{if isset($dataBD.fk_cbb004_num_plan_cuenta_pub20)}{$dataBD.cuentaPub20}{/if}" id="indCuentaPub20" disabled>
                    <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta_pub20]" id="fk_cbb004_num_plan_cuenta_pub20" value="{if isset($dataBD.fk_cbb004_num_plan_cuenta_pub20)}{$dataBD.fk_cbb004_num_plan_cuenta_pub20}{/if}">
                    <label for="fk_cbb004_num_plan_cuenta_pub20">Cuenta Public. 20</label>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group ">
                    <button
                            class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                            type="button"
                            data-toggle="modal" data-target="#formModal2"
                            titulo="Listado de Plan Cuenta"
                            url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuentaPub20"
                            {if $ver==1} disabled {/if}>
                        <i class="md md-search"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-4">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($dataBD.num_flag_monto) and $dataBD.num_flag_monto==1} checked{/if} value="1" name="form[int][num_flag_monto]" {if $ver==1 } disabled{/if}>
                        <span>Por Monto Directo</span>
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($dataBD.num_flag_cantidad) and $dataBD.num_flag_cantidad==1} checked{/if} value="1" name="form[int][num_flag_cantidad]" {if $ver==1 } disabled{/if}>
                        <span>Multiplicar Cantidad </span>
                    </label>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($dataBD.num_flag_unidad_combustible) and $dataBD.num_flag_unidad_combustible==1} checked{/if} value="1" name="form[int][num_flag_unidad_combustible]" id="num_flag_unidad_combustible" {if $ver==1 } disabled{/if}>
                        <span>Unidad Combustible </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-6">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($dataBD.num_estatus) and $dataBD.num_estatus==0}{else} checked{/if} value="1" name="form[int][num_estatus]" {if $ver==1 } disabled{/if}>
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6 ">
                <div class="col-sm-12">
                    <div class="form-group ">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($dataBD.ind_usuario)}{$dataBD.ind_usuario}{/if}"
                               id="ind_usuario">
                        <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group " id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($dataBD.fec_ultima_modificacion)}{$dataBD.fec_ultima_modificacion}{/if}"
                               id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer" >
            <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
                    data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
            {if $ver!=1}
                <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
                {if $idConceptoViatico!=0}
                    <i class="fa fa-edit"></i>&nbsp;Modificar
                {else}
                    <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                {/if}
                    </button>
            {/if}
        </div>
    </div>
</form>

<script type="text/javascript">
        $(document).ready(function() {
            var app = new  AppFunciones();
            $("#formAjax").submit(function(){
                return false;
            });

            $('.accionModal').click(function () {
                $('#formModalLabel2').html($(this).attr('titulo'));
                $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                    $('#ContenidoModal2').html($dato);
                });
            });

            $('.select2').select2({ allowClear: true });
            $('#modalAncho').css( "width", "50%" );

            //al seleccionar unidad de viaticos
            $("#fk_b008_num_unidad_viatico").change(function(){
                $.post($(this).attr('url'), { unidad_viatico: $(this).val() }, function ($dato) {
                     $("#num_monto").val($dato['monto']);
                },'json');
            });

            //al hacer click en unidad de combustible... busco el valor de la unidad de combustible ultimo aprobado
            $("#num_flag_unidad_combustible").click(function(){
                //alert("BUSCAR VALOR UNIDAD DE COMBUSTIBLE");
                if( $('#num_flag_unidad_combustible').is(':checked') ) {
                    $.post('{$_Parametros.url}modCP/maestros/viaticos/conceptoViaticos/conceptoViaticosCONTROL/ConsultarMontoUnidadCombistibleMET', {  }, function ($dato) {
                        $("#num_monto").val($dato['monto']);
                    },'json');
                }else{
                    $("#num_monto").val('0.00');

                }
            });


            $('#accion').click(function(){
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                    var arrayCheck = ['num_flag_monto','num_flag_cantidad','num_estatus'];
                    var arrayMostrarOrden = ['cod_concepto','ind_descripcion','categoria','num_valor_ut','num_estatus'];
                    if(dato['status']=='error'){
                        app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                    }else if(dato['status']=='errorSQL'){
                        app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                    }else if(dato['status']=='errorNumerico'){
                        app.metValidarError(dato,'Disculpa. el Campo solo debe contener valores numericos');
                    }else if(dato['status']=='modificar'){
                        app.metActualizarRegistroTabla(dato,dato['pk_num_concepto_gasto_viatico'],'idConceptoViatico',arrayCheck,arrayMostrarOrden,'El Concepto de Viatico fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }else if(dato['status']=='nuevo'){
                        app.metNuevoRegistroTabla(dato,dato['idConceptoViatico'],'idConceptoViatico',arrayCheck,arrayMostrarOrden,'El Concepto de Viatico fue guardado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }
                },'json');
            });
        });
</script>
