<form action="{$_Parametros.url}modCP/maestros/pagos/tiposDocumentosTransaccion/documentosTransaccionCONTROL/nuevoDocumentoMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idDocumento}" name="idDocumento"/>

    <div class="modal-body">
        <div class="col-sm-12">
            <div class="col-sm-4">
                <div class="form-group floating-label" id="cod_tipo_documentoError">
                    {if isset($dataBD.cod_tipo_documento)}
                        <input type="text" class="form-control" name="form[alphaNum][cod_tipo_documento]" id="cod_tipo_documento"
                             value="{$dataBD.cod_tipo_documento}" disabled >
                    {else}
                        <input type="text" class="form-control" maxlength="2" name="form[alphaNum][cod_tipo_documento]" id="cod_tipo_documento" {if $ver==1} disabled {/if}>
                    {/if}
                    <label for="cod_tipo_documento"><i class="md md-border-color"></i> Codigo</label>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="ind_descripcionError">
                <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion"
                       value="{if isset($dataBD.ind_descripcion)}{$dataBD.ind_descripcion}{/if}" {if $ver==1} disabled {/if}>
                <label for="ind_descripcion"><i class="md md-border-color"></i> Descripcion</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="fk_a006_miscelaneo_detalle_tipo_transaccionError">
                <select id="fk_a006_miscelaneo_detalle_tipo_transaccion" name="form[int][fk_a006_miscelaneo_detalle_tipo_transaccion]"
                        class="form-control select2-list select2" required {if $ver==1} disabled {/if}>
                    <option value=""></option>
                    {foreach item=i from=$listadoTransaccion}
                        {if isset($dataBD.fk_a006_miscelaneo_detalle_tipo_transaccion) and $dataBD.fk_a006_miscelaneo_detalle_tipo_transaccion == $i.pk_num_miscelaneo_detalle}
                            <option value="{$i.pk_num_miscelaneo_detalle}" selected >{$i.ind_nombre_detalle}</option>
                        {else}
                            <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_a006_miscelaneo_detalle_tipo_transaccion">Tipo de Transacción:</label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group floating-label" id="num_estatusError">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($dataBD.num_estatus) and $dataBD.num_estatus==0}{else} checked{/if} value="1" name="form[int][num_estatus]" {if $ver==1} disabled {/if}>
                        <span>Estatus </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled"
                           value="{if isset($dataBD.ind_usuario)}{$dataBD.ind_usuario}{/if}"
                           id="ind_usuario">
                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fec_ultima_modificacionError">
                    <input type="text" disabled class="form-control disabled"
                           value="{if isset($dataBD.fec_ultima_modificacion)}{$dataBD.fec_ultima_modificacion}{/if}"
                           id="fec_ultima_modificacion">
                    <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                </div>
            </div>
        </div>
    </div>


    <div class="modal-footer" >
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $ver!=1}
        <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">
            {if $idDocumento!=0}
                <i class="fa fa-edit"></i>&nbsp;Modificar
            {else}
                <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
            {/if}
        </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $("#formAjax").submit(function(){
            return false;
        });
        $('.select2').select2({ allowClear: true });
        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#modalAncho').css( "width", "45%" );
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = '';
                var arrayMostrarOrden = ['cod_tipo_documento','ind_descripcion','ind_nombre_detalle','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['pk_num_tipo_documento'],'idDocumento',arrayCheck,arrayMostrarOrden,'El Tipo de Documento fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idDocumento'],'idDocumento',arrayCheck,arrayMostrarOrden,'El Tipo de Documento fue guardado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });

    });
</script>