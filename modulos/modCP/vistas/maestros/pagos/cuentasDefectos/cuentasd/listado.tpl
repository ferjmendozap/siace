
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">CUENTAS BANCARIAS POR DEFECTO</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Tipo de Pago</th>
                            <th>Cuenta Bancaria</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=cuentad from=$cuentasdBD}
                                <tr id="idCuentad{$cuentad.pk_num_cuenta_bancaria_defecto}">
                                    <td>{$cuentad.ind_nombre_detalle}</td>
                                    <td>{$cuentad.ind_num_cuenta}</td>
                                    <td>
                                        {if in_array('CP-01-07-02-02-01-M',$_Parametros.perfil)}
                                        <button class="modificar Cuenta btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idCuentad="{$cuentad.pk_num_cuenta_bancaria_defecto}" title="Editar"
                                                descipcion="El Usuario ha Modificado una Cuenta Bancaria" titulo="Editar Cuenta Bancaria por Defecto">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                        {if in_array('CP-01-07-02-02-02-V',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idCuentad="{$cuentad.pk_num_cuenta_bancaria_defecto}" title="Consultar"
                                                descipcion="El Usuario esta viendo una Cuenta Bancaria" titulo="<i class='md md-remove-red-eye'></i> Consultar Cuenta Bancaria">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                        {if in_array('CP-01-07-02-02-03-E',$_Parametros.perfil)}
                                        <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCuentad="{$cuentad.pk_num_cuenta_bancaria_defecto}" title="Eliminar"  boton="si, Eliminar"
                                                descipcion="El usuario ha eliminado una Cuenta Bancaria" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar una Cuenta Bancaria!!">
                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="3">
                                {if in_array('CP-01-07-02-02-04-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                        descipcion="el Usuario ha creado una Cuenta Bacaria por Defecto"
                                        data-toggle="modal" data-target="#formModal"
                                        titulo="<i class='md md-create'></i>&nbsp;Cuenta Bancaria por Defecto" id="nuevo"
                                        data-keyboard="false" data-backdrop="static"><i class="md md-create"></i>&nbsp;Cuenta Bancaria por Defecto
                                </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function() {

        var $url='{$_Parametros.url}modCP/maestros/pagos/cuentasDefectos/cuentasdCONTROL/nuevaCuentadMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCuentad:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCuentad: $(this).attr('idCuentad')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( $url,{ idCuentad: $(this).attr('idCuentad') , ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idCuentad=$(this).attr('idCuentad');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/pagos/cuentasDefectos/cuentasdCONTROL/eliminarMET';
                $.post($url, { idCuentad: idCuentad },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idCuentad'+dato['idCuentad'])).html('');
                        swal("Eliminado!", "La Cuenta Bancaria ha sido eliminada satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>