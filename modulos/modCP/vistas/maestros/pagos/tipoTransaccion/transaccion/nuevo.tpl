<form action="{$_Parametros.url}modCP/maestros/pagos/tipoTransaccion/transaccionCONTROL/nuevaTransaccionMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idTransaccion}" name="idTransaccion"/>

    <div class="modal-body">

        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group floating-label" id="cod_tipo_transaccionError">
                        <input type="text" class="form-control" name="form[alphaNum][cod_tipo_transaccion]" id="cod_tipo_transaccion" value="{if isset($transaccionBD.cod_tipo_transaccion)}{$transaccionBD.cod_tipo_transaccion}{/if}" {if $ver==1 } disabled{/if}>
                        <label for="cod_tipo_transaccion"><i class="md md-border-color"></i> Codigo</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-10">
                    <div class="form-group floating-label" id="ind_descripcionError">
                        <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion" value="{if isset($transaccionBD.ind_descripcion)}{{$transaccionBD.ind_descripcion}}{/if}" {if $ver==1 } disabled{/if}>
                        <label for="ind_descripcion"><i class="md md-border-color"></i> Descripcion</label>
                    </div>
                </div>
            </div>
            <div class="form-group floating-label" id="fk_a006_miscelaneo_detalle_tipo_transaccionError">
                 <select id="fk_a006_miscelaneo_detalle_tipo" name="form[int][fk_a006_miscelaneo_detalle_tipo_transaccion]"
                         class="form-control select2-list select2" {if $ver==1 } disabled{/if}>
                     <option value=""></option>
                     {foreach item=tipo from=$selectTIPOTRANS}
                         {if isset($transaccionBD.fk_a006_miscelaneo_detalle_tipo_transaccion) and $transaccionBD.fk_a006_miscelaneo_detalle_tipo_transaccion == $tipo.pk_num_miscelaneo_detalle}
                             <option value="{$tipo.pk_num_miscelaneo_detalle}" selected >{$tipo.ind_nombre_detalle}</option>
                         {else}
                             <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                         {/if}
                     {/foreach}
                 </select>
                <label for="fk_a006_miscelaneo_detalle_tipo">Tipo de Transaccion</label>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="num_flag_voucherError" >
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" {if isset($transaccionBD.num_flag_voucher) and $transaccionBD.num_flag_voucher==1} checked{/if} value="1" name="form[int][num_flag_voucher]" {if $ver==1 } disabled{/if}>
                                <span>Genera Voucher </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group" id="fk_cbb004_num_plan_cuentaError">
                            <input type="text" class="form-control"   value="{if isset($transaccionBD.fk_cbb004_num_plan_cuenta)}{$transaccionBD.cod_cuenta}{/if}" id="indCuenta" disabled >
                            <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta]" id="fk_cbb004_num_plan_cuenta" value="{if isset($transaccionBD.fk_cbb004_num_plan_cuenta)}{$transaccionBD.fk_cbb004_num_plan_cuenta}{/if}">
                            <label for="ind_icono">Cuenta Contable</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group floating-label">
                        <button
                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                type="button"
                                {if $ver==1} disabled {/if}
                                data-toggle="modal" data-target="#formModal2"
                                titulo="Listado de Plan Cuenta"
                                url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuenta/">
                            <i class="md md-search"></i>
                        </button>
                    </div>
                </div>
           </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group" id="fk_cbb004_num_plan_cuenta_pub20Error">
                        <input type="text" class="form-control"   value="{if isset($transaccionBD.fk_cbb004_num_plan_cuenta_pub20)}{$transaccionBD.cod_cuentaPub20}{/if}" id="indCuentaPub20" disabled >
                        <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta_pub20]" id="fk_cbb004_num_plan_cuenta_pub20" value="{if isset($transaccionBD.fk_cbb004_num_plan_cuenta_pub20)}{$transaccionBD.fk_cbb004_num_plan_cuenta_pub20}{/if}">
                        <label for="ind_icono">Cuenta Contable (Pub. 20)</label>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group floating-label">
                        <button
                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                type="button"
                                {if $ver==1} disabled {/if}
                                data-toggle="modal" data-target="#formModal2"
                                titulo="Listado de Plan Cuenta"
                                url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuentaPub20/">
                            <i class="md md-search"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="form-group floating-label" id="fk_cbc003_tipo_voucherError">
                <select id="fk_cbc003_tipo_voucher" name="form[int][fk_cbc003_tipo_voucher]"
                        class="form-control select2-list select2" {if $ver==1} disabled {/if}>
                    <option value=""></option>
                    {foreach item=proceso from=$listadoVoucher}
                        {if isset($transaccionBD.fk_cbc003_tipo_voucher) and $transaccionBD.fk_cbc003_tipo_voucher == $proceso.pk_num_voucher }
                            <option value="{$proceso.pk_num_voucher}" selected >{$proceso.ind_descripcion}</option>
                        {else}
                            <option value="{$proceso.pk_num_voucher}">{$proceso.ind_descripcion}</option>
                        {/if}
                    {/foreach}
                </select>
                <label for="fk_cbc003_tipo_voucher">Tipo de Voucher</label>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="num_flag_transaccionError">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" {if isset($transaccionBD.num_flag_transaccion) and $transaccionBD.num_flag_transaccion==1} checked{/if} value="1" name="form[int][num_flag_transaccion]" {if $ver==1 } disabled{/if}>
                                <span>Transaccion del Sistema </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="num_estatusError">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" {if isset($transaccionBD.num_estatus) and $transaccionBD.num_estatus==0}{else} checked {/if} value="1" name="form[int][num_estatus]" {if $ver==1 } disabled{/if}>
                                <span>Estatus </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        <div class="row">
            <div class="col-sm-5 ">
                <div class="col-sm-12">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($transaccionBD.ind_usuario)}{$transaccionBD.ind_usuario}{/if}"
                               id="ind_usuario">
                        <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($transaccionBD.fec_ultima_modificacion)}{$transaccionBD.fec_ultima_modificacion}{/if}"
                               id="fec_ultima_modificacion">
                        <label for="cod_voucher"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
   </div>

    <div class="modal-footer" >
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro"
                data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $ver!=1 }
        <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">
            {if $idTransaccion!=0}
                <i class="fa fa-edit"></i>&nbsp;Modificar
            {else}
                <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
            {/if}
        </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('.select2').select2({ allowClear: true });
        $('#modalAncho').css( "width", "40%" );
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ['num_flag_voucher','num_flag_transaccion','num_estatus'];
                var arrayMostrarOrden = ['cod_tipo_transaccion','ind_descripcion','num_flag_voucher','cod_cuenta','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['pk_num_banco_tipo_transaccion'],'idTransaccion',arrayCheck,arrayMostrarOrden,'El Tipo de Transaccion Bancaria fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idTransaccion'],'idTransaccion',arrayCheck,arrayMostrarOrden,'El Tipo de Transaccion Bancaria fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>