<form action="{$_Parametros.url}modCP/maestros/otros/tiposVoucher/voucherCONTROL/nuevoVoucherMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idVoucher}" name="idVoucher"/>

    <div class="modal-body">
        <div class="col-lg-12">
            <div class="col-lg-5">
                <div class="form-group floating-label" id="cod_voucherError">
                    {if isset($dataBD.cod_voucher)}
                        <input type="hidden" class="form-control" name="form[alphaNum][cod_voucher]" id="cod_voucher"
                               value="{$dataBD.cod_voucher}">
                        <input type="text" class="form-control" name="form[alphaNum][cod_voucher]" id="cod_voucher"
                               value="{$dataBD.cod_voucher}" disabled >
                    {else}
                        <input type="text" class="form-control" name="form[alphaNum][cod_voucher]" id="cod_voucher" disabled>
                    {/if}
                    <label for="cod_voucher"><i class="md md-border-color"></i> Codigo</label>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="form-group floating-label" id="ind_descripcionError">
                <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion" value="{if isset($dataBD.ind_descripcion)}{{$dataBD.ind_descripcion}}{/if}" {if $ver==1} disabled {/if}>
                <label for="ind_descripcion"><i class="md md-border-color"></i> Descripción</label>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-6">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($dataBD.num_flag_manual) and $dataBD.num_flag_manual==1} checked{/if} value="1" name="form[int][num_flag_manual]" {if $ver==1} disabled {/if}>
                    <span>Solo para Voucher Manual</span>
                </label>
            </div>
                </div>
            <div class="col-lg-6">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($dataBD.num_estatus) and $dataBD.num_estatus==1} checked {/if} value="1" name="form[int][num_estatus]" {if $ver==1} disabled {/if}>
                        <span>Estatus</span>
                    </label>
                </div>
            </div>
            </div>
        <div class="row">
            <div class="col-sm-5 ">
                <div class="col-sm-12">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($dataBD.ind_usuario)}{$dataBD.ind_usuario}{/if}"
                               id="ind_usuario">
                        <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="cod_voucherError">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($dataBD.fec_ultima_modificacion)}{$dataBD.fec_ultima_modificacion}{/if}"
                               id="fec_ultima_modificacion">
                        <label for="cod_voucher"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>

            <div class="modal-footer" >
                <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro"
                        data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
                {if $ver!=1}
                <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">
                    {if $idVoucher !=0}
                        <i class="fa fa-edit"></i>&nbsp;Modificar
                    {else}
                        <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                    {/if}
                </button>
                {/if}
            </div>
        </div>
</form>
</script>
<script type="text/javascript">
        $(document).ready(function() {
            var app = new  AppFunciones();
            $("#formAjax").submit(function(){
                return false;
            });
            $('#modalAncho').css( "width", "35%" );
            $('#accion').click(function(){
                $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                    var arrayCheck = ['num_estatus','num_flag_manual'];
                    var arrayMostrarOrden = ['cod_voucher','ind_descripcion','num_flag_manual','num_estatus'];
                    if(dato['status']=='error'){
                        app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                    }else if(dato['status']=='errorSQL'){
                        app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                    }else if(dato['status']=='modificar'){
                        app.metActualizarRegistroTabla(dato,dato['idVoucher'],'idVoucher',arrayCheck,arrayMostrarOrden,'El Tipo de Voucher fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }else if(dato['status']=='nuevo'){
                        app.metNuevoRegistroTabla(dato,dato['idVoucher'],'idVoucher',arrayCheck,arrayMostrarOrden,'El Tipo de Voucher fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                    }
                },'json');
            });
        });
</script>