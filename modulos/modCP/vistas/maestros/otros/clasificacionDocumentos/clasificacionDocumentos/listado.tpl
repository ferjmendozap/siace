<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">LISTADO DE CLASIFICACION DE DOCUMENTOS</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Descripción</th>
                                <th>Ite.</th>
                                <th>Cli.</th>
                                <th>Com.</th>
                                <th>Transacción.</th>
                                <th>Estatus</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=i from=$listado}
                                <tr id="idClasifDocumentos{$i.pk_num_documento_clasificacion}">
                                    <td><label>{$i.ind_documento_clasificacion}</label></td>
                                    <td><label>{$i.ind_descripcion}</label></td>
                                    <td><label><i class="{if $i.num_flag_item==1}md md-check{else}md md-not-interested{/if}"></label></td>
                                    <td><label><i class="{if $i.num_flag_cliente==1}md md-check{else}md md-not-interested{/if}"></label></td>
                                    <td><label><i class="{if $i.num_flag_compromiso==1}md md-check{else}md md-not-interested{/if}"></label></td>
                                    <td><label><i class="{if $i.num_flag_transaccion==1}md md-check{else}md md-not-interested{/if}"></label></td>
                                    <td><label><i class="{if $i.num_estatus==1}md md-check{else}md md-not-interested{/if}"></label></td>
                                    <td>
                                        {if in_array('CP-01-07-05-03-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idClasifDocumentos="{$i.pk_num_documento_clasificacion}" title="Modificar"
                                                    descipcion="El Usuario ha Modificado una Unidad Tributaria" titulo="<i class='md md-map'></i> Modificar Clasificación de Documentos">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        {if in_array('CP-01-07-05-03-03-V',$_Parametros.perfil)}
                                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idClasifDocumentos="{$i.pk_num_documento_clasificacion}" title="Consultar"
                                                    descipcion="El Usuario esta viendo una Clasificación de Documentos" titulo="<i class='md md-remove-red-eye'></i> Consultar Clasificación de Documentos">
                                                <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        {if in_array('CP-01-07-05-03-04-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idClasifDocumentos="{$i.pk_num_documento_clasificacion}" title="Eliminar"  boton="si, Eliminar"
                                                    descipcion="El usuario ha eliminado una Clasificación de Documentos" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la Clasificación de Documentos!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="8">
                                {if in_array('CP-01-07-05-03-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario ha creado una Clasificación de Documentos"  titulo="<i class='md md-create'></i> Crear Clasificación de Documentos" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nueva Clasificación de Documentos
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}modCP/maestros/otros/clasificacionDocumentos/clasificacionDocumentosCONTROL/crearModificarMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idClasifDocumentos:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post(url,{ idClasifDocumentos: $(this).attr('idClasifDocumentos') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
        $('#datatable tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( url,{ idClasifDocumentos: $(this).attr('idClasifDocumentos'), ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable tbody').on( 'click', '.eliminar', function () {
            var idClasifDocumentos=$(this).attr('idClasifDocumentos');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCP/maestros/otros/clasificacionDocumentos/clasificacionDocumentosCONTROL/eliminarMET';
                $.post($url, { idClasifDocumentos: idClasifDocumentos },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idClasifDocumentos'+dato['idClasifDocumentos'])).html('');
                        swal("Eliminado!", "La Clasificación de Documentos ha sido eliminado satisfactoriamente.", "success");
                        $(document.getElementById('cerrarModal')).click();
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>