<form action="{$_Parametros.url}modCP/maestros/otros/unidadTributaria/unidadTributariaCONTROL/crearModificarMET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idUnidadTributaria}" name="idUnidadTributaria"/>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_anioError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_anio)}{$formDB.ind_anio}{else}{date('Y')}{/if}" name="form[int][ind_anio]" id="ind_anio" {if $ver==1 } disabled{/if}>
                        <label for="ind_anio"><i class="md md-map"></i> Anio</label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="fk_a006_num_miscelaneo">Nominas</label>
                            <div class="col-sm-12">
                                <select id="fk_a006_num_miscelaneo" name="form[int][fk_a006_num_miscelaneo]" {if isset($formDB.fk_a006_num_miscelaneo_detalle_clasificacion_unidad) and isset($ver) and $ver==1} disabled{/if} class="form-control select2-list">
                                    <option value="">Seleccione El Tipo De UT</option>
                                    {foreach item=i from=$tipo}
                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_clasificacion_unidad)}
                                        {if $i.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_clasificacion_unidad}
                                        <option selected value="{$i.pk_num_miscelaneo_detalle}" selected>{$i.ind_nombre_detalle}</option>
                                        {else}
                                        <option value="{$i.pk_num_miscelaneo_detalle}" >{$i.ind_nombre_detalle}</option>
                                        {/if}
                                        {else}
                                            <option value="{$i.pk_num_miscelaneo_detalle}" >{$i.ind_nombre_detalle}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="fecError">
                        <div class="input-group date" id="fec">
                            <div class="input-group-content">
                                <input type="text" id="fec" class="form-control" name="form[txt][fec]" value="{if isset($formDB.fec)}{$formDB.fec}{/if}" {if $ver==1 } disabled{/if}>
                                <label for="ind_anio">Fecha</label>
                            </div>
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_valorError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_valor)}{$formDB.ind_valor}{/if}" name="form[int][ind_valor]" id="ind_valor" {if $ver==1 } disabled{/if}>
                        <label for="ind_valor"><i class="md md-map"></i> Valor</label>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">

                <div class="form-group floating-label" id="ind_providencia_numError">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_providencia_num)}{$formDB.ind_providencia_num}{/if}" name="form[int][ind_providencia_num]" id="ind_providencia_num" {if $ver==1 } disabled{/if}>
                    <label for="ind_providencia_num"><i class="md md-map"></i> Numero de Providencia </label>
                </div>
                <div class="form-group floating-label" id="txt_gaceta_oficialError">
                    <textarea id="txt_gaceta_oficial" name="form[txt][txt_gaceta_oficial]" class="form-control" rows="3" placeholder="" {if $ver==1 } disabled{/if}>{if isset($formDB.txt_gaceta_oficial)}{$formDB.txt_gaceta_oficial}{/if} </textarea>
                    <label for="txt_gaceta_oficial"><i class="md md-comment"></i> Gaceta Oficial </label>
                </div>
            </div>
        </div>

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
                data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if !$ver==1 }
            <button type="button" class="btn btn-primary ink-reaction logsUsuarioModal" id="accion">
                {if $idUnidadTributaria!=0}
                    <i class="fa fa-edit"></i>&nbsp;Modificar
                {else}
                    <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    var app = new  AppFunciones();
    $("#formAjax").submit(function(){
        return false;
    });
    $('#modalAncho').css( "width", "55%" );
    $('#fec').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es'});
    $('#accion').click(function(){
        swal({
            title: "¡Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
        $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
            var arrayCheck = ["num_estatus"];
            var arrayMostrarOrden = ['ind_valor','num_estatus'];
            if(dato['status']=='error'){
                app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
            }else if(dato['status']=='modificar'){
                app.metActualizarRegistroTabla(dato,dato['idUnidadTributaria'],'idUnidadTributaria',arrayCheck,arrayMostrarOrden,'La aplicacion fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
            }else if(dato['status']=='nuevo'){
                app.metNuevoRegistroTabla(dato,dato['idUnidadTributaria'],'idUnidadTributaria',arrayCheck,arrayMostrarOrden,'La aplicacion fue guardado satisfactoriamente.','cerrarModal','ContenidoModal');
            }
        },'json');

    });
});
</script>