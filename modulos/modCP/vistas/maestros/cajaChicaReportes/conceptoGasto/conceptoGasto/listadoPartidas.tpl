<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado - Partidas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Partida</th>
                            <th>Descripcion</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=partida from=$lista}
                            <tr id="idPlanCuenta{$partida.pk_num_partida_presupuestaria}">
                                <input type="hidden" value="{$partida.pk_num_partida_presupuestaria}" class="partida"
                                       pk="{$partida.pk_num_partida_presupuestaria}"
                                       partida="{$partida.cod_partida}" >
                                <td><label>{$partida.cod_partida}</label></td>
                                <td><label>{$partida.ind_denominacion}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');

            $(document.getElementById('fk_prb002_num_partida_presupuestaria')).val(input.attr('pk'));
            $(document.getElementById('partida')).val(input.attr('partida'));
            $('#cerrarModal2').click();
            {if $listaPartida == 'partidaObligacion'}
            $(document.getElementById('fk_prb002_num_partida_presupuestaria')).val(input.attr('pk'));
            $(document.getElementById('partida')).val(input.attr('partida'));
            $('#cerrarModal2').click();
            {/if}
        });
    });
</script>
