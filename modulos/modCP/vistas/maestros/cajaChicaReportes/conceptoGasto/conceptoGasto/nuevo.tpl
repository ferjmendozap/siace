<form action="{$_Parametros.url}modCP/maestros/cajaChicaReportes/conceptoGasto/conceptoGastoCONTROL/nuevoConceptoGastoMET" class="form" role="form" id="formAjax" method="post">
    <input type="hidden" value="1" name="valido" />
    <input type="hidden" value="{$idConceptoGasto}" name="idConceptoGasto"/>

    <div class="modal-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group floating-label" id="cod_concepto_gastoError">
                    <input type="text" class="form-control" name="form[alphaNum][cod_concepto_gasto]" id="cod_concepto_gasto" {if isset($conceptoGastoBD.cod_concepto_gasto)} value="{$conceptoGastoBD.cod_concepto_gasto}" disabled {/if} >
                    <label for="cod_concepto_gasto"><i class="md md-border-color"></i> Codigo</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group floating-label" id="ind_descripcionError">
                    <input type="text" class="form-control" name="form[alphaNum][ind_descripcion]" id="ind_descripcion" value="{if isset($conceptoGastoBD.ind_descripcion)}{$conceptoGastoBD.ind_descripcion}{/if}" {if $ver==1 } disabled{/if} >
                    <label for="ind_descripcion"><i class="md md-border-color"></i> Descripción</label>
                </div>
            </div>
        </div>
        <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_concepto_gasto_grupoError">
            <select id="fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo" name="form[int][fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo]"
                    class="form-control select2-list select2"  {if $ver==1 } disabled{/if}>
                <option value=""></option>
                {foreach item=grupo from=$selectGrupo}
                    {if isset($conceptoGastoBD.fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo) and $conceptoGastoBD.fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo == $grupo.pk_num_miscelaneo_detalle}
                        <option value="{$grupo.pk_num_miscelaneo_detalle}" selected >{$grupo.ind_nombre_detalle}</option>
                    {else}
                        <option value="{$grupo.pk_num_miscelaneo_detalle}">{$grupo.ind_nombre_detalle}</option>
                    {/if}
                {/foreach}
            </select>
            <label for="fk_a006_num_miscelaneo_detalle_concepto_gasto_grupo">Seleccione la Clasificacion</label>
        </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-8">
                            <div class="form-group " id="fk_prb002_num_partida_presupuestariaError">
                                <input type="text" class="form-control"   value="{if isset($conceptoGastoBD.fk_prb002_num_partida_presupuestaria)}{$conceptoGastoBD.cod_partida}{/if}" id="partida" disabled >
                                <input type="hidden" name="form[int][fk_prb002_num_partida_presupuestaria]" id="fk_prb002_num_partida_presupuestaria" value="{if isset($conceptoGastoBD.pk_num_partida_presupuestaria)}{$conceptoGastoBD.pk_num_partida_presupuestaria}{/if}" >

                                <label for="fk_prb002_num_partida_presupuestaria">Partida</label>
                            </div>

                    </div>
                    <div class="col-sm-4">
                        <div class="form-group floating-label">
                            <button
                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                    type="button"
                                    data-toggle="modal" data-target="#formModal2"
                                    titulo="Listado - Partidas"
                                    url="{$_Parametros.url}modCP/maestros/cajaChicaReportes/conceptoGasto/conceptoGastoCONTROL/partidaMET/partida/"
                                    {if $ver==1 } disabled {/if}>
                                <i class="md md-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group " id="fk_cbb004_num_plan_cuentaError">
                            <input type="text" class="form-control"   value="{if isset($conceptoGastoBD.fk_cbb004_num_plan_cuenta)}{$conceptoGastoBD.cod_cuenta}{/if}" id="indCuenta"  disabled >
                            <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta]" id="fk_cbb004_num_plan_cuenta" value="{if isset($conceptoGastoBD.pk_num_cuenta)}{$conceptoGastoBD.pk_num_cuenta}{/if}"} >
                            <label for="fk_cbb004_num_plan_cuenta">Cuenta Contable</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group floating-label">
                            <button
                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                    type="button"
                                    data-toggle="modal" data-target="#formModal2"
                                    titulo="Listado de Plan Cuenta"
                                    url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuenta/"
                                    {if $ver==1 } disabled {/if}>
                                <i class="md md-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group " id="fk_cbb004_num_plan_cuenta_pub20Error">
                            <input type="text" class="form-control"   value="{if isset($conceptoGastoBD.fk_cbb004_num_plan_cuenta_pub20)}{$conceptoGastoBD.cod_cuentaPub20}{/if}" id="indCuentaPub20"  disabled >
                            <input type="hidden" name="form[int][fk_cbb004_num_plan_cuenta_pub20]" id="fk_cbb004_num_plan_cuenta_pub20" value="{if isset($conceptoGastoBD.fk_cbb004_num_plan_cuenta_pub20)}{$conceptoGastoBD.fk_cbb004_num_plan_cuenta_pub20}{/if}"} >
                            <label for="fk_cbb004_num_plan_cuenta_pub20">Cuenta Contable (Pub. 20)</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group floating-label">
                            <button
                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                    type="button"
                                    data-toggle="modal" data-target="#formModal2"
                                    titulo="Listado de Plan Cuenta"
                                    url="{$_Parametros.url}modCP/maestros/obligaciones/impuesto/impuestoCONTROL/CuentaMET/cuentaPub20/"
                                    {if $ver==1 } disabled {/if}>
                                <i class="md md-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        <div class="col-lg-12">
            <div class="checkbox checkbox-styled">
                <label>
                    <input type="checkbox" {if isset($conceptoGastoBD.num_estatus) and $conceptoGastoBD.num_estatus==0}{else} checked{/if} value="1" name="form[int][num_estatus]" {if $ver==1 } disabled{/if}>
                    <span>Estatus</span>
                </label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="card">
                <div class="card-head card-head-xs style-primary" align="center">
                    <header>Clasificaciones Validas</header>
                </div>
                <div class="card-body" style="padding: 4px;">
                    <table class="table">
                        <tbody id="imput_nuevo">
                        {if isset($conceptoGastoDetalle.fk_cpb004_num_clasificacion_gastos)}
                            {$nuevo=0}
                            {foreach item=clasific from=$clasificacion}
                                {$nuevo = $nuevo+1}
                                <tr id="nuevo{$nuevo}">
                                    <input type="hidden" value="{$nuevo}" name="form[int][nuevo][secuencia][{$nuevo}]">
                                    <td>
                                        {$var = $clasific.fk_cpb004_num_clasificacion_gastos}
                                        <select class="form-control" data-placeholder="Seleccione la Clasificación de Gasto" name="form[int][nuevo][{$nuevo}][pk_num_clasificacion_gastos]" {if $ver==1 } disabled{/if}>
                                            {foreach item=clasif from=$selectClasifGasto}
                                                {if $var == $clasif.pk_num_clasificacion_gastos}
                                                    <option value="{$clasif.pk_num_clasificacion_gastos}" selected>{$clasif.ind_descripcion}</option>
                                                {else}
                                                    <option value="{$clasif.pk_num_clasificacion_gastos}" >{$clasif.ind_descripcion}</option>
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </td>
                                    <td style="text-align: center">
                                        <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="{$nuevo}" {if $ver==1}disabled{/if}><i class="md md-delete"></i></button>
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}
                        </tbody>
                    </table>
                <div class="col-sm-12" align="center">
                    <button id="nuevoCampo" class="btn btn-info ink-reaction btn-raised" type="button" {if $ver==1 } disabled{/if}>
                    <i class="md md-add "></i> Agregar</button>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-5 ">
                <div class="col-sm-12">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($conceptoGastoBD.ind_usuario)}{$conceptoGastoBD.ind_usuario}{/if}"
                               id="ind_usuario">
                        <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="cod_voucherError">
                        <input type="text" disabled class="form-control disabled"
                               value="{if isset($conceptoGastoBD.fec_ultima_modificacion)}{$conceptoGastoBD.fec_ultima_modificacion}{/if}"
                               id="fec_ultima_modificacion">
                        <label for="cod_voucher"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>

            <div class="modal-footer" >
                <button type="button" class="btn btn-default logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro"
                        data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
                {if $ver!=1}
                <button type="button" class="btn btn-primary logsUsuarioModal" id="accion">
                    {if $idConceptoGasto!=0}
                        <i class="fa fa-edit"></i>&nbsp;Modificar
                    {else}
                        <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
                    {/if}
                </button>
                {/if}
            </div>
        </div>
</form>
</script>
<script type="text/javascript">
         $(document).ready(function() {
             var app = new  AppFunciones();
             $("#formAjax").submit(function(){
                 return false;
             });
             $('.select2').select2({ allowClear: true });
             $('#nuevoCampo').click(function () {
                 var nuevo = $('#imput_nuevo tr').length + 1;
                 $(document.getElementById('imput_nuevo')).append(
                         '<tr id="nuevo' + nuevo + '">' +
                         '<input type="hidden" value="'+nuevo+'" name="form[int][nuevo][secuencia]['+nuevo+']"> '+
                         '<td>' +
                         '<select class="form-control tipoPago" data-placeholder="Seleccione la Clasificación de Gasto" name="form[int][nuevo]['+nuevo+'][pk_num_clasificacion_gastos]" idNum="'+nuevo+'" >' +
                         '{foreach item=clasif from=$selectClasifGasto}'+
                         '<option value="{$clasif.pk_num_clasificacion_gastos}" >{$clasif.ind_descripcion}</option>' +
                         '{/foreach}' +
                         '</select>' +
                         '</td>' +
                         '<td style="text-align: center">'+
                         '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="'+nuevo+'"><i class="md md-delete"></i></button>' +
                         '</td>'+
                         '</tr>'
                 );
             });
             $('#imput_nuevo').on('click', '.delete', function () {
                 var campo = $(this).attr('id');
                 $('#nuevo' + campo).remove();
             });

             $('.accionModal').click(function () {
                 $('#formModalLabel2').html($(this).attr('titulo'));
                 $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                     $('#ContenidoModal2').html($dato);
                 });
             });

            $('#modalAncho').css( "width", "40%" );
            $('#accion').click(function(){
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                    var arrayCheck = ['num_estatus'];
                    var arrayMostrarOrden = ['cod_concepto_gasto','ind_descripcion','cod_partida','cod_cuenta','num_estatus'];
                    if(dato['status']=='error'){
                        app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                    }else if(dato['status']=='errorSQL'){
                        app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                    }else if(dato['status']=='modificar'){
                        app.metActualizarRegistroTabla(dato,dato['pk_num_concepto_gasto'],'idConceptoGasto',arrayCheck,arrayMostrarOrden,'El Concepto de Gasto fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }else if(dato['status']=='nuevo'){
                        app.metNuevoRegistroTabla(dato,dato['idConceptoGasto'],'idConceptoGasto',arrayCheck,arrayMostrarOrden,'El Concepto de Gasto fue guardado satisfactoriamente.','cerrarModal','ContenidoModal');
                    }
                },'json');
            });
        });
</script>