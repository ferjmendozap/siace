<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">REPORTE / CONCILIACION DE LIBRO</h2>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="col-sm-12">
                                <div class="col-sm-4">
                                    <div class="col-sm-3 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Banco:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group" id="pk_num_banco_tipo_transaccionError" style="margin-top: -10px;">
                                            <select name="form[int][fk_a006_num_miscelaneo_detalle_banco]"
                                                    class="form-control select2-list select2" required
                                                    data-placeholder="Seleccione el Tipo de Banco"
                                                    id="BANCO">
                                                <option value="">Seleccione el Banco</option>
                                                {foreach item=banco from=$selectBANCOS}
                                                    <option value="{$banco.pk_num_miscelaneo_detalle}" >{$banco.ind_nombre_detalle}</option>
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="col-sm-3 text-right">
                                        <label for="pk_num_tipo_documento"
                                               class="control-label" style="margin-top: 10px;"> Cta. Bancaria:</label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group"
                                             id="pk_num_tipo_documentoError" style="margin-top: -10px;">
                                            <select class="form-control select2" data-placeholder="Seleccione Cuenta Bancaria"
                                                    id="CUENTA">
                                                <option value="">Seleccione Cuenta Bancaria</option>}
                                                <option value=""></option>

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="col-sm-3 text-right">
                                        <label for="pago"
                                               class="control-label" style="margin-top: 10px;"> Periodo:</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center datePeriodo"
                                               id="periodo_desde"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Desde"
                                               readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control text-center datePeriodo"
                                               id="periodo_hasta"
                                               style="text-align: center"
                                               value=""
                                               placeholder="Hasta"
                                               readonly>
                                    </div>
                                </div>
                            </div>
                            <div align="center">
                                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary"
                                        id="buscar" idCuenta="{$proceso.pk_num_cuenta}" >BUSCAR
                                </button>
                            </div>
                        </div>
                    </div>

                        <form action="{$_Parametros.url}modCP/pagos/chequesCONTROL" id="formAjax"
                              class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idOrdenPago}" id="idOrdenPago" name="idOrdenPago"/>
                            <div class="section-header">
                                <h3 class="text-danger" align="center">Resultados</h3>
                            </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-body" id="resultado" style="height: 560px" >

                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
       $(document).ready(function () {
           /// Complementos
           $('.datePeriodo').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es' });

           $("#BANCO").change(function(){
               var url = '{$_Parametros.url}modCP/procesos/procesosCONTROL/cuentasBancariasMET/';
               var idBanco = $(this).val();
               $.post(url,{  idBanco: idBanco },
                       function (dato) {
                           if (dato) {
                               $('#CUENTA').html('');
                               $('#CUENTA').append('<option value="">Seleccione...</option>');
                               var id = dato['id'];
                               for (var i = 0; i < id.length; i++) {
                                   $('#CUENTA').append('<option value="' + id[i]['pk_num_cuenta'] + '" selected>' + id[i]['ind_num_cuenta'] + '</option>');
                               }
                           }
                       }, 'json');
           });

           $('#buscar').click(function () {
               var idCuenta = $('#CUENTA').val();
               var periodo = $('#periodo_hasta').val();
               var url = '{$_Parametros.url}modCP/procesos/procesosCONTROL/imprimirSaldoLibroMET/?periodo='+periodo+'&'+'idCuenta='+idCuenta;
               if(idCuenta!='' && periodo!='') {
                   $('#resultado').html('<iframe frameborder="0" src="' + url + '" width="100%" height="540px"></iframe>');
               }
           });
       });

</script>