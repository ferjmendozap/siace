<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once ROOT.'librerias'.DS.'Number.php';
class obligacionControlador extends Controlador
{
    private $atFPDF;
    private $atObligacionModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atObligacionModelo = $this->metCargarModelo('obligacion');
        $this->metObtenerLibreria('cabeceraCP','modCP');
        $this->atFPDF = new pdf('L','mm','Letter');
    }

    public function metIndex($estado = false)
    {
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('estado', $estado);
        $this->atVista->assign('listadoDocumento', $this->atObligacionModelo->metDocumentoListar());
        $this->atVista->assign('listadoEstado', $this->atObligacionModelo->atMiscelaneoModelo->metMostrarSelect('EDOOBLI'));
        if($estado == 'facturacionLG'){

            $this->atVista->assign('clasifDoc', $this->atObligacionModelo->atClasifDocumentosModelo->metClasificacionDocumentosListar());
            $this->atVista->metRenderizar('facturacionLG');
        }else{
            $this->atVista->assign('parametroRevisar', Session::metObtener('REVOBLIG'));
            $this->atVista->assign('parametroContabilidad', Session::metObtener('CONTABILIDAD'));
            $this->atVista->assign('obligacionBD', $this->atObligacionModelo->metObligacionListar($estado));
            $this->atVista->metRenderizar('listado');
        }
    }

    public function metListasDocumentoServicio($tipo)
    {
        $idPersona = $this->metObtenerInt('idPersona');
        if ($tipo == 'documento') {
            $id = $this->atObligacionModelo->metListarDocumentosPersona($idPersona);
        } else if($tipo == 'servicio'){
            $id = $this->atObligacionModelo->metListarServicioPersona($idPersona);
        }else{
            $id = $this->atObligacionModelo->metListarTipoPagoPersona($idPersona);
        }

        if (!$id) {

        }
        echo json_encode($id);
        exit;
    }

    public function metImpuesto($iva = false)
    {
        if ($iva) {
            $idTipoServicio = $this->metObtenerInt('idTipoServicio');
            $iva = $this->atObligacionModelo->metBuscarIva($idTipoServicio);
           /* if (!isset($iva['num_factor_porcentaje'])) {
                $iva['num_factor_porcentaje'] = 0;
            }*/
            echo json_encode($iva);
            exit;
        }
        $this->atVista->assign('sustraendo', Session::metObtener('SUSTISLR'));
        $this->atVista->assign('unidadTributaria', $this->atObligacionModelo->metBuscarUnidadTrib());
        $this->atVista->assign('lista', $this->atObligacionModelo->atImpuestoModelo->metImpuestoListar());
        $this->atVista->assign('listaImpuesto', 1);
        $this->atVista->assign('tr', $this->metObtenerInt('tr'));
        $this->atVista->metRenderizar('listadoImpuesto', 'modales');
    }

    public function metPresupuesto()
    {
        /*$this->atVista->assign('lista', $this->atObligacionModelo->atImpuestoModelo->metImpuestoListar());
        $this->atVista->assign('listaImpuesto', 1);
        $this->atVista->assign('tr', $this->metObtenerInt('tr'));*/
        $this->atVista->metRenderizar('dispPresupuestaria', 'modales');
    }

    public function metAlarma($iva = false, $estatus = false, $doc = false)
    {
        $form = $this->metValidarFormArrayDatos('form', 'int');
        $secuencia = $form['partidaCuenta']['ind_secuencia'];
        foreach ($secuencia as $i) {
            $partidas = $form['partidaCuenta'][$i];
            if ($partidas['fk_prb002_num_partida_presupuestaria'] != '') {
                $partida = $this->atObligacionModelo->metBuscarPartidaCuenta($partidas['fk_prb002_num_partida_presupuestaria']);
                $cuenta = $this->atObligacionModelo->metBuscarCuenta($partidas['fk_cbb004_num_cuenta']);
                $cuenta20 = $this->atObligacionModelo->metBuscarCuenta($partidas['fk_cbb004_num_cuenta_pub20']);

                $codPartida = str_replace(".", "", $partida['cod_partida']);
                if ($partida['num_monto_ajustado']) {
                    if (isset($partidasCuenta['partidas'][$codPartida])) {
                        $partidas['num_monto'] = $partidas['num_monto'] + $partidasCuenta['partidas'][$codPartida]['monto'];
                    } else {
                        $partidas['num_monto'] = $partidas['num_monto'];
                    }

                    if ($doc == 1) {
                        $monto = 0;
                    } else {
                        if(!isset($estatus) or $estatus == 'CO'){
                            $monto = 0;
                        }else{
                            $monto = $partidas['num_monto'];
                        }
                    }
                    $calculo = (($partida['num_monto_compromiso'] + $monto) * 100) / $partida['num_monto_ajustado'];
                    $partidasCuenta['partidas'][$codPartida] = array(
                        'partida' => $partida['cod_partida'],
                        'descripcion' => $partida['ind_denominacion'],
                        'monto' => $partidas['num_monto'],
                        'monto2' => $monto,
                        'num_monto_compromiso' => $partida['num_monto_compromiso'],
                        'num_monto_ajustado' => $partida['num_monto_ajustado'],
                        'porcentaje' => round($calculo, 2),
                    );
                }
            }
        }


        foreach ($secuencia as $i) {
            $partidas = $form['partidaCuenta'][$i];
            $cuenta = $this->atObligacionModelo->metBuscarCuenta($partidas['fk_cbb004_num_cuenta']);
            $cuenta20 = $this->atObligacionModelo->metBuscarCuenta($partidas['fk_cbb004_num_cuenta_pub20']);
                if (isset($partidasCuenta['cuentas'][$cuenta['cod_cuenta'] . $cuenta20['cod_cuenta']])) {
                    $partidas['num_monto'] = $partidas['num_monto'] + $partidasCuenta['cuentas'][$cuenta['cod_cuenta'] . $cuenta20['cod_cuenta']]['monto'];
                }

            $partidasCuenta['cuentas'][$cuenta['cod_cuenta'] . $cuenta20['cod_cuenta']] = array(
                'cuenta' => $cuenta['cod_cuenta'],
                'cuenta20' => $cuenta20['cod_cuenta'],
                'descripcion' => $cuenta['ind_descripcion'],
                'descripcion20' => $cuenta20['ind_descripcion'],
                'monto' => $partidas['num_monto']
            );
        }

        if ($iva != 0) {
            #partida del iva
            $partidaIva = $this->atObligacionModelo->metBuscar('pr_b002_partida_presupuestaria','cod_partida',Session::metObtener('IVADEFAULT'));
            $partida = $this->atObligacionModelo->metBuscarPartidaCuenta($partidaIva['pk_num_partida_presupuestaria']);
            $codPartida = str_replace(".", "", $partida['cod_partida']);
            if ($partida['num_monto_ajustado']) {
                if (isset($partidasCuenta['partidas'][$codPartida])) {
                    $iva = $iva + $partidasCuenta['partidas'][$codPartida]['monto'];
                } else {
                    $iva = $iva;
                }

                if (!$estatus) {
                    $monto = $iva;
                } else {
                    $monto = 0;
                }
                $calculo = (($partida['num_monto_compromiso'] + $monto) * 100) / $partida['num_monto_ajustado'];
                $partidasCuenta['partidas'][$codPartida] = array(
                    'partida' => $partida['cod_partida'],
                    'descripcion' => $partida['ind_denominacion'],
                    'monto' => $iva,
                    'porcentaje' => round($calculo, 2),
                );
            }
            #cuenta del iva
            $cuentaIva = $this->atObligacionModelo->metBuscar('cp_b015_impuesto','cod_impuesto',Session::metObtener('IVGCODIGO'));
                $cuenta = $this->atObligacionModelo->metBuscarCuenta($cuentaIva['fk_cbb004_num_plan_cuenta']);
                $cuenta20 = $this->atObligacionModelo->metBuscarCuenta($cuentaIva['fk_cbb004_num_plan_cuenta_pub20']);
            if ($partidas['fk_prb002_num_partida_presupuestaria'] == '') {
                if (isset($partidasCuenta['cuentas'][$cuenta['cod_cuenta'] . $cuenta20['cod_cuenta']])) {
                    $partidas['num_monto'] = $partidas['num_monto'] + $partidasCuenta['cuentas'][$cuenta['cod_cuenta'] . $cuenta20['cod_cuenta']]['monto'];
                }
            }
            $partidasCuenta['cuentas'][$cuenta['cod_cuenta'] . $cuenta20['cod_cuenta']] = array(
                'cuenta' => $cuenta['cod_cuenta'],
                'cuenta20' => $cuenta20['cod_cuenta'],
                'descripcion' => $cuenta['ind_descripcion'],
                'descripcion20' =>$cuenta20['ind_descripcion'],
                'monto' => $iva
            );
        }
        if (!isset($partidasCuenta)) {
            $partidasCuenta = 0;
        }
        echo json_encode($partidasCuenta);
        exit;
    }

    public function metAjaxDocumentoDet()
    {
        $idOrden = $this->metObtenerInt('idOrden');
        if($idOrden){
           echo json_encode($this->atObligacionModelo->metMostrarDocumentosDet($idOrden));
        }
        exit;
    }

    public function metCentroCosto($parametro = false, $idCampo = false)
    {
        $this->atVista->assign('lista', $this->atObligacionModelo->metListaCC());
        $this->atVista->assign('parametro', $parametro);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoCC', 'modales');
    }

    public function metDocumentos()
    {
        $complementosJs[] = 'wizard/jquery.bootstrap.wizard.min';
        $complementosCss[] = 'wizard/wizardfa6c';
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listaDocumentos');
        $this->atVista->assign('tr', $this->metObtenerInt('tr'));
        $this->atVista->assign('documentosBD', $this->atObligacionModelo->metMostrarDocumentos($this->metObtenerInt('proveedor')));
        $this->atVista->metRenderizar('listadoDocumento', 'modales');
    }

    public function metPartidasCuentas($titulo, $idCampo = false)
    {
        $this->atVista->assign('tr', $this->metObtenerInt('tr'));
        if ($titulo == 'Partidas' or $titulo == 'PartidasTrans' or $titulo == 'PartidasConcepto') {
            $this->atVista->assign('lista', $this->atObligacionModelo->metListaPartidas());
        } else {
            $this->atVista->assign('listaOnco', $this->atObligacionModelo->metListaCuentas("cuentaOnco"));
            $this->atVista->assign('listaPub20', $this->atObligacionModelo->metListaCuentas("cuentaPub20"));
        }
        $this->atVista->assign('titulo', $titulo);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoPartidasCuentas', 'modales');
    }

    public function metNuevaObligacion()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $tipo = $this->metObtenerAlphaNumerico('tipo');
        $idObligacion = $this->metObtenerInt('idObligacion');
        $idOrden = $this->metObtenerInt('idOrden');
        $estado = $this->metObtenerAlphaNumerico('estado');
        $motivoAnulacion = $this->metObtenerAlphaNumerico('motivoAnulacion');

        if ($valido == 1) {
            if($estado=='RE' || $estado=='CO' || $estado=='AP' || $estado=='RECHAZO' || $estado=='AN' || $estado == 'IM'){
                if($estado=='RECHAZO'){
                    $id=$this->atObligacionModelo->metRechazarObligacion($idObligacion,$estado);
                    $validacion['Mensaje'] = array('titulo'=>'Rechazado','contenido'=>'La Obligacion fue rechazada Satisfactoriamente');
                }elseif($estado=='AN'){
                    if($motivoAnulacion==''){
                        $validacion['status']='error';
                        $validacion['ind_motivo_anulacion']='error';
                        echo json_encode($validacion);
                        exit;
                    }
                    $id=$this->atObligacionModelo->metRechazarObligacion($idObligacion,$estado,$motivoAnulacion,$idOrden);
                    $validacion['Mensaje'] = array('titulo'=>'Anulado','contenido'=>'La Obligacion fue anulada Satisfactoriamente');
                }elseif($estado=='RE'){
                    $id=$this->atObligacionModelo->metRevisarObligacion($idObligacion);
                    $validacion['Mensaje'] = array('titulo'=>'Revisada','contenido'=>'La Obligacion fue revisada Satisfactoriamente');
                    $validacion['fk_prc002_num_presupuesto_det'] = $id;
                }elseif($estado=='CO'){
                    $id=$this->atObligacionModelo->metConformarObligacion($idObligacion, Session::metObtener('CONTABILIDAD'));
                    $generaVoucherProvision = $this->atObligacionModelo->metConsultaObligacion($idObligacion);
                    $validacion['num_flag_presupuesto'] = $generaVoucherProvision['num_flag_provision'];
                    $validacion['Mensaje'] = array('titulo'=>'Conformada','contenido'=>'La Obligacion fue conformada Satisfactoriamente');
                    $validacion['contabilidad'] = Session::metObtener('CONTABILIDAD');
                }elseif($estado=='AP'){
                     $resultado=$this->atObligacionModelo->metAprobarObligacion($idObligacion, Session::metObtener('CONTABILIDAD'));
                     $obligacion = $this->atObligacionModelo->metConsultaObligacion($idObligacion);
                    if (is_array($resultado)) {
                        $validacion['status'] = 'errorSQL';
                        echo json_encode($resultado);
                        exit;
                    }
                    $id=str_getcsv($resultado,'-');
                    $validacion['num_flag_presupuesto'] = $obligacion['num_flag_presupuesto'];
                    $validacion['num_flag_provision'] = $obligacion['num_flag_provision'];
                    $validacion['Mensaje'] = array('titulo'=>'APROBADA','contenido'=>'La Obligacion fue aprobada Satisfactoriamente se ha generado el pago N°'.$id[1]);
                    $validacion['contabilidad'] = Session::metObtener('CONTABILIDAD');
                    $id=$id[0];
                }

                if (is_array($id)) {
                    foreach ($validacion as $titulo => $valor) {
                        if(is_array($validacion[$titulo])) {
                            if (strpos($id[2], $titulo)) {
                                $validacion[$titulo] = 'error';
                            }
                        }
                    }
                    $validacion['status'] = 'errorSQL';
                    echo json_encode($validacion);
                    exit;
                }
                $validacion['idObligacion'] = $id;
                $validacion['status'] = 'OK';
                echo json_encode($validacion);
                exit;
            }
            $excepcion = array(
                'num_flag_caja_chica', 'num_flag_pago_individual', 'num_flag_obligacion_auto',
                'num_flag_diferido', 'num_flag_pago_diferido', 'num_flag_afecto_IGV',
                'num_flag_compromiso', 'num_flag_presupuesto', 'num_flag_obligacion_directa',
                'num_monto_descuento', 'fk_cpb015_num_impuesto', 'cant',
                'ind_secuencia', 'num_monto_afecto', 'num_monto_impuesto',
                'num_monto_no_afecto', 'num_monto_impuesto_otros', 'num_monto_obligacion',
                'nomtoTotalPagar','ind_comentarios_adicional', 'ind_motivo_anulacion', 'ind_tipo_descuento',
                'num_monto_adelanto','num_monto_pago_parcial'
            );
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum', $excepcion);
            $txt = $this->metValidarFormArrayDatos('form', 'txt', $excepcion);
            $ind = $this->metValidarFormArrayDatos('form', 'int', $excepcion);
            $formula = $this->metValidarFormArrayDatos('form', 'formula', $excepcion);

            if ($alphaNum != null && $ind == null && $txt == null && $formula == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null && $formula == null) {
                $validacion = $ind;
            }elseif ($alphaNum == null && $ind == null && $txt != null && $formula == null) {
                $validacion = $txt;
            }elseif ($alphaNum == null && $ind == null && $txt == null && $formula != null) {
                $validacion = $formula;
            } elseif ($alphaNum != null && $ind != null && $txt == null && $formula == null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null && $formula == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif ($alphaNum != null && $ind == null && $txt == null && $formula != null) {
                $validacion = array_merge($alphaNum, $formula);
            } elseif ($alphaNum != null && $ind != null && $txt != null && $formula == null) {
                $validacion = array_merge($alphaNum, $ind, $txt);
            } elseif ($alphaNum != null && $ind != null && $txt == null && $formula != null) {
                $validacion = array_merge($alphaNum, $ind, $formula);
            } else {
                $validacion = array_merge($ind, $txt, $alphaNum,$formula);
            }
            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }
            if (!isset($validacion['num_flag_caja_chica'])) {
                $validacion['num_flag_caja_chica'] = 0;
            }
            if (!isset($validacion['num_flag_pago_individual'])) {
                $validacion['num_flag_pago_individual'] = 0;
            }
            if (!isset($validacion['num_flag_obligacion_auto'])) {
                $validacion['num_flag_obligacion_auto'] = 0;
            }
            if (!isset($validacion['num_flag_diferido'])) {
                $validacion['num_flag_diferido'] = 0;
            }
            if (!isset($validacion['num_flag_pago_diferido'])) {
                $validacion['num_flag_pago_diferido'] = 0;
            }
            if (!isset($validacion['num_flag_afecto_IGV'])) {
                $validacion['num_flag_afecto_IGV'] = 0;
            }
            if (!isset($validacion['num_flag_compromiso'])) {
                $validacion['num_flag_compromiso'] = 0;
            }
            if (!isset($validacion['num_flag_presupuesto'])) {
                $validacion['num_flag_presupuesto'] = 0;
            }
            if (!isset($validacion['num_flag_adelanto'])) {
                $validacion['num_flag_adelanto'] = 0;
            }
            if (!isset($validacion['num_flag_obligacion_directa'])) {
                $validacion['num_flag_obligacion_directa'] = 0;
            }
            $aux1 = strpos($validacion['nomtoTotalPagar'] , ',');
            $aux2 = strpos($validacion['num_monto_adelanto'] , ',');
            $aux3 = strpos($validacion['num_monto_afecto'] , ',');
            $aux4 = strpos($validacion['num_monto_descuento'] , ',');
            $aux5 = strpos($validacion['num_monto_impuesto_otros'] , ',');
            $aux6 = strpos($validacion['num_monto_impuesto'] , ',');
            $aux7 = strpos($validacion['num_monto_no_afecto'] , ',');
            $aux8 = strpos($validacion['num_monto_obligacion'] , ',');
            $aux9 = strpos($validacion['num_monto_pago_parcial'] , ',');

            if($aux1===false){ $validacion['nomtoTotalPagar']=$validacion['nomtoTotalPagar'];  }else{ $validacion['nomtoTotalPagar'] = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $validacion['nomtoTotalPagar'])); } 
            if($aux2===false){ $validacion['num_monto_adelanto']=$validacion['num_monto_adelanto'];  }else{ $validacion['num_monto_adelanto'] = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $validacion['num_monto_adelanto'])); } 
            if($aux3===false){ $validacion['num_monto_afecto']=$validacion['num_monto_afecto'];  }else{ $validacion['num_monto_afecto'] = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $validacion['num_monto_afecto'])); } 
            if($aux4===false){ $validacion['num_monto_descuento']=$validacion['num_monto_descuento'];  }else{ $validacion['num_monto_descuento'] = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $validacion['num_monto_descuento'])); } 
            if($aux5===false){ $validacion['num_monto_impuesto_otros']=$validacion['num_monto_impuesto_otros'];  }else{ $validacion['num_monto_impuesto_otros'] = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $validacion['num_monto_impuesto_otros'])); } 
            if($aux6===false){ $validacion['num_monto_impuesto']=$validacion['num_monto_impuesto'];  }else{ $validacion['num_monto_impuesto'] = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $validacion['num_monto_impuesto'])); } 
            if($aux7===false){ $validacion['num_monto_no_afecto']=$validacion['num_monto_no_afecto'];  }else{ $validacion['num_monto_no_afecto'] = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $validacion['num_monto_no_afecto'])); } 
            if($aux8===false){ $validacion['num_monto_obligacion']=$validacion['num_monto_obligacion'];  }else{ $validacion['num_monto_obligacion'] = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $validacion['num_monto_obligacion'])); } 
            if($aux9===false){ $validacion['num_monto_pago_parcial']=$validacion['num_monto_pago_parcial'];  }else{ $validacion['num_monto_pago_parcial'] = str_replace(",",".",preg_replace('/[^0-9 ,]/i', '', $validacion['num_monto_pago_parcial'])); } 
            if ($idObligacion == 0) {
                $procedencia = "CP";
                if(isset($tipo)) {
                    if ($tipo == 'cajaChica') {
                        $procedencia = 'CC';
                        $idCajaChica = $this->metObtenerInt('idCajaChica');
                    } elseif ($tipo == 'viatico') {
                        $procedencia = 'VI';
                        $idViatico = $this->metObtenerInt('idViatico');
                        $idBeneficiario = $this->metObtenerInt('idBeneficiario');
                    }elseif ($tipo == 'facturacionLG') {
                        $procedencia = 'LG';
                        $idBeneficiario = $this->metObtenerInt('idBeneficiario');
                    }
                }
                $validacion['status'] = 'nuevo';
                $id=$this->atObligacionModelo->metNuevaObligacion($validacion,$procedencia);
                $validacion['idObligacion'] = $id;
                if(isset($tipo)) {
                    if ($tipo == 'cajaChica') {
                        $actualizarCC = $this->atObligacionModelo->metActualizarCajaChica($idCajaChica, $validacion['idObligacion']);
                    }elseif($tipo == 'viatico'){
                        $actualizarVia = $this->atObligacionModelo->metActualizarViatico($idViatico, $idBeneficiario, $validacion['idObligacion']);
                    }
                }
            } else {
                $validacion['status'] = 'modificar';
                $id=$this->atObligacionModelo->metModificarObligacion($idObligacion,$validacion,'CP');
                $validacion['idObligacion'] = $id;
            }
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $titulo)) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $idNuevo = $this->atObligacionModelo->metObligacionListar(false,false,$validacion['idObligacion']);
            $idNuevo['status'] = $validacion['status'];
            echo json_encode($idNuevo);
            exit;
        }

        if ($idObligacion != 0) {

            $this->atVista->assign('obligacionBD', $this->atObligacionModelo->metConsultaObligacion($idObligacion));
            $this->atVista->assign('impuestoBD', $this->atObligacionModelo->metMostrarImpuesto($idObligacion));
            $this->atVista->assign('documentoBD', $this->atObligacionModelo->metMostrarDocumentoObligacion($idObligacion));
            $this->atVista->assign('partidaBD', $this->atObligacionModelo->metMostrarPartidas($idObligacion));
            $this->atVista->assign('trDocumentos', 1);

            if($estado!='modificar'){
                $this->atVista->assign('ver', 1);
            }
        }

        if($estado!=''){
            $this->atVista->assign('estado', $estado);
        }

        $this->atVista->assign('listadoDocumento', $this->atObligacionModelo->metDocumentoListar());
        $this->atVista->assign('listadoServicio', $this->atObligacionModelo->atServicioModelo->metServiciosListar());
        $this->atVista->assign('listadoCuentas', $this->atObligacionModelo->atCuentasModelo->metCuentaListar());
        $this->atVista->assign('listadoTipoPago', $this->atObligacionModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
        $this->atVista->assign('tipoDescuento', $this->atObligacionModelo->atMiscelaneoModelo->metMostrarSelect('TIPODESC'));
        $this->atVista->assign('parametroRevisar', Session::metObtener('REVOBLIG'));

        $this->atVista->assign('idObligacion', $idObligacion);
        $this->atVista->metRenderizar('nuevo', 'modales');


    }

    public function metImprimirVoucher()
    {
        $idObligacion = $_GET['idObligacion'];
        if(Session::metObtener('CONTABILIDAD') == 'ONCOP'){
            $contab='T';
        }else{
            $contab='F';
        }
        $contabilidades = $this->atObligacionModelo->metBuscar('cb_b005_contabilidades','ind_tipo_contabilidad',$contab);
        $datos = $this->atObligacionModelo->metBuscarVoucher($idObligacion,$contabilidades['pk_num_contabilidades']);

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 5, 5);
        $this->atFPDF->SetAutoPageBreak(5, 5);
        $this->atFPDF->AddPage();

        //	Cabecera de página
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CEM.jpg", 5, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(15, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(15, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(230, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(230, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 12);
        $this->atFPDF->SetY(20); $this->atFPDF->Cell(270, 5, utf8_decode('VOUCHER DE PROVISIÓN'), 0, 1, 'C');
        $this->atFPDF->Ln(8);
        ##	-------------------
        $this->atFPDF->SetFillColor(200,200,200);
        ##
        $this->atFPDF->SetFont('Arial','',10);
        $this->atFPDF->Cell(23, 6, utf8_decode('Organismo'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial','B',10);
        $this->atFPDF->Cell(127, 6, utf8_decode(APP_ORGANISMO), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial','',10);
        $this->atFPDF->Cell(27, 6, utf8_decode('Preparado Por'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial','B',10);
        $this->atFPDF->Cell(123, 6, $datos['personaPrepara'], 0, 1, 'L');
        $this->atFPDF->Ln(1);
        $this->atFPDF->SetFont('Arial','',10);
        $this->atFPDF->Cell(23, 6, utf8_decode('Fecha'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial','B',10);
        $this->atFPDF->Cell(127, 6, $datos['fec_fecha_preparacion'], 0, 0, 'L');
        $this->atFPDF->SetFont('Arial','',10);
        $this->atFPDF->Cell(27, 6, utf8_decode('Aprobado Por'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial','B',10);
        $this->atFPDF->Cell(123, 6, $datos['personaPrepara'], 0, 1, 'L');
        $this->atFPDF->Ln(1);
        $this->atFPDF->SetFont('Arial','',10);
        $this->atFPDF->Cell(23, 6, utf8_decode('Voucher'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial','B',10);
        $this->atFPDF->Cell(127, 6, $datos['ind_anio'].'-'.$datos['ind_mes'].' '.$datos['ind_voucher'], 0, 0, 'L');
        $this->atFPDF->SetFont('Arial','',10);
        $this->atFPDF->Cell(27, 6, utf8_decode('Libro Contable'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial','B',10);
        $this->atFPDF->Cell(123, 6, utf8_decode($datos['libroContable']), 0, 1, 'L');
        $this->atFPDF->Ln(1);
        $this->atFPDF->SetFont('Arial','',10);
        $this->atFPDF->Cell(23, 6, utf8_decode('Descripción'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial','B',10);
        $this->atFPDF->MultiCell(247, 6, $datos['txt_titulo_voucher'], 0, 'L');
        $this->atFPDF->Ln(5);
        ##	-------------------
        $this->atFPDF->SetDrawColor(0,0,0);
        $this->atFPDF->SetFillColor(255,255,255);
        $this->atFPDF->SetWidths(array(30,18,18,140,32,32));
        $this->atFPDF->SetAligns(array('L','C','C','L','R','R'));
        $this->atFPDF->SetFont('Arial','B',10);
        $this->atFPDF->Row(array('Cuenta',
            'Persona',
            'C.C',
            utf8_decode('Descripción'),
            'Debe',
            'Haber'));
        $this->atFPDF->Ln(1);
        //
        $debe = 0; $haber = 0; $totalDebe = 0; $totalHaber = 0;
        $datosDetalle = $this->atObligacionModelo->metDetalleVoucher($datos['pk_num_voucher_mast']);
        foreach ($datosDetalle as $i) {
            if($i['num_debe']=='0'){
                $debe = 0;
                $haber = $i['num_haber'];
            } else {
                $debe = $i['num_debe'];
                $haber = 0;
            }
            $totalDebe += $debe;
            $totalHaber += $haber;
            ##
            $this->atFPDF->SetDrawColor(255,255,255);
            $this->atFPDF->SetFillColor(255,255,255);
            $this->atFPDF->SetFont('Arial','',10);
            $this->atFPDF->Row(array(utf8_decode($i['cod_cuenta']),
                $i['pk_num_persona'],
                $i['fk_a023_num_centro_costo'],
                utf8_decode($i['ind_descripcion']),
                number_format($debe,2,',','.'),
                number_format($haber,2,',','.')
            ));
            $this->atFPDF->Ln(2);
        }
        $this->atFPDF->SetDrawColor(0,0,0);
        $this->atFPDF->Line(5, $this->atFPDF->GetY(), 275, $this->atFPDF->GetY());
        $this->atFPDF->Ln(2);
        $this->atFPDF->SetFillColor(255,255,255);
        $this->atFPDF->SetFont('Arial','B',10);
        $this->atFPDF->Cell(206, 6);
        $this->atFPDF->Cell(32, 6, number_format($totalDebe,2,',','.'), 0, 0, 'R', 1);
        $this->atFPDF->Cell(32, 6, number_format($totalHaber,2,',','.'), 0, 0, 'R', 1);
        //	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    //facturacion LG
    public function metFacturacionLg()
    {
        $proveedor =  $this->metObtenerInt('proveedor');
        $clasificacion =  $this->metObtenerAlphaNumerico('clasificacion');
        $consulta = $this->atObligacionModelo->metBuscarFacturacion($proveedor,$clasificacion);
        echo json_encode($consulta);
    }

    public function metPrepararFacturaLG()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $proveedor =  $this->metObtenerInt('proveedor');
        $clasificacion =  $this->metObtenerInt('clasificacion');

        $this->atVista->assign('dataBD',$this->atObligacionModelo->metBuscarFacturacion($clasificacion));
        $this->atVista->metRenderizar('listadoEstado');
    }

    public function metDocumentoFacturaLG()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idDocumento =  $this->metObtenerInt('idDocumento');
        $consulta = $this->atObligacionModelo->metBuscarDocumento($idDocumento);
        $datos['documento'] = $consulta;
        $datos['ordenDetalle'] = $this->atObligacionModelo->metMostrarDocumentosDet($consulta['fk_lgb019_num_orden'], $consulta['num_monto_afecto'],$consulta['num_tipo_documento']);

        echo json_encode($datos);
    }

    public function metFacturaLG()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idDocumento = $this->metObtenerInt('idDocumento');
        $idProveedor = $this->metObtenerInt('idProveedor');

        $datoProveedor = $this->atObligacionModelo->metBuscarProveedorDocumento($idProveedor);

        $this->atVista->assign('listadoDocumento', $this->atObligacionModelo->metDocumentoListar());
        $this->atVista->assign('listadoServicio', $this->atObligacionModelo->atServicioModelo->metServiciosListar());
        $this->atVista->assign('listadoCuentas', $this->atObligacionModelo->atCuentasModelo->metCuentaListar());
        $this->atVista->assign('listadoTipoPago', $this->atObligacionModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
        $this->atVista->assign('tipoDescuento', $this->atObligacionModelo->atMiscelaneoModelo->metMostrarSelect('TIPODESC'));
        $this->atVista->assign('parametroRevisar', Session::metObtener('REVOBLIG'));
        //Enviar a la vista el Centro de Costo por defecto para los vouchers
        $centroCosto = $this->atObligacionModelo->metBuscar('a023_centro_costo','ind_abreviatura',Session::metObtener('CCOSTOVOUCHER'));

        $obligacionBD=array(
            'fk_a003_num_persona_proveedor' => $datoProveedor['pk_num_persona'],
            'proveedor' => $datoProveedor['proveedor'],
            'fk_a003_num_persona_proveedor_a_pagar' => $datoProveedor['pk_num_persona'],
            'proveedorPagar' => $datoProveedor['proveedor'],
            'documentoFiscal' => $datoProveedor['ind_documento_fiscal'],
            'fk_a023_num_centro_de_costo' => $centroCosto['pk_num_centro_costo'],
            'ind_tipo_procedencia'=> 'FT'
        );
        $this->atVista->assign('obligacionBD',$obligacionBD);
        $this->atVista->metRenderizar('nuevo', 'modales');


    }

    //paginado
    public function metPersona($persona, $idCampo = false)
    {
        $this->atVista->assign('listaPersona', $persona);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->assign('lista', $this->atObligacionModelo->metListaPersona());
        $this->atVista->metRenderizar('listadoPersona', 'modales');

    }

    public function metJsonDataTabla($estado = false,
                                     $proveedor = false,
                                     $codigoIngresadoPor = false,
                                     $doc = false,
                                     $nroDoc = false,
                                     $centroCosto = false,
                                     $desde = false,
                                     $hasta = false,
                                     $desdeR = false,
                                     $hastaR = false
    )
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol = Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                cp_d001_obligacion.*,
                cp_d001_obligacion.ind_estado AS estado,
                cp_b002_tipo_documento.cod_tipo_documento,
                (CASE
                        WHEN cp_d001_obligacion.ind_estado='PR' THEN 'PREPARADO'
                        WHEN cp_d001_obligacion.ind_estado='RE' THEN 'REVISADO'
                        WHEN cp_d001_obligacion.ind_estado='CO' THEN 'CONFORMADO'
                        WHEN cp_d001_obligacion.ind_estado='AP' THEN 'APROBADO'
                        WHEN cp_d001_obligacion.ind_estado='PA' THEN 'PAGADO'
                        WHEN cp_d001_obligacion.ind_estado='AN' THEN 'ANULADO'
                END) AS ind_estado,
                 (CASE
                        WHEN cp_d001_obligacion.ind_tipo_procedencia='CP' THEN 'CP'
                        WHEN cp_d001_obligacion.ind_tipo_procedencia='NM' THEN 'NOMINA'
                        WHEN cp_d001_obligacion.ind_tipo_procedencia='VI' THEN 'VIATICOS'
                        WHEN cp_d001_obligacion.ind_tipo_procedencia='CC' THEN 'CAJA CHICA'
                        WHEN cp_d001_obligacion.ind_tipo_procedencia='GC' THEN 'G. CONTRATO'
                END) AS ind_tipo_procedencia,
                CONCAT(
                    REPLACE(FORMAT(FLOOR(ROUND(cp_d001_obligacion.num_monto_obligacion, 2)), 0), ',', '.'),
                    ',',
                    SUBSTRING_INDEX(FORMAT(ROUND(cp_d001_obligacion.num_monto_obligacion,2), 2), '.', -1)
                ) AS num_monto_obligacion,
                CONCAT_WS(' ',a003_persona.ind_nombre1, a003_persona.ind_apellido1) AS fk_a003_num_persona_proveedor
            FROM
              cp_d001_obligacion
              INNER JOIN cp_b002_tipo_documento ON cp_d001_obligacion.fk_cpb002_num_tipo_documento = cp_b002_tipo_documento.pk_num_tipo_documento
              INNER JOIN a003_persona ON cp_d001_obligacion.fk_a003_num_persona_proveedor = a003_persona.pk_num_persona
            WHERE
              ind_estado<>'NM'
            ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (
                        pk_num_obligacion LIKE '%$busqueda[value]%' OR
                        fk_a003_num_persona_proveedor LIKE '%$busqueda[value]%' OR
                        cp_b002_tipo_documento.cod_tipo_documento LIKE '%$busqueda[value]%' OR
                        ind_nro_factura LIKE '%$busqueda[value]%' OR
                        fec_documento LIKE '%$busqueda[value]%' OR
                        num_monto_obligacion LIKE '%$busqueda[value]%' OR
                        ind_tipo_procedencia LIKE '%$busqueda[value]%' OR
                        ind_estado LIKE '%$busqueda[value]%'
                        )
                        ";
        }
        if ($estado!='' AND $estado!='false') {
            if ($estado == 'RE' and Session::metObtener('REVOBLIG') == 'N') {
                $sql .= " AND ind_estado='PR' ";
            } else {
                $sql .= " AND ind_estado='$estado' ";
            }

        }
        if($proveedor!='' AND $proveedor!='false'){
            $sql .= " AND fk_a003_num_persona_proveedor=$proveedor ";
        }
        if($codigoIngresadoPor!='' AND $codigoIngresadoPor!='false'){
            $sql .= " AND fk_rhb001_num_empleado_ingresa=$codigoIngresadoPor ";
        }
        if($doc!='' AND $doc!='false'){
            $sql .= " AND fk_cpb002_num_tipo_documento=$doc ";
        }
        if($nroDoc!='' AND $nroDoc!='false'){
            $sql .= " AND ind_nro_factura LIKE '%$nroDoc%' ";
        }
        if($centroCosto!='' AND $centroCosto!='false'){
            $sql .= " AND fk_a023_num_centro_de_costo=$centroCosto ";
        }
        if($desde!='' AND $desde!='false' AND $hasta!='' AND $hasta!='false'){
            $sql .= " AND (fec_documento>='$desde' AND fec_documento<='$hasta') ";
        }
        if($desdeR!='' AND $desdeR!='false' AND $hastaR!='' AND $hastaR!='false'){
            $sql .= " AND (fec_registro>='$desdeR' AND fec_registro<='$hastaR') ";
        }
#        var_dump($sql);
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_obligacion','fk_a003_num_persona_proveedor', 'cod_tipo_documento', 'ind_nro_factura', 'fec_documento', 'num_monto_obligacion', 'ind_tipo_procedencia', 'ind_estado');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_obligacion';
        #construyo el listado de botones

        if (Session::metObtener('REVOBLIG') == 'S') {
            if (in_array('CP-01-01-02-R', $rol) AND $estado == 'PR') {
                $campos['boton']['Revisar'] = '
                <button class="accion Obligacion btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" id="PR" data-backdrop="static" idObligacion="' . $clavePrimaria . '" title="Revisar"
                        descipcion="El Usuario ha Revisado una Obligacion" titulo="<i class=\'icm icm-rating\'></i> Revisar Obligacion">
                    <i class="icm icm-rating" style="color: #ffffff;"></i>
                </button>
            ';
            } else {
                $campos['boton']['Revisar'] = false;
            }
        }

        if (in_array('CP-01-01-03-C',$rol) AND $estado == 'RE') {
            $campos['boton']['Conformar'] = '
                <button class="accion Obligacion btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" id="RE" data-backdrop="static" idObligacion="'.$clavePrimaria.'" title="Conformar"
                        descipcion="El Usuario ha Conformar una Obligacion" titulo="<i class=\'icm icm-rating2\'></i> Conformar Obligacion">
                    <i class="icm icm-rating2" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Conformar'] = false;
        }

        if (in_array('CP-01-01-04-AP',$rol) AND $estado == 'CO') {
            $campos['boton']['Aprobar'] = '
                <button class="accion Obligacion btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" id="CO" data-backdrop="static" idObligacion="'.$clavePrimaria.'" title="Aprobar"
                        descipcion="El Usuario ha Aprobado una Obligacion" titulo="<i class=\'icm icm-rating3\'></i> Aprobar Obligacion">
                    <i class="icm icm-rating3" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('CP-01-01-06-M',$rol) AND $estado != 'PR' ) {
            $campos['boton']['Editar'] = array("
                <button class='modificar Obligacion btn ink-reaction btn-raised btn-xs btn-primary' data-toggle='modal' data-target='#formModal'
                        data-keyboard='false' data-backdrop='static' id='modificar' idObligacion=$clavePrimaria title='Editar'
                        descipcion='El Usuario ha Modificado una Obligacion' titulo='Editar Obligacion'>
                    <i class='fa fa-edit' style='color: #ffffff;'></i>
                </button>
                ",
                'if( $i["ind_estado"] == "PREPARADO" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );

        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('CP-01-01-07-V',$rol)) {
            $campos['boton']['Ver'] = '
               <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" id="ver" idObligacion="'.$clavePrimaria.'" title="Consultar"
                        descipcion="El Usuario esta viendo una Obligacion" titulo="<i class=\'md md-remove-red-eye\'></i> Consultar Obligacion">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }

    //vouchers
    public function metVouchers()
    {
        $idObligacion = $this->metObtenerInt('idObligacion');
        $voucherContab = $this->metObtenerAlphaNumerico('voucher');
        //Enviar a la vista el Centro de Costo por defecto para los vouchers
        $this->atVista->assign('centroCosto', $this->atObligacionModelo->metBuscar('a023_centro_costo','ind_abreviatura',Session::metObtener('CCOSTOVOUCHER')));
        //Enviar a la vista el organismo
        $this->atVista->assign('organismo', $this->atObligacionModelo->metOrganismo());
        $this->atVista->assign('libroContable', $this->atObligacionModelo->atLibroContableModelo->metListar());
        $this->atVista->assign('voucher', $this->atObligacionModelo->atVoucherModelo->metListar());

        $datosOblig = $this->atObligacionModelo->metConsultaObligacion($idObligacion);
        //para obligaciones desde nomina
        $tipoProceso = $this->atObligacionModelo->metBuscar('nm_b003_tipo_proceso','cod_proceso',substr($datosOblig['ind_nro_factura'], 2, 2),true);
        $tipoNomina = $this->atObligacionModelo->metBuscar('nm_b001_tipo_nomina','cod_tipo_nomina',substr($datosOblig['ind_nro_factura'], 0, 2));
        if($voucherContab == 'ONCOP'){
            $contab = 'T';
            $codVoucher = $datosOblig['fk_cbc003_num_tipo_voucher'];
            //vouchers de provision
            $datos = $this->atObligacionModelo->metArmarOrdenPagoContabilidadFinanciera($idObligacion,$tipoProceso['pk_num_tipo_proceso'],$tipoNomina['pk_num_tipo_nomina']);
            $debitos=0; $creditos=0;
            if($tipoProceso['pk_num_tipo_proceso']){
                $tipoProvision = '';
            }else{
                $tipoProvision = 'P';
            }
            foreach ($datos as $i) {

                    if($i['orden'] == '01'){
                        $retencion = $this->atObligacionModelo->metRetenciones($idObligacion,$tipoProvision);
                        $montoVoucher = ($i['MontoVoucher'] + $retencion['montoRetencion']);
                    }else{
                        $montoVoucher = $i['MontoVoucher'];
                    }

                    if($i['columna']=='Debe'){
                        $debitos = $debitos + $montoVoucher;

                        $datoArreglo['Debe'][]=array(
                            'pk_num_cuenta' => $i['pk_num_cuenta'],
                            'cod_cuenta' => $i['cod_cuenta'],
                            'ind_descripcion' => $i['ind_descripcion'],
                            'MontoVoucher' => $montoVoucher
                        );
                    }else{
                        $creditos = $creditos + $montoVoucher;

                        $datoArreglo['Haber'][]=array(
                            'pk_num_cuenta' => $i['pk_num_cuenta'],
                            'cod_cuenta' => $i['cod_cuenta'],
                            'ind_descripcion' => $i['ind_descripcion'],
                            'MontoVoucher' => $montoVoucher
                        );
                    }


            }
            if(!isset($datoArreglo)){
                $datoArreglo=false;
            }
            $fechaVocuher = $datosOblig['fec_aprobado'];

        }else{
            $contab = 'F';
            if($voucherContab == 'provisionPUB20'){
                $codVoucher = $datosOblig['fk_cbc003_num_tipo_voucher'];
                //vouchers de provision
                $datos = $this->atObligacionModelo->metArmarOrdenPagoPub20($idObligacion,$tipoProceso['pk_num_tipo_proceso'],$tipoNomina['pk_num_tipo_nomina']);
                $debitos=0; $creditos=0;
                foreach ($datos as $i) {
                    if($i['orden'] == '05'){
                        $retencion = $this->atObligacionModelo->metRetenciones($idObligacion,'P');
                        $montoVoucher = ($i['MontoVoucher'] + $retencion['montoRetencion']);
                    }else{
                        $montoVoucher = $i['MontoVoucher'];
                    }
                    if($i['columna']=='Debe'){
                        $debitos = $debitos + $i['MontoVoucher'];

                        $datoArreglo['Debe'][]=array(
                            'pk_num_cuenta' => $i['pk_num_cuenta'],
                            'cod_cuenta' => $i['cod_cuenta'],
                            'ind_descripcion' => $i['ind_descripcion'],
                            'MontoVoucher' => $montoVoucher
                        );
                    }else{
                        $creditos = $creditos + $i['MontoVoucher'];

                        $datoArreglo['Haber'][]=array(
                            'pk_num_cuenta' => $i['pk_num_cuenta'],
                            'cod_cuenta' => $i['cod_cuenta'],
                            'ind_descripcion' => $i['ind_descripcion'],
                            'MontoVoucher' => $montoVoucher
                        );
                    }
                }
                $fechaVocuher = $datosOblig['fec_conformado'];
                if(!isset($datoArreglo)){
                    $datoArreglo=false;
                }
            }else{
                $codVoucher = $datosOblig['fk_cbc003_num_tipo_voucher_ordPago'];
                //vouchers de ordenanza de pago
                if(isset($tipoProceso['pk_num_tipo_proceso'])){
                    $impuesto1 = $this->atObligacionModelo->metRetencionesNomina($idObligacion,$tipoProceso['pk_num_tipo_proceso'],$tipoNomina['pk_num_tipo_nomina']);
                    $impuesto2 = NULL;
                }else{
                    $impuesto1 = $this->atObligacionModelo->metRetenciones($idObligacion,'N');
                    $impuesto2 = $this->atObligacionModelo->metRetenciones($idObligacion,'P');
                }
                $datos = $this->atObligacionModelo->metOrdenacionPago($idObligacion,$impuesto1['montoRetencion'],$impuesto2['montoRetencion'],$tipoProceso['pk_num_tipo_proceso'],$tipoNomina['pk_num_tipo_nomina']);
                $debitos=0; $creditos=0;
                foreach ($datos as $i) {
                    if($i['columna']=='Debe'){
                        $debitos = $debitos + $i['MontoVoucher'];

                        $datoArreglo['Debe'][]=array(
                            'pk_num_cuenta' => $i['pk_num_cuenta'],
                            'cod_cuenta' => $i['cod_cuenta'],
                            'ind_descripcion' => $i['ind_descripcion'],
                            'MontoVoucher' => $i['MontoVoucher']
                        );
                    }else{
                        $creditos = $creditos + $i['MontoVoucher'];

                        $datoArreglo['Haber'][]=array(
                            'pk_num_cuenta' => $i['pk_num_cuenta'],
                            'cod_cuenta' => $i['cod_cuenta'],
                            'ind_descripcion' => $i['ind_descripcion'],
                            'MontoVoucher' => $i['MontoVoucher']
                        );
                    }
                    $fechaVocuher = $datosOblig['fec_aprobado'];
                    if(!isset($datoArreglo)){
                        $datoArreglo=false;
                    }
                }
            }

        }
        $contabilidades = $this->atObligacionModelo->metBuscar('cb_b005_contabilidades','ind_tipo_contabilidad',$contab);
        $this->atVista->assign('datoArreglo',$datoArreglo);
        $this->atVista->assign('datosOblig', $datosOblig);
        $this->atVista->assign('creditos', round($creditos));
        $this->atVista->assign('debitos', round($debitos));
        $this->atVista->assign('datos', $datos);
        $this->atVista->assign('fechaVocuher', $fechaVocuher);
        $this->atVista->assign('codVoucher', $codVoucher);
        $this->atVista->assign('voucherContab', $voucherContab);
        $this->atVista->assign('contabilidad', $contabilidades['pk_num_contabilidades']);
        $this->atVista->assign('idObligacion', $idObligacion);

        $this->atVista->metRenderizar('vouchers', 'modales');

    }

    public function metRegistrarVouchers()
    {
        $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
        $txt = $this->metValidarFormArrayDatos('form', 'txt');
        $ind = $this->metValidarFormArrayDatos('form', 'int');
        $idOrdenPago = $this->metObtenerInt('idOrdenPago');
        $idObligacion = $this->metObtenerInt('idObligacion');
        $voucherContab = $this->metObtenerAlphaNumerico('voucherContab');
        $txt_titulo_voucher = $this->metObtenerFormulas('txt_titulo_voucher');

        if ($alphaNum != null && $ind == null && $txt == null) {
            $validacion = $alphaNum;
        } elseif ($alphaNum == null && $ind != null && $txt == null) {
            $validacion = $ind;
        } elseif ($alphaNum != null && $ind != null && $txt == null) {
            $validacion = array_merge($alphaNum, $ind);
        } elseif ($alphaNum != null && $txt != null && $ind == null) {
            $validacion = array_merge($alphaNum, $txt);
        } elseif ($ind != null && $txt != null && $alphaNum == null) {
            $validacion = array_merge($ind, $txt);
        } else {
            $validacion = array_merge($ind, $txt, $alphaNum);
        }
        if($txt_titulo_voucher != null){
            $formula['txt_titulo_voucher'] = $txt_titulo_voucher;
            $validacion = array_merge($validacion, $formula);
        }
        if (in_array('error', $validacion)) {
            $validacion['status'] = 'error';
            echo json_encode($validacion);
            exit;
        }

        $this->idUsuario = Session::metObtener('idUsuario');
        $validacion['fk_a003_num_preparado_por'] = $this->idUsuario;
        $organismo = $this->atObligacionModelo->metObtenerOrganismo($this->idUsuario);
        $validacion['fk_a001_num_organismo'] = $organismo['fk_a001_num_organismo'];
        $validacion['fk_a004_num_dependencia'] = $organismo['fk_a004_num_dependencia'];
        // datos default
        $validacion['fk_cbc002_num_sistema_fuente'] = "6";
        $validacion['num_flag_transferencia'] = "0";

        $resultado = $this->atObligacionModelo->metNuevo($validacion);
        $codVoucher = $this->atObligacionModelo->metBuscar('cb_b001_voucher', 'pk_num_voucher_mast', $resultado);
        $validacion['codVoucher'] = $codVoucher['ind_voucher'];
        $validacion['idVoucher'] = $resultado;

        $ordenPago = $this->atObligacionModelo->metBuscar('cp_d009_orden_pago', 'fk_cpd001_num_obligacion', $idObligacion);

        $voucherPago = $this->atObligacionModelo->metVouchersPago($ordenPago['pk_num_orden_pago'], $validacion['idVoucher'], $idObligacion);

        if ($voucherContab == 'provisionPUB20'){

            if (isset($tipo) and $tipo == 'anulacion') {

            } else {
                $voucherObligacion = $this->atObligacionModelo->metVouchersObligacion($idObligacion, $validacion['idVoucher']);
            }
        }

        $validacion['idOrdenPago'] = $idOrdenPago;
        if($validacion['idVoucher'] == 'no-periodo'){
            $validacion['status'] = 'no-periodo';
            echo json_encode($validacion);
            exit;
        }
        $validacion['status'] = 'ok2';
        echo json_encode($validacion);
        exit;

    }

    public function metVoucherObligacion(){

        $idObligacion = $this->metObtenerInt('idObligacion');

        $idVoucherObligacion = $this->metObtenerInt('idVoucherObligacion');
        $this->atVista->assign('centroCosto', $this->atObligacionModelo->metBuscar('a023_centro_costo','ind_abreviatura',Session::metObtener('CCOSTOVOUCHER')));
        $this->atVista->assign('libroContable', $this->atObligacionModelo->atLibroContableModelo->metListar());
        $this->atVista->assign('organismo', $this->atObligacionModelo->metOrganismo());
        $datosVoucher = $this->atObligacionModelo->atListaVoucherModelo->metMostrar($idVoucherObligacion);
        $contabilidades = $this->atObligacionModelo->metBuscar('cb_b005_contabilidades','ind_tipo_contabilidad','T');
        $datos = $this->atObligacionModelo->metDetalleVoucher($idVoucherObligacion);
        $this->atVista->assign('codVoucher',$this->atObligacionModelo->metBuscar('cb_c003_tipo_voucher','pk_num_voucher',$datosVoucher['fk_cbc003_num_voucher']));

        $debitos=0; $creditos=0;
        foreach ($datos as $i) {
            if($i['num_debe']=='0'){
                $creditos = $creditos+$i['num_haber'];
                $datoArreglo['Debe'][]=array(
                    'pk_num_cuenta' => $i['fk_cbb004_num_cuenta'],
                    'cod_cuenta' => $i['cod_cuenta'],
                    'ind_descripcion' => $i['ind_descripcion'],
                    'MontoVoucher' => $i['num_haber'],
                    'proveedor' => $i['proveedor'],
                    'pk_num_persona' => $i['pk_num_persona'],
                    'fk_a023_num_centro_costo' => $i['fk_a023_num_centro_costo']
                );
            }else{
                $debitos = $debitos+$i['num_debe'];
                $datoArreglo['Haber'][]=array(
                    'pk_num_cuenta' => $i['fk_cbb004_num_cuenta'],
                    'cod_cuenta' => $i['cod_cuenta'],
                    'ind_descripcion' => $i['ind_descripcion'],
                    'MontoVoucher' => $i['num_debe'],
                    'proveedor' => $i['proveedor'],
                    'pk_num_persona' => $i['pk_num_persona'],
                    'fk_a023_num_centro_costo' => $i['fk_a023_num_centro_costo']
                );
            }
        }

        $this->atVista->assign('idObligacion',$idObligacion);
        $this->atVista->assign('contabilidad', $contabilidades['pk_num_contabilidades']);
        $this->atVista->assign('datoArreglo',$datoArreglo);
        $this->atVista->assign('datosVoucher', $datosVoucher);

        $this->atVista->metRenderizar('vouchersObligacion', 'modales');
    }

    public function metFlagContabilizacion(){
        $idObligacion = $this->metObtenerInt('idObligacion');
        $voucherPago = $this->atObligacionModelo->metVouchersPago(false, false, $idObligacion);
        echo json_encode($voucherPago);
        exit;
    }

}
