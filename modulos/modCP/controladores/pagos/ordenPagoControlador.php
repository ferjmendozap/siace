<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_Modulo.'modCP'.DS.'controladores'.DS.'trait'.DS.'funcionesTrait.php';
class ordenPagoControlador extends Controlador
{
    use funcionesTrait;
    private $atOrdenPagoModelo;
    private $atFPDF;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atOrdenPagoModelo = $this->metCargarModelo('ordenPago');

        $this->metObtenerLibreria('cabecera', 'modCP');

    }

    public function metIndex($lista = false)
    {
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        $complementosJs = array(
            'materialSiace/core/demo/DemoTableDynamic',
        );

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($complementosJs);

        if ($lista == 'prepago') {
            $this->atVista->assign('ordenPagoBD', $this->atOrdenPagoModelo->metListarOrdenPago($lista));
        } else {
            $this->atVista->assign('ordenPagoBD', $this->atOrdenPagoModelo->metListarOrdenPago());
        }
        $this->atVista->assign('lista', $lista);
        $this->atVista->metRenderizar('listado');

    }

    public function metHojaRetencion($y, $h, $retencion, $ln, $imprimir)
    {
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(195, $h, 'RETENCIONES', 1, 0, 'C');
        $y += $h;
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(15, $h, 'Cod.', 1, 0, 'C');
        $this->atFPDF->Cell(105, $h, utf8_decode('Denominación'), 1, 0, 'L');
        $this->atFPDF->Cell(30, $h, 'Partida', 1, 0, 'C');
        $this->atFPDF->Cell(15, $h, 'Tasa(%)', 1, 0, 'R');
        $this->atFPDF->Cell(30, $h, 'Monto', 1, 0, 'R');
        $y += $h;
        $this->atFPDF->Rect(10, $y, 195, $h * ($ln+1), "D");
        if ($imprimir) {
            foreach ($retencion AS $i) {
                $this->atFPDF->SetXY(10, $y);
                $this->atFPDF->SetFont('Arial', '', 9);
                $this->atFPDF->Cell(15, $h, $i['CODIGO'], 0, 0, 'C');
                $this->atFPDF->Cell(105, $h, strtoupper(substr(utf8_decode($i['DESCRIPCION']), 0, 60)), 0, 0, 'L');
                $this->atFPDF->Cell(30, $h, '', 0, 0, 'C');
                $this->atFPDF->Cell(15, $h, $i['PORCENTAJE'], 0, 0, 'R');
                $this->atFPDF->Cell(30, $h, number_format($i['num_monto_impuesto'],2,',','.'), 0, 0, 'R');
                $this->atFPDF->ln();
                $y += $h;
            }
        } else {
            $y = $this->atFPDF->GetY();
#            $y += ($h * 2);
            $this->atFPDF->SetXY(10, $y);
            $this->atFPDF->SetFont('Arial', 'B', 9);
            $this->atFPDF->Cell(195, $h, $y.'VER RELACION ANEXA', 0, 0, 'C');
        }
    }

    public function metHojaPresupuesto($y, $h, $presupuesto, $ln, $imprimir, $iva,$partidaIva)
    {
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(195, $h, 'CONTABILIDAD PRESUPUESTARIA', 1, 0, 'C');
        $y += $h;
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(20, $h, 'Partida', 1, 0, 'C');
        $this->atFPDF->Cell(145, $h, utf8_decode('Denominación'), 1, 0, 'L');
        $this->atFPDF->Cell(30, $h, 'Monto', 1, 0, 'R');
        $y += $h;
        $this->atFPDF->Rect(10, $y, 195, $h * $ln, "D");
        if ($imprimir) {
            $total_monto=0;
            foreach ($presupuesto AS $i) {
                $total_monto += $i['num_monto'];
                $this->atFPDF->SetXY(10, $y);
                $this->atFPDF->SetFont('Arial', '', 9);
                $this->atFPDF->Cell(20, $h, $i['cod_partida'], 0, 0, 'C');
                $this->atFPDF->Cell(145, $h, strtoupper(substr(utf8_decode($i['ind_denominacion']), 0, 60)), 0, 0, 'L');
                $this->atFPDF->Cell(30, $h, number_format($i['num_monto'], 2, ',', '.'), 0, 0, 'R');
                $y += $h;
            }
            if($iva['num_monto_impuesto'] != 0){
                $total_monto += $iva['num_monto_impuesto'];
                $this->atFPDF->SetXY(10, $y);
                $this->atFPDF->SetFont('Arial', '', 9);
                $this->atFPDF->Cell(20, $h, $partidaIva['cod_partida'], 0, 0, 'C');
                $this->atFPDF->Cell(145, $h, strtoupper(substr($partidaIva['ind_denominacion'], 0, 60)), 0, 0, 'L');
                $this->atFPDF->Cell(30, $h, number_format($iva['num_monto_impuesto'], 2, ',', '.'), 0, 0, 'R');
                $y += $h;
            }
            $this->atFPDF->SetXY(10, $y + 1);
            $this->atFPDF->SetFont('Arial', 'B', 9);
            $this->atFPDF->Cell(30, $h, '', 0, 0, 'C');
            $this->atFPDF->Cell(135, $h, '', 0, 0, 'L');
            $this->atFPDF->Cell(30, $h, number_format($total_monto, 2, ',', '.'), 0, 0, 'R');
            $this->atFPDF->SetXY(10, $y + 1);
            $this->atFPDF->Cell(90, $h + 10, utf8_decode('JOSÉ GUERRA  ________________'), 0, 0, 'L');
            $this->atFPDF->Cell(10, $h + 10, '__/__/____', 0, 0, 'L');
            $y += $h;
        } else {
            $y += ($h * 3);
            $this->atFPDF->SetXY(10, $y);
            $this->atFPDF->SetFont('Arial', 'B', 9);
            $this->atFPDF->Cell(195, $h, 'VER RELACION ANEXA', 0, 0, 'C');
        }
    }

    public function metHojaCuenta($y, $h, $contabilidad, $ln, $imprimir,$idOrdenPago) {

        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(195, $h, 'CONTABILIDAD FINANCIERA', 1, 0, 'C');
        $y+=$h;
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(20, $h, 'Cuenta', 1, 0, 'C');
        $this->atFPDF->Cell(115, $h, utf8_decode('Denominación'), 1, 0, 'L');
        $this->atFPDF->Cell(30, $h, 'Debe', 1, 0, 'R');
        $this->atFPDF->Cell(30, $h, 'Haber', 1, 0, 'R');
        $y+=$h;
        if(Session::metObtener('CONTABILIDAD')=='PUB20'){
            $a = 17.3;
        }else{
            $a = 0;
        }
        $this->atFPDF->Rect(10, $y, 195, ($h*$ln)-$a, "D");
        if ($imprimir) {
            $haber = 0.00;
            $debe = 0.00;
            $total_debe = 0.00;
            $total_haber = 0.00;
            foreach($contabilidad AS $i){
                if($codImpuestoIva=Session::metObtener('CONTABILIDAD') == 'ONCOP') {
                    if ($i['orden'] == '01') {
                        $obligacion = $this->atOrdenPagoModelo->atObligacionModelo->metBuscar('cp_d009_orden_pago', 'pk_num_orden_pago', $idOrdenPago);
                        $retencion = $this->atOrdenPagoModelo->atObligacionModelo->metRetenciones($obligacion['fk_cpd001_num_obligacion'], 'P');
                        $montoVoucher = ($i['MontoVoucher'] + $retencion['montoRetencion']);
                    } else {
                        $montoVoucher = $i['MontoVoucher'];
                    }
                }else{
                    $montoVoucher = $i['MontoVoucher'];
                }
                if ($i['columna'] == 'Debe'){
                    $debe = $montoVoucher;
                    $haber = 0.00;
                }else{
                    $haber = $montoVoucher;
                    $debe = 0.00;
                }
                $total_debe += $debe;
                $total_haber += $haber;
                $this->atFPDF->SetXY(10, $y);
                $this->atFPDF->SetFont('Arial', '', 9);
                $this->atFPDF->Cell(20, $h, $i['cod_cuenta'], 0, 0, 'C');
                $this->atFPDF->Cell(115, $h, strtoupper(substr(utf8_decode($i['ind_descripcion']), 0, 57)), 0, 0, 'L');
                $this->atFPDF->Cell(30, $h, number_format($debe, 2, ',', '.'), 0, 0, 'R');
                $this->atFPDF->Cell(30, $h, number_format($haber, 2, ',', '.'), 0, 0, 'R');
                $y+=$h;
            }
            $this->atFPDF->SetXY(10, $y+1);
            $this->atFPDF->SetFont('Arial', 'B', 9);
            $this->atFPDF->Cell(30, $h, '', 0, 0, 'C');
            $this->atFPDF->Cell(105, $h, '', 0, 0, 'L');
            $this->atFPDF->Cell(30, $h, number_format($total_debe, 2, ',', '.'), 0, 0, 'R');
            $this->atFPDF->Cell(30, $h, number_format($total_haber, 2, ',', '.'), 0, 0, 'R');
            $this->atFPDF->SetXY(10, $y);
//            $nombreResponsable='EMILYSMAR QUIJADA';
            $nombreResponsable='GEOVANNY CARABALLO';

            $this->atFPDF->Cell(90, $h+10, utf8_decode($nombreResponsable.' ________________'), 0, 0, 'L');
            $this->atFPDF->Cell(10, $h+10, ' __/__/____', 0, 0, 'L');
            $y+=$h;
        } else {
            $y+=($h*3);
            $this->atFPDF->SetXY(10, $y);
            $this->atFPDF->SetFont('Arial', 'B', 9);
            $this->atFPDF->Cell(195, $h, 'VER RELACION ANEXA', 0, 0, 'C');
        }
    }
    public function metHojaCuenta2($y, $h, $contabilidad, $ln, $imprimir,$idOrdenPago) {

        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(195, $h, 'CONTABILIDAD FINANCIERA (ORDEN DE PAGO)', 1, 0, 'C');
        $y+=$h;
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(20, $h, 'Cuenta', 1, 0, 'C');
        $this->atFPDF->Cell(115, $h, utf8_decode('Denominación'), 1, 0, 'L');
        $this->atFPDF->Cell(30, $h, 'Debe', 1, 0, 'R');
        $this->atFPDF->Cell(30, $h, 'Haber', 1, 0, 'R');
        $y+=$h;
        $this->atFPDF->Rect(10, $y, 195, ($h*$ln)-15, "D");
        if ($imprimir) {
            $haber = 0.00;
            $debe = 0.00;
            $total_debe = 0.00;
            $total_haber = 0.00;
            foreach($contabilidad AS $i){
                if($codImpuestoIva=Session::metObtener('CONTABILIDAD') == 'ONCOP') {
                    if ($i['orden'] == '01') {
                        $obligacion = $this->atOrdenPagoModelo->atObligacionModelo->metBuscar('cp_d009_orden_pago', 'pk_num_orden_pago', $idOrdenPago);
                        $retencion = $this->atOrdenPagoModelo->atObligacionModelo->metRetenciones($obligacion['fk_cpd001_num_obligacion'], 'P');
                        $montoVoucher = ($i['MontoVoucher'] + $retencion['montoRetencion']);
                    } else {
                        $montoVoucher = $i['MontoVoucher'];
                    }
                }else{
                    $montoVoucher = $i['MontoVoucher'];
                }
                if ($i['columna'] == 'Debe'){
                    $debe = $montoVoucher;
                    $haber = 0.00;
                }else{
                    $haber = $montoVoucher;
                    $debe = 0.00;
                }
                $total_debe += $debe;
                $total_haber += $haber;
                $this->atFPDF->SetXY(10, $y);
                $this->atFPDF->SetFont('Arial', '', 9);
                $this->atFPDF->Cell(20, $h, $i['cod_cuenta'], 0, 0, 'C');
                $this->atFPDF->Cell(115, $h, strtoupper(substr(mb_strtoupper($i['ind_descripcion'], 'utf-8'), 0, 57)), 0, 0, 'L');
                $this->atFPDF->Cell(30, $h, number_format($debe, 2, ',', '.'), 0, 0, 'R');
                $this->atFPDF->Cell(30, $h, number_format($haber, 2, ',', '.'), 0, 0, 'R');
                $y+=$h;
            }
            $this->atFPDF->SetXY(10, $y+1);
            $this->atFPDF->SetFont('Arial', 'B', 9);
            $this->atFPDF->Cell(30, $h, '', 0, 0, 'C');
            $this->atFPDF->Cell(105, $h, '', 0, 0, 'L');
            $this->atFPDF->Cell(30, $h, number_format($total_debe, 2, ',', '.'), 0, 0, 'R');
            $this->atFPDF->Cell(30, $h, number_format($total_haber, 2, ',', '.'), 0, 0, 'R');
            $this->atFPDF->SetXY(10, $y);
            //$nombreResponsable='ROSSELIS GUEVARA';
            //$this->atFPDF->Cell(90, $h+10, utf8_decode($nombreResponsable.' ________________'), 0, 0, 'L');
            //$this->atFPDF->Cell(10, $h+10, ' __/__/____', 0, 0, 'L');
            $y+=$h;
        } else {
            $y+=($h*3);
            $this->atFPDF->SetXY(10, $y);
            $this->atFPDF->SetFont('Arial', 'B', 9);
            $this->atFPDF->Cell(195, $h, 'VER RELACION ANEXA', 0, 0, 'C');
        }
    }

    public function metImprimir($idOrdenPago)
    {
        $ordenPago = $this->atOrdenPagoModelo->metConsultarOrdenPago($idOrdenPago);
        $retenciones = $this->atOrdenPagoModelo->metListarRetencionesImpuesto($idOrdenPago);
        $presupuesto = $this->atOrdenPagoModelo->metListarPartidasPresupuesto($idOrdenPago);
        $iva = $this->atOrdenPagoModelo->metBuscarIva($idOrdenPago);
        $partidaIva = $this->atOrdenPagoModelo->atObligacionModelo->metBuscar('pr_b002_partida_presupuestaria','cod_partida',Session::metObtener('IVADEFAULT'));

        $datosOblig = $this->atOrdenPagoModelo->atObligacionModelo->metConsultaObligacion($ordenPago['fk_cpd001_num_obligacion'], 'S');
        //para obligaciones desde nomina
        $tipoProceso = $this->atOrdenPagoModelo->atObligacionModelo->metBuscar('nm_b003_tipo_proceso', 'cod_proceso', substr($datosOblig['ind_nro_factura'], 2, 2), true);

        //valido la el tipo de contabilidad utilizada para de acuerdo a eso mostrar el sustento.
        $contabilidad = $this->atOrdenPagoModelo->metArmarOrdenPagoPub20($ordenPago['fk_cpd001_num_obligacion']);
        if($codImpuestoIva=Session::metObtener('CONTABILIDAD') == 'ONCOP') {
            $contabilidad = $this->atOrdenPagoModelo->metArmarOrdenPagoContabilidadFinanciera($ordenPago['fk_cpd001_num_obligacion'], $tipoProceso['pk_num_tipo_proceso']);
        }else{
            if(isset($tipoProceso['pk_num_tipo_proceso'])){
                $impuesto1 = $this->atOrdenPagoModelo->atObligacionModelo->metRetencionesNomina($ordenPago['fk_cpd001_num_obligacion'], $tipoProceso['pk_num_tipo_proceso']);
                $impuesto2 = NULL;
                //var_dump($impuesto1); exit;
            }else{
                $impuesto1 = $this->atOrdenPagoModelo->atObligacionModelo->metRetenciones($ordenPago['fk_cpd001_num_obligacion'],'N');
                $impuesto2 = $this->atOrdenPagoModelo->atObligacionModelo->metRetenciones($ordenPago['fk_cpd001_num_obligacion'],'P');
            }

            $contabilidad2 = $this->atOrdenPagoModelo->metOrdenacionPago($ordenPago['fk_cpd001_num_obligacion'], $impuesto1['montoRetencion'],$impuesto2['montoRetencion'],$tipoProceso['pk_num_tipo_proceso']);
        }
        define('PAGUESE',$ordenPago['EMPLEADO_APRUEBA']);
        define('COMFORMA',$ordenPago['EMPLEADO_CONFORMA']);
        define('FECHA_PAGUESE',$ordenPago['fec_aprobado']);
        define('FECHA_CONFORMA',$ordenPago['fec_conformado']);
        define('CARGO_CONFORMA',$ordenPago['CARGO_CONFORMA']);
        define('CARGO_APRUEBA',$ordenPago['CARGO_APRUEBA']);
        $this->atFPDF = new pdf('P', 'mm', 'Letter');
        $this->atFPDF->metNumeroOrdenPago(' ' . $ordenPago['ind_num_orden']);
        $this->atFPDF->metfecOrdenPago($ordenPago['fec_orden_pago']);
        $this->atFPDF->metHoja('ORDEN DE PAGO');
        $this->atFPDF->metOrganismo(utf8_decode(APP_ORGANISMO));
        $h = 4;
        $y = 30;
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->Rect(10, $y, 165, $h, "D");
        $this->atFPDF->Rect(175, $y, 30, $h, "D");
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(165, $h, 'BENEFICIARIO', 0, 0, 'L');
        $this->atFPDF->Cell(30, $h, 'CEDULA/R.I.F.', 0, 0, 'C');
        $y += $h;
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->Rect(10, $y, 165, $h, "D");
        $this->atFPDF->Rect(175, $y, 30, $h, "D");
        $this->atFPDF->SetFont('Arial', '', 9);
        $this->atFPDF->Cell(165, $h, utf8_decode($ordenPago['fk_a003_num_persona_proveedor_a_pagar']), 0, 0, 'L');
        $this->atFPDF->Cell(30, $h, $ordenPago['ind_documento_fiscal_pagar'], 0, 0, 'C');
        $y += $h;
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->Rect(10, $y, 195, $h * 2, "D");
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(40, $h, 'POR LA CANTIDAD DE:', 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 9);
      //  $this->atFPDF->MultiCell(155, $h, 'UN MILLÓN SETECIENTOS NOVENTA Y CINCO MIL TRESCIENTOS TREINTA CON NOVENTA Y CUATRO CENTIMOS', 0, 'L');
        $this->atFPDF->MultiCell(155, $h, utf8_decode($this->numtoletras(round($ordenPago['num_monto_obligacion'], 2))), 0, 'L');
        $y += $h * 2;
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', '', 9);
        $this->atFPDF->MultiCell(195, $h, 'POR CONCEPTO DE: ' . utf8_decode($ordenPago['ind_comentarios']), 'LTRB', 'J');
        $y = $this->atFPDF->GetY();
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->Rect(10, $y, 110, $h, "D");
        $this->atFPDF->Rect(120, $y, 85, $h, "D");
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(15, $h, 'BANCO:', 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 9);
        $this->atFPDF->Cell(95, $h, $ordenPago['a006_miscelaneo_detalle_banco'], 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(25, $h, ' NRO. CUENTA:', 0, 0, 'C');
        $this->atFPDF->SetFont('Arial', '', 9);
        $this->atFPDF->Cell(63, $h, $ordenPago['ind_num_cuenta'], 0, 0, 'L');
        $y += $h;
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->Rect(10, $y, 195, $h + 3, "D");
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(150, 8, 'MONTO BRUTO (Bs.): ', 0, 0, 'R');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(45, 8, number_format($ordenPago['num_monto_afecto'] + $ordenPago['num_monto_no_afecto'] + $ordenPago['num_monto_impuesto'], 3, ',', '.'), 0, 0, 'R');
        $y += $h + 3;
        if (count($retenciones) > 10) {
            $this->metHojaRetencion($y, $h, $retenciones, 11, false);
            $y += ($h * 11);
        } else {
            $this->metHojaRetencion($y, $h, $retenciones, 11, true);
            $y += ($h * (11 - count($retenciones)));
        }/*
        if ($ordenPago['num_monto_descuento'] > 0) {
            $this->atFPDF->SetXY(10, $y);
            $codigo = '';
            $this->atFPDF->SetFont('Arial', 'B', 9);
            $this->atFPDF->Cell(15, $h, $codigo, 0, 0, 'C');
            if ($ordenPago['ind_tipo_descuento'] == "1") {
                $this->atFPDF->Cell(105, $h, 'Notas de Creditos y Diferencias por Reclasificar', 0, 0, 'L');
            } elseif ($ordenPago['ind_tipo_descuento'] == "2") {
                $this->atFPDF->Cell(105, $h, 'DEDUCCIONES VARIAS POR RECLASIFICAR', 0, 0, 'L');
            }
            $this->atFPDF->Cell(30, $h, '', 0, 0, 'C');
            $this->atFPDF->Cell(15, $h, '', 0, 0, 'R');
            $this->atFPDF->Cell(30, $h, number_format($ordenPago['num_monto_descuento'], 2, ',', '.'), 0, 0, 'R');
            $y += $h;
        }*/
        $y = $this->atFPDF->GetY();
        if($y < 80){
            $y = 84;
        }
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(165, $h, 'MONTO RETENCIONES (Bs.): ', 0, 0, 'R');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(30, $h, number_format($ordenPago['num_monto_impuesto_otros'], 2, ',', '.'), 0, 0, 'R');
        $y += $h;
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(165, $h, 'AMORTIZACION ANTICIPO (Bs.): ', 0, 0, 'R');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(30, $h, number_format($ordenPago['num_monto_adelanto'], 2, ',', '.'), 0, 0, 'R');
        $y += $h;
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->Cell(165, $h, 'MONTO NETO (Bs.): ', 0, 0, 'R');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(30, $h, number_format($ordenPago['num_monto_obligacion'], 2, ',', '.'), 0, 0, 'R');
        $y += $h;
        //	imprimo distribucion presupuestaria
        if (count($presupuesto) > 4) {
            $this->metHojaPresupuesto($y, $h, $presupuesto, 8, false, $iva,$partidaIva);
            $y += ($h * 8);
        } else {
            $this->metHojaPresupuesto($y, $h, $presupuesto, 8, true, $iva,$partidaIva);
            $y += ($h * (10 - count($presupuesto)));
        }
        //	imprimo contabilidad financiera
        if (count($contabilidad) > 4) {
            $this->metHojaCuenta($y, $h, $contabilidad, 11, false,$idOrdenPago);
            $y+=($h*6);
        } else {
            $this->metHojaCuenta($y, $h, $contabilidad, 11, true,$idOrdenPago);
            $y+=($h*(8-count($contabilidad)))+15;
        }
        if($codImpuestoIva=Session::metObtener('CONTABILIDAD') == 'PUB20') {
            //	imprimo contabilidad financiera2
            if (count($contabilidad) > 4) {
                $this->metHojaCuenta2($y, $h, $contabilidad2, 11, false,$idOrdenPago);
                $y+=($h*6);
            } else {
                $this->metHojaCuenta2($y, $h, $contabilidad2, 11, true,$idOrdenPago);
                $y+=($h*(8-count($contabilidad2)));
            }
        }
        //	imprimo las hojas anexas
        $y = 30;
        if (count($retenciones) > 10) {
            $this->atFPDF->AddPage();
            $this->atFPDF->metHoja('RELACIÓN ANEXA');
            $this->metHojaRetencion($y, $h, $retenciones, 57, true, $iva);
        }
        $y = 30;
        if (count($presupuesto) > 4) {
            $this->atFPDF->AddPage();
            $this->atFPDF->metHoja('RELACIÓN ANEXA');
            $this->metHojaPresupuesto($y, $h, $presupuesto, 57, true, $iva,$partidaIva);
        }
        $y = 30;
        if (count($contabilidad) > 4) {
            $this->atFPDF->AddPage();
            $this->atFPDF->metHoja('RELACIÓN ANEXA');

            $this->metHojaCuenta($y, $h, $contabilidad, 57, true,$idOrdenPago);
        }
        if($codImpuestoIva=Session::metObtener('CONTABILIDAD') == 'PUB20') {
            $y = 30;
            if (count($contabilidad2) > 4) {
                $this->atFPDF->AddPage();
                $this->atFPDF->metHoja('RELACIÓN ANEXA');

                $this->metHojaCuenta2($y, $h, $contabilidad2, 57, true,$idOrdenPago);
            }
        }
        $this->atFPDF->Output();
    }

    public function metAccionesOrdenPago()
    {
        $idOrdenPago = $this->metObtenerInt('idOrdenPago');
        $idVoucherPago = $this->metObtenerInt('idVoucherPago');
        $estado = $this->metObtenerAlphaNumerico('estado');
        if ($estado == 'ANULAR') {
            $motivo = $this->metObtenerAlphaNumerico('motivo');
            if($motivo==''){
                $validacion['status']='error';
                $validacion['ind_motivo_anulacion']='error';
                echo json_encode($validacion);
                exit;
            }
            $voucher = $this->atOrdenPagoModelo->metAnularVoucher($idVoucherPago);
            //insertar --fk_cbb001_num_voucher_anulacion-- en pago
            $id = $this->atOrdenPagoModelo->metAnularOrdenPago($idOrdenPago, $motivo);
            if (is_array($id)) {
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['Mensaje'] = array('titulo' => 'ANULADO', 'contenido' => 'La Orden de Pago fue Anulada Satisfactoriamente');
            $validacion['status'] = 'OK';
            $validacion['idOrdenPago'] = $id;
            echo json_encode($validacion);
            exit;
        } elseif ($estado == 'GENERAR' or $estado == 'generarPP') {
            $detalleCuentaBD = $this->atOrdenPagoModelo->metConsultarOrdenPagoCuenta($idOrdenPago);
            //saldo actual en banco y monto de la orden de pago
            $dispFinanciera = $this->atOrdenPagoModelo->metDispFinanciera($idOrdenPago);
            //monto de pagos pendientes por la misma cuenta
            $saldoPendiente = $this->atOrdenPagoModelo->metPagoPendiente($detalleCuentaBD['pk_num_cuenta']);
            $saldoDisponible = $dispFinanciera['montoActual'] - ($dispFinanciera['montoTotal'] + $saldoPendiente['montoPendiente']);

            if($saldoDisponible < 0){
                $validacion['status'] = 'noDispFinanciera';
                echo json_encode($validacion);
                exit;
            }
            if($estado == 'GENERAR'){
                $id = $this->atOrdenPagoModelo->metGenerarOrdenPago($idOrdenPago);
                if (is_array($id)) {
                    $validacion['status'] = 'errorSQL';
                    echo json_encode($validacion);
                    exit;
                }
                $id = str_getcsv($id, '-');
                $validacion['Mensaje'] = array('titulo' => 'GENERADO', 'contenido' => 'La Orden de Pago fue Generada Satisfactoriamente y creado el pago N°' . $id[1]);

            }else{
                $ind=$this->metValidarFormArrayDatos('form','int');
                $registro = $ind;

                $montoPagado = $this->metObtenerFormulas('montoTotal');
                $montoRestante = $this->metObtenerFormulas('montoRestante');

                if (!is_numeric((float)$montoPagado)){
                    $validacion['num_monto_pago'] = 'error';
                    $validacion['status'] = 'errorMonto';
                    echo json_encode($validacion);
                    exit;
                }
                if ($montoPagado == 0){
                    $validacion['num_monto_pago'] = 'error';
                    $validacion['status'] = 'errorMontoPagado';
                    echo json_encode($validacion);
                    exit;
                }
                if($montoRestante == 0){
                    $estado = 'GP';
                }else{
                    $estado = 'PP';
                }
                $id = $this->atOrdenPagoModelo->metGenerarOrdenPagoParcial($idOrdenPago,$montoPagado, $estado);
                if (is_array($id)) {
                    $validacion['status'] = 'errorSQL';
                    echo json_encode($validacion);
                    exit;
                }
                $id = str_getcsv($id, '-');
                $validacion['Mensaje'] = array('titulo' => 'PAGO PARCIAL GENERADO', 'contenido' => 'La Orden de Pago fue Generada Satisfactoriamente y creado el pago N°');
                $pagoParcialDetalle = $this->atOrdenPagoModelo->metDetallePagoParcial($idOrdenPago,$registro,$id[2]);
            }
            $validacion['status'] = 'OK';
            $validacion['idOrdenPago'] = $id[0];
            echo json_encode($validacion);
            exit;
        }
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $ordenPagoBD = $this->atOrdenPagoModelo->metConsultarOrdenPago($idOrdenPago);
        $this->atVista->assign('ordenPagoBD', $ordenPagoBD);
        $this->atVista->assign('idOrdenPago', $idOrdenPago);

        if ($estado == 'VER' || $estado == 'AN') {
            $this->atVista->assign('estado', $estado);
            $this->atVista->assign('documento', $this->atOrdenPagoModelo->atDocumentosModelo->metDocumentoListar());
            $this->atVista->assign('listadoTipoPago', $this->atOrdenPagoModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
            $this->atVista->assign('listadoCuentas', $this->atOrdenPagoModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->metRenderizar('nuevo', 'modales');
        } elseif ($estado == 'GE') {
            $this->atVista->assign('estado', $estado);
            //verificar si la orden tiene pagos parcial
            $detallePagoParcial = $this->atOrdenPagoModelo->metConsultarOrdenPagoPP($idOrdenPago);
            $detalleCuentaBD = $this->atOrdenPagoModelo->metConsultarOrdenPagoCuenta($idOrdenPago);
            $detallePartidaBD = $this->atOrdenPagoModelo->metConsultarOrdenPagoPartida($idOrdenPago);

            $detallePartidas = array();
            foreach ($detallePartidaBD as $i){
                $detallePartidaRestante = $this->atOrdenPagoModelo->metConsultarPagoPP($idOrdenPago,$i['fk_prb002_num_partida_presupuestaria']);
                $detallePartidas[] = array(
                    'pkCuenta' => $i['pkCuenta'],
                    'cod_cuenta' => $i['cod_cuenta'],
                    'pkCuenta20' => $i['pkCuenta20'],
                    'cod_cuenta20' => $i['cod_cuenta20'],
                    'pkPartida' => $i['pkPartida'],
                    'cod_partida' => $i['cod_partida'],
                    'num_monto' => $i['num_monto'] - $detallePartidaRestante['num_monto'],
                    'montoRestante' => $i['num_monto'] - $detallePartidaRestante['num_monto'],
                    'num_monto_pagar' => 0,
                    'clase' => 'montoPartida'
                );
            }
            if($detallePagoParcial == null){
                if($ordenPagoBD['num_monto_impuesto'] != 0){
                    $detalleImpuesto = $this->atOrdenPagoModelo->metConsultarOrdenPagoImpuesto($idOrdenPago);
                    $pkPartida = $this->atOrdenPagoModelo->atObligacionModelo->metBuscar('pr_b002_partida_presupuestaria', 'cod_partida', Session::metObtener('IVADEFAULT'));
                    $detallePartidas[] = array(
                        'pkCuenta' => $detalleImpuesto['pkCuenta'],
                        'cod_cuenta' => $detalleImpuesto['cod_cuenta'],
                        'pkCuenta20' => $detalleImpuesto['pkCuenta20'],
                        'cod_cuenta20' => $detalleImpuesto['cod_cuenta20'],
                        'pkPartida' => $pkPartida['pk_num_partida_presupuestaria'],
                        'cod_partida' => Session::metObtener('IVADEFAULT'),
                        'num_monto' => $detalleImpuesto['num_monto'],
                        'num_monto_pagar' => $detalleImpuesto['num_monto'],
                        'montoRestante' => 0,
                        'clase' => 'montoPartida2'
                    );
                }
            }

            //saldo actual en banco y monto de la orden de pago
            $dispFinanciera = $this->atOrdenPagoModelo->metDispFinanciera($idOrdenPago);
            //monto de pagos pendientes por la misma cuenta
            $saldoPendiente = $this->atOrdenPagoModelo->metPagoPendiente($detalleCuentaBD['pk_num_cuenta']);
            $saldoDisponible = $dispFinanciera['montoActual'] - ($dispFinanciera['montoTotal'] + $saldoPendiente['montoPendiente']);

            $this->atVista->assign('saldoDisponible', $saldoDisponible);
            $this->atVista->assign('saldoPendiente', $saldoPendiente);
            $this->atVista->assign('dispFinanciera', $dispFinanciera);
            $this->atVista->assign('detallePagoParcial', $detallePagoParcial);
            $this->atVista->assign('detalleCuentaBD', $detalleCuentaBD);
            $this->atVista->assign('detallePartidaBD', $detallePartidas);
            $this->atVista->assign('idOrdenPago', $idOrdenPago);
            $this->atVista->metRenderizar('generar', 'modales');
        }
    }

    public function metRegistrarAnulacion()
    {
        $idOrdenPago = $this->metObtenerInt('idOrdenPago');
    }

}
