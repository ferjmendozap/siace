<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once ROOT.'librerias'.DS.'Number.php';
class imprimirTransferirControlador extends Controlador
{
    private $atImprimirTransferir;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atImprimirTransferir = $this->metCargarModelo('imprimirTransferir');


    }
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('dataBD',$this->atImprimirTransferir->metListarPago());
        $this->atVista->metRenderizar('listado');

    }

    public function metImprimirTransferir()
    {
        $complementosJs = array(
            'select2/select2.min',
        );
        $complementoCss = array(
            'select2/select201ef',
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idPago = $this->metObtenerInt('idPago');
        $verPago = $this->metObtenerInt('verPago');
        $valido = $this->metObtenerInt('valido');
        if($verPago){
            $idPago = $this->metObtenerAlphaNumerico('idPago');
            $idPago = (int)str_replace('idPago', '', $idPago);
            $registro= $this->atImprimirTransferir->metConsultarPagoSeleccionado($idPago);
            echo json_encode($registro);
            exit;
        }
        if($valido==1){
            //recibir el nro de cheque ingresado por el usuario.
            $chequeUsuario = $this->metObtenerAlphaNumerico('ind_cheque_usuario');
            //buscar el tipo de pago
            $tipoPago = $this->metObtenerAlphaNumerico('a006_miscelaneo_detalle_tipoPago');

            //VALIDAR QUE EL TIPO DE PAGO SEA CHEQUE
            if($tipoPago == 'CHEQUE'){
                //validar que el usurio ingreso el numero de cheque.
                if($chequeUsuario == 0){
                    $registro['status'] = 'errorCheque';
                    $registro['ind_cheque_usuario'] = 'error';
                    echo json_encode($registro);
                    exit;
                }
                //consultar nro de cheque del sistema para esa cuenta bancaria.
                $registro= $this->atImprimirTransferir->metConsultarPagoSeleccionado($idPago);
                $chequeSist =  $this->atImprimirTransferir->metBuscarNroCheque($registro['fk_cpb014_num_cuenta']);

                if(isset($chequeSist['ind_num_cheque'])) {
                    $chequeSis = $chequeSist['ind_num_cheque'] + 1;
                    $cheque = substr($chequeUsuario, -4, 4);
                    //validar si el cheque ingresado por el usuario concuerda con el que lleva el sistema.
                    if ($cheque == $chequeSis) {
                        //se realiza pago.
                        $id = $this->atImprimirTransferir->metImprimirTransferir($idPago, $chequeUsuario, $chequeSis);
                        if (is_array($id)) {
                            $registro['status'] = 'errorSQL';
                            $registro['id'] = $id;
                            echo json_encode($registro);
                            exit;
                        }
                        //actualizo el numero de correlativo de la chequera
                        $this->atImprimirTransferir->metActualizarNroCheque($chequeSist['pk_num_chequera'], $chequeSis);
                        //valido si es el ultimo numero de correlativo de la chequera
                        if($chequeSist['num_chequera_hasta'] == $chequeSis) {
                            //en caso de que lo sea, descativo la chequera. *estatus = 0
                            $this->atImprimirTransferir->metCambioChequera($registro['fk_cpb014_num_cuenta'], $chequeSist['ind_num_chequera'], 0);
                            //si existe otra chequera para la cuenta, se actica automaticamente. *estatus = 1
                            $this->atImprimirTransferir->metCambioChequera($registro['fk_cpb014_num_cuenta'], $chequeSist['ind_num_chequera']+1, 1);
                        }
                    } else {
                        //si no existe, mostrar alerta
                        $registro['status'] = 'errorNroCheque';
                        $registro['ind_cheque_usuario'] = 'error';
                        echo json_encode($registro);
                        exit;
                    }
                }else{
                    //si no existe, mostrar alerta
                    $registro['status'] = 'errorNroChequeNO';
                    echo json_encode($registro);
                    exit;
                }
            }else{
                //SI NO ES TIPO PAGO CHEQUE
                //se realiza pago.
                $id = $this->atImprimirTransferir->metImprimirTransferir($idPago, $chequeUsuario);
                if (is_array($id)) {
                    $registro['status'] = 'errorSQL';
                    echo json_encode($registro);
                    exit;
                }
            }

            $id=str_getcsv($id,'-');
            $validacion['Mensaje'] = array('titulo'=>'IMPRESO','contenido'=>'El pago fue Impreso y creado bajo el N°'.$id[1]);
            $validacion['contabilidad'] = Session::metObtener('CONTABILIDAD');
            $validacion['status'] = 'OK';
            $validacion['idPago'] = $id[0];
            $validacion['tipoPago'] = $id[2];

            $nroPagoParcial = $this->atImprimirTransferir->atObligacionModelo->metBuscar('cp_d015_orden_pago_parcial','fk_cpd010_num_pago',$idPago);

            $validacion['nroPagoParcial'] = $nroPagoParcial['num_secuencia_pago'];
            echo json_encode($validacion);
            exit;
        }

        $this->atVista->assign('idPago', $idPago);
        $this->atVista->assign('formDB', $this->atImprimirTransferir->metConsultarPagoSeleccionado($idPago));
        $this->atVista->metRenderizar('nuevo', 'modales');
    }

    public function metVouchers()
    {
        $idPago = $this->metObtenerInt('idPago');
        //en caso de que sea pago parcial se indica el nro de pago para armar el vouchers (para incluir o no los impuetsos y retenciones)
        $nroPagoParcial = $this->metObtenerInt('nroPagoParcial');
        //parametro para verificar si es un pago parcial
        $tipoPago = $this->metObtenerAlphaNumerico('tipoPago');

        $voucher = $this->metObtenerAlphaNumerico('voucher');
        $pkObligacion = $this->atImprimirTransferir->atObligacionModelo->metObligacion($idPago);
        $idObligacion = $pkObligacion['pk_num_obligacion'];

        //Enviar a la vista el Centro de Costo por defecto para los vouchers
        $this->atVista->assign('centroCosto', $this->atImprimirTransferir->atObligacionModelo->metBuscar('a023_centro_costo','ind_abreviatura',Session::metObtener('CCOSTOVOUCHER')));
        //Enviar a la vista el organismo
        $this->atVista->assign('organismo', $this->atImprimirTransferir->atObligacionModelo->metOrganismo());
        $this->atVista->assign('libroContable', $this->atImprimirTransferir->atObligacionModelo->atLibroContableModelo->metListar());
        $this->atVista->assign('voucher', $this->atImprimirTransferir->atObligacionModelo->atVoucherModelo->metListar());

        $datosOblig = $this->atImprimirTransferir->atObligacionModelo->metConsultaObligacion($idObligacion);

        if($voucher == 'ONCOP'){
            $contab = 'T';
            $contabilidades = $this->atImprimirTransferir->atObligacionModelo->metBuscar('cb_b005_contabilidades','ind_tipo_contabilidad','T');
            $contabilidad = $contabilidades['pk_num_contabilidades'];
            $codVoucher = $datosOblig['fk_cbc003_num_tipo_voucher'];
            $retencion = $this->atImprimirTransferir->atObligacionModelo->metRetenciones($idObligacion,'P');
            //vouchers de Pago
            if($tipoPago == 'pago'){
                $datos = $this->atImprimirTransferir->metArmarPagoContabilidad($idObligacion,$datosOblig['num_flag_provision'],$retencion['montoRetencion']);
            }else{
                $datos = $this->atImprimirTransferir->metArmarPagoParcial($nroPagoParcial, $idPago, $idObligacion,$datosOblig['num_flag_provision'],$retencion['montoRetencion']);
            }
            $debitos=0; $creditos=0;
            foreach ($datos as $i) {
                //descuento
                if($i['orden'] == '03'){
                    $montoVoucher = $i['MontoVoucher'];
                }else{
                    $montoVoucher = $i['MontoVoucher'];
                }

                if($i['columna']=='Debe'){
                    $debitos = $debitos + $montoVoucher;

                    $datoArreglo['Debe'][]=array(
                        'pk_num_cuenta' => $i['pk_num_cuenta'],
                        'cod_cuenta' => $i['cod_cuenta'],
                        'ind_descripcion' => $i['ind_descripcion'],
                        'MontoVoucher' => $montoVoucher
                    );
                }else{
                    $creditos = $creditos + $montoVoucher;
                    $datoArreglo['Haber'][]=array(
                        'pk_num_cuenta' => $i['pk_num_cuenta'],
                        'cod_cuenta' => $i['cod_cuenta'],
                        'ind_descripcion' => $i['ind_descripcion'],
                        'MontoVoucher' => $montoVoucher
                    );
                }
            }
            if(!isset($datoArreglo)){
                $datoArreglo=false;
            }
        }else{
            $contab = 'F';
            $codVoucher = $datosOblig['fk_cbc003_num_tipo_voucher'];
            $impuesto1 = $this->atImprimirTransferir->atObligacionModelo->metRetenciones($idObligacion,'N');

            //para obligaciones desde nomina
            $tipoProceso = $this->atImprimirTransferir->atObligacionModelo->metBuscar('nm_b003_tipo_proceso','cod_proceso',substr($datosOblig['ind_nro_factura'], 2, 2),true);
            if(isset($tipoProceso['pk_num_tipo_proceso'])){
                $impuesto1 = $this->atImprimirTransferir->atObligacionModelo->metRetencionesNomina($idObligacion,$tipoProceso['pk_num_tipo_proceso']);
                $impuesto2 = NULL;
            }else{
                if($datosOblig['num_flag_provision'] == 0){
                    $impuesto2 = $this->atImprimirTransferir->metImpuesto($idObligacion);
                }else{
                    $impuesto2 = 0;
                }

            }
            //vouchers de Pago
            if($tipoPago == 'pago'){
                $datos = $this->atImprimirTransferir->metPago($idObligacion,$datosOblig['num_flag_provision'],$impuesto1['montoRetencion'],$impuesto2['montoRetencion']);
            }else{
                $datos = $this->atImprimirTransferir->metPagoParcial($nroPagoParcial, $idPago, $idObligacion,$datosOblig['num_flag_provision'],$impuesto1['montoRetencion'],$impuesto2['montoRetencion']);
            }
            $debitos=0; $creditos=0;
            foreach ($datos as $i) {
                if($i['columna'] == 'Debe'){
                    $debitos = $debitos + $i['MontoVoucher'];

                    $datoArreglo['Debe'][] = array(
                        'pk_num_cuenta' => $i['pk_num_cuenta'],
                        'cod_cuenta' => $i['cod_cuenta'],
                        'ind_descripcion' => $i['ind_descripcion'],
                        'MontoVoucher' => $i['MontoVoucher']
                    );
                }else{
                    $creditos = $creditos + $i['MontoVoucher'];
                    $datoArreglo['Haber'][] = array(
                        'pk_num_cuenta' => $i['pk_num_cuenta'],
                        'cod_cuenta' => $i['cod_cuenta'],
                        'ind_descripcion' => $i['ind_descripcion'],
                        'MontoVoucher' => $i['MontoVoucher']
                    );
                }
            }
            if(!isset($datoArreglo)){
                $datoArreglo=false;
            }

        }
        $contabilidades = $this->atImprimirTransferir->atObligacionModelo->metBuscar('cb_b005_contabilidades','ind_tipo_contabilidad',$contab);

        $this->atVista->assign('datoArreglo',$datoArreglo);
        $this->atVista->assign('datosOblig', $datosOblig);
        $this->atVista->assign('creditos', $creditos);
        $this->atVista->assign('debitos', $debitos);
        $this->atVista->assign('datos', $datos);
        $this->atVista->assign('codVoucher', $codVoucher);
        $this->atVista->assign('contabilidad', $contabilidades['pk_num_contabilidades']);
        $this->atVista->assign('idPago', $idPago);

        $this->atVista->metRenderizar('vouchersPago', 'modales');

    }

    public function metRegistrarVouchers()
    {
        $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
        $txt = $this->metValidarFormArrayDatos('form', 'txt');
        $ind = $this->metValidarFormArrayDatos('form', 'int');
        $idPago = $this->metObtenerInt('idPago');
        $idObligacion = $this->metObtenerInt('idObligacion');
        $tipo = $this->metObtenerAlphaNumerico('tipo');
        $tipoVoucher = $this->metObtenerAlphaNumerico('tipoVoucher');
        $txt_titulo_voucher = $this->metObtenerFormulas('txt_titulo_voucher');

        if ($alphaNum != null && $ind == null && $txt == null) {
            $validacion = $alphaNum;
        } elseif ($alphaNum == null && $ind != null && $txt == null) {
            $validacion = $ind;
        } elseif ($alphaNum != null && $ind != null && $txt == null) {
            $validacion = array_merge($alphaNum, $ind);
        } elseif ($alphaNum != null && $txt != null && $ind == null) {
            $validacion = array_merge($alphaNum, $txt);
        } elseif ($ind != null && $txt != null && $alphaNum == null) {
            $validacion = array_merge($ind, $txt);
        } else {
            $validacion = array_merge($ind, $txt, $alphaNum);
        }
        if($txt_titulo_voucher != null){
            $formula['txt_titulo_voucher'] = $txt_titulo_voucher;
            $validacion = array_merge($validacion, $formula);
        }
        if (in_array('error', $validacion)) {
            $validacion['status'] = 'error';
            echo json_encode($validacion);
            exit;
        }

        $this->idUsuario = Session::metObtener('idUsuario');
        $validacion['fk_a003_num_preparado_por'] = $this->idUsuario;
        $organismo=$this->atImprimirTransferir->atObligacionModelo->metObtenerOrganismo($this->idUsuario);
        $validacion['fk_a001_num_organismo']=$organismo['fk_a001_num_organismo'];
        $validacion['fk_a004_num_dependencia']=$organismo['fk_a004_num_dependencia'];
        // datos default
        $validacion['fk_cbc002_num_sistema_fuente']= "6";
        $validacion['num_flag_transferencia']= "0";

        $resultado=$this->atImprimirTransferir->atObligacionModelo->metNuevo($validacion);
        $codVoucher = $this->atImprimirTransferir->atObligacionModelo->metBuscar('cb_b001_voucher','pk_num_voucher_mast',$resultado);
        $validacion['codVoucher'] = $codVoucher['ind_voucher'];
        $validacion['idVoucher'] = $resultado;
        if(isset($tipo) and $tipo == 'anulacion'){
            if($tipoVoucher == 'ordenPago'){
                $voucherPago = $this->atImprimirTransferir->atObligacionModelo->metVoucherAnulacion($this->metObtenerInt('idOrdenPago'), $validacion['idVoucher']);
                //conseguir pk de orden pago para actualizar registro
            }else{
                $voucherPago = $this->atImprimirTransferir->metVoucherAnulacion($idPago, $validacion['idVoucher']);
            }
        }else {
            $voucherPago = $this->atImprimirTransferir->metVouchersPago($idPago, $validacion['idVoucher'], $idObligacion);
        }
        if($validacion['idVoucher'] == 'no-periodo'){
            $validacion['status'] = 'no-periodo';
            echo json_encode($validacion);
            exit;
        }
        $validacion['idPago'] = $idPago;
        $validacion['status'] = 'ok2';
        echo json_encode($validacion);
        exit;

    }


}