<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class transaccionesBancariasControlador extends Controlador
{
    private $atTransaccionesModelo;
    private $atFPDF;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atTransaccionesModelo = $this->metCargarModelo('transaccionesBancarias');
        $this->metObtenerLibreria('cabeceraCP','modCP');
        $this->atFPDF = new PDF('P', 'mm', 'Letter');

    }
    public function metIndex($lista=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        if($lista=='saldoBancos') {
            $this->atVista->assign('dataBD', $this->atTransaccionesModelo->metSaldoBanco());
            $this->atVista->metRenderizar('saldoBancos');
        }else{
            $this->atVista->assign('dataBD',$this->atTransaccionesModelo->metTransacciones($lista));
            $this->atVista->assign('lista', $lista);
            $this->atVista->assign('estados', $this->atTransaccionesModelo->atMiscelaneoModelo->metMostrarSelect('ESTADO'));
            $this->atVista->assign('selectBANCOS', $this->atTransaccionesModelo->atMiscelaneoModelo->metMostrarSelect('BANCOS'));
            $this->atVista->assign('listadoCuentas', $this->atTransaccionesModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->metRenderizar('listado');

        }
    }

    public function metTipoTransacciones($transaccion, $idCampo = false)
    {
        $this->atVista->assign('lista', $this->atTransaccionesModelo->metListaTransacciones());
        $this->atVista->assign('listaTransaccion', $transaccion);
        $this->atVista->assign('idCampo', $idCampo);
        $this->atVista->metRenderizar('listadoTipoTransacciones', 'modales');

    }

    public function metAcciones()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min'
        );
        $complementoCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idTransaccion = $this->metObtenerInt('idTransaccion');
        $estado = $this->metObtenerAlphaNumerico('estado');
        $valido = $this->metObtenerAlphaNumerico('valido');

        if ($valido == 1) {

            $Excepcion = array('txt_comentario_anulacion');
            $ExcepcionInt = array('num_flag_presupuesto');

            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');
            $txt = $this->metValidarFormArrayDatos('form', 'txt',$Excepcion);
            $ind = $this->metValidarFormArrayDatos('form', 'int',$ExcepcionInt);

            if ($alphaNum != null && $ind == null && $txt == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $registro = $ind;
            }elseif ($alphaNum == null && $ind == null && $txt != null) {
                $registro = $txt;
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $registro = array_merge($alphaNum, $ind);
            } elseif ($alphaNum != null && $txt != null && $ind == null) {
                $registro = array_merge($alphaNum, $txt);
            } elseif ($ind != null && $txt != null && $alphaNum == null) {
                $registro = array_merge($ind, $txt);
            } else {
                $registro = array_merge($ind, $txt, $alphaNum);
            }
            if (!isset($registro['num_flag_presupuesto'])) {
                $registro['num_flag_presupuesto'] = '0';
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if(!isset($registro['codDetalle'])){
                $registro['status'] = 'errorCodDetalle';
                echo json_encode($registro);
                exit;
            }else{
                $secuencia = $registro['codDetalle']['num_secuencia'];
                foreach($secuencia as $sec){
                    //var_dump($registro['codDetalle'][$sec]);
                    $tipoTransaccion = $registro['codDetalle'][$sec]['fk_cpb006_num_banco_tipo_transaccion'];
                    if($registro['codDetalle'][$sec] == '') {
                        $registro['status'] = 'errorDetalle';
                        echo json_encode($registro);
                        exit;
                    }
                    if(!isset($registro['codDetalle'][$sec]['fk_cpb002_num_tipo_documento'])
                        or $registro['codDetalle'][$sec]['fk_cpb014_num_cuenta']==''
                        or $registro['codDetalle'][$sec]['fk_a003_num_persona_proveedor']==''
                        or $registro['codDetalle'][$sec]['fk_a023_num_centro_costo']=='') {
                        $registro['status'] = 'errorDetalle';
                        echo json_encode($registro);
                        exit;
                    }
                }
            }

            if ($idTransaccion == 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atTransaccionesModelo->metNuevaTransaccionBancaria($registro);
                $registro['idTransaccion'] = $id;
                $idNuevo = $this->atTransaccionesModelo->metTransacciones(false,$registro['idTransaccion']);
            } else {
                $id=$this->atTransaccionesModelo->metModificarTransaccionBancaria($idTransaccion,$registro);
                $registro['status'] = 'modificar';
            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($id);
                exit;
            }

            $idNuevo['status']=$registro['status'];
            echo json_encode($idNuevo);
            exit;
        }
        
        if($estado == 'RV'){
            $id = $this->atTransaccionesModelo->metRevisarTransaccionBancaria($idTransaccion);
            if (is_array($id)) {
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['Mensaje'] = array('titulo' => 'REVISADO', 'contenido' => 'La Transaccion Bancaria fue Revisada Satisfactoriamente');
            $validacion['status'] = 'OK';
            $validacion['idTransaccion'] = $id;
            echo json_encode($validacion);
            exit;
        }elseif($estado == 'AP'){
            $id = $this->atTransaccionesModelo->metAprobarTransaccionBancaria($idTransaccion);
            $flagPresupuesto = $this->atTransaccionesModelo->atObligacionModelo->metBuscar('cp_d011_banco_transaccion','pk_num_banco_transaccion',$idTransaccion);
            if($flagPresupuesto['num_flag_presupuesto'] == '1'){
                $presupuesto = $this->atTransaccionesModelo->metPresupuesto($idTransaccion);

            }
            if (is_array($id)) {
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $generaVoucherProvision = $this->atTransaccionesModelo->metConsultarTransacciones($idTransaccion);
            $validacion['num_flag_voucher'] = $generaVoucherProvision['num_flag_genera_voucher'];
            $validacion['contabilidad'] = Session::metObtener('CONTABILIDAD');
            $validacion['Mensaje'] = array('titulo' => 'ACTUALIZADA', 'contenido' => 'La Transaccion Bancaria fue Actualizada Satisfactoriamente');
            $validacion['status'] = 'OK';
            $validacion['idTransaccion'] = $id;
            echo json_encode($validacion);
            exit;
        }elseif($estado == 'AN'){
        $motivoAnulacion = $this->metObtenerAlphaNumerico('motivo');
            $dispFinanciera = $this->atTransaccionesModelo->metDispFinanciera($idTransaccion);
            if($dispFinanciera['montoTotal'] > $dispFinanciera['montoActual']){
                $validacion['status'] = 'noDispFinanciera';
                echo json_encode($validacion);
                exit;
            }
            $id = $this->atTransaccionesModelo->metAnularTransaccionBancaria($idTransaccion, $motivoAnulacion);
            $transacion = $this->atTransaccionesModelo->metConsultarTransacciones($idTransaccion);
            $validacion['fk_cbb001_num_voucher']=$transacion['fk_cbb001_num_voucher'];

        if (is_array($id)) {
            $validacion['status'] = 'errorSQL';
            echo json_encode($validacion);
            exit;
        }
        $validacion['Mensaje'] = array('titulo' => 'DESACTUALIZADA', 'contenido' => 'La Transaccion Bancaria fue Desactualizada Satisfactoriamente');
        $validacion['status'] = 'OK';
        $validacion['idTransaccion'] = $id;
        echo json_encode($validacion);
        exit;
    }elseif($estado == 'RECHAZAR'){
        $motivoAnulacion = $this->metObtenerAlphaNumerico('motivo');
            if($motivoAnulacion==''){
                $validacion['status']='error';
                $validacion['txt_comentario_anulacion']='error';
                echo json_encode($validacion);
                exit;
            }
            $id = $this->atTransaccionesModelo->metRechazarTransaccionBancaria($idTransaccion, $motivoAnulacion);

        if (is_array($id)) {
            $validacion['status'] = 'errorSQL';
            echo json_encode($validacion);
            exit;
        }
        $validacion['Mensaje'] = array('titulo' => 'RECHAZADA', 'contenido' => 'La Transaccion Bancaria fue Rechazada Satisfactoriamente');
        $validacion['status'] = 'OK';
        $validacion['idTransaccion'] = $id;
        echo json_encode($validacion);
        exit;
    }

        $datoTransaccion = $this->atTransaccionesModelo->metConsultarTransacciones($idTransaccion);
        $this->atVista->assign('transaccionBD', $datoTransaccion);
        $this->atVista->assign('idTransaccion', $idTransaccion);

        if($datoTransaccion['num_flag_obligacion'] == 0){
            $documento = $this->atTransaccionesModelo->atDocumentosTransaccionModelo->metDocumentoListar();
        }else{
            $documento = $this->atTransaccionesModelo->atDocumentosModelo->metDocumentoListar();
        }
        if($estado=='NUEVO') {
            $this->atVista->assign('estado', $estado);
            $this->atVista->assign('empleado', $this->atTransaccionesModelo->metEmpleado());
            $this->atVista->assign('listadoCuentas', $this->atTransaccionesModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->assign('documento',$documento);
            $this->atVista->metRenderizar('nuevo', 'modales');

        }elseif($estado=='MODIFICAR') {
            $this->atVista->assign('estado', $estado);
            $this->atVista->assign('transaccionDetalleBD', $this->atTransaccionesModelo->metConsultarTransaccionesDetalle($idTransaccion));
            $this->atVista->assign('empleado', $this->atTransaccionesModelo->metEmpleado());
            $this->atVista->assign('listadoCuentas', $this->atTransaccionesModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->assign('documento',$documento);
            $this->atVista->metRenderizar('nuevo', 'modales');

        }elseif($estado=='VER' || $estado=='REVISAR' || $estado=='ACTUALIZAR' || $estado=='DESACTUALIZAR') {
            $this->atVista->assign('estado', $estado);
            $ver=1; $this->atVista->assign('ver', $ver);
            $this->atVista->assign('transaccionDetalleBD', $this->atTransaccionesModelo->metConsultarTransaccionesDetalle($idTransaccion));

            $this->atVista->assign('empleado', $this->atTransaccionesModelo->metEmpleado());
            $this->atVista->assign('listadoCuentas', $this->atTransaccionesModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->assign('documento',$documento);
            $this->atVista->metRenderizar('nuevo', 'modales');
        }else{
            $idCuenta = $this->metObtenerInt('idCuenta');
            $this->atVista->assign('dataBD', $this->atTransaccionesModelo->metSaldoBancoDetalle($idCuenta));
            $this->atVista->assign('dataDetalleBD', $this->atTransaccionesModelo->metSaldoBanco($idCuenta));
            $this->atVista->assign('listadoCuentas', $this->atTransaccionesModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->assign('listadoTipoTrans', $this->atTransaccionesModelo->metTransaccionListar());

            $this->atVista->metRenderizar('transaccionesBanco', 'modales');
        }

    }

    public function metListasDocumentoTransaccion(){
        $tipoTransaccion = $this->metObtenerInt('tipoTransaccion');
        $id = $this->atTransaccionesModelo->metListarTipoDocumento($tipoTransaccion);
        if (!$id) {
        }
        echo json_encode($id);
        exit;
    }

    public function metImprimir(){
        $idTransaccion = $_GET['idTransaccion'];
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(1, 10);
        $this->atFPDF->AddPage();

        $transaccion = $this->atTransaccionesModelo->metConsultarTransaccionesImprimir($idTransaccion);

        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CEM.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5,utf8_decode('CONTRALORIA DEL ESTADO MONAGAS'), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCI�N DE ADMINISTRACI�N Y SERVICIOS'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(165, 5); $this->atFPDF->Cell(20, 5, ('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, $transaccion['fec_transaccion'], 0, 1, 'L');
        $this->atFPDF->SetXY(165, 10); $this->atFPDF->Cell(20, 5, utf8_decode('P�gina: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo() . ' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->SetXY(10, 20); $this->atFPDF->Cell(195, 5, utf8_decode('SUSTENTO DE TRANSACCION'), 0, 1, 'C', 0);
        $this->atFPDF->Ln(10);

        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255); $this->atFPDF->SetFillColor(245, 245, 245);
        $this->atFPDF->SetFont('Arial', 'B', 8); $this->atFPDF->Cell(28, 5, utf8_decode('Nro. Transacci�n:'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial', '', 8); $this->atFPDF->Cell(45, 5, utf8_decode($transaccion['ind_num_transaccion']), 0, 0, 'L', 0);
        $this->atFPDF->SetFont('Arial', 'B', 8); $this->atFPDF->Cell(28, 5, utf8_decode('Preparado Por:'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial', '', 8); $this->atFPDF->Cell(94, 5, utf8_decode($transaccion['EMPLEADO_PREPARA']).' '.$transaccion['fec_preparacion'], 0, 0, 'L', 0);
        $this->atFPDF->Ln(6);
        $this->atFPDF->SetFont('Arial', 'B', 8); $this->atFPDF->Cell(28, 5, ('Fecha:'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial', '', 8); $this->atFPDF->Cell(45, 5, $transaccion['fec_transaccion'], 0, 0, 'L', 0);
        $this->atFPDF->SetFont('Arial', 'B', 8); $this->atFPDF->Cell(28, 5, ('Estado:'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial', '', 8); $this->atFPDF->Cell(94, 5, $transaccion['ind_estado'], 0, 0, 'L', 0);
        $this->atFPDF->Ln(6);
        $this->atFPDF->SetFont('Arial', 'B', 8); $this->atFPDF->Cell(28, 5, ('Comentarios:'), 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial', '', 8); $this->atFPDF->MultiCell(167, 5, $transaccion['txt_comentarios'], 0, 'L', 0);
        $this->atFPDF->Ln(5);

        $this->atFPDF->SetDrawColor(0, 0, 0); $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetWidths(array(85, 45, 40, 25));
        $this->atFPDF->SetAligns(array('L', 'C', 'L', 'R'));
        $this->atFPDF->Row(array(utf8_decode('Tipo de Transacci�n'),
            ('Cuenta Bancaria'),
            ('Doc. Referencia'),
            ('Monto')));
        $this->atFPDF->Ln(2);

        $detalles = $this->atTransaccionesModelo->metConsultarTransaccionesDetalle($idTransaccion);
        foreach ($detalles AS $detalle) {
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->Row(array(utf8_decode($detalle['decripcionTransaccion']),
                $detalle['cuentaBancaria'],
                $detalle['ind_num_documento_referencia'],
                number_format($detalle['num_monto'], 2, ',', '.')));
            $this->atFPDF->Ln(2);

        }
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(170, 5, 'Totales:', 0, 0, 'R', 0);
        if ($transaccion['num_monto'] > 0) $this->atFPDF->SetTextColor(0, 0, 0); else $this->atFPDF->SetTextColor(255, 0, 0);
        $this->atFPDF->Cell(25, 5, number_format(abs($transaccion['num_monto']), 2, ',', '.'), 0, 0, 'R', 1);

        //	falta

        //	Muestro los firmantes
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->Rect(10, 10, 70, 0.1, "DF");
        $this->atFPDF->Rect(140, 10, 70, 0.1, "DF");
        $this->atFPDF->SetFont('Arial', '', 7);
        $this->atFPDF->SetXY(10, 220);
        $this->atFPDF->Cell(130, 3, ('Preparado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(80, 3, ('Revisado Por:'), 0, 1, 'L');

        $this->atFPDF->Cell(130, 3, utf8_decode($transaccion['EMPLEADO_PREPARA']), 0, 0, 'L');//nombre de quien Emite
        $this->atFPDF->Cell(80, 3, utf8_decode($transaccion['EMPLEADO_REVISA']), 0, 1, 'L');//nombre de quien revisa

        $this->atFPDF->Cell(130, 3, utf8_decode($transaccion['CARGO_PREPARA']), 0, 0, 'L');//cargo de quien elabora
        $this->atFPDF->Cell(80, 3, utf8_decode($transaccion['CARGO_REVISA']), 0, 1, 'L');//Cargo de quien revisa

        $this->atFPDF->Rect(10, 24, 70, 0.1, "DF");
        $this->atFPDF->Rect(140, 24, 70, 0.1, "DF");

        $this->atFPDF->SetFont('Arial', '', 7);
        $this->atFPDF->SetXY(10, 250);

        $this->atFPDF->Cell(130, 3, ('Conformado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(80, 3, ('Aprobado Por:'), 0, 1, 'L');

        $this->atFPDF->Cell(130, 3, utf8_decode($transaccion['EMPLEADO_CONFORMA']), 0, 0, 'L');//nombre de quien conforma
        $this->atFPDF->Cell(80, 3, utf8_decode($transaccion['EMPLEADO_APRUEBA']), 0, 1, 'L');//nombre de quien aprueba

        $this->atFPDF->Cell(130, 3, utf8_decode($transaccion['CARGO_CONFORMA']), 0, 0, 'L');//cargo de quien conforma
        $this->atFPDF->Cell(80, 3, utf8_decode($transaccion['CARGO_APRUEBA']), 0, 1, 'L');//Cargo de quien aprueba

        //---------------------------------------------------
////	Muestro el contenido del pdf.

        $this->atFPDF->Output();
    }

    public function metVouchers()
    {
        $idTransaccion = $this->metObtenerInt('idTransaccion');
        //Enviar a la vista el Centro de Costo por defecto para los vouchers
        $this->atVista->assign('centroCosto', $this->atTransaccionesModelo->atObligacionModelo->metBuscar('a023_centro_costo','ind_abreviatura',Session::metObtener('CCOSTOVOUCHER')));
        //Enviar a la vista el organismo
        $this->atVista->assign('organismo', $this->atTransaccionesModelo->atObligacionModelo->metOrganismo());
        $this->atVista->assign('libroContable', $this->atTransaccionesModelo->atLibroContableModelo->metListar());

        $transacion = $this->atTransaccionesModelo->metBuscarTransacciones($idTransaccion);
        if(Session::metObtener('CONTABILIDAD') == 'ONCOP') {
            $contab = 'T';
        }else{
            $contab = 'F';
        }
        $contabilidades = $this->atTransaccionesModelo->atObligacionModelo->metBuscar('cb_b005_contabilidades', 'ind_tipo_contabilidad', $contab);

        $datosVoucher = $this->atTransaccionesModelo->atListaVoucherModelo->metMostrar($transacion['fk_cbb001_num_voucher']);
        $datos = $this->atTransaccionesModelo->metDetalleVoucher($transacion['fk_cbb001_num_voucher']);
        $this->atVista->assign('codVoucher',$this->atTransaccionesModelo->atObligacionModelo->metBuscar('cb_c003_tipo_voucher','pk_num_voucher',$transacion['fk_cbc003_tipo_voucher']));

        $debitos=0; $creditos=0;
        foreach ($datos as $i) {
            if($i['num_debe']=='0'){
                $creditos = $creditos+$i['num_haber'];
                $datoArreglo['Debe'][]=array(
                    'pk_num_cuenta' => $i['fk_cbb004_num_cuenta'],
                    'cod_cuenta' => $i['cod_cuenta'],
                    'ind_descripcion' => $i['ind_descripcion'],
                    'MontoVoucher' => $i['num_haber'],
                    'proveedor' => $i['proveedor'],
                    'pk_num_persona' => $i['pk_num_persona'],
                    'fk_a023_num_centro_costo' => $i['fk_a023_num_centro_costo']
                );
            }else{
                $debitos = $debitos+$i['num_debe'];
                $datoArreglo['Haber'][]=array(
                    'pk_num_cuenta' => $i['fk_cbb004_num_cuenta'],
                    'cod_cuenta' => $i['cod_cuenta'],
                    'ind_descripcion' => $i['ind_descripcion'],
                    'MontoVoucher' => $i['num_debe'],
                    'proveedor' => $i['proveedor'],
                    'pk_num_persona' => $i['pk_num_persona'],
                    'fk_a023_num_centro_costo' => $i['fk_a023_num_centro_costo']
                );
            }
        }
        if(!isset($datoArreglo)){
            $datoArreglo=false;
        }
        $this->atVista->assign('datoArreglo',$datoArreglo);
        $this->atVista->assign('creditos', $creditos);
        $this->atVista->assign('debitos', $debitos);
        $this->atVista->assign('transacion', $transacion);
        $this->atVista->assign('datosVoucher', $datosVoucher);
        $this->atVista->assign('idTransaccion', $idTransaccion);
        $this->atVista->assign('contabilidad', $contabilidades['pk_num_contabilidades']);
        $this->atVista->metRenderizar('vouchers', 'modales');
    }


}