<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_Modulo . 'modCP' . DS . 'controladores' . DS . 'trait' . DS . 'funcionesTrait.php';

class pagosControlador extends Controlador
{
    use funcionesTrait;
    private $atPagos;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atPagos = $this->metCargarModelo('pagos');
        $this->metObtenerLibreria('pago', 'modCP');
        $this->metObtenerLibreria('fpdf', 'fpdf-V1.7');
        $this->atFPDF = new pdf('P', 'mm', 'Letter');
        $this->atFPDF2 = new FPDF('L', 'mm', array(177, 80));
        $this->atFPDF3 = new FPDF('P', 'mm', 'Letter');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('dataBD', $this->atPagos->metListarPagos());
        $this->atVista->metRenderizar('listado');
    }

    public function metAccionesPago($idPagoUrl = false)
    {
        $idPago = $this->metObtenerInt('idPago');
        $idVoucherPago = $this->metObtenerInt('idVoucherPago');
        $estado = $this->metObtenerAlphaNumerico('estado');
        if ($estado == 'ANULAR') {
            $motivo = $this->metObtenerAlphaNumerico('motivo');
            if($motivo==''){
                $validacion['status']='error';
                $validacion['ind_motivo_anulacion']='error';
                echo json_encode($validacion);
                exit;
            }
            $datoPago = $this->atPagos->metConsultarPago($idPago);
            $id = $this->atPagos->metAnularPago($idPago, $motivo,$datoPago['fk_cpb014_num_cuenta']);
            $buscar = $this->atPagos->atObligacionModelo->metBuscar('cp_d010_pago','pk_num_pago',$idPago);
            if($buscar['fk_cbb001_num_voucher_pago'] == null){
                $voucher = 'no';
            }else{
                $voucher = '';
            }
            if (is_array($id)) {
                $validacion['status'] = 'errorSQL';
                echo json_encode($id);
                exit;
            }
            $validacion['Mensaje'] = array('titulo' => 'ANULADO', 'contenido' => 'El Pago fue Anulado Satisfactoriamente');
            $validacion['status'] = 'OK';
            $validacion['idPago'] = $id;
            $validacion['voucher'] = $voucher;
            $validacion['ind_motivo_anulacion'] = $buscar['ind_motivo_anulacion'];
            echo json_encode($validacion);
            exit;
        } elseif ($estado == 'IM' || $idPagoUrl) {
            $this->atVista->assign('idPago', $idPago);
            $this->atVista->metRenderizar('imprimirSustento', 'modales');
            exit;
        }
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('pagoBD', $this->atPagos->metConsultarPago($idPago));
        $this->atVista->assign('idPago', $idPago);

        if ($estado == 'VER' || $estado == 'AN') {
            $this->atVista->assign('estado', $estado);
            $this->atVista->assign('listadoTipoPago', $this->atPagos->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
            $this->atVista->assign('listadoCuentas', $this->atPagos->atCuentasModelo->metCuentaListar());
            $this->atVista->metRenderizar('nuevo', 'modales');

        }elseif($estado=='VOUCHER'){
            $this->atVista->assign('estado', $estado);
            $this->metVoucher('verVocuhers');
        }
    }

    public function metAnularPago()
    {
        $id = $this->atPagos->metAnularPago(7, 'prueba');

    }

    public function metFormatoCheque($pago)
    {
        if ($pago['tipo_pago'] == 'CHEQUE') {
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', 'B', 10);
            $this->atFPDF->SetXY(138, 7);
            $this->atFPDF->Cell(35, 4, number_format($pago['pago'], 2, ',', '.') . '*****', 0, 0, 'L');
            $this->atFPDF->SetXY(33, 24);
            $this->atFPDF->Cell(160, 4, utf8_decode($pago['fk_a003_num_persona_proveedor_a_pagar']), 0, 0, 'L');
            $this->atFPDF->SetXY(14, 30);
            $this->atFPDF->MultiCell(160, 4, '                       ' . $this->numtoletras(round($pago['pago'], 2)) . ' **********', 0, 'L');
            $this->atFPDF->SetXY(14, 42);
            $this->atFPDF->Cell(160, 4, utf8_decode('Cumaná, ') . strftime("%d de %B                                %Y", strtotime($pago['fechaPago'])), 0, 0, 'L');
        } else {
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->Rect(9, 5, 196, 40, "D");
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(60, 5, $pago['a006_miscelaneo_detalle_banco'], 0, 0, 'L');
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(16, 5, 'Nro. Pago', 0, 0, 'L');
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->Cell(100, 5, $pago['ind_cheque_usuario'], 0, 0, 'L');
            $this->atFPDF->Ln(5);
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(27, 5, 'Pagar a la orden de ', 0, 0, 'L');
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->Cell(160, 5, utf8_decode($pago['fk_a003_num_persona_proveedor_a_pagar']), 0, 0, 'L');
            $this->atFPDF->Ln(5);
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(17, 5, 'La suma de ', 0, 0, 'L');
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->MultiCell(160, 5, $this->numtoletras(round($pago['pago'], 2)), 0, 'L');
            $this->atFPDF->Ln(5);
            $this->atFPDF->Ln(5);
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(15, 5, 'Proceso ', 0, 0, 'L');
            $this->atFPDF->Cell(25, 5, $pago['ind_num_pago'], 0, 0, 'R');
            $this->atFPDF->Cell(5, 5, 'Secuencia', 0, 0, 'L');#modificar aqui
            $this->atFPDF->Ln(5);
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(15, 5, 'Cta. Cte. ', 0, 0, 'L');
            $this->atFPDF->Cell(25, 5, $pago['ind_num_cuenta'], 0, 0, 'L');
            $this->atFPDF->Ln(10);
        }
    }

    public function metImprimirSustentoPago($idPago)
    {
        $pago = $this->atPagos->metConsultarPago($idPago);
        //valido la el tipo de contabilidad utilizada para de acuerdo a eso mostrar el sustento.
        if($codImpuestoIva=Session::metObtener('CONTABILIDAD') == 'ONCOP'){
            $retencion = $this->atPagos->atObligacionModelo->metRetenciones($pago['pk_num_obligacion'],'P');
            $vouchers = $this->atPagos->metArmarPagoContabilidad($pago['pk_num_obligacion'],$pago['num_flag_provision'],$retencion['montoRetencion']);
        }else{
            $datosOblig = $this->atPagos->atObligacionModelo->metConsultaObligacion($pago['pk_num_obligacion']);

            //para obligaciones desde nomina
            $tipoProceso = $this->atPagos->atObligacionModelo->metBuscar('nm_b003_tipo_proceso','cod_proceso',substr($datosOblig['ind_nro_factura'], 2, 2),true);
            if(isset($tipoProceso['pk_num_tipo_proceso'])){
                $montoImpuesto1 = $this->atPagos->atObligacionModelo->metRetencionesNomina($pago['pk_num_obligacion'],$tipoProceso['pk_num_tipo_proceso']);
                $montoImpuesto2 = NULL;
            }else{
                $montoImpuesto1 = $this->atPagos->atObligacionModelo->metRetenciones($pago['pk_num_obligacion'],'N');
                if($datosOblig['num_flag_provision'] == 0){
                    $montoImpuesto2 = $this->atPagos->metImpuesto($pago['pk_num_obligacion']);
                }else{
                    $montoImpuesto2 = 0;
                }
                $montoImpuesto2 = $this->atPagos->atObligacionModelo->metBuscarMontoImpuesto($pago['pk_num_obligacion']);

            }
            $vouchers = $this->atPagos->metPago($pago['pk_num_obligacion'],$pago['num_flag_provision'], $montoImpuesto1['montoRetencion'], $montoImpuesto2['Monto']);
        }
        $usuario = Session::metObtener('nombreUsuario');
        $cargoUsuario = Session::metObtener('cargoEmpleado');
        $revisa = $this->atPagos->metBuscarRevisor(Session::metObtener('firmanteSustentoPago'));
        define('PAGUESE',$usuario);
        define('CARGO_USUARIO',$cargoUsuario);
        define('NOMBRE_REVISA',$revisa['nombreRevisor']);
        define('CARGO_REVISA',$revisa['ind_descripcion_cargo']);
        define('CARGO_CONFORMA',$pago['CARGO_CONFORMA']);
        define('CARGO_APRUEBA',$pago['CARGO_APRUEBA']);

        $this->atFPDF->metArrayEmitido('', 'Nombre');
        $this->atFPDF->metArrayRevisa('', 'Nombre');
        $this->atFPDF->metArrayConforma($pago['EMPLEADO_CONFORMA'], 'Nombre');
        $this->atFPDF->metArrayAprueba($pago['EMPLEADO_APRUEBA'], 'Nombre');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();
        $this->metFormatoCheque($pago);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetXY(10, 100);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(20, 5, utf8_decode('Beneficiario: '), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(175, 5, utf8_decode($pago['fk_a003_num_persona_proveedor_a_pagar']), 0, 0, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(20, 5, utf8_decode('Nro. Cuenta: '), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(105, 5, $pago['ind_num_cuenta'], 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(20, 5, utf8_decode('Nro. Orden de Pago: '), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(50, 5, '            ' . $pago['ind_num_orden'], 0, 0, 'L');
        $this->atFPDF->Ln(5);
        if ($pago['tipo_pago'] == 'CHEQUE') {
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(20, 5, utf8_decode('Nro. Pago: '), 0, 0, 'L');
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->Cell(50, 5, $pago['ind_cheque_usuario'], 0, 0, 'L');
            $this->atFPDF->Ln(5);
        }
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(20, 5, utf8_decode('Voucher: '), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(105, 5, $pago['ind_voucher'], 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(50, 5, date_format(date_create($pago['fechaPago']), 'd-m-Y'), 0, 0, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(20, 5, utf8_decode('Banco: '), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(105, 5, $pago['a006_miscelaneo_detalle_banco'], 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(20, 5, utf8_decode('Monto: '), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(50, 5, $pago['num_monto_pago'], 0, 0, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(20, 5, utf8_decode('Descripción: '), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->MultiCell(175, 5, utf8_decode($pago['ind_comentarios']), 0, 'L');
        if ($pago['estadoPago'] == 'AN') {
            $this->atFPDF->Ln(1);
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(26, 5, utf8_decode('Motivo Anulación: '), 0, 0, 'L');
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->MultiCell(169, 5, utf8_decode($pago['motivoAnulacionPago']), 0, 'L');
        }
        $this->atFPDF->Ln(3);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(30, 15, 15, 85, 25, 25));
        $this->atFPDF->SetAligns(array('L', 'C', 'C', 'L', 'R', 'R'));
        $this->atFPDF->Row(array('Cuenta',
            'Persona',
            'C.C',
            utf8_decode('Descripción'),
            'Debe',
            'Haber'));
        $this->atFPDF->Ln(1);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 8);
        $haber = 0.00;
        $debe = 0.00;
        $total_debe = 0.00;
        $total_haber = 0.00;
        foreach ($vouchers AS $i) {
            if ($i['columna'] == 'Debe') {
                $debe = $i['MontoVoucher'];
                $haber = 0.00;
            } else {
                $haber = $i['MontoVoucher'];
                $debe = 0.00;
            }
            $total_debe += $debe;
            $total_haber += $haber;
            $this->atFPDF->Row(array($i['cod_cuenta'],
                '',#CODPERSONA
                '',#CENTROCOSTO
                mb_strtoupper($i['ind_descripcion'], 'utf-8'),
                number_format($debe, 2, ',', '.'),
                number_format($haber, 2, ',', '.')));
        }
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->Rect(10, $y, 195, 0.1, "FD");
        $this->atFPDF->SetY($y + 2);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Row(array('',
            '',
            '',
            '',
            number_format($total_debe, 2, ',', '.'),
            number_format($total_haber, 2, ',', '.')));


        $this->atFPDF->Output();
    }

    public function metImprimirChequePago($idPago)
    {
        $pago = $this->atPagos->metConsultarPago($idPago);
        if ($pago['tipo_pago'] == 'CHEQUE') {
            $this->atFPDF2->AliasNbPages();
            $this->atFPDF2->SetMargins(10, 5, 10);
            $this->atFPDF2->SetAutoPageBreak(5, 1);
            $this->atFPDF2->AddPage();
            $monto_letras = $this->numtoletras(round($pago['pago'], 2));
            $fecha = utf8_decode('Cumaná, ') . strftime("%d de %B %Y", strtotime($pago['fechaPago']));
            $monto = $pago['num_monto_pago'];
            $proveedor = $pago['fk_a003_num_persona_proveedor_a_pagar'];
            $this->atFPDF2->SetTextColor(0, 0, 0);
            $this->atFPDF2->SetDrawColor(0, 0, 0);
            $this->atFPDF2->SetFillColor(255, 255, 255);
            $this->atFPDF2->SetFont('Arial', 'B', 8);
//VENEZUELA
            if ($pago['a006_miscelaneo_detalle_banco'] == 'BANCO DE VENEZUELA') {
//	----------------
                $this->atFPDF2->SetXY(138, 6.5);
                $this->atFPDF2->Cell(35, 4, $monto . '*****', 0, 0, 'L');
                $this->atFPDF2->SetXY(33, 24);
                $this->atFPDF2->Cell(160, 4, utf8_decode($proveedor), 0, 0, 'L');
                $this->atFPDF2->SetXY(14, 30);
                $this->atFPDF2->MultiCell(160, 4, '                       ' . $monto_letras . ' **********', 0, 'L');
                $this->atFPDF2->SetXY(14, 42);
                $this->atFPDF2->Cell(160, 4, $fecha, 0, 0, 'L');
            }
//BANESCO
            if ($pago['a006_miscelaneo_detalle_banco'] == 'BANCO BANESCO') //0003
            {
                $this->atFPDF2->SetXY(138, 6.5);
                $this->atFPDF2->Cell(35, 4, $monto . '*****', 0, 0, 'L');
                $this->atFPDF2->SetXY(29, 22);
                $this->atFPDF2->Cell(160, 4, utf8_decode($proveedor), 0, 0, 'L');
                $this->atFPDF2->SetXY(11, 28);
                $this->atFPDF2->MultiCell(160, 4, '                       ' . $monto_letras . ' **********', 0, 'L');
                $this->atFPDF2->SetXY(13, 40);
                $this->atFPDF2->Cell(160, 4, $fecha, 0, 0, 'L');
            }
//ACTIVO
            if ($pago['a006_miscelaneo_detalle_banco'] == 'BANCO ACTIVO') //0004
            {
                $this->atFPDF2->SetXY(138, 9.5);
                $this->atFPDF2->Cell(35, 4, $monto . '*****', 0, 0, 'L');
                $this->atFPDF2->SetXY(29, 25);
                $this->atFPDF2->Cell(160, 4, utf8_decode($proveedor), 0, 0, 'L');
                $this->atFPDF2->SetXY(11, 31);
                $this->atFPDF2->MultiCell(160, 4, '                       ' . $monto_letras . ' **********', 0, 'L');
                $this->atFPDF2->SetXY(13, 43);
                $this->atFPDF2->Cell(160, 4, $fecha, 0, 0, 'L');
            }
            $this->atFPDF2->Output();
        }
    }

    public function metFormatoRetencion($pagoRetenciones)
    {
        foreach ($pagoRetenciones AS $i) {
            $this->atFPDF3->AddPage();
            $this->atFPDF3->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 8, 5, 12, 10);
            $this->atFPDF3->SetFont('Arial', '', 10);
            $this->atFPDF3->SetXY(20, 5);
            $this->atFPDF3->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
            $this->atFPDF3->SetXY(20, 9);
            $this->atFPDF3->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN Y PRESUPUESTO'), 0, 0, 'L');
            $this->atFPDF3->SetXY(20, 13);
            $this->atFPDF3->Cell(100, 5, APP_ORGANISMO_RIF, 0, 0, 'L');
            $this->atFPDF3->Ln(15);
            $this->atFPDF3->SetFont('Arial', 'B', 14);
            if ($i['ind_tipo_comprobante'] == 'IVA') {
                $articulo = 'I.V.A.';
            } elseif ($i['ind_tipo_comprobante'] == 'ISLR') {
                $articulo = 'I.S.L.R.';
            } elseif ($i['ind_tipo_comprobante'] == '1X1000') {
                $articulo = '1X1000';
            }elseif ($i['ind_tipo_comprobante'] == 'DESCCS') {
                $articulo = 'Compromiso de Responsabilidad Social';
            }
	    $fc = new DateTime($i['fec_comprobante']);
	    $i['fec_comprobante'] = $fc->format('d-m-Y');
            $this->atFPDF3->Cell(195, 5, utf8_decode('Comprobante de Retención '.$articulo), 0, 1, 'C');
            $this->atFPDF3->Cell(195, 5, 'Nro. ' . $i['ind_periodo_fiscal'] . $i['ind_num_comprobante'], 0, 0, 'C');#numComprobante
            $this->atFPDF3->Ln(20);
            $this->atFPDF3->SetFont('Arial', '', 10);
            //$this->atFPDF3->Cell(70, 5, utf8_decode('Número de Comprobante: '), 0, 0, 'L');
            $this->atFPDF3->Cell(60, 5, ('Fecha: '), 0, 0, 'L');
            $this->atFPDF3->Cell(70, 5, ('Periodo Fiscal: '), 0, 0, 'L');
            $this->atFPDF3->Cell(70, 5, utf8_decode('Número de Orden de Pago: '), 0, 0, 'L');
            $this->atFPDF3->Ln(6);
            $this->atFPDF3->SetFont('Arial', 'B', 10);
            //$this->atFPDF3->Cell(70, 5, $nrocomprobante, 0, 0, 'L');
            $this->atFPDF3->Cell(60, 5, $i['fec_comprobante'], 0, 0, 'L');#fecComprobante
            $this->atFPDF3->Cell(70, 5, $i['ind_periodo_fiscal'], 0, 0, 'L');#periodoFiscal
            $this->atFPDF3->Cell(70, 5, $i['ind_num_orden'], 0, 0, 'L');#numOrdenPago
            $this->atFPDF3->Ln(10);
            $this->atFPDF3->SetFont('Arial', '', 10);
            $this->atFPDF3->Cell(130, 5, utf8_decode('Nombre o Razón Social Agente de Retención: '), 0, 0, 'L');
            $this->atFPDF3->Cell(80, 5, utf8_decode('R.I.F. del Agente de Retención: '), 0, 0, 'L');
            $this->atFPDF3->Ln(6);
            $this->atFPDF3->SetFont('Arial', 'B', 10);
            $this->atFPDF3->Cell(130, 5, utf8_decode(APP_ORGANISMO), 0, 0, 'L');#nombreContraloria
            $this->atFPDF3->Cell(80, 5, APP_ORGANISMO_RIF, 0, 0, 'L');#rifContraloria
            $this->atFPDF3->Ln(10);
            $this->atFPDF3->SetFont('Arial', '', 10);
            $this->atFPDF3->Cell(190, 5, utf8_decode('Dirección Fiscal del Agente de Retención: '), 0, 0, 'L');
            $this->atFPDF3->Ln(6);
            $this->atFPDF3->SetFont('Arial', 'B', 10);
            $this->atFPDF3->MultiCell(195, 4, utf8_decode(APP_ORGANISMO_DIRECCION), 0, 'L');#Direccion Del La Contraloria
            $this->atFPDF3->Ln(4);
            $this->atFPDF3->SetFont('Arial', '', 10);
            $this->atFPDF3->Cell(130, 5, utf8_decode('Nombre o Razón Social del Sujeto Retenido: '), 0, 0, 'L');
            $this->atFPDF3->Cell(80, 5, ('R.I.F. del Agente del Sujeto Retenido: '), 0, 0, 'L');
            $this->atFPDF3->Ln(6);
            $this->atFPDF3->SetFont('Arial', 'B', 10);
            $this->atFPDF3->Cell(130, 5, utf8_decode($i['ind_nombre1'] . ' ' . $i['ind_nombre2'] . ' ' . $i['ind_apellido1'] . ' ' . $i['ind_apellido2']), 0, 0, 'L');#nombreProveedor
            $this->atFPDF3->Cell(80, 5, utf8_decode($i['ind_documento_fiscal']), 0, 0, 'L');#rifProveedor
            $this->atFPDF3->Ln(15);
            //---------------------------------------------------
            //	imprimo el concepto
            $this->atFPDF3->SetFont('Arial', '', 10);
            $this->atFPDF3->Cell(195, 5, ('Concepto: '), 0, 0, 'L');
            $this->atFPDF3->Ln(6);
            $this->atFPDF3->SetFont('Arial', 'B', 10);
            $this->atFPDF3->SetX(15);
            $this->atFPDF3->MultiCell(195, 6, utf8_decode($i['ind_comentarios']), 0, 'J');#concepto(Descripcion)
            $this->atFPDF3->Ln(15);
            //---------------------------------------------------
            //	imprimo el articulo
            if ($i['ind_tipo_comprobante'] == 'IVA') {
                $articulo = "(Ley IVA Art. 11. Serán responsables del Impuesto de Agentes de Retención, los Compradores o adquirientes de determinados muebles y los receptores de ciertos servicios, a quienes la Administración Tributaria designe como tal).";
            } elseif ($i['ind_tipo_comprobante'] == 'ISLR') {
                $articulo = "(Menos  del  2,00%  del  Impuesto  sobre  la  Renta,  correspondiente  al  mismo  mes,  según  Artículo  Nº  9,  Numeral  11, del  Decreto  1.808  de  Retención  del  I.S.L.R.  del  23  de  Abril  de  1997,  según  recibo  Número  00-000105  de  fecha 22-02-2016).";
            } elseif ($i['ind_tipo_comprobante'] == '1X1000') {
                $articulo = " ";
            }elseif ($i['ind_tipo_comprobante'] == 'DESCCS') {
                $articulo = "(Retención del Compromiso de Responsabilidad Social relacionada en el artículo 31 del decreto con Rango Valor y Fuerza de Ley de Contrataciones Públicas)";
            }
            $this->atFPDF3->SetFont('Arial', 'B', 10);
            $this->atFPDF3->MultiCell(195, 6, utf8_decode($articulo), 0, 'J');
            $this->atFPDF3->Ln(15);
            //---------------------------------------------------
            //	imprimo los montos
            $this->atFPDF3->SetTextColor(0, 0, 0);
            $this->atFPDF3->SetDrawColor(255, 255, 255);
            $this->atFPDF3->SetFillColor(255, 255, 255);
	    $date = new DateTime($i['fec_factura']);
	    $i['fec_factura'] = $date->format('d-m-Y');
            if ($i['ind_tipo_comprobante'] == 'IVA') {
                $this->atFPDF3->SetFont('Arial', 'B', 8);
                $this->atFPDF3->SetWidths(array(20, 25, 25, 21, 21, 19, 21, 21, 21));
                $this->atFPDF3->SetAligns(array('C', 'C', 'C', 'R', 'R', 'R', 'R', 'R', 'R'));
                $this->atFPDF3->Row(array(
                    'Fecha Fac.',
                    'Nro. Factura',
                    'Nro. Control',
                    'Imponible',
                    'Exento',
                    'IVA',
                    'Total Fac.',
                    '% Retenido',
                    'IVA Retenido'));
                $this->atFPDF3->Ln(1);
                $this->atFPDF3->SetDrawColor(255, 255, 255);
                $this->atFPDF3->SetFont('Arial', '', 9);
                $this->atFPDF3->Row(array(
                    $i['fec_factura'],#fecFactura
                    $i['ind_nro_factura'],#numFactura
                    $i['ind_nro_control'],#numDocumento
                    number_format($i['num_monto_afecto'], 2, ',', '.'), #montoAfectoCuenta
                    number_format($i['num_monto_no_afecto'], 2, ',', '.'), #montoNoAfectoCuenta
                    number_format($i['num_monto_impuesto'], 2, ',', '.'), #montoIva
                    number_format($i['num_monto_afecto']+$i['num_monto_no_afecto']+$i['num_monto_impuesto'], 2, ',', '.'), #montoTotal
                    number_format($i['num_factor_porcentaje'], 2, ',', '.'),#porcentaje
                    number_format($i['montoRetenido'], 2, ',', '.') #montoRetenido
                ));
            } elseif ($i['ind_tipo_comprobante'] == 'ISLR') {
                $this->atFPDF3->SetFont('Arial', 'B', 8);
                $this->atFPDF3->SetWidths(array(20, 25, 25, 21, 21, 19, 21, 21, 21));
                $this->atFPDF3->SetAligns(array('C', 'C', 'C', 'R', 'R', 'R', 'R', 'R', 'R'));
                $this->atFPDF3->Row(array(
                    'Fecha Fac.',
                    'Nro. Factura',
                    'Nro. Control',
                    'Base Imponible',
                    'Exento',
                    'IVA',
                    'Total Fac.',
                    'Tarifa Aplicable (%)',
                    'I.S.L.R. Retenido'));
                $this->atFPDF3->Ln(1);
                $this->atFPDF3->SetDrawColor(255, 255, 255);
                $this->atFPDF3->SetFont('Arial', '', 9);
                $this->atFPDF3->Row(array(
                    $i['fec_factura'],#fecFactura
                    $i['ind_nro_factura'],#numFactura
                    $i['ind_nro_control'],#numDocumento
                    number_format($i['num_monto_afecto'], 2, ',', '.'), #montoAfectoCuenta
                    number_format($i['num_monto_no_afecto'], 2, ',', '.'), #montoNoAfectoCuenta
                    number_format($i['num_monto_impuesto'], 2, ',', '.'), #montoIva
                    number_format($i['num_monto_afecto']+$i['num_monto_no_afecto']+$i['num_monto_impuesto'], 2, ',', '.'), #montoTotal
                    number_format($i['num_factor_porcentaje'], 2, ',', '.'),#porcentaje
                    number_format($i['montoRetenido'], 2, ',', '.') #montoRetenido
                ));
            } elseif ($i['ind_tipo_comprobante'] == '1X1000') {
                $this->atFPDF3->SetFont('Arial', 'B', 8);
                $this->atFPDF3->SetWidths(array(20, 25, 25, 21, 21, 19, 21, 21, 21));
                $this->atFPDF3->SetAligns(array('C', 'C', 'C', 'R', 'R', 'R', 'R', 'R', 'R'));
                $this->atFPDF3->Row(array(
                    'Fecha Fac.',
                    'Nro. Factura',
                    'Nro. Control',
                    'Base Imponible',
                    'Exento',
                    'IVA',
                    'Total Fac.',
                    'Tarifa Aplicable (%)',
                    'Total a Enterar'
                ));
                $this->atFPDF3->Ln(1);
                $this->atFPDF3->SetDrawColor(255, 255, 255);
                $this->atFPDF3->SetFont('Arial', '', 9);
                $this->atFPDF3->Row(array(
                    $i['fec_factura'],#fecFactura
                    $i['ind_nro_factura'],#numFactura
                    $i['ind_nro_control'],#numDocumento
                    number_format($i['num_monto_afecto'], 2, ',', '.'), #montoAfectoCuenta
                    number_format($i['num_monto_no_afecto'], 2, ',', '.'), #montoNoAfectoCuenta
                    number_format($i['num_monto_impuesto'], 2, ',', '.'), #montoIva
                    number_format($i['num_monto_afecto']+$i['num_monto_no_afecto']+$i['num_monto_impuesto'], 2, ',', '.'), #montoTotal
                    number_format($i['num_factor_porcentaje'], 2, ',', '.'),#porcentaje
                    number_format($i['montoRetenido'], 2, ',', '.') #montoRetenido
                ));
            }elseif ($i['ind_tipo_comprobante'] == 'DESCCS') {
                $this->atFPDF3->SetFont('Arial', 'B', 8);
                $this->atFPDF3->SetWidths(array(20, 25, 25, 21, 21, 19, 21, 21, 21));
                $this->atFPDF3->SetAligns(array('C', 'C', 'C', 'R', 'R', 'R', 'R', 'R', 'R'));
                $this->atFPDF3->Row(array(
                    'Fecha Fac.',
                    'Nro. Factura',
                    'Nro. Control',
                    'Base Imponible',
                    'Exento',
                    'IVA',
                    'Total Fac.',
                    'Tarifa Aplicable (%)',
                    'Total a Enterar'
                ));
                $this->atFPDF3->Ln(1);
                $this->atFPDF3->SetDrawColor(255, 255, 255);
                $this->atFPDF3->SetFont('Arial', '', 9);
                $this->atFPDF3->Row(array(
                    $i['fec_factura'],#fecFactura
                    $i['ind_nro_factura'],#numFactura
                    $i['ind_nro_control'],#numDocumento
                    number_format($i['num_monto_afecto'], 2, ',', '.'), #montoAfectoCuenta
                    number_format($i['num_monto_no_afecto'], 2, ',', '.'), #montoNoAfectoCuenta
                    number_format($i['num_monto_impuesto'], 2, ',', '.'), #montoIva
                    number_format($i['num_monto_afecto']+$i['num_monto_no_afecto']+$i['num_monto_impuesto'], 2, ',', '.'), #montoTotal
                    number_format($i['num_factor_porcentaje'], 2, ',', '.'),#porcentaje
                    number_format($i['montoRetenido'], 2, ',', '.') #montoRetenido
                ));
            }
##

            $this->atFPDF3->SetTextColor(0, 0, 0);
            $this->atFPDF3->SetDrawColor(0, 0, 0);
            $this->atFPDF3->SetFillColor(255, 255, 255);
            $this->atFPDF3->SetXY(10, 255);
            $this->atFPDF3->Rect(10, 255, 65, 0.1, "D");
            $this->atFPDF3->Rect(125, 255, 65, 0.1, "D");
            $this->atFPDF3->SetFont('Arial', 'B', 10);
            $this->atFPDF3->Cell(65, 5, 'BENEFICIARIO', 0, 0, 'C');
            $this->atFPDF3->Cell(50, 5);
            $this->atFPDF3->Cell(65, 5, 'CONTRALORIA', 0, 0, 'C');

        }
    }

    public function metImprimirRetencionesPago($idPago)
    {
        $pago = $this->atPagos->metConsultarPago($idPago);
        $pagoRetencion = $this->atPagos->metBuscarRetencionesReporte($idPago);
        $this->atFPDF3->AliasNbPages();
        $this->atFPDF3->SetMargins(10, 5, 10);
        $this->atFPDF3->SetAutoPageBreak(5, 1);
        $this->metFormatoRetencion($pagoRetencion);

        $this->atFPDF3->Output();
    }

    public function metVoucher($opcion = false)
    {
        if($this->metObtenerAlphaNumerico('tipo')){
            $tipo = $this->metObtenerAlphaNumerico('tipo');
        }else{
            $tipo = false;
        }
        if($this->metObtenerAlphaNumerico('tipoVoucher')){
            $tipoVoucher = $this->metObtenerAlphaNumerico('tipoVoucher');
        }else{
            $tipoVoucher = false;
        }
        $idPago = $this->metObtenerInt('idPago');

        $idOrdenPago = $this->metObtenerInt('idOrdenPago');
        //Enviar a la vista el Centro de Costo por defecto para los vouchers
        $this->atVista->assign('centroCosto', $this->atPagos->atObligacionModelo->metBuscar('a023_centro_costo','ind_abreviatura',Session::metObtener('CCOSTOVOUCHER')));
        //Enviar a la vista el organismo
        $this->atVista->assign('organismo', $this->atPagos->atObligacionModelo->metOrganismo());
        $this->atVista->assign('libroContable', $this->atPagos->atObligacionModelo->atLibroContableModelo->metListar());
        $this->atVista->assign('voucher', $this->atPagos->atObligacionModelo->atVoucherModelo->metListar());

        $datosVoucher = $this->atPagos->atListaVoucherModelo->metMostrar($this->metObtenerInt('idVoucherPago'));
        $datos = $this->atPagos->metDetalleVoucher($this->metObtenerInt('idVoucherPago'),$tipo);
        $this->atVista->assign('codVoucher',$this->atPagos->atObligacionModelo->metBuscar('cb_c003_tipo_voucher','pk_num_voucher',$datosVoucher['fk_cbc003_num_voucher']));

        $debitos=0; $creditos=0;
        if($opcion == 'verVocuhers'){
            foreach ($datos as $i) {
                if($i['num_haber']=='0'){
                    $debitos = $debitos+$i['num_debe'];

                    $datoArreglo['Debe'][]=array(
                        'pk_num_cuenta' => $i['fk_cbb004_num_cuenta'],
                        'cod_cuenta' => $i['cod_cuenta'],
                        'ind_descripcion' => $i['ind_descripcion'],
                        'MontoVoucher' => $i['num_debe'],
                        'proveedor' => $i['proveedor'],
                        'pk_num_persona' => $i['pk_num_persona'],
                        'fk_a023_num_centro_costo' => $i['fk_a023_num_centro_costo']
                    );
                }else{
                    $creditos = $creditos+$i['num_haber'];
                    $datoArreglo['Haber'][]=array(
                        'pk_num_cuenta' => $i['fk_cbb004_num_cuenta'],
                        'cod_cuenta' => $i['cod_cuenta'],
                        'ind_descripcion' => $i['ind_descripcion'],
                        'MontoVoucher' => $i['num_haber'],
                        'proveedor' => $i['proveedor'],
                        'pk_num_persona' => $i['pk_num_persona'],
                        'fk_a023_num_centro_costo' => $i['fk_a023_num_centro_costo']
                    );
                }
            }
        }else{
            if(Session::metObtener('CONTABILIDAD') == 'ONCOP'){
                $contab = 'T';
            }else{
                $contab = 'F';
            }
            foreach ($datos as $i) {
                if($i['num_debe']=='0'){
                    $creditos = $creditos+$i['num_haber'];
                    $datoArreglo['Debe'][]=array(
                        'pk_num_cuenta' => $i['fk_cbb004_num_cuenta'],
                        'cod_cuenta' => $i['cod_cuenta'],
                        'ind_descripcion' => $i['ind_descripcion'],
                        'MontoVoucher' => $i['num_haber'],
                        'proveedor' => $i['proveedor'],
                        'pk_num_persona' => $i['pk_num_persona'],
                        'fk_a023_num_centro_costo' => $i['fk_a023_num_centro_costo']
                    );
                }else{
                    $debitos = $debitos+$i['num_debe'];
                    $datoArreglo['Haber'][]=array(
                        'pk_num_cuenta' => $i['fk_cbb004_num_cuenta'],
                        'cod_cuenta' => $i['cod_cuenta'],
                        'ind_descripcion' => $i['ind_descripcion'],
                        'MontoVoucher' => $i['num_debe'],
                        'proveedor' => $i['proveedor'],
                        'pk_num_persona' => $i['pk_num_persona'],
                        'fk_a023_num_centro_costo' => $i['fk_a023_num_centro_costo']
                    );
                }
            }
            $contabilidades = $this->atPagos->atObligacionModelo->metBuscar('cb_b005_contabilidades','ind_tipo_contabilidad',$contab);
            $this->atVista->assign('contabilidad', $contabilidades['pk_num_contabilidades']);
        }
        if(!isset($datoArreglo)){
            $datoArreglo=false;
        }
        $this->atVista->assign('tipo',$tipo);
        $this->atVista->assign('tipoVoucher',$tipoVoucher);
        $this->atVista->assign('idPago',$idPago);
        $this->atVista->assign('idOrdenPago',$idOrdenPago);
        $this->atVista->assign('datoArreglo',$datoArreglo);
        $this->atVista->assign('datosVoucher', $datosVoucher);
        $this->atVista->metRenderizar('vouchers', 'modales');


    }
}
