<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once ROOT.'librerias'.DS.'Number.php';
require_once RUTA_Modulo.'modCP'.DS.'controladores'.DS.'trait'.DS.'funcionesTrait.php';
class provisionObligacionesControlador extends Controlador
{
    use funcionesTrait;
    private $atProvisionObligacionesModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atProvisionObligacionesModelo = $this->metCargarModelo('generacionVouchers');

    }

    public function metIndex()
    {
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $complementosJs = array(
            'materialSiace/core/demo/DemoTableDynamic',
        );
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($complementosJs);

        $this->atVista->assign('dataBD', $this->atProvisionObligacionesModelo->metListarObligaciones());
        $this->atVista->metRenderizar('listado');

    }

    public function metGenerarVouchers(){

        $idObligacion = $this->metObtenerInt('idObligacion');
        $idPago = $this->metObtenerInt('idPago');
        //Enviar a la vista el Centro de Costo por defecto para los vouchers
        $this->atVista->assign('centroCosto', $this->atProvisionObligacionesModelo->metBuscar('a023_centro_costo','ind_abreviatura',Session::metObtener('CCOSTOVOUCHER')));
        //Enviar a la vista el organismo
        $this->atVista->assign('organismo', $this->atProvisionObligacionesModelo->atObligacionModelo->metOrganismo());

        $this->atVista->assign('libroContable', $this->atProvisionObligacionesModelo->atLibroContableModelo->metListar());
        $this->atVista->assign('voucher', $this->atProvisionObligacionesModelo->atVoucherModelo->metListar());

        $datosOblig = $this->atProvisionObligacionesModelo->atObligacionModelo->metConsultaObligacion($idObligacion);
        //para obligaciones desde nomina
        $tipoProceso = $this->atProvisionObligacionesModelo->atObligacionModelo->metBuscar('nm_b003_tipo_proceso', 'cod_proceso', substr($datosOblig['ind_nro_factura'], 2, 2), true);
        $montoImpuesto = $this->atProvisionObligacionesModelo->atObligacionModelo->metBuscarMontoImpuesto($idObligacion);
        //$datos = $this->atProvisionObligacionesModelo->metArmarPagoContabilidad($idObligacion,$datosOblig['num_flag_provision']);
        $codVoucher = $datosOblig['fk_cbc003_num_tipo_voucher'];
        if(Session::metObtener('CONTABILIDAD') == 'ONCOP') {
            $contab = 'T';
        }else{
            $contab = 'F';
        }
        $contabilidades = $this->atProvisionObligacionesModelo->atObligacionModelo->metBuscar('cb_b005_contabilidades', 'ind_tipo_contabilidad', $contab);

        $this->atVista->assign('datosOblig', $this->atProvisionObligacionesModelo->atObligacionModelo->metConsultaObligacion($idObligacion));
        $documento = $this->atProvisionObligacionesModelo->meConsultarDatosObligacion($idObligacion);
        $datos =  $this->atProvisionObligacionesModelo->metArmarOrdenPagoContabilidadFinanciera($idObligacion, $tipoProceso['pk_num_tipo_proceso']);
        if($tipoProceso['pk_num_tipo_proceso']){
            $tipoProvision = '';
        }else{
            $tipoProvision = 'P';
        }

        $debitos=0; $creditos=0;
        foreach ($datos as $i) {
                if ($i['orden'] == '01') {
                    $retencion = $this->atProvisionObligacionesModelo->atObligacionModelo->metRetenciones($idObligacion, $tipoProvision);
                    $montoVoucher = ($i['MontoVoucher'] + $retencion['montoRetencion']);
                } else {
                    $montoVoucher = $i['MontoVoucher'];
                }
                if ($i['columna'] == 'Debe') {
                    $debitos = $debitos + $montoVoucher;

                    $datoArreglo['Debe'][] = array(
                        'pk_num_cuenta' => $i['pk_num_cuenta'],
                        'cod_cuenta' => $i['cod_cuenta'],
                        'ind_descripcion' => $i['ind_descripcion'],
                        'MontoVoucher' => $montoVoucher
                    );
                } else {
                    $creditos = $creditos + $montoVoucher;
                    $datoArreglo['Haber'][] = array(
                        'pk_num_cuenta' => $i['pk_num_cuenta'],
                        'cod_cuenta' => $i['cod_cuenta'],
                        'ind_descripcion' => $i['ind_descripcion'],
                        'MontoVoucher' => $montoVoucher
                    );
                }

        }
        if(!isset($datoArreglo)){
            $datoArreglo=false;
        }
        $this->atVista->assign('datoArreglo',$datoArreglo);
        $this->atVista->assign('datosOblig', $datosOblig);
        $this->atVista->assign('creditos', $creditos);
        $this->atVista->assign('debitos', $debitos);
        $this->atVista->assign('datos', $datos);
        $this->atVista->assign('codVoucher', $codVoucher);
        $this->atVista->assign('documento', $documento);
        $this->atVista->assign('idObligacion', $idObligacion);
        $this->atVista->assign('contabilidad', $contabilidades['pk_num_contabilidades']);
        $this->atVista->metRenderizar('vouchersProvision', 'modales');
    }

    public function metRegistrarVouchers()
    {
        $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
        $txt = $this->metValidarFormArrayDatos('form', 'txt');
        $ind = $this->metValidarFormArrayDatos('form', 'int');
        $idObligacion = $this->metObtenerInt('idObligacion');
        $idTransaccion = $this->metObtenerInt('idTransaccion');
        $formula= $this->metObtenerFormulas('txt_titulo_voucher');

        if ($alphaNum != null && $ind == null && $txt == null) {
            $validacion = $alphaNum;
        } elseif ($alphaNum == null && $ind != null && $txt == null) {
            $validacion = $ind;
        } elseif ($alphaNum != null && $ind != null && $txt == null) {
            $validacion = array_merge($alphaNum, $ind);
        } elseif ($alphaNum != null && $txt != null && $ind == null) {
            $validacion = array_merge($alphaNum, $txt);
        } elseif ($ind != null && $txt != null && $alphaNum == null) {
            $validacion = array_merge($ind, $txt);
        } else {
            $validacion = array_merge($ind, $txt, $alphaNum);
        }
        if($formula){
            $validacion['txt_titulo_voucher'] = $formula;
        }
        if(!isset($validacion['fk_cbc003_num_voucher'])){
            $validacion['fk_cbc003_num_voucher'] = 'error';
            $validacion['status'] = 'errorVoucher';
            echo json_encode($validacion);
            exit;
        }
        if (in_array('error', $validacion)) {
            $validacion['status'] = 'error';
            echo json_encode($validacion);
            exit;
        }

        $this->idUsuario = Session::metObtener('idUsuario');
        $this->idEmpleado = Session::metObtener('idEmpleado');
        $idPersona = $this->atProvisionObligacionesModelo->metObtenerPersona($this->idEmpleado);
        $validacion['fk_a003_num_preparado_por'] = $idPersona["pk_num_persona"];
        $organismo = $this->atProvisionObligacionesModelo->atObligacionModelo->metObtenerOrganismo($this->idEmpleado);
        $validacion['fk_a001_num_organismo'] = $organismo['fk_a001_num_organismo'];
        $validacion['fk_a004_num_dependencia'] = $organismo['fk_a004_num_dependencia'];
        // datos default
        $validacion['fk_cbc002_num_sistema_fuente']= "6";
        $validacion['num_flag_transferencia']= "0";

        $resultado=$this->atProvisionObligacionesModelo->atObligacionModelo->metNuevo($validacion);
        $codVoucher = $this->atProvisionObligacionesModelo->atObligacionModelo->metBuscar('cb_b001_voucher','pk_num_voucher_mast',$resultado);
        $validacion['codVoucher'] = $codVoucher['ind_voucher'];
        $validacion['idVoucher'] = $resultado;

        if($idTransaccion!=''){
            $validacion['idTransaccion'] = $idTransaccion;
            $voucher = $this->atProvisionObligacionesModelo->metVouchersTransaccion($idTransaccion, $validacion['idVoucher']);
        }else{
            $validacion['idObligacion'] = $idObligacion;
            $voucher = $this->atProvisionObligacionesModelo->metVouchersOblig($idObligacion, $validacion['idVoucher']);
        }


        if($validacion['idVoucher'] == 'no-periodo'){
            $validacion['status'] = 'no-periodo';
            echo json_encode($validacion);
            exit;
        }
        $validacion['status'] = 'ok2';
        echo json_encode($validacion);
        exit;

    }

}