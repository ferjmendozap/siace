<?php

require_once 'dataCxP.php';

class scriptCargaControlador extends Controlador
{
    use dataCxP;
    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        $this->atScriptCarga = $this->metCargarModelo('scriptCarga');
    }

    public function metIndex()
    {
        ### CxP ###
        $this->metCxP();

        echo "TERMINADO";
    }

    public function metCxP()
    {
        echo 'INICIO CARGA DE MAESTROS MODULO CxP<br><br>';
        echo '-----Carga Impuesto-----<br><br>';
        $this->atScriptCarga->metCxPImpuesto($this->metDataImpuestos());
        echo '-----Carga Servicios-----<br><br>';
        $this->atScriptCarga->metCxPTipoServicios($this->metDataServicios());
        echo '-----Carga Documentos-----<br><br>';
        $this->atScriptCarga->metCxPDocumentos($this->metDataDocumetos());
        echo '-----Carga Clasificacion de Gatos-----<br><br>';
        $this->atScriptCarga->metCxPClasificacion($this->metDataClasificacionGasto());
        echo '-----Carga Concepto de Gatos-----<br><br>';
        $this->atScriptCarga->metCxPConcepto($this->metDataConceptoGasto());
        echo '-----Carga Cuentas Bancarias-----<br><br>';
        $this->atScriptCarga->metCxPCuentaBancaria($this->metDataCuentasBancarias());
        echo '-----Carga Tipo Transacciones Bancarias-----<br><br>';
        $this->atScriptCarga->metCxPtipoTransaccion($this->metDataTipoTransaccion());
        echo 'FIN CARGA DE CxP<br><br>';
    }

}
