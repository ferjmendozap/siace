<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
require_once RUTA_Modulo.'modCP'.DS.'controladores'.DS.'trait'.DS.'funcionesTrait.php';
class obligacionesControlador extends Controlador
{
    use funcionesTrait;
    private $atObligacionesModelo;
    private $atFPDF;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atObligacionesModelo = $this->metCargarModelo('reportes');
        $this->metObtenerLibreria('cabeceraCP', 'modCP');

    }

    public function metIndex($lista = false)
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        if($lista == 'obligaciones'){
            $this->atVista->assign('listadoDocumento', $this->atObligacionesModelo->atDocumentoModelo->metDocumentoListar());
            $this->atVista->metRenderizar('listaObligacionesReportes');
        }elseif($lista == 'registroCompra'){
            $this->atVista->metRenderizar('registroCompraReportes');
        }elseif($lista == 'obligacionPendiente'){
            $this->atVista->metRenderizar('obligacionesPendientesReportes');
        }elseif($lista == 'comparacionPagoOblig'){
            $this->atVista->metRenderizar('comparacionPagosObligacionReportes');
        }elseif($lista == 'obligDistribucionContable'){
            $this->atVista->metRenderizar('obligVsDistribucionContableReportes');
        }elseif($lista == 'documentosEmitidos'){
            $this->atVista->assign('listadoCuentas', $this->atObligacionesModelo->atCuentasModelo->metCuentaListar());
            $this->atVista->assign('listadoTipoPago', $this->atObligacionesModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
            $this->atVista->metRenderizar('documentosEmitidosReportes');
        }
        elseif($lista == 'estadoCuenta'){
            $this->atVista->assign('listadoDocumento', $this->atObligacionesModelo->atDocumentoModelo->metDocumentoListar());
            $this->atVista->metRenderizar('estadoCuentaReportes');

        }

    }
    ## OBLIGACIONES / LISTA DE OBLIGACIONES
    public function metImprimirListaObligaciones()
    {
        $this->atFPDF = new PDF('L', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();

        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 7, 5, 13, 10);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, APP_ORGANISMO, 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(260, 5, utf8_decode('Lista de Obligaciones'), 0, 0, 'C');
        $this->atFPDF->Ln(5);
        //	imprimo cuerpo
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(30, 15, 15, 15, 10, 20, 20, 20, 20, 20, 75));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'C', 'C', 'R', 'R', 'R', 'R', 'C', 'L'));
        $this->atFPDF->Row(array('Nro. Documento',
            'Nro. Reg.',
            'Fecha Doc.',
            'Fecha Pago',
            'Est.',
            'Monto Obligacion',
            '(-Adelantos)',
            '(-Pago Parcial)',
            'Total',
            'Voucher',
            'Concepto'));
        $this->atFPDF->Ln(1);
        //---------------------------------------------------
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 6);
        //---------------------------------------------------
        $datos = $this->atObligacionesModelo->metConsultaListaObligaciones();
        $grupo=0; $total_registros = 0; $sub_total_registros = 0; $flagvertotales = 1; $total = 0;
        $sub_total = 0;
        foreach ($datos as $i){
            //$voucher = "$anio$mes-$codvoucher$nrovocuher";
            $total_registros++;
            $monto_total = $i['num_monto_obligacion'] - $i['num_monto_adelanto'] - $i['num_monto_pago_parcial'];
            $total += $monto_total;
            //	si cambia de proveedor
            if($grupo != $i['fk_a003_num_persona_proveedor']){
                $grupo = $i['fk_a003_num_persona_proveedor'];
                //	imprimo sub-total
                if ($flagvertotales == "1" && $total_registros > 1) {
                    $this->atFPDF->SetFont('Arial', 'B', 6);
                    $this->atFPDF->Cell(70, 5, 'Nro. Registros: '.$sub_total_registros, 0, 0, 'L');
                    $this->atFPDF->Cell(75, 5, 'Sub-Total: ', 0, 0, 'R');
                    $this->atFPDF->Cell(20, 5, number_format($sub_total, 2, ',', '.'), 0, 0, 'R');
                    $this->atFPDF->Ln(5);
                }
                //	imprimo proveedor
                $this->atFPDF->Ln(2);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', 'B', 6);
                $this->atFPDF->SetWidths(array(80, 30, 30, 120));
                $this->atFPDF->SetAligns(array('L', 'L', 'L', 'L'));
                $this->atFPDF->Row(array(utf8_decode($i['proveedor']),
                    'Documento '.$i['ind_cedula_documento'],
                    'Telf. '.$i['ind_telefono'],
                    utf8_decode('Dirección ').utf8_decode($i['ind_direccion'])));
                $sub_total_registros = 0;
                $sub_total = 0;
            }
            
            $sub_total_registros++;
            $sub_total += $monto_total;
            //	imprimo documento
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->SetWidths(array(30, 15, 15, 15, 10, 20, 20, 20, 20, 20, 75));
            $this->atFPDF->SetAligns(array('C', 'C', 'C', 'C', 'C', 'R', 'R', 'R', 'R', 'C', 'L'));
            $this->atFPDF->Row(array($i['cod_tipo_documento'].'-'.$i['cod_tipo_documento'],
                $i['ind_nro_registro'],
                $i['fec_documento'],
                $i['fec_pago'],
                $i['ind_estado'],
                number_format($i['num_monto_obligacion'], 2, ',', '.'),
                number_format($i['num_monto_adelanto'], 2, ',', '.'),
                number_format($i['num_monto_pago_parcial'],2, ',', '.'),
                number_format($monto_total,2, ',', '.'),
                //$voucher,
                utf8_decode($i['ind_comentarios'])));
            
        }
        if ($flagvertotales == "1") {
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(70, 5, 'Nro. Registros: '.$sub_total_registros, 0, 0, 'L');
            $this->atFPDF->Cell(75, 5, 'Sub-Total: ', 0, 0, 'R');
            $this->atFPDF->Cell(20, 5, number_format($sub_total, 2, ',', '.'), 0, 0, 'R');
        }
        $this->atFPDF->Ln(10);
        //	imprimo total
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->Rect(155, $y, 20, 0.1, "FD");
        $y = $this->atFPDF->GetY() + 5;
        $this->atFPDF->Rect(155, $y, 20, 0.1, "FD");
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Cell(70, 5, 'Nro. Registros: '.$total_registros, 0, 0, 'L');
        $this->atFPDF->Cell(75, 5, 'Total: ', 0, 0, 'R');
        $this->atFPDF->Cell(20, 5, number_format($total,2, ',', '.'), 0, 0, 'R');

        //// Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    ## OBLIGACIONES / OBLIGACIONES PENDIENTES A PROVEEDORES
    public function metImprimirObligacionesPendientes()
    {
        $proveedor = $_GET['proveedor'];
        $desde = $_GET['desde'];
        $hasta = $_GET['hasta'];
        $centroCosto = $_GET['centroCosto'];

        $this->atFPDF = new PDF('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();

        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 7, 5, 13, 10);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN Y SERVICIOS'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(175, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(175, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(200, 5, utf8_decode('Obligaciones Pendientes'), 0, 0, 'C');
        $this->atFPDF->Ln(5);

        //	imprimo datos generales
		//if ($fvencimientod != "") { $desde = "Desde: ".$fvencimientod;}
		//if ($fvencimientoh != "") { $hasta = "Hasta: ".$fvencimientoh;}
		$this->atFPDF->SetFont('Arial', 'B', 6);
		$this->atFPDF->Cell(200, 5, 'Fecha de Vencimiento: '.$desde.' '.$hasta, 0, 0, 'L');
		$this->atFPDF->Ln(5);

        //	imprimo cuerpo
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(30, 15, 75, 20, 20, 20, 20));
        $this->atFPDF->SetAligns(array('C', 'C', 'L', 'C', 'C', 'C', 'R'));
        $this->atFPDF->Row(array('Nro. Documento',
            'Nro. Reg.',
            'Detalle',
            'Fecha Doc.',
            utf8_decode('Fecha Recep.'),
            'Fecha Prog.',
            'Monto'));
        //---------------------------------------------------
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 6);
        //---------------------------------------------------
        $obligaciones = $this->atObligacionesModelo->metConsultaObligacionesPend($proveedor, $desde, $hasta,$centroCosto);
        $grupo=0;
        $totalRegistros = 0;   $totalContabilizado = 0;   $totalProveedor = 0;
        foreach ($obligaciones AS $i) {
            $totalRegistros++;
            if ($i['num_contabilizacion_pendiente'] == 0) {
                $totalContabilizado = $totalContabilizado + $i['num_monto_obligacion'];
                $totalProveedor = $totalProveedor + $i['num_monto_obligacion'];
            }
            //	si cambia de proveedor
            if($grupo != $i['fk_a003_num_persona_proveedor']){
                $grupo = $i['fk_a003_num_persona_proveedor'];

                //	imprimo sub-total
                if ($totalRegistros > 1) {
                    $this->atFPDF->SetFont('Arial', 'B', 6);
                    $this->atFPDF->Cell(180, 5, 'Total Contabilizado: ', 0, 0, 'R');
                    $this->atFPDF->Cell(20, 5, '$sub_total_contabilizado', 0, 0, 'R');
                    $this->atFPDF->Ln(5);
                    $this->atFPDF->Cell(180, 5, 'Total Proveedor: ', 0, 0, 'R');
                    $this->atFPDF->Cell(20, 5, '$sub_total_proveedor', 0, 0, 'R');
                    $this->atFPDF->Ln(5);
                }
                //	imprimo proveedor
                $this->atFPDF->Ln(2);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', 'B', 6);
                $this->atFPDF->SetWidths(array(200));
                $this->atFPDF->SetAligns(array('L'));
                $this->atFPDF->Row(array(
                    $i['fk_a003_num_persona_proveedor'].' '.utf8_decode($i['proveedor'])
                ));
                $subTotalRegistros = 0;
                $subTotalContabilizado = 0;
                $subTotalProveedor = 0;
            }
            $subTotalRegistros++;
            if ($i['num_contabilizacion_pendiente'] == 0) {
               $subTotalContabilizado = $subTotalContabilizado + $i['num_monto_obligacion'];
                $subTotalProveedor = $subTotalProveedor + $i['num_monto_obligacion'];
            }
            //	imprimo documento
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->SetWidths(array(30, 15, 75, 20, 20, 20, 20));
            $this->atFPDF->SetAligns(array('C', 'C', 'L', 'C', 'C', 'C', 'R'));
            $this->atFPDF->Row(array(
                $i['cod_tipo_documento'].'-'.$i['cod_tipo_documento'],
                $i['ind_nro_registro'],
                utf8_decode($i['ind_comentarios']),
                $i['fec_documento'],
                $i['fec_recepcion'],
                $i['fec_programada'],
                $i['num_monto_obligacion']
            ));


        //	imprimo sub-total
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Cell(180, 5, 'Total Contabilizado: ', 0, 0, 'R');
        $this->atFPDF->Cell(20, 5, $subTotalContabilizado, 0, 0, 'R');
        $this->atFPDF->Ln(5);
        $this->atFPDF->Cell(180, 5, 'Total Proveedor: ', 0, 0, 'R');
        $this->atFPDF->Cell(20, 5, $subTotalProveedor, 0, 0, 'R');
        $this->atFPDF->Ln(10);
        //	imprimo total
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->Rect(190, $y, 20, 0.1, "FD");
        $y = $this->atFPDF->GetY() + 10;
        $this->atFPDF->Rect(190, $y, 20, 0.1, "FD");
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Cell(180, 5, 'Total Contabilizado: ', 0, 0, 'R');
        $this->atFPDF->Cell(20, 5, $totalContabilizado, 0, 0, 'R');
        $this->atFPDF->Ln(5);
        $this->atFPDF->Cell(180, 5, 'Total Proveedor: ', 0, 0, 'R');
        $this->atFPDF->Cell(20, 5, $totalProveedor, 0, 0, 'R');
        }
    //// Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    ## OBLIGACIONES / COMPARACION PAGOS Y OBLIGACIONES
    public function metImprimirCompracionPagosOblig()
    {
        $proveedor = $_GET['proveedor'];
        $periodo = $_GET['periodo'];
        if($periodo){
            $fecha = explode("-",$periodo);
                $año = $fecha[0];
                $mes = $fecha[1];
        }else{
            $año = "";
            $mes = "";
        }
        $this->atFPDF = new PDF('L', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();

        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 7, 5, 13, 10);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN Y SERVICIOS'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(260, 5, utf8_decode('Pagos Vs. Obligaciones'), 0, 0, 'C');
        $this->atFPDF->Ln(5);
        //	imprimo datos generales
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Cell(20, 5, 'Periodo: '.$periodo, 0, 0, 'L');
        $this->atFPDF->Ln(5);
        //	imprimo cuerpo
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(65, 15, 15, 20, 20, 30, 15, 20, 20, 20, 20));
        $this->atFPDF->SetAligns(array('L', 'C', 'C', 'C', 'R', 'C', 'C', 'R', 'R', 'R', 'C'));
        $this->atFPDF->Row(array('Pagar A',
            'Fecha de Pago',
            'Nro. Proceso',
            'Voucher',
            'Monto Pagado',
            'Nro. Documento',
            'Fecha Documento',
            'Monto Obligaciones',
            'Monto Adelantos',
            'Pagado',
            utf8_decode('Voucher Provisión')));
        //---------------------------------------------------
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 6);
        //---------------------------------------------------
        $totalPagos = 0;   $totalNovoucher = 0;

        $datos  = $this->atObligacionesModelo->metConsultaPagosOblig($proveedor, $año, $mes);
        $grupo = 0; $totalPagos = 0; $subTotalPagos = 0; $subTotalNovoucher = 0;
        foreach ($datos AS $i) {
            $totalPagos++;
            $voucherPago = $i['fec_anio'].$i['fec_mes']-$i['fk_cbb001_num_voucher_pago'];
            $montoAdelanto = $i['num_monto_adelanto'] + $i['num_monto_pago_parcial'];
            $montoNeto = $i['num_monto_obligacion'] - $montoAdelanto;
            $voucherProvision = "-";
            //	si cambia de proveedor
            if($grupo != $i['fk_cpb014_num_cuenta']){
                    $grupo = $i['fk_cpb014_num_cuenta'];

                //	imprimo sub-total
                if ($totalPagos > 1) {
                    $this->atFPDF->SetFont('Arial', 'B', 6);
                    $this->atFPDF->Cell(60, 5, 'Total Pagos: '.$subTotalPagos, 0, 0, 'L');
                    $this->atFPDF->Cell(75, 5, 'Total Obligaciones No Contabilizadas: '.$subTotalNovoucher, 0, 0, 'R');
                    $this->atFPDF->Ln(5);
                }
                //	imprimo proveedor
                $this->atFPDF->Ln(2);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', 'B', 6);
                $this->atFPDF->SetWidths(array(25, 210));
                $this->atFPDF->SetAligns(array('L', 'L'));
                $this->atFPDF->Row(array($i['ind_num_cuenta'],
                    $i['ind_descripcion']));
            }
            $subTotalPagos++;

            if ($i['voucherProvision'] != "") $subTotalNovoucher++;
            //	imprimo documento
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->SetWidths(array(65, 15, 15, 20, 20, 30, 15, 20, 20, 20, 20));
            $this->atFPDF->SetAligns(array('L', 'C', 'C', 'C', 'R', 'C', 'C', 'R', 'R', 'R', 'C'));
            $this->atFPDF->Row(array(
                $i['proveedorPagar'],
                $i['fec_pago'],
                $i['ind_nro_proceso'],
                $voucherPago,
                number_format($i['num_monto_pago'], 2, ',', '.'),
                $i['cod_tipo_documento'].'-'.$i['cod_tipo_documento'],
                $i['fec_documento'],
                number_format($i['num_monto_obligacion'], 2, ',', '.'),
                number_format($montoAdelanto, 2, ',', '.'),
                number_format($montoNeto, 2, ',', '.'),
                $voucherProvision
            ));
        }
    //	imprimo sub-total
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Cell(60, 5, 'Total Pagos: '.$subTotalPagos, 0, 0, 'L');
        $this->atFPDF->Cell(75, 5, 'Total Obligaciones No Contabilizadas: '.$subTotalNovoucher, 0, 0, 'R');

    ////	Muestro el contenido del pdf.

    $this->atFPDF->Output();

    }

    public function metImprimirCompracionObligPagos()
    {
        $proveedor = $_GET['proveedor'];
        $periodo = $_GET['periodo'];
        if($periodo){
            $fecha = explode("-",$periodo);
            $año = $fecha[0];
            $mes = $fecha[1];
        }else{
            $año = "";
            $mes = "";
        }
        $this->atFPDF = new PDF('L', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();

        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 7, 5, 13, 10);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN Y SERVICIOS'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(260, 5, utf8_decode('Obligaciones Vs. Pagos'), 0, 0, 'C');
        $this->atFPDF->Ln(5);

        //	imprimom datos generales
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Cell(20, 5, 'Periodo: '.$periodo, 0, 0, 'L');
        $this->atFPDF->Ln(5);
        //	imprimo cuerpo
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(15, 55, 30, 18, 18, 18, 18, 15, 20, 15, 18, 15, 5));
        $this->atFPDF->SetAligns(array('C', 'L', 'C', 'R', 'R', 'R', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
        $this->atFPDF->Row(array(
            'Fecha Documento',
            'Proveedor',
            'Nro. Documento',
            'Monto Obligaciones',
            '(-)Adelantos (-)Pago Parcial',
            'Neto por Pagar',
            utf8_decode('Voucher Provisión'),
            'Fecha de Pago',
            'Cuenta Bancaria',
            'Nro. Pago',
            'Voucher',
            'Nro. Proceso',
            '#'
        ));
        $this->atFPDF->Ln(2);
        //---------------------------------------------------
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 6);
        //---------------------------------------------------
        $totalObligaciones = 0;  $totalNovoucher = 0;  $totalNopago = 0;
        $datos  = $this->atObligacionesModelo->metConsultaObligPagos($proveedor, $año, $mes);
        foreach ($datos AS $i) {
            $totalObligaciones++;
            if ($i['fk_cbb001_num_voucher_pago'] == "") $totalNovoucher++;
            if ($i['ind_num_pago'] == "") $totalNopago++;
            $montoAdelanto = $i['num_monto_adelanto'] + $i['num_monto_pago_parcial'];
            $montoNeto = $i['num_monto_obligacion'] - $montoAdelanto;
            $voucherProvision = "-";
            $voucherPago = "-";

            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->Row(array(
                $i['fec_documento'],
                $i['proveedor'],
                $i['cod_tipo_documento'].'-'.$i['cod_tipo_documento'],
                $i['num_monto_obligacion'],
                number_format($montoAdelanto, 2, ',', '.'),
                number_format($montoNeto, 2, ',', '.'),
                $voucherProvision,
                $i['fec_pago'],
                $i['ind_num_cuenta'],
                $i['ind_num_pago'],
                $voucherPago,
                $i['ind_nro_proceso'],
                $i['num_proceso_secuencia']
            ));

        }
        //	imprimo sub-total
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Cell(132, 5, 'Total Obligaciones: '.$totalObligaciones, 0, 0, 'L');
        $this->atFPDF->Cell(75, 5, 'Total Pagos No Contabilizados: '.$totalNovoucher, 0, 0, 'R');
        $this->atFPDF->Ln(8);
        $this->atFPDF->Cell(60, 5, 'Total Obligaciones No Pagadas: '.$totalNopago, 0, 0, 'L');


        ////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }


    ## OBLIGACIONES / REGISTRO DE COMPRAS
    public function metImprimirRegistroCompra()
    {
        $this->atFPDF = new PDF('L', 'mm', array(215.9, 375.6));
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 5, 5);
        $this->atFPDF->SetAutoPageBreak(5, 5);
        $this->atFPDF->AddPage();

        ##	membrete (titulo)
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 7, 5, 13, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        ##	fecha, pagina
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(315, 5); $this->atFPDF->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(315, 10); $this->atFPDF->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        ##	titulo
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(347, 5, 'LIBRO DE COMPRAS $nombre_periodo', 0, 0, 'C');
        ##
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        ##
        $this->atFPDF->Ln(5);

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 5);
        $this->atFPDF->Cell(277, 5, '', 0, 0, 'C');
        $this->atFPDF->Cell(45, 5, 'IMPORTACIONES E IMPORTACIONES', 1, 1, 'C');
        $this->atFPDF->SetFont('Arial', 'B', 5);
        $this->atFPDF->SetWidths(array(8, 14, 18, 55, 10, 14, 14, 14, 24, 24, 14, 14, 10, 14, 15, 15, 15, 15, 15, 15, 15, 15));
        $this->atFPDF->SetAligns(array('R', 'C', 'L', 'L', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'R', 'R', 'R', 'R', 'R', 'R', 'R', 'R'));
        $this->atFPDF->Row(array('# Op.',
            'F.Factura',
            'RIF',
            utf8_decode('Nombre o Razón Social'),
            'Tipo Prov.',
            'Nro. Comprobante',
            'Nro. Planilla Imp.',
            'Nro. Expediente',
            'Nro. Factura',
            'Nro. Control',
            utf8_decode('Nro. Nota Crédito'),
            utf8_decode('Nro. Nota Débito'),
            'Tipo. Trans.',
            'Nro. Factura Afectada',
            'Total Compras + IVA',
            utf8_decode('Compras sin derecho a Crédito IVA'),
            'Base Imponible',
            '% Alicuota',
            'Impuesto IVA',
            'IVA Retenido al Vendedor',
            'IVA Retenido a Terceros',
            utf8_decode('Anticipo de IVA (Importación)')));
        $this->atFPDF->Ln(1);
//---------------------------------------------------
////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    public function metImprimirRetencion()
    {
        $tipo = $_GET['tipo'];
        if($tipo == 'IVA'){
            $titulo = 'I.V.A.';
        }elseif ($tipo == 'ISLR'){
            $titulo = 'I.S.L.R';
        }elseif ($tipo == '1X1000'){
            $titulo = '1x1000';
        }

        $proveedor = $_GET['proveedor'];
        $desde = $_GET['desde'];
        $hasta = $_GET['hasta'];
        $diaDesde = $_GET['diaDesde'];
        $diaHasta = $_GET['diaHasta'];

        $this->atFPDF = new pdf('L', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 5, 5);
        $this->atFPDF->SetAutoPageBreak(5, 5);
        $this->atFPDF->AddPage();

        ##	membrete (titulo)
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 7, 5, 13, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        ##	fecha, pagina
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(235, 5); $this->atFPDF->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(235, 10); $this->atFPDF->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'L');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        ##	titulo
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(268, 5, utf8_decode('RELACION DETALLADA DEL '.$titulo.' RETENIDO'), 0, 0, 'C');
        ##
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        ##
        $this->atFPDF->Ln(10);
        ##	imprimir titulos
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(20, 61, 12, 16, 25, 25, 16, 16, 16, 16, 16, 13, 16));
        $this->atFPDF->SetAligns(array('C', 'L', 'C', 'C', 'L', 'L', 'C', 'R', 'R', 'R', 'R', 'R', 'R'));
        $this->atFPDF->Row(array('Comprobantes',
            utf8_decode('Nombre o Razón Social'),
            'Periodo Fiscal',
            'Fecha Comprobante',
            'Nro. Control',
            'Nro. Factura',
            'Fecha Factura',
            'Monto Imponible',
            'Monto Exento',
            'Monto Impuesto',
            'Monto Factura',
            'Porcentaje',
            'Monto Retenido'));
        $this->atFPDF->Ln(1);

        //consulta de los comprobantes
        $retencion = $this->atObligacionesModelo->metConsultaRetencion($tipo, $proveedor, $desde, $hasta, $diaDesde, $diaHasta);
        $MontoRetenido=0;
        foreach ($retencion AS $i) {
            if ($i['ind_estado'] == 'AN') {
                $monto = 0;
            } else {
                $monto = $i['num_monto_impuesto'];
            }

            $MontoRetenido = $MontoRetenido + round($monto, 2);
            ##	imprimo linea
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->Row(array(
                $i['ind_num_comprobante'],
                $i['nombreProveedor'],
                $i['ind_periodo_fiscal'],
                $i['fec_comprobante'],
                $i['ind_num_control'],
                $i['ind_nro_factura'],
                $i['fec_factura'],
                number_format($i['montoAfecto'], 2, ',', '.'),
                number_format($i['num_monto_no_afecto'], 2, ',', '.'),
                number_format($i['montoImpuesto'], 2, ',', '.'),
                number_format(($i['montoAfecto']+$i['montoImpuesto']), 2, ',', '.'),
                number_format($i['num_factor_porcentaje'], 2, ',', '.'),
                number_format($monto, 2, ',', '.')
            ));
        }
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Row(array('',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                'TOTAL:',
                '',
            number_format($MontoRetenido, 2, ',', '.')
        ));
//---------------------------------------------------
////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }


    ## OBLIGACIONES / OBLIGACIONES VS DISTRIBUCION CONTABLE
    public function metImprimirObligacionesContable()
    {
        $proveedor = $_GET['proveedor'];            $centroCosto = $_GET['centroCosto'];
        $registroDesde = $_GET['registroDesde'];    $registroHasta = $_GET['registroHasta'];
        $pagoDesde = $_GET['pagoDesde'];    $pagoHasta = $_GET['pagoHasta'];
        $cuenta = $_GET['cuenta'];


        $this->atFPDF = new PDF('L', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();

        // imprimo cabecera

        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN Y SERVICIOS'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(260, 5, utf8_decode('Obligaciones Vs. Distribución Contable'), 0, 0, 'C');
        $this->atFPDF->Ln(10);
        //	imprimo cuerpo
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->SetWidths(array(65, 20, 20, 20, 20, 20, 20, 20, 20, 15, 20));
        $this->atFPDF->SetAligns(array('L', 'L', 'R', 'R', 'R', 'C', 'C', 'C', 'C', 'C', 'R'));
        $this->atFPDF->Row(array(utf8_decode('Razón Social'),
            'Documento',
            'Importe',
            'IVA',
            'Impuestos',
            'F. Documento',
            utf8_decode('F. Recepción'),
            'F. Pago',
            'Cta. Contable',
            'C.Costo',
            'Monto'));
        $this->atFPDF->Ln(1);

        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 6);
        //-------------------------------------------------
        $datos = $this->atObligacionesModelo->metConsultaObligacionesContable($proveedor, $centroCosto,
            $registroDesde, $registroHasta,
            $pagoDesde, $pagoHasta, $cuenta);

        $totalRegistros = 0; $totalImporte = 0; $totalIva = 0; $totalImpuestos = 0; $totalMonto = 0;
        foreach ($datos as $i){
            $totalRegistros++;
            $totalImporte += $i['num_monto_obligacion'];
            $totalIva += $i['num_monto_impuesto'];
            $totalImpuestos += $i['num_monto_impuesto_otros'];
            $monto = $i['num_monto_obligacion'] + $i['num_monto_impuesto'] - $i['num_monto_impuesto_otros'];
            $totalMonto += $monto;
            //	imprimo documento
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 7);
            $this->atFPDF->Row(array(utf8_decode(
                $i['proveedor']),
                $i['cod_tipo_documento'].'-'.$i['cod_tipo_documento'],
                number_format($i['num_monto_obligacion'], 2, ',', '.'),
                number_format($i['num_monto_impuesto'], 2, ',', '.'),
                number_format($i['num_monto_impuesto_otros'], 2, ',', '.'),
                $i['fec_documento'],
                $i['fec_recepcion'],
                $i['fec_registro'],
                $i['ind_num_cuenta'],
                $i['fk_a023_num_centro_de_costo'],
                number_format($monto, 2, ',', '.')
            ));
            $this->atFPDF->Ln(2);
        }
        $this->atFPDF->Ln(5);
        //	imprimo total
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->Rect(75, $y, 195, 0.1, "FD");
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(85, 5, 'Importe Total: ', 0, 0, 'R');
        $this->atFPDF->Cell(20, 5, number_format($totalImporte, 2, ',', '.'), 0, 0, 'R');
        $this->atFPDF->Cell(20, 5, number_format($totalIva, 2, ',', '.'), 0, 0, 'R');
        $this->atFPDF->Cell(20, 5, number_format($totalImpuestos, 2, ',', '.'), 0, 0, 'R');
        $this->atFPDF->Cell(95, 5, '', 0, 0, 'R');
        $this->atFPDF->Cell(20, 5, number_format($totalMonto, 2, ',', '.'), 0, 0, 'R');

        ////	Muestro el contenido del pdf.

        $this->atFPDF->Output();

    }

    ## OBLIGACIONES / ESTADO DE CUENTA
    public function metImprimirEstadoCuenta()
    {
        $proveedor = $_GET['proveedor'];
        $tipoDoc = $_GET['tipoDoc'];
        $hasta = $_GET['hasta'];
        if(isset($_GET['desde'])){
            $desde = $_GET['desde'];
        }else{
            $desde = "";
        }


        $this->atFPDF = new PDF('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();
        
        $datosProveedor = $this->atObligacionesModelo->metConsultaProveedorEC($proveedor);

        //	imprimo cabecera
        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        if ($desde != "" && $hasta != ""){
            $titulo = "del ".$desde." al ".$hasta;
        } else {
            $titulo = " al ".$hasta;
        }
        $this->atFPDF->Cell(198, 5, utf8_decode('Estado de Cuenta '.$titulo), 0, 0, 'C');
        $this->atFPDF->Ln(10);
        //	imprimo datos generales
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $y = $this->atFPDF->GetY();	$x = 10;
        $this->atFPDF->RoundedRect($x, $y, 135, 30, 2.5, 'DF');
        $this->atFPDF->SetXY($x, $y);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(125, 5, utf8_decode('Nombre / Razón Social del Proveedor'), 0, 0, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', '', 12);
        $this->atFPDF->Cell(125, 5, utf8_decode($datosProveedor['proveedor']), 0, 0, 'L');
        $this->atFPDF->Ln(6);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(125, 5, utf8_decode('Dirección'), 0, 0, 'L');
        $this->atFPDF->Ln(4);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->MultiCell(125, 5, utf8_decode($datosProveedor['ind_direccion']), 0, 'L');
        $this->atFPDF->SetXY($x, $y+25);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(15, 5, utf8_decode('Teléfono:'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(35, 5, $datosProveedor['ind_telefono'], 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(8, 5, utf8_decode('Fax:'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(37, 5, $datosProveedor['ind_telefono'], 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(8, 5, utf8_decode('Rif:'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(22, 5, $datosProveedor['ind_documento_fiscal'], 0, 0, 'L');
        ##
        $x = 147;
        $this->atFPDF->RoundedRect($x, $y, 60, 30, 2.5, 'DF');
        $this->atFPDF->SetXY($x, $y);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(60, 8, utf8_decode('DEPOSITOS'), 0, 0, 'L');
        $this->atFPDF->Ln(11); $this->atFPDF->SetX($x);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(18, 5, utf8_decode('Nro. Cuenta:'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(42, 5, '', 0, 0, 'L');
        $this->atFPDF->Ln(7); $this->atFPDF->SetX($x);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(18, 5, utf8_decode('Tipo:'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(42, 5,'', 0, 0, 'L');
        $this->atFPDF->Ln(7); $this->atFPDF->SetX($x);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(18, 5, utf8_decode('Banco:'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(42, 5, '', 0, 0, 'L');
        ##
        $this->atFPDF->Ln(10);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(198, 5, utf8_decode('Documentos Pagados'), 0, 0, 'L');
        $this->atFPDF->Ln(6);
        ##
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->SetWidths(array(32, 20, 20, 20, 20, 20, 20, 35, 10));
        $this->atFPDF->SetAligns(array('L', 'C', 'C', 'C', 'C', 'C', 'C', 'R', 'C'));
        $this->atFPDF->Row(array(
            utf8_decode('Obligación'),
            'Nro. Registro',
            'F. Documento',
            'F. Registro',
            utf8_decode('F. Recepción'),
            'F. Prog. Pago',
            'F. Pago',
            'Monto',
            'Est.'));
        ##
        $this->atFPDF->SetFillColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Rect(10, $this->atFPDF->GetY(), 197, 0.1, "FD");
        $this->atFPDF->Ln(5);

        //---------------------------------------------------
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 6);

        $obligacion = $this->atObligacionesModelo->metConsultaObligacionesProveedorEC($proveedor, $hasta, $tipoDoc, $desde);
        $total_registros = 0; $total_documentos= 0; $total_pagados = 0;

        foreach ($obligacion as $i) {
            $total_registros++;
            $total_documentos += $i['num_monto_obligacion'];

            if ($i['ind_estado'] == "PA") {
                $total_pagados += $i['num_monto_obligacion'];
                //	imprimo documento
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 8);
                $this->atFPDF->Row(array(
                    $i['cod_tipo_documento'].'-'.$i['cod_tipo_documento'],
                    $i['ind_nro_registro'],
                    $i['fec_documento'],
                    $i['fec_registro'],
                    $i['fec_recepcion'],
                    $i['fec_programada'],
                    $i['fec_pago'],
                    number_format($i['num_monto_obligacion'], 2, ',', '.')
                ));
                $this->atFPDF->Ln(2);
            }
        }

        $this->atFPDF->Ln(5);
        //	imprimo total
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->Rect(162, $y, 35, 0.1, "FD");
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(145, 5, 'Total Pagados: ', 0, 0, 'R');
        $this->atFPDF->Cell(42, 5, number_format($total_pagados, 2, ',', '.'), 0, 0, 'R');
        $this->atFPDF->Ln(10);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(145, 5, 'Total Documentos del Proveedor: ', 0, 0, 'R');
        $this->atFPDF->Cell(7, 5);
        $this->atFPDF->Cell(35, 5, number_format($total_documentos, 2, ',', '.'), 1, 0, 'R');

        ////	Muestro el contenido del pdf.
        $this->atFPDF->Output();

    }


    ## OBLIGACIONES / DOCUEMNTOS EMITIDOS - CHEQUES GIRADOS
    public function metImprimirDocumentosEmitidos()
    {
        $cuenta = $_GET['cuenta'];
        $tipoPago = $_GET['tipoPago'];
        $desde = $_GET['desde'];
        $hasta = $_GET['hasta'];

        $this->atFPDF = new PDF('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();

        $datos = $this->atObligacionesModelo->metConsultaDocumentosEmitidos($cuenta, $tipoPago, $desde, $hasta);

        $this->atFPDF->Image(ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CES.jpg", 10, 5, 13, 10);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(20, 5); $this->atFPDF->Cell(100, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
        $this->atFPDF->SetXY(20, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIRECCIÓN DE ADMINISTRACIÓN'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 5); $this->atFPDF->Cell(20, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(225, 10); $this->atFPDF->Cell(20, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->atFPDF->Cell(30, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(198, 5, utf8_decode('Documentos Emitidos'), 0, 1, 'C');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(198, 5, utf8_decode('Fecha de Emisión del '.$desde.' al '.$hasta), 0, 0, 'C');
        $this->atFPDF->Ln(10);
        foreach ($datos as $dato){
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->Cell(25, 5, 'Cta. Bancaria', 0, 0, 'L');
            $this->atFPDF->Cell(75, 5, $dato['ind_num_cuenta'].'  '.utf8_decode($dato['ind_descripcion']), 0, 1, 'L');
            $this->atFPDF->Cell(25, 5, 'Cta. Contable', 0, 0, 'L');
            $this->atFPDF->Cell(75, 5, $dato['cod_cuenta'], 0, 0, 'L');
            $this->atFPDF->Ln(5);

            //	imprimo cuerpo
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->SetWidths(array(20, 20, 78, 20, 20, 20, 20));
            $this->atFPDF->SetAligns(array('C', 'C', 'L', 'R', 'C', 'C', 'C'));
            $this->atFPDF->Row(array(
                'Nro. Cheque',
                utf8_decode('Fecha de Emisión'),
                'Pagar A',
                'Monto',
                'Voucher Pago',
                utf8_decode('Voucher Anulación'),
                'Estado'));
            $this->atFPDF->Ln(1);
            //---------------------------------------------------
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 6);
            //---------------------------------------------------

            $grupo = 0;     $totalRegistros = 0;     $totalNroCheques = 0;     $totalCtabancaria = 0;
            foreach ($datos as $i){
                $totalRegistros++;
                //	si cambia de proveedor
                if($grupo != $i['fk_cpb014_num_cuenta']){
                    $grupo = $i['fk_cpb014_num_cuenta'];

                    if ($totalRegistros > 1) {
                        $this->atFPDF->Ln(5);
                        //	imprimo total
                        $this->atFPDF->SetDrawColor(0, 0, 0);
                        $this->atFPDF->SetFillColor(0, 0, 0);
                        $y = $this->atFPDF->GetY();
                        $this->atFPDF->Rect(128, $y-1, 20, 0.1, "FD");
                        $this->atFPDF->Rect(128, $y, 20, 0.1, "FD");
                        $this->atFPDF->SetFont('Arial', 'B', 7);
                        $this->atFPDF->Cell(50, 5, 'Total Nro. Cheques: '.$totalNroCheques, 0, 0, 'L');
                        $this->atFPDF->Cell(68, 5, 'Total Nro. Cheques: ', 0, 0, 'R');
                        $this->atFPDF->Cell(20, 5, number_format($totalCtabancaria, 2, ',', '.'), 0, 0, 'R');
                    }
                    //$this->atFPDF->AddPage();
                }
                $totalNroCheques++;
                $totalCtabancaria += $i['num_monto_pago'];
                //	imprimo documento
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 7);
                $this->atFPDF->Row(array(
                    $i['ind_num_pago'],
                    $i['fec_pago'],
                    utf8_decode($i['proveedor']),
                    number_format($i['num_monto_pago'],2, ',', '.'),
                    'VoucherPago',
                    $i['fk_cbb001_num_voucher_anulacion'],
                    $i['ind_estado']
                ));
                $this->atFPDF->Ln(2);
            }
            $this->atFPDF->Ln(5);

            //	imprimo total
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(0, 0, 0);
            $y = $this->atFPDF->GetY();
            $this->atFPDF->Rect(128, $y-1, 20, 0.1, "FD");
            $this->atFPDF->Rect(128, $y, 20, 0.1, "FD");
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $this->atFPDF->Cell(50, 5, 'Total Nro. Cheques: '.$totalNroCheques, 0, 0, 'L');
            $this->atFPDF->Cell(68, 5, 'Total Cta. Bancaria: ', 0, 0, 'R');
            $this->atFPDF->Cell(20, 5, number_format($totalCtabancaria, 2, ',', '.'), 0, 0, 'R');
        }
            ////	Muestro el contenido del pdf.
            $this->atFPDF->Output();


    }



}
