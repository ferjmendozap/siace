<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba | y.alcoba@contraloriamonagas.gob.ve        | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class serviciosControlador extends Controlador
{
    private $atServicioModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atServicioModelo = $this->metCargarModelo('servicios');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('listado');
    }

    public function metNuevoServicio()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $ver = $this->metObtenerInt('ver');
        $valido = $this->metObtenerInt('valido');
        $idServicio = $this->metObtenerInt('idServicio');
        if ($valido == 1){
            $Excepcion=array('num_flag_manual','num_estatus','cant','pk_num_impuesto','numero','pkImpuesto','pk');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excepcion);
            $formAlphaNum=$this->metValidarFormArrayDatos('form','alphaNum');

            if($formAlphaNum!=null && $ind==null){
                $registro=$formAlphaNum;
            }elseif($formAlphaNum==null && $ind!=null){
                $registro=$ind;
            }else{
                $registro=array_merge($formAlphaNum,$ind);
            }
            if(in_array('error',$registro)){
                $registro['status']='error';
                echo json_encode($registro);
                exit;
            }
            if (!isset($registro['num_estatus'])) {
                $registro['num_estatus'] = 0;
            }
            if (!isset($registro['cant'])) {
                $registro['cant'] = 0;
            }
            if (!isset($registro['numero'])) {
                $registro['numero'] = 0;
            }
            if (!isset($registro['pk_num_impuesto'])) {
                $registro['pk_num_impuesto'] = 0;
            }
            if (!isset($registro['pkImpuesto'])) {
                $registro['pkImpuesto'] = 0;
            }
            if (!isset($registro['pk'])) {
                $registro['pk'] = 0;
            }
            if ($idServicio == 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atServicioModelo->metNuevoServicio($registro);
                $registro['idServicio'] = $id;

                }else{
                $registro['status'] = 'modificar';
                $id = $this->atServicioModelo->metModificarServicio($idServicio, $registro);
                $registro['idServicio']= $id;

                $Regimen=$this->atServicioModelo->metServicioConsulta($registro['idServicio']);
                $registro['RegimenFiscal']=$Regimen['RegimenFiscal'];

            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            echo json_encode($registro);
            exit;

        }
        if ($idServicio != 0) {
            $this->atVista->assign('dataBD', $this->atServicioModelo->metServicioConsulta($idServicio));
            $this->atVista->assign('servicioDetalle',$this->atServicioModelo->metServicioConsultaDetalle($idServicio));
            $this->atVista->assign('servicioImpuesto',$this->atServicioModelo->metServicioImpuesto($idServicio));
        }

        $this->atVista->assign('listadoRegimen',$this->atServicioModelo->atMiscelaneoModelo->metMostrarSelect('REGFISCAL'));
        $this->atVista->assign('listadoImpuesto',$this->atServicioModelo->atImpuestoModelo->metImpuestoListar(''));
        $this->atVista->assign('ver', $ver);

        $this->atVista->assign('idServicio',$idServicio);
        $this->atVista->metRenderizar('nuevo', 'modales');
    }

    public function metEliminarServicio(){

        $idServicio = $this->metObtenerInt('idServicio');
        if($idServicio!=0){
            $id=$this->atServicioModelo->metEliminar($idServicio);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Servicio se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idServicio'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;

    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  s.*,
                  rm.ind_nombre_detalle as RegimenFiscal
                 FROM
                  cp_b017_tipo_servicio AS s
                 INNER JOIN
                  a006_miscelaneo_detalle AS rm ON rm.pk_num_miscelaneo_detalle=s.fk_a006_num_miscelaneo_regimen_fiscal
            ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        s.cod_tipo_servicio LIKE '%$busqueda[value]%' OR
                        s.ind_descripcion LIKE '%$busqueda[value]%' OR
                        rm.ind_nombre_detalle LIKE '%$busqueda[value]%' 
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_tipo_servicio','ind_descripcion','RegimenFiscal','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_tipo_servico';
        #construyo el listado de botones

        if (in_array('CP-01-07-01-04-01-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button class="modificar Servicios btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idServicio="'.$clavePrimaria.'" title="Editar"
                        descipcion="El Usuario ha Modificado un Servicio" titulo="<i class=\'fa fa-edit\'></i> Editar Tipo de Servicio">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('CP-01-07-01-04-02-V',$rol)) {
            $campos['boton']['Ver'] = '
               <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idServicio="'.$clavePrimaria.'" title="Consultar"
                        descipcion="El Usuario esta viendo un un Servicio" titulo="<i class=\'md md-remove-red-eye\'></i> Consultar Tipo de Servicio">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('CP-01-07-01-04-03-E',$rol)) {
            $campos['boton']['Eliminar'] = '
               <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idServicio="'.$clavePrimaria.'" title="Eliminar"  boton="si, Eliminar"
                        descipcion="El usuario ha eliminado un Tipo de Servicio" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Tipo de Servicio!!">
                    <i class="md md-delete" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }
}