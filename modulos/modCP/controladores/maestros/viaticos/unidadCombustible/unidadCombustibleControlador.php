<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Fernando Mendoza     | dt.jefe.ait@cgesucre.gob.ve          | 0416-2830043
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class unidadCombustibleControlador extends Controlador
{
    private $atUnidadCombustibleModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atUnidadCombustibleModelo = $this->metCargarModelo('unidadCombustible');
    }

    public function metIndex($estado=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('estado', $estado);
        $this->atVista->assign('unidadConBD', $this->atUnidadCombustibleModelo->metUnidadCombustibleListar($estado));
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevaUnidadCombustible()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $ver = $this->metObtenerInt('ver');
        $idUnidadCombustible = $this->metObtenerInt('idUnidadCombustible');
        $estado = $this->metObtenerAlphaNumerico('estado');

        if ($valido == 1) {
            if($estado=='PR'){
                $id=$this->atUnidadCombustibleModelo->metAprobatUnidadCombustible($idUnidadCombustible);
                $registro['Mensaje'] = array('titulo'=>'Aprobado','contenido'=>'La Unidad de Viatico fue aprobada Satisfactoriamente');
                $registro['idUnidadViatico'] = $id;
                $registro['status'] = 'OK';
                echo json_encode($registro);
                exit;
            }
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int');
            $txt = $this->metValidarFormArrayDatos('form', 'txt');
            if ($alphaNum != null && $ind == null && $txt==null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $registro = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $registro = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $registro = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $registro = array_merge($ind, $txt);
            }else{
                $registro = array_merge($ind, $txt,$alphaNum);
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }

            if ($idUnidadCombustible == 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atUnidadCombustibleModelo->metNuevaUnidadCombustible($registro['ind_num_resolucion'],
                    $registro['ind_periodo'], $registro['num_unidad_combustible'], $registro['ind_descripcion']);
                $registro['idUnidadCombustible'] = $id;
                $idNuevo = $this->atUnidadCombustibleModelo->metUnidadCombustibleListar(false, $registro['idUnidadCombustible']);
            } else {
                $registro['status'] = 'modificar';
               $id = $this->atUnidadCombustibleModelo->metModificarUnidadCombustible($idUnidadCombustible,$registro['ind_num_resolucion'],
                    $registro['ind_periodo'], $registro['num_unidad_combustible'], $registro['ind_descripcion']);
                $registro['idUnidadCombustible'] = $id;
                $idNuevo = $this->atUnidadCombustibleModelo->metUnidadCombustibleListar(false, $idUnidadCombustible);
            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($id);
                exit;
            }
            $idNuevo['status']=$registro['status'];
            echo json_encode($idNuevo);
            exit;
        }

        if ($idUnidadCombustible != 0) {
            $this->atVista->assign('unidadCombustibleBD', $this->atUnidadCombustibleModelo->metConsultaUnidadCombustibleModelo($idUnidadCombustible));
        }
        if($estado!=''){
            $this->atVista->assign('estado', $estado);
        }
        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idUnidadCombustible', $idUnidadCombustible);

        $this->atVista->metRenderizar('nuevo', 'modales');
    }


}