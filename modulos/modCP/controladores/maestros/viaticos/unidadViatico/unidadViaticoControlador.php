<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * | 2 |
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class unidadViaticoControlador extends Controlador
{
    private $atUnidadViaticoModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atUnidadViaticoModelo = $this->metCargarModelo('unidadViatico');
        $this->atMiscelaneoModelo = $this->metCargarModelo('miscelaneo','','APP');
    }

    public function metIndex($estado=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('estado', $estado);
        $this->atVista->assign('unidadViaBD', $this->atUnidadViaticoModelo->metUnidadViaticoModeloListar($estado));
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevaUnidadViatico()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $ver = $this->metObtenerInt('ver');
        $idUnidadViatico = $this->metObtenerInt('idUnidadViatico');
        $estado = $this->metObtenerAlphaNumerico('estado');

        if ($valido == 1) {
            if($estado=='PR'){
                $id=$this->atUnidadViaticoModelo->metAprobatUnidadViatico($idUnidadViatico);
                $registro['Mensaje'] = array('titulo'=>'Aprobado','contenido'=>'La Unidad de Viatico fue aprobada Satisfactoriamente');
                $registro['idUnidadViatico'] = $id;
                $registro['status'] = 'OK';
                echo json_encode($registro);
                exit;
            }
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int');
            $txt = $this->metValidarFormArrayDatos('form', 'txt');
            if ($alphaNum != null && $ind == null && $txt==null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $registro = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $registro = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $registro = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $registro = array_merge($ind, $txt);
            }else{
                $registro = array_merge($ind, $txt,$alphaNum);
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }

            if ($idUnidadViatico == 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atUnidadViaticoModelo->metNuevaUnidadViatico($registro['ind_num_resolucion'],
                    $registro['ind_periodo'], $registro['fk_a006_num_miscelaneo_detalle_tviatico'],$registro['num_unidad_viatico'], $registro['ind_descripcion']);
                $registro['idUnidadViatico'] = $id;
                $idNuevo = $this->atUnidadViaticoModelo->metUnidadViaticoModeloListar(false, $registro['idUnidadViatico']);
            } else {
                $registro['status'] = 'modificar';
               $id = $this->atUnidadViaticoModelo->metModificarUnidadViatico($idUnidadViatico,$registro['ind_num_resolucion'],
                    $registro['ind_periodo'], $registro['fk_a006_num_miscelaneo_detalle_tviatico'],$registro['num_unidad_viatico'], $registro['ind_descripcion']);
                $registro['idUnidadViatico'] = $id;
                $idNuevo = $this->atUnidadViaticoModelo->metUnidadViaticoModeloListar(false, $idUnidadViatico);
            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($id);
                exit;
            }
            $idNuevo['status']=$registro['status'];
            echo json_encode($idNuevo);
            exit;
        }

        if ($idUnidadViatico != 0) {
            $this->atVista->assign('unidadViaBD', $this->atUnidadViaticoModelo->metConsultaUnidadViaticoModelo($idUnidadViatico));
        }
        if($estado!=''){
            $this->atVista->assign('estado', $estado);
        }

	      $TVIATICO = $this->atMiscelaneoModelo->metMostrarSelect('TVIATICO');
        $this->atVista->assign('tviatico',$TVIATICO);

        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idUnidadViatico', $idUnidadViatico);

        $this->atVista->metRenderizar('nuevo', 'modales');
    }


}
