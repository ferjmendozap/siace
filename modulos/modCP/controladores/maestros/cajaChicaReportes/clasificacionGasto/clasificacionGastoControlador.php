<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/

class clasificacionGastoControlador extends Controlador
{
    private $atClasificacionGastoModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atClasificacionGastoModelo = $this->metCargarModelo('clasificacionGasto');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);


        $this->atVista->assign('clasifGastoBD', $this->atClasificacionGastoModelo->metClasificacionGastoListar());
        $this->atVista->metRenderizar('listado');
    }

    public function metNuevaClasifGasto()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $ver = $this->metObtenerInt('ver');
        $idClasifGasto = $this->metObtenerInt('idClasifGasto');

        if ($valido == 1) {
            $Excepcion = array('num_estatus');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $Excepcion);

            if ($alphaNum != null && $ind == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $registro = $ind;
            } else {
                $registro = array_merge($ind, $alphaNum);
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if (!isset($registro['num_estatus'])) {
                $registro['num_estatus'] = 0;
            }

            if ($idClasifGasto == 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atClasificacionGastoModelo->metNuevaClasifGasto($registro);
                $registro['idClasifGasto'] = $id;
                $idNuevo = $this->atClasificacionGastoModelo->metClasificacionGastoListar($registro['idClasifGasto']);

            } else {
                $registro['status'] = 'modificar';
                $id = $this->atClasificacionGastoModelo->metModificarClasifGasto($idClasifGasto, $registro);
                $registro['idClasifGasto'] = $id;
                $idNuevo = $this->atClasificacionGastoModelo->metClasificacionGastoListar($idClasifGasto);

            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $idNuevo['status'] = $registro['status'];
            echo json_encode($idNuevo);
            exit;
        }
        if ($idClasifGasto != 0) {
            $this->atVista->assign('clasifGastoBD', $this->atClasificacionGastoModelo->metClasifGastoConsulta($idClasifGasto));
            $this->atVista->assign('clasifGastoDetalle', $this->atClasificacionGastoModelo->metClasifGastoConsultaDetalle($idClasifGasto));
            $this->atVista->assign('concepto', $this->atClasificacionGastoModelo->metMostrarConcepto($idClasifGasto));

        }
        $this->atVista->assign('selectAPLICG',$this->atClasificacionGastoModelo->atMiscelaneoModelo->metMostrarSelect('APLICG'));
        $this->atVista->assign('selectConceptoGasto',$this->atClasificacionGastoModelo->metConceptoGastoListar());


        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('idClasifGasto', $idClasifGasto);
        $this->atVista->metRenderizar('nuevo', 'modales');

    }
    public function metEliminar()
    {
        $idClasifGasto = $this->metObtenerInt('idClasifGasto');
        if($idClasifGasto!=0){
            $id=$this->atClasificacionGastoModelo->metEliminarClasifGasto($idClasifGasto);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Clasificacion de Gastos se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idClasifGasto'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

}