<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class autorizacionCajaChicaControlador extends Controlador
{
    private $atAutorizacionCajaChicaModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atAutorizacionCajaChicaModelo = $this->metCargarModelo('autorizacionCajaChica');
    }
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('autorizacionBD',$this->atAutorizacionCajaChicaModelo->metListarAutorizacionCC());
        $this->atVista->metRenderizar('listado');
    }

    public function metEmpleado($empleado)
    {
        $this->atVista->assign('lista', $this->atAutorizacionCajaChicaModelo->metListarEmpleado());
        $this->atVista->assign('listaEmpleado', $empleado );
        $this->atVista->metRenderizar('listadoEmpleado', 'modales');
    }

    
    public function metNuevaAutorizacion(){

        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $idAutorizacionCC = $this->metObtenerInt('idAutorizacionCC');
        $ver = $this->metObtenerInt('ver');
        $mod = $this->metObtenerInt('mod');
        $valido = $this->metObtenerInt('valido');

        if ($valido == 1) {
            $Excepcion = array('num_estatus');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $Excepcion,true);

            if ($alphaNum != null && $ind == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $registro = $ind;
            } else {
                $registro = array_merge($ind, $alphaNum);
            }
            if(in_array('datoInvalido', $registro)){
                $registro['num_monto_autorizado']='error';
                $registro['status'] = 'errorMonto';
                echo json_encode($registro);
                exit;
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if(!isset($registro['num_estatus'])){
                $registro['num_estatus']=0;
            }
            if ($idAutorizacionCC == 0) {

                $registro['status'] = 'nuevo';
                $id = $this->atAutorizacionCajaChicaModelo->metNuevaAutorizacion($registro['fk_rhb001_num_empleado'], $registro['num_monto_autorizado'], $registro['num_estatus']);
                $registro['idAutorizacionCC'] = $id;
                $idNuevo = $this->atAutorizacionCajaChicaModelo->metListarAutorizacionCC($registro['idAutorizacionCC']);

            }
            else {
                $registro['status'] = 'modificar';
                $id = $this->atAutorizacionCajaChicaModelo->metModificarAutorizacion($idAutorizacionCC, $registro['num_monto_autorizado'], $registro['num_estatus']);
                $registro['idAutorizacionCC'] = $id;
                $idNuevo = $this->atAutorizacionCajaChicaModelo->metListarAutorizacionCC($idAutorizacionCC);
            }

            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            $idNuevo['status'] = $registro['status'];
            echo json_encode($idNuevo);
            exit;
        }
        if ($idAutorizacionCC != 0) {

            $this->atVista->assign('autorizacionBD', $this->atAutorizacionCajaChicaModelo->metConsultaAutorizacionCC($idAutorizacionCC));

        }

        $this->atVista->assign('idAutorizacionCC', $idAutorizacionCC);
        $this->atVista->assign('ver', $ver);
        $this->atVista->assign('mod', $mod);
        $this->atVista->metRenderizar('nuevo', 'modales');
    }
    public function metEliminar()
    {
        $idAutorizacionCC = $this->metObtenerInt('idAutorizacionCC');
        if($idAutorizacionCC!=0){
            $id=$this->atAutorizacionCajaChicaModelo->metEliminarAutorizacion($idAutorizacionCC);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Autorizacion de Caja Chica se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idAutorizacionCC'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

}