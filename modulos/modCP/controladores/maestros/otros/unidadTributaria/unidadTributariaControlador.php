<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class unidadTributariaControlador extends Controlador
{
    private $atUnidadTributariaModelo;
    public function __construct()
	{
		parent::__construct();
        Session::metAcceso();
        $this->atUnidadTributariaModelo=$this->metCargarModelo('unidadTributaria');
	}

	public function metIndex()
	{
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atUnidadTributariaModelo->metListarUnidadTributaria());
        $this->atVista->metRenderizar('listado');
	}

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $ver = $this->metObtenerInt('ver');
        $idUnidadTributaria=$this->metObtenerInt('idUnidadTributaria');
        $valido=$this->metObtenerInt('valido');
        if($valido==1){
            $this->metValidarToken();
            $excepcion = array('ind_providencia_num');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $excepcion);
            $txt = $this->metValidarFormArrayDatos('form', 'txt');
            if ($alphaNum != null && $ind == null && $txt==null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $validacion = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            }else{
                $validacion = array_merge($ind, $txt,$alphaNum);
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if($idUnidadTributaria==0){
                $id=$this->atUnidadTributariaModelo->metCrearModificarUnidadTributaria($idUnidadTributaria,$validacion);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atUnidadTributariaModelo->metCrearModificarUnidadTributaria($idUnidadTributaria,$validacion);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idUnidadTributaria']=$id;
            echo json_encode($validacion);
            exit;

        }
        if($idUnidadTributaria!=0){
            $db=$this->atUnidadTributariaModelo->metMostrarUnidadTributaria($idUnidadTributaria);
            $tipo=$this->atUnidadTributariaModelo->metSeleccionarTipoUnidadTributaria($db['pk_num_miscelaneo_detalle']);

            $this->atVista->assign('formDB',$db);
            $this->atVista->assign('tipo',$tipo);
            $this->atVista->assign('ver',$ver);
        }
        $tipo=$this->atUnidadTributariaModelo->metMostrarTipoUnidadTributaria();

        $this->atVista->assign('tipo',$tipo);
        $this->atVista->assign('ver',$ver);
        $this->atVista->assign('idUnidadTributaria',$idUnidadTributaria);
        $this->atVista->metRenderizar('crear','modales');
    }
}
