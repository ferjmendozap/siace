<?php
/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/
class clasificacionDocumentosControlador extends Controlador
{
    private $atClasificacionDocumentosModelo;
    public function __construct()
	{
		parent::__construct();
        Session::metAcceso();
        $this->atClasificacionDocumentosModelo=$this->metCargarModelo('clasificacionDocumentos');
	}

	public function metIndex()
	{
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atClasificacionDocumentosModelo->metClasificacionDocumentosListar());
        $this->atVista->metRenderizar('listado');
	}

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $ver = $this->metObtenerInt('ver');
        $idClasifDocumentos=$this->metObtenerInt('idClasifDocumentos');
        $valido=$this->metObtenerInt('valido');
        if($valido==1){
            $this->metValidarToken();
            $excepcion = array('num_flag_item','num_flag_cliente','num_flag_compromiso','num_flag_transaccion','num_estatus');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $excepcion);
            if ($alphaNum != null && $ind == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null) {
                $validacion = $ind;
            } elseif($alphaNum != null && $ind != null) {
                $validacion = array_merge($alphaNum, $ind);
            } else{
                $validacion = array_merge($ind,$alphaNum);
            }
            if (!isset($validacion['num_flag_item'])) {
                $validacion['num_flag_item'] = '0';
            }
            if (!isset($validacion['num_flag_cliente'])) {
                $validacion['num_flag_cliente'] = '0';
            }
            if (!isset($validacion['num_flag_compromiso'])) {
                $validacion['num_flag_compromiso'] = '0';
            }
            if (!isset($validacion['num_flag_transaccion'])) {
                $validacion['num_flag_transaccion'] = '0';
            }
            if (!isset($validacion['num_estatus'])) {
                $validacion['num_estatus'] = '0';
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if($idClasifDocumentos == 0){
                $id=$this->atClasificacionDocumentosModelo->metNuevaClasifDocumentos($validacion);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atClasificacionDocumentosModelo->metModificarClasifDocumentos($idClasifDocumentos,$validacion);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idClasifDocumentos']=$id;
            echo json_encode($validacion);
            exit;

        }
        if($idClasifDocumentos!=0){
            $db=$this->atClasificacionDocumentosModelo->metConsultaClasificacionDocumentos($idClasifDocumentos);
            $this->atVista->assign('dataBD',$db);
        }
        $this->atVista->assign('ver',$ver);
        $this->atVista->assign('idClasifDocumentos',$idClasifDocumentos);
        $this->atVista->metRenderizar('crear','modales');
    }

    public function metEliminar()
    {
        $idClasifDocumentos = $this->metObtenerInt('idClasifDocumentos');
        if($idClasifDocumentos!=0){
            $id=$this->atClasificacionDocumentosModelo->metEliminarClasificacionDocumentos($idClasifDocumentos);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Clasificacion de Documentos se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idClasifDocumentos'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
    
}
