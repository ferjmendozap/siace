<?php

/****************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Cuentas Por Pagar
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELEFONO.
 * | 1 | Yohandry Alcoba      | y.alcoba@contraloriamonagas.gob.ve   | 0424-9417144
 * |_____________________________________________________________________________________
 *****************************************************************************************/


class cuentasControlador extends Controlador
{
    private $atCuentasModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atCuentasModelo = $this->metCargarModelo('cuentas');
    }
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('listado');
    }

    public function metBuscarTipoPago()
    {
        $idTipoPago = $this->metObtenerInt('idTipoPago');
        $consulta = $this->atCuentasModelo->metBuscarIdTipoPago($idTipoPago);
        echo json_encode($consulta);
        exit;
    }

    public function metNuevaCuenta()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'multi-select/jquery.multi-select',
            'bootstrap-tagsinput/bootstrap-tagsinput.min',
            'moment/moment.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'typeahead/typeahead.bundle.min',
            'dropzone/dropzone.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idCuenta = $this->metObtenerInt('idCuenta');
        $ver = $this->metObtenerInt('ver');

        if ($valido == 1){
            $Excepcion=array('num_flag_conciliacion_bancaria','num_flag_conciliacion_cp','num_flag_debito_bancario','num_estatus',
                'fk_cbb004_num_plan_cuenta','fk_cbb004_num_plan_cuenta_pub20');
            $ExcepcionFormula = array('int_numero_ultimo_cheque','pk_tipo_pago');
            $ExcepcionAlphaNum=array('ind_agencia','ind_distrito','ind_atencion','ind_cargo','ind_comentarios','num_saldo_inicial');

            $txt = $this->metValidarFormArrayDatos('form', 'txt');
            $ind = $this -> metValidarFormArrayDatos('form','int',$Excepcion,true);
            $alphaNum = $this -> metValidarFormArrayDatos('form','alphaNum',$ExcepcionAlphaNum);
            $formula = $this->metValidarFormArrayDatos('form', 'formula',$ExcepcionFormula);

            if ($alphaNum != null && $ind == null && $txt == null && $formula == null) {
                $registro = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null && $formula == null) {
                $registro = $ind;
            }elseif ($alphaNum == null && $ind == null && $txt != null && $formula == null) {
                $registro = $txt;
            }elseif ($alphaNum == null && $ind == null && $txt == null && $formula != null) {
                $registro = $formula;
            } elseif ($alphaNum != null && $ind != null && $txt == null && $formula == null) {
                $registro = array_merge($alphaNum, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null && $formula == null) {
                $registro = array_merge($alphaNum, $txt);
            } elseif ($alphaNum != null && $ind == null && $txt == null && $formula != null) {
                $registro = array_merge($alphaNum, $formula);
            } elseif ($alphaNum != null && $ind != null && $txt != null && $formula == null) {
                $registro = array_merge($alphaNum, $ind, $txt);
            } elseif ($alphaNum != null && $ind != null && $txt == null && $formula != null) {
                $registro = array_merge($alphaNum, $ind, $formula);
            } else {
                $registro = array_merge($ind, $txt, $alphaNum,$formula);
            }
            if(in_array('datoInvalido', $registro)){
                $registro['ind_num_cuenta'] = 'error';
                $registro['status'] = 'errorCuenta';
                echo json_encode($registro);
                exit;
            }
            if (in_array('error', $registro)) {
                $registro['status'] = 'error';
                echo json_encode($registro);
                exit;
            }
            if (!isset($registro['num_estatus'])) {
                $registro['num_estatus'] = 0;
            }
            if (!isset($registro['num_saldo_inicial'])) {
                $registro['num_saldo_inicial'] = 0;
            }
            if (!isset($registro['int_numero_ultimo_cheque'])) {
                $registro['int_numero_ultimo_cheque'] = 0;
            }
            if (!isset($registro['num_flag_conciliacion_bancaria'])) {
                $registro['num_flag_conciliacion_bancaria'] = 0;
            }
            if (!isset($registro['num_flag_conciliacion_cp'])) {
                $registro['num_flag_conciliacion_cp'] = 0;
            }
            if (!isset($registro['num_flag_debito_bancario'])) {
                $registro['num_flag_debito_bancario'] = 0;
            }
            if ($registro['fk_cbb004_num_plan_cuenta']==0) {
                $registro['fk_cbb004_num_plan_cuenta'] = null;
            }
            if ($registro['fk_cbb004_num_plan_cuenta_pub20']==0) {
                $registro['fk_cbb004_num_plan_cuenta_pub20'] = null;
            }
            if(!isset($registro['pk_num_cuenta_bancaria_tipo_pago'])){
                $registro['pk_num_cuenta_bancaria_tipo_pago']=null;
            }
            if(!isset($registro['pk_num_miscelaneo_detalle_tipo_pago'])){
                $registro['pk_num_miscelaneo_detalle_tipo_pago']=null;
            }
            if(!isset($registro['pk_tipo_pago'])){
                $registro['pk_tipo_pago']=null;
            }
            if(!isset($registro['numero'])){
                $registro['numero']=0;
            }
            if ($idCuenta == 0) {
                $registro['status'] = 'nuevo';
                $id = $this->atCuentasModelo->metNuevaCuenta($registro);
                $registro['idCuenta'] = $id;
            } else {
                $registro['status'] = 'modificar';
                $id = $this->atCuentasModelo->metModificarCuenta($idCuenta, $registro);
                $registro['idCuenta'] = $id;
            }
            if (is_array($id)) {
                foreach ($registro as $titulo => $valor) {
                    if(!is_array($registro[$titulo])){
                        if (strpos($id[2], $registro[$titulo])) {
                            $registro[$titulo] = 'error';
                        }
                    }
                }
                $registro['status'] = 'errorSQL';
                echo json_encode($registro);
                exit;
            }
            echo json_encode($registro);
            exit;
        }
        if ($idCuenta != 0) {
            $this->atVista->assign('cuentasBD', $this->atCuentasModelo->metCuentaConsulta($idCuenta));
            $this->atVista->assign('cuentasDetalle', $this->atCuentasModelo->metCuentaConsultaDetalle($idCuenta));
            $this->atVista->assign('tipoPago', $this->atCuentasModelo->metMostrarTipoPago($idCuenta));
        }

        $this->atVista->assign('selectBANCOS',$this->atCuentasModelo->atMiscelaneoModelo->metMostrarSelect('BANCOS'));
        $this->atVista->assign('selectTIPCUENTAB',$this->atCuentasModelo->atMiscelaneoModelo->metMostrarSelect('TIPCUENTAB'));
        $this->atVista->assign('selectdocPago',$this->atCuentasModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
        $this->atVista->assign('idCuenta',$idCuenta);
        $this->atVista->assign('ver',$ver);
        $this->atVista->metRenderizar('nuevo', 'modales');
    }

    public function metEliminar()
    {
       $idCuenta = $this->metObtenerInt('idCuenta');

        if($idCuenta!=0){
            $id=$this->atCuentasModelo->metEliminarCuenta($idCuenta);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Cuenta Bancaria se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idCuenta'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                b.ind_nombre_detalle AS banco,
                c.ind_descripcion,
                c.ind_num_cuenta,
                c.num_estatus,
                c.pk_num_cuenta
              FROM
                cp_b014_cuenta_bancaria AS c
              INNER JOIN  a006_miscelaneo_detalle AS b ON c.fk_a006_num_miscelaneo_detalle_banco = b.pk_num_miscelaneo_detalle
            ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        b.ind_nombre_detalle LIKE '%$busqueda[value]%' OR
                        c.ind_num_cuenta LIKE '%$busqueda[value]%' OR
                        c.ind_descripcion LIKE '%$busqueda[value]%' 
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('banco','ind_descripcion','ind_num_cuenta','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_cuenta';
        #construyo el listado de botones

        if (in_array('CP-01-07-02-01-01-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button class="modificar Cuenta btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idCuenta="'.$clavePrimaria.'" title="Editar"
                        descipcion="El Usuario ha Modificado una Cuenta Bancaria" titulo="Editar Cuenta Bancaria">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('CP-01-07-02-01-02-V',$rol)) {
            $campos['boton']['Ver'] = '
               <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" idCuenta="'.$clavePrimaria.'" title="Consultar"
                            descipcion="El Usuario esta viendo una Cuenta Bancaria" titulo="<i class=\'md md-remove-red-eye\'></i> Consultar Cuenta Bancaria">
                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('CP-01-07-02-01-03-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                 <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCuenta="'.$clavePrimaria.'" title="Eliminar"  boton="si, Eliminar"
                        descipcion="El usuario ha eliminado una Cuenta Bancaria" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar una Cuenta Bancaria!!">
                        <i class="md md-delete" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }
}