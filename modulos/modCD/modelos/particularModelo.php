<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 
 *****************************************************************************************************************************************/
 
 
require_once 'listaModelo.php';
class particularModelo extends listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoParticular($idParticular)
    {
        $tipoParticular = $this->_db->query("
          SELECT
		  	a008.ind_pais,
	a009.ind_estado,
	a011.ind_municipio,
	a010.ind_ciudad,
	a008.pk_num_pais,
	a009.pk_num_estado,
	a011.pk_num_municipio,
	a010.pk_num_ciudad,
          a003.*,
		  CONCAT(ind_nombre1,' ',ind_apellido1)  AS nombre_apellidos,
			a006.ind_nombre_detalle
			 FROM 
			 a003_persona  a003
				LEFT JOIN
	  a010_ciudad AS a010  on a003.fk_a010_num_ciudad=a010.pk_num_ciudad
			LEFT JOIN
	a014_ciudad_municipio AS  a014 on a014.fk_a010_num_ciudad=a010.pk_num_ciudad
		LEFT JOIN
		a011_municipio AS a011 on a014.fk_a011_num_municipio=a011.pk_num_municipio
		LEFT JOIN
		a009_estado AS a009 on a010.fk_a009_num_estado=a009.pk_num_estado
		LEFT JOIN
		a008_pais AS a008  on a009.fk_a008_num_pais=a008.pk_num_pais
		LEFT JOIN
            a006_miscelaneo_detalle a006 ON a006.pk_num_miscelaneo_detalle = a003.fk_a006_num_miscelaneo_det_tipopersona
         WHERE
            a003.pk_num_persona='$idParticular'
        ");
        $tipoParticular->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoParticular->fetch();
    }
	
	public function metListarTipoPart()
    {
        $dependencia = $this->_db->query(
            "SELECT 
			vl_rh.*,
			rh_c063.ind_descripcion_cargo AS cargoempleado,
			rh_c063.pk_num_puestos,
			a004.ind_dependencia,
			a004.pk_num_dependencia,
		  CONCAT(ind_nombre1,' ',ind_nombre2,' ',ind_apellido1,' ',ind_apellido2) AS nombre_particular
			FROM
			a004_dependencia a004 
 			LEFT JOIN
			vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.fk_a004_num_dependencia = a004.pk_num_dependencia 
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = vl_rh.fk_rhc063_num_puestos_cargo  
			 WHERE vl_rh.ind_estatus='1' GROUP BY vl_rh.pk_num_persona"
       );
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }
	
    public function metListarTipoParticular()
    {
        $tipoParticular = $this->_db->query(
            "SELECT 
			a003.*,
			a003.ind_nombre1,
			a003.ind_apellido1,
			a003.ind_nombre2,
			a003.ind_apellido2,
			CONCAT(ind_nombre1,'  ',ind_apellido1)  AS nombre_particular,
			a006.ind_nombre_detalle,
			a004.pk_num_dependencia,
			a004.ind_dependencia,
			rh_c006.pk_num_cargo,
			rh_c006.ind_nombre_cargo AS cargoempleado
			 FROM a003_persona  a003
             INNER JOIN rh_b001_empleado ON a003.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
			LEFT JOIN
            a006_miscelaneo_detalle a006 ON a006.pk_num_miscelaneo_detalle = a003.fk_a006_num_miscelaneo_det_tipopersona
			 LEFT JOIN
            a004_dependencia  a004 ON a004.fk_a003_num_persona_responsable =  a003.pk_num_persona
			INNER JOIN rh_c005_empleado_laboral on rh_b001_empleado.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado
LEFT JOIN rh_c063_puestos on rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo=rh_c063_puestos.pk_num_puestos
	LEFT JOIN rh_c006_tipo_cargo rh_c006 ON rh_c006.pk_num_cargo = rh_c063_puestos.fk_rhc006_num_cargo 
			GROUP BY a003.pk_num_persona
			"
        );
        $tipoParticular->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoParticular->fetchAll();
    }
	
	 public function metListarParticular()
    {
        $tipoParticular = $this->_db->query(
            "SELECT 
			a003.*,
			CONCAT(ind_nombre1,' ',ind_apellido1)  AS nombre_particular,
			a006.ind_nombre_detalle
			 FROM a003_persona  a003
			LEFT JOIN
            a006_miscelaneo_detalle a006 ON a006.pk_num_miscelaneo_detalle = a003.fk_a006_num_miscelaneo_det_tipopersona
			WHERE a006.cod_detalle IN ('PART','REP','OTRO','EMP') GROUP BY a003.pk_num_persona
			"
        );
        $tipoParticular->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoParticular->fetchAll();
    }
	
	 public function metListarTipo()
    {
        $tipoParticular = $this->_db->query(
            "SELECT * FROM cd_c010_particular
			"
        );
        $tipoParticular->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoParticular->fetchAll();
    }
	
	 public function metListarTipoParticulares()
    {
        $tipoParticular = $this->_db->query(
            "SELECT 
			a003.*,
			CONCAT(ind_nombre1,' ',ind_apellido1)  AS nombre_apellidos,
			a006.ind_nombre_detalle,
			a004.ind_dependencia,
			rh_c006.ind_nombre_cargo
			 FROM a003_persona  a003
			LEFT JOIN
            a006_miscelaneo_detalle a006 ON a006.pk_num_miscelaneo_detalle = a003.fk_a006_num_miscelaneo_det_tipopersona
			 LEFT JOIN
            a004_dependencia  a004 ON a004.fk_a003_num_persona_responsable =  a003.pk_num_persona
				LEFT JOIN
			rh_c006_tipo_cargo rh_c006 ON rh_c006.pk_num_cargo = a003.pk_num_persona
			 WHERE
            a006.pk_num_miscelaneo_detalle='3'"
        );
        $tipoParticular->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoParticular->fetchAll();
    }
	
    public function metCrearTipoParticular($cedula,$nombre1,$nombre2,$apellido1,$apellido2,$email,$codpersona,$status,$tipopersona,$ciudad)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    a003_persona
                  SET
				    fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
                    ind_cedula_documento=:ind_cedula_documento,ind_documento_fiscal=:ind_documento_fiscal,
					ind_nombre1=:ind_nombre1,ind_nombre2=:ind_nombre2,ind_apellido1=:ind_apellido1,ind_apellido2=:ind_apellido2,
					fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
					fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
					fec_nacimiento=:fec_nacimiento,ind_lugar_nacimiento=:ind_lugar_nacimiento,
					fk_a006_num_miscelaneo_detalle_edocivil=:fk_a006_num_miscelaneo_detalle_edocivil,fec_estado_civil=:fec_estado_civil,						
					ind_email=:ind_email, ind_foto=:ind_foto, ind_tipo_persona=:ind_tipo_persona,
					num_estatus=:num_estatus, fk_a006_num_miscelaneo_det_gruposangre=:fk_a006_num_miscelaneo_det_gruposangre,
					fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona, fk_a010_num_ciudad=:fk_a010_num_ciudad,
					fk_a013_num_sector=:fk_a013_num_sector
                ");
            $nuevoRegistro->execute(array(
                 'ind_cedula_documento'=>$cedula,
				'ind_documento_fiscal'=>NULL,
                'ind_nombre1'=>$nombre1,
				'ind_nombre2'=>$nombre2,
				'ind_apellido1'=>$apellido1,
				'ind_apellido2'=>$apellido2,
				'fk_a006_num_miscelaneo_detalle_sexo'=>NULL,
				'fk_a006_num_miscelaneo_detalle_nacionalidad'=>NULL,
				'fec_nacimiento'=>'0000-00-00',
				'ind_lugar_nacimiento'=>NULL,
				'fk_a006_num_miscelaneo_detalle_edocivil'=>NULL,
				'fec_estado_civil'=>'0000-00-00',
				'ind_email'=>$email,
				'ind_foto'=>NULL,
				'ind_tipo_persona'=>$codpersona,
				'num_estatus'=>$status,
				'fk_a006_num_miscelaneo_det_gruposangre'=>NULL,
				'fk_a006_num_miscelaneo_det_tipopersona'=>$tipopersona,
				'fk_a010_num_ciudad'=>$ciudad,
				'fk_a013_num_sector'=>NULL,
							
             ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarTipoParticular($cedula,$nombre1,$nombre2,$apellido1,$apellido2,$email,$codpersona,$status,$tipopersona,$ciudad,$idParticular)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        a003_persona
                      SET
				 	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
                    ind_cedula_documento=:ind_cedula_documento,ind_documento_fiscal=:ind_documento_fiscal,
					ind_nombre1=:ind_nombre1,ind_nombre2=:ind_nombre2,ind_apellido1=:ind_apellido1,ind_apellido2=:ind_apellido2,
					fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
					fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
					fec_nacimiento=:fec_nacimiento,ind_lugar_nacimiento=:ind_lugar_nacimiento,
					fk_a006_num_miscelaneo_detalle_edocivil=:fk_a006_num_miscelaneo_detalle_edocivil,fec_estado_civil=:fec_estado_civil,						
					ind_email=:ind_email, ind_foto=:ind_foto, ind_tipo_persona=:ind_tipo_persona,
					num_estatus=:num_estatus, fk_a006_num_miscelaneo_det_gruposangre=:fk_a006_num_miscelaneo_det_gruposangre,
					fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona, fk_a010_num_ciudad=:fk_a010_num_ciudad,
					fk_a013_num_sector=:fk_a013_num_sector
                      WHERE
                      pk_num_persona='$idParticular'
            ");
            $nuevoRegistro->execute(array(
                 'ind_cedula_documento'=>$cedula,
				'ind_documento_fiscal'=>NULL,
                'ind_nombre1'=>$nombre1,
				'ind_nombre2'=>$nombre2,
				'ind_apellido1'=>$apellido1,
				'ind_apellido2'=>$apellido2,
				'fk_a006_num_miscelaneo_detalle_sexo'=>NULL,
				'fk_a006_num_miscelaneo_detalle_nacionalidad'=>NULL,
				'fec_nacimiento'=>'0000-00-00',
				'ind_lugar_nacimiento'=>NULL,
				'fk_a006_num_miscelaneo_detalle_edocivil'=>NULL,
				'fec_estado_civil'=>'0000-00-00',
				'ind_email'=>$email,
				'ind_foto'=>NULL,
				'ind_tipo_persona'=>$codpersona,
				'num_estatus'=>$status,
				'fk_a006_num_miscelaneo_det_gruposangre'=>NULL,
				'fk_a006_num_miscelaneo_det_tipopersona'=>$tipopersona,
				'fk_a010_num_ciudad'=>$ciudad,
				'fk_a013_num_sector'=>NULL,
			
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idParticular;
            }
    }

 public function metEliminarTipoParticular($idParticular)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM a003_persona WHERE pk_num_persona=:pk_num_persona
            ");
            $elimar->execute(array(
                'pk_num_persona'=>$idParticular
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idParticular;
            }

    }
	
    

  
}
