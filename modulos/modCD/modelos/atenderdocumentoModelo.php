<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Atender Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/


require_once 'listaModelo.php';
class atenderdocumentoModelo extends listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoDocumento($idDocumento)
    {
        $tipoDocumento = $this->_db->query(" 
         SELECT
            cd_c004.*,
			a003.ind_cedula_documento,
			persona.ind_documento_fiscal,
			a018.ind_usuario,
			a039.pk_num_ente,
			dep.pk_num_ente,
			dep.ind_nombre_ente AS Dependencia,
			a039.ind_nombre_ente AS Organismo,
		  CONCAT(a003.ind_nombre1,'  ',a003.ind_apellido1)  AS nombre_apellidos,
		   concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nomProveedor
          FROM
            cd_c004_documento_entrada cd_c004
			 INNER JOIN
            a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cd_c004.fk_a018_num_seguridad_usuario
			 LEFT JOIN
           a039_ente  dep ON dep.pk_num_ente =  cd_c004.num_depend_ext
		    LEFT JOIN
           a039_ente  a039 ON a039.pk_num_ente =  cd_c004.num_org_ext
			LEFT JOIN		
		a003_persona a003 ON a003.pk_num_persona = cd_c004.num_particular_rem 
		
		LEFT JOIN a003_persona AS persona ON persona.pk_num_persona =  cd_c004.num_empresa
		  WHERE
            cd_c004.pk_num_documento='$idDocumento'
        ");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }

    public function metListarTipoDocumento()
    {
        $tipoDocumento = $this->_db->query(
            "SELECT 
			cd_c004.*,
			a001.ind_descripcion_empresa,
			a021.ind_descripcion
			FROM cd_c004_documento_entrada cd_c004
			 LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c004.num_org_ext
			  LEFT JOIN
            a021_dependencia_ext  a021 ON a021.pk_num_dependencia_ext =  cd_c004.num_depend_ext
			WHERE
            cd_c004.ind_estado='PE' ORDER BY cd_c004.pk_num_documento DESC "
			
       );
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }
	
	public function metJsonCentroCosto($dependencia)
    {

        $centros = $this->_db->query(
            "SELECT
              pk_num_ente,
              ind_nombre_ente
            FROM
              a039_ente
            WHERE
              num_ente_padre ='$dependencia' AND num_estatus=1
            ");
        $centros->setFetchMode(PDO::FETCH_ASSOC);
        return $centros->fetchAll();

    }
	
	    public function metListarOrganismoEnte()
    {
        $menu = $this->_db->query("SELECT pk_num_ente, ind_nombre_ente FROM a039_ente where num_ente_padre=0");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
		
    }
	
	
	  // M�todo que permite listar los centros de costos
    public function metListarCentroCosto()
    {
        $centroCosto =  $this->_db->query(
            "SELECT pk_num_ente, ind_nombre_ente FROM a039_ente where num_ente_padre=2"
        );
        $centroCosto->setFetchMode(PDO::FETCH_ASSOC);
        return $centroCosto->fetchAll();
    }

    public function metModificarTipoDocumento(																		
																				$informescrito,$hablarconmigo,$coordcon,$prepmemoran,
																				$investigarinform,$tramitarconclusion,$distribuir,$conocimiento,
																				$preparcontest,$archivar,$registrode,$prepaoficio,
																				$conoceropin,$tramitarcaso,$acusarecibo,$tramitaren,
																				$observacion,$persondest,$dependest,$descripdep,$descriprep,$cargoextern,$idDocumento)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c004_documento_entrada
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), ind_estado=:ind_estado 
                      WHERE
                        pk_num_documento='$idDocumento'
            ");
   	         $nuevoRegistro->execute(array(
                'ind_estado'=>'RE'
            ));

		
			$nuevoRegistro=$this->_db->prepare("
                    INSERT INTO
                    cd_c006_entrada_detalle
                  SET
				  	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), 
					ind_informe_escrito=:ind_informe_escrito,  ind_hablar_conmigo=:ind_hablar_conmigo, 
					ind_coordinar_con=:ind_coordinar_con , ind_preparar_memorandum=:ind_preparar_memorandum,
					ind_investigar_informar=:ind_investigar_informar, ind_tramitar_conclusion=:ind_tramitar_conclusion,
					ind_distribuir=:ind_distribuir , ind_conocimiento=:ind_conocimiento,
					ind_preparar_constentacion=:ind_preparar_constentacion, ind_archivar=:ind_archivar, 
					ind_registro_de=:ind_registro_de , ind_preparar_oficio=:ind_preparar_oficio,
					ind_conocer_opinion=:ind_conocer_opinion, ind_tramitar_caso=:ind_tramitar_caso, 
					ind_acusar_recibo=:ind_acusar_recibo , ind_tramitar_en=:ind_tramitar_en,  ind_observacion=:ind_observacion,  ind_estado=:ind_estado,
					fk_cdc004_num_documento='$idDocumento'
					
                ");

              $nuevoRegistro->execute(array(
                'ind_informe_escrito'=>$informescrito,
                'ind_hablar_conmigo'=>$hablarconmigo,
                'ind_coordinar_con'=>$coordcon,
                'ind_preparar_memorandum'=>$prepmemoran,
				'ind_investigar_informar'=>$investigarinform,
                'ind_tramitar_conclusion'=>$tramitarconclusion,
                'ind_distribuir'=>$distribuir,
                'ind_conocimiento'=>$conocimiento,
				'ind_preparar_constentacion'=>$preparcontest,
                'ind_archivar'=>$archivar,
                'ind_registro_de'=>$registrode,
				'ind_preparar_oficio'=>$prepaoficio,
                'ind_conocer_opinion'=>$conoceropin,
                'ind_tramitar_caso'=>$tramitarcaso,
				'ind_acusar_recibo'=>$acusarecibo,
				'ind_tramitar_en'=>$tramitaren,
			 	'ind_observacion'=>$observacion,
				'ind_estado'=>'RE'
            ));
            
			
			$nuevoRegistro=$this->_db->prepare("
                    INSERT INTO
                    cd_c005_distribucion_entrada
                  SET
				  	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),ind_persona_destinataria=:ind_persona_destinataria , 
					ind_dependencia_destinataria=:ind_dependencia_destinataria ,
					ind_desc_dependencia=:ind_desc_dependencia ,
					ind_desc_representante=:ind_desc_representante,
					ind_cargo_destinatario=:ind_cargo_destinatario,
					fec_envio=:fec_envio, fec_acuse=:fec_acuse,  ind_estado=:ind_estado, fk_cdc004_num_documento='$idDocumento'
					
                ");
	
			  if($dependest){
			foreach($dependest AS $i){
				$nuevoRegistro->execute(array(
				'ind_persona_destinataria'=>'',
                'ind_dependencia_destinataria'=>$i,
				'ind_desc_dependencia'=>$descripdep[$i],
				'ind_desc_representante'=>$descriprep[$i],
				'ind_cargo_destinatario'=>$cargoextern[$i],
                'fec_envio'=>date('Y-m-d H:i:s'),
				'ind_estado'=>'EV',
                'fec_acuse'=>'0000-00-00 00:00:00',
            	));	
			}			
		  }
		  
			    if($persondest){
			foreach($persondest AS $i){
				$nuevoRegistro->execute(array(
				'ind_persona_destinataria'=>$i,
                'ind_dependencia_destinataria'=>'',
				'ind_desc_dependencia'=>$descripdep[$i],
				'ind_desc_representante'=>$descriprep[$i],
				'ind_cargo_destinatario'=>$cargoextern[$i],
                'fec_envio'=>date('Y-m-d H:i:s'),
				'ind_estado'=>'EV',
                'fec_acuse'=>'0000-00-00 00:00:00',
            	));	
			}			
		  }

            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }

    }

}
