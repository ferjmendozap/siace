<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Preparar Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                     |a.hurtado@contraloriadebolivar.gob.ve   |         0416-6867297           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'listaModelo.php';
class preparardocumenextModelo extends listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoDocumento($idDocumento)
    {
        $tipoDocumento = $this->_db->query(" 
               SELECT
            cd_c011.*,
			cd_c012.*,
			a018.ind_usuario,
			rh_c063.ind_descripcion_cargo,
			rh_c063.pk_num_puestos,
			a004.ind_dependencia,
		  CONCAT(ind_nombre1,'   ',ind_apellido1)  AS nombre_apellidos
          FROM
            cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			INNER JOIN
			a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cd_c011.fk_a018_num_seguridad_usuario
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_c011.ind_persona_remitente
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = cd_c011.ind_cargo_remitente
		  WHERE
            cd_c011.pk_num_documento='$idDocumento'
        ");
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	
	
    public function metListarTipoDocumento($usuario)
    {
        $tipoDocumento = $this->_db->query(
               "SELECT 
			cd_c011.*,
			a001.ind_descripcion_empresa,
			a004.ind_dependencia,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM cd_c011_documento_salida cd_c011
			 LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c011.fk_a001_num_organismo
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			   LEFT JOIN 
		  a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c011.fk_a004_num_dependencia
			 WHERE
            cd_c011.ind_estado='PR'  and a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario
             AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
             ORDER BY cd_c011.pk_num_documento DESC

		");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }
		

    public function metModificarTipoDocumento($contenido,$mediafirma,$status,$idDocumento)
																				
	
    {
	
        $this->_db->beginTransaction();
		//var_dump($this->atTipoProceso->metMostrarTipoProceso($idProceso)); 
			//exit;
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c011_documento_salida
                      SET
                	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), txt_contenido=:txt_contenido,
					ind_media_firma=:ind_media_firma, ind_estado=:ind_estado
                      WHERE
                    pk_num_documento='$idDocumento'
            ");
				
   	         $nuevoRegistro->execute(array(
				'txt_contenido'=>$contenido,
                'ind_media_firma'=>$mediafirma,
				'ind_estado'=>'PP',

            ));
            
			
			 $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c012_distribucion_salida
                      SET
                	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), ind_estado=:ind_estado
                      WHERE
                    fk_cdc011_num_documento='$idDocumento'
            ");
			
			$nuevoRegistro->execute(array(
				'ind_estado'=>'PE',
           ));
	
	
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
    }

}
