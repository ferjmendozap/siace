<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Entrada documento detalle
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class entradadetModelo extends Modelo
{
        public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarCorrespondencia($idCorrespondencia)
    {
        $tipoCorrespondencia = $this->_db->query("
          SELECT
               cd_c006.*,
            a018.ind_usuario
          FROM
            cd_c006_entrada_detalle  cd_c006
			 INNER JOIN
            a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario =  cd_c006.fk_a018_num_seguridad_usuario
         WHERE
            cd_c006.pk_documento_detalle='$idCorrespondencia'
        ");
        $tipoCorrespondencia->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCorrespondencia->fetch();
    }

    public function metListarCorrespondencia()
    {
        $tipoCorrespondencia = $this->_db->query(
            "SELECT * FROM cd_c006_entrada_detalle"
        );
        $tipoCorrespondencia->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCorrespondencia->fetchAll();
    }

    public function metCrearCorrespondencia($informescrito,$hablarconmigo,$coordcon,$prepmemoran,
																				$investigarinform,$tramitarconclusion,$distribuir,$conocimiento,
																				$preparcontest,$archivar,$registrode,$prepaoficio,
																				$conoceropin,$tramitarcaso,$acusarecibo,$tramitaren,
																				$observacion)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                   INSERT INTO
                    cd_c006_entrada_detalle
                  SET
				  	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), 
					ind_informe_escrito=:ind_informe_escrito,  ind_hablar_conmigo=:ind_hablar_conmigo, 
					ind_coordinar_con=:ind_coordinar_con , ind_preparar_memorandum=:ind_preparar_memorandum,
					ind_investigar_informar=:ind_investigar_informar, ind_tramitar_conclusion=:ind_tramitar_conclusion,
					ind_distribuir=:ind_distribuir , ind_conocimiento=:ind_conocimiento,
					ind_preparar_constentacion=:ind_preparar_constentacion, ind_archivar=:ind_archivar, 
					ind_registro_de=:ind_registro_de , ind_preparar_oficio=:ind_preparar_oficio,
					ind_conocer_opinion=:ind_conocer_opinion, ind_tramitar_caso=:ind_tramitar_caso, 
					ind_acusar_recibo=:ind_acusar_recibo , ind_tramitar_en=:ind_tramitar_en,  ind_observacion=:ind_observacion,  ind_estado=:ind_estado,  
					fk_cdc004_num_documento=:fk_cdc004_num_documento
                ");

              $nuevoRegistro->execute(array(
                'ind_informe_escrito'=>$informescrito,
                'ind_hablar_conmigo'=>$hablarconmigo,
                'ind_coordinar_con'=>$coordcon,
                'ind_preparar_memorandum'=>$prepmemoran,
				'ind_investigar_informar'=>$investigarinform,
                'ind_tramitar_conclusion'=>$tramitarconclusion,
                'ind_distribuir'=>$distribuir,
                'ind_conocimiento'=>$conocimiento,
				'ind_preparar_constentacion'=>$preparcontest,
                'ind_archivar'=>$archivar,
                'ind_registro_de'=>$registrode,
				'ind_preparar_oficio'=>$prepaoficio,
                'ind_conocer_opinion'=>$conoceropin,
                'ind_tramitar_caso'=>$tramitarcaso,
				'ind_acusar_recibo'=>$acusarecibo,
				'ind_tramitar_en'=>$tramitaren,
			 	'ind_observacion'=>$observacion,
				'ind_estado'=>'RE',
              	'fk_cdc004_num_documento'=> '2'
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarCorrespondencia($informescrito,$hablarconmigo,$coordcon,$prepmemoran,
																				$investigarinform,$tramitarconclusion,$distribuir,$conocimiento,
																				$preparcontest,$archivar,$registrode,$prepaoficio,
																				$conoceropin,$tramitarcaso,$acusarecibo,$tramitaren,
																				$observacion,$idCorrespondencia)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c006_entrada_detalle
                      SET
                         fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), 
					ind_informe_escrito=:ind_informe_escrito,  ind_hablar_conmigo=:ind_hablar_conmigo, 
					ind_coordinar_con=:ind_coordinar_con , ind_preparar_memorandum=:ind_preparar_memorandum,
					ind_investigar_informar=:ind_investigar_informar, ind_tramitar_conclusion=:ind_tramitar_conclusion,
					ind_distribuir=:ind_distribuir , ind_conocimiento=:ind_conocimiento,
					ind_preparar_constentacion=:ind_preparar_constentacion, ind_archivar=:ind_archivar, 
					ind_registro_de=:ind_registro_de , ind_preparar_oficio=:ind_preparar_oficio,
					ind_conocer_opinion=:ind_conocer_opinion, ind_tramitar_caso=:ind_tramitar_caso, 
					ind_acusar_recibo=:ind_acusar_recibo , ind_tramitar_en=:ind_tramitar_en,  ind_observacion=:ind_observacion,  ind_estado=:ind_estado,  
					fk_cdc004_num_documento=:fk_cdc004_num_documento
                      WHERE
                        pk_documento_detalle='$idCorrespondencia'
            ");
            $nuevoRegistro->execute(array(
                  'ind_informe_escrito'=>$informescrito,
                'ind_hablar_conmigo'=>$hablarconmigo,
                'ind_coordinar_con'=>$coordcon,
                'ind_preparar_memorandum'=>$prepmemoran,
				'ind_investigar_informar'=>$investigarinform,
                'ind_tramitar_conclusion'=>$tramitarconclusion,
                'ind_distribuir'=>$distribuir,
                'ind_conocimiento'=>$conocimiento,
				'ind_preparar_constentacion'=>$preparcontest,
                'ind_archivar'=>$archivar,
                'ind_registro_de'=>$registrode,
				'ind_preparar_oficio'=>$prepaoficio,
                'ind_conocer_opinion'=>$conoceropin,
                'ind_tramitar_caso'=>$tramitarcaso,
				'ind_acusar_recibo'=>$acusarecibo,
				'ind_tramitar_en'=>$tramitaren,
			 	'ind_observacion'=>$observacion,
				'ind_estado'=>'RE',
              	'fk_cdc004_num_documento'=> '2',
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idCorrespondencia;
            }
    }

    public function metEliminarCorrespondencia($idCorrespondencia)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM cd_c006_entrada_detalle WHERE pk_documento_detalle=:pk_documento_detalle
            ");
            $elimar->execute(array(
                'pk_documento_detalle'=>$idCorrespondencia
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idCorrespondencia;
            }

    }

}
