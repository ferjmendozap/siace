<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Dependencias Externas
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                   | ahurtado@contraloriadebolivar.gob.ve   |         0416-6867297           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        12-11-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 
require_once 'listaModelo.php';
class dependenciaModelo extends listaModelo
{
	
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoDependencia($idDependencia)
    {
        $dependencia = $this->_db->query(" 
          SELECT
		  a021.*,
		  a022.*,
		  CONCAT(ind_nombre1,'  ',ind_apellido1)  AS nombre_apellidos
          FROM
		a021_dependencia_ext AS a021
           INNER JOIN
        a022_dependencia_extdetalle AS a022 on a021.pk_num_dependencia_ext=a022.fk_a021_num_dependencia_ext
			INNER JOIN
		a003_persona a003 ON a003.pk_num_persona = a022.fk_a003_num_persona_representante
         WHERE
            pk_num_dependencia_ext='$idDependencia'
        ");
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetch();
    }

    public function metListarTipoDependencia()
    {
        $dependencia = $this->_db->query(
            "SELECT 
		a021.*,
		a022.*,
		  a003.ind_nombre1,
		  a003.ind_nombre2,
		  a003.ind_apellido1,
		  a003.ind_apellido2,
		  CONCAT(vl_rh.ind_nombre1,'  ',vl_rh.ind_nombre2,'  ',,'  ',vl_rh.ind_apellido1,'  ',vl_rh.ind_apellido2) AS nombre_apellidos
		 FROM a021_dependencia_ext AS a021
        LEFT JOIN
        a022_dependencia_extdetalle AS a022 on a021.pk_num_dependencia_ext=a022.fk_a021_num_dependencia_ext
		INNER JOIN
		a003_persona a003 ON a003.pk_num_persona = a022.fk_a003_num_persona_representante"
       );
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }
	
	public function metListarDepExt()
    {
        $dependint= $this->_db->query("SELECT * FROM a039_ente WHERE
             num_ente_padre!='0' AND ind_nombre_ente!='Particular'");
        $dependint->setFetchMode(PDO::FETCH_ASSOC);
        return $dependint->fetchAll();
		
    }

    public function metListarTipoDependenciaInt()
    {
        $dependencia = $this->_db->query(
            "SELECT 
			vl_rh.*,
			rh_c063.ind_descripcion_cargo AS cargodepen,
			rh_c063.pk_num_puestos,
			a004.ind_dependencia,
			a004.pk_num_dependencia,
		  CONCAT(vl_rh.ind_nombre1,' ',vl_rh.ind_nombre2,' ',vl_rh.ind_apellido1,' ',vl_rh.ind_apellido2)  AS nombre_apellidos
			FROM
			a004_dependencia a004 
 			LEFT JOIN
			vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.pk_num_persona = a004.fk_a003_num_persona_responsable 
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = vl_rh.fk_rhc063_num_puestos_cargo  
			 WHERE vl_rh.ind_estatus='1' GROUP BY a004.pk_num_dependencia"
        );
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        $resp= $dependencia->fetchAll();
        $dependencia2 = $this->_db->query(
            "SELECT 
                  vl_rh.*,
                   rh_c063.ind_descripcion_cargo AS cargodepen, 
                   rh_c063.pk_num_puestos,
                    a004.ind_dependencia, 
                   a004.pk_num_dependencia, 
                   CONCAT(vl_rh.ind_nombre1,' ',vl_rh.ind_nombre2,' ',vl_rh.ind_apellido1,' ',vl_rh.ind_apellido2) AS nombre_apellidos 
                   FROM a004_dependencia a004 
                   LEFT JOIN vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.fk_a004_num_dependencia = a004.pk_num_dependencia 
                   LEFT JOIN rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = vl_rh.fk_rhc063_num_puestos_cargo 
                   WHERE vl_rh.ind_estatus='1' 
                   and a004.fk_a003_num_persona_responsable=0
                    and a004.num_estatus=1 "
        );
        $dependencia2->setFetchMode(PDO::FETCH_ASSOC);
        $resp2= $dependencia2->fetchAll();
        $allDependencia=array_merge($resp,$resp2);
        return $allDependencia;


    }
	
	public function metListarDependenciaExt()
    {
        $organismo = $this->_db->query(
            "SELECT a039.*, CONCAT_WS(' ',a003.ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombre_apellidos,
 a006.ind_nombre_detalle as ind_cargo_personal_externo
            FROM a039_ente a039  
 LEFT JOIN 
a041_persona_ente a041 ON a041.fk_a039_num_ente=a039.pk_num_ente
          LEFT JOIN    
a003_persona a003   ON a041.fk_a003_num_persona_titular=a003.pk_num_persona
			 LEFT JOIN a006_miscelaneo_detalle a006 on a006.pk_num_miscelaneo_detalle=a041.fk_a006_num_miscdet_cargo_pers


            WHERE  num_ente_padre!='0' "
       );
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetchAll();
    }
	
	
    public function metCrearTipoDependencia($documenfiscal,$descripcion,$status,$organismo,$cargo,$direccion,$numpersona)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                     a021_dependencia_ext
                  SET
                    ind_documento_fiscal=:ind_documento_fiscal,
                    num_estatus=:num_estatus, ind_descripcion=:ind_descripcion , fk_a001_num_organismo=:fk_a001_num_organismo
                ");
            $nuevoRegistro->execute(array(
                'ind_documento_fiscal'=>$documenfiscal,
                'num_estatus'=>$status,
                'ind_descripcion'=>$descripcion,
                'fk_a001_num_organismo'=>$organismo
            ));
			
		  $idDependencia=$this->_db->lastInsertId();
			
             $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                     a022_dependencia_extdetalle
                  SET
                    ind_cargo_representante=:ind_cargo_representante, ind_direccion=:ind_direccion,
                    fk_a021_num_dependencia_ext='$idDependencia', fk_a003_num_persona_representante=:fk_a003_num_persona_representante
                ");
            $nuevoRegistro->execute(array(
                'ind_cargo_representante'=>$cargo,
                'ind_direccion'=>$direccion,
                'fk_a003_num_persona_representante'=>$numpersona
             ));


            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarTipoDependencia($documenfiscal,$descripcion,$status,$organismo,$cargo,$direccion,$numpersona,$idDependencia)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        a021_dependencia_ext
                      SET
                        ind_documento_fiscal=:ind_documento_fiscal,
                        ind_descripcion=:ind_descripcion , num_estatus=:num_estatus, fk_a001_num_organismo=:fk_a001_num_organismo
                      WHERE
                        pk_num_dependencia_ext='$idDependencia'
            ");
            $nuevoRegistro->execute(array(
                'ind_documento_fiscal'=>$documenfiscal,
                'num_estatus'=>$status,
                'ind_descripcion'=>$descripcion,
                'fk_a001_num_organismo'=>$organismo
            ));

            
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        a022_dependencia_extdetalle
                      SET
                        ind_cargo_representante=:ind_cargo_representante, ind_direccion=:ind_direccion,
                   fk_a003_num_persona_representante=:fk_a003_num_persona_representante
                      WHERE
                        fk_a021_num_dependencia_ext='$idDependencia'
            ");
            $nuevoRegistro->execute(array(
                'ind_cargo_representante'=>$cargo,
                'ind_direccion'=>$direccion,
                'fk_a003_num_persona_representante'=>$numpersona
            ));
            
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDependencia;
            }
    }

    public function metEliminarTipoDependencia($idDependencia)	
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE  FROM a021_dependencia_ext WHERE pk_num_dependencia_ext=:pk_num_dependencia_ext
            ");
            $elimar->execute(array(
                'pk_num_dependencia_ext'=>$idDependencia
            ));
			
			  $elimar=$this->_db->prepare("
                DELETE FROM a022_dependencia_extdetalle  WHERE fk_a021_num_dependencia_ext=:fk_a021_num_dependencia_ext
            ");
            $elimar->execute(array(
                'fk_a021_num_dependencia_ext'=>$idDependencia
            ));
			
			$error=$elimar->errorInfo();
			
			 if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDependencia;
            }

    }

}
