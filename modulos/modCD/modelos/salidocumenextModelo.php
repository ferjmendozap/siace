<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Salida de Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'listaModelo.php';
class salidocumenextModelo extends listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoDocumento($idDocumento)
    {
        $tipoDocumento = $this->_db->query(" 
               SELECT
            cd_c011.*,
			cd_c012.*,
			a018.ind_usuario,
			DATE_FORMAT( cd_c011.fec_documento  ,'%d de %M de %Y') as fec_salida,
			rh_c063.ind_descripcion_cargo,
			rh_c063.pk_num_puestos,
			a004.ind_dependencia,
		  CONCAT(a003.ind_nombre1,'  ',a003.ind_apellido1)  AS nombre_apellidos,
		  CONCAT(p.ind_nombre1,'  ',p.ind_apellido1)  AS persona_anula
          FROM
            cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			INNER JOIN
			a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cd_c011.fk_a018_num_seguridad_usuario
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.ind_dependencia_remitente
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_c011.ind_persona_remitente 
			LEFT JOIN
			a003_persona p ON p.pk_num_persona = cd_c011.ind_persona_anulado 
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = cd_c011.ind_cargo_remitente  
		  WHERE
            cd_c011.pk_num_documento='$idDocumento'
        ");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	
	 public function metMostrarOrganismosDetalle($idDocumento)
    {
        $detalle = $this->_db->query(
            "SELECT
            cd_c011.*,
			cd_c012.*,
			cd_c003.ind_descripcion,
			a039.ind_nombre_ente AS ind_descripcion_empresa
          FROM
            cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			LEFT JOIN
			a039_ente a039  ON a039.pk_num_ente=cd_c012.ind_organismo_externo
				LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento  
            WHERE fk_cdc011_num_documento='$idDocumento'"
        );
        $detalle->setFetchMode(PDO::FETCH_ASSOC);
        return $detalle->fetchAll();
    }
	
	 public function metMostrarDepenenciaDetalle($idDocumento)
    {
        $detalle = $this->_db->query(
            "SELECT
            cd_c011.*,
			cd_c012.*,
			cd_c003.ind_descripcion,
			a039.ind_nombre_ente AS descipDependencia
          FROM
            cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			LEFT JOIN
			a039_ente a039  ON a039.pk_num_ente=cd_c012.ind_dependencia_externa
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento  
            WHERE fk_cdc011_num_documento='$idDocumento'"
        );
        $detalle->setFetchMode(PDO::FETCH_ASSOC);
        return $detalle->fetchAll();
    }
	
	public function metMostrarProveedorDetalle($idDocumento)
    {
		  
        $detalle = $this->_db->query(
            "SELECT
            cd_c011.*,
			cd_c012.*,
			concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nomProveedor,
			concat_ws(' ',p.ind_nombre1,p.ind_nombre2,p.ind_apellido1,p.ind_apellido2) AS nomRepresentante,
			persona.ind_documento_fiscal,
              persona.num_estatus
          FROM
            cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			 LEFT JOIN
            lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor =  cd_c012.ind_empresa_externa
			 INNER JOIN 
			 a003_persona AS persona ON persona.pk_num_persona =  proveedor.fk_a003_num_persona_proveedor
		  INNER JOIN 
		  a003_persona AS p ON p.pk_num_persona =  proveedor.fk_a003_num_persona_representante
            WHERE fk_cdc011_num_documento='$idDocumento'"
        );
        $detalle->setFetchMode(PDO::FETCH_ASSOC);
        return $detalle->fetchAll();
    }
	
	
	
	public function metListaRepresentantExt($usuario)
    {
        $persona = $this->_db->query(
            "SELECT 
			vl_rh.*,
			rh_c063.ind_descripcion_cargo,
			rh_c063.pk_num_puestos,
			a004.ind_dependencia,
			a004.ind_codinterno,
			a004.pk_num_dependencia,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias,
		  CONCAT(vl_rh.ind_nombre1,'   ',vl_rh.ind_apellido1)  AS nombre_apellidos
			FROM
			a004_dependencia a004 
 			INNER JOIN
			vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.pk_num_persona = a004.fk_a003_num_persona_responsable 
			INNER JOIN 
		  a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=a004.pk_num_dependencia
			INNER JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = vl_rh.fk_rhc063_num_puestos_cargo  
			 WHERE vl_rh.ind_estatus='1' 
			 AND  a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario 
			 AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
			 GROUP BY a004.pk_num_dependencia
			"
		);
        $persona->setFetchMode(PDO::FETCH_ASSOC); 
		
        return $persona->fetchAll();
    }
	
	
	 public function metMostrarRepresentDetalle($idDocumento)
    {
        $detalle = $this->_db->query(
            " SELECT
            cd_c011.*,
			cd_c012.*,
			rh_c006.pk_num_cargo,
			rh_c006.ind_nombre_cargo AS cargorep,
			a003.ind_cedula_documento,
		  CONCAT(a003.ind_nombre1,'   ',a003.ind_apellido1)  AS nombre_particular
          FROM
            cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_c012.flag_r 
			LEFT JOIN
			rh_c006_tipo_cargo rh_c006 ON rh_c006.pk_num_cargo = a003.pk_num_persona  
            WHERE fk_cdc011_num_documento='$idDocumento'"
        );
        $detalle->setFetchMode(PDO::FETCH_ASSOC);
        return $detalle->fetchAll();
    }

	public function metListarDodumentSald()
    {
        $DodumentSal= $this->_db->query("SELECT * FROM cd_c011_documento_salida");
        $DodumentSal->setFetchMode(PDO::FETCH_ASSOC);
        return $DodumentSal->fetchAll();
		
    }

    public function metListarTipoDocumento($usuario)
    {
        $tipoDocumento = $this->_db->query(
               "SELECT 
            cd_c012.ind_estado,
			cd_c011.pk_num_documento,cd_c011.num_cod_interno,cd_c011.fk_a004_num_dependencia,cd_c011.ind_asunto,cd_c011.fk_cdc003_num_tipo_documento,cd_c011.fec_documento,cd_c011.num_secuencia,cd_c011.fec_annio,
			cd_c011.txt_descripcion,a001.ind_descripcion_empresa,
			a004.ind_dependencia,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM cd_c011_documento_salida cd_c011
			LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c011.fk_a001_num_organismo
			LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.ind_dependencia_remitente
			LEFT JOIN 
		    a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c011.fk_a004_num_dependencia
            LEFT JOIN cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento=cd_c011.pk_num_documento
            WHERE
            cd_c011.ind_estado!='PE' and a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario 
            AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
            ORDER BY cd_c011.pk_num_documento DESC

            
		");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }

	// traer el ultimo numero secuencial del tipo de documento---
	
		 public function metContar($tabla,$anio,$campo,$codigodep,$campo3,$codigocorresp,$campo2)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              max(num_secuencia) AS cantidad
            FROM
            $tabla
            WHERE
            $campo='$anio'  AND $campo2='$codigocorresp' 
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }


    public function metContar2($idDocumento)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              max(num_secuencia) AS cantidad2
            FROM
            cd_c011_documento_salida
            WHERE
            pk_num_documento='$idDocumento' 
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }


    public function metBuscarDependencia($id)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
            a004_dependencia
            WHERE
            pk_num_dependencia='$id'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

	public function metBuscarcorrespondencia($id)
    {
        $generartipoCorresp = $this->_db->query("
            SELECT
              *
            FROM
            cd_c003_tipo_correspondencia
            WHERE
            pk_num_tipo_documento='$id'
              ");
        $generartipoCorresp->setFetchMode(PDO::FETCH_ASSOC);
        return $generartipoCorresp->fetch();
    }
	
		
    public function metCrearTipoDocumento($secuencia,$codigoInt,$codigoReq,$codigodep,$persremitent,
										$cargoremitent,$asunto,
										$descripasunto,$anexo,$descripanexo,
										$plazoatencion,$estatus,$codigocorresp,$numorganismo,
										$orgaextern=false,$depextern=false,$proveedorext=false,$flag_r=false,$represextern=false,$cargoextern=false,$fec_documento,$atencion)
	
    {
        
		$this->_db->beginTransaction();
		
            $nuevoRegistro=$this->_db->prepare("
                    INSERT INTO
                    cd_c011_documento_salida
                  SET
				  	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
					num_secuencia=:num_secuencia,
					num_cod_interno=:num_cod_interno,
					 ind_cod_completo=:ind_cod_completo, ind_dependencia_remitente=:ind_dependencia_remitente, 
					ind_persona_remitente=:ind_persona_remitente , ind_cargo_remitente=:ind_cargo_remitente,
					 fec_registro=:fec_registro, ind_asunto=:ind_asunto, txt_descripcion=:txt_descripcion , ind_anexo=:ind_anexo,
					  txt_descripcion_anexo=:txt_descripcion_anexo, ind_plazo_atencion=:ind_plazo_atencion, fec_documento=:fec_documento , txt_contenido=:txt_contenido,
					  ind_media_firma=:ind_media_firma, fec_anulado=:fec_anulado, ind_motivo_anulado=:ind_motivo_anulado ,
					  ind_persona_anulado=:ind_persona_anulado, ind_estado=:ind_estado, fec_mes=:fec_mes, fec_annio=:fec_annio,
					  fk_cdc003_num_tipo_documento=:fk_cdc003_num_tipo_documento, fk_a001_num_organismo=:fk_a001_num_organismo, 
					  fk_a004_num_dependencia=:fk_a004_num_dependencia
                ");
			
            $nuevoRegistro->execute(array(
		        'num_secuencia'=>$secuencia,
				'num_cod_interno'=>$codigoInt,
			    'ind_cod_completo'=>$codigoReq,
                'ind_dependencia_remitente'=>$codigodep,
                'ind_persona_remitente'=>$persremitent,
                'ind_cargo_remitente'=>$cargoremitent,
				'fec_registro'=>date('Y-m-d'),
                'ind_asunto'=>$asunto,
                'txt_descripcion'=>$descripasunto,
                'ind_anexo'=>$anexo,
				'txt_descripcion_anexo'=>$descripanexo,
                'ind_plazo_atencion'=>$plazoatencion,
                'fec_documento'=>$fec_documento,
				'txt_contenido'=>NULL,
                'ind_media_firma'=>NULL,
                'fec_anulado'=>NULL,
				'ind_motivo_anulado'=>NULL,
				'ind_persona_anulado'=>NULL,
				'ind_estado'=>'PR',
                'fec_mes'=>date('m'),
				'fec_annio'=>date('Y'),
				'fk_cdc003_num_tipo_documento'=>$codigocorresp,
				'fk_a001_num_organismo'=>$numorganismo,
				'fk_a004_num_dependencia'=>$codigodep
			
            ));
			
			$idDocumento= $this->_db->lastInsertId();
						
		 	$nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    cd_c012_distribucion_salida
                  SET
				  	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), ind_organismo_externo=:ind_organismo_externo, ind_dependencia_externa=:ind_dependencia_externa,
					ind_empresa_externa=:ind_empresa_externa, flag_r=:flag_r, 
					ind_representante_externo=:ind_representante_externo , ind_cargo_externo=:ind_cargo_externo,
					ind_con_atencion=:ind_con_atencion, ind_plazo_atencion=:ind_plazo_atencion, fec_envio=:fec_envio , fec_acuse=:fec_acuse,
					ind_estado=:ind_estado, ind_motivo_devolucion=:ind_motivo_devolucion, fec_devolucion=:fec_devolucion , fk_cdc011_num_documento='$idDocumento',
 					fk_a003_num_persona=:fk_a003_num_persona	
            ");
		if($orgaextern){
			foreach($orgaextern AS $i){
				$nuevoRegistro->execute(array(
					'ind_organismo_externo'=>$i,
					'ind_dependencia_externa'=>'',
					'ind_empresa_externa'=>'',
					'flag_r'=>'',
					'ind_representante_externo'=>$represextern[$i],
					'ind_cargo_externo'=>$cargoextern[$i],
					'ind_con_atencion'=>$atencion,
					'ind_plazo_atencion'=>$plazoatencion,
					'fec_envio'=>NULL,
					'fec_acuse'=>NULL,
					'ind_estado'=>'PR',
					'ind_motivo_devolucion'=>NULL,
					'fec_devolucion'=>NULL,
					'fk_a003_num_persona'=>NULL,
            	));
			}
		  }
		  	if($depextern){
			foreach($depextern AS $i){
				$nuevoRegistro->execute(array(
					'ind_organismo_externo'=>'',
					'ind_dependencia_externa'=>$i,
					'ind_empresa_externa'=>'',
					'flag_r'=>'',
					'ind_representante_externo'=>$represextern[$i],
					'ind_cargo_externo'=>$cargoextern[$i],
					'ind_con_atencion'=>$atencion,
					'ind_plazo_atencion'=>$plazoatencion,
					'fec_envio'=>NULL,
					'fec_acuse'=>NULL,
					'ind_estado'=>'PR',
					'ind_motivo_devolucion'=>NULL,
					'fec_devolucion'=>NULL,
					'fk_a003_num_persona'=>NULL,

            	));
			}
       	}


		 if($flag_r){
			foreach($flag_r AS $i){
				$nuevoRegistro->execute(array(
					'ind_organismo_externo'=>'',
					'ind_dependencia_externa'=>'',
					'ind_empresa_externa'=>'',
					'flag_r'=>$i,
					'ind_representante_externo'=>$represextern[$i],
					'ind_cargo_externo'=>$cargoextern[$i],
					'ind_con_atencion'=>$atencion,
					'ind_plazo_atencion'=>$plazoatencion,
					'fec_envio'=>NULL,
					'fec_acuse'=>NULL,
					'ind_estado'=>'PR',
					'ind_motivo_devolucion'=>NULL,
					'fec_devolucion'=>NULL,
					'fk_a003_num_persona'=>NULL,

            	));

       	}
		}

		if($proveedorext){
			foreach($proveedorext AS $i){
				$nuevoRegistro->execute(array(
					'ind_organismo_externo'=>'',
					'ind_dependencia_externa'=>'',
					'ind_empresa_externa'=>$i,
					'flag_r'=>'',
					'ind_representante_externo'=>$represextern[$i],
					'ind_cargo_externo'=>'',
					'ind_con_atencion'=>$atencion,
					'ind_plazo_atencion'=>$plazoatencion,
					'fec_envio'=>NULL,
					'fec_acuse'=>NULL,
					'ind_estado'=>'PR',
					'ind_motivo_devolucion'=>NULL,
					'fec_devolucion'=>NULL,
					'fk_a003_num_persona'=>NULL,

            	));

       	}
		}

          $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }

    }

    public function metModificarTipoDocumento(
                                              $codigomod2,
                                              $codigoReq2,
                                              $codigodep,
                                              $persremitent,
                                              $cargoremitent,
                                              $asunto,
										      $descripasunto,
                                              $anexo,
                                              $descripanexo,
										      $plazoatencion,
                                              $estatus,
										      $tipodocumento,
                                              $numorganismo,
										      $orgaextern=false,
                                              $depextern=false,
                                              $proveedorext=false,
                                              $flag_r=false,
                                              $represextern=false,
                                              $cargoextern=false,
                                              $atencion,
                                              $fecha,
                                              $idDocumento)
																				
	
    {
	
        $this->_db->beginTransaction();


            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c011_documento_salida
                      SET
                    num_cod_interno=:num_cod_interno,
                    ind_cod_completo=:ind_cod_completo,
					ind_dependencia_remitente=:ind_dependencia_remitente, 
					ind_persona_remitente=:ind_persona_remitente , 
					ind_cargo_remitente=:ind_cargo_remitente,
					ind_asunto=:ind_asunto,
					txt_descripcion=:txt_descripcion , 
					ind_anexo=:ind_anexo,
					txt_descripcion_anexo=:txt_descripcion_anexo, 
					ind_plazo_atencion=:ind_plazo_atencion, 
				  
					ind_media_firma=:ind_media_firma, 
					fec_anulado=:fec_anulado,
					ind_motivo_anulado=:ind_motivo_anulado ,
					ind_persona_anulado=:ind_persona_anulado,
					fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
				  	fec_registro=:fec_registro,
				  	fec_documento=:fec_documento
                      WHERE
                    pk_num_documento='$idDocumento'
            ");

   	            $nuevoRegistro->execute(array(
                 'num_cod_interno'=>$codigomod2,
                'ind_cod_completo'=>$codigoReq2,
                'ind_dependencia_remitente'=>$codigodep,
                'ind_persona_remitente'=>$persremitent,
                'ind_cargo_remitente'=>$cargoremitent,
                'ind_asunto'=>$asunto,
                'txt_descripcion'=>$descripasunto,
                'ind_anexo'=>$anexo,
				'txt_descripcion_anexo'=>$descripanexo,
                'ind_plazo_atencion'=>$plazoatencion,
                'ind_media_firma'=>NULL,
                'fec_anulado'=>NULL,
				'ind_motivo_anulado'=>NULL,
				'ind_persona_anulado'=>NULL,
                'fec_registro'=>$fecha,
                 'fec_documento'=>$fecha
				
            ));
            
			 $nuevoRegistro = $this->_db->query(
            "DELETE FROM cd_c012_distribucion_salida WHERE fk_cdc011_num_documento='$idDocumento'"
        );
			
				$nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    cd_c012_distribucion_salida
                  SET
				  	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
				  	 ind_organismo_externo=:ind_organismo_externo, 
				  	 ind_dependencia_externa=:ind_dependencia_externa,
					 ind_empresa_externa=:ind_empresa_externa, 
					 flag_r=:flag_r,
					 ind_representante_externo=:ind_representante_externo ,
					 ind_cargo_externo=:ind_cargo_externo,
					 ind_con_atencion=:ind_con_atencion,
					 ind_plazo_atencion=:ind_plazo_atencion,
					 fec_envio=:fec_envio , fec_acuse=:fec_acuse,
					 ind_estado=:ind_estado, 
					 ind_motivo_devolucion=:ind_motivo_devolucion,
					 fec_devolucion=:fec_devolucion ,
				     fk_cdc011_num_documento='$idDocumento',
 					 fk_a003_num_persona=:fk_a003_num_persona	
            ");
		if($orgaextern){
			foreach($orgaextern AS $i){
				$nuevoRegistro->execute(array(
					'ind_organismo_externo'=>$i,
					'ind_dependencia_externa'=>'',
					'ind_empresa_externa'=>'',
					'flag_r'=>'',
					'ind_representante_externo'=>$represextern[$i],
                    'ind_cargo_externo'=>$cargoextern[$i],
					'ind_con_atencion'=>$atencion,
					'ind_plazo_atencion'=>$plazoatencion,
					'fec_envio'=>NULL,
					'fec_acuse'=>NULL,
					'ind_estado'=>'PR',
					'ind_motivo_devolucion'=>NULL,
					'fec_devolucion'=>NULL,
					'fk_a003_num_persona'=>NULL,
            	));
			}
		  }
		  	if($depextern){
			foreach($depextern AS $i){
				$nuevoRegistro->execute(array(
					'ind_organismo_externo'=>'',
					'ind_dependencia_externa'=>$i,
					'ind_empresa_externa'=>'',
					'flag_r'=>'',
					'ind_representante_externo'=>$represextern[$i],
					'ind_cargo_externo'=>$cargoextern[$i],
					'ind_con_atencion'=>$atencion,
					'ind_plazo_atencion'=>$plazoatencion,
					'fec_envio'=>NULL,
					'fec_acuse'=>NULL,
					'ind_estado'=>'PR',
					'ind_motivo_devolucion'=>NULL,
					'fec_devolucion'=>NULL,
					'fk_a003_num_persona'=>NULL,

            	));
			}
       	}


		 if($flag_r){
			foreach($flag_r AS $i){
				$nuevoRegistro->execute(array(
					'ind_organismo_externo'=>'',
					'ind_dependencia_externa'=>'',
					'ind_empresa_externa'=>'',
					'flag_r'=>$i,
					'ind_representante_externo'=>$represextern[$i],
					'ind_cargo_externo'=>$cargoextern[$i],
					'ind_con_atencion'=>$atencion,
					'ind_plazo_atencion'=>$plazoatencion,
					'fec_envio'=>NULL,
					'fec_acuse'=>NULL,
					'ind_estado'=>'PR',
					'ind_motivo_devolucion'=>NULL,
					'fec_devolucion'=>NULL,
					'fk_a003_num_persona'=>NULL,

            	));

       	}
		}

		if($proveedorext){
			foreach($proveedorext AS $i){
				$nuevoRegistro->execute(array(
					'ind_organismo_externo'=>'',
					'ind_dependencia_externa'=>'',
					'ind_empresa_externa'=>$i,
					'flag_r'=>'',
					'ind_representante_externo'=>$represextern[$i],
					'ind_cargo_externo'=>NULL,
					'ind_con_atencion'=>$atencion,
					'ind_plazo_atencion'=>$plazoatencion,
					'fec_envio'=>NULL,
					'fec_acuse'=>NULL,
					'ind_estado'=>'PR',
					'ind_motivo_devolucion'=>NULL,
					'fec_devolucion'=>NULL,
					'fk_a003_num_persona'=>NULL,

            	));

       	}
		}
		
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
    }

   public function metanularTipoDocumento($motivoanulado,$personanulado,$estatus,$idDocumento)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                 UPDATE
                cd_c011_documento_salida
                SET fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  fec_anulado=:fec_anulado, ind_motivo_anulado=:ind_motivo_anulado, 
					ind_persona_anulado=:ind_persona_anulado, ind_estado=:ind_estado  
				 WHERE
                    pk_num_documento='$idDocumento'
            ");
            $elimar->execute(array(
                'fec_anulado'=>date('Y-m-d'),
				'ind_motivo_anulado'=>$motivoanulado,
				'ind_persona_anulado'=>$personanulado,
				'ind_estado'=>'AN'
        ));
		
		$elimar=$this->_db->prepare("
                 UPDATE
                cd_c012_distribucion_salida
                SET fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  ind_estado=:ind_estado  
				 WHERE
                    fk_cdc011_num_documento='$idDocumento'
            ");
            $elimar->execute(array(
				'ind_estado'=>'AN'
        ));
		
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }

    }
	
}
