<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: distribucion de documentos externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                   | ahurtado@contraloriadebolivar.gob.ve   |         0416-6867297           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        12-11-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
//require_once 'listaModelo.php';
class distribdocumenextModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metListarTipoDocumento()
    {
        $tipoDocumento = $this->_db->query(
            "SELECT 
			cd_c004.*,
			cd_c005.*,
			a004.ind_dependencia AS depres,
			r.ind_dependencia AS depdes,
		  a004.fk_a003_num_persona_responsable,
			 CONCAT(a003.ind_nombre1,'  ',a003.ind_apellido1)  AS PersonaDest,
			 CONCAT(d.ind_nombre1,'  ',d.ind_apellido1)  AS ResponsableDep,
			cd_c003.ind_descripcion
			FROM 
			 cd_c005_distribucion_entrada cd_c005 
			LEFT JOIN
			 cd_c004_documento_entrada cd_c004 ON cd_c004.pk_num_documento =  cd_c005.fk_cdc004_num_documento
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c005.ind_dependencia_destinataria
			LEFT JOIN
			 a004_dependencia r ON r.pk_num_dependencia =  cd_c005.ind_persona_destinataria
			LEFT JOIN
			a003_persona  a003 ON a003.pk_num_persona =  cd_c005.ind_persona_destinataria
			LEFT JOIN
			a003_persona d ON d.pk_num_persona = a004.fk_a003_num_persona_responsable
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c004.fk_cdc003_num_tipo_documento
			  WHERE
            cd_c004.fk_cdc003_num_tipo_documento='2'  AND cd_c005.ind_estado='EV' 
            ORDER BY cd_c004.pk_num_documento DESC

			");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }

}
