<?php


/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                     |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867297           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class scriptCargaCdModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
    }

    public function metBusquedaSimple($tabla, $innerJoin, $where)
    {
        $busquedaSimple = $this->_db->query("
            SELECT
              *
            FROM
              $tabla
              $innerJoin
            WHERE
              $where
        ");
        $busquedaSimple->setFetchMode(PDO::FETCH_ASSOC);
        return $busquedaSimple->fetch();
    }


      #### CORRESPONDENCIAS CONTROLS DE  DOCUMENTOS
    public function metCargarCorrespondencia($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                cd_c003_tipo_correspondencia
              SET
			  	fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
               	ind_descripcion=:ind_descripcion, 
				ind_descripcion_corta=:ind_descripcion_corta,
				ind_procedencia=:ind_procedencia,
				num_estatus=:num_estatus
                
            ");

        foreach ($arrays as $array) {
          //  $busqueda = $this->metBusquedaSimple('cd_c003_tipo_correspondencia', false, "ind_descripcion = '" . $array['ind_descripcion'] . "'");
           
                $registro->execute(array(
				    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_descripcion' => mb_strtoupper($array['ind_descripcion'], 'utf-8'),
					':ind_descripcion_corta' => $array['ind_descripcion_corta'],
					':ind_procedencia' => $array['ind_procedencia'],
					':num_estatus' => $array['num_estatus']
                ));
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
		
    }


 #### CARGA DE ORGANISMOS EXTERNOS PARA CONTROL  DE  DOCUMENTOS
    public function metCargarOrganismosExt($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                a001_organismo
              SET
			  	fec_ultima_modificacion=NOW(),
                fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
               	ind_descripcion_empresa=:ind_descripcion_empresa, 
				ind_numero_registro=:ind_numero_registro,
				ind_tomo_registro=:ind_tomo_registro,
				ind_logo=:ind_logo,
				num_estatus=:num_estatus,
				ind_tipo_organismo=:ind_tipo_organismo,
				ind_direccion=:ind_direccion,
				fk_a006_num_miscelaneo_detalle_carcater_social=:fk_a006_num_miscelaneo_detalle_carcater_social
               
            ");

        foreach ($arrays as $array) {

                $registro->execute(array(
				    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_descripcion_empresa' => mb_strtoupper($array['ind_descripcion_empresa'], 'utf-8'),
					':ind_numero_registro' => $array['ind_numero_registro'],
					':ind_tomo_registro' => $array['ind_tomo_registro'],
					':ind_logo' => $array['ind_logo'],
					':num_estatus' => $array['num_estatus'],
					':ind_tipo_organismo' => $array['ind_tipo_organismo'],
					':ind_direccion' => $array['ind_direccion'],
					':fk_a006_num_miscelaneo_detalle_carcater_social' => $array['fk_a006_num_miscelaneo_detalle_carcater_social']
					
                ));
				$fk_a001_num_organismo= $this->_db->lastInsertId();
				 $registroDet = $this->_db->prepare("
				  INSERT INTO
					a002_organismo_detalle
				  SET
					ind_cargo_representante=:ind_cargo_representante, 
					ind_pagina_web=:ind_pagina_web,
					ind_sujeto_control=:ind_sujeto_control,
					fk_a001_num_organismo=:fk_a001_num_organismo,
					fk_a003_num_persona_representante=:fk_a003_num_persona_representante,
					fk_a010_num_ciudad=:fk_a010_num_ciudad
				   
				");	
				foreach ($array['Det'] as $arraydet) {
                       
                $registroDet->execute(array(
				    ':ind_cargo_representante' => $arraydet['ind_cargo_representante'],
                    ':ind_pagina_web' => $arraydet['ind_pagina_web'],
					':ind_sujeto_control' => $arraydet['ind_sujeto_control'],
					':fk_a001_num_organismo' => $arraydet['fk_a001_num_organismo'],
					':fk_a003_num_persona_representante' => $arraydet['fk_a003_num_persona_representante'],
					':fk_a010_num_ciudad' => $arraydet['fk_a010_num_ciudad']
					
                ));
           
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
		
    }
	

		 #### CARGA DE DEPENDENCIAS EXTERNAS PARA CONTROL  DE  DOCUMENTOS
    public function metCargarDependenciaExt($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                a021_dependencia_ext
              SET
			  	ind_documento_fiscal=:ind_documento_fiscal, 
				ind_descripcion=:ind_descripcion,
				num_estatus=:num_estatus,
				fk_a001_num_organismo=:fk_a001_num_organismo
               
            ");

        foreach ($arrays as $array) {
           
                $registro->execute(array(
				    ':ind_documento_fiscal' => $array['ind_documento_fiscal'],
                    ':ind_descripcion' => mb_strtoupper($array['ind_descripcion'], 'utf-8'),
					':num_estatus' => $array['num_estatus'],
					':fk_a001_num_organismo' => $array['fk_a001_num_organismo']
					
                ));
			$fk_a021_num_dependencia_ext= $this->_db->lastInsertId();
			 $registroDepDEt = $this->_db->prepare("
              INSERT INTO
                a022_dependencia_extdetalle
              SET
			  	ind_cargo_representante=:ind_cargo_representante, 
				ind_direccion=:ind_direccion,
				fk_a021_num_dependencia_ext=:fk_a021_num_dependencia_ext,
				fk_a003_num_persona_representante=:fk_a003_num_persona_representante
               
            ");
			
			foreach ($array['Det'] as $arrayDep) {
                    $registroDepDEt->execute(array(
				    ':ind_cargo_representante' => $arrayDep['ind_cargo_representante'],
                    ':ind_direccion' => mb_strtoupper($arrayDep['ind_direccion'], 'utf-8'),
					':fk_a021_num_dependencia_ext' => $arrayDep['fk_a021_num_dependencia_ext'],
					':fk_a003_num_persona_representante' => $arrayDep['fk_a003_num_persona_representante']
					
                ));
            }

        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
		
    }
	
	/*
	
		 #### CARGA DE DEPENDENCIAS EXTERNAS PARA CONTROL  DE  DOCUMENTOS
    public function metCargarDependenciaDetExt($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare("
              INSERT INTO
                a022_dependencia_extdetalle
              SET
			  	ind_cargo_representante=:ind_cargo_representante, 
				ind_direccion=:ind_direccion,
				fk_a021_num_dependencia_ext=:fk_a021_num_dependencia_ext,
				fk_a003_num_persona_representante=:fk_a003_num_persona_representante
               
            ");

        foreach ($arrays as $array) {
                    $registro->execute(array(
				    ':ind_cargo_representante' => $array['ind_cargo_representante'],
                    ':ind_direccion' => mb_strtoupper($array['ind_direccion'], 'utf-8'),
					':fk_a021_num_dependencia_ext' => $array['fk_a021_num_dependencia_ext'],
					':fk_a003_num_persona_representante' => $array['fk_a003_num_persona_representante']
					
                ));
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
		
    }
	
	*/

}
