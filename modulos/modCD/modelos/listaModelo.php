<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 

class listaModelo extends Modelo
{

    public function __construct() 
    {
        parent::__construct();
    }

    public function metListarOrganismo()
    {
        $menu = $this->_db->query("SELECT * FROM a001_organismo WHERE
             pk_num_organismo!='1'");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
		
    }
	
	public function metListarOrganismoInt()
    {
        $organismoint= $this->_db->query("SELECT * FROM a001_organismo WHERE
             pk_num_organismo='1'");
        $organismoint->setFetchMode(PDO::FETCH_ASSOC);
        return $organismoint->fetchAll();
		
    }
	

	public function metListaPersona()
    {
        $persona = $this->_db->query(
            "SELECT 
			 vl_rh.*,
		  a004.ind_dependencia,
		  CONCAT(vl_rh.ind_nombre1,' ',vl_rh.ind_apellido1) AS nombre_apellidos			
			FROM 
			 vl_rh_persona_empleado_datos_laborales  vl_rh
			 INNER JOIN
			 a004_dependencia a004 ON a004.pk_num_dependencia =  vl_rh.fk_a004_num_dependencia 
			 WHERE num_estatus='1'"
			
	 
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }
    public function metListaPersonasActivas()
    {
        $persona = $this->_db->query(
            "SELECT 
			 vl_rh.*,
		  a004.ind_dependencia,
		  CONCAT(vl_rh.ind_nombre1,' ',vl_rh.ind_apellido1) AS nombre_apellidos			
			FROM 
			 vl_rh_persona_empleado_datos_laborales  vl_rh
			 INNER JOIN
			 a004_dependencia a004 ON a004.pk_num_dependencia =  vl_rh.fk_a004_num_dependencia 
             INNER JOIN rh_b001_empleado on vl_rh.pk_num_empleado=rh_b001_empleado.pk_num_empleado
			 WHERE rh_b001_empleado.num_estatus='1'"


        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }
    public function metListaPersona2()
    {
        $persona = $this->_db->query(
            "SELECT
                  a003_persona.*,
                  CONCAT(
                    a003_persona.ind_nombre1,
                    ' ',
                    a003_persona.ind_apellido1
                  ) AS nombre_apellidos
                FROM
                  a003_persona
                WHERE
                  num_estatus = '1'"


        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }

	public function metListaRepresentante($usuario)
    {
        $persona = $this->_db->query(
            "SELECT 
			vl_rh.*,
			rh_c063.ind_descripcion_cargo,
			rh_c063.pk_num_puestos,
			a004.ind_codinterno,
			a004.ind_dependencia,
			a004.pk_num_dependencia,
		  CONCAT(vl_rh.ind_nombre1,'   ',vl_rh.ind_nombre2,'   ',vl_rh.ind_apellido1,'   ',vl_rh.ind_apellido2)  AS nombre_apellidos,
		  a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM
			a004_dependencia a004 
 			INNER JOIN
			vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.pk_num_persona = a004.fk_a003_num_persona_responsable 
			INNER JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = vl_rh.fk_rhc063_num_puestos_cargo  
			 LEFT JOIN 
		  a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=a004.pk_num_dependencia
			 WHERE vl_rh.ind_estatus='1' 
			 AND  a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario 
			 AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
             GROUP BY a004.pk_num_dependencia
			
			"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        $dat1= $persona->fetchAll();

        $persona2 = $this->_db->query(
            "SELECT
            vl_rh.*,
			rh_c063.ind_descripcion_cargo,
			rh_c063.pk_num_puestos,
			a004.ind_codinterno,
			a004.ind_dependencia,
			a004.pk_num_dependencia,
		  CONCAT(vl_rh.ind_nombre1,'   ',vl_rh.ind_nombre2,'   ',vl_rh.ind_apellido1,'   ',vl_rh.ind_apellido2)  AS nombre_apellidos,
		  a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM
			a004_dependencia a004 
			INNER JOIN vl_rh_persona_empleado_datos_laborales vl_rh ON a004.pk_num_dependencia = vl_rh.fk_a004_num_dependencia 
			INNER JOIN rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = vl_rh.fk_rhc063_num_puestos_cargo 
			LEFT JOIN a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=a004.pk_num_dependencia 
			WHERE vl_rh.ind_estatus='1' AND a004.fk_a003_num_persona_responsable=0 
			AND  a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario
			AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7' GROUP BY vl_rh.pk_num_persona
			
			"
        );
        $persona2->setFetchMode(PDO::FETCH_ASSOC);
        $dat2= $persona2->fetchAll();

        $resp=array_merge($dat1,$dat2);

        return $resp;
    }
	
	public function metListaEncargEspecial()
    {
        $persona = $this->_db->query(
            "SELECT 
			vl_rh.fk_rhc063_num_puestos_cargo,
			rh_c063.ind_descripcion_cargo,
			rh_c063.pk_num_puestos
		FROM
			vl_rh_persona_empleado_datos_laborales vl_rh
			INNER JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = vl_rh.fk_rhc063_num_puestos_cargo
			GROUP BY rh_c063.pk_num_puestos
			"
		);
        $persona->setFetchMode(PDO::FETCH_ASSOC); 
        return $persona->fetchAll();
    }
	
		 public function metListaCiudad()
    {
        $ciudad = $this->_db->query("SELECT * FROM a010_ciudad");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);
        return $ciudad->fetchAll();
		
    }

	 public function metListarDependencia()
    {
        $exdependencia = $this->_db->query("SELECT * FROM a021_dependencia_ext");
        $exdependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $exdependencia->fetchAll();
		
    }
	
	public function metListarMiscelaneos()
    {
        $miscelaneo = $this->_db->query("SELECT * FROM a006_miscelaneo_detalle  WHERE cod_detalle IN ('PART','REP','OTRO')");
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneo->fetchAll();
	}
	
	public function metListarMiscelaneosp()
    {
       $miscelaneop = $this->_db->query("SELECT * FROM a006_miscelaneo_detalle  WHERE cod_detalle IN ('NAT','JUR')");
      	$miscelaneop->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneop->fetchAll();
	}
	
	public function metListarDependenciaInt()
    {
        $dependenciaint = $this->_db->query("SELECT * FROM a004_dependencia");
        $dependenciaint->setFetchMode(PDO::FETCH_ASSOC);
        return $dependenciaint->fetchAll();
		
    }
	
	  public function metListarCorrespondencia()
    {
        $correspondencia = $this->_db->query("SELECT * FROM cd_c003_tipo_correspondencia WHERE ind_procedencia='I'");
        $correspondencia->setFetchMode(PDO::FETCH_ASSOC);
        return $correspondencia->fetchAll();
		
    }
	
	 public function metListarCorrespon()
    {
        $corresp = $this->_db->query("SELECT * FROM cd_c003_tipo_correspondencia WHERE ind_procedencia='E'");
        $corresp->setFetchMode(PDO::FETCH_ASSOC);
        return $corresp->fetchAll();
		
    }
	
	

}
