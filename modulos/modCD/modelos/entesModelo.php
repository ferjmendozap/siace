<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Organismos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 2 |          José Diaz                        |        diaz.jose@cmldc.gob.ve      |         0416-2124302           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #2                      |        20-01-2016       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modCD' . DS . 'modelos' . DS . 'funcionesGenerales.php';
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once 'listaModeloPF.php';
class entesModelo extends listaModeloPF
{

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
        $this->atFuncionesGrles = new funcionesGenerales();
        $this->atMiscelaneoModelo = new miscelaneoModelo();
    }

    public function metMostrarEntePersona($idEnte)
    {
        $organismo = $this->_db->query(" 
         SELECT 
          a039.*,
          a041.*, 
          CONCAT_WS(' ',ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2) AS nombres,
          a009_estado.pk_num_estado,
          a008_pais.pk_num_pais,
          a011_municipio.pk_num_municipio
          FROM 
          a041_persona_ente a041
          INNER JOIN a003_persona a003 ON a041.fk_a003_num_persona_titular=a003.pk_num_persona
          RIGHT JOIN a039_ente a039 ON a041.fk_a039_num_ente=a039.pk_num_ente          
          INNER join a010_ciudad ON a010_ciudad.pk_num_ciudad = a039.fk_a010_num_ciudad
          INNER JOIN a009_estado ON a009_estado.pk_num_estado = a010_ciudad.fk_a009_num_estado
          INNER JOIN a008_pais ON a008_pais.pk_num_pais = a009_estado.fk_a008_num_pais          
          INNER JOIN a011_municipio ON a011_municipio.fk_a009_num_estado = a009_estado.pk_num_estado
          WHERE 
          fk_a039_num_ente=$idEnte
        ");
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetch();
    }

    public function metMostrarPais($ciudad)
    {
        $pais = $this->_db->query("
         SELECT
          a008_pais.pk_num_pais,
          a009_estado.pk_num_estado,
          a010_ciudad.pk_num_ciudad
        FROM
          a010_ciudad
        INNER JOIN a009_estado ON a009_estado.pk_num_estado = a010_ciudad.fk_a009_num_estado
        INNER JOIN a008_pais ON a008_pais.pk_num_pais = a009_estado.fk_a008_num_pais
        WHERE
          pk_num_ciudad = '$ciudad';
        ");
        $pais->setFetchMode(PDO::FETCH_ASSOC);
        return $pais->fetch();
    }

    public function metMostrarEnte($idEnte)
    {
        $organismo = $this->_db->query("
         SELECT a039_ente.*,a009_estado.pk_num_estado, a011_municipio.pk_num_municipio, a008_pais.pk_num_pais FROM a039_ente
        LEFT JOIN a010_ciudad ON a010_ciudad.pk_num_ciudad = a039_ente.fk_a010_num_ciudad
        LEFT JOIN a009_estado ON a009_estado.pk_num_estado = a010_ciudad.fk_a009_num_estado
        LEFT JOIN a011_municipio ON a009_estado.pk_num_estado = a011_municipio.fk_a009_num_estado
        LEFT JOIN a008_pais ON a008_pais.pk_num_pais = a009_estado.fk_a008_num_pais
         WHERE pk_num_ente=$idEnte
        ");
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetch();
    }

    public function metListarOrganismoInt()
    {
        $organismoint= $this->_db->query("SELECT * FROM a001_organismo WHERE
             pk_num_organismo='1'");
        $organismoint->setFetchMode(PDO::FETCH_ASSOC);
        return $organismoint->fetchAll();

    }

    public function metCompararNombre($descripcion)
    {
        $resultado = $this->_db->query("
          SELECT * FROM
		a039_ente
         WHERE
            ind_nombre_ente='$descripcion' LIMIT 1
        ");
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetch();
    }

    public function metCompararDocFiscal($doc_fiscal)
    {
        $resultado = $this->_db->query("
          SELECT * FROM
		a039_ente
         WHERE
            ind_numero_registro='$doc_fiscal'
            AND ind_numero_registro!=''
        ");
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetch();
    }

    public function metSelectDatoComparar($idEnte)
    {
        $resultado = $this->_db->query("
          SELECT ind_nombre_ente,ind_numero_registro FROM
		a039_ente
         WHERE
            pk_num_ente='$idEnte'
        ");
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetch();
    }

    /* Listar 1- Muestra todos los organismo independientemente si tiene o no representante--------------------------*/
    public function metListarEnte()
    {
        $organismo = $this->_db->query(
            "SELECT pk_num_ente AS idEnte, ind_nombre_ente, a039.num_estatus
                FROM a039_ente a039
                WHERE a039.num_estatus=1"
        );
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetchAll();
    }

    public function metListarResponsable()
    {
        $responsable= $this->_db->query(
            "SELECT a039.pk_num_ente,CONCAT_WS(' ', a003.ind_nombre1,a003.ind_nombre2,a003.ind_apellido1,a003.ind_apellido2) AS representante
                FROM a003_persona a003
                  INNER JOIN a041_persona_ente a041
                    ON a041.fk_a003_num_persona_titular = a003.pk_num_persona
                  INNER JOIN a039_ente a039
                  ON a039.pk_num_ente = a041.fk_a039_num_ente
                WHERE a041.num_estatus_titular=1"
        );
        $responsable->setFetchMode(PDO::FETCH_ASSOC);
        return $responsable->fetchAll();
    }

    public function metBuscaEntesGrilla($idEnte){
        $tabla="a041_persona_ente";
        $campos="pk_num_persona_ente,fk_a039_num_ente AS id_ente,num_ente_padre,ind_nombre_ente AS nombre_ente,num_estatus_titular AS estatus_titular,
        ind_nombre1,ind_nombre2,ind_apellido1,ind_apellido2,ind_cargo_personal_externo AS cargo_titular,ind_nombre_detalle AS situacion_titular ";
        $Union=" LEFT JOIN a039_ente ON a041_persona_ente.fk_a039_num_ente = a039_ente.pk_num_ente
        LEFT JOIN a003_persona ON a041_persona_ente.fk_a003_num_persona_titular = a003_persona.pk_num_persona
        LEFT JOIN a040_cargo_personal_externo ON a041_persona_ente.fk_a006_num_miscdet_cargo_pers = a040_cargo_personal_externo.pk_num_cargo_personal_externo
        INNER JOIN a006_miscelaneo_detalle ON a041_persona_ente.fk_a006_num_miscdetalle_situacion = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle";
        $sql_criterio=" WHERE ind_nombre_ente!='Particular' AND a039_ente.num_estatus=1 AND fk_a039_num_ente NOT IN ($idEnte) group BY pk_num_ente";
        $sql_query="SELECT $campos FROM $tabla$Union".$sql_criterio;
        $result=$this->_db->query($sql_query);
        //var_dump($sql_query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetchAll();
    }

    public function metBuscaEntesGrilla2(){
        $resultado = $this->_db->query("
          SELECT pk_num_persona_ente,pk_num_ente AS id_ente,num_ente_padre,ind_nombre_ente AS nombre_ente
           FROM a039_ente a039
            LEFT JOIN a041_persona_ente a041
              ON a041.fk_a039_num_ente=a039.pk_num_ente
            WHERE a039.num_estatus=1 AND (a041.num_estatus_titular=0 OR a041.num_estatus_titular IS NULL)
        ");
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetchAll();
    }
    /*---------------------------------------------------------------------------------------------------------------*/
    public function metEliminarEnte($idEnte)
    {
        $this->_db->beginTransaction();

        $eliminar=$this->_db->prepare("
                DELETE FROM a039_ente  WHERE pk_num_ente=:idEnte
            ");
        $eliminar->execute(array(
            'idEnte'=>$idEnte
        ));

        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idEnte;
        }

    }

    public function metEliminarResponsable($idResp){
        $sql="DELETE FROM a003_persona WHERE pk_num_persona=:idResp";
        $this->_db->beginTransaction();
        $eliminar=$this->_db->prepare($sql);
        $eliminar->execute(array('idResp'=>$idResp));

        $error=$eliminar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idResp;
        }
    }

    public function metRegistraPersona($ind_cedula_documento,$ind_documento_fiscal,$ind_nombre1,
                                       $ind_nombre2,$ind_apellido1,$ind_apellido2,$fk_a006_num_miscelaneo_detalle_nacionalidad,
                                       $ind_email,$num_estatus,$fk_a006_num_miscelaneo_det_tipopersona,$ind_tipo_persona)
    {
        $this->_db->beginTransaction();
        $registroPersona=$this->_db->prepare("
                INSERT INTO
                  a003_persona
                SET
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW(),
                  ind_cedula_documento=:ind_cedula_documento,
                  ind_documento_fiscal=:ind_documento_fiscal,
                  ind_nombre1=:ind_nombre1,
                  ind_nombre2=:ind_nombre2,
                  ind_apellido1=:ind_apellido1,
                  ind_apellido2=:ind_apellido2,
                  fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
                  ind_email=:ind_email,num_estatus=:num_estatus,
                  fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona,
                  ind_tipo_persona=:ind_tipo_persona

        ");

        $registroPersona->execute(array(
            'ind_cedula_documento'=>$ind_cedula_documento,
            'ind_documento_fiscal'=>$ind_documento_fiscal,
            'ind_nombre1'=>$ind_nombre1,
            'ind_nombre2'=>$ind_nombre2,
            'ind_apellido1'=>$ind_apellido1,
            'ind_apellido2'=>$ind_apellido2,
            'fk_a006_num_miscelaneo_detalle_nacionalidad'=>$fk_a006_num_miscelaneo_detalle_nacionalidad,
            'ind_email'=>$ind_email,
            'num_estatus'=>$num_estatus,
            'fk_a006_num_miscelaneo_det_tipopersona'=>$fk_a006_num_miscelaneo_det_tipopersona,
            'ind_tipo_persona'=>$ind_tipo_persona
        ));

        $idPersona=(int)$this->_db->lastInsertId();

        $error = $registroPersona->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idPersona;
        }

    }

    public function metCrearEnte(
        $doc_fiscal,$idEntev,$categoriaEnte,$descripcionEmpresa,
        $caracterSocial=false,$tomoRegistro,$fechaFuncdacion=false,$telefonos,$sitioWeb=false,
        $direccion, $parroquia = false, $ciudad = false,$estatus,$sujetoAcontrol=false,$gaceta=false,
        $resolucion=false,$mision=false,$vision=false,$otros=false)
    {
        $this->_db->beginTransaction();
        $ente=$this->_db->prepare("
                  INSERT INTO
                     a039_ente
                  SET
                     fk_a018_num_seg_usermod=$this->atIdUsuario,
                     fec_ultima_modificacion=NOW(),
                     num_ente_padre=:idEntev,
                     fk_a038_num_categoria_ente=:categoriaEnte,
                     ind_nombre_ente=:descripcionEmpresa,
                     ind_numero_registro=:doc_fiscal,
                     ind_tomo_registro=:tomoRegistro,
                     fec_fundacion=:fechaFuncdacion,
                     fk_a006_num_miscdetalle_caractersocial=:caracterSocial,
                     num_sujeto_control=:sujetoAcontrol,
                     ind_mision=:mision,
                     ind_vision=:vision,
                     ind_gaceta=:gaceta,
                     ind_resolucion=:resolucion,
                     ind_otros=:otros,
                     ind_pagina_web=:sitioWeb,
                     fk_a010_num_ciudad=:ciudad,
                     fk_a012_num_parroquia=:parroquia,
                     ind_direccion=:direccion,
                     ind_telefono=:telefonos,
                     num_estatus=:estatus
                ");

        $ente->execute(array(
            'idEntev'=>$idEntev,
            'categoriaEnte'=>$categoriaEnte,
            'descripcionEmpresa'=>$descripcionEmpresa,
            'doc_fiscal'=>$doc_fiscal,
            'tomoRegistro'=>$tomoRegistro,
            'fechaFuncdacion'=>$fechaFuncdacion,
            'caracterSocial'=>$caracterSocial,
            'sujetoAcontrol'=>$sujetoAcontrol,
            'mision'=>$mision,
            'vision'=>$vision,
            'gaceta'=>$gaceta,
            'resolucion'=>$resolucion,
            'otros'=>$otros,
            'sitioWeb'=>$sitioWeb,
            'ciudad'=>$ciudad,
            'parroquia'=>$parroquia,
            'direccion'=>$direccion,
            'telefonos'=>$telefonos,
            'estatus'=>$estatus
        ));

        $idEnte=$this->_db->lastInsertId();
        $error = $ente->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idEnte;
        }
    }

    public function metModificarEnte(
        $doc_fiscal,$idEnte,$idEntev,$tipo_Ente,$categoriaEnte,$descripcionEmpresa,
        $caracterSocial=false,$tomoRegistro,$fechaFuncdacion=false,$telefonos,$sitioWeb=false,
        $direccion,$estado=false,$municipio=false,$parroquia=false,$ciudad=false,$estatus,$sujetoAcontrol=false,$gaceta=false,
        $resolucion=false,$mision=false,$vision=false,$otros=false)
    {
        $this->_db->beginTransaction();
        $ente=$this->_db->prepare("
                  UPDATE
                     a039_ente
                  SET
                     fk_a018_num_seg_usermod=$this->atIdUsuario,
                     fec_ultima_modificacion=NOW(),
                     num_ente_padre=:idEntev,
                     fk_a038_num_categoria_ente=:categoriaEnte,
                     ind_nombre_ente=:descripcionEmpresa,
                     ind_numero_registro=:doc_fiscal,
                     ind_tomo_registro=:tomoRegistro,
                     fec_fundacion=:fechaFuncdacion,
                     fk_a006_num_miscdetalle_caractersocial=:caracterSocial,
                     num_sujeto_control=:sujetoAcontrol,
                     ind_mision=:mision,
                     ind_vision=:vision,
                     ind_gaceta=:gaceta,
                     ind_resolucion=:resolucion,
                     ind_otros=:otros,
                     ind_pagina_web=:sitioWeb,
                     fk_a010_num_ciudad=:ciudad,
                     fk_a012_num_parroquia=:parroquia,
                     ind_direccion=:direccion,
                     ind_telefono=:telefonos,
                     num_estatus=:estatus
                     WHERE
                        pk_num_ente=:idEnte
                ");

        $ente->execute(array(
            'idEntev'=>$idEntev,
            'categoriaEnte'=>$categoriaEnte,
            'descripcionEmpresa'=>$descripcionEmpresa,
            'doc_fiscal'=>$doc_fiscal,
            'tomoRegistro'=>$tomoRegistro,
            'fechaFuncdacion'=>$fechaFuncdacion,
            'caracterSocial'=>$caracterSocial,
            'sujetoAcontrol'=>$sujetoAcontrol,
            'mision'=>$mision,
            'vision'=>$vision,
            'gaceta'=>$gaceta,
            'resolucion'=>$resolucion,
            'otros'=>$otros,
            'sitioWeb'=>$sitioWeb,
            'ciudad'=>$ciudad,
            'parroquia'=>$parroquia,
            'direccion'=>$direccion,
            'telefonos'=>$telefonos,
            'estatus'=>$estatus,
            'idEnte'=>$idEnte,
        ));

        $error = $ente->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idEnte;
        }
    }

    public function metExistePersonaEnte($pkPersonaEnte)
    {
        $resultado = $this->_db->query(
            "SELECT * FROM a041_persona_ente
                WHERE pk_num_persona_ente=$pkPersonaEnte"
        );
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetch();
    }

    public function metCargo(){
        $cargo = $this->_db->query(
            "SELECT pk_num_cargo_personal_externo, ind_cargo_personal_externo FROM a040_cargo_personal_externo"
        );
        $cargo->setFetchMode(PDO::FETCH_ASSOC);
        return $cargo->fetchAll();
    }

    public function metSituacionCargo(){
        $situacionCargo = $this->_db->query(
            "SELECT pk_num_miscelaneo_detalle, ind_nombre_detalle
            FROM a006_miscelaneo_detalle
            WHERE fk_a005_num_miscelaneo_maestro=(SELECT pk_num_miscelaneo_maestro
            FROM a005_miscelaneo_maestro
            WHERE cod_maestro='PFSPE')"
        );
        $situacionCargo->setFetchMode(PDO::FETCH_ASSOC);
        return $situacionCargo->fetchAll();
    }


    /*Mostrar Usuario*/

    public function metMostrarUsuario($usuario)
    {
        $resultado = $this->_db->query("
          SELECT CONCAT_WS(' ',ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2) AS responsable FROM `a003_persona`
          WHERE `pk_num_persona`=$usuario
        ");
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetch();
    }

    /*Tipo de Entes*/

    public function metTipoEntes(){
        $tipo=$this->_db->query("
          SELECT pk_num_tipo_ente, ind_tipo_ente
          FROM a037_tipo_ente
        ");
        $tipo->setFetchMode(PDO::FETCH_ASSOC);
        return $tipo->fetchAll();
    }

    /*Tipos de Categoria*/

    public function metTipoCategorias($idTipoEnte=false){
        $where='';
        if($idTipoEnte!=false){
            $where="WHERE fk_a006_miscdet_tipoente=$idTipoEnte ";
        }
        $tipo=$this->_db->query("
          SELECT pk_num_categoria_ente, ind_categoria_ente FROM a038_categoria_ente
          $where
        ");
        $tipo->setFetchMode(PDO::FETCH_ASSOC);
        return $tipo->fetchAll();
    }

    public function metListaEntes($iddep_padre)
    {/*Se busca los entes u organismos externos*/
        $retornar=array("filas"=>false);
        $result=$this->atFuncionesGrles->metCargaCboxRecursivoEnte($iddep_padre,'');
        if(COUNT($result)>0) {
            $retornar=array("filas"=>$result);
        }
        return $retornar;
    }

    public function metMiscelaneo($valor){
        $cargo = $this->_db->query(
            "SELECT pk_num_miscelaneo_detalle, ind_nombre_detalle
             FROM a006_miscelaneo_detalle
             WHERE fk_a005_num_miscelaneo_maestro=(SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro WHERE cod_maestro='$valor')"
        );
        $cargo->setFetchMode(PDO::FETCH_ASSOC);
        return $cargo->fetchAll();
    }

    public function metEntePadre($valor){
        $ente = $this->_db->query("
            SELECT num_ente_padre FROM a039_ente
            WHERE pk_num_ente=$valor;
        ");
        $ente->setFetchMode(PDO::FETCH_ASSOC);
        return $ente->fetch();
    }

    public function metCaracterSocial(){
        $sql="SELECT * FROM a006_miscelaneo_detalle
                WHERE fk_a005_num_miscelaneo_maestro=(SELECT pk_num_miscelaneo_maestro FROM a005_miscelaneo_maestro  WHERE cod_maestro='PFTCSE')
                AND num_estatus=1;";
        $caracterSocial=$this->_db->query($sql);
        $caracterSocial->setFetchMode(PDO::FETCH_ASSOC);
        return $caracterSocial->fetchAll();
    }

    public function metResponsableEnte($idEnte){
        $sql="SELECT a003.pk_num_persona,concat_ws(' ',ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2) AS responsable
            FROM a041_persona_ente a041
            INNER JOIN a039_ente a039
            ON a039.pk_num_ente=fk_a039_num_ente
            INNER JOIN a003_persona a003
            ON a003.pk_num_persona=a041.fk_a003_num_persona_titular
            WHERE a039.pk_num_ente=$idEnte";

        $responsable = $this->_db->query($sql);
        $responsable->setFetchMode(PDO::FETCH_ASSOC);
        return $responsable->fetchAll();
    }

    public function metListarResponsables(){
        $sql="SELECT pk_num_persona AS idResponsable, ind_cedula_documento AS cedula, ind_documento_fiscal AS rif,
              CONCAT_WS(' ', ind_nombre1,ind_nombre2) AS nombres, CONCAT_WS(' ',ind_apellido1, ind_apellido2) AS apellidos,num_estatus AS estatus
              FROM a003_persona";

        $responsable = $this->_db->query($sql);
        $responsable->setFetchMode(PDO::FETCH_ASSOC);
        return $responsable->fetchAll();
    }

    public function metObtenerResponsable($idResp){
        $sql="SELECT pk_num_persona, ind_cedula_documento, ind_documento_fiscal,
               ind_nombre1,ind_nombre2,ind_apellido1, ind_apellido2 ,ind_email, fk_a006_num_miscelaneo_detalle_nacionalidad,
               ind_tipo_persona,fk_a006_num_miscelaneo_det_tipopersona, num_estatus
              FROM a003_persona
              WHERE pk_num_persona=$idResp";

        $responsable = $this->_db->query($sql);
        $responsable->setFetchMode(PDO::FETCH_ASSOC);
        return $responsable->fetch();
    }

    public function metModificaPersona($ind_cedula_documento,$ind_documento_fiscal,$ind_nombre1,
                                       $ind_nombre2,$ind_apellido1,$ind_apellido2,$fk_a006_num_miscelaneo_detalle_nacionalidad,
                                       $ind_email,$num_estatus,$fk_a006_num_miscelaneo_det_tipopersona,$ind_tipo_persona,$pk_num_persona)
    {
        $this->_db->beginTransaction();
        $registroPersona=$this->_db->prepare("
                UPDATE
                  a003_persona
                SET
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW(),
                  ind_cedula_documento=:ind_cedula_documento,
                  ind_documento_fiscal=:ind_documento_fiscal,
                  ind_nombre1=:ind_nombre1,
                  ind_nombre2=:ind_nombre2,
                  ind_apellido1=:ind_apellido1,
                  ind_apellido2=:ind_apellido2,
                  fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
                  ind_email=:ind_email,num_estatus=:num_estatus,
                  fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona,
                  ind_tipo_persona=:ind_tipo_persona
                  WHERE pk_num_persona=:pk_num_persona

        ");

        $registroPersona->execute(array(
            'ind_cedula_documento'=>$ind_cedula_documento,
            'ind_documento_fiscal'=>$ind_documento_fiscal,
            'ind_nombre1'=>$ind_nombre1,
            'ind_nombre2'=>$ind_nombre2,
            'ind_apellido1'=>$ind_apellido1,
            'ind_apellido2'=>$ind_apellido2,
            'fk_a006_num_miscelaneo_detalle_nacionalidad'=>$fk_a006_num_miscelaneo_detalle_nacionalidad,
            'ind_email'=>$ind_email,
            'num_estatus'=>$num_estatus,
            'fk_a006_num_miscelaneo_det_tipopersona'=>$fk_a006_num_miscelaneo_det_tipopersona,
            'ind_tipo_persona'=>$ind_tipo_persona,
            'pk_num_persona'=>$pk_num_persona
        ));

        $error = $registroPersona->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $pk_num_persona;
        }

    }

    public function metObtenerEntes($idResp){
        $sql="SELECT a041.pk_num_persona_ente,a039.pk_num_ente AS idEnte,a038.ind_categoria_ente,ind_nombre_ente,a039.num_estatus,a009.ind_estado, a011.ind_municipio ,a012.ind_parroquia, a010.ind_ciudad, a041.num_estatus_titular,a041.fec_registro,a041.fec_ultima_modificacion, a006.ind_nombre_detalle AS situacion,a040.ind_cargo_personal_externo AS cargo
                FROM a039_ente a039
                  INNER JOIN a038_categoria_ente a038
                    ON a039.fk_a038_num_categoria_ente=a038.pk_num_categoria_ente
                  LEFT JOIN a009_estado a009
                    ON a039.fk_a009_num_estado=a009.pk_num_estado
                  LEFT JOIN a011_municipio a011
                    ON a039.fk_a011_num_municipio=a011.pk_num_municipio
                  LEFT JOIN a012_parroquia a012
                    ON a039.pf_a012_num_parroquia=a012.pk_num_parroquia
                  LEFT JOIN a010_ciudad a010
                    ON a039.fk_a010_num_ciudad=a010.pk_num_ciudad
                  INNER JOIN a041_persona_ente a041
                    ON a039.pk_num_ente= a041.fk_a039_num_ente
                  INNER JOIN a040_cargo_personal_externo a040
                    ON a040.pk_num_cargo_personal_externo= a041.fk_a040_num_cargo_personal_externo
                  INNER JOIN a006_miscelaneo_detalle a006
                    ON a006.pk_num_miscelaneo_detalle= a041.fk_a006_num_miscdetalle_situacion
                WHERE a041.fk_a003_num_persona_titular=$idResp";

        $responsable = $this->_db->query($sql);
        $responsable->setFetchMode(PDO::FETCH_ASSOC);
        return $responsable->fetchAll();
    }

    public function metActOInactRespsableEnte($estado,$idRegistro){
        $sql="UPDATE a041_persona_ente
                SET num_estatus_titular=:estado,
                    fk_a018_num_seg_usermod='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
              WHERE pk_num_persona_ente=:idRegistro";
        $this->_db->beginTransaction();
        $estaResp=$this->_db->prepare($sql);

        $estaResp->execute(array(
            'estado'=>$estado,
            'idRegistro'=>$idRegistro
        ));

        $error = $estaResp->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return 1;
        }

    }

    public function metResponsableEnteActivo($idResp){
        $sql="SELECT pk_num_persona_ente FROM  a041_persona_ente
              WHERE fk_a003_num_persona_titular=$idResp
              AND num_estatus_titular=1;";

        $resultado = $this->_db->query($sql);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetchAll();
    }

    public function metDesactivarResponsablEnte($idResp){
        $sql="UPDATE a041_persona_ente
                SET num_estatus_titular=0,
                    fk_a018_num_seg_usermod='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
              WHERE pk_num_persona_ente IN (:idResp) AND num_estatus_titular=1";

        $this->_db->beginTransaction();
        $resultado = $this->_db->prepare($sql);

        $resultado->execute(array(
            'idResp'=>$idResp
        ));

        $error = $resultado->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return 1;
        }
    }

    public function metAsignarEnte($idPersona,$idEnte,$idCargo,$idSituacon,$actualizar=FALSE)
    { //var_dump($actualizar);die;
        $this->_db->beginTransaction();

        if($actualizar==0) {

            $registroPersona = $this->_db->prepare("
                INSERT INTO
                  a041_persona_ente
                SET
                  fk_a018_num_seg_usermod='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW(),
                  fk_a039_num_ente=:idEnte,
                  fk_a003_num_persona_titular=:idPersona,
                  fk_a006_num_miscdet_cargo_pers=:idCargo,
                  fk_a006_num_miscdetalle_situacion=:idSituacon,
                  num_estatus_titular=1
            ");

            $registroPersona->execute(array(
                'idPersona' => $idPersona,
                'idEnte' => $idEnte,
                'idCargo' => $idCargo,
                'idSituacon' => $idSituacon
            ));

            //$idPersona=(int)$this->_db->lastInsertId();

        }else{
            $registroPersona = $this->_db->prepare("
                UPDATE
                  a041_persona_ente
                SET
                  fk_a018_num_seg_useregistra='$this->atIdUsuario',
                  fec_registro=NOW(),
                  fk_a039_num_ente=:idEnte,
                  fk_a003_num_persona_titular=:idPersona,
                  fk_a040_num_cargo_personal_externo=:idCargo,
                  fk_a006_num_miscdetalle_situacion=:idSituacon,
                  num_estatus_titular=1
                  WHERE pk_num_persona_ente=:actualizar
            ");

            $registroPersona->execute(array(
                'idPersona' => $idPersona,
                'idEnte' => $idEnte,
                'idCargo' => $idCargo,
                'idSituacon' => $idSituacon,
                'actualizar'=> $actualizar
            ));
        }

        $error = $registroPersona->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return (int)$idEnte;
        }

    }

    public function metInactivarResponsablEnte($idEnte){
        $sql="UPDATE a041_persona_ente
                SET num_estatus_titular=0,
                    fk_a018_num_seg_usermod='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
              WHERE fk_a039_num_ente=:idEnte AND num_estatus_titular=1";

        $this->_db->beginTransaction();
        $resultado = $this->_db->prepare($sql);

        $resultado->execute(array(
            'idEnte'=>$idEnte
        ));

        $error = $resultado->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return 1;
        }
    }

    public function metInactivarResponsablEnte2($pkPersonaEnte){
        $sql="UPDATE a041_persona_ente
                SET num_estatus_titular=0,
                    fk_a018_num_seg_usermod='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
              WHERE pk_num_persona_ente=:pkPersonaEnte AND num_estatus_titular=1";

        $this->_db->beginTransaction();
        $resultado = $this->_db->prepare($sql);

        $resultado->execute(array(
            'pkPersonaEnte'=>$pkPersonaEnte
        ));

        $error = $resultado->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return 1;
        }
    }

    public function metExisteRegistro($idResp){
        $sql="SELECT * FROM a041_persona_ente a041
              WHERE a041.fk_a003_num_persona_titular=$idResp
              AND a041.num_estatus_titular=1
              ";
        $resultado = $this->_db->query($sql);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetchAll();
    }

    public function metResponsableActivo($idResp){
        $sql="SELECT a003.pk_num_persona,a003.ind_cedula_documento,CONCAT_WS(' ',a003.ind_nombre1,a003.ind_nombre2,a003.ind_apellido1,a003.ind_apellido2) AS responsable,fk_a006_num_miscdet_cargo_pers,fk_a006_num_miscdetalle_situacion
                FROM a041_persona_ente a041
                  INNER JOIN a003_persona a003
                    ON a003.pk_num_persona=a041.fk_a003_num_persona_titular
                WHERE a041.fk_a039_num_ente=$idResp
                    AND a041.num_estatus_titular=1";

        $resultado = $this->_db->query($sql);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetch();
    }

}