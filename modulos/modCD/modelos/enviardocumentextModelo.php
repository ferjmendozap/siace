<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 
 
require_once 'listaModelo.php';
class enviardocumentextModelo extends listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoDocumento($idDocumento)
    {
        setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');
        $tipoDocumento = $this->_db->query(" 
               SELECT
            cd_c011.*,
			cd_c012.*,
			a018.ind_usuario,
			cd_c011.fec_documento as fec_salida,
			rh_c063.ind_descripcion_cargo,
			rh_c063.pk_num_puestos,
			a004.pk_num_dependencia,
			a004.ind_dependencia,
			dep.ind_dependencia AS depExterna,
			a001.ind_descripcion_empresa,
			a003.ind_cedula_documento,
		  CONCAT(a003.ind_nombre1,' ',a003.ind_apellido1)  AS nombre_apellidos
          FROM
            cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			INNER JOIN
			a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cd_c011.fk_a018_num_seguridad_usuario
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			 LEFT JOIN
			 a004_dependencia  dep ON dep.pk_num_dependencia =  cd_c012.ind_dependencia_externa
			 LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c012.ind_organismo_externo
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_c011.ind_persona_remitente
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = cd_c011.ind_cargo_remitente 
			
		  WHERE
            cd_c012.pk_num_distribucion='$idDocumento'
             AND cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
        ");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }


    public function metDocDist($idDocumento)
    {
        $TipoDis= $this->_db->query(
           "
          SELECT
        *
          from
          cd_c012_distribucion_salida,a039_ente
         where
        (cd_c012_distribucion_salida.ind_organismo_externo=a039_ente.pk_num_ente
        or 
        cd_c012_distribucion_salida.ind_dependencia_externa=a039_ente.pk_num_ente
         or 
        cd_c012_distribucion_salida.ind_empresa_externa=a039_ente.pk_num_ente
        )
          
          and
          cd_c012_distribucion_salida.pk_num_distribucion='$idDocumento' 
    
      
         
          ");
        $TipoDis->setFetchMode(PDO::FETCH_ASSOC);
        return $TipoDis->fetch();
    }


    public function metListarTipoDocumento($usuario)
    {
        $tipoDocumento = $this->_db->query(
               "SELECT 
			cd_c011.*,
			cd_c012.*,
			a001.ind_descripcion_empresa,
			a004.ind_dependencia,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			 LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c012.ind_organismo_externo
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			    LEFT JOIN 
		  a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c011.fk_a004_num_dependencia
			 WHERE
            cd_c012.ind_estado in('PE','DV','PR')
              AND a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario
           AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
           ORDER BY cd_c011.pk_num_documento DESC

		");

        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }

    public function metModificarTipoDocumento($ind_asunto,
                                              $ind_descripcion,
                                              $fecRegistro,
                                              $fecDocument,
                                              $PersoRemit,
                                              $CargoRrmit,
                                              $pkdocument,
                                              $responsableper,
                                              $depend,
                                              $organismo,
                                              $idDocumento)
    {
        $this->_db->beginTransaction();
    
   			
			 $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c012_distribucion_salida
                      SET
                	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
				  	 fec_envio=:fec_envio, 
				  	 ind_estado=:ind_estado,
				  	  fk_a003_num_persona=:fk_a003_num_persona
                      WHERE
                    pk_num_distribucion='$idDocumento'
            ");

			$nuevoRegistro->execute(array(
				'fec_envio'=>date('Y-m-d H:i:s'),
				'ind_estado'=>'EV',
				'fk_a003_num_persona'=>$responsableper,
           ));
		   
		   
		  	$sql = "SELECT fk_cdc011_num_documento FROM cd_c012_distribucion_salida WHERE pk_num_distribucion = '$idDocumento'";
			$query = $this->_db->query($sql);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$field = $query->fetch();
						
						
			 $nuevoRegistro2=$this->_db->prepare(" 
                      UPDATE
                        cd_c011_documento_salida
                      SET
                	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),
				  	ind_estado=:ind_estado
                      WHERE
                   pk_num_documento='".$field['fk_cdc011_num_documento']."'
            ");

   	         $nuevoRegistro2->execute(array(
				'ind_estado'=>'EV',

            ));
            

			  $nuevoRegistro3=$this->_db->prepare("
                  INSERT INTO
                     cd_e001_historico
                  SET
                     	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  		fec_ultima_modificacion=NOW(),  
						fec_historico=:fec_historico,  
						fk_cdc011_num_documento=:fk_cdc011_num_documento, 
                   		fk_cdc012_num_distribucion=:fk_cdc012_num_distribucion, 
						fk_a003_num_persona=:fk_a003_num_persona, 						 
						fk_a004_num_dependencia=:fk_a004_num_dependencia,
						fk_a001_num_organismo=:fk_a001_num_organismo
                ");
            $nuevoRegistro3->execute(array(
                'fec_historico'=>date('Y-m-d'),
				'fk_cdc011_num_documento'=>$pkdocument,
                'fk_cdc012_num_distribucion'=>$idDocumento,
                'fk_a003_num_persona'=>$responsableper,
                'fk_a004_num_dependencia'=>$depend,
				'fk_a001_num_organismo'=>$organismo
            ));

			 $idDocumentoext=$this->_db->lastInsertId();

			  $nuevoRegistro4=$this->_db->prepare("
                  INSERT INTO
                     cd_e002_historico_detalle
                  SET
                     	 fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  		fec_ultima_modificacion=NOW(),  
						ind_asunto=:ind_asunto, 
						txt_descripcion=:txt_descripcion,
						fec_registro=:fec_registro,
						fec_documento=:fec_documento, 
						ind_persona_remitente=:ind_persona_remitente, 
						ind_cargo_remitente=:ind_cargo_remitente, 
						fec_annio=:fec_annio, 
						fec_envio=NOW(), 
						ind_estado=:ind_estado, 
						fk_cde001_num_historico='$idDocumentoext'
                ");
            $nuevoRegistro4->execute(array(
			'ind_asunto'=>$ind_asunto,
			'txt_descripcion'=>$ind_descripcion,
			'fec_registro'=>$fecRegistro,
			'fec_documento'=>$fecDocument,
			'ind_persona_remitente'=>$PersoRemit,
			'ind_cargo_remitente'=>$CargoRrmit,
			'fec_annio'=>date('Y'),
			'ind_estado'=>'EV'

           ));


		
            $error = $nuevoRegistro->errorInfo();
            $error2 = $nuevoRegistro2->errorInfo();
            $error3 = $nuevoRegistro3->errorInfo();
            $error4 = $nuevoRegistro4->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }elseif(!empty($error2[1]) && !empty($error2[2])){
                $this->_db->rollBack();
                return $error2;
            }elseif(!empty($error3[1]) && !empty($error3[2])){
                $this->_db->rollBack();
                return $error3;
            }elseif(!empty($error4[1]) && !empty($error4[2])){
                $this->_db->rollBack();
                return $error4;
            }else{
                $this->_db->commit();
                return $nuevoRegistro2;
            }
    }

}
