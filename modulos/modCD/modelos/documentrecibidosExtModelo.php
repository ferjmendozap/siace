<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Distribucion Salida Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
  
require_once 'listaModelo.php';
class documentrecibidosExtModelo extends  listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }
	
	public function metMostrarTipoDocumento($idDocumento)
    {
        $tipoDocumento = $this->_db->query(" 
               SELECT 
			cd_c004.*,
			cd_c005.*,
			a004.ind_dependencia,
		  CONCAT(ind_nombre1,'   ',ind_apellido1)  AS nombre_apellidos,
			cd_c003.ind_descripcion
			FROM 
			 cd_c005_distribucion_entrada cd_c005 
			INNER JOIN
			 cd_c004_documento_entrada cd_c004 ON cd_c004.pk_num_documento =  cd_c005.fk_cdc004_num_documento
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c005.ind_dependencia_destinataria
			LEFT JOIN
			a003_persona  a003 ON a003.pk_num_persona =  cd_c005.ind_persona_destinataria
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c004.fk_cdc003_num_tipo_documento  
		  WHERE
            cd_c004.pk_num_documento='$idDocumento'
            
        ");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	
   
    public function metListarTipoDocumento($usuario)
    {
        $tipoDocumento = $this->_db->query(
               "SELECT 
			cd_c004.*,
			cd_c005.*,
			a004.ind_dependencia,
		  CONCAT(ind_nombre1,'   ',ind_apellido1)  AS nombre_apellidos,
			cd_c003.ind_descripcion,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM 
			 cd_c005_distribucion_entrada cd_c005 
			INNER JOIN
			 cd_c004_documento_entrada cd_c004 ON cd_c004.pk_num_documento =  cd_c005.fk_cdc004_num_documento
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c005.ind_dependencia_destinataria
			LEFT JOIN
			a003_persona  a003 ON a003.pk_num_persona =  cd_c005.ind_persona_destinataria
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c004.fk_cdc003_num_tipo_documento
			INNER JOIN 
		    a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c005.ind_dependencia_destinataria
			WHERE
            cd_c004.fk_cdc003_num_tipo_documento='2' 
            AND cd_c005.ind_estado='EV' 
            and a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario
            AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
            ORDER BY cd_c004.pk_num_documento DESC

		");
		
		$tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }
	

		

 public function metAcuseReciboExt($idDocumento)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                 UPDATE
                cd_c005_distribucion_entrada
                SET fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  fec_acuse=:fec_acuse, ind_estado=:ind_estado  
				 WHERE
                    pk_num_entrada='$idDocumento'
            ");
            $elimar->execute(array(
              	'fec_acuse'=>date('Y-m-d H:i:s'),
			    'ind_estado'=>'RE'
            ));
			
			$sql = "SELECT fk_cdc004_num_documento FROM cd_c005_distribucion_entrada WHERE pk_num_entrada = '$idDocumento'";
			$query = $this->_db->query($sql);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$field = $query->fetch();
			
			$elimar=$this->_db->prepare("
                      UPDATE
                        cd_c004_documento_entrada
                      SET
                	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),ind_estado=:ind_estado
                      WHERE
                   pk_num_documento='".$field['fk_cdc004_num_documento']."'
            ");
		
   	         $elimar->execute(array(
				'ind_estado'=>'RE',

            ));
			
            $error=$elimar->errorInfo();
	
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
    }



  
}
