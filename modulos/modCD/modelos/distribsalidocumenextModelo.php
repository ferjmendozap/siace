<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Distribucion Salida Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 
require_once 'listaModelo.php';
class distribsalidocumenextModelo extends  listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }
	
	public function metMostrarTipoDocumento($idDocumento)
    {
	
	
        $tipoDocumento = $this->_db->query(" 
              SELECT
            cd_c011.*,
			cd_c012.*,
			a018.ind_usuario,
			DATE_FORMAT( cd_c011.fec_documento  ,'%d de %M de %Y') as fec_salida,
			a004.pk_num_dependencia,
			a004.ind_dependencia,
			dep.ind_dependencia AS depExterna,
			a001.ind_descripcion_empresa,
			a003.ind_cedula_documento,
		  CONCAT(a003.ind_nombre1,'   ',a003.ind_apellido1)  AS nombre_apellidos
          FROM
            cd_c011_documento_salida cd_c011
			 INNER JOIN
           cd_c012_distribucion_salida cd_c012 ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			INNER JOIN
			a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cd_c011.fk_a018_num_seguridad_usuario
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			 LEFT JOIN
			 a004_dependencia  dep ON dep.pk_num_dependencia =  cd_c012.ind_dependencia_externa
			 LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c012.ind_organismo_externo
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_c012.fk_a003_num_persona
		  WHERE
            cd_c012.pk_num_distribucion='$idDocumento' AND cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
        ");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	
   
    public function metListarTipoDocumento($usuario)
    {
        $tipoDocumento = $this->_db->query(
               "SELECT 
			cd_c011.*,
			cd_c012.*,
			a001.ind_descripcion_empresa,
			a004.ind_dependencia,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM  cd_c012_distribucion_salida cd_c012 
			LEFT JOIN
            cd_c011_documento_salida cd_c011  ON cd_c012.fk_cdc011_num_documento = cd_c011.pk_num_documento
			LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c012.ind_organismo_externo
			LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			LEFT JOIN 
		    a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c011.fk_a004_num_dependencia
			WHERE
            cd_c012.ind_estado='EV'
               AND a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario
           AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
           ORDER BY cd_c011.pk_num_documento DESC

		");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }
	
	public function metdevolucionDocumento($asunto,$descripcion,$fecReg,$fecDoc,$PersoRmit,$CargoRmit,$fecEnvio,$fec_devol,$motivodevol,$pkdoc,$responsable,$org,$dep,$idDocumento)
    {
        $this->_db->beginTransaction();
	
		$nuevoRegistro=$this->_db->prepare("
                 UPDATE
                cd_c012_distribucion_salida
                SET fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),  ind_estado=:ind_estado ,  fec_envio=:fec_envio, ind_motivo_devolucion=:ind_motivo_devolucion,  fec_devolucion=:fec_devolucion
				 WHERE
                    pk_num_distribucion='$idDocumento'
            ");
            $nuevoRegistro->execute(array(
				'ind_estado'=>'PE',
				'fec_envio'=>'',
				'ind_motivo_devolucion'=>'',
				'fec_devolucion'=>'',
        ));
		
		  	$sql = "SELECT fk_cdc011_num_documento FROM cd_c012_distribucion_salida WHERE pk_num_distribucion = '$idDocumento'";
			$query = $this->_db->query($sql);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$field = $query->fetch();
						
						
			 $nuevoRegistro=$this->_db->prepare(" 
                      UPDATE
                        cd_c011_documento_salida
                      SET
                	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),ind_estado=:ind_estado
                      WHERE
                   pk_num_documento='".$field['fk_cdc011_num_documento']."'
            ");
		
   	         $nuevoRegistro->execute(array(
				'ind_estado'=>'PR',

            ));
			
		$nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                     cd_e001_historico
                  SET
                     	 fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  		fec_ultima_modificacion=NOW(),  
						  fec_historico=:fec_historico,  
						  fk_cdc011_num_documento=:fk_cdc011_num_documento, 
                   		 fk_cdc012_num_distribucion=:fk_cdc012_num_distribucion, 
						 fk_a003_num_persona=:fk_a003_num_persona, 						 
						 fk_a004_num_dependencia=:fk_a004_num_dependencia,
						 fk_a001_num_organismo=:fk_a001_num_organismo
                ");
            $nuevoRegistro->execute(array(
                'fec_historico'=>date('Y-m-d'),
				'fk_cdc011_num_documento'=>$pkdoc,
                'fk_cdc012_num_distribucion'=>$idDocumento,
                'fk_a003_num_persona'=>$responsable,
                'fk_a004_num_dependencia'=>$dep,
				'fk_a001_num_organismo'=>$org
            ));
			
			 $idDocumento=$this->_db->lastInsertId();
			 
			  $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                     cd_e002_historico_detalle
                  SET
                     	 fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  		fec_ultima_modificacion=NOW(),  
						ind_asunto=:ind_asunto, 
						txt_descripcion=:txt_descripcion,
						fec_registro=:fec_registro,
						fec_documento=:fec_documento, 
						ind_persona_remitente=:ind_persona_remitente, 
						ind_cargo_remitente=:ind_cargo_remitente, 
						fec_annio=:fec_annio, 
						fec_envio=:fec_envio, 
						fec_devolucion=:fec_devolucion, 
						ind_motivo_devolucion=:ind_motivo_devolucion, 
						ind_estado=:ind_estado, 
						fk_cde001_num_historico='$idDocumento'
                ");
            $nuevoRegistro->execute(array(
			'ind_asunto'=>$asunto,
			'txt_descripcion'=>$descripcion,
			'fec_registro'=>$fecReg,
			'fec_documento'=>$fecDoc,
			'ind_persona_remitente'=>$PersoRmit,
			'ind_cargo_remitente'=>$CargoRmit,
			'fec_annio'=>date('Y'),
			'fec_envio'=>$fecEnvio,
			'fec_devolucion'=>$fec_devol,
			'ind_motivo_devolucion'=>$motivodevol,
			'ind_estado'=>'DV'
			
           ));
		
	
	
             $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }

    }
	
		 public function metAcuseRecibo($asunto,$descripcion,$fecReg,$fecDoc,$PersoRmit,$CargoRmit,$fecEnvio,$fec_acuse,$pkdoc,$responsable,$org,$dep,$idDocumento)
    {
        $this->_db->beginTransaction();
         			
			 $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c012_distribucion_salida
                      SET
                	  fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(), fec_acuse=:fec_acuse, ind_estado=:ind_estado
                      WHERE
                    pk_num_distribucion='$idDocumento'
            ");
			
			$nuevoRegistro->execute(array(
				'fec_acuse'=>date('Y-m-d H:i:s'),
				'ind_estado'=>'RE',
	
           ));
			
		 
			$sql = "SELECT fk_cdc011_num_documento FROM cd_c012_distribucion_salida WHERE pk_num_distribucion = '$idDocumento'";
			$query = $this->_db->query($sql);
			$query->setFetchMode(PDO::FETCH_ASSOC);
			$field = $query->fetch();
						
						
			 $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c011_documento_salida
                      SET
                	fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  	fec_ultima_modificacion=NOW(),ind_estado=:ind_estado
                      WHERE
                   pk_num_documento='".$field['fk_cdc011_num_documento']."'
            ");
		
   	         $nuevoRegistro->execute(array(
				'ind_estado'=>'RE',

            ));
            			
				$nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                     cd_e001_historico
                  SET
                     	 fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  		fec_ultima_modificacion=NOW(),  
						  fec_historico=:fec_historico,  
						  fk_cdc011_num_documento=:fk_cdc011_num_documento, 
                   		 fk_cdc012_num_distribucion=:fk_cdc012_num_distribucion, 
						 fk_a003_num_persona=:fk_a003_num_persona, 						 
						 fk_a004_num_dependencia=:fk_a004_num_dependencia,
						 fk_a001_num_organismo=:fk_a001_num_organismo
                ");
            $nuevoRegistro->execute(array(
                'fec_historico'=>date('Y-m-d'),
				'fk_cdc011_num_documento'=>$pkdoc,
                'fk_cdc012_num_distribucion'=>$idDocumento,
                'fk_a003_num_persona'=>$responsable,
                'fk_a004_num_dependencia'=>$dep,
				'fk_a001_num_organismo'=>$org
            ));
			
			 $idDocumento=$this->_db->lastInsertId();
			 
			  $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                     cd_e002_historico_detalle
                  SET
                     	 fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
				  		fec_ultima_modificacion=NOW(),  
						ind_asunto=:ind_asunto, 
						txt_descripcion=:txt_descripcion,
						fec_registro=:fec_registro,
						fec_documento=:fec_documento, 
						ind_persona_remitente=:ind_persona_remitente, 
						ind_cargo_remitente=:ind_cargo_remitente, 
						fec_annio=:fec_annio, 
						fec_envio=:fec_envio, 
						fec_acuse=:fec_acuse, 
						ind_estado=:ind_estado, 
						fk_cde001_num_historico='$idDocumento'
                ");
            $nuevoRegistro->execute(array(
			'ind_asunto'=>$asunto,
			'txt_descripcion'=>$descripcion,
			'fec_registro'=>$fecReg,
			'fec_documento'=>$fecDoc,
			'ind_persona_remitente'=>$PersoRmit,
			'ind_cargo_remitente'=>$CargoRmit,
			'fec_annio'=>date('Y'),
			'fec_envio'=>$fecEnvio,
			'fec_acuse'=>date('Y-m-d H:i:s'),
			'ind_estado'=>'RE'
			
           ));
			
	
		   $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idDocumento;
            }
    }




  
}
