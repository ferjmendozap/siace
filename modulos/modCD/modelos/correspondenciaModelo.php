<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class correspondenciaModelo extends Modelo
{
        public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarCorrespondencia($idCorrespondencia)
    {
        $tipoCorrespondencia = $this->_db->query("
          SELECT
              cd_c003.*,
            a018.ind_usuario
          FROM
            cd_c003_tipo_correspondencia cd_c003
			 INNER JOIN
            a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cd_c003.fk_a018_num_seguridad_usuario
         WHERE
           cd_c003.pk_num_tipo_documento='$idCorrespondencia'
        ");
        $tipoCorrespondencia->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCorrespondencia->fetch();
    }

    public function metListarCorrespondencia()
    {
        $tipoCorrespondencia = $this->_db->query(
            "SELECT * FROM cd_c003_tipo_correspondencia"
        );
        $tipoCorrespondencia->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoCorrespondencia->fetchAll();
    }

    public function metCrearCorrespondencia($descripcion,$descripcioncorta,$procedencia,$status)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    cd_c003_tipo_correspondencia
                  SET
                     fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_descripcion=:ind_descripcion,
                    num_estatus=:num_estatus ,ind_descripcion_corta=:ind_descripcion_corta , ind_procedencia=:ind_procedencia
                ");
            $nuevoRegistro->execute(array(
                'ind_descripcion'=>$descripcion,
                'num_estatus'=>$status,
                'ind_descripcion_corta'=>$descripcioncorta,
                'ind_procedencia'=>$procedencia,
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarCorrespondencia($descripcion,$descripcioncorta,$procedencia,$status,$idCorrespondencia)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                      UPDATE
                        cd_c003_tipo_correspondencia
                      SET
                         fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), ind_descripcion=:ind_descripcion,
                        num_estatus=:num_estatus ,ind_descripcion_corta=:ind_descripcion_corta , ind_procedencia=:ind_procedencia
                      WHERE
                        pk_num_tipo_documento='$idCorrespondencia'
            ");
            $nuevoRegistro->execute(array(
                'ind_descripcion'=>$descripcion,
                'num_estatus'=>$status,
                'ind_descripcion_corta'=>$descripcioncorta,
                'ind_procedencia'=>$procedencia,
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idCorrespondencia;
            }
    }

    public function metEliminarCorrespondencia($idCorrespondencia)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM cd_c003_tipo_correspondencia WHERE pk_num_tipo_documento=:pk_num_tipo_documento
            ");
            $elimar->execute(array(
                'pk_num_tipo_documento'=>$idCorrespondencia
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idCorrespondencia;
            }

    }

}
