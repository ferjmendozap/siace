<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Guidmar Espinoza     | g.espinoza@contraloriamonagas.gob.ve    | 0414-1913443      | MONAGAS
 * | 2 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_MODELO . 'paisModelo.php';
require_once RUTA_MODELO . 'ciudadModelo.php';
require_once RUTA_MODELO . 'estadoModelo.php';
require_once RUTA_Modulo . 'modCP' . DS . 'modelos' . DS . 'serviciosModelo.php';
require_once RUTA_Modulo . 'modCP' . DS . 'modelos' . DS . 'documentoModelo.php';

class proveedorModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atPaisModelo = new paisModelo();
        $this->atEstadoModelo = new estadoModelo();
        $this->atTipoServicioModelo = new serviciosModelo();
        $this->atDocumentoModelo = new documentoModelo();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metListaPersona()
    {
        $personaPost = $this->_db->query("
            SELECT
              persona.*,
              dependencia.ind_dependencia
            FROM
              a003_persona AS persona 
              LEFT JOIN rh_b001_empleado AS empleado ON persona.pk_num_persona = empleado.fk_a003_num_persona
              LEFT JOIN rh_c076_empleado_organizacion AS organizacion ON organizacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
              LEFT JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
              ");
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        return $personaPost ->fetchAll();
    }

    public function metListarProveedor()
    {
        $proveedorPost = $this->_db->query("
          SELECT
              proveedor.*,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nomProveedor,
			  concat_ws(' ',p.ind_nombre1,p.ind_nombre2,p.ind_apellido1,p.ind_apellido2) AS nomRepresentante,
              persona.ind_documento_fiscal,
              persona.num_estatus
          FROM
            lg_b022_proveedor AS proveedor
          INNER JOIN a003_persona AS persona ON persona.pk_num_persona =  proveedor.fk_a003_num_persona_proveedor
		  INNER JOIN a003_persona AS p ON p.pk_num_persona =  proveedor.fk_a003_num_persona_representante
		  
              ");
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetchAll();
    }

    public function metListarTelefonos($idPersona)
    {
        $proveedorPost = $this->_db->query("
              SELECT
            telefono.*
            FROM lg_b022_proveedor AS proveedor
            INNER JOIN a007_persona_telefono AS telefono ON telefono.fk_a003_num_persona =  proveedor.fk_a003_num_persona_proveedor
            AND proveedor.fk_a003_num_persona_proveedor = '$idPersona'
            ");
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetchAll();
    }

    public function metServiciosProveedor($idProveedor)
    {
        $proveedorPost = $this->_db->query("
            SELECT
            serv.*
            FROM lg_b022_proveedor AS proveedor
            INNER JOIN lg_e003_proveedor_servicio AS pserv ON proveedor.pk_num_proveedor = pserv.fk_lgb022_num_proveedor
            INNER JOIN cp_b017_tipo_servicio AS serv ON serv.pk_num_tipo_servico = pserv.fk_cpb017_num_tipo_servicio
            WHERE proveedor.pk_num_proveedor = '$idProveedor'
            ");
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetchAll();
    }

    public function metDocumentoProveedor($idProveedor)
    {
        $proveedorPost = $this->_db->query("
            SELECT
            doc.*
            FROM lg_b022_proveedor AS proveedor
            INNER JOIN lg_e002_proveedor_documento AS pdoc ON proveedor.pk_num_proveedor = pdoc.fk_lgb022_num_proveedor
            INNER JOIN cp_b002_tipo_documento AS doc ON doc.pk_num_tipo_documento = pdoc.fk_cpb002_num_tipo_documento
            WHERE proveedor.pk_num_proveedor = '$idProveedor'
            ");
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetchAll();
    }

    public function metBuscarPersona($cedula)
    {
        $proveedorPost = $this->_db->query("
            SELECT
            *
            FROM a003_persona
            WHERE ind_cedula_documento = '$cedula' OR ind_documento_fiscal = '$cedula'
            ");
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetch();
    }

    public function metBuscarPersonaProveedor($idPersona)
    {
        $proveedorPost = $this->_db->query("
            SELECT
            *
            FROM lg_b022_proveedor
            WHERE fk_a003_num_persona_proveedor = '$idPersona'
            ");
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetch();
    }

    public function metBuscarProveedor($idProveedor)
    {
        $proveedorPost = $this->_db->query("
              SELECT
                  proveedor.*,
                  concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nomProveedor,
                  persona.*,
                  persona.num_estatus AS estado_persona,
                  estado.*,
                  ciudad.*,
                  vendedor.pk_num_persona AS contactoId,
                  vendedor.ind_documento_fiscal AS contactoCed,
                  vendedor.ind_nombre1 AS contactoNom1,
                  vendedor.ind_apellido1 AS contactoAp1,
                  repre.pk_num_persona AS repId,
                  repre.ind_documento_fiscal AS repCed,
                  repre.ind_nombre1 AS repNom1,
                  repre.ind_apellido1 AS repAp1,
                  direccion.*
              FROM lg_b022_proveedor AS proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona =  proveedor.fk_a003_num_persona_proveedor
              INNER JOIN a010_ciudad AS ciudad ON persona.fk_a010_num_ciudad = ciudad.pk_num_ciudad
              INNER JOIN a009_estado AS estado ON estado.pk_num_estado = ciudad.fk_a009_num_estado
              INNER JOIN a003_persona AS vendedor ON vendedor.pk_num_persona =  proveedor.fk_a003_num_persona_vendedor
              INNER JOIN a003_persona AS repre ON repre.pk_num_persona =  proveedor.fk_a003_num_persona_representante
              LEFT JOIN a036_persona_direccion AS direccion ON persona.pk_num_persona = direccion.fk_a003_num_persona
              WHERE
                  proveedor.pk_num_proveedor = '$idProveedor'"
        );
        $proveedorPost->SETFetchMode(PDO::FETCH_ASSOC);
        return $proveedorPost->fetch();
    }

    public function metCrearProveedor(
        $docfiscal,$nombre1,$nombre2 = null,$apellido1 = null,
        $apellido2 = null,$sexo,$nacionalidad,$edoCivil,
        $dirE,$tipopersona,
        $estatus,$idCiudad,$tel = null,$telEmer = null,

        $cant = null,$dCant,$sCant,$tpago,
        $fPago,$dId,$sId,$diasPago,
        $registro,$licencia,$fecConst,$snc,
        $nroSNC,$fSNC,$fValSNC,$condicion,
        $calificacion,$nivel,$capacidad,$representante,
        $contacto,$dirF)
    {

        $this->_db->beginTransaction();

        $NuevoPost1 = $this->_db->prepare("
            INSERT INTO
              a003_persona
            SET
              ind_cedula_documento=:ind_cedula_documento,
              ind_documento_fiscal=:ind_documento_fiscal,
              ind_nombre1=:ind_nombre1,
              ind_nombre2=:ind_nombre2,
              ind_apellido1=:ind_apellido1,
              ind_apellido2=:ind_apellido2,
              fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
              fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
              fec_nacimiento=:fec_nacimiento,
              ind_lugar_nacimiento=:ind_lugar_nacimiento,
              fk_a006_num_miscelaneo_detalle_edocivil=:fk_a006_num_miscelaneo_detalle_edocivil,
              fec_estado_civil=:fec_estado_civil,
              ind_email=:ind_email,
              ind_foto=:ind_foto,
              ind_tipo_persona=:ind_tipo_persona,
              num_estatus=:num_estatus,
              fk_a006_num_miscelaneo_det_gruposangre=:fk_a006_num_miscelaneo_det_gruposangre,
              fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona,
              fk_a010_num_ciudad=:fk_a010_num_ciudad,
              fk_a013_num_sector=:fk_a013_num_sector,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");

        $error=0;
        $persona = $this->metBuscarPersona($docfiscal);
        if($persona['pk_num_persona']==null){
            $NuevoPost1->execute(array(
                'ind_cedula_documento' => $docfiscal,
                'ind_documento_fiscal' => $docfiscal,
                'ind_nombre1' => $nombre1,
                'ind_nombre2' => $nombre2,
                'ind_apellido1' => $apellido1,
                'ind_apellido2' => $apellido2,
                'fk_a006_num_miscelaneo_detalle_sexo' => $sexo,
                'fk_a006_num_miscelaneo_detalle_nacionalidad' => $nacionalidad,
                'fec_nacimiento'=>null,
                'ind_lugar_nacimiento'=>null,
                'fk_a006_num_miscelaneo_detalle_edocivil' => $edoCivil,
                'fec_estado_civil'=>null,
                'ind_email' => $dirE,
                'ind_foto'=>null,
                'ind_tipo_persona' => $tipopersona,
                'num_estatus' => $estatus,
                'fk_a006_num_miscelaneo_det_gruposangre'=>null,
                'fk_a006_num_miscelaneo_det_tipopersona'=>null,
                'fk_a010_num_ciudad' => $idCiudad,
                'fk_a013_num_sector'=>null
            ));
            $idPersona = $this->_db->lastInsertId();
        } else {
            $idPersona = $persona['pk_num_persona'];
            $proveedor = $this->metBuscarPersonaProveedor($idPersona);
            if($proveedor['pk_num_proveedor']!=null){
                $error=1;
            }
        }

        if ($cant!=0 AND $error!=1) {
            $contador=1;
            while ($contador<=$cant) {
                $NuevoPost2 = $this->_db->prepare(
                    "INSERT INTO
                      a007_persona_telefono
                     SET
                      ind_telefono=:ind_telefono,
                      fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                      fk_a003_num_persona=$idPersona"
                );
                #execute — Ejecuta una sentencia preparada
                $NuevoPost2->execute(array(
                    'ind_telefono' => $tel[$contador],
                    'fk_a006_num_miscelaneo_detalle_tipo_telefono'=> $telEmer[$contador]
                ));
                $contador=$contador+1;
            }
        }


        if ($error!=1) {
            $NuevoPost1 = $this->_db->prepare("
                INSERT INTO
                  a036_persona_direccion
                SET
                  fk_a003_num_persona=:fk_a003_num_persona,
                  fk_a006_num_miscelaneo_detalle_domicilio=250,
                  fk_a006_num_miscelaneo_detalle_tipodir=250,
                  ind_direccion=:ind_direccion
                  ");
            $NuevoPost1->execute(array(
                'fk_a003_num_persona' => $idPersona,
                'ind_direccion' => $dirF
            ));


            $NuevoPost3 = $this->_db->prepare(
                "INSERT INTO
                  lg_b022_proveedor
                SET
                  cod_tipo_pago=:cod_tipo_pago,
                  cod_forma_pago=:cod_forma_pago,
                  cod_tipo_servicio=:cod_tipo_servicio,
                  num_dias_pago=:num_dias_pago,
                  ind_registro_publico=:ind_registro_publico,
                  ind_licencia_municipal=:ind_licencia_municipal,
                  fec_constitucion=:fec_constitucion,
                  ind_snc=:ind_snc,
                  ind_num_inscripcion_snc=:ind_num_inscripcion_snc,
                  fec_emision_snc=:fec_emision_snc,
                  fec_validacion_snc=:fec_validacion_snc,
                  ind_nacionalidad=:ind_nacionalidad,
                  ind_condicion_rcn=:ind_condicion_rcn,
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW(),
                  ind_calificacion=:ind_calificacion,
                  ind_nivel=:ind_nivel,
                  num_capacidad_financiera=:num_capacidad_financiera,
                  fk_lgb016_num_tipo_documento=:fk_lgb016_num_tipo_documento,
                  fk_a003_num_persona_representante=:fk_a003_num_persona_representante,
                  fk_a003_num_persona_vendedor=:fk_a003_num_persona_vendedor,
                  fk_a003_num_persona_proveedor=:fk_a003_num_persona_proveedor");
            $NuevoPost3->execute(array(
                'cod_tipo_pago' => $tpago,
                'cod_forma_pago' => $fPago,
                'cod_tipo_servicio' => 1,
                'num_dias_pago' => $diasPago,
                'ind_registro_publico' => $registro,
                'ind_licencia_municipal' => $licencia,
                'fec_constitucion' => $fecConst,
                'ind_snc' => $snc,
                'ind_num_inscripcion_snc' => $nroSNC,
                'fec_emision_snc' => $fSNC,
                'fec_validacion_snc' => $fValSNC,
                'ind_nacionalidad' => $nacionalidad,
                'ind_condicion_rcn' => $condicion,
                'ind_calificacion' => $calificacion,
                'ind_nivel' => $nivel,
                'num_capacidad_financiera' => $capacidad,
                'fk_lgb016_num_tipo_documento'=>1,
                'fk_a003_num_persona_representante' => $representante,
                'fk_a003_num_persona_vendedor' => $contacto,
                'fk_a003_num_persona_proveedor' => $idPersona
            ));

            $idProveedor = $this->_db->lastInsertId();

            $NuevoPost4 = $this->_db->prepare("
                INSERT INTO
                    lg_e002_proveedor_documento
                SET
                    fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                    fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
                    ");
            if ($dCant!=0) {
                $contador=1;
                while ($contador<=$dCant) {
                    #insertar
                    $NuevoPost4->execute(array(
                        'fk_cpb002_num_tipo_documento' => $dId[$contador],
                        'fk_lgb022_num_proveedor'=> $idProveedor
                    ));
                    $contador = $contador + 1;
                }
            }

            $NuevoPost5 = $this->_db->prepare("
                INSERT INTO
                    lg_e003_proveedor_servicio
                SET
                    fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                    fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
                    ");
            if ($sCant!=0) {
                $contador=1;
                while ($contador<=$sCant) {
                    #insertar
                    $NuevoPost5->execute(array(
                        'fk_cpb017_num_tipo_servicio' => $sId[$contador],
                        'fk_lgb022_num_proveedor'=> $idProveedor
                    ));
                    $contador = $contador + 1;
                }
            }
        }
        if($error==1){
            return 1;
            exit;
        }
        $error1 = $NuevoPost1->errorInfo();
        $error3 = $NuevoPost3->errorInfo();
        $error4 = $NuevoPost4->errorInfo();
        if (!empty($error1[1]) && !empty($error1[2])) {
            $this->_db->rollBack();
            return $error1;
        } elseif (!empty($error3[1]) && !empty($error3[2])) {
            $this->_db->rollBack();
            return $error3;
        } elseif (!empty($error4[1]) && !empty($error4[2])) {
            $this->_db->rollBack();
            return $error4;
        } else {
            $this->_db->commit();
            return $idProveedor;
        }
    }

    public function metModificarProveedor(
        $docfiscal,$nombre1,$nombre2 = null,$apellido1 = null,
        $apellido2 = null,$sexo,$nacionalidad,$edoCivil,
        $dirE,$tipopersona,
        $estatus,$idCiudad,$tel,$telEmer,
        $cant,$idTelefono,$dCant,$sCant,
		$tpago,$fPago,$dId,$sId,
        $diasPago,$registro,$licencia,$fecConst,
        $snc,$nroSNC,$fSNC,$fValSNC,
        $condicion,$calificacion,$nivel,$capacidad,
        $representante,$contacto,$idProveedor,$idPersona,$idDir,$dirF)
    {
        $this->_db->beginTransaction();
        $NuevoPost1 = $this->_db->prepare("
            UPDATE
              lg_b022_proveedor
            SET
              cod_tipo_pago=:cod_tipo_pago,
              cod_forma_pago=:cod_forma_pago,
              num_dias_pago=:num_dias_pago,
              ind_registro_publico=:ind_registro_publico,
              ind_licencia_municipal=:ind_licencia_municipal,
              fec_constitucion=:fec_constitucion,
              ind_snc=:ind_snc,
              ind_num_inscripcion_snc=:ind_num_inscripcion_snc,
              fec_emision_snc=:fec_emision_snc,
              fec_validacion_snc=:fec_validacion_snc,
              ind_nacionalidad=:ind_nacionalidad,
              ind_condicion_rcn=:ind_condicion_rcn,
              fk_a018_num_seguridad_usuario=$this->atIdUsuario,
              fec_ultima_modificacion=NOW(),
              ind_calificacion=:ind_calificacion,
              ind_nivel=:ind_nivel,
              num_capacidad_financiera=:num_capacidad_financiera,
              fk_a003_num_persona_representante=:fk_a003_num_persona_representante,
              fk_a003_num_persona_vendedor=:fk_a003_num_persona_vendedor
            WHERE
              pk_num_proveedor=$idProveedor"
        );
        $NuevoPost1->execute(array(
            'cod_tipo_pago'=> $tpago,
            'cod_forma_pago' => $fPago,
            'num_dias_pago' => $diasPago,
            'ind_registro_publico' => $registro,

            'ind_licencia_municipal' => $licencia,
            'fec_constitucion' => $fecConst,
            'ind_snc' => $snc,
            'ind_num_inscripcion_snc' => $nroSNC,
            'fec_emision_snc' => $fSNC,

            'fec_validacion_snc' => $fValSNC,
            'ind_nacionalidad' => $nacionalidad,
            'ind_condicion_rcn' => $condicion,
            'ind_calificacion' => $calificacion,
            'ind_nivel' => $nivel,

            'num_capacidad_financiera' => $capacidad,
            'fk_a003_num_persona_representante' => $representante,
            'fk_a003_num_persona_vendedor' => $contacto
        ));
        $NuevoPost2 = $this->_db->prepare("
            UPDATE
              a003_persona
            SET
              ind_cedula_documento=:ind_cedula_documento,
              ind_documento_fiscal=:ind_documento_fiscal,
              ind_nombre1=:ind_nombre1,
              ind_nombre2=:ind_nombre2,
              ind_apellido1=:ind_apellido1,
              ind_apellido2=:ind_apellido2,
              fk_a006_num_miscelaneo_detalle_sexo=:fk_a006_num_miscelaneo_detalle_sexo,
              fk_a006_num_miscelaneo_detalle_nacionalidad=:fk_a006_num_miscelaneo_detalle_nacionalidad,
              fec_nacimiento=:fec_nacimiento,
              ind_lugar_nacimiento=:ind_lugar_nacimiento,
              fk_a006_num_miscelaneo_detalle_edocivil=:fk_a006_num_miscelaneo_detalle_edocivil,
              fec_estado_civil=:fec_estado_civil,
              ind_email=:ind_email,
              ind_foto=:ind_foto,
              ind_tipo_persona=:ind_tipo_persona,
              num_estatus=:num_estatus,
              fk_a006_num_miscelaneo_det_gruposangre=:fk_a006_num_miscelaneo_det_gruposangre,
              fk_a006_num_miscelaneo_det_tipopersona=:fk_a006_num_miscelaneo_det_tipopersona,
              fk_a010_num_ciudad=:fk_a010_num_ciudad,
              fk_a013_num_sector=:fk_a013_num_sector,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE pk_num_persona = '$idPersona'
            ");
        $NuevoPost2->execute(array(
            'ind_cedula_documento' => $docfiscal,
            'ind_documento_fiscal' => $docfiscal,
            'ind_nombre1' => $nombre1,
            'ind_nombre2' => $nombre2,
            'ind_apellido1' => $apellido1,
            'ind_apellido2' => $apellido2,
            'fk_a006_num_miscelaneo_detalle_sexo' => $sexo,
            'fk_a006_num_miscelaneo_detalle_nacionalidad' => $nacionalidad,
            'fec_nacimiento'=>null,
            'ind_lugar_nacimiento'=>null,
            'fk_a006_num_miscelaneo_detalle_edocivil' => $edoCivil,
            'fec_estado_civil'=>null,
            'ind_email' => $dirE,
            'ind_foto'=>null,
            'ind_tipo_persona' => $tipopersona,
            'num_estatus' => $estatus,
            'fk_a006_num_miscelaneo_det_gruposangre'=>null,
            'fk_a006_num_miscelaneo_det_tipopersona'=>null,
            'fk_a010_num_ciudad' => $idCiudad,
            'fk_a013_num_sector'=>null
        ));

        if($idDir){
            $NuevoPost1 = $this->_db->prepare("
            UPDATE
              a036_persona_direccion
            SET
              fk_a003_num_persona=:fk_a003_num_persona,
              ind_direccion=:ind_direccion
            WHERE pk_num_direccion_persona=:pk_num_direccion_persona
              ");
            $NuevoPost1->execute(array(
                'fk_a003_num_persona' => $idPersona,
                'ind_direccion' => $dirF,
                'pk_num_direccion_persona'=>$idDir
            ));
        } else {
            $NuevoPost1 = $this->_db->prepare("
            INSERT INTO
              a036_persona_direccion
            SET
              fk_a003_num_persona=:fk_a003_num_persona,
              ind_direccion=:ind_direccion
              ");
            $NuevoPost1->execute(array(
                'fk_a003_num_persona' => $idPersona,
                'ind_direccion' => $dirF
            ));
        }


        $NuevoPost31 = $this->_db->prepare("
              UPDATE
                a007_persona_telefono
              SET
                ind_telefono=:ind_telefono,
                fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono
              WHERE
                pk_num_telefono=:pk_num_telefono"
        );
        $NuevoPost32 = $this->_db->prepare("
              INSERT INTO
                a007_persona_telefono
              SET
                ind_telefono=:ind_telefono,
                fk_a006_num_miscelaneo_detalle_tipo_telefono=:fk_a006_num_miscelaneo_detalle_tipo_telefono,
                fk_a003_num_persona=:fk_a003_num_persona"
        );
        if ($cant>0) {
            $contador=1;
            while ($contador<=$cant) {
                if ($idTelefono[$contador]!=0){
                    #si el telefono esta registrado lo actualiza
                    #execute — Ejecuta una sentencia preparada
                    $NuevoPost31->execute(array(
                        'ind_telefono' => $tel[$contador],
                        'fk_a006_num_miscelaneo_detalle_tipo_telefono'=> $telEmer[$contador],
                        'pk_num_telefono'=>$idTelefono[$contador]
                    ));
                } else {
                    #sino lo inserta
                    #execute — Ejecuta una sentencia preparada
                    $NuevoPost32->execute(array(
                        'ind_telefono' => $tel[$contador],
                        'fk_a006_num_miscelaneo_detalle_tipo_telefono'=> $telEmer[$contador],
                        'fk_a003_num_persona'=>$idPersona
                    ));
                }
                $contador=$contador+1;
            }
        }

        if ($dCant!=0) {
            $contador=1;
            while ($contador<=$dCant) {
                #insertar
                $NuevoPost4 = $this->_db->prepare("
                    INSERT INTO
                        lg_e002_proveedor_documento
                    SET
                        fk_cpb002_num_tipo_documento=:fk_cpb002_num_tipo_documento,
                        fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
                ");
                $NuevoPost4->execute(array(
                    'fk_cpb002_num_tipo_documento' => $dId[$contador],
                    'fk_lgb022_num_proveedor'=> $idProveedor
                ));
                $contador = $contador + 1;
            }
        }

        if ($sCant!=0) {
            $contador=1;
            while ($contador<=$sCant) {
                #insertar
                $NuevoPost5 = $this->_db->prepare("
                    INSERT INTO
                        lg_e003_proveedor_servicio
                    SET
                        fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                        fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
                ");
                $NuevoPost5->execute(array(
                    'fk_cpb017_num_tipo_servicio' => $sId[$contador],
                    'fk_lgb022_num_proveedor'=> $idProveedor
                ));
                $contador = $contador + 1;
            }
        }
        $error1 = $NuevoPost1->errorInfo();
        $error2 = $NuevoPost2->errorInfo();
        $error31 = $NuevoPost31->errorInfo();
        $error32 = $NuevoPost32->errorInfo();
        if (!empty($error1[1]) && !empty($error1[2])) {
            $this->_db->rollBack();
            return $error1;
        } elseif (!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        } elseif (!empty($error31[1]) && !empty($error13[2])) {
            $this->_db->rollBack();
            return $error31;
        } elseif (!empty($error32[1]) && !empty($error32[2])) {
            $this->_db->rollBack();
            return $error32;
        } else {
            $this->_db->commit();
            return $idProveedor;
        }
    }

    public function metEliminarTelefono($idTel)
    {
        $this->_db->beginTransaction();
        $eliminarTelf=$this->_db->query(
            "DELETE FROM a007_persona_telefono
              WHERE pk_num_telefono = '$idTel'
              ");
        $error = $eliminarTelf->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idTel;
        }
    }

    public function metEliminarProveedor($idProveedor,$idPersona)
    {
        $this->_db->beginTransaction();
        $eliminarDocumento=$this->_db->prepare(
            "DELETE FROM lg_e002_proveedor_documento
              WHERE fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
              ");
        $eliminarDocumento->execute(array("fk_lgb022_num_proveedor"=>$idProveedor));

        $eliminarServicio=$this->_db->prepare(
            "DELETE FROM lg_e003_proveedor_servicio
              WHERE fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
              ");
        $eliminarServicio->execute(array("fk_lgb022_num_proveedor"=>$idProveedor));

        $eliminarTelf=$this->_db->prepare(
            "DELETE FROM a007_persona_telefono
              WHERE fk_a003_num_persona=:fk_a003_num_persona
              ");
        $eliminarTelf->execute(array("fk_a003_num_persona"=>$idPersona));

        $eliminarDireccion=$this->_db->prepare(
            "DELETE FROM a036_persona_direccion
              WHERE fk_a003_num_persona=:fk_a003_num_persona
              ");
        $eliminarDireccion->execute(array("fk_a003_num_persona"=>$idPersona));

        $eliminarProveedor=$this->_db->prepare(
            "DELETE FROM lg_b022_proveedor
              WHERE pk_num_proveedor=:pk_num_proveedor
              ");
        $eliminarProveedor->execute(array("pk_num_proveedor"=>$idProveedor));

        $eliminarPersona=$this->_db->prepare(
            "DELETE FROM a003_persona
              WHERE pk_num_persona=:pk_num_persona
              ");
        $eliminarPersona->execute(array("pk_num_persona"=>$idPersona));

        $errorProveedor = $eliminarProveedor->errorInfo();
        $errorPersona = $eliminarPersona->errorInfo();
        $errorTelefono = $eliminarTelf->errorInfo();
        $errorDireccion = $eliminarDireccion->errorInfo();
        $errorServicio = $eliminarServicio->errorInfo();
        $errorDoc = $eliminarDocumento->errorInfo();

        if(!empty($errorTelefono[1]) && !empty($errorTelefono[2])){
            $this->_db->rollBack();
            return $errorTelefono;
        }elseif(!empty($errorServicio[1]) && !empty($errorServicio[2])){
            $this->_db->rollBack();
            return $errorServicio;
        }elseif(!empty($errorDireccion[1]) && !empty($errorDireccion[2])){
            $this->_db->rollBack();
            return $errorDireccion;
        }elseif(!empty($errorDoc[1]) && !empty($errorDoc[2])){
            $this->_db->rollBack();
            return $errorDoc;
        }elseif(!empty($errorProveedor[1]) && !empty($errorProveedor[2])){
            $this->_db->rollBack();
            return $errorProveedor;
        }elseif(!empty($errorPersona[1]) && !empty($errorPersona[2])){
            $this->_db->rollBack();
            return $errorPersona;
        }else{
            $this->_db->commit();
            return $idProveedor;
        }
    }

    public function metEliminarServicio($id1,$id2)
    {
        $this->_db->beginTransaction();
        $eliminarServicio=$this->_db->query(
            "DELETE FROM lg_e003_proveedor_servicio
              WHERE fk_cpb017_num_tipo_servicio = '$id1'
              AND fk_lgb022_num_proveedor = '$id2'
              ");
        $error = $eliminarServicio->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $id1.$id2;
        }
    }

    public function metEliminarDocumento($id1,$id2)
    {
        $this->_db->beginTransaction();
        $eliminarDocumento=$this->_db->query(
            "DELETE FROM lg_e002_proveedor_documento
              WHERE fk_cpb002_num_tipo_documento = '$id1'
              AND fk_lgb022_num_proveedor = '$id2'
              ");
        $error = $eliminarDocumento->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $id1.$id2;
        }
    }
}
?>