<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Documentos Enviados
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once 'listaModelo.php';
class documentenviadosModelo extends listaModelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTipoDocumento($idDocumento)
    {
        $tipoDocumento = $this->_db->query(" 
                SELECT
            cd_b001.*,
			cd_c001.*,
			a018.ind_usuario,
			rh_c063.ind_descripcion_cargo,
			a004.ind_dependencia,
		cd_c003.ind_descripcion AS TipoDocumento,
		  CONCAT(ind_nombre1,'   ',ind_apellido1)  AS nombre_apellidos
          FROM
            cd_b001_documento_interno cd_b001
			 INNER JOIN
           cd_c001_distribucion_interno cd_c001 ON cd_c001.fk_cdb001_num_documento = cd_b001.pk_num_documento
			INNER JOIN
			a018_seguridad_usuario a018 ON a018.pk_num_seguridad_usuario = cd_b001.fk_a018_num_seguridad_usuario
			 LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_b001.fk_a004_num_dependencia
			LEFT JOIN
			a003_persona a003 ON a003.pk_num_persona = cd_b001.ind_persona_remitente
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = a003.pk_num_persona  
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_b001.fk_cdc003_num_tipo_documento
		  WHERE
            cd_b001.pk_num_documento='$idDocumento'
        ");
		
		
			
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetch();
    }
	

    public function metListarTipoDocumento($usuario)
    {
        $tipoDocumento = $this->_db->query(
             	 "SELECT 
			cd_c011.*,
			cd_c001.*,
			a001.ind_descripcion_empresa,
			a004.ind_dependencia,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias,
			cd_c003.ind_descripcion AS TipoDocumento
			FROM   cd_c001_distribucion_interno cd_c001
			LEFT JOIN
			cd_b001_documento_interno cd_c011  ON cd_c001.fk_cdb001_num_documento = cd_c011.pk_num_documento
			 LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c011.fk_a001_num_organismo
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento
			LEFT JOIN 
		  a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c011.fk_a004_num_dependencia
			WHERE
              cd_c001.ind_estado='EV' 
               AND a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario
           AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7' 
		");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }


    public function metListarTipoDocumentoEnviados($usuario)
    {
        $tipoDocumento = $this->_db->query(
            "SELECT
			cd_c011.*,
			cd_c001.*,
			a001.ind_descripcion_empresa,
			a004.ind_dependencia,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias,
			cd_c003.ind_descripcion AS TipoDocumento
			FROM   cd_c001_distribucion_interno cd_c001
			LEFT JOIN
			cd_b001_documento_interno cd_c011  ON cd_c001.fk_cdb001_num_documento = cd_c011.pk_num_documento
			 LEFT JOIN
            a001_organismo a001 ON a001.pk_num_organismo = cd_c011.fk_a001_num_organismo
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			LEFT JOIN
			cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento
			LEFT JOIN
		  a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c011.ind_dependencia_remitente
			WHERE
              cd_c001.ind_estado='EV'
               AND a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario
           AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7'
          ORDER BY cd_c011.pk_num_documento DESC

		");

        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }

   
}
