<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        17-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class listaModeloPF extends Modelo
{

    public function __construct() 
    {
        parent::__construct();
    }

    public function metListarOrganismo()
    {
        $menu = $this->_db->query("SELECT * FROM a001_organismo WHERE
             num_estatus='1'");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
		
    }

    public function metGetOrganismosByDependencia($idDependencia)
    {
        $organismoint= $this->_db->query("SELECT `fk_a001_num_organismo` FROM `a021_dependencia_ext` WHERE `pk_num_dependencia_ext`=$idDependencia");
        $organismoint->setFetchMode(PDO::FETCH_ASSOC);
        return $organismoint->fetch();

    }

    public function metListarOrganismoReg($idOrganismo)
    {
        $organismoint= $this->_db->query("SELECT * FROM `a001_organismo`
        WHERE `ind_tipo_organismo`='E'
        AND (`pk_num_organismo`=$idOrganismo
        OR `num_estatus`=1)
        ORDER BY `ind_descripcion_empresa` ASC");
        $organismoint->setFetchMode(PDO::FETCH_ASSOC);
        return $organismoint->fetchAll();

    }
	
	public function metListarOrganismoInt()
    {
        $organismoint= $this->_db->query("SELECT * FROM a001_organismo WHERE
             num_estatus='1'");
        $organismoint->setFetchMode(PDO::FETCH_ASSOC);
        return $organismoint->fetchAll();
		
    }


    public function metListarPais()
    {
        $pais= $this->_db->query("SELECT * FROM a008_pais WHERE
             num_estatus='1'");
        $pais->setFetchMode(PDO::FETCH_ASSOC);
        return $pais->fetchAll();

    }

    public function metListarEstado($pais=false)
    {
        $sql="SELECT * FROM a009_estado WHERE num_estatus='1'";

        if($pais==false){
            $sql;
        }else{
            $sql.=" and fk_a008_num_pais=$pais";
        }

        $estado= $this->_db->query($sql);

        $estado->setFetchMode(PDO::FETCH_ASSOC);
        return $estado->fetchAll();

    }

    public function metListarMunicipio($estado=false)
    {
        $sql="SELECT * FROM a011_municipio WHERE num_estatus='1'";

        if($estado==false){
            $sql;
        }else{
            $sql.="and fk_a009_num_estado=$estado ORDER BY `ind_municipio` ASC";
        }

        $municipio= $this->_db->query($sql);
        $municipio->setFetchMode(PDO::FETCH_ASSOC);
        return $municipio->fetchAll();

    }

    public function metListarCiudad($estado=false)
    {
        $sql="SELECT * FROM a010_ciudad WHERE num_estatus='1'";

        if($estado==false) {
          $sql;
        }else{
            $sql.=" and fk_a009_num_estado=$estado";
        }

        $ciudad=$this->_db->query($sql);
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);
        return $ciudad->fetchAll();
    }

    /*public function metBuscarCiudad()
    {
        $sql = "SELECT * FROM a010_ciudad WHERE num_estatus='1' ";

        $ciudad = $this->_db->query($sql);
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);
        return $ciudad->fetchAll();
    }*/

    /*Buscar Id Municipio*/

    public function metBuscarIdMunicipio($ciudad)
    {
        if($ciudad) {
        $sql="SELECT `fk_a011_num_municipio` FROM `a010_ciudad` WHERE `pk_num_ciudad`=$ciudad";

        $municipio=$this->_db->query($sql);
        $municipio->setFetchMode(PDO::FETCH_ASSOC);
        return $municipio->fetch();
         }else{return 0;}
    }

    /*Buscar nombre municipio*/

    public function metBuscarNombreMunicipio($municipio)
    {
        if($municipio) {
        $sql="SELECT `fk_a009_num_estado` FROM `a011_municipio` WHERE `pk_num_municipio`=$municipio";

        $resultado=$this->_db->query($sql);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetchAll();
        }else{return 0;}
    }

    /*Buscar Estado municipio

    public function metBuscarMunicipio()
    {
        $sql="SELECT * FROM `a011_municipio`";

        $resultado=$this->_db->query($sql);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetchAll();
    }

    Buscar Estado x Pais

    public function metBuscarEstado()
    {

        $sql="SELECT * FROM `a009_estado` ";

        $resultado=$this->_db->query($sql);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetchAll();

    }*/

    /*Listar municipios en base al Estado*/

    public function metListarMunicipioxEstado($estado)
    {
        if($estado) {
            $sql="SELECT * FROM `a011_municipio` WHERE `fk_a009_num_estado`=$estado";

            $resultado=$this->_db->query($sql);
            $resultado->setFetchMode(PDO::FETCH_ASSOC);
            return $resultado->fetchAll();
        }else{return 0;}
    }

    /*Buscar pais*/

    /*public function metBuscarPais()
    {
        $sql="SELECT * FROM `a008_pais`";

        $resultado=$this->_db->query($sql);
        $resultado->setFetchMode(PDO::FETCH_ASSOC);
        return $resultado->fetchAll();
    }*/

    public function metListarParroquia($municipio=false)
    {
        $sql="SELECT * FROM a012_parroquia WHERE num_estatus='1'";

        if($municipio==false){
            $sql;
        }else{
            $sql.=" and fk_a011_num_municipio=$municipio";
        }

        $parroquia= $this->_db->query($sql);
        $parroquia->setFetchMode(PDO::FETCH_ASSOC);
        return $parroquia->fetchAll();

    }

    public function metListarSector($parroquia=false)
    {
        $sql="SELECT * FROM a013_sector WHERE num_estatus='1'";

        if($parroquia==false){
            $sql;
        }else{
            $sql.=" and fk_a012_num_parroquia=$parroquia";
        }

        $sector= $this->_db->query($sql);
        $sector->setFetchMode(PDO::FETCH_ASSOC);
        return $sector->fetchAll();

    }


	
	/*
	 public function metJsonOrganismo($idDependencia)
    {
        $dependencia = $this->_db->query(
            "SELECT
              *
            FROM
              a021_dependencia_ext  a021
			  INNER JOIN a001_organismo ON a001.pk_num_organismo = a021.fk_a001_num_organismo
            WHERE
              a021.fk_a001_num_organismo='$idDependencia' 
            ");
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }
*/
	public function metListaPersona()
    {
        $persona = $this->_db->query(
            "SELECT a003.*, CONCAT_WS(' ',ind_nombre1,'  ',ind_nombre2,'  ',ind_apellido1,'  ',ind_apellido2)  AS nombre_apellidos
             FROM a003_persona a003
             WHERE a003.pk_num_persona not in(SELECT pk_num_empleado_laboral FROM rh_c005_empleado_laboral)
             AND a003.num_estatus=1 AND a003.pk_num_persona not in(SELECT fk_a003_num_persona_titular FROM a041_persona_ente
             WHERE num_estatus_titular=1)"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }
	
	public function metListaRepresentante()
    {
        $persona = $this->_db->query(
            "SELECT 
			a003.*,
			rh_c063.ind_descripcion_cargo,
			a004.ind_dependencia,
		  CONCAT(ind_nombre1,'  ',ind_nombre2,'  ',ind_apellido1,'  ',ind_apellido2)  AS nombre_apellidos
			FROM
            a004_dependencia a004
			 INNER JOIN
            a003_persona a003 ON a003.pk_num_persona = a004.fk_a003_num_persona_responsable
			LEFT JOIN
			rh_c063_puestos rh_c063 ON rh_c063.pk_num_puestos = a003.pk_num_persona 
			 WHERE num_estatus_persona=1
			"
		);
        $persona->setFetchMode(PDO::FETCH_ASSOC); 
        return $persona->fetchAll();
    }
	
		 public function metListaCiudad()
    {
        $ciudad = $this->_db->query("SELECT * FROM a010_ciudad");
        $ciudad->setFetchMode(PDO::FETCH_ASSOC);
        return $ciudad->fetchAll();
		
    }

	 public function metListarDependencia()
    {
        $exdependencia = $this->_db->query("SELECT * FROM a021_dependencia_ext");
        $exdependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $exdependencia->fetchAll();
		
    }
	
	public function metListarMiscelaneos()
    {
        $miscelaneo = $this->_db->query("SELECT * FROM a006_miscelaneo_detalle  WHERE pk_num_miscelaneo_detalle in('2','3','4','14')");
        $miscelaneo->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneo->fetchAll();
	}
	
	public function metListarMiscelaneosp()
    {
       $miscelaneop = $this->_db->query("SELECT * FROM a006_miscelaneo_detalle  WHERE pk_num_miscelaneo_detalle in('1','5')");
      	$miscelaneop->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneop->fetchAll();
	}
	
	public function metListarDependenciaInt()
    {
        $dependenciaint = $this->_db->query("SELECT * FROM a004_dependencia");
        $dependenciaint->setFetchMode(PDO::FETCH_ASSOC);
        return $dependenciaint->fetchAll();
		
    }
	
	  public function metListarCorrespondencia()
    {
        $correspondencia = $this->_db->query("SELECT * FROM cd_c003_tipo_correspondencia WHERE pk_num_tipo_documento in('1','3','5')");
        $correspondencia->setFetchMode(PDO::FETCH_ASSOC);
        return $correspondencia->fetchAll();
		
    }
	
	 public function metListarCorrespon()
    {
        $corresp = $this->_db->query("SELECT * FROM cd_c003_tipo_correspondencia WHERE pk_num_tipo_documento in('2','4')");
        $corresp->setFetchMode(PDO::FETCH_ASSOC);
        return $corresp->fetchAll();
		
    }

	

}
