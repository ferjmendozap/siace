<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Preparar Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
//require_once 'listaModelo.php';
class distribdocumenintModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

   
    public function metListarTipoDocumento($usuario)
    {
        $tipoDocumento = $this->_db->query(
               "SELECT 
			cd_c011.*,
			cd_c012.*,
			cd_c003.ind_descripcion,
			a004.ind_dependencia,
			a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM   cd_c001_distribucion_interno cd_c012
			 LEFT JOIN
           cd_b001_documento_interno cd_c011 ON cd_c012.fk_cdb001_num_documento = cd_c011.pk_num_documento
			 LEFT JOIN
            cd_c003_tipo_correspondencia cd_c003 ON cd_c003.pk_num_tipo_documento = cd_c011.fk_cdc003_num_tipo_documento
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  cd_c011.fk_a004_num_dependencia
			 LEFT JOIN 
		  a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=cd_c011.fk_a004_num_dependencia
			  WHERE
             a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario AND
             a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='7' ORDER BY cd_c011.pk_num_documento DESC
		");
		
        $tipoDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoDocumento->fetchAll();
    }
		

  
}
