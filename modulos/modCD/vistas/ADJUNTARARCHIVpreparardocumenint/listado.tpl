<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Documentos Internos | Preparar</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                   <th width="100">Nro de Documento</th>
								  <th  width="190">Remitente</th>
                                <th  width="100">Asunto</th>
								<th  width="200">Comentario</th>
								 <th  width="70">Estado</th>
                                <th width="70">Contenido</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
                                    <td><label>{$documento.ind_documento_completo}</label></td>
                                    <td><label>{$documento.ind_dependencia}</label></td>
									<td><label>{$documento.ind_asunto}</label></td>
									 <td><label>{$documento.ind_descripcion} </label></td>
									 <td>
									 <label>{if $documento.ind_estado=='PR'}Preparacion{/if}</label>
									 
									 </td>
                                   
                                    <td align="left">
                                        {if in_array('CD-01-01-03-02-01-N',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Preparar Documento"
                                                    descipcion="El Usuario a preparado un Documento Interno" titulo="<i class='icm icm-cog3'></i>&nbsp;Editor de Documentos Internos">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                       
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/preparardocumenintCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
       
    });
</script>