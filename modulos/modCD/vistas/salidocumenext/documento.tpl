<form action="{$_Parametros.url}modCD/enviardocumentextCONTROL/ImprmirMET"  autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
<div id="imprimeme">

<table id="principal"  align="center" border="0">
<tr><td width="707">
  <!-- *********************** -->
  <table align="center">
  <tr><td>
  <!-- CABECERA DEL DOCUMENTO -->
  <table width="679" align="right" id="cabecera" cellpadding="0" cellspacing="0">
  <tr>
   <td width="3"></td>
   <td width="124" align="center"><img src="{$_Parametros.url}publico/imagenes/modCD/logo.png" style="height:70px; width:80px" ></td>
   <td width="10"></td>
   <td width="420">
   <!-- *********************** -->
   <table cellpadding="0" cellspacing="0">
   <tr><td align="center" width="414"><font size="2" face="Arial">REPUBLICA BOLIVARIANA DE VENEZUELA</font></td></tr>
   <tr><td align="center"><font size="2" face="Arial">CONTRALORIA DEL ESTADO BOLIVAR</font></td></tr>
   <tr><td align="center"><font size="2" face="Arial">DESPACHO DEL CONTRALOR</font></td></tr>
   </table>
   <!-- *********************** -->
   </td>
   <td width="120" align="center"> <div align="right"><img src="{$_Parametros.url}publico/imagenes/modCD/Logo SNCF.jpg" style="height:80px; width:80px" ></div></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  </table>
  <!-- FIN CABECERA DEL DOCUMENTO -->
  </td></tr>
  </table>
  <!-- *********************** -->
  
</td></tr>
<tr><td>
   <!-- *********************** -->
   <table width="688" height="27" id="numero_doc"> 
   <tr>
    <td width="26" height="21"></td>
    <td width="26"></td>
    <td width="26"></td>
    <td width="436"></td>
    <td width="150"><font face="Arial"><b></b></font></td>
   </tr>
   </table>
   <!-- *********************** -->
</td></tr>

<tr>
    <td align="right" ><font size="3" face="Arial"><b>Ciudad Bol&iacute;var {if isset($formDB.fec_salida)}{$formDB.fec_salida}{/if}</b></font></td>
   <!-- *********************** -->
</td></tr>

<tr><td>&nbsp;
  <!-- CUERPO 1 DEL DOCUMENTO -->
  <table id="cuerpo1" cellpadding="0" cellspacing="0" width="100%" border="0">
  <tr>
    <td width="85"><font face="Arial" size="4"><b>Oficio N&deg;: </b></font></td>
    <td width="563"><font face="Arial" size="4"> <b>{if isset($formDB.ind_cod_completo)}{$formDB.ind_cod_completo}{/if}</b></font></td>
  </tr>
  <tr>
    <td><font face="Arial" size="3">Ciudadano(a):<br /></font></td>
    <td width="563"><font face="Arial" size="3"></font></td>
  </tr>
  <tr>
    <td colspan="2"><font face="Arial" size="3">{if isset($formDB.ind_representante_externo)}{$formDB.ind_representante_externo}{/if}</font></td>
  </tr>
 <tr>
    <td colspan="2"><font face="Arial" size="3">{if isset($formDB.ind_cargo_externo)}{$formDB.ind_cargo_externo}{/if}</font></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr>
<td align="justify" width="95%"><font face="Arial" size="2"><br />{if isset($formDB.txt_contenido)}{$formDB.txt_contenido}{/if}</font>
  <!-- CONTENIDO DEL DOCUMENTO -->
 
  </td>
  <!-- *********************** -->
<tr><td>
  <!-- *********************** -->
  <table  height="300"  align="center" id="atentamente" width="700" border="0">
  <tr><br /><br /><br /><br /><br />
    <td width="58"></td>
    <td align="center"><font face="Arial" size="3">Atentamente,</font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td height="25"></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center"></td>
    <td width="61"></td>
  </tr>
  <tr>

    <td width="58"></td>
    <td align="center" width="365">________________________________</td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center" width="600"><font face="Arial" size="3">Licda. GLINYS HERNANDEZ
	<br />CONTRALOR DEL ESTADO BOLIVAR</font><br />
	<font face="Arial" size="1">Designado mediante Resoluci&oacute;n N&deg;. 01-00-000003 de Fecha 05-01-2009,<br />
														Emanada del Despacho del Contralor General de la Rep&uacute;blica

	</font></td>
    <td> </td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td align="left">

</td></tr>
<!--<center><input type="button" name="imprimir" value="Imprimir" onclick="window.print();"></center>-->
</table>

</td>
</tr>

</table>

			</div>		   
					<div align="center">
<div class="btn-group">
                                <a id="imprime" class="btn btn-icon-toggle btn-refresh">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
		  
                </div>
      
</form>

<script type="text/javascript">


 $(document).ready(function() {
        $("#imprime").click(function () {
            $("#myPrintArea").printThis();
        })
    });
</script>
