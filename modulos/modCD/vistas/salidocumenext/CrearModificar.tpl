<form action="{$_Parametros.url}modCD/salidocumenextCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
        
				    <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab"><font color="#0066FF">Datos Generales</font></a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab"><font color="#0066FF">Detalle de Documento</font></a></li>
             </ul>
			 
			   <!-- Tab panes -->
            <div class="tab-content">
			 <div role="tabpanel" class="tab-pane active" id="Datos">
			   <div class="row">
			   <div class="col-sm-12">
					   <div style="size:8" class="text-primary text-center text-sm-2"><center><b>Datos del Documento</b></center></div>
					   						 	<hr class="ruler-lg">
					   </div>
			<div class="col-sm-6">
                     <div class="form-group floating-label" id="fk_cdc003_num_tipo_documentoError">
                        <select  id="fk_cdc003_num_tipo_documento"  {if isset($veranular)}disabled{/if}  name="form[alphaNum][fk_cdc003_num_tipo_documento]" class="form-control" >
                            
                            {foreach item=app from=$corresp}
                                {if isset($formDB.fk_cdc003_num_tipo_documento)}
                                    {if $app.pk_num_tipo_documento==$formDB.fk_cdc003_num_tipo_documento}
                                        <option selected value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}                        
						</select>
                           <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
                    </div>
   	
                    <div class="form-group">
                        <input type="text"  readonly  class="form-control" value="{if isset($formDB.ind_cod_completo)}{$formDB.ind_cod_completo}{/if}" name="form[txt][ind_cod_completo]" id="ind_cod_completo">
                        <label for="ind_cod_completo"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
       </div>
				
 				
				<div class="col-sm-6">

                    <div class="form-group">
                        <input type="text"   class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{else}{date('Y-m-d')}{/if}" name=""  id="fec_documento" disabled>
                        <label for="fec_documento"><i class="fa fa-calendar"></i>&nbsp;Fecha de Documento</label>
                        <input type="hidden"   class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{else}{date('Y-m-d')}{/if}" name="form[txt][fec_documento]" >
                    </div>

                    <div class="col-sm-3">
                <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_con_atencion) and $formDB.ind_con_atencion==1} checked{/if} value="1" name="form[alphaNum][ind_con_atencion]" id="checkAtencion" {if isset($veranular)}disabled{/if}>
						
							
                            <span>Atenci&oacute;n</span>
                        </label>
                    </div>
                </div>
				
 				 <div class="col-sm-9">
                    <div class="form-group floating-label" id="ind_plazo_atencionError">
                        <input type="text" {if isset($veranular)}disabled{/if}  disabled class="form-control"   value="{if isset($formDB.ind_plazo_atencion)}{$formDB.ind_plazo_atencion}{/if}" name="form[int][ind_plazo_atencion]" id="ind_plazo_atencion">
                        <label for="ind_plazo_atencion"><i class="md md-assignment-ind"></i>&nbsp;Plazo de Atencion</label>
                    </div>
                </div>
				</div>

				
			</div>
			
			<div class="row">
			
			 <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_asuntoError">
                        <input type="text" {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}" name="form[txt][ind_asunto]" id="ind_asunto"    >
                        <label for="ind_asunto"><i class="md md-question-answer"></i>&nbsp;Asunto</label>
                    </div>
			</div>
			
		<div class="col-sm-6">	
		
		 <div class="col-sm-3">
              <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_anexo) and $formDB.ind_anexo==1} checked{/if}  value="1" name="form[alphaNum][ind_anexo]"  id="checkAnexo"  {if isset($veranular)}disabled{/if}>
							
							
                            <span>Anexo</span>
                        </label>
                    </div>
                </div>
	
			 <div class="col-sm-9">	
				<div class="form-group" id="txt_descripcion_anexoError">
            			
				 <textarea name="form[alphaNum][txt_descripcion_anexo]" id="txt_descripcion_anexo"  disabled class="form-control " rows="1"     >{if isset($formDB.txt_descripcion_anexo)}{$formDB.txt_descripcion_anexo}{/if}</textarea>
                <label for="txt_descripcion_anexo">Descripcion Anexo</label>
				
            </div>
			</div>
				 </div>
		
				  
				 <div class="col-sm-12">
				<div class="form-group" id="txt_descripcionError">
                <textarea name="form[txt][txt_descripcion]" id="txt_descripcion"  {if isset($veranular)}disabled{/if}  class="form-control" rows="1" placeholder="" required data-msg-required="Introduzca la Descripci�n del Asunto"    >{if isset($formDB.txt_descripcion)}{$formDB.txt_descripcion}{/if}</textarea>
                <label for="txt_descripcion">Descripcion Asunto</label>
            </div>
			</div>
				 
				 
				 
	 
				<div class="col-sm-12">
		  <div style="size:8" class="text-primary text-center text-sm-2"><center><b>Remitente</b></center></div>
			<hr class="ruler-lg">
			<div class="col-lg-4">
			
			 <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo"   {if isset($veranular)}disabled{/if}  name="form[alphaNum][fk_a001_num_organismo]" class="form-control" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo Remitente</label>
                    </div>
					</div> 
					
								 <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="docProveedor"><i class="md md-contacts"></i>
                                             Cargo Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_cargo_remitente)}{$formDB.ind_cargo_remitente}{/if}"
                                                   id="docProveedor" disabled readonly>
                                        </div>
                                    </div>
									
									 <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="nombreProveedor"><i class="md md-group"></i>
                                              Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_persona_remitente)}{$formDB.ind_persona_remitente}{/if}"
                                                   id="nombreProveedor" disabled readonly>
                                        </div>
                                    </div>
														
              						 <div class="col-sm-3">
									 <div class="form-group floating-label" id="ind_dependencia_remitenteError">
                                            <label for="dependencia"><i class="icm icm-calculate2"></i>
                                                Dependencia Remitente</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}"
                                                   id="dependencia" disabled readonly>

                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <input  type="hidden"  class="form-control"  value="{if isset($formDB.ind_dependencia_remitente)}{$formDB.ind_dependencia_remitente}{/if}" name="form[alphaNum][ind_dependencia_remitente]" id="pk_num_dependencia">
											<input type="hidden" class="form-control"   value="{if isset($formDB.ind_persona_remitente)}{$formDB.ind_persona_remitente}{/if}" name="form[alphaNum][ind_persona_remitente]" id="nombre_apellidos">
											<input type="hidden" class="form-control"   value="{if isset($formDB.ind_cargo_remitente)}{$formDB.ind_cargo_remitente}{/if}" name="form[alphaNum][ind_cargo_remitente]" id="ind_descripcion_cargo">	
											<input type="hidden" class="form-control"   value="{if isset($formDB.num_cod_interno)}{$formDB.num_cod_interno}{/if}" name="form[alphaNum][num_cod_interno]" id="ind_codinterno">		
											 
										 
											 
											 						
											  {if isset($veranular)} {else}
                                          <button type="button"
                                                   class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                  id="num_org_ext"
                                                  data-toggle="modal" data-target="#formModal2"
                                                  data-keyboard="false" data-backdrop="static"
                                                    titulo="Buscar Dependencia"
                                                    url="{$_Parametros.url}modCD/salidocumenextCONTROL/proveedorMET/proveedor/"
                                                    >
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
											{/if}
                                        </div>
                                    </div>
			</div>
				
				<div class="col-sm-12">
					<div class="col-sm-6">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
            	</div>
			
			<div class="col-sm-6">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
					
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
			  		</div>
               </div>
				 </div>
				
				 
						  
						  </div>
					  </div>
					   
					 <!-----------DETALLE DOCUMENTO---------------------------------->		 
    <div role="tabpanel" class="tab-pane" id="Detalle">
			   
			     <div class="well clearfix">
				<div class="col-lg-12">
				 <div class="card">
                                   <div  style="padding: 0px;">
                                                <div class="table-responsive">
                                                    <table class="table no-margin" id="Organismo">
                                                        <thead>
                                                        <tr>
                                                            <td>Organismo</td>
															<td>Representante Legal</td>
															<td>Cargo</td>
															<td>Acciones</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                         {if isset($orgaextern)}
                                                            {foreach item=i from=$orgaextern}
                                                                    <tr class="idOrganismo{$i.id}">
<input type="hidden" value="{$i.id}" name="form[alphaNum][ind_organismo_externo][{$i.id}]" class="organismoInput"  organismo="{$i.id}" /> 
<input type="hidden" value="{$i.representanteor}" name="form[alphaNum][ind_representante_externo][{$i.id}]" class="representanteInput"  representanteor="{$i.representanteor}" />
<input type="hidden" value="{$i.cargoor}" name="form[alphaNum][ind_cargo_personal_externo][{$i.id}]" class="cargoInput"  cargoor="{$i.cargoor}" />
																   <td>{$i.organismo}</td>
                                                                    <td>{$i.representanteor}</td>
                                                                    <td>{$i.cargoor}</td>
                                                                    <td> {if isset($veranular)} {else}<button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idOrganismo{$i.id}"><i class="md md-delete" style="color: #ffffff;"></i></button> {/if}
																	</td>
                                                                    </tr>
                                                                {/foreach}
                                                            {/if}
															
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="4" align="right">
                                                                {if isset($veranular)} {else}
                                                                    <button type="button" 
																		   class="logsUsuario btn ink-reaction btn-raised btn-info accionModal"
                                                                            id="agregarOrganismo"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Organismos"
                                                                            url="{$_Parametros.url}modCD/organismoCONTROL/OrganismoMET/listadoOrganismo">
																+ Insertar Nuevo Org.
                                                                    </button>
					
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
									</div>	

							<div class="col-lg-12">
				 <div class="card">
                                   <div class="card-body" style="padding: 0px;">
                                                <div class="table-responsive">
                                                    <table class="table no-margin" id="Dependencia">
                                                        <thead>
                                                        <tr>
                                                            <td>Dependencia</td>
															<td>Representante Legal</td>
															<td>Cargo</td>
															<td>Acciones</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                         {if isset($depextern)}
                                                            {foreach item=i from=$depextern}
                                                                    <tr class="idDependencia{$i.id}">
<input type="hidden" value="{$i.id}" name="form[alphaNum][ind_dependencia_externa][{$i.id}]" class="dependenciaInput"  dependencia="{$i.id}" />
<input type="hidden" value="{$i.representantedep}" name="form[alphaNum][ind_representante_externo][{$i.id}]" class="representanteInput"  representantedep="{$i.representantedep}" />
<input type="hidden" value="{$i.cargodep}" name="form[alphaNum][ind_cargo_personal_externo][{$i.id}]" class="cargoInput"  cargo="{$i.cargodep}" />
																   <td>{$i.dependencia}</td>
                                                                    <td>{$i.representantedep}</td>
                                                                    <td>{$i.cargodep}</td>
                                                                    <td> {if isset($veranular)} {else}<button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idDependencia{$i.id}"><i class="md md-delete" style="color: #ffffff;"></i></button> {/if}
																	</td>
                                                                    </tr>
                                                                {/foreach}
                                                            {/if}
															
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="4" align="right">
                                                                {if isset($veranular)} {else}
                                                                    <button type="button" 
                                                                             class="logsUsuario btn ink-reaction btn-raised btn-info accionModal"
                                                                            id="agregarDependencia"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Dependencias"
                                                                            url="{$_Parametros.url}modCD/dependenciaCONTROL/DependenciaMET/listadoModal">
																+ Insertar Nuevo Dep.
                                                                    </button>
					
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
									</div>	
							 
							<div class="col-lg-12">
				 <div class="card">
                                   <div class="card-body" style="padding: 0px;">
                                                <div class="table-responsive">
                                                    <table class="table no-margin" id="Particular">
                                                        <thead>
                                                        <tr>
                                                           <td>Cedula</td>
														   <td>Nombre y Apellidos</td>
														    <td>Cargo</td>
															<td>Acciones</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                         {if isset($flag_r)}
                                                            {foreach item=i from=$flag_r}
                                                                    <tr class="idParticular{$i.id}">
	<input type="hidden" value="{$i.id}" name="form[alphaNum][flag_r][{$i.id}]" class="IdrepInput"  idParticular="{$i.id}" />
	<input type="hidden" value="{$i.particular}" name="form[alphaNum][ind_representante_externo][{$i.id}]" class="representantInput"  particular="{$i.particular}" />
	<input type="hidden" value="{$i.cargopart}" name="form[alphaNum][ind_cargo_personal_externo][{$i.id}]" class="cargInput"  cargopart="{$i.cargopart}" />
											
																   <td>{$i.cedula}</td>
																   <td>{$i.particular}</td>
																    <td>{$i.cargopart}</td>
                                                                   <td>   {if isset($veranular)} {else}<button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idParticular{$i.id}"><i class="md md-delete" style="color: #ffffff;"></i></button> {/if}
																	</td>
                                                                    </tr>
                                                                {/foreach}
                                                            {/if}
															
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="4" align="right">
                                                                 {if isset($veranular)} {else}
                                                                    <button type="button" 
                                                                          class="logsUsuario btn ink-reaction btn-raised btn-info accionModal"
                                                                            id="agregarParticular"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Particular"
                                                                            url="{$_Parametros.url}modCD/particularCONTROL/ParticularMET/MODAL">
																+ Insertar Nuevo Part.
                                                                    </button>
					
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
									</div>		
					 
					 
					 	<div class="col-lg-12">
				 <div class="card">
                                   <div class="card-body" style="padding: 0px;">
                                                <div class="table-responsive">
                                                    <table class="table no-margin" id="Proveedor">
                                                        <thead>
                                                        <tr>
														<td>Cedula</td>
                                                        <td>Proveedor</td>
														<td>Representante Legal</td>
														<td>Acciones</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                         {if isset($proveedorext)}
                                                            {foreach item=i from=$proveedorext}
                                                                    <tr class="idProveedor{$i.id}">
	<input type="hidden" value="{$i.id}" name="form[alphaNum][ind_empresa_externa][{$i.id}]" class="proveedorInput"  proveedor="{$i.id}" />
	<input type="hidden" value="{$i.representanteprov}" name="form[alphaNum][ind_representante_externo][{$i.id}]" class="represproveInput"  representanteprov="{$i.representanteprov}" />
											
																   <td>{$i.cedula}</td>
                                                                   <td>{$i.cargoprov}</td>
                                                                   <td>{$i.representanteprov}</td>
                                                                   <td>   {if isset($veranular)} {else}<button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idProveedor{$i.id}"><i class="md md-delete" style="color: #ffffff;"></i></button> {/if}
                                                                   </td>
                                                                   </tr>
                                                                {/foreach}
                                                            {/if}
															
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="4" align="right">
                                                                 {if isset($veranular)} {else}
                                                                    <button type="button" 
                                                                          class="logsUsuario btn ink-reaction btn-raised btn-info accionModal"
                                                                            id="agregarProveedor"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Proveedor"
                                                                            url="{$_Parametros.url}modCD/proveedorCONTROL/IndexMET/MODAL">
																+ Insertar Nuevo Prov.
                                                                    </button>
					
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
									</div>		
			
				
				
			  
                </div>
        <span class="clearfix"></span>
                <textarea id="ckeditor" name="form[formula][txt_contenido]" class="form-control" rows="6" style="visibility: hidden;">
                    {if isset($formDB.txt_contenido)}{$formDB.txt_contenido}{/if}
                        </textarea>

            </div>

        </div>
    </div>
       <div class="modal-footer">

        <button type="button" title="Cancelar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
			 {if isset($veranular)} {else}
        <button type="button" title="Guardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar</button>
   {/if}
    </div>
</form>

<script type="text/javascript">

	 
		
    $(document).ready(function() {

//habilitar y deshabilitar campos del filtro
      $('.checked').click(function () {
               if(this.checked=!this.checked){
                   this.checked
               }
           });
		  
		   $('#checkAnexo').click(function () {
               if(this.checked){
                   $('#txt_descripcion_anexo').attr('disabled', false);
               }else{
                   $('#txt_descripcion_anexo').attr('disabled', true);
                   $(document.getElementById('txt_descripcion_anexo')).val("");
               }
           });
		   
		 if(document.getElementById("txt_descripcion_anexo").value!=""){
			  	$("#checkAnexo").attr('checked', true);
                   $('#txt_descripcion_anexo').attr('disabled', false);
               }
			   
	$('#checkAtencion').click(function () {
               if(this.checked){
                   $('#ind_plazo_atencion').attr('disabled', false);
               }else{
                   $('#ind_plazo_atencion').attr('disabled', true);
                   $(document.getElementById('ind_plazo_atencion')).val("");
               }
           });
		   
		 if(document.getElementById("ind_plazo_atencion").value!=""){
			  	$("#checkAtencion").attr('checked', true);
                   $('#ind_plazo_atencion').attr('disabled', false);
               }
			   

		if(document.getElementById("fk_a001_num_organismo").value!=""){
                   $('#fk_a001_num_organismo').attr('readonly', true);
               }

        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
	
	
		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		//modales..
			$('#agregarOrganismo').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		    
		 $('#agregarDependencia').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		
		    $('#agregarParticular').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		  $('#agregarProveedor').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		$('#Organismo').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
		
		$('#Dependencia').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
		
	
		  $('#Particular').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
		
		$('#Proveedor').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
	
		
		    //inicializar el datepicker
  
				
	
		$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd",  language:'es' });
	//	$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
      //  $('#fec_anulado').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
	    $('#modalAncho').css("width","75%");
        $('#accion').click(function(){
		 swal({
            title: "Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
		
			/*validar*/
			var filas_org = $('#Organismo >tbody >tr').length;
			var filas_dep = $('#Dependencia >tbody >tr').length;
			var filas_per = $('#Particular >tbody >tr').length;	
			var filas_pro = $('#Proveedor >tbody >tr').length;		
			
			if(filas_org==0 && filas_dep==0 && filas_per==0 && filas_pro==0){
			    swal("Aviso!", "Debe Registrar por lo menos un (1) Organismo, Dependencia, Particular o un Proveedor.", "error");
				return false;
		    }
		    else{ 
				
				$.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='actualizar'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
               // if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                $(document.getElementById('idDocumento'+dato['idDocumento'])).html('<td>'+dato['fec_documento']+'</td>' +
				            '<td>'+dato['ind_cod_completo']+'</td>' +
                            '<td>'+dato['ind_asunto']+'</td>' +
                            '<td>'+dato['txt_descripcion']+'</td>' +
							 '<td>'+dato['ind_estado']+'</td>' +
							 
                            '<td  align="center">' +
                            '{if in_array('CD-01-01-02-01-03-M',$_Parametros.perfil)}'+
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Modificar Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '</td>');
                    swal("Registro Modificado!", "El Documento fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/salidocumenextCONTROL');
                }else if(dato['status']=='nuevo'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    //if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idDocumento'+dato['idDocumento']+'">' +
                            '<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['ind_cod_completo']+'</td>' +
                            '<td>'+dato['ind_asunto']+'</td>' +
							'<td>'+dato['txt_descripcion']+'</td>' +
                            '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-01-02-01-03-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Modificar Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
                            '</td>');
                    swal("Registro exitoso!", "El Documento fue registrado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/salidocumenextCONTROL');
                }
            },'json');
				
		}
            
        });
    });
</script>
