<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Documentos Enviados - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                   <th width="100">Nro de Documento</th>
								   <th width="30">Tipo Documento</th>
								  <th  width="190">Remitente</th>
                                <th  width="100">Asunto</th>
								<th  width="200">Comentario</th>
								 <th  width="50">Estado</th>
                                <th width="120">Ver - Documento</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
                                    <td><label>{$documento.ind_documento_completo}</label></td>
									  <td><label>{$documento.TipoDocumento}</label></td>
                                    <td><label>{$documento.ind_dependencia}</label></td>
									<td><label>{$documento.ind_asunto}</label></td>
									 <td><label>{$documento.ind_descripcion} </label></td>
									 <td>
									 <label>{if $documento.ind_estado=='EV'}Enviado{else if $documento.ind_estado=='RE'}Recibido{/if}</label>
									 
									 </td>
                                   
                                    <td align="left">
									{if $documento.ind_estado=='EV' or $documento.ind_estado=='RE'}

									 {if in_array('CD-01-01-04-02-01-V',$_Parametros.perfil)}
                                        <button class="veranular logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}"
                                                descipcion="El Usuario esta viendo un Documento" titulo="<i class='icm icm-calculate2'></i> Documentos Enviados | Ver">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
							
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										
										 {if $documento.fk_cdc003_num_tipo_documento=='1' && $documento.ind_con_copia!='2'}  
									{if in_array('CD-01-01-02-01-07-I',$_Parametros.perfil)}
                                              <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumCopiaMET/{$documento.pk_num_distribucion}/{$documento.pk_num_documento}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_documento}"
                                                        idReq="{$documento.pk_num_documento}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumCopiaMET/">
                                                     <i class="md md-assignment" style="color: #ffffff;"></i>
                                                </button>
                                            </a>
                                        {/if} 
									 
									{else if $documento.fk_cdc003_num_tipo_documento=='1' && $documento.ind_con_copia=='2'}  
									 {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
									 <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumMET/{$documento.pk_num_distribucion}/{$documento.pk_num_documento}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumMET/">
                                                     <i class="md md-assignment" style="color: #ffffff;"></i>
                                                </button>
                                            </a>
									     {/if} 	 
									
																	
			    				{else if $documento.fk_cdc003_num_tipo_documento=='5'}  
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCredencialMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCredencialMET/">
                                                    <i class="md md-assignment" style="color: #ffffff;"></i>
                                                </button>
                                            </a>

                                        {/if}
									
									{else if $documento.fk_cdc003_num_tipo_documento=='6'}  
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCircularMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCircularMET/">
                                                    <i class="md md-assignment" style="color: #ffffff;"></i>
                                                </button>
                                            </a>

                                        {/if}	
						
								 {else}  
								{if $documento.fk_cdc003_num_tipo_documento=='3'}  
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirPuntoCuentaMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirPuntoCuentaMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>

                                        {/if}
										  {/if} 													 
																		 
  		 						 {/if} 	
									 {/if}
										 
										 
                                    </td>
									
									
                                </tr>
                            {/foreach}
                        </tbody>
                      
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/documentenviadosCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.actualizar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
		 $('#datatable1 tbody').on( 'click', '.veranular', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/documentenviadosCONTROL/verMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
		
		$('#datatable1 tbody').on( 'click', '.memorandum', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/documentinternosCONTROL/memorandumMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });

				
    });
</script>