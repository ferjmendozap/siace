<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Documentos Internos | Distribuci&oacute;n</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="100">Nro de Documento</th>
								<th  width="190">Remitente</th>
								<th  width="200">Tipo Documento</th>
                                <th  width="100">Asunto</th>
								<th  width="70">Estado</th>
								<th width="50" align="left">Documento</th>
								
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
                                    <td><label>{$documento.num_cod_interno}-{$documento.num_secuencia}-{$documento.fec_annio}</label></td>                                    <td><label>{$documento.ind_dependencia}</label></td>
									 <td><label>{$documento.ind_descripcion}</label></td>
									<td><label>{$documento.ind_asunto}</label></td>
									 <td>
									 <label>{if $documento.ind_estado=='AN'}Anulado{else if $documento.ind_estado=='PR'}Preparacion{else if $documento.ind_estado=='EV'}Enviado{else if $documento.ind_estado=='PE'}Pendiente{else if $documento.ind_estado=='RE'}Recibido{/if}</label>
									 
									 </td>
									  <td align="center">
										  {if $documento.fk_cdc003_num_tipo_documento=='1' && $documento.ind_con_copia!='2'}  
									{if in_array('CD-01-01-02-01-07-I',$_Parametros.perfil)}
                                              <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumCopiaMET/{$documento.pk_num_distribucion}/{$documento.pk_num_documento}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_documento}"
                                                        idReq="{$documento.pk_num_documento}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumCopiaMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>
                                        {/if} 
									 
									{else if $documento.fk_cdc003_num_tipo_documento=='1' && $documento.ind_con_copia=='2'}  
									 {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
									 <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumMET/{$documento.pk_num_distribucion}/{$documento.pk_num_documento}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>
									     {/if} 	 
									
																	
			    				{else if $documento.fk_cdc003_num_tipo_documento=='5'}  
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCredencialMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCredencialMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>

                                        {/if}
									
									{else if $documento.fk_cdc003_num_tipo_documento=='6'}
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCircularMET/{$documento.pk_num_distribucion}/{$documento.pk_num_documento}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCircularMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>

                                        {/if}	
						
								 {else}  
								{if $documento.fk_cdc003_num_tipo_documento=='3'}  
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirPuntoCuentaMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirPuntoCuentaMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>

                                        {/if}
										  {/if} 													 
																		 
  		 						 {/if} 	
                                    </td>
								  </tr>
                            {/foreach}
                        </tbody>
                       
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript"> 
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/distribdocumenintCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		 $('#datatable1 tbody').on( 'click', '.memorandum', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/impdocumenintCONTROL//memorandumMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
        
       
    });
</script>