<form action="{$_Parametros.url}modLG/maestros/proveedorCONTROL/crearModificarProveedorMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div>
                    <div class="tab-pane" id="cabecera">
                        <div class="col-sm-6">
                            <label for="nro" class="control-label">Proveedor Nro.:{if isset($formDB.pk_num_proveedor)}{$formDB.pk_num_proveedor}{/if}</label>
                            <input type="hidden" id="idProveedor" class="form-control" name="idProveedor" value="{if isset($formDB.pk_num_proveedor)}{$formDB.pk_num_proveedor}{/if}">
                            <input type="hidden" id="idPersona" class="form-control" name="idPersona" value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}">
                        </div>
                        <div class="col-sm-6">
                            <div class="checkbox checkbox-styled">
                                <label for="estatus" class="control-label">
                                    <input type="checkbox" {if isset($ver) and $ver==1} disabled {/if} {if isset($formDB.estado_persona)} {if $formDB.estado_persona==1}checked{/if}{else}checked{/if} value="1" id="estatus" name="form[int][num_estatus]">
                                    <span>Estatus</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="text" name="nombreV" id="nombreV" class="form-control" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1} {$formDB.ind_nombre2} {$formDB.ind_apellido1} {$formDB.ind_apellido2}{/if}">
                            </div>
                        </div>
                    </div>
                    <!--end #cabecera -->
                    <div class="card-body">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <form class="form floating-label">
                                <div class="form-wizard-nav">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-primary"></div>
                                    </div>
                                    <ul class="nav nav-justified">
                                        <li class="active">
                                            <a href="#tab1" data-toggle="tab"><span class="step">1</span> <br/> <span class="title">INFORMACIÓN GENERAL</span> </a></li>
                                        <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <br/> <span class="title">INFORMACIÓN PARA PAGOS</span> </a></li>
                                        <li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <br/> <span class="title">INFORMACIÓN SNC</span> </a></li>
                                        <li><a href="#tab4" data-toggle="tab"><span class="step">4</span> <br/> <span class="title">INFORMACIÓN ADICIONAL</span> </a></li>
                                    </ul>
                                </div>
                                <!--end .form-wizard-nav -->
                                <div class="tab-content clearfix">
                                    <div class="tab-pane active" id="tab1">
                                        <!-- cabecera -->
                                        <div class="card tabs-left style-default-light">
                                            <ul class="card-head nav nav-tabs" data-toggle="tabs">
                                                <li class="active"><a href="#first5"><i class="icm icm-profile"> </i>Datos Personales</a></li>
                                                <li><a href="#second5"><i class="icm icm-location5"> </i>Dirección</a></li>
                                                <li><a href="#third5"><i class="icm icm-phone"> </i>Teléfonos </a></li>
                                            </ul>
                                            <div class="card-body tab-content style-default-bright">
                                                <div class="tab-pane active" id="first5">
                                                    <div class="card-body">
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="tipopersonaError">
                                                                    <select id="tipopersona" name="form[txt][tipopersona]" {if isset($ver) and $ver==1} disabled {/if} class="form-control select2 select2-list">
                                                                        <option value="N" {if isset($formDB.ind_tipo_persona) and $formDB.ind_tipo_persona=='N'} selected {/if}>Persona Natural</option>
                                                                        <option value="J" {if isset($formDB.ind_tipo_persona) and $formDB.ind_tipo_persona=='J'} selected {/if}>Persona Jurídica</option>
                                                                        <option value="F" {if isset($formDB.ind_tipo_persona) and $formDB.ind_tipo_persona=='F'} selected {/if}>Firma Personal</option>
                                                                    </select>
                                                                    <label for="tipoPersona">Tipo de Persona </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="docfiscalError">
                                                                    <input type="text" name="form[alphaNum][docfiscal]"
                                                                            {if isset($ver) and $ver==1} disabled {/if}
                                                                           id="docfiscal"
                                                                           data-inputmask="'mask': 'A-99999999-9'"
                                                                           value="{if isset($formDB.ind_documento_fiscal)}{$formDB.ind_documento_fiscal}{/if}"
                                                                           class="form-control"
                                                                           required="required" aria-required="true">
                                                                    <label for="docfiscal">Documento Fiscal</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group" id="nombre1Error">
                                                                <input type="text" name="form[txt][nombre1]"
                                                                        {if isset($ver) and $ver==1} disabled {/if}
                                                                       id="nombre1"
                                                                       value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1}{/if}"
                                                                       class="form-control nom"
                                                                       required="required" aria-required="true">
                                                                <label for="nombres1">Primer Nombre</label>
                                                            </div>
                                                            <div class="form-group" id="nombre2Error">
                                                                <input type="text" name="form[txt][nombre2]"
                                                                        {if isset($ver) and $ver==1} disabled {/if}
                                                                       id="nombre2"
                                                                       value="{if isset($formDB.ind_nombre2)}{$formDB.ind_nombre2}{/if}"
                                                                       class="form-control nom" title="Segundo Nombre">
                                                                <label for="nombre2">Segundo Nombre</label>
                                                            </div>
                                                            <div class="form-group" id="apellido1Error">
                                                                <input type="text" name="form[txt][apellido1]"
                                                                        {if isset($ver) and $ver==1} disabled {/if}
                                                                       id="apellido1"
                                                                       value="{if isset($formDB.ind_apellido1)}{$formDB.ind_apellido1}{/if}"
                                                                       class="form-control nom">
                                                                <label for="apellido1">Primer Apellido</label>
                                                            </div>
                                                            <div class="form-group" id="apellido2Error">
                                                                <input type="text" name="form[txt][apellido2]"
                                                                        {if isset($ver) and $ver==1} disabled {/if}
                                                                       id="apellido2"
                                                                       value="{if isset($formDB.ind_apellido2)}{$formDB.ind_apellido2}{/if}"
                                                                       class="form-control nom">
                                                                <label for="apellido2">Segundo Apellido</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12" id="nacionalidadError">
                                                            <div class="col-sm-3">
                                                                <label for="nacionalidad">Nacionalidad</label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="radio radio-styled">
                                                                    <label>
                                                                        <input type="radio" id="nacionalidad" name="form[alphaNum][nacionalidad]" value="1" {if isset($formDB.fk_a006_num_miscelaneo_detalle_nacionalidad) and $formDB.fk_a006_num_miscelaneo_detalle_nacionalidad=='1'} checked {elseif !isset($formDB.fk_a006_num_miscelaneo_detalle_nacionalidad)} checked {/if} {if isset($ver) and $ver==1} disabled {/if}>
                                                                        <span>Nacional</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <div class="radio radio-styled">
                                                                    <label>
                                                                        <input type="radio" id="nacionalidad" name="form[alphaNum][nacionalidad]" value="2" {if isset($formDB.fk_a006_num_miscelaneo_detalle_nacionalidad) and $formDB.fk_a006_num_miscelaneo_detalle_nacionalidad=='2'} checked {/if} {if isset($ver) and $ver==1} disabled {/if}>
                                                                        <span>Extranjero</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12" id="sexoError">
                                                            <div class="col-sm-3">
                                                                <label for="sexo">Sexo</label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="radio radio-styled">
                                                                    <label>
                                                                        <input type="radio" id="sexo" name="form[txt][sexo]" value="1" {if isset($formDB.fk_a006_num_miscelaneo_detalle_sexo) and $formDB.fk_a006_num_miscelaneo_detalle_sexo=='1'} checked {elseif !isset($formDB.fk_a006_num_miscelaneo_detalle_sexo)} checked {/if} {if isset($ver) and $ver==1} disabled {/if}>
                                                                        <span>Masculino</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-5">
                                                                <div class="radio radio-styled">
                                                                    <label>
                                                                        <input type="radio" id="sexo" name="form[txt][sexo]" value="2" {if isset($formDB.fk_a006_num_miscelaneo_detalle_sexo) and $formDB.fk_a006_num_miscelaneo_detalle_sexo=='2'} checked {/if} {if isset($ver) and $ver==1} disabled {/if}>
                                                                        <span>Femenino</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group" id="ind_estado_civilError">
                                                                <select {if isset($ver) and $ver==1} disabled {/if} id="ind_estado_civil"
                                                                        name="form[int][ind_estado_civil]"
                                                                        class="form-control select2 select2-list">
                                                                    <option value=""></option>
                                                                    {foreach item=ind_estado_civil from=$selectEstadoCivil}
                                                                        {if isset($formDB.fk_a006_num_miscelaneo_detalle_edocivil) and $formDB.fk_a006_num_miscelaneo_detalle_edocivil == $ind_estado_civil.cod_detalle}
                                                                            <option value="{$ind_estado_civil.cod_detalle}" selected>{$ind_estado_civil.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$ind_estado_civil.cod_detalle}">{$ind_estado_civil.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="ind_estado_civil">Estado Civil</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="second5">
                                                    <div class="card-body">
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="fk_a008_num_paisError">
                                                                    <label for="fk_a008_num_pais"><i
                                                                                class="md md-map"></i> Pais</label>
                                                                    <select id="fk_a008_num_pais" {if isset($ver) and $ver==1} disabled {/if}
                                                                            class="form-control select2-list select2">
                                                                        <option value="">Seleccione el Pais</option>
                                                                        {foreach item=i from=$listadoPais}
                                                                                {if $i.pk_num_pais==$formDB.fk_a008_num_pais}
                                                                                    <option value="{$i.pk_num_pais}" selected>{$i.ind_pais}</option>
                                                                                {else}
                                                                                    <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                                {/if}
                                                                        {/foreach}
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="fk_a009_num_estadoError">
                                                                    <label for="fk_a009_num_estado">
                                                                        <i class="md md-map"></i> Estado</label>
                                                                    <select {if isset($ver) and $ver==1} disabled {/if} id="fk_a009_num_estado"
                                                                            class="form-control select2-list select2">
                                                                        <option value="">Seleccione el Estado</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <!--div class="col-sm-6">
                                                                <div class="form-group" id="fk_a011_num_municipioError">
                                                                    <label for="fk_a011_num_municipio">
                                                                        <i class="md md-map"></i> Municipio</label>
                                                                    <select class="form-control select2-list select2" id="fk_a011_num_municipio"
                                                                            {if isset($ver) and $ver==1} disabled {/if}>
                                                                        <option value="">Seleccione el Municipio</option>
                                                                    </select>
                                                                </div>
                                                            </div-->
                                                            <div class="col-sm-6">
                                                                <div class="form-group" id="fk_a010_num_ciudadError">
                                                                    <label for="fk_a010_num_ciudad">
                                                                        <i class="md md-map"></i> Ciudad</label>
                                                                    <select name="form[int][fk_a010_num_ciudad]" id="fk_a010_num_ciudad"
                                                                            class="form-control select2-list select2"
                                                                            {if isset($ver) and $ver==1} disabled {/if}>
                                                                        <option value="">Seleccione la Ciudad</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group" id="dirFError">
                                                                <input type="hidden" name="form[int][idDir]" value="{if isset($formDB.pk_num_direccion_persona)}{$formDB.pk_num_direccion_persona}{/if}">
                                                                <input type="text" name="form[txt][dirF]" id="dirF" {if isset($ver) and $ver==1} disabled {/if}
                                                                       value="{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}"
                                                                       class="form-control"
                                                                       required="required" aria-required="true">
                                                                <label for="dirF">Dirección Fiscal</label>
                                                            </div>
                                                            <div class="form-group" id="dirSError">
                                                                <input type="text" name="form[txt][dirS]" id="dirS" {if isset($ver) and $ver==1} disabled {/if}
                                                                       value="{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}"
                                                                        class="form-control"
                                                                       aria-required="true">
                                                                <label for="dirS">Dirección Secundaria</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group" id="dirEError">
                                                                <input type="text" name="form[txt][dirE]" id="dirE" {if isset($ver) and $ver==1} disabled {/if}
                                                                       value="{if isset($formDB.ind_email)}{$formDB.ind_email}{/if}"
                                                                        class="form-control"
                                                                       required="required" aria-required="true">
                                                                <label for="dirE">Dirección Electrónica</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="third5">
                                                    <div class="card-body">
                                                        <div class="col-sm-12">
                                                            <table class="table table-striped no-margin" id="contenidoTabla">
                                                                <thead>
                                                                <tr>
                                                                    <th>Teléfonos de Contacto</th>
                                                                    <th style="width: 20px;">Emergencia</th>
                                                                    <th style="width: 20px;">Borrar</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tel">
                                                                {if isset($telefono)}
                                                                    {$cont=1}
                                                                    {foreach item=telf from=$telefono}
                                                                        <tr id="{$cont}">
                                                                            <td style="vertical-align: middle;">
                                                                                <div class="col-sm-12">
                                                                                    <div class="form-group floating-label" id="ind_telefonoError">
                                                                                        <input type="text" class="form-control" data-inputmask="'mask': '9999-999.99.99'" value="{if isset($telf.ind_telefono)}{$telf.ind_telefono}{/if}"
                                                                                               name="form[alphaNum][ind_telefono][{$cont}]" id="ind_telefono" {if isset($ver) and $ver==1} disabled {/if}>
                                                                                        <label for="ind_telefono"><i class="md md-phone-in-talk"></i> Teléfono {$cont}</label>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class="text-center" style="vertical-align: middle;">
                                                                                <div class="col-sm-12">
                                                                                    <input type="hidden" value="{$cont}" name="form[int][cant]">
                                                                                    <input type="hidden" value="{$telf.pk_num_telefono}" name="form[int][pk_num_telefono][{$cont}]">
                                                                                    <div class="checkbox checkbox-styled">
                                                                                        <label>
                                                                                            <input type="checkbox" {if isset($telf.fk_a006_num_miscelaneo_detalle_tipo_telefono) and
                                                                                            $telf.fk_a006_num_miscelaneo_detalle_tipo_telefono==2} checked {/if} value="2"
                                                                                                   name="form[int][telEmer][{$cont}]" {if isset($ver) and $ver==1} disabled {/if}>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class="text-center" style="vertical-align: middle;">
                                                                                <div class="col-sm-12">
                                                                                    <button class="borrarTelf btn ink-reaction btn-raised btn-xs btn-danger"
                                                                                            url="{$_Parametros.url}modLG/maestros/proveedorCONTROL/eliminarTelefonoMET/"
                                                                                            secuencia="{$cont}" id="{$telf.pk_num_telefono}"
                                                                                            >
                                                                                        <i class="md md-delete" style="color: #ffffff;"></i></button>
                                                                                    <div class="checkbox checkbox-styled">

                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        {$cont=$cont+1}
                                                                    {/foreach}
                                                                {/if}
                                                                </tbody>
                                                                <tfoot>
                                                                <tr>
                                                                    <td colspan="3">
                                                                        <div class="col-sm-offset-3 col-sm-6">
                                                                            <button type="button" id="nuevoCampo" class="btn btn-info ink-reaction btn-raised">
                                                                                <i class="md md-add"></i> Insertar Teléfono
                                                                            </button>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end cabecera -->
                                    </div>
                                    <!--end #tab1 -->
                                    <div class="tab-pane" id="tab2">
                                        <br>
                                        <div class="tab-content clearfix">
                                            <div class="col-sm-12">
                                                <div class="col-sm-6">
                                                    <div class="form-group" id="tipoDocError">
                                                        <label for="tipoDoc">Documento de Tipo de Pago</label>
                                                        <table class="table table-striped no-margin" id="contenidoTabla1">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 20px;">#</th>
                                                                <th style="width: 20px;">Descripción</th>
                                                                <th style="width: 20px;">Código</th>
                                                                <td style="width: 20px;">Borrar</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tipoDoc" style="overflow: auto;">

                                                            {if isset($documento)}
                                                                {$cont=1}
                                                                {foreach item=doc from=$documento}
                                                                    <tr class="idDoc{$cont}">
                                                                    <td width="10px" style="vertical-align: middle;">{$cont}</td>
                                                                    <td width="10px"><input type="text" class="form-control" readonly value="{$doc.ind_descripcion}" size="2"></td>
                                                                    <td width="10px"><input type="text" class="form-control" readonly value="{$doc.cod_tipo_documento}" size="2"></td>
                                                                    <td style="vertical-align: middle;">
                                                                        <input type="hidden" value="{$doc.pk_num_tipo_documento}" name="form[int][dId{$cont}]" size="2">
                                                                        <button class="borrar btn ink-reaction btn-raised btn-xs btn-danger"
                                                                                id1="{$doc.pk_num_tipo_documento}"
                                                                                id2="{$formDB.pk_num_proveedor}"
                                                                                url="{$_Parametros.url}modLG/maestros/proveedorCONTROL/eliminarDocumentoMET/"
                                                                                id="idDoc{$cont}">
                                                                            <i class="md md-delete" style="color: #ffffff;"></i></button></td>
                                                                    </tr>
                                                                    {$cont=$cont+1}
                                                                {/foreach}
                                                            {/if}
                                                            </tbody>
                                                            <tfoot>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <button type="button" id="nuevoServicio" class="accionModal2 btn btn-info ink-reaction btn-raised"
                                                                            title="Buscar Tipo de Documento" data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static" titulo="Buscar Tipo de Documento"
                                                                            url="{$_Parametros.url}modLG/maestros/proveedorCONTROL/serviciosDocumentosMET/documentos/">
                                                                        <i class="md md-add"></i> Insertar Documento
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group" id="tipoServicioError">

                                                        <table class="table table-striped no-margin" id="contenidoTabla2">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 20px;">#</th>
                                                                <th style="width: 20px;">Descripción</th>
                                                                <th style="width: 20px;">Código</th>
                                                                <td style="width: 20px;">Borrar</td>
                                                            </tr>
                                                            </thead>
                                                            <tbody id="tipoServicio">
                                                            {if isset($tipoServicio)}
                                                                {$cont=1}
                                                                {foreach item=serv from=$tipoServicio}
                                                                    <tr class="tipoServicio{$cont}">
                                                                        <td width="10px" style="vertical-align: middle;">{$cont}</td>
                                                                        <td width="10px"><input type="text" class="form-control" readonly value="{$serv.ind_descripcion}" size="2"></td>
                                                                        <td width="10px"><input type="text" class="form-control" readonly value="{$serv.cod_tipo_servicio}" size="2"></td>
                                                                        <td style="vertical-align: middle;">
                                                                            <button class="borrar btn ink-reaction btn-raised btn-xs btn-danger"
                                                                                    id1="{$serv.pk_num_tipo_servico}"
                                                                                    id2="{$formDB.pk_num_proveedor}"
                                                                                    url="{$_Parametros.url}modLG/maestros/proveedorCONTROL/eliminarServicioMET/"
                                                                                    id="tipoServicio{$cont}">
                                                                                <i class="md md-delete" style="color: #ffffff;"></i></button></td>
                                                                    </tr>
                                                                    {$cont=$cont+1}
                                                                {/foreach}
                                                            {/if}
                                                            </tbody>
                                                            <tfoot>
                                                            <tr>
                                                                <td colspan="3">
                                                                    <button type="button" id="nuevoServicio" class="accionModal2 btn btn-info ink-reaction btn-raised"
                                                                            title="Buscar Tipo de Servicio" data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static" titulo="Buscar Tipo de Servicio"
                                                                            url="{$_Parametros.url}modLG/maestros/proveedorCONTROL/serviciosDocumentosMET/servicios/">
                                                                        <i class="md md-add"></i> Insertar Tipo de Servicio
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            </tfoot>
                                                        </table>

                                                        <label for="tipoServicio">Tipo de Servicio</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">

                                                <div class="col-sm-6">
                                                    <div class="form-group" id="tipoPagoError">
                                                        <select id="tipoPago" name="form[alphaNum][tipoPago]" {if isset($ver) and $ver==1} disabled {/if}
                                                                class="form-control select2 select2-list" required="required">
                                                            {foreach item=tipoPago from=$selecttipoPago}
                                                                {if isset($formDB.cod_tipo_pago) and $formDB.cod_tipo_pago == $tipoPago.cod_detalle}
                                                                    <option value="{$tipoPago.cod_detalle}" selected>{$tipoPago.ind_nombre_detalle}</option>
                                                                {else}
                                                                    <option value="{$tipoPago.cod_detalle}">{$tipoPago.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="tipoPago">Tipo de Pago</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group" id="formaPagoError">
                                                        <select id="formaPago" name="form[alphaNum][formaPago]" {if isset($ver) and $ver==1} disabled {/if}
                                                                class="form-control select2 select2-list" required="required">
                                                            {foreach item=formaPago from=$selectformaPago}
                                                                {if isset($formDB.cod_forma_pago) and $formDB.cod_forma_pago == $formaPago.cod_detalle}
                                                                    <option value="{$formaPago.cod_detalle}" selected>{$formaPago.ind_nombre_detalle}</option>
                                                                {else}
                                                                    <option value="{$formaPago.cod_detalle}">{$formaPago.ind_nombre_detalle}</option>
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="formaPago">Forma de Pago</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end #tab2 -->
                                    <div class="tab-pane" id="tab3"><br><br>
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="col-sm-6">
                                                    <label class="checkbox checkbox-styled">
                                                        <input type="checkbox" {if isset($formDB.ind_snc) and $formDB.ind_snc == 1} checked {/if}value="1" id="snc" name="form[int][ind_snc]" {if isset($ver) and $ver==1} disabled {/if}>
                                                        <span>Inscripción SNC</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group" id="nroSNCError">
                                                    <input type="text" {if isset($ver) and $ver==1} disabled {/if}
                                                           value="{if isset($formDB.ind_num_inscripcion_snc)}{$formDB.ind_num_inscripcion_snc}{/if}"
                                                           name="form[int][nroSNC]" id="nroSNC"
                                                           class="form-control">
                                                    <label for="nroSNC">Nro. Insc. SNC </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group control-width-normal">
                                                    <div class="input-group date" id="demo-date">
                                                        <div class="input-group-content" id="fSNCError">
                                                            <input readonly type="text" name="form[txt][fSNC]" id="fSNC" {if isset($ver) and $ver==1} disabled {/if}
                                                                   value="{if isset($formDB.fec_emision_snc)}{$formDB.fec_emision_snc}{/if}"
                                                                   class="form-control">
                                                            <label>Fecha de Emisión </label>
                                                        </div>
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group control-width-normal">
                                                    <div class="input-group date" id="demo-date2">
                                                        <div class="input-group-content" id="fValSNCError">
                                                            <input type="text" readonly name="form[txt][fValSNC]"
                                                                   id="fValSNC" {if isset($ver) and $ver==1} disabled {/if}
                                                                   value="{if isset($formDB.fec_validacion_snc)}{$formDB.fec_validacion_snc}{/if}"
                                                                   class="form-control">
                                                            <label for="fValSNC">Fecha de Validación </label>
                                                        </div>
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-12">
                                                <div class="form-group" id="condicionError">
                                                    <select id="condicion" name="form[txt][condicion]" {if isset($ver) and $ver==1} disabled {/if}
                                                            class="form-control select2 select2-list">
                                                        {foreach item=condicion from=$selectCondSNC}
                                                            {if isset($formDB.ind_condicion_rcn) and $formDB.ind_condicion_rcn == $condicion.cod_detalle}
                                                                <option value="{$condicion.cod_detalle}" selected>{$condicion.ind_nombre_detalle}</option>
                                                            {else}
                                                                <option value="{$condicion.cod_detalle}">{$condicion.ind_nombre_detalle}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="condicion">Condición </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group" id="nivelError">
                                                    <input type="text" readonly name="form[txt][nivel]" id="nivel" {if isset($ver) and $ver==1} disabled {/if}
                                                           value="{if isset($formDB.ind_nivel)}{$formDB.ind_nivel}{/if}"
                                                            class="form-control">
                                                    <label for="nivel">Nivel Estimado de Contratación </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group" id="calificacionError">
                                                    <input type="text" readonly name="form[txt][calificacion]"
                                                           id="calificacion" {if isset($ver) and $ver==1} disabled {/if}
                                                           value="{if isset($formDB.ind_calificacion)}{$formDB.ind_calificacion}{else}0{/if}"
                                                            class="form-control">
                                                    <label for="calificacion">Calificación Financiera </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group" id="capacidadError">
                                                    <input type="text" readonly name="form[int][capacidad]"
                                                           id="capacidad" {if isset($ver) and $ver==1} disabled {/if}
                                                           value="{if isset($formDB.num_capacidad_financiera)}{$formDB.num_capacidad_financiera}{/if}"
                                                            class="form-control">
                                                    <label for="capacidad">Capacidad Financiera </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end #tab3 -->
                                    <div class="tab-pane" id="tab4">
                                        <br/><br/>
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group" id="diasPagoError">
                                                    <input type="text" id="diasPago" name="form[int][diasPago]" {if isset($ver) and $ver==1} disabled {/if}
                                                           value="{if isset($formDB.num_dias_pago)}{$formDB.num_dias_pago}{else}1{/if}"
                                                            class="form-control">
                                                    <label for="diasPago">Nro. Dias para pago </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group" id="registroError">
                                                    <input type="text" id="registro" name="form[alphaNum][registro]" {if isset($ver) and $ver==1} disabled {/if}
                                                           value="{if isset($formDB.ind_registro_publico)}{$formDB.ind_registro_publico}{/if}"
                                                            class="form-control">
                                                    <label for="registro">Registro Público </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="col-sm-6">
                                                <div class="form-group" id="licenciaError">
                                                    <input type="text" id="licencia" name="form[alphaNum][licencia]" {if isset($ver) and $ver==1} disabled {/if}
                                                           value="{if isset($formDB.ind_licencia_municipal)}{$formDB.ind_licencia_municipal}{/if}"
                                                            class="form-control">
                                                    <label for="licencia">Licencia Municipal </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group control-width-normal">
                                                    <div class="input-group date" id="demo-date2">
                                                        <div class="input-group-content" id="fecConstError">
                                                            <input type="text" id="fecConst" name="form[txt][fecConst]" {if isset($ver) and $ver==1} disabled {/if}
                                                                   value="{if isset($formDB.fec_constitucion)}{$formDB.fec_constitucion}{/if}"
                                                                    class="form-control">
                                                            <label for="fecConst">Fecha de Const. </label>
                                                        </div>
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group floating-label col-sm-6" id="representanteError">
                                            <div class="form-group floating-label col-sm-12" id="representanteError">
                                                <label for="personaResp"><i class="md md-people"></i> Representante Legal</label>
                                                <input type="hidden" class="form-control" name="form[int][representante]"
                                                       value="{if isset($formDB.repId)}{$formDB.repId}{/if}" id="idPersona1">
                                                <input type="text" class="form-control disabled" id="personaResp1" value="{if isset($formDB.repNom1)}{$formDB.repNom1} {$formDB.repAp1}{/if}" disabled readonly>
                                                <input type="text" class="form-control disabled" id="cedula1" value="{if isset($formDB.repCed)}{$formDB.repCed}{/if}" disabled readonly>
                                                <button {if isset($ver) and $ver==1} disabled {/if}
                                                        class="btn ink-reaction btn-raised btn-xs btn-primary accionModal2"
                                                        type="button" title="Buscar Representante Legal" data-toggle="modal" data-target="#formModal2"
                                                        data-keyboard="false" data-backdrop="static" titulo="Buscar Representante Legal"
                                                        url="{$_Parametros.url}modLG/maestros/proveedorCONTROL/personaMET/persona1/">
                                                    <i class="md md-search" style="color: #ffffff;"></i>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="form-group floating-label col-sm-6" id="contactoError">
                                            <div class="form-group floating-label col-sm-12" id="contactoError">
                                                <label for="contacto"><i class="md md-people"></i>
                                                    Contacto/Vendedor</label>
                                                <input type="hidden" class="form-control" name="form[int][contacto]"
                                                       value="{if isset($formDB.contactoId)}{$formDB.contactoId}{/if}" id="idPersona2">
                                                <input type="text" class="form-control disabled" id="personaResp2" value="{if isset($formDB.contactoNom1)}{$formDB.contactoNom1} {$formDB.contactoAp1}{/if}" disabled readonly>
                                                <input type="text" class="form-control disabled" id="cedula2" value="{if isset($formDB.contactoCed)}{$formDB.contactoCed}{/if}" disabled readonly>
                                                <button {if isset($ver) and $ver==1} disabled {/if}
                                                        class="btn ink-reaction btn-raised btn-xs btn-primary accionModal2"
                                                        type="button" title="Buscar Contacto/Vendedor" data-toggle="modal" data-target="#formModal2"
                                                        data-keyboard="false" data-backdrop="static" titulo="Buscar Contacto/Vendedor"
                                                        url="{$_Parametros.url}modLG/maestros/proveedorCONTROL/personaMET/persona2/">
                                                    <i class="md md-search" style="color: #ffffff;"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end #tab4 -->
                                </div>
                                <!--end .tab-content -->
                                <ul class="pager wizard">
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                </ul>
                            </form>
                        </div>
                        <!--end #rootwizard -->
                    </div>
                    <!--end .card-body -->
                </div>
                <!--end .card -->
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
        <!-- END FORM WIZARD -->
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idProveedor)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "80%");
    $(document).ready(function () {
        var app = new AppFunciones();
        var rh2 = new ModRhFunciones();
        var idPais = '{if isset($formDB.fk_a008_num_pais)}{$formDB.fk_a008_num_pais}{/if}';
        var idEstado = '{if isset($formDB.fk_a009_num_estado)}{$formDB.fk_a009_num_estado}{/if}';
//        var idMunicipio = '{if isset($formDB.fk_a011_num_municipio)}{$formDB.fk_a011_num_municipio}{/if}';
        var idCiudad = '{if isset($formDB.fk_a010_num_ciudad)}{$formDB.fk_a010_num_ciudad}{/if}';
        app.metJsonEstado('{$_Parametros.url}estado/jsonEstado',idPais,idEstado);
//        app.metJsonMunicipio('{$_Parametros.url}municipio/JsonMunicipio',idEstado,idMunicipio);
        app.metJsonCiudad('{$_Parametros.url}ciudad/JsonCiudad',idEstado,idCiudad);
//        rh2.metJsonMunicipioN('{$_Parametros.url}municipio/JsonMunicipio',idEstado,idMunicipio);
//        rh2.metJsonCiudadN('{$_Parametros.url}ciudad/JsonCiudad',idMunicipio,idCiudad);
        $('.select2').select2({ allowClear: true });
        $("#formAjax").submit(function () {
            return false;
        });

        $("#formAjax").validate();

        $('.accionModal2').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0, tr1: $("#contenidoTabla1 > tbody > tr").length+1, tr2: $("#contenidoTabla2 > tbody > tr").length+1}, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#fValSNC').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#fSNC').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#fecConst').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('#accion').click(function () {
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                var arrayCheck = ["estado_persona"];
                var arrayMostrarOrden = ['idProveedor','ind_documento_fiscal','nomProveedor','estado_persona'];
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato, dato['idProveedor'], 'idProveedor', arrayCheck, arrayMostrarOrden, 'El Proveedor fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorPersona'){
                    app.metValidarError(dato,'Disculpa. el proveedor ya está registrado');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato, dato['idProveedor'], 'idProveedor', arrayCheck, arrayMostrarOrden, 'El Proveedor fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
        $('.nom').keyup(function () {
            $('#nombreV').val($('#nombre1').val() + ' ' + $('#nombre2').val() + ' ' + $('#apellido1').val() + ' ' + $('#apellido2').val());
        });

        $('#tipopersona').click(function () {
            if ($('#tipopersona').val() != 'N') {
                $("#nombre2").attr('readonly', 'readonly');
                $("#apellido1").attr('readonly', 'readonly');
                $("#apellido2").attr('readonly', 'readonly');
            }
            if ($('#tipopersona').val() == 'N') {
                $("#nombre2").removeAttr("readonly");
                $("#apellido1").removeAttr("readonly");
                $("#apellido2").removeAttr("readonly");
            }
        });

        $('#snc').click(function () {
            if ($('#snc').is(':checked')) {
                $("#nroSNC").removeAttr("readonly");
                $("#fSNC").removeAttr("readonly");
                $("#fValSNC").removeAttr("readonly");
                $("#condicion").removeAttr("disabled");
                $("#nivel").removeAttr("readonly");
                $("#calificacion").removeAttr("readonly");
                $("#capacidad").removeAttr("readonly");
            }
            else {
                $("#nroSNC").attr('readonly', 'readonly');
                $("#fSNC").attr('readonly', 'readonly');
                $("#fValSNC").attr('readonly', 'readonly');
                $("#condicion").attr('disabled', 'disabled');
                $("#nivel").attr('readonly', 'readonly');
                $("#calificacion").attr('readonly', 'readonly');
                $("#capacidad").attr('readonly', 'readonly');
            }
        });
        $(":input").inputmask();

        $("#nuevoCampo").click(function() {
            var idtabla= $("#contenidoTabla");
            var tr= $("#contenidoTabla > tbody > tr");

            if($("#nuevoCampo").val()==""){
                var numTr=tr.length - 1;
            }else{
                numTr=parseInt($("#nuevoCampo").val());
            }
            var nuevoTr=numTr+1;
            var numero=nuevoTr+1;
            var nombreOpciones='<div class="form-group floating-label" id="ind_telefono'+nuevoTr+'Error">'+
                    '<input type="text" class="form-control telf"  name="form[alphaNum][ind_telefono]['+numero+']" id="ind_telefono'+nuevoTr+'">'+
                    '<label for="ind_telefono'+nuevoTr+'"><i class="md md-phone-in-talk"></i> Teléfono '+numero+'</label>'+
                    '</div>';
            var checkOpciones='<div class="checkbox checkbox-styled">'+
                    '<label><input type="checkbox" value="2" name="form[int][telEmer]['+numero+']"><span></span> </label>'+
                    '</div>';
            idtabla.append('<tr id="tel'+nuevoTr+'" n="'+nuevoTr+'">'+
                    //'<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                    '<input type="hidden" value="'+numero+'" name="form[int][cant]">' +
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+nombreOpciones+'</div></td>'+
                    '<td  class="text-center" style="vertical-align: middle;"><div class="col-sm-12">'+checkOpciones+'</div></td>'+
                    '<td  class="text-center" style="vertical-align: middle;"><div class="col-sm-12">' +
                    '<button class="btn ink-reaction btn-raised btn-xs btn-danger delete" id="'+nuevoTr+'">' +
                    '<i class="md md-delete" style="color:#ffffff;" ></i></button></div></td>'+
                    '</tr>');
            $("#nuevoCampo").val(nuevoTr);
//            $(".telf").mask("9999-999.99.99");
            $(".telf").attr('data-inputmask',"'mask':'9999-999.99.99'");
            $(":input").inputmask();
        });

        $('#tel').on('click', '.delete' ,function () {
            var campo = $(this).attr('id');
            $('#tel'+campo).remove();
        });

        $('.borrarTelf').on('click',function () {
            var sec = $(this).attr('secuencia');
            $('#'+sec).remove();
            $.post($(this).attr('url'), { id: $(this).attr('id')}, function (dato) {
                $('#ContenidoModal2').html(dato);
            });

        });

        $('#tipoServicio').on('click', '.borrar' ,function () {
            $('.'+$(this).attr('id')).remove();
            $.post($(this).attr('url'), { id: $(this).attr('id'), id1: $(this).attr('id1'), id2: $(this).attr('id2') }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#tipoDoc').on('click', '.borrar' ,function () {
            $('.'+$(this).attr('id')).remove();
            $.post($(this).attr('url'), { id: $(this).attr('id'), id1: $(this).attr('id1'), id2: $(this).attr('id2') }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#tipoServicio').on('click', '.eliminar' ,function () {
            $('.'+$(this).attr('id')).remove();
        });

        $('#tipoDoc').on('click', '.eliminar' ,function () {
            $('.'+$(this).attr('id')).remove();
        });

        $('#docfiscal').on('blur',function () {
            var url = '{$_Parametros.url}modLG/maestros/proveedorCONTROL/obtenetInfoRifSeniatMET';
            $.post(url,  { docfiscal: $(this).val() }, function (dato) {

                var codigo = '';
                var nombre = '';
                for (var i=0; i<dato.length; i=i+1){
                    if(dato[i]+dato[i+1]=='--' && i>2){
                        i = dato.length;
                    } else {

                        if(i==0){
                            codigo = dato[i];
                        } else if(i>2){
                            nombre += dato[i];
                        }
                    }
                }
                if(codigo==1){
                    swal('Error!','Rif Inexistente o Inválido','error');
                } else if(codigo==2){
                    swal('Error!','No Existe Soporte Curl','error');
                } else if(codigo==3){
                    swal('Error!','Sin Conexión a Internet','error');
                } else if(codigo==4){
                    swal('Exito!','Consulta Satisfactoria del Rif','success');
                    $('#nombre1').val(nombre);
                    $('#nombreV').val(nombre);
                } else {
                    swal('Error!','Error de Conexión con el SENIAT','error');
                }
            });
        });
    });
</script>