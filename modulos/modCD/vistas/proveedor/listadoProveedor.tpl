<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Proveedores - Listado</h2>
    </div>

        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="6">Check</th>
                            <th width="100">Cedula</th>
                            <th width="80">Proveedor </th>
                            <th width="50">Representante Legal</th>
						</tr>
                        </thead>
                        <tbody>
                        {foreach item=proveedor from=$listado}
                            <tr id="idProveedor{$proveedor.pk_num_proveedor}">
                                <td>
                                   <div class="checkbox checkbox-styled">
                                        <label>
                       <input type="checkbox" class="valores" idProveedor="{$proveedor.pk_num_proveedor}" 
					   representanteprov="{$proveedor.nomRepresentante}"
					   cargoprov="{$proveedor.nomProveedor}" cedula="{$proveedor.ind_documento_fiscal}">
					   
												   
                                        </label>
                                    </div>
                                </td>
                                <td><label>{$proveedor.ind_documento_fiscal}</label></td>
                                <td><label>{$proveedor.nomProveedor}</label></td>
                                <td><label>{$proveedor.nomRepresentante}</label></td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>
<div class="modal-footer">
      <button type="button"   title="Cancelar"  class="btn btn-default ink-reaction btn-raised" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
  	<button type="button"  title="Agregar"  class="btn btn-primary ink-reaction btn-raised" id="agregarProveedorSeleccionado"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Agregar</button>
    </button>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#agregarProveedorSeleccionado').click(function () {
            var input = $('.valores');
            for (i = 0; i < input.length; i++) {
                if (input[i].checked == true) {
                    $(document.getElementsByClassName('idProveedor' + input[i].getAttribute('idProveedor'))).remove();
                    $(document.getElementById('Proveedor')).append(
                            '<tr class="idProveedor' + input[i].getAttribute('idProveedor') + '">' +
                            '<input type="hidden" value="' + input[i].getAttribute('idProveedor') + '" name="form[alphaNum][ind_empresa_externa][' + input[i].getAttribute('idProveedor') + ']" class="proveedorInput" proveedor="' + input[i].getAttribute('idProveedor') + '" />' +
							
														 
							'<input type="hidden" value="' + input[i].getAttribute('representanteprov') + '" name="form[alphaNum][ind_representante_externo][' + input[i].getAttribute('idProveedor') + ']" class="represproveInput" representanteprov="' + input[i].getAttribute('representanteprov') + '" />' +
							
															
							'<td>' + input[i].getAttribute('cedula') + '</td>' +
							'<td>' + input[i].getAttribute('cargoprov') + '</td>' +
                            '<td>' + input[i].getAttribute('representanteprov') + '</td>' +

                            '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idProveedor' + input[i].getAttribute('idProveedor') + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                            '</tr>'
							
											
                    );
                }
            }
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>