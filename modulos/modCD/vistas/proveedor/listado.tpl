<!--section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Proveedores</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Nro.</th>
                            <th>Rif</th>
                            <th>Nombre</th>
                            <th>Estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="5">
                                {if in_array('LG-01-07-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                            descipcion="el Usuario a creado el proveedor Nro. " data-toggle="modal"
                                            data-target="#formModal" titulo="Registrar Nuevo Proveedor" id="nuevo">
                                        <i class="md md-create"></i> Nuevo Proveedor</button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function () {

        var url = '{$_Parametros.url}modLG/maestros/proveedorCONTROL/crearModificarProveedorMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/maestros/proveedorCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_proveedor" },
                    { "data": "ind_documento_fiscal" },
                    { "data": "nomProveedor" },
                    { "orderable": false,"data": "num_estatus"},
                    { "orderable": false,"data": "acciones"}
                ]
        );

        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idProveedor: 0 }, function ($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on('click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idProveedor: $(this).attr('idPost'), idPersona: $(this).attr('idPersona') }, function ($dato) {
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on('click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idProveedor: $(this).attr('idPost'), idPersona: $(this).attr('idPersona'), ver:1 }, function ($dato) {
                $('#ContenidoModal').html($dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idPersona = $(this).attr('idPersona');
            var idProveedor = $(this).attr('idProveedor');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modLG/maestros/proveedorCONTROL/eliminarProveedorMET';
                $.post(url, { idProveedor: idProveedor, idPersona: idPersona },function(dato){
                    if(dato['status']=='ok'){
                        $('#dataTablaJson').dataTable().api().row().remove().draw(false);
                        swal("Eliminado!", "el Proveedor fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script-->
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Proveedores</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Nro.</th>
                            <th>Rif</th>
                            <th>Nombre</th>
                            <th>Estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=post from=$listado}
                            <tr id="idProveedor{$post.pk_num_proveedor}">
                                <td>{$post.pk_num_proveedor}</td>
                                <td>{$post.ind_documento_fiscal}</td>
                                <td>{$post.nomProveedor}</td>
                                <td>
                                    <i class="{if ($post.num_estatus==1)}md md-check{else}md md-not-interested{/if} "></i>
                                </td>
                                <td>
                                    {if in_array('LG-01-07-01-02-M',$_Parametros.perfil)}
                                    <button accion="modificar" title="Editar"
                                            class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                            descipcion="El Usuario a Modificado el Proveedor Nro. {$post.pk_num_proveedor}"
                                            idPost="{$post.pk_num_proveedor}" titulo="Modificar Proveedor" idPersona="{$post.fk_a003_num_persona_proveedor}"
                                            data-toggle="modal" data-target="#formModal">
                                        <i class="fa fa-edit" style="color: #ffffff;"></i></button>
                                    {/if}
                                    {if in_array('LG-01-07-01-03-V',$_Parametros.perfil)}
                                    <button accion="ver" title="Consultar"
                                            class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                                            descipcion="El Usuario a Visto el Proveedor Nro. {$post.pk_num_proveedor}"
                                            idPost="{$post.pk_num_proveedor}" titulo="Visualizar Proveedor" idPersona="{$post.fk_a003_num_persona_proveedor}"
                                            data-toggle="modal" data-target="#formModal">
                                        <i class="fa fa-eye" style="color: #ffffff;"></i></button>
                                    {/if}
                                    {if in_array('LG-01-07-01-04-E',$_Parametros.perfil)}
                                    <button accion="eliminar" title="Eliminar"
                                            class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                            descipcion="El Usuario ha Eliminado el Proveedor Nro. {$post.pk_num_proveedor}"
                                            idProveedor="{$post.pk_num_proveedor}" idPersona="{$post.fk_a003_num_persona_proveedor}" titulo="Eliminar Proveedor"
                                            mensaje="Estas seguro que desea eliminar el Proveedor!!" id="eliminar">
                                        <i class="md md-delete"></i>
                                    </button>
                                    {/if}

                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="5">
                                {if in_array('LG-01-07-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                            descipcion="el Usuario a creado el proveedor Nro. " data-toggle="modal"
                                            data-target="#formModal" titulo="Registrar Nuevo Proveedor" id="nuevo">
                                        <i class="md md-create"></i> Nuevo Proveedor</button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function () {

        var $url = '{$_Parametros.url}modLG/maestros/proveedorCONTROL/crearModificarProveedorMET';

        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idProveedor: 0 }, function ($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on('click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idProveedor: $(this).attr('idPost'), idPersona: $(this).attr('idPersona') }, function ($dato) {
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on('click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url, { idProveedor: $(this).attr('idPost'), idPersona: $(this).attr('idPersona'), ver:1 }, function ($dato) {
                $('#ContenidoModal').html($dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idPersona = $(this).attr('idPersona');
            var idProveedor = $(this).attr('idProveedor');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modLG/maestros/proveedorCONTROL/eliminarProveedorMET';
                $.post($url, { idProveedor: idProveedor, idPersona: idPersona },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idProveedor'+dato['id'])).remove();
                        swal("Eliminado!", "el Proveedor fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>