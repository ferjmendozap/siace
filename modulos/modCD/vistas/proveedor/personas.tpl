<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Personas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Nro Documento</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Dependencia</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr>
                                <input type="hidden" class="persona"
                                       nombre="{$i.ind_nombre1} {$i.ind_apellido1}"
                                       documento="{$i.ind_cedula_documento}"
                                       idPersona="{$i.pk_num_persona}">
                                <td>{$i.ind_cedula_documento}</td>
                                <td>{$i.ind_nombre1}</td>
                                <td>{$i.ind_apellido1}</td>
                                <td>{$i.ind_dependencia}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');
            {if $persona == 'persona1'}
            $(document.getElementById('idPersona1')).val(input.attr('idPersona'));
            $(document.getElementById('personaResp1')).val(input.attr('nombre'));
            $(document.getElementById('cedula1')).val(input.attr('documento'));
            {elseif ($persona == 'persona2')}
            $(document.getElementById('idPersona2')).val(input.attr('idPersona'));
            $(document.getElementById('personaResp2')).val(input.attr('nombre'));
            $(document.getElementById('cedula2')).val(input.attr('documento'));
            {elseif ($persona == 'persona3')}
            $(document.getElementById('idPersona3')).val(input.attr('idPersona'));
            $(document.getElementById('personaResp3')).val(input.attr('nombre'));
            $(document.getElementById('cedula3')).val(input.attr('documento'));
            {/if}
            $('#cerrarModal2').click();
        });
    });
</script>