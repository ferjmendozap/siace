<form action="{$_Parametros.url}modCD/enviardocumentintCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
        
			   <div class="row">
			   <div class="col-sm-4">
                     <div class="form-group floating-label" id="fk_cdc003_num_tipo_documentoError">
                       <select  id="fk_cdc003_num_tipo_documento" disabled name="form[alphaNum][fk_cdc003_num_tipo_documento]" class="form-control" >
                            
                            {foreach item=app from=$correspondencia}
                                {if isset($formDB.fk_cdc003_num_tipo_documento)}
                                    {if $app.pk_num_tipo_documento==$formDB.fk_cdc003_num_tipo_documento}
                                        <option selected value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
                    </div>
   				</div>
				
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="fec_documentoError">
                              <input type="text" disabled class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}" name="form[alphaNum][fec_documento]" >
					          <label for="fec_documento"><i class="md md-event"></i>&nbsp;Fecha Documento</label>
                       </div>
                </div>
				
					
					<div class="col-sm-4">
                    <div class="form-group floating-label" id="ind_documento_completoError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_documento_completo)}{$formDB.ind_documento_completo}{/if}" name="form[alphaNum][ind_documento_completo]" id="ind_documento_completo">
                        <label for="ind_documento_completo"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
                </div>
 
 				<div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_asuntoError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}" name="form[alphaNum][ind_asunto]" id="ind_asunto">
                        <label for="ind_asunto"><i class="md md-question-answer"></i>&nbsp;Asunto</label>
                    </div>
				</div>
				
				
			<div class="col-lg-6">
			 <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo" disabled  name="form[alphaNum][fk_a001_num_organismo]" class="form-control" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo Remitente</label>
                    </div>
					</div> 
					
								 <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="docProveedor"><i class="md md-contacts"></i>
                                             Cargo Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_cargo_remitente)}{$formDB.ind_cargo_remitente}{/if}"
                                                   id="docProveedor" disabled readonly>
                                        </div>
                                    </div>
									
									 <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="nombreProveedor"><i class="md md-group"></i>
                                              Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_persona_remitente)}{$formDB.ind_persona_remitente}{/if}"
                                                   id="nombreProveedor" disabled readonly>
                                        </div>
                                    </div>
														
              						 <div class="col-sm-6">
                                        <div class="form-group" id="ind_dependenciaError">
                                            <label for="dependencia"><i class="icm icm-calculate2"></i>
                                                Dependencia Remitente</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_dependencia_remitente)}{$formDB.ind_dependencia}{/if}"
                                                   id="dependencia" disabled readonly>

                                        </div>
                                    </div>
	
								 
				 <div class="col-sm-12">
              <div class="card">
				<div class="card-body style-default-light height-1"  align="center">DOCUMENTO ADJUNTO:&nbsp;"
				{if $formDB.ind_ruta_doc!=''}{$formDB.ind_ruta_doc}{else}No tiene Documento Adjunto{/if}"</div>      <br />                  
              <div class="col-md-12 col-sm-12" align="center">
						<a type="button" class="btn ink-reaction btn-raised btn-primary btn-loading-state" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Descargando..." href="{$ruta.ruta}/{$formDB.ind_ruta_doc}" download="{$formDB.ind_ruta_doc}">Ver o Descargar Archivo</a>
					</div>

					</div>	  
						  </div>
					  </div>
					     </div>
					

			  
                </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
       
</form>

