<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Documentos Internos | Env&iacute;o</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="90">Nro de Documento</th>
								<th  width="190">Remitente</th>
                                <th  width="190">Destinatario</th>
								<th  width="80">Tipo de Documento</th>
                                <th  width="100">Asunto</th>
								<th  width="30">Copia</th>
								<th  width="50">Estado</th>
                                <th width="90">Enviar</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
                                    <td><label>{$documento.num_cod_interno}-{$documento.num_secuencia}-{$documento.fec_annio}</label></td>
                                    <td><label>{$documento.ind_dependencia}</label></td>
                                    <td><label>{$documento.ind_desc_dependencia}</label></td>
									 <td><label>{$documento.ind_descripcion}</label></td>
									<td><label>{$documento.ind_asunto}</label></td>
									<td>          <i class="{if $documento.ind_con_copia==1}md md-check{else}md md-not-interested{/if}"></i></td>
									 <td>
									 <label>{if $documento.ind_estado=='PE'}Pendiente{/if}</label>
									 
									 </td>
                                   
                                    <td align="left">
								  {if $documento.fk_cdc003_num_tipo_documento=='1' && $documento.ind_con_copia!='2'}  
									{if in_array('CD-01-01-02-01-07-I',$_Parametros.perfil)}
                                              <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumCopiaMET/{$documento.pk_num_distribucion}/{$documento.pk_num_documento}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumCopiaMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>
                                        {/if}

									{elseif $documento.fk_cdc003_num_tipo_documento=='1' && $documento.ind_con_copia=='2'}
									 {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
									 <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumMET/{$documento.pk_num_distribucion}/{$documento.pk_num_documento}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirMemorandumMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>
									     {/if} 	 
									
																	
			    				{elseif $documento.fk_cdc003_num_tipo_documento=='5'}
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCredencialMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCredencialMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>

                                        {/if}
									
									{elseif $documento.fk_cdc003_num_tipo_documento=='2'}
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCircularMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirCircularMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>

                                        {/if}	
						
								 {else}  
								{if $documento.fk_cdc003_num_tipo_documento=='3'}  
									       {if in_array('CD-01-01-02-01-09-L',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirPuntoCuentaMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Imprimir" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentintCONTROL/ImprimirPuntoCuentaMET/">
                                                    <i class="md md-print"></i>
                                                </button>
                                            </a>

                                        {/if}
										  {/if} 													 
																		 
  		 						 {/if} 	
								 
										{if $documento.ind_ruta_doc!='' }
										{if in_array('CD-01-01-03-03-02-L',$_Parametros.perfil)}
									<button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_distribucion}" title="Ver documento adjunto"  titulo="<i class='icm icm-cog3'></i>&nbsp;Documentos Internos | Ver Documento Adjunto"><i class="glyphicon glyphicon-search"></i></button>
									 {/if} 	 
									 {else}
									 {if in_array('CD-01-01-03-03-02-L',$_Parametros.perfil)}
									 <button disabled="disabled"class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_distribucion}" title="Ver documento adjunto"  titulo="<i class='icm icm-cog3'></i>&nbsp;Documentos Internos | Ver Documento Adjunto"><i class="glyphicon glyphicon-search"></i></button>
									    {/if} 	 
									   {/if} 	  							  
                                        {if in_array('CD-01-01-03-03-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_distribucion}" title="Enviar Documento"
                                                    descipcion="El Usuario a Modificado un Documento Externo" titulo="<i class='icm icm-cog3'></i>&nbsp;Documentos Internos | Env&iacute;o">
                                                <i class="md md-exit-to-app" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
										
                                    
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                       
                    </table>
                </div>
            </div>
        </div> 
    </div>
</section>

<script type="text/javascript"> 
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/enviardocumentintCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
		$('#datatable1 tbody').on( 'click', '.memorandum', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/impdocumenintCONTROL/memorandumMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
	
		$('#datatable1 tbody').on( 'click', '.documento', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/impdocumenintCONTROL/documentoMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
		
		
			$('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/enviardocumentintCONTROL/VerArchivoMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        })


    });
</script>