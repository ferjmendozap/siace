<form action="{$_Parametros.url}modCD/documentinternosCONTROL/credencialMET"  autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}


 <div id="myPrintArea">

<table id="principal"  align="center"  width="770">
<tr>
<td>
  <!-- *********************** -->
  <table align="center" width="100%">
  <tr><td>
  <!-- CABECERA DEL DOCUMENTO -->
  <table width="100%" align="center" id="cabecera" cellpadding="0" cellspacing="0">
  <tr>
   <td width="3"></td>
   <td width="124" align="center"><img src="{$_Parametros.url}publico/imagenes/modCD/logo.png" style="height:70px; width:80px" ></td>
   <td width="10"></td>
   <td width="420">
   <!-- *********************** -->
   <table align="center" cellpadding="0" cellspacing="0" width="100%">
   <tr>
      <td align="center"><font size="3" face="Arial">REPUBLICA BOLIVARIANA DE VENEZUELA</font></td>
   </tr>
   <tr>
      <td align="center"><font size="3" face="Arial">CONTRALORIA DEL ESTADO BOLIVAR</font></td>
   </tr>
  <tr>
      <td align="center"><font size="3" face="Arial">{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}</font></td>
    </tr>
   </table>
   <!-- *********************** -->
   </td>
   <td width="120" align="center"><img src="{$_Parametros.url}publico/imagenes/modCD/Logo SNCF.jpg" style="height:80px; width:80px" ></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  </table>
  <!-- FIN CABECERA DEL DOCUMENTO -->
  </td></tr>
  </table>
  <!-- *********************** -->
  
</td></tr>
<tr><td>
   <!-- *********************** -->
   <table width="688" height="27" id="numero_doc"> 
   <tr>
     <td height="20"></td>
   </tr>
   <tr>
    <td width="30"></td>
    <td width="250" height="21"><font face="Arial" size="3"><b>Nro: &nbsp; {if isset($formDB.ind_documento_completo)}{$formDB.ind_documento_completo}{/if}</b></font></td>
    <td width="26"></td>
    <td width="26"></td>
    <td width="436"></td>
    <td width="26"></td>
   </tr>
   </table>
   <!-- *********************** -->
</td></tr>

<tr><td>
   <!-- *********************** -->
   <table id="titulo" width="100%">
   <tr>
    <td align="center"><font size="3" face="Arial">CREDENCIAL</b></font></td>
    </tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CUERPO 1 DEL DOCUMENTO -->
  
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CONTENIDO DEL DOCUMENTO -->
  <table width="100%">
  <tr>
   <td height="300"><div align="center" style="width:100%;height:300px; text-align:justify"><font face="Arial" size="2">{if isset($formDB.txt_contenido)}{$formDB.txt_contenido}{/if}</font></div></td>
    <td width="11"></td>
  </tr>
  </table>
  <!-- *********************** -->
<tr><td>
  <!-- *********************** -->
  
  
  <tr><td>
  <!-- *********************** -->
  <table align="center" id="atentamente" width="500">
  <tr>
    <td width="58"></td>
    <td align="center"><font face="Arial" size="3"></font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td height="25"></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center"></td>
    <td width="61"></td>
  </tr>
  <tr>

    <td width="58"></td>
    <td align="center" width="365"><font face="Arial" size="2">{if isset($formDB.nombre_apellidos)}{$formDB.nombre_apellidos}{/if}</font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center" width="600"><font face="Arial" size="2">{if isset($formDB.ind_descripcion_cargo)}{$formDB.ind_descripcion_cargo}{/if}</font></td>
    <td width="61"></td>
  </tr>

  </table>
  <!-- *********************** -->
</td></tr>

  <tr><td>
  <!-- *********************** -->

 
 
</td></tr>

<tr><td>
  <!-- *********************** -->
  <table width="686" id="pie_pagina">
  <tr>
     <td width="26"></td>
     <td width="70"><font face="Arial" size="2">{if isset($formDB.ind_media_firma)}{$formDB.ind_media_firma}{/if}</font></td>
     <td width="421"></td>
     <td width="149" align="right"></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>

   </td></tr>

</table>  
</div>
<div align="center">
<div class="btn-group">
                                <a id="imprime" class="btn btn-icon-toggle btn-refresh">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
		  
                </div>
 
</form>

<script type="text/javascript">
$(document).ready(function() {
        $("#imprime").click(function () {
            $("#myPrintArea").printThis();
        })
    });

   </script>
