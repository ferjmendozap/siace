<form action="{$_Parametros.url}modCD/documentinternosCONTROL/memorandumMET"  autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}

<table id="Padre" name="Padre" align="center" cellpadding="0" cellspacing="0" width="770">
<tr>
<td>

 <div id="myPrintArea">
  
<table align="center" width="700" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="119" align="center"><img src="{$_Parametros.url}publico/imagenes/modCD/logo.png" style="height:70px; width:80px"></td>
    <td width="397">
    <table width="395" height="101" border="1" align="center">
    <tr>
       <td width="402" height="35" align="center"><font size="2" face="Arial, Helvetica, sans-serif"><b>CONTRALORIA DEL ESTADO DELTA AMACURO</b></font></td>
    </tr>
    <tr>
       <td align="center" height="25"><font size="2" face="Arial, Helvetica, sans-serif"><b>PUNTO DE CUENTA</b></font></td>
    </tr>   
    </table>
    </td>
    <td width="176">
    <table width="175" height="100" border="1" align="center" cellpadding="0" cellspacing="0">
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>CODIGO:</b></font></td>
    </tr>
    <tr>
       <td align="center" colspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>FOR-CEDA-001</b></font></td>
    </tr>
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>REVISION</b></font></td>
    </tr>
    <tr>
       <td width="87" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>N�:</b></font></td>
       <td width="82" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA</b></font></td>
    </tr>
     <tr>
       <td height="15" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>0</b></font></td>
       <td height="15" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>05/2010</b></font></td>
    </tr>
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>PAGINA</b></font></td>
    </tr>
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>1 DE 1</b></font></td>
    </tr>
    </table>
    </td>
  </tr>
  </table>
  </td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- TABLA DATOS GENERALES ---------- /////////// --->
<table align="center" width="700" border="1">
<tr>
  <td width="75" rowspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>PRESENTADO:</b></font></td>
  <td width="27"><font size="1" face="Arial, Helvetica, sans-serif"><b>A:</b></font></td>
  <td width="192"><font size="1" face="Arial, Helvetica, sans-serif"><b>{if isset($formDB.cargodepen)}{$formDB.cargodepen}{/if}</b></font></td>
  <td width="378"><font size="1" face="Arial, Helvetica, sans-serif"><b>NUMERO: {if isset($formDB.ind_documento_completo)}{$formDB.ind_documento_completo}{/if}</b></font></td>
</tr>
<tr>

  <td><font size="1" face="Arial, Helvetica, sans-serif"><b>POR:</b></font></td>
  <td><font size="1" face="Arial, Helvetica, sans-serif"><b>{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}</b></font></td>
  <td><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA:  {if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}</b></font></td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- ASUNTO ---------- /////////// --->
<table id="asunto" align="center" width="700" border="1">
<tr>
  <td width="85" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>ASUNTO:</b></font></td>
  <td width="599"><font size="1" face="Arial, Helvetica, sans-serif"><b> {if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}</b></font></td>
</tr>
<tr>
  <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>PROPUESTA:</b></font></td>
</tr>
<tr>
 <td height="300" colspan="2" align="center"><div align="center" style="width:690px;height:300px; text-align:justify"><font face="Arial" size="2">
 {if isset($formDB.txt_contenido)}{$formDB.txt_contenido}{/if}
  <!-- CONTENIDO DEL DOCUMENTO -->
 </font></div>
 <!--<textarea cols="83" rows="17" id="instrucciones"><?=$fa['Contenido']?></textarea>--></td>
</tr>
</table>
<br />
<!-- ////////// ---------- RESULTADO ---------- /////////// --->
<table id="resultado" align="center" width="700" cellpadding="0" cellspacing="0" border="1">
<tr>
   <td colspan="4" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>RESULTADO:</b></font></td>
</tr>
<tr>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>APROBADO</b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>NEGADO</b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>DIFERIDO</b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>VISTO</b></font></td>
</tr>
<tr>
 <td align="center" height="15"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
<tr>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
<tr>
 <td colspan="4" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>INSTRUCCIONES U OBSERVACIONES</b></font></td>
</tr>
<tr>
 <td colspan="4" height="150"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- FIRMAS ---------- /////////// --->
<table align="center" id="firmas" cellpadding="0" cellspacing="0" width="700" border="1">
<tr>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>PREPARADO POR:</b></font></td>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>SELLO</b></font></td>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>CONFORMADO POR:</b></font></td>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>SELLO</b></font></td>
</tr>
<tr>
   <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>FIRMA:</b></font></td>
   <td rowspan="3"></td>
   <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>FIRMA:</b></font></td>
   <td rowspan="3"></td>
</tr>
<tr>
   <td height="25"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
   <td height="10"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
<tr>
   <td height="20"><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA:  {if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}</b></font></td>
   <td><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA:</b></font></td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- ANEXOS ---------- /////////// --->
<table align="center" id="anexos" width="700" border="1">
<tr>

   <td width="44" rowspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>ANEXOS:</b></font></td>
   <td width="35" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>SI</b></font></td>
   <td width="26" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>{if $formDB.ind_anexo=='1'} X {else if $formDB.ind_anexo=='0'}{/if}</b></font></td>
    <td width="567" rowspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>LISTA DE ANEXOS:{if isset($formDB.txt_descripcion_anexo)}{$formDB.txt_descripcion_anexo}{/if}</b></font></td>
</tr>
<tr>
   <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>NO</b></font></td>
   <td rowspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>{if $formDB.ind_anexo=='0'} X {else if $formDB.ind_anexo=='1'}{/if}</b></font></td>
</tr>
</table>

<div align="center">
<div class="btn-group">
                                <a id="imprime" class="btn btn-icon-toggle btn-refresh">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
		  
                </div>
</div>

   </td></tr>

</table>  
</div>
<div align="center">
<div class="btn-group">
                                <a id="imprime" class="btn btn-icon-toggle btn-refresh">
                                    <i class="fa fa-print"></i>
                                </a>
                            </div>
		  
                </div>
 
</form>

<script type="text/javascript">
$(document).ready(function() {
        $("#imprime").click(function () {
            $("#myPrintArea").printThis();
        })
    });

   </script>
