<style type="text/css">
    {literal}
    .table thead>tr>th.vert-align{ /*Permite centrar verticalmente los datos en las filas*/
        font-weight: bold; vertical-align: middle;
    }
    {/literal}
</style>
<div class="section-body">
    <div class="card">
        <div class="card-body" style="margin-bottom: 0;">
            <div class="table-responsive">
                <table id="datatablePf" class="table table-condensed table-hover" width="100%">
                    <thead>
                    <tr>
                        <th class="text-center vert-align">Ente</th>
                        <th class="text-center vert-align">Cargo</th>
                        <th class="text-center vert-align">Situación</th>
                        <th class="text-center vert-align">Estatus</th>
                        <th class="text-center vert-align">Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                        {if $listaOrganizacion|count > 0 }
                            {foreach item=fila from=$listaOrganizacion}
                                <tr>
                                    <td>{$fila.nombre_ente}</td>
                                    <td align="center">{$fila.nombre_cargo}</td>
                                    <td align="center">{$fila.nombre_situacion}</td>
                                    <td align="center"><i class="{if $fila.estatus_Reprstnte==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    <td align="center">
                                        {if in_array('CD-01-03-01-03-02-M',$_Parametros.perfil)}
                                            <button class="modificar btn ink-reaction btn-raised btn-xs btn-primary"
                                                onclick="metMontaOrganizacion({$fila.id_personaente},{$fila.id_ente},{$fila.id_cargopers},{$fila.id_situacionpers},{$fila.estatus_Reprstnte})"
                                                title="Click para modificar" alt="Click para modifica"">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('CD-01-03-01-03-03-E',$_Parametros.perfil)}
                                            <button class="eliminar btn ink-reaction btn-raised btn-xs btn-danger"
                                                onclick="metEliminaAdscripcion({$fila.id_personaente})"
                                                title="Click para eliminar" alt="Click para eliminar"">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {else}
                                            <button id="btn_generar" class="btn btn-raised btn-xs btn-default" title="No tienes permiso para esta opción" alt="No tienes permiso para esta opción">
                                                <i class="md md-delete"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>