<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Entes</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Ente</th>
                                <th>Descripcion</th>
                                <th>Estatus</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=Ente from=$listadoEnte}
                                <tr id="idEnte{$Ente.idEnte}">
								<td><label>{if empty($Ente.idEnte)}{'N/A'}{else}{$Ente.idEnte}{/if}</label></td>
                                    <td><label>{$Ente.ind_nombre_ente}</label></td>
                                    <td>
                                        <i class="{if $Ente.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('CD-01-03-01-01-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idEnte="{$Ente.idEnte}"
                                                    descipcion="El Usuario a Modificado un Ente" titulo="Modificar Ente">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('CD-01-03-01-01-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEnte="{$Ente.idEnte}"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un Ente" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Ente!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4">
                                    {if in_array('CD-01-03-01-01-01-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descipcion="el Usuario a creado un Nuevo Ente"  titulo="<i class='icm icm-cog3'></i> Crear Ente" id="nuevo" >
                                            Nuevo Ente &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                        </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modCD/entesCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idEnte:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idEnte: $(this).attr('idEnte')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idEnte=$(this).attr('idEnte');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCD/entesCONTROL/eliminarMET';
                $.post($url, { idEnte: idEnte },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idEnte'+dato['idEnte'])).html('');
                        swal("Eliminado!", "el Ente fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>