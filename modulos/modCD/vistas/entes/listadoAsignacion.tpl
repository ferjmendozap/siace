<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Entes</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Ente</th>
                            <th>Descripcion</th>
                            <th>Representante Legal</th>
                            <th>Estatus</th>
                            <th>Acción</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=Ente from=$listadoEnte}
                            <tr id="idEnte{$Ente.idEnte}">
                                <td><label>{if empty($Ente.idEnte)}{'N/A'}{else}{$Ente.idEnte}{/if}</label></td>
                                <td><label>{$Ente.ind_nombre_ente}</label></td>
                                <td><label>{if $Ente.num_estatus_titular==0}{'No Asignado'}{else}{$Ente.representante}{/if}</label></td>
                                <td>
                                    <i class="{if $Ente.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                </td>
                                <td align="center">
                                    {if in_array('CD-01-03-01-03-01-M',$_Parametros.perfil)}
                                        <button class="asignar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idEnte="{$Ente.idEnte}"
                                                descipcion="El Usuario a Modificado un Ente" titulo="Modificar Ente">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                    {/if}
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modCD/entesCONTROL/asignarEntesMET';

        $('#datatable1 tbody').on( 'click', '.asignar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idEnte: $(this).attr('idEnte')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

    });
</script>