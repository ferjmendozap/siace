<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form id="formAjax2" action="{$_Parametros.url}modCD/entesCONTROL/asignarEntesMET" class="form form-validate floating-label" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idEnte}" name="idEnte" id="idEnte"/>
                            {*<input type="hidden" value="{$idResponsable}" name="idResponsable" id="idResp"/>*}

                            <div class="tab-content clearfix">
                                <div class="tab-pane active floating-label" id="tab1">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head style-primary-light">
                                                    <header>Asignar Ente</header>
                                                </div>
                                                <div class="card-body">
                                                    <div class="col-lg-12">
                                                        <label for="id_ente" id="id_responsableError" class="control-label" style="margin-top: 10px; margin-bottom: 25px;">
                                                            <a id="lbl_ente" href="#" data-toggle="modal" data-target="#formModal3"
                                                               data-keyboard="false" data-backdrop="static"
                                                               title="Click para buscar el responsable" alt="Click para buscar el responsable">
                                                                <span class="md md-account-circle"></span>Responsable:
                                                            </a>
                                                        </label>
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-6" style="margin-left: 60px">
                                                                <div id="nombre_responsable" style="margin-top: -46px;margin-left: 40px;">
                                                                    {if !empty($formDB.responsable)}
                                                                        <input type="hidden" value="1" name="actualizar" />
                                                                        {$formDB.responsable }<br>CEDULA: {$formDB.ind_cedula_documento}
                                                                    {/if}
                                                                </div>
                                                            </div>
                                                            <button id='limpiarResponsable' class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger {if !isset($formDB.responsable)}hide{/if}" title="Click para limpiar ente padre" style="margin-left: -383px;margin-bottom: 10px; margin-top: -10px">
                                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                                            </button>
                                                        </div>
                                                        <input type="hidden" id="id_responsable" name="form[int][id_responsable]" {if isset($formDB.pk_num_persona)}value="{$formDB.pk_num_persona}" {/if}>

                                                        <div class="col-sm-4">
                                                            <div class="form-group floating-label" id="ind_cargo_representanteError">
                                                                <select id="ind_cargo_representante"  name="form[int][ind_cargo_representante]" class="form-control select2">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=vcargos from=$cargos}
                                                                        {if $vcargos.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscdet_cargo_pers}
                                                                            <option selected value="{$vcargos.pk_num_miscelaneo_detalle}">{$vcargos.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$vcargos.pk_num_miscelaneo_detalle}">{$vcargos.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="ind_cargo_representante"><i class="md md-account-circle"></i>&nbsp;Cargo Representante</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group floating-label" id="fk_a006_num_miscdetalle_situacionError">
                                                                <select id="fk_a006_num_miscdetalle_situacion"  name="form[int][fk_a006_num_miscdetalle_situacion]" class="form-control select2">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=vsituacion from=$situacion}
                                                                        {if $vsituacion.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscdetalle_situacion}
                                                                            <option selected value="{$vsituacion.pk_num_miscelaneo_detalle}">{$vsituacion.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$vsituacion.pk_num_miscelaneo_detalle}">{$vsituacion.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="fk_a006_num_miscdetalle_situacion"><i class="md md-account-circle"></i>&nbsp;Situacion del Cargo</label>
                                                            </div>
                                                        </div>

                                                        {*<div class="checkbox checkbox-styled" style="margin-top: 25px">
                                                            <label>
                                                                <input type="checkbox" {if (isset($formDB.num_estatus) and $formDB.num_estatus==1) OR !isset($idEnte) } checked{/if} value="1" name="form[alphaNum][num_estatus]">
                                                                <span>Estatus Ente</span>
                                                            </label>
                                                        </div>*}

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button" id="cancelar" class="btn btn-default logsUsuarioModal " descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
        Cancelar
    </button>
    <button type="button" class="btn btn-primary logsUsuarioModal" id="guardar2">
        Guardar
    </button>
</div>
<script type="text/javascript">

    function validarError(datos,mensaje){
        for (var item in datos) {
            if(datos[item]=='error'){
                $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                swal("Error!", mensaje, "error");
            }else{
                $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
            }
        }
    }



    $('#lbl_ente').click(function(){
        var url='{$_Parametros.url}modCD/entesCONTROL/CargaGrillaEntes2MET';
        $.post(url,{ lista:true, idEnte: $('#idEnte').val() }, function (dato) {
            if(dato){
                $('#formModalLabel3').html('<i class="md md-search"></i> Listado Entes');
                $('#ContenidoModal3').html(dato);
            }else{
                $('#formModalLabel3').html('');
                $('#ContenidoModal3').html('');
                $('#cerrarModal3').click();
                swal("Información del Sistema", "¡¡Disculpe!! La Planificación no se pudo montar. Intente de nuevo", "error");
            }
        });
    });

    $('#limpiarResponsable').click(function (event) {
        event.preventDefault();
        $('#id_responsable').val(0);
        $('#nombre_responsable').empty();
        $("option:selected").removeAttr("selected");
        $('#limpiarResponsable').addClass('hide');
    });

    $('#guardar2').click(function () {
        $.post($("#formAjax2").attr("action"), $("#formAjax2").serialize(), function (datos) {
            if(datos['status']=='error'){
                validarError(datos,'Disculpa. todos los datos son obligatorios');
            }

            if(datos['resultado']=='exito'){

                swal("Registro Guardado!", "Se ha asignado el responsable satisfactoriamente.", "success");

                datos['num_estatus']=1;
                if(datos['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }

                $(document.getElementById('idEnte'+datos['idEnte'])).html(
                        '<td>' + datos['idEnte'] + '</td>' +
                        '<td>' + datos['nombreEnte'] + '</td>' +
                        '<td>' + datos['responsable'] + '</td>' +
                        '<td><i class="' + icono + '"></i></td>' +
                        '<td  align="center">' +

                        '{if in_array('CD-01-03-01-03-01-M',$_Parametros.perfil)}'+
                        '<button class="asignar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"'+
                        'data-keyboard="false" data-backdrop="static" idEnte="'+datos['idEnte']+'"'+
                        'descipcion="El Usuario a Asignado un Responsable" titulo="Asignar Responsable">'+
                        '<i class="fa fa-edit" style="color: #ffffff;"></i>'+
                        '</button>'+
                        '{/if}'+
                        '&nbsp;&nbsp;'
                );

                $('#cancelar').click();
            }
            if(datos['resultado']=='error'){
                swal("Registro Fallido!", "No se ha podido asignar el usuario", "error");
            }
        }, 'json');

    });

</script>
