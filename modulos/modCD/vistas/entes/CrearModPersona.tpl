<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form id="formAjax2" action="{$_Parametros.url}modCD/entesCONTROL/crearModPersonaMET" class="form form-validate floating-label" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{$idResponsable}" name="idResponsable" id="idResp"/>
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span><span class="title">INFORMACIÓN GENERAL</span></a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span><span class="title">ENTES</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active floating-label" id="tab1">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head style-primary-light">
                                                    <header>Registrar Persona</header>
                                                </div>
                                                <div class="card-body">
                                                    <div class="col-lg-12">

                                                        <!------->
                                                        <div class="col-lg-3">
                                                            <div class="form-group" id="ind_cedula_documentoError">
                                                                <input type="text" maxlength="12" class="form-control" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" name="form[int][ind_cedula_documento]" id="ind_cedula_documento">
                                                                <label for="ind_cedula_documento"><i class="md md-border-color"></i>&nbsp;Numero de Cedula</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="form-group floating-label" id="ind_documento_fiscalError">
                                                                <input type="text" maxlength="12" class="form-control" value="{if isset($formDB.ind_documento_fiscal)}{$formDB.ind_documento_fiscal}{/if}" name="form[alphaNum][ind_documento_fiscal]" id="ind_documento_fiscal">
                                                                <label for="ind_documento_fiscal"><i class="md md-border-color"></i>&nbsp;Documento Fiscal</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="form-group floating-label" id="ind_nombre1Error">
                                                                <input type="text" maxlength="15" class="form-control letras" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1}{/if}" name="form[alphaNum][ind_nombre1]" id="ind_nombre1">
                                                                <label for="ind_nombre1"><i class="md md-border-color"></i>&nbsp;1er Nombre</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="form-group floating-label" id="ind_nombre2Error">
                                                                <input type="text" maxlength="15" class="form-control letras" value="{if isset($formDB.ind_nombre2)}{$formDB.ind_nombre2}{/if}" name="form[alphaNum][ind_nombre2]" id="ind_nombre2">
                                                                <label for="ind_nombre2"><i class="md md-border-color"></i>&nbsp;2do Nombre</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="form-group floating-label" id="ind_apellido1Error">
                                                                <input type="text" maxlength="15" class="form-control letras" value="{if isset($formDB.ind_apellido1)}{$formDB.ind_apellido1}{/if}" name="form[alphaNum][ind_apellido1]" id="ind_apellido1">
                                                                <label for="ind_apellido1"><i class="md md-border-color"></i>&nbsp;1er Apellido</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="form-group floating-label" id="ind_apellido2Error">
                                                                <input type="text" maxlength="15" class="form-control letras" value="{if isset($formDB.ind_apellido2)}{$formDB.ind_apellido2}{/if}" name="form[alphaNum][ind_apellido2]" id="ind_apellido2">
                                                                <label for="ind_apellido2"><i class="md md-border-color"></i>&nbsp;2do Apellido</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <div class="form-group floating-label" id="ind_emailError">
                                                                <input type="text" maxlength="40" class="form-control" value="{if isset($formDB.ind_email)}{$formDB.ind_email}{/if}" name="form[alphaNum][ind_email]" id="ind_email">
                                                                <label for="ind_email"><i class="md md-border-color"></i>&nbsp;Email</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-3">
                                                            <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_nacionalidadError">
                                                                <select id="fk_a006_num_miscelaneo_detalle_nacionalidad"  name="form[int][fk_a006_num_miscelaneo_detalle_nacionalidad]" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=vnacionalidad from=$nacionalidad}
                                                                        {if $vnacionalidad.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_detalle_nacionalidad}
                                                                            <option selected value="{$vnacionalidad.pk_num_miscelaneo_detalle}">{$vnacionalidad.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$vnacionalidad.pk_num_miscelaneo_detalle}">{$vnacionalidad.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="fk_a006_num_miscelaneo_detalle_nacionalidad"><i class="md md-account-circle"></i>&nbsp;Nacionalidad</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-3">
                                                            <div class="form-group floating-label" id="ind_tipo_personaError">
                                                                <select id="ind_tipo_persona"  name="form[alphaNum][ind_tipo_persona]" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    {*{foreach item=vtipo from=$tipo}
                                                                        {if $vtipo.pk_num_miscelaneo_detalle==$formDB.ind_tipo_persona}
                                                                            <option selected value="{$vtipo.pk_num_miscelaneo_detalle}">{$vtipo.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$vtipo.pk_num_miscelaneo_detalle}">{$vtipo.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}*}
                                                                    {if isset($formDB.ind_tipo_persona) and $formDB.ind_tipo_persona == 'J'}
                                                                        <option selected value="J">JURIDICA</option>
                                                                    {else}
                                                                        <option value="J">JURIDICA</option>
                                                                    {/if}
                                                                    {if isset($formDB.ind_tipo_persona) and $formDB.ind_tipo_persona == 'N'}
                                                                        <option selected value="N">NATURAL</option>
                                                                    {else}
                                                                        <option value="N">NATURAL</option>
                                                                    {/if}
                                                                </select>
                                                                <label for="ind_tipo_persona"><i class="md md-account-circle"></i>&nbsp; Tipo</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-3">
                                                            <div class="form-group floating-label" id="fk_a006_num_miscelaneo_det_tipopersonaError">
                                                                <select id="fk_a006_num_miscelaneo_det_tipopersona"  name="form[int][fk_a006_num_miscelaneo_det_tipopersona]" class="form-control">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=vtiponatural from=$tiponatural}
                                                                        {if $vtiponatural.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_det_tipopersona}
                                                                            <option selected value="{$vtiponatural.pk_num_miscelaneo_detalle}">{$vtiponatural.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$vtiponatural.pk_num_miscelaneo_detalle}">{$vtiponatural.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="fk_a006_num_miscelaneo_det_tipopersona"><i class="md md-account-circle"></i>&nbsp; Tipo de Persona</label>
                                                            </div>
                                                        </div>

                                                        <div class="checkbox checkbox-styled col-sm-3">
                                                            <label>
                                                                <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]" checked>
                                                                <span>Estatus</span>
                                                            </label>
                                                        </div>
                                                        <!------->

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head style-primary-light">
                                                    <header>Entes</header>
                                                </div>
                                                <div class="card-body">
                                                    <div class="col-lg-12">
                                                        <table class="table no-margin" id="">
                                                            <div class="row">
                                                                <div class="col-lg-12 contain-lg">
                                                                    <div class="table-responsive">
                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-6" style="margin-left: 170px">
                                                                                <div id="nombre_ente" style="margin-top: 10px;">
                                                                                </div>
                                                                            </div>
                                                                            <button id='limpiarEnte' class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger hide" title="Click para limpiar ente padre" style="margin-left: -420px;margin-bottom: 40px; margin-top: 15px">
                                                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                                                            </button>
                                                                        </div>
                                                                        <input type="hidden" id="id_ente" name="id_ente">
                                                                        <table id="datatable1" class="table table-striped table-hover">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>Categoria</th>
                                                                                <th>Descripcion</th>
                                                                                <th>Estado</th>
                                                                                {*<th>Municipio</th>
                                                                                <th>Parroquia</th>*}
                                                                                <th>Ciudad</th>
                                                                                <th>Inicio</th>
                                                                                <th>Culminación</th>
                                                                                <th>Estatus</th>
                                                                                <th>Situación</th>
                                                                                <th>Cargo</th>
                                                                                <th>Acción</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            {if !empty($entesAsignados)}
                                                                                {foreach item=Ente from=$entesAsignados}
                                                                                    <tr id="idEnte{$Ente.idEnte}">
                                                                                        <td><label>{if empty($Ente.ind_categoria_ente)}{'N/A'}{else}{$Ente.ind_categoria_ente}{/if}</label></td>
                                                                                        <td><label>{$Ente.ind_nombre_ente}</label></td>
                                                                                        <td><label>{if empty($Ente.ind_estado)}{'N/A'}{else}{$Ente.ind_estado}{/if}</label></td>
                                                                                        {*<td><label>{if empty($Ente.ind_municiio)}{'N/A'}{else}{$Ente.ind_municiio}{/if}</label></td>
                                                                                        <td><label>{if empty($Ente.ind_parroquia)}{'N/A'}{else}{$Ente.ind_parroquia}{/if}</label></td>*}
                                                                                        <td><label>{if empty($Ente.ind_ciudad)}{'N/A'}{else}{$Ente.ind_ciudad}{/if}</label></td>
                                                                                        <td><label>{if empty($Ente.fec_registro)}{'N/A'}{else}{$Ente.fec_registro}{/if}</label></td>
                                                                                        <td><label>{if empty($Ente.num_estatus_titular==0)}{'N/A'}{else}{$Ente.fec_ultima_modificacion}{/if}</label></td>
                                                                                        <td><label>{if empty($Ente.situacion)}{'N/A'}{else}{$Ente.situacion}{/if}</label></td>
                                                                                        <td><label>{if empty($Ente.cargo)}{'N/A'}{else}{$Ente.cargo}{/if}</label></td>
                                                                                        <td>
                                                                                            <i class="{if $Ente.num_estatus_titular==1}md md-check{else}md md-not-interested{/if}"></i>
                                                                                        </td>

                                                                                        <td align="center">
                                                                                            {if in_array('CD-01-03-01-02-L',$_Parametros.perfil)}
                                                                                                {if $Ente.num_estatus_titular==1}
                                                                                                    <button class="activar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" data-toggle="modal" data-target="#formModal"
                                                                                                            data-keyboard="false" data-backdrop="static" idRegistro="{$Ente.pk_num_persona_ente}" estatus="{$Ente.num_estatus_titular}"
                                                                                                            descipcion="Inactivar Ente" titulo="Inactivar Ente">
                                                                                                        <i class="md-remove-circle" style="color: #ffffff;"></i>
                                                                                                    </button>
                                                                                                {/if}
                                                                                            {/if}
                                                                                        </td>
                                                                                    </tr>
                                                                                {/foreach}
                                                                            {else}
                                                                                <th style="text-align: center" colspan="10">
                                                                                    <div id="vacio">Aun no se han asignado Entes</div>
                                                                                </th>
                                                                            {/if}
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button" id="cancelar" class="btn btn-default logsUsuarioModal " descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
        Cancelar
    </button>
    <button type="button" class="btn btn-primary logsUsuarioModal" id="guardar2">
        Guardar
    </button>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        //Activar el wizard
        var app = new AppFunciones();
        app.metWizard();
    });
    function validarDocFiscal(campo){
        $(campo).keyup(function () {
            var valor1=this.value.charAt(0);

            if(valor1 == 'G' || valor1 == 'V' || valor1 == 'J' || valor1 == 'E' || valor1== 'g'||valor1 == 'v' || valor1 == 'j' || valor1 == 'e') {

                var letra = this.value;

                if(letra.substring(1) != '') {
                    var numeros = this.value.substring(1).replace(/[^0-9]/ig, '');
                    this.value=valor1+numeros;
                }

            }else{
                if(valor1 != 'G' || valor1 != 'V' || valor1 != 'J' || valor1 != 'E' || valor1 != 'g'||valor1 != 'v' || valor1 != 'j' || valor1 != 'e') {
                    this.value='';
                }

            }

        });
    }

    $(".letras").keypress(function (key) {
        /*window.console.log(key.charCode)*/
        if ((key.charCode < 97 || key.charCode > 122)//letras mayusculas
                && (key.charCode < 65 || key.charCode > 90) //letras minusculas
                && (key.charCode != 45) //retroceso
                && (key.charCode != 241) //ñ
                && (key.charCode != 209) //Ñ
                && (key.charCode != 32) //espacio
                && (key.charCode != 225) //á
                && (key.charCode != 233) //é
                && (key.charCode != 237) //í
                && (key.charCode != 243) //ó
                && (key.charCode != 250) //ú
                && (key.charCode != 193) //Á
                && (key.charCode != 201) //É
                && (key.charCode != 205) //Í
                && (key.charCode != 211) //Ó
                && (key.charCode != 218) //Ú

        )
            return false;
    });

    function mayuscula(campo){
        $(campo).keyup(function() {
            $(this).val($(this).val().toUpperCase());
        });
    }

    $('#ind_cedula_documento').numeric();
    validarDocFiscal($('#ind_documento_fiscal'));
    mayuscula($('#ind_documento_fiscal'));
    mayuscula($('.letras'));

    function validarError(datos,mensaje){
        for (var item in datos) {
            if(datos[item]=='error'){
                $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                swal("Error!", mensaje, "error");
            }else{
                $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
            }
        }
    }
    $(".activar").click(function (event) {
        event.preventDefault();
        var $url='{$_Parametros.url}modCD/entesCONTROL/actOInactRespsableEnteMET';

        $.post($url, { idRegistro: $(this).attr('idRegistro'), estatus: $(this).attr('estatus'), idResp: $('#idResp').val() }, function($data){
            if($data==1){
                swal("Asignacion Exitosa!", "Se ha asignado satisfactoriamente el responsable.", "success");
            };
        }, 'json');

    });

    $('#lbl_ente').click(function(){
        var url='{$_Parametros.url}modCD/entesCONTROL/CargaGrillaEntes2MET';
        $.post(url,{ lista:true, idEnte: $('#idEnte').val() }, function (dato) {
            if(dato){
                $('#formModalLabel3').html('<i class="md md-search"></i> Listado Entes');
                $('#ContenidoModal3').html(dato);
            }else{
                $('#formModalLabel3').html('');
                $('#ContenidoModal3').html('');
                $('#cerrarModal3').click();
                swal("Información del Sistema", "¡¡Disculpe!! La Planificación no se pudo montar. Intente de nuevo", "error");
            }
        });
    });

    $('#limpiarEnte').click(function (event) {
        event.preventDefault();
        $('#id_ente').val(0);
        $('#nombre_ente').empty();
        $('#limpiarEnte').addClass('hide');
    });

    $('#guardar2').click(function () {
        $.post($("#formAjax2").attr("action"), $("#formAjax2").serialize(), function (datos) {
            if(datos['status']=='error'){
                validarError(datos,'Disculpa. los campos marcados con X en rojo son obligatorios');
            }else {
                if (datos['ind_email']=='error'){
                    validarError(datos,'Por favor introduzca un correo valido.');
                }
            }

            if(datos['resultado']=='exito'){
                if(datos['proceso']=='nuevo') {
                    swal("Registro Guardado!", "Se ha registrado satisfactoriamente el registro.", "success");
                }

                else if(datos['proceso']=='modificar') {
                    swal("Registro Guardado!", "Se ha actualizado satisfactoriamente el registro.", "success");
                }

                if(datos['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                if(datos['ind_nombre2']==false){
                    datos['ind_nombre2']='';
                }

                if(datos['ind_apellido2']==false){
                    datos['ind_apellido2']='';
                }

                if(datos['proceso']=='nuevo') {
                    $(document.getElementById('datatable1')).append('<tr  id="idResponsable' + datos['pk_persona'] + '">' +
                            '<td>' + datos['ind_cedula_documento'] + '</td>' +
                            '<td>' + datos['ind_documento_fiscal'] + '</td>' +
                            '<td>' + datos['ind_nombre1'] + ' ' + datos['ind_nombre2'] + '</td>' +
                            '<td>' + datos['ind_apellido1'] + ' ' + datos['ind_apellido2'] + '</td>' +
                            '<td><i class="' + icono + '"></i></td>' +
                            '<td  align="center">' +
                            '{if in_array('CD-01-03-01-02-02-M',$_Parametros.perfil)}' +
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" ' +
                            'data-keyboard="false" data-backdrop="static" idResponsable="' + datos['pk_persona'] + '" ' +
                            'descipcion="El Usuario a Modificado un Responsable" titulo="Modificar Responsable"> ' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i> ' +
                            '</button>' +
                            '{/if}' +
                            '&nbsp;&nbsp;' +
                            '&nbsp;&nbsp;' +
                            '{if in_array('CD-01-03-01-02-03-E',$_Parametros.perfil)}' +
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idResponsable="' + datos['pk_persona'] + '"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Responsable" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Responsable!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i>' +
                            '</button>' +
                            '{/if}' +
                            '</td>'
                    );
                }else if(datos['proceso']=='modificar') {
                    $(document.getElementById('idResponsable'+datos['pk_persona'])).html(
                            '<td>' + datos['ind_cedula_documento'] + '</td>' +
                            '<td>' + datos['ind_documento_fiscal'] + '</td>' +
                            '<td>' + datos['ind_nombre1'] + ' ' + datos['ind_nombre2'] + '</td>' +
                            '<td>' + datos['ind_apellido1'] + ' ' + datos['ind_apellido2'] + '</td>' +
                            '<td><i class="' + icono + '"></i></td>' +
                            '<td  align="center">' +
                            '{if in_array('CD-01-03-01-02-02-M',$_Parametros.perfil)}' +
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" ' +
                            'data-keyboard="false" data-backdrop="static" idResponsable="' + datos['pk_persona'] + '" ' +
                            'descipcion="El Usuario a Modificado un Responsable" titulo="Modificar Responsable"> ' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i> ' +
                            '</button>' +
                            '{/if}' +
                            '&nbsp;&nbsp;' +
                            '&nbsp;&nbsp;' +
                            '{if in_array('CD-01-03-01-02-03-E',$_Parametros.perfil)}' +
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idResponsable="' + datos['pk_persona'] + '"  boton="si, Eliminar"' +
                            'descipcion="El usuario a eliminado un Responsable" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Responsable!!">' +
                            '<i class="md md-delete" style="color: #ffffff;"></i>' +
                            '</button>' +
                            '{/if}' +
                            '</td>'
                    );
                }
                $('#cancelar').click();
            }
            if(datos['resultado']=='error'){
                swal("Registro Fallido!", "No se ha podido registrar la informacion, Cedula o Rif ya existen", "error");
            }
        }, 'json');

    });

</script>
