<div class="row">
    <div class="col-lg-12 contain-lg">
        <div class="table-responsive">
            <table id="datatable2" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Nro. Documento</th>
                    <th>Documento Fiscal</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=i from=$lista}
                    <tr>
                        <input type="hidden" value="{$i.pk_num_persona}" class="persona"
                         nombre="{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}  "
                         documento="{$i.ind_cedula_documento}">
                        <td>{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2} </td>
                        <td>{$i.ind_cedula_documento}</td>
                        <td>{$i.ind_documento_fiscal}</td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2 tbody tr').click(function() {
            var input = $(this).find('input');
            {if $tipoProveedor == 'proveedor'}
                $(document.getElementById('nombreProveedor')).val(input.attr('nombre'));
                $(document.getElementById('fk_a003_num_persona_representante')).val(input.val());
                $('#cerrarModal2').click();
            {/if}
        });
    });
</script>