<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <form id="formAjax" action="{$_Parametros.url}modCD/entesCONTROL/CrearModificarMET" class="form form-validate floating-label" novalidate="novalidate">
                            <input type="hidden" value="1" name="valido"/>
                            <input type="hidden" value="{if isset($formDB.pk_num_ente)} {$formDB.pk_num_ente} {/if}" id="idEnte" name="idEnte"/>
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span><span class="title">INFORMACIÓN GENERAL</span></a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span><span class="title">INFORMACIÓN ADICIONAL</span></a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane active floating-label" id="tab1">
                                    <div class="row">
                                        <div class="col-sm-45">
                                            <div class="card">
                                                <div class="card-head style-primary-light">
                                                    <header>Información General</header>
                                                </div>
                                                <div class="card-body">
                                                    <div class="col-lg-45">

                                                        <!--Entes Externos:  md md-account-balance-->
                                                        <div class="col-sm-12">
                                                            <div class="col-sm-2">
                                                                <label for="id_ente" id="idPersonaEnteError" class="control-label" style="margin-top: 10px; margin-bottom: 25px;">
                                                                    <a id="lbl_ente" href="#" data-toggle="modal" data-target="#formModal3"
                                                                       data-keyboard="false" data-backdrop="static"
                                                                       title="Buscar el ente padre" alt="Click para buscar el ente">
                                                                        <span class="md md-account-balance"></span>Ente Padre:
                                                                    </a>
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-6" style="text-align: center">
                                                                <div id="nombre_ente" style="margin-top: 10px;">
                                                                    {if isset($entes.cadEntes)}{$entes.cadEntes}{/if}
                                                                </div>
                                                            </div>
                                                            <button id='limpiarEnte' class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger{if !isset($entes.cadEntes)} hide{/if}" title="Click para limpiar ente padre" style="margin-left: -510px;margin-bottom: 10px; margin-top: 50px">
                                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                                            </button>
                                                        </div>
                                                        <input type="hidden" id="id_ente" value="{$entes.id_padre}" name="form[int][id_ente]">
                                                        <!---------------------->

                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="tipoEnteError">
                                                                <select id="tipoEnte" name="form[int][tipoEnte]" class="form-control select2">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=tipo from=$tipoEnte}
                                                                        {if $tipo.pk_num_miscelaneo_detalle==$formDB.fk_a037_num_tipo_ente}
                                                                            <option selected value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="tipoEnte"><i class="md md-account-balance"></i>&nbsp; Seleccione el tipo de ente</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-group floating-label" id="tipoCategoriaError">
                                                                <select id="tipoCategoria" name="form[int][tipoCategoria]" class="form-control select2">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=categoria from=$categoriaEnte}
                                                                        {if $categoria.pk_num_categoria_ente == $formDB.fk_a038_num_categoria_ente}
                                                                            <option selected value="{$categoria.pk_num_categoria_ente}">{$categoria.ind_categoria_ente}</option>
                                                                        {else}
                                                                            <option value="{$categoria.pk_num_categoria_ente}">{$categoria.ind_categoria_ente}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="tipoCategoria"><i class="md md-account-balance"></i>&nbsp; Seleccione la categoria</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="form-group floating-label" id="ind_nombre_enteError">
                                                                <input type="text" class="form-control" value="{if isset($formDB.ind_nombre_ente)}{$formDB.ind_nombre_ente}{/if}" name="form[alphaNum][ind_nombre_ente]" id="ind_nombre_ente">
                                                                <label for="ind_nombre_ente"><i class="fa fa-file-text"></i>&nbsp;Descripcion Completa</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-3">
                                                            <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_carcater_socialError">
                                                                <select id="fk_a006_num_miscelaneo_detalle_carcater_social" name="form[int][fk_a006_num_miscelaneo_detalle_carcater_social]" class="form-control select2">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=caracter from=$tipoCaracter}
                                                                        {if $caracter.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscdetalle_caractersocial}
                                                                            <option selected value="{$caracter.pk_num_miscelaneo_detalle}">{$caracter.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$caracter.pk_num_miscelaneo_detalle}">{$caracter.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="fk_a006_num_miscelaneo_detalle_carcater_social"><i class="icm icm-calculate2"></i>&nbsp; Tipo de Caracter</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-3">
                                                            <div class="form-group floating-label" id="ind_numero_registroError">
                                                                <input type="text" maxlength="10" class="form-control" value="{if isset($formDB.ind_numero_registro)}{$formDB.ind_numero_registro}{/if}" name="form[alphaNum][ind_numero_registro]" id="ind_numero_registro">
                                                                <label for="ind_numero_registro"><i class="md md-border-color"></i>&nbsp;Nº de Registro(R.I.F)</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <div class="form-group floating-label" id="ind_tomo_registroError">
                                                                <input type="text" maxlength="10" class="form-control" value="{if isset($formDB.ind_tomo_registro)}{$formDB.ind_tomo_registro}{/if}" name="form[alphaNum][ind_tomo_registro]" id="ind_tomo_registro">
                                                                <label for="ind_tomo_registro"><i class="md md-border-color"></i>&nbsp;N tomo de Registro</label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-3 floating-label" id="finicioError">
                                                            <input type="text"
                                                                   name="form[alphaNum][fecha_fundacion]"
                                                                   maxlength="10"
                                                                   id="finicio"
                                                                   class="form-control date"
                                                                   value="{if isset($formDB.ind_fecha_fundacion)}{$formDB.ind_fecha_fundacion}{elseif isset($formDB.fec_fundacion)}{$formDB.fec_fundacion}{/if}"
                                                                   readonly />
                                                            <label for="finicio">Fecha de Fundación <i class="fa fa-calendar"></i></label>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="form-group floating-label" id="ind_direccionError">
                                                                <input type="text" class="form-control" value="{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}" name="form[alphaNum][ind_direccion]" id="ind_direccion">
                                                                <label for="ind_direccion"><i class="md md-store"></i>&nbsp;Direccion</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-4">
                                                            <div class="form-group floating-label" id="ind_telefonoError">
                                                                <input type="text" maxlength="11" class="form-control" value="{if isset($formDB.ind_telefono)}{$formDB.ind_telefono}{/if}" name="form[alphaNum][ind_telefono]" id="ind_telefono">
                                                                <label for="ind_telefono"><i class="md md-border-color"></i>&nbsp;Telefono 1</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="form-group floating-label" id="ind_telefono2Error">
                                                                <input type="text" maxlength="11" class="form-control" value="{if isset($formDB.ind_telefono2)}{$formDB.ind_telefono2}{/if}" name="form[alphaNum][ind_telefono2]" id="ind_telefono2">
                                                                <label for="ind_telefono2"><i class="md md-border-color"></i>&nbsp;Telefono 2</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="form-group floating-label" id="ind_telefono3Error">
                                                                <input type="text" maxlength="11" class="form-control" value="{if isset($formDB.ind_telefono3)}{$formDB.ind_telefono3}{/if}" name="form[alphaNum][ind_telefono3]" id="ind_telefono3">
                                                                <label for="ind_telefono3"><i class="md md-border-color"></i>&nbsp;Telefono 3</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="form-group floating-label" id="ind_faxError">
                                                                <input type="text" maxlength="11" class="form-control" value="{if isset($formDB.ind_fax)}{$formDB.ind_fax}{/if}" name="form[alphaNum][ind_fax]" id="ind_fax">
                                                                <label for="ind_fax"><i class="md md-border-color"></i>&nbsp;Fax 1</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="form-group floating-label" id="ind_fax2Error">
                                                                <input type="text" maxlength="11" class="form-control" value="{if isset($formDB.ind_fax2)}{$formDB.ind_fax2}{/if}" name="form[alphaNum][ind_fax2]" id="ind_fax2">
                                                                <label for="ind_fax2"><i class="md md-border-color"></i>&nbsp;Fax 2</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <div class="form-group floating-label" id="ind_pagina_webError">
                                                                <input type="text" maxlength="50" class="form-control" value="{if isset($formDB.ind_pagina_web)}{$formDB.ind_pagina_web}{/if}" name="form[alphaNum][ind_pagina_web]" id="ind_pagina_web">
                                                                <label for="ind_pagina_web"><i class="md md-web"></i>&nbsp;Pagina Web</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group floating-label" id="pk_num_paisError">
                                                                <select id="pk_num_pais" class="form-control select2">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=vpais from=$pais}
                                                                        {if $vpais.pk_num_pais==$paisOrigen}
                                                                            <option selected value="{$vpais.pk_num_pais}">{$vpais.ind_pais}</option>
                                                                        {else}
                                                                            <option value="{$vpais.pk_num_pais}">{$vpais.ind_pais}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="pk_num_pais"><i class="md md-map"></i>&nbsp; Seleccione el pais</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group floating-label" id="pk_num_estadoError">
                                                                <select id="pk_num_estado" name="form[int][pk_num_estado]" class="form-control select2">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=vestado from=$estado}
                                                                        {if $vestado.pk_num_estado==$formDB.pk_num_estado}
                                                                            <option selected value="{$vestado.pk_num_estado}">{$vestado.ind_estado}</option>
                                                                        {else}
                                                                            <option value="{$vestado.pk_num_estado}">{$vestado.ind_estado}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="pk_num_estado"><i class="md md-map"></i>&nbsp; Seleccione el estado</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group floating-label" id="pk_num_municipioError">
                                                                <select id="pk_num_municipio" name="form[int][pk_num_municipio]" class="form-control select2">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=vmunicipio from=$municipio}
                                                                        {if $vmunicipio.pk_num_municipio==$formDB.pk_num_municipio}
                                                                            <option selected value="{$vmunicipio.pk_num_municipio}">{$vmunicipio.ind_municipio}</option>
                                                                        {else}
                                                                            <option value="{$vmunicipio.pk_num_municipio}">{$vmunicipio.ind_municipio}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="pk_num_municipio"><i class="md md-map"></i>&nbsp; Seleccione el municipio</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group floating-label" id="pk_num_parroquiaError">
                                                                <select id="fk_num_parroquia" name="form[int][fk_num_parroquia]" class="form-control select2">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=vparroquia from=$parroquia}
                                                                        {if $vparroquia.pk_num_parroquia==$formDB.fk_a012_num_parroquia}
                                                                            <option selected value="{$vparroquia.pk_num_parroquia}">{$vparroquia.ind_parroquia}</option>
                                                                        {else}
                                                                            <option value="{$vparroquia.pk_num_parroquia}">{$vparroquia.ind_parroquia}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="fk_num_parroquia"><i class="md md-map"></i>&nbsp; Seleccione la parroquia</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <div class="form-group floating-label" id="fk_a010_num_ciudadError">
                                                                <select id="fk_a010_num_ciudad" name="form[int][fk_a010_num_ciudad]" class="form-control select2">
                                                                    <option value="">&nbsp;</option>
                                                                    {foreach item=app from=$ciudad}
                                                                        {if $app.pk_num_ciudad==$formDB.fk_a010_num_ciudad}
                                                                            <option selected value="{$app.pk_num_ciudad}">{$app.ind_ciudad}</option>
                                                                        {else}
                                                                            <option value="{$app.pk_num_ciudad}">{$app.ind_ciudad}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="fk_a010_num_ciudad"><i class="md md-map"></i>&nbsp; Seleccione la Ciudad</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="col-sm-4">
                                                                <div class="form-group floating-label">
                                                                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                                                                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-4">
                                                                <div class="form-group floating-label">
                                                                    <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                                                                    <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                                                </div>
                                                            </div>

                                                            <div class="checkbox checkbox-styled">
                                                                <label>
                                                                    <input type="checkbox" {if (isset($formDB.num_estatus) and $formDB.num_estatus==1) OR !isset($idEnte) } checked{/if} value="1" name="form[alphaNum][num_estatus]">
                                                                    <span>Estatus Ente</span>
                                                                </label>
                                                            </div>
                                                            <div class="checkbox checkbox-styled">
                                                                <label>
                                                                    <input type="checkbox" {if isset($formDB.num_sujeto_control) and $formDB.num_sujeto_control==1} checked{/if} value="1" name="form[alphaNum][ind_sujeto_control]">
                                                                    <span>Sujeto a Control</span>
                                                                </label>
                                                            </div>
                                                        </div>

                                                        <!------->

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card">
                                                <div class="card-head style-primary-light">
                                                    <header>Información Adicional</header>
                                                </div>
                                                <div class="card-body">
                                                    <div class="col-lg-12">
                                                        <table class="table no-margin" id="">
                                                            <!----->
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="ind_gacetaError">
                                                                    <input type="text" maxlength="10" class="form-control" value="{if isset($formDB.ind_gaceta)}{$formDB.ind_gaceta}{/if}" name="form[alphaNum][ind_gaceta]" id="ind_gaceta">
                                                                    <label for="ind_gaceta"><i class="fa fa-file-text"></i>&nbsp;Nro Gaceta</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="ind_resolucionError">
                                                                    <input type="text" maxlength="10" class="form-control" value="{if isset($formDB.ind_resolucion)}{$formDB.ind_resolucion}{/if}" name="form[alphaNum][ind_resolucion]" id="ind_resolucion">
                                                                    <label for="ind_resolucion"><i class="fa fa-file-text"></i>&nbsp;Resolución</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="ind_misionError">
                                                                    <textarea rows="5" cols="40" class="form-control" value="{if isset($formDB.ind_mision)}{$formDB.ind_mision}{/if}" name="form[alphaNum][ind_mision]" id="ind_mision" >{if isset($formDB.ind_mision)}{$formDB.ind_mision}{/if}</textarea>
                                                                    <label for="ind_mision"><i class="fa fa-file-text"></i>&nbsp;Misión</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="ind_visionError">
                                                                    <textarea class="form-control" value="{if isset($formDB.ind_vision)}{$formDB.ind_vision}{/if}" name="form[alphaNum][ind_vision]" id="ind_vision" rows="5" cols="40" >{if isset($formDB.ind_vision)}{$formDB.ind_vision}{/if}</textarea>
                                                                    <label for="ind_vision"><i class="fa fa-file-text"></i>&nbsp;Visión</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group floating-label" id="ind_otrosError">
                                                                    <textarea class="form-control" value="{if isset($formDB.ind_otros)}{$formDB.ind_otros}{/if}" name="form[alphaNum][ind_otros]" id="ind_otros" rows="5" cols="40" >{if isset($formDB.ind_otros)}{$formDB.ind_otros}{/if}</textarea>
                                                                    <label for="ind_otros"><i class="fa fa-file-text"></i>&nbsp;Otros</label>
                                                                </div>
                                                            </div>
                                                            <!---->
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button" class="btn btn-default logsUsuarioModal " descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
        <span class="glyphicon glyphicon-floppy-remove"></span>Cancelar
    </button>
    <button type="button" class="btn btn-primary logsUsuarioModal" id="guardar">
        <span class="glyphicon glyphicon-floppy-saved"></span>Guardar
    </button>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#ind_telefono').numeric();
        $('#ind_telefono2').numeric();
        $('#ind_telefono3').numeric();
        $('#ind_fax').numeric();
        $('#ind_fax2').numeric();
        $('#ind_tomo_registro').numeric();
        $('#ind_gaceta').numeric();
        $('ind_resolucion').numeric();




        //Activar el wizard
        var app = new AppFunciones();
        app.metWizard();

        function validarError(datos,mensaje){
            for (var item in datos) {
                if(datos[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }

        //Complementos
        $('.select2').select2({ allowClear: true });
        $('input.date').datepicker({ autoclose: true, todayHighlight: true, format: "dd-mm-yyyy", language: 'es'});


        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#limpiarEnte').click(function (event) {
            event.preventDefault();
            $('#id_ente').val(0);
            $('#nombre_ente').empty();
            $('#limpiarEnte').addClass('hide');
        });

        //Guardado y modales
        $('#guardar').click(function () {

            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (datos) {
                if(datos['status']=='error'){
                    validarError(datos,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }if(datos['tel_digitos']=='error'){
                    validarError(datos,'Disculpa. el numero ingresado no puede ser menor de 11 digitos');
                }if(datos['fax_digitos']=='error'){
                    validarError(datos,'Disculpa. el numero ingresado no puede ser menor de 11 digitos');
                }if(datos['tel_repeat']=='error'){
                    validarError(datos,'Disculpa. el numero ingresado no puede ser identico a los anteriores');
                }if(datos['fax_repeat']=='error'){
                    validarError(datos,'Disculpa. el numero de fax ingresado no puede ser identico a los anteriores');
                }if(datos['urlWeb']=='error'){
                    validarError(datos,'Disculpa. la URL introducida en Pagina Web no es valida');
                }if(datos['urlLogo']=='error'){
                    validarError(datos,'Disculpa. la URL introducida en Logo no es valida');
                }if(datos['urlWebyLogo']=='error'){
                    validarError(datos,'Disculpa. la URL introducida en Pagina Web y Logo no es valida');
                }if(datos['status']=='errorSQL'){
                    validarError(datos,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(datos['status']=='Duplicado'){
                    swal("Error!", datos['mensaje'] , "error");
                }else if(datos['status']=='modificar'){
                    if(datos['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('idEnte'+datos['idEnte'])).html('<td>'+datos['idEnte']+'</td>' +
                            '<td>'+datos['ind_nombre_ente']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-03-01-01-02-M',$_Parametros.perfil)}'+
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idEnte="'+datos['idEnte']+'"' +
                            'descipcion="El Usuario a Modificado una Organismo" titulo="Modificar Organismo">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
                            '&nbsp;&nbsp;'+
                            '{if in_array('CD-01-03-01-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEnte="'+datos['idEnte']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Organismo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Organismo!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Organismo fue modificada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(datos['status']=='nuevo'){
                    if(datos['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idEnte'+datos['idEnte']+'">' +
                            '<td>'+datos['idEnte']+'</td>' +
                            '<td>'+datos['ind_nombre_ente']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +
                            '<td  align="center">' +
                            '{if in_array('CD-01-03-01-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idEnte="'+datos['idEnte']+'"' +
                            'descipcion="El Usuario a Modificado una Organismo" titulo="Modificar Dependencia">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
                            '&nbsp;&nbsp;'+
                            '{if in_array('CD-01-03-01-01-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idEnte="'+datos['idEnte']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Organismo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Organismo!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Guardado!", "El Organismo fue registrado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }

            }, 'json');

        });

    });

    $("#tipoEnte").change(function () {
        var $url='{$_Parametros.url}modCD/entesCONTROL/ListarCategoriaMET';
        $("#tipoEnte option:selected").each(function () {
            ente=$(this).val();

            $.post($url, { ente: ente }, function($data){
                $("#tipoCategoria").html($data);
            });

        });
    })

    $("#pk_num_pais").change(function () {
        var $url='{$_Parametros.url}modCD/entesCONTROL/ListarEstadoMET';
        $("#pk_num_pais option:selected").each(function () {
            pais=$(this).val();

            $.post($url, { pais: pais }, function($data){
                $("#pk_num_estado").html($data);
                /*$("#pk_num_municipio").html("");*/
                $("#fk_a010_num_ciudad").html("");
                $("#fk_num_parroquia").html("");
            });

        });
    })

    $("#pk_num_estado").change(function () {
        estado=$(this).val();
        var $url='{$_Parametros.url}modCD/entesCONTROL/ListarMunicipioMET';
        var $url2='{$_Parametros.url}modCD/entesCONTROL/ListarCiudadMET';
        $("#pk_num_municipio option:selected").each(function () {

            $.post($url, { estado: estado }, function(data){
                $("#pk_num_municipio").html(data);
                /*$("#fk_a010_num_ciudad").html("");*/
                $("#fk_num_parroquia").html("");
            });
        });

        $("#pk_num_municipio option:selected").each(function () {

            $.post($url2, { estado: estado }, function(data){
                /*$("#pk_num_municipio").html(data);*/
                $("#fk_a010_num_ciudad").html(data);
            });
        });
    })

    $("#pk_num_municipio").change(function () {
        var $url='{$_Parametros.url}modCD/entesCONTROL/ListarParroquiaMET';
        $("#pk_num_municipio option:selected").each(function () {
            municipio=$(this).val();

            $.post($url, { municipio: municipio }, function(data){
                $("#fk_num_parroquia").html(data);
            });
        });
    })


    function mayuscula(campo){
        $(campo).keyup(function() {
            $(this).val($(this).val().toUpperCase());
        });
    }

    function minuscula(campo){
        $(campo).keyup(function() {
            $(this).val($(this).val().toLowerCase());
        });
    }

    function validaUrl(campo, tipo){
        $(campo).keyup(function () {



            if (this.value.charAt(0) != 'w' && this.value.charAt(0) != '') {
                this.value = this.value.charAt(0).replace(/[^w]/i, '');
            }

            if (this.value.charAt(1) != 'w' && this.value.charAt(1) != '') {
                var valor = this.value.charAt(0);
                var valor2 = this.value.charAt(1).replace(/[^w]/i, '');
                this.value = valor+valor2;
            }

            if (this.value.charAt(2) != 'w' && this.value.charAt(2) != '') {
                var valor = this.value.charAt(0);
                var valor2 = this.value.charAt(1).replace(/[^w]/i, '');
                var valor3 = this.value.charAt(2).replace(/[^w]/i, '');
                this.value = valor+valor2+valor3;
            }

            var valor = this.value;
            if(valor=='www'){
                this.value = valor+'.';
            }

            if(tipo=='web') {
                this.value = this.value.replace(/[^A-Za-z0-9ñÑáéíóúÁÉÍÓÚ.]/i, '');
            }else if(tipo=='logo'){
                this.value = this.value.replace(/[^A-Za-z0-9ñÑáéíóúÁÉÍÓÚ.\/]/i, '');
            }
        });
    }

    function justLetra(campo){
        $(campo).keyup(function() {
            this.value = this.value.replace(/[^A-Z ]/ig, '');
        });
    }

    /**
     *Visualiza el from modal para seleccionar el ente a objeto de actuación fiscal
     */
    $('#lbl_ente').click(function(){
        var url='{$_Parametros.url}modCD/entesCONTROL/CargaGrillaEntesMET';
        $.post(url,{ lista:true, idEnte: $('#idEnte').val() }, function (dato) {
            if(dato){
                $('#formModalLabel3').html('<i class="md md-search"></i> Listado Entes');
                $('#ContenidoModal3').html(dato);
            }else{
                $('#formModalLabel3').html('');
                $('#ContenidoModal3').html('');
                $('#cerrarModal3').click();
                swal("Información del Sistema", "¡¡Disculpe!! La Planificación no se pudo montar. Intente de nuevo", "error");
            }
        });
    });

    function validarDocFiscal(campo){
        $(campo).keyup(function () {
            var valor1=this.value.charAt(0);

            if(valor1 == 'G' || valor1 == 'V' || valor1 == 'J' || valor1 == 'E' || valor1== 'g'||valor1 == 'v' || valor1 == 'j' || valor1 == 'e') {

                var letra = this.value;

                if(letra.substring(1) != '') {
                    var numeros = this.value.substring(1).replace(/[^0-9]/ig, '');
                    this.value=valor1+numeros;
                }

            }else{
                if(valor1 != 'G' || valor1 != 'V' || valor1 != 'J' || valor1 != 'E' || valor1 != 'g'||valor1 != 'v' || valor1 != 'j' || valor1 != 'e') {
                    this.value='';
                }

            }

        });
    }

</script>
