<form action="{$_Parametros.url}modCD/cargosPersExternosCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{if isset($listado.pk_num_cargo_personal_externo) }{$listado.pk_num_cargo_personal_externo}{else}0{/if}" name="form[int][idCargo]"/>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group floating-label" id="ind_cargo_personal_externoError">
                    <input type="text" class="form-control" name="form[alphaNum][ind_cargo_personal_externo]" id="ind_cargo_personal_externo" value="{if isset($listado.ind_cargo_personal_externo)}{$listado.ind_cargo_personal_externo}{/if}">
                    <label for="ind_cargo_personal_externo"><i class="icm icm-cog3"></i>Cargo</label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled" value="{if isset($listado.ind_usuario)}{$listado.ind_usuario}{/if}" id="ind_usuario">
                    <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label">
                    <input type="text" disabled class="form-control disabled" value="{if isset($listado.fecha_modificacion)}{$listado.fecha_modificacion}{/if}" id="fecha_modificacion">
                    <label for="fecha_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="btn_guardar">
            <span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Guardar
        </button>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function() {
        var appf=new AppfFunciones();
        $("#formAjax").submit(function(){ return false; });
        $('#modalAncho').css("width","35%");
        $('#btn_guardar').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(data){
                if(data['status']=='error'){
                    appf.metActivaError(data);
                    swal("¡Atención!", "Debes ingresar la descripción del cargo", "error");
                }else if(data['status']=='errorSQL'){
                    swal("¡Atención!", data.mensaje, "error");
                }else if(data['status']=='modificar' && data['reg_afectado']){
                    swal("Registro Modificado!", data.mensaje, "success");
                    metBuscarCargos();
                    $('#ContenidoModal').html(''); $('#cerrarModal').click();
                }else if(data['status']=='nuevo' && data['idCargo']){
                    metBuscarCargos();
                    swal("Registro Ingresado!", data.mensaje, "success");
                    $('#ContenidoModal').html(''); $('#cerrarModal').click();
                }
            },'json');
        });
    });
</script>