<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">&nbsp;Cargos Personal Externo</h2>
    </div>
    <div class="section-body scontain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div id="resultadoConsulta"></div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var metBuscarCargos="",metCrear="";
    $(document).ready(function() {
        var url='{$_Parametros.url}modCD/cargosPersExternosCONTROL/';
        metCrear=function(){
            $('#formModalLabel').html('<i class="icm icm-cog3"></i> Crear Cargo Externo');
            $.post(url+'crearModificarMET',{ idCargo:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        }
        /**
         * Busca y monta los datos en la grilla.
         */
        metBuscarCargos=function(){
            $.post(url+'ListarCargosMET', '', function (data) {
                if(data){
                    $('#resultadoConsulta').html(data);
                }else{
                    swal("Información del Sistema", "No hay resultados", "error");
                }
            });
        }
        /**
         * Monta los datos en el form modal para modificar.
         * @param idCargo
         */
        metMontaCargo=function(idCargo){
            $('#formModalLabel').html('');$('#ContenidoModal').html('');
            $.post(url+'crearModificarMET', { idCargo:idCargo }, function (data) {
                if(data){
                    $('#formModalLabel').html('<i class="fa fa-edit"></i> Modificación de Cargo');
                    $('#ContenidoModal').html(data);
                }else{
                    $('#formModalLabel').html('');$('#ContenidoModal').html('');$('#cerrarModal').click();
                    swal("Información del Sistema", "Disculpe, no se pudo montar los datos. Intente nuevamente", "error");
                }
            });
        }
        /**
         * Ejecuta el proceso de eliminar un registro
         * @param idCargo
         */
        metEliminaCargo=function(idCargo){
            $.post(url+'EliminarCargoMET', { idCargo: idCargo },function(dato){
                if(dato.reg_afectado){
                    swal("Registro Eliminado", dato.mensaje, "success");
                    metBuscarCargos();
                }else{
                    swal("¡Atención!", dato.mensaje, "error");
                }
            },'json');
        }
        metBuscarCargos();/*Se ejecuta la función*/
    });
</script>