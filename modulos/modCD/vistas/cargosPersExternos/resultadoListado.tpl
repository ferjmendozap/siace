<div class="section-body contain-lg">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th class="text-center">Descripción</th>
                        <th class="text-center">Acción</th>
                    </tr>
                    </thead>
                    <tbody>
                        {if $listado|count > 0}
                            {foreach item=fila from=$listado}
                                <tr id="tr_{$fila.pk_num_cargo_personal_externo}">
                                    <td>{$fila.ind_cargo_personal_externo}</td>
                                    <td align="center">
                                        {if in_array('PF-01-05-02-02-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsCargo btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                onclick="metMontaCargo({$fila.pk_num_cargo_personal_externo})"
                                                title="Click para modificar" alt="Click para modifica"">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('PF-01-05-02-02-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsCargo btn ink-reaction btn-raised btn-xs btn-danger"
                                                onclick="metEliminaCargo({$fila.pk_num_cargo_personal_externo})"
                                                title="Click para eliminar" alt="Click para eliminar"">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        {/if}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="2">
                            <!--Botón para crear un nuevo cargo-->
                            {if in_array('PF-01-05-02-02-01-N',$_Parametros.perfil)}
                                <a id="nuevo" href="#" class="button logsCargo btn ink-reaction btn-raised btn-info" title="Click para crear un nuevo cargo" alt="Click para crear un nuevo cargo" onclick="metCrear()"
                                        data-toggle="modal"
                                        data-target="#formModal"
                                        data-keyboard="false"
                                        data-backdrop="static"
                                        descipcion="el Usuario a creado un nuevo cargo"
                                    </button>
                                    <i class="md md-create"></i>&nbsp;Nuevo Cargo
                                </a>
                            {/if}
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>