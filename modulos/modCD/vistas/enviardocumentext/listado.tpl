<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Salida de Documentos Externos | Env&iacute;o</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="70">Nro de Documento</th>
								<th  width="190">Remitente</th>
								<th  width="200">Destinatario</th>
                                <th  width="100">Asunto</th>
								<th  width="70">Estado</th>
                                <th width="90" align="left">Documento - Enviar</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
                                    <td><label>{$documento.ind_cod_completo}</label></td>
                                    <td><label>{$documento.ind_dependencia}</label></td>
									 <td><label>{if $documento.ind_organismo_externo!=''} {$documento.ind_descripcion_empresa} {else}PARTICULAR{/if}</label></td>
									<td><label>{$documento.ind_asunto}</label></td>
									 <td>
									 <label>{if $documento.ind_estado=='PE'}Pendiente{else if $documento.ind_estado=='DV'}Devuelto{/if}</label>
									 
									 </td>
                                   
                                    <td align="center">
											
											       {if in_array('CD-01-01-02-01-07-I',$_Parametros.perfil)}
                                            <a href="{$_Parametros.url}modCD/enviardocumentextCONTROL/ImprimirOficioMET/{$documento.pk_num_distribucion}" target="_blank">
                                                <button accion="cuadro" title="Ver Documento" class="cuadro logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                        descipcion="Ver Documento Nro. {$documento.pk_num_distribucion}"
                                                        idReq="{$documento.pk_num_distribucion}"
                                                         titulo="Imprimir" id="cuadro"
                                                        url="{$_Parametros.url}modCD/enviardocumentextCONTROL/ImprimirOficioMET/">
                                                      <i class="md md-description" style="color: #ffffff;"></i>
                                                </button>
                                            </a>

                                        {/if}
										&nbsp;&nbsp;
                                        {if in_array('CD-01-01-02-03-01-N',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_distribucion}" title="Enviar Documento"
                                                    descipcion="El Usuario a Modificado un Documento Externo" titulo="<i class='icm icm-cog3'></i>&nbsp;Salida de Documentos Externos | Env&iacute;o">
                                                <i class="md md-exit-to-app" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
										
                                    
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                       
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript"> 
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/enviardocumentextCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		 $('#datatable1 tbody').on( 'click', '.documento', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/enviardocumentextCONTROL/documentoMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
        
       
    });
</script>