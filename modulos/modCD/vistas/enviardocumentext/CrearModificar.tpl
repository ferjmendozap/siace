<form action="{$_Parametros.url}modCD/enviardocumentextCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
        
			   <div class="row">
			 				
			   <div class="col-sm-4">
                     <div class="form-group floating-label" id="fk_cdc003_num_tipo_documentoError">
                       <select id="fk_cdc003_num_tipo_documento" readonly name="" class="form-control">
                         {foreach item=app from=$corresp}
                                {if isset($formDB.fk_cdc003_num_tipo_documento)}
                                    {if $app.pk_num_tipo_documento==$formDB.fk_cdc003_num_tipo_documento}
                                        <option selected value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                           </select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
                    </div>
   				</div>


				
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="fec_documentoError">
                              <input type="text" readonly class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}" name="form[formula][fec_documento]" >
					          <label for="fec_documento"><i class="md md-event"></i>&nbsp;Fecha Documento</label>
                       </div>
                </div>
				
					
					<div class="col-sm-4">
                    <div class="form-group floating-label" id="ind_cod_completoError">
                        <input type="text" readonly class="form-control" value="{if isset($formDB.ind_cod_completo)}{$formDB.ind_cod_completo}{/if}" name="form[formula][ind_cod_completo]" id="ind_cod_completo">
                        <label for="ind_cod_completo"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
                </div>
 
 				<div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_asuntoError">
                        <input type="text" readonly class="form-control" value="{if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}" name="form[formula][ind_asunto]" id="ind_asunto">
                        <label for="ind_asunto"><i class="md md-question-answer"></i>&nbsp;Asunto</label>
                    </div>
				</div>
				   <input  type="hidden"  class="form-control" value="{if isset($formDB.txt_descripcion)}{$formDB.txt_descripcion}{/if}" name="form[alphaNum][txt_descripcion]" id="txt_descripcion">
				   <input  type="hidden"  class="form-control" value="{if isset($formDB.fec_registro)}{$formDB.fec_registro}{/if}" name="form[formula][fec_registro]" id="fec_registro">
				
				<input  type="hidden"  class="form-control" value="{if isset($formDB.pk_num_documento)}{$formDB.pk_num_documento}{/if}" name="form[alphaNum][fk_cdc011_num_documento]" id="pk_num_documento">
				
			<div class="col-lg-6">
			 <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo" readonly  name="" class="form-control" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo Remitente</label>
                    </div>
                <input  type="hidden"  class="form-control" value="{$app.pk_num_organismo}" name="form[alphaNum][fk_a001_num_organismo]" id="fk_a001_num_organismo">

            </div>
					
								 <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="docProveedor"><i class="md md-contacts"></i>
                                             Cargo Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_cargo_remitente)}{$formDB.ind_cargo_remitente}{/if}"
                                                   id="docProveedor" disabled readonly>
                                        </div>
                                    </div>
									
									 <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="nombreProveedor"><i class="md md-group"></i>
                                              Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_persona_remitente)}{$formDB.ind_persona_remitente}{/if}"
                                                   id="nombreProveedor" disabled readonly>
                                        </div>
                                    </div>
									
									   <input  type="hidden"  class="form-control" value="{if isset($formDB.ind_persona_remitente)}{$formDB.ind_persona_remitente}{/if}" name="form[formula][ind_persona_remitente]" id="ind_persona_remitente">
									   
									     <input  type="hidden"  class="form-control" value="{if isset($formDB.ind_cargo_remitente)}{$formDB.ind_cargo_remitente}{/if}" name="form[formula][ind_cargo_remitente]" id="ind_cargo_remitente">
									
														
              						 <div class="col-sm-6">
                                        <div class="form-group" id="ind_dependenciaError">
                                            <label for="dependencia"><i class="icm icm-calculate2"></i>
                                                Dependencia Remitente</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}"
                                                   id="dependencia" disabled readonly>
							
							 <input  type="text"  class="form-control" value="{if isset($formDB.pk_num_dependencia)}{$formDB.pk_num_dependencia}{/if}" name="form[formula][fk_a004_num_dependencia]" id="pk_num_dependencia">
							 
												  
						
							  
                                        </div>
                                    </div>
	
				<div class="col-sm-6">
				    <center>&nbsp;&nbsp;</center>
					<div class="col-sm-6">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
            	</div>
			
			<div class="col-sm-6">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
			  		</div>
               </div>
				 </div>
				 
				 <div class="col-sm-6">
                            <div align="left">&nbsp;&nbsp;<font color="#0066FF"><b>Seleccione el Responsable de Env&iacute;o</b></font></div>
                                      
               <div class="col-sm-11">
                                        <div class="form-group" id="fk_a003_num_personaError">
                                            <label for="nombrePersona"><i class="icm icm-calculate2"></i>
                                                Nombre del Responsable</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.responsable)}{$formDB.responsable}{/if}"
                                                   id="nombrePersona" disabled readonly>

                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <input id="pk_num_persona"  type="hidden"  value="{if isset($formDB.fk_a003_num_persona)}{$formDB.fk_a003_num_persona}{/if}" name="form[formula][fk_a003_num_persona]">
											  {if isset($ver)} {else}
                                          <button type="button"
                                                   class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                  id="responsable"
                                                  data-toggle="modal" data-target="#formModal2"
                                                  data-keyboard="false" data-backdrop="static"
                                                    titulo="Buscar responsable"
                                                   url="{$_Parametros.url}modCD/enviardocumentextCONTROL/personaMET/persona/"
                                                    >
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
											 {/if}
                                        </div>
                                    </div>

					</div>	  
						  </div>
					  </div>
					   
					

			  
                </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function() {

        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
	
	
		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
			//modales..
        $('#agregarParticular').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

		  $('#tipoparticular').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
	
		$('#fec_registro').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
		$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
	    $('#modalAncho').css("width","90%");
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                
                    swal("Registro Enviado!", "El Documento fue Enviado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/enviardocumentextCONTROL');
                }
            },'json');
        });
    });
</script>
