<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipo Persona - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="15">Cedula</th>
                                <th width="25">Primer Nombre</th>
								<th width="25">Segundo Nombre </th>
								<th width="25"> Primer Apellidos</th>
								<th width="40">Segundo Apellidos</th>
                                <th width="15">Estatus</th>
                                <th width="20" align="center">Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=particular from=$listado}
                                <tr id="idParticular{$particular.pk_num_persona}">
                                    <td><label>{$particular.ind_cedula_documento}</label></td>
                                    <td><label>{$particular.ind_nombre1}</label></td>
									<td><label>{$particular.ind_nombre2}</label></td>
									<td><label>{$particular.ind_apellido1}</label></td>
									<td><label>{$particular.ind_apellido2}</label></td>
                                    <td><i class="{if $particular.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>

                                    <td align="center">
                                        {if in_array('CD-01-03-01-04-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idParticular="{$particular.pk_num_persona}" title="Modificar" 
                                                    descipcion="El Usuario a Modificado un Particular de Personas" titulo="Modificar Particular">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
										 &nbsp;&nbsp;
                                        {if in_array('CD-01-03-01-04-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idParticular="{$particular.pk_num_persona}"  title="Eliminar" boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un Particular" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el particular!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="7">
								<div class="form-group   col-lg-12">
                                    {if in_array('CD-01-03-01-04-01-N',$_Parametros.perfil)}
                                        <button title="Nuevo Registro"  class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descipcion="el Usuario a creado un Nuevo Particular de Personas"  titulo="<i class='icm icm-cog3'></i> Crear Particular" id="nuevo" >
                                           <i class="md md-create"></i>&nbsp;Nuevo Particular
                                        </button>
                                    {/if}
									</div>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modCD/particularCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idParticular:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idParticular: $(this).attr('idParticular')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
		
		  $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idParticular=$(this).attr('idParticular');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                //var $url='{$_Parametros.url}modCD/correspondenciaCONTROL/eliminarMET';
                var $url='{$_Parametros.url}modCD/particularCONTROL/eliminarMET';
                $.post($url, { idParticular: idParticular },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idParticular'+dato['idParticular'])).html('');
                        swal("Eliminado!", "el Particular fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>