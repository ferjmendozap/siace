<form action="{$_Parametros.url}modCD/correspondenciaCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idCorrespondencia) }
            <input type="hidden" value="{$idCorrespondencia}" name="idCorrespondencia"/>
        {/if}
        <div class="row">
            <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_descripcionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" name="form[alphaNum][ind_descripcion]" id="ind_descripcion">
                        <label for="ind_descripcion"><i class="md md-description"></i> &nbsp;Descripcion Completa</label>
                    </div>
                </div>
				
                    <div class="col-sm-6">
                  <div class="form-group floating-label" id="ind_procedenciaError">
                        <select id="ind_procedencia" name="form[alphaNum][ind_procedencia]" class="form-control">
                            {if isset($formDB.ind_procedencia) and $formDB.ind_procedencia == E}
                                    <option selected value="E">EXTERNO</option>
                                {else}
                                    <option value="E">EXTERNO</option>
                            {/if}
                            {if isset($formDB.ind_procedencia) and $formDB.ind_procedencia == I}
                                <option selected value="I">INTERNO</option>
                                {else}
                                <option value="I">INTERNO</option>
                            {/if}
                          
                        </select>
                        <label for="ind_procedencia"><i class="md md-cached"></i> &nbsp;Seleccione el Uso</label>
                   </div>
                </div>
				
				 <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_descripcion_cortaError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion_corta)}{$formDB.ind_descripcion_corta}{/if}" name="form[alphaNum][ind_descripcion_corta]" id="ind_descripcion_corta">
                        <label for="ind_descripcion_corta"><i class="md md-dashboard"></i>&nbsp; Abreviatura</label>
                    </div>
                </div>
				
				 <div class="col-sm-6">
                    <div class="checkbox checkbox-styled">
                        <label>
								  <input type="checkbox" {if (isset($formDB.num_estatus) and $formDB.num_estatus==1) OR !isset($idCorrespondencia) } checked{/if} value="1" name="form[alphaNum][num_estatus]">
							
                            <span>Estatus</span>
                        </label>
                    </div>
                </div>
				
            </div>
 
				<div class="col-sm-6">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
            	</div>
			
			<div class="col-sm-6">
                    <div class="form-group floating-label" id="cod_procesoError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
			  </div>
               
				  </div>
				</div>
		
          
        </div>

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button"  title="Cancelar"  class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
        <button type="button"  title="Guardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css("width","65%");
		$('#accion').click(function(){
            swal({
                title: "�Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
		       $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('idCorrespondencia'+dato['idCorrespondencia'])).html('<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['ind_descripcion_corta']+'</td>' +
                            '<td>'+dato['ind_procedencia']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-03-01-03-02-M',$_Parametros.perfil)}'+
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCorrespondencia="'+dato['idCorrespondencia']+'"' +
                            'descipcion="El Usuario a Modificado una correspondencia" titulo="Modificar Proceso">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('CD-01-03-01-03-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCorrespondencia="'+dato['idCorrespondencia']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado una correspondencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la  correspondencia!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El  maestro de correspondencia fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/correspondenciaCONTROL');
                }else if(dato['status']=='nuevo'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    if(dato['num_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idCorrespondencia'+dato['idCorrespondencia']+'">' +
                            '<td>'+dato['ind_descripcion']+'</td>' +
                            '<td>'+dato['ind_descripcion_corta']+'</td>' +
                            '<td>'+dato['ind_procedencia']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-03-01-03-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCorrespondencia="'+dato['idCorrespondencia']+'"' +
                            'descipcion="El Usuario a Modificado un Proceso de Nomina" titulo="Modificar Proceso">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('CD-01-03-01-03-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCorrespondencia="'+dato['idCorrespondencia']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Proceso" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Proceso!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro exitoso!", "El tipo de Correspondencia fue registrado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/correspondenciaCONTROL');
                }
            },'json');
        });
    });
</script>