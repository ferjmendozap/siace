<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipo Organismo - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
							 <th>#</th>
                                <th>Descripcion Completa</th>
								<th>Representante Legal</th>
                                <th>Tipo Organismo</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=proceso from=$listado}
                                <tr id="idOrganismo{$proceso.pk_num_organismo}">
								<td><label>{$proceso.pk_num_organismo}</label></td>
                                    <td><label>{$proceso.ind_descripcion_empresa}</label></td>
									<td><label>{$proceso.nombre_apellidos}</label></td>
                                    <td><label>
									{if $proceso.ind_tipo_organismo=='I'}Interno{else if $proceso.ind_tipo_organismo=='E'}Externo{/if}
										</label></td>
                                    <td>
                                        <i class="{if $proceso.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('CD-04-01-04-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idOrganismo="{$proceso.pk_num_organismo}" title="Modificar"
                                                    descipcion="El Usuario a Modificado un Organismo" titulo="Modificar Organismo">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
										
                                        &nbsp;&nbsp;
                                        {if in_array('CD-04-01-04-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idOrganismo="{$proceso.pk_num_organismo}"  title="Eliminar" boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un Organismo" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Organismo!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="6">
								<div class="form-group   col-lg-12"">
                                    {if in_array('CD-04-01-04-01-N',$_Parametros.perfil)}
                                        <button title="Nuevo Registro"  class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descipcion="el Usuario a creado un Nuevo Organismo"  titulo="<i class='icm icm-cog3'></i> Crear Organismo" id="nuevo" >
                                          <i class="md md-create"></i>&nbsp;Nuevo Organismo&nbsp;
                                        </button>
                                    {/if}
									</div>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modCD/organismoCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idOrganismo:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idOrganismo: $(this).attr('idOrganismo')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idOrganismo=$(this).attr('idOrganismo');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                //var $url='{$_Parametros.url}modCD/correspondenciaCONTROL/eliminarMET';
                var $url='{$_Parametros.url}modCD/organismoCONTROL/eliminarMET';
                $.post($url, { idOrganismo: idOrganismo },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idOrganismo'+dato['idOrganismo'])).html('');
                        swal("Eliminado!", "el organismo fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>