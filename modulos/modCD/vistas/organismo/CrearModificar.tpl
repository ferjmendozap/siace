<form action="{$_Parametros.url}modCD/organismoCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post" enctype="multipart/form-data">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idOrganismo) }
            <input type="hidden" value="{$idOrganismo}" name="idOrganismo"/>
        {/if}
		
		
		   <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab"><font color="#0066FF">Datos Generales</font></a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab"><font color="#0066FF">Inf. Adicional</font></a></li>
             </ul>
			 
			   <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="Datos">
		
		 <div style="size:8" class="text-primary text-center text-sm-2"><center><b>Datos del &Oacute;rganismo</b></center></div>
						 	<hr class="ruler-lg">
							
        <div class="row">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_descripcion_empresaError">
           <input type="text" class="form-control" value="{if isset($formDB.ind_descripcion_empresa)}{$formDB.ind_descripcion_empresa}{/if}" name="form[alphaNum][ind_descripcion_empresa]" id="ind_descripcion_empresa"    >
                        <label for="ind_descripcion_empresa"><i class="fa fa-file-text"></i>&nbsp;Descripcion Completa</label>
						<p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                    </div>
                </div>
				
				
			   
				 <div class="col-sm-4">
                                        <div class="form-group" id="fk_a003_num_persona_representanteError">
                                            <label for="nombreProveedor"><i class="md md-contacts"></i>
                                                Representante Legal</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.nombre_apellidos)}{$formDB.nombre_apellidos}{/if}"
                                                   id="nombreProveedor" disabled readonly>
												
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <input id="pk_num_persona" type="hidden" value="{if isset($formDB.fk_a003_num_persona_representante)}{$formDB.fk_a003_num_persona_representante}{/if}" name="form[alphaNum][fk_a003_num_persona_representante]"> 
                                            <button
                                                    class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                    type="button"
                                                    title="Buscar Persona"
                                                    data-toggle="modal" data-target="#formModal2"
                                                    data-keyboard="false" data-backdrop="static"
                                                    titulo="Buscar Persona"
                                                    url="{$_Parametros.url}modCD/organismoCONTROL/proveedorMET/proveedor/"
                                                    >
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        </div>
                                    </div>
					<div class="col-sm-4"> 
					<div class="form-group floating-label" id="ind_cargo_representanteError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_cargo_representante)}{$formDB.ind_cargo_representante}{/if}" name="form[alphaNum][ind_cargo_representante]" id="ind_cargo_representante"    >
                        <label for="ind_cargo_representante"><i class="md md-account-circle"></i>&nbsp;Cargo Representante</label>
                    </div>
				</div>
               
			   <div class="col-sm-3">
                   <div class="form-group floating-label" id="fk_a006_num_miscelaneo_detalle_carcater_socialError">
                        <select id="fk_a006_num_miscelaneo_detalle_carcater_social" name="form[alphaNum][fk_a006_num_miscelaneo_detalle_carcater_social]" class="form-control">
                          {if isset($formDB.fk_a006_num_miscelaneo_detalle_carcater_social) and $formDB.fk_a006_num_miscelaneo_detalle_carcater_social == 1}
                                <option selected value="1">SOCIAL</option>
                                {else}
                                <option value="1">SOCIAL</option>
                            {/if}
						    {if isset($formDB.fk_a006_num_miscelaneo_detalle_carcater_social) and $formDB.fk_a006_num_miscelaneo_detalle_carcater_social == 2}
                                <option selected value="2">POLITICO</option>
                                {else}
                                <option value="2">POLITICO</option>
                            {/if}
							
							{if isset($formDB.fk_a006_num_miscelaneo_detalle_carcater_social) and $formDB.fk_a006_num_miscelaneo_detalle_carcater_social == 3}
                                <option selected value="3">COMUNAL</option>
                                {else}
                                <option value="3">COMUNAL</option>
                            {/if}
                        </select>
                        <label for="fk_a006_num_miscelaneo_detalle_carcater_social"><i class="icm icm-calculate2"></i>&nbsp; Tipo de Caracter</label>
						<p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                    </div>
				
                </div>
				
				 <div class="col-sm-3">

                    <div class="form-group floating-label" id="ind_tipo_organismoError">
                        <select id="ind_tipo_organismo" name="form[alphaNum][ind_tipo_organismo]" class="form-control">
                           {if isset($formDB.ind_tipo_organismo) and $formDB.ind_tipo_organismo == I}
                                <option  selected value="I">INTERNO</option>
                                {else}
                                <option  value="I">INTERNO</option>
                            {/if}
                            
                        </select>
                        <label for="ind_tipo_organismo"><i class="md md-recent-actors"></i>&nbsp;Tipo Organismo</label>
                    </div>
               </div>
			   		
				<div class="col-sm-3"> 
					<div class="form-group floating-label" id="ind_documento_fiscalError">
                        <input type="text" class="form-control"   data-inputmask="'mask': 'A-99999999-9'"  value="{if isset($formDB.ind_documento_fiscal)}{$formDB.ind_documento_fiscal}{/if}" name="form[alphaNum][ind_documento_fiscal]" id="ind_documento_fiscal" required="required" aria-required="true">
                        <label for="ind_documento_fiscal"><i class="md md-account-circle"></i>&nbsp;Documento Fiscal (RIF)</label>
						<p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                    </div>
				</div>					
 
 <div class="col-sm-2">
	  <div class="checkbox checkbox-styled">
                        <label>
               							  <input type="checkbox" {if (isset($formDB.num_estatus) and $formDB.num_estatus==1) OR !isset($idOrganismo) } checked{/if} value="1" name="form[alphaNum][num_estatus]">
                            <span>Estatus</span>
                        </label>
                    </div>
					   </div>
					   
 	
	<div class="col-sm-4">
                <div class="card">
                             <div class="table-responsive no-margin">
                            <table class="table table-striped no-margin" id="contenidoTabla">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Telefonos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {if isset($telefono)}
                                    {foreach item=opciones from=$telefono}
                                        <tr>
                                            <input type="hidden" value="{$opciones.pk_num_organismo_telefono}" name="form[alphaNum][pk_num_organismo_telefono][{$n}]">
                                            <td class="text-right" style="vertical-align: middle;">
                                                {$numero++}
                                            </td>
                                           <td style="vertical-align: middle;">
                                                <div class="col-sm-8">
                                                    <div class="form-group floating-label" id="ind_telefono{$n}Error">
                                                        <input type="text" class="form-control"  value="{$opciones.ind_telefono}" name="form[alphaNum][ind_telefono][{$n}]" id="ind_telefono{$n}" maxlength="11">
                                                        <label for="ind_telefono{$n}"><i class=" md-settings-phone"></i> Telefono </label> 
                                                    </div>
                                                </div>
                                            </td>
										<td  class="text-center" style="vertical-align: middle;"><button class="btn ink-reaction btn-raised btn-xs btn-danger delete"><i class="md md-delete"></i></button></td>
                                     </tr>
                                    {/foreach}
                                {/if}
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="3">
                                            <div class="col-sm-3" align="center">
                                                <button type="button" id="nuevoCampo" class="btn btn-info ink-reaction btn-raised">
                                                    <i class="md md-add"></i> Insertar Nuevo Telefono
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        
                </div>
            </div>
        </div>
		
			
					
	
				</div>
				</div>
				
						 <!-----------DETALLE DOCUMENTO---------------------------------->		 
    <div role="tabpanel" class="tab-pane" id="Detalle">
	 <div style="size:8" class="text-primary text-center text-sm-2"><center><b>Informaci&oacute;n Adicional</b></center></div>
						 	<hr class="ruler-lg">
				<div class="col-lg-4">
                    <div class="form-group floating-label" id="ind_numero_registroError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_numero_registro)}{$formDB.ind_numero_registro}{/if}" name="form[alphaNum][ind_numero_registro]" id="ind_numero_registro">
                        <label for="ind_numero_registro"><i class="md md-border-color"></i>&nbsp;Nro. Reg. Mercantil</label>
                    </div>
					</div>
            <div class="col-lg-4">
                    <div class="form-group floating-label" id="ind_tomo_registroError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_tomo_registro)}{$formDB.ind_tomo_registro}{/if}" name="form[alphaNum][ind_tomo_registro]" id="ind_tomo_registro">
                        <label for="ind_tomo_registro"><i class="md md-border-color"></i>&nbsp;Tomo de Reg Mercantil</label>
                    </div>
                </div>
				
				<div class="col-sm-4">
                    <div class="form-group floating-label" id="ind_pagina_webError">
                        <input type="text" class="form-control"  value="{if isset($formDB.ind_pagina_web)}{$formDB.ind_pagina_web}{/if}" name="form[alphaNum][ind_pagina_web]" id="ind_pagina_web">
                        <label for="ind_pagina_web"><i class="md md-web"></i>&nbsp;Pagina Web</label>
                    </div>
					</div>
					
					<div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_direccionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}" name="form[formula][ind_direccion]" id="ind_direccion"    >
                        <label for="ind_direccion"><i class="md md-store"></i>&nbsp;Direccion</label>
                    </div>
                </div>
				
				<div class="col-md-3">
                                                     <div class="form-group" id="fk_a008_num_paisError">
                                                        <select id="fk_a008_num_pais"  name="form[alphaNum][fk_a008_num_pais]"  class="form-control input-sm" required data-msg-required="Seleccione Pa�s">
                                                           
                                                            {foreach item=i from=$listadoPais}
                                                                {if isset($formDB.fk_a008_num_pais)}
                                                                    {if $i.pk_num_pais==$formDB.fk_a008_num_pais}
                                                                        <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {/if}
                                                                {else}
                                                                    {if $i.pk_num_pais==$DefaultPais}
                                                                        <option selected value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_pais}">{$i.ind_pais}</option>
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a008_num_pais"><i class="md md-map"></i> Pais</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                  <div class="form-group" id="fk_a009_num_estadoError">
                                                        <select id="fk_a009_num_estado" name="form[alphaNum][fk_a009_num_estado]"   class="form-control input-sm" required data-msg-required="Seleccione Estado">
                                                																 <option selected value="">Seleccione el estado</option>
                                                            {foreach item=i from=$listadoEstado}
                                                                {if isset($formDB.fk_a009_num_estado)}
                                                                    {if $i.pk_num_estado==$formDB.fk_a009_num_estado}
                                                                        <option  value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                                    {/if}
                                                                {else}
                                                                   <option value="{$i.pk_num_estado}">{$i.ind_estado}</option>
                                                               {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a009_num_estado"><i class="md md-map"></i> Estado</label>
                                                        <p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group" id="fk_a011_num_municipioError">
                                                        <select id="fk_a011_num_municipio" name="form[alphaNum][fk_a011_num_municipio]"  class="form-control input-sm" required data-msg-required="Seleccione Municipio">
                                                     	 <option selected value="">Seleccione el municipio</option>
                                                            {foreach item=i from=$listadoMunicipio}
                                                                {if isset($formDB.fk_a011_num_municipio)}
                                                                    {if $i.pk_num_municipio==$formDB.fk_a011_num_municipio}
                                                                        <option selected value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {/if}
                                                                {else}
                                                                   <option value="{$i.pk_num_municipio}">{$i.ind_municipio}</option>
                                                                    {/if}
                                                            {/foreach}
                                                        </select>
                                                        <label for="fk_a011_num_municipio"><i class="md md-map"></i> Municipio</label>
														<p class="help-block"><span class="text-xs" style="color: red">*</span></p>
                                                    </div>
                                                </div>
                                       

                                                <div class="col-md-3">
                                                   <div class="form-group" id="fk_a010_num_ciudadError">
                                                        <select id="fk_a010_num_ciudad" name="form[alphaNum][fk_a010_num_ciudad]"  class="form-control input-sm" required data-msg-required="Seleccione Ciudad">
                                                      	 <option selected value="">Seleccione la ciudad</option>
                                                            {foreach item=i from=$listadoCiudad}
                                                                {if isset($formDB.fk_a010_num_ciudad)}
                                                                    {if $i.pk_num_ciudad==$formDB.fk_a010_num_ciudad}
                                                                        <option  value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {else}
                                                                        <option value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                    {/if}
                                                               		 {else}
                                                                	<option value="{$i.pk_num_ciudad}">{$i.ind_ciudad}</option>
                                                                {/if}
															 {/foreach}
                                                        </select>
                                                        <label for="fk_a010_num_ciudad"><i class="md md-map"></i> Ciudad</label>
														<p class="help-block"><span class="text-xs" style="color: red">*</span></p>

                                                    </div>
                                                </div>
												
				
					
					<div class="col-sm-4">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
            	</div>
			
			<div class="col-sm-4">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
			  		</div>
               </div>
			   
		<div class="col-sm-4">
				<div class="col-sm-6" align="center">
							<div class="form-group"  >
							       	<div id="fotoCargada">
                                    	<img class="img-thumbnail border-gray border-lg img-responsive auto-width" src="{$_Parametros.ruta_Img}modCD/imagen_evento.png" alt="Cargar Imagen" title="Cargar Imagen" id="cargarImagen" style="cursor: pointer; "/>
                                    </div>
                                    <input type="file" name="ind_ruta_img" id="ind_ruta_img" style="display: none" />
									<input type="hidden" name="ind_logo" id="ind_logo"  value="{if isset($formDB.ind_logo)}{$formDB.ind_logo}{/if}">
                            </div>
							

        </div>
		
		<div class="col-sm-6" align="center">
							<div class="form-group"  >
							       	<div id="fotoCargada2">
                                    	<img class="img-thumbnail border-gray border-lg img-responsive auto-width" src="{$_Parametros.ruta_Img}modCD/imagen_evento.png" alt="Cargar Imagen" title="Cargar Imagen" id="cargarImagen2" style="cursor: pointer; "/>
                                    </div>
                                    <input type="file" name="ind_ruta" id="ind_ruta" style="display: none" />
									<input type="hidden" name="ind_logo_secundario" id="ind_logo_secundario"  value="{if isset($formDB.ind_logo_secundario)}{$formDB.ind_logo_secundario}{/if}">
                            </div>
 					</div>
        </div>
		
	
	
	</div>
                

            
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button"  title="Cancelar"  class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
        <button type="button"   title="Guardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
	
			$('#contenidoTabla  tbody').on('keyup change', 'tr td .floating-label input' ,function (e) {
            var input = $(e.currentTarget);
            if ($.trim(input.val()) !== '') {
                input.addClass('dirty').removeClass('static');
            } else {
                input.removeClass('dirty').removeClass('static');
            }
        });
        $('#contenidoTabla').on('click', '.delete', function () {
            $(this).parent('td').parent('tr').remove();
        });
        $("#nuevoCampo").click(function() {
            var idtabla= $("#contenidoTabla");
            var tr= $("#contenidoTabla > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;
            var TelefonoObciones='<div class="form-group floating-label" id="ind_telefono'+nuevoTr+'Error">'+
                                    '<input type="text" class="form-control" value="" name="form[alphaNum][ind_telefono]['+nuevoTr+']" id="ind_telefono'+nuevoTr+'">'+
                                    '<label for="ind_telefono'+nuevoTr+'"><i class="md-settings-phone"></i> Telefono '+numero+'</label>'+
                                '</div>';
           
            idtabla.append('<tr>'+
                                '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                                '<td style="vertical-align: middle;"><div class="col-sm-8">'+TelefonoObciones+'</div></td>'+
                                '<td  class="text-center" style="vertical-align: middle;"><button class="btn ink-reaction btn-raised btn-xs btn-danger delete"><i class="md md-delete"></i></button></td>'+
                            '</tr>');

        });
		
	var ind_logo = $("#ind_logo").val();
                if(ind_logo == "") {
                 //swal("El Documento es Obligatorio", "Por favor redarctar el contenido", "error");
					//return false;
    $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modCD/imagen_evento1.png');
	$("#cargarImagen").css({ 'width':'130px', 'height':'100px' })       
		        }
                else if(ind_logo != "") 
                {
				
$("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}logos/{$formDB.ind_logo}');
$("#cargarImagen").css({ 'width':'130px', 'height':'100px' });
	 }
	 
	 var ind_logo_secundario = $("#ind_logo_secundario").val();
                if(ind_logo_secundario == "") {
                 //swal("El Documento es Obligatorio", "Por favor redarctar el contenido", "error");
					//return false;
    $("#cargarImagen2").attr("src",'{$_Parametros.ruta_Img}modCD/imagen_evento2.png');
	$("#cargarImagen2").css({ 'width':'130px', 'height':'100px' })       
		        }
                else if(ind_logo_secundario != "") 
                {
				
$("#cargarImagen2").attr("src",'{$_Parametros.ruta_Img}logos/{$formDB.ind_logo_secundario}');
$("#cargarImagen2").css({ 'width':'130px', 'height':'100px' });
	 }
	
	//*********************************************
	//*			CARGAR IMAGEN UNO
	//*********************************************
	$("#cargarImagen").click(function() {
       $("#ind_ruta_img").click();//input file
    });
	
	$("#ind_ruta_img").change(function(e) {
        var files = e.target.files;
        var file = $("#ind_ruta_img")[0].files[0]; //Obtenemos el primer imputfile (unico que hay en este caso)
        var fileName = file.name; //obtenemos el nombre del archivo

        var aux_n = fileName.split('.');
        var aux_n2 = 0;
        if ( aux_n.length > 2 || fileName.indexOf(',') > -1 || fileName.indexOf('"') > -1 || fileName.indexOf("'") > -1 || fileName.indexOf('(') > -1 || fileName.indexOf(')') > -1 || fileName.indexOf('}') > -1 )
        {
            aux_n2 = 1;
        }
        if(aux_n2 == 0)
        {
            $("#ind_logo").val(fileName);
            var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);//obtenemos la extensi�n del archivo
            if (fileExtension == 'png' || fileExtension == 'PNG' || fileExtension == 'jpg' || fileExtension == 'JPG' || fileExtension == 'jpeg' || fileExtension == 'JPEG')
            {   // Obtenemos la imagen del campo "file".
                for (var i = 0, f; f = files[i]; i++) {
                    if(!f.type.match('image.*'))
                    {
                        continue;
                    }
                    var reader = new FileReader();
                    reader.onload = (function (theFile) {
                        return function (e) {
                            // Insertamos la imagen en la etiqueta img para dar el
                            // efecto que cargo temporalmente la imagen nueva
                            $("#cargarImagen").attr("src", e.target.result);
                            $("#cargarImagen").css({
                                'width':'130px',
                                'height': '100px'
                            });
                        };
                    })(f);
                    reader.readAsDataURL(f);
               }
            }
            else
            {
                swal("Extension de archivo no valido", "Asegurese de subir un archivo de tipo imagen: jpg/jpeg/png", "warning");
                $("#ind_ruta_img").val('');
                $("#ind_logo").val('');
                $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modCD/imagen_evento1.png');
                $("#cargarImagen").css({ 'width':'130px', 'height':'100px' })
            }
        }
        else
        {
            swal("�Nombre de archivo no valido!", "Renombre el archivo, sin caracteres especiales", "warning");
            $("#ind_ruta_img").val('');
            $("#ind_logo").val('');
            $("#cargarImagen").attr("src",'{$_Parametros.ruta_Img}modCD/imagen_evento1.png');
            $("#cargarImagen").css({ 'width':'130px', 'height':'100px' })
       }
    });

	//*********************************************
	//*			CARGAR IMAGEN DOS
	//*********************************************
	$("#cargarImagen2").click(function() {
       $("#ind_ruta").click();//input file
    });
	
	$("#ind_ruta").change(function(e) {
        var files = e.target.files;
        var file = $("#ind_ruta")[0].files[0]; //Obtenemos el primer imputfile (unico que hay en este caso)
        var fileName = file.name; //obtenemos el nombre del archivo

        var aux_n = fileName.split('.');
        var aux_n2 = 0;
        if ( aux_n.length > 2 || fileName.indexOf(',') > -1 || fileName.indexOf('"') > -1 || fileName.indexOf("'") > -1 || fileName.indexOf('(') > -1 || fileName.indexOf(')') > -1 || fileName.indexOf('}') > -1 )
        {
            aux_n2 = 1;
        }
        if(aux_n2 == 0)
        {
            $("#ind_logo_secundario").val(fileName);
            var fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);//obtenemos la extensi�n del archivo
            if (fileExtension == 'png' || fileExtension == 'PNG' || fileExtension == 'jpg' || fileExtension == 'JPG' || fileExtension == 'jpeg' || fileExtension == 'JPEG')
            {   // Obtenemos la imagen del campo "file".
                for (var i = 0, f; f = files[i]; i++) {
                    if(!f.type.match('image.*'))
                    {
                        continue;
                    }
                    var reader = new FileReader();
                    reader.onload = (function (theFile) {
                        return function (e) {
                            // Insertamos la imagen en la etiqueta img para dar el
                            // efecto que cargo temporalmente la imagen nueva
                            $("#cargarImagen2").attr("src", e.target.result);
                            $("#cargarImagen2").css({
                                'width':'130px',
                                'height': '100px'
                            });
                        };
                    })(f);
                    reader.readAsDataURL(f);
               }
            }
            else
            {
                swal("Extension de archivo no valido", "Asegurese de subir un archivo de tipo imagen: jpg/jpeg/png", "warning");
                $("#ind_ruta").val('');
                $("#ind_logo_secundario").val('');
                $("#cargarImagen2").attr("src",'{$_Parametros.ruta_Img}modCD/imagen_evento2.png');
                $("#cargarImagen2").css({ 'width':'130px', 'height':'100px' })
            }
        }
        else
        {
            swal("�Nombre de archivo no valido!", "Renombre el archivo, sin caracteres especiales", "warning");
            $("#ind_ruta").val('');
            $("#ind_logo_secundario").val('');
            $("#cargarImagen2").attr("src",'{$_Parametros.ruta_Img}modCD/imagen_evento2.png');
            $("#cargarImagen2").css({ 'width':'130px', 'height':'100px' })
       }
    });

		//	var rh  = new  AppFunciones();
 			var cd = new  ModCdFunciones();
  
     //   var idDependencia = '{if isset($formDB.num_org_ext)}{$formDB.num_org_ext}{/if}';
       // var idCentroCosto = '{if isset($formDB.num_depend_ext)}{$formDB.num_depend_ext}{/if}';
		
		 var idPais   = '{if isset($formDB.pk_num_pais)}{$formDB.pk_num_pais}{/if}';
        var idEstado = '{if isset($formDB.pk_num_estado)}{$formDB.pk_num_estado}{/if}';
        var idCiudad = '{if isset($formDB.pk_num_ciudad)}{$formDB.pk_num_ciudad}{/if}';
        var idMunicipio = '{if isset($formDB.pk_num_municipio)}{$formDB.pk_num_municipio}{/if}';
		
		cd.metJsonEstadoD('{$_Parametros.url}estado/jsonEstado',idPais,idEstado);
		//rh.metJsonEstado('{$_Parametros.url}estado/jsonEstado',idPais,idEstado);
        cd.metJsonMunicipioN('{$_Parametros.url}municipio/JsonMunicipio',idEstado,idMunicipio);
        cd.metJsonCiudadN('{$_Parametros.url}ciudad/JsonCiudad',idEstado,idCiudad);
	
	
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
		
		 $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		$(":input").inputmask();
		
        $('#modalAncho').css("width","75%");
        $('#accion').click(function(){
		
		/* swal({
            title: "�Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
		*/
			var form = $("#formAjax");
    		var formData = new FormData(document.getElementById(form.attr('id')));
 			
    		$.ajax({
		        url: $("#formAjax").attr("action"),
		        type: "POST",
		        dataType: "json",
		        data: formData,
		        cache: false,
		        contentType: false,
		        processData: false
		    })
		        .done(function(dato){
				
           // $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
           			swal("Registro Modificado!", "El Organismo fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                   $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/organismoCONTROL');
                }else if(dato['status']=='nuevo'){
                    swal("Registro exitoso!", "El Organismo fue registrado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                  	$(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/organismoCONTROL');
                }
         //  },'json');
			 });
        });
    });
	
	
</script>