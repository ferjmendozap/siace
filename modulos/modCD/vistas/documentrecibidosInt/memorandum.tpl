<form action="{$_Parametros.url}modCD/distribsalidocumenextCONTROL/DevolucionMET"  autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}



<table id="principal"  align="center">
<tr><td width="707">
  <!-- *********************** -->
  <table align="center">
  <tr><td>
  <!-- CABECERA DEL DOCUMENTO -->
  <table width="679" align="right" id="cabecera" cellpadding="0" cellspacing="0">
  <tr>
   <td width="3"></td>
   <td width="124" align="center"><img src="imagenes/logoLlaves.jpg" style="height:80px; width:80px" /></td>
   <td width="10"></td>
   <td width="420">
   <!-- *********************** -->
   <table cellpadding="0" cellspacing="0">
   <tr>
      <td align="center" width="414"><font size="3" face="Arial">REPUBLICA BOLIVARIANA DE VENEZUELA</font></td>
   </tr>
   <tr>
      <td align="center"><font size="3" face="Arial">CONTRALORIA DEL ESTADO DELTA AMACURO</font></td>
   </tr>
   <tr>
      <td align="center"><font size="3" face="Arial"><?=htmlentities($fb['Dependencia'])?></font></td>
    </tr>
   </table>
   <!-- *********************** -->
   </td>
   <td width="120" align="center"><img src="imagenes/logoContraloria.jpg" style="height:80px; width:80px"/></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  </table>
  <!-- FIN CABECERA DEL DOCUMENTO -->
  </td></tr>
  </table>
  <!-- *********************** -->
  
</td></tr>
<tr><td>
   <!-- *********************** -->
   <table width="688" height="27" id="numero_doc"> 
   <tr>
    <td width="26" height="21"></td>
    <td width="26"></td>
    <td width="26"></td>
    <td width="436"></td>
    <td width="150"><font face="Arial"><b>N�:<?=$cod_documentocompleto?></b></font></td>
   </tr>
   </table>
   <!-- *********************** -->
</td></tr>

<tr><td>
   <!-- *********************** -->
   <table id="titulo">
   <tr>
    <td width="0"></td>
    <td width="0"></td>
    <td width="268"></td>
    <td width="148" align="center"><font size="3" face="Arial"><b>MEMORANDUM</b></font></td>
    <td width="0"></td>
  </tr>
  <tr><td height="5"></td></tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CUERPO 1 DEL DOCUMENTO -->
  <table id="cuerpo1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="34"></td>
    <td width="79"><font face="Arial" size="3"><b>PARA:</b></font></td>
    <td width="563"><font face="Arial" size="3"><?=htmlentities($fc['NomCompleto']);?></font></td>
  </tr>
  <tr>
    <td width="34"></td>
    <td width="79"></td>
    <td width="563"><font face="Arial" size="3"><?=$fc['DescripCargo']?></font></td>
  </tr>
   <tr><td height="5"></td></tr>
  <tr>
    <td></td>
    <td><font face="Arial" size="3"><b>DE:</b></font></td>
    <td><font face="Arial" size="3"><?=htmlentities($fb['Dependencia'])?></font></td>
  </tr>
   <tr><td height="5"></td></tr>
  <tr>
    <td></td>
    <td><font face="Arial" size="3"><b>FECHA:</b></font></td>
    <? 
	 list($a, $m, $d)=SPLIT( '[/.-]', $fa['FechaDocumento']); $f_documento=$d.'-'.$m.'-'.$a
	?>
    <td><font face="Arial" size="3"><?=$f_documento?></font></td>
  </tr>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td></td>
    <td><font face="Arial" size="3"><b>ASUNTO:</b></font></td>
    <td><font face="Arial" size="3"><?=$fa['Asunto']?></font></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CONTENIDO DEL DOCUMENTO -->
  <table width="701">
  <tr>
    <td width="28"></td>
    <td width="646"><div style="width:650px;"><font face="Arial" size="3"><?=$fa['Contenido']?></font></div></td>
    <td width="11"></td>
  </tr>
  </table>
  <!-- *********************** -->
<tr><td>
  <!-- *********************** -->
  <table align="center" id="atentamente" width="500">
  <tr>
    <td width="58"></td>
    <td align="center"><font face="Arial" size="3">Atentamente,</font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td height="25"></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center"></td>
    <td width="61"></td>
  </tr>
  <tr>
  <?
   $sd = "select * from rh_puestos where CodCargo = '".$fa['Cod_CargoRemitente']."'";
   $qd = mysql_query($sd) or die ($sd.mysql_error()); //echo $sa;
   $fd = mysql_fetch_array($qd); 
  ?>
    <td width="58"></td>
    <td align="center" width="365"><font face="Arial" size="3"><?=$fb['NomCompleto']?></font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center" width="600"><font face="Arial" size="3"><?=htmlentities($fd['DescripCargo'])?></font></td>
    <td width="61"></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- *********************** -->
  <table width="686" id="pie_pagina">
  <tr>
     <td width="26"></td>
     <td width="70"><font face="Arial" size="2"><?=$fa['MediaFirma']?></font></td>
     <td width="421"></td>
     <td width="149" align="right"></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>
<!--<center><input type="button" name="imprimir" value="Imprimir" onclick="window.print();"></center>-->
</table>


</td></tr>
<div id="divButtons" name="divButtons">  
<input type="button" id="imprimir" name="imprimir" value = "Imprimir" onclick="printPage()"/> 
</div> 
<div id="divButtons2" name="divButtons2">  
<input type="button" id="vista" name="vista" value = "Vista Previa" onclick="printPage2()"/> 
</div> 
</table>
					   
					

			  
                </div>
      
</form>

<script type="text/javascript">

    $(document).ready(function() {

        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
	
	function printPage() {  
if(document.all) {  
document.all.divButtons.style.visibility = 'hidden';  
document.all.divButtons2.style.visibility = 'hidden';  
window.print();  
document.all.divButtons.style.visibility = 'visible';  
document.all.divButtons2.style.visibility = 'visible';
} else {  
document.getElementById('divButtons').style.visibility = 'hidden';  
document.getElementById('divButtons2').style.visibility = 'hidden'; 
window.print();  
document.getElementById('divButtons').style.visibility = 'visible';  
document.getElementById('divButtons2').style.visibility = 'visible';  
}  
}  
/// VISTA PREVIA
function printPage2() {  
if(document.all) {  
document.all.divButtons2.style.visibility = 'hidden';
document.all.divButtons.style.visibility = 'hidden';   
window.print();  
document.all.divButtons2.style.visibility = 'visible'; 
document.all.divButtons.style.visibility = 'visible'; 
} else {  
document.getElementById('divButtons2').style.visibility = 'hidden';  
document.getElementById('divButtons').style.visibility = 'hidden'; 
window.print();  
document.getElementById('divButtons2').style.visibility = 'visible';  
document.getElementById('divButtons2').style.visibility = 'visible';
}  
}  

		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

		
	
		$('#fec_registro').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
		$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
      //  $('#fec_anulado').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
	    $('#modalAncho').css("width","90%");
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='devolucion'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
               // if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                $(document.getElementById('idDocumento'+dato['idDocumento'])).html('<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['txt_descripcion']+'</td>' +
                            '<td>'+dato['fk_a004_num_dependencia']+'</td>' +
                            '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-01-02-04-02-D',$_Parametros.perfil)}'+
                            '<button class="devolucion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Devolucion de Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '</td>');
                    swal("Registro Modificado!", "El Documento fue Devuelto satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='acuse'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    //if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idDocumento'+dato['idDocumento']+'">' +
                            '<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['txt_descripcion']+'</td>' +
                            '<td>'+dato['fk_a004_num_dependencia']+'</td>' +
                             '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-01-02-04-01-R',$_Parametros.perfil)}<button class="acuse logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Acuse Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
                            '</td>');
                    swal("Registro Modificado!", "El Documento recibio Acuse satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });
</script>
