<form action="{$_Parametros.url}modCD/distribsalidocumenextCONTROL/DevolucionMET"  autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
         <table id="Padre" name="Padre" align="center" cellpadding="0" cellspacing="0">
<tr>
<td>
<? 
  //echo $ftcuenta['DescripCorta'];
  if($ftcuenta['DescripCorta']=='ME'){
?>
<table id="principal"  align="center">
<tr><td width="707">
  <!-- *********************** -->
  <table align="center">
  <tr><td>
  <!-- CABECERA DEL DOCUMENTO -->
  <table width="679" align="right" id="cabecera" cellpadding="0" cellspacing="0">
  <tr>
   <td width="3"></td>
   <td width="124" align="center"><img src="imagenes/logoLlaves.jpg" style="height:80px; width:80px" /></td>
   <td width="10"></td>
   <td width="420">
   <!-- *********************** -->
   <table cellpadding="0" cellspacing="0">
   <tr>
      <td align="center" width="414"><font size="3" face="Arial">REPUBLICA BOLIVARIANA DE VENEZUELA</font></td>
   </tr>
   <tr>
      <td align="center"><font size="3" face="Arial">CONTRALORIA DEL ESTADO DELTA AMACURO</font></td>
   </tr>
   <tr>
      <td align="center"><font size="3" face="Arial"><?=htmlentities($fb['Dependencia'])?></font></td>
    </tr>
   </table>
   <!-- *********************** -->
   </td>
   <td width="120" align="center"><img src="imagenes/logoContraloria.jpg" style="height:80px; width:80px"/></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  </table>
  <!-- FIN CABECERA DEL DOCUMENTO -->
  </td></tr>
  </table>
  <!-- *********************** -->
  
</td></tr>
<tr><td>
   <!-- *********************** -->
   <table width="688" height="27" id="numero_doc"> 
   <tr>
    <td width="26" height="21"></td>
    <td width="26"></td>
    <td width="26"></td>
    <td width="436"></td>
    <td width="150"><font face="Arial"><b>N�:<?=$cod_documentocompleto?></b></font></td>
   </tr>
   </table>
   <!-- *********************** -->
</td></tr>

<tr><td>
   <!-- *********************** -->
   <table id="titulo">
   <tr>
    <td width="0"></td>
    <td width="0"></td>
    <td width="268"></td>
    <td width="148" align="center"><font size="3" face="Arial"><b>MEMORANDUM</b></font></td>
    <td width="0"></td>
  </tr>
  <tr><td height="5"></td></tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CUERPO 1 DEL DOCUMENTO -->
  <table id="cuerpo1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="34"></td>
    <td width="79"><font face="Arial" size="3"><b>PARA:</b></font></td>
    <td width="563"><font face="Arial" size="3"><?=htmlentities($fc['NomCompleto']);?></font></td>
  </tr>
  <tr>
    <td width="34"></td>
    <td width="79"></td>
    <td width="563"><font face="Arial" size="3"><?=$fc['DescripCargo']?></font></td>
  </tr>
   <tr><td height="5"></td></tr>
  <tr>
    <td></td>
    <td><font face="Arial" size="3"><b>DE:</b></font></td>
    <td><font face="Arial" size="3"><?=htmlentities($fb['Dependencia'])?></font></td>
  </tr>
   <tr><td height="5"></td></tr>
  <tr>
    <td></td>
    <td><font face="Arial" size="3"><b>FECHA:</b></font></td>
    <? 
	 list($a, $m, $d)=SPLIT( '[/.-]', $fa['FechaDocumento']); $f_documento=$d.'-'.$m.'-'.$a
	?>
    <td><font face="Arial" size="3"><?=$f_documento?></font></td>
  </tr>
  <tr>
    <td height="5"></td>
  </tr>
  <tr>
    <td></td>
    <td><font face="Arial" size="3"><b>ASUNTO:</b></font></td>
    <td><font face="Arial" size="3"><?=$fa['Asunto']?></font></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CONTENIDO DEL DOCUMENTO -->
  <table width="701">
  <tr>
    <td width="28"></td>
    <td width="646"><div style="width:650px;"><font face="Arial" size="3"><?=$fa['Contenido']?></font></div></td>
    <td width="11"></td>
  </tr>
  </table>
  <!-- *********************** -->
<tr><td>
  <!-- *********************** -->
  <table align="center" id="atentamente" width="500">
  <tr>
    <td width="58"></td>
    <td align="center"><font face="Arial" size="3">Atentamente,</font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td height="25"></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center"></td>
    <td width="61"></td>
  </tr>
  <tr>
  <?
   $sd = "select * from rh_puestos where CodCargo = '".$fa['Cod_CargoRemitente']."'";
   $qd = mysql_query($sd) or die ($sd.mysql_error()); //echo $sa;
   $fd = mysql_fetch_array($qd); 
  ?>
    <td width="58"></td>
    <td align="center" width="365"><font face="Arial" size="3"><?=$fb['NomCompleto']?></font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center" width="600"><font face="Arial" size="3"><?=htmlentities($fd['DescripCargo'])?></font></td>
    <td width="61"></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- *********************** -->
  <table width="686" id="pie_pagina">
  <tr>
     <td width="26"></td>
     <td width="70"><font face="Arial" size="2"><?=$fa['MediaFirma']?></font></td>
     <td width="421"></td>
     <td width="149" align="right"></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>
<!--<center><input type="button" name="imprimir" value="Imprimir" onclick="window.print();"></center>-->
</table>
<? 
  }else{
  //// ------------------------------- PUNTO DE CUENTA -------------------------
  //echo $ftcuenta['DescripCorta'];	
   if($ftcuenta['DescripCorta']=='PC'){
?>
<table id="principal" name="principal" width="700" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
  <td>
  <table align="center" width="700" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="119" align="center"><img src="imagenes/logoLlaves.jpg" width="64" height="83"/></td>
    <td width="397">
    <table width="395" height="101" border="1" align="center">
    <tr>
       <td width="402" height="35" align="center"><font size="2" face="Arial, Helvetica, sans-serif"><b>CONTRALORIA DEL ESTADO DELTA AMACURO</b></font></td>
    </tr>
    <tr>
       <td align="center" height="25"><font size="2" face="Arial, Helvetica, sans-serif"><b>PUNTO DE CUENTA</b></font></td>
    </tr>   
    </table>
    </td>
    <td width="176">
    <table width="175" height="100" border="1" align="center" cellpadding="0" cellspacing="0">
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>CODIGO:</b></font></td>
    </tr>
    <tr>
       <td align="center" colspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>FOR-CEDA-001</b></font></td>
    </tr>
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>REVISION</b></font></td>
    </tr>
    <tr>
       <td width="87" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>N�:</b></font></td>
       <td width="82" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA</b></font></td>
    </tr>
     <tr>
       <td height="15" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>0</b></font></td>
       <td height="15" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>05/2010</b></font></td>
    </tr>
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>PAGINA</b></font></td>
    </tr>
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>1 DE 1</b></font></td>
    </tr>
    </table>
    </td>
  </tr>
  </table>
  </td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- TABLA DATOS GENERALES ---------- /////////// --->
<table align="center" width="700" border="1">
<tr>
  <td width="87" rowspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>PRESENTADO:</b></font></td>
  <td width="33"><font size="1" face="Arial, Helvetica, sans-serif"><b>A:</b></font></td>
  <td width="385"><font size="1" face="Arial, Helvetica, sans-serif"><b><?=$fc['DescripCargo']?></b></font></td>
  <td width="167"><font size="1" face="Arial, Helvetica, sans-serif"><b>NUMERO: <?=$cod_documentocompleto;?></b></font></td>
</tr>
<tr>
    <? 
	 list($a, $m, $d)=SPLIT( '[/.-]', $fa['FechaDocumento']); $f_documento=$d.'-'.$m.'-'.$a
	?>
  <td><font size="1" face="Arial, Helvetica, sans-serif"><b>POR:</b></font></td>
  <td><font size="1" face="Arial, Helvetica, sans-serif"><b><?=htmlentities($fb['Dependencia'])?></b></font></td>
  <td><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA: <?=$f_documento;?></b></font></td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- ASUNTO ---------- /////////// --->
<table id="asunto" align="center" width="700" border="1">
<tr>
  <td width="20" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>ASUNTO:</b></font></td>
  <td><font size="1" face="Arial, Helvetica, sans-serif"><b><?=$fa['Asunto']?></b></font></td>
</tr>
<tr>
  <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>PROPUESTA:</b></font></td>
</tr>
<tr>
 <td height="300" colspan="2" align="center"><div align="center" style="width:690px;height:300px; text-align:justify"><font face="Arial" size="2"><?=$fa['Contenido']?></font></div><!--<textarea cols="83" rows="17" id="instrucciones"><?=$fa['Contenido']?></textarea>--></td>
</tr>
</table><br />
<!-- ////////// ---------- RESULTADO ---------- /////////// --->
<table id="resultado" align="center" width="700" cellpadding="0" cellspacing="0" border="1">
<tr>
   <td colspan="4" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>RESULTADO:</b></font></td>
</tr>
<tr>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>APROBADO</b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>NEGADO</b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>DIFERIDO</b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>VISTO</b></font></td>
</tr>
<tr>
 <td align="center" height="15"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
<tr>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
<tr>
 <td colspan="4" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>INSTRUCCIONES U OBSERVACIONES</b></font></td>
</tr>
<tr>
 <td colspan="4" height="150"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- FIRMAS ---------- /////////// --->
<table align="center" id="firmas" cellpadding="0" cellspacing="0" width="700" border="1">
<tr>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>PREPARADO POR:</b></font></td>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>SELLO</b></font></td>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>CONFORMADO POR:</b></font></td>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>SELLO</b></font></td>
</tr>
<tr>
   <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>FIRMA:</b></font></td>
   <td rowspan="3"></td>
   <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>FIRMA:</b></font></td>
   <td rowspan="3"></td>
</tr>
<tr>
   <td height="25"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
   <td height="10"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
<tr><? 
      list($a,$m,$d)=SPLIT('[-]',$fa['FechaDocumento']);
      $fechaDocumento = $d.'-'.$m.'-'.$a;
	?>
   <td height="20"><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA: <?=$fechaDocumento?></b></font></td>
   <td><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA:</b></font></td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- ANEXOS ---------- /////////// --->
<table align="center" id="anexos" width="700" border="1">
<tr>
<?
 if($fa['FlagsAnexo']=='S'){
	 $valor1 = 'X'; $valor2 = '';
 }else{
   if($fa['FlagsAnexo']=='N'){
	 $valor1 = ''; $valor2 = 'X';
 }
 }
?>
   <td width="44" rowspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>ANEXOS:</b></font></td>
   <td width="35" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>SI</b></font></td>
   <td width="26" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b><?=$valor1;?></b></font></td>
    <td width="567" rowspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>LISTA DE ANEXOS:<?=$fa['DescripcionAnexo']?></b></font></td>
</tr>
<tr>
   <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>NO</b></font></td>
   <td rowspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b><?=$valor2;?></b></font></td>
</tr>
</table>
<?	
 }else{
 if($ftcuenta['DescripCorta']=='CR'){
?>
<table id="principal"  align="center">
<tr><td width="707">
  <!-- *********************** -->
  <table align="center">
  <tr><td>
  <!-- CABECERA DEL DOCUMENTO -->
  <table width="679" align="right" id="cabecera" cellpadding="0" cellspacing="0">
  <tr>
   <td width="3"></td>
   <td width="124" align="center"><img src="imagenes/logoLlaves.jpg" style="height:80px; width:80px" /></td>
   <td width="10"></td>
   <td width="420">
   <!-- *********************** -->
   <table cellpadding="0" cellspacing="0">
   <tr>
      <td align="center" width="414"><font size="3" face="Arial">REPUBLICA BOLIVARIANA DE VENEZUELA</font></td>
   </tr>
   <tr>
      <td align="center"><font size="3" face="Arial">CONTRALORIA DEL ESTADO DELTA AMACURO</font></td>
   </tr>
  <tr>
      <td align="center"><font size="3" face="Arial"><?=htmlentities($fb['Dependencia'])?></font></td>
    </tr>
   </table>
   <!-- *********************** -->
   </td>
   <td width="120" align="center"><img src="imagenes/logoContraloria.jpg" style="height:80px; width:80px"/></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  </table>
  <!-- FIN CABECERA DEL DOCUMENTO -->
  </td></tr>
  </table>
  <!-- *********************** -->
  
</td></tr>
<tr><td>
   <!-- *********************** -->
   <table width="688" height="27" id="numero_doc"> 
   <!-- <tr><? list($ano, $meses, $dia) = SPLIT('[-]',$fa['FechaRegistro']);
   switch ($meses) {
		case 01: $mes = Enero; break;  case 07: $mes = Julio; break;
		case 02: $mes = Febrero;break; case 08: $mes = Agosto; break;
		case 03: $mes = Marzo;break;   case 09: $mes = Septiembre; break;
		case 04: $mes = Abril;break;   case 10: $mes = Octubre; break;
		case 05: $mes = Mayo;break;    case 11: $mes = Noviembre; break;
		case 06: $mes = Junio;break;   case 12: $mes = Diciembre; break;
	 }
       ?>
     <td colspan="5" align="right">Tucupita, <?=$dia;?> de <?=$mes;?> de <?=$ano;?></td>
   </tr>
   <tr>
     <td colspan="5" align="right">202� y 153�</td>
   </tr>-->
   <tr>
     <td height="20"></td>
   </tr>
   <tr>
    <td width="30"></td>
    <td width="250" height="21"><font face="Arial"><b><?=$cod_documentocompleto?></b></font></td>
    <td width="26"></td>
    <td width="26"></td>
    <td width="436"></td>
    <td width="26"></td>
   </tr>
   </table>
   <!-- *********************** -->
</td></tr>

<tr><td>
   <!-- *********************** -->
   <table id="titulo">
   <tr>
    <td width="0"></td>
    <td width="0"></td>
    <td width="268"></td>
    <td width="148" align="center"><font size="3" face="Arial"><b>CREDENCIAL</b></font></td>
    <td width="0"></td>
  </tr>
  <tr><td height="5"></td></tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CUERPO 1 DEL DOCUMENTO -->
  
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CONTENIDO DEL DOCUMENTO -->
  <table width="701">
  <tr>
    <td width="28"></td>
    <td width="646"><div style="width:650px;"><font face="Arial" size="2"><?=$fa['Contenido']?></font></div></td>
    <td width="11"></td>
  </tr>
  </table>
  <!-- *********************** -->
<tr><td>
  <!-- *********************** -->
  
  
  <tr><td>
  <!-- *********************** -->
  <table align="center" id="atentamente" width="500">
  <tr>
    <td width="58"></td>
    <td align="center"><font face="Arial" size="3"></font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td height="25"></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center"></td>
    <td width="61"></td>
  </tr>
  <tr>
  <?
   $sd = "select * from rh_puestos where CodCargo = '".$fa['Cod_CargoRemitente']."'";
   $qd = mysql_query($sd) or die ($sd.mysql_error()); //echo $sa;
   $fd = mysql_fetch_array($qd); 
  ?>
    <td width="58"></td>
    <td align="center" width="365"><font face="Arial" size="2"><?=$fb['NomCompleto']?></font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center" width="600"><font face="Arial" size="2"><?=$fd['DescripCargo']?></font></td>
    <td width="61"></td>
  </tr>
  <?
    $s_nivelacion = "select max(Secuencia) from rh_empleadonivelacion where CodPersona='".$fb['CodPersona']."'";
	$q_nivelacion = mysql_query($s_nivelacion) or die ($s_nivelacion.mysql_error());
	$f_nivelacion = mysql_fetch_array($q_nivelacion);
	
    $s_nivelacion2 = "select Documento from rh_empleadonivelacion where CodPersona='".$fb['CodPersona']."' and Secuencia = '".$f_nivelacion['0']."'";
	$q_nivelacion2 = mysql_query($s_nivelacion2) or die ($s_nivelacion2.mysql_error());
	$f_nivelacion2 = mysql_fetch_array($q_nivelacion2);
  ?>
  <!--<tr>
    <td width="58"></td>
    <td align="center" width="600"><font size="1"><?=$f_nivelacion2['Documento'];?></font></td>
    <td width="61"></td>
  </tr>-->
  </table>
  <!-- *********************** -->
</td></tr>

  <tr><td>
  <!-- *********************** -->
  
  <? if($fb['CodPersona'] == '000071'){?>
  <table align="center" id="atentamente" cellpadding="0" cellspacing="0">
  <tr>
    <td height="1"></td>
  </tr>
  <tr>
    <td align="center"></td>
  </tr>
  <!--<tr>
    <td align="center">Licdo. FREDDY CUDJOE</td>
  </tr>
  <tr>
    <td align="center">CONTRALOR (I) DEL ESTADO DELTA AMACURO</td>
  </tr> -->
  <tr>
    <td align="center"><font size="1">Designado mediante Resoluci�n N�. 01-00-000002 de  Fecha 05-01-2009,</font></td>
  </tr>
  <tr>
    <td align="center"><font size="1">Emanada del Despacho del Contralor General de la Rep�blica,</font></td>
  </tr>
  <tr>
    <td align="center"><font size="1">publicada en G.O.N� 39.092 de fecha 06-01-2009</font></td>
  </tr>
  </table>
  <? }else{
	   if($fb['CodPersona'] == '000049'){?>
  <table align="center" id="atentamente" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"></td>
  </tr>
  <!--<tr>
    <td align="center">Licdo. FREDDY CUDJOE</td>
  </tr>
  <tr>
    <td align="center">CONTRALOR (I) DEL ESTADO DELTA AMACURO</td>
  </tr> -->
  <tr>
    <td align="center"><font size="1">Seg&uacute;n Resoluci�n N�. CEDA 0003-2011 de  Fecha 07-01-2011,</font></td>
  </tr>
  <tr>
    <td align="center"><font size="1">Publicada en Gaceta Oficial Estado Delta Amacuro N� 01 Extraordinario de Fecha 10-01-2011</font></td>
  </tr>
  </table>
  <? }else{
	   if($fb['CodPersona'] == '000061'){?>
         <table align="center" id="atentamente" cellpadding="0" cellspacing="0">
        <tr>
        <td align="center"></td>
        </tr>
        <!--<tr>

        <td align="center">Licdo. FREDDY CUDJOE</td>
        </tr>
        <tr>
        <td align="center">CONTRALOR (I) DEL ESTADO DELTA AMACURO</td>
        </tr> -->
        <tr>
        <td align="center"><font size="1">Seg&uacute;n Resoluci�n N�. CEDA 0029-2012 de  Fecha 22-02-2012</font></td>
        </tr>
        <!--<tr>
        <td align="center"><font size="1">Publicada en Gaceta Oficial Estado Delta Amacuro N� 01 Extraordinario de Fecha 10-01-2011</font></td>
        </tr>-->
        </table>
  <?   }
	  }}?>
</td></tr>

<tr><td>
  <!-- *********************** -->
  <table width="686" id="pie_pagina">
  <tr>
     <td width="26"></td>
     <td width="70"><font face="Arial" size="2"><?=$fa['MediaFirma']?></font></td>
     <td width="421"></td>
     <td width="149" align="right"></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>
<!--<center><input type="button" name="imprimir" value="Imprimir" onclick="window.print();"></center>-->
</table>
<?
 }}}}}else
 if($fcon['Procedencia']=='EXT'){    
?>
<table align="center">
<tr>
  <td><font size="4"><b>Documento de Procedencia Externa</b></font></td>
</tr>
</table>

<?}?>

</td></tr>
<div id="divButtons" name="divButtons">  
<input type="button" id="imprimir" name="imprimir" value = "Imprimir" onclick="printPage()"/> 
</div> 
<div id="divButtons2" name="divButtons2">  
<input type="button" id="vista" name="vista" value = "Vista Previa" onclick="printPage2()"/> 
</div> 
</table>
					   
					

			  
                </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Guardar</button>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function() {

        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
	
	
		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

		
	
		$('#fec_registro').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
		$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
      //  $('#fec_anulado').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
	    $('#modalAncho').css("width","90%");
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='devolucion'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
               // if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                $(document.getElementById('idDocumento'+dato['idDocumento'])).html('<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['txt_descripcion']+'</td>' +
                            '<td>'+dato['fk_a004_num_dependencia']+'</td>' +
                            '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-01-02-04-02-D',$_Parametros.perfil)}'+
                            '<button class="devolucion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Devolucion de Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '</td>');
                    swal("Registro Modificado!", "El Documento fue Devuelto satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='acuse'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    //if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idDocumento'+dato['idDocumento']+'">' +
                            '<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['txt_descripcion']+'</td>' +
                            '<td>'+dato['fk_a004_num_dependencia']+'</td>' +
                             '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-01-02-04-01-R',$_Parametros.perfil)}<button class="acuse logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Acuse Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
                            '</td>');
                    swal("Registro Modificado!", "El Documento recibio Acuse satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });
</script>
