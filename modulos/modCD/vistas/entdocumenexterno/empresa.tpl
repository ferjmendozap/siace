<div class="row">
    <div class="col-lg-12 contain-lg">
        <div class="table-responsive">
            <table id="datatable2" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Documento Fiscal</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=i from=$lista}
                    <tr>
                        <input type="hidden" value="{$i.pk_num_persona}" class="persona"
                         nombre="{$i.nomProveedor} "
                         documento="{$i.ind_documento_fiscal}" pk="{$i.pk_num_persona}">
                        <td>{$i.nomProveedor}</td>
                        <td>{$i.ind_documento_fiscal}</td>
                    </tr>
							
                {/foreach}
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
	$('#datatable2').on('click', 'tbody tr', function () {
      //  $('#datatable2 tbody tr').click(function() {
            var input = $(this).find('input');
            {if $tipoEmpresa == 'empresa'}
                $(document.getElementById('docEmpresa')).val(input.attr('documento'));
                $(document.getElementById('nomEmpresa')).val(input.attr('nombre'));
				 $(document.getElementById('num_persona')).val(input.attr('pk'));
                $('#cerrarModal2').click();
            {/if}
        });
    });
</script>