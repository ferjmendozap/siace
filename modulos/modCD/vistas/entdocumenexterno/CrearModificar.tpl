<form action="{$_Parametros.url}modCD/entdocumenexternoCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
        
				    <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab"><font color="#0066FF">Datos Generales</font></a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab"><font color="#0066FF">Detalle de Documento</font></a></li>
             </ul>
			 
			   <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active" id="Datos">
			   <div class="row">
					<div class="col-sm-6">
                     <div class="form-group floating-label" id="fk_cdc003_num_tipo_documentoError">
                        <select  id="fk_cdc003_num_tipo_documento"  {if isset($veranular)}disabled{/if}  name="form[alphaNum][fk_cdc003_num_tipo_documento]" class="form-control" >
                            
                            {foreach item=app from=$corresp}
                                {if isset($formDB.fk_cdc003_num_tipo_documento)}
                                    {if $app.pk_num_tipo_documento==$formDB.fk_cdc003_num_tipo_documento}
                                        <option selected value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option> 
                                        {else}
                                        <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
                    </div>
                     <div class="form-group">
                                                        <div class="input-group date" id="fec_documentoError">
                                                            <div class="input-group-content">
															<input type="text"  {if isset($ver)}disabled{/if} class="form-control input-sm" name="form[alphaNum][fec_documento]" id="fec_documento" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}">

                                                                <label for="fec_documento">Fecha de Documento</label>
															</div>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                    </div>
					           </div>
							   

					<div class="col-sm-6">
                    <div class="form-group floating-label" id="num_documentoError">
                        <input type="text" {if isset($ver)}disabled{/if}  class="form-control" value="{if isset($formDB.num_documento)}{$formDB.num_documento}{/if}" name="form[txt][num_documento]" id="num_documento" >
                        <label for="num_documento"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
         
                      <div class="form-group">

                              <input type="text"   class="form-control" value="{if isset($formDB.fec_registro)}{$formDB.fec_registro}{else}{date('Y-m-d')}{/if}" name=""  id="fec_registro" disabled>
                              <label for="fec_registro"><i class="fa fa-calendar"></i>&nbsp;Fecha de Registro</label>
                              <input type="hidden"   class="form-control" value="{if isset($formDB.fec_registro)}{$formDB.fec_registro}{else}{date('Y-m-d')}{/if}" name="form[txt][fec_registro]" >
                          </div>

													

													
													
													
                </div>
				
		<div class="col-lg-4 center center-align">

		    <div class="radio radio-styled">
                <label>
					  <div style="size:6" class="text-primary text-center text-sm-2"><input type="radio"  value="checkDoc" id="checkDoc"  name="habilitarDeshabilitar"  {if isset($ver)}disabled{/if}>&nbsp;<b>Organismo Remitente</b></div>
			   </label>
            </div>

                                        <div class="col-lg-12">
								           <div class="form-group">
			                                   <select id="num_org_ext" name="form[alphaNum][num_org_ext]" class="form-control select2 input-sm"  disabled  {if isset($ver)}disabled{/if}  value="{if isset($formDB.Organismo)}{$formDB.Organismo}{/if}">

                                                            <option value="">Seleccione el organismo...</option>
                                                            {foreach item=dat from=$lista}
                                                                {if isset($formDB.num_org_ext)}
                                                                    {if $dat.pk_num_ente==$formDB.num_org_ext}
                                                                        <option selected value="{$dat.pk_num_ente}">{$dat.ind_nombre_ente}</option>
                                                                    {else}
                                                                        <option value="{$dat.pk_num_ente}">{$dat.ind_nombre_ente}</option>
                                                                    {/if}
                                                                {else}
                                                                    <option value="{$dat.pk_num_ente}">{$dat.ind_nombre_ente}</option>
                                                                {/if}
                                                            {/foreach}
                                               </select>
														  <label for="pk_num_ente"><i class="md md-account-balance"></i>&nbsp;Org. Remitente</label>
										   </div>
										</div>



                           <div class="col-lg-12">
 				                      	<div class="form-group">
                                               <select id="num_depend_ext" name="form[alphaNum][num_depend_ext]" disabled  value="{if isset($formDB.Dependencia)}{$formDB.Dependencia}{/if}"  {if isset($ver)}disabled{/if} class="form-control input-sm">
                                                 <option value="">Seleccione la Dependencia</option>
										      </select>
                                                       <label for="num_depend_ext"><i class="md md-map"></i> Dependencia</label>
										 </div>
							</div>

							</div>



           <!--        <div class="col-sm-4">
                       <center>
                           <div class="radio radio-styled">
                               <label>
                                   <div style="size:8" class="text-primary text-center text-sm-2"><center><input type="radio"  value="checkEmpresa" id="checkEmpresa"  name="habilitarDeshabilitar"  {if isset($ver)}disabled{/if}>&nbsp;
                                           <b>Organismo  Nuevo</b></center></div>
                               </label>
                           </div>
                       </center>
                       <hr class="ruler-lg">
                       <div class="col-sm-10">

                           <div class="form-group" id="num_empresaError">
                               <label for="nomEmpresa"><i class="icm icm-calculate2"></i>
                                   Nombre del Organismo</label>
                               <input type="text" class="form-control"
                                      value="{if isset($formDB.nomProveedor)}{$formDB.nomProveedor}{/if}"
                                      id="nomEmpresa" disabled readonly>

                           </div>

                           <div class="form-group">
                               <label for="docEmpresa"><i class="icm icm-calculate2"></i>
                                   Doc. Fiscal</label>
                               <input type="text" class="form-control"
                                      value="{if isset($formDB.ind_documento_fiscal)}{$formDB.ind_documento_fiscal}{/if}"
                                      id="docEmpresa" disabled readonly>
                           </div>

                       </div>
                       <div class="col-sm-1">
                           <div class="form-group">
                               <input id="num_persona" type="hidden"  value="{if isset($formDB.num_empresa)}{$formDB.num_empresa}{/if}" name="form[int][num_empresa]">
                               {if isset($ver)} {else}
                                   <button
                                           class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                           type="button"
                                           id="botonEmpresa"  disabled readonly
                                           data-toggle="modal" data-target="#formModal2"
                                           titulo="Buscar Empresa"
                                           url="{$_Parametros.url}modCD/entdocumenexternoCONTROL/EmpresaMET/empresa/"
                                   >
                                       <i class="md md-search"></i>
                                   </button>

                               {/if}
                           </div>
                       </div>



                   </div>-->


			   <div class="col-sm-4">
			<center>
			  <div class="radio radio-styled">
                  <label>
					       <div style="size:8" class="text-primary text-center text-sm-2"><center><input type="radio"  value="checkEmpresa" id="checkEmpresa"  name="habilitarDeshabilitar"  {if isset($ver)}disabled{/if}>&nbsp;
					 <b>Empresa Remitente</b></center></div>
					  </label>
                </div>
			</center>
					 	<hr class="ruler-lg">
                                        <div class="col-sm-10">
                                
                                        <div class="form-group" id="num_empresaError">
                                            <label for="nomEmpresa"><i class="icm icm-calculate2"></i>
                                                Nombre de empresa</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.nomProveedor)}{$formDB.nomProveedor}{/if}"
                                                   id="nomEmpresa" disabled readonly>

                                        </div>
										
										   <div class="form-group">
                                            <label for="docEmpresa"><i class="icm icm-calculate2"></i>
                                                Doc. Fiscal</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_documento_fiscal)}{$formDB.ind_documento_fiscal}{/if}"
                                                   id="docEmpresa" disabled readonly>
                                        </div>
										
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <input id="num_persona" type="hidden"  value="{if isset($formDB.num_empresa)}{$formDB.num_empresa}{/if}" name="form[int][num_empresa]">
											  {if isset($ver)} {else}
                                                                          <button
                                                                                class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                                type="button"
																				 id="botonEmpresa"  disabled readonly
                                                                                data-toggle="modal" data-target="#formModal2"
                                                                                titulo="Buscar Empresa"
                                                                                  url="{$_Parametros.url}modCD/entdocumenexternoCONTROL/EmpresaMET/empresa/"
                                                                                >
                                                                            <i class="md md-search"></i>
                                                                        </button>
             
											 {/if}
                                        </div>
                                    </div>
									
									
									
									 </div>
									 
									  <div class="col-sm-4">
									  
			<center>
			<div class="radio radio-styled">
                  <label>
					       <div style="size:8" class="text-primary text-center text-sm-2"><center><input type="radio"  value="checkProveedor" id="checkProveedor"  name="habilitarDeshabilitar"  {if isset($ver)}disabled{/if}>&nbsp;
					 <b>Particular Remitente</b></center></div>
					  </label>
                </div>
			</center>
					 	<hr class="ruler-lg">
                                        <div class="col-sm-10">
                                
                                        <div class="form-group" id="num_particular_remError">
                                            <label for="nombreProveedor"><i class="icm icm-calculate2"></i>
                                                Nombre del Particular</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.nombre_apellidos)}{$formDB.nombre_apellidos}{/if}"
                                                   id="nombreProveedor" disabled readonly>

                                        </div>
										
										   <div class="form-group">
                                            <label for="docProveedor"><i class="icm icm-calculate2"></i>
                                                Doc. Fiscal</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}"
                                                   id="docProveedor" disabled readonly>
                                        </div>
										
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <input id="pk_num_persona" type="hidden"  value="{if isset($formDB.num_particular_rem)}{$formDB.num_particular_rem}{/if}" name="form[alphaNum][num_particular_rem]">
											  {if isset($ver)} {else}
                                          <button type="button"
                                                  class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                  id="botonPersona"  disabled readonly
                                                  data-toggle="modal" data-target="#formModal2"
                                                  data-keyboard="false" data-backdrop="static"
                                                    titulo="Buscar Particular"
                                                    url="{$_Parametros.url}modCD/entdocumenexternoCONTROL/proveedorMET/proveedor/"
                                                    >
                                                  <i class="md md-search"></i>
                                            </button>
											 {/if}
                                        </div>
                                    </div>
									
									 </div>
							
									 
									 
									 
									 </div>
									 
							

					   </div>
					   
					 <!-----------DETALLE DOCUMENTO---------------------------------->		 
    <div role="tabpanel" class="tab-pane" id="Detalle">
	
						
				   <div class="col-sm-6">
				    <div style="size:8" class="text-primary text-center text-sm-2"><center><b>Datos de Recepci&oacute;n</b></center></div>
						 	<hr class="ruler-lg">
					
                    <div class="form-group floating-label" id="text_asuntoError">
                        <input type="text" {if isset($ver)}disabled{/if}  class="form-control" value="{if isset($formDB.text_asunto)}{$formDB.text_asunto}{/if}" name="form[txt][text_asunto]" id="text_asunto" >
                        <label for="text_asunto"><i class="md md-border-color"></i>&nbsp;AsuntoTratado</label>
                    </div>
		
				<div class="form-group" id="txt_descripcion_anexoError">
                <textarea name="form[txt][txt_descripcion_asunto]" id="txt_descripcion_asunto" class="form-control" rows="1"  {if isset($ver)}disabled{/if} placeholder="" required data-msg-required="Introduzca la Descripci�n del Asunto" >{if isset($formDB.txt_descripcion_asunto)}{$formDB.txt_descripcion_asunto}{/if}</textarea>
                <label for="txt_descripcion_asunto"><i class="md md-question-answer"></i>&nbsp;Comentario</label>
            </div>
			
			  <div class="col-sm-11">
                                        <div class="form-group" id="ind_persona_recibidoError">
                                            <label for="nombrePersona"><i class="md md-account-box"></i>
                                               Recibido Por</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_persona_recibido)}{$formDB.ind_persona_recibido}{/if}"
                                                   id="nombrePersona" disabled readonly>

                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <input id="ind_persona_recibido"  type="hidden" value="{if isset($formDB.ind_persona_recibido)}{$formDB.ind_persona_recibido}{/if}" name="form[alphaNum][ind_persona_recibido]">
											  {if isset($ver)} {else}
                                            <button 
											    class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
												 type="button"
                                                    title="Buscar Particular"
                                                    data-toggle="modal" data-target="#formModal2"
                                                    data-keyboard="false" data-backdrop="static"
                                                    titulo="Buscar Destinatario" 
                                                    url="{$_Parametros.url}modCD/entdocumenexternoCONTROL/personaMET/persona/"
                                                    > 
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
											 {/if}
                                        </div>
                                    </div>
			
			
			</div>
				
				<div class="col-sm-6">
				    <div style="size:8" class="text-primary text-center text-sm-2"><center><b>Datos del Documento</b></center></div>
						 	<hr class="ruler-lg">
							
            <div class="col-sm-12">
									
                    <div class="col-lg-4">
					<div class="form-group floating-label" id="num_folioError">
                  <input type="text"  class="form-control" value="{if isset($formDB.num_folio)}{$formDB.num_folio}{/if}" name="form[int][num_folio]" id="num_folio" >
                        <label for="num_folio"><i class="md md-content-paste"></i>Nro Folio</label>
                    </div>
				</div>	
				<div class="col-lg-4">
					<div class="form-group floating-label" id="num_anexoError">
                  <input type="text"  class="form-control" value="{if isset($formDB.num_anexo)}{$formDB.num_anexo}{/if}" name="form[int][num_anexo]" id="num_anexo">
                        <label for="num_anexo"><i class="md md-content-paste"></i>Nro Anexo</label>
                    </div>
				</div>	
				<div class="col-lg-4">
					<div class="form-group floating-label" id="num_carpetaError">
                  <input type="text"  class="form-control" value="{if isset($formDB.num_carpeta)}{$formDB.num_carpeta}{/if}" name="form[int][num_carpeta]" id="num_carpeta">
                        <label for="num_carpeta"><i class="md md-content-paste"></i>Nro Carpeta</label>
                    </div>
				</div>	
				
			</div>
			
			        <div class="col-sm-12">          
				<div class="form-group" id="ind_descripcion_anexoError">
                <textarea name="form[txt][ind_descripcion_anexo]" id="ind_descripcion_anexo"   class="form-control " rows="1"   >{if isset($formDB.ind_descripcion_anexo)}{$formDB.ind_descripcion_anexo}{/if}</textarea>
                <label for="ind_descripcion_anexo"><i class="md md-assignment"></i>&nbsp;Descripcion</label> 
            </div>
			 </div>
			
 </div>
 
				<div class="col-sm-6">
				    <div style="size:8" class="text-primary text-center text-sm-2"><center><b>Datos del Mensajero</b></center></div>
						 	<hr class="ruler-lg">
							
				 <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_nombre_mensajeroError">
                        <input type="text"  {if isset($ver)}disabled{/if}  class="form-control" value="{if isset($formDB.ind_nombre_mensajero)}{$formDB.ind_nombre_mensajero}{/if}" name="form[txt][ind_nombre_mensajero]" id="ind_nombre_mensajero" maxlength="25"  >
                        <label for="ind_nombre_mensajero"><i class="md md-assignment-ind"></i>&nbsp;Mensajero</label>
                    </div>
                </div>

				<div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_cedula_mensajeroError">
                        <input type="text" {if isset($ver)}disabled{/if}  class="form-control"  data-inputmask="'mask': '99999999'"  value="{if isset($formDB.ind_cedula_mensajero)}{$formDB.ind_cedula_mensajero}{/if}" name="form[int][ind_cedula_mensajero]" id="ind_cedula_mensajero"  maxlength="8">
                        <label for="ind_cedula_mensajero"><i class="md md-aspect-ratio"></i>&nbsp;Cedula</label>
                    </div>
                </div>
                                    
				    </div>
				
					
					  
					 				
				<div class="col-sm-12">
					<div class="col-sm-6">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
            	</div>
			
			<div class="col-sm-6">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
			  		</div>
               </div>
				 </div>
             
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
	
        <button type="button"  title="Cancelar" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span>&nbsp;Cancelar</button>
		{if isset($ver)} {else}
        <button type="button"  title="Guardar" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Guardar</button>
		{/if}
    </div>
</form>

<script type="text/javascript">


    $(document).ready(function() {

        $('.select2').select2({ allowClear: true });

		 //habilitar y deshabilitar campos del filtro
      $('.checked').click(function () {
               if(this.checked=!this.checked){
                   this.checked
               }
           });
		
		           $('#checkProveedor').click(function () {
               if(this.checked){
                   $('#botonPersona').attr('disabled', false);
				  $(document.getElementById('num_org_ext')).val("");
			   $(document.getElementById('num_depend_ext')).val("");
			     $(document.getElementById('nomEmpresa')).val("");
				  $(document.getElementById('docEmpresa')).val("");
			   $(document.getElementById('num_persona')).val("");
			   
				   $('#num_org_ext').attr('disabled', true);
				   $('#num_depend_ext').attr('disabled', true);
				   $('#botonEmpresa').attr('disabled', true);
               }else{
                   $('#botonPersona').attr('disabled', true);
              $(document.getElementById('nombreProveedor')).val("");
                 $(document.getElementById('docProveedor')).val("");
			   $(document.getElementById('pk_num_persona')).val("");

               }
           });
		   
		     $('#checkEmpresa').click(function () {
               if(this.checked){
                   $('#botonEmpresa').attr('disabled', false);
				  $(document.getElementById('num_org_ext')).val("");
			   $(document.getElementById('num_depend_ext')).val("");
			   $(document.getElementById('nombreProveedor')).val("");
                 $(document.getElementById('docProveedor')).val("");
			   $(document.getElementById('pk_num_persona')).val("");
			     $('#botonPersona').attr('disabled', true);
				   $('#num_org_ext').attr('disabled', true);
				   $('#num_depend_ext').attr('disabled', true);
               }else{
                   $('#botonEmpresa').attr('disabled', true);
               $(document.getElementById('nombreProveedor')).val("");
               $(document.getElementById('docProveedor')).val("");
			   $(document.getElementById('pk_num_persona')).val("");
			   $(document.getElementById('num_org_ext')).val("");
			   $(document.getElementById('num_depend_ext')).val("");
			   

               }
           });
	  
		    $('#checkDoc').click(function () {
			if(this.checked){
                   $('#num_org_ext').attr('disabled', false);
				   $('#num_depend_ext').attr('disabled', false);
				   $('#botonPersona').attr('disabled', true);
				   $('#botonEmpresa').attr('disabled', true);
			   $(document.getElementById('nombreProveedor')).val("");
                  $(document.getElementById('docProveedor')).val("");
				   $(document.getElementById('pk_num_persona')).val("");
				 $(document.getElementById('nomEmpresa')).val("");
				  $(document.getElementById('docEmpresa')).val("");
			   $(document.getElementById('num_persona')).val("");
               }else{
                   $('#num_org_ext').attr('disabled', true);
                 $(document.getElementById('num_org_ext')).val("");
			   $(document.getElementById('num_depend_ext')).val("");
               }
           });
		   
		   $('#checkAnexo').click(function () {
               if(this.checked){
                   $('#ind_descripcion_anexo').attr('disabled', false);
               }else{
                   $('#ind_descripcion_anexo').attr('disabled', true);
                   $(document.getElementById('ind_descripcion_anexo')).val("");
               }
           });

              if(document.getElementById("num_org_ext").value!=""){
			  	$("#checkDoc").attr('checked', true);
                   $('#botonPersona').attr('disabled', true);
				   $('#num_org_ext').attr('disabled', false);
				   $('#num_depend_ext').attr('disabled', false);
               }
			   
			     if(document.getElementById("nombreProveedor").value!="" || document.getElementById("docProveedor").value!=""){
			  	$("#checkProveedor").attr('checked', true);
                   $('#botonPersona').attr('disabled', false);
				   $('#num_org_ext').attr('disabled', true);
				   $('#num_depend_ext').attr('disabled', true);
               }
 			
			 if(document.getElementById("nomEmpresa").value!="" || document.getElementById("docEmpresa").value!=""){
			  	$("#checkEmpresa").attr('checked', true);
                   $('#botonEmpresa').attr('disabled', false);
				   $('#num_org_ext').attr('disabled', true);
				   $('#num_depend_ext').attr('disabled', true);
				   $('#botonPersona').attr('disabled', true);
               }
			
			
			 if(document.getElementById("ind_descripcion_anexo").value!=""){
			  	$("#checkAnexo").attr('checked', true);
                   $('#ind_descripcion_anexo').attr('disabled', false);
               }
 		
		   
		    $("#fec_documento").datepicker({
        format:'yyyy-mm-dd',
        language:'es',
        autoclose: true
    });

        $("#fec_registro").datepicker({
            format:'yyyy-mm-dd',
            language:'es',
            autoclose: true
        });
	
	 
	  
		$('#fec_registro').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
		
	 var cd = new  ModCdFunciones();
  
        var idDependencia = '{if isset($formDB.num_org_ext)}{$formDB.num_org_ext}{/if}';
        var idCentroCosto = '{if isset($formDB.num_depend_ext)}{$formDB.num_depend_ext}{/if}';
		
	cd.metJsonCentroCosto('{$_Parametros.url}modCD/entdocumenexternoCONTROL/JsonCentroCostoMET',idDependencia,idCentroCosto);
		  
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
	
		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
	
		$('#fec_documento').change(function () {
         var $from=$("#fec_registro").datepicker('getDate');
         var $to =$("#fec_documento").datepicker('getDate');
            if($to>$from)
		    {
			 $("#fec_documento").val(' ');
			  swal("Error!", "La fecha del documento, no puede ser superior a la fecha de registro.", "error");
			 
		 	}
         });

//		$(":input").inputmask();
        $('#modalAncho').css("width","80%");
        $('#accion').click(function(){
		 swal({
            title: "�Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
			
		
		
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
               // if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                $(document.getElementById('idDocumento'+dato['idDocumento'])).html('<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['num_documento']+'</td>' +
                            '<td>'+dato['text_asunto']+'</td>' +
							'<td>'+dato['txt_descripcion_asunto']+'</td>' +
                            '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="right">' +
                            '{if in_array('CD-01-01-01-01-02-M',$_Parametros.perfil)}'+
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Modificar Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
                            '</td>');
                    swal("Registro Modificado!", "El Documento fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/entdocumenexternoCONTROL');
                }else if(dato['status']=='nuevo'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    //if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idDocumento'+dato['fec_documento']+'">' +
                           '<td>'+dato['fec_documento']+'</td>' +
						    '<td>'+dato['num_documento']+'</td>' +
                            '<td>'+dato['text_asunto']+'</td>' +
							'<td>'+dato['txt_descripcion_asunto']+'</td>' +
                             '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="right">' +
                            '{if in_array('CD-01-01-01-01-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Modificar Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +
                            '</td>');
                    swal("Registro exitoso!", "El Documento fue registrado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/entdocumenexternoCONTROL');
                }
            },'json');
	
        });
    });
</script>
