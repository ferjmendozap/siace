<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Entrada de Documentos Externos - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>

                                <th width="100">Fecha de Documento</th>
                                <th  width="100">Secuencia</th>
                                <th  width="100">N de Documento</th>
								<th  width="210">Asunto</th>
								  <th  width="220">Comentario</th>
								 <th  width="70">Estatus</th>
                                <th width="70">Modificar - Ver</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
                                    <td><label>{$documento.fec_documento}</label></td>
                                    <td><label>{$documento.num_secuencia}</label></td>
                                    <td><label>{$documento.num_documento}</label></td>
									<td><label>{$documento.text_asunto}</label></td>
									 <td><label>{$documento.txt_descripcion_asunto}</label></td>
									 <td>
									 <label>{if $documento.ind_estado=='PE'}Pendiente{elseif $documento.ind_estado=='RE'}Recibido{elseif

									 $documento.ind_estado=='EV'}Enviado{/if}</label>
									 
									 </td>
                                   
                                    <td align="right">
										{if $documento.ind_estado!='RE' }
										{if in_array('CD-01-01-01-01-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Modificar"
                                                    descipcion="El Usuario a Modificado un Documento Externo" titulo="<i class='icm icm-cog3'></i>&nbsp;Documentos Externos | Editar Registro">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i> 
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                      	  {if in_array('CD-01-01-01-01-03-V',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Ver"
                                                descipcion="El Usuario esta viendo un Documento" titulo="<i class='icm icm-calculate2'></i> Documentos Externos | Ver Registro">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
										  {/if}

                                        {/if}

										{if $documento.ind_estado=='RE' }
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									  {if in_array('CD-01-01-01-01-03-V',$_Parametros.perfil)}
                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}" title="Ver"
                                                descipcion="El Usuario esta viendo un Documento" titulo="<i class='icm icm-calculate2'></i> Documentos Externos | Ver Registro">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
										
                                        {/if}
										   {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="6">
									<div class="form-group   col-lg-12">
                                    {if in_array('CD-01-01-01-01-01-N',$_Parametros.perfil)}
									
                                     <button  title="Nuevo Registro" class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
									                                                 descipcion="el Usuario a creado un Documento Externo "  titulo="<i class='icm icm-cog3'></i> Documentos Externo | Nuevo Registro" id="nuevo">
                                            <i class="md md-create"></i>&nbsp;Nuevo Documento 
                                    {/if}
									</div>
									
									
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}modCD/entdocumenexternoCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idDocumento:0 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post(url,{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });

        $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/entdocumenexternoCONTROL/verMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
		

    });
</script>