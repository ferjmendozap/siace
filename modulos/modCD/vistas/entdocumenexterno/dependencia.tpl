<div class="row">
    <div class="col-lg-12 contain-lg">
        <div class="table-responsive">
            <table id="datatable2" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Nro</th>
                    <th>Dependencia</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=i from=$lista}
                    <tr>
                        <input type="hidden" value="{$i.pk_num_dependencia_ext}" class="persona"
                         nombre="{$i.ind_descripcion}">
                        <td>{$i.pk_num_dependencia_ext}</td>
                        <td>{$i.ind_descripcion}</td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2 tbody tr').click(function() {
            var input = $(this).find('input');
            {if $tipoDependencia == 'dependencia'}
                $(document.getElementById('nombreDependencia')).val(input.attr('nombre'));
                $(document.getElementById('pk_num_dependencia_ext')).val(input.val());
                $('#cerrarModal2').click();
            {/if}
        });
    });
</script>