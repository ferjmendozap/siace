<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Dependencia Externa - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Descripcion Completa</th>
                                <th>Cargo Representante</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=dependencia from=$listado}
                                <tr id="idDependencia{$dependencia.pk_num_dependencia_ext}">
                                    <td><label>{$dependencia.pk_num_dependencia_ext}</label></td>
                                    <td><label>{$dependencia.ind_descripcion}</label></td>
                                    <td><label>{$dependencia.ind_cargo_representante}</label></td>
                                    <td>
                                        <i class="{if $dependencia.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('CD-01-03-01-02-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDependencia="{$dependencia.pk_num_dependencia_ext}" title="Modificar"
                                                    descipcion="El Usuario a Modificado una Dependencia" titulo="Modificar Dependencia">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('CD-01-03-01-02-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idDependencia="{$dependencia.pk_num_dependencia_ext}" title="Eliminar" boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado una Dependencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la  Dependencia!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="5">
									<div class="form-group   col-lg-12">
                                    {if in_array('CD-01-03-01-02-01-N',$_Parametros.perfil)}
                                        <button title="Nuevo Registro" class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descipcion="el Usuario a creado una Nueva Dependencia "  titulo="<i class='icm icm-cog3'></i> Crear Dependencia" id="nuevo" >
                                          <i class="md md-create"></i>&nbsp;Nuevo Dependencia
                                        </button>
                                    {/if}
									</div>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/dependenciaCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDependencia:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDependencia: $(this).attr('idDependencia')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idDependencia=$(this).attr('idDependencia');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                
                var $url='{$_Parametros.url}modCD/dependenciaCONTROL/eliminarMET';
                $.post($url, { idDependencia: idDependencia },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idDependencia'+dato['idDependencia'])).html('');
                        swal("Eliminado!", "la Dependencia fue eliminada satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>