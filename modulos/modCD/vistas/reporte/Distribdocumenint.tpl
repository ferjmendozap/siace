<!--********************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Reporte de Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867297           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************
 -->
<div class="card">

    <div class="card-head">
        <header> <h2 class="text-primary"> Reporte | Lista Distribuci&oacute;n Internos</h2></header>
    </div>

	
        <form   class="form" action="{$_Parametros.url}modCD/reporteCONTROL/DistribdocumenintReportePdfMET"   method="post" target="iReporte">
            <div class="col-lg-5">
			 <div class="form-group">   
                      <select id="ind_dependencia_remitente" name="form[int][ind_dependencia_remitente]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$_TipoDependencia}
                                {if isset($formDB.ind_dependencia_remitente)}
                                    {if $app.pk_num_dependencia==$formDB.ind_dependencia_remitente}
                                        <option selected value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                        {else}
                                        <option value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="ind_dependencia_remitente"><i class="md md-account-balance"></i>&nbsp;Dep. Remitente</label>
 						</div>
					 
					  <div class="form-group">   
                       <select id="ind_dependencia_destinataria" name="form[int][ind_dependencia_destinataria]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$_TipoDependencia}
                                {if isset($formDB.ind_dependencia_destinataria)}
                                    {if $app.pk_num_dependencia==$formDB.ind_dependencia_destinataria}
                                        <option selected value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                        {else}
                                        <option value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_dependencia}">{$app.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
						 <label for="ind_dependencia_destinataria"><i class="md md-face-unlock"></i>&nbsp;Dep. Destinatario</label>
					</div>	
			</div>
			
					<div class="col-lg-3">
		<div class="form-group">
				<select  id="ind_estado" name="form[txt][ind_estado]" class="form-control" >
						 <option value="">&nbsp;</option>	
						   
						   {if isset($formDB.ind_estado) and $formDB.ind_estado == EV}
						   <option selected value="EV">Enviado</option>
							  {else}
							  <option value="EV">Enviado</option>
							    {/if}
					
						</select>
					   
                        <label for="ind_estado"><i class="md md-assignment-turned-in"></i>&nbsp;Estado</label>
						</div>
						
						
					<div class="form-group">
							  <select id="fk_cdc003_num_tipo_documento"  name="form[txt][fk_cdc003_num_tipo_documento]" class="form-control">
					           	 <option value="">&nbsp;</option>	
                            {if isset($formDB.fk_cdc003_num_tipo_documento) and $formDB.fk_cdc003_num_tipo_documento == 1}
							
                                    <option selected value="1">MEMORANDUM</option>
                                {else}
                                    <option value="1">MEMORANDUM</option>
                            {/if}
							  {if isset($formDB.fk_cdc003_num_tipo_documento) and $formDB.fk_cdc003_num_tipo_documento == 3}
                                    <option selected value="3">PUNTO DE CUENTA</option>
                                {else}
                                    <option value="3">PUNTO DE CUENTA</option>
                            {/if}
							  {if isset($formDB.fk_cdc003_num_tipo_documento) and $formDB.fk_cdc003_num_tipo_documento == 5}
                                    <option selected value="5">CREDENCIALES</option>
                                {else}
                                    <option value="5">CREDENCIALES</option>
                            {/if}
                           </select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
					</div>
					
					 </div>
					 	
					
						 <div class="form-group col-lg-4">
						 

                    <div id="fec_documento" class="input-daterange input-group">
					<span class="input-group-addon">Desde</span>
                        <div class="input-group-content">
                            <input class="form-control" type="text" id="desde" name="form[txt][desde]" >
                            <label>Fecha  Registro</label>
                        </div>

					
                        <span class="input-group-addon">Hasta</span>
                        <div class="input-group-content">
                            <input class="form-control" type="text"  id="hasta"  name="form[txt][hasta]">
                            <div class="form-control-line"></div>
                        </div>
                   </div>
				   
	<div class="form-group   col-lg-12"  align="left">

 
                       <select id="pk_num_documento" name="form[int][pk_num_documento]" class="form-control">
                            <option value="">&nbsp;</option>
                            {foreach item=app from=$_TipoDocumento}
                                {if isset($formDB.pk_num_documento)}
                                    {if $app.pk_num_documento==$formDB.pk_num_documento}
                                        <option selected value="{$app.pk_num_documento}">{$app.ind_documento_completo}</option>
                                        {else}
                                        <option value="{$app.pk_num_documento}">{$app.ind_documento_completo}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_documento}">{$app.ind_documento_completo}</option>
                                {/if}
                            {/foreach}
                        </select>
						 <label for="pk_num_documento"><i class="md md-description"></i>&nbsp;Num Documento</label>

			</div>       
                </div>


		<div class="form-group   col-lg-12"  align="center">
   			<button type="submit" class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:-20px">Buscar</button>
		</div>  

	    </form>
</div><!--end .card -->

<div class="card">
 <div align="center" style="width:100%; background:#66CCCC">&nbsp;&nbsp;<font color="#FFFFFF"><b>Listado de Documentos</b></font></div>

          <center><iframe name="iReporte" id="iReporte" style="border:solid 1px #CDCDCD; width:1000px; height:400px;"></iframe></center>
 </div>			

    <script type="text/javascript">

        $(document).ready(function() {
            $('#modalAncho').css("width", "85%");
           	 //$('#fechas').datepicker({ format: 'dd/mm/yyyy' });
		$("#fec_documento").datepicker({
        format:'yyyy-mm-dd',
        language:'es',
        autoclose: true
    });
	
        });
    </script>


