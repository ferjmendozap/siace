<section class="style-default-bright">
<form action="{$_Parametros.url}modCD/preparardocumenextCONTROL/ModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
        
				    <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab"><font color="#0066FF">Datos Generales</font></a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab"><font color="#0066FF">Contenido del Documento</font></a></li>
             </ul>
			 
			   <!-- Tab panes -->
            <div class="tab-content">
			 <div role="tabpanel" class="tab-pane active" id="Datos">
			   <div class="row">
			   <div class="col-sm-4">
                     <div class="form-group floating-label" id="fk_cdc003_num_tipo_documentoError">
                       <select id="fk_cdc003_num_tipo_documento" disabled name="form[alphaNum][fk_cdc003_num_tipo_documento]" class="form-control">
                            {if isset($formDB.fk_cdc003_num_tipo_documento) and $formDB.fk_cdc003_num_tipo_documento == 2}
                                    <option selected value="2">OFICIO</option>
                                {else}
                                    <option value="2">OFICIO</option>
                            {/if}
							{if isset($formDB.fk_cdc003_num_tipo_documento) and $formDB.fk_cdc003_num_tipo_documento == 4}
                                    <option selected value="4">OFICIO CIRCULAR</option>
                                {else}
                                    <option value="4">OFICIO CIRCULAR</option>
                            {/if}
                           </select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
                    </div>
   				</div>
				
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="fec_documentoError">
                              <input type="text" disabled class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}" name="form[alphaNum][fec_documento]" >
					          <label for="fec_documento"><i class="md md-event"></i>&nbsp;Fecha Documento</label>
                       </div>
                </div>
				
					
					<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_cod_completoError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_cod_completo)}{$formDB.ind_cod_completo}{/if}" name="form[alphaNum][ind_cod_completo]" id="ind_cod_completo">
                        <label for="ind_cod_completo"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
                </div>
				
				
				<div class="col-lg-2">
					<div class="form-group floating-label" id="ind_media_firmaError">
                  <input type="text"  class="form-control" value="{if isset($formDB.ind_media_firma)}{$formDB.ind_media_firma}{/if}" name="form[formula][ind_media_firma]" id="ind_media_firma" maxlength="5">
                        <label for="ind_media_firma"><i class="md md-description"></i>&nbsp;Iniciales</label>
                    </div>
				</div>	
				
 
 				<div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_asuntoError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}" name="form[alphaNum][ind_asunto]" id="ind_asunto">
                        <label for="ind_asunto"><i class="md md-question-answer"></i>&nbsp;Asunto</label>
                    </div>
				</div>
				
				

	<div class="col-lg-6">
			 <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo" disabled  name="form[alphaNum][fk_a001_num_organismo]" class="form-control" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo Remitente</label>
                    </div>
					</div> 
					
								 <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="docProveedor"><i class="md md-contacts"></i>
                                             Cargo Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_cargo_remitente)}{$formDB.ind_cargo_remitente}{/if}"
                                                   id="docProveedor" disabled readonly>
                                        </div>
                                    </div>
									
									 <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="nombreProveedor"><i class="md md-group"></i>
                                              Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_persona_remitente)}{$formDB.ind_persona_remitente}{/if}"
                                                   id="nombreProveedor" disabled readonly>
                                        </div>
                                    </div>
														
              						 <div class="col-sm-6">
                                        <div class="form-group" id="ind_dependenciaError">
                                            <label for="dependencia"><i class="icm icm-calculate2"></i>
                                                Dependencia Remitente</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}"
                                                   id="dependencia" disabled readonly>

                                        </div>
                                    </div>
	
				<div class="col-sm-12">
					<div class="col-sm-6">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
            	</div>
			
			<div class="col-sm-6">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
			  		</div>
               </div>
				 </div>
				 
						  
						  </div>
					  </div>
					   
					 <!-----------DETALLE DOCUMENTO---------------------------------->		 
    <div role="tabpanel" class="tab-pane" id="Detalle">
			   
			      <div>
			<div class="col-lg-12">
						<div class="card">
						<div class="form-group" id="txt_contenidoError">
						<textarea id="ckeditor" name="form[formula][txt_contenido]" class="form-control" rows="6">
							{if isset($formDB.txt_contenido)}{$formDB.txt_contenido}{/if}
								</textarea>
								
							</div>	
						</div><!--end .card -->
					</div>

			  
                </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Guardar</button>
    </div>
</form>
 </div>
<script type="text/javascript">

    $(document).ready(function() {

        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
	
	
		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

	
		$('#fec_registro').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
		$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
	    $('#modalAncho').css("width","75%");
        $('#accion').click(function(){
		 swal({
            title: "�Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                
                    swal("Registro Preparado!", "El Documento fue Preparado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#prueba').html('');
					$('#_contenido').load('{$_Parametros.url}modCD/preparardocumenextCONTROL');
                }
            },'json');
        });
    });
</script>
