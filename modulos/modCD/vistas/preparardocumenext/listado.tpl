<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Salida de Documentos | Preparar</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                   <th width="100">Nro de Documento</th>
								  <th  width="190">Remitente</th>
                                <th  width="100">Asunto</th>
								<th  width="200">Comentario</th>
								 <th  width="70">Estado</th>
                                <th width="70">Contenido</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_documento}">
                                    <td><label>{$documento.ind_cod_completo}</label></td>
                                    <td><label>{$documento.ind_dependencia}</label></td>
									<td><label>{$documento.ind_asunto}</label></td>
									 <td><label>{$documento.txt_descripcion} </label></td>
									 <td>
									 <label>{if $documento.ind_estado=='PR'}Preparacion{/if}</label>
									 
									 </td>
                                   
                                    <td align="left">
                                        {if in_array('CD-01-01-02-02-01-N',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" 
                                                    idDocumento="{$documento.pk_num_documento}" title="Preparar Documento"
                                                    descipcion="El Usuario a Modificado un Documento Externo" titulo="<i class='icm icm-cog3'></i>&nbsp;Editor de Documentos Salidas">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                       
                    </table>
                </div>
            </div>
        </div>
    </div>
   
   
</section>

<section class="style-default-bright">
   	 <div class="well clearfix">
			 <div class="card">
		 <div id="prueba"></div>
		 </div>
		 </div>

</section>	


<script type="text/javascript">
    $(document).ready(function() {
       
         var $url='{$_Parametros.url}modCD/preparardocumenextCONTROL/ModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento: $(this).attr('idDocumento')},function($dato){
                $('#prueba').html($dato);
				
            });
        });
        
       
    });
</script>