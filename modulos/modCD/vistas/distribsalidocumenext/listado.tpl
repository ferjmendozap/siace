<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Salida de Documentos Externos | Distribuci&oacute;n</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                               <th  width="80">Fecha Documento</th>
							    <th width="80">Nro de Documento</th>
                                <th  width="170">Asunto</th>
								<th  width="170">Comentario</th>
								<th  width="60">Estado</th>
								 <th width="90">Acuse - Devoluci&oacute;n</th>
								
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_distribucion}">
                                    <td><label>{$documento.fec_documento}</label></td>
                                    <td><label>{$documento.ind_cod_completo}</label></td>
									 <td><label>{$documento.ind_asunto}</label></td>
									 <td><label>{$documento.txt_descripcion}</label></td>
									 <td>
									 <label>{if $documento.ind_estado=='EV'}Enviado{/if}</label>
									 
									 </td>
									 
									 <td>
										{if $documento.ind_estado=='EV' }
									
									        {if in_array('CD-01-01-02-04-01-R',$_Parametros.perfil)}
                                   		<button class="acuse logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_distribucion}" title="Acuse"
                                                descipcion="El Usuario esta viendo un Documento" titulo="<i class='icm icm-calculate2'></i> Documentos de Salida | Acuse">
                                            <i class="md md-assignment-turned-in" style="color: #ffffff;"></i>
                                        </button>	
											
                                        {/if}
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                   	  {if in_array('CD-01-01-02-04-02-D',$_Parametros.perfil)}
                                        <button class="devolucion logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_distribucion}" title="Devolución"
                                                descipcion="El Usuario esta viendo un Documento" titulo="<i class='icm icm-calculate2'></i> Documentos de Salida | Devolucion">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
									
                                
									 {/if}
									 </td>
									 
								  </tr>
                            {/foreach}
                        </tbody>
                       
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript"> 
    $(document).ready(function() {
       
      		
		 $('#datatable1 tbody').on( 'click', '.devolucion', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/distribsalidocumenextCONTROL/devolucionMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
		
		
		 $('#datatable1 tbody').on( 'click', '.acuse', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/distribsalidocumenextCONTROL/AcuseMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
        
		 
       
    });
</script>