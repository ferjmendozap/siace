<form action="{$_Parametros.url}modCD/distribsalidocumenextCONTROL/DevolucionMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
          
			     <div class="row">
			   <div class="col-sm-3">
                     <div class="form-group floating-label" id="fk_cdc003_num_tipo_documentoError">
                       <select  id="fk_cdc003_num_tipo_documento" readonly  {if isset($veranular)}disabled{/if}   name="form[alphaNum][fk_cdc003_num_tipo_documento]" class="form-control" >
                            
                            {foreach item=app from=$correspon}
                                {if isset($formDB.fk_cdc003_num_tipo_documento)}
                                    {if $app.pk_num_tipo_documento==$formDB.fk_cdc003_num_tipo_documento}
                                        <option selected value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
                    </div>
   				</div>
				
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="fec_envioError">
                              <input type="text"  readonly {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.fec_envio)}{$formDB.fec_envio}{/if}" name="form[txt][fec_envio]" >
					          <label for="fec_envio"><i class="md md-event"></i>&nbsp;Fecha de env&iacute;o</label>
                       </div>
                </div>
				
					
					<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_cod_completoError">
                        <input type="text"  readonly  {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.ind_cod_completo)}{$formDB.ind_cod_completo}{/if}" name="form[txt][ind_cod_completo]" id="ind_cod_completo">
                        <label for="ind_cod_completo"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
                </div>
				
				<div class="col-sm-3">
 			   <div class="form-group">
                 <input type="text"  readonly  class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}" name="form[txt][fec_documento]" id="fec_documento">
                        <label for="fec_documento"><i class="fa fa-calendar"></i>&nbsp;Fecha de Documento</label>
                    </div>		
				</div>
					
 				<div class="col-sm-4">
                    <div class="form-group floating-label" id="ind_asuntoError">
                        <input type="text" readonly  {if isset($veranular)}disabled{/if}  class="form-control" value="{if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}" name="form[alphaNum][ind_asunto]" id="ind_asunto">
                        <label for="ind_asunto"><i class="md md-question-answer"></i>&nbsp;Asunto</label>
                    </div>
				</div>

								
	<div class="col-sm-8">
				<div class="form-group" id="txt_descripcionError">
                <textarea name="form[txt][txt_descripcion]" id="txt_descripcion" readonly  {if isset($veranular)}disabled{/if}  class="form-control" rows="1" placeholder="" required data-msg-required="Introduzca la Descripci�n del Asunto" >{if isset($formDB.txt_descripcion)}{$formDB.txt_descripcion}{/if}</textarea>
                <label for="txt_descripcion">Descripcion Asunto</label>
            </div>
			</div>
			
                                      
               <div class="col-sm-8">
                                        <div class="form-group" id="ind_persona_anuladoError">
                                            <label for="nombrePersona"><i class="icm icm-calculate2"></i>
                                                Nombre del Responsable</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.nombre_apellidos)}{$formDB.nombre_apellidos}{/if}"
                                                   id="nombrePersona" disabled readonly>

                                        </div>
                                    </div>
						      <input id="pk_num_persona"  type="hidden"  value="{if isset($formDB.fk_a003_num_persona)}{$formDB.fk_a003_num_persona}{/if}" name="form[alphaNum][fk_a003_num_persona]">
					
					<div class="col-sm-4">
					 <div class="form-group">
                                                        <div class="input-group date" id="fec_devolucionError">
                                                            <div class="input-group-content">
															<input type="text"  class="form-control input-sm" name="form[alphaNum][fec_devolucion]" id="fec_devolucion" value="{if isset($formDB.fec_devolucion)}{$formDB.fec_devolucion}{/if}">
															
                                                                <label for="fec_devolucion">Fecha de Devoluci&oacute;n</label>
															</div>
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                    </div>
							</div>
					
					<div class="col-sm-12">
				  <div   align="left">&nbsp;&nbsp;<font color="#20B2AA"><b>Indique cual es el motivo de la Anulaci&oacute;n</b></font></div>
                    <div class="form-group floating-label" id="ind_motivo_anuladoError">
 				  
							  
									     <textarea  required="" placeholder="" rows="1" class="form-control" id="ind_motivo_devolucion" name="form[alphaNum][ind_motivo_devolucion]" >{if isset($formDB.ind_motivo_devolucion)}{$formDB.ind_motivo_devolucion}{/if}</textarea>
			
                        <label for="ind_motivo_anulado"><i class="md md-question-answer"></i>&nbsp;Motivo de Anulaci&oacute;n</label>
                    </div>
				</div>	
						  </div>
					  </div>
					   
 <input  type="hidden"  class="form-control" value="{if isset($formDB.fec_registro)}{$formDB.fec_registro}{/if}" name="form[alphaNum][fec_registro]" id="fec_registro">
 
 <input  type="hidden"  class="form-control" value="{if isset($formDB.pk_num_documento)}{$formDB.pk_num_documento}{/if}" name="form[alphaNum][fk_cdc011_num_documento]" id="pk_num_documento">
 
    <input  type="hidden"  class="form-control" value="{if isset($formDB.ind_persona_remitente)}{$formDB.ind_persona_remitente}{/if}" name="form[alphaNum][ind_persona_remitente]" id="ind_persona_remitente">
									   
									     <input  type="hidden"  class="form-control" value="{if isset($formDB.ind_cargo_remitente)}{$formDB.ind_cargo_remitente}{/if}" name="form[alphaNum][ind_cargo_remitente]" id="ind_cargo_remitente">
										 
										 		 <input  type="hidden"  class="form-control" value="{if isset($formDB.pk_num_dependencia)}{$formDB.pk_num_dependencia}{/if}" name="form[alphaNum][fk_a004_num_dependencia]" id="pk_num_dependencia">
												 
												 <input  type="hidden"  class="form-control" value="{if isset($formDB.fk_a001_num_organismo)}{$formDB.fk_a001_num_organismo}{/if}" name="form[alphaNum][fk_a001_num_organismo]" id="fk_a001_num_organismo">	 
												 
			  
                </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Guardar</button>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function() {

        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });

	  $("#fec_devolucion").datepicker({
        format:'yyyy-mm-dd',
        language:'es',
        autoclose: true
    });

		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

		
	
	//$('#fec_registro').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
	//	$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
      //  $('#fec_anulado').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
	    $('#modalAncho').css("width","75%");
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='devolucion'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
               // if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                $(document.getElementById('idDocumento'+dato['idDocumento'])).html(
                            '<td>'+dato['fec_documento']+'</td>' +
                            '<td>'+dato['ind_cod_completo']+'</td>' +
							 '<td>'+dato['ind_asunto']+'</td>' +
                            '<td>'+dato['txt_descripcion']+'</td>' +
							 '<td>'+dato['ind_estado']+'</td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-01-02-04-02-D',$_Parametros.perfil)}'+
                            '<button class="devolucion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idDocumento="'+dato['idDocumento']+'"' +
                            'descipcion="El Usuario a Modificado un Documento Externo" titulo="Devolucion de Documento">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '</td>');
                    swal("Registro Modificado!", "El Documento fue Devuelto satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					//$(document.getElementById('idDocumento'+dato['idDocumento'])).html('');
					$('#_contenido').load('{$_Parametros.url}modCD/distribsalidocumenextCONTROL');
                }
            },'json');
        });
    });
</script>
