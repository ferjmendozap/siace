<form action="{$_Parametros.url}modCD/entradadetCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idCorrespondencia) }
            <input type="hidden" value="{$idCorrespondencia}" name="idCorrespondencia"/>
        {/if}
<div class="row">
					<div class="col-sm-3">
                    
					<div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_informe_escrito) and $formDB.ind_informe_escrito==1} checked{/if} value="1" name="form[alphaNum][ind_informe_escrito]">
                            <span>Informarme por escrito</span>
                        </label>
                    </div>
  
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_hablar_conmigo) and $formDB.ind_hablar_conmigo==1} checked{/if} value="1" name="form[alphaNum][ind_hablar_conmigo]">
                            <span>Hablar conmigo al respecto</span>
                        </label>
                    </div>
     
	 
	 			 <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_investigar_informar) and $formDB.ind_investigar_informar==1} checked{/if} value="1" name="form[alphaNum][ind_investigar_informar]">
                            <span>Investigar e informar verbalmente</span>
                        </label>
                    </div>
			
                </div>				
							
						
				
				<div class="col-sm-3">
	
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_tramitar_conclusion) and $formDB.ind_tramitar_conclusion==1} checked{/if} value="1" name="form[alphaNum][ind_tramitar_conclusion]">
                            <span>Tramitar hasta su conclusi&oacute;n</span>
                        </label>
                    </div>
     
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_distribuir) and $formDB.ind_distribuir==1} checked{/if} value="1" name="form[alphaNum][ind_distribuir]">
                            <span>Distribuir</span>
                        </label>
                    </div>

                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_conocimiento) and $formDB.ind_conocimiento==1} checked{/if} value="1" name="form[alphaNum][ind_conocimiento]">
                            <span>Para su conocimiento y fines pertinentes</span>
                        </label>
                    </div>
					
                </div>				

				<div class="col-sm-3">
				
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_preparar_constentacion) and $formDB.ind_preparar_constentacion==1} checked{/if} value="1" name="form[alphaNum][ind_preparar_constentacion]">
                            <span>Preparar contestacion para mi firma</span>
                        </label>
                    </div>
  
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_archivar) and $formDB.ind_archivar==1} checked{/if} value="1" name="form[alphaNum][ind_archivar]">
                            <span>Archivar</span>
                        </label>
                    </div>
		
                </div>	
						
					
				<div class="col-sm-3">
                   <div class="checkbox checkbox-styled">
                        <label>
                        <input type="checkbox" {if isset($formDB.ind_conocer_opinion) and $formDB.ind_conocer_opinion==1} checked{/if} value="1" name="form[alphaNum][ind_conocer_opinion]">
                            <span>Para conocer su opinion</span>
                        </label>
                    </div>
  
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_tramitar_caso) and $formDB.ind_tramitar_caso==1} checked{/if} value="1" name="form[alphaNum][ind_tramitar_caso]">
                            <span>Tramitar en caso de proceder</span>
                        </label>
                    </div>
     
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.ind_acusar_recibo) and $formDB.ind_acusar_recibo==1} checked{/if} value="1" name="form[alphaNum][ind_acusar_recibo]">
                            <span>Acusa recibo</span>
                        </label>
                    </div>
			
		   		  </div>	
				  </div>	
				  	
				 <div class="col-sm-2">	
				<div class="form-group floating-label" id="ind_coordinar_conError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_coordinar_con)}{$formDB.ind_coordinar_con}{/if}" name="form[alphaNum][ind_coordinar_con]" 
				  id="ind_coordinar_con">
                        <label for="ind_coordinar_con"><i class="md md-description"></i>Coordinar con:</label>
					 </div>	
					  	</div>	
						
						<div class="col-sm-3">
						<div class="form-group floating-label" id="ind_preparar_memorandumError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_preparar_memorandum)}{$formDB.ind_preparar_memorandum}{/if}" name="form[alphaNum][ind_preparar_memorandum]" 
				  id="ind_preparar_memorandum">
                        <label for="ind_preparar_memorandum"><i class="md md-description"></i>Prepara memo a:</label>
					 </div>	
          					</div>					
							
				
					<div class="col-sm-2">	
				<div class="form-group floating-label" id="ind_registro_deError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_registro_de)}{$formDB.ind_registro_de}{/if}" name="form[alphaNum][ind_registro_de]" 
				  id="ind_registro_de">
                        <label for="ind_registro_de"><i class="md md-description"></i>Registro de:</label>
					 </div>	
					  </div>	
		  
				  <div class="col-sm-2">	
				  <div class="form-group floating-label" id="ind_preparar_oficioError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_preparar_oficio)}{$formDB.ind_preparar_oficio}{/if}" name="form[alphaNum][ind_preparar_oficio]" 
				  id="ind_preparar_oficio">
                        <label for="ind_preparar_oficio"><i class="md md-description"></i>Preparar oficio a:</label>
					 </div>	
					  </div>	
					 
					 
				   <div class="col-sm-3">	
				  	<div class="form-group floating-label" id="ind_tramitar_enError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_tramitar_en)}{$formDB.ind_tramitar_en}{/if}" name="form[alphaNum][ind_tramitar_en]" 
				  id="ind_tramitar_en">
                        <label for="ind_tramitar_en"><i class="md md-description"></i>Tramitar en:  (D&iacute;as)</label>
					 </div>	
					  </div>	
					  
						 
				 				  
				  	<div class="col-lg-10">
				  <div class="form-group floating-label" id="ind_observacionError">
                  <input type="text" class="form-control" value="{if isset($formDB.ind_observacion)}{$formDB.ind_observacion}{/if}" name="form[alphaNum][ind_observacion]" 
				  id="ind_observacion">
                        <label for="ind_observacion"><i class="md md-description"></i>Obrsevaciones</label>
					 </div>	
			</div>					
 
				<div class="col-sm-6">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
            	</div>
			
			<div class="col-sm-6">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
			  </div>
               
				  </div>
				</div>
		
          
        </div>

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css("width","75%");
        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('idCorrespondencia'+dato['idCorrespondencia'])).html('<td>'+dato['ind_observacion']+'</td>' +
                            '<td>'+dato['ind_coordinar_con']+'</td>' +
                            '<td>'+dato['ind_preparar_oficio']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-04-02-M',$_Parametros.perfil)}'+
                            '<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCorrespondencia="'+dato['idCorrespondencia']+'"' +
                            'descipcion="El Usuario a Modificado una correspondencia" titulo="Modificar Proceso">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('CD-01-03-01-03-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCorrespondencia="'+dato['idCorrespondencia']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado una correspondencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la  correspondencia!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El  maestro de correspondencia fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='nuevo'){
                    //if(dato['num_flag_adelanto']==1){ var iconoFlag='md md-check'  }else{ var iconoFlag='md md-not-interested' }
                    if(dato['ind_estatus']==1){ var icono='md md-check'  }else{ var icono='md md-not-interested' }
                    $(document.getElementById('datatable1')).append('<tr  id="idCorrespondencia'+dato['idCorrespondencia']+'">' +
                            '<td>'+dato['ind_observacion']+'</td>' +
                            '<td>'+dato['ind_coordinar_con']+'</td>' +
                            '<td>'+dato['ind_preparar_oficio']+'</td>' +
                            '<td><i class="'+icono+'"></i></td>' +

                            '<td  align="center">' +
                            '{if in_array('CD-01-04-02-M',$_Parametros.perfil)}<button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"' +
                            'data-keyboard="false" data-backdrop="static" idCorrespondencia="'+dato['idCorrespondencia']+'"' +
                            'descipcion="El Usuario a Modificado un Proceso de Nomina" titulo="Modificar Proceso">' +
                            '<i class="fa fa-edit" style="color: #ffffff;"></i></button>{/if}&nbsp;&nbsp;' +

                            '{if in_array('CD-01-03-01-03-03-E',$_Parametros.perfil)}'+
                            '<button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCorrespondencia="'+dato['idCorrespondencia']+'"  boton="si, Eliminar"'+
                            'descipcion="El usuario a eliminado un Proceso" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Proceso!!">'+
                            '<i class="md md-delete" style="color: #ffffff;"></i>'+
                            '</button>{/if}'+
                            '</td>');
                    swal("Registro Modificado!", "El Proceso fue modificado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });
</script>