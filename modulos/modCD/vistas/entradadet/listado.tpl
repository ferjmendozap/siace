<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Correspondencia - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Observacion</th>
                                <th>Coordinar Con</th>
                                <th>Preparar Oficio</th>
                                <th>Estatus</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=correspondencia from=$listado}
                                <tr id="idCorrespondencia{$correspondencia.pk_documento_detalle}">
                                    <td><label>{$correspondencia.ind_observacion}</label></td>
                                    <td><label>{$correspondencia.ind_coordinar_con}</label></td>
                                    <td><label>{$correspondencia.ind_preparar_oficio}</label></td>
                                    <td>
                                        <i class="{if $correspondencia.ind_estado==RE}md md-check{else}md md-not-interested{/if}"></i>
                                    </td>
                                    <td align="center">
                                        {if in_array('CD-01-04-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idCorrespondencia="{$correspondencia.pk_documento_detalle}"
                                                    descipcion="El Usuario a Modificado un Proceso de correspondencia" titulo="Modificar correspondencia">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('CD-01-03-01-03-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idCorrespondencia="{$correspondencia.pk_documento_detalle}"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un  maestro de correspondencia" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar un maestro de correspondencia!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="5">
                                    {if in_array('CD-01-04-01-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                                descipcion="el Usuario a creado un Nuevo Proceso de correspondencia"  titulo="<i class='icm icm-cog3'></i> Crear correspondencia" id="nuevo" >
                                            Nuevo Registro &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="md md-create"></i>
                                        </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modCD/entradadetCONTROL/crearModificarMET';
         //var $url='{$_Parametros.url}modCD/pruebaCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCorrespondencia:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idCorrespondencia: $(this).attr('idCorrespondencia')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idCorrespondencia=$(this).attr('idCorrespondencia');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCD/entradadetCONTROL/eliminarMET';
               // var $url='{$_Parametros.url}modCD/pruebaCONTROL/eliminarMET';
                $.post($url, { idCorrespondencia: idCorrespondencia },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idCorrespondencia'+dato['idCorrespondencia'])).html('');
                        swal("Eliminado!", "La correspondencia fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>