<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Documentos Recibidos Externos</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
							<th width="10">Nro</th>
                                <th width="100">Nro de Registro</th>
                                <th width="100">Nro de Documento</th>
								<th  width="100">Tipo Documento</th>

								<th  width="200">Dependencia</th>
                                <th  width="150">Asunto</th>
								<th  width="60">Estado</th>
								 <th width="100"> Acuse</th>
								
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listado}
                                <tr id="idDocumento{$documento.pk_num_entrada}">
								<td><label>{$documento.pk_num_entrada}</label></td>
                                    <td><label>{$documento.num_secuencia}</label></td>
                                    <td><label>{$documento.num_documento}</label></td>
									<td><label>{$documento.ind_descripcion}</label></td>

                                    <td><label>{$documento.ind_dependencia}</label></td>
									<td><label>{$documento.text_asunto}</label></td>
									 <td>
									 <label>{if $documento.ind_estado=='EV'}Enviado{/if}</label>
									 
									 </td>
									 
									 <td align="left">
										{if $documento.ind_estado=='EV' }
									
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 {if in_array('CD-01-01-02-04-02-D',$_Parametros.perfil)}
                                      <button class="acusext logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idDocumento="{$documento.pk_num_entrada}"  boton="si, Acuse"
                                                    descipcion="El usuario a eliminado un Documento" titulo="Estas Seguro?" mensaje="Estas seguro que desea dar Acuse a un Documento Externo!!">
                                                <i class="md md-assignment-turned-in" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
									 {/if}
									 </td>
									 
								  </tr>
                            {/foreach}
                        </tbody>
                       
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript"> 
    $(document).ready(function() {

		$('#datatable1 tbody').on( 'click', '.acusext', function () {
            var idDocumento=$(this).attr('idDocumento');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                
                var $url='{$_Parametros.url}modCD/documentrecibidosExtCONTROL/AcuseExtMET';
                $.post($url, { idDocumento: idDocumento },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idDocumento'+dato['idDocumento'])).html('');
                        swal("Recibido!", "Acuse satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
		
		$('#datatable1 tbody').on( 'click', '.documento', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/enviardocumentextCONTROL/documentoMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
		
       
    });
</script>