<section class="style-default-bright">
<form action="{$_Parametros.url}modCD/preparardocumenintCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post" enctype="multipart/form-data">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}
        
				    <!-- Nav tabs -->
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#Datos" aria-controls="home" role="tab" data-toggle="tab"><font color="#0066FF">Datos Generales</font></a></li>
                <li role="presentation"><a href="#Detalle" aria-controls="profile" role="tab" data-toggle="tab"><font color="#0066FF">Contenido del Documento</font></a></li>
             </ul>
			 
			   <!-- Tab panes -->
            <div class="tab-content">
			 <div role="tabpanel" class="tab-pane active" id="Datos">
			   <div class="row">
			   <div class="col-sm-4">
                     <div class="form-group floating-label" id="fk_cdc003_num_tipo_documentoError">
                        <select  id="fk_cdc003_num_tipo_documento" disabled name="form[alphaNum][fk_cdc003_num_tipo_documento]" class="form-control" >
                            
                            {foreach item=app from=$correspondencia}
                                {if isset($formDB.fk_cdc003_num_tipo_documento)}
                                    {if $app.pk_num_tipo_documento==$formDB.fk_cdc003_num_tipo_documento}
                                        <option selected value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                        {else}
                                        <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_tipo_documento}">{$app.ind_descripcion}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_cdc003_num_tipo_documento"><i class="md md-description"></i>&nbsp;Tipo de Documento</label>
                    </div>
   				</div>
				
				<div class="col-sm-3">
                    <div class="form-group floating-label" id="fec_documentoError">
                              <input type="text" disabled class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}" name="form[alphaNum][fec_documento]" >
					          <label for="fec_documento"><i class="md md-event"></i>&nbsp;Fecha Documento</label>
                       </div>
                </div>
				
					
					<div class="col-sm-3">
                    <div class="form-group floating-label" id="ind_documento_completoError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_documento_completo)}{$formDB.ind_documento_completo}{/if}" name="form[alphaNum][ind_documento_completo]" id="ind_documento_completo">
                        <label for="ind_documento_completo"><i class="md md-assignment"></i>&nbsp;N de Documento</label>
                    </div>
                </div>
				
				<div class="col-lg-2">
					<div class="form-group floating-label" id="ind_media_firmaError">
                  <input type="text"  class="form-control" value="{if isset($formDB.ind_media_firma)}{$formDB.ind_media_firma}{/if}" name="form[formula][ind_media_firma]" id="ind_media_firma"  maxlength="5">
                        <label for="ind_media_firma"><i class="md md-description"></i>&nbsp;Iniciales</label>
                    </div>
				</div>	
 
 				<div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_asuntoError">
                        <input type="text" disabled class="form-control" value="{if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}" name="form[alphaNum][ind_asunto]" id="ind_asunto">
                        <label for="ind_asunto"><i class="md md-question-answer"></i>&nbsp;Asunto</label>
                    </div>
				</div>
				
				
				

	<div class="col-lg-6">
			 <div class="form-group floating-label">
                        <select  id="fk_a001_num_organismo" disabled  name="form[alphaNum][fk_a001_num_organismo]" class="form-control" >
                            
                            {foreach item=app from=$organismoint}
                                {if isset($formDB.fk_a001_num_organismo)}
                                    {if $app.pk_num_organismo==$formDB.fk_a001_num_organismo}
                                        <option selected value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                        {else}
                                        <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                    {/if}
                                    {else}
                                    <option value="{$app.pk_num_organismo}">{$app.ind_descripcion_empresa}</option>
                                {/if}
                            {/foreach}                        
						</select>
                        <label for="fk_a001_num_organismo"><i class="md md-event"></i>&nbsp;Organismo Remitente</label>
                    </div>
					</div> 
					
								 <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="docProveedor"><i class="md md-contacts"></i>
                                             Cargo Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_cargo_remitente)}{$formDB.ind_cargo_remitente}{/if}"
                                                   id="docProveedor" disabled readonly>
                                        </div>
                                    </div>
									
									 <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="nombreProveedor"><i class="md md-group"></i>
                                              Representante</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_persona_remitente)}{$formDB.ind_persona_remitente}{/if}"
                                                   id="nombreProveedor" disabled readonly>
                                        </div>
                                    </div>
														
              						 <div class="col-sm-6">
                                        <div class="form-group" id="ind_dependenciaError">
                                            <label for="dependencia"><i class="icm icm-calculate2"></i>
                                                Dependencia Remitente</label>
                                            <input type="text" class="form-control"
                                                   value="{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}"
                                                   id="dependencia" disabled readonly>

                                        </div>
                                    </div>
	
				<div class="col-sm-12">
					<div class="col-sm-6">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
            	</div>
			
			<div class="col-sm-6">
                    <div class="form-group floating-label">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_proceso"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
			  		</div>
               </div>
				 </div>
				 
						  
						  </div>
					  </div>
					   
					 <!-----------DETALLE DOCUMENTO---------------------------------->		 
    <div role="tabpanel" class="tab-pane" id="Detalle">
			   
			      <div>
			<div class="col-lg-12">
						<div class="card">
						<div class="form-group" id="txt_contenidoError">
							<div class="form-group" >
							<textarea id="ckeditor"  name="txt_contenido"  class="form-control control-12-rows" style="visibility: hidden; display: none;" data-msg-required="Introduzca el cuerpo de la Noticia" aria-required="true" aria-invalid="true" required>
							{if isset($formDB.txt_contenido)}{$formDB.txt_contenido}{/if}
							</textarea>							
						</div>
						</div>		
						</div><!--end .card -->
						
						 <div class="col-sm-4" align="left">
                              <div class="form-group floating-label" id="ind_ruta_docError">
								 <div class="row">
                                            <img src="{$_Parametros.ruta_Img}modCD/pdf-icon.jpg" alt="Cargar PDF" title="Cargar PDF" id="cargarPdf" style="cursor: pointer;" width="240" height="50"/>
                                        </div> 
                                        <div class="row">
                                            <span id="cargarPdf_descripcion_pdf"></span>
                                        </div>
                                    <input type="file" name="ind_ruta_pdf" id="ind_ruta_pdf" style="display: none" />
									<input type="hidden" name="ind_ruta_doc" id="ind_ruta_doc"  value="{if isset($formDB.ind_ruta_doc)}{$formDB.ind_ruta_doc}{/if}">

                                </div>

                            </div>
							
					</div>
					
					

			  
                </div>
       </div>
    <div class="modal-footer">
        <button type="button"  title="Guardar"  class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span> &nbsp;Guardar</button>
    </div>
</form>
</div>
<script type="text/javascript">

     $(document).ready(function() {


				
  $('#cargarPdf_descripcion_pdf').html('{$formDB.ind_ruta_doc}');
	//*********************************************
	//*			CARGAR PDF
	//*********************************************
	$("#cargarPdf").click(function() { //la imagen de carga de pdf

        $("#ind_ruta_pdf").click();//ejecuto un clic al input file oculto
    });
	
	$("#ind_ruta_pdf").change(function(e) {
				
		var files = e.target.files;
        var file = $("#ind_ruta_pdf")[0].files[0]; //Obtenemos el primer imputfile (unico que hay en este caso)
        var fileName = file.name;

        var aux_e = fileName.split('.');
        var aux_e2 = 0;
        if ( aux_e.length > 2 || fileName.indexOf(',') > -1 || fileName.indexOf('"') > -1 || fileName.indexOf("'") > -1 || fileName.indexOf('(') > -1 || fileName.indexOf(')') > -1 || fileName.indexOf('}') > -1 )
        {
            aux_e2 = 1;
        }

        if(aux_e2 == 0)
        {
            fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
            if (fileExtension == 'docx' || fileExtension == 'DOCX' || fileExtension == 'odt' || fileExtension == 'ODT' ||  fileExtension == 'doc' || fileExtension == 'DOC' ||
                fileExtension == 'ods' || fileExtension == 'ODS' || fileExtension == 'pdf' || fileExtension == 'PDF' || fileExtension == 'zip'
                || fileExtension == 'ZIP' || fileExtension == 'RAR' || fileExtension == 'rar') {
                $("#cargarPdf").attr("src", "{$_Parametros.ruta_Img}modCD/ico-pdf-ok.jpg");
                $("#cargarPdf_descripcion_pdf").html(fileName);
                $("#ind_ruta_doc").val(fileName);

            } else {

                swal("Extension de archivo no valido", "Asegurese de subir un archivo de tipo: .doc, .docx, .odt, .ods, .pdf, .rar, .zip", "error");
                $("#cargarPdf").attr("src", "{$_Parametros.ruta_Img}modCD/pdf-icon.jpg");
                $("#cargarPdf_descripcion_pdf").html('');
                $("#ind_ruta_doc").val('');//es el que se va a validar
            }
        }
        else
        {
            swal("Nombre de archivo no valido", "Renombre el archivo, sin caracteres especiales", "error");
            $("#cargarPdf").attr("src", "{$_Parametros.ruta_Img}modCD/pdf-icon.jpg");
            $("#cargarPdf_descripcion_pdf").html('');
            $("#ind_ruta_doc").val('');//es el que se va a validar
        }


    });

        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
	
	
		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
			//modales..
        $('#agregarParticular').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

		  $('#tipoparticular').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
	
		$('#fec_registro').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
		$('#fec_documento').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd" });
	    $('#modalAncho').css("width","75%");
        $('#accion').click(function(){
		 swal({
            title: "�Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
			
			var form = $("#formAjax");
    		var formData = new FormData(document.getElementById(form.attr('id')));
			var editor = CKEDITOR.instances.ckeditor;
       		var txt_contenido = editor.getData();
			
			  var ckeditor = $("#ckeditor").val();
                if(ckeditor == "") {
                    swal("El Documento es Obligatorio", "Por favor redarctar el contenido", "error");
					//return false;
                }
                else if(ckeditor != "") 
                {
		
			    formData.append('ckeditor',txt_contenido);
				
 			
    		$.ajax({
		        url: $("#formAjax").attr("action"),
		        type: "POST",
		        dataType: "json",
		        data: formData,
		        cache: false,
		        contentType: false,
		        processData: false
		    })
		        .done(function(dato){
				
        //    $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='modificar'){
                   
                    swal("Contenido Preparado!", "El Documento fue preparado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
					$('#prueba').html('');
					$('#_contenido').load('{$_Parametros.url}modCD/preparardocumenintCONTROL');
                }
          //  },'json');
		  
		   });
		    }
        });
    });
</script>
