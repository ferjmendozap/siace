<form action="{$_Parametros.url}modCD/documentinternosCONTROL/ListaDocumentosMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}

<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Documentos Internos |  Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                   <th width="60">Nro de Documento</th>
								  <th  width="70">Tipo Documento</th>
                                <th  width="170">Remitente</th>
								<th  width="110">Asunto</th>
								 <th  width="70">Estado</th>
                              <th align="left" width="210">Modificar -  Ver -   Anular -  Modif. Rest- Imprimir</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=documento from=$listamia}
                                <tr id="idDocumento{$documento.fk_cdb001_num_documento}">
                                    <td><label>{$documento.ind_documento_completo}</label></td>
									 <td><label>{$documento.tipoDocumento} </label></td>
                                    <td><label>{$documento.ind_dependencia}</label></td>
									<td><label>{$documento.ind_asunto}</label></td>
									
									 <td>
									 <label>{if $documento.ind_estado=='AN'}Anulado{else if $documento.ind_estado=='PR'}Preparacion{else if $documento.ind_estado=='EV'}Enviado{else if $documento.ind_estado=='PP'}Preparado{else if     

									 $documento.ind_estado=='RE'}Recibido{/if}</label>
									 
									 </td>
                                  
                                     <td align="left">
										 
										  {if in_array('CD-01-01-02-01-07-I',$_Parametros.perfil)}
                                            <button class="memorandum logsUsuario btn ink-reaction btn-raised btn-xs btn-default"  data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idDocumento="{$documento.pk_num_documento}"
                                                    descipcion="El Usuario a Modificado un Documento Externo" titulo="<i class='icm icm-cog3'></i>&nbsp;Editor de Documentos Salidas">
                                                <i class="md md-local-print-shop" style="color: #0000FF;"></i>
                                            </button>
                                        {/if} 
										 
										
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                                </table>
                </div>
            </div>
        </div>
    </div>
</section>

</form>

<script type="text/javascript">
    $(document).ready(function() {
       
	   
         var $url='{$_Parametros.url}modCD/documentinternosCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idDocumento:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

	
		 $('#datatable1 tbody').on( 'click', '.memorandum', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/documentinternosCONTROL/memorandumMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
		
		//modales..
       	  $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
		
		 $('#datatable1 tbody').on( 'click', '.credencial', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/documentinternosCONTROL/credencialMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
		
		 $('#datatable1 tbody').on( 'click', '.puntocuenta', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post('{$_Parametros.url}modCD/documentinternosCONTROL/puntocuentaMET',{ idDocumento: $(this).attr('idDocumento') },function(dato){
                    $('#ContenidoModal').html(dato);
                });
        });
	    
    });
	
</script>