<form action="{$_Parametros.url}modCD/documentinternosCONTROL/memorandumMET"  autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idDocumento) }
            <input type="hidden" value="{$idDocumento}" name="idDocumento"/>
        {/if}

<table id="Padre" name="Padre" align="center" cellpadding="0" cellspacing="0" width="770">
<tr>
<td>

		{if $formDB.fk_cdc003_num_tipo_documento=='1'}
<div id="imprimeme">
<table id="principal"  align="center" border="0">
<tr><td width="887">
  <!-- *********************** -->
  <table align="center">
  <tr><td>
  <!-- CABECERA DEL DOCUMENTO -->
  <table width="679" align="right" id="cabecera" cellpadding="0" cellspacing="0">
  <tr>
   <td width="3"></td>
   <td width="124" align="center"><img src="{$_Parametros.url}publico/imagenes/modCD/logo.png" style="height:70px; width:80px" ></td>
   <td width="10"></td>
   <td width="420">
   <!-- *********************** -->
   <table cellpadding="0" cellspacing="0">
   <tr>
      <td align="center" width="414"><font size="3" face="Arial">REPUBLICA BOLIVARIANA DE VENEZUELA</font></td>
   </tr>
   <tr>
      <td align="center"><font size="3" face="Arial">CONTRALORIA DEL ESTADO BOLIVAR</font></td>
   </tr>
   <tr>
      <td align="center"><font size="3" face="Arial">{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}</font></td>
    </tr>
   </table>
   <!-- *********************** -->
   </td>
   <td width="120" align="center"><img src="{$_Parametros.url}publico/imagenes/modCD/Logo SNCF.jpg" style="height:80px; width:80px" ></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  </table>
  <!-- FIN CABECERA DEL DOCUMENTO -->
  </td></tr>
  </table>
  <!-- *********************** -->
  
</td></tr>
<tr><td>
   <!-- *********************** -->
   <table width="688" height="27" id="numero_doc"> 
   <tr>
    <td width="26" height="21"></td>
    <td width="26"></td>
    <td width="26"></td>
    <td width="436"></td>
    <td width="150"><font face="Arial" size="3"><b>Nro: &nbsp; {if isset($formDB.ind_documento_completo)}{$formDB.ind_documento_completo}{/if}</b></font></td>
   </tr>
   </table>
   <!-- *********************** -->
</td></tr>

<tr><td>
   <!-- *********************** -->
   <table id="titulo">
   <tr>
    <td width="0"></td>
    <td width="0"></td>
    <td width="268"></td>
    <td width="148" align="center"><font size="3" face="Arial"><b>MEMORANDUM</b></font></td>
    <td width="0"></td>
  </tr>
  <tr><td height="5"></td></tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CUERPO 1 DEL DOCUMENTO -->
  <table id="cuerpo1" cellpadding="0" cellspacing="0" width="100%" border="0">
  <tr>
    <td width="79"><font face="Arial" size="3"><b>PARA:</b></font></td>
    <td width="563"><font face="Arial" size="3">{if isset($formDB.nombre_apellidos)}{$formDB.nombre_apellidos}{/if}</font></td>
  </tr>
  <tr>
    <td width="79"></td>
    <td width="563"><font face="Arial" size="3">{if isset($formDB.cargodepen)}{$formDB.cargodepen}{/if}</font></td>
  </tr>
   
  <tr>
    <td><font face="Arial" size="3"><b>DE:</b></font></td>
    <td><font face="Arial" size="3">{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}</font></td>
  </tr>
   
  <tr>
    <td><font face="Arial" size="3"><b>FECHA:</b></font></td>
	
    <td><font face="Arial" size="3">{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}</font></td>
  </tr>
  
  <tr>
    <td><font face="Arial" size="3"><b>ASUNTO:</b></font></td>
    <td><font face="Arial" size="3">{if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}</font></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td align="justify" width="887" height="300"><font face="Arial" size="3">{if isset($formDB.txt_contenido)}{$formDB.txt_contenido}{/if}</font>
  <!-- CONTENIDO DEL DOCUMENTO -->
  <br />
  <br /><br /><br /><br /><br /><br /><br />
  </td>
  <!-- *********************** -->
<tr><td height="300">
  <!-- *********************** -->
  <table align="center" id="atentamente" width="500">
  <tr><br /><br /><br /><br /><br />
    <td width="58"></td>
    <td align="center"><font face="Arial" size="3">Atentamente,</font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td height="25"></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center"></td>
    <td width="61"></td>
  </tr>
  <tr>

    <td width="58"></td>
    <td align="center" width="365"><font face="Arial" size="3">{if isset($formDB.nombre_apellidos)}{$formDB.nombre_apellidos}{/if}</font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center" width="600"><font face="Arial" size="3">{if isset($formDB.ind_nombre_cargo)}{$formDB.ind_nombre_cargo}{/if}</font></td>
    <td width="61"></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td align="left"><font face="Arial" size="2">{if isset($formDB.ind_media_firma)}{$formDB.ind_media_firma}{/if}</font>
  
</td></tr>
<!--<center><input type="button" name="imprimir" value="Imprimir" onclick="window.print();"></center>-->
</table>
  
<div align="center"><button onclick="imprimir();">  IMPRIMIR</button></div> 
		  
    </div>

      {/if}	

   
{if $formDB.fk_cdc003_num_tipo_documento=='3'}
<div id="imprimeme">
<table align="center" width="700" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="119" align="center"><img src="{$_Parametros.url}publico/imagenes/modCD/logo.png" style="height:70px; width:80px"></td>
    <td width="397">
    <table width="395" height="101" border="1" align="center">
    <tr>
       <td width="402" height="35" align="center"><font size="2" face="Arial, Helvetica, sans-serif"><b>CONTRALORIA DEL ESTADO DELTA AMACURO</b></font></td>
    </tr>
    <tr>
       <td align="center" height="25"><font size="2" face="Arial, Helvetica, sans-serif"><b>PUNTO DE CUENTA</b></font></td>
    </tr>   
    </table>
    </td>
    <td width="176">
    <table width="175" height="100" border="1" align="center" cellpadding="0" cellspacing="0">
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>CODIGO:</b></font></td>
    </tr>
    <tr>
       <td align="center" colspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>FOR-CEDA-001</b></font></td>
    </tr>
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>REVISION</b></font></td>
    </tr>
    <tr>
       <td width="87" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>N�:</b></font></td>
       <td width="82" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA</b></font></td>
    </tr>
     <tr>
       <td height="15" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>0</b></font></td>
       <td height="15" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>05/2010</b></font></td>
    </tr>
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>PAGINA</b></font></td>
    </tr>
    <tr>
       <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>1 DE 1</b></font></td>
    </tr>
    </table>
    </td>
  </tr>
  </table>
  </td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- TABLA DATOS GENERALES ---------- /////////// --->
<table align="center" width="700" border="1">
<tr>
  <td width="75" rowspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>PRESENTADO:</b></font></td>
  <td width="27"><font size="1" face="Arial, Helvetica, sans-serif"><b>A:</b></font></td>
  <td width="192"><font size="1" face="Arial, Helvetica, sans-serif"><b>{if isset($formDB.cargodepen)}{$formDB.cargodepen}{/if}</b></font></td>
  <td width="378"><font size="1" face="Arial, Helvetica, sans-serif"><b>NUMERO: {if isset($formDB.ind_documento_completo)}{$formDB.ind_documento_completo}{/if}</b></font></td>
</tr>
<tr>

  <td><font size="1" face="Arial, Helvetica, sans-serif"><b>POR:</b></font></td>
  <td><font size="1" face="Arial, Helvetica, sans-serif"><b>{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}</b></font></td>
  <td><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA:  {if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}</b></font></td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- ASUNTO ---------- /////////// --->
<table id="asunto" align="center" width="700" border="1">
<tr>
  <td width="85" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>ASUNTO:</b></font></td>
  <td width="599"><font size="1" face="Arial, Helvetica, sans-serif"><b> {if isset($formDB.ind_asunto)}{$formDB.ind_asunto}{/if}</b></font></td>
</tr>
<tr>
  <td colspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>PROPUESTA:</b></font></td>
</tr>
<tr>
 <td height="300" colspan="2" align="center"><div align="center" style="width:690px;height:300px; text-align:justify"><font face="Arial" size="2">
 {if isset($formDB.txt_contenido)}{$formDB.txt_contenido}{/if}
  <!-- CONTENIDO DEL DOCUMENTO -->
 </font></div>
 <!--<textarea cols="83" rows="17" id="instrucciones"><?=$fa['Contenido']?></textarea>--></td>
</tr>
</table>
<br />
<!-- ////////// ---------- RESULTADO ---------- /////////// --->
<table id="resultado" align="center" width="700" cellpadding="0" cellspacing="0" border="1">
<tr>
   <td colspan="4" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>RESULTADO:</b></font></td>
</tr>
<tr>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>APROBADO</b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>NEGADO</b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>DIFERIDO</b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>VISTO</b></font></td>
</tr>
<tr>
 <td align="center" height="15"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
<tr>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
 <td><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
<tr>
 <td colspan="4" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>INSTRUCCIONES U OBSERVACIONES</b></font></td>
</tr>
<tr>
 <td colspan="4" height="150"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- FIRMAS ---------- /////////// --->
<table align="center" id="firmas" cellpadding="0" cellspacing="0" width="700" border="1">
<tr>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>PREPARADO POR:</b></font></td>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>SELLO</b></font></td>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>CONFORMADO POR:</b></font></td>
   <td width="175"><font size="1" face="Arial, Helvetica, sans-serif"><b>SELLO</b></font></td>
</tr>
<tr>
   <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>FIRMA:</b></font></td>
   <td rowspan="3"></td>
   <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>FIRMA:</b></font></td>
   <td rowspan="3"></td>
</tr>
<tr>
   <td height="25"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
   <td height="10"><font size="1" face="Arial, Helvetica, sans-serif"><b></b></font></td>
</tr>
<tr>
   <td height="20"><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA:  {if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}</b></font></td>
   <td><font size="1" face="Arial, Helvetica, sans-serif"><b>FECHA:</b></font></td>
</tr>
</table>
<table width="700"><tr><td height="5"></td></tr></table>
<!-- ////////// ---------- ANEXOS ---------- /////////// --->
<table align="center" id="anexos" width="700" border="1">
<tr>

   <td width="44" rowspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>ANEXOS:</b></font></td>
   <td width="35" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>SI</b></font></td>
   <td width="26" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>{if $formDB.ind_anexo=='1'} X {else if $formDB.ind_anexo=='0'}{/if}</b></font></td>
    <td width="567" rowspan="2"><font size="1" face="Arial, Helvetica, sans-serif"><b>LISTA DE ANEXOS:{if isset($formDB.txt_descripcion_anexo)}{$formDB.txt_descripcion_anexo}{/if}</b></font></td>
</tr>
<tr>
   <td align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>NO</b></font></td>
   <td rowspan="2" align="center"><font size="1" face="Arial, Helvetica, sans-serif"><b>{if $formDB.ind_anexo=='0'} X {else if $formDB.ind_anexo=='1'}{/if}</b></font></td>
</tr>
</table>
</div>
<div align="center"><button onclick="imprimir();">  IMPRIMIR</button></div> 

      {/if}	

{if $formDB.fk_cdc003_num_tipo_documento=='5'}
			<div id="imprimeme">

<table id="principal"  align="center"  width="770">
<tr>
<td>
  <!-- *********************** -->
  <table align="center" width="100%">
  <tr><td>
  <!-- CABECERA DEL DOCUMENTO -->
  <table width="100%" align="center" id="cabecera" cellpadding="0" cellspacing="0">
  <tr>
   <td width="3"></td>
   <td width="124" align="center"><img src="{$_Parametros.url}publico/imagenes/modCD/logo.png" style="height:70px; width:80px" ></td>
   <td width="10"></td>
   <td width="420">
   <!-- *********************** -->
   <table align="center" cellpadding="0" cellspacing="0" width="100%">
   <tr>
      <td align="center"><font size="3" face="Arial">REPUBLICA BOLIVARIANA DE VENEZUELA</font></td>
   </tr>
   <tr>
      <td align="center"><font size="3" face="Arial">CONTRALORIA DEL ESTADO BOLIVAR</font></td>
   </tr>
  <tr>
      <td align="center"><font size="3" face="Arial">{if isset($formDB.ind_dependencia)}{$formDB.ind_dependencia}{/if}</font></td>
    </tr>
   </table>
   <!-- *********************** -->
   </td>
   <td width="120" align="center"><img src="{$_Parametros.url}publico/imagenes/modCD/Logo SNCF.jpg" style="height:80px; width:80px" ></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  </table>
  <!-- FIN CABECERA DEL DOCUMENTO -->
  </td></tr>
  </table>
  <!-- *********************** -->
  
</td></tr>
<tr><td>
   <!-- *********************** -->
   <table width="688" height="27" id="numero_doc"> 
   <tr>
     <td height="20"></td>
   </tr>
   <tr>
    <td width="30"></td>
    <td width="250" height="21"><font face="Arial" size="3"><b>Nro: &nbsp; {if isset($formDB.ind_documento_completo)}{$formDB.ind_documento_completo}{/if}</b></font></td>
    <td width="26"></td>
    <td width="26"></td>
    <td width="436"></td>
    <td width="26"></td>
   </tr>
   </table>
   <!-- *********************** -->
</td></tr>

<tr><td>
   <!-- *********************** -->
   <table id="titulo" width="100%">
   <tr>
    <td align="center"><font size="3" face="Arial">CREDENCIAL</b></font></td>
    </tr>
  </table>
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CUERPO 1 DEL DOCUMENTO -->
  
  <!-- *********************** -->
</td></tr>

<tr><td>
  <!-- CONTENIDO DEL DOCUMENTO -->
  <table width="100%">
  <tr>
   <td height="300"><div align="center" style="width:100%;height:300px; text-align:justify"><font face="Arial" size="2">{if isset($formDB.txt_contenido)}{$formDB.txt_contenido}{/if}</font></div></td>
    <td width="11"></td>
  </tr>
  </table>
  <!-- *********************** -->
<tr><td>
  <!-- *********************** -->
  
  
  <tr><td>
  <!-- *********************** -->
  <table align="center" id="atentamente" width="500">
  <tr>
    <td width="58"></td>
    <td align="center"><font face="Arial" size="3"></font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td height="25"></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center"></td>
    <td width="61"></td>
  </tr>
  <tr>

    <td width="58"></td>
    <td align="center" width="365"><font face="Arial" size="2">{if isset($formDB.nombre_apellidos)}{$formDB.nombre_apellidos}{/if}</font></td>
    <td width="61"></td>
  </tr>
  <tr>
    <td width="58"></td>
    <td align="center" width="600"><font face="Arial" size="2">{if isset($formDB.ind_descripcion_cargo)}{$formDB.ind_descripcion_cargo}{/if}</font></td>
    <td width="61"></td>
  </tr>
 
  <!--<tr>
    <td width="58"></td>
    <td align="center" width="600"><font size="1"><?=$f_nivelacion2['Documento'];?></font></td>
    <td width="61"></td>
  </tr>-->
  </table>
  <!-- *********************** -->
</td></tr>

  <tr><td>
  <!-- *********************** -->

 
 
</td></tr>

<tr><td>
  <!-- *********************** -->
  <table width="686" id="pie_pagina">
  <tr>
     <td width="26"></td>
     <td width="70"><font face="Arial" size="2">{if isset($formDB.ind_media_firma)}{$formDB.ind_media_firma}{/if}</font></td>
     <td width="421"></td>
     <td width="149" align="right"></td>
  </tr>
  </table>
  <!-- *********************** -->
</td></tr>
<!--<center><input type="button" name="imprimir" value="Imprimir" onclick="window.print();"></center>-->
</table>

  
			
					<div align="center">
<button onclick="imprimir();">  IMPRIMIR</button></div> 
		  
                </div>
      {/if}
   </td></tr>
<!--<center><input type="button" name="imprimir" value="Imprimir" onclick="window.print();"></center>-->
</table>   
</form>

<script type="text/javascript">


function imprimir(){
  var objeto=document.getElementById('imprimeme');  //obtenemos el objeto a imprimir
  var ventana=window.open('','_blank');  //abrimos una ventana vac�a nueva
  ventana.document.write(objeto.innerHTML);  //imprimimos el HTML del objeto en la nueva ventana
  ventana.document.close();  //cerramos el documento
  ventana.print();  //imprimimos la ventana
  ventana.close();  //cerramos la ventana
}

   </script>
