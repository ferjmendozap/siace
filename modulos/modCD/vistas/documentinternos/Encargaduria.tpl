<div class="row">
    <div class="col-lg-12 contain-lg">
        <div class="table-responsive">
		 <div class="form-group col-sm-12">
            <table id="datatable2" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Cargo</th>

                </tr>
                </thead>
                <tbody>
                {foreach item=i from=$lista}
                    <tr>
					
					   <input type="hidden" value="{$i.pk_num_puestos}" class="persona"
                         nombre="{$i.pk_num_puestos} "
                         documento="{$i.ind_descripcion_cargo}">
					
					
                        <td>{$i.pk_num_puestos} </td>
                        <td>{$i.ind_descripcion_cargo}</td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                </tfoot>
            </table>
			</div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
		 $('#datatable2').on('click', 'tbody tr', function () {
      //  $('#datatable2 tbody tr').click(function() {
            var input = $(this).find('input');
            {if $tipoEncargaduria == 'Encargaduria'}
 			
				 $(document.getElementById('docEncargaduria')).val(input.attr('documento'));
                $(document.getElementById('cargo')).val(input.attr('nombre'));
           
				
				
				
                $('#cerrarModal2').click();
            {/if}
        });
    });
</script>