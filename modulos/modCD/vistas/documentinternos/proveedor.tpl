<div class="row">
    <div class="col-lg-12 contain-lg">
        <div class="table-responsive">
		 <div class="form-group col-sm-12">
            <table id="datatable2" class="table table-striped table-hover">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Cargo</th>
                    <th>Dependencia</th>
                </tr>
                </thead>
                <tbody>
                {foreach item=i from=$lista}
                    <tr>
						 
						 	  <input type="hidden" value="{$i.pk_num_dependencia}" class="persona"
                         nombre="{$i.nombre_apellidos}"
                         documento="{$i.ind_descripcion_cargo}"  dependencia="{$i.ind_dependencia}" dep="{$i.pk_num_dependencia}" per="{$i.pk_num_persona}"  car="{$i.pk_num_puestos} " codint="{$i.ind_codinterno}"> 
						 
				
                       <td>{$i.nombre_apellidos}  </td>
                        <td>{$i.ind_descripcion_cargo}</td>
                        <td>{$i.ind_dependencia}</td>
                    </tr>
                {/foreach}
                </tbody>
                <tfoot>
                </tfoot>
            </table>
			</div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
		 $('#datatable2').on('click', 'tbody tr', function () {
      //  $('#datatable2 tbody tr').click(function() {
            var input = $(this).find('input');
          {if $tipoProveedor == 'proveedor'}
				$(document.getElementById('dependencia')).val(input.attr('dependencia'));
				  $(document.getElementById('nombreProveedor')).val(input.attr('nombre'));
                $(document.getElementById('docProveedor')).val(input.attr('documento'));
				$(document.getElementById('nombre_apellidos')).val(input.attr('nombre'));
				$(document.getElementById('ind_dependencia')).val(input.attr('dependencia'));
				$(document.getElementById('ind_descripcion_cargo')).val(input.attr('documento'));
				 $(document.getElementById('pk_num_dependencia')).val(input.attr('dep'));
				 $(document.getElementById('pk_num_persona')).val(input.attr('per'));
				 $(document.getElementById('pk_num_puestos')).val(input.attr('car'));
				 $(document.getElementById('ind_codinterno')).val(input.attr('codint'));
               
                $('#cerrarModal2').click();
            {/if}
        });
    });
</script>