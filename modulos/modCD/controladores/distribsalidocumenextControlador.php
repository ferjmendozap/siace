<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Distribucion Salida Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 
class distribsalidocumenextControlador extends Controlador
{
    private $atTipoDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDocumento=$this->metCargarModelo('distribsalidocumenext');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumento($usuario));
        $this->atVista->metRenderizar('listado');
    }
	
		public function metAcuse()
    {
     	$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
        $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
		  $txt = $this->metValidarFormArrayDatos('form', 'txt');
		 
          	   if ($alphaNum != null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $txt != null) {
                $validacion = $txt;
            } else {
               $validacion = array_merge($txt, $alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
            if  ($idDocumento!=0)
			{
                $id=$this->atTipoDocumento->metAcuseRecibo($validacion['ind_asunto'],$validacion['txt_descripcion'],$validacion['fec_registro'],$validacion['fec_documento'],
				$validacion['ind_persona_remitente'],$validacion['ind_cargo_remitente'],
				$validacion['fec_envio'],$validacion['fec_acuse'],
				$validacion['fk_cdc011_num_documento'],$validacion['fk_a003_num_persona'],
				$validacion['fk_a004_num_dependencia'],$validacion['fk_a001_num_organismo'],
				$idDocumento);
				
                $validacion['status']='acuse';
            }
 
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
               exit;
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
       }
		
		$correspon=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('correspon',$correspon);
		
        $this->atVista->metRenderizar('acuse','modales');
    }
	 
	 
    	public function metdevolucion()
    {
	
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
        $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
		  $txt = $this->metValidarFormArrayDatos('form', 'txt');
		 
          	   if ($alphaNum != null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $txt != null) {
                $validacion = $txt;
            } else {
               $validacion = array_merge($txt, $alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
            if  ($idDocumento!=0)
			{
                $id=$this->atTipoDocumento->metdevolucionDocumento($validacion['ind_asunto'],$validacion['txt_descripcion'],$validacion['fec_registro'],$validacion['fec_documento'],
				$validacion['ind_persona_remitente'],$validacion['ind_cargo_remitente'],
				$validacion['fec_envio'],$validacion['fec_devolucion'],$validacion['ind_motivo_devolucion'],
				$validacion['fk_cdc011_num_documento'],$validacion['fk_a003_num_persona'],
				$validacion['fk_a004_num_dependencia'],$validacion['fk_a001_num_organismo'],
				$idDocumento);
				
                $validacion['status']='devolucion';
            }
 
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
               exit;
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
       }
		
		$correspon=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('correspon',$correspon);
		
        $this->atVista->metRenderizar('devolucion','modales');
    }
	
 
 
 
  
}

