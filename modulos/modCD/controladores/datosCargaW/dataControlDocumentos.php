<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                     |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867297           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        13-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
trait dataControlDocumentos
{

    public function metDataCorrespondencia()
    {
        $cd_c003_tipo_correspondencia = array(
 	 	array('ind_descripcion'=>'MEMORANDUM','ind_descripcion_corta'=>'ME','ind_procedencia'=>'I','num_estatus'=>'1','fec_ultima_modificacion'=>'2015-10-15 09:16:20','fk_a018_num_seguridad_usuario'=>'1'),
  		
		array('ind_descripcion'=>'OFICIO','ind_descripcion_corta'=>'OF','ind_procedencia'=>'E','num_estatus'=>'1','fec_ultima_modificacion'=>'2015-10-15 09:16:23','fk_a018_num_seguridad_usuario'=>'1'),
  		
		array('ind_descripcion'=>'PUNTO DE CUENTA','ind_descripcion_corta'=>'PC','ind_procedencia'=>'I','num_estatus'=>'1','fec_ultima_modificacion'=>'2015-10-15 09:16:27','fk_a018_num_seguridad_usuario'=>'1'),
  		
		array('ind_descripcion'=>'OFICIO CIRCULAR','ind_descripcion_corta'=>'OC','ind_procedencia'=>'E','num_estatus'=>'1','fec_ultima_modificacion'=>'2015-10-15 09:16:29','fk_a018_num_seguridad_usuario'=>'1'),
  		
		array('ind_descripcion'=>'CREDENCIALES','ind_descripcion_corta'=>'CR','ind_procedencia'=>'I','num_estatus'=>'1','fec_ultima_modificacion'=>'2016-01-11 09:12:02','fk_a018_num_seguridad_usuario'=>'1')
);
        return $cd_c003_tipo_correspondencia;
    }
	
	
	 public function metDataOrganismosExt()
    {

		$a001_organismo = array(
		
    array('ind_descripcion_empresa'=>'GOBERNACION DEL ESTADO BOLIVAR','ind_numero_registro'=>'33','ind_tomo_registro'=>'47','ind_logo'=>'','num_estatus'=>'1','ind_tipo_organismo'=>'E','ind_direccion'=>'FRENTE A LA PLAZA BOLIVAR','fk_a018_num_seguridad_usuario'=>'1','fec_ultima_modificacion'=>'2017-01-24 14:21:02','fk_a006_num_miscelaneo_detalle_carcater_social'=>'1',
  	 'Det' => array(
       		 array('ind_cargo_representante'=>'GOBERNADOR','ind_pagina_web'=>'','ind_sujeto_control'=>'1','fk_a001_num_organismo'=>'2','fk_a003_num_persona_representante'=>'8','fk_a010_num_ciudad'=>'4')
			 )
    ),
  
  array('ind_descripcion_empresa'=>'INSTITUTO AUTONOMO DE VIVIENDA OBRAS Y SERVICIOS DEL ESTADO BOLIVAR','ind_numero_registro'=>'223','ind_tomo_registro'=>'223','ind_logo'=>'','num_estatus'=>'1','ind_tipo_organismo'=>'E','ind_direccion'=>'SABANITA AVENIDA PRINCIPAL, CRUCE CON CALLE COLON','fk_a018_num_seguridad_usuario'=>'1','fec_ultima_modificacion'=>'2017-01-24 14:20:58','fk_a006_num_miscelaneo_detalle_carcater_social'=>'1',
  	'Det' => array(		
 			 array('ind_cargo_representante'=>'DIRECTOR','ind_pagina_web'=>'www.cgr.go.ve','ind_sujeto_control'=>'0','fk_a001_num_organismo'=>'3','fk_a003_num_persona_representante'=>		'5','fk_a010_num_ciudad'=>'4')
		 			)
      ),
  
  array('ind_descripcion_empresa'=>'CONTRALORIA GENERAL DE LA REPUBLICA','ind_numero_registro'=>'23','ind_tomo_registro'=>'78','ind_logo'=>'ana','num_estatus'=>'1','ind_tipo_organismo'=>'E','ind_direccion'=>'CARACAS','fk_a018_num_seguridad_usuario'=>'1','fec_ultima_modificacion'=>'2016-12-06 15:41:25','fk_a006_num_miscelaneo_detalle_carcater_social'=>'2',
  	'Det' => array(	
  			 array('ind_cargo_representante'=>'SECRETATRIO','ind_pagina_web'=>'','ind_sujeto_control'=>'1','fk_a001_num_organismo'=>'4','fk_a003_num_persona_representante'=>'6','fk_a010_num_ciudad'=>'4')
  					)
  	),
  
  array('ind_descripcion_empresa'=>'MINISTERIO DEL PODER POPULAR PARA EL AMBIENTE','ind_numero_registro'=>'22','ind_tomo_registro'=>'11','ind_logo'=>'','num_estatus'=>'1','ind_tipo_organismo'=>'E','ind_direccion'=>'SABANITA','fk_a018_num_seguridad_usuario'=>'1','fec_ultima_modificacion'=>'2017-01-24 14:03:21','fk_a006_num_miscelaneo_detalle_carcater_social'=>'1',
  	'Det' => array(	
  			 array('ind_cargo_representante'=>'PRESIDENTE','ind_pagina_web'=>'','ind_sujeto_control'=>'1','fk_a001_num_organismo'=>'7','fk_a003_num_persona_representante'=>'8','fk_a010_num_ciudad'=>'4')		
					
					)
    	),
  
  array('ind_descripcion_empresa'=>'INSTITUTO AUTONOMO DE MINAS BOLIVAR','ind_numero_registro'=>'22','ind_tomo_registro'=>'11','ind_logo'=>'','num_estatus'=>'1','ind_tipo_organismo'=>'E','ind_direccion'=>'VISTA HERMOSA CALLE PRINCIPAL','fk_a018_num_seguridad_usuario'=>'1','fec_ultima_modificacion'=>'2017-01-24 14:12:31','fk_a006_num_miscelaneo_detalle_carcater_social'=>'1',
    'Det' => array(	
  			 array('ind_cargo_representante'=>'PRESIDENTE','ind_pagina_web'=>'','ind_sujeto_control'=>'1','fk_a001_num_organismo'=>'8','fk_a003_num_persona_representante'=>'7','fk_a010_num_ciudad'=>'4')		
					
					)
  		)
);

 	return $a001_organismo;
    }

	


	 public function metDataDependenciaExt()
    {
	
	$a021_dependencia_ext = array(
	
		 array('ind_documento_fiscal'=>'10125632','ind_descripcion'=>'DESPACHO DE LA GOBERNACION','num_estatus'=>'1','fk_a001_num_organismo'=>'2',
  		 'Det' => array(
	   			  array('ind_cargo_representante'=>'DIRECTOR','ind_direccion'=>'CASCO HISTORICO SEDE','fk_a021_num_dependencia_ext'=>'1','fk_a003_num_persona_representante'=>'3')
						)
			),
			
    	array('ind_documento_fiscal'=>'10114411','ind_descripcion'=>'SECRETARIA GENERAL DE GOBIERNO','num_estatus'=>'1','fk_a001_num_organismo'=>'3',
		'Det' => array(
				 array('ind_cargo_representante'=>'SECRETARIO GENERAL DE GOBIERNO','ind_direccion'=>'VISTA HERMOSA','fk_a021_num_dependencia_ext'=>'2','fk_a003_num_persona_representante'=>'5')
					)
			),
    	array('ind_documento_fiscal'=>'10458963','ind_descripcion'=>'SECRETARIA DE TURISMO','num_estatus'=>'1','fk_a001_num_organismo'=>'2',
		'Det' => array(
				 array('ind_cargo_representante'=>'DIRECTOR','ind_direccion'=>'CASCO HISTORICO','fk_a021_num_dependencia_ext'=>'3','fk_a003_num_persona_representante'=>'3')		
					)
			),
  		array('ind_documento_fiscal'=>'17161502','ind_descripcion'=>'DIRECCION DE  PROTECCION CIVIL Y GESTION DE RIESGO','num_estatus'=>'1','fk_a001_num_organismo'=>'3',
		'Det' => array(
				 array('ind_cargo_representante'=>'JEFE DE DIVISION','ind_direccion'=>'PLAZA BOLIVAR','fk_a021_num_dependencia_ext'=>'4','fk_a003_num_persona_representante'=>'5')	
						)
			),
    	array('ind_documento_fiscal'=>'14523698','ind_descripcion'=>'SECRETARIA DE  INFORMACION Y RELACIONES PUBLICAS','num_estatus'=>'1','fk_a001_num_organismo'=>'7',
		'Det' => array(
				 array('ind_cargo_representante'=>'DIRECTOR','ind_direccion'=>'SECTOR CRUZ VERDE CALLE BOLIVAR','fk_a021_num_dependencia_ext'=>'5',
				 'fk_a003_num_persona_representante'=>'2')		
					)
			)
	);
		return $a021_dependencia_ext;
    }
	
	
	
	
}

