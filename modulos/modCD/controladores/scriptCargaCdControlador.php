<?php

require_once 'datosCargaW/dataControlDocumentos.php';


class scriptCargaCdControlador extends Controlador

{
    use dataControlDocumentos;
    private $atscriptCargaCd;

    public function __construct()
    {
        parent::__construct();
        $this->atscriptCargaCd = $this->metCargarModelo('scriptCargaCd');
    }

    public function metIndex()
    {
	
        echo "INICIANDO<br>";
        // CORRESPONDENCIAS CD //
      	$this->metCorrespondencia();
        // ORGANISMOS EXT //
		$this->metOrgExt();
	  // ORGANISMOS EXT DETALLE //
		//$this->metOrgDetExt();
		// DEPENDENCIA EXT //
		$this->metDependenciaExt();
		
		 // DEPENDENCIA EXT DETALLE //
		//$this->metDependenciaDetExt();

          echo "TERMINADO";
    }


    public function metCorrespondencia()
   {
        //Cargar las fases
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE CORRESPONDENCIAS<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>CORRESPONDENCIAS<br>';
       $this->atscriptCargaCd->metCargarCorrespondencia($this->metDataCorrespondencia());
       echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
    }
	
	 public function metOrgExt()
   {
        //Cargar las fases
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE ORGANISMOS EXTERNOS<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>ORGANISMOS EXTERNOS<br>';
       $this->atscriptCargaCd->metCargarOrganismosExt($this->metDataOrganismosExt());
       echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
    }
	
	/*
		 public function metOrgDetExt()
   {
        //Cargar las fases
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE ORGANISMOS EXTERNOS DETALLE<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>ORGANISMOS EXTERNOS DETALLE<br>';
       $this->atscriptCargaCd->metCargarOrganismosDetExt($this->metDataOrganismosDetExt());
       echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
    }
	*/
	
			 public function metDependenciaExt()
   {
        //Cargar las fases
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE DEPENDENCIA EXTERNAS <br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>DEPENDENCIA EXTERNAS<br>';
       $this->atscriptCargaCd->metCargarDependenciaExt($this->metDataDependenciaExt());
       echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
    }

/*
		 public function metDependenciaDetExt()
   {
        //Cargar las fases
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE DEPENDENCIA EXTERNAS DETALLE<br>';
        echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->>DEPENDENCIA EXTERNAS DETALLE<br>';
       $this->atscriptCargaCd->metCargarDependenciaDetExt($this->metDataDependenciaDetExt());
       echo '&nbsp;&nbsp;&nbsp;->>FIN CARGA DATA BASICA<br><br>';
    }
*/

}
