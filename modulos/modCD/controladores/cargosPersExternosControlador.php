<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación fiscal
 * PROCESO: Ingreso y mantenimiento de cargos de personal externo.
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        08-12-2016       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class cargosPersExternosControlador extends Controlador
{
    private $atCargosExtModelo;
    public $atFuncGnrles;/*Funciones generales*/
    public function __construct()
    {
        parent::__construct();
        $this->atCargosExtModelo=$this->metCargarModelo('cargosPersExternos');
        $this->atFuncGnrles = new funcionesGenerales();
    }
    public function metIndex($lista=false){
        if($lista){/*Form modal*/
            $this->atVista->metRenderizar('listadoModal','modales');
        }else{/*Form ppal.*/
            $this->atVista->metRenderizar('listado');
        }
    }
    /**
     * Busca los registros y los monta en la grilla.
     */
    public function metListarCargos(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $result=$this->atCargosExtModelo->metListarCargos();
        if(count($result)>0){
            $this->atVista->assign('listado',$result);
            $this->atVista->metRenderizar('resultadoListado');
        }else{
            echo false;
        }
    }
    /**
     * Crea ó modifica un registro
     */
    public function metCrearModificar(){
        $valido=$this->metObtenerInt('valido');
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarJs($js);
        if($valido==1){
            $this->metValidarToken();
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $Excceccion=array('idCargo');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }
            if(in_array('error',$validacion)){
                if($validacion['idCargo']=="error"){
                    $validacion['idCargo']=0;
                }
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            $idCargo=$validacion['idCargo'];
            $error="";
            if($idCargo==0){/*Se ingresa un registro*/
                $result=$this->atCargosExtModelo->metConsultaCargo('valida_ingreso',$validacion['ind_cargo_personal_externo']);/*Se valida si existe*/
                if(COUNT($result)==0){
                    $idCargo=$this->atCargosExtModelo->metCrearCargo($validacion['ind_cargo_personal_externo']);
                    if($idCargo){
                        $arrResul['mensaje'] = 'El proceso se ejecutó exitosamente';
                        $arrResul['status'] = 'nuevo';
                    }else{
                        $arrResul['mensaje'] = 'Falló el ingreso intente mas tarde';
                        $error=true;
                    }
                }else{
                    $error=true;
                    $arrResul['mensaje'] = 'Ese cargo ya existe';
                }
            }else{/*Se actualiza un registro*/
                $result=$this->atCargosExtModelo->metConsultaCargo('valida_modificar',array("idCargo"=>$idCargo,"nombre_cargo"=>$validacion['ind_cargo_personal_externo']));/*Se valida si existe*/
                $arrResul['status'] = 'modificar';
                if(COUNT($result)==0) {
                    $result = $this->atCargosExtModelo->metModificarCargo($validacion);
                    $arrResul['reg_afectado']=$result;
                    if ($result) {
                        $arrResul['mensaje'] = 'El proceso se ejecutó exitosamente';
                    } else {
                        $error = true;
                        $arrResul['mensaje'] = 'El proceso se no se ejecutó. Intente mas tarde';
                    }
                }else{
                    $arrResul['reg_afectado']=false; $error = true;
                    $arrResul['mensaje'] = 'Ese registro ya existe';
                }
            }
            if($error){
                $arrResul['status']='errorSQL';
                echo json_encode($arrResul);
                exit;
            }
            $arrResul['idCargo']=$idCargo;
            echo json_encode($arrResul);
            exit;
        }else{
            $idCargo=$this->metObtenerInt('idCargo');
            if($idCargo>0){
                $result=$this->atCargosExtModelo->metMostrarCargo($idCargo);
                if(is_array($result)){
                    $this->atVista->assign('listado',$result);
                }else{
                    echo exit;
                }
            }
        }
        $this->atVista->metRenderizar('CrearModificar','modales');
    }
    /**
     * Ejecuta el proceso de Eliminar un registro
     */
    public function metEliminarCargo(){
        $idCargo = $this->metObtenerInt('idCargo');
        /*Se verifica si el registro está relacionado*/
        $reg_afectado=$this->atCargosExtModelo->metEliminaCargo($idCargo);
        if(is_array($reg_afectado)){
            $error=$reg_afectado; $retornar['reg_afectado']=false;
            if($error[1]==1451){
                $retornar['mensaje']="se registro está relacionado. No se puede eliminar";
            }else{
                $retornar['mensaje']="Se a producido un error. Intente mas tarde";
            }
        }else{
            $retornar['reg_afectado']=$reg_afectado;
            if($reg_afectado){
                $retornar['mensaje']="El proceso se efectuó exitosamente";
            }else{
                $retornar['mensaje']="El proceso no se ejecutó. Intente mas tarde";
            }
        }
        echo json_encode($retornar);
        exit;
    }
}
?>