<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Dependencias Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class dependenciaControlador extends Controlador
{
    private $atTipoDependencia;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDependencia=$this->metCargarModelo('dependencia');
    }

    public function metIndex($listas=false)
    {
	
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
		
		 if(!$listas){
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
		}
			$this->atVista->assign('listado',$this->atTipoDependencia->metListarTipoDependencia());
        if($listas){
		$this->atVista->metRenderizar('listadoModal','modales');
			}
		else{  
		$this->atVista->metRenderizar('listado');
        }

    }
	
	 public function metDependencia($dependencia)
    {
        $this->atVista->assign('listado', $this->atTipoDependencia->metListarDependenciaExt());
        $this->atVista->metRenderizar('listadoModal', 'modales');
    }
	
		public function metlist($list)
    {
	
		$this->atVista->assign('listado',$this->atTipoDependencia->metListarTipoDependenciaInt());
		$this->atVista->metRenderizar('listadoDepInterna','modales');
    }
	
	
	public function metlista($lista)
    {
		$this->atVista->assign('listado',$this->atTipoDependencia->metListarTipoDependenciaInt());
        $this->atVista->metRenderizar('listadodepInt','modales');
    }
	
	
	   public function metProveedor($proveedor)
    {
        $this->atVista->assign('lista', $this->atTipoDependencia->metListaPersona());
        $this->atVista->assign('tipoProveedor', $proveedor);
        $this->atVista->metRenderizar('proveedor', 'modales');
    }
	/*
	     $lista=$this->atTipoDependencia->metListarOrganismo();
        $this->atVista->assign('lista',$lista);
        $this->atVista->metRenderizar('CrearModificar','modales');
	*/
    public function metCrearModificar()
    {
		
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $complementosCss = array(
            'select2/select201ef',
        );
        $complementosJs = array(
            'select2/select2.min',
        );
		
		$this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		
        $valido=$this->metObtenerInt('valido');
        $idDependencia=$this->metObtenerInt('idDependencia');
        if($valido==1){
            $this->metValidarToken();
              $Excceccion=array('ind_documento_fiscal','fk_a003_num_persona_representante');
            $Excceccion2=array('num_estatus');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion2);
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
           if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            /*
            if(!isset($validacion['num_flag_adelanto'])){
                $validacion['num_flag_adelanto']=0;
            }
            */
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idDependencia==0){
                $id=$this->atTipoDependencia->metCrearTipoDependencia($validacion['ind_documento_fiscal'],$validacion['ind_descripcion'],$validacion['num_estatus'],$validacion['fk_a001_num_organismo'],
                $validacion['ind_cargo_representante'],$validacion['ind_direccion'],$validacion['fk_a003_num_persona_representante']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoDependencia->metModificarTipoDependencia($validacion['ind_documento_fiscal'],$validacion['ind_descripcion'],$validacion['num_estatus'],$validacion['fk_a001_num_organismo'],
                  $validacion['ind_cargo_representante'],$validacion['ind_direccion'],$validacion['fk_a003_num_persona_representante'],$idDependencia);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idDependencia']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idDependencia!=0){
            $this->atVista->assign('formDB',$this->atTipoDependencia->metMostrarTipoDependencia($idDependencia));
            $this->atVista->assign('idDependencia',$idDependencia);
		        }

					 
        $lista=$this->atTipoDependencia->metListarOrganismo();
        $this->atVista->assign('lista',$lista);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metEliminar()
    {
        $idDependencia = $this->metObtenerInt('idDependencia');
                    $id=$this->atTipoDependencia->metEliminarTipoDependencia($idDependencia);
           
                $valido=array(
                    'status'=>'ok',
                    'idDependencia'=>$id
                );

        echo json_encode($valido);
        exit;
    }
}

