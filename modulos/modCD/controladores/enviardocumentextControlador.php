<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
setlocale(LC_ALL,"es_ES");
class enviardocumentextControlador extends Controlador
{
    private $atTipoDocumento;

    public function __construct()
    {
        parent::__construct();
		$this->atTipoDocumento=$this->metCargarModelo('enviardocumentext');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumento($usuario));
        $this->atVista->metRenderizar('listado');
    }
	
	 public function metPersona($persona)
    {
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaPersonasActivas());
        $this->atVista->assign('tipoPersona', $persona);
        $this->atVista->metRenderizar('persona', 'modales');
    }
	
    public function metCrearModificar()
    {
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $complementosCss = array(
			'bootstrap-datepicker/datepicker',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		//$this->atVista->metCargarJs($js);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
             $Excceccion=array('ind_estado');
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $formula = $this->metValidarFormArrayDatos('form', 'formula');
            //$ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);

            if ($alphaNum != null && $formula == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $formula != null) {
                $validacion = $formula;
            } else {
                $validacion = array_merge($formula, $alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
            if($idDocumento==0){
                $id=$this->atTipoDocumento->metCrearTipoDocumento($validacion['fk_a003_num_persona']);
				
                $validacion['status']='nuevo';
            }
            else{
                $id=$this->atTipoDocumento->metModificarTipoDocumento(
                $validacion['ind_asunto'],
                $validacion['txt_descripcion'],
                $validacion['fec_registro'],
                $validacion['fec_documento'],
				$validacion['ind_persona_remitente'],
                $validacion['ind_cargo_remitente'],
				$validacion['fk_cdc011_num_documento'],
                $validacion['fk_a003_num_persona'],
				$validacion['fk_a004_num_dependencia'],
                $validacion['fk_a001_num_organismo'],
                $idDocumento);
				$validacion['status']='modificar';
            }
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['id']=$id;
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);





       }
		$corresp=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('corresp',$corresp);
		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metMesLetras($mes){

        if(strlen($mes)==1){
            $mes="0".$mes;
        }
        $metMesLetras=array(
            '01'=>'Enero',
            '02'=>'Febrero',
            '03'=>'Marzo',
            '04'=>'Abril',
            '05'=>'Mayo',
            '06'=>'Junio',
            '07'=>'Julio',
            '08'=>'Agosto',
            '09'=>'Septiembre',
            '10'=>'Octubre',
            '11'=>'Noviembre',
            '12'=>'Diciembre'
        );

        return $metMesLetras[$mes];
    } // END FUNC


    public function metFormatoFecha($fecha,$formato,$separador){
        #1  2016-01-01 a 01-01-2016
        #0  01-01-2016 a 2016-01-01
        if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
            $dia=substr($fecha,8,2);
            $mes=substr($fecha,5,2);
            $anio=substr($fecha,0,4);
        } elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
            $dia=substr($fecha,0,2);
            $mes=substr($fecha,3,2);
            $anio=substr($fecha,6,4);
        } else {
            $dia=0;
            $mes=0;
            $anio=0;
        }
        if (checkdate($mes, $dia, $anio)==false){
            return "la fecha es incorrecta";
            exit;
        }
        if($formato==1) {
            $fecha =$dia.$separador.$mes.$separador.$anio;
        } else {
            $fecha =$anio.$separador.$mes.$separador.$dia;
        }
        return $fecha;
    }

    public function  metImprimirOficio($idDocumento)
    {
        $this->metObtenerLibreria('documentoExterno','modCD');
        setlocale(LC_TIME, 'es_ES', 'Spanish_Spain', 'Spanish');
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $formInt=$this->metObtenerInt('form','int');
        $formTxt=$this->metObtenerTexto('form','txt');
        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
        $this->atFPDF = new pdf('P','mm','LETTER', true, 'UTF-8', false);
        $this->atFPDF->SetTitle('oficio');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetFont('Helvetica', '', 10);
        $this->atFPDF->SetMargins(25, 55, 25,true);
        $this->atFPDF->SetHeaderMargin(30);
        $this->atFPDF->SetFooterMargin(30);
        $this->atFPDF->setImageScale(1.53);
        $this->atFPDF->SetAutoPageBreak(TRUE, 50);
        $this->atFPDF->AddPage();
        $consulta= $this->atTipoDocumento->metMostrarTipoDocumento($idDocumento);
        $distribucionx= $this->atTipoDocumento->metDocDist($idDocumento);
        define('ASUNTO',$consulta['ind_asunto']);
        define('NRO_DOC_REF',$consulta['ind_dependencia']);
        define('PERSONAREMIT',$consulta['ind_persona_remitente']);
        define('NRO_DOC',$consulta['ind_documento_completo']);
        define('REPRESENTANTE',$consulta['ind_representante_externo']);
        define('CARG_REPREST',$consulta['ind_cargo_externo']);
        define('NOM_PROV',1);
        define('CONTENIDO',$consulta['txt_contenido']);
        $dia=substr($consulta['fec_salida'],8,2);
        $mes=substr($consulta['fec_salida'],5,2);
        $anio=substr($consulta['fec_salida'],0,4);
        define('FECHA',$dia.' de '.$this->metMesLetras($mes).' de '.$anio);
        if($consulta['txt_descripcion_anexo']!=null)
        {
            $textoanexos='Anexo ';
        }
        else {
            $textoanexos = '';
        }

        if($consulta['ind_representante_externo']=='Error')
        {
            $representante='';
        }
        else if($consulta['ind_representante_externo']=='error')
        {
            $representante='';
        }
        else {
            $representante = $consulta['ind_representante_externo'];
        }
        $texto=html_entity_decode($consulta['txt_contenido']);
        ##	primera pagina solamente
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetXY(152, 47);
        $this->atFPDF->Cell(30, 25,'Cumaná,'.' ' .FECHA, 0, 0, 'L');
        $this->atFPDF->SetFont('Helvetica', '', 10);
        $this->atFPDF->SetXY(25,32);
        //$this->atFPDF->Cell(25, 30, 'CES-', 0, 0, 'L');
        $this->atFPDF->SetFont('Helvetica', '', 11);
        $this->atFPDF->SetXY(25, 55);
        $this->atFPDF->Cell(25, 12, 'OFICIO N° :'.$consulta['num_cod_interno'].'-'.$consulta['num_secuencia'].'-'.$consulta['fec_annio'], 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', '', 11);
        $this->atFPDF->Ln(2);
        $this->atFPDF->SetFont('Helvetica', '',11);
        $this->atFPDF->MultiCell(100,10, 'Ciudadano(a)  ',0,'L',0,0);
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetFont('Helvetica', 'B',9);
        $this->atFPDF->MultiCell(120,12, $representante,0,'L',0,0);
        $this->atFPDF->Ln(2);
        if($consulta['ind_representante_externo']=='Error')
        {

        }
        else if($consulta['ind_representante_externo']=='error')
        {

        }
        else {
            $this->atFPDF->Ln(2);
        }


        $this->atFPDF->SetFont('Helvetica', '', 11);
        $this->atFPDF->MultiCell(70,12, $consulta['ind_cargo_externo'],0,'L',0,0);
        $this->atFPDF->Ln(5);
        $this->atFPDF->MultiCell(110,15, $distribucionx['ind_nombre_ente'],0,'L',0,0);
        $this->atFPDF->Ln(9);
        $this->atFPDF->MultiCell(128,11,$distribucionx['ind_direccion'],0,'L',0,0);
        $this->atFPDF->Ln(9);
        $this->atFPDF->MultiCell(70,10, str_replace(',',' ', str_replace('-N,',' ',str_replace('-F',' ',$distribucionx['ind_telefono']))),0,'L',0,0);
        $this->atFPDF->SetFont('Helvetica', '', 11);
        $this->atFPDF->SetXY(25, 110);
        $this->atFPDF->MultiCell(20,6,$this->atFPDF->writeHTML($texto, true, false, true, false, 'J'),0,'J',0,0);
        $y = $this->atFPDF->GetY();

        if($y<185) {
            $this->atFPDF->Ln(6);
            $this->atFPDF->SetFont('Helvetica', '', 10);
            $this->atFPDF->MultiCell(0, 0, 'Atentamente, ', 0, 'C', 0, 0);
            $this->atFPDF->Ln(17);
            $this->atFPDF->SetFont('Helvetica', 'B', 10);
            $this->atFPDF->MultiCell(0, 0, 'LCDO. ANDY VÁSQUEZ', 0, 'C', 0, 0);
            $this->atFPDF->SetFont('Helvetica', '', 10);
            $this->atFPDF->Ln(4);
            $this->atFPDF->MultiCell(0, 0, 'CONTRALOR PROVISIONAL DEL ESTADO SUCRE', 0, 'C', 0, 0);
            $this->atFPDF->SetFont('Helvetica', '', 8);
            $this->atFPDF->Ln(4);
            $this->atFPDF->MultiCell(0, 0, 'Resolución N° 01-00-000158, de fecha  18 de Septiembre de 2013', 0, 'C', 0, 0);
            $this->atFPDF->SetFont('Helvetica', '', 8);
            $this->atFPDF->Ln(4);
            $this->atFPDF->MultiCell(0, 0, 'Gaceta Oficial de la República Bolivariana de Venezuela, N° 40254 del 19-09-2013', 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(30, 9, $consulta['ind_media_firma'], 0, 'L', 0, 0);
            $this->atFPDF->Ln(5);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(30, 9, $textoanexos . $consulta['txt_descripcion_anexo'], 0, 'C', 0, 0);
        }
        else
        {
            $this->atFPDF->AddPage();
            $this->atFPDF->Ln(17);
            $this->atFPDF->SetFont('Helvetica', '', 10);
            $this->atFPDF->MultiCell(0, 0, 'Atentamente, ', 0, 'C', 0, 0);
            $this->atFPDF->Ln(17);
            $this->atFPDF->SetFont('Helvetica', 'B', 10);
            $this->atFPDF->MultiCell(0, 0, 'LCDO. ANDY VÁSQUEZ', 0, 'C', 0, 0);
            $this->atFPDF->SetFont('Helvetica', '', 10);
            $this->atFPDF->Ln(4);
            $this->atFPDF->MultiCell(0, 0, 'CONTRALOR PROVISIONAL DEL ESTADO SUCRE', 0, 'C', 0, 0);
            $this->atFPDF->SetFont('Helvetica', '', 8);
            $this->atFPDF->Ln(4);
            $this->atFPDF->MultiCell(0, 0, 'Resolución N° 01-00-000158, de fecha  18 de Septiembre de 2013', 0, 'C', 0, 0);
            $this->atFPDF->SetFont('Helvetica', '', 8);
            $this->atFPDF->Ln(4);
            $this->atFPDF->MultiCell(0, 0, 'Gaceta Oficial de la República Bolivariana de Venezuela, N° 40254 del 19-09-2013', 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(30, 9, $consulta['ind_media_firma'], 0, 'L', 0, 0);
            $this->atFPDF->Ln(5);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(30, 9, $textoanexos . $consulta['txt_descripcion_anexo'], 0, 'C', 0, 0);
        }
        $this->atFPDF->Output('oficio.pdf', 'I');


    }


		
public function metdocumento()
    {
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
        $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            if($alphaNum!=null){
                $validacion=$alphaNum;
            }else{
                $validacion=array_merge($alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
                  
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idDocumento']=$id;
            echo json_encode($validacion);
            exit;
        }
        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
       }
		
        $this->atVista->metRenderizar('documento','modales');
    }
	
  
	
}

