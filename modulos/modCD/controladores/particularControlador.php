<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Particulares
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/


class particularControlador extends Controlador
{
    private $atTipoParticular;
	private $atPaisModelo;
    private $atEstadoModelo;
    private $atMunicipioModelo;
    private $atCiudadModelo;

    public function __construct()
    {
        parent::__construct();
		Session::metAcceso();
        $this->atTipoParticular=$this->metCargarModelo('particular');
		$this->atPaisModelo   = $this->metCargarModelo('pais',false,'APP');
        $this->atEstadoModelo = $this->metCargarModelo('estado',false,'APP');
        $this->atMunicipioModelo = $this->metCargarModelo('municipio',false,'APP');
        $this->atCiudadModelo = $this->metCargarModelo('ciudad',false,'APP');
		$this->atIdUsuario    = Session::metObtener('idUsuario');
    }

    //public function metIndex()
		public function metIndex()
	{
	     $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
	  	$this->atVista->assign('listado',$this->atTipoParticular->metListarParticular());
		$this->atVista->metRenderizar('listado');
    }
	
    public function metParticular($lista=false)
	{
	     $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
		
		  if(!$lista){
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
			}
	  	$this->atVista->assign('listado',$this->atTipoParticular->metListarTipoParticular());
        if($lista){
		$this->atVista->metRenderizar('listadoParticular','modales');
	     }else{
		$this->atVista->metRenderizar('listado');
        }
    }
	
	
	public function metpersona($persona=false)
	{
	     $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
		
		  if(!$persona){
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
			}
	  	$this->atVista->assign('listado',$this->atTipoParticular->metListarTipoPart());
        if($persona){
		$this->atVista->metRenderizar('listadoEmpleado','modales');
	     }else{
		$this->atVista->metRenderizar('listado');
        }
    }
	
	public function metdestinatario($destinatario=false)
	{
	     $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
		
		  if(!$destinatario){
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
			}
	  	$this->atVista->assign('listado',$this->atTipoParticular->metListarTipoParticular());
        if($destinatario){
		$this->atVista->metRenderizar('listadoEmplDes','modales');
	     }else{
		$this->atVista->metRenderizar('listado');
        }
    }
	
    public function metCrearModificar()
    {
	
	 $js[] = 'modCD/modCDFunciones';
		 
	  $complementoJs =array(
            'inputmask/jquery.inputmask.bundle.min'
        );
        $this->atVista->metCargarJsComplemento($complementoJs);
		$this->atVista->metCargarJs($js);
		
        $valido=$this->metObtenerInt('valido');
        $idParticular=$this->metObtenerInt('idParticular');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('ind_apellido2','ind_nombre2','ind_email','ind_tipo_persona');
             $Excceccion2=array('ind_email');
		 
			$formula = $this->metValidarFormArrayDatos('form', 'formula', $Excceccion2);
            $ind = $this->metValidarFormArrayDatos('form', 'int');
            $txt = $this->metValidarFormArrayDatos('form', 'txt',$Excceccion);
            if ($formula != null && $ind == null && $txt==null) {
                $validacion = $formula;
            } elseif ($formula == null && $ind != null && $txt==null) {
                $validacion = $ind;
            } elseif($formula != null && $ind != null && $txt==null) {
                $validacion = array_merge($formula, $ind);
            } elseif($formula != null && $txt != null &&$ind == null) {
                $validacion = array_merge($formula, $txt);
            } elseif($ind != null && $txt != null && $formula == null) {
                $validacion = array_merge($ind, $txt);
            }else{
                $validacion = array_merge($ind, $txt,$formula);
            }


			
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
       
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idParticular==0){
                $id=$this->atTipoParticular->metCrearTipoParticular($validacion['ind_cedula_documento'],$validacion['ind_nombre1'],$validacion['ind_nombre2'],
				$validacion['ind_apellido1'],$validacion['ind_apellido2'],$validacion['ind_email'],$validacion['ind_tipo_persona'],
				$validacion['num_estatus'],$validacion['fk_a006_num_miscelaneo_det_tipopersona'],
				$validacion['fk_a010_num_ciudad']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoParticular->metModificarTipoParticular($validacion['ind_cedula_documento'],$validacion['ind_nombre1'],$validacion['ind_nombre2'],
				$validacion['ind_apellido1'],$validacion['ind_apellido2'],$validacion['ind_email'],$validacion['ind_tipo_persona'],
				$validacion['num_estatus'],$validacion['fk_a006_num_miscelaneo_det_tipopersona'],
				$validacion['fk_a010_num_ciudad'],$idParticular);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idParticular']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idParticular!=0){
            $this->atVista->assign('formDB',$this->atTipoParticular->metMostrarTipoParticular($idParticular));
            $this->atVista->assign('idParticular',$idParticular);
			
			//var_dump($this->atTipoParticular->metMostrarTipoParticular($idParticular)); 
			//exit;
        }
		
		$PAIS = Session::metObtener('DEFAULTPAIS');
        $ESTADO = Session::metObtener('DEFAULTESTADO');
        $MUNICIPIO = Session::metObtener('DEFAULTMUNICIPIO');
        $CIUDAD = Session::metObtener('DEFAULTCIUDAD');
		$this->atVista->assign('DefaultPais',$PAIS);
        $this->atVista->assign('DefaultEstado',$ESTADO);
        $this->atVista->assign('DefaultMunicipio',$MUNICIPIO);
        $this->atVista->assign('DefaultCiudad',$CIUDAD);

	    /*PARA LISTAR SELECT PAISES, ESTADO, MUNICIPIO, CIUDAD*/
        $this->atVista->assign('listadoPais',$this->atPaisModelo->metListarPais(1));
        $this->atVista->assign('listadoEstado',$this->atEstadoModelo->metJsonEstado($PAIS));
        $this->atVista->assign('listadoMunicipio',$this->atMunicipioModelo->metJsonMunicipio($ESTADO));
        $this->atVista->assign('listadoCiudad',$this->atCiudadModelo->metJsonCiudad($ESTADO));	
		
		$miscelaneo=$this->atTipoParticular->metListarMiscelaneos();
        $this->atVista->assign('miscelaneo',$miscelaneo);
		
		$miscelaneop=$this->atTipoParticular->metListarMiscelaneosp();
        $this->atVista->assign('miscelaneop',$miscelaneop);

        $this->atVista->metRenderizar('CrearModificar','modales');
    }
	
    public function metEliminar()
    {
        $idParticular = $this->metObtenerInt('idParticular');
                    $id=$this->atTipoParticular->metEliminarTipoParticular($idParticular);
           
                $valido=array(
                    'status'=>'ok',
                    'idParticular'=>$id
                );

        echo json_encode($valido);
        exit;
    }
	
}