<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: PLANIFICACIÓN FISCAL
 * PROCESO: REGISTRO Y MANTENIMIENTO DE REPRESENTANTES DE ENTES
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |        0426-5144382            |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        30-05-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class representantesEnteControlador extends Controlador{
    private $atReprstnteModelo;   //Para el modelo de Representantes entes.
    private $atFuncGnrles;          //Para las Funciones generales
    private $atIdContraloria;       //Para el Id de la contraloría que ejecuta.
    private $atIdDependenciaUser;   //Para el Id de la dependencia al cual pertenece el usuario.
    private $atIdEmpleadoUser;      //Para el Id empleado del usuario que ejecuta.
    private $atTipoUser;            //Para el tipo de usuario logueado
    private $atUserValido;          //Contendrá la validéz del usuario
    public function __construct(){
        parent::__construct();
        $this->atReprstnteModelo = $this->metCargarModelo('representantesEnte');
        $this->atFuncGnrles = new funcionesGenerales();
        //Se determina la validéz y el tipo de usuario.
        $arrUser=$this->atFuncGnrles->metValidaUsuario();
        if($arrUser['userValido']){//Entra, si es un Usuario Válido.
            $this->atTipoUser=$arrUser['tipoUser'];
            $this->atUserValido=$arrUser['userValido'];
            $this->atIdContraloria=$arrUser['pk_num_organismo'];
            $this->atIdEmpleadoUser=$arrUser['pk_num_empleado'];
            $this->atIdDependenciaUser=$arrUser['pk_num_dependencia'];
        }else{//De lo contrario, usuario no válido
            $this->atUserValido=$arrUser['userValido'];
        }
        $arrUser=null;
    }
    public function metIndex(){
        if($this->atUserValido) {
            $this->atVista->metRenderizar('listado');
        }else{
            $this->atVista->assign('valido', $this->atUserValido);
            $this->atVista->metRenderizar('../../error');
        }
    }
    /**
     *Busca los Representantes de entes y los Visualiza en el listado principal
     *@throws Exception
     */
    public function metListarRepresentantes(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $result = $this->atReprstnteModelo->metConsultasRepresentantes('por_listado');
        $this->atVista->assign('listado',$result);
        $this->atVista->metRenderizar('resultadoListado');
    }
    public function metComplementosForm(){
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c',
            'select2/select201ef'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
    }
    /**
     * Prepara la data a hacer montada en la ficha "Representantes" del form modal
     * @param $arrPers
     * @return array
     */
    public function metPreparaData($arrPers){
        $arrPersona=array(
            "idRpstte"=>$arrPers["pk_num_persona"],"txt_cedula"=>$arrPers["ind_cedula_documento"],"txt_nombrepers1"=>$arrPers["ind_nombre1"],"txt_nombrepers2"=>$arrPers["ind_nombre2"],
            "txt_apellido1"=>$arrPers["ind_apellido1"],"txt_apellido2"=>$arrPers["ind_apellido2"],"txt_email"=>$arrPers["ind_email"],
            "cbox_situacionjuridica"=>$arrPers["ind_tipo_persona"],"chk_estatuspers"=>$arrPers["num_estatus"],
            "txt_documento_fiscal"=>$arrPers["ind_documento_fiscal"],"cbox_nacionalidad"=>$arrPers["fk_a006_num_miscelaneo_detalle_nacionalidad"],
            "cbox_tipopersona"=>$arrPers["fk_a006_num_miscelaneo_det_tipopersona"],"desc_tipopersona"=>$arrPers["desc_tipopersona"]);
        return $arrPersona;
    }
    /**
     * Verifica si existe la persona al darle enter en el campo cédula de la ficha "Representante" del form modal
     * Retorna los datos de la persona.
     */
    public function metBuscaPersona(){
        $txt_cedula=$this->metObtenerTexto('txt_cedula');
        $result=$this->atReprstnteModelo->metConsultasRepresentantes('valida_persona',array("txt_cedula"=>$txt_cedula));
        if(COUNT($result)>0){
            $arrPersona=$this->metPreparaData($result[0]);
            echo json_encode($arrPersona);
        }else{
            echo false;
        }
    }
    /**
     * Visualiza el form modal para ingresar un nuevo regitro ó actualizar.
     */
    public function metcrearModPersona(){
        $valido=$this->metObtenerInt('valido'); $error=false;
        if($valido==1){
            $Exceccion_int = array('idRpstte');
            $arrInt = $this->metValidarFormArrayDatos('form', 'int', $Exceccion_int);
            $Exceccion_txt = array('txt_nombrepers2', 'txt_apellido2','txt_email');
            $arrTxt = $this->metValidarFormArrayDatos('form', 'txt', $Exceccion_txt);
            $arrParams = array_merge($arrInt, $arrTxt);
            if (in_array('error', $arrParams)) {
                $arrParams['Error'] = 'error';
                echo json_encode($arrParams); exit;
            }
            if(!isset($arrParams['chk_estatuspers'])){
                $arrParams['chk_estatuspers']=0;
            }
            if($arrParams["idRpstte"]==0){//Se ingresa un registro
                $result=$this->atReprstnteModelo->metConsultasRepresentantes('valida_persona',array("txt_cedula"=>$arrParams["txt_cedula"]));//Valida si existe la persona en el sistema
                if(COUNT($result)==0){
                    $idRpstte=$this->atReprstnteModelo->metIngresaPersona($arrParams);
                    if($idRpstte){
                        $arrResult["idRpstte"]=$idRpstte;
                        $arrResult['mensaje'] = 'El ingreso se ejecutó exitosamente. Ahora asígnalo a su respectiva organización';
                        unset($arrParams["idRpstte"]);
                        $arrResult['dataValidada']=$arrParams;
                    }else{
                        $arrResult['mensaje'] = 'Falló el ingreso. Intente nuevamente';$error=true;
                    }
                }else{
                    $result2=$this->atReprstnteModelo->metConsultasRepresentantes('por_listado',$result[0]['pk_num_persona']);//Valida si está relacionado a una organización externa
                    if(COUNT($result2)>0){//Entra si está relacionado a una organización
                        $arrResult['mensaje'] = 'Esa persona existe en una organización externa. Búsquela en el listado y haga la actualización correspondiente';$error = true;
                    }else{//Entra cuando la persona existe en la tabla persona pero no esta relacionado a un ente u organización para mostrarlo en el formulario
                        $arrResult['Error']='errorExiste';
                        $arrResult['dataPersona']=$this->metPreparaData($result[0]);
                        $arrResult['mensaje'] = 'Esa persona ya está registrada en el sistema. Verifica sus datos y actualízalo. Luego, asígnalo a su respectiva organización';
                    }
                    $result=null; $result2=null;
                }
            }else{//Entra cuando se actualiza un registro
                $result=$this->atReprstnteModelo->metConsultasRepresentantes('valida_modificar',array('idRpstte'=>$arrParams['idRpstte'],'txt_cedula'=>$arrParams['txt_cedula']));//Se valida si existe
                if(COUNT($result)==0) {
                    $reg_afectado = $this->atReprstnteModelo->metModificaRepresentante($arrParams);
                    if ($reg_afectado) {
                        $arrResult['reg_afectado'] = $reg_afectado;
                        $arrResult['mensaje'] = 'La actualización se ejecutó exitosamente';
                        $arrResult['dataValidada']=$arrParams;
                    } else {
                        $arrResult['mensaje'] = 'Falló la actualización. Intente nuevamente';$error = true;
                    }
                }else{
                    $arrResult['mensaje'] = 'Ese persona ya existe'; $error=true;
                }

            }
            if($error){
                $arrResult['Error']='errorSQL';
            }
            echo json_encode($arrResult);

        }else{//Entra para montar el registro desde la grilla principal ya sea para actualizarlo ó Visualizar el form modal para un nuevo registro.
            $idRpstte=$this->metObtenerInt('idRpstte');
            if($idRpstte>0){//Carga el form modal para actualizar los datos. De lo contrario, visualiza el form modal en limpio para ingresar.
                $result=$this->atReprstnteModelo->metConsultasRepresentantes('valida_persona',array("idRpstte"=>$idRpstte));
                if(COUNT($result)>0){
                    $this->atVista->assign('formDB',$this->metPreparaData($result[0]));
                }else{
                    echo false; exit;
                }
            }else{
                $this->atVista->assign('formDB',array("idRpstte"=>$idRpstte));
            }
            $this->metComplementosForm();
            $this->atVista->assign('opcionProceso','ingresar');
            $campos="pk_num_miscelaneo_detalle AS id_nacionalidad,ind_nombre_detalle AS desc_nacionalidad, cod_detalle";
            $arrNacionalidad=$this->atFuncGnrles->metBuscaMiscelanio($campos,'NACION');
            $this->atVista->assign('listaNacionalidad',$arrNacionalidad);
            //Se listan los entes padres
            $arrEntes=$this->atFuncGnrles->metCargaCboxRecursivoEnte(0);
            $this->atVista->assign('listaEntes',$arrEntes);
            //Se lista los tipos de persona
            $campos="pk_num_miscelaneo_detalle AS idtipo_persona,ind_nombre_detalle AS desctipo_persona, cod_detalle";
            $arrTiposPers=$this->atFuncGnrles->metBuscaMiscelanio($campos,'TIP_PERSON');
            foreach($arrTiposPers as $fila){
                if($fila["cod_detalle"]=="PART" OR $fila["cod_detalle"]=="REP"){
                    $data[]=$fila;
                }
            }
            $this->atVista->assign('tipopersona',$data);
            //Se lista los cargos de personal externo
            $campos="pk_num_miscelaneo_detalle AS id_cargo,ind_nombre_detalle AS nombre_cargo";
            $arrCargos=$this->atFuncGnrles->metBuscaMiscelanio($campos,'PFCPEXT');
            $this->atVista->assign('listaCargos',$arrCargos);
            //Se lista las situaciones de la persona con respecto al ente
            $campos="pk_num_miscelaneo_detalle AS id_situacionpers,ind_nombre_detalle AS desc_situacion";
            $arrSituacionPers=$this->atFuncGnrles->metBuscaMiscelanio($campos,'PFSPE');
            $this->atVista->assign('listaSituacionpers',$arrSituacionPers);
            $this->atVista->metRenderizar('CrearModificar','modales');
        }
    }
    /**
     * Busca los entes para llenar el combo cbox_entes
     */
    public function metBuscaEntes(){
        $idEntePadre=$this->metObtenerInt('idEnte');
        $arrEntes=$this->atFuncGnrles->metCargaCboxRecursivoEnte($idEntePadre);
        echo json_encode(array("filas"=>$arrEntes));
    }
    /**
     * Busca las organizaciones en las cuales está adscrita una persona en particular y las presenta en la grilla de la ficha "Organización".
     * @throws Exception
     */
    public function metBuscaOrganizacionPers(){
        $js[] = 'modPF/DataTablePf';
        $this->atVista->metCargarJs($js);
        $idRpstte=$this->metObtenerInt('idRpstte');
        $result=$this->atReprstnteModelo->metListaOrganizacionPers($idRpstte);
        if(COUNT($result)>0){
            foreach($result as $fila){
                $arrEnte=$this->atFuncGnrles->metBuscaRecursivoEntes($fila['id_ente']);
                $fila['nombre_ente'] = $arrEnte["cadEntes"];
                $fila["estatus_respente"] =  $this->atFuncGnrles->metEstatus($fila['estatus_Reprstnte']);
                $data[]=$fila;
            }
            $this->atVista->assign('idPersonaOrg',$idRpstte);
            $this->atVista->assign('listaOrganizacion',$data);
            $this->atVista->metRenderizar('resultadoOrganizacion');
        }else{
            echo "";
        }
    }
    /**
     * Permite agregar ó actualizar la adscripción de una persona a un ente.
     */
    public function metcreaModOrganizacion(){
        $Exceccion_int = array('idPersonaEnte','cbox_estatus');
        $arrParams = $this->metValidarFormArrayDatos('form', 'int', $Exceccion_int);
        $error=false;
        if (in_array('error', $arrParams)) {
            $arrParams['Error'] = 'error';
            echo json_encode($arrParams); exit;
        }else{
            if($arrParams["cbox_estatus"]==1){
                $result=$this->atReprstnteModelo->metValidaOrganizacion($arrParams);//Se valida si existe otra persona activa como titular ó encargado para el mismo ente y cargo
                if($result){
                    $arrResult['mensaje'] = 'La persona '.$result['nombre_persona'].', C.I. Nro. '.number_format($result['cedula_persona'],0,',','.').', está activa en el ente seleccionado con el mísmo cargo.';
                    $arrResult['Error']='errorSQL';
                    echo json_encode($arrResult);
                    exit;
                }
            }
            if($arrParams["idPersonaEnte"]==0){//Se ingresa un registro
                $result=$this->atReprstnteModelo->metValidaIngresoOrganizacion($arrParams);//Valida si la persona existe en esa organización con el mísmo cargo
                if(!$result){
                    $idPersonaEnte = $this->atReprstnteModelo->metIngresaOrganizacion($arrParams);
                    if ($idPersonaEnte) {
                        $arrResult["idPersonaEnte"] = $idPersonaEnte;
                        $arrResult['mensaje'] = 'La operación se ejecutó exitosamente';
                        unset($arrParams["idPersonaEnte"]);
                        $arrResult['dataValidada'] = $arrParams;
                    } else {
                        $arrResult['mensaje'] = 'Falló el ingreso. Intente nuevamente';
                        $error = true;
                    }
                }else{
                    $arrResult['mensaje'] = 'Esa persona ya está asignada a ese ente';$error = true;
                }
            }else{//Entra cuando se actualiza un registro
                $reg_afectado = $this->atReprstnteModelo->metModificaOrganizacion($arrParams);
                if ($reg_afectado) {
                    $arrResult['reg_afectado'] = $reg_afectado;
                    $arrResult['mensaje'] = 'El proceso se ejecutó exitosamente';
                    $arrResult['dataValidada']=$arrParams;
                } else {
                    $arrResult['mensaje'] = 'Falló la actualización. Intente nuevamente';$error = true;
                }
            }
            if($error){
                $arrResult['Error']='errorSQL';
            }
            echo json_encode($arrResult);
        }
    }
    /**
     * Elimina ó quita una perona Representante adscrita a un ente en particular siempre y cuando el registro no esté relacionado.
     */
    public function metEliminarAdscripcion(){
        $idPersonaEnte=$this->metObtenerInt('idPersonaEnte');
        $reg_afectado=$this->atReprstnteModelo->metEliminaAdscripcion($idPersonaEnte);
        if(is_array($reg_afectado)){
            $retornar['reg_afectado']=false;
            if($reg_afectado[1]==1451){
                $retornar['mensaje']="Imposible de eliminar ese registro. ¡Está relacionado!";
            }else{
                $retornar['mensaje']="Se a producido un error. Intente mas tarde";
            }
        }else{
            $retornar['reg_afectado']=$reg_afectado;
            if($reg_afectado){
                $retornar['mensaje']="El proceso se efectuó exitosamente";
            }else{
                $retornar['mensaje']="El proceso no se ejecutó. Intente mas tarde";
            }
        }
        echo json_encode($retornar);
    }
}