<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Entrada de Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class entdocumenexternoControlador extends Controlador
{
    private $atTipoDocumento;
	private $atTipoDependencia;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDocumento=$this->metCargarModelo('entdocumenexterno');
		$this->atTipoDependencia=$this->metCargarModelo('dependencia');
		
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029'
        );
		
		 $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
		
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
		
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumento());
        $this->atVista->metRenderizar('listado');
    }
	
 public function metProveedor($proveedor)
    {
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaPersona2());
        $this->atVista->assign('tipoProveedor', $proveedor);
        $this->atVista->metRenderizar('proveedor', 'modales');
    }
	
	  	
	 public function metPersona($persona)
    {
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaPersona());
        $this->atVista->assign('tipoPersona', $persona);
        $this->atVista->metRenderizar('persona', 'modales');
    }
	
	 public function metEmpresa($empresa)
    {
        $this->atVista->assign('lista', $this->atTipoDocumento->metListarProveedor());
        $this->atVista->assign('tipoEmpresa', $empresa);
        $this->atVista->metRenderizar('empresa', 'modales');
    }

		
    public function metCrearModificar()
    {
	
		  $js[] = 'modCD/modCDFunciones';
	
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
			'select2/select201ef'
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'inputmask/jquery.inputmask.bundle.min',
			'select2/select2.min'
        );
	

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
// ,'num_folio','num_anexo','num_carpeta',
		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
		$idDependencia = $this->metObtenerInt('idDependencia');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_particular_rem','num_depend_ext','num_org_ext','num_folio','num_anexo','num_carpeta','num_empresa','txt_descripcion_asunto','ind_descripcion_anexo','fec_documento','fec_registro','text_asunto');
			$alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum',$Excceccion);
            $ind = $this->metValidarFormArrayDatos('form', 'int',$Excceccion);
            $txt = $this->metValidarFormArrayDatos('form', 'txt',$Excceccion);
            if ($alphaNum != null && $ind == null && $txt==null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $validacion = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            }else{
                $validacion = array_merge($ind, $txt,$alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
				
		
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']='Pendiente';
            }
			
			if(!isset($validacion['num_folio'])){
                $validacion['num_folio']=0;
            }
			
			if(!isset($validacion['num_anexo'])){
                $validacion['num_anexo']=0;
            }
			
			if(!isset($validacion['num_carpeta'])){
                $validacion['num_carpeta']=0;
            }
	
			  if(!isset($validacion['num_particular_rem'])){
                $validacion['num_particular_rem']=0;
            }
			  if(!isset($validacion['num_depend_ext'])){
                $validacion['num_depend_ext']=0;
            }
			  if(!isset($validacion['num_org_ext'])){
                $validacion['num_org_ext']=0;
            }
			
			  if(!isset($validacion['ind_descripcion_anexo'])){
                $validacion['ind_descripcion_anexo']=NULL;
            }
	
			  if(!isset($validacion['num_empresa'])){
                $validacion['num_empresa']=0;
            }
			
			  if(!isset($validacion['ind_representante'])){
                $validacion['ind_representante']=NULL;
            }
			
			  if(!isset($validacion['ind_cargo'])){
                $validacion['ind_cargo']=NULL;
            }

            if($idDocumento==0){
                $id=$this->atTipoDocumento->metCrearTipoDocumento(
                    $validacion['num_documento'],
                    $validacion['fec_documento'],
                    $validacion['fec_registro'],
				    $validacion['text_asunto'],
                    $validacion['txt_descripcion_asunto'],
                    $validacion['ind_persona_recibido'],
                    $validacion['num_folio'],
                    $validacion['num_anexo'],
                    $validacion['num_carpeta'],
                    $validacion['ind_descripcion_anexo'],
				    $validacion['ind_nombre_mensajero'],
                    $validacion['ind_cedula_mensajero'],
                    $validacion['ind_estado'],
                    $validacion['num_particular_rem'],
                    $validacion['num_depend_ext'],
                    $validacion['fk_cdc003_num_tipo_documento'],
                    $validacion['num_org_ext'],
                    $validacion['num_empresa']);
                    $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoDocumento->metModificarTipoDocumento(
                    $validacion['num_documento'],
                    $validacion['fec_documento'],
                    $validacion['fec_registro'],
				$validacion['text_asunto'],
                    $validacion['txt_descripcion_asunto'],
                    $validacion['ind_persona_recibido'],
                    $validacion['num_folio'],
                    $validacion['num_anexo'],
                    $validacion['num_carpeta'],
                    $validacion['ind_descripcion_anexo'],
				$validacion['ind_nombre_mensajero'],
                    $validacion['ind_cedula_mensajero'],
                    $validacion['ind_estado'],
                    $validacion['num_particular_rem'],
                    $validacion['num_depend_ext'],
                    $validacion['fk_cdc003_num_tipo_documento'],
                    $validacion['num_org_ext'],
                    $validacion['num_empresa'],
				$idDocumento);
                $validacion['status']='modificar';
            }

         
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
			
			
	//	var_dump($this->atTipoDocumento->metListarOrganismo($lista)); 
		//	exit;
            
             
        }
        $lista=$this->atTipoDocumento->metListarOrganismoEnte();
        $this->atVista->assign('lista',$lista);
		
		  $exdependencia=$this->atTipoDocumento->metListarDependencia();
        $this->atVista->assign('exdependencia',$exdependencia);
		
		$centroCosto = $this->atTipoDocumento->metListarCentroCosto();
		 $this->atVista->assign('centroCosto',$centroCosto);
		 
		$corresp=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('corresp',$corresp);

		
        $this->atVista->metRenderizar('CrearModificar','modales');
    }
	
	    #para armar json centro de costos
    public function metJsonCentroCosto()
    {

        $idDependencia = $this->metObtenerInt('idDependencia');
        $centro_costo  = $this->atTipoDocumento->metJsonCentroCosto($idDependencia);
        echo json_encode($centro_costo);
        exit;

    }
	
	public function metVer()
    {
		
		$js[] = 'modCD/modCDFunciones';
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarJs($js);
		$this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
       


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
    
        }
        
		
   
       $lista=$this->atTipoDocumento->metListarOrganismoEnte();
        $this->atVista->assign('lista',$lista);
		
		  $exdependencia=$this->atTipoDocumento->metListarDependencia();
        $this->atVista->assign('exdependencia',$exdependencia);
		
		$centroCosto = $this->atTipoDocumento->metListarCentroCosto();
		 $this->atVista->assign('centroCosto',$centroCosto);
		 
		 $corresp=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('corresp',$corresp);
		
		
        $this->atVista->assign('ver', 1);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

	
}

