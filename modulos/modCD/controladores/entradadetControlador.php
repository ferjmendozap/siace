<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class entradadetControlador extends Controlador
{
    private $atCorrespondencia;

    public function __construct()
    {
        parent::__construct();
        $this->atCorrespondencia=$this->metCargarModelo('entradadet');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->atCorrespondencia->metListarCorrespondencia());
        $this->atVista->metRenderizar('listado');
    }

    public function metCrearModificar()
    {
        $valido=$this->metObtenerInt('valido');
        $idCorrespondencia=$this->metObtenerInt('idCorrespondencia');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('ind_informe_escrito','ind_hablar_conmigo','ind_coordinar_con','ind_preparar_memorandum',
			'ind_investigar_informar','ind_tramitar_conclusion','ind_distribuir','ind_conocimiento',
			'ind_preparar_constentacion','ind_archivar','ind_registro_de','ind_preparar_oficio',
			'ind_conocer_opinion','ind_tramitar_caso','ind_acusar_recibo','ind_tramitar_en');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            //$ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null){
                $validacion=$alphaNum;
            }else{
                $validacion=array_merge($alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
        
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
			
			if(!isset($validacion['ind_informe_escrito'])){
                $validacion['ind_informe_escrito']=0;
            }
			
			if(!isset($validacion['ind_hablar_conmigo'])){
                $validacion['ind_hablar_conmigo']=0;
            }
			
			if(!isset($validacion['ind_investigar_informar'])){
                $validacion['ind_investigar_informar']=0;
            }
			
			if(!isset($validacion['ind_tramitar_conclusion'])){
                $validacion['ind_tramitar_conclusion']=0;
            }
			
			if(!isset($validacion['ind_distribuir'])){
                $validacion['ind_distribuir']=0;
            }
			
			if(!isset($validacion['ind_conocimiento'])){
                $validacion['ind_conocimiento']=0;
            }
			
			if(!isset($validacion['ind_preparar_constentacion'])){
                $validacion['ind_preparar_constentacion']=0;
            }
			
			if(!isset($validacion['ind_archivar'])){
                $validacion['ind_archivar']=0;
            }
			
			if(!isset($validacion['ind_conocer_opinion'])){
                $validacion['ind_conocer_opinion']=0;
            }
			
			if(!isset($validacion['ind_tramitar_caso'])){
                $validacion['ind_tramitar_caso']=0;
            }
			
			if(!isset($validacion['ind_acusar_recibo'])){
                $validacion['ind_acusar_recibo']=0;
            }
			
            if($idCorrespondencia==0){
                $id=$this->atCorrespondencia->metCrearCorrespondencia(
				$validacion['ind_informe_escrito'],$validacion['ind_hablar_conmigo'],$validacion['ind_coordinar_con'],$validacion['ind_preparar_memorandum'],
				$validacion['ind_investigar_informar'],$validacion['ind_tramitar_conclusion'],$validacion['ind_distribuir'],$validacion['ind_conocimiento'],
				$validacion['ind_preparar_constentacion'],$validacion['ind_archivar'],$validacion['ind_registro_de'],$validacion['ind_preparar_oficio'],
				$validacion['ind_conocer_opinion'],$validacion['ind_tramitar_caso'],$validacion['ind_acusar_recibo'],$validacion['ind_tramitar_en'],
				$validacion['ind_observacion']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atCorrespondencia->metModificarCorrespondencia(
				$validacion['ind_informe_escrito'],$validacion['ind_hablar_conmigo'],$validacion['ind_coordinar_con'],$validacion['ind_preparar_memorandum'],
				$validacion['ind_investigar_informar'],$validacion['ind_tramitar_conclusion'],$validacion['ind_distribuir'],$validacion['ind_conocimiento'],
				$validacion['ind_preparar_constentacion'],$validacion['ind_archivar'],$validacion['ind_registro_de'],$validacion['ind_preparar_oficio'],
				$validacion['ind_conocer_opinion'],$validacion['ind_tramitar_caso'],$validacion['ind_acusar_recibo'],$validacion['ind_tramitar_en'],
				$validacion['ind_observacion'],$idCorrespondencia);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idCorrespondencia']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idCorrespondencia!=0){
            $this->atVista->assign('formDB',$this->atCorrespondencia->metMostrarCorrespondencia($idCorrespondencia));
            $this->atVista->assign('idCorrespondencia',$idCorrespondencia);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metEliminar()
    {
        $idCorrespondencia = $this->metObtenerInt('idCorrespondencia');
        if($idCorrespondencia!=0){
            $id=$this->atCorrespondencia->metEliminarCorrespondencia($idCorrespondencia);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idCorrespondencia'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }
}