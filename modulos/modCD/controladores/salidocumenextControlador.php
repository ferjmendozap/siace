<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Salida de Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class salidocumenextControlador extends Controlador
{
    private $atTipoDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDocumento=$this->metCargarModelo('salidocumenext');
		
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumento(($usuario)));
        $this->atVista->metRenderizar('listado');

    }
	
 public function metProveedor($proveedor)
    {
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaRepresentantExt($usuario));
        $this->atVista->assign('tipoProveedor', $proveedor);
        $this->atVista->metRenderizar('proveedor', 'modales');
    }
	
		 public function metPersona($persona)
    {
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaPersona());
        $this->atVista->assign('tipoPersona', $persona);
        $this->atVista->metRenderizar('persona', 'modales');
    }
	
	public function rellenarCeros($nro,$cantidad){
	  
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

	
    public function metCrearModificar()
    {
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		//$this->atVista->metCargarJs($js);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
             $Excceccion=array(
                 'ind_asunto',
                 'ind_estado',
                 'txt_descripcion',
                 'ind_cargo_remitente',
                 'fec_documento',
                 'ind_plazo_atencion',
                 'ind_anexo',
                 'txt_descripcion_anexo',
                 'num_cod_interno',
                 'fec_mes',
                 'fec_annio',
			     'ind_persona_remitente',
                 'ind_cargo_remitente',
                 'fk_a001_num_organismo',
                 'ind_con_atencion',
                 'ind_cargo_personal_externo',
                 'ind_organismo_externo',
                 'ind_dependencia_externa');
			 $Excceccion2=array('ind_cod_completo');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            $ind = $this->metValidarFormArrayDatos('form', 'int',$Excceccion);
            $txt = $this->metValidarFormArrayDatos('form', 'txt',$Excceccion2);

            if ($alphaNum != null && $ind == null && $txt==null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $validacion = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            }else{
                $validacion = array_merge($ind, $txt,$alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
        
			
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']='0';
            }
			
			if(!isset($validacion['ind_anexo'])){
                $validacion['ind_anexo']=0;
            }

			if(!isset($validacion['txt_descripcion_anexo'])){
                $validacion['txt_descripcion_anexo']=NULL;
            }
			
			if(!isset($validacion['ind_con_atencion'])){
                $validacion['ind_con_atencion']=0;
            }
			
			if(!isset($validacion['ind_plazo_atencion'])){
                $validacion['ind_plazo_atencion']=0;
            }
			
			 if(!isset($validacion['ind_organismo_externo'])){
                $validacion['ind_organismo_externo']=false;
            }
			
			 if(!isset($validacion['ind_dependencia_externa'])){
                $validacion['ind_dependencia_externa']=false;
            }
			
			 if(!isset($validacion['ind_empresa_externa'])){
                $validacion['ind_empresa_externa']=false;
            }
			
			 if(!isset($validacion['ind_representante_externo'])){
                $validacion['ind_representante_externo']=false;
            }
			
            if(!isset($validacion['ind_cargo_personal_externo'])){
                $validacion['ind_cargo_personal_externo']=false;
				
            }

			 if(!isset($validacion['flag_r'])){
                $validacion['flag_r']=false;
			 }
			
			if(!isset($validacion['num_secuencia'])){
                $validacion['num_secuencia']=0;
            }
			
				if(!isset($validacion['num_cod_interno'])){
                $validacion['num_cod_interno']=NULL;
            }
           
		   	// busco el tipo de documento en la tabla y m traigo el pk
			$documento = $this->atTipoDocumento->metBuscarcorrespondencia($validacion['fk_cdc003_num_tipo_documento']);
			$codigocorresp=$documento['pk_num_tipo_documento'];
			
			// busco en la tabla dependencias  y m traigo el pk y el codigo internoz
            $dependencia = $this->atTipoDocumento->metBuscarDependencia($validacion['ind_dependencia_remitente']);
            $codigoInt=$dependencia['ind_codinterno'];
			$codigodep=$dependencia['pk_num_dependencia'];
			
			//  hago la consulta a la tabla
			$orden=$this->atTipoDocumento->metContar('cd_c011_documento_salida',date('Y'),'fec_annio',$codigodep,'ind_dependencia_remitente',$codigocorresp,'fk_cdc003_num_tipo_documento');
			$orden['cantidad'] = $orden['cantidad'] + 1;	 
			$secuencia=$this->rellenarCeros($orden['cantidad'],4);

            // modificar
			$orden2=$this->atTipoDocumento->metContar2($idDocumento);
            $codigoReq2=$dependencia['ind_codinterno']."-".$this->rellenarCeros($orden2['cantidad2'],4)."-".date('Y');
            $codigomod2=$dependencia['ind_codinterno'];

            // contruyo el codigo completo del documento
			$codigoReq=$dependencia['ind_codinterno']."-".$this->rellenarCeros($orden['cantidad'],4)."-".date('Y');

            $codigomod=$dependencia['ind_codinterno'];

		   
            if($idDocumento==0){
                $id=$this->atTipoDocumento->metCrearTipoDocumento(
				$secuencia,
                $codigoInt,
                $codigoReq,
                $validacion['ind_dependencia_remitente'],
                $validacion['ind_persona_remitente'],
				$validacion['ind_cargo_remitente'],
                $validacion['ind_asunto'],
                $validacion['txt_descripcion'],
				$validacion['ind_anexo'],
                $validacion['txt_descripcion_anexo'],
                $validacion['ind_plazo_atencion'],
				$validacion['ind_estado'],
				$validacion['fk_cdc003_num_tipo_documento'],
                $validacion['fk_a001_num_organismo'],
				$validacion['ind_organismo_externo'],
                $validacion['ind_dependencia_externa'],
                $validacion['ind_empresa_externa'],
                $validacion['flag_r'],
				$validacion['ind_representante_externo'],
                $validacion['ind_cargo_personal_externo'],
                $validacion['fec_documento'],
                $validacion['ind_con_atencion']);
                $validacion['status']='nuevo';
				
            }else 
			{

                $id=$this->atTipoDocumento->metModificarTipoDocumento(
                $codigomod2,
                 $codigoReq2,
				$validacion['ind_dependencia_remitente'],
                $validacion['ind_persona_remitente'],
				$validacion['ind_cargo_remitente'],
                 $validacion['ind_asunto'],
                $validacion['txt_descripcion'],
				$validacion['ind_anexo'],
                $validacion['txt_descripcion_anexo'],
                $validacion['ind_plazo_atencion'],
				$validacion['ind_estado'],
				$validacion['fk_cdc003_num_tipo_documento'],
                $validacion['fk_a001_num_organismo'],
				$validacion['ind_organismo_externo'],
                $validacion['ind_dependencia_externa'],
                $validacion['ind_empresa_externa'],
                $validacion['flag_r'],
				$validacion['ind_representante_externo'],
                $validacion['ind_cargo_personal_externo'],
                $validacion['ind_con_atencion'],
                $validacion['fec_documento'],
                $idDocumento);
                $validacion['status']='actualizar';
            }


            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if($validacion[$titulo])
                    {
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
				
            }
            $validacion['idDocumento']=$id;
            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
				$this->atVista->assign('idDocumento',$idDocumento);


		           $conceptoDetalle = $this->atTipoDocumento->metMostrarOrganismosDetalle($idDocumento);
                     for ($i = 0; $i < count($conceptoDetalle); $i++)
             {
				if($conceptoDetalle[$i]['ind_organismo_externo']!="")
				{
                       $orgaextern[$conceptoDetalle[$i]['ind_organismo_externo']] =
                    array(
				 	'id' => $conceptoDetalle[$i]['ind_organismo_externo'],
                    'organismo' => $conceptoDetalle[$i]['ind_descripcion_empresa'],
                    'representanteor' => $conceptoDetalle[$i]['ind_representante_externo'],
                    'cargoor' => $conceptoDetalle[$i]['ind_cargo_externo'],
                     );
                     if (isset($orgaextern))
                     {
                       $this->atVista->assign('orgaextern', $orgaextern);
                     }
                     $this->atVista->assign('formDBDet', $conceptoDetalle);
                }
		     }
		         $depenDetalle = $this->atTipoDocumento->metMostrarDepenenciaDetalle($idDocumento);
                   for ($i = 0; $i < count($depenDetalle); $i++)
                {
				if($depenDetalle[$i]['ind_dependencia_externa']!="")
				{
				    $depextern[$depenDetalle[$i]['ind_dependencia_externa']] =
                        array(
				 	'id' => $depenDetalle[$i]['ind_dependencia_externa'],
                    'dependencia' => $depenDetalle[$i]['descipDependencia'],
                    'representantedep' => $depenDetalle[$i]['ind_representante_externo'],
                    'cargodep' => $depenDetalle[$i]['ind_cargo_externo'],
                );
			         if (isset($depextern))
			         {
                     $this->atVista->assign('depextern', $depextern);
                     }
                    $this->atVista->assign('formDBDit', $depenDetalle);
                }
                }
		
		$provDetalle = $this->atTipoDocumento->metMostrarproveedorDetalle($idDocumento);
            for ($i = 0; $i < count($provDetalle); $i++) {
		//	var_dump($this->atTipoDocumento->metMostrarproveedorDetalle($idDocumento)); 
		//	exit;
				
				if($provDetalle[$i]['ind_empresa_externa']!=""){
	
				 $proveedorext[$provDetalle[$i]['ind_empresa_externa']] = array(
				 	'id' => $provDetalle[$i]['ind_empresa_externa'],
                    'cedula' => $provDetalle[$i]['ind_documento_fiscal'],
                    'cargoprov' => $provDetalle[$i]['nomProveedor'],
					'representanteprov' => $provDetalle[$i]['ind_representante_externo'],
                );
		
			 if (isset($proveedorext)) {
                $this->atVista->assign('proveedorext', $proveedorext);
            }
            $this->atVista->assign('formDBprot', $provDetalle);
        }
		}
		
		$RepresDetalle = $this->atTipoDocumento->metMostrarRepresentDetalle($idDocumento);
            for ($i = 0; $i < count($RepresDetalle); $i++) {
				
				if($RepresDetalle[$i]['flag_r']!=""){
	
				 $flag_r[$RepresDetalle[$i]['flag_r']] = array(
				 	'id' => $RepresDetalle[$i]['flag_r'],
					'cedula' => $RepresDetalle[$i]['ind_cedula_documento'],
                    'particular' => $RepresDetalle[$i]['ind_representante_externo'],
                    'cargopart' => $RepresDetalle[$i]['cargorep'],
                );
		
			 if (isset($flag_r)) {
                $this->atVista->assign('flag_r', $flag_r);
            }
            $this->atVista->assign('formDBDwt', $RepresDetalle);
        }
		}
		
		
       }
		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$corresp=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('corresp',$corresp);
		
        $this->atVista->metRenderizar('CrearModificar','modales');
				
    }
	
	public function metVer()
    {
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');

       if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
			
			 	 $conceptoDetalle = $this->atTipoDocumento->metMostrarOrganismosDetalle($idDocumento);
            for ($i = 0; $i < count($conceptoDetalle); $i++) {
				
					if($conceptoDetalle[$i]['ind_organismo_externo']!=""){
					
                $orgaextern[$conceptoDetalle[$i]['ind_organismo_externo']] = array(
				 	'id' => $conceptoDetalle[$i]['ind_organismo_externo'],
                    'organismo' => $conceptoDetalle[$i]['ind_descripcion_empresa'],
                    'representanteor' => $conceptoDetalle[$i]['ind_representante_externo'],
                    'cargoor' => $conceptoDetalle[$i]['ind_cargo_externo'],
                );
			
            if (isset($orgaextern)) {
                $this->atVista->assign('orgaextern', $orgaextern);
            }

            $this->atVista->assign('formDBDet', $conceptoDetalle);
        }
		}
		
		 $depenDetalle = $this->atTipoDocumento->metMostrarDepenenciaDetalle($idDocumento);
            for ($i = 0; $i < count($depenDetalle); $i++) {
				
				if($depenDetalle[$i]['ind_dependencia_externa']!=""){
	
				 $depextern[$depenDetalle[$i]['ind_dependencia_externa']] = array(
				 	'id' => $depenDetalle[$i]['ind_dependencia_externa'],
                    'dependencia' => $depenDetalle[$i]['descipDependencia'],
                    'representantedep' => $depenDetalle[$i]['ind_representante_externo'],
                    'cargodep' => $depenDetalle[$i]['ind_cargo_externo'],
                );
		
			 if (isset($depextern)) {
                $this->atVista->assign('depextern', $depextern);
            }
            $this->atVista->assign('formDBDit', $depenDetalle);
        }
		}
			
			$provDetalle = $this->atTipoDocumento->metMostrarproveedorDetalle($idDocumento);
            for ($i = 0; $i < count($provDetalle); $i++) {
				
				if($provDetalle[$i]['ind_empresa_externa']!=""){
	
				 $proveedorext[$provDetalle[$i]['ind_empresa_externa']] = array(
				 	'id' => $provDetalle[$i]['ind_empresa_externa'],
                    'cedula' => $provDetalle[$i]['ind_documento_fiscal'],
                    'cargoprov' => $provDetalle[$i]['nomProveedor'],
					'representanteprov' => $provDetalle[$i]['ind_representante_externo'],
                );
		
			 if (isset($proveedorext)) {
                $this->atVista->assign('proveedorext', $proveedorext);
            }
            $this->atVista->assign('formDBprot', $provDetalle);
        }
		}
		
				$RepresDetalle = $this->atTipoDocumento->metMostrarRepresentDetalle($idDocumento);
            for ($i = 0; $i < count($RepresDetalle); $i++) {
				
				if($RepresDetalle[$i]['flag_r']!=""){
	
				 $flag_r[$RepresDetalle[$i]['flag_r']] = array(
				 	'id' => $RepresDetalle[$i]['flag_r'],
					'cedula' => $RepresDetalle[$i]['ind_cedula_documento'],
                    'particular' => $RepresDetalle[$i]['nombre_particular'],
                    'cargopart' => $RepresDetalle[$i]['cargorep'],
                );
		
			 if (isset($flag_r)) {
                $this->atVista->assign('flag_r', $flag_r);
            }
            $this->atVista->assign('formDBDwt', $RepresDetalle);
        }
		}
		

       }

		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$corresp=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('corresp',$corresp);
		
		 $this->atVista->assign('ver', 1);
        $this->atVista->metRenderizar('anular','modales');
    }
	
	public function metVerAnular()
    {
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');

       if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
			
				 $conceptoDetalle = $this->atTipoDocumento->metMostrarOrganismosDetalle($idDocumento);
            for ($i = 0; $i < count($conceptoDetalle); $i++) {
				
					if($conceptoDetalle[$i]['ind_organismo_externo']!=""){
					
                $orgaextern[$conceptoDetalle[$i]['ind_organismo_externo']] = array(
				 	'id' => $conceptoDetalle[$i]['ind_organismo_externo'],
                    'organismo' => $conceptoDetalle[$i]['ind_descripcion_empresa'],
                    'representanteor' => $conceptoDetalle[$i]['ind_representante_externo'],
                    'cargoor' => $conceptoDetalle[$i]['ind_cargo_externo'],
                );
			
            if (isset($orgaextern)) {
                $this->atVista->assign('orgaextern', $orgaextern);
            }

            $this->atVista->assign('formDBDet', $conceptoDetalle);
        }
		}
		
		 $depenDetalle = $this->atTipoDocumento->metMostrarDepenenciaDetalle($idDocumento);
            for ($i = 0; $i < count($depenDetalle); $i++) {
				
				if($depenDetalle[$i]['ind_dependencia_externa']!=""){
	
				 $depextern[$depenDetalle[$i]['ind_dependencia_externa']] = array(
				 	'id' => $depenDetalle[$i]['ind_dependencia_externa'],
                    'dependencia' => $depenDetalle[$i]['descipDependencia'],
                    'representantedep' => $depenDetalle[$i]['ind_representante_externo'],
                    'cargodep' => $depenDetalle[$i]['ind_cargo_externo'],
                );
		
			 if (isset($depextern)) {
                $this->atVista->assign('depextern', $depextern);
            }
            $this->atVista->assign('formDBDit', $depenDetalle);
        }
		}
		
		$provDetalle = $this->atTipoDocumento->metMostrarproveedorDetalle($idDocumento);
            for ($i = 0; $i < count($provDetalle); $i++) {
				
				if($provDetalle[$i]['ind_empresa_externa']!=""){
	
				 $proveedorext[$provDetalle[$i]['ind_empresa_externa']] = array(
				 	'id' => $provDetalle[$i]['ind_empresa_externa'],
                    'cedula' => $provDetalle[$i]['ind_documento_fiscal'],
                    'cargoprov' => $provDetalle[$i]['nomProveedor'],
					'representanteprov' => $provDetalle[$i]['ind_representante_externo'],
                );
		
			 if (isset($proveedorext)) {
                $this->atVista->assign('proveedorext', $proveedorext);
            }
            $this->atVista->assign('formDBprot', $provDetalle);
        }
		}
		
				$RepresDetalle = $this->atTipoDocumento->metMostrarRepresentDetalle($idDocumento);
            for ($i = 0; $i < count($RepresDetalle); $i++) {
				
				if($RepresDetalle[$i]['flag_r']!=""){
	
				 $flag_r[$RepresDetalle[$i]['flag_r']] = array(
				 	'id' => $RepresDetalle[$i]['flag_r'],
					'cedula' => $RepresDetalle[$i]['ind_cedula_documento'],
                    'particular' => $RepresDetalle[$i]['nombre_particular'],
                    'cargopart' => $RepresDetalle[$i]['cargorep'],
                );
		
			 if (isset($flag_r)) {
                $this->atVista->assign('flag_r', $flag_r);
            }
            $this->atVista->assign('formDBDwt', $RepresDetalle);
        }
		}
		
       }

		
		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$corresp=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('corresp',$corresp);
		
		 $this->atVista->assign('veranular', 1);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }
	
		public function metanular()
    {
	
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
         $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int');
            $txt = $this->metValidarFormArrayDatos('form', 'txt');
			
          if ($alphaNum != null && $ind == null && $txt==null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $validacion = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            }else{
                $validacion = array_merge($ind, $txt,$alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']='Anulado';
            }
            if  ($idDocumento!=0)
			{
                $id=$this->atTipoDocumento->metanularTipoDocumento(
				$validacion['ind_motivo_anulado'],$validacion['ind_persona_anulado'],$validacion['ind_estado'],$idDocumento);
				
                $validacion['status']='anular';
            }
       
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
				
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
				
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);

       }

		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$corresp=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('corresp',$corresp);
		
        $this->atVista->metRenderizar('anular','modales');
    }
	
  public function metdocumento()
    {

        $idDocumento=$this->metObtenerInt('idDocumento');

        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);

       }
		
        $this->atVista->metRenderizar('documento','modales');
    }
	
 }

