<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Planificación fiscal
 * PROCESO: Ingreso y mantenimiento de categorías de entes.
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 3 |          Alexis Ontiveros                 |  ontiveros.alexis@cmldc.gob.ve     |         0426-5144382           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #3                      |        26-01-2017       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modPF' . DS . 'modelos' . DS . 'funcionesGenerales.php';
class categoriaEnteControlador extends Controlador
{
    private $atCategoriaModelo;
    public $atFuncGnrles;/*Funciones generales*/
    public function __construct()
    {

        parent::__construct();
        $this->atCategoriaModelo=$this->metCargarModelo('categoriaEnte');
        $this->atFuncGnrles = new funcionesGenerales();
    }
    public function metIndex(){
        $result=$this->atCategoriaModelo->metListarTiposEnte(); //Se busca los tipos de entes
        $this->atVista->assign('listadoTipoEnte',$result);
        $this->atVista->metRenderizar('listado');
    }
    /**
     * Busca las categorías de entes y las monta en la grilla.
     */
    public function metListarCategoria(){
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $Exceccion_int=array('id_tipoente');
        $arrParams=$this->metValidarFormArrayDatos('form', 'int',$Exceccion_int);
        $result=$this->atCategoriaModelo->metConsultaCategorias('por_listado',$arrParams);
        if(count($result)>0){
            $this->atVista->assign('listado',$result);
            $this->atVista->metRenderizar('resultadoListado');
        }else{
            echo false;
        }
    }
    /**
     * Monta los datos de l cargo en el form modal para modificar
     */
    public function metMontaCategoria(){
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarJs($js);
        $idCategoria=$this->metObtenerInt('idCategoria');
        $result=$this->atCategoriaModelo->metMostrarCategoria($idCategoria);
        if($result){
            $resultTiposEnte=$this->atCategoriaModelo->metListarTiposEnte(); //Se busca los tipos de entes
            $this->atVista->assign('listadoTipoEnte',$resultTiposEnte);
            $this->atVista->assign('formDB',$result);
            $this->atVista->metRenderizar('CrearModificar','modales');
        }else{
            echo false;
        }
    }
    /**
     * Crea ó modifica un registro
     */
    public function metCrearModificar()
    {
        $valido=$this->metObtenerInt('valido');
        $js[] = 'modPF/funcionesModPf';
        $this->atVista->metCargarJs($js);
        if($valido==1){
            $this->metValidarToken();
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $Excceccion=array('idCategoria');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $arrParams=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $arrParams=$ind;
            }else{
                $arrParams=array_merge($alphaNum,$ind);
            }
            if(in_array('error',$arrParams)){
                if($arrParams['idCategoria']=="error"){
                    $arrParams['idCategoria']=0;
                }
                $arrParams['status']='error';
                echo json_encode($arrParams); exit;
            }
            $idCategoria=$arrParams['idCategoria']; $error=false;
            if($idCategoria==0){/*Se ingresa un registro*/
                $result=$this->atCategoriaModelo->metConsultaCategorias('valida_ingreso',$arrParams);/*Se valida si existe*/
                if(COUNT($result)==0){
                    $idCategoria=$this->atCategoriaModelo->metIngresaCategoria($arrParams);
                    if($idCategoria){
                        $arrResul['mensaje'] = 'El proceso se ejecutó exitosamente';
                        $arrResul['status'] = 'nuevo';
                    }else{
                        $arrResul['mensaje'] = 'Falló el ingreso intente mas tarde';
                        $error=true;
                    }
                }else{
                    $error=true;
                    $arrResul['mensaje'] = 'Ese categoría ya existe';
                }
            }else{/*Se actualiza un registro*/
                $result=$this->atCategoriaModelo->metConsultaCategorias('valida_modificar',$arrParams);/*Se valida si existe*/
                $arrResul['status'] = 'modificar';
                if(COUNT($result)==0) {
                    $result = $this->atCategoriaModelo->metModificarCategoria($arrParams);
                    $arrResul['reg_afectado']=$result;
                    if ($result) {
                        $arrResul['mensaje'] = 'El proceso se ejecutó exitosamente';
                    } else {
                        $error = true;
                        $arrResul['mensaje'] = 'El proceso se no se ejecutó. Intente mas tarde';
                    }
                }else{
                    $arrResul['reg_afectado']=false; $error = true;
                    $arrResul['mensaje'] = 'Ese registro ya existe';
                }
            }
            if($error){
                $arrResul['status']='errorSQL';
            }else{
                $arrResul['idCategoria']=$idCategoria;
            }
            echo json_encode($arrResul);
        }else{
            $result=$this->atCategoriaModelo->metListarTiposEnte(); //Se busca los tipos de entes
            $this->atVista->assign('listadoTipoEnte',$result);
            $this->atVista->metRenderizar('CrearModificar','modales');
        }
    }
    /**
     * Ejecuta el proceso de Eliminar un registro
     */
    public function metEliminarCategiria(){
        $idCategoria = $this->metObtenerInt('idCategoria');
        $reg_afectado=$this->atCategoriaModelo->metEliminaCategoria($idCategoria);
        if(is_array($reg_afectado)){
            $error=$reg_afectado; $retornar['reg_afectado']=false;
            if($error[1]==1451){
                $retornar['mensaje']="Imposible de eliminar ese registro. ¡Está relacionado!";
            }else{
                $retornar['mensaje']="Se a producido un error. Intente mas tarde";
            }
        }else{
            $retornar['reg_afectado']=$reg_afectado;
            if($reg_afectado){
                $retornar['mensaje']="El proceso se efectuó exitosamente";
            }else{
                $retornar['mensaje']="El proceso no se ejecutó. Intente mas tarde";
            }
        }
        echo json_encode($retornar);
    }
}
?>