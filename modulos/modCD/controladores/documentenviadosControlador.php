<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Salida de Documentos Externos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class documentenviadosControlador extends Controlador
{
    private $atTipoDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDocumento=$this->metCargarModelo('documentenviados');
		
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumentoEnviados($usuario));
        $this->atVista->metRenderizar('listado');
    }
	
 public function metProveedor($proveedor)
    {
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaRepresentante());
        $this->atVista->assign('tipoProveedor', $proveedor);
        $this->atVista->metRenderizar('proveedor', 'modales');
    }
	
		 public function metPersona($persona)
    {
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaPersona());
        $this->atVista->assign('tipoPersona', $persona);
        $this->atVista->metRenderizar('persona', 'modales');
    }
	
	
    public function metCrearModificar()
    {
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		//$this->atVista->metCargarJs($js);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
             $Excceccion=array('ind_estado','ind_cargo_remitente','ind_plazo_atencion','ind_anexo','txt_descripcion_anexo','fec_mes','fec_annio',
			 'ind_dependencia_remitente','ind_persona_remitente','ind_cargo_remitente','fk_a001_num_organismo','ind_con_atencion');
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            //$ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null){
                $validacion=$alphaNum;
            }else{
                $validacion=array_merge($alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
        
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }

           
            if($idDocumento==0){
                $id=$this->atTipoDocumento->metCrearTipoDocumento(
				$validacion['ind_documento_completo'],$validacion['ind_dependencia_remitente'],$validacion['ind_persona_remitente'],
				$validacion['ind_cargo_remitente'],$validacion['ind_asunto'],$validacion['ind_descripcion'],$validacion['ind_plazo_atencion'],$validacion['fec_documento'],
				$validacion['ind_estado'],$validacion['fec_mes'],$validacion['fec_annio'],
				$validacion['fk_cdc003_num_tipo_documento'],$validacion['fk_a001_num_organismo']);
				
                $validacion['status']='nuevo';
				
            }else 
			{
                $id=$this->atTipoDocumento->metModificarTipoDocumento(
				$validacion['ind_documento_completo'],$validacion['ind_dependencia_remitente'],$validacion['ind_persona_remitente'],
				$validacion['ind_cargo_remitente'],$validacion['ind_asunto'],$validacion['ind_descripcion'],$validacion['ind_plazo_atencion'],$validacion['fec_documento'],
				$validacion['ind_estado'],$validacion['fec_mes'],$validacion['fec_annio'],
				$validacion['fk_cdc003_num_tipo_documento'],$validacion['fk_a001_num_organismo'],$idDocumento);
				
                $validacion['status']='actualizar';
            }
			
		        
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
				
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
				
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);

       }
		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$correspon=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('correspon',$correspon);
		
        $this->atVista->metRenderizar('CrearModificar','modales');
    }
	
	public function metVer()
    {
		$complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );
        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');

       if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);

       }

		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$correspon=$this->atTipoDocumento->metListarCorrespon();
        $this->atVista->assign('correspon',$correspon);
		
		 $this->atVista->assign('veranular', 1);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

  
 }

