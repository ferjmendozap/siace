<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Preparar Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class preparardocumenintControlador extends Controlador
{
    private $atTipoDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDocumento=$this->metCargarModelo('preparardocumenint');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumento($usuario));
        $this->atVista->metRenderizar('listado');
    }
	
	
	
    public function metCrearModificar()
    {
	$js = array(
          	'materialSiace/core/demo/DemoFormEditors'
       );

        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
			'ckeditor/ckeditor',
			'ckeditor/adapters/jquery'
        );
        $complementosCss = array(
			'bootstrap-datepicker/datepicker',
			
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
             $Excceccion=array('ind_estado','ind_media_firma','ind_ruta_doc');
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
           	$formula = $this->metValidarFormArrayDatos('form', 'formula', $Excceccion);
			
       	   if ($alphaNum != null && $formula == null) {
                $validacion = $formula;
            } elseif ($alphaNum == null && $formula != null) {
                $validacion = $formula;
            } else {
               $validacion = array_merge($formula, $alphaNum);
            }
			
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
        
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
			 
			if(!isset($validacion['ind_ruta_doc'])){
                $validacion['ind_ruta_doc']=NULL;
            }
	

		
        $destino =  "publico/imagenes/modCD/documentosEntrada/";
     //   $band = 0;
        $ind_ruta_pdf = $_POST['ind_ruta_doc'];
		$txt_contenido = $_POST["ckeditor"];
        if(!empty($_FILES["ind_ruta_pdf"]["name"]))
        {
            $pdf_f   = $_FILES["ind_ruta_pdf"]["name"];
            $ruta    = $destino.$pdf_f;

            if (!file_exists($ruta))//False si no existe
            {
                $resultado = move_uploaded_file($_FILES["ind_ruta_pdf"]["tmp_name"], $ruta);
                chmod($ruta, 0777);
                if ($resultado)
                {
				
                $ind_ruta_pdf = $pdf_f;
         }
						   }    //  --------------
	}
            $txt_contenido=str_replace("font-size:8px", "font-size:+8", $txt_contenido);
            $txt_contenido=str_replace("font-size:9px", "font-size:+9", $txt_contenido);
            $txt_contenido=str_replace("font-size:10px", "font-size:+10", $txt_contenido);
            $txt_contenido=str_replace("font-size:11px", "font-size:+11", $txt_contenido);
            $txt_contenido=str_replace("font-size:12px", "font-size:+12", $txt_contenido);
            $txt_contenido=str_replace("font-size:14px", "font-size:+14", $txt_contenido);
            $txt_contenido=str_replace("font-size:16px", "font-size:+16", $txt_contenido);
            $txt_contenido=str_replace("font-size:18px", "font-size:+18", $txt_contenido);
            $txt_contenido=str_replace("font-size:20px", "font-size:+20", $txt_contenido);
            $txt_contenido=str_replace("font-size:22px", "font-size:+22", $txt_contenido);
            $txt_contenido=str_replace("font-size:24px", "font-size:+24", $txt_contenido);
            $txt_contenido=str_replace("font-size:26px", "font-size:+26", $txt_contenido);
            $txt_contenido=str_replace("font-size:28px", "font-size:+28", $txt_contenido);
            $txt_contenido=str_replace("font-size:36px", "font-size:+36", $txt_contenido);
            $txt_contenido=str_replace("font-size:48px", "font-size:+48", $txt_contenido);
            $txt_contenido=str_replace("font-size:72px", "font-size:+72", $txt_contenido);
		
                $id=$this->atTipoDocumento->metModificarTipoDocumento($txt_contenido,$validacion['ind_media_firma'],$ind_ruta_pdf,$validacion['ind_estado'],$idDocumento);
				$validacion['status']='modificar';
				
		
         
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
				
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
				
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);

       }
		

		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$correspondencia=$this->atTipoDocumento->metListarCorrespondencia();
        $this->atVista->assign('correspondencia',$correspondencia);
		
		
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metEliminar()
    {
        $idDocumento = $this->metObtenerInt('idDocumento');
                    $id=$this->atTipoDocumento->metEliminarTipoDocumento($idDocumento);
           
                $valido=array(
                    'status'=>'ok',
                    'idDocumento'=>$id
                );

        echo json_encode($valido);
        exit;
    }
	/*
	 public function metJsonOrganismo()
    {
        $idOrganismo=$this->metObtenerInt('idOrganismo');
        $organismo = $this->atTipoDocumento->metJsonOrganismo($idOrganismo);
        echo json_encode($organismo);
        exit;
    }
	*/
}

