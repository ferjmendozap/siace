<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Documentos
 * PROCESO: Envio de Documentos Internos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Ana Hurtado                    |ahurtado@contraloriadebolivar.gob.ve   |         0416-6867197           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
 

class enviardocumentintControlador extends Controlador
{
    private $atTipoDocumento;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoDocumento=$this->metCargarModelo('enviardocumentint');

    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->assign('listado',$this->atTipoDocumento->metListarTipoDocumento($usuario));
        $this->atVista->metRenderizar('listado');
    }
	
	 public function metPersona($persona)
    {
        $this->atVista->assign('lista', $this->atTipoDocumento->metListaPersona());
        $this->atVista->assign('tipoPersona', $persona);
        $this->atVista->metRenderizar('persona', 'modales');
    }
	
    public function metCrearModificar()
    {

        $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $complementosCss = array(
			'bootstrap-datepicker/datepicker',
			
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		//$this->atVista->metCargarJs($js);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
             $Excceccion=array('ind_estado');
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            //$ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null){
                $validacion=$alphaNum;
            }else{
                $validacion=array_merge($alphaNum);
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }
            if($idDocumento==0){
                $id=$this->atTipoDocumento->metCrearTipoDocumento($validacion['fk_a003_num_persona']);
				
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoDocumento->metModificarTipoDocumento($validacion['fk_a003_num_persona'],$idDocumento);
				$validacion['status']='modificar';
            }
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idDocumento']=$id;
            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
		//var_dump($this->atTipoDocumento->metMostrarTipoDocumento($idDocumento)); 
		//exit;
       }
		
		$organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		
		$correspondencia=$this->atTipoDocumento->metListarCorrespondencia();
        $this->atVista->assign('correspondencia',$correspondencia);
		
        $this->atVista->metRenderizar('CrearModificar','modales');
    }
	
	public function formatoFecha($fecha,$formato,$separador){
        #1  2016-01-01 a 01-01-2016
        #0  01-01-2016 a 2016-01-01
        if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
            $dia=substr($fecha,8,2);
            $mes=substr($fecha,5,2);
            $anio=substr($fecha,0,4);
        } elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
            $dia=substr($fecha,0,2);
            $mes=substr($fecha,3,2);
            $anio=substr($fecha,6,4);
        } else {
            $dia=0;
            $mes=0;
            $anio=0;
        }
        if (checkdate($mes, $dia, $anio)==false){
            return "la fecha es incorrecta";
            exit;
        }
        if($formato==1) {
            $fecha =$dia.$separador.$mes.$separador.$anio;
        } else {
            $fecha =$anio.$separador.$mes.$separador.$dia;
        }
        return $fecha;
    }

	public function metImprimirMemorandum($idDocumento,$pk_num_documento)
    {
        $this->metObtenerLibreria('documentoInterno', 'modCD');
       # ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $formInt = $this->metObtenerInt('form', 'int');
        $formTxt = $this->metObtenerTexto('form', 'txt');
        if (!empty($formInt)) {
            foreach ($formInt as $tituloInt => $valorInt) {
                if (!empty($formInt[$tituloInt])) {
                    $validacion[$tituloInt] = $valorInt;
                } else {
                    $validacion[$tituloInt] = 'error';
                }
            }
        }
        if (!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }
        $this->atFPDF = new pdf('P', 'mm', 'LETTER', true, 'UTF-8', false);

        $consulta = $this->atTipoDocumento->metMostrarTipoDocumento($idDocumento);
        $distrib = $this->atTipoDocumento->metDocDist($pk_num_documento);
        $copy= $this->atTipoDocumento->metDocDistOnly($pk_num_documento);
        $copias =$copy['ind_con_copia'];
        $cuenta = $this->atTipoDocumento->metCountDist($pk_num_documento);
        $cc = $this->atTipoDocumento->metDoccc($pk_num_documento);


        define('DEPENDENCIA_REMITENTE', strtoupper($consulta['ind_dependencia']));
        define('FEC_DOC', $this->formatoFecha($consulta['fec_documento'], 1, '-'));
        define('ASUNTO', $consulta['ind_asunto']);
        define('NRO_DOC_REF', $consulta['ind_dependencia']);
        define('PERSONAREMIT', strtoupper($consulta['ind_persona_remitente']));
        define('NRO_DOC', $consulta['ind_documento_completo']);
        define('REPRESENTANTE', $consulta['ind_desc_representante']);
        define('CARG_REPREST', $consulta['ind_cargo_destinatario']);
        define('NOM_PROV', 1);
        define('CONTENIDO', $consulta['txt_contenido']);
        define('MEDIAFIRM', $consulta['ind_media_firma']);
        define('DEPENDENCIA', $consulta['ind_desc_dependencia']);
        define('DEPENDENCIAREMITENTE', strtoupper($consulta['ind_dependencia']));
        if ($consulta['ind_dependencia_destinataria'] != "0") {

            $cargo = utf8_decode($consulta['ind_cargo_destinatario']);

        } else {

            $cargo = utf8_decode($consulta['ind_cargo_destinatario']);
        }

        if ($consulta['ind_flag_encargaduria'] !=0) {
            $cargoEspecial = $consulta['cargo_especial'];
        } else {
            $cargoEspecial = $consulta['ind_cargo_remitente'];
        }
        if ($consulta['txt_descripcion_anexo'] != null) {
            $textoanexos = 'Anexo ';
        } else {
            $textoanexos = '';
        }

        if ($cuenta['cantidad'] >1) {
            $concopia = 'C/c: ';
        } else {
            $concopia = '';
        }

        if ($copias ==0 and  $cuenta >1) {
            $concopia = 'C/c: ';
            $destinatario = $consulta['ind_desc_representante'];
            $cargo= $consulta['ind_cargo_destinatario'];
            $dependencia= $consulta['ind_desc_dependencia'];
        }

        else if ($copias ==1) {

            $destinatario = $cc['ind_desc_representante'];
            $cargo= $cc['ind_cargo_destinatario'];
            $dependencia= $cc['ind_desc_dependencia'];
        }

        else if ($copias ==2) {

            $destinatario = $consulta['ind_desc_representante'];
            $cargo= $consulta['ind_cargo_destinatario'];
            $dependencia= $consulta['ind_desc_dependencia'];
        }
        else {

            $destinatario = $consulta['ind_desc_representante'];
            $cargo= $consulta['ind_cargo_destinatario'];
            $dependencia=$consulta['ind_desc_dependencia'];
        }

        #####dependencia
        if ($cargo =='JEFE DE DIVISIÓN DE AUTOMATIZACION INFORMATICA Y TELECOMUNICACIONES GRADO 99 PASO A') {

            $cargo= 'JEFE DE DIVISIÓN DE A.I.T.';

        }
        else if ($cargo =='CONTRALOR GRADO 99 PASO D')
        {
            $cargo='CONTRALOR PROVISIONAL DEL ESTADO SUCRE';
        }

        else if ($cargo=='DIRECTOR A DE CONTROL DE ADMINISTRACION DESCENTRAL')
        {
            $cargo='DIR. DE CONTROL DE LA ADMÓN. DESCENTRALIZADA';
        }


        if ($cargoEspecial =='JEFE DE DIVISIÓN DE AUTOMATIZACION INFORMATICA Y TELECOMUNICACIONES GRADO 99 PASO A') {

            $cargoEspecial= 'JEFE DE DIVISIÓN DE A.I.T.';

        }
        else if ($cargoEspecial =='CONTRALOR GRADO 99 PASO D')
        {
            $cargoEspecial='CONTRALOR PROVISIONAL DEL ESTADO SUCRE';
        }else if ($cargoEspecial =='DIRECTOR GRADO 99 PASO B')
        {
            $cargoEspecial='DIRECTOR';
        }


        ##fin dependencia
        $this->atFPDF->SetTitle('memorandum');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetFont('Helvetica', '', 10);
        $this->atFPDF->SetMargins(25, 55, 25, true);
        $this->atFPDF->SetHeaderMargin(50);
        $this->atFPDF->SetFooterMargin(40);
        $this->atFPDF->setImageScale(1.53);
        $this->atFPDF->SetAutoPageBreak(TRUE, 50);
        $this->atFPDF->AddPage();

        ##	primera pagina solamente
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Helvetica', 'B', 12);
        $this->atFPDF->SetXY(140, 31);
        $this->atFPDF->Cell(30, 25, 'N°:  ' . $consulta['ind_codinterno'] . '-' . $consulta['num_secuencia'] . '-' . $consulta['fec_annio'], 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 12);
        $this->atFPDF->SetXY(25, 30);
        $this->atFPDF->Cell(150, 45, 'MEMORANDUM', 0, 1, 'C', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(25, 30, '', 1, 1, 'L', 1);
        $this->atFPDF->Cell(30, 65, 'PARA', 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(40, 60);
        $this->atFPDF->MultiCell(160, 10, ': ' . $destinatario, 0, 'L', 0, 0);
        $this->atFPDF->SetXY(25, 30);
        $this->atFPDF->Cell(30, 65, '', 0, 0, 'L');
        $this->atFPDF->SetFont('Helvetica', '', 10);
        $this->atFPDF->SetXY(40, 30);
        $this->atFPDF->Cell(30, 75,'  '.  $cargo , 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->Ln(10);
        $this->atFPDF->SetXY(25, 30);
        $this->atFPDF->Cell(30, 85, 'DE', 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(40, 30);
        $this->atFPDF->Cell(30, 85, ': ' . strtoupper($consulta['ind_persona_remitente']), 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', '', 10);
        $this->atFPDF->SetXY(40, 30);
        $this->atFPDF->Cell(30, 95,'  '.  DEPENDENCIAREMITENTE , 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(25, 30);
        $this->atFPDF->Cell(30, 105, 'FECHA', 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(40, 30);
        $this->atFPDF->Cell(30, 105, ' : ' . FEC_DOC, 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(25, 30);
        $this->atFPDF->Cell(30, 115, 'ASUNTO', 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(40,85);
        $this->atFPDF->MultiCell(150, 10, ': ' . strtoupper(ASUNTO), 0, 'L', 0, 0);
        $this->atFPDF->Ln(2);
        $this->atFPDF->SetFont('Helvetica', '', 12);
        $this->atFPDF->SetXY(25, 95);
        $this->atFPDF->MultiCell(20, 2,
        $this->atFPDF->writeHTML($consulta['txt_contenido'], true, false, true, false, 'J'), 0, 'J', 0, 0);
        $y = $this->atFPDF->GetY();


        if($y<200) {
            $this->atFPDF->Ln(10);
            $this->atFPDF->MultiCell(160, 4, 'Atentamente , ', 0, 'C', 0, 0);
            $this->atFPDF->Ln(17);
            $this->atFPDF->SetFont('Helvetica', '', 10);
            $this->atFPDF->MultiCell(160, 10, strtoupper($consulta['ind_persona_remitente']), 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(160, 8, $cargoEspecial, 0, 'C', 0, 0);
            $this->atFPDF->Ln(5);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(14,9,$consulta['ind_media_firma'],0,'L',0,0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '',9);
            $this->atFPDF->MultiCell(30,9,$textoanexos.$consulta['txt_descripcion_anexo'],0,'L',0,0);

        }
        else
        {
            $this->atFPDF->AddPage();
            $this->atFPDF->Ln(10);
            $this->atFPDF->MultiCell(160, 8, 'Atentamente , ', 0, 'C', 0, 0);
            $this->atFPDF->Ln(14);
            $this->atFPDF->SetFont('Helvetica', '', 10);
            $this->atFPDF->MultiCell(160, 12, strtoupper($consulta['ind_persona_remitente']), 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(160, 8, $cargoEspecial, 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(14,14,$consulta['ind_media_firma'],0,'L',0,0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '',9);
            $this->atFPDF->MultiCell(30,14,$textoanexos.$consulta['txt_descripcion_anexo'],0,'L',0,0);
        }


        if ($cuenta['cantidad'] >1)
        {

            $depCopia = '';
            foreach($distrib AS $titulo=>$valor){
              $depCopia .= $distrib[$titulo]['ind_desc_dependencia'].' / ';
            }

        } else {
            $depCopia = '';
        }

        /*$y = $this->atFPDF->GetY();
        if($y>205){
            $this->atFPDF->AddPage();
        }*/

        if ($cuenta['cantidad'] >1)
        {
            $this->atFPDF->SetFont('Helvetica', '', 8);
            $this->atFPDF->MultiCell(160, 9, $concopia. $depCopia, 0, 'L', 0, 0);


        } else {
            $this->atFPDF->Ln(8);
            $this->atFPDF->SetFont('Helvetica', '', 8);
            $this->atFPDF->MultiCell(160, 9,'', 0, 'L', 0, 0);
        }
       /* $this->atFPDF->Ln(7);
        $this->atFPDF->SetFont('Helvetica', '', 9);
        $this->atFPDF->MultiCell(14,14,$consulta['ind_media_firma'],0,'L',0,0);

        $this->atFPDF->Ln(4);
        $this->atFPDF->SetFont('Helvetica', '',9);
        $this->atFPDF->MultiCell(30,14,$textoanexos.$consulta['txt_descripcion_anexo'],0,'L',0,0);
*/





// add a page




// add a page


        $this->atFPDF->Output('memo.pdf', 'I');

    }

    public function metImprimirMemorandumCopia($idDocumento,$pk_num_documento)
    {
        $this->metObtenerLibreria('documentoInterno', 'modCD');
        #  ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $formInt = $this->metObtenerInt('form', 'int');
        $formTxt = $this->metObtenerTexto('form', 'txt');
        if (!empty($formInt)) {
            foreach ($formInt as $tituloInt => $valorInt) {
                if (!empty($formInt[$tituloInt])) {
                    $validacion[$tituloInt] = $valorInt;
                } else {
                    $validacion[$tituloInt] = 'error';
                }
            }
        }
        if (!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }

        $consulta = $this->atTipoDocumento->metMostrarTipoDocumento($idDocumento);
        $distrib = $this->atTipoDocumento->metDocDist($pk_num_documento);
        $copy= $this->atTipoDocumento->metDocDistOnly($pk_num_documento);
        $copias =$copy['ind_con_copia'];
        $cuenta = $this->atTipoDocumento->metCountDist($pk_num_documento);
        $cc = $this->atTipoDocumento->metDoccc($pk_num_documento);
$dep_remite=strtoupper($consulta['ind_dependencia']);

        define('DEPENDENCIA_REMITENTE', strtoupper($consulta['ind_dependencia']));
        define('FEC_DOC', $this->formatoFecha($consulta['fec_documento'], 1, '-'));
        define('ASUNTO', $consulta['ind_asunto']);
        define('NRO_DOC_REF', $consulta['ind_dependencia']);
        define('PERSONAREMIT', strtoupper($consulta['ind_persona_remitente']));
        define('NRO_DOC', $consulta['ind_documento_completo']);
        define('REPRESENTANTE', $consulta['ind_desc_representante']);
        define('CARG_REPREST', $consulta['ind_cargo_destinatario']);
        define('NOM_PROV', 1);
        define('CONTENIDO', $consulta['txt_contenido']);
        define('MEDIAFIRM', $consulta['ind_media_firma']);
        define('DEPENDENCIA', $consulta['ind_desc_dependencia']);


        if ($consulta['ind_dependencia_destinataria'] != "0") {

            $cargo = $consulta['ind_cargo_destinatario'];

        } else {

            $cargo = $consulta['ind_cargo_destinatario'];
        }

        if ($consulta['ind_flag_encargaduria']!= "0") {
            $cargoEspecial = $consulta['cargo_especial'];
        } else {
            $cargoEspecial = $consulta['ind_cargo_remitente'];
        }
        if ($consulta['txt_descripcion_anexo'] != null) {
            $textoanexos = 'Anexo ';
        } else {
            $textoanexos = '';
        }

        if ($cuenta['cantidad'] >1) {
            $concopia = 'c.c: ';
        } else {
            $concopia = '';
        }

        if ($copias ==0 and  $cuenta >1) {
            $concopia = 'c.c: ';
            $destinatario = $consulta['ind_desc_representante'];
            $cargo= $consulta['ind_cargo_destinatario'];
            $dependencia= $consulta['ind_desc_dependencia'];
        }

        else if ($copias ==1) {

            $destinatario = $cc['ind_desc_representante'];
            $cargo= $cc['ind_cargo_destinatario'];
            $dependencia= $cc['ind_desc_dependencia'];
        }

        else if ($copias ==2) {

            $destinatario = $consulta['ind_desc_representante'];
            $cargo= $consulta['ind_cargo_destinatario'];
            $dependencia= $consulta['ind_desc_dependencia'];
        }
        else {

            $destinatario = $consulta['ind_desc_representante'];
            $cargo= $consulta['ind_cargo_destinatario'];
            $dependencia=$consulta['ind_desc_dependencia'];
        }

        #####dependencia
        if ($cargo =='JEFE DE DIVISIÓN DE AUTOMATIZACION INFORMATICA Y TELECOMUNICACIONES GRADO 99 PASO A') {

            $cargo= 'JEFE DE DIVISIÓN DE A.I.T. GRADO 99 PASO A';

        }   else if ($cargo =='CONTRALOR GRADO 99 PASO D') {
            $cargo = 'CONTRALOR PROVISIONAL DE ESTADO SUCRE';
        }

        ##fin dependencia
        if ($cargoEspecial =='JEFE DE DIVISIÓN DE AUTOMATIZACION INFORMATICA Y TELECOMUNICACIONES GRADO 99 PASO A') {

            $cargoEspecial= 'JEFE DE DIVISIÓN DE A.I.T.';

        }
        else if ($cargoEspecial =='CONTRALOR GRADO 99 PASO D')
        {
            $cargoEspecial='CONTRALOR PROVISIONAL DE ESTADO SUCRE';
        }else if ($cargoEspecial =='DIRECTOR GRADO 99 PASO B')
        {
            $cargoEspecial='DIRECTOR';
        }



        $this->atFPDF = new pdf('P', 'mm', 'LETTER', true, 'UTF-8', false);
        $this->atFPDF->SetTitle('memorandum');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetFont('Helvetica', '', 10);
        $this->atFPDF->SetMargins(25, 55, 25, true);
        $this->atFPDF->SetHeaderMargin(50);
        $this->atFPDF->SetFooterMargin(40);
        $this->atFPDF->setImageScale(1.53);
        $this->atFPDF->SetAutoPageBreak(TRUE, 50);
        $this->atFPDF->AddPage();

        ##	primera pagina solamente
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Helvetica', 'B', 12);
        $this->atFPDF->SetXY(140, 31);
        $this->atFPDF->Cell(30, 25, 'N°:  ' . $consulta['ind_codinterno'] . '-' . $consulta['num_secuencia'] . '-' . $consulta['fec_annio'], 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 12);
        $this->atFPDF->SetXY(25, 30);
        $this->atFPDF->Cell(150, 45, 'MEMORANDUM', 0, 1, 'C', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(25, 30, '', 1, 1, 'L', 1);
        $this->atFPDF->Cell(30, 65, 'PARA', 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(40, 60);
        $this->atFPDF->MultiCell(160, 10, ': ' . $destinatario, 0, 'L', 0, 0);
        $this->atFPDF->SetXY(25, 30);
        $this->atFPDF->Cell(30, 65, '', 0, 0, 'L');
        $this->atFPDF->SetFont('Helvetica', '', 10);
        $this->atFPDF->SetXY(40, 30);
        $this->atFPDF->Cell(30, 75, ' ' . $cargo, 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(25, 30);
        $this->atFPDF->Cell(30, 85, 'DE', 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(40, 30);
        $this->atFPDF->Cell(30, 85, ': ' . DEPENDENCIA_REMITENTE, 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', '', 10);
        $this->atFPDF->SetXY(40, 30);
        $this->atFPDF->Cell(30, 95, ' ' . strtoupper($consulta['ind_persona_remitente']), 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(25, 30);
        $this->atFPDF->Cell(30, 105, 'FECHA', 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(40, 30);
        $this->atFPDF->Cell(30, 105, ': ' . FEC_DOC, 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(25, 30);
        $this->atFPDF->Cell(30, 115, 'ASUNTO', 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
        $this->atFPDF->SetXY(40,85);
        $this->atFPDF->MultiCell(150, 10, ': ' . strtoupper(ASUNTO), 0, 'L', 0, 0);
        $this->atFPDF->Ln(2);
        $this->atFPDF->SetFont('Helvetica', '', 12);
        $this->atFPDF->SetXY(30, 30);
     //   $this->atFPDF->Cell(30, 130, '__________________________________________________________________________', 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Helvetica', '', 12);
        $this->atFPDF->SetXY(25, 100);
        $this->atFPDF->MultiCell(20, 6,
            $this->atFPDF->writeHTML($consulta['txt_contenido'], true, false, true, false, 'J'), 0, 'J', 0, 0);
        $y = $this->atFPDF->GetY();
        if($y<200){
            $this->atFPDF->Ln(12);
            $this->atFPDF->MultiCell(160, 4, 'Atentamente , ', 0, 'C', 0, 0);
            $this->atFPDF->Ln(15);
            $this->atFPDF->SetFont('Helvetica', '', 10);
            $this->atFPDF->MultiCell(160, 10, strtoupper($consulta['ind_persona_remitente']), 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(160, 8, $cargoEspecial, 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(14,5,$consulta['ind_media_firma'],0,'L',0,0);

            $this->atFPDF->Ln(2);
            $this->atFPDF->SetFont('Helvetica', '',9);
            $this->atFPDF->MultiCell(30,4,$textoanexos.$consulta['txt_descripcion_anexo'],0,'L',0,0);
        }
        else
        {
            $this->atFPDF->AddPage();
            $this->atFPDF->Ln(4);
            $this->atFPDF->MultiCell(160, 8, 'Atentamente , ', 0, 'C', 0, 0);
            $this->atFPDF->Ln(12);
            $this->atFPDF->SetFont('Helvetica', '', 10);
            $this->atFPDF->MultiCell(160, 10, strtoupper($consulta['ind_persona_remitente']), 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Times', '', 9);
            $this->atFPDF->MultiCell(160, 8, $cargoEspecial, 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '', 9);
            $this->atFPDF->MultiCell(14,5,$consulta['ind_media_firma'],0,'L',0,0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Helvetica', '',9);
            $this->atFPDF->MultiCell(30,5,$textoanexos.$consulta['txt_descripcion_anexo'],0,'L',0,0);
        }


        if ($cuenta['cantidad'] >1)
        {

            $depCopia = '';
            foreach($distrib AS $titulo=>$valor){
                $depCopia .= $distrib[$titulo]['ind_desc_dependencia'].' ';
            }

        } else {
            $depCopia = '';
        }

       /*$y = $this->atFPDF->GetY();
        if($y>235){
            $this->atFPDF->AddPage();
        }*/

        if ($cuenta['cantidad'] >1)
        {
            $this->atFPDF->SetX(25);
            $this->atFPDF->Ln(8);
            $this->atFPDF->SetFont('Helvetica', '', 8);
            $this->atFPDF->MultiCell(80, 4,$concopia. $depCopia, 0, 'L', 0, 0);


        } else {
            $this->atFPDF->SetFont('Helvetica', '', 8);
            $this->atFPDF->MultiCell(100, 4,'', 0, 'L', 0, 0);
        }





// add a page




// add a page


        $this->atFPDF->Output('memoramdumcopia.pdf', 'I');

    }




    public function metImprimirCredencial($idDocumento)
    {
     
	# ini_set('error_reporting', 'E_ALL & ~E_STRICT');
	 
		
		 $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfCredencial('P','mm','Letter');
        $this->atFPDF->AliasNbPages();
		$this->atFPDF->SetMargins(10, 10, 5);
        $this->atFPDF->SetAutoPageBreak(5, 40);
        $this->atFPDF->AddPage();
		
        $consulta= $this->atTipoDocumento->metMostrarTipoDocumento($idDocumento);
		$this->atFPDF->Dependencia($consulta['ind_dependencia']);
		
		
        define('FEC_DOC',$this->formatoFecha($consulta['fec_documento'],1,'-'));
        define('ASUNTO',$consulta['ind_asunto']);
        define('NRO_DOC_REF',$consulta['ind_dependencia']);
		 define('PERSONAREMIT',$consulta['ind_persona_remitente']);
		define('NRO_DOC',$consulta['ind_documento_completo']);
		define('REPRESENTANTE',$consulta['ind_desc_representante']);
		define('CARG_REPREST',$consulta['ind_cargo_destinatario']);
        define('NOM_PROV',1);
        define('CONTENIDO',$consulta['txt_contenido']);
        define('MEDIAFIRM',$consulta['ind_media_firma']);
	
		if($consulta['ind_dependencia_destinataria']!="0"){
          $dependenciadesti=$consulta['ind_desc_dependencia'];
		  $cargo=$consulta['ind_cargo_destinatario'];
        }
	     else{
	     $dependenciadesti=$consulta['ind_desc_representante'];
	     $cargo=$consulta['ind_cargo_destinatario'];
	         }
			if($consulta['ind_flag_encargaduria']!="0"){
            $cargoEspecial=$consulta['cargo_especial'];
             }
	         else
             {
	         $cargoEspecial=$consulta['ind_cargo_remitente'];
	          }
		
        ##	primera pagina solamente
		
		$this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
		
        $this->atFPDF->SetFont('Helvetica', 'B', 10);
		$this->atFPDF->SetXY(15, 40); $this->atFPDF->Cell(30, 25, utf8_decode('Nro:'), 0, 0, 'L');
		$this->atFPDF->SetXY(30, 40); $this->atFPDF->Cell(30, 25, $consulta['ind_documento_completo'], 0, 1, 'L', 0);
		 $this->atFPDF->SetFont('Helvetica', 'B', 10);
		$this->atFPDF->SetXY(95, 45); $this->atFPDF->Cell(30, 45, 'CREDENCIAL', 0, 1, 'C', 0);
		 $this->atFPDF->SetFont('Helvetica', 'B', 8);

		 $this->atFPDF->SetFont('Helvetica', '', 8);
		$this->atFPDF->SetXY(20, 90); 
		//$this->atFPDF->MultiCell(175, 5, utf8_decode($consulta['txt_contenido']));
		$this->atFPDF->MultiCell(175, 5,  $this->atFPDF->WriteHTML(utf8_decode($consulta['txt_contenido']), $this->atFPDF->GetX()+100, $this->atFPDF->GetY()), 0);
		$this->atFPDF->SetFont('Helvetica', '', 7);
		$this->atFPDF->SetXY(15, 190); $this->atFPDF->Cell(30, 5, $consulta['ind_media_firma'], 0, 1, 'L', 0);
		//$this->atFPDF->SetXY(15, 195); $this->atFPDF->Cell(30, 5, 'Cc:',0, 1, 'L', 0);
	//	$this->atFPDF->SetXY(20, 195); $this->atFPDF->Cell(30, 5, utf8_decode($copia), 0, 1, 'L', 0);
		 $this->atFPDF->SetFont('Helvetica', '', 9);
		$this->atFPDF->SetXY(95, 225); $this->atFPDF->Cell(35, 5, utf8_decode($consulta['ind_persona_remitente']), 0, 1, 'C', 0);
		$this->atFPDF->SetXY(70, 230); $this->atFPDF->Cell(85, 5, utf8_decode($cargoEspecial), 0, 1, 'C', 0);
		
        $this->atFPDF->Output();

    }
	
		    public function metImprimirPuntoCuenta($idDocumento)
    {
     
	 ini_set('error_reporting', 'E_ALL & ~E_STRICT');

		
	 	$usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('cabeceraEquipo','modCD');
        $pdf=new pdfPuntoCuenta('P','mm','Letter');
        $this->atFPDF->AliasNbPages();
		$this->atFPDF->SetMargins(10, 10, 5);
        $this->atFPDF->SetAutoPageBreak(5, 10);
        $this->atFPDF->AddPage();
		
        $consulta= $this->atTipoDocumento->metMostrarTipoDocumento($idDocumento);
		$this->atFPDF->Dependencia($consulta['ind_dependencia']);

		
        define('FEC_DOC',$this->formatoFecha($consulta['fec_documento'],1,'-'));
        define('ASUNTO',$consulta['ind_asunto']);
        define('NRO_DOC_REF',$consulta['ind_dependencia']);
		 define('PERSONAREMIT',$consulta['ind_persona_remitente']);
		define('NRO_DOC',$consulta['ind_documento_completo']);
		define('REPRESENTANTE',$consulta['ind_desc_representante']);
		define('CARG_REPREST',$consulta['ind_cargo_destinatario']);
        define('NOM_PROV',1);
        define('CONTENIDO',$consulta['txt_contenido']);
        define('MEDIAFIRM',$consulta['ind_media_firma']);
	
		if($consulta['ind_dependencia_destinataria']!="0"){
          $dependenciadesti=$consulta['ind_desc_dependencia'];
		  $cargo=$consulta['ind_cargo_destinatario'];
        }
	else{
	$dependenciadesti=$consulta['ind_desc_representante'];	
	$cargo=$consulta['ind_cargo_destinatario'];
	}
	
	if($consulta['ind_con_copia']!="0"){
          $copia=$consulta['ind_desc_dependencia'];
        }
		
		if($consulta['ind_flag_encargaduria']!="0"){
          $cargoEspecial=$consulta['cargo_especial'];
        }
	else{
	$cargoEspecial=$consulta['ind_cargo_remitente'];
	}
	
	
	if($consulta['ind_anexo']=="0"){
         $consulta['ind_anexo']='' ;
		$anexo="X";
        }
		
		if($consulta['ind_anexo']=="1"){
         $consulta['ind_anexo']='X' ;
		$anexo="";
        }
	

			
        ##	primera pagina solamente
		
		$this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetDrawColor(0, 0, 0);
		
        $this->atFPDF->SetFont('Times', 'B', 10);
		 $this->atFPDF->SetFont('Times', 'B', 10);
		$this->atFPDF->SetXY(95, 25); $this->atFPDF->Cell(30, 30, '', 0, 1, 'C', 0);
		//$this->atFPDF->Ln(2);
		
		$this->atFPDF->SetFont('Times', 'B', 8);
		$this->atFPDF->Cell(24, 14, utf8_decode('Presentado'), 1, 0, 'R');
		$this->atFPDF->Cell(12, 7, utf8_decode('A'), 1, 0, 'C');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(110, 7, utf8_decode($dependenciadesti), 1, 0, 'L');
		$this->atFPDF->SetFont('Times', 'B', 8);
		$this->atFPDF->Cell(46, 7, utf8_decode('NUMERO:'), 1, 0, 'L');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->SetXY(170,'55');$this->atFPDF->Cell(10, 7, $consulta['ind_documento_completo'], 0, 0, 'L');
		
		$this->atFPDF->SetXY(34,'62');
		$this->atFPDF->SetFont('Times', 'B', 8);
		$this->atFPDF->Cell(12, 7, utf8_decode('Por'), 1, 0, 'C'); 
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(110, 7, $consulta['ind_dependencia'], 1, 0, 'L');
		$this->atFPDF->SetFont('Times', 'B', 8);
		$this->atFPDF->Cell(46, 7, utf8_decode('FECHA:'), 1, 0, 'L');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->SetXY(170,'62');$this->atFPDF->Cell(10, 7, $consulta['fec_documento'], 0, 0, 'L');
		
		$this->atFPDF->SetXY(10,'70');
		$this->atFPDF->SetFont('Times', 'B', 8);
		$this->atFPDF->Cell(24, 7, utf8_decode('ASUNTO'), 1, 0, 'C'); 
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(168, 7, $consulta['ind_asunto'], 1, 0, 'L');
		
		$this->atFPDF->SetXY(10,'77');
		$this->atFPDF->SetFont('Times', 'B', 8);
		$this->atFPDF->Cell(192, 7, utf8_decode('PROPUESTA'), 1, 0, 'C'); 
		$this->atFPDF->SetXY(10,'85');
		$this->atFPDF->SetFont('Times', '', 8);

		$this->atFPDF->MultiCell(-114, 30, $this->atFPDF->WriteHTML(utf8_decode($consulta['txt_contenido']), 1), 0);
		
		$this->atFPDF->SetXY(10,'127');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(192, 7, utf8_decode('RESULTADO'), 1, 0, 'C'); 
		$this->atFPDF->SetFont('Times', 'B', 8);
		
		$this->atFPDF->SetXY(10,'134');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(48, 7, utf8_decode('APROBADO'), 1, 0, 'C'); 
		$this->atFPDF->Cell(48, 7, utf8_decode('NEGADO'), 1, 0, 'C');
		$this->atFPDF->Cell(48, 7, utf8_decode('DIFERIDO'), 1, 0, 'C');
		$this->atFPDF->Cell(48, 7, utf8_decode('VISTO'), 1, 0, 'C');
		
		$this->atFPDF->SetXY(10,'141');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(48, 7, '', 1, 0, 'C'); 
		$this->atFPDF->Cell(48, 7, '', 1, 0, 'C');
		$this->atFPDF->Cell(48, 7, '', 1, 0, 'C');
		$this->atFPDF->Cell(48, 7, '', 1, 0, 'C');
		
		$this->atFPDF->SetXY(10,'148');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(192, 7, utf8_decode('INSTRUCCIONES U OBSERVACIONES'), 1, 0, 'C'); 
		$this->atFPDF->SetXY(10,'155');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(192, 50, '', 1, 0, 'C'); 

		$this->atFPDF->SetXY(10,'205');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(48, 7, utf8_decode('REVISADO POR:'), 1, 0, 'L'); 
		$this->atFPDF->Cell(48, 7, utf8_decode('SELLO'), 1, 0, 'L');
		$this->atFPDF->Cell(48, 7, utf8_decode('CONFIMADO POR:'), 1, 0, 'L');
		$this->atFPDF->Cell(48, 7, utf8_decode('SELLO'), 1, 0, 'L');
		
		$this->atFPDF->SetXY(10,'212');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(48, 7, utf8_decode('FIRMA:'), 1, 0, 'C'); 
		$this->atFPDF->Cell(48, 21, '', 1, 0, 'L');
		$this->atFPDF->Cell(48, 7, utf8_decode('FIRMA:'), 1, 0, 'C');
		$this->atFPDF->Cell(48, 21, utf8_decode('SELLO'), 1, 0, 'L');
		
		$this->atFPDF->SetXY(10,'219');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(48, 7, utf8_decode(''), 1, 0, 'L'); 
		
		$this->atFPDF->SetXY(10,'226');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(48, 7, utf8_decode('FECHA:'), 1, 0, 'L'); 
		
		$this->atFPDF->Cell(48, 7, '', 0, 0, 'L');
		$this->atFPDF->Cell(48, 7, utf8_decode('FECHA'), 1, 0, 'L');
	
		$this->atFPDF->SetXY(10,'235');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(17, 14, utf8_decode('ANEXOS'), 1, 0, 'C');
		$this->atFPDF->Cell(7, 7, utf8_decode('SI'), 1, 0, 'C');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(7, 7,  $consulta['ind_anexo'], 1, 0, 'C');
		$this->atFPDF->SetFont('Times', '', 7);
		$this->atFPDF->Cell(161, 7, utf8_decode($consulta['txt_descripcion_anexo']), 1, 0, 'L');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->SetXY(170,'55');$this->atFPDF->Cell(10, 7, $consulta['ind_documento_completo'], 0, 0, 'L');
		//$this->atFPDF->Cell(24, 14, utf8_decode('Valor Unitario Bs.'), 1, 0, 'C');
	//	$this->Cell(15, 14, utf8_decode('Valor Unitario'), 1, 0, 'C');	
		//$this->atFPDF->Cell(30, 7, utf8_decode('Diferencia'), 1, 1, 'C');
		
		
		$this->atFPDF->SetXY(27,'242');
		$this->atFPDF->SetFont('Times', '', 7);
		$this->atFPDF->Cell(7, 7, utf8_decode('NO'), 1, 0, 'C'); 
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->Cell(7, 7,  $anexo, 1, 0, 'C');
		$this->atFPDF->SetFont('Times', 'B', 8);
		$this->atFPDF->Cell(161, 7, '', 1, 0, 'L');
		$this->atFPDF->SetFont('Times', '', 8);
		$this->atFPDF->SetXY(170,'62');$this->atFPDF->Cell(10, 7, $consulta['fec_documento'], 0, 0, 'L');


		
        $this->atFPDF->Output();

    }

    public function metMesLetras($mes){

        if(strlen($mes)==1){
            $mes="0".$mes;
        }
        $metMesLetras=array(
            '01'=>'Enero',
            '02'=>'Febrero',
            '03'=>'Marzo',
            '04'=>'Abril',
            '05'=>'Mayo',
            '06'=>'Junio',
            '07'=>'Julio',
            '08'=>'Agosto',
            '09'=>'Septiembre',
            '10'=>'Octubre',
            '11'=>'Noviembre',
            '12'=>'Diciembre'
        );

        return $metMesLetras[$mes];
    } // END FUNC


	  public function metImprimirCircular($idDocumento)
    {

        $this->metObtenerLibreria('documentoInterno','modCD');
        ini_set('error_reporting', 'E_ALL & ~E_STRICT');

        $formInt=$this->metObtenerInt('form','int');
        $formTxt=$this->metObtenerTexto('form','txt');


        if(!empty($formInt)){
            foreach ($formInt as $tituloInt => $valorInt) {
                if(!empty($formInt[$tituloInt])){
                    $validacion[$tituloInt]=$valorInt;
                }else{
                    $validacion[$tituloInt]='error';
                }
            }
        }
        if(!empty($formTxt)) {
            foreach ($formTxt as $tituloTxt => $valorTxt) {
                if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                    $validacion[$tituloTxt] = $valorTxt;
                } else {
                    $validacion[$tituloTxt] = 'error';
                }
            }
        }





        $consulta= $this->atTipoDocumento->metMostrarTipoDocumento($idDocumento);
        define('DEPENDENCIA_REMITENTE', strtoupper($consulta['ind_dependencia']));
        define('ASUNTO',$consulta['ind_asunto']);
        define('NRO_DOC_REF',$consulta['ind_dependencia']);
        define('PERSONAREMIT',strtoupper($consulta['ind_persona_remitente']));
        define('NRO_DOC',$consulta['ind_documento_completo']);
        define('REPRESENTANTE',$consulta['ind_desc_representante']);
        define('CARG_REPREST',$consulta['ind_cargo_destinatario']);
        define('NOM_PROV',1);
        define('CONTENIDO',$consulta['txt_contenido']);
        define('MEDIAFIRM',$consulta['ind_media_firma']);
        $dia=substr($consulta['fec_documento'],8,2);
        $mes=substr($consulta['fec_documento'],5,2);
        $anio=substr($consulta['fec_documento'],0,4);
        define('FECHA',$dia.' de '.$this->metMesLetras($mes).' de '.$anio);
        if($consulta['ind_dependencia_destinataria']!="0"){
            $dependenciadesti=$consulta['ind_desc_dependencia'];
            $cargo=$consulta['ind_cargo_destinatario'];

        }
        else{
            $dependenciadesti=$consulta['ind_desc_representante'];
            $cargo=$consulta['ind_cargo_destinatario'];
        }
        if($consulta['ind_con_copia']!="0"){
            $copia=$consulta['ind_desc_dependencia'];
        }

        if($consulta['ind_flag_encargaduria']!="0"){
            $cargoEspecial=$consulta['cargo_especial'];
        }
        else{
            $cargoEspecial=$consulta['ind_cargo_remitente'];

        }
        if($consulta['txt_descripcion_anexo']!=null)
        {
            $textoanexos='Anexo ';
        }
        else {
        $textoanexos = '';
    }
        $this->atFPDF = new pdf('P','mm','LETTER', true, 'UTF-8', false);
        $this->atFPDF->SetTitle('CIRCULAR');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetFont('Times', '', 10);
        $this->atFPDF->SetMargins(25, 60, 25,true);
        $this->atFPDF->SetHeaderMargin(50);
        $this->atFPDF->SetFooterMargin(50);
        $this->atFPDF->setImageScale(1.53);
        $this->atFPDF->SetAutoPageBreak(TRUE, 50);
        $this->atFPDF->AddPage();
        ##	primera pagina solamente
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetXY(148, 35);
        $this->atFPDF->SetFont('Times', '', 11);
        $this->atFPDF->Cell(30, 25,'Cumaná,'.' ' .FECHA, 0, 0, 'L');
        $this->atFPDF->SetFont('Times', 'B', 11);
        $this->atFPDF->SetXY(25,38);
        $this->atFPDF->Cell(30, 35, $consulta['ind_codinterno'].'-'.$consulta['num_secuencia'].'-'.$consulta['fec_annio'], 0, 1, 'L', 0);
        $this->atFPDF->SetFont('Times', 'B', 12);
        $this->atFPDF->SetXY(25, 40);
        $this->atFPDF->Cell(150, 45, 'CIRCULAR', 0, 1, 'C', 0);


        $this->atFPDF->SetFont('Times', '', 11);
        $this->atFPDF->SetXY(25, 80);
        $this->atFPDF->MultiCell(20,6,$this->atFPDF->writeHTML($consulta['txt_contenido'], true, false, true, false, 'J'),0,'C',0,0);
        ///  $this->atFPDF->Cell(30, 115,  $this->atFPDF->writeHTML($texto, true, false, true, false, 'C'),0, 1, 'L', 0);
        // writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
        $this->atFPDF->Ln(25);
        $y = $this->atFPDF->GetY();
        if($y<180) {

            $this->atFPDF->MultiCell(160, 15, 'Atentamente, ', 0, 'C', 0, 0);
            $this->atFPDF->Ln(15);
            $this->atFPDF->SetFont('Times', 'B', 11);
            $this->atFPDF->MultiCell(160, 10, strtoupper($consulta['ind_persona_remitente']), 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Times', '', 11);
            $this->atFPDF->MultiCell(160, 10, $cargoEspecial, 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Times', '', 8);
            $this->atFPDF->MultiCell(14, 14, $consulta['ind_media_firma'], 0, 'L', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Times', '', 8);
            $this->atFPDF->MultiCell(40, 20, $textoanexos . $consulta['txt_descripcion_anexo'], 0, 'C', 0, 0);
        }else{
            $this->atFPDF->AddPage();
            $this->atFPDF->MultiCell(160, 15, 'Atentamente, ', 0, 'C', 0, 0);
            $this->atFPDF->Ln(15);
            $this->atFPDF->SetFont('Times', 'B', 11);
            $this->atFPDF->MultiCell(160, 10, strtoupper($consulta['ind_persona_remitente']), 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Times', '', 11);
            $this->atFPDF->MultiCell(160, 10, $cargoEspecial, 0, 'C', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Times', '', 8);
            $this->atFPDF->MultiCell(14, 14, $consulta['ind_media_firma'], 0, 'L', 0, 0);
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetFont('Times', '', 8);
            $this->atFPDF->MultiCell(40, 20, $textoanexos . $consulta['txt_descripcion_anexo'], 0, 'C', 0, 0);
        }


        $this->atFPDF->Output();

    }
	
	
	
   	// M�todo que permite ver los archivos
	public function metVerArchivo()
	{
		
		 $complementosJs = array(
            'bootstrap-datepicker/bootstrap-datepicker',
        );
        $complementosCss = array(
			'bootstrap-datepicker/datepicker',
			
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
		//$this->atVista->metCargarJs($js);

		$valido=$this->metObtenerInt('valido');
        $idDocumento=$this->metObtenerInt('idDocumento');
        if($valido==1){
            $this->metValidarToken();
             $Excceccion=array('ind_estado');
			$alphaNum=$this->metValidarFormArrayDatos('form','alphaNum',$Excceccion);
            //$ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null){
                $validacion=$alphaNum;
            }else{
                $validacion=array_merge($alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
        
            if(!isset($validacion['ind_estado'])){
                $validacion['ind_estado']=0;
            }

            if($idDocumento==0){
                $id=$this->atTipoDocumento->metCrearTipoDocumento($validacion['fk_a003_num_persona']);
				
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoDocumento->metModificarTipoDocumento($validacion['fk_a003_num_persona'],$idDocumento);
				$validacion['status']='modificar';
            }

         
            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
				
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
				
            }
            $validacion['idDocumento']=$id;

            echo json_encode($validacion);
            exit;
        }


        if($idDocumento!=0){
            $this->atVista->assign('formDB',$this->atTipoDocumento->metMostrarTipoDocumento($idDocumento));
			$this->atVista->assign('idDocumento',$idDocumento);
		//var_dump($this->atTipoDocumento->metMostrarTipoDocumento($idDocumento)); 
		//exit;
		
		  }
		  
		$ruta = 'publico/imagenes/modCD/documentosEntrada/';
		$rutaArchivo = array(
			'ruta' => $ruta
		);
		$this->atVista->assign('ruta', $rutaArchivo);
	   $organismoint=$this->atTipoDocumento->metListarOrganismoInt();
        $this->atVista->assign('organismoint',$organismoint);
		$correspondencia=$this->atTipoDocumento->metListarCorrespondencia();
        $this->atVista->assign('correspondencia',$correspondencia);
		$this->atVista->metRenderizar('ver', 'modales');
		 
	}
}

