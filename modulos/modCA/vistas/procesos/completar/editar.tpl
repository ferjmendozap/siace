<form id="formAjax" action="{$_Parametros.url}modCA/procesos/completarCONTROL/EditarMET"  class="form" role="form" novalidate="novalidate">
<div class="modal-body">
	<input type="hidden" value="1" name="valido" />
	{if isset($form.pk_num_solicitud)}
		<input type="hidden" value="{$form.pk_num_solicitud}" name="pk_num_solicitud" id="pk_num_solicitud" />
	{/if}

	<div class="form-group floating-label">
		<input type="text" class="form-control" value="{$form.ind_equipo}"  name="ind_equipo" id="ind_equipo" readonly>
	</div>

	<div class="form-group floating-label">
		<input type="text" class="form-control" value="{$form.ind_detalles}"  name="ind_detalles" id="ind_detalles" readonly>
		<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Solicitud del usuario</label>
	</div>

	<div class="form-group floating-label">
		<input type="text" class="form-control" value="{$form.ind_detalles_supervisor}"  name="ind_detalles_supervisor" id="ind_detalles_supervisor" readonly>
		<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Indicaciones del supervisor</label>
	</div>

	<div class="form-group floating-label">
		<input type="text" class="form-control" value="{$form.ind_detalles_analista}"  name="ind_detalles_analista" id="ind_detalles_analista" readonly>
		<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Comentarios del analista</label>
	</div>

	<div class="form-group floating-label">
		<select id="fk_num_modalidad" name="fk_num_modalidad"
				class="select2-container form-control select2" required>
			<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
				{foreach item=us from=$modalidad}
					<option selected value="{$us.pk_num_modalidad}">{$us.ind_modalidad}</option>
				{/foreach}
		</select>
		<label for="fk_num_modalidad"><i class="md md-person"></i>Modalidad</label>
	</div>

	<div class="form-group floating-label">
		<select name="ind_tipo" id="ind_tipo"
				class="select2-container form-control select2" required>
			{foreach item=us from=$tipo}
				<option value="{$us.pk_num_tipo_soporte}">{$us.ind_soporte_realizado}</option>
			{/foreach}
		</select>
		<label for="ind_tipo">Tipo de solicitud</label>
	</div>

	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="ind_detalles_analista_final" id="ind_detalles_analista_final">
		<label for="regular2"><i class="glyphicon glyphicon-pencil"></i>Resumen de actividad</label>
	</div>


	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accionco"><i class=" md md-thumb-up" style="color: #ffffff;"></i> Completar</button>
	</div>
</div>
</form>



<script type="text/javascript">
	$("#formAjax").submit(function () {
		return false;
	});

	$(document).ready(function () {
		//validation rules
		$("#accionco").click(function (form) {
			$.post($("#formAjax").attr('action'), $("#formAjax").serialize(), function (dato) {
				$(document.getElementById('pk_num_solicitud'+dato['pk_num_solicitud'])).html('');
				swal("Registro Guardado", "completado satisfactoriamente", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
			}, 'json');
		});

	});





	var placeholder = "";



	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult(repo) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if (repo.description) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}
	function repoFormatSelection(repo) {
		return repo.full_name;
	}

	$("button[data-select2-open]").click(function () {
		$("#" + $(this).data("select2-open")).select2("open");
	});
</script>


