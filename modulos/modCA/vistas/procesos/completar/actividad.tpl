<form id="formAjax" action="{$_Parametros.url}modCA/procesos/completarCONTROL/ActividadMET" class="form floating-label" novalidate>
    <input type="hidden" value="1" name="valido"/>
    <div class="modal-body">
        <div class="row">
            <div style="text-align: center; background-color: #0aa49a; color: #ffffff;">
                <header>Información General</header>
            </div>

            {if isset($form.pk_num_solicitud)}
                <input type="hidden" value="{$form.pk_num_solicitud}" name="pk_num_solicitud" id="pk_num_solicitud" />
            {/if}

            <span class="clearfix"></span>
            <div class="form-group floating-label">
                <input type="text" class="form-control"  name="ind_actividad" id="ind_actividad">
                <label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Actividad</label>
            </div>

            <div class="form-group floating-label">
                <select name="ind_porcentaje_ejecucion" id="ind_porcentaje_ejecucion"	class="select2-container form-control select2" required>
                    <option value="25">25%</option>
                    <option value="50">50%</option>
                    <option value="75">75%</option>
                    <option value="100">100%</option>
                </select>
                <label for="ind_porcentaje_ejecucion"><i class="md md-person"></i>Valor</label>
            </div>


            <span class="clearfix"></span>

            <div class="modal-footer">

                <div align="right">
                    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
                    <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="action"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
                </div>

            </div>

</form>


<script type="text/javascript">
    $(document).ready(function () {
        //validation rules
        $("#formAjax").validate({
            submitHandler: function(form) {
                $.post($(form).attr('action'), $(form).serialize(),function(dato){
                    $(document.getElementById('datatable1')).append('<tr id="pk_num_solicitud'+dato['pk_num_solicitud']+'"><td>'+dato['pk_num_solicitud']+'</td><td>'+dato['fecha']+'</td><td>'+dato['equipo']+'</td><td>'+dato['ind_detalles']+'</td><td>'+dato['estatus']+'</td>' +
                            '{if in_array('CA-01',$_Parametros.perfil)}<td><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado una solicitud" titulo="¿Estás Seguro?" title="Eliminar Solicitud" mensaje="¿Estás seguro de eliminar la solicitud?" boton="si, Eliminar" pk_num_solicitud="'+dato['pk_num_solicitud']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
                    swal("Solicitud Guardada", "Solicitud guardada satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                },'json');
            }
        });
    });
    var placeholder = "";

    $( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
    $( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

    // @see https://github.com/ivaynberg/select2/commit/6661e3
    function repoFormatResult( repo ) {
        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if ( repo.description ) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }

    function repoFormatSelection( repo ) {
        return repo.full_name;
    }

    $( "button[data-select2-open]" ).click( function() {
        $( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
    });
</script>
