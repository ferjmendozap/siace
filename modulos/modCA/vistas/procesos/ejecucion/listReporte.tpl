<!--********************************************************************************************************************
* DEV: CONTRALORIA DE ESTADOS
* PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
* MODULO: Parque Automotor
* PROCESO: Listado de revisiones de vehículos
* PROGRAMADORES:
* | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
* | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
* |   |                                        |                                     |                                |
* |___|________________________________________|_____________________________________|________________________________|
*
* VERSION
*
* |          PROGRAMADOR                  |          FECHA          |       VERSION      |
* |               #1                      |        09-07-2015       |         1.0        |
* |                                       |                         |                    |
* |_______________________________________|_________________________|____________________|
*
************************************************************************************************************************
 -->
<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary">SOLICITUDES ASIGNADAS</h2></header>
    </div>
    <div class="col-md-12">
        <h5>Reporte en pdf de solitudes asignadas</h5>
    </div><!--end .col -->

</div><!--end .card -->

<div class="card   ">
    <div class="card-head">
        <header>Búsqueda</header>
    </div>

    <form   class="form" action="{$_Parametros.url}modCA/procesos/ejecucionCONTROL/salidaMET"   method="post" target="_blank">
        <div class="col-lg-4">
            <div class="form-group floating-label">
                <select name="pk_num_empleado" id="pk_num_empleado"
                        class="select2-container form-control select2">
                    <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                        {foreach item=us from=$usuario}
                            <option selected value="{$us.pk_num_empleado}">{$us.ind_nombre1} {$us.ind_apellido1}</option>
                        {/foreach}
                </select>
                <label for="pk_num_empleado"><i class="md md-person"></i> Empleado</label>
            </div>

            <select name="num_estatus" id="num_estatus"
                    class="select2-container form-control select2" required>
                <option value="0">Creado</option>
                <option value="1">Aprobado </option>
                <option value="2"> Revisado</option>
                <option value="3">Asignado </option>
                <option value="4">Ejecutado </option>
                <option value="5">Completado </option>
                <option value="6">Evaluado</option>
                <option value="7">Cerrado</option>
            </select>


        </div>

        <div class="form-group   col-lg-12   " align="center">

            <button type="submit" class="btn btn-info ink-reaction btn-raised"  >Buscar</button>

    </form>
  </div>

</div><!--end .card -->

<section class="style-default-bright">




    <script type="text/javascript">
        $(document).ready(function() {
            $('#modalAncho').css("width", "85%");
            $('#fechas').datepicker({ format: 'dd/mm/yyyy', language:'es', startDate:'0d' });
        });
    </script>
