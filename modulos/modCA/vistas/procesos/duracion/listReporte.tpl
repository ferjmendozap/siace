<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">
           Historico de las solicitudes
        </h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">

                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th>Fecha Solicitud</th>
                        <th>Detalles</th>
                        <th>Usuario</th>
                        <th>Aprobacion</th>
                        <th>Revision</th>
                        <th>Asignacion</th>
                        <th>Ejecucion</th>
                        <th>Culminacion</th>
                        <th>Evaluacion</th>
                        <th>Cierre</th>
                        <th>Estatus</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=i from=$listado}
                        <tr id="pk_num_solicitud{$i.pk_num_solicitud}">
                            <td>{$i.fecha_solicitud}</td>
                            <td>{$i.ind_detalles}</td>
                            <td>{$i.ind_nombre1} {$i.ind_apellido1}</td>
                            <td>{$i.fec_aprobado}</td>
                            <td>{$i.fec_revisado}</td>
                            <td>{$i.fec_asignado}</td>
                            <td>{$i.fec_ejecutado}</td>
                            <td>{$i.fec_culminado}</td>
                            <td>{$i.fec_evaluado}</td>
                            <td>{$i.fec_cerrado}</td>

                            <td>{$i.ind_estatus}</td>

                        </tr>
                    {/foreach}
                    </tbody>

                 <tr>
                        <th colspan="6">
                            <button  class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reported" descripcion="Generar Reporte de salida: preparados" data-toggle="modal" data-target="#formModal"  titulo="Listado de salidas">Reporte </button><br/>
                        </th>
                 </tr>

                </table>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    $(document).ready(function () {
        var $url = '{$_Parametros.url}modCA/procesos/solicitudCONTROL/SolicitudMET';
        $('#nuevosolx').click(function () {
            $('#modalAncho').css("width", "45%");
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url, '', function ($dato) {
                $('#ContenidoModal').html($dato);
            });
        });


        $('#reported').click(function () {
            $('#modalAncho').css( 'width', '95%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $('#ContenidoModal').html("<iframe src='{$_Parametros.url}modCA/procesos/duracionCONTROL/ImprimirSolicitudesMET' border='1' width='100%' height='440px'></iframe>");
        });
    });

</script>