<form id="formAjax" action="{$_Parametros.url}modCA/procesos/solicitudCONTROL/CrearMET" method="post" class="form form-validate floating-label" novalidate="novalidate">
<!--form action="{$_Parametros.url}modLG/admin/periodoCONTROL/crearModificarPeriodoMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate"-->
    <input type="hidden" name="valido" value="1"/>
    <input type="hidden" name="idSolicitud" value="{if isset($idSolicitud)}{$idSolicitud}{/if}"/>
    <div class="modal-body">
        <div class="row">
            <!--div class="col-lg-12">
                <div class="form-group" id="fk_rhb001_num_empleado_solicitanteError">
                    <select id="fk_rhb001_num_empleado_solicitante" name="form[int][fk_rhb001_num_empleado_solicitante]" class="form-control select2 select2-list">
                            <option value=""></option>
                            {foreach item=us from=$usuario}
                                <option value="{$us.pk_num_empleado}">{$us.ind_nombre1} {$us.ind_apellido1}</option>
                            {/foreach}
                    </select>
                    <label for="fk_rhb001_num_empleado_solicitante"><i class="md md-person"></i> Usuario</label>
                </div>
            </div-->
            <!--div class="col-lg-12">
                <div class="form-group" id="fk_a004_num_dependenciaError">
                    <select id="fk_a004_num_dependencia" name="form[int][fk_a004_num_dependencia]" class="form-control select2 select2-list">
                        <option value=""></option>
                        {foreach item=dep from=$dependencia}
                            <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                        {/foreach}
                    </select>
                    <label for="fk_a004_num_dependencia"><i class="md md-business"></i> Dependencia</label>
                </div>
            </div-->

            <div class="form-group floating-label" id="ind_equipoError">
                <input type="text" class="form-control" name="form[alphaNum][ind_equipo]" id="ind_equipo">
                <label for="ind_equipo"><i class="md-desktop-windows"></i> Equipo</label>
            </div>
            <div class="form-group floating-label" id="ind_direccionError">
                <input type="text" class="form-control" name="form[alphaNum][ind_direccion]" id="ind_direccion">
                <label for="ind_direccion"><i class="md-my-location"></i> Ubicación</label>
            </div>
            <div class="form-group floating-label" id="ind_detalleError">
                <input type="text" class="form-control" name="form[alphaNum][ind_detalle]" id="ind_detalle">
                <label for="ind_detalle"><i class="md-cast"></i> Detalles</label>
            </div>
        </div>

        <div class="modal-footer">
            <div align="left">
                <a href="javascript:myJsFunction('{$smarty.server.SERVER_NAME}','{$smarty.server.SERVER_ADDR}')">Click para ver información de su red</a>
            </div>

            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                    descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
            </button>
            &nbsp;&nbsp;
            <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span
                        class="glyphicon glyphicon-floppy-disk"></span> Guardar
            </button>
        </div>
</form>

<script language="JavaScript" type="text/javascript">
    {literal}
    function myJsFunction(name, ip){
        alert("Servidor\n" + name + "\n" + ip);
    }
    {/literal}
</script>
<script type="text/javascript">
    $("#formAjax").submit(function () {
        return false;
    });
    $('.select2').select2({ allowClear: true });

    $(document).ready(function () {
        var app = new  AppFunciones();
        //validation rules
        $("#accion").click(function () {
            $.post($("#formAjax").attr('action'), $("#formAjax").serialize(), function (dato) {
                var arrayCheck = [""];
                var arrayMostrarOrden = [''];
                if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato, dato['idSolicitud'], 'idSolicitud', arrayCheck, arrayMostrarOrden, 'La Solicitud fue guardada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                } else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                } else if(dato['status']=='errorSql'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son UNICOS');
                }
            }, 'json');
        });
    });


/*

    var placeholder = "";



    // @see https://github.com/ivaynberg/select2/commit/6661e3
    function repoFormatResult(repo) {
        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if (repo.description) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }
    function repoFormatSelection(repo) {
        return repo.full_name;
    }

    $("button[data-select2-open]").click(function () {
        $("#" + $(this).data("select2-open")).select2("open");
    });
    */
</script>
