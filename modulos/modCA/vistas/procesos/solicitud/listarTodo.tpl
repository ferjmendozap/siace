<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">
            Crear Solicitud
        </h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">

                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th>N°</th>
                        <th>Fecha</th>
                        <th>Equipo</th>
                        <th>Detalles</th>
                        <th>Comentario del Supervisor</th>
                        <th>Comentario del Analista</th>
                        <th>Estatus</th>
                        <th>Evaluacion</th>
                        <th>Modalidad</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=u from=$listado}
                        <tr id="pk_num_solicitud{$u.pk_num_solicitud}">
                            <td>{$u.pk_num_solicitud}</td>
                            <td>{$u.fecha_solicitud}</td>
                            <td>{$u.ind_descripcion}</td>
                            <td>{$u.ind_detalles}</td>
                            <td>{$u.ind_detalles_supervisor}</td>
                            <td>{$u.ind_detalles_analista}</td>
                            <td>{$u.ind_estatus}</td>
                            <td>{$u.ind_evaluacion}</td>
                            <td>{$u.ind_modalidad}</td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tr>
                        <th colspan="8">
                            <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                    descripcion="El usuario ha creado un pasillo" data-toggle="modal"
                                    data-target="#formModal" titulo="Registrar" id="nuevosolx"><i
                                        class="md md-create"></i>Nuevo
                            </button>
                        </th>
                    </tr>

                </table>
            </div>
        </div>
    </div>

</section>

<script>
    jQuery("#datatable1").DataTable({
        "bDestroy": true,
        "bPaginate": true,
        "bInfo": false,
        "bFilter": true,
        "bSort": true
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        var $url='{$_Parametros.url}modCA/procesos/solicitudCONTROL/SolicitudMET';
        $('#nuevosolx').click(function(){
            $('#modalAncho').css( "width", "75%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });


        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var pk_num_solicitud=$(this).attr('pk_num_solicitud');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCA/procesos/solicitudCONTROL/EliminarMET';
                $.post($url, { pk_num_caja: pk_num_caja},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('pk_num_solicitud'+$dato['pk_num_solicitud'])).html('');
                        swal("Eliminado!", "eliminado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });


    });

</script>
