<form action="{$_Parametros.url}modCA/procesos/solicitudCONTROL/{if isset($ap) }verMET{/if}" autocomplete="off" id="formAjax" class="form" role="form" method="post">



<div class="modal-body">
        <input type="hidden" value="1" name="valido" />
    {if isset($idSolicitud) }
  <input type="hidden" value="{$idSolicitud}" name="idSolicitud" id="idSolicitud"/>
        {/if}

    <input type="hidden" value="3" name="nuevoestatus" id="nuevoestatus"/>

    <div class="row">

            <div class="col-sm-12">

                <div class="form-group floating-label" id="ind_estadoError">
                    <input type="text" class="form-control" value="{if isset($formDB.pk_num_solicitud)}{$formDB.pk_num_solicitud}{/if}" disabled name="form[alphaNum][pk_num_solicitud]" id="pk_num_solicitud">
                    <label for="pk_num_solicitud"><i class="fa fa-eye"></i> Solicitud:</label>
                </div>


                <div class="form-group floating-label" id="fk_a004_num_dependencia">
                    <input type="text" class="form-control" value="{if isset($formDB.fk_a018_num_seguridad_usuario)}{$formDB.fk_a018_num_seguridad_usuario}{/if}" name="form[alphaNum][fk_a018_num_seguridad_usuario]" disabled id="fk_a018_num_seguridad_usuario">
                    <label for="fk_a018_num_seguridad_usuario"><i class="md md-person"></i> Funcionario Solicitante</label>
                </div>



                <div class="form-group floating-label" id="ind_detalles">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_detalles)}{$formDB.ind_detalles}{/if}" name="form[alphaNum][ind_detalles]" disabled id="ind_detalles">
                    <label for="ind_detalles"><i class="md md-person"></i> Detalles</label>
                </div>


                <div class="form-group floating-label" id="ind_equipo">
                    <input type="text" class="form-control" value="{if isset($formDB.ind_equipo)}{$formDB.ind_equipo}{/if}" name="form[alphaNum][ind_equipo]" disabled id="ind_equipo">
                    <label for="ind_equipo"><i class="md md-person"></i> equipo</label>
                </div>



</div>
    </div>



<span class="clearfix"></span>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">Cancelar</button>
    </div>
</form>


<script type="text/javascript">
    $(document).ready(function() {
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });

        $('#aprobar').click(function(){
            $.post($("#formAjax").attr("action"), { idSolicitudForm: $('#idSolicitud').val(), valido:1 },function(dato){
                if(dato['status']=='2'){
                    $(document.getElementById('idSolicitud'+dato['idSolicitud'])).remove();
                    swal("Aprobado!","Aprobado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
        $('#revisar').click(function(){
            $.post($("#formAjax").attr("action"), { idSolicitudForm: $('#idSolicitud').val(), valido:1 },function(dato){
                if(dato['status']=='3'){
                    $(document.getElementById('idSolicitud'+dato['idSolicitud'])).remove();
                    swal("Revisado!","Revisado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
        $('#asignar').click(function(){
            $.post($("#formAjax").attr("action"), { idSolicitudForm: $('#idSolicitud').val(), valido:1 },function(dato){
                if(dato['status']=='4'){
                    $(document.getElementById('idSolicitud'+dato['idSolicitud'])).remove();
                    swal("Asignado!","Asignado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
        $('#ejecutar').click(function(){
            $.post($("#formAjax").attr("action"), { idSolicitudForm: $('#idSolicitud').val(), valido:1 },function(dato){
                if(dato['status']=='5'){
                    $(document.getElementById('idSolicitud'+dato['idSolicitud'])).remove();
                    swal("Ejecutado!",  "Ejecutado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
        $('#completar').click(function(){
            $.post($("#formAjax").attr("action"), { idSolicitudForm: $('#idSolicitud').val(), valido:1 },function(dato){
                if(dato['status']=='6'){
                    $(document.getElementById('idSolicitud'+dato['idSolicitud'])).remove();
                    swal("Asignado!",  "Revisado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });

        $('#culminar').click(function(){
            $.post($("#formAjax").attr("action"), { idSolicitudForm: $('#idSolicitud').val(), valido:1 },function(dato){
                if(dato['status']=='7'){
                    $(document.getElementById('idSolicitud'+dato['idSolicitud'])).remove();
                    swal("Culminado!",  "Culminado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });



        $('#evaluar').click(function(){
            $.post($("#formAjax").attr("action"), { idSolicitudForm: $('#idSolicitud').val(), valido:1 },function(dato){
                if(dato['status']=='8'){
                    $(document.getElementById('idSolicitud'+dato['idSolicitud'])).remove();
                    swal("Evaluado!",  "Evaluado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });



        $('#cerrar').click(function(){
            $.post($("#formAjax").attr("action"), { idSolicitudForm: $('#idSolicitud').val(), valido:1 },function(dato){
                if(dato['status']=='2'){
                    $(document.getElementById('idSolicitud'+dato['idSolicitud'])).remove();
                    swal("Cerrado!",  "Cerrado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
    });
</script>


