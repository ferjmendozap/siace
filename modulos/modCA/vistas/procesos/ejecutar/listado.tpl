<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
			<h2 class="text-primary">&nbsp;Ejecutar Solicitud</h2>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr align="center">
                            <th>N° de Solicitud</th>
                            <th>Fecha Solicitud</th>
                            <th>Detalles del usuario</th>
                            <th>Indicación del supervisor</th>
                            <th>Equipo</th>
                            <th>Dependencia</th>
                            <th>Usuario</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_solicitud{$post.pk_num_solicitud}">
                            <td>{$post.pk_num_solicitud}</td>
                            <td>{$post.fecha_solicitud}</td>
                            <td>{$post.ind_detalles}</td>
                            <td>{$post.ind_detalles_supervisor}</td>
                            <td>
                                {if $u.ind_descripcion==''}
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-default"  title="Eliminar">No requiere</button>
                                {else}
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" title="Eliminar">{$u.ind_descripcion}</button>
                                {/if}
                            </td>
                            <td>{$post.dependencia}</td>
                                <td>{$post.nombres}</td>
                            <td><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud="{$post.pk_num_solicitud}" title="Ver Solicitud" titulo="Ver Solicitud" id="ver"><i class="fa fa-eye" style="color: #ffffff;"></i></button>
                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud="{$post.pk_num_solicitud}" descripcion="El Usuario ha Modificado un almacén" title="Ejecutar Solicitud"  titulo="Ejecutar Solicitud"><i class=" md md-work" style="color: #ffffff;"></i></button></td>
                        </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {
                var $url_modificar='{$_Parametros.url}modCA/procesos/ejecutarCONTROL/EditarMET';
                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		        $('#modalAncho').css( "width", "45%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $('#ContenidoModal').html("");
                $.post($url_modificar,{ pk_num_solicitud: $(this).attr('pk_num_solicitud')},function($dato){
                $('#ContenidoModal').html($dato);
                });
                });
            });

            var $urlVer='{$_Parametros.url}modCA/procesos/ejecutarCONTROL/VerMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "45%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_solicitud: $(this).attr('pk_num_solicitud')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
</script>
