<form id="formAjax" action="{$_Parametros.url}modCA/procesos/ejecutarCONTROL/EditarMET"  class="form" role="form" novalidate="novalidate">
<div class="modal-body">
	<input type="hidden" value="1" name="valido" />
	{if isset($form.pk_num_solicitud)}
		<input type="hidden" value="{$form.pk_num_solicitud}" name="pk_num_solicitud" id="pk_num_solicitud" />
	{/if}

	<div class="form-group floating-label">
		<input type="text" class="form-control" value="{$form.ind_detalles_supervisor}"  name="ind_detalles_supervisor" id="ind_detalles_supervisor" readonly>
		<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Indicaciones del supervisor</label>
	</div>
		<input type="hidden" class="form-control" value="{$form.fecha_solicitud}"  name="fecha_solicitud" id="fecha_solicitud" readonly>


	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="ind_detalles_analista" id="ind_detalles_analista" required>
		<label for="regular2"><i class="glyphicon glyphicon-pencil"></i> Descripción del analista</label>
	</div>

	<div class="input-group-content">
		<input type="text" class="form-control dirty" name="ind_fecha_inicio" id="ind_fecha_inicio"  value="{$fecha_actual}" readonly>
		<label><i class="glyphicon glyphicon-calendar"></i>Fecha de Inicio</label>
	</div>


	<span class="input-group-addon">a</span>
	<div class="input-group date" id="demo-date">
		<div class="input-group-content">
		<input type="text" class="form-control dirty" name="ind_fecha_fin" id="ind_fecha_fin">
		<label><i class="glyphicon glyphicon-calendar"></i>Fecha Fin</label>
		<div class="form-control-line"></div>
	</div>
	</div>

		<input type="hidden" value="{$form.ind_equipo}" class="form-control"  name="ind_equipo" id="ind_equipo">




	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accionej"><i class=" md md-work" style="color: #ffffff;"></i>Ejecutar</button>
	</div>
</div>
</form>



<script type="text/javascript">
	$("#formAjax").submit(function () {
		return false;
	});

	$(document).ready(function () {
		//validation rules
		$("#accionej").click(function (form) {
			$.post($("#formAjax").attr('action'), $("#formAjax").serialize(), function (dato) {
				$(document.getElementById('pk_num_solicitud'+dato['pk_num_solicitud'])).html('');
				swal("Registro Guardado", "ejecutado satisfactoriamente", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
			}, 'json');
		});



		$("#formAjax").validate({
			rules:{
				fechaInicio:{
					required:true
				},
				fechaFin:{
					required:true
				},
				"dependencia[]":{
					required:true,
					minlength: 1
				}
			},
			messages:{
				fechaInicio:{
					required: "La fecha de inicio es requerida"
				},
				fechaFin:{
					required: "La fecha final es requerida"
				},
				"dependencia[]": "Debe seleccionar al menos una dependencia"
			},
			errorPlacement: function(error, element) {
				if (element.attr("name") == "dependencia[]"){
					alert(error.text());
				}
				else{
					error.insertAfter(element);
				}
			}
		});


        $("#ind_fecha_inicio").datepicker({
            format:'yyyy-mm-dd',
            language:'es',
            autoclose: true

        });
        $("#ind_fecha_fin").datepicker({
            format:'yyyy-mm-dd',
            language:'es',
            autoclose: true

        });

	});






</script>


