<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">&nbsp;Control de tipos de solicitudes</h2>

    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th>Tipo Solicitud</th>
                        <th>Tipo de Soporte</th>
                       <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=post from=$_PruebaPost}
                        <tr id="pk_num_tipo_solicitud{$post.pk_num_tipo_solicitud}">
                            <td>{$post.ind_tipo_solicitud}</td>
                            <td>{$post.ind_soporte_realizado}</td>
                            <td><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_tipo_solicitud="{$post.pk_num_tipo_solicitud}" title="Ver Solicitud" titulo="Ver Solicitud" id="ver"><i class="fa fa-eye" style="color: #ffffff;"></i></button>
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_tipo_solicitud="{$post.pk_num_tipo_solicitud}" descipcion="El Usuario ha Modificado un tipo de solicitud" title="Actualizar tipo de Solicitud"  titulo="Actualizar tipo de Solicitud"> <i class="md md-view-list" style="color: #ffffff;"></i></button>

                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <td>
                    <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                            descripcion="El usuario ha creado un pasillo" data-toggle="modal"
                            data-target="#formModal" titulo="Registrar" id="nuevosolx"><i
                                class="md md-create"></i>Nuevo
                    </button>
                    </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {

        var $url='{$_Parametros.url}modCA/procesos/tiposolicitudCONTROL/SolicitudMET';
        $('#nuevosolx').click(function(){
            $('#modalAncho').css( "width", "75%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });


        var $url_modificar='{$_Parametros.url}modCA/procesos/tiposolicitudCONTROL/EditarMET';
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#modalAncho').css( "width", "45%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url_modificar,{ pk_num_tipo_solicitud: $(this).attr('pk_num_tipo_solicitud')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });
    var $urlVer='{$_Parametros.url}modCA/procesos/tiposolicitudCONTROL/VerMET';
    $('#datatable1 tbody').on( 'click', '.ver', function () {
        $('#modalAncho').css( "width", "45%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        $.post($urlVer,{ pk_num_tipo_solicitud: $(this).attr('pk_num_tipo_solicitud')},function($dato){
            $('#ContenidoModal').html($dato);
        });
    });
</script>
