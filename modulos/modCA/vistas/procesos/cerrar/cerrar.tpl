
<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Cerrar Solicitudes</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Nro.</th>
                            <th>Descripcion</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=post from=$_RequerimientoPost}
                                <tr id="idPost{$post.requerimientoPost}">
                                    <td>{$post.cod_requerimiento}</td>
                                    <td>{$post.descripcion}</td>
                                    <td><button accion="modificar" title="Modificar" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario a Modificado el requerimiento Nro. {$post.cod_requerimiento}" idPost="{$post.pk_requerimiento}" titulo="Modificar Requerimiento" data-toggle="modal" data-target="#formModal" ><i class="fa fa-edit" style="color: #ffffff;"></i></button>
                                        <button accion="anular" title="Anular" class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario a anulado el requerimiento Nro. {$post.cod_requerimiento}" idPost="{$post.pk_requerimiento}" titulo="Esta Seguro?" mensaje="Estas seguro que desea anular el requerimiento Nro {$post.cod_requerimiento}!!" boton="Sí, Anular"><i class="icm icm-file-remove2" style="color: #ffffff;"></i></button>

                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {

        var $url='{$_Parametros.url}modCA/procesos/asistenciaCONTROL/CompletarAsistenciaMET';

        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,'',function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.modificar', function () {
                $('#formModalLabel').html($(this).attr('titulo'));
                $.post($url,{ requerimientoPost: $(this).attr('requerimientoPost')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
        });
        $('#datatable1 tbody').on( 'click', '.anular', function () {

            var idPost=$(this).attr('idPost');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}procesos/asistenciaCONTROL/CompletarAsistenciaMET';
                $.post($url, { requerimientoPost: requerimientoPost},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('requerimientoPost'+$dato['requerimientoPost'])).html('');
                        swal("Anulado!", "el requerimiento fue Anulado.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>