<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">Entregar Equipo en Mantenimiento</h2>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr align="center">
                            <th>Dependencia</th>
                            <th>Equipo</th>
                            <th>Fecha de ingreso</th>
                            <th>Accion</th>
                        </tr>
                    </thead>
                      <tbody>
                          {foreach item=post from=$_PruebaPost}
                         <tr id="pk_num_activo{$post.pk_num_activo}">
                            <td>{$post.ind_dependencia}</td>
                             <td>{$post.ind_descripcion}</td>
                            <td>{$post.fec_ultima_modificacion}</td>
                                <td colspan="6">
                                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" pk_num_activo="{$post.pk_num_activo}"  boton="si, Eliminar" descripcion="El usuario ha eliminado una solicitud" titulo="¿Estás Seguro?" title="Eliminar" mensaje="¿Desea eliminar?" title="Eliminar"><i class="md md-delete"></i></button>
                                    <i class="md md-group-work" style="color: #ffffff;"></i> </button>
                                </td>
                        </tr>
                          {/foreach}
                      </tbody>
                    <tfoot>
                    <tr>
                    <th>

                    </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var pk_num_activo=$(this).attr('pk_num_activo');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modCA/procesos/entregarCONTROL/EliminarMET';
                $.post($url, { pk_num_activo: pk_num_activo},function($dato){
                    if($dato['status']=='OK'){
                        $(document.getElementById('pk_num_activo'+$dato['pk_num_activo'])).html('');
                        swal("Eliminado!", "Equipo Devuelto.", "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });


    });

</script>