<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">&nbsp;Control de activos</h2>

    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                    <tr align="center">
                        <th>N° activo</th>
                        <th>Descripcion</th>
                        <th>Asignado a </th>
                        <th>Tipo</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=post from=$_PruebaPost}
                        <tr id="pk_num_activo{$post.pk_num_activo}">
                            <td>{$post.pk_num_activo}</td>
                            <td>{$post.ind_descripcion}</td>
                            <td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
<td>
                            {if $post.num_flag_activo_tecnologico=='0'}
                                Activo comun
                            {else}
                                Activo tecnologico
                            {/if}
</td>
                            <td><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_activo="{$post.pk_num_activo}" title="Ver Solicitud" titulo="Ver Solicitud" id="ver"><i class="fa fa-eye" style="color: #ffffff;"></i></button>
                                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_activo="{$post.pk_num_activo}" descipcion="El Usuario ha Modificado un activo" title="Actualizar Activo"  titulo="Actualizar activo"> <i class="md md-view-list" style="color: #ffffff;"></i></button>
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        var $url_modificar='{$_Parametros.url}modCA/procesos/mantenimientoCONTROL/EditarMET';
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#modalAncho').css( "width", "45%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url_modificar,{ pk_num_activo: $(this).attr('pk_num_activo')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });

    var $urlVer='{$_Parametros.url}modCA/procesos/mantenimientoCONTROL/VerMET';
    $('#datatable1 tbody').on( 'click', '.ver', function () {
        $('#modalAncho').css( "width", "45%" );
        $('#formModalLabel').html($(this).attr('titulo'));
        $('#ContenidoModal').html("");
        $.post($urlVer,{ pk_num_activo: $(this).attr('pk_num_activo')},function($dato){
            $('#ContenidoModal').html($dato);
        });
    });
</script>
