<form id="formAjax" action="{$_Parametros.url}modCA/procesos/mantenimientoCONTROL/NuevoMET" class="form floating-label"
      novalidate>
    <input type="hidden" name="valido" value="1"/>
    <div class="modal-body">
        <div class="form-group floating-label">
            <select name="ind_dependencia" id="ind_dependencia"
                    class="select2-container form-control select2">
                <a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
                    <option value="">&nbsp;</option>
                    {foreach item=us from=$dependencia}
                        <option  selected value="{$us.pk_num_dependencia}">{$us.ind_dependencia} {$us.ind_dependencia}</option>
                    {/foreach}
            </select>
            <label for="ind_dependencia"><i class="md md-person"></i>Dependencia</label>
        </div>
        <div class="form-group floating-label">
            <input type="text" class="form-control" name="ind_descripcion" id="ind_descripcion">
            <label for="ind_descripcion"><i class="glyphicon glyphicon-pencil"></i> Descripción</label>
        </div>
        <div align="right">
            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
                    descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span
                        class="glyphicon glyphicon-floppy-remove"></span> Cancelar
            </button>
            &nbsp;&nbsp;
            <button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accionnv"><span
                        class="glyphicon glyphicon-floppy-disk"></span>Guardar
            </button>
        </div>
</form>
<script type="text/javascript">
    $("#formAjax").submit(function () {
        return false;
    });
    $(document).ready(function () {
//validation rules
        $("#accionnv").click(function (form) {
            $.post($("#formAjax").attr('action'), $("#formAjax").serialize(), function (dato) {
                $(document.getElementById('pk_num_solicitud' + dato['pk_num_solicitud'])).html('');
                swal("Registro Guardado", "evaluado satisfactoriamente", "success");
                $(document.getElementById('cerrarModal')).click();
                $(document.getElementById('ContenidoModal')).html('');
            }, 'json');
        });

    });


    var placeholder = "";


    // @see https://github.com/ivaynberg/select2/commit/6661e3
    function repoFormatResult(repo) {
        var markup = "<div class='select2-result-repository clearfix'>" +
                "<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
                "<div class='select2-result-repository__meta'>" +
                "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

        if (repo.description) {
            markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
        }

        markup += "<div class='select2-result-repository__statistics'>" +
                "<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
                "<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
                "<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
                "</div>" +
                "</div></div>";

        return markup;
    }
    function repoFormatSelection(repo) {
        return repo.full_name;
    }

    $("button[data-select2-open]").click(function () {
        $("#" + $(this).data("select2-open")).select2("open");
    });
</script>


