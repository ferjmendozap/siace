<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">
           Solicitudes
        </h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-sm-9">
                <table id="datatable1"  class="table responsive-table" style="overflow: scroll;overflow-x: scroll">
                    <thead>
                    <tr align="center">
                        <th>Numero</th>
                        <th>Fecha</th>
                        <th>Dependencia</th>
                        <th>Detalles</th>
                        <th>Tipo</th>

                        <th>Usuario</th>
                        <th>Supervisor</th>
                        <th>Recom. Sup.</th>
                        <th>Det. Analista</th>
                        <th>Evaluación</th>
                        <th>Sugerencia</th>
                        <th>Estatus</th>

                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=i from=$listado}
                        <tr id="pk_num_solicitud{$i.pk_num_solicitud}">
                            <td>{$i.pk_num_solicitud}</td>
                            <td>{$i.fecha_solicitud}</td>
                            <td>{$i.dependencia}</td>
                            <td>
                                <textarea disabled class="animate">
                                    {$i.ind_detalles}
                                </textarea>
                                </td>
                            <td>{$i.tiposolicitud}</td>

                            <td>{$i.nombres}</td>
                            <td>{$i.funcionario_s}</td>
                            <td>{$i.ind_detalles_supervisor}</td>
                            <td>{$i.ind_detalles_analista_final}</td>
                            {if $i.ind_estatus!='Evaluado' and $i.ind_estatus!='Cerrado'  }
                                <td> No </td>
                            {/if}
                            {if $i.ind_estatus=='Evaluado' or $i.ind_estatus=='Cerrado'}
                                <td>{$i.evaluacion}</td>
                            {/if}
                            <td>{$i.ind_sugerencias}</td>
                            <td>{$i.ind_estatus}</td>

                        </tr>
                    {/foreach}
                    </tbody>

                 <tr>
                        <th colspan="6">
                            <button  class="logsUsuario btn ink-reaction btn-raised btn-primary" id="reporte" descripcion="Generar Reporte de salida: preparados" data-toggle="modal" data-target="#formModal"  titulo="Listado de salidas">Reporte</button><br/>
                        </th>
                 </tr>

                </table>
            </div>
        </div>
    </div>

</section>

<script type="text/javascript">
    $(document).ready(function () {
        var $url = '{$_Parametros.url}modCA/procesos/solicitudCONTROL/SolicitudMET';
        $('#nuevosolx').click(function () {
            $('#modalAncho').css("width", "45%");
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url, '', function ($dato) {
                $('#ContenidoModal').html($dato);
            });
        });


        $('#reporte').click(function () {
            $('#modalAncho').css( 'width', '95%' );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $('#ContenidoModal').html("<iframe src='{$_Parametros.url}modCA/procesos/reporteSolicitudCONTROL/ImprimirSolicitudesMET' border='1' width='100%' height='440px'></iframe>");
        });
    });

</script>