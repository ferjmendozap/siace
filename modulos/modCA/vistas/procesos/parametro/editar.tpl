<form id="formAjax" action="{$_Parametros.url}modCA/procesos/parametroCONTROL/EditarMET"  class="form" role="form" novalidate="novalidate">
<div class="modal-body">
	<input type="hidden" value="1" name="valido" />
	{if isset($form.pk_num_actividad)}
		<input type="hidden" value="{$form.pk_num_actividad}" name="pk_num_actividad" id="pk_num_actividad" />
	{/if}
	<div class="form-group floating-label">
		<input type="text" class="form-control" value="{$form.ind_nombre_actividad}" name="ind_nombre_actividad" id="ind_nombre_actividad">
		<label for="regular2"><i class="glyphicon glyphicon-pencil"></i>Acción</label>
	</div>
    <div class="form-group floating-label">
        <input type="text" class="form-control" value="{$form.ind_orden_actividad}" name="ind_orden_actividad" id="ind_orden_actividad" disabled>
        <label for="regular2"><i class="glyphicon glyphicon-pencil"></i>Orden</label>
    </div>

    <div class="form-group floating-label">
        <select name="ind_estatus" id="ind_estatus"	class="select2-container form-control select2" required>
            <option value="1">Activo</option>
            <option value="0">Inactivo</option>
        </select>
        <label for="ind_estatus"><i class="md md-person"></i>Estatus</label>
    </div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descripcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="md md-view-list" style="color: #ffffff;"></i>Actualizar</button>
	</div>
</div>
</form>



<script type="text/javascript">
	$("#formAjax").submit(function () {
		return false;
	});

	$(document).ready(function () {
		//validation rules
		$("#accion").click(function (form) {
			$.post($("#formAjax").attr('action'), $("#formAjax").serialize(), function (dato) {
				$(document.getElementById('pk_num_actividad'+dato['pk_num_actividad'])).html('');
				swal("Registro Guardado", "actualizado satisfactoriamente", "success");
				$(document.getElementById('cerrarModal')).click();
				$(document.getElementById('ContenidoModal')).html('');
			}, 'json');
		});

	});






	var placeholder = "";



	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult(repo) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if (repo.description) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}
	function repoFormatSelection(repo) {
		return repo.full_name;
	}

	$("button[data-select2-open]").click(function () {
		$("#" + $(this).data("select2-open")).select2("open");
	});
</script>


