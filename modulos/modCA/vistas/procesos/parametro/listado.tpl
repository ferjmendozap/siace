<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">Configuracion De Solicitud</h2>
	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr align="center">
                            <th>Actividad</th>
                            <th>Orden</th>
                            <th>Estatus</th>
                            <th>Acción</th>
                        </tr>
                    </thead>
                      <tbody>
                          {foreach item=post from=$_PruebaPost}
                         <tr id="pk_num_actividad{$post.pk_num_actividad}">
                            <td>{$post.ind_nombre_actividad}</td>
                            <td>{$post.ind_orden_actividad}</td>
                             <td>
                                 {if $post.ind_estatus=='1'}
                                     Activo
                                 {else}
                                     Inactivo
                                 {/if}
                             </td>
                                <td colspan="6">
                                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_actividad="{$post.pk_num_actividad}" descipcion="El Usuario ha Modificado un tipo de solicitud" title="Actualizar tipo de Solicitud"  titulo="Actualizar tipo de Solicitud"> <i class="md md-view-list" style="color: #ffffff;"></i></button>
                                </td>
                        </tr>
                          {/foreach}
                      </tbody>
                    <tfoot>
                    <tr>
                    <th>
                    </th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        var $url_modificar='{$_Parametros.url}modCA/procesos/parametroCONTROL/EditarMET';
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#modalAncho').css( "width", "45%" );
            $('#formModalLabel').html($(this).attr('titulo'));
            $('#ContenidoModal').html("");
            $.post($url_modificar,{ pk_num_actividad: $(this).attr('pk_num_actividad')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
    });

</script>