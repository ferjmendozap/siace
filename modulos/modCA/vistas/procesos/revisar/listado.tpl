<br/>
<section class="style-default-bright">
    <div class="col-lg-12">
        <h2 class="text-primary">&nbsp;Revisar Solicitud</h2>

	</div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <table id="datatable1" class="table table-striped table-hover">
                    <thead>
                        <tr align="center">
                            <th>N° de Solicitud</th>
                            <th>Detalles</th>
                            <th>Equipo</th>
                            <th>Usuario</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            {foreach item=post from=$_PruebaPost}
                            <tr id="pk_num_solicitud{$post.pk_num_solicitud}">
                            <td>{$post.pk_num_solicitud}</td>
                            <td>{$post.ind_detalles}</td>
                                <td>
                                {if $u.ind_descripcion==''}
                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-default"  title="Eliminar">No requiere</button>
                                {else}
                                    <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" title="Eliminar">{$u.ind_descripcion}</button>
                                {/if}
                                </td>
                             <td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
                            <td><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud="{$post.pk_num_solicitud}" title="Ver Solicitud" titulo="Ver Solicitud" id="ver"><i class="fa fa-eye" style="color: #ffffff;"></i></button>
                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" pk_num_solicitud="{$post.pk_num_solicitud}" descipcion="El Usuario ha Modificado un almacén" title="Revisar Solicitud"  titulo="Revisar Solicitud"> <i class="md md-view-list" style="color: #ffffff;"></i></button>
                            </td>
                        </tr>
                        {/foreach}
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
            $(document).ready(function() {
                var $url_modificar='{$_Parametros.url}modCA/procesos/revisarCONTROL/EditarMET';
                $('#datatable1 tbody').on( 'click', '.modificar', function () {
		        $('#modalAncho').css( "width", "45%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $('#ContenidoModal').html("");
                $.post($url_modificar,{ pk_num_solicitud: $(this).attr('pk_num_solicitud')},function($dato){
                $('#ContenidoModal').html($dato);
                });
                });

                $('#datatable1 tbody').on( 'click', '.eliminar', function () {
                    var pk_num_solicitud=$(this).attr('pk_num_solicitud');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        var $url='{$_Parametros.url}modCA/procesos/revisarCONTROL/EliminarMET';
                        $.post($url, { pk_num_solicitud: pk_num_solicitud},function($dato){
                            if($dato['status']=='OK'){
                                $(document.getElementById('pk_num_solicitud'+$dato['pk_num_solicitud'])).html('');
                                swal("Eliminado!", "El almacén fue eliminado.", "success");
                                $('#cerrar').click();
                            }
                        },'json');
                    });
                });
            });



            var $urlVer='{$_Parametros.url}modCA/procesos/revisarCONTROL/VerMET';
            $('#datatable1 tbody').on( 'click', '.ver', function () {
                $('#modalAncho').css( "width", "45%" );
                $('#formModalLabel').html($(this).attr('titulo'));
                $('#ContenidoModal').html("");
                $.post($urlVer,{ pk_num_solicitud: $(this).attr('pk_num_solicitud')},function($dato){
                    $('#ContenidoModal').html($dato);
                });
            });
</script>
