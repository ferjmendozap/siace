<form id="formAjax" action="{$_Parametros.url}modAD/almacenCONTROL/NuevoAlmacenMET" class="form floating-label" novalidate="novalidate">
<input type="hidden" name="valido" value="1" />
<div class="modal-body">
	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="ind_descripcion_almacen" id="ind_descripcion_almacen">
		<label for="ind_descripcion_almacen"><i class="md md-create"></i> Descripción </label>
	</div>
	<div class="form-group floating-label">
		<input type="text" class="form-control"  name="ind_ubicacion_almacen" id="ind_ubicacion_almacen">
		<label for="ind_ubicacion_almacen"><i class="md md-location-searching"></i> Ubicación </label>
	</div>
	<div class="form-group floating-label">
		<select id="pk_num_empleado" name="pk_num_empleado" id="s2id_single" class="select2-container form-control select2">
			<a href="javascript:void(0)" class="select2-choice select2-default" tabindex="-1">
			<option value="">&nbsp;</option>
			{foreach item=us from=$usuario}
			<option value="{$us.pk_num_empleado}">{$us.ind_nombre1} {$us.ind_apellido1}</option>
			{/foreach}
		</select>
		<label for="fk_a003_num_persona"><i class="md md-person"></i>Encargado</label>
	</div>
	<div align="right">
		<button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><span class="glyphicon glyphicon-floppy-remove"></span> Cancelar</button>&nbsp;&nbsp;
		<button type="submit" class="btn btn-primary ink-reaction btn-raised" id="accion"><span class="glyphicon glyphicon-floppy-disk"></span>Guardar</button>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function () {
		//validation rules
		$("#formAjax").validate({
			rules:{
				pk_num_empleado:{
					required:true
				},
				ind_descripcion_almacen:{
					required:true,
					remote:{
						url:"{$_Parametros.url}modAD/almacenCONTROL/VerificarAlmacenMET",
						type:"post"
					}
				},
				ind_ubicacion_almacen:{
					required:true
				}
			},
			messages:{
				pk_num_empleado:{
					required: "Este campo es requerido"
				},
				ind_descripcion_almacen:{
					required: "Este campo es requerido",
					remote: jQuery.validator.format("Ya se encuentra registrado en el sistema")
				},
				ind_ubicacion_almacen: {
					required: "Este campo es requerido"
				}
			},
			submitHandler: function(form) {
				$.post($(form).attr('action'), $(form).serialize(),function(dato){
					$(document.getElementById('datatable1')).append('<tr id="pk_num_almacen'+dato['pk_num_almacen']+'"><td>'+dato['pk_num_almacen']+'</td><td>'+dato['ind_descripcion_almacen']+'</td><td>'+dato['ind_ubicacion_almacen']+'</td><td>'+dato['empleado']+'</td>' +
							'{if in_array('AD-01-02-01-03-V',$_Parametros.perfil)}<td align="center"><button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha visualizado el almacén" titulo="Visualizar Almacen" title="Visualizar Almacen" data-toggle="modal" data-target="#formModal" pk_num_almacen="'+dato['pk_num_almacen']+'"><i class="glyphicon glyphicon-search" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-01-04-M',$_Parametros.perfil)}<td align="center"><button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" descipcion="El Usuario ha Modificado el almacén" titulo="Modificar Almacén" title="Modificar Almacén" data-toggle="modal" data-target="#formModal" pk_num_almacen="'+dato['pk_num_almacen']+'"><i class="fa fa-edit" style="color: #ffffff;"></i></button></td>{/if}' +
							'{if in_array('AD-01-02-01-05-E',$_Parametros.perfil)}<td align="center"><button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" descipcion="El usuario ha eliminado un almacén" titulo="¿Estás Seguro?" title="Eliminar Almacén" mensaje="¿Estás seguro de eliminar el almacén?" boton="si, Eliminar" pk_num_almacen="'+dato['pk_num_almacen']+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>{/if}</tr>');
					swal("Almacén Guardado", "Almacén guardado satisfactoriamente", "success");
					$(document.getElementById('cerrarModal')).click();
					$(document.getElementById('ContenidoModal')).html('');
				},'json');
			}
		});
	});
	var placeholder = "";

	$( ".select2, .select2-multiple" ).select2( { placeholder: placeholder } );
	$( ".select2-allow-clear" ).select2( { allowClear: true, placeholder: placeholder } );

	// @see https://github.com/ivaynberg/select2/commit/6661e3
	function repoFormatResult( repo ) {
		var markup = "<div class='select2-result-repository clearfix'>" +
				"<div class='select2-result-repository__avatar'><img src='" + repo.owner.avatar_url + "' /></div>" +
				"<div class='select2-result-repository__meta'>" +
				"<div class='select2-result-repository__title'>" + repo.full_name + "</div>";

		if ( repo.description ) {
			markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
		}

		markup += "<div class='select2-result-repository__statistics'>" +
				"<div class='select2-result-repository__forks'><span class='glyphicon glyphicon-flash'></span> " + repo.forks_count + " Forks</div>" +
				"<div class='select2-result-repository__stargazers'><span class='glyphicon glyphicon-star'></span> " + repo.stargazers_count + " Stars</div>" +
				"<div class='select2-result-repository__watchers'><span class='glyphicon glyphicon-eye-open'></span> " + repo.watchers_count + " Watchers</div>" +
				"</div>" +
				"</div></div>";

		return markup;
	}

	function repoFormatSelection( repo ) {
		return repo.full_name;
	}

	$( "button[data-select2-open]" ).click( function() {
		$( "#" + $( this ).data( "select2-open" ) ).select2( "open" );
	});
</script>
