<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de almacenes
class tiposolicitudModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListar()
    {
        $lista =  $this->_db->query(
        "SELECT * FROM  st_b002_tipo_solicitud,st_b002_tipo_soporte WHERE st_b002_tipo_solicitud.fk_num_tipo_soporte=st_b002_tipo_soporte.pk_num_tipo_soporte"
        );
        $lista->setFetchMode(PDO::FETCH_ASSOC);
        return $lista->fetchAll();
    }

    public function metTipoSoporte()
    {
        $soporte = $this->_db->query(
            "SELECT * FROM st_b002_tipo_soporte"
        );
        $soporte->setFetchMode(PDO::FETCH_ASSOC);
        return $soporte->fetchAll();
    }




    public function metVer($pk_num_tipo_solicitud,$metodo)
    {
        $verAprobar =  $this->_db->query(
            "SELECT * FROM  st_b002_tipo_solicitud,st_b002_tipo_soporte WHERE st_b002_tipo_solicitud.fk_num_tipo_soporte=st_b002_tipo_soporte.pk_num_tipo_soporte AND st_b002_tipo_solicitud.pk_num_tipo_solicitud='$pk_num_tipo_solicitud'"
        );
        $verAprobar->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verAprobar->fetch();
        } else {
            return $verAprobar->fetchAll();
        }
    }


    public function metGuardarNueva($ind_tipo_solicitud, $fk_num_tipo_soporte, $num_requiere_auto)
    {
        $this->_db->beginTransaction();
        $registroUnidades = $this->_db->prepare("
          INSERT INTO
           st_b002_tipo_solicitud
          SET
            ind_tipo_solicitud=:ind_tipo_solicitud,
            fk_num_tipo_soporte=:fk_num_tipo_soporte,
            num_requiere_auto=:num_requiere_auto ,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          ");

        $registroUnidades->execute(array(
            'ind_tipo_solicitud'=>$ind_tipo_solicitud,
            'fk_num_tipo_soporte'=>$fk_num_tipo_soporte,
            'num_requiere_auto'=>$num_requiere_auto
        ));
        $idRegistroa= $this->_db->lastInsertId();

        $fallaTansaccion = $registroUnidades->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            #$idRegistroa= $this->_db->lastInsertId();
            $this->_db->commit();

        }
        return $idRegistroa;
    }



    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT *
 from rh_b001_empleado,a003_persona,rh_c076_empleado_organizacion,a004_dependencia,st_d001_grupo_tecnica
         where
         rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona AND rh_b001_empleado.pk_num_empleado=rh_c076_empleado_organizacion.fk_rhb001_num_empleado AND rh_c076_empleado_organizacion.fk_a004_num_dependencia=a004_dependencia.pk_num_dependencia AND a004_dependencia.pk_num_dependencia=st_d001_grupo_tecnica.fk_a004_num_dependencia
         order by ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }


    public function metEditar($ind_tipo_solicitud,$pk_num_tipo_soporte,$pk_num_tipo_solicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update st_b002_tipo_solicitud set st_b002_tipo_solicitud.ind_tipo_solicitud='$ind_tipo_solicitud',st_b002_tipo_solicitud.fk_num_tipo_soporte ='$pk_num_tipo_soporte'  where st_b002_tipo_solicitud.pk_num_tipo_solicitud='$pk_num_tipo_solicitud'"
        );
        $this->_db->commit();
    }


    public function metEliminar($pk_num_tipo_solicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_b002_tipo_solicitud where pk_num_tipo_solicitud= '$pk_num_tipo_solicitud'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }



}// fin de la clase
?>
