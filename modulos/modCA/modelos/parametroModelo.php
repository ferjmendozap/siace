<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de parametros
class parametroModelo extends Modelo
{

public function __construct()
{
    parent::__construct();
    $this->atIdUsuario = Session::metObtener("idUsuario");
}




public function metListar()
    {
        $actividad=  $this->_db->query(
            "SELECT * FROM  st_c002_actividad"
        );
        $actividad->setFetchMode(PDO::FETCH_ASSOC);
        return $actividad->fetchAll();
    }

    public function metEditar($ind_nombre_actividad,$ind_estatus,$pk_num_actividadd)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update st_c002_actividad set ind_nombre_actividad='$ind_nombre_actividad',ind_estatus='$ind_estatus'  where pk_num_actividad='$pk_num_actividadd'"
        );
        $this->_db->commit();
    }



    public function metDependencia()
    {
        $dependencia=  $this->_db->query(
            "SELECT * FROM a004_dependencia"
        );
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }


    public function metSelector()
    {
        $selector=  $this->_db->query(
            "SELECT ind_orden_actividad FROM st_c002_actividad"
        );
        $selector->setFetchMode(PDO::FETCH_ASSOC);
        return $selector->fetchAll();
    }


    public function metVer($pk_num_actividad, $metodo)
    {
        $verAprobar =  $this->_db->query(
            "SELECT * FROM  st_c002_actividad WHERE pk_num_actividad='$pk_num_actividad'"
        );
        $verAprobar->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verAprobar->fetch();
        } else {
            return $verAprobar->fetchAll();
        }
    }

    public function metVerUsuario($pkNumAlmacen)
    {
        $verUsuario =  $this->_db->query(
            "select d.ind_nombre1, d.ind_apellido1 from ad_c001_almacen as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.pk_num_almacen='$pkNumAlmacen'"
        );
        $verUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $verUsuario->fetchAll();

    }

    public function metUsuarioGrupo($pkNumEmpleado){
        $usuario = $this->_db->query(
            "SELECT  b.ind_nombre1, b.ind_apellido1, a.fk_a003_num_persona from rh_b001_empleado as a, a003_persona as b
             where a.fk_a003_num_persona=b.pk_num_persona and a.pk_num_empleado='$pkNumEmpleado'"
        );
        $nombre_usuario = $usuario->fetch();
        return $nombre_usuario[0].' '.$nombre_usuario[1];
    }

    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT a.fk_a003_num_persona, a.pk_num_empleado, b.ind_nombre1, b.ind_apellido1 from rh_b001_empleado as a, a003_persona as b
         where a.fk_a003_num_persona=b.pk_num_persona order by b.ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }



    public function metListaGrupo($pk_registro)
    {
        $grupo= $this->_db->query("
          SELECT grupo,pk_grupo_tecnica FROM
        st_d001_grupo_tecnica WHERE pk_grupo_tecnica='$pk_registro'
       "
        );
        $grupo->setFetchMode(PDO::FETCH_ASSOC);
        return $grupo->fetch();
    }

    public function metDependenciaGrupo($pk_registro)
    {
        $dependencia= $this->_db->query("
          SELECT st_d001_grupo_tecnica.grupo,a004_dependencia.pk_num_dependencia,st_d001_grupo_tecnica.fk_a004_num_dependencia,a004_dependencia.ind_dependencia FROM
        st_d001_grupo_tecnica,a004_dependencia WHERE pk_grupo_tecnica='$pk_registro' AND a004_dependencia.pk_num_dependencia=st_d001_grupo_tecnica.fk_a004_num_dependencia
       "
        );
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetch();
    }

    public function metEliminar($pk_grupo_tecnica)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_d001_grupo_tecnica " .
            "where pk_grupo_tecnica = '$pk_grupo_tecnica'"
        );
        $this->_db->commit();
    }


    public function metGuardarGrupo($ind_dependencia,$ind_descripcion)
    {
        $this->_db->beginTransaction();
        $registroUnidades = $this->_db->prepare("
          INSERT INTO
            st_d001_grupo_tecnica
          SET
            fk_a004_num_dependencia=:fk_a004_num_dependencia,
            grupo=:grupo,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          ");

        $registroUnidades->execute(array(
            'grupo'=>$ind_descripcion,
            'fk_a004_num_dependencia'=>$ind_dependencia,
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroUnidades->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

}// fin de la clase
?>
