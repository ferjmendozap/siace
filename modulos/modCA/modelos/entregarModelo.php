<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación
class entregarModelo extends Modelo
{

public function __construct()
{
    parent::__construct();
    $this->atIdUsuario = Session::metObtener("idUsuario");
}





public function metListar()
    {
        $equipo =  $this->_db->query(
            "SELECT  * FROM st_a001_equipo,a004_dependencia,af_b001_activo
 WHERE
st_a001_equipo.ind_estatus='0'
AND st_a001_equipo.fk_num_activo=af_b001_activo.pk_num_activo
AND st_a001_equipo.fk_num_dependencia=a004_dependencia.pk_num_dependencia 
GROUP BY af_b001_activo.pk_num_activo
 "
        );
        $equipo->setFetchMode(PDO::FETCH_ASSOC);
        return $equipo->fetchAll();
    }



    public function metDependencia()
    {
        $dependencia=  $this->_db->query(
            "SELECT * FROM a004_dependencia"
        );
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencia->fetchAll();
    }


    public function metVer($pkNumSolicitud, $metodo)
    {
        $verAprobar =  $this->_db->query(
            "SELECT * FROM  st_d001_solicitud WHERE pk_num_solicitud='$pkNumSolicitud'"
        );
        $verAprobar->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verAprobar->fetch();
        } else {
            return $verAprobar->fetchAll();
        }
    }

    public function metVerUsuario($pkNumAlmacen)
    {
        $verUsuario =  $this->_db->query(
            "select d.ind_nombre1, d.ind_apellido1 from ad_c001_almacen as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.pk_num_almacen='$pkNumAlmacen'"
        );
        $verUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $verUsuario->fetchAll();

    }

    public function metUsuarioGrupo($pkNumEmpleado){
        $usuario = $this->_db->query(
            "SELECT  b.ind_nombre1, b.ind_apellido1, a.fk_a003_num_persona from rh_b001_empleado as a, a003_persona as b
             where a.fk_a003_num_persona=b.pk_num_persona and a.pk_num_empleado='$pkNumEmpleado'"
        );
        $nombre_usuario = $usuario->fetch();
        return $nombre_usuario[0].' '.$nombre_usuario[1];
    }

    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT a.fk_a003_num_persona, a.pk_num_empleado, b.ind_nombre1, b.ind_apellido1 from rh_b001_empleado as a, a003_persona as b
         where a.fk_a003_num_persona=b.pk_num_persona order by b.ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }


    public function metEditar( $pk_num_empleado,$pkNumSolicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update st_d001_solicitud set num_estatus='2',fk_rhb001_num_empleado_supervisor='$pk_num_empleado'  where pk_num_solicitud='$pkNumSolicitud'"
        );
        $this->_db->commit();
    }




    public function metEliminar($pk_num_activo)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_a001_equipo where fk_num_activo= '$pk_num_activo'"
        );
        $this->_db->query(
            "update st_d001_solicitud set st_d001_solicitud.num_flag_mantenimiento='0'  where st_d001_solicitud.ind_equipo='$pk_num_activo'"
        );
        $this->_db->commit();
    }


    public function metGuardarGrupo($ind_dependencia,$ind_descripcion)
    {
        $this->_db->beginTransaction();
        $registroUnidades = $this->_db->prepare("
          INSERT INTO
            st_d001_grupo_tecnica
          SET
            fk_a004_num_dependencia=:fk_a004_num_dependencia,
            grupo=:grupo,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          ");

        $registroUnidades->execute(array(
            'grupo'=>$ind_descripcion,
            'fk_a004_num_dependencia'=>$ind_dependencia,
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroUnidades->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

}// fin de la clase
?>
