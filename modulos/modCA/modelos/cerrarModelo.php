<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación
class cerrarModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }

    public function metListar()
    {
        $lista =  $this->_db->query(
            "
            SELECT 
			st_d001_solicitud.*,
			a004.ind_dependencia AS dependencia ,
			st_b002.ind_tipo_solicitud AS tiposolicitud,
			st_b003.ind_soporte_realizado AS  soporte,
			st_d001e.ind_evaluacion AS evaluacion,
			af_b001.ind_descripcion AS descrip, 
			st_d001_mod.ind_modalidad AS modalidad, 
			CONCAT(vl_rh.ind_nombre1,' ',vl_rh.ind_apellido1)  AS nombres,	
			CONCAT(vl_rh1.ind_nombre1,' ',vl_rh1.ind_apellido1)  AS funcionario
			  FROM st_d001_solicitud 
			  LEFT JOIN
            st_d001_evaluacion st_d001e ON st_d001e.pk_num_evaluacion = st_d001_solicitud.fk_num_evaluacion
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  st_d001_solicitud.fk_a004_num_dependencia
			  LEFT JOIN
			st_b002_tipo_solicitud st_b002 ON st_b002.pk_num_tipo_solicitud = st_d001_solicitud.fk_num_tipo_solicitud
		      LEFT JOIN
			st_b002_tipo_soporte st_b003 ON st_b003.pk_num_tipo_soporte= st_d001_solicitud.fk_num_tipo_soporte
              LEFT JOIN
			st_d001_modalidad st_d001_mod ON st_d001_mod.pk_num_modalidad= st_d001_solicitud.fk_num_modalidad
              LEFT JOIN
			af_b001_activo af_b001 ON af_b001.pk_num_activo= st_d001_solicitud.ind_equipo
			  LEFT JOIN
			  vl_rh_persona_empleado_datos_laborales vl_rh1 ON vl_rh1.pk_num_empleado = st_d001_solicitud.fk_rhb001_num_empleado_asignado
            
              INNER JOIN
			  vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.pk_num_empleado = st_d001_solicitud.fk_rhb001_num_empleado_solicitante
              WHERE
              st_d001_solicitud.num_estatus='6' GROUP BY st_d001_solicitud.pk_num_solicitud
              "
        );
        $lista->setFetchMode(PDO::FETCH_ASSOC);
        return $lista->fetchAll();
    }

    public function metVer($pkNumSolicitud, $metodo)
    {
              $verAprobar =  $this->_db->query(
            "
              SELECT 
			st_d001_solicitud.*,
			a004.ind_dependencia AS dependencia ,
			st_b002.ind_tipo_solicitud AS tiposolicitud,
			st_b003.ind_soporte_realizado AS  soporte,
			st_d001e.ind_evaluacion AS evaluacion,
			af_b001.ind_descripcion AS descrip, 
			st_d001_mod.ind_modalidad AS modalidad, 
			CONCAT(vl_rh.ind_nombre1,' ',vl_rh.ind_apellido1)  AS nombres,	
			CONCAT(vl_rh1.ind_nombre1,' ',vl_rh1.ind_apellido1)  AS funcionario
			  FROM st_d001_solicitud 
			  LEFT JOIN
            st_d001_evaluacion st_d001e ON st_d001e.pk_num_evaluacion = st_d001_solicitud.fk_num_evaluacion
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  st_d001_solicitud.fk_a004_num_dependencia
			  LEFT JOIN
			st_b002_tipo_solicitud st_b002 ON st_b002.pk_num_tipo_solicitud = st_d001_solicitud.fk_num_tipo_solicitud
		      LEFT JOIN
			st_b002_tipo_soporte st_b003 ON st_b003.pk_num_tipo_soporte= st_d001_solicitud.fk_num_tipo_soporte
              LEFT JOIN
			st_d001_modalidad st_d001_mod ON st_d001_mod.pk_num_modalidad= st_d001_solicitud.fk_num_modalidad
              LEFT JOIN
			af_b001_activo af_b001 ON af_b001.pk_num_activo= st_d001_solicitud.ind_equipo
			  LEFT JOIN
			  vl_rh_persona_empleado_datos_laborales vl_rh1 ON vl_rh1.pk_num_empleado = st_d001_solicitud.fk_rhb001_num_empleado_asignado
            
              INNER JOIN
			  vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.pk_num_empleado = st_d001_solicitud.fk_rhb001_num_empleado_solicitante
              
            WHERE 
            pk_num_solicitud='$pkNumSolicitud'
          
                       "
        );
        $verAprobar->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verAprobar->fetch();
        } else {
            return $verAprobar->fetchAll();
        }

    }

    public function metVerUsuario($pkNumAlmacen)
    {
        $verUsuario =  $this->_db->query(
            "select d.ind_nombre1, d.ind_apellido1 from ad_c001_almacen as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.pk_num_almacen='$pkNumAlmacen'"
        );
        $verUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $verUsuario->fetchAll();

    }


    public function metUsuarioAlmacen($pkNumEmpleado){
        $usuario = $this->_db->query(
            "SELECT  b.ind_nombre1, b.ind_apellido1, a.fk_a003_num_persona from rh_b001_empleado as a, a003_persona as b
             where a.fk_a003_num_persona=b.pk_num_persona and a.pk_num_empleado='$pkNumEmpleado'"
        );
        $nombre_usuario = $usuario->fetch();
        return $nombre_usuario[0].' '.$nombre_usuario[1];
    }


    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT a.fk_a003_num_persona, a.pk_num_empleado, b.ind_nombre1, b.ind_apellido1 from rh_b001_empleado as a, a003_persona as b
         where a.fk_a003_num_persona=b.pk_num_persona order by b.ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }


    public function metEditar($pkNumSolicitud)
    {
        $this->_db->beginTransaction();
        $con = $this->_db->query("select ind_orden_actividad from st_c002_actividad WHERE ind_nombre_actividad='cerrar' AND ind_estatus='1' LIMIT 1");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        $data1 = $con->fetch();
        $valor = $data1['ind_orden_actividad'];
        $this->_db->query(
            "update st_d001_solicitud set num_estatus='$valor'  where pk_num_solicitud='$pkNumSolicitud'"
        );
        $this->_db->query(
            "update st_d002_registro set st_d002_registro.fec_cerrado=NOW()  where st_d002_registro.fk_std001_num_relacion='$pkNumSolicitud'"
        );
        $this->_db->commit();
    }

    public function metEliminar($pkNumSolicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_d001_solicitud where pk_num_solicitud = '$pkNumSolicitud'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metBuscarAlmacen($pk_num_solicitud)
    {
        if($pk_num_solicitud == false) {
            $buscarSolicitud = $this->_db->query(
                "select st_d001_solicitud.pk_num_solicitud where pk_num_solicitud='$pk_num_solicitud'"
            );
            $buscarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarSolicitud->fetch();
            $arreglo = $buscarSolicitud->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            $buscarSolicitud = $this->_db->query(
                "select pk_num_solicitud from st_d001_solicitud where  pk_num_solicitud<>'$pk_num_solicitud'"
            );
            $buscarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarSolicitud->fetch();
            $arreglo = $buscarSolicitud->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        }

    }
}// fin de la clase
?>
