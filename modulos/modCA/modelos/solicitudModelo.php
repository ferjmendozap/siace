<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class solicitudModelo extends Modelo
{
    private $atIdUsuario;
    private $atIdEmpleado;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListarSolicitud()
    {
        $solicitud = $this->_db->query(
            "
          SELECT
			st_d001.*
          FROM
           st_d001_solicitud st_d001
			 LEFT JOIN
          af_b001_activo ON af_b001_activo.pk_num_activo = st_d001.ind_equipo
			WHERE st_d001.fk_rhb001_num_empleado_solicitante='" . $this->atIdEmpleado . "'
			"
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }

    public function metVerALL()
    {
        $usuario =  $this->_db->query(
            "select * from 
              st_d001_solicitud,
              rh_b001_empleado,
              a003_persona 
              where 
              st_d001_solicitud.fk_rhb001_num_empleado_solicitante=rh_b001_empleado.pk_num_empleado
              and rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona "
        );
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();

    }


// Metodo que permite obtener el nombre de un empleado
    public function metConsultarEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2 from vl_rh_persona_empleado where pk_num_empleado=$pkNumEmpleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metFechaSolicitud($idRegistro)
    {
        $fecha= $this->_db->query("
          SELECT fecha_solicitud FROM
        st_d001_solicitud WHERE pk_num_solicitud='$idRegistro'
       "
        );
        $fecha->setFetchMode(PDO::FETCH_ASSOC);
        return $fecha->fetch();
    }


    public function metEquipoSolicitud($idRegistro)
    {
        $equipo= $this->_db->query("
          SELECT ind_descripcion,pk_num_activo,pk_num_solicitud,ind_equipo  FROM
        af_b001_activo,st_d001_solicitud WHERE st_d001_solicitud.pk_num_solicitud='$idRegistro' AND af_b001_activo.pk_num_activo=st_d001_solicitud.ind_equipo
       "
        );
        $equipo->setFetchMode(PDO::FETCH_ASSOC);
        return $equipo->fetch();

    }

    public function metEliminar($pk_num_solicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_d001_solicitud " .
            "where pk_num_solicitud = '$pk_num_solicitud'"
        );
        $this->_db->commit();
    }


//lista todas las solicitudes del usuario WHERE fk_rhb001_num_empleado_solicitante='$this->atIdUsuario'
    public function metListarTodo()
    {
        $solicitud = $this->_db->query(
          "
          SELECT
			st_d001.*
          FROM
           st_d001_solicitud st_d001
			 LEFT JOIN
          af_b001_activo ON af_b001_activo.pk_num_activo = st_d001.ind_equipo
           LEFT JOIN
          st_d001_modalidad ON st_d001_modalidad.pk_num_modalidad = st_d001.fk_num_modalidad
			   LEFT JOIN
          st_d001_evaluacion ON st_d001_evaluacion.pk_num_evaluacion = st_d001.fk_num_evaluacion
          		
		  
			"
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }

    public function metListarTodov($id_solicitud)
    {
        $solicitud = $this->_db->query(
            "SELECT * FROM  st_d001_solicitud,af_b001_activo,rh_b001_empleado,a003_persona,st_d001_evaluacion,st_d001_modalidad  WHERE af_b001_activo.pk_num_activo=ind_equipo
AND st_d001_solicitud.fk_rhb001_num_empleado_solicitante=rh_b001_empleado.pk_num_empleado
AND rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona
AND st_d001_solicitud.fk_num_modalidad=st_d001_modalidad.pk_num_modalidad
AND st_d001_solicitud.fk_num_evaluacion=st_d001_evaluacion.pk_num_evaluacion

AND st_d001_solicitud.pk_num_solicitud='$id_solicitud'

"
        );
        $solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $solicitud->fetchAll();
    }

//Buscar las solicitudes
    public function metBuscarSolicitud($idSolicitud)
    {
        $Solicitud = $this->_db->query(
            "SELECT 
                solicitud.*,
                dependencia.ind_dependencia,
                CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS funcionario
            FROM 
                st_d001_solicitud AS solicitud 
                INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = solicitud.fk_a004_num_dependencia 
                INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = solicitud.fk_rhb001_num_empleado_solicitante
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
            WHERE 
                solicitud.pk_num_solicitud ='" . $idSolicitud . "'"
        );
        $Solicitud->setFetchMode(PDO::FETCH_ASSOC);
        return $Solicitud->fetch();
    }


    public function metDependenciaDependiente($usuario)
    {
        $persona = $this->_db->query(
            "SELECT 
			a004.ind_codinterno,
			a004.ind_dependencia,
			a004.pk_num_dependencia,
		  a019_seguridad_dependencia.fk_a004_num_dependencia AS dependencias
			FROM
			a004_dependencia a004 
			 LEFT JOIN 
		  a019_seguridad_dependencia on a019_seguridad_dependencia.fk_a004_num_dependencia=a004.pk_num_dependencia
			 WHERE 
			 a019_seguridad_dependencia.fk_a018_num_seguridad_usuario=$usuario 
			 AND a019_seguridad_dependencia.fk_a015_num_seguridad_aplicacion='4'
            
			
			"
        );
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }


//Buscar la dependencia del solicitante
    public function metBuscarPersona()
    {
        $empleado = $this->_db->query("
          SELECT * FROM 
          rh_b001_empleado,a018_seguridad_usuario,a003_persona
          WHERE 
        a018_seguridad_usuario.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado
          AND
          rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona
          AND 
          rh_b001_empleado.pk_num_empleado='" . $this->atIdEmpleado . "'"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }




    public function metGuardarNueva($fk_a003_num_persona, $ind_equipo, $ind_dependencia, $ind_detalles,$fk_num_tipo_solicitud)
    {
        $this->_db->beginTransaction();
        $registroUnidades = $this->_db->prepare("
          INSERT INTO
            st_d001_solicitud
          SET
          
            fecha_solicitud=NOW(),
            fk_num_modalidad='1',
            ind_equipo=:ind_equipo,
            ind_detalles=:ind_detalles,
            ind_detalles_analista=NULL ,
            ind_detalles_supervisor=NULL ,
            num_estatus='0',
            ind_estatus='Preparado',
            ind_sugerencias=NULL , 
            fk_num_evaluacion='1' ,
            ind_aprobado_por=NULL ,
            ind_ip=NULL ,
            fk_rhb001_num_empleado_asignado=NULL ,
            fk_rhb001_num_empleado_supervisor=NULL ,
            ind_detalles_analista_final=NULL ,
            fk_num_tipo_solicitud=$fk_num_tipo_solicitud,
            fk_num_tipo_soporte=NULL ,
            fk_a004_num_dependencia=:ind_dependencia,
            fk_rhb001_num_empleado_solicitante=:empleado,
            fec_ultima_modificacion=NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
             ind_fecha_inicio=NULL ,
            ind_fecha_fin=NULL 
          ");

        $registroUnidades->execute(array(
            'empleado'=>$fk_a003_num_persona,
            'ind_equipo'=>$ind_equipo,
            'ind_dependencia'=>$ind_dependencia,
            'ind_detalles'=>$ind_detalles
        ));

        $idRegistroa= $this->_db->lastInsertId();

        $this->_db->query(
            "update st_d001_solicitud set st_d001_solicitud.num_flag_mantenimiento='1'  where st_d001_solicitud.pk_num_solicitud=last_insert_id()"
        );

        $actSolicitud = $this->_db->query("
                    INSERT INTO
                    st_d002_registro
                    SET 
                        fk_std001_num_relacion='$idRegistroa' ,
                         st_d002_registro.fec_creado = NOW()
                
        ");

        $actSolicitud2 = $this->_db->query("
                    INSERT INTO
                    st_a001_equipo
                    SET 
                        fk_num_activo='$ind_equipo',
                        st_a001_equipo.fec_ultima_modificacion = NOW(),
                        st_a001_equipo.fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        st_a001_equipo.fk_num_dependencia='$ind_dependencia',
                        st_a001_equipo.ind_estatus='0'
        ");




        $fallaTansaccion = $registroUnidades->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            #$idRegistroa= $this->_db->lastInsertId();
            $this->_db->commit();

        }
        return $idRegistroa;
    }






    public function metActEstadoSolicitud($idSolicitud, $estatus, $campo)
    {

        $this->_db->beginTransaction();
        $actSolicitud1 = $this->_db->prepare("
                  UPDATE
                    st_d001_solicitud
                  SET
                   num_estatus=:num_estatus,
                   fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                    fec_ultima_modificacion=NOW()
                  WHERE
                     pk_num_solicitud='" . $idSolicitud . "'
        ");
        $actSolicitud1->execute(array(
            'num_estatus' => $estatus + 1
        ));

        $actSolicitud2 = $this->_db->query("
                    UPDATE 
                        st_d002_registro
                    SET 
                        $campo = NOW()
                    WHERE 
                        fk_std001_num_relacion = '" . $idSolicitud . "'
        ");

        $error = $actSolicitud1->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idSolicitud;
        }
    }


    public function metEquipo()
    {
        $equipo = $this->_db->query(
            "SELECT * FROM rh_b001_empleado,a003_persona,af_b001_activo
WHERE 
             rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona 
             AND af_b001_activo.fk_a003_num_persona_responsable=a003_persona.pk_num_persona
             AND af_b001_activo.num_flag_activo_tecnologico='1'
          AND rh_b001_empleado.pk_num_empleado='" . $this->atIdEmpleado . "'
          

"
        );
        $equipo->setFetchMode(PDO::FETCH_ASSOC);
        return $equipo->fetchAll();
    }




    public function metTipo()
    {
        $tipo = $this->_db->query(
            "SELECT * FROM st_b002_tipo_solicitud"
        );
        $tipo->setFetchMode(PDO::FETCH_ASSOC);
        return $tipo->fetchAll();
    }



    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT a.pk_num_empleado, b.ind_nombre1, b.ind_apellido1 FROM rh_b001_empleado AS a, a003_persona AS b
             WHERE a.fk_a003_num_persona=b.pk_num_persona  ORDER BY b.ind_nombre1 ASC");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $usuario->fetchAll();
    }


    public function metListarDependencia()
    {
        // Consulto las dependencias de la contraloría y se las asigno a la variable $dependencia
        $dependencia = $this->_db->query(
            "SELECT pk_num_dependencia, ind_dependencia, num_piso FROM a004_dependencia ORDER BY ind_dependencia"
        );
        #devuelvo la consulta para ser usada en php con formato json.
        #PDO::FETCH_ASSOC: devuelve un array indexado por los nombres de las columnas del conjunto de resultados.
        $dependencia->setFetchMode(PDO::FETCH_ASSOC);
        #retorno lo consultado al controlador para ser usado.
        #fetchAll — Devuelve un array que contiene todas las filas del conjunto de resultados
        return $dependencia->fetchAll();
    }



    // empleado

    public function metListarEmpleado()
    {
        $listarEmpleado =  $this->_db->query(
            "
          SELECT * FROM 
          rh_b001_empleado,a018_seguridad_usuario,a003_persona
          WHERE 
        a018_seguridad_usuario.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado
          AND
          rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona
          AND 
          rh_b001_empleado.pk_num_empleado='" . $this->atIdUsuario . "'"
        );
        $listarEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEmpleado->fetchAll();
    }





    public function metListar()
    {
        $usuarios = $this->_db->query(
            "SELECT *
    FROM a003_persona,af_b001_activo
         WHERE 
        a003_persona.pk_num_persona=af_b001_activo.fk_a003_num_persona_responsable
         ORDER BY ind_nombre1 ASC");
        $usuarios->setFetchMode(PDO::FETCH_ASSOC);
        return $usuarios->fetchAll();
    }





}
