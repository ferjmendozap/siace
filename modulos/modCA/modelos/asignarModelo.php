<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de almacenes
class asignarModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListar()
    {
        $lista =  $this->_db->query(
            " SELECT
         *
          FROM st_d001_solicitud 
		  LEFT JOIN
          af_b001_activo ON st_d001_solicitud.ind_equipo=af_b001_activo.pk_num_activo
      
          WHERE
          st_d001_solicitud.num_estatus='2' 
          AND fk_rhb001_num_empleado_supervisor='" . $this->atIdEmpleado . "'
"
        );
        $lista->setFetchMode(PDO::FETCH_ASSOC);
        return $lista->fetchAll();
    }

    public function metVer($pkNumSolicitud, $metodo)
    {
        $verAprobar =  $this->_db->query(
            "SELECT * FROM  st_d001_solicitud
       LEFT JOIN a004_dependencia ON st_d001_solicitud.fk_a004_num_dependencia=a004_dependencia.pk_num_dependencia
 WHERE pk_num_solicitud='$pkNumSolicitud'"
        );
        $verAprobar->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verAprobar->fetch();
        } else {
            return $verAprobar->fetchAll();
        }

    }

    public function metVerALL()
    {
        $usuario =  $this->_db->query("
            select * from 
              st_d001_solicitud,
              rh_b001_empleado,
              a003_persona 
              where 
              st_d001_solicitud.fk_rhb001_num_empleado_solicitante=rh_b001_empleado.pk_num_empleado
              and rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona  
              and st_d001_solicitud.num_estatus='2'
              AND st_d001_solicitud.fk_rhb001_num_empleado_supervisor = '$this->atIdEmpleado'
              "
        );
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();

    }
    public function metVerUsuario($pkNumAlmacen)
    {
        $verUsuario =  $this->_db->query(
            "select d.ind_nombre1, d.ind_apellido1 from ad_c001_almacen as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.pk_num_almacen='$pkNumAlmacen'"
        );
        $verUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $verUsuario->fetchAll();

    }


    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT *
 from rh_b001_empleado,a003_persona,rh_c076_empleado_organizacion,a004_dependencia,st_d001_grupo_tecnica
         where 
         rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona AND rh_b001_empleado.pk_num_empleado=rh_c076_empleado_organizacion.fk_rhb001_num_empleado AND rh_c076_empleado_organizacion.fk_a004_num_dependencia=a004_dependencia.pk_num_dependencia AND a004_dependencia.pk_num_dependencia=st_d001_grupo_tecnica.fk_a004_num_dependencia
         order by ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }


    public function metTipo()
    {
        $tipo = $this->_db->query(
            "SELECT * FROM st_b002_tipo_solicitud"
        );
        $tipo->setFetchMode(PDO::FETCH_ASSOC);
        return $tipo->fetchAll();
    }




    public function metEditar($pkNumSolicitud,$pk_num_empleado,$ind_detalles_supervisor)
    {
        $this->_db->beginTransaction();
        $con = $this->_db->query("select ind_orden_actividad from st_c002_actividad WHERE ind_nombre_actividad='asignar' AND ind_estatus='1'");
        $con->setFetchMode(PDO::FETCH_ASSOC);
        $data1 = $con->fetch();
        $valor = $data1['ind_orden_actividad'];

        $con2 = $this->_db->query("select ind_orden_actividad from st_c002_actividad WHERE st_c002_actividad.ind_estatus='1' AND st_c002_actividad.ind_orden_actividad>'$valor' ORDER BY ind_orden_actividad ASC  LIMIT 1");
        $con2->setFetchMode(PDO::FETCH_ASSOC);
        $data2 = $con2->fetch();
        $actualizado = $data2['ind_orden_actividad'];

        $this->_db->query(
            "update st_d001_solicitud set num_estatus='$valor',fk_rhb001_num_empleado_asignado='$pk_num_empleado',ind_detalles_supervisor='$ind_detalles_supervisor'  where pk_num_solicitud='$pkNumSolicitud'"
        );
        $this->_db->query(
            "update st_d002_registro set st_d002_registro.fec_asignado=NOW()  where st_d002_registro.fk_std001_num_relacion='$pkNumSolicitud'"
        );
        $this->_db->commit();
    }

    public function metEliminar($pkNumSolicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_d001_solicitud where pk_num_solicitud = '$pkNumSolicitud'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metBuscarAlmacen($pk_num_solicitud)
    {
        if($pk_num_solicitud == false) {
            $buscarSolicitud = $this->_db->query(
                "select st_d001_solicitud.pk_num_solicitud where pk_num_solicitud='$pk_num_solicitud'"
            );
            $buscarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarSolicitud->fetch();
            $arreglo = $buscarSolicitud->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            $buscarSolicitud = $this->_db->query(
                "select pk_num_solicitud from st_d001_solicitud where  pk_num_solicitud<>'$pk_num_solicitud'"
            );
            $buscarSolicitud->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarSolicitud->fetch();
            $arreglo = $buscarSolicitud->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        }

    }
}// fin de la clase
?>
