<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de almacenes
class completarModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListar()
    {
        $lista =  $this->_db->query(
            "
          SELECT *
          FROM st_d001_solicitud 
		  LEFT JOIN
          af_b001_activo ON st_d001_solicitud.ind_equipo=af_b001_activo.pk_num_activo

          WHERE
          st_d001_solicitud.num_estatus='4' AND fk_rhb001_num_empleado_asignado='" . $this->atIdEmpleado . "'
          

 "
        );
        $lista->setFetchMode(PDO::FETCH_ASSOC);
        return $lista->fetchAll();
    }


    public function metVer($pkNumSolicitud, $metodo)
    {
        $verAprobar =  $this->_db->query(
            "
                SELECT 
			st_d001_solicitud.*,
			a004.ind_dependencia AS dependencia ,
			st_b002.ind_tipo_solicitud AS tiposolicitud,
			st_b003.ind_soporte_realizado AS  soporte,
			st_d001e.ind_evaluacion AS evaluacion,
			af_b001.ind_descripcion AS descrip, 
			st_d001_mod.ind_modalidad AS modalidad, 
			CONCAT(vl_rh.ind_nombre1,' ',vl_rh.ind_apellido1)  AS nombres,	
			CONCAT(vl_rh1.ind_nombre1,' ',vl_rh1.ind_apellido1)  AS funcionario
			  FROM st_d001_solicitud 
			  LEFT JOIN
            st_d001_evaluacion st_d001e ON st_d001e.pk_num_evaluacion = st_d001_solicitud.fk_num_evaluacion
			  LEFT JOIN
            a004_dependencia  a004 ON a004.pk_num_dependencia =  st_d001_solicitud.fk_a004_num_dependencia
			  LEFT JOIN
			st_b002_tipo_solicitud st_b002 ON st_b002.pk_num_tipo_solicitud = st_d001_solicitud.fk_num_tipo_solicitud
		      LEFT JOIN
			st_b002_tipo_soporte st_b003 ON st_b003.pk_num_tipo_soporte= st_d001_solicitud.fk_num_tipo_soporte
              LEFT JOIN
			st_d001_modalidad st_d001_mod ON st_d001_mod.pk_num_modalidad= st_d001_solicitud.fk_num_modalidad
              LEFT JOIN
			af_b001_activo af_b001 ON af_b001.pk_num_activo= st_d001_solicitud.ind_equipo
			  LEFT JOIN
			  vl_rh_persona_empleado_datos_laborales vl_rh1 ON vl_rh1.pk_num_empleado = st_d001_solicitud.fk_rhb001_num_empleado_asignado
            
              INNER JOIN
			  vl_rh_persona_empleado_datos_laborales vl_rh ON vl_rh.pk_num_empleado = st_d001_solicitud.fk_rhb001_num_empleado_solicitante
              WHERE
               pk_num_solicitud='$pkNumSolicitud'
        
                       "
        );
        $verAprobar->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verAprobar->fetch();
        } else {
            return $verAprobar->fetchAll();
        }
    }

    public function metVerActividad($pkNumSolicitud, $metodo)
    {
        $verAprobar =  $this->_db->query(
            "SELECT * FROM  st_c003_actividades_analista WHERE st_c003_actividades_analista.pk_num_actividades_analista='$pkNumSolicitud'"
        );
        $verAprobar->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verAprobar->fetch();
        } else {
            return $verAprobar->fetchAll();
        }

    }
    public function metVerALL()
    {
        $usuario =  $this->_db->query(
            "select * from 
              st_d001_solicitud,
              rh_b001_empleado,
              a003_persona 
              where 
              st_d001_solicitud.fk_rhb001_num_empleado_solicitante=rh_b001_empleado.pk_num_empleado
              and rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona 
               and st_d001_solicitud.num_estatus='4'
            AND st_d001_solicitud.fk_rhb001_num_empleado_asignado='" . $this->atIdEmpleado . "'
            "
        );
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();

    }

    public function metModalidad()
    {
        $modalidad= $this->_db->query("
          SELECT * FROM 
         st_d001_modalidad
       "
        );
        $modalidad->setFetchMode(PDO::FETCH_ASSOC);
        return $modalidad->fetchAll();
    }

    public function metTipo()
    {
        $tipo = $this->_db->query(
            "SELECT * FROM st_b002_tipo_soporte"
        );
        $tipo->setFetchMode(PDO::FETCH_ASSOC);
        return $tipo->fetchAll();
    }


    public function metVerUsuario($pkNumAlmacen)
    {
        $verUsuario =  $this->_db->query(
            "select d.ind_nombre1, d.ind_apellido1 from ad_c001_almacen as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.pk_num_almacen='$pkNumAlmacen'"
        );
        $verUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $verUsuario->fetchAll();

    }




    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT a.fk_a003_num_persona, a.pk_num_empleado, b.ind_nombre1, b.ind_apellido1 from rh_b001_empleado as a, a003_persona as b
         where a.fk_a003_num_persona=b.pk_num_persona order by b.ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }


    public function metEditar($pkNumSolicitud,$ind_detalles_analista_final,$fk_num_modalidad,$fk_num_tipo_solicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update st_d001_solicitud set num_estatus='5',ind_detalles_analista_final='$ind_detalles_analista_final',fk_num_modalidad='$fk_num_modalidad',fk_num_tipo_soporte='$fk_num_tipo_solicitud'  where pk_num_solicitud='$pkNumSolicitud'"
        );
        $this->_db->query(
            "update st_d002_registro set st_d002_registro.fec_culminado=NOW()  where st_d002_registro.fk_std001_num_relacion='$pkNumSolicitud'"
        );
        $this->_db->commit();
    }



    public function metGuardarActividad($pkNumSolicitud, $ind_actividad, $ind_porcentaje_ejecucion)
    {
        $this->_db->beginTransaction();
        $registroUnidades = $this->_db->prepare("
          INSERT INTO
           st_c003_actividades_analista
          SET
            ind_actividad =:ind_actividad,
            ind_porcentaje_ejecucion = :ind_porcentaje_ejecucion,
            num_requiere_auto=:num_requiere_auto
          ");

        $registroUnidades->execute(array(
            'ind_actividad'=>$ind_actividad,
            'ind_porcentaje_ejecucion'=>$ind_porcentaje_ejecucion,
            'num_requiere_auto'=>'0'
        ));
        $idRegistroa= $this->_db->lastInsertId();

        $fallaTansaccion = $registroUnidades->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            #$idRegistroa= $this->_db->lastInsertId();
            $this->_db->commit();

        }
        return $idRegistroa;
    }





    public function metEliminar($pkNumSolicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_d001_solicitud where pk_num_solicitud = '$pkNumSolicitud'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }


}// fin de la clase
?>
