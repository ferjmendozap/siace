<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de almacenes
class mantenimientoModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
    }


    public function metListar()
    {
        $lista =  $this->_db->query(
            "SELECT * FROM  af_b001_activo,a003_persona WHERE af_b001_activo.fk_a003_num_persona_usuario=a003_persona.pk_num_persona AND 

af_b001_activo.num_flag_activo_tecnologico='1'"
        );
        $lista->setFetchMode(PDO::FETCH_ASSOC);
        return $lista->fetchAll();
    }


    public function metVer($pk_num_activo, $metodo)
    {
        $verAprobar =  $this->_db->query(
            "SELECT * FROM  af_b001_activo  WHERE pk_num_activo='$pk_num_activo'"
        );
        $verAprobar->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1){
            return $verAprobar->fetch();
        } else {
            return $verAprobar->fetchAll();
        }
    }


    public function metVerUsuario($pkNumAlmacen)
    {
        $verUsuario =  $this->_db->query(
            "select d.ind_nombre1, d.ind_apellido1 from ad_c001_almacen as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona and a.pk_num_almacen='$pkNumAlmacen'"
        );
        $verUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $verUsuario->fetchAll();

    }

    public function metListarUsuario()
    {
        $usuario = $this->_db->query(
            "SELECT *
 from rh_b001_empleado,a003_persona,rh_c076_empleado_organizacion,a004_dependencia,st_d001_grupo_tecnica
         where
         rh_b001_empleado.fk_a003_num_persona=a003_persona.pk_num_persona AND rh_b001_empleado.pk_num_empleado=rh_c076_empleado_organizacion.fk_rhb001_num_empleado AND rh_c076_empleado_organizacion.fk_a004_num_dependencia=a004_dependencia.pk_num_dependencia AND a004_dependencia.pk_num_dependencia=st_d001_grupo_tecnica.fk_a004_num_dependencia
         order by ind_nombre1 asc");
        $usuario->setFetchMode(PDO::FETCH_ASSOC);
        return $usuario->fetchAll();
    }


    public function metEditar($num_flag_activo_tecnologico,$pk_num_activo)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update af_b001_activo set num_flag_activo_tecnologico='$num_flag_activo_tecnologico'  where pk_num_activo='$pk_num_activo'"
        );
        $this->_db->commit();
    }


    public function metEliminar($pkNumSolicitud)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from st_d001_solicitud where pk_num_solicitud = '$pkNumSolicitud'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }


}// fin de la clase
?>
