<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Clase donde se genera el reporte general de consumo de todas las estaciones de trabajo registradas en el sistema
class planificacionControlador extends Controlador
{
    private $atPlanificacion;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.s
        $this->atPlanificacion = $this->metCargarModelo('planificacion');

    }

    #Metodo Index del controlador Prueba.
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker'
        );
        $js = array('materialSiace/core/demo/DemoTableDynamic', 'materialSiace/App', 'materialSiace/core/demo/DemoFormComponents');
        $dateRage[]= 'moment/moment.min';
        $datepickerJs[] = 'bootstrap-datepicker/bootstrap-datepicker';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($datepickerJs);
        $this->atVista->metCargarJsComplemento($dateRage);
        $this->atVista->metCargarJs($js);
        $validarjs= array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validarjs);
        $asistencias = $this->atPlanificacion->metConsultart();
        $this->atVista->assign('asistencias', $asistencias);
        $this->atVista->metRenderizar('listadoRed');
    }

    public function metReporteGeneral()
    {
        $this->metObtenerLibreria('cabeceraMaestros', 'modCA');
        $pdf = new pdfPlanificacion('L','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true,10); // establece el margen inferior
        $pdf->SetFont('Arial', '', 10);
        if(isset($_POST['fi'])!='') {
            $fi = $_POST['fi'];
            $fechaIniciob=date("Y-m-d 00:i:s",strtotime($fi));
            $ff = $_POST['ff'];
            $fechaFinb=date("Y-m-d 23:59:59",strtotime($ff));
            $campo = $_POST['ind_orden_actividad'];

            $tipo= $this->atPlanificacion->metConsultara($fechaIniciob, $fechaFinb,$campo);
            $tipo2= $this->atPlanificacion->metConsultarb($fechaIniciob, $fechaFinb,$campo);
            $listarArchivos= $this->atPlanificacion->metConsultar($fechaIniciob, $fechaFinb,$campo);
            $pdf->SetWidths(array(10,20,20,20,20,20,80,30, 30));
            #$contador=0;
            foreach ($listarArchivos as $listarArchivo) {
                $pdf->Row(array(
                    utf8_decode($listarArchivo['pk_num_solicitud']),
                    utf8_decode($listarArchivo['fecha_solicitud']),
                    utf8_decode($listarArchivo['dependencia']),
                    utf8_decode($listarArchivo['funcionario']),
                    utf8_decode($listarArchivo['descrip']),
                    utf8_decode($listarArchivo['soporte']),
                    utf8_decode($listarArchivo['ind_detalles']),
                    $listarArchivo['nombres'],
                    $listarArchivo['ind_estatus']
                ),9);
                #$prueba=++$contador;
            }
       #$pdf->MultiCell(155, 20, 'Total de solicitudes : ' .$prueba, 0, 'L', 0);
            $pdf->MultiCell(155, 20, 'Total de solicitudes de servicio a Aplicaciones : ' .$tipo['total1'], 0, 'L', 0);
            $pdf->MultiCell(155, 20, 'Total de solicitudes de servicio al Hardware : ' .$tipo2['total2'], 0, 'L', 0);


        $y = $pdf->GetY();
        if($y>230){
            $pdf->AddPage();
        }



        $pdf->Output('doc.pdf', 'I');
        }

        else {
            echo "<script languaje='javascript' type='text/javascript'>
            alert('Debe cargar el reporte desde formulario de fechas'); window.close();</script>";
        }
    }





    // función que permite verificar las fechas recibidas
    public function metVerificarFechas()
    {
        $fechaAux = explode("/",$_POST['fechaInicio']);
        $fechaInicio = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
        $fechaAux = explode("/",$_POST['fechaFin']);
        $fechaFin = $fechaAux[2].'-'.$fechaAux[1].'-'.$fechaAux[0];
        if(strtotime($fechaFin) >= strtotime($fechaInicio)) {
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }

}
?>
