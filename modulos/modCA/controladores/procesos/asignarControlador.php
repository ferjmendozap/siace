<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminació
class asignarControlador extends Controlador
{
    private $atAsignarModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atAsignarModelo = $this->metCargarModelo('asignar');
    }

    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef'
        );
        $complementosJs = array('select2/select2.min');
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('_PruebaPost', $this->atAsignarModelo->metVerAll());
        $this->atVista->metRenderizar('listado');
    }

    //Método que permite actualizar la solicitud
    public function metEditar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $valido = $this->metObtenerInt('valido');
        if (isset($valido) && $valido == 1) {
            $pk_num_empleado = $this->metObtenerTexto('pk_num_empleado');
            $ind_detalles_supervisor = $this->metObtenerTexto('ind_detalles_supervisor');
            $this->metValidarToken();
            $this->atAsignarModelo->metEditar($pkNumSolicitud, $pk_num_empleado, $ind_detalles_supervisor);
            $arrayPost = array(
                'status' => 'modificar',
                'pk_num_solicitud' => $pkNumSolicitud,
                'ind_empleado' => $pk_num_empleado,
                'ind_detalles_supervisor' => $ind_detalles_supervisor
            );
            echo json_encode($arrayPost);
            exit;
        }
        if (!empty($pkNumSolicitud)) {
            $form = $this->atAsignarModelo->metVer($pkNumSolicitud, 1);
            $form = array(
                'status' => '1',
                'ind_detalles_supervisor' => $form['ind_detalles_supervisor'],
                'pk_num_solicitud' => $pkNumSolicitud,
            );
            $usuario = $this->atAsignarModelo->metListarUsuario();
            $this->atVista->assign('usuario', $usuario);
            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('editar', 'modales');
        }
    }

    //Método que permite eliminar
    public function metEliminar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pkNumSolicitud');
        $this->atAsignarModelo->metEliminar($pkNumSolicitud);
        $arrayPost = array(
            'status' => 'OK',
            'pkNumSolicitud' => $pkNumSolicitud
        );
        echo json_encode($arrayPost);
    }

    //Método que permite visualizar
    public function metVer()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $almacen = $this->atAsignarModelo->metVer($pkNumSolicitud, 2);
        $usuario = $this->atAsignarModelo->metVerUsuario($pkNumSolicitud);
        $this->atVista->assign('usuario', $usuario);
        $this->atVista->assign('almacen', $almacen);
        $this->atVista->metRenderizar('ver', 'modales');
    }


}

?>