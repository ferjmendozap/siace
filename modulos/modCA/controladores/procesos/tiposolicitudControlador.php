<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación
class tiposolicitudControlador extends Controlador
{
    private $atMantenimientoModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atMantenimientoModelo = $this->metCargarModelo('tiposolicitud');
    }

    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef'
        );
        $complementosJs = array('select2/select2.min');
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('_PruebaPost', $this->atMantenimientoModelo->metListar());
        $this->atVista->metRenderizar('listado');
    }

    //Método que permite actualizar el estado del tipo de la solicitud
    public function metEditar()
    {
        $valido = $this->metObtenerInt('valido');
        $pk_num_tipo_solicitud = $this->metObtenerInt('pk_num_tipo_solicitud');
        if (isset($valido) && $valido == 1) {
            $ind_tipo_solicitud = $this->metObtenerFormulas('ind_tipo_solicitud');
            $pk_num_tipo_soporte = $this->metObtenerInt('pk_num_tipo_soporte');

            $this->metValidarToken();
            $this->atMantenimientoModelo->metEditar($ind_tipo_solicitud, $pk_num_tipo_soporte, $pk_num_tipo_solicitud);
            $arrayPost = array(
                'status' => 'modificar',
                'ind_tipo_solicitud' => $ind_tipo_solicitud,
                'pk_num_tipo_solicitud' => $pk_num_tipo_solicitud,
                'pk_num_tipo_soporte' => $pk_num_tipo_soporte
            );
            echo json_encode($arrayPost);
            exit;
        }
        if (!empty($pk_num_tipo_solicitud)) {
            $form = $this->atMantenimientoModelo->metVer($pk_num_tipo_solicitud, 1);
            $form = array(
                'status' => '1',
                'pk_num_tipo_solicitud' => $pk_num_tipo_solicitud,
                'ind_soporte_realizado' => $form['ind_soporte_realizado'],
                'ind_tipo_solicitud' => $form['ind_tipo_solicitud']
            );
            $soporte = $this->atMantenimientoModelo->metTipoSoporte();
            $this->atVista->assign('soporte', $soporte);

            $this->atVista->assign('form', $form);

            $this->atVista->metRenderizar('editar', 'modales');
        }
    }

    public function metSolicitud()
    {
        $valido = $this->metObtenerInt('valido');
        if (isset($valido) && $valido == 1) {
            $ind_tipo_solicitud = $this->metObtenerTexto('ind_tipo_solicitud');
            $fk_num_tipo_soporte = $this->metObtenerTexto('fk_num_tipo_soporte');
            $num_requiere_auto = $this->metObtenerTexto('num_requiere_auto');
            $idRegistro = $this->atMantenimientoModelo->metGuardarNueva($ind_tipo_solicitud, $fk_num_tipo_soporte, $num_requiere_auto);
            $arrayPost = array(
                'ind_tipo_solicitud' => $ind_tipo_solicitud,
                'fk_num_tipo_soporte' => $fk_num_tipo_soporte,
                'num_requiere_auto' => $num_requiere_auto
            );
            echo json_encode($arrayPost);
            exit;
        }
        $form = array(
            'ind_tipo_solicitud ' => null,
            'fk_num_tipo_soporte' => null,
            'num_requiere_auto' => null
        );

        $soporte = $this->atMantenimientoModelo->metTipoSoporte();
        $this->atVista->assign('soporte', $soporte);

        $this->atVista->assign('form', $form);
        $this->atVista->metRenderizar('nuevo', 'modales');
    }



    //Método que permite eliminar
    public function metEliminar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_tipo_solicitud');
        $this->atMantenimientoModelo->metEliminar($pkNumSolicitud);
        $arrayPost = array(
            'status' => 'OK',
            'pk_num_tipo_solicitud' => $pkNumSolicitud
        );
        echo json_encode($arrayPost);
    }

    //Método que permite visualizar
    public function metVer()
    {
        $pk_num_tipo_solicitud = $this->metObtenerInt('pk_num_tipo_solicitud');
        $alm = $this->atMantenimientoModelo->metVer($pk_num_tipo_solicitud, 2);
        $this->atVista->metRenderizar('ver', 'modales');
    }


    // Método que permite verificar
    public function metVerificar()
    {
        $pk_num_solicitud = $this->metObtenerInt("pk_num_solicitud");
        $almacenExiste = $this->atMantenimientoModelo->metBuscar($pk_num_solicitud);
        echo json_encode(!$almacenExiste);
    }
}

?>
