<?php

/****************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Parque Automotor
 * PROCESO: Listado de revisiones de vehículos
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS           |               CORREO                |          TELEFONO              |
 * | 1 |           José Pereda                  |dt.ait.programador2@cgesucre.gob.ve  |         04248040078            |
 * |   |                                        |                                     |                                |
 * |___|________________________________________|_____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        09-07-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************/
class mantenimientocControlador extends Controlador
{
    private $atMantenimientoModelo;


    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atMantenimientoModelo = $this->metCargarModelo('mantenimiento');

    }


    #Metodo Index del controlador Mantenimiento.
    public function metIndex()
    {

    }

    #Metodo parac cargar el listado de las Mantenimiento
    public function metListarMantenimiento()
    {

        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.tableTools4029',
            'DataTables/extensions/dataTables.colVis941e',
            'multi-select/multi-select555c',

        );

        $js = array(
            'materialSiace/core/demo/DemoTableDynamic',

        );
        $complementosJs = array(

            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'inputmask/jquery.inputmask.bundle.min',
            'multi-select/jquery.multi-select',);


        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);


        $this->atVista->assign('_MantenimientoPost', $this->atMantenimientoModelo->getMantenimiento());
        $this->atVista->metRenderizar('ListadoMantenimiento');
    }


    #Funcion para redireccionar a un lugar dentro de la aplicacion


#Metodo para cargar el form registro  póliza
    public function metRegistrarMantenimiento()
    {


        $valido = $this->metObtenerInt('valido');


        $Accion = array("accion" => "registrar",);

        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );

        if ($valido == 1) {

            #Recibiendo las variables del formulario que utiliza la clase mantenimiento
            $formInt = $this->metObtenerInt('form', 'int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form', 'alphaNum');
            $formTxt = $this->metObtenerTexto('form', 'txt');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
            if (!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if (!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
            $validacion['mantenimiento'] = $this->FormatoFecha($validacion['mantenimiento']);

            $validacion['mantenimiento'] = trim($validacion['mantenimiento'] . " 00:00:00");


            $validacion['prox_mantenimiento'] = $this->FormatoFecha($validacion['prox_mantenimiento']);


            $validacion['prox_mantenimiento'] = trim($validacion['prox_mantenimiento'] . " 00:00:00");


            $id = $this->atMantenimientoModelo->setMantenimiento($validacion['ocasion'], $validacion['mantenimiento'], $validacion['prox_mantenimiento'], $validacion['kilometraje'], $validacion['observacion'], $validacion['TipoMantenimiento'], $validacion['Vehiculo']);
        }
        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost', $this->_VehiculoModelo->getVehiculo());

        $this->_tipoMantenimientoModelo = $this->metCargarModelo('tipoMantenimiento');
        $this->atVista->assign('_tipoMantenimientoPost', $this->_tipoMantenimientoModelo->getTipoMantenimiento());

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);

        //$this->atVista->assign('_MantenimientoPost',$this->atMantenimientoModelo->Mostrar($id_valor));
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarMantenimiento', "modales");


    }


    #Metodo para cargar el form registro  póliza
    public function metModificarMantenimiento()
    {


        $valido = $this->metObtenerInt('valido');
        $id_valor = $this->metObtenerInt('idPost');

        $Accion = array("accion" => "modificar");


        $js = array(
            'materialSiace/core/demo/DemoFormWizard',
            'materialSiace/core/demo/DemoFormComponents'

        );

        $complementosJs = array(
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $complementosCss = array(
            'wizard/wizardfa6c',
            'jquery-validation/dist/site-demo',
        );


        if ($valido == 1) {

            #Recibiendo las variables del formulario que utiliza la clase mantenimiento
            $formInt = $this->metObtenerInt('form', 'int');
            $formAlphaNum = $this->metObtenerAlphaNumerico('form', 'alphaNum');
            $formTxt = $this->metObtenerTexto('form', 'txt');
            if (!empty($formInt)) {
                foreach ($formInt as $tituloInt => $valorInt) {
                    if (!empty($formInt[$tituloInt])) {
                        $validacion[$tituloInt] = $valorInt;
                    } else {
                        $validacion[$tituloInt] = 'error';
                    }
                }
            }
            if (!empty($formAlphaNum)) {
                foreach ($formAlphaNum as $tituloAlphaNum => $valorAlphaNum) {
                    if (!empty($formAlphaNum[$tituloAlphaNum]) && $formAlphaNum[$tituloAlphaNum] != '') {
                        $validacion[$tituloAlphaNum] = $valorAlphaNum;
                    } else {
                        $validacion[$tituloAlphaNum] = 'error';
                    }
                }
            }

            if (!empty($formTxt)) {
                foreach ($formTxt as $tituloTxt => $valorTxt) {
                    if (!empty($formTxt[$tituloTxt]) && $formTxt[$tituloTxt] != '') {
                        $validacion[$tituloTxt] = $valorTxt;
                    } else {
                        $validacion[$tituloTxt] = 'error';
                    }
                }
            }
            $validacion['mantenimiento'] = $this->FormatoFecha($validacion['mantenimiento']);

            $validacion['mantenimiento'] = trim($validacion['mantenimiento'] . " 00:00:00");


            $validacion['prox_mantenimiento'] = $this->FormatoFecha($validacion['prox_mantenimiento']);


            $validacion['prox_mantenimiento'] = trim($validacion['prox_mantenimiento'] . " 00:00:00");


            $id = $this->atMantenimientoModelo->updateMantenimiento($validacion['ocasion'], $validacion['mantenimiento'], $validacion['prox_mantenimiento'], $validacion['kilometraje'], $validacion['observacion'], $validacion['TipoMantenimiento'], $validacion['Vehiculo'], $validacion['idPost']);
        }


        $this->atVista->assign('_MantenimientoPost', $this->atMantenimientoModelo->Mostrar($id_valor));

        $this->_VehiculoModelo = $this->metCargarModelo('vehiculo');
        $this->atVista->assign('_VehiculoPost', $this->_VehiculoModelo->getVehiculo());

        $this->_tipoMantenimientoModelo = $this->metCargarModelo('tipoMantenimiento');
        $this->atVista->assign('_tipoMantenimientoPost', $this->_tipoMantenimientoModelo->getTipoMantenimiento());

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementosCss);

        $this->atVista->assign('_MantenimientoPost', $this->atMantenimientoModelo->Mostrar($id_valor));
        $this->atVista->assign('_Acciones', $Accion);

        $this->atVista->metRenderizar('RegistrarMantenimiento', "modales");


    }


    public function metEliminarMantenimiento()
    {

        $id_valor = $this->metObtenerInt('idPost');

        $this->atMantenimientoModelo->deleteMantenimiento($id_valor);


    }

    protected function FormatoFecha($fecha)
    {
        $resultado = explode("/", $fecha);
        $resultado = $resultado[2] . "-" . $resultado[1] . "-" . $resultado[0];
        return $resultado;

    }

}