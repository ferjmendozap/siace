<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación
class mantenimientoControlador extends Controlador
{
    private $atMantenimientoModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atMantenimientoModelo = $this->metCargarModelo('mantenimiento');
    }

    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef'
        );
        $complementosJs = array('select2/select2.min');
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('_PruebaPost', $this->atMantenimientoModelo->metListar());
        $this->atVista->metRenderizar('listado');
    }

    //Método que permite actualizar el estado de la solicitud por revisar
    public function metEditar()
    {
        $pk_num_activo = $this->metObtenerInt('pk_num_activo');
        $valido = $this->metObtenerInt('valido');
        if (isset($valido) && $valido == 1) {
            $num_flag_activo_tecnologico = $this->metObtenerInt('num_flag_activo_tecnologico');
            $this->metValidarToken();
            $this->atMantenimientoModelo->metEditar($num_flag_activo_tecnologico, $pk_num_activo);
            $arrayPost = array(
                'status' => 'modificar',
                'num_flag_activo_tecnologico' => $num_flag_activo_tecnologico,
                'pk_num_activo' => $pk_num_activo
            );
            echo json_encode($arrayPost);
            exit;
        }
        if (!empty($pk_num_activo)) {
            $form = $this->atMantenimientoModelo->metVer($pk_num_activo, 1);
            $form = array(
                'status' => '1',
                'pk_num_activo' => $pk_num_activo,
                'num_flag_activo_tecnologico' => $form['num_flag_activo_tecnologico']
            );
            $usuario = $this->atMantenimientoModelo->metListarUsuario();
            $this->atVista->assign('usuario', $usuario);
            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('editar', 'modales');
        }
    }

    //Método que permite eliminar
    public function metEliminar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pkNumSolicitud');
        $this->atMantenimientoModelo->metEliminar($pkNumSolicitud);
        $arrayPost = array(
            'status' => 'OK',
            'pkNumSolicitud' => $pkNumSolicitud
        );
        echo json_encode($arrayPost);
    }

    //Método que permite visualizar
    public function metVer()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $almacen = $this->atMantenimientoModelo->metVer($pkNumSolicitud, 2);
        $usuario = $this->atMantenimientoModelo->metVerUsuario($pkNumSolicitud);
        $this->atVista->assign('usuario', $usuario);
        $this->atVista->assign('almacen', $almacen);
        $this->atVista->metRenderizar('ver', 'modales');
    }


    // Método que permite verificar
    public function metVerificar()
    {
        $pk_num_solicitud = $this->metObtenerInt("pk_num_solicitud");
        $almacenExiste = $this->atMantenimientoModelo->metBuscar($pk_num_solicitud);
        echo json_encode(!$almacenExiste);
    }
}

?>
