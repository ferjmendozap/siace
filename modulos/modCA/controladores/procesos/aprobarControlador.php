<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
//
class aprobarControlador extends Controlador
{
    private $atAprobarModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atAprobarModelo = $this->metCargarModelo('aprobar');
    }


    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'select2/select201ef'
        );
        $complementosJs = array('select2/select2.min');
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $usuario = Session::metObtener('idUsuario');
        $this->atVista->metCargarJsComplemento($validar);
        $this->atVista->assign('_PruebaPost', $this->atAprobarModelo->metListar($usuario));
        $this->atVista->metRenderizar('listado');
    }

    // Método para actualizar
    public function metEditar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $valido = $this->metObtenerInt('valido');
        if (isset($valido) && $valido == 1) {
            $this->metValidarToken();
            $this->atAprobarModelo->metEditar($pkNumSolicitud);
            $arrayPost = array(
                'status' => 'modificar',
                'pk_num_solicitud' => $pkNumSolicitud
            );
            echo json_encode($arrayPost);
            exit;
        }
        if (!empty($pkNumSolicitud)) {
            $form = $this->atAprobarModelo->metVer($pkNumSolicitud, 1);
            $usuario = $this->atAprobarModelo->metListarUsuario();
            $form = array(
                'status' => '1',
                'ind_detalles_supervisor' => $form['ind_detalles_supervisor'],
                'pk_num_solicitud' => $pkNumSolicitud,
            );

            $this->atVista->assign('usuario', $usuario);
            $this->atVista->assign('form', $form);
            $this->atVista->metRenderizar('editar', 'modales');
        }
    }


    public function metConfigurarCorreo($archivo, $nombreEvento, $correo, $nombrePersona, $cuerpoCorreo)
    {
        $this->metObtenerLibreria('PHPMailerAutoload', 'PHPMailer');
        $mail = new PHPMailer();
        //Definir que vamos a usar SMTP
        $mail->IsSMTP();

        $mail->SMTPOptions = array(
            'ssl' => array (
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        //Esto es para activar el modo depuración. En entorno de pruebas lo mejor es 2, en producción siempre 0
        // 0 = off (producción)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug  = 0;
        //Ahora definimos gmail como servidor que aloja nuestro SMTP
        $mail->Host       = 'smtp.gmail.com';
        //El puerto será el 587 ya que usamos encriptación TLS
        $mail->Port       = 587;
        //Definmos la seguridad como TLS
        $mail->SMTPSecure = 'TLS';
        //Tenemos que usar gmail autenticados, así que esto a TRUE
        $mail->SMTPAuth   = true;
        //Definimos la cuenta que vamos a usar. Dirección completa de la misma
        $mail->Username   = "i.lezama@contraloriamonagas.gob.ve";
        //Introducimos nuestra contraseña de gmail
        $mail->Password   = "i.456789";
        //Definimos el remitente (dirección y, opcionalmente, nombre)
        $mail->SetFrom('i.lezama@contraloriamonagas.gob.ve', 'SIACE');
        //Esta línea es por si queréis enviar copia a alguien (dirección y, opcionalmente, nombre)
        //$mail->AddReplyTo('i.lezama@contraloriamonagas.gob.ve','El de la réplica');
        //Y, ahora sí, definimos el destinatario (dirección y, opcionalmente, nombre)
        $mail->AddAddress($correo, $nombrePersona);
        //Definimos el tema del email
        $mail->Subject = 'Solicitud '.$nombreEvento;
        //Y por si nos bloquean el contenido HTML (algunos correos lo hacen por seguridad) una versión alternativa en texto plano (también será válida para lectores de pantalla)
        $mail->Body = $cuerpoCorreo;
        //Enviamos el correo
        $mail->addAttachment($archivo);
        // Envio el correo
        $mail->Send();
    }





    // Método para eliminar
    public function metEliminar()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $this->atAprobarModelo->metEliminar($pkNumSolicitud);
        $arrayPost = array(
            'status' => 'OK',
            'pk_num_solicitud' => $pkNumSolicitud
        );
        echo json_encode($arrayPost);
    }

    //
    public function metVer()
    {
        $pkNumSolicitud = $this->metObtenerInt('pk_num_solicitud');
        $almacen = $this->atAprobarModelo->metVer($pkNumSolicitud, 2);
        $usuario = $this->atAprobarModelo->metVerUsuario($pkNumSolicitud);
        $this->atVista->assign('usuario', $usuario);
        $this->atVista->assign('almacen', $almacen);
        $this->atVista->metRenderizar('ver', 'modales');
    }


}

?>