<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Irvin Lezama                   |i.lezama@contraloriamonagas.gob.ve   |        0424-9371200           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de tipos de documento
class ejecucionControlador extends Controlador
{
    private $atEjecucionModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo de solicitud.
        $this->atEjecucionModelo = $this->metCargarModelo('ejecucion');
    }

    // Método index del controlador
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $validar = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $this->atEjecucionModelo = $this->metCargarModelo('ejecucion');
        $usuario = $this->atEjecucionModelo->metListarUsuario();
        $this->atVista->assign('usuario', $usuario);
        $this->atVista->metRenderizar('listReporte');
    }


    public function metsalida()
    {
        $pk_num_empleado = $this->metObtenerInt('pk_num_empleado');
        $estatus = $this->metObtenerInt('num_estatus');
        $this->metObtenerLibreria('cabeceraMaestros', 'modCA');
        $pdf = new pdfEjecucion('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetFont('Arial', '', 9);
        $listarArchivo = $this->atEjecucionModelo->metListarRespuesta($pk_num_empleado,$estatus);
        $pdf->SetWidths(array(40, 40, 40, 40, 40));
        foreach ($listarArchivo as $listarArchivo) {
            $pdf->Row(array(
                utf8_decode($listarArchivo['fecha_solicitud']),
                $listarArchivo['nombres'],
                utf8_decode($listarArchivo['dependencia']),
                utf8_decode($listarArchivo['ind_detalles']),
                $listarArchivo['ind_estatus']
            ), 6);

        }
        $pdf->Output();
    }


    // Método que permite ver el reporte de solicitudes
    public function metImprimirSolicitudes()
    {
        $this->metObtenerLibreria('cabeceraMaestros', 'modCA');
        $pdf = new pdfSolicitud('L', 'mm', 'Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 10);
        $pdf->SetFont('Arial', '', 10);
        $listarArchivo = $this->atEjecucionModelo->metListarArchivo();
        $pdf->SetWidths(array(35, 30, 30, 50, 30, 15));
        foreach ($listarArchivo as $listarArchivo) {
            $pdf->Row(array(
                utf8_decode($listarArchivo['fecha_solicitud']),
                utf8_decode($listarArchivo['ind_dependencia']),
                utf8_decode($listarArchivo['ind_descripcion']),
                utf8_decode($listarArchivo['ind_detalles']),
                $listarArchivo['ind_nombre1'],
                $listarArchivo['ind_estatus']
            ), 6);

        }
        $pdf->Output();
    }

    // Método que verifica la existencia de un tipo de documento

}

