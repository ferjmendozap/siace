<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'ordenCompra' . DS . 'ordenCompraModelo.php';
class reporteContratistaModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atOrdenCompraModelo = new ordenCompraModelo();
    }

    public function metListarActaFecha($desde=false,$hasta=false){

        $filtro = "";
        if ($desde != "") $filtro.=" AND (acta.fec_registro >= '".$desde."')";
        if ($hasta != "") $filtro.=" AND (acta.fec_registro <= '".$hasta."')";

        $actaPost =  $this->_db->query("
            SELECT
              acta.*,
              acta.ind_estado AS estadoActa
            FROM lg_b009_acta_inicio AS acta
            WHERE 1 $filtro
             ");

        $actaPost->setFetchMode(PDO::FETCH_ASSOC);
        return $actaPost->fetchAll();
    }


}

?>