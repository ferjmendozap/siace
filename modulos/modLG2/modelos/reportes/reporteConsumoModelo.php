<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'clasificacionModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'commodityModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'ordenCompra' . DS . 'ordenCompraModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'almacen' . DS . 'transaccionModelo.php';
class reporteConsumoModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atCommodityModelo = new commodityModelo();
        $this->atClasificacionModelo = new clasificacionModelo();
        $this->atOrdenCompraModelo = new ordenCompraModelo();
        $this->atTransaccionModelo = new transaccionModelo();
    }

    public function metListarStock($filtro){

        $actaPost =  $this->_db->query("
            SELECT
              *,
              item.ind_descripcion AS descripcionItem,
              unidades.ind_descripcion AS unidad
            FROM lg_b002_item AS item
            INNER JOIN lg_c007_item_stock AS stock ON item.fk_lgc007_num_item_stock = stock.pk_num_item_stock
            INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = item.fk_lgb014_num_almacen
            INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = item.fk_a006_num_miscelaneo_tipo_item
            INNER JOIN lg_b004_unidades AS unidades ON item.fk_lgb004_num_unidad_compra = unidades.pk_num_unidad
            $filtro
             ");

        $actaPost->setFetchMode(PDO::FETCH_ASSOC);
        return $actaPost->fetchAll();
    }

    public function metListarTranDet($filtro){

        $actaPost =  $this->_db->query("
            SELECT
              *,
              item.ind_descripcion AS descripcionItem
            FROM lg_d001_transaccion AS transaccion
            INNER JOIN lg_d002_transaccion_detalle AS detalle ON detalle.fk_lgd001_num_transaccion = transaccion.pk_num_transaccion
            INNER JOIN lg_b002_item AS item ON detalle.fk_lgb002_num_item = item.pk_num_item
            $filtro
             ");

        $actaPost->setFetchMode(PDO::FETCH_ASSOC);
        return $actaPost->fetchAll();
    }


}

?>