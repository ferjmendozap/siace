<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'ordenCompra' . DS . 'ordenCompraModelo.php';
class reporteConfirmacionServicioModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atOrdenCompraModelo = new ordenCompraModelo();
    }

    public function metListarOrdenesServicio($where = false,$where2)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              orden.*,
              (SELECT COUNT(*) FROM lg_b021_confirmacion_servicio AS confirmacion
              WHERE confirmacion.fk_lgb019_num_orden = orden.pk_num_orden
              $where2
               ) AS confirm
            FROM
              lg_b019_orden AS orden
            WHERE
            orden.ind_tipo_orden = 'OS' 
            $where
              ");
        //var_dump($partidaCuenta);
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetchAll();
    }

    public function metListarConfirmaciones($idOrden,$where)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              confirmacion.*,
              commodity.*
            FROM
              lg_b021_confirmacion_servicio AS confirmacion
              INNER JOIN lg_b019_orden AS orden ON confirmacion.fk_lgb019_num_orden = orden.pk_num_orden
              INNER JOIN lg_c009_orden_detalle AS detalle ON orden.pk_num_orden = detalle.fk_lgb019_num_orden
              INNER JOIN lg_b003_commodity AS commodity ON detalle.fk_lgb003_num_commodity = commodity.pk_num_commodity
            WHERE
            confirmacion.fk_lgb019_num_orden = '$idOrden'
            $where
              ");
        //var_dump($partidaCuenta);
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetchAll();
    }

    public function metMostrarEmpleado($idEmpleado){
        $persona = $this->_db->query("
            SELECT
              CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre,
              per.ind_documento_fiscal,
              puesto.ind_descripcion_cargo
            FROM rh_b001_empleado AS emp
            INNER JOIN a003_persona AS per ON per.pk_num_persona = emp.fk_a003_num_persona
            LEFT JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = emp.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            WHERE
              emp.pk_num_empleado='$idEmpleado'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }
}

?>