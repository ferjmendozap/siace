<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'ordenCompra' . DS . 'ordenCompraModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'proveedorModelo.php';
class reporteProveedoresModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atOrdenCompraModelo = new ordenCompraModelo();
        $this->atProveedorModelo = new proveedorModelo();
    }


    public function metBuscarProveedores($filtro){
        $actaPost =  $this->_db->query("
          SELECT
              proveedor.*,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              CONCAT_WS(' ',persona2.ind_nombre1,persona2.ind_nombre2,persona2.ind_apellido1,persona2.ind_apellido2) AS nombre2,
              persona.*,
              direccion.*
          FROM
            lg_b022_proveedor AS proveedor
          INNER JOIN a003_persona AS persona ON persona.pk_num_persona =  proveedor.fk_a003_num_persona_proveedor
          INNER JOIN a003_persona AS persona2 ON persona2.pk_num_persona =  proveedor.fk_a003_num_persona_vendedor
          LEFT JOIN lg_e003_proveedor_servicio AS servicio ON proveedor.pk_num_proveedor = servicio.fk_lgb022_num_proveedor
          INNER JOIN a036_persona_direccion AS direccion ON persona.pk_num_persona = direccion.fk_a003_num_persona
          WHERE 1 $filtro
             ");
//        echo $filtro;
        $actaPost->setFetchMode(PDO::FETCH_ASSOC);
        return $actaPost->fetchAll();
    }

}

?>