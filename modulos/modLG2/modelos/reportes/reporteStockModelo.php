<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'clasificacionModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'commodityModelo.php';
class reporteStockModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atCommodityModelo = new commodityModelo();
        $this->atClasificacionModelo = new clasificacionModelo();
    }

    public function metListarStock($filtro){

        $actaPost =  $this->_db->query("
            SELECT
              item.*,
              item.ind_descripcion AS descripcionItem,
              unidades.ind_descripcion AS unidad,
              stock.*
            FROM lg_b002_item AS item
            INNER JOIN lg_b004_unidades AS unidades ON item.fk_lgb004_num_unidad_compra = unidades.pk_num_unidad
            INNER JOIN lg_c007_item_stock AS stock ON stock.pk_num_item_stock = item.fk_lgc007_num_item_stock
            INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = item.fk_a006_num_miscelaneo_tipo_item
            WHERE 1 $filtro
             ");

        $actaPost->setFetchMode(PDO::FETCH_ASSOC);
        return $actaPost->fetchAll();
    }


}

?>