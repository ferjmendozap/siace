<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'ordenCompra' . DS . 'ordenCompraModelo.php';
class reporteControlPerceptivoModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atOrdenCompraModelo = new ordenCompraModelo();
    }

    public function metListarControlesPreceptivos($where = false)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              perceptivo.*,
              inicio.*,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_apellido1) AS nombreProveedor,
              orden.*,
              perceptivo.*
            FROM
              lg_b020_control_perceptivo AS perceptivo
            INNER JOIN lg_b019_orden AS orden ON perceptivo.fk_lgb019_num_orden = orden.pk_num_orden
            INNER JOIN lg_b022_proveedor AS proveedor ON orden.fk_lgb022_num_proveedor = proveedor.pk_num_proveedor
            INNER JOIN a003_persona AS persona ON proveedor.fk_a003_num_persona_proveedor = persona.pk_num_persona
            LEFT JOIN lg_b013_adjudicacion AS adjudicacion ON orden.fk_lgb013_num_adjudicacion = adjudicacion.pk_num_adjudicacion
            LEFT JOIN lg_b012_informe_recomendacion AS recomendacion ON adjudicacion.fk_lgb012_num_informe_recomendacion = recomendacion.pk_num_informe_recomendacion
            LEFT JOIN lg_b011_evaluacion AS evaluacion ON recomendacion.fk_lgb011_num_evaluacion = evaluacion.pk_num_evaluacion
            LEFT JOIN lg_b009_acta_inicio AS inicio ON evaluacion.fk_lgb009_num_acta_inicio = inicio.pk_num_acta_inicio
            WHERE 1 
            $where
              ");
        //var_dump($partidaCuenta);
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetchAll();
    }

    public function metMostrarEmpleado($idEmpleado){
        $persona = $this->_db->query("
            SELECT
              CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre,
              per.ind_documento_fiscal,
              puesto.ind_descripcion_cargo
            FROM rh_b001_empleado AS emp
            INNER JOIN a003_persona AS per ON per.pk_num_persona = emp.fk_a003_num_persona
            LEFT JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = emp.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            WHERE
              emp.pk_num_empleado='$idEmpleado'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }
}

?>