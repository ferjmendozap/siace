<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'itemsModelo.php';
class reporteItemsModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atItemsModelo = new itemsModelo();
    }

    public function metListarActaFecha($desde=false,$hasta=false){

        $filtro = "";
        if ($desde != "") $filtro.=" AND (acta.fec_registro >= '".$desde."')";
        if ($hasta != "") $filtro.=" AND (acta.fec_registro <= '".$hasta."')";

        $actaPost =  $this->_db->query("
            SELECT
              acta.*,
              acta.num_estatus AS estadoActa,
              requerimiento.*
            FROM lg_b009_acta_inicio AS acta
            INNER JOIN lg_b001_requerimiento AS requerimiento ON requerimiento.pk_num_requerimiento = acta.fk_lgb001_num_requerimiento
            WHERE 1 $filtro
             ");

        $actaPost->setFetchMode(PDO::FETCH_ASSOC);
        return $actaPost->fetchAll();
    }

    public function metMostrarEmpleado($idEmpleado){
        $persona = $this->_db->query("
            SELECT
              CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre,
              per.ind_documento_fiscal,
              puesto.ind_descripcion_cargo
            FROM rh_b001_empleado AS emp
            INNER JOIN a003_persona AS per ON per.pk_num_persona = emp.fk_a003_num_persona
            LEFT JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = emp.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            WHERE
              emp.pk_num_empleado='$idEmpleado'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }


}

?>