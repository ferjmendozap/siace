<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'ordenCompra' . DS . 'ordenCompraModelo.php';
class controlPerceptivoModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atOrdenCompraModelo = new ordenCompraModelo();
    }

    public function metMostrarEmpleado($id,$campo,$idControl = false)
    {
        $where = "";
        if($idControl){
            $where = " AND perceptivo.pk_num_control_perceptivo = '$idControl'";
        }
        $c1 = "fk_rhb001_num_empleado_conforme_".$campo;
        $c2 = "fk_a006_num_miscelaneo_tipo_firmante_".$campo;
        $personaPost = $this->_db->query("
            SELECT
              p.pk_num_persona,
              p.ind_nombre1,
              p.ind_apellido1,
              p.ind_cedula_documento,
              p.ind_documento_fiscal,
              p.ind_cedula_documento AS ind_documento_fiscal,
              e.pk_num_empleado,
              puesto.ind_descripcion_cargo,
              tipo.ind_nombre_detalle
            FROM a003_persona AS p
            INNER JOIN rh_b001_empleado AS e ON p.pk_num_persona=e.fk_a003_num_persona
            LEFT JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = e.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            INNER JOIN lg_b020_control_perceptivo AS perceptivo ON e.pk_num_empleado = perceptivo.$c1
            LEFT JOIN a006_miscelaneo_detalle AS tipo ON tipo.pk_num_miscelaneo_detalle = perceptivo.$c2
            WHERE e.pk_num_empleado ='$id' 
            $where
              ");
//        var_dump($c1."--".$c2);
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        return $personaPost ->fetch();
    }

    public function metListarConPer()
    {
        $ConPerPost = $this->_db->query("
            SELECT
                *
            FROM
                lg_b020_control_perceptivo
          ");
        $ConPerPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ConPerPost->fetchAll();
    }

    public function metListarOrdenCompras()
    {
        $ConPerPost = $this->_db->query("
            SELECT
                orden.*,
                persona.ind_nombre1,
                persona.ind_apellido1,
                perceptivo.*,
                perceptivo.num_estatus AS percEstado
            FROM
                lg_b019_orden AS orden
                INNER JOIN lg_b017_clasificacion AS clasificacion ON clasificacion.pk_num_clasificacion = orden.fk_lgb017_num_clasificacion
                INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = orden.fk_lgb014_num_almacen
                INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                LEFT JOIN lg_b020_control_perceptivo AS perceptivo ON orden.pk_num_orden = perceptivo.fk_lgb019_num_orden
                WHERE orden.ind_tipo_orden = 'OC' AND (orden.ind_estado='AP' OR orden.ind_estado='CO')
          ");
        $ConPerPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ConPerPost->fetchAll();
    }

    public function metMostrarConPer($idConPer){
        $mostrarConPer = $this->_db->query("
              SELECT
                perceptivo.*
              FROM
                lg_b020_control_perceptivo AS perceptivo
                WHERE perceptivo.pk_num_control_perceptivo = '$idConPer'
        ");
        $mostrarConPer->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarConPer->fetch();
    }

    public function metMostrarConPerDetalle($idConPer=false,$cual=false){
        $where = '';
        if($idConPer AND !$cual){
            $where = " WHERE detalle.fk_lgb020_num_control_perceptivo = '$idConPer' ";
        }
        if($cual){
            $where = " WHERE detalle.fk_lgc009_num_orden_detalle = '$idConPer' ";
        }
        $mostrarConPer = $this->_db->query("
              SELECT
                detalle.fk_lgc009_num_orden_detalle,
                detalle2.num_cantidad AS cantidadPedida,
                detalle.num_cantidad AS cantidadRecibida,
                detalle.fk_lgb020_num_control_perceptivo,
                detalle.ind_observacion,
                perceptivo.num_estatus,
                item.ind_descripcion AS descripcionItem,
                commodity.ind_descripcion AS descripcionComm,
                unidad.ind_descripcion AS unidadCompra
              FROM
                lg_c010_control_perceptivo_detalle AS detalle
                INNER JOIN lg_c009_orden_detalle AS detalle2 ON detalle2.pk_num_orden_detalle = detalle.fk_lgc009_num_orden_detalle
                INNER JOIN lg_b020_control_perceptivo AS perceptivo ON detalle.fk_lgb020_num_control_perceptivo = perceptivo.pk_num_control_perceptivo
                LEFT JOIN lg_b002_item AS item ON item.pk_num_item = detalle2.fk_lgb002_num_item
                LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle2.fk_lgb003_num_commodity
                INNER JOIN lg_b004_unidades AS unidad ON item.fk_lgb004_num_unidad_compra = unidad.pk_num_unidad OR commodity.fk_lgb004_num_unidad = unidad.pk_num_unidad
              $where
        ");
        $mostrarConPer->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarConPer->fetchAll();
    }

    public function metCrearConPer($datos)
    {
        $this->_db->beginTransaction();
        $registroConPer = $this->_db->prepare("
          INSERT INTO
            lg_b020_control_perceptivo
          SET
              num_control=:num_control,
              fec_anio=:fec_anio,
              fec_registro=NOW(),
              num_estatus=1,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              fk_lgb019_num_orden=:fk_lgb019_num_orden,
              fk_rhb001_num_empleado_conforme_a=:fk_rhb001_num_empleado_conforme_a,
              fk_a006_num_miscelaneo_tipo_firmante_a=:fk_a006_num_miscelaneo_tipo_firmante_a,
              fk_rhb001_num_empleado_conforme_b=:fk_rhb001_num_empleado_conforme_b,
              fk_a006_num_miscelaneo_tipo_firmante_b=:fk_a006_num_miscelaneo_tipo_firmante_b,
              fk_rhb001_num_empleado_conforme_c=:fk_rhb001_num_empleado_conforme_c,
              fk_a006_num_miscelaneo_tipo_firmante_c=:fk_a006_num_miscelaneo_tipo_firmante_c,
              fk_rhb001_num_empleado_conforme_d=:fk_rhb001_num_empleado_conforme_d,
              fk_a006_num_miscelaneo_tipo_firmante_d=:fk_a006_num_miscelaneo_tipo_firmante_d,
              fk_rhb001_num_empleado_conforme_e=:fk_rhb001_num_empleado_conforme_e,
              fk_a006_num_miscelaneo_tipo_firmante_e=:fk_a006_num_miscelaneo_tipo_firmante_e
           ");

        $registroConPer->execute(array(
            'num_control'=>$datos['cant'],
            'fec_anio'=>date('Y'),
            'fk_lgb019_num_orden'=>$datos['idOrden'],
            'fk_rhb001_num_empleado_conforme_a'=>$datos['asisA'],
            'fk_a006_num_miscelaneo_tipo_firmante_a'=>$datos['tipoFirmanteA'],
            'fk_rhb001_num_empleado_conforme_b'=>$datos['asisB'],
            'fk_a006_num_miscelaneo_tipo_firmante_b'=>$datos['tipoFirmanteB'],
            'fk_rhb001_num_empleado_conforme_c'=>$datos['asisC'],
            'fk_a006_num_miscelaneo_tipo_firmante_c'=>$datos['tipoFirmanteC'],
            'fk_rhb001_num_empleado_conforme_d'=>$datos['asisD'],
            'fk_a006_num_miscelaneo_tipo_firmante_d'=>$datos['tipoFirmanteD'],
            'fk_rhb001_num_empleado_conforme_e'=>$datos['asisE'],
            'fk_a006_num_miscelaneo_tipo_firmante_e'=>$datos['tipoFirmanteE']
        ));
        $idRegistro= $this->_db->lastInsertId();


        $NuevoPostDetalle=$this->_db->prepare(
            "INSERT INTO
                lg_e005_documento
              SET
                fk_cpb001_num_documento_clasificacion=:fk_cpb001_num_documento_clasificacion, 
                fk_lgb019_num_orden=:fk_lgb019_num_orden, 
                fec_documento=NOW(), 
                num_anio=:num_anio, 
                num_tipo_documento='OC', 
                num_monto_afecto=:num_monto_afecto, 
                num_monto_no_afecto=:num_monto_no_afecto, 
                num_monto_impuesto=:num_monto_impuesto, 
                num_monto_total=:num_monto_total, 
                num_monto_pendiente=:num_monto_pendiente, 
                num_monto_pagado=:num_monto_pagado, 
                ind_estado=:ind_estado,
                ind_comentarios=:ind_comentarios, 
                fec_ultima_modificacion=NOW(), 
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");


        $NuevoPostDetalle->execute(array(
            'fk_cpb001_num_documento_clasificacion'=>$datos['clasificacion'],
            'fk_lgb019_num_orden'=>$datos['idOrden'],
            'num_anio'=>$datos['datosOrden']['fec_anio'],
            'num_monto_afecto'=>$datos['datosOrden']['num_monto_afecto'],
            'num_monto_no_afecto'=>$datos['datosOrden']['num_monto_no_afecto'],
            'num_monto_impuesto'=>$datos['datosOrden']['num_monto_igv'],
            'num_monto_total'=>$datos['datosOrden']['num_monto_total'],
            'num_monto_pendiente'=>$datos['datosOrden']['num_monto_pendiente'],
            'num_monto_pagado'=>$datos['datosOrden']['num_monto_pagado'],
            'ind_estado'=>'PR',
            'ind_comentarios'=>$datos['datosOrden']['ind_comentario']
        ));

        $idRegistro2= $this->_db->lastInsertId();

        $registroConPerDet = $this->_db->prepare("
          INSERT INTO
            lg_c010_control_perceptivo_detalle
          SET
              num_secuencia=:num_secuencia,
              ind_observacion=:ind_observacion,
              num_cantidad=:num_cantidad,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              fk_lgb020_num_control_perceptivo=:fk_lgb020_num_control_perceptivo,
              fk_lgc009_num_orden_detalle=:fk_lgc009_num_orden_detalle
           ");


        $NuevoPostDetalle2=$this->_db->prepare(
            "INSERT INTO
                lg_e006_documento_detalle
              SET
                num_secuencia=:num_secuencia, 
                fk_lge005_num_documento=:fk_lge005_num_documento, 
                fk_lgc009_num_orden_detalle=:fk_lgc009_num_orden_detalle, 
                num_cantidad=:num_cantidad, 
                num_precio_unit=:num_precio_unit,
                num_precio_cantidad=:num_precio_cantidad,
                num_total=:num_total,
                fec_ultima_modificacion=NOW(), 
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");
        $contador=0;

        while($contador<$datos['cantidad']){
            $contador=$contador+1;

            $registroConPerDet->execute(array(
                'num_secuencia'=>$contador,
                'ind_observacion'=>$datos['comentario'][$contador],
                'num_cantidad'=>$datos['cantRecibida'][$contador],
                'fk_lgb020_num_control_perceptivo'=>$idRegistro,
                'fk_lgc009_num_orden_detalle'=>$datos['idDetalleOrden'][$contador]
            ));

            $total = $datos['cantRecibida'][$contador]*$datos['precio'][$contador];
            $NuevoPostDetalle2->execute(array(
                'num_secuencia'=>$contador,
                'fk_lge005_num_documento'=>$idRegistro2,
                'fk_lgc009_num_orden_detalle'=>$datos['idDetalleOrden'][$contador],
                'num_cantidad'=>$datos['cantRecibida'][$contador],
                'num_precio_unit'=>$datos['precio'][$contador],
                'num_precio_cantidad'=>$total,
                'num_total'=>$total
            ));
        }


        $fallaTansaccion = $registroConPer->errorInfo();
        $fallaTansaccion2 = $registroConPerDet->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarConPer(
    $datos
    )
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
                      UPDATE
                        lg_b020_control_perceptivo
                      SET
                        fec_anio=:fec_anio,
                        num_estatus=:num_estatus,
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW(),
                        fk_lgb019_num_orden=:fk_lgb019_num_orden,
                        fk_rhb001_num_empleado_conforme_a=:fk_rhb001_num_empleado_conforme_a,
                        fk_a006_num_miscelaneo_tipo_firmante_a=:fk_a006_num_miscelaneo_tipo_firmante_a,
                        fk_rhb001_num_empleado_conforme_b=:fk_rhb001_num_empleado_conforme_b,
                        fk_a006_num_miscelaneo_tipo_firmante_b=:fk_a006_num_miscelaneo_tipo_firmante_b,
                        fk_rhb001_num_empleado_conforme_c=:fk_rhb001_num_empleado_conforme_c,
                        fk_a006_num_miscelaneo_tipo_firmante_c=:fk_a006_num_miscelaneo_tipo_firmante_c,
                        fk_rhb001_num_empleado_conforme_d=:fk_rhb001_num_empleado_conforme_d,
                        fk_a006_num_miscelaneo_tipo_firmante_d=:fk_a006_num_miscelaneo_tipo_firmante_d,
                        fk_rhb001_num_empleado_conforme_e=:fk_rhb001_num_empleado_conforme_e,
                        fk_a006_num_miscelaneo_tipo_firmante_e=:fk_a006_num_miscelaneo_tipo_firmante_e
                      WHERE
                        pk_num_control_perceptivo =:pk_num_control_perceptivo
            ");
        $modificarRegistro->execute(array(
            'fec_anio'=>date('Y'),
            'fk_lgb019_num_orden'=>$datos['idOrden'],
            'num_estatus'=>$datos['cerrar'],
            'fk_rhb001_num_empleado_conforme_a'=>$datos['asisA'],
            'fk_a006_num_miscelaneo_tipo_firmante_a'=>$datos['tipoFirmanteA'],
            'fk_rhb001_num_empleado_conforme_b'=>$datos['asisB'],
            'fk_a006_num_miscelaneo_tipo_firmante_b'=>$datos['tipoFirmanteB'],
            'fk_rhb001_num_empleado_conforme_c'=>$datos['asisC'],
            'fk_a006_num_miscelaneo_tipo_firmante_c'=>$datos['tipoFirmanteC'],
            'fk_rhb001_num_empleado_conforme_d'=>$datos['asisD'],
            'fk_a006_num_miscelaneo_tipo_firmante_d'=>$datos['tipoFirmanteD'],
            'fk_rhb001_num_empleado_conforme_e'=>$datos['asisE'],
            'fk_a006_num_miscelaneo_tipo_firmante_e'=>$datos['tipoFirmanteE'],
            'pk_num_control_perceptivo'=>$datos['idConPer']
        ));

        $registroConPerDet = $this->_db->prepare("
          UPDATE
            lg_c010_control_perceptivo_detalle
          SET
              ind_observacion=:ind_observacion,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              num_flag_recibido=:num_flag_recibido,
              num_cantidad=:num_cantidad,
              fk_lgb020_num_control_perceptivo=:fk_lgb020_num_control_perceptivo
            WHERE
              pk_num_control_perceptivo_detalle=:pk_num_control_perceptivo_detalle
           ");

        $NuevoPostDetalle=$this->_db->prepare(
            "UPDATE
                lg_c009_orden_detalle
              SET
                ind_estado=:ind_estado,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
				WHERE pk_num_orden_detalle=:pk_num_orden_detalle
              ");


        $actDocumentoDet=$this->_db->prepare("
            DELETE FROM
              lg_e006_documento_detalle
            WHERE
              fk_lge005_num_documento=:fk_lge005_num_documento
              ");
        $actDocumentoDet->execute(array(
            'fk_lge005_num_documento'=>$datos['idDoc']
        ));

        $actDocumento=$this->_db->prepare("
            UPDATE
              lg_e005_documento
            SET
              num_monto_afecto=:num_monto_afecto,
              num_monto_no_afecto=:num_monto_no_afecto,
              num_monto_impuesto=:num_monto_impuesto,
              num_monto_total=:num_monto_total,
              num_monto_pendiente=:num_monto_pendiente
            WHERE
              pk_num_documento=:pk_num_documento
              ");

        $actDocumento->execute(array(
            'num_monto_afecto'=>$datos['datosOrden']['num_monto_afecto'],
            'num_monto_no_afecto'=>$datos['datosOrden']['num_monto_no_afecto'],
            'num_monto_impuesto'=>$datos['datosOrden']['num_monto_igv'],
            'num_monto_total'=>$datos['datosOrden']['num_monto_total'],
            'num_monto_pendiente'=>$datos['datosOrden']['num_monto_pendiente'],
            'pk_num_documento'=>$datos['idDoc']
        ));

        $NuevoPostDetalle3=$this->_db->prepare(
            "INSERT INTO
                lg_e006_documento_detalle
              SET
                num_secuencia=:num_secuencia, 
                fk_lge005_num_documento=:fk_lge005_num_documento, 
                fk_lgc009_num_orden_detalle=:fk_lgc009_num_orden_detalle, 
                num_cantidad=:num_cantidad, 
                num_precio_unit=:num_precio_unit,
                num_precio_cantidad=:num_precio_cantidad,
                num_total=:num_total,
                fec_ultima_modificacion=NOW(), 
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");

        $contador=0;
        while($contador<$datos['cantidad']){
            $contador=$contador+1;
/*
            $registroConPerDet->execute(array(
                'ind_observacion'=>$datos['comentario'][$contador],
                'fk_lgb020_num_control_perceptivo'=>$datos['idConPer'],
                'num_flag_recibido'=>$datos['recibido'][$contador],
                'num_cantidad'=>$datos['cantRecibida'][$contador],
                'pk_num_control_perceptivo_detalle'=>$datos['idDetalle'][$contador]
            ));

            $precioCantidad = $datos['datosOrdenDetalle'][$contador-1]['num_precio_unitario']*$datos['cantRecibida'][$contador];

            $NuevoPostDetalle3->execute(array(
                'num_secuencia'=>$contador,
                'fk_lge005_num_documento'=>$datos['idDoc'],
                'fk_lgc009_num_orden_detalle'=>$datos['idDetalle'][$contador],
                'num_cantidad'=>$datos['cantRecibida'][$contador],
                'num_precio_unit'=>$datos['datosOrdenDetalle'][$contador-1]['num_precio_unitario'],
                'num_precio_cantidad'=>$precioCantidad,
                'num_total'=>$precioCantidad
            ));


            $NuevoPostDetalle->execute(array(
                'ind_estado'=>'CE',
                'pk_num_orden_detalle'=>$datos['idDetalleOrden'][$contador]
            ));
            */
        }

        $error = $modificarRegistro->errorInfo();
//        $error2 = $registroConPerDet->errorInfo();
//        $error3 = $NuevoPostDetalle->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
            /*
        }elseif(!empty($error2[1]) && !empty($error2[2])){
            $this->_db->rollBack();
            return $error2;
        }elseif(!empty($error3[1]) && !empty($error3[2])){
            $this->_db->rollBack();
            return $error3;
            */
        }else{
            $this->_db->commit();
            return $datos['idConPer'];
        }
    }

    public function metMostrarOrdenControlPer($idControl){
        $mostrarConPer = $this->_db->query("
              SELECT
                perceptivo.*,
                perceptivo.fec_anio AS anio,
                orden.*,
                CONCAT_WS('-',acta.num_anio,acta.num_procedimiento) AS proc,
                CONCAT_WS(' ',persona.ind_nombre1,persona.ind_apellido1) AS proveedor,
                acta.ind_codigo_acta,
                acta.ind_codigo_procedimiento
              FROM
                lg_b020_control_perceptivo AS perceptivo
                INNER JOIN lg_b019_orden AS orden ON orden.pk_num_orden = perceptivo.fk_lgb019_num_orden
                INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                LEFT JOIN lg_b013_adjudicacion AS adjudicacion ON adjudicacion.pk_num_adjudicacion = orden.fk_lgb013_num_adjudicacion
                LEFT JOIN lg_b012_informe_recomendacion AS recomendacion ON adjudicacion.fk_lgb012_num_informe_recomendacion = recomendacion.pk_num_informe_recomendacion
                LEFT JOIN lg_b011_evaluacion AS evaluacion ON recomendacion.fk_lgb011_num_evaluacion = evaluacion.pk_num_evaluacion
                LEFT JOIN lg_b009_acta_inicio AS acta ON evaluacion.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
                WHERE perceptivo.pk_num_control_perceptivo = '$idControl'
        ");
        $mostrarConPer->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarConPer->fetch();
    }

    public function metBuscarClasificacionCxP($codigo)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
              cp_b001_documento_clasificacion
            WHERE
              ind_documento_clasificacion='$codigo'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metBuscarDocumento($idOrden)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
              lg_e005_documento
            WHERE
              fk_lgb019_num_orden='$idOrden'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }


    public function metActualizarOrden($idOrden)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $NuevoPost=$this->_db->prepare(
            "UPDATE lg_b019_orden
              SET
              num_flag_control_perceptivo=:num_flag_control_perceptivo,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE pk_num_orden = $idOrden
              ");
        $NuevoPost->execute(array(
            'num_flag_control_perceptivo'=>0
        ));

        $fallaTansaccion = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idOrden;
        }
    }


}

?>