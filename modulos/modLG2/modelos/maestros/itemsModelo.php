<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'subFamiliasModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'unidadesConversionModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'unidadesModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'almacenModelo.php';
class itemsModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atSubFamiliasModelo = new subFamiliasModelo();
        $this->atUnidadesConversionModelo = new unidadesConversionModelo();
        $this->atUnidadesModelo = new unidadesModelo();
        $this->atAlmacenModelo = new almacenModelo();
    }

    public function metListarItems($id=false,$filtro=false)
    {
        $where = 'ORDER BY i.pk_num_item';
        if($filtro) {
            $id=false;
            $where = $filtro;
        } elseif($id) {
            $where = "WHERE i.pk_num_item = '".$id."' ORDER BY i.pk_num_item";
        }
        $itemPost = $this->_db->query(
            "SELECT
              i.*,
              i.ind_descripcion AS descripcion,
              unidad.ind_descripcion AS uni,
              unidad.ind_descripcion AS unidad,
              unidad.pk_num_unidad AS idUnidad,
              
              cuenta.pk_num_cuenta,
              cuenta.cod_cuenta,
              cuenta.ind_descripcion AS descripcionCuenta,
              
              presupuestaria.pk_num_partida_presupuestaria,
              presupuestaria.cod_partida,
              presupuestaria.ind_denominacion AS descripcionPartida,
              stock.num_stock_actual,
              stock.num_stock_inicial,
              detalle.pk_num_miscelaneo_detalle,
              detalle.ind_nombre_detalle,
              detalle.cod_detalle,
              detalle2.ind_nombre_detalle AS tipo,
              familia.pk_num_clase_familia,
              subfamilia.pk_num_subfamilia,
              almacen.ind_descripcion AS almacen
              FROM lg_b002_item AS i
              INNER JOIN lg_c002_unidades_conversion AS conversion ON conversion.pk_num_unidad_conversion = i.fk_lgc002_num_unidad_conversion
              INNER JOIN lg_b004_unidades AS unidad ON conversion.fk_lgb004_num_unidad = unidad.pk_num_unidad
              LEFT JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = i.fk_cbb004_num_plan_cuenta_gasto_oncop
              LEFT JOIN cb_b004_plan_cuenta AS cuenta2 ON cuenta2.pk_num_cuenta = i.fk_cbb004_num_plan_cuenta_gasto_pub_veinte
              LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria ON presupuestaria.pk_num_partida_presupuestaria = i.fk_prb002_num_partida_presupuestaria
              INNER JOIN lg_c007_item_stock AS stock ON i.fk_lgc007_num_item_stock = stock.pk_num_item_stock
              INNER JOIN lg_b007_clase_subfamilia AS subfamilia ON subfamilia.pk_num_subfamilia = i.fk_lgb007_num_clase_subfamilia
              INNER JOIN lg_b006_clase_familia AS familia ON familia.pk_num_clase_familia = subfamilia.fk_lgb006_num_clase_familia
              LEFT JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = familia.fk_a006_num_miscelaneo_clase_linea
              LEFT JOIN a006_miscelaneo_detalle AS detalle2 ON detalle2.pk_num_miscelaneo_detalle = i.fk_a006_num_miscelaneo_tipo_item
              INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = i.fk_lgb014_num_almacen
              $where
          ");
        $itemPost->setFetchMode(PDO::FETCH_ASSOC);
        if($id){
            return $itemPost->fetch();
        } else {
            return $itemPost->fetchAll();
        }
    }

    public function metMostrarItem($idItem){
        $mostrarItem = $this->_db->query("
        SELECT
            i.*,
            concat_ws(' ',persona.ind_nombre1,ind_apellido1) AS usuario,
            concat_ws(' ',presupuestaria.cod_partida,presupuestaria.ind_denominacion) AS nombrePartida,
            presupuestaria.pk_num_partida_presupuestaria AS idPartida,
            presupuestaria.cod_partida AS codPartida,
            presupuestaria.ind_denominacion AS descPartida,
            concat_ws(' ',cuenta1.cod_cuenta,cuenta1.ind_descripcion) AS nombreCuenta1,
            concat_ws(' ',cuenta2.cod_cuenta,cuenta2.ind_descripcion) AS nombreCuenta2,
            concat_ws(' ',cuenta3.cod_cuenta,cuenta3.ind_descripcion) AS nombreCuenta3,
            concat_ws(' ',cuenta4.cod_cuenta,cuenta4.ind_descripcion) AS nombreCuenta4,
            cuenta4.pk_num_cuenta AS idCuenta2,
            cuenta3.pk_num_cuenta AS idCuenta,
            cuenta4.cod_cuenta AS codCuenta2,
            cuenta3.cod_cuenta AS codCuenta,
            cuenta4.ind_descripcion AS descCuenta2,
            cuenta3.ind_descripcion AS descCuenta,
            stock.num_stock_actual,
            unidad.pk_num_unidad AS idUnidad,
            unidad.ind_descripcion AS unidad,
            unidad2.pk_num_unidad AS idUnidad2,
            unidad2.ind_descripcion AS unidad2
        FROM
            lg_b002_item AS i
            INNER JOIN lg_c002_unidades_conversion AS conversion ON i.fk_lgc002_num_unidad_conversion = conversion.pk_num_unidad_conversion
            INNER JOIN lg_b004_unidades AS unidad ON conversion.fk_lgb004_num_unidad = unidad.pk_num_unidad 
            INNER JOIN lg_b004_unidades AS unidad2 ON unidad2.pk_num_unidad = i.fk_lgb004_num_unidad_compra
            INNER JOIN a018_seguridad_usuario AS usuario ON usuario.pk_num_seguridad_usuario = i.fk_a018_num_seguridad_usuario
            INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = usuario.fk_rhb001_num_empleado
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
            INNER JOIN lg_c007_item_stock AS stock ON stock.pk_num_item_stock = i.fk_lgc007_num_item_stock
            LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria ON presupuestaria.pk_num_partida_presupuestaria = i.fk_prb002_num_partida_presupuestaria
            LEFT JOIN cb_b004_plan_cuenta AS cuenta1 ON cuenta1.pk_num_cuenta = i.fk_cbb004_num_plan_cuenta_inventario_oncop
            LEFT JOIN cb_b004_plan_cuenta AS cuenta2 ON cuenta2.pk_num_cuenta = i.fk_cbb004_num_plan_cuenta_inventario_pub_veinte
            LEFT JOIN cb_b004_plan_cuenta AS cuenta3 ON cuenta3.pk_num_cuenta = i.fk_cbb004_num_plan_cuenta_gasto_oncop
            LEFT JOIN cb_b004_plan_cuenta AS cuenta4 ON cuenta4.pk_num_cuenta = i.fk_cbb004_num_plan_cuenta_gasto_pub_veinte
        WHERE
            pk_num_item='$idItem'
        ");
        $mostrarItem->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarItem->fetch();
    }

    public function metCrearItems(
        $descripcion,$codigo,$tipoItem,$estado,$lotes,$kit,
        $impuesto,$auto,$disponible,$presupuesto,
        $partida,$inventarioO,$gastoO,$inventario2,$gasto2,
        $min,$max,$reorden,$subf,$conversion,
        $compra,$despacho,$almacen
    )
    {
        $this->_db->beginTransaction();
        $registroItem = $this->_db->prepare("
          INSERT INTO
          lg_c007_item_stock
          SET
          num_stock_inicial=:num_stock_inicial,
          num_stock_actual=:num_stock_actual,
          num_stock_comprometido=:num_stock_comprometido,
          num_precio_unitario=:num_precio_unitario,
          fec_mes=:fec_mes,
          fec_anio=:fec_anio,
          fk_a018_num_seguridad_usuario='$this->atIdUsuario',
          fec_ultima_modificacion=NOW(),
          fk_lgc002_num_unidad_conversion=:fk_lgc002_num_unidad_conversion,
          fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
          fk_rhb001_num_empleado_ingresadopor='$this->atIdEmpleado'
          ");

        $registroItem->execute(array(
            'num_stock_inicial'=>'0.000000',
            'num_stock_actual'=>'0.000000',
            'num_stock_comprometido'=>'0.000000',
            'num_precio_unitario'=>'0.000000',
            'fec_mes'=>date('m'),
            'fec_anio'=>date('Y'),
            'fk_lgc002_num_unidad_conversion'=>$conversion,
            'fk_lgb014_num_almacen'=>$almacen
        ));

        $stock= $this->_db->lastInsertId();
        $registroItem = $this->_db->prepare("
            INSERT INTO
            lg_b002_item
            SET
            ind_descripcion=:ind_descripcion,
            ind_codigo_interno=:ind_codigo_interno,
            num_flag_lotes=:num_flag_lotes,
            num_flag_kit=:num_flag_kit,
            num_flag_impuesto_venta=:num_flag_impuesto_venta,
            num_flag_auto=:num_flag_auto,
            num_flag_disponible=:num_flag_disponible,
            num_flag_verificado_presupuesto=:num_flag_verificado_presupuesto,
            ind_imagen=:ind_imagen,
            num_stock_minimo=:num_stock_minimo,
            num_stock_maximo=:num_stock_maximo,
            num_punto_reorden=:num_punto_reorden,
            fk_cbb004_num_plan_cuenta_inventario_oncop=:fk_cbb004_num_plan_cuenta_inventario_oncop,
            fk_cbb004_num_plan_cuenta_gasto_oncop=:fk_cbb004_num_plan_cuenta_gasto_oncop,
            fk_cbb004_num_plan_cuenta_inventario_pub_veinte=:fk_cbb004_num_plan_cuenta_inventario_pub_veinte,
            fk_cbb004_num_plan_cuenta_gasto_pub_veinte=:fk_cbb004_num_plan_cuenta_gasto_pub_veinte,
            fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fk_lgb007_num_clase_subfamilia=:fk_lgb007_num_clase_subfamilia,
            fk_lgc002_num_unidad_conversion=:fk_lgc002_num_unidad_conversion,
            fk_lgc007_num_item_stock=:fk_lgc007_num_item_stock,
            fk_lgb004_num_unidad_compra=:fk_lgb004_num_unidad_compra,
            fk_lgb004_num_unidad_despacho=:fk_lgb004_num_unidad_despacho,
            fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
            fk_a006_num_miscelaneo_tipo_item=:fk_a006_num_miscelaneo_tipo_item
          ");

        $registroItem->execute(array(
            'ind_descripcion'=>$descripcion,
            'ind_codigo_interno'=>$codigo,
            'num_flag_lotes'=>$lotes,
            'num_flag_kit'=>$kit,
            'num_flag_impuesto_venta'=>$impuesto,
            'num_flag_auto'=>$auto,
            'num_flag_disponible'=>$disponible,
            'num_flag_verificado_presupuesto'=>$presupuesto,
            'ind_imagen'=>'ninguna',
            'num_stock_minimo'=>$min,
            'num_stock_maximo'=>$max,
            'num_punto_reorden'=>$reorden,
            'fk_cbb004_num_plan_cuenta_inventario_oncop'=>$inventarioO,
            'fk_cbb004_num_plan_cuenta_gasto_oncop'=>$gastoO,
            'fk_cbb004_num_plan_cuenta_inventario_pub_veinte'=>$inventario2,
            'fk_cbb004_num_plan_cuenta_gasto_pub_veinte'=>$gasto2,
            'fk_prb002_num_partida_presupuestaria'=>$partida,
            'num_estatus'=>$estado,
            'fk_lgb007_num_clase_subfamilia'=>$subf,
            'fk_lgc002_num_unidad_conversion'=>$conversion,
            'fk_lgc007_num_item_stock'=>$stock,
            'fk_lgb004_num_unidad_compra'=>$compra,
            'fk_lgb004_num_unidad_despacho'=>$despacho,
            'fk_lgb014_num_almacen'=>$almacen,
            'fk_a006_num_miscelaneo_tipo_item'=>$tipoItem
        ));



        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroItem->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarItems(
        $descripcion,$codigo,$tipoItem,$estado,$lotes,$kit,
        $impuesto,$auto,$disponible,$presupuesto,
        $partida,$inventarioO,$gastoO,$inventario2,$gasto2,
        $min,$max,$reorden,$subf,$conversion,
        $compra,$despacho,$almacen,$idItem)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
            UPDATE
              lg_b002_item
            SET
            ind_descripcion=:ind_descripcion,
            ind_codigo_interno=:ind_codigo_interno,
            num_flag_lotes=:num_flag_lotes,
            num_flag_kit=:num_flag_kit,
            num_flag_impuesto_venta=:num_flag_impuesto_venta,
            num_flag_auto=:num_flag_auto,
            num_flag_disponible=:num_flag_disponible,
            num_flag_verificado_presupuesto=:num_flag_verificado_presupuesto,
            ind_imagen=:ind_imagen,
            num_stock_minimo=:num_stock_minimo,
            num_stock_maximo=:num_stock_maximo,
            num_punto_reorden=:num_punto_reorden,
            fk_cbb004_num_plan_cuenta_inventario_oncop=:fk_cbb004_num_plan_cuenta_inventario_oncop,
            fk_cbb004_num_plan_cuenta_gasto_oncop=:fk_cbb004_num_plan_cuenta_gasto_oncop,
            fk_cbb004_num_plan_cuenta_inventario_pub_veinte=:fk_cbb004_num_plan_cuenta_inventario_pub_veinte,
            fk_cbb004_num_plan_cuenta_gasto_pub_veinte=:fk_cbb004_num_plan_cuenta_gasto_pub_veinte,
            fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fk_lgb007_num_clase_subfamilia=:fk_lgb007_num_clase_subfamilia,
            fk_lgc002_num_unidad_conversion=:fk_lgc002_num_unidad_conversion,
            fk_lgb004_num_unidad_compra=:fk_lgb004_num_unidad_compra,
            fk_lgb004_num_unidad_despacho=:fk_lgb004_num_unidad_despacho,
            fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
            fk_a006_num_miscelaneo_tipo_item=:fk_a006_num_miscelaneo_tipo_item
            WHERE
              pk_num_item='$idItem'
            ");
        $modificarRegistro->execute(array(
            'ind_descripcion'=>$descripcion,
            'ind_codigo_interno'=>$codigo,
            'num_flag_lotes'=>$lotes,
            'num_flag_kit'=>$kit,
            'num_flag_impuesto_venta'=>$impuesto,
            'num_flag_auto'=>$auto,
            'num_flag_disponible'=>$disponible,
            'num_flag_verificado_presupuesto'=>$presupuesto,
            'ind_imagen'=>'ninguna',
            'num_stock_minimo'=>$min,
            'num_stock_maximo'=>$max,
            'num_punto_reorden'=>$reorden,
            'fk_cbb004_num_plan_cuenta_inventario_oncop'=>$inventarioO,
            'fk_cbb004_num_plan_cuenta_gasto_oncop'=>$gastoO,
            'fk_cbb004_num_plan_cuenta_inventario_pub_veinte'=>$inventario2,
            'fk_cbb004_num_plan_cuenta_gasto_pub_veinte'=>$gasto2,
            'fk_prb002_num_partida_presupuestaria'=>$partida,
            'num_estatus'=>$estado,
            'fk_lgb007_num_clase_subfamilia'=>$subf,
            'fk_lgc002_num_unidad_conversion'=>$conversion,
            'fk_lgb004_num_unidad_compra'=>$compra,
            'fk_lgb004_num_unidad_despacho'=>$despacho,
            'fk_lgb014_num_almacen'=>$almacen,
            'fk_a006_num_miscelaneo_tipo_item'=>$tipoItem
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idItem;
        }
    }

    public function metEliminarItems($idItem){
        $this->_db->beginTransaction();
        $eliminarItem=$this->_db->prepare("
            DELETE FROM
            lg_c007_item_stock
            WHERE
            pk_num_item_stock=:pk_num_item_stock
            ");
        $eliminarItem->execute(array(
            'pk_num_item_stock'=>$idItem
        ));

        $eliminarItem2=$this->_db->prepare("
            DELETE FROM
            lg_b002_item
            WHERE
            pk_num_item=:pk_num_item
            ");
        $eliminarItem2->execute(array(
            'pk_num_item'=>$idItem
        ));

        $error = $eliminarItem2->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idItem;
        }
    }

    public function metListarPlanCuenta($tipo)
    {
        $personaPost = $this->_db->query("
            SELECT
              *
            FROM
              cb_b004_plan_cuenta
              WHERE num_flag_tipo = '$tipo'
              ");
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        return $personaPost ->fetchAll();
    }


}

?>