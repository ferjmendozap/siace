<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'commodityModelo.php';
class familiasModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atCommodityModelo = new commodityModelo();
        $this->atMiscelaneoModelo = new miscelaneoModelo();
    }

    public function metListarFamilias()
    {
        $familiasPost = $this->_db->query(
            "SELECT
              *
              FROM lg_b006_clase_familia
          ");

        $familiasPost->setFetchMode(PDO::FETCH_ASSOC);

        return $familiasPost->fetchAll();
    }

    public function metMostrarFamilias($idFamilias){
        $mostrarFamilias = $this->_db->query("
          SELECT
            f.*,
            s.ind_usuario
          FROM
            lg_b006_clase_familia AS f
            INNER JOIN a018_seguridad_usuario AS s ON f.fk_a018_num_seguridad_usuario = s.pk_num_seguridad_usuario
          WHERE
            pk_num_clase_familia='$idFamilias'
        ");
        $mostrarFamilias->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarFamilias->fetch();
    }

    public function metCrearFamilias($desc,$codigo,$inventario,$gasto,$partida,$estado,$linea)
    {
        $this->_db->beginTransaction();
        $registroFamilias = $this->_db->prepare("
          INSERT INTO
          lg_b006_clase_familia
          SET
            ind_descripcion=:ind_descripcion,
            ind_cod_familia=:ind_cod_familia,
            fk_cbb004_cuenta_inventario=:fk_cbb004_cuenta_inventario,
            fk_cbb004_cuenta_gasto=:fk_cbb004_cuenta_gasto,
            fk_prb002_partida_presupuestaria=:fk_prb002_partida_presupuestaria,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario=$this->atIdUsuario,
            fec_ultima_modificacion=NOW(),
            fk_a006_num_miscelaneo_clase_linea=:fk_a006_num_miscelaneo_clase_linea
          ");

        $registroFamilias->execute(array(
            'ind_descripcion'=>$desc,
            'ind_cod_familia'=>$codigo,
            'fk_cbb004_cuenta_inventario'=>$inventario,
            'fk_cbb004_cuenta_gasto'=>$gasto,
            'fk_prb002_partida_presupuestaria'=>$partida,
            'num_estatus'=>$estado,
            'fk_a006_num_miscelaneo_clase_linea'=>$linea
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroFamilias->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarFamilias($desc,$codigo,$inventario,$gasto,$partida,$estado,$linea,$idFamilias)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
                UPDATE
                  lg_b006_clase_familia
                SET
                  ind_descripcion=:ind_descripcion,
                  ind_cod_familia=:ind_cod_familia,
                  fk_cbb004_cuenta_inventario=:fk_cbb004_cuenta_inventario,
                  fk_cbb004_cuenta_gasto=:fk_cbb004_cuenta_gasto,
                  fk_prb002_partida_presupuestaria=:fk_prb002_partida_presupuestaria,
                  num_estatus=:num_estatus,
                  fk_a018_num_seguridad_usuario=$this->atIdUsuario,
                  fec_ultima_modificacion=NOW(),
                  fk_a006_num_miscelaneo_clase_linea=:fk_a006_num_miscelaneo_clase_linea
                WHERE
                  pk_num_clase_familia='$idFamilias'
            ");
        $modificarRegistro->execute(array(
            'ind_descripcion'=>$desc,
            'ind_cod_familia'=>$codigo,
            'fk_cbb004_cuenta_inventario'=>$inventario,
            'fk_cbb004_cuenta_gasto'=>$gasto,
            'fk_prb002_partida_presupuestaria'=>$partida,
//            'fk_prb002_partida_presupuestaria'=>null,
            'num_estatus'=>$estado,
            'fk_a006_num_miscelaneo_clase_linea'=>$linea
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idFamilias;
        }
    }

    public function metEliminarFamilias($idFamilias){
        $this->_db->beginTransaction();
        $eliminarFamilias=$this->_db->prepare("
            DELETE FROM
            lg_b006_clase_familia
            WHERE
            pk_num_clase_familia=:pk_num_clase_familia
            ");
        $eliminarFamilias->execute(array(
            'pk_num_clase_familia'=>$idFamilias
        ));

        $error = $eliminarFamilias->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idFamilias;
        }
    }


}

?>