<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
class aspectosCualitativosModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
    }

    public function metMostrarAspecto($idAspecto){
        $mostrarAspecto = $this->_db->query("
            SELECT
                cualitativo.*,
                usuario.ind_usuario
            FROM
                lg_e007_aspecto_cualitativo AS cualitativo 
                INNER JOIN a018_seguridad_usuario AS usuario ON cualitativo.fk_a018_num_seguridad_usuario = usuario.pk_num_seguridad_usuario
            WHERE pk_num_aspecto = '$idAspecto'
        ");
        $mostrarAspecto->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarAspecto->fetch();
    }

    public function metListarAspecto(){
        $mostrarAspecto = $this->_db->query("
            SELECT
                *
            FROM
                lg_e007_aspecto_cualitativo AS cualitativo 
                INNER JOIN a006_miscelaneo_detalle AS detalle ON cualitativo.fk_a006_num_miscelaneo_detalle = detalle.pk_num_miscelaneo_detalle
            WHERE cualitativo.num_estatus = 1
        ");
        $mostrarAspecto->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarAspecto->fetchAll();
    }

    public function metMostrarParametro($codigo){
        $mostrarAspecto = $this->_db->query("
            SELECT
                *
            FROM
                a035_parametros 
            WHERE ind_parametro_clave = '$codigo'
        ");
        $mostrarAspecto->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarAspecto->fetch();
    }

    public function metContarTotalPuntaje($id,$cual){
        $mostrarAspecto = $this->_db->query("
            SELECT
                SUM(cualitativo.num_puntaje_maximo) AS maximo
            FROM
                lg_e007_aspecto_cualitativo AS cualitativo 
                INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = cualitativo.fk_a006_num_miscelaneo_detalle
            WHERE cualitativo.pk_num_aspecto != '$id'
            AND detalle.cod_detalle = '$cual'
        ");
        $mostrarAspecto->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarAspecto->fetch();
    }

    public function metCrearAspecto($datos)
    {
        $this->_db->beginTransaction();
        $registroAspecto = $this->_db->prepare("
          INSERT INTO
              lg_e007_aspecto_cualitativo
          SET
              ind_nombre=:ind_nombre,
              ind_cod_aspecto=:ind_cod_aspecto,
              num_puntaje_maximo=:num_puntaje_maximo,
              num_estatus=:num_estatus,
              fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
          ");

        $registroAspecto->execute(array(
            'ind_nombre'=>$datos['ind_nombre'],
            'ind_cod_aspecto'=>$datos['ind_cod_aspecto'],
            'num_puntaje_maximo'=>$datos['num_puntaje_maximo'],
            'num_estatus'=>$datos['num_estatus'],
            'fk_a006_num_miscelaneo_detalle'=>$datos['fk_a006_num_miscelaneo_detalle']
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroAspecto->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarAspecto($datos)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
              UPDATE
                  lg_e007_aspecto_cualitativo
              SET
                  ind_nombre=:ind_nombre,
                  ind_cod_aspecto=:ind_cod_aspecto,
                  num_puntaje_maximo=:num_puntaje_maximo,
                  num_estatus=:num_estatus,
                  fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle,
                  fec_ultima_modificacion=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE
                  pk_num_aspecto=:pk_num_aspecto
            ");
        $modificarRegistro->execute(array(
            'ind_nombre'=>$datos['ind_nombre'],
            'ind_cod_aspecto'=>$datos['ind_cod_aspecto'],
            'num_puntaje_maximo'=>$datos['num_puntaje_maximo'],
            'num_estatus'=>$datos['num_estatus'],
            'pk_num_aspecto'=>$datos['idAspecto'],
            'fk_a006_num_miscelaneo_detalle'=>$datos['fk_a006_num_miscelaneo_detalle']
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $datos['idAspecto'];
        }
    }

    public function metEliminarAspecto($idAspecto){
        $this->_db->beginTransaction();
        $eliminarAspecto=$this->_db->prepare("
            DELETE FROM
            lg_e007_aspecto_cualitativo
            WHERE
            pk_num_aspecto=:pk_num_aspecto
            ");
        $eliminarAspecto->execute(array(
            'pk_num_aspecto'=>$idAspecto
        ));

        $error = $eliminarAspecto->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idAspecto;
        }
    }

}

?>