<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'tipoDocumentoModelo.php';
class tipoTransaccionModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atTipoDocumentoModelo = new tipoDocumentoModelo();
    }

    public function metListarTipoTran()
    {
        $tipotranPost = $this->_db->query("
            SELECT
                *
            FROM
                lg_b015_tipo_transaccion
          ");
        $tipotranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tipotranPost->fetchAll();
    }

    public function metListarTipoMov()
    {
        $tipotranPost = $this->_db->query("
            SELECT
                *
            FROM
                a006_miscelaneo_detalle AS detalle 
                INNER JOIN a005_miscelaneo_maestro AS maestro ON detalle.fk_a005_num_miscelaneo_maestro = maestro.pk_num_miscelaneo_maestro
                WHERE maestro.cod_maestro = 'TIPOTRANS'
          ");
        $tipotranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tipotranPost->fetchAll();
    }

    public function metListarTipoDoc()
    {
        $tipotranPost = $this->_db->query("
            SELECT
                pk_num_tipo_documento,
                ind_descripcion,
                num_estatus
            FROM
                cp_b002_tipo_documento
            WHERE
                num_estatus='1'
          ");
        $tipotranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tipotranPost->fetchAll();
    }

    public function metMostrarTipoTran($idTipoTran){
        $mostrarTipoTran = $this->_db->query("
            SELECT
                t.*,
                s.ind_usuario
            FROM
                lg_b015_tipo_transaccion AS t
                INNER JOIN a018_seguridad_usuario AS s ON s.pk_num_seguridad_usuario = t.fk_a018_num_seguridad_usuario
                and t.pk_num_tipo_transaccion = '$idTipoTran'
        ");
        $mostrarTipoTran->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTipoTran->fetch();
    }

    public function metCrearTipoTran($descTipoTran,$tipoMovi,$tipoDocGen,$tipoDocTran,$vCTipoTran,$vATipoTran,$estadoTipoTran,$codigo)
    {
        $this->_db->beginTransaction();
        $registroTipoTran = $this->_db->prepare("
          INSERT INTO
          lg_b015_tipo_transaccion
          SET
          ind_descripcion=:ind_descripcion,
          ind_cod_tipo_transaccion=:ind_cod_tipo_transaccion,
          fk_a006_num_miscelaneo_detalle_tipo_documento=:fk_a006_num_miscelaneo_detalle_tipo_documento,
          fk_lgb016_num_tipo_documento_generado=:fk_lgb016_num_tipo_documento_generado,
          fk_lgb016_num_tipo_documento_transaccion=:fk_lgb016_num_tipo_documento_transaccion,
          num_flag_voucher_consumo=:num_flag_voucher_consumo,
          num_flag_voucher_ajuste=:num_flag_voucher_ajuste,
          num_estatus=:num_estatus,
          fk_a018_num_seguridad_usuario='$this->atIdUsuario',
          fec_ultima_modificacion=NOW()
          ");

        $registroTipoTran->execute(array(
            'ind_descripcion'=>$descTipoTran,
            'ind_cod_tipo_transaccion'=>$codigo,
            'fk_a006_num_miscelaneo_detalle_tipo_documento'=>$tipoMovi,
            'fk_lgb016_num_tipo_documento_generado'=>$tipoDocGen,
            'fk_lgb016_num_tipo_documento_transaccion'=>$tipoDocTran,
            'num_flag_voucher_consumo'=>$vCTipoTran,
            'num_flag_voucher_ajuste'=>$vATipoTran,
            'num_estatus'=>$estadoTipoTran
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroTipoTran->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarTipoTran($descTipoTran,$tipoMovi,$tipoDocGen,$tipoDocTran,$vCTipoTran,$vATipoTran,$estadoTipoTran,$codigo,$idTipoTran)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
              UPDATE
                lg_b015_tipo_transaccion
              SET
                ind_descripcion=:ind_descripcion,
                ind_cod_tipo_transaccion=:ind_cod_tipo_transaccion,
                fk_a006_num_miscelaneo_detalle_tipo_documento=:fk_a006_num_miscelaneo_detalle_tipo_documento,
                fk_lgb016_num_tipo_documento_generado=:fk_lgb016_num_tipo_documento_generado,
                fk_lgb016_num_tipo_documento_transaccion=:fk_lgb016_num_tipo_documento_transaccion,
                num_flag_voucher_consumo=:num_flag_voucher_consumo,
                num_flag_voucher_ajuste=:num_flag_voucher_ajuste,
                num_estatus=:num_estatus,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
              WHERE
                pk_num_tipo_transaccion='$idTipoTran'
            ");
        $modificarRegistro->execute(array(
            'ind_descripcion'=>$descTipoTran,
            'ind_cod_tipo_transaccion'=>$codigo,
            'fk_a006_num_miscelaneo_detalle_tipo_documento'=>$tipoMovi,
            'fk_lgb016_num_tipo_documento_generado'=>$tipoDocGen,
            'fk_lgb016_num_tipo_documento_transaccion'=>$tipoDocTran,
            'num_flag_voucher_consumo'=>$vCTipoTran,
            'num_flag_voucher_ajuste'=>$vATipoTran,
            'num_estatus'=>$estadoTipoTran
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idTipoTran;
        }
    }

    public function metEliminarTipoTran($idTipoTran){
        $this->_db->beginTransaction();
        $eliminarTipoTran=$this->_db->prepare("
            DELETE FROM
            lg_b015_tipo_transaccion
            WHERE
            pk_num_tipo_transaccion=:pk_num_tipo_transaccion
            ");
        $eliminarTipoTran->execute(array(
            'pk_num_tipo_transaccion'=>$idTipoTran
        ));

        $error = $eliminarTipoTran->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idTipoTran;
        }
    }

}

?>