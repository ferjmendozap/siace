<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
class unidadesModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
    }

    public function metListarUnidades()
    {
        $unidadesPost = $this->_db->query("
            SELECT
                *
            FROM
                lg_b004_unidades
          ");
        $unidadesPost->setFetchMode(PDO::FETCH_ASSOC);
        return $unidadesPost->fetchAll();
    }

    public function metMostrarUnidades($idUnidades){
        $mostrarUnidades = $this->_db->query("
            SELECT
              u.*,
              s.ind_usuario
            FROM
              lg_b004_unidades AS u
              INNER JOIN a018_seguridad_usuario AS s
              ON s.pk_num_seguridad_usuario = u.fk_a018_num_seguridad_usuario
              and u.pk_num_unidad = '$idUnidades'
        ");
        $mostrarUnidades->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarUnidades->fetch();
    }

    public function metMostrarUnidadesC($idUnidades){
        $mostrarUnidades = $this->_db->query("
            SELECT
              uc.*,
              uc.ind_cod_unidad_conversion,
              uc.num_cantidad
            FROM
              lg_c002_unidades_conversion AS uc
            WHERE uc.fk_lgb004_num_unidad = '$idUnidades'
        ");
        $mostrarUnidades->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarUnidades->fetchAll();
    }

    public function metCrearUnidades($datos)
    {
        $this->_db->beginTransaction();
        $registroUnidades = $this->_db->prepare("
          INSERT INTO
            lg_b004_unidades
          SET
            ind_cod_unidad=:ind_cod_unidad,
            ind_descripcion=:ind_descripcion,
            fk_a006_num_miscelaneos_tipo_medida=:fk_a006_num_miscelaneos_tipo_medida,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          ");

        $registroUnidades->execute(array(
            'ind_cod_unidad'=>$datos['codigo'],
            'ind_descripcion'=>$datos['descUnidades'],
            'fk_a006_num_miscelaneos_tipo_medida'=>$datos['tipoMedi'],
            'num_estatus'=>$datos['estadoUnidades']
        ));

        $idRegistro= $this->_db->lastInsertId();

        $eliminarConversion = $this->_db->query("DELETE FROM lg_c002_unidades_conversion WHERE fk_lgb004_num_unidad = '$idRegistro'");
        $eliminarConversion = $this->_db->query("ALTER TABLE lg_c002_unidades_conversion auto_increment = 1;");


        $registroUnidadesConversion = $this->_db->prepare("
          INSERT INTO
            lg_c002_unidades_conversion
          SET
            num_cantidad=:num_cantidad,
            ind_cod_unidad_conversion=:ind_cod_unidad_conversion,
            fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          ");

        foreach ($datos['cantidadC'] as $key=>$value) {
            $registroUnidadesConversion->execute(array(
                'ind_cod_unidad_conversion'=>$datos['codigoC'][$key],
                'num_cantidad'=>$value,
                'fk_lgb004_num_unidad'=>$idRegistro,
                'num_estatus'=>$datos['estadoC'][$key]
            ));
        }

        $fallaTansaccion = $registroUnidades->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarUnidades($datos)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
          UPDATE
            lg_b004_unidades
          SET
            ind_descripcion=:ind_descripcion,
            fk_a006_num_miscelaneos_tipo_medida=:fk_a006_num_miscelaneos_tipo_medida,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          WHERE
            pk_num_unidad=:pk_num_unidad
            ");
        $modificarRegistro->execute(array(
            'ind_descripcion'=>$datos['descUnidades'],
            'fk_a006_num_miscelaneos_tipo_medida'=>$datos['tipoMedi'],
            'num_estatus'=>$datos['estadoUnidades'],
            'pk_num_unidad'=>$datos['idUnidades']
        ));
/*
*/
        $eliminarConversion = $this->_db->query("DELETE FROM lg_c002_unidades_conversion WHERE fk_lgb004_num_unidad = '".$datos['idUnidades']."'");
        $eliminarConversion = $this->_db->query("ALTER TABLE lg_c002_unidades_conversion auto_increment = 1;");

        $registroUnidadesConversion = $this->_db->prepare("
          INSERT INTO
            lg_c002_unidades_conversion
          SET
            num_cantidad=:num_cantidad,
            ind_cod_unidad_conversion=:ind_cod_unidad_conversion,
            fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          ");
        $modificarUnidadesConversion = $this->_db->prepare("
          INSERT INTO
            lg_c002_unidades_conversion
          SET
            pk_num_unidad_conversion=:pk_num_unidad_conversion,
            num_cantidad=:num_cantidad,
            ind_cod_unidad_conversion=:ind_cod_unidad_conversion,
            fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          ");

        foreach ($datos['cantidadC'] as $key=>$value) {
            if(isset($datos['idUC'][$key])){
                $modificarUnidadesConversion->execute(array(
                    'pk_num_unidad_conversion'=>$datos['idUC'][$key],
                    'ind_cod_unidad_conversion'=>$datos['codigoC'][$key],
                    'num_cantidad'=>$value,
                    'fk_lgb004_num_unidad'=>$datos['idUnidades'],
                    'num_estatus'=>$datos['estadoC'][$key]
                ));
            } else {
                $registroUnidadesConversion->execute(array(
                    'ind_cod_unidad_conversion'=>$datos['codigoC'][$key],
                    'num_cantidad'=>$value,
                    'fk_lgb004_num_unidad'=>$datos['idUnidades'],
                    'num_estatus'=>$datos['estadoC'][$key]
                ));
            }
        }

        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $datos['idUnidades'];
        }
    }

    public function metEliminarUnidades($idUnidades){
        $this->_db->beginTransaction();
        $eliminarUnidades2=$this->_db->prepare("
            DELETE FROM
            lg_c002_unidades_conversion
            WHERE
            fk_lgb004_num_unidad=:fk_lgb004_num_unidad
            ");
        $eliminarUnidades2->execute(array(
            'fk_lgb004_num_unidad'=>$idUnidades
        ));
        $eliminarUnidades=$this->_db->prepare("
            DELETE FROM
            lg_b004_unidades
            WHERE
            pk_num_unidad=:pk_num_unidad
            ");
        $eliminarUnidades->execute(array(
            'pk_num_unidad'=>$idUnidades
        ));

        $error = $eliminarUnidades->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idUnidades;
        }
    }

}

?>