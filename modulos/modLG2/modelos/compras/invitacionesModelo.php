<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'compras' . DS . 'actaInicioModelo.php';
class invitacionesModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->nombreUsuario = Session::metObtener('nombreUsuario');
        $this->perfil = Session::metObtener('perfil');
        $this->atActaInicioModelo = new actaInicioModelo();
    }

    public function metListarInvitaciones()
    {
        $invitacionesPost = $this->_db->query("
            SELECT
				invitacion.*,
                CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS proveedor,
                
                (SELECT SUM(num_total) FROM lg_c003_cotizacion AS cotizacion,lg_b010_invitacion AS invitacion3 
                WHERE 
                invitacion3.ind_num_invitacion = invitacion.ind_num_invitacion
                AND invitacion3.fec_anio = invitacion.fec_anio
                AND cotizacion.fk_lgb010_num_invitacion = invitacion3.pk_num_invitacion
                ) AS totalCotizado,

				(SELECT COUNT(*) FROM lg_b010_invitacion AS invitacion2
                 WHERE invitacion.ind_num_invitacion = invitacion2.ind_num_invitacion
                 AND invitacion.fec_anio = invitacion2.fec_anio
                ) AS nroLineas,
                cotizacion2.pk_num_cotizacion
            FROM
            	lg_b010_invitacion AS invitacion
            INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
            LEFT JOIN lg_c003_cotizacion AS cotizacion2 ON cotizacion2.fk_lgb010_num_invitacion = invitacion.pk_num_invitacion
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            GROUP BY invitacion.ind_num_invitacion
              ");
        $invitacionesPost->setFetchMode(PDO::FETCH_ASSOC);
        return $invitacionesPost->fetchAll();
    }

    public function metListarDesiertosPendientes()
    {
        $invitacionesPost = $this->_db->query("
            SELECT
              recomendacion.*,
              acta.pk_num_acta_inicio
            FROM
              lg_b012_informe_recomendacion AS recomendacion
              INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
              INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
              WHERE recomendacion.ind_estado <> 'AP'
              AND acta.ind_estado <> 'DS'
              ");
        $invitacionesPost->setFetchMode(PDO::FETCH_ASSOC);
        return $invitacionesPost->fetchAll();
    }

    public function metCotizar($datos)
    {
        $this->_db->beginTransaction();

        $registroCotizacion = $this->_db->prepare("
          INSERT INTO
            lg_c003_cotizacion
          SET
            ind_num_cotizacion=:ind_num_cotizacion,
			fec_documento=NOW(),
			fec_apertura=:fec_apertura,
			fec_recepcion=:fec_recepcion,
			fec_entrega=:fec_entrega,
			num_precio_unitario=:num_precio_unitario,
			num_precio_unitario_iva=:num_precio_unitario_iva,
			num_cantidad=:num_cantidad,
			num_total=:num_total,
			num_validez_oferta=:num_validez_oferta,
			num_dias_entrega=:num_dias_entrega,
			ind_num_cotizacion_proveedor=:ind_num_cotizacion_proveedor,
			fec_cotizacion_proveedor=NOW(),
			num_flag_asignado=:num_flag_asignado,
			num_flag_exonerado=:num_flag_exonerado,
			num_flag_elegido_sistema=:num_flag_elegido_sistema,
            ind_razon_elegido=:ind_razon_elegido,
            ind_observacion=:ind_observacion,
            ind_recomendacion=:ind_recomendacion,
			num_descuento_fijo=:num_descuento_fijo,
			num_descuento_porcentaje=:num_descuento_porcentaje,
            ind_estatus='PR',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
			fec_anio=:fec_anio,
			fk_lgb023_num_acta_detalle=:fk_lgb023_num_acta_detalle,
			fk_lgb010_num_invitacion=:fk_lgb010_num_invitacion,
			fk_rhb001_num_empleado_crea='$this->atIdEmpleado',
			fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
            fk_a006_num_miscelaneo_detalle_forma_pago=:fk_a006_num_miscelaneo_detalle_forma_pago
           ");

        $actualizarCotizacion = $this->_db->prepare("
          UPDATE
            lg_c003_cotizacion
          SET
            fec_recepcion=:fec_recepcion,
            fec_entrega=:fec_entrega,
			num_precio_unitario=:num_precio_unitario,
			num_precio_unitario_iva=:num_precio_unitario_iva,
			num_cantidad=:num_cantidad,
			num_total=:num_total,
			num_validez_oferta=:num_validez_oferta,
			num_dias_entrega=:num_dias_entrega,
			ind_num_cotizacion_proveedor=:ind_num_cotizacion_proveedor,
			num_flag_asignado=:num_flag_asignado,
			num_flag_exonerado=:num_flag_exonerado,
			num_flag_elegido_sistema=:num_flag_elegido_sistema,
            ind_razon_elegido=:ind_razon_elegido,
            ind_observacion=:ind_observacion,
            ind_recomendacion=:ind_recomendacion,
			num_descuento_fijo=:num_descuento_fijo,
			num_descuento_porcentaje=:num_descuento_porcentaje,
			fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fk_a006_num_miscelaneo_detalle_forma_pago=:fk_a006_num_miscelaneo_detalle_forma_pago
          WHERE
            pk_num_cotizacion=:pk_num_cotizacion
           ");

        $c=1;
        while($c<=$datos['secuencia']){
            if ($datos['idCotizacion'][$c]!=0) {
                $actualizarCotizacion->execute(array(
                    'fec_recepcion'=>$datos['frecepcion'],
                    'fec_entrega'=>$datos['fentrega'],
                    'num_precio_unitario'=>$datos['pUni'][$c],
                    'num_precio_unitario_iva'=>$datos['pIVA'][$c],
                    'num_cantidad'=>$datos['cPedida'][$c],
                    'num_total'=>$datos['total'][$c],
                    'num_validez_oferta'=>$datos['vofer'],
                    'num_dias_entrega'=>$datos['dias'],
                    'ind_num_cotizacion_proveedor'=>$c,
                    'num_flag_asignado'=>$datos['asignado'][$c],
                    'num_flag_exonerado'=>$datos['exo'][$c],
                    'num_flag_elegido_sistema'=>0,
                    'ind_razon_elegido'=>NULL,
                    'ind_observacion'=>$datos['observacion'][$c],
                    'ind_recomendacion'=>NULL,
                    'num_descuento_fijo'=>$datos['dfijo'][$c],
                    'num_descuento_porcentaje'=>$datos['dpor'][$c],
                    'pk_num_cotizacion'=>$datos['idCotizacion'][$c],
                    'fk_lgb004_num_unidad'=>$datos['uniCompra'][$c],
                    'fk_a006_num_miscelaneo_detalle_forma_pago'=>$datos['formaPago']
                ));

                $idRegistro=$datos['idCotizacion'][$c];
            } else {
                $registroCotizacion->execute(array(
                    'ind_num_cotizacion'=>$datos['ind_num_cotizacion'][$c],
                    'fec_apertura'=>$datos['fechaApertura'][$c],
                    'fec_recepcion'=>$datos['frecepcion'],
                    'fec_entrega'=>$datos['fentrega'],
                    'num_precio_unitario'=>$datos['pUni'][$c],
                    'num_precio_unitario_iva'=>$datos['pIVA'][$c],
                    'num_cantidad'=>$datos['cPedida'][$c],
                    'num_total'=>$datos['total'][$c],
                    'num_validez_oferta'=>$datos['vofer'],
                    'num_dias_entrega'=>$datos['dias'],
                    'ind_num_cotizacion_proveedor'=>$c,
                    'num_flag_asignado'=>$datos['asignado'][$c],
                    'num_flag_exonerado'=>$datos['exo'][$c],
                    'num_flag_elegido_sistema'=>0,
                    'ind_razon_elegido'=>NULL,
                    'ind_observacion'=>$datos['observacion'][$c],
                    'ind_recomendacion'=>NULL,
                    'num_descuento_fijo'=>$datos['dfijo'][$c],
                    'num_descuento_porcentaje'=>$datos['dpor'][$c],
                    'fec_anio'=>date('Y'),
                    'fk_lgb023_num_acta_detalle'=>$datos['idActaDet'][$c],
                    'fk_lgb010_num_invitacion'=>$datos['idInvitacion'][$c],
                    'fk_lgb004_num_unidad'=>$datos['uniCompra'][$c],
                    'fk_a006_num_miscelaneo_detalle_forma_pago'=>$datos['formaPago']
                ));
                $idRegistro= $this->_db->lastInsertId();
            }
            $c = $c+1;
        }
        $fallaTansaccion = $registroCotizacion->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metObtenerMaximaCotizacion()
    {
        $mostrarInvitacion = $this->_db->query("
              SELECT
                MAX(ind_num_cotizacion) AS maximo
              FROM
                lg_c003_cotizacion
                WHERE fec_anio=NOW()
                GROUP BY ind_num_cotizacion
                ORDER BY ind_num_cotizacion DESC
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetch();
    }

    public function metBuscarCotizaciones($numInvitacion,$anio)
    {
        $invitacionesPost = $this->_db->query("
            SELECT
              *,
              detalle.num_secuencia AS secuencia,
              detalle.ind_descripcion AS descripcion, 
              CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre,
              cotizacion.fk_lgb004_num_unidad AS unidadCompra,
              cotizacion.num_flag_exonerado AS exonerado,
              invitacion.num_dias_entrega AS num_dias,
              invitacion.num_validez_oferta AS num_oferta,
              invitacion.fec_entrega AS entrega,
              invitacion.fec_apertura AS apertura
            FROM
              lg_b010_invitacion AS invitacion 
            INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = invitacion.fk_lgb023_num_acta_detalle
            LEFT JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.fk_lgb010_num_invitacion = invitacion.pk_num_invitacion
            AND cotizacion.fk_lgb023_num_acta_detalle = detalle.pk_num_acta_detalle
            INNER JOIN lg_b022_proveedor AS prove ON prove.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
            INNER JOIN a003_persona AS per ON per.pk_num_persona = prove.fk_a003_num_persona_proveedor
            LEFT JOIN lg_b002_item AS item ON item.pk_num_item = detalle.fk_lgb002_num_item
            LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle.fk_lgb003_num_commodity
            WHERE invitacion.ind_num_invitacion = '$numInvitacion'
            AND invitacion.fec_anio = '$anio'
            ORDER BY invitacion.pk_num_invitacion,cotizacion.pk_num_cotizacion ASC
              ");
        $invitacionesPost->setFetchMode(PDO::FETCH_ASSOC);
        return $invitacionesPost->fetchAll();
    }

    public function metMostrarInformeRecomendacion($idInfo)
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              *
            FROM
              lg_b012_informe_recomendacion
            WHERE
              pk_num_informe_recomendacion = '$idInfo'
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetch();
    }

    public function metListarUnidades()
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              *
            FROM
              lg_b004_unidades
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetchAll();
    }

    public function metListarFormaPago()
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              detalle.pk_num_miscelaneo_detalle,
              detalle.ind_nombre_detalle
            FROM
              a006_miscelaneo_detalle AS detalle 
              INNER JOIN a005_miscelaneo_maestro AS maestro ON detalle.fk_a005_num_miscelaneo_maestro = maestro.pk_num_miscelaneo_maestro
              WHERE cod_maestro = 'FDPLG'
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetchAll();
    }

    public function metMostrarInvitacion($idInvi)
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              *,
              invitacion.fec_invitacion AS fechaInvi,
              invitacion.fk_a018_num_seguridad_usuario,
              acta.ind_nombre_procedimiento,
              acta.num_presupuesto_base
            FROM
              lg_b010_invitacion AS invitacion
              INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = invitacion.fk_lgb023_num_acta_detalle
              LEFT JOIN lg_c003_cotizacion AS cotizacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
              INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = detalle.fk_lgb009_num_acta_inicio
            WHERE
              pk_num_invitacion = '$idInvi'
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetch();
    }

    public function metDeclararDesierto($idActa){
        $this->_db->beginTransaction();
        $declararDesierto= $this->_db->prepare("
            UPDATE
              lg_b009_acta_inicio
            SET
              ind_estado=:ind_estado
            WHERE
              pk_num_acta_inicio=:pk_num_acta_inicio
        ");
        $declararDesierto->execute(array(
            'ind_estado' => 'DS',
            'pk_num_acta_inicio' => $idActa
        ));
        $fallaTansaccion = $declararDesierto->errorInfo();
        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $declararDesierto;
        }
    }

    public function metAnularInvitacion($idInvitacion){
        $this->_db->beginTransaction();

        $anularInvitacion=$this->_db->prepare("
            UPDATE
                lg_b010_invitacion
            SET
                ind_estado = 'AN',
                fec_ultima_modificacion = NOW(),
                fk_a018_num_seguridad_usuario = '$this->atIdUsuario'
            WHERE
                pk_num_invitacion=:pk_num_invitacion
            ");
        $anularInvitacion->execute(array(
            'pk_num_invitacion'=>$idInvitacion
        ));

        $error = $anularInvitacion->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idInvitacion;
        }
    }

    public function metListarActaDetalle($idActa)
    {
        $requerimientoPost =  $this->_db->query("
            SELECT
            
              detalle.*,
              invitacion.*,
              cotizacion.*,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              unidad.ind_descripcion AS unidad
            FROM lg_b023_acta_detalle AS detalle
            LEFT JOIN lg_c003_cotizacion AS cotizacion ON detalle.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
            LEFT JOIN lg_b010_invitacion AS invitacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
            LEFT JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
            LEFT JOIN lg_b004_unidades AS unidad ON detalle.fk_lgb004_num_unidad = unidad.pk_num_unidad
            LEFT JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            RIGHT JOIN lg_b009_acta_inicio AS acta on acta.pk_num_acta_inicio=detalle.fk_lgb009_num_acta_inicio
            WHERE
              detalle.fk_lgb009_num_acta_inicio = '$idActa'
              GROUP BY detalle.pk_num_acta_detalle
             ");

        $requerimientoPost->setFetchMode(PDO::FETCH_ASSOC);
        return $requerimientoPost->fetchAll();
    }

    public function metMostrarEmpleadoInvitacion($idUsuario)
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              *,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_apellido1) AS nombre
            FROM
              a003_persona AS persona 
              INNER JOIN rh_b001_empleado AS empleado ON empleado.fk_a003_num_persona = persona.pk_num_persona
              INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos AS puesto ON laboral.fk_rhc063_num_puestos_cargo = puesto.pk_num_puestos
              INNER JOIN a018_seguridad_usuario AS usuario ON empleado.pk_num_empleado = usuario.fk_rhb001_num_empleado
            WHERE
              usuario.pk_num_seguridad_usuario = '$idUsuario'
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetch();
    }

}

?>