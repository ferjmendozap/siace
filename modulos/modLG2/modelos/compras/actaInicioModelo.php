<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'compras' . DS . 'requerimientoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'aspectosCualitativosModelo.php';
class actaInicioModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atRequerimientoModelo = new requerimientoModelo();
        $this->atAspectosCualitativosModelo = new aspectosCualitativosModelo();
    }

    public function metListarInformeW($estatus){
        $informePost =  $this->_db->query("
            SELECT
              informe.*,
              detalle.*,
              cuantitativa.*
            FROM lg_b012_informe_recomendacion AS informe
            INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = informe.fk_lgb011_num_evaluacion
            INNER JOIN lg_c004_evaluacion_cuantitativa AS cuantitativa ON evaluacion.pk_num_evaluacion = cuantitativa.fk_lgb011_num_evaluacion
            INNER JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.pk_num_cotizacion = cuantitativa.fk_lgc003_num_cotizacion
            INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
            WHERE
              informe.ind_estado = '$estatus'
            GROUP BY informe.pk_num_informe_recomendacion
             ");

        $informePost->setFetchMode(PDO::FETCH_ASSOC);
        return $informePost->fetchAll();
    }

    public function metListarInformeAdj(){
        $informePost =  $this->_db->query("
            SELECT
              informe.*,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              proveedor.pk_num_proveedor,
              recomendado.num_secuencia
            FROM lg_c006_proveedor_recomendado AS recomendado
            INNER JOIN lg_b012_informe_recomendacion AS informe ON recomendado.fk_lgb012_num_informe_recomendacion = informe.pk_num_informe_recomendacion
            INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = recomendado.fk_lgb022_num_proveedor
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE
              informe.ind_estado = 'AP'
            GROUP BY recomendado.fk_lgb022_num_proveedor,recomendado.fk_lgb012_num_informe_recomendacion
            ORDER BY informe.pk_num_informe_recomendacion,informe.num_recomendacion ASC
             ");

        $informePost->setFetchMode(PDO::FETCH_ASSOC);
        return $informePost->fetchAll();
    }

    public function metActualizarInforme($idInfor,$estatus,$campo=false,$fecha=false){
        $this->_db->beginTransaction();
        if($campo) {
            $insert=",$campo=$this->atIdUsuario, $fecha=NOW() ";
        } else {
            $insert="";
        }
        $informePost=$this->_db->prepare("
            UPDATE
              lg_b012_informe_recomendacion
            SET
              ind_estado=:ind_estado
              $insert
            WHERE
              pk_num_informe_recomendacion='$idInfor'
             ");
        $informePost->execute(array(
            'ind_estado'=> $estatus
        ));
        $fallaTansaccion = $informePost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idInfor;
        }
    }

    public function metListarInforme(){
        $informePost =  $this->_db->query("
            SELECT
              *
            FROM lg_b012_informe_recomendacion
             ");

        $informePost->setFetchMode(PDO::FETCH_ASSOC);
        return $informePost->fetchAll();
    }

    public function metListarProveedores($id,$cual){
        if($cual==1){
            $informePost =  $this->_db->query("
            SELECT
              persona.*,
              proveedor.*,
              detalle.*
            FROM lg_b022_proveedor AS proveedor
            INNER JOIN a003_persona AS persona ON proveedor.fk_a003_num_persona_proveedor = persona.pk_num_persona
            LEFT JOIN lg_b010_invitacion AS invitacion ON invitacion.fk_lgb022_num_proveedor = proveedor.pk_num_proveedor
            INNER JOIN lg_b023_acta_detalle AS detalle2 ON detalle2.pk_num_acta_detalle = invitacion.fk_lgb023_num_acta_detalle
            LEFT JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.fk_lgb010_num_invitacion = invitacion.pk_num_invitacion
            AND cotizacion.num_flag_asignado = 1
            LEFT JOIN a006_miscelaneo_detalle AS detalle ON detalle.cod_detalle = proveedor.ind_condicion_rcn 
            WHERE detalle2.fk_lgb009_num_acta_inicio = '$id'
            GROUP BY persona.pk_num_persona
             ");
        } elseif ($cual==2){
            $informePost =  $this->_db->query("
            SELECT
              persona.*,
              proveedor.*,
              detalle.*
            FROM lg_b022_proveedor AS proveedor
            LEFT JOIN lg_c006_proveedor_recomendado AS recomendado ON proveedor.pk_num_proveedor = recomendado.fk_lgb022_num_proveedor
            INNER JOIN a003_persona AS persona ON proveedor.fk_a003_num_persona_proveedor = persona.pk_num_persona
            INNER JOIN lg_b010_invitacion AS invitacion ON invitacion.fk_lgb022_num_proveedor = proveedor.pk_num_proveedor
            INNER JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.fk_lgb010_num_invitacion = invitacion.pk_num_invitacion
            AND cotizacion.num_flag_asignado = 1
            LEFT JOIN a006_miscelaneo_detalle AS detalle ON detalle.cod_detalle = proveedor.ind_condicion_rcn 
            WHERE recomendado.fk_lgb012_num_informe_recomendacion = '$id'
            GROUP BY persona.pk_num_persona
             ");
        }

        $informePost->setFetchMode(PDO::FETCH_ASSOC);
        return $informePost->fetchAll();
    }

    public function metEval($idEval){
        $informePost =  $this->_db->query("
            SELECT
              *
            FROM lg_b011_evaluacion
            WHERE pk_num_evaluacion = '$idEval'
             ");

        $informePost->setFetchMode(PDO::FETCH_ASSOC);
        return $informePost->fetchAll();
    }

    public function metBuscarDatosEvalCualitativa($idEval)
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT
              cualitativa.*,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              detalle.cod_detalle,
              aspecto.ind_cod_aspecto
            FROM 
              lg_c012_evaluacion_cualitativa AS cualitativa 
            INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = cualitativa.fk_lgb022_num_proveedor_recomendado
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            INNER JOIN lg_e007_aspecto_cualitativo AS aspecto ON cualitativa.fk_lge007_num_aspecto_cualitativo = aspecto.pk_num_aspecto
            INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = aspecto.fk_a006_num_miscelaneo_detalle
            WHERE
              fk_lgb011_num_evaluacion = '$idEval'
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metBuscarDatosEvalCuantitativa($idEval=false)
    {
        $sql = '';
        if($idEval) {
            $sql = "WHERE cuantitativa.fk_lgb011_num_evaluacion = '$idEval'";
        }
        $mostrarInvitacion = $this->_db->query("
            SELECT
              *
            FROM 
              lg_c004_evaluacion_cuantitativa AS cuantitativa 
              LEFT JOIN lg_c003_cotizacion AS cotizacion ON cuantitativa.fk_lgc003_num_cotizacion = cotizacion.pk_num_cotizacion
              $sql
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metBuscarProveedorEval($idEval,$cual=false)
    {
        $sql = '';
        if($cual){
            $sql = 'GROUP BY cualitativa.fk_lgb022_num_proveedor_recomendado';
        }
        $mostrarInvitacion = $this->_db->query("
            SELECT
              *
            FROM 
              lg_c012_evaluacion_cualitativa AS cualitativa
               INNER JOIN lg_b022_proveedor AS proveedor ON cualitativa.fk_lgb022_num_proveedor_recomendado = proveedor.pk_num_proveedor
               INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE
              cualitativa.fk_lgb011_num_evaluacion = '$idEval'
              $sql
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metInvitacionesActa($idActa){
        $informePost =  $this->_db->query("
            SELECT
              *,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombreProveedor
            FROM
            lg_b010_invitacion AS invitacion
            INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
            INNER JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.fk_lgb010_num_invitacion = invitacion.pk_num_invitacion
            INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
            INNER JOIN lg_b009_acta_inicio AS inicio ON detalle.fk_lgb009_num_acta_inicio = inicio.pk_num_acta_inicio
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE detalle.fk_lgb009_num_acta_inicio = '$idActa' 
            GROUP BY proveedor.pk_num_proveedor
            ORDER BY invitacion.pk_num_invitacion ASC
             ");

        $informePost->setFetchMode(PDO::FETCH_ASSOC);
        return $informePost->fetchAll();
    }

    public function metCotizacionesActa($idActa){
        $informePost =  $this->_db->query("
            SELECT
              *
            FROM
            lg_b010_invitacion AS invitacion
            INNER JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.fk_lgb010_num_invitacion = invitacion.pk_num_invitacion
            INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
            LEFT JOIN lg_c004_evaluacion_cuantitativa AS cuantitativa ON cuantitativa.fk_lgc003_num_cotizacion = cotizacion.pk_num_cotizacion
            INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE detalle.fk_lgb009_num_acta_inicio = '$idActa' 
            GROUP BY invitacion.pk_num_invitacion
            ORDER BY invitacion.pk_num_invitacion ASC
             ");

        $informePost->setFetchMode(PDO::FETCH_ASSOC);
        return $informePost->fetchAll();
    }

    public function metListarCotizacion($idActa=false){
        if($idActa){
            $informePost =  $this->_db->query("
            SELECT
              *
            FROM
            lg_c003_cotizacion AS cotizacion 
            INNER JOIN lg_b010_invitacion AS invitacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
            INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
            WHERE detalle.fk_lgb009_num_acta_inicio = '$idActa' 
             ");
        } else {
            $informePost =  $this->_db->query("
            SELECT
              *
            FROM
            lg_c003_cotizacion  
             ");
        }

        $informePost->setFetchMode(PDO::FETCH_ASSOC);
        return $informePost->fetchAll();
    }


    public function metMenorCotizacion($idActa){
        $informePost =  $this->_db->query("
            SELECT
             MIN(num_precio_unitario_iva) AS menor
            FROM
            lg_c003_cotizacion AS cotizacion
            INNER JOIN lg_b010_invitacion AS invitacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
            INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = invitacion.fk_lgb023_num_acta_detalle
            WHERE
              detalle.fk_lgb009_num_acta_inicio = '$idActa' 
            AND num_precio_unitario_iva>0
             ");

        $informePost->setFetchMode(PDO::FETCH_ASSOC);
        return $informePost->fetch();
    }

    public function metListarActaInvitacionCotizacion(){
        $informePost =  $this->_db->query("
            SELECT
              acta.pk_num_acta_inicio
            FROM lg_b009_acta_inicio AS acta
            INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
            INNER JOIN lg_b010_invitacion AS invitacion ON detalle.pk_num_acta_detalle = invitacion.fk_lgb023_num_acta_detalle
            INNER JOIN lg_c003_cotizacion AS cotizacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
            GROUP BY acta.pk_num_acta_inicio
            ORDER BY acta.pk_num_acta_inicio DESC
             ");

        $informePost->setFetchMode(PDO::FETCH_ASSOC);
        return $informePost->fetchAll();
    }

    public function metListarActaDetalle($idActa)
    {
        $requerimientoPost =  $this->_db->query("
            SELECT
              detalle.*,
              invitacion.*,
              cotizacion.*,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              unidad.ind_descripcion AS unidad
            FROM lg_b023_acta_detalle AS detalle
            LEFT JOIN lg_c003_cotizacion AS cotizacion ON detalle.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
            LEFT JOIN lg_b010_invitacion AS invitacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
            LEFT JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
            LEFT JOIN lg_b004_unidades AS unidad ON detalle.fk_lgb004_num_unidad = unidad.pk_num_unidad
            LEFT JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE
              detalle.fk_lgb009_num_acta_inicio = '$idActa' AND cotizacion.num_precio_unitario_iva>0
              GROUP BY detalle.pk_num_acta_detalle
             ");

        $requerimientoPost->setFetchMode(PDO::FETCH_ASSOC);
        return $requerimientoPost->fetchAll();
    }

    public function metListarInvitaciones()
    {
        $listInvi = $this->_db->query("
              SELECT * FROM lg_b010_invitacion 
              ORDER BY pk_num_invitacion ASC
          ");
        $listInvi ->setFetchMode(PDO::FETCH_ASSOC);
        return $listInvi ->fetchAll();
    }

    public function metBuscarCotizacion($idActaDet)
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT min(cotizacion.num_precio_unitario_iva) AS menor 
            FROM lg_c003_cotizacion AS cotizacion 
            INNER JOIN lg_b010_invitacion AS invitacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
            WHERE cotizacion.fk_lgb023_num_acta_detalle = '$idActaDet' AND cotizacion.num_precio_unitario_iva>0
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetch();
    }

    public function metMostrarActa($idActa){
        $mostrarActa = $this->_db->query("
            SELECT
              act.*,
              usu.ind_usuario,
              detalle.*
            FROM lg_b009_acta_inicio AS act
            INNER JOIN a018_seguridad_usuario AS usu ON usu.pk_num_seguridad_usuario = act.fk_a018_num_seguridad_usuario
            LEFT JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = act.fk_a006_num_miscelaneos_modalidad
            WHERE
              act.pk_num_acta_inicio='$idActa'
        ");
        $mostrarActa->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarActa->fetch();
    }

    public function metMostrarActaDetalle($idActa){
        $mostrarActa = $this->_db->query("
            SELECT
              detalle.*,
              unidad.ind_descripcion AS unidad,
              detalle.pk_num_acta_detalle
            FROM 
              lg_b023_acta_detalle AS detalle
               INNER JOIN lg_b004_unidades AS unidad ON detalle.fk_lgb004_num_unidad = unidad.pk_num_unidad
            WHERE
              fk_lgb009_num_acta_inicio='$idActa'
        ");
        $mostrarActa->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarActa->fetchAll();
    }
    
    public function metMostrarEmpleado($idEmpleado){
        $persona = $this->_db->query("
              SELECT CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre, 
            per.ind_documento_fiscal,
             per.ind_cedula_documento,
              puesto.ind_descripcion_cargo,
               laboral.fec_ingreso,
               laboral.ind_resolucion_ingreso
              FROM rh_b001_empleado AS emp 
              INNER JOIN a003_persona AS per ON per.pk_num_persona = emp.fk_a003_num_persona 
              LEFT JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = emp.pk_num_empleado 
              LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            WHERE
              emp.pk_num_empleado='$idEmpleado'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metBuscarDetalleReq($valor){
        $persona = $this->_db->query("
            SELECT
              *,
              
              item.ind_descripcion AS descItem,
              item.fk_lgb004_num_unidad_compra AS uniItem,
              item.fk_prb002_num_partida_presupuestaria AS partidaItem,
              item.fk_cbb004_num_plan_cuenta_gasto_oncop AS cuentaItem,
              commodity.ind_descripcion AS descComm,
              commodity.fk_lgb004_num_unidad AS uniComm,
              commodity.fk_prb002_num_partida_presupuestaria AS partidaComm,
              commodity.fk_cbb004_num_plan_cuenta AS cuentaComm,
              detalle.fk_lgb004_num_unidad
            FROM lg_c001_requerimiento_detalle AS detalle 
            LEFT JOIN lg_b002_item AS item ON item.pk_num_item = detalle.fk_lgb002_num_item
            LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle.fk_lgb003_num_commodity
            WHERE
              fk_lgb001_num_requerimiento = '$valor'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetchAll();
    }

    public function metBuscarReq($valor,$cual=false){
        if($cual){
            $sql="acta.pk_num_acta_inicio = '$valor'";
        } else {
            $sql="requerimiento.cod_requerimiento = '$valor'";
        }
        $persona = $this->_db->query("
            SELECT
              requerimiento.*,
              acta.ind_estado AS estadoActa
            FROM lg_b001_requerimiento AS requerimiento 
            LEFT JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON requerimiento.pk_num_requerimiento = acta_requerimiento.fk_lgb001_num_requerimiento
            LEFT JOIN lg_b009_acta_inicio AS acta ON acta_requerimiento.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
            WHERE
              $sql
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        if($cual){
            return $persona->fetchAll();
        } else {
            return $persona->fetch();
        }
    }

    public function metCrearActa($datos)
    {
        $this->_db->beginTransaction();
        $registroActa = $this->_db->prepare("
          INSERT INTO
            lg_b009_acta_inicio
          SET
            fk_rhb001_empleado_asistente_a=:fk_rhb001_empleado_asistente_a,
            fk_rhb001_empleado_asistente_b=:fk_rhb001_empleado_asistente_b,
            fk_rhb001_empleado_asistente_c=:fk_rhb001_empleado_asistente_c,
            fec_registro=NOW(),
            num_procedimiento=:num_procedimiento,
            ind_codigo_acta=:ind_codigo_acta,
            ind_codigo_procedimiento=:ind_codigo_procedimiento,
            ind_nombre_procedimiento=:ind_nombre_procedimiento,
            fec_reunion=:fec_reunion,
            fec_hora_reunion=:fec_hora_reunion,
            num_presupuesto_base=:num_presupuesto_base,
            fec_inicio=:fec_inicio,
            fec_fin=:fec_fin,
            num_flag_contrato=:num_flag_contrato,
            fk_a006_num_miscelaneos_modalidad=:fk_a006_num_miscelaneos_modalidad,
            ind_modalidad_contratacion=:ind_modalidad_contratacion,
            num_anio=:num_anio,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
           ");

        $registroActa->execute(array(
            'fk_rhb001_empleado_asistente_a'=>$datos['asisA'],
            'fk_rhb001_empleado_asistente_b'=>$datos['asisB'],
            'fk_rhb001_empleado_asistente_c'=>$datos['asisC'],
            'num_procedimiento'=>$datos['proce'],
            'ind_codigo_acta'=>$datos['numActa'],
            'ind_codigo_procedimiento'=>$datos['numProce'],
            'ind_nombre_procedimiento'=>$datos['nombre'],
            'fec_reunion'=>$datos['freu'],
            'fec_hora_reunion'=>$datos['hreu'],
            'num_presupuesto_base'=>$datos['pres'],
            'fec_inicio'=>$datos['fini'],
            'fec_fin'=>$datos['ffin'],
            'num_flag_contrato'=>$datos['contrato'],
            'fk_a006_num_miscelaneos_modalidad'=>$datos['tipoC'],
            'ind_modalidad_contratacion'=>$datos['contratacion'],
            'num_anio'=>date('Y'),
        ));

        $idRegistro= $this->_db->lastInsertId();

        $c=0;
        foreach ($datos['codReq'] AS $key=>$value){
            $c=$c+1;
            $detalles = $this->_db->query("
            INSERT INTO
              lg_c011_acta_requerimiento
            SET
              num_secuencia = '$c',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              fk_lgb009_num_acta_inicio='$idRegistro',
              fk_lgb001_num_requerimiento='$value'
            ");
        }

        $registroActaDetalle = $this->_db->prepare("
          INSERT INTO
            lg_b023_acta_detalle
          SET
            num_secuencia=:num_secuencia,
            num_cantidad_pedida=:num_cantidad_pedida,
            num_flag_exonerado='0',
            ind_descripcion=:ind_descripcion,
            ind_estado='PR',
            fk_lgb009_num_acta_inicio=:fk_lgb009_num_acta_inicio,
            fk_lgb002_num_item=:fk_lgb002_num_item,
            fk_lgb003_num_commodity=:fk_lgb003_num_commodity,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
            fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
            fk_prb002_num_partida=:fk_prb002_num_partida
           ");

        $c=0;
        foreach ($datos['comentario'] as $key =>$value) {
            $c=$c+1;
            $registroActaDetalle->execute(array(
                'num_secuencia'=>$c,
                'num_cantidad_pedida'=>$datos['cantidad'][$key],
                'ind_descripcion'=>$datos['comentario'][$key],
                'fk_lgb009_num_acta_inicio'=>$idRegistro,
                'fk_lgb002_num_item'=>$datos['fk_lgb002_num_item'][$key],
                'fk_lgb003_num_commodity'=>$datos['fk_lgb003_num_commodity'][$key],
                'fk_lgb004_num_unidad'=>$datos['unidad'][$key],
                'fk_cbb004_num_plan_cuenta'=>$datos['cuenta'][$key],
                'fk_prb002_num_partida'=>$datos['partida'][$key],
            ));
            $idRegistroDet[$key]= $this->_db->lastInsertId();

        }
        $registroActaReqDetalle = $this->_db->prepare("
                  INSERT INTO
                    lg_e009_acta_detalle_requerimiento_detalle
                  SET
                    fk_lgb023_num_acta_detalle=:fk_lgb023_num_acta_detalle,
                    fk_lgc001_num_requerimiento_detalle=:fk_lgc001_num_requerimiento_detalle
           ");

        foreach ($datos['id'] as $key2 =>$value2) {
            foreach ($datos['idReqDet'] as $key3 =>$value3) {
                #$value son los detalles del requerimiento
                $registroActaReqDetalle->execute(array(
                    'fk_lgb023_num_acta_detalle'=>$idRegistroDet[$key2],
                    'fk_lgc001_num_requerimiento_detalle'=>$value3
                ));
            }
        }

        $fallaTansaccion = $registroActa->errorInfo();
        $fallaTansaccion2 = $registroActaDetalle->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarActa($datos)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
          UPDATE
            lg_b009_acta_inicio
          SET
            fk_rhb001_empleado_asistente_a=:fk_rhb001_empleado_asistente_a,
            fk_rhb001_empleado_asistente_b=:fk_rhb001_empleado_asistente_b,
            fk_rhb001_empleado_asistente_c=:fk_rhb001_empleado_asistente_c,
            num_presupuesto_base=:num_presupuesto_base,
            ind_nombre_procedimiento=:ind_nombre_procedimiento,
            fec_hora_reunion=:fec_hora_reunion,
            num_flag_contrato=:num_flag_contrato,
            fk_a006_num_miscelaneos_modalidad=:fk_a006_num_miscelaneos_modalidad,
            ind_modalidad_contratacion=:ind_modalidad_contratacion,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          WHERE
            pk_num_acta_inicio=:pk_num_acta_inicio
            ");
        $modificarRegistro->execute(array(
            'fk_rhb001_empleado_asistente_a'=>$datos['asisA'],
            'fk_rhb001_empleado_asistente_b'=>$datos['asisB'],
            'fk_rhb001_empleado_asistente_c'=>$datos['asisC'],
            'num_presupuesto_base'=>$datos['pres'],
            'ind_nombre_procedimiento'=>$datos['nombre'],
            'fec_hora_reunion'=>$datos['hreu'],
            'num_flag_contrato'=>$datos['contrato'],
            'fk_a006_num_miscelaneos_modalidad'=>$datos['tipoC'],
            'ind_modalidad_contratacion'=>$datos['contratacion'],
            'pk_num_acta_inicio'=>$datos['idActa']
        ));

        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $datos['idActa'];
        }
    }

    public function metEliminarActa($idActa){
        $this->_db->beginTransaction();
        $eliminaridActa=$this->_db->prepare("
            DELETE FROM
            lg_b009_acta_inicio
            WHERE
            pk_num_acta_inicio=:pk_num_acta_inicio
            ");
        $eliminaridActa->execute(array(
            'pk_num_acta_inicio'=>$idActa
        ));

        $error = $eliminaridActa->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idActa;
        }
    }

    public function metListarEvaluacion()
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT 
              evaluacion.*,
              recomendacion.pk_num_informe_recomendacion,
              recomendacion.ind_estado AS estadoInfor 
            FROM lg_b011_evaluacion AS evaluacion 
            LEFT JOIN lg_b012_informe_recomendacion AS recomendacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metBuscarUT()
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT 
              *
            FROM 
              cp_b018_unidad_tributaria
              WHERE ind_anio = '".date('Y')."'
              ORDER BY pk_num_unidad_tributaria DESC
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetch();
    }

    public function metListarEvaluacionCC($idEval)
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT
            CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
            recomendacion.pk_num_informe_recomendacion,
            cuantitativa.*,
            cotizacion.num_precio_unitario_iva
            FROM lg_c004_evaluacion_cuantitativa AS cuantitativa
            INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = cuantitativa.fk_lgb022_num_proveedor_recomendado
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = cuantitativa.fk_lgb011_num_evaluacion
            INNER JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.pk_num_cotizacion = cuantitativa.fk_lgc003_num_cotizacion
            LEFT JOIN lg_b012_informe_recomendacion AS recomendacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
            WHERE evaluacion.pk_num_evaluacion = '$idEval'
            ORDER BY cuantitativa.pk_num_cuantitativa ASC

        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metListarEvaluacionyECC()
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT
              cuantitativa.fk_lgc003_num_cotizacion,
              cuantitativa.fec_anio AS anio,
              evaluacion.fk_rhb001_num_empleado_asistente_a AS asisa,
              evaluacion.fk_rhb001_num_empleado_asistente_b AS asisb,
              evaluacion.fk_rhb001_num_empleado_asistente_c AS asisc,
              evaluacion.*,
              evaluacion.fec_anio AS fecanio,
              cuantitativa.*,
              recomendacion.ind_estado AS estadoInfor,
              recomendacion.*
            FROM
              lg_c004_evaluacion_cuantitativa AS cuantitativa
            INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = cuantitativa.fk_lgb011_num_evaluacion
            INNER JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.pk_num_cotizacion = cuantitativa.fk_lgc003_num_cotizacion
            INNER JOIN lg_b010_invitacion AS invitacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
            LEFT JOIN lg_b012_informe_recomendacion AS recomendacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
            WHERE cuantitativa.num_pmo_poe = '1.000000'
            GROUP BY evaluacion.pk_num_evaluacion
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metMostrarEvaluacion($idEval)
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT
              evaluacion.*,
              inicio.ind_codigo_acta,
              inicio.ind_codigo_procedimiento
            FROM
              lg_b011_evaluacion AS evaluacion 
              INNER JOIN lg_b009_acta_inicio AS inicio ON inicio.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
            WHERE
              pk_num_evaluacion = '$idEval'
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetch();
    }

    public function metMostrarInforme($idInfo)
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              recomendacion.*,
              recomendacion.ind_recomendacion AS rec,
              recomendacion.ind_estado AS estatus,
              persona.ind_nombre1 AS n1,
              persona.ind_nombre2 AS n2,
              persona.ind_apellido1 AS a1,
              persona.ind_apellido2 AS a2,
              proveedor.pk_num_proveedor,
              cotizacion.*,
              invitacion.*,
              invitacion.fec_anio AS anioInvi,
              cuantitativa.*,
              acta.pk_num_acta_inicio,
              acta.ind_codigo_procedimiento
            FROM
              lg_b012_informe_recomendacion AS recomendacion
              INNER JOIN lg_c006_proveedor_recomendado AS provRec ON recomendacion.pk_num_informe_recomendacion = provRec.fk_lgb012_num_informe_recomendacion
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = provRec.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
              INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
              INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
              INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
              INNER JOIN lg_b010_invitacion AS invitacion ON detalle.pk_num_acta_detalle = invitacion.fk_lgb023_num_acta_detalle
              INNER JOIN lg_c003_cotizacion AS cotizacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
              INNER JOIN lg_c004_evaluacion_cuantitativa AS cuantitativa ON cotizacion.pk_num_cotizacion = cuantitativa.fk_lgc003_num_cotizacion
            WHERE
              recomendacion.pk_num_informe_recomendacion = '$idInfo'
              GROUP BY recomendacion.pk_num_informe_recomendacion
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetch();
    }

    public function metBuscarProveedorRec($idInfo)
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              recomendado.*,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre
            FROM
              lg_c006_proveedor_recomendado AS recomendado
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = recomendado.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
              INNER JOIN lg_b012_informe_recomendacion AS recomendacion ON recomendacion.pk_num_informe_recomendacion = recomendado.fk_lgb012_num_informe_recomendacion
              WHERE
              recomendacion.pk_num_informe_recomendacion = '$idInfo'
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetch();
    }

    public function metMostrarInformeCompleto($idInfo)
    {
        $mostrarInforme= $this->_db->query("
        SELECT
          detalle.*,
          recomendacion.fk_a006_num_miscelaneo_detalle_tipo_adjudicacion,
          recomendacion.pk_num_informe_recomendacion,
          recomendacion.fk_rhb001_num_empleado_asistente_a,
          recomendacion.fk_rhb001_num_empleado_asistente_b,
          recomendacion.fk_rhb001_num_empleado_asistente_c
        FROM 
          lg_b023_acta_detalle AS detalle
          INNER JOIN lg_b009_acta_inicio AS acta ON detalle.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
          INNER JOIN lg_b011_evaluacion AS evauacion ON evauacion.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
          INNER JOIN lg_b012_informe_recomendacion AS recomendacion ON recomendacion.fk_lgb011_num_evaluacion = evauacion.pk_num_evaluacion
        WHERE
              recomendacion.pk_num_informe_recomendacion = '$idInfo'
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetchAll();
    }

    public function metMostrarInformeLista($idInfo)
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              *
            FROM
              lg_b012_informe_recomendacion
            WHERE
              pk_num_informe_recomendacion = '$idInfo'
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetchAll();
    }

    public function metMostrarECC($idEval)
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT
              *
            FROM
              lg_c004_evaluacion_cuantitativa
            WHERE
              fk_lgb011_num_evaluacion = '$idEval'
            GROUP BY fk_lgb022_num_proveedor_recomendado
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metMostrarECCId($idEval)
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT
              cuantitativa.*,
              cotizacion.*,
              detalle.ind_descripcion,
              detalle.num_cantidad_pedida,
              unidad.ind_descripcion AS unidadCompra
            FROM
              lg_c004_evaluacion_cuantitativa AS cuantitativa
               INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = cuantitativa.fk_lgb011_num_evaluacion
               LEFT JOIN lg_c003_cotizacion as cotizacion ON cotizacion.pk_num_cotizacion = cuantitativa.fk_lgc003_num_cotizacion
               INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
               INNER JOIN lg_b004_unidades AS unidad ON detalle.fk_lgb004_num_unidad = unidad.pk_num_unidad
            WHERE
              evaluacion.pk_num_evaluacion = '$idEval'
              ORDER BY cuantitativa.pk_num_cuantitativa
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metCrearEvaluacion($datos)
    {
        $this->_db->beginTransaction();

        $registroEval1 = $this->_db->prepare("
          INSERT INTO
            lg_b011_evaluacion
          SET
            num_evaluacion=:num_evaluacion,
            ind_cod_evaluacion=:ind_cod_evaluacion,
			fec_evaluacion=NOW(),
			ind_objeto_evaluacion=:ind_objeto_evaluacion,
			ind_conclusion=:ind_conclusion,
			ind_recomendacion=:ind_recomendacion,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
			fec_anio=:fec_anio,
			fk_lgb009_num_acta_inicio=:fk_lgb009_num_acta_inicio,
			fk_rhb001_num_empleado_asistente_a=:fk_rhb001_num_empleado_asistente_a,
			fk_rhb001_num_empleado_asistente_b=:fk_rhb001_num_empleado_asistente_b,
			fk_rhb001_num_empleado_asistente_c=:fk_rhb001_num_empleado_asistente_c,
			ind_proveedores=:ind_proveedores
           ");

        $registroEval1->execute(array(
            'num_evaluacion'=>$datos['cantidad'],
            'ind_cod_evaluacion'=>$datos['ind_cod_evaluacion'],
            'ind_objeto_evaluacion'=>$datos['objeto'],
            'ind_conclusion'=>$datos['conclusion'],
            'ind_recomendacion'=>$datos['recomendacion'],
            'fec_anio'=>date('Y'),
            'fk_lgb009_num_acta_inicio'=>$datos['idActa'],
            'fk_rhb001_num_empleado_asistente_a'=>$this->atIdEmpleado,
            'fk_rhb001_num_empleado_asistente_b'=>$datos['segundo'],
            'fk_rhb001_num_empleado_asistente_c'=>$datos['tercero'],
            'ind_proveedores'=>$datos['ind_proveedores']
        ));

        $idEval= $this->_db->lastInsertId();
        $registroEval2 = $this->_db->prepare("
          INSERT INTO
            lg_c004_evaluacion_cuantitativa
          SET
			num_pmo_poe=:num_pmo_poe,
			num_pp=:num_pp,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
			fec_anio=:fec_anio,
			fk_lgc003_num_cotizacion=:fk_lgc003_num_cotizacion,
			fk_lgb022_num_proveedor_recomendado=:fk_lgb022_num_proveedor_recomendado,
			fk_lgb011_num_evaluacion=:fk_lgb011_num_evaluacion
           ");
        foreach ($datos['proveedores'] AS $key=>$value){
            foreach ($datos['pmoPoe'][$value] as $key2=>$value2) {
                $registroEval2->execute(array(
                    'num_pmo_poe'=>$value2,
                    'num_pp'=>$datos['pp'][$value][$key2],
                    'fec_anio'=>date('Y'),
                    'fk_lgc003_num_cotizacion'=>$datos['idCotizacion'][$value][$key2],
                    'fk_lgb022_num_proveedor_recomendado'=>$value,
                    'fk_lgb011_num_evaluacion'=>$idEval
                ));
            }
        }

        $registroEval3 = $this->_db->prepare("
          INSERT INTO
            lg_c012_evaluacion_cualitativa
          SET
			ind_nombre_aspecto=:ind_nombre_aspecto,
			num_puntaje=:num_puntaje,
			num_puntaje_maximo_evaluado=:num_puntaje_maximo_evaluado,
			num_total_puntaje_cualitativo=:num_total_puntaje_cualitativo,
			num_total_puntaje_evaluado=:num_total_puntaje_evaluado,
			fk_lge007_num_aspecto_cualitativo=:fk_lge007_num_aspecto_cualitativo,
			fk_lgb022_num_proveedor_recomendado=:fk_lgb022_num_proveedor_recomendado,
			fk_lgb011_num_evaluacion=:fk_lgb011_num_evaluacion,
            fec_ultima_modificacion=NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
           ");
        foreach ($datos['asp'] AS $key=>$value){
            #$key = pk_num_aspecto
            foreach ($value AS $key2=>$value2){
                #$key2 = pk_num_proveedor

                $registroEval3->execute(array(
                    'ind_nombre_aspecto'=>$datos['nombreAsp'][$key],
                    'num_puntaje'=>$value[$key2],
                    'num_puntaje_maximo_evaluado'=>$datos['pm'][$key],
                    'num_total_puntaje_cualitativo'=>$datos['total'][$key2],
                    'num_total_puntaje_evaluado'=>$datos['totalPm'],
                    'fk_lge007_num_aspecto_cualitativo'=>$key,
                    'fk_lgb022_num_proveedor_recomendado'=>$key2,
                    'fk_lgb011_num_evaluacion'=>$idEval
                ));
            }
        }

        $fallaRegistroEval = $registroEval2->errorInfo();

        if(!empty($fallaRegistroEval[1]) && !empty($fallaRegistroEval[2])){
            $this->_db->rollBack();
            return $fallaRegistroEval;
        }else{
            $this->_db->commit();
            return $idEval;
        }

    }

    public function metModificarEvaluacion($datos)
    {
        $this->_db->beginTransaction();

        $registroEval1 = $this->_db->prepare("
              UPDATE
                lg_b011_evaluacion
              SET
                ind_objeto_evaluacion=:ind_objeto_evaluacion,
                ind_conclusion=:ind_conclusion,
                ind_recomendacion=:ind_recomendacion,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_rhb001_num_empleado_asistente_a=:fk_rhb001_num_empleado_asistente_a,
                fk_rhb001_num_empleado_asistente_b=:fk_rhb001_num_empleado_asistente_b,
                fk_rhb001_num_empleado_asistente_c=:fk_rhb001_num_empleado_asistente_c,
                ind_proveedores=:ind_proveedores
              WHERE
                pk_num_evaluacion=:pk_num_evaluacion"
        );

        $idEval= $datos['idEval'];

        $registroEval1->execute(array(
            'ind_objeto_evaluacion'=>$datos['objeto'],
            'ind_conclusion'=>$datos['conclusion'],
            'ind_recomendacion'=>$datos['recomendacion'],
            'fk_rhb001_num_empleado_asistente_a'=>$this->atIdEmpleado,
            'fk_rhb001_num_empleado_asistente_b'=>$datos['segundo'],
            'fk_rhb001_num_empleado_asistente_c'=>$datos['tercero'],
            'ind_proveedores'=>$datos['ind_proveedores'],
            'pk_num_evaluacion'=>$idEval
        ));


        $registroEvalDet1 = $this->_db->prepare("
          INSERT INTO
            lg_c004_evaluacion_cuantitativa
          SET
			num_pmo_poe=:num_pmo_poe,
			num_pp=:num_pp,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
			fec_anio=:fec_anio,
			fk_lgc003_num_cotizacion=:fk_lgc003_num_cotizacion,
			fk_lgb022_num_proveedor_recomendado=:fk_lgb022_num_proveedor_recomendado,
			fk_lgb011_num_evaluacion=:fk_lgb011_num_evaluacion
           ");
        $registroEvalDet2 = $this->_db->prepare("
              UPDATE
                lg_c004_evaluacion_cuantitativa
              SET
                num_pmo_poe=:num_pmo_poe,
                num_pp=:num_pp,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_lgb022_num_proveedor_recomendado=:fk_lgb022_num_proveedor_recomendado
              WHERE
                pk_num_cuantitativa=:pk_num_cuantitativa
       ");

        foreach ($datos['proveedores'] AS $key=>$value){
            foreach ($datos['pmoPoe'][$value] as $key2=>$value2) {
                if(isset($datos['idEvalCC'][$value][$key2])){
                    $registroEvalDet2->execute(array(
                        'num_pmo_poe'=>$value2,
                        'num_pp'=>$datos['pp'][$value][$key2],
                        'fk_lgb022_num_proveedor_recomendado'=>$value,
                        'pk_num_cuantitativa'=>$datos['idEvalCC'][$value][$key2]
                    ));
                } else {
                    $registroEvalDet1->execute(array(
                        'num_pmo_poe'=>$value2,
                        'num_pp'=>$datos['pp'][$value][$key2],
                        'fec_anio'=>date('Y'),
                        'fk_lgc003_num_cotizacion'=>$datos['idCotizacion'][$value][$key2],
                        'fk_lgb022_num_proveedor_recomendado'=>$value,
                        'fk_lgb011_num_evaluacion'=>$idEval
                    ));
                }
            }
        }

        $registroEval3 = $this->_db->prepare("
          UPDATE
            lg_c012_evaluacion_cualitativa
          SET
			ind_nombre_aspecto=:ind_nombre_aspecto,
			num_puntaje=:num_puntaje,
			num_puntaje_maximo_evaluado=:num_puntaje_maximo_evaluado,
			num_total_puntaje_cualitativo=:num_total_puntaje_cualitativo,
			num_total_puntaje_evaluado=:num_total_puntaje_evaluado,
			fk_lge007_num_aspecto_cualitativo=:fk_lge007_num_aspecto_cualitativo,
			fk_lgb022_num_proveedor_recomendado=:fk_lgb022_num_proveedor_recomendado,
            fec_ultima_modificacion=NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE pk_num_evaluacion_cualitativa=:pk_num_evaluacion_cualitativa
           ");
        $registroEval32 = $this->_db->prepare("
          INSERT INTO
            lg_c012_evaluacion_cualitativa
          SET
			ind_nombre_aspecto=:ind_nombre_aspecto,
			num_puntaje=:num_puntaje,
			num_puntaje_maximo_evaluado=:num_puntaje_maximo_evaluado,
			num_total_puntaje_cualitativo=:num_total_puntaje_cualitativo,
			num_total_puntaje_evaluado=:num_total_puntaje_evaluado,
			fk_lge007_num_aspecto_cualitativo=:fk_lge007_num_aspecto_cualitativo,
			fk_lgb022_num_proveedor_recomendado=:fk_lgb022_num_proveedor_recomendado,
			fk_lgb011_num_evaluacion=:fk_lgb011_num_evaluacion,
            fec_ultima_modificacion=NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario'
           ");
        foreach ($datos['asp'] AS $key=>$value){
            #$key = pk_num_aspecto
            foreach ($value AS $key2=>$value2){
                #$key2 = pk_num_proveedor
                if(!isset($datos['idCuali'][$key][$key2])){
                    $registroEval32->execute(array(
                        'ind_nombre_aspecto'=>$datos['nombreAsp'][$key],
                        'num_puntaje'=>$datos['asp'][$key][$key2],
                        'num_puntaje_maximo_evaluado'=>$datos['pm'][$key],
                        'num_total_puntaje_cualitativo'=>$datos['total'][$key2],
                        'num_total_puntaje_evaluado'=>$datos['totalPm'],
                        'fk_lge007_num_aspecto_cualitativo'=>$key,
                        'fk_lgb022_num_proveedor_recomendado'=>$key2,
                        'fk_lgb011_num_evaluacion'=>$idEval
                    ));
                } else {
                    $registroEval3->execute(array(
                        'ind_nombre_aspecto'=>$datos['nombreAsp'][$key],
                        'num_puntaje'=>$datos['asp'][$key][$key2],
                        'num_puntaje_maximo_evaluado'=>$datos['pm'][$key],
                        'num_total_puntaje_cualitativo'=>$datos['total'][$key2],
                        'num_total_puntaje_evaluado'=>$datos['totalPm'],
                        'fk_lge007_num_aspecto_cualitativo'=>$key,
                        'fk_lgb022_num_proveedor_recomendado'=>$key2,
                        'pk_num_evaluacion_cualitativa'=>$datos['idCuali'][$key][$key2]
                    ));
                }
            }
        }

        $fallaRegistroEval = $registroEvalDet2->errorInfo();

        if(!empty($fallaRegistroEval[1]) && !empty($fallaRegistroEval[2])){
            $this->_db->rollBack();
            return $fallaRegistroEval;
        }else{
            $this->_db->commit();
            return $idEval;
        }

    }

    public function metCrearInforme(
        $conclusion,$recomendacion,$asisA,
        $asisB,$asisC,$objeto,
        $asunto,$artNum,$adjsudicacion,
        $idProveedor,$idEval,
        $num)
    {
        $this->_db->beginTransaction();

        $registroInforme1 = $this->_db->prepare("
          INSERT INTO
            lg_b012_informe_recomendacion
          SET
			ind_conclusiones=:ind_conclusiones,
			ind_recomendacion=:ind_recomendacion,
			fk_rhb001_num_empleado_asistente_a=:fk_rhb001_num_empleado_asistente_a,
			fk_rhb001_num_empleado_asistente_b=:fk_rhb001_num_empleado_asistente_b,
			fk_rhb001_num_empleado_asistente_c=:fk_rhb001_num_empleado_asistente_c,
            ind_objeto_consulta=:ind_objeto_consulta,
            ind_asunto=:ind_asunto,
            num_recomendacion=:num_recomendacion,
            fk_rhb001_num_empleado_creado_por='$this->atIdEmpleado',
            fec_creacion=NOW(),
            fk_a006_num_miscelaneo_detalle_tipo_adjudicacion=:fk_a006_num_miscelaneo_detalle_tipo_adjudicacion,
            ind_articulo_numeral=:ind_articulo_numeral,
            ind_estado='PR',
			fec_anio=:fec_anio,
			fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fk_lgb011_num_evaluacion='$idEval'
			");

        $registroInforme1->execute(array(
            'ind_conclusiones'=>$conclusion,
			'ind_recomendacion'=>$recomendacion,
			'fk_rhb001_num_empleado_asistente_a'=>$asisA,
			'fk_rhb001_num_empleado_asistente_b'=>$asisB,
			'fk_rhb001_num_empleado_asistente_c'=>$asisC,
            'ind_objeto_consulta'=>$objeto,
            'ind_asunto'=>$asunto,
            'num_recomendacion'=>$num,
            'fk_a006_num_miscelaneo_detalle_tipo_adjudicacion'=>$adjsudicacion,
            'ind_articulo_numeral'=>$artNum,
            'fec_anio'=>date('Y')
        ));
        $idInforme= $this->_db->lastInsertId();

        $registroInforme2 = $this->_db->prepare("
          INSERT INTO
            lg_c006_proveedor_recomendado
          SET
            num_secuencia=:num_secuencia,
			fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fk_lgb012_num_informe_recomendacion=:fk_lgb012_num_informe_recomendacion,
            fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
			");

        $c=0;
        foreach ($idProveedor as $key => $value) {
            if($value != NULL){
                $c=$c+1;
                $registroInforme2->execute(array(
                    'num_secuencia'=>$c,
                    'fk_lgb012_num_informe_recomendacion'=>$idInforme,
                    'fk_lgb022_num_proveedor'=>$value
                ));
            }
        }

        if(!empty($fallaRegistroInfor[1]) && !empty($fallaRegistroInfor[2])){
            $this->_db->rollBack();
            return $fallaRegistroInfor;
        }else{
            $this->_db->commit();
            return $idInforme;
        }

    }

    public function metModificarInforme(
        $conclusion,$recomendacion,$asisA,
        $asisB,$asisC,$objeto,
        $asunto,$adjudicacion,
        $artNum,$idProveedor,$idInfo)
    {
        $this->_db->beginTransaction();

        $registroInforme = $this->_db->prepare("
          UPDATE
            lg_b012_informe_recomendacion
          SET
			ind_conclusiones=:ind_conclusiones,
			ind_recomendacion=:ind_recomendacion,
			fk_rhb001_num_empleado_asistente_a=:fk_rhb001_num_empleado_asistente_a,
			fk_rhb001_num_empleado_asistente_b=:fk_rhb001_num_empleado_asistente_b,
			fk_rhb001_num_empleado_asistente_c=:fk_rhb001_num_empleado_asistente_c,
            ind_objeto_consulta=:ind_objeto_consulta,
            ind_asunto=:ind_asunto,
            fk_a006_num_miscelaneo_detalle_tipo_adjudicacion=:fk_a006_num_miscelaneo_detalle_tipo_adjudicacion,
            ind_articulo_numeral=:ind_articulo_numeral,
			fec_anio=:fec_anio,
			fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          WHERE
            pk_num_informe_recomendacion=:pk_num_informe_recomendacion

			");

        $registroInforme->execute(array(
            'ind_conclusiones'=>$conclusion,
            'ind_recomendacion'=>$recomendacion,
            'fk_rhb001_num_empleado_asistente_a'=>$asisA,
            'fk_rhb001_num_empleado_asistente_b'=>$asisB,
            'fk_rhb001_num_empleado_asistente_c'=>$asisC,
            'ind_objeto_consulta'=>$objeto,
            'ind_asunto'=>$asunto,
            'fk_a006_num_miscelaneo_detalle_tipo_adjudicacion'=>$adjudicacion,
            'ind_articulo_numeral'=>$artNum,
            'fec_anio'=>date('Y'),
            'pk_num_informe_recomendacion'=>$idInfo
        ));
        $this->_db->query("
        DELETE FROM lg_c006_proveedor_recomendado WHERE fk_lgb012_num_informe_recomendacion = '$idInfo'
        ");
        $this->_db->query("
        ALTER TABLE `lg_c006_proveedor_recomendado` auto_increment = 1;
        ");

        $registroInforme2 = $this->_db->prepare("
          INSERT INTO
            lg_c006_proveedor_recomendado
          SET
            num_secuencia=:num_secuencia,
			fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fk_lgb012_num_informe_recomendacion=:fk_lgb012_num_informe_recomendacion,
            fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
			");

        $c=0;
        foreach ($idProveedor as $key => $value) {
            if($value != NULL){
                $c=$c+1;
                $registroInforme2->execute(array(
                    'num_secuencia'=>$c,
                    'fk_lgb012_num_informe_recomendacion'=>$idInfo,
                    'fk_lgb022_num_proveedor'=>$value
                ));
            }
        }

        $fallaRegistroEval = $registroInforme->errorInfo();

        if(!empty($fallaRegistroEval[1]) && !empty($fallaRegistroEval[2])){
            $this->_db->rollBack();
            return $fallaRegistroEval;
        }else{
            $this->_db->commit();
            return $idInfo;
        }

    }

    public function metMostrarProveedoresInvitados($idRec)
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              cuantitativa.*
            FROM
              lg_b012_informe_recomendacion AS recomendacion
              INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
              INNER JOIN lg_c004_evaluacion_cuantitativa AS cuantitativa ON evaluacion.pk_num_evaluacion = cuantitativa.fk_lgb011_num_evaluacion
              INNER JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.pk_num_cotizacion = cuantitativa.fk_lgc003_num_cotizacion
              INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
              INNER JOIN lg_b010_invitacion AS invitacion ON acta.pk_num_acta_inicio = invitacion.fk_lgb009_num_acta_inicio
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE
              recomendacion.pk_num_informe_recomendacion = '$idRec'
              GROUP BY cuantitativa.pk_num_cuantitativa
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetchAll();
    }

    public function metAnularActa($idActa,$estatus,$motivo){
        $this->_db->beginTransaction();
        $informePost=$this->_db->prepare("
            UPDATE
              lg_b009_acta_inicio
            SET
              ind_estado=:ind_estado,
              fk_rhb001_empleado_anulado_termino='$this->atIdEmpleado',
              fec_anulado_termino=NOW(),
              ind_motivo_anulado_termino=:ind_motivo_anulado_termino
            WHERE
              pk_num_acta_inicio='$idActa' AND
              ind_estado <> 'AN'
             ");
        $informePost->execute(array(
            'ind_estado'=> $estatus,
            'ind_motivo_anulado_termino'=>$motivo
        ));
        $fallaTansaccion = $informePost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idActa;
        }
    }

    public function metProveedores($idEval){
        $mostrarInforme= $this->_db->query("
            SELECT
              cuantitativa.*,
              cotizacion.*,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre
            FROM
              lg_c004_evaluacion_cuantitativa AS cuantitativa
               INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = cuantitativa.fk_lgb011_num_evaluacion
               INNER JOIN lg_c003_cotizacion as cotizacion ON cotizacion.pk_num_cotizacion = cuantitativa.fk_lgc003_num_cotizacion
               INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = cuantitativa.fk_lgb022_num_proveedor_recomendado
               INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE
              evaluacion.pk_num_evaluacion = '$idEval'
              GROUP BY cuantitativa.fk_lgb022_num_proveedor_recomendado
              ORDER BY cuantitativa.pk_num_cuantitativa
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetchAll();

    }

}

?>