<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'compras' . DS . 'requerimientoModelo.php';

class ordenCompraModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdOrganismo = Session::metObtener('app_organismo');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atRequerimientoModelo = new requerimientoModelo();
    }

    public function metContar($tabla,$anio,$campo,$where=false)
    {
        $ordenCompraPost = $this->_db->query("
            SELECT
              count(*) AS cantidad
            FROM
            $tabla
            WHERE
            $campo='$anio' 
            $where
              ");
        $ordenCompraPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ordenCompraPost->fetch();
    }

    public function metBuscarCargo($idEmpleado)
    {
        $ordenCompraPost = $this->_db->query("
            SELECT
                *
            FROM
              rh_b001_empleado AS empleado
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
              INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
              INNER JOIN rh_c006_tipo_cargo AS cargo ON cargo.pk_num_cargo = puesto.fk_rhc006_num_cargo
            WHERE
                empleado.pk_num_empleado = '$idEmpleado'
              ");
        $ordenCompraPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ordenCompraPost->fetch();
    }

    public function metListarOrdenCompras($estatus=false,$adj=false,$idOrden = false,$cual = false)
    {
        $where="";
        if($idOrden){
            $where.=" AND orden.pk_num_orden='$idOrden' ";
        } else {
            if($estatus){
                $where.=" AND orden.ind_estado='$estatus' ";
            }
            if($adj){
                $where.=" AND orden.fk_lgb013_num_adjudicacion<>NULL ";
            }
            if($cual){
                $where.=" AND orden.ind_tipo_orden = '$cual' ";
            }
        }

        $requerimientoPost =  $this->_db->query("
            SELECT
                orden.*,
                orden.fk_a023_num_centro_costo,
                orden.ind_estado AS estatus,
                persona.*,
                concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS provNom,
                almacen.ind_descripcion AS nomAlmacen,
                perceptivo.*,
                (SELECT SUM(oc1.num_monto_total)
                FROM lg_b019_orden oc1
                WHERE oc1.fk_lgb022_num_proveedor = orden.fk_lgb022_num_proveedor
                GROUP BY fk_lgb022_num_proveedor) AS TotalProveedor
            FROM
                lg_b019_orden AS orden
            LEFT JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
            LEFT JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            LEFT JOIN lg_b014_almacen AS almacen ON orden.fk_lgb014_num_almacen = almacen.pk_num_almacen
            LEFT JOIN lg_b020_control_perceptivo AS perceptivo ON orden.pk_num_orden = perceptivo.fk_lgb019_num_orden
            WHERE 1 $where 
            GROUP BY orden.pk_num_orden
            ORDER BY orden.pk_num_orden DESC
             ");

        $requerimientoPost->setFetchMode(PDO::FETCH_ASSOC);
        if($idOrden){
            return $requerimientoPost->fetch();
        } else {
            return $requerimientoPost->fetchAll();
        }
    }

    public function metListarOrdenComprasPendientes()
    {
        $ordenCompraPost = $this->_db->query("
            SELECT
              recomendacion.*,
              acta.pk_num_acta_inicio
            FROM
              lg_b012_informe_recomendacion AS recomendacion
              INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
              INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
              WHERE recomendacion.ind_estado <> 'AP'
              AND acta.ind_estado <> 'DS'
              ");
        $ordenCompraPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ordenCompraPost->fetchAll();
    }

    public function metListarDependencias($where=false)
{
    $dependencias = $this->_db->query("
                SELECT
                  dependencia.*
                FROM
                  a004_dependencia AS dependencia
                INNER JOIN
                  a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = dependencia.pk_num_dependencia
                LEFT JOIN
                  rh_c076_empleado_organizacion AS organizacion ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
                WHERE 
                  (
                  seguridad.fk_a018_num_seguridad_usuario = '$this->atIdUsuario' 
                  AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                  $where
                  ) 
                  
--                  OR dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
                  GROUP BY dependencia.pk_num_dependencia
                ");
    $dependencias->setFetchMode(PDO::FETCH_ASSOC);
    return $dependencias->fetchAll();
}

    public function metListarTipoServicio()
    {
        /*
        AS cb017ts
        INNER JOIN lg_e003_proveedor_servicio AS le003ps ON cb017ts.pk_num_tipo_servico = le003ps.fk_cpb017_num_tipo_servicio
        WHERE le003ps.fk_lgb022_num_proveedor='$idProveedor'
        */
        $ordenCompraPost = $this->_db->query("
            SELECT
              *
            FROM
              cp_b017_tipo_servicio
              ");
        $ordenCompraPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ordenCompraPost->fetchAll();
    }

    public function metListarFormaPago()
    {
        $ordenCompraPost = $this->_db->query("
            SELECT
              *
            FROM
              a001_organismo
              ");
        $ordenCompraPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ordenCompraPost->fetchAll();
    }

    public function metBuscarImpuesto($id)
    {
        $ordenCompraPost = $this->_db->query("
            SELECT
              impuesto.*
            FROM
              cp_b015_impuesto AS impuesto
            INNER JOIN cp_b020_servicio_impuesto AS ServImp ON impuesto.pk_num_impuesto = ServImp.fk_cpb015_num_impuesto
            INNER JOIN cp_b017_tipo_servicio AS servicio ON servicio.pk_num_tipo_servico = ServImp.fk_cpb017_num_tipo_servico
            WHERE
              servicio.pk_num_tipo_servico = '$id'
              ");
        $ordenCompraPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ordenCompraPost->fetchAll();
    }

    public function metBuscarPartidaCuenta($codigo)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              *
            FROM
              pr_b002_partida_presupuestaria AS presupuestaria 
              LEFT JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = presupuestaria.fk_cbb004_num_plan_cuenta_onco
            WHERE
              cod_partida = '$codigo'
              ");
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetch();
    }

    public function metBuscarPartidaCuentaOrden($idOrden)
    {
        $partidaCuenta = $this->_db->query("
                SELECT
                    distribucion.*,
                    presupuestaria.cod_partida,
                    presupuestaria.ind_denominacion,
                    cuenta.cod_cuenta,
                    cuenta.ind_descripcion
                FROM
                    lg_d007_distribucion_orden AS distribucion
                    INNER JOIN pr_b002_partida_presupuestaria AS presupuestaria ON distribucion.fk_prb002_num_partida_presupuestaria = presupuestaria.pk_num_partida_presupuestaria
                    INNER JOIN cb_b004_plan_cuenta AS cuenta ON distribucion.fk_cbb004_num_plan_cuenta_oncop = cuenta.pk_num_cuenta
                WHERE
                    distribucion.fk_lgb019_num_orden = '".$idOrden."'
              ");
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetchAll();
    }

    public function metBuscarCuentaOrdenDetalle($idOrden)
    {
        $partidaCuenta = $this->_db->query("
                SELECT
                    cuenta.*,
                    SUM(detalle.num_precio_unitario) AS num_precio_unitario
                FROM
                    cb_b004_plan_cuenta AS cuenta
                    INNER JOIN lg_c009_orden_detalle AS detalle ON detalle.fk_cbb004_plan_cuenta_pub_veinte = cuenta.pk_num_cuenta
                WHERE
                    detalle.fk_lgb019_num_orden = '".$idOrden."'
                GROUP BY
                    cuenta.cod_cuenta
              ");
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetchAll();
    }

    public function metBuscarDependencia($id)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              costo.pk_num_centro_costo
            FROM
              a023_centro_costo AS costo
              INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = costo.fk_a004_num_dependencia
            WHERE
              dependencia.pk_num_dependencia = '$id'
              ");
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetch();
    }

    public function metBuscarServicios($id)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              servicio.*
            FROM
              cp_b017_tipo_servicio AS servicio
              INNER JOIN lg_e003_proveedor_servicio AS proServ ON proServ.fk_cpb017_num_tipo_servicio = servicio.pk_num_tipo_servico
              INNER JOIN lg_b022_proveedor AS proveedor ON proServ.fk_lgb022_num_proveedor = proveedor.pk_num_proveedor
            WHERE
            proveedor.pk_num_proveedor ='$id'
              ");
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetchAll();
    }

    public function metNuevaOrden($datos)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "INSERT INTO
                lg_b019_orden
              SET
                fec_mes=:fec_mes,
                fec_anio=:fec_anio,
                ind_orden=:ind_orden,
                ind_tipo_orden=:ind_tipo_orden,
                fk_a004_num_dependencia=:fk_a004_num_dependencia,
                fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                fec_prometida=:fec_prometida,
                fk_rhb001_num_empleado_creado_por='$this->atIdEmpleado',
                fec_creacion=NOW(),
                fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                num_monto_bruto=:num_monto_bruto,
                num_monto_igv=:num_monto_igv,
                num_monto_total=:num_monto_total,
                num_monto_pendiente=:num_monto_pendiente,

                num_monto_afecto=:num_monto_afecto,
                num_monto_no_afecto=:num_monto_no_afecto,
                fk_a006_num_miscelaneos_forma_pago=:fk_a006_num_miscelaneos_forma_pago,
                fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
                num_plazo_entrega=:num_plazo_entrega,
                ind_entregar_en=:ind_entregar_en,
                ind_direccion_entrega=:ind_direccion_entrega,
                ind_comentario=:ind_comentario,
                ind_descripcion=:ind_descripcion,
                ind_estado=:ind_estado,

                fk_cbb004_num_plan_cuenta_oncop=:fk_cbb004_num_plan_cuenta_oncop,
                fk_cbb004_num_plan_cuenta_pub_veinte=:fk_cbb004_num_plan_cuenta_pub_veinte,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor,
                fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion
              ");
        #execute — Ejecuta una sentencia preparada

        $NuevoPost->execute(array(
                'fec_mes'=>date('m'),
                'fec_anio'=>date('Y'),
                'ind_orden'=>$datos['orden'],
                'ind_tipo_orden'=>$datos['tipoOrden'],
                'fk_a004_num_dependencia'=>$datos['dependencia'],
                'fk_a023_num_centro_costo'=>$datos['centro_costo'],
                'fec_prometida'=>date('Y-m-d'),
                'fk_cpb017_num_tipo_servicio'=>$datos['tipo_servicio'],
                'num_monto_bruto'=>$datos['mBruto'],
                'num_monto_igv'=>$datos['impuestos'],
                'num_monto_total'=>$datos['mTotal'],
                'num_monto_pendiente'=>$datos['mPendiente'],

                'num_monto_afecto'=>$datos['mAfecto'],
                'num_monto_no_afecto'=>$datos['mNoAfecto'],
                'fk_a006_num_miscelaneos_forma_pago'=>$datos['formaPago'],
                'fk_lgb014_num_almacen'=>$datos['almacen'],
                'num_plazo_entrega'=>$datos['plazo'],
                'ind_entregar_en'=>$datos['entregar'],
                'ind_direccion_entrega'=>$datos['direccion'],
                'ind_comentario'=>$datos['comentario'],
                'ind_descripcion'=>$datos['descripcion'],
                'ind_estado'=>'PR',

                'fk_cbb004_num_plan_cuenta_oncop'=>$datos['cuenta'],
                'fk_cbb004_num_plan_cuenta_pub_veinte'=>$datos['cuenta'],
                'fk_prb002_num_partida_presupuestaria'=>$datos['partida'],
                'fk_lgb022_num_proveedor'=>$datos['proveedor'],
                'fk_lgb017_num_clasificacion'=>$datos['clasificacion']

        ));

        $numOrden=$this->_db->lastInsertId();
        $NuevoPostDetalle=$this->_db->prepare(
            "INSERT INTO
                lg_c009_orden_detalle
              SET
                num_secuencia=:num_secuencia,
				fec_anio=:fec_anio,
				fec_mes=:fec_mes,
				num_cantidad=:num_cantidad,
				num_precio_unitario=:num_precio_unitario,

				num_precio_unitario_iva=:num_precio_unitario_iva,
				num_descuento_porcentaje=:num_descuento_porcentaje,
				num_descuento_fijo=:num_descuento_fijo,
				num_total=:num_total,
				num_monto_base=:num_monto_base,
				num_flag_exonerado=:num_flag_exonerado,
				fk_cbb004_plan_cuenta_oncop=:fk_cbb004_plan_cuenta_oncop,

				fk_cbb004_plan_cuenta_pub_veinte=:fk_cbb004_plan_cuenta_pub_veinte,
				fk_prb002_partida_presupuestaria=:fk_prb002_partida_presupuestaria,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
				fk_lgb004_num_unidad=:fk_lgb004_num_unidad,

				fk_lgb019_num_orden='$numOrden',
				fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
				fk_lgb002_num_item=:fk_lgb002_num_item,
				fk_lgb003_num_commodity=:fk_lgb003_num_commodity
              ");

        $contador=1;
        foreach ($datos['cant'] as $key=>$value) {
            $base = $datos['precio'][$key]*$value;
            $total = $datos['montoPU'][$key]*$value;
            $NuevoPostDetalle->execute(array(
                'num_secuencia'=>$contador,
                'fec_anio'=>date('Y'),
                'fec_mes'=>date('m'),
				'num_cantidad'=>$value,
				'num_precio_unitario'=>$datos['precio'][$key],

				'num_precio_unitario_iva'=>$datos['montoPU'][$key],
				'num_descuento_porcentaje'=>$datos['descPorc'][$key],
				'num_descuento_fijo'=>$datos['descFijo'][$key],
				'num_total'=>$total,
				'num_monto_base'=>$base,

				'num_flag_exonerado'=>$datos['exonerado'][$key],
				'fk_cbb004_plan_cuenta_oncop'=>$datos['cuentaDetalle'][$key],

				'fk_cbb004_plan_cuenta_pub_veinte'=>$datos['cuentaDetalle'][$key],
				'fk_prb002_partida_presupuestaria'=>$datos['partidaDetalle'][$key],
				'fk_lgb004_num_unidad'=>$datos['unidad'][$key],
				'fk_a023_num_centro_costo'=>$datos['centro_costo'],
                'fk_lgb002_num_item'=>$datos['item'][$key],
				'fk_lgb003_num_commodity'=>$datos['comm'][$key]
            ));

            $contador=$contador+1;
        }



        #commit — Consigna una transacción

        $fallaTansaccion1 = $NuevoPost->errorInfo();
        $fallaTansaccion2 = $NuevoPostDetalle->errorInfo();
        $fallaTansaccion3 = '';
//        $fallaTansaccion3 = $NuevoPostDistribucion->errorInfo();

        if(!empty($fallaTansaccion1[1]) && !empty($fallaTansaccion1[2])){
            $this->_db->rollBack();
            return $fallaTansaccion1;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }elseif(!empty($fallaTansaccion3[1]) && !empty($fallaTansaccion3[2])){
            $this->_db->rollBack();
            return $fallaTansaccion3;
        }else{
            $this->_db->commit();
            return $numOrden;
        }

    }

    public function metMostrarOrden($idOrden,$almacen=false)
    {
        if($almacen){
            $where = " AND almacen.num_flag_commodity!='1'";
        } else {
            $where = "";
        }
        $buscarOrden=$this->_db->query("
                  SELECT 
                    orden.*, 
                    orden.ind_estado AS estadoOS, 
                    concat_ws(
                        ' ', persona.ind_nombre1, persona.ind_apellido1
                    ) AS proveedor, 
                    persona.pk_num_persona,
                    persona.ind_documento_fiscal,
                    persona.ind_email,
                    direccion.ind_direccion, 
                    proveedor.ind_num_inscripcion_snc, 
                    detalle.ind_nombre_detalle,
                    concat_ws(
                        ' ', personaCrea.ind_nombre1, personaCrea.ind_apellido1
                    ) AS empleadoCrea, 
                    concat_ws(
                        ' ', personaRevisa.ind_nombre1, personaRevisa.ind_apellido1
                    ) AS empleadoRevisa, 
                    concat_ws(
                        ' ', personaApr.ind_nombre1, personaApr.ind_apellido1
                    ) AS empleadoApr, 
                    usuario.ind_usuario,
                    clasificacion.ind_descripcion AS clasificacion,
                    transaccion.ind_comentario AS comenTran,
                    almacen.pk_num_almacen,
                    dependencia.ind_dependencia,
                    impuesto.num_factor_porcentaje
                FROM 
                    lg_b019_orden AS orden 
                    INNER JOIN lg_b017_clasificacion AS clasificacion ON clasificacion.pk_num_clasificacion = orden.fk_lgb017_num_clasificacion
                    INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = orden.fk_lgb014_num_almacen 
                    INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor 
                    INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor 
                    LEFT JOIN a036_persona_direccion AS direccion ON direccion.fk_a003_num_persona = persona.pk_num_persona 
                    LEFT JOIN rh_b001_empleado AS empCrea ON empCrea.pk_num_empleado = orden.fk_rhb001_num_empleado_creado_por 
                    LEFT JOIN a003_persona AS personaCrea ON personaCrea.pk_num_persona = empCrea.fk_a003_num_persona 
                    LEFT JOIN rh_b001_empleado AS empRevisa ON empRevisa.pk_num_empleado = orden.fk_rhb001_num_empleado_revisado_por 
                    LEFT JOIN a003_persona AS personaRevisa ON personaRevisa.pk_num_persona = empRevisa.fk_a003_num_persona 
                    LEFT JOIN rh_b001_empleado AS empApr ON empApr.pk_num_empleado = orden.fk_rhb001_num_empleado_aprobado_por 
                    LEFT JOIN a003_persona AS personaApr ON personaApr.pk_num_persona = empApr.fk_a003_num_persona 
                    LEFT JOIN a018_seguridad_usuario AS usuario ON usuario.pk_num_seguridad_usuario = orden.fk_a018_num_seguridad_usuario
                    LEFT JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = orden.fk_a006_num_miscelaneos_forma_pago
                    LEFT JOIN a004_dependencia AS dependencia ON orden.fk_a004_num_dependencia = dependencia.pk_num_dependencia
                    LEFT JOIN cp_b017_tipo_servicio AS servicio ON servicio.pk_num_tipo_servico = orden.fk_cpb017_num_tipo_servicio
                    LEFT JOIN cp_b020_servicio_impuesto AS servicio_impuesto ON servicio.pk_num_tipo_servico = servicio_impuesto.fk_cpb017_num_tipo_servico
                    LEFT JOIN cp_b015_impuesto AS impuesto ON impuesto.pk_num_impuesto = servicio_impuesto.fk_cpb015_num_impuesto
                    
                    LEFT JOIN lg_d001_transaccion AS transaccion ON transaccion.ind_num_documento_referencia = orden.ind_orden 
                    AND orden.fec_anio = transaccion.fec_anio
                WHERE 
                    pk_num_orden = '$idOrden'
                  $where
                  GROUP BY orden.pk_num_orden
        ");
        //fk_cpb017_num_tipo_servicio
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetch();
    }

    public function metMostrarOrdenDetalle($idOrden,$almacen=false)
    {
        if($almacen){
            $where = " AND almacen.num_flag_commodity!='1'";
        } else {
            $where = "";
        }
        $buscarOrden=$this->_db->query("
            SELECT
              detalle.*,
              detalle.ind_estado AS detalleEstatus,
              item.*,
              item.ind_descripcion AS ind_descripcion_item,
              item.fk_prb002_num_partida_presupuestaria AS partidaItem,
              cuenta1.pk_num_cuenta AS cuentaItem,
              cuenta1.cod_cuenta AS codCuentaItem,
              cuenta1.ind_descripcion AS descCuentaItem,
              cuenta2.pk_num_cuenta AS cuentaItem2,
              cuenta2.cod_cuenta AS codCuentaItem2,
              cuenta2.ind_descripcion AS descCuentaItem2,
              presupuestaria1.cod_partida AS codPartidaItem,
              presupuestaria1.ind_denominacion AS descPartidaItem,
              unidad1.pk_num_unidad AS uniItem1,
              unidad1.ind_descripcion AS uniItem,
              stock1.num_stock_actual,
              stock1.pk_num_item_stock,
              
              commodity.*,
              commodity.ind_descripcion AS ind_descripcion_commodity,
              cuenta3.pk_num_cuenta AS cuentaComm,
              cuenta3.cod_cuenta AS codCuentaComm,
              cuenta3.ind_descripcion AS descCuentaComm,
              cuenta4.pk_num_cuenta AS cuentaComm2,
              cuenta4.cod_cuenta AS codCuentaComm2,
              cuenta4.ind_descripcion AS descCuentaComm2,
              presupuestaria2.pk_num_partida_presupuestaria AS partidaComm,
              presupuestaria2.cod_partida AS codPartidaComm,
              presupuestaria2.ind_denominacion AS descPartidaComm,
              unidad2.pk_num_unidad AS uniComm1,
              unidad2.ind_descripcion AS uniComm,
              stock2.num_cantidad AS num_stock_actual_comm,
              stock2.pk_num_commodity_stock,
              costo.ind_descripcion_centro_costo
            FROM
              lg_c009_orden_detalle AS detalle
              INNER JOIN lg_b019_orden AS orden ON orden.pk_num_orden = detalle.fk_lgb019_num_orden
              LEFT JOIN lg_b013_adjudicacion AS adjudicacion ON orden.fk_lgb013_num_adjudicacion = adjudicacion.pk_num_adjudicacion
              LEFT JOIN lg_c005_adjudicacion_detalle AS detalle2 ON adjudicacion.pk_num_adjudicacion = detalle2.fk_lgb013_num_adjudicacion
              LEFT JOIN lg_b002_item AS item ON item.pk_num_item = detalle.fk_lgb002_num_item
              LEFT JOIN lg_c007_item_stock AS stock1 ON stock1.pk_num_item_stock = item.fk_lgc007_num_item_stock
              LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle.fk_lgb003_num_commodity
              LEFT JOIN lg_c008_commodity_stock AS stock2 ON stock2.fk_lgb003_num_commodity = commodity.pk_num_commodity
              LEFT JOIN lg_b004_unidades AS unidad1 ON unidad1.pk_num_unidad = item.fk_lgb004_num_unidad_compra
              LEFT JOIN lg_b004_unidades AS unidad2 ON unidad2.pk_num_unidad = commodity.fk_lgb004_num_unidad
              INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = orden.fk_lgb014_num_almacen
              LEFT JOIN cb_b004_plan_cuenta AS cuenta1 ON cuenta1.pk_num_cuenta = item.fk_cbb004_num_plan_cuenta_gasto_oncop
              LEFT JOIN cb_b004_plan_cuenta AS cuenta2 ON cuenta2.pk_num_cuenta = item.fk_cbb004_num_plan_cuenta_gasto_pub_veinte
              LEFT JOIN cb_b004_plan_cuenta AS cuenta3 ON cuenta3.pk_num_cuenta = commodity.fk_cbb004_num_plan_cuenta
              LEFT JOIN cb_b004_plan_cuenta AS cuenta4 ON cuenta4.pk_num_cuenta = commodity.fk_cbb004_num_plan_cuenta_pub_veinte
              LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria1 ON presupuestaria1.pk_num_partida_presupuestaria = item.fk_prb002_num_partida_presupuestaria
              LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria2 ON presupuestaria2.pk_num_partida_presupuestaria = commodity.fk_prb002_num_partida_presupuestaria
               INNER JOIN a023_centro_costo AS costo ON detalle.fk_a023_num_centro_costo = costo.pk_num_centro_costo
            WHERE
              detalle.fk_lgb019_num_orden = '$idOrden'
              $where
              GROUP BY detalle.pk_num_orden_detalle
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetchAll();
    }

    public function metModificarOrden(
        $dependencia,$centro_costo,$clasificacion,$almacen,
        $proveedor,
        $servicio,$bruto,$igv,
        $mTotal,$pendiente,$afecto,
        $noAfecto,$formaPago,$plazo,
        $entrega,$direccion,$comentario,
        $descripcion,$cuenta,
        $partida,$adjudicacion,
        $sec,$cant,$precio,
        $montoPU,$descPorc,$descFijo,
        $exonerado,$total,$cuentaDetalle,
        $partidaDetalle,$unidad,$cCosto,
        $idOrden,$idOrdenDetalle,$item,$comm
    )
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost=$this->_db->prepare(
            "UPDATE
                lg_b019_orden
              SET
                fec_mes=:fec_mes,
                fec_anio=:fec_anio,
                fk_a004_num_dependencia=:fk_a004_num_dependencia,
                fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                fk_cpb017_num_tipo_servicio=:fk_cpb017_num_tipo_servicio,
                num_monto_bruto=:num_monto_bruto,
                num_monto_igv=:num_monto_igv,
                num_monto_total=:num_monto_total,
                num_monto_pendiente=:num_monto_pendiente,
                num_monto_afecto=:num_monto_afecto,
                num_monto_no_afecto=:num_monto_no_afecto,
                fk_a006_num_miscelaneos_forma_pago=:fk_a006_num_miscelaneos_forma_pago,
                fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
                num_plazo_entrega=:num_plazo_entrega,
                ind_entregar_en=:ind_entregar_en,
                ind_direccion_entrega=:ind_direccion_entrega,
                ind_comentario=:ind_comentario,
                ind_descripcion=:ind_descripcion,
                ind_estado=:ind_estado,
                fk_cbb004_num_plan_cuenta_oncop=:fk_cbb004_num_plan_cuenta_oncop,
                fk_cbb004_num_plan_cuenta_pub_veinte=:fk_cbb004_num_plan_cuenta_pub_veinte,
                fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_lgb013_num_adjudicacion=:fk_lgb013_num_adjudicacion,
                fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor,
                fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion
                WHERE pk_num_orden=:pk_num_orden
              ");
        #execute — Ejecuta una sentencia preparada

        $NuevoPost->execute(array(
            'fec_mes'=>date('m'),
            'fec_anio'=>date('Y'),
            'fk_a004_num_dependencia'=>$dependencia,
            'fk_a023_num_centro_costo'=>$centro_costo,
            'fk_cpb017_num_tipo_servicio'=>$servicio,
            'num_monto_bruto'=>$bruto,
            'num_monto_igv'=>$igv,
            'num_monto_total'=>$mTotal,
            'num_monto_pendiente'=>$pendiente,

            'num_monto_afecto'=>$afecto,
            'num_monto_no_afecto'=>$noAfecto,
            'fk_a006_num_miscelaneos_forma_pago'=>$formaPago,
            'fk_lgb014_num_almacen'=>$almacen,
            'num_plazo_entrega'=>$plazo,
            'ind_entregar_en'=>$entrega,
            'ind_direccion_entrega'=>$direccion,
            'ind_comentario'=>$comentario,
            'ind_descripcion'=>$descripcion,
            'ind_estado'=>'PR',

            'fk_cbb004_num_plan_cuenta_oncop'=>$cuenta,
            'fk_cbb004_num_plan_cuenta_pub_veinte'=>$cuenta,
            'fk_prb002_num_partida_presupuestaria'=>$partida,
            'fk_lgb013_num_adjudicacion'=>$adjudicacion,
            'fk_lgb022_num_proveedor'=>$proveedor,
            'fk_lgb017_num_clasificacion'=>$clasificacion,
            'pk_num_orden'=>$idOrden
        ));

        $NuevoPostDetalle2=$this->_db->prepare(
            "INSERT INTO
                lg_c009_orden_detalle
              SET
                num_secuencia=:num_secuencia,
				fec_anio=:fec_anio,
				fec_mes=:fec_mes,
				num_cantidad=:num_cantidad,
				num_precio_unitario=:num_precio_unitario,

				num_precio_unitario_iva=:num_precio_unitario_iva,
				num_descuento_porcentaje=:num_descuento_porcentaje,
				num_descuento_fijo=:num_descuento_fijo,
				num_total=:num_total,
				num_monto_base=:num_monto_base,
				num_flag_exonerado=:num_flag_exonerado,
				
				fk_cbb004_plan_cuenta_oncop=:fk_cbb004_plan_cuenta_oncop,
				fk_cbb004_plan_cuenta_pub_veinte=:fk_cbb004_plan_cuenta_pub_veinte,
				fk_prb002_partida_presupuestaria=:fk_prb002_partida_presupuestaria,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                
				fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
				fk_lgb019_num_orden='$idOrden',
				fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
				fk_lgb002_num_item=:fk_lgb002_num_item,
				fk_lgb003_num_commodity=:fk_lgb003_num_commodity
              ");

        $this->_db->query("
            DELETE FROM lg_c009_orden_detalle WHERE fk_lgb019_num_orden = '$idOrden' AND fk_lgb023_num_acta_detalle IS NULL
              ");
        $this->_db->query("
            ALTER TABLE `lg_c009_orden_detalle` auto_increment = 1;
        ");
        $contador=1;
        foreach ($cant as $key=>$value) {
            $base = $precio[$key]*$cant[$key];
            $NuevoPostDetalle2->execute(array(
                'num_secuencia'=>$contador,
                'fec_anio'=>date('Y'),
                'fec_mes'=>date('m'),
                'num_cantidad'=>$cant[$key],
                'num_precio_unitario'=>$precio[$key],

                'num_precio_unitario_iva'=>$montoPU[$key],
                'num_descuento_porcentaje'=>$descPorc[$key],
                'num_descuento_fijo'=>$descFijo[$key],
                'num_total'=>$total[$key],
                'num_monto_base'=>$base,

                'num_flag_exonerado'=>$exonerado[$key],
                'fk_cbb004_plan_cuenta_oncop'=>$cuentaDetalle[$key],

                'fk_cbb004_plan_cuenta_pub_veinte'=>$cuentaDetalle[$key],
                'fk_prb002_partida_presupuestaria'=>$partidaDetalle[$key],
                'fk_lgb004_num_unidad'=>$unidad[$key],
                'fk_a023_num_centro_costo'=>$cCosto,
                'fk_lgb002_num_item'=>$item[$key],
                'fk_lgb003_num_commodity'=>$comm[$key]
            ));
            $contador=$contador+1;
        }

        #commit — Consigna una transacción

        $fallaTansaccion1 = $NuevoPost->errorInfo();
        $fallaTansaccion2 = $NuevoPostDetalle2->errorInfo();

        if(!empty($fallaTansaccion1[1]) && !empty($fallaTansaccion1[2])){
            $this->_db->rollBack();
            return $fallaTansaccion1;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }else{
            $this->_db->commit();
            return $idOrden;
        }

    }

    public function metActualizarOrden($estatus,$idOrden,$empleado=false,$fecha=false,$rechazo = false)
    {
        $datoFecha=date('Y-m-d h:i:s');
        if($empleado) {
            $set=" $empleado='$this->atIdEmpleado',
              $fecha='$datoFecha', ";
            if($empleado=='fk_a018_num_seguridad_usuario'){
                $set=" $empleado='$this->atIdUsuario',
                $fecha='$datoFecha', ";
            }
        } else {
            $set="";
        }
        if($rechazo) {
        $set=" ind_motivo_rechazo='$rechazo', 
              $empleado='$this->atIdUsuario',
              $fecha='$datoFecha', ";
        }

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $NuevoPost=$this->_db->prepare(
            "UPDATE lg_b019_orden
              SET
              $set
              ind_estado=:ind_estado
              WHERE pk_num_orden = $idOrden
              ");
        $NuevoPost->execute(array(
            'ind_estado'=>$estatus
        ));

        #commit — Consigna una transacción

        $fallaTansaccion = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idOrden;
        }
    }

    public function metActualizarOrdenDetalle($estado,$idOrden)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $NuevoPostDetalle=$this->_db->prepare("
              UPDATE
                lg_c009_orden_detalle
              SET
                ind_estado=:ind_estado
              WHERE fk_lgb019_num_orden=:fk_lgb019_num_orden
              ");

        $NuevoPostDetalle->execute(array(
            'ind_estado'=>$estado,
            'fk_lgb019_num_orden'=>$idOrden
        ));

        #commit — Consigna una transacción

        $fallaTansaccion = $NuevoPostDetalle->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $NuevoPostDetalle;
        }
    }

    public function metBuscarPresupuestoDetalle($partida)
    {
        $anio=date('Y');
        $ordenCompraPost = $this->_db->query("
            SELECT
              pc002pd.pk_num_presupuesto_det,
              pc002pd.num_monto_ajustado,
              pc002pd.num_monto_compromiso,
              presupuestaria.cod_partida,
              presupuestaria.pk_num_partida_presupuestaria,
              presupuestaria.fk_cbb004_num_plan_cuenta_onco,
              presupuestaria.fk_cbb004_num_plan_cuenta_pub20,
              (SELECT
              SUM(pded.num_monto)
              FROM
               pr_c002_presupuesto_det pcpd
            INNER JOIN  pr_d008_estado_distribucion pded ON (pded.fk_prc002_num_presupuesto_det=pcpd.pk_num_presupuesto_det)
            WHERE  ind_tipo_distribucion='CO' AND ind_estado='AC' AND pded.fk_prc002_num_presupuesto_det=pc002pd.pk_num_presupuesto_det)
            AS compromiso_dist
            FROM
              pr_c002_presupuesto_det AS pc002pd
            INNER JOIN pr_b004_presupuesto AS presupuesto ON presupuesto.pk_num_presupuesto = pc002pd.fk_prb004_num_presupuesto
            INNER JOIN pr_b002_partida_presupuestaria AS presupuestaria ON presupuestaria.pk_num_partida_presupuestaria = pc002pd.fk_prb002_num_partida_presupuestaria
            WHERE
              presupuesto.fec_anio = '$anio'
              AND presupuestaria.pk_num_partida_presupuestaria = '$partida'
              ");
        $ordenCompraPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ordenCompraPost->fetch();
    }

    public function metBuscarPresupuestoDetallePreparado($idPartida,$idOrden)
    {
        $ordenCompraPost = $this->_db->query("
            SELECT
              detalle.num_monto_base
            FROM
              lg_c009_orden_detalle AS detalle
               INNER JOIN lg_b019_orden AS orden ON orden.pk_num_orden = detalle.fk_lgb019_num_orden
            WHERE
              detalle.fk_prb002_partida_presupuestaria = '$idPartida' 
              AND orden.ind_estado = 'PR' 
              AND orden.pk_num_orden != '$idOrden'
              ");
        $ordenCompraPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ordenCompraPost->fetch();
    }

    public function metActualizarPresupuestoDetalle($partida,$monto)
    {
        $this->_db->beginTransaction();
        $ordenCompraPost=$this->_db->prepare("
            UPDATE
              pr_c002_presupuesto_det
              SET
              num_monto_compromiso=(num_monto_compromiso + :num_monto_compromiso)
            WHERE
               fk_prb002_num_partida_presupuestaria = '$partida'
              ");

        $ordenCompraPost->execute(array(
            ':num_monto_compromiso'=>$monto
        ));

        $fallaTansaccion = $ordenCompraPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $ordenCompraPost;
        }
    }

    public function metCrearEstadoDistribucion($idPresupuestoDet,$idPartida,$idCuentaOnco,$idCuenta20,$contador,$idOrden,$mTotal)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        $NuevoPostDistribucion=$this->_db->prepare("
              INSERT INTO
                pr_d008_estado_distribucion
              SET
                fec_periodo=NOW(),
                num_monto=:num_monto,
                fk_prc002_num_presupuesto_det=:fk_prc002_num_presupuesto_det,
                fk_lgb019_num_orden=:fk_lgb019_num_orden
              ");
            $NuevoPostDistribucion->execute(array(
                'num_monto'=>$mTotal,
                'fk_prc002_num_presupuesto_det'=>$idPresupuestoDet,
                'fk_lgb019_num_orden'=>$idOrden
            ));

        $NuevoPostDistribucion2=$this->_db->prepare(
            "INSERT INTO
                lg_d007_distribucion_orden
              SET
				fec_anio=:fec_anio,
				fec_mes=:fec_mes,
                num_secuencia=:num_secuencia,
				fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
				fk_cbb004_num_plan_cuenta_oncop=:fk_cbb004_num_plan_cuenta_oncop,
				fk_cbb004_num_plan_cuenta_pub_veinte=:fk_cbb004_num_plan_cuenta_pub_veinte,
				num_monto=:num_monto,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
				fk_lgb019_num_orden='$idOrden'
              ");
        $NuevoPostDistribucion2->execute(array(
            'fec_anio'=>date('Y'),
            'fec_mes'=>date('m'),
            'num_secuencia'=>$contador,
            'fk_prb002_num_partida_presupuestaria'=>$idPartida,
            'fk_cbb004_num_plan_cuenta_oncop'=>$idCuentaOnco,
            'fk_cbb004_num_plan_cuenta_pub_veinte'=>$idCuenta20,
            'num_monto'=>$mTotal
        ));
        $fallaTansaccion = $NuevoPostDistribucion->errorInfo();
        $fallaTansaccion2 = $NuevoPostDistribucion2->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }else{
            $this->_db->commit();
            return $NuevoPostDistribucion;
        }
    }

    public function metEliminarEstadoDistribucion($idOrden)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        $NuevoPostDistribucion=$this->_db->prepare("
              DELETE FROM
                pr_d008_estado_distribucion
              WHERE
                fk_lgb019_num_orden=:fk_lgb019_num_orden
              ");
        $NuevoPostDistribucion->execute(array(
            'fk_lgb019_num_orden'=>$idOrden
        ));
        $NuevoPostDistribucion2=$this->_db->prepare("
              DELETE FROM
                lg_d007_distribucion_orden
              WHERE
                fk_lgb019_num_orden=:fk_lgb019_num_orden
              ");
        $NuevoPostDistribucion2->execute(array(
            'fk_lgb019_num_orden'=>$idOrden
        ));

        $fallaTansaccion = $NuevoPostDistribucion->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idOrden;
        }
    }

    public function metEliminarDetalle($idDetalle)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        $NuevoPostDistribucion=$this->_db->prepare("
              DELETE FROM
                lg_c009_orden_detalle
              WHERE
                pk_num_orden_detalle=:pk_num_orden_detalle
              ");
        $NuevoPostDistribucion->execute(array(
            'pk_num_orden_detalle'=>$idDetalle
        ));

        $fallaTansaccion = $NuevoPostDistribucion->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idDetalle;
        }
    }

    public function metBuscarCotizaciones($idAdj)
    {
        $buscarOrden=$this->_db->query("
            SELECT
              detalle.*,
              detalle2.*,
              proveedor.pk_num_proveedor,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              cotizacion.*
            FROM
              lg_c003_cotizacion AS cotizacion
              INNER JOIN lg_b023_acta_detalle AS detalle2 ON detalle2.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
              INNER JOIN lg_c005_adjudicacion_detalle AS detalle ON detalle.fk_lgb023_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
              INNER JOIN lg_b010_invitacion AS invitacion ON cotizacion.fk_lgb010_num_invitacion = invitacion.pk_num_invitacion
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE
              detalle.fk_lgb013_num_adjudicacion = '$idAdj'
              ORDER BY detalle2.pk_num_acta_detalle, proveedor.pk_num_proveedor ASC
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetchAll();
    }

    public function metBuscarRequerimientos($idAdj)
    {
        $buscarOrden=$this->_db->query("
            SELECT
              detalle.*,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              requerimiento.*
            FROM
              lg_c001_requerimiento_detalle AS detalle
              INNER JOIN lg_e009_acta_detalle_requerimiento_detalle AS detalle2 ON detalle.pk_num_requerimiento_detalle = detalle2.fk_lgc001_num_requerimiento_detalle
              INNER JOIN lg_b023_acta_detalle AS detalle3 ON detalle3.pk_num_acta_detalle = detalle2.fk_lgb023_num_acta_detalle
              INNER JOIN lg_c005_adjudicacion_detalle AS detalle4 ON detalle3.pk_num_acta_detalle = detalle4.fk_lgb023_num_acta_detalle
              INNER JOIN lg_b001_requerimiento AS requerimiento ON requerimiento.pk_num_requerimiento = detalle.fk_lgb001_num_requerimiento
              INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = requerimiento.fk_rhb001_num_empleado_preparado_por
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
            WHERE
              detalle4.fk_lgb013_num_adjudicacion = '$idAdj'
              ORDER BY detalle.pk_num_requerimiento_detalle ASC
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetchAll();
    }
}

?>

