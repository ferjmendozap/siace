<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'compras' . DS . 'actaInicioModelo.php';
class invitarCotizarModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atActaInicioModelo = new actaInicioModelo();
    }

    public function metListarStock($idIC=false)
    {
        if ($idIC) {
            $were = "
              (reqDet.fk_lgb002_num_item = '$idIC' OR
              reqDet.fk_lgb003_num_commodity = '$idIC') AND";
        } else {
            $were = "";
        }
        $stockItem = $this->_db->query("
              SELECT
                  reqDet.fk_lgb002_num_item AS pk_item,
                  reqDet.fk_lgb003_num_commodity AS pk_comm,
                  reqDet.*,
                  req.*,
                  acta.*,
                  cotizacion.*
              FROM lg_c001_requerimiento_detalle AS reqDet
              INNER JOIN lg_b001_requerimiento AS req ON req.pk_num_requerimiento = reqDet.fk_lgb001_num_requerimiento
              INNER JOIN lg_b009_acta_inicio AS acta ON req.pk_num_requerimiento = acta.fk_lgb001_num_requerimiento
              LEFT JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.pk_num_cotizacion = reqDet.fk_lgc003_num_cotizacion
              WHERE $were
              req.ind_estado = 'AP'
              GROUP BY reqDet.pk_num_requerimiento_detalle
              ORDER BY (req.pk_num_requerimiento)
          ");
        $stockItem->setFetchMode(PDO::FETCH_ASSOC);
        if ($idIC) {
            return $stockItem->fetch();
        } else {
            return $stockItem->fetchAll();
        }

    }
/*
    public function metMostrarRequerimientosStock()
    {
        $mostrarInvitacion = $this->_db->query("
              SELECT
                  reqDet.fk_lgb002_num_item AS pk_item,
                  reqDet.fk_lgb003_num_commodity AS pk_comm,
                  reqDet.*,
                  item.ind_descripcion AS item,
                  commodity.ind_descripcion AS commodity,
                  unidades1.ind_descripcion AS itemUni,
                  unidades2.ind_descripcion AS commUni,
                  (SELECT 
                    max(pk_num_acta_inicio) 
                  FROM lg_b009_acta_inicio AS acta 
                  INNER JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON acta_requerimiento.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
                  WHERE acta_requerimiento.fk_lgb001_num_requerimiento = req.pk_num_requerimiento 
                  AND acta.ind_estado = 'PR') AS idActa
              FROM lg_c001_requerimiento_detalle AS reqDet
              INNER JOIN lg_b001_requerimiento AS req ON req.pk_num_requerimiento = reqDet.fk_lgb001_num_requerimiento
              LEFT JOIN lg_b002_item AS item ON item.pk_num_item = reqDet.fk_lgb002_num_item
              LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = reqDet.fk_lgb003_num_commodity
              LEFT JOIN lg_c002_unidades_conversion AS unidad1 ON unidad1.pk_num_unidad_conversion = item.fk_lgc002_num_unidad_conversion
              LEFT JOIN lg_b004_unidades AS unidades1 ON unidades1.pk_num_unidad = unidad1.fk_lgb004_num_unidad
              LEFT JOIN lg_c002_unidades_conversion AS unidad2 ON unidad2.pk_num_unidad_conversion = commodity.fk_lgb004_num_unidad
              LEFT JOIN lg_b004_unidades AS unidades2 ON unidades2.pk_num_unidad = unidad2.fk_lgb004_num_unidad
              GROUP BY reqDet.pk_num_requerimiento_detalle
              ORDER BY (req.pk_num_requerimiento)
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }
*/
    public function metMostrarInvitacionRealizada($idActaDet)
    {
        $mostrarInvitacion = $this->_db->query("
              SELECT
                  invi.*,
                  invi.ind_estado AS estadoInvi,
                  detalle.*,
                  CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre,
                  item.ind_descripcion AS item,
                  cuenta1.cod_cuenta AS itemCuenta,
                  commodity.ind_descripcion AS commodity,
                  cuenta2.cod_cuenta AS commCuenta,
                  cotizacion.*,
                  invi.fk_lgb023_num_acta_detalle,
                  unidad.ind_descripcion AS unidad,
                  inicio.ind_estado AS estadoActa
              FROM lg_b010_invitacion AS invi
              INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = invi.fk_lgb023_num_acta_detalle
              INNER JOIN lg_b009_acta_inicio AS inicio ON inicio.pk_num_acta_inicio = detalle.fk_lgb009_num_acta_inicio
              INNER JOIN lg_b022_proveedor AS prove ON prove.pk_num_proveedor = invi.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS per ON per.pk_num_persona = prove.fk_a003_num_persona_proveedor
              LEFT JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.fk_lgb023_num_acta_detalle = detalle.pk_num_acta_detalle
              AND cotizacion.fk_lgb010_num_invitacion = invi.pk_num_invitacion
              LEFT JOIN lg_b002_item AS item ON item.pk_num_item = detalle.fk_lgb002_num_item
              LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle.fk_lgb003_num_commodity
              LEFT JOIN cb_b004_plan_cuenta AS cuenta1 ON item.fk_cbb004_num_plan_cuenta_gasto_oncop = cuenta1.pk_num_cuenta
              LEFT JOIN cb_b004_plan_cuenta AS cuenta2 ON commodity.fk_cbb004_num_plan_cuenta = cuenta2.pk_num_cuenta
              LEFT JOIN lg_b004_unidades AS unidad ON unidad.pk_num_unidad = detalle.fk_lgb004_num_unidad
              WHERE
                  detalle.pk_num_acta_detalle = '$idActaDet'
                  AND invi.fk_lgb023_num_acta_detalle = '$idActaDet'
              GROUP BY invi.pk_num_invitacion
              ORDER BY invi.pk_num_invitacion ASC
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metMostrarInvitacionesProveedores($idReqDet)
    {
        $mostrarInvitacion = $this->_db->query("
              SELECT
              invitacion.*,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              cotizacion.*,
              cotizacion.num_total,
              acta.*,
              item.ind_descripcion AS descripcionItem,
              commodity.ind_descripcion AS descripcionComm,
              unidades.ind_descripcion AS descripcionUnidad
              FROM lg_b010_invitacion AS invitacion
              INNER JOIN lg_c003_cotizacion AS cotizacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
              INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = invitacion.fk_lgb023_num_acta_detalle
              INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = detalle.fk_lgb009_num_acta_inicio AND (acta.ind_estado = 'PR' OR acta.ind_estado = 'CO')
              LEFT JOIN lg_b002_item AS item ON item.pk_num_item = detalle.fk_lgb002_num_item
              LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle.fk_lgb003_num_commodity
              LEFT JOIN lg_b004_unidades AS unidades ON unidades.pk_num_unidad = item.fk_lgb004_num_unidad_compra OR 
              unidades.pk_num_unidad = commodity.fk_lgb004_num_unidad
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = invitacion.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
              WHERE
                  detalle.pk_num_acta_detalle = '$idReqDet'
                  GROUP BY invitacion.pk_num_invitacion
              ORDER BY invitacion.pk_num_invitacion ASC
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metMostrarCotizacionesProveedores($idActa)
    {
        $mostrarInvitacion = $this->_db->query("
                SELECT 
                    cotizacion.*,
                    invitacion.fk_lgb022_num_proveedor
                FROM 
                    lg_c003_cotizacion AS cotizacion 
                    INNER JOIN lg_b010_invitacion AS invitacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion 
                    INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = invitacion.fk_lgb023_num_acta_detalle
                    INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = detalle.fk_lgb009_num_acta_inicio
                WHERE 
                    acta.pk_num_acta_inicio = '$idActa' 
                ORDER BY 
                    cotizacion.fk_lgb010_num_invitacion
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metMostrarRequerimientosCotizaciones($idReq)
    {
        $mostrarInvitacion = $this->_db->query("
              SELECT
              detalle.*,
              item.ind_descripcion AS descripcionItem,
              unidad.ind_descripcion AS descripcionUnidad,
              commodity.ind_descripcion AS descripcionComm
              FROM
              lg_c003_cotizacion AS cotizacion
              INNER JOIN lg_c001_requerimiento_detalle AS detalle ON cotizacion.pk_num_cotizacion = detalle.fk_lgc003_num_cotizacion
              INNER JOIN lg_b010_invitacion AS invitacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
              INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = invitacion.fk_lgb009_num_acta_inicio
              LEFT JOIN lg_b002_item AS item ON item.pk_num_item = detalle.fk_lgb002_num_item
              LEFT JOIN lg_b003_commodity AS commodity  ON commodity.pk_num_commodity = detalle.fk_lgb003_num_commodity
              LEFT JOIN lg_b004_unidades AS unidad ON unidad.pk_num_unidad = item.fk_lgb004_num_unidad_compra OR
              unidad.pk_num_unidad = commodity.fk_lgb004_num_unidad
              WHERE detalle.fk_lgb001_num_requerimiento = '$idReq'
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metCrearInvitacion($num,$validez,$dias,$flim,$condiciones,$observaciones,$idActaDet,$proveedor,$secuencia)
    {
        $this->_db->beginTransaction();
        $registroInvitacion = $this->_db->prepare("
          INSERT INTO
            lg_b010_invitacion
          SET
            ind_num_invitacion=:ind_num_invitacion,
            fec_invitacion=NOW(),
            fec_apertura=NOW(),
            fec_entrega=NULL,
            num_validez_oferta=:num_validez_oferta,
            num_dias_entrega=:num_dias_entrega,
            fec_limite=:fec_limite,
            ind_condiciones=:ind_condiciones,
            ind_observaciones=:ind_observaciones,
            ind_estado='PR',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fec_anio=:fec_anio,
            fk_lgb023_num_acta_detalle=:fk_lgb023_num_acta_detalle,
            fk_lgb022_num_proveedor=:fk_lgb022_num_proveedor
           ");

        $cant = 1;
        $cant2 = 0;
        foreach ($proveedor as $key=>$value) {
            if($value!=0){


                foreach ($idActaDet AS $key2=>$value2){
                    $cant2 = $cant2+1;
                    $registroInvitacion->execute(array(
                        'ind_num_invitacion'=>$num[$key],
                        'num_validez_oferta'=>$validez,
                        'num_dias_entrega'=>$dias,
                        'fec_limite'=>$flim,
                        'ind_condiciones'=>$condiciones,
                        'ind_observaciones'=>$observaciones,
                        'fec_anio'=>date('Y'),
                        'fk_lgb023_num_acta_detalle'=>$value2,
                        'fk_lgb022_num_proveedor'=>$value
                    ));
                }
            }
            $cant = $cant+1;
        }

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroInvitacion->errorInfo();

        if(!empty($fallaTansaccion[1]) || !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metCotizar($datos)
    {
        $this->_db->beginTransaction();

        $registroCotizacion = $this->_db->prepare("
          INSERT INTO
            lg_c003_cotizacion
          SET
            ind_num_cotizacion=:ind_num_cotizacion,
			fec_documento=NOW(),
			fec_apertura=:fec_apertura,
			fec_recepcion=:fec_recepcion,
			fec_entrega=:fec_entrega,
			num_precio_unitario=:num_precio_unitario,
			num_precio_unitario_iva=:num_precio_unitario_iva,
			num_cantidad=:num_cantidad,
			num_total=:num_total,
			num_validez_oferta=:num_validez_oferta,
			num_dias_entrega=:num_dias_entrega,
			ind_num_cotizacion_proveedor=:ind_num_cotizacion_proveedor,
			fec_cotizacion_proveedor=NOW(),
			num_flag_asignado=:num_flag_asignado,
			num_flag_exonerado=:num_flag_exonerado,
			num_flag_elegido_sistema=:num_flag_elegido_sistema,
            ind_razon_elegido=:ind_razon_elegido,
            ind_observacion=:ind_observacion,
            ind_recomendacion=:ind_recomendacion,
			num_descuento_fijo=:num_descuento_fijo,
			num_descuento_porcentaje=:num_descuento_porcentaje,
            ind_estatus='PR',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
			fec_anio=:fec_anio,
			fk_lgb023_num_acta_detalle=:fk_lgb023_num_acta_detalle,
			fk_lgb010_num_invitacion=:fk_lgb010_num_invitacion,
			fk_rhb001_num_empleado_crea='$this->atIdEmpleado',
			fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
			fk_a006_num_miscelaneo_detalle_forma_pago=:fk_a006_num_miscelaneo_detalle_forma_pago
           ");

        $actualizarCotizacion = $this->_db->prepare("
          UPDATE
            lg_c003_cotizacion
          SET
            fec_recepcion=:fec_recepcion,
            fec_entrega=:fec_entrega,
			num_precio_unitario=:num_precio_unitario,
			num_precio_unitario_iva=:num_precio_unitario_iva,
			num_cantidad=:num_cantidad,
			num_total=:num_total,
			num_dias_entrega=:num_dias_entrega,
			ind_num_cotizacion_proveedor=:ind_num_cotizacion_proveedor,
			num_flag_asignado=:num_flag_asignado,
			num_flag_exonerado=:num_flag_exonerado,
			num_flag_elegido_sistema=:num_flag_elegido_sistema,
            ind_razon_elegido=:ind_razon_elegido,
            ind_observacion=:ind_observacion,
            ind_recomendacion=:ind_recomendacion,
			num_descuento_fijo=:num_descuento_fijo,
			num_descuento_porcentaje=:num_descuento_porcentaje,
			fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
			fk_a006_num_miscelaneo_detalle_forma_pago=:fk_a006_num_miscelaneo_detalle_forma_pago,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          WHERE
            pk_num_cotizacion=:pk_num_cotizacion
           ");

        $c=1;
        while($c<=$datos['secuencia']){
            if ($datos['idCotizacion'][$c]!=0) {
                $actualizarCotizacion->execute(array(
                    'fec_recepcion'=>$datos['frecepcion'][$c],
                    'fec_entrega'=>$datos['fentrega'][$c],
                    'num_precio_unitario'=>$datos['pUni'][$c],
                    'num_precio_unitario_iva'=>$datos['pIVA'][$c],
                    'num_cantidad'=>$datos['cantidadPedida'][$c],
                    'num_total'=>$datos['total'][$c],
                    'num_dias_entrega'=>$datos['dias'][$c],
                    'ind_num_cotizacion_proveedor'=>$c,
                    'num_flag_asignado'=>$datos['asignado'][$c],
                    'num_flag_exonerado'=>$datos['exo'][$c],
                    'num_flag_elegido_sistema'=>$datos['sugerido'][$c],
                    'ind_razon_elegido'=>$datos['razon'][$c],
                    'ind_observacion'=>$datos['observacion'][$c],
                    'ind_recomendacion'=>$datos['recomendacion'][$c],
                    'num_descuento_fijo'=>$datos['dfijo'][$c],
                    'num_descuento_porcentaje'=>$datos['dpor'][$c],
                    'pk_num_cotizacion'=>$datos['idCotizacion'][$c],
                    'fk_lgb004_num_unidad'=>$datos['uniCompra'][$c],
                    'fk_a006_num_miscelaneo_detalle_forma_pago'=>$datos['formaPago'][$c]
                ));

                $idRegistro=$datos['idCotizacion'][$c];
            } else {
                $registroCotizacion->execute(array(
                    'ind_num_cotizacion'=>$datos['ind_num_cotizacion'][$c],
                    'fec_apertura'=>$datos['finvi'][$c],
                    'fec_recepcion'=>$datos['frecepcion'][$c],
                    'fec_entrega'=>$datos['fentrega'][$c],
                    'num_precio_unitario'=>$datos['pUni'][$c],
                    'num_precio_unitario_iva'=>$datos['pIVA'][$c],
                    'num_cantidad'=>$datos['cantidadPedida'][$c],
                    'num_total'=>$datos['total'][$c],
                    'num_validez_oferta'=>$datos['vofer'][$c],
                    'num_dias_entrega'=>$datos['dias'][$c],
                    'ind_num_cotizacion_proveedor'=>$c,
                    'num_flag_asignado'=>$datos['asignado'][$c],
                    'num_flag_exonerado'=>$datos['exo'][$c],
                    'num_flag_elegido_sistema'=>$datos['sugerido'][$c],
                    'ind_razon_elegido'=>$datos['razon'][$c],
                    'ind_observacion'=>$datos['observacion'][$c],
                    'ind_recomendacion'=>$datos['recomendacion'][$c],
                    'num_descuento_fijo'=>$datos['dfijo'][$c],
                    'num_descuento_porcentaje'=>$datos['dpor'][$c],
                    'fec_anio'=>date('Y'),
                    'fk_lgb023_num_acta_detalle'=>$datos['idActaDet'][$c],
                    'fk_lgb010_num_invitacion'=>$datos['invi'][$c],
                    'fk_lgb004_num_unidad'=>$datos['uniCompra'][$c],
                    'fk_a006_num_miscelaneo_detalle_forma_pago'=>$datos['formaPago'][$c]
                ));
                $idRegistro= $this->_db->lastInsertId();
            }
            $c = $c+1;
        }
        $fallaTansaccion = $registroCotizacion->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metBuscarActaDet($idActaDet)
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT
              detalle.*,
              inicio.pk_num_acta_inicio,
              unidades.ind_descripcion AS unidad,
              inicio.*,
              inicio.ind_estado AS estadoActa,
              detalle2.cod_detalle,
              item.ind_codigo_interno,
              commodity.ind_cod_commodity,
              invitacion.fec_invitacion,
              invitacion.fec_limite
              FROM
              lg_b023_acta_detalle AS detalle
              INNER JOIN lg_b009_acta_inicio AS inicio ON inicio.pk_num_acta_inicio = detalle.fk_lgb009_num_acta_inicio
              LEFT JOIN lg_b002_item AS item ON detalle.fk_lgb002_num_item = item.pk_num_item
              LEFT JOIN lg_b003_commodity AS commodity ON detalle.fk_lgb003_num_commodity = commodity.pk_num_commodity
              LEFT JOIN a006_miscelaneo_detalle AS detalle2 ON detalle2.pk_num_miscelaneo_detalle = inicio.fk_a006_num_miscelaneos_modalidad
              INNER JOIN lg_b004_unidades AS unidades ON detalle.fk_lgb004_num_unidad = unidades.pk_num_unidad
              LEFT JOIN lg_b010_invitacion AS invitacion ON invitacion.fk_lgb023_num_acta_detalle = detalle.pk_num_acta_detalle
              WHERE detalle.pk_num_acta_detalle = '$idActaDet'
              GROUP BY detalle.pk_num_acta_detalle
              ORDER BY detalle.pk_num_acta_detalle
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetch();
    }

    public function metBuscarPartidasActa($idActa)
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT
              inicio.*,
              presupuestaria.*,
              presupuestaria.ind_denominacion,
              det.num_monto_ajustado,
              det.num_monto_compromiso
              FROM
                lg_b009_acta_inicio AS inicio 
                INNER JOIN lg_b023_acta_detalle AS detalle ON inicio.pk_num_acta_inicio = detalle.fk_lgb009_num_acta_inicio
                INNER JOIN pr_b002_partida_presupuestaria AS presupuestaria ON detalle.fk_prb002_num_partida = presupuestaria.pk_num_partida_presupuestaria
                LEFT JOIN pr_c002_presupuesto_det AS det ON det.fk_prb002_num_partida_presupuestaria = presupuestaria.pk_num_partida_presupuestaria
              WHERE inicio.pk_num_acta_inicio = '$idActa'
              GROUP BY presupuestaria.cod_partida
              ORDER BY presupuestaria.cod_partida ASC
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metBuscarPartidaCuenta($codigo)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              *
            FROM
              pr_b002_partida_presupuestaria AS presupuestaria 
              INNER JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = presupuestaria.fk_cbb004_num_plan_cuenta_onco
              INNER JOIN pr_c002_presupuesto_det AS det ON det.fk_prb002_num_partida_presupuestaria = presupuestaria.pk_num_partida_presupuestaria
            WHERE
              cod_partida = '$codigo'
              ");
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetch();
    }

    public function metBuscarActaDetalle($idActaDet)
    {
        $mostrarInvitacion = $this->_db->query("
            SELECT
              *,
              detalle.ind_descripcion AS comen,
              inicio.pk_num_acta_inicio
            FROM
              lg_b023_acta_detalle AS detalle 
              INNER JOIN lg_b009_acta_inicio AS inicio ON inicio.pk_num_acta_inicio = detalle.fk_lgb009_num_acta_inicio  
              AND inicio.ind_estado = 'PR'
              INNER JOIN lg_b010_invitacion AS invitacion ON detalle.pk_num_acta_detalle = invitacion.fk_lgb023_num_acta_detalle
            WHERE 
              detalle.pk_num_acta_detalle = '$idActaDet'
              GROUP BY detalle.pk_num_acta_detalle
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetch();
    }

    public function metActualizarActa($idActa)
    {
        $this->_db->beginTransaction();
        $mostrarInvitacion = $this->_db->query("
            UPDATE
              lg_b009_acta_inicio
            SET
              num_flag_evaluacion='1',
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
            WHERE pk_num_acta_inicio='$idActa'
        ");
        $this->_db->commit();
    }

    public function metListarUnidades()
    {
        $mostrarInvitacion = $this->_db->query("
              SELECT
                *
              FROM
                lg_b004_unidades
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetchAll();
    }

    public function metObtenerMaximaInvitacion()
    {
        $mostrarInvitacion = $this->_db->query("
              SELECT
                MAX(ind_num_invitacion) AS maximo
              FROM
                lg_b010_invitacion
                WHERE fec_anio=NOW()
                GROUP BY ind_num_invitacion
                ORDER BY ind_num_invitacion DESC
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetch();
    }

    public function metObtenerMaximaCotizacion()
    {
        $mostrarInvitacion = $this->_db->query("
              SELECT
                MAX(ind_num_cotizacion) AS maximo
              FROM
                lg_c003_cotizacion
                WHERE fec_anio=NOW()
                GROUP BY ind_num_cotizacion
                ORDER BY ind_num_cotizacion DESC
        ");
        $mostrarInvitacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInvitacion->fetch();
    }
}

?>