<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'almacen' . DS . 'transaccionModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'ordenCompra' . DS . 'ordenCompraModelo.php';
class recepcionModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atTransaccionModelo = new transaccionModelo();
        $this->atOrdenCompraModelo = new ordenCompraModelo();
    }

    public function metListarOrdenesAprobadas()
    {
        $tranPost = $this->_db->query("
            SELECT
              orden.*,
              orden.ind_estado AS estatus,
              proveedor.pk_num_proveedor,
              persona.*,
              almacen.ind_descripcion AS nomAlmacen,
              detalle.ind_nombre_detalle,
              clasificacion.ind_descripcion AS clasificaionDesc
            FROM
                lg_b019_orden AS orden
            INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            INNER JOIN lg_b014_almacen AS almacen ON orden.fk_lgb014_num_almacen = almacen.pk_num_almacen
            INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = orden.fk_a006_num_miscelaneos_forma_pago
            INNER JOIN lg_b017_clasificacion AS clasificacion ON orden.fk_lgb017_num_clasificacion = clasificacion.pk_num_clasificacion
            WHERE
              orden.ind_estado='AP' AND orden.ind_tipo_orden='OC' AND almacen.num_flag_commodity=0 AND orden.num_flag_control_perceptivo = 0
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }

    public function metBuscarUnidad($idItem)
    {
        $tranPost = $this->_db->query("
            SELECT
              conversion.num_cantidad
            FROM
                lg_c002_unidades_conversion AS conversion 
            INNER JOIN lg_b002_item AS item ON item.fk_lgc002_num_unidad_conversion = conversion.pk_num_unidad_conversion
            WHERE
              item.pk_num_item = '$idItem'
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetch();
    }

    public function metMostrarOrdenDetalle($idOrden)
    {
        $tranPost = $this->_db->query("
            SELECT
              detalle2.*,
              stock.pk_num_item_stock,
              stock.num_stock_actual,
              item.ind_descripcion AS ind_descripcion_item,
              sum(detalle.num_cantidad) AS cantidad
            FROM
                lg_c010_control_perceptivo_detalle AS detalle
                INNER JOIN lg_c009_orden_detalle AS detalle2 ON detalle.fk_lgc009_num_orden_detalle = detalle2.pk_num_orden_detalle
                INNER JOIN lg_b002_item AS item ON detalle2.fk_lgb002_num_item = item.pk_num_item
                INNER JOIN lg_c007_item_stock AS stock ON item.fk_lgc007_num_item_stock = stock.pk_num_item_stock
                
                INNER JOIN lg_b020_control_perceptivo AS perceptivo ON detalle.fk_lgb020_num_control_perceptivo = perceptivo.pk_num_control_perceptivo
            WHERE
              perceptivo.fk_lgb019_num_orden = '$idOrden'
              GROUP BY detalle2.pk_num_orden_detalle
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }

    public function metActualizarStock($cant,$cantidad,$item)
    {
        $this->_db->beginTransaction();
        $NuevoPost=$this->_db->prepare("
            UPDATE
                lg_c007_item_stock
            SET
                num_stock_actual=(num_stock_actual + :num_stock_actual),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            WHERE
                pk_num_item_stock=:pk_num_item
            ");

        if ($cant!=0) {
            while($cant>0){
                #execute — Ejecuta una sentencia preparada
                $NuevoPost->execute(array(
                    'num_stock_actual'=>$cantidad[$cant],
                    'pk_num_item'=>$item[$cant]
                ));
                $cant = $cant -1;
            }
        }

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }
}

?>