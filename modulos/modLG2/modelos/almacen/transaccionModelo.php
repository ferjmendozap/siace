<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'almacenModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'tipoDocumentoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'tipoTransaccionModelo.php';
class transaccionModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atAlmacenModelo = new almacenModelo();
        $this->atTipoDocumentoModelo = new tipoDocumentoModelo();
        $this->atTipoTransaccionModelo = new tipoTransaccionModelo();
    }

    public function metListarTransacciones()
    {
        $tranPost = $this->_db->query("
            SELECT
              transaccion.*,
              tipo_transaccion.ind_descripcion AS transaccion
            FROM
              lg_d001_transaccion AS transaccion
               INNER JOIN lg_b015_tipo_transaccion AS tipo_transaccion ON tipo_transaccion.pk_num_tipo_transaccion = transaccion.fk_lgb015_num_tipo_transaccion
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }

    public function metListarTransaccionesDetalles()
    {
        $tranPost = $this->_db->query("
            SELECT
              *
            FROM
              lg_d002_transaccion_detalle AS detalle
              INNER JOIN lg_d001_transaccion AS transaccion ON transaccion.pk_num_transaccion = detalle.fk_lgd001_num_transaccion
              INNER JOIN lg_b015_tipo_transaccion AS tipoTran ON transaccion.fk_lgb015_num_tipo_transaccion = tipoTran.pk_num_tipo_transaccion
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }

    public function metBuscarPredeterminado()
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              fk_a004_num_dependencia
            FROM
            rh_c076_empleado_organizacion 
            WHERE
            fk_rhb001_num_empleado='$this->atIdEmpleado'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metListarCC($idCC = false)
    {
        $where = "";
        if($idCC){
            $where = $idCC;
        }
        $tranPost = $this->_db->query("
            SELECT
              *
            FROM
              a023_centro_costo 
              $where
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }

    public function metMostrarTransaccion($idTran){
        $mostrarTransaccion = $this->_db->query("
              SELECT
                  tran.*,
                  usu.ind_usuario,
                  transaccion.fk_a006_num_miscelaneo_detalle_tipo_documento,
                  transaccion.ind_cod_tipo_transaccion,
                  transaccion.ind_descripcion AS nom_transaccion,
                  documento.ind_descripcion AS nom_documento,
                  (SELECT 
                      orden.fec_creacion
                  FROM lg_b019_orden AS orden 
                  WHERE orden.ind_orden = tran.ind_num_documento_referencia
                  AND orden.fec_anio = tran.fec_anio AND orden.ind_tipo_orden='OC') AS fec_creacion1,
                  (SELECT 
                      requerimiento.fec_preparacion
                  FROM lg_b001_requerimiento AS requerimiento 
                  WHERE requerimiento.cod_requerimiento = tran.ind_num_documento_referencia
                  AND requerimiento.fec_anio = tran.fec_anio) AS fec_creacion2,
                  (SELECT transaccion2.num_transaccion
                  FROM
                  lg_d001_transaccion AS transaccion2 
                  WHERE (transaccion2.ind_num_documento_referencia = orden2.ind_orden OR transaccion2.ind_num_documento_referencia = requerimiento2.cod_requerimiento)
                  AND transaccion2.fk_lgb015_num_tipo_transaccion <> tran.fk_lgb015_num_tipo_transaccion
                  ORDER BY transaccion2.pk_num_transaccion DESC
                  LIMIT 0,1
                  ) AS doc_ref_ingreso,
                  detalle.ind_nombre_detalle,
                  orden2.pk_num_orden,
                  orden2.ind_orden,
                  orden2.fec_anio AS anioOrden,
                  concat_ws(' ',persona.ind_nombre1, persona.ind_nombre2, persona.ind_apellido1, persona.ind_apellido2) AS nomProveedor,
                  persona.ind_documento_fiscal,
                  telefono.ind_telefono,
                  direccion.ind_direccion,
                  concat_ws(' ',persona2.ind_nombre1, persona2.ind_nombre2, persona2.ind_apellido1, persona2.ind_apellido2) AS nomDespachadoPor,
                  puesto1.ind_descripcion_cargo AS cargoDespachadoPor,
                  concat_ws(' ',persona3.ind_nombre1, persona3.ind_nombre2, persona3.ind_apellido1, persona3.ind_apellido2) AS nomAprobadoPor,
                  puesto2.ind_descripcion_cargo AS cargoAprobadoPor,
                  dependencia.ind_dependencia AS dep_solicitante,
                  CONCAT_WS('',orden2.fk_rhb001_num_empleado_aprueba_despacho,requerimiento2.fk_rhb001_num_empleado_aprueba_despacho) AS fk_rhb001_num_empleado_aprueba_despacho
              FROM
                lg_d001_transaccion AS tran
                INNER JOIN a018_seguridad_usuario AS usu ON usu.pk_num_seguridad_usuario = tran.fk_a018_num_seguridad_usuario
                INNER JOIN lg_b015_tipo_transaccion AS transaccion ON transaccion.pk_num_tipo_transaccion = tran.fk_lgb015_num_tipo_transaccion
                INNER JOIN lg_b016_tipo_documento AS documento ON documento.pk_num_tipo_documento = tran.fk_lgb016_num_tipo_documento
                INNER JOIN a006_miscelaneo_detalle AS detalle ON transaccion.fk_a006_num_miscelaneo_detalle_tipo_documento = detalle.pk_num_miscelaneo_detalle
                LEFT JOIN lg_b019_orden AS orden2 ON tran.ind_num_documento_referencia = orden2.ind_orden AND tran.fec_anio = orden2.fec_anio
                LEFT JOIN lg_b001_requerimiento AS requerimiento2 ON tran.ind_num_documento_referencia = requerimiento2.cod_requerimiento AND tran.fec_anio = requerimiento2.fec_anio
                LEFT JOIN a004_dependencia AS dependencia ON requerimiento2.fk_a004_num_dependencia = dependencia.pk_num_dependencia
                LEFT JOIN lg_b022_proveedor AS proveedor ON orden2.fk_lgb022_num_proveedor = proveedor.pk_num_proveedor
                LEFT JOIN a003_persona AS persona ON proveedor.fk_a003_num_persona_proveedor = persona.pk_num_persona
                LEFT JOIN a007_persona_telefono AS telefono ON persona.pk_num_persona = telefono.fk_a003_num_persona
                LEFT JOIN a036_persona_direccion AS direccion ON persona.pk_num_persona = direccion.fk_a003_num_persona
                LEFT JOIN rh_b001_empleado AS empleado ON tran.fk_rhb001_num_empleado_ejecutado_por = empleado.pk_num_empleado
                LEFT JOIN a003_persona AS persona2 ON empleado.fk_a003_num_persona = persona2.pk_num_persona
                LEFT JOIN rh_c005_empleado_laboral AS laboral1 ON laboral1.fk_rhb001_num_empleado = empleado.pk_num_empleado
                LEFT JOIN rh_c063_puestos AS puesto1 ON puesto1.pk_num_puestos = laboral1.fk_rhc063_num_puestos_cargo 

                LEFT JOIN rh_b001_empleado AS empleado2 ON requerimiento2.fk_rhb001_num_empleado_aprueba_despacho = empleado2.pk_num_empleado
                LEFT JOIN a003_persona AS persona3 ON empleado2.fk_a003_num_persona = persona3.pk_num_persona
                LEFT JOIN rh_c005_empleado_laboral AS laboral2 ON laboral2.fk_rhb001_num_empleado = empleado2.pk_num_empleado
                LEFT JOIN rh_c063_puestos AS puesto2 ON puesto2.pk_num_puestos = laboral2.fk_rhc063_num_puestos_cargo 
               
              WHERE
                tran.pk_num_transaccion='$idTran'
                GROUP BY tran.pk_num_transaccion
        ");
        $mostrarTransaccion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTransaccion->fetch();
    }

    public function metBuscarStock($idIC,$cual){
        if($cual=="I"){
            $mostrarTransaccion = $this->_db->query("
              SELECT
                stock.*
              FROM
                lg_c007_item_stock AS stock
                INNER JOIN lg_b002_item AS item ON item.fk_lgc007_num_item_stock = stock.pk_num_item_stock
              WHERE
                stock.pk_num_item_stock='$idIC'
        ");
        } elseif($cual=="C"){
            $mostrarTransaccion = $this->_db->query("
              SELECT
                stock.*,
                stock.num_cantidad AS num_stock_actual
              FROM
                lg_c008_commodity_stock AS stock 
              WHERE
                stock.pk_num_commodity_stock='$idIC'
        ");
        }
        $mostrarTransaccion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTransaccion->fetch();
    }

    public function metMostrarNro($codigo){
        $mostrarTransaccion = $this->_db->query("
              SELECT
                COUNT(transaccion.num_transaccion) AS nro
              FROM
                lg_d001_transaccion AS transaccion 
              INNER JOIN lg_b015_tipo_transaccion AS tipo ON transaccion.fk_lgb015_num_tipo_transaccion = tipo.pk_num_tipo_transaccion
              INNER JOIN a006_miscelaneo_detalle AS detalle ON tipo.fk_a006_num_miscelaneo_detalle_tipo_documento = detalle.pk_num_miscelaneo_detalle
              WHERE
                fec_anio='".date('Y')."' AND detalle.cod_detalle ='$codigo'
        ");
        $mostrarTransaccion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTransaccion->fetch();
    }

    public function metMostrarDetalles($idTran){
        $mostrarTransaccion = $this->_db->query("
              SELECT
                det.*,
                item.ind_descripcion AS descripcionItem,
                item.ind_codigo_interno,
                commodity.ind_descripcion AS descripcionComm,
                unidad1.ind_descripcion AS unidadItem,
                unidad2.ind_descripcion AS unidadItem2,
                commodity.ind_descripcion AS descripcionComm,
                commodity.ind_cod_commodity,
                unidad3.ind_descripcion AS unidadComm,
                stock.pk_num_commodity_stock,
                item.fk_lgc007_num_item_stock
              FROM
                lg_d002_transaccion_detalle AS det
                INNER JOIN lg_d001_transaccion AS tran ON tran.pk_num_transaccion = det.fk_lgd001_num_transaccion
                LEFT JOIN lg_b002_item AS item ON item.pk_num_item = det.fk_lgb002_num_item
                LEFT JOIN lg_c002_unidades_conversion AS conversion ON conversion.pk_num_unidad_conversion = item.fk_lgc002_num_unidad_conversion
                LEFT JOIN lg_b004_unidades AS unidad1 ON unidad1.pk_num_unidad = det.fk_lgb004_num_unidad
                LEFT JOIN lg_b004_unidades AS unidad2 ON unidad2.pk_num_unidad = conversion.fk_lgb004_num_unidad
                LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = det.fk_lgb003_num_commodity
                LEFT JOIN lg_b004_unidades AS unidad3 ON commodity.fk_lgb004_num_unidad = unidad3.pk_num_unidad
                LEFT JOIN lg_c008_commodity_stock AS stock ON stock.fk_lgb003_num_commodity = commodity.pk_num_commodity
              WHERE
                tran.pk_num_transaccion='$idTran'
                ORDER BY (det.num_secuencia) ASC
        ");
        $mostrarTransaccion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTransaccion->fetchAll();
    }

    public function metMostrarDetallesActivos($idTran){
        $mostrarTransaccion = $this->_db->query("
              SELECT
                det.*,
                fijo.*,
                commodity.ind_cod_commodity,
                marca.ind_nombre_detalle AS marca,
                color.ind_nombre_detalle AS color
              FROM
                lg_d002_transaccion_detalle AS det
                INNER JOIN lg_d001_transaccion AS tran ON tran.pk_num_transaccion = det.fk_lgd001_num_transaccion
                INNER JOIN lg_e004_activo_fijo AS fijo ON det.pk_num_transaccion_detalle = fijo.fk_lgd002_num_transaccion_detalle
                INNER JOIN lg_b003_commodity AS commodity ON det.fk_lgb003_num_commodity = commodity.pk_num_commodity
                INNER JOIN a006_miscelaneo_detalle AS marca ON fijo.fk_a006_num_marca = marca.pk_num_miscelaneo_detalle
                INNER JOIN a006_miscelaneo_detalle AS color ON fijo.fk_a006_num_color = color.pk_num_miscelaneo_detalle
              WHERE
                tran.pk_num_transaccion='$idTran'
                ORDER BY det.num_secuencia ASC
        ");
        $mostrarTransaccion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTransaccion->fetchAll();
    }

    public function metBuscarObligacion($idOrdenDet){
        $mostrarTransaccion = $this->_db->query("
              SELECT
                obligacion.fk_cpd001_num_obligacion
              FROM
                cp_d002_documento_obligacion AS obligacion
                 INNER JOIN lg_b019_orden AS orden ON orden.pk_num_orden = obligacion.fk_lgb019_num_orden
                 INNER JOIN lg_c009_orden_detalle AS detalle ON detalle.fk_lgb019_num_orden = orden.pk_num_orden
              WHERE
                detalle.pk_num_orden_detalle = '$idOrdenDet'
        ");
        $mostrarTransaccion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTransaccion->fetch();
    }

    public function metCrearTransaccion(
        $nro,$fdoc,$ccosto,
        $docRef,$comen,$manual,
        $pendiente,$nota,$estado,
        $tipoTran,$almacen,$tipoDoc,
        $cant,$sAnt,$canti,
        $precio,$numItem,$numComm,
        $numUni,$idDetalleOrden,$idDetalleReq,
        $af=false,$valores=false)
    {

        $this->_db->beginTransaction();
        $registroTran = $this->_db->prepare("
          INSERT INTO
            lg_d001_transaccion
          SET
            num_transaccion=:num_transaccion,
            fec_documento=:fec_documento,
            fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
            ind_num_documento_referencia=:ind_num_documento_referencia,
            ind_comentario=:ind_comentario,
            num_flag_manual=:num_flag_manual,
            num_flag_pendiente=:num_flag_pendiente,
            ind_nota_entrega_factura=:ind_nota_entrega_factura,
            fk_rhb001_num_empleado_ejecutado_por='$this->atIdEmpleado',
            fec_ejecucion=NOW(),
            num_estatus=:num_estatus,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fec_mes=:fec_mes,
            fec_anio=:fec_anio,
            fk_lgb015_num_tipo_transaccion=:fk_lgb015_num_tipo_transaccion,
            fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
            fk_lgb016_num_tipo_documento=:fk_lgb016_num_tipo_documento
           ");

        $registroTran->execute(array(
            'num_transaccion'=>$nro,
            'fec_documento'=>$fdoc,
            'fk_a023_num_centro_costo'=>$ccosto,
            'ind_num_documento_referencia'=>$docRef,
            'ind_comentario'=>$comen,
            'num_flag_manual'=>$manual,
            'num_flag_pendiente'=>$pendiente,
            'ind_nota_entrega_factura'=>$nota,
            'num_estatus'=>$estado,
            'fec_mes'=>date('m'),
            'fec_anio'=>date('Y'),
            'fk_lgb015_num_tipo_transaccion'=>$tipoTran,
            'fk_lgb014_num_almacen'=>$almacen,
            'fk_lgb016_num_tipo_documento'=>$tipoDoc
        ));
        $idTran= $this->_db->lastInsertId();

        $NuevoPost=$this->_db->prepare("
            INSERT INTO
                lg_d002_transaccion_detalle
            SET
                num_secuencia=:num_secuencia,
                ind_descripcion_ic=:ind_descripcion_ic,
				num_stock_anterior=:num_stock_anterior,
				num_cantidad_transaccion=:num_cantidad_transaccion,
				num_precio_unitario=:num_precio_unitario,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_lgb003_num_commodity=:fk_lgb003_num_commodity,
				fk_lgb002_num_item=:fk_lgb002_num_item,
				fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
				fk_lgd001_num_transaccion=:fk_lgd001_num_transaccion,
				fk_lgc001_num_requerimiento_detalle=:fk_lgc001_num_requerimiento_detalle,
				fk_lgc009_num_orden_detalle=:fk_lgc009_num_orden_detalle
            ");

        $NuevoPost3=$this->_db->prepare("
            INSERT INTO
                lg_e004_activo_fijo
            SET
                fk_lgd002_num_transaccion_detalle=:fk_lgd002_num_transaccion_detalle, 
                fk_lgc009_num_orden_detalle=:fk_lgc009_num_orden_detalle, 
                fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion, 
                fec_ingreso=NOW(), 
                ind_estado='PE', 
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            ");

        $i=0;
        if(is_array($numItem)){
            $ic = $numItem;
        } elseif(is_array($numComm)){
            $ic = $numComm;
        } else {
            $ic = array_merge($numItem,$numComm);
        }
        foreach ($ic AS $key=>$value){
            $i=$i+1;
            #execute — Ejecuta una sentencia preparada
            #detalle de la transaccion
            if($canti[$key]!=0){

                $nombre1 = $this->metBuscarNombre($numItem[$key],1);
                $nombre2 = $this->metBuscarNombre($numComm[$key],2);
                if($nombre1!=null AND $nombre2==null){
                    $nombre = $nombre1;
                } elseif($nombre1==null AND $nombre2!=null){
                    $nombre = $nombre2;
                } else {
                    $nombre = array_merge($nombre1,$nombre2);
                }

                $NuevoPost->execute(array(
                    'num_secuencia'=>$i,
                    'ind_descripcion_ic'=>$nombre['ind_descripcion'],
                    'num_stock_anterior'=>$sAnt[$key],
                    'num_cantidad_transaccion'=>$canti[$key],
                    'num_precio_unitario'=>$precio[$key],
                    'fk_lgb002_num_item'=>$numItem[$key],
                    'fk_lgb003_num_commodity'=>$numComm[$key],
                    'fk_lgb004_num_unidad'=>$numUni[$key],
                    'fk_lgd001_num_transaccion'=>$idTran,
                    'fk_lgc001_num_requerimiento_detalle'=>$idDetalleReq[$key],
                    'fk_lgc009_num_orden_detalle'=>$idDetalleOrden[$key]
                ));
                $idDetalle=$this->_db->lastInsertId();
            }

            if($af==1) {
                $obligacion = $this->metBuscarObligacion($idDetalleOrden[$key]);
                #execute — Ejecuta una sentencia preparada
                $canti[$key] = number_format($canti[$key],0);
                for ($j=1;$j<=$canti[$key];$j++){
                    $NuevoPost3->execute(array(
                        'fk_lgd002_num_transaccion_detalle'=>$idDetalle,
                        'fk_lgc009_num_orden_detalle'=>$idDetalleOrden[$key],
                        'fk_cpd001_num_obligacion'=>$obligacion['fk_cpd001_num_obligacion'],
                    ));
                    $fallaTansaccion3 = $NuevoPost3->errorInfo();

                }
            } else {
                $fallaTansaccion3 = NULL;
            }
        }


        $fallaTansaccion1 = $registroTran->errorInfo();
        $fallaTansaccion2 = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion1[1]) && !empty($fallaTansaccion1[2])){
            $this->_db->rollBack();
            return $fallaTansaccion1;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }elseif(!empty($fallaTansaccion3[1]) && !empty($fallaTansaccion3[2])){
            $this->_db->rollBack();
            return $fallaTansaccion3;
        }else{
            $this->_db->commit();
            return $idTran;
        }
    }

    public function metActualizarStock($cantidad,$id,$cual)
    {
        if($cual==1) {
            #comprometer
            $sql = "UPDATE
                        lg_c007_item_stock
                    SET
                        num_stock_comprometido=(num_stock_comprometido + :num_stock_comprometido),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW()
                    WHERE
                        pk_num_item_stock=:pk_num_stock";
        } elseif ($cual==2){
            #despachar
            $sql = "UPDATE
                        lg_c007_item_stock
                    SET
                        num_stock_comprometido=(num_stock_comprometido - :num_stock_comprometido),
                        num_stock_actual=(num_stock_actual - :num_stock_comprometido),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW()
                    WHERE
                        pk_num_item_stock=:pk_num_stock";
        } elseif ($cual==3) {
            #recepcionar
            $sql = "UPDATE
                        lg_c008_commodity_stock
                    SET
                        num_cantidad=(num_cantidad + :num_stock_comprometido),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW()
                    WHERE
                        pk_num_commodity_stock=:pk_num_stock";
        } elseif ($cual==4){
            #despachar
            $sql = "UPDATE
                        lg_c008_commodity_stock
                    SET
                        num_cantidad=(num_cantidad - :num_stock_comprometido),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW()
                    WHERE
                        pk_num_commodity_stock=:pk_num_stock";
        } elseif ($cual==5){
            #recepcionar
            $sql = "UPDATE
                        lg_c007_item_stock
                    SET
                        num_stock_actual=(num_stock_actual + :num_stock_comprometido),
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW()
                    WHERE
                        pk_num_item_stock=:pk_num_stock";
        }

        $this->_db->beginTransaction();
        $NuevoPost=$this->_db->prepare($sql);

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            'num_stock_comprometido'=>$cantidad,
            'pk_num_stock'=>$id
        ));
        $fallaTansaccion1 = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion1[1]) && !empty($fallaTansaccion1[2])){
            $this->_db->rollBack();
            return $fallaTansaccion1;
        }else{
            $this->_db->commit();
            return $cual;
        }
    }

    public function metModificarTransaccion(
        $fdoc,$ccosto,
        $docRef,$comen,$manual,
        $pendiente,$nota,$tipoTran,
        $almacen,$tipoDoc,$cant,
        $sAnt,$canti,$precio,
        $numItem,$numUni,
        $idTran,$idDet,$descripcionIC)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
          UPDATE
            lg_d001_transaccion
          SET
            fec_documento=:fec_documento,
            fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
            ind_num_documento_referencia=:ind_num_documento_referencia,
            ind_comentario=:ind_comentario,
            num_flag_manual=:num_flag_manual,
            num_flag_pendiente=:num_flag_pendiente,
            ind_nota_entrega_factura=:ind_nota_entrega_factura,
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fk_lgb015_num_tipo_transaccion=:fk_lgb015_num_tipo_transaccion,
            fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
            fk_lgb016_num_tipo_documento=:fk_lgb016_num_tipo_documento
          WHERE
            pk_num_transaccion='$idTran'
            ");
        $modificarRegistro->execute(array(
            'fec_documento'=>$fdoc,
            'fk_a023_num_centro_costo'=>$ccosto,
            'ind_num_documento_referencia'=>$docRef,
            'ind_comentario'=>$comen,
            'num_flag_manual'=>$manual,
            'num_flag_pendiente'=>$pendiente,
            'ind_nota_entrega_factura'=>$nota,
            'fk_lgb015_num_tipo_transaccion'=>$tipoTran,
            'fk_lgb014_num_almacen'=>$almacen,
            'fk_lgb016_num_tipo_documento'=>$tipoDoc
        ));

        $NuevoUpdate=$this->_db->prepare("
            UPDATE
                lg_d002_transaccion_detalle
            SET
                num_secuencia=:num_secuencia,
                ind_descripcion_ic=:ind_descripcion_ic,
				num_stock_anterior=:num_stock_anterior,
				num_cantidad_transaccion=:num_cantidad_transaccion,
				num_precio_unitario=:num_precio_unitario,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
				fk_lgb002_num_item=:fk_lgb002_num_item,
				fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
				fk_lgd001_num_transaccion=:fk_lgd001_num_transaccion
			WHERE
			    pk_num_transaccion_detalle=:pk_num_transaccion_detalle
            ");
        $NuevoInsert=$this->_db->prepare("
            INSERT INTO
                lg_d002_transaccion_detalle
            SET
                num_secuencia=:num_secuencia,
                ind_descripcion_ic=:ind_descripcion_ic,
				num_stock_anterior=:num_stock_anterior,
				num_cantidad_transaccion=:num_cantidad_transaccion,
				num_precio_unitario=:num_precio_unitario,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
				fk_lgb002_num_item=:fk_lgb002_num_item,
				fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
				fk_lgd001_num_transaccion=:fk_lgd001_num_transaccion
            ");

        $i=0;
        foreach ($numItem AS $key=>$value){
            $i=$i+1;
            #execute — Ejecuta una sentencia preparada
            if($idDet[$key]!=0){
                $NuevoUpdate->execute(array(
                    'num_secuencia'=>$i,
                    'num_stock_anterior'=>$sAnt[$key],
                    'ind_descripcion_ic'=>$descripcionIC[$key],
                    'num_cantidad_transaccion'=>$canti[$key],
                    'num_precio_unitario'=>$precio[$key],
                    'fk_lgb002_num_item'=>$numItem[$key],
                    'fk_lgb004_num_unidad'=>$numUni[$key],
                    'fk_lgd001_num_transaccion'=>$idTran,
                    'pk_num_transaccion_detalle'=>$idDet[$key]
                ));
            } else {
                $NuevoInsert->execute(array(
                    'num_secuencia'=>$i,
                    'ind_descripcion_ic'=>$descripcionIC[$key],
                    'num_stock_anterior'=>$sAnt[$key],
                    'num_cantidad_transaccion'=>$canti[$key],
                    'num_precio_unitario'=>$precio[$key],
                    'fk_lgb002_num_item'=>$numItem[$key],
                    'fk_lgb004_num_unidad'=>$numUni[$key],
                    'fk_lgd001_num_transaccion'=>$idTran
                ));
            }
            $cant = $cant -1;
        }
        

        $error = $modificarRegistro->errorInfo();
        $errorInsert = $NuevoInsert->errorInfo();
        $errorUpdate = $NuevoUpdate->errorInfo();
        if (!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        } elseif (!empty($errorInsert[1]) && !empty($errorInsert[2])){
            $this->_db->rollBack();
            return $errorInsert;
        } elseif (!empty($errorUpdate[1]) && !empty($errorUpdate[2])){
            $this->_db->rollBack();
            return $errorUpdate;
        }else{
            $this->_db->commit();
            return $idTran;
        }
    }

    public function metEliminarTransaccion($idTran){
        $this->_db->beginTransaction();

        $eliminarDetalle=$this->_db->prepare("
            DELETE FROM
            lg_d002_transaccion_detalle
            WHERE
            fk_lgd001_num_transaccion=:fk_lgd001_num_transaccion
            ");
        $eliminarDetalle->execute(array(
            'fk_lgd001_num_transaccion'=>$idTran
        ));

        $eliminaridTransaccion=$this->_db->prepare("
            DELETE FROM
            lg_d001_transaccion
            WHERE
            pk_num_transaccion=:pk_num_transaccion
            ");
        $eliminaridTransaccion->execute(array(
            'pk_num_transaccion'=>$idTran
        ));

        $errorTran = $eliminaridTransaccion->errorInfo();
        $errorDet = $eliminarDetalle->errorInfo();
        if(!empty($errorDet[1]) && !empty($errorDet[2])){
            $this->_db->rollBack();
            return $errorDet;
        }elseif(!empty($errorTran[1]) && !empty($errorTran[2])){
            $this->_db->rollBack();
            return $errorTran;
        }else{
            $this->_db->commit();
            return $idTran;
        }
    }

    public function metEliminarDetalle($idDet){
        $this->_db->beginTransaction();
        $eliminaridTransaccion=$this->_db->prepare("
            DELETE FROM
            lg_d002_transaccion_detalle
            WHERE
            pk_num_transaccion_detalle=:pk_num_transaccion_detalle
            ");
        $eliminaridTransaccion->execute(array(
            'pk_num_transaccion_detalle'=>$idDet
        ));

        $error = $eliminaridTransaccion->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idDet;
        }
    }

    public function metMostrarEmpleado($idPersona){
        $persona = $this->_db->query("
            SELECT
              per.ind_nombre1,
              per.ind_nombre2,
              per.ind_apellido1,
              per.ind_apellido2,
              per.ind_documento_fiscal,
              puesto.ind_descripcion_cargo
            FROM rh_b001_empleado AS emp
            INNER JOIN a003_persona AS per ON per.pk_num_persona = emp.fk_a003_num_persona
            INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = emp.pk_num_empleado
            INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            WHERE
              emp.pk_num_empleado='$idPersona'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metBuscarPeriodo(){
        $persona = $this->_db->query("
            SELECT
              *
            FROM lg_b018_control_periodo
            WHERE
              num_flag_transaccion = '1' AND
              fec_mes = '".date('m')."' AND
              fec_anio = '".date('Y')."'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metBuscarNombre($id,$cual){
        if($cual==1){
            $query = $this->_db->query("
            SELECT
              *
            FROM lg_b002_item
            WHERE
            pk_num_item='$id'
        ");
        } elseif($cual==2){
            $query = $this->_db->query("
            SELECT
              *
            FROM lg_b003_commodity
            WHERE
            pk_num_commodity='$id'
        ");
        }
        $query->setFetchMode(PDO::FETCH_ASSOC);
        return $query->fetch();
    }

    public function metActualizarEstadoDet($idOrdenDet,$estado)
    {
        $this->_db->beginTransaction();

        $NuevoPost=$this->_db->prepare("
        UPDATE
            lg_c009_orden_detalle
        SET
            ind_estado='$estado',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_orden_detalle=:pk_num_orden_detalle
        ");
        $NuevoPost->execute(array(
            'pk_num_orden_detalle'=>$idOrdenDet
        ));
        $fallaTansaccion = $NuevoPost->errorInfo();


        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idOrdenDet;
        }
    }

    public function metBuscarTipoTransaccion($codigo,$cual=false){
        if ($cual){
            $where = "tipo.pk_num_tipo_transaccion = '$codigo'";
        } else {
            $where = "tipo.ind_cod_tipo_transaccion = '$codigo'";
        }
        $persona = $this->_db->query("
            SELECT
              pk_num_tipo_transaccion,
              detalle.cod_detalle
            FROM lg_b015_tipo_transaccion AS tipo
            INNER JOIN a006_miscelaneo_detalle AS detalle ON tipo.fk_a006_num_miscelaneo_detalle_tipo_documento = detalle.pk_num_miscelaneo_detalle
            WHERE 
              $where 
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metListarDependencias()
    {
        $dependencias = $this->_db->query("
                SELECT
                  dependencia.*
                FROM
                  a004_dependencia AS dependencia
                INNER JOIN
                  a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = dependencia.pk_num_dependencia
                LEFT JOIN
                  rh_c076_empleado_organizacion AS organizacion ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
                WHERE 
                  (
                  seguridad.fk_a018_num_seguridad_usuario = '$this->atIdUsuario' 
                  AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                  ) 
                  OR dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
                  GROUP BY dependencia.pk_num_dependencia
                ");
        $dependencias->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencias->fetchAll();
    }


}

?>