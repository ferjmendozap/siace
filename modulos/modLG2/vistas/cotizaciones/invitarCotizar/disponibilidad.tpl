<div class="form form-validate floating-label" novalidate="novalidate">
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-body">
                    <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                        <form class="form floating-label form-validation" role="form" novalidate="novalidate">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active">
                                        <a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title" style="font-weight:bold;">GENERAL</span> </a></li>
                                    <li>
                                        <a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title" style="font-weight:bold;">DETALLADA</span> </a></li>
                                </ul>
                            </div>

                            <div class="tab-content clearfix">
                                <div class="tab-pane floating-label active" id="tab1">
                                    <table class="table table-striped no-margin" id="contenidoTabla" style="display: inline-block;width: 1400px; overflow: auto;">
                                        <thead>
                                        <tr>
                                            <th width="100px">Partida</th>
                                            <th width="500px">Descripción</th>
                                            <th width="100px">Nro. Items</th>
                                            <th width="100px">Monto</th>
                                            <th width="100px">Disponible</th>
                                            <th width="100px">Resta</th>
                                        </tr>
                                        </thead>
                                        <tbody id="general">
                                        {$impuesto = 0}
                                        {$nroItemImpuesto = 0}
                                        {foreach item=p from=$partidas}
                                            {$nroItems = 0}
                                            {$monto = 0}
                                            {foreach item=det from=$actaDet}
                                                {if $det.fk_prb002_num_partida==$p.pk_num_partida_presupuestaria}
                                                    {$nroItems = $nroItems + 1}
                                                    {if $det.pk_num_cotizacion!=NULL}
                                                        {$m = $det.num_cantidad*$det.num_precio_unitario}
                                                    {else}
                                                        {$m = 0}
                                                    {/if}
                                                    {$impuesto = $impuesto+($det.num_total-$m)}
                                                    {if $det.num_total-$m!=0}
                                                        {$nroItemImpuesto = $nroItemImpuesto+1}
                                                    {/if}
                                                    {$monto = number_format($monto + $m,2,'.','')}
                                                {/if}
                                            {/foreach}
                                            {$disponible = number_format($p.num_monto_ajustado-$p.num_monto_compromiso,2,'.','')}
                                            {$resta = number_format($disponible-$monto,2,'.','')}
                                            {if $resta>0}
                                                {$color='green'}
                                            {else}
                                                {$color='red'}
                                            {/if}
                                            <tr style="font-weight:bold; background-color: {$color}">
                                                <td>{$p.cod_partida}</td>
                                                <td>{$p.ind_denominacion}</td>
                                                <td>{$nroItems}</td>
                                                <td>{$monto}</td>
                                                <td>{$disponible}</td>
                                                <td>{$resta}</td>
                                            </tr>
                                        {/foreach}

                                        {$disponible = number_format($partidaIVA.num_monto_ajustado-$partidaIVA.num_monto_compromiso,2,'.','')}
                                        {$resta = number_format($disponible-$impuesto,2,'.','')}
                                        {if $resta>0}
                                            {$color='green'}
                                        {else}
                                            {$color='red'}
                                        {/if}
                                        <tr style="font-weight:bold; background-color: {$color}">
                                            <td>{$partidaIVA.cod_partida}</td>
                                            <td>{$partidaIVA.ind_denominacion}</td>
                                            <td>{$nroItemImpuesto}</td>
                                            <td>{$impuesto}</td>
                                            <td>{$disponible}</td>
                                            <td>{$resta}</td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td width="30">
                                                <div style="background-color:red; width:25px; height:20px;"></div>
                                            </td>
                                            <td>Sin disponibilidad presupuestaria</td>
                                            <td width="30">
                                                <div style="background-color:green; width:25px; height:20px;"></div>
                                            </td>
                                            <td colspan="3">Disponibilidad presupuestaria</td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <div class="tab-pane floating-label" id="tab2">
                                    <table class="table table-striped no-margin" id="contenidoTabla" style="display: inline-block;width: 1400px; overflow: auto;">
                                        <thead>
                                        <tr>
                                            <th width="100px">Item / Commodity</th>
                                            <th width="500px">Descripción</th>
                                            <th width="100px">Partida</th>
                                            <th width="100px">Cantidad</th>
                                            <th width="100px">Precio Unit.</th>
                                            <th width="100px">Total</th>
                                            <th width="100px">Disponible</th>
                                        </tr>
                                        </thead>
                                        <tbody id="detallado">
                                        {foreach item=det from=$actaDet}
                                            {$montoAjustado = "no existe"}
                                            {$codPartida = $det.fk_prb002_num_partida}
                                            {$idItem = $det.fk_lgb002_num_item}
                                            {$idComm = $det.fk_lgb003_num_commodity}
                                            {foreach item=p from=$partidas}
                                                {if $det.fk_prb002_num_partida==$p.pk_num_partida_presupuestaria}
                                                    {$codPartida = $p.cod_partida}
                                                    {$montoAjustado = $p.num_monto_ajustado}
                                                {/if}
                                            {/foreach}
                                            {$total = number_format($det.num_cantidad_pedida*$det.num_precio_unitario,2,'.','')}
                                            {if $montoAjustado-$total>0}
                                                {$color='green'}
                                            {else}
                                                {$color='red'}
                                            {/if}
                                            <tr style="font-weight:bold; background-color: {$color}">
                                                <td>{$idItem}{$idComm}</td>
                                                <td>{$det.ind_descripcion}</td>
                                                <td>{$codPartida}</td>
                                                <td>{number_format($det.num_cantidad_pedida,2,'.','')}</td>
                                                <td>{number_format($det.num_precio_unitario,2,'.','')}</td>
                                                <td>{$total}</td>
                                                {if is_numeric($montoAjustado)}
                                                    <td>{number_format($montoAjustado,2,'.','')}</td>
                                                {else}
                                                    <td>{number_format(0,2,'.','')}</td>
                                                {/if}
                                            </tr>
                                        {/foreach}

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td width="30">
                                                <div style="background-color:red; width:25px; height:20px;"></div>
                                            </td>
                                            <td>Sin disponibilidad presupuestaria</td>
                                            <td width="30">
                                                <div style="background-color:green; width:25px; height:20px;"></div>
                                            </td>
                                            <td colspan="3">Disponibilidad presupuestaria</td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <ul class="pager wizard">
                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">

        <button type="button" class="btn btn-default ink-reaction btn-raised" data-dismiss="modal">
            <span class="md md-close"></span> Cerrar
        </button>
    </div>
</div>