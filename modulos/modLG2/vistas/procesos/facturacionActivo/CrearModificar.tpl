<form action="{$_Parametros.url}modLG/procesos/facturacionActivoCONTROL/crearFacturacionActivoMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idActivo)}{$idActivo}{/if}" name="idActivo"/>
    <input type="hidden" value="{$formDB.pk_num_orden}" id="idOrden"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12 style-primary" style="text-align: center; background-color: #0aa89e; height: 40px; font-size: 20px; vertical-align: middle; color: #fff;">
                    Datos de la Orden
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.proveedor}" disabled>
                    <label for="">Proveedor: </label>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.cod_orden}" disabled>
                    <label for="">O/C: </label>
                </div>
                <div class="col-lg-12 style-primary" style="text-align: center; background-color: #0aa89e; height: 40px; font-size: 20px; vertical-align: middle; color: #fff;">
                    Datos del Activo
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.ind_cod_commodity}-{$formDB.descripcion}" disabled>
                    <label for="">Descripción: </label>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.fec_ingreso}" disabled>
                    <label for="">Fecha de Ingreso: </label>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.ind_modelo}" disabled>
                    <label for="">Modelo: </label>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.ind_nro_serie}" disabled>
                    <label for="">Nro. Serie: </label>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.ind_cod_barra}" disabled>
                    <label for="">Cod. Barra: </label>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.ind_nro_placa}" disabled>
                    <label for="">Nro. Placa: </label>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.marca}" disabled>
                    <label for="">Marca: </label>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.ubicacion}" disabled>
                    <label for="">Ubicación: </label>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.color}" disabled>
                    <label for="">Color: </label>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" value="{$formDB.ind_descripcion_centro_costo}" disabled>
                    <label for="">Centro de Costo: </label>
                </div>
                <div class="col-lg-12 style-primary" style="text-align: center; background-color: #0aa89e; height: 40px; font-size: 20px; vertical-align: middle; color: #fff;">
                    Seleccionar Obligación
                </div>
                <div class="col-lg-6 form-group" id="idObligacionError">
                    <div class="col-lg-10 form-group">
                        <label for="">*Obligación: </label>
                        <input type="hidden" class="form-control" id="idObligacion" name="form[int][idObligacion]">
                        <input type="text" class="form-control" id="codDocumento" disabled>
                    </div>
                    <div class="col-lg-2">
                        <button class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                            type="button"
                            title="Buscar Obligación"
                            data-toggle="modal" data-target="#formModal2"
                            data-keyboard="false" data-backdrop="static"
                            titulo="Buscar Obligaciones"
                            url="{$_Parametros.url}modLG/procesos/facturacionActivoCONTROL/crearFacturacionActivoMET/obligacionesMET">
                        <i class="md md-search" style="color: #ffffff;"></i>
                    </button>
                    </div>
                </div>
                <div class="col-lg-6 form-group">
                    <label for="fechaDocumento">Fecha Documento: </label>
                    <input type="text" class="form-control" id="fechaDocumento" readonly>
                </div>
            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .modal-doby-->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
            <span class="icm icm-subtract"></span>
            Facturar
        </button>
    </div>
</form>
<script type="text/javascript">
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { idOrden: $('#idOrden').val() }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#modalAncho').css("width", "60%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    $('#idActivo'+dato['idActivo']).remove();
                    swal('Exito!','El activo fue facturado exitosamente.','success');
                    $(document.getElementById("cerrarModal")).click();
                    $(document.getElementById("ContenidoModal")).html('');
                }
            }, 'json');
        });
    });
</script>