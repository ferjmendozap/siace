<form action="{$_Parametros.url}modLG/procesos/cargaInventarioCONTROL/crearCargaMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <section class="style-default-bright">
        <div class="section-header">
            <h2 class="text-primary">Carga de Inventario Físico</h2>
        </div>
        <div class="section-body contain-lg">
            <div class="row">
                <div class="col-lg-12 contain-lg">
                    <div class="table-responsive">
                        <table id="" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>ITEM</th>
                                    <th>DESCRIPCION</th>
                                    <th><center>UND.</center></th>
                                    <th><center>COD. INTERNO</center></th>
                                    <th>STOCK REAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <input type="hidden" readonly id="slog" name="form[int][slog]">
                                    <td><input type="text" readonly id="numItem" class="form-control" name="form[int][idItem]"></td>
                                    <td><input type="text" readonly id="nombreItem" class="form-control"></td>
                                    <td><input type="text" readonly id="nomUni" class="form-control"></td>
                                    <td><input type="text" readonly id="codInt" class="form-control"></td>
                                    <td><input type="text" onfocus="$(this).val('')" class="form-control" id="sreal" name="form[int][sreal]"></td>
                                <tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="5">
                                    {if in_array('LG-01-06-02-01-F',$_Parametros.perfil)}
                                        <div class="col-sm-2">
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-primary"
                                                    descripcion="el Usuario a Cargado el Inventario Nro. "
                                                    titulo="Formato" id="formato"><i class="md md-print"></i> IMPRIMIR FORMATO
                                            </button>
                                        </div>
                                    {/if}
                                    {if in_array('LG-01-06-02-02-L',$_Parametros.perfil)}
                                        <div class="col-sm-2">
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-primary accionModal"
                                                    descripcion="el Usuario a Cargado el Inventario Nro. "
                                                    data-toggle="modal" data-target="#formModal2"
                                                    data-keyboard="false" data-backdrop="static"
                                                    url="{$_Parametros.url}modLG/procesos/cargaInventarioCONTROL/listarRealizadasMET/"
                                                    titulo="Realizadas" id="realizadas"><i class="fa fa-list"></i> LISTAR REALIZADAS
                                            </button>
                                        </div>
                                    {/if}
                                    {if in_array('LG-01-06-02-03-G',$_Parametros.perfil)}
                                        <div class="col-sm-2">
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-primary"
                                                    descripcion="el Usuario a Cargado el Inventario Nro. "
                                                    titulo="Guardar" id="cargar"><i class="glyphicon glyphicon-floppy-disk"></i> GUARDAR
                                            </button>
                                        </div>
                                    {/if}
                                    {if in_array('LG-01-06-02-04-B',$_Parametros.perfil)}
                                        <div class="col-sm-2">
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-primary accionModal"
                                                    descripcion="el Usuario a Cargado el Inventario Nro. "
                                                    titulo="Buscar Item"
                                                    data-toggle="modal" data-target="#formModal2"
                                                    data-keyboard="false" data-backdrop="static"
                                                    url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/item4/">
                                                <i class="md md-search" style="color: #ffffff;"></i>Buscar Item
                                            </button>
                                        </div>
                                    {/if}
                                </th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>
<script type="text/javascript">
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#cargar').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='error'){
                    swal("Error!", "No se puede Cargar el Inventario, datos incorrectos.", "error");
                }else if(dato['status']=='nuevo'){
                    swal("Exito!", "Se Cargó el Inventario satisfactoriamente.", "success");

                    $(document.getElementById('sreal')).val('');
                    $(document.getElementById('slog')).val('');
                    $(document.getElementById('numItem')).val('');
                    $(document.getElementById('nombreItem')).val('');
                    $(document.getElementById('nomUni')).val('');
                    $(document.getElementById('codInt')).val('');
//                    setTimeout("location.reload()", 2000);
                }
            }, 'json');
        });

        $('#formato').click(function () {
            window.open('{$_Parametros.url}modLG/procesos/cargaInventarioCONTROL/formatoMET/');
        });

    });
</script>