<form action="{$_Parametros.url}modLG/maestros/aspectosCualitativosCONTROL/crearModificarAspectoMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idAspecto)}{$idAspecto}{/if}" name="idAspecto"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group floating-label col-lg-12" id="ind_nombreError">
                    <input type="text" class="form-control" name="form[alphaNum][ind_nombre]" value="{if isset($formDB.ind_nombre)}{$formDB.ind_nombre}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="ind_nombre">
                    <label for="ind_nombre"><i class="md md-border-color"></i>Nombre</label>
                </div>
                <div class="form-group floating-label col-lg-6" id="num_puntaje_maximoError">
                    <input type="text" class="form-control puntaje" name="form[int][num_puntaje_maximo]" value="{if isset($formDB.num_puntaje_maximo)}{$formDB.num_puntaje_maximo}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="num_puntaje_maximo">
                    <label for="num_puntaje_maximo"><i class="md md-border-color"></i>Puntaje Máximo</label>
                </div>
                <div class="form-group floating-label col-lg-4" id="ind_cod_aspectoError">
                    <input type="text" class="form-control" name="form[alphaNum][ind_cod_aspecto]" value="{if isset($formDB.ind_cod_aspecto)}{$formDB.ind_cod_aspecto}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="ind_cod_aspecto" maxlength="3">
                    <label for="ind_cod_aspecto"><i class="md md-border-color"></i>Código</label>
                </div>
                <div class="form-group floating-label col-lg-2">
                    <label class="checkbox-styled">
                        <span>Estatus</span>
                        <input type="checkbox" name="form[int][num_estatus]" value="1" {if isset($ver) and $ver==1} disabled {/if} {if isset($formDB.num_estatus)} {if $formDB.num_estatus==1}checked{/if}{else}checked{/if}>
                    </label>
                </div>
                <div class="form-group col-lg-12">
                    <select class="form-control select2-list select2" name="form[int][fk_a006_num_miscelaneo_detalle]" {if isset($ver) and $ver==1} disabled {/if}>
                        {foreach item=condicion from=$tipoAspecto}
                            {if isset($formDB.fk_a006_num_miscelaneo_detalle) and $formDB.fk_a006_num_miscelaneo_detalle == $condicion.pk_num_miscelaneo_detalle}
                                <option value="{$condicion.pk_num_miscelaneo_detalle}" selected>{$condicion.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$condicion.pk_num_miscelaneo_detalle}">{$condicion.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="tipoAlmacen">Tipo de Aspecto</label>
                </div>
                <div class="form-group floating-label col-lg-12" id="usuAspectoError">
                    <input type="text" class="form-control disabled" id="usuAspecto" disabled value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}">
                    <label for="usuAspecto"><i class="md md-border-color"></i>Ultimo Usuario</label>
                </div>
                <div class="form-group floating-label col-lg-12" id="fecAspectoError">
                    <input type="text" class="form-control disabled" id="fecAspecto" disabled value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}">
                    <label for="fecAspecto"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                </div>
            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .modal-doby-->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idAspecto)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css("width", "30%");
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $(":input").inputmask();
        $(".puntaje").inputmask('99,00',{ rightAlignNumerics: true,numericInput: true });

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Aspecto fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='errorMax'){
                    var puntajeCualitativo = dato['puntajeMaximo'];
                    var puntajeCuantitativo = 100-dato['puntajeMaximo'];
                    app.metValidarError(dato,'Disculpa. El valor excede el Máximo de puntaje establecido. ' +
                            '\nCualitativo: '+puntajeCualitativo+', Cuantitativo: '+puntajeCuantitativo);
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Aspecto fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
    });
</script>