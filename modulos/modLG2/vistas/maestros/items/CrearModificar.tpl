<form action="{$_Parametros.url}modLG/maestros/itemsCONTROL/crearModificarItemsMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idItem)}{$idItem}{/if}" name="idItem"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <form class="form floating-label">
                                <div class="form-wizard-nav">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-primary"></div>
                                    </div>
                                    <ul class="nav nav-justified">
                                        <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span>  <br/><span class="title">DATOS GENERALES</span> </a></li>
                                        <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <br/> <span class="title">INFORMACION ADICIONAL</span> </a></li>
                                    </ul>
                                </div><!--end .form-wizard-nav -->
                                <div class="tab-content clearfix">
                                    <div class="tab-pane active floating-label" id="tab1">
                                        <div class="col-lg-12">
                                            <!--inicio de acordeon -->
                                            <div class="panel-group floating-label" id="accordion1">
                                                <div class="card panel">
                                                    <div class="card-head style-primary" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion1-1">
                                                        <header>Datos Generales</header>
                                                        <div class="tools">
                                                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                                        </div>
                                                    </div>
                                                    <div id="accordion1-1">
                                                        <div class="card-body">
                                                            <div class="form-group floating-label col-lg-12" id="descripcionError">
                                                                <input type="text" class="form-control" name="descripcion" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="desItem">
                                                                <label for="descripcion"><i class="md md-border-color"></i>Descripcion</label>
                                                            </div>
                                                            <div class="form-group floating-label col-lg-6" id="codigoError">
                                                                <input type="text" class="form-control" name="codigo" value="{if isset($formDB.ind_codigo_interno)}{$formDB.ind_codigo_interno}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="codigo" maxlength="15">
                                                                <label for="codigo"><i class="md md-border-color"></i>Código</label>
                                                            </div>
                                                            <div class="form-group col-lg-6" id="tipoItemError">
                                                                <select id="tipoItem" class="form-control select2-list select2" name="form[int][tipoItem]" {if isset($ver) and $ver==1} disabled {/if}>
                                                                    <option value=""></option>
                                                                    {foreach item=condicion from=$selectTipoItem}
                                                                        {if isset($formDB.fk_a006_num_miscelaneo_tipo_item) and $formDB.fk_a006_num_miscelaneo_tipo_item == $condicion.pk_num_miscelaneo_detalle}
                                                                            <option value="{$condicion.pk_num_miscelaneo_detalle}" selected>{$condicion.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$condicion.pk_num_miscelaneo_detalle}">{$condicion.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="tipoItem"><i class="md md-border-color"></i>Tipo de Item</label>
                                                            </div>
                                                            <div class="form-group col-lg-6" id="compraError">
                                                                <select id="compra" class="form-control select2-list select2" name="form[int][compra]" {if isset($ver) and $ver==1} disabled {/if}>
                                                                    <option value=""></option>
                                                                    {foreach item=comp from=$unidades}
                                                                        {if isset($formDB.fk_lgb004_num_unidad_compra) and $formDB.fk_lgb004_num_unidad_compra == $comp.pk_num_unidad}
                                                                            <option value="{$comp.pk_num_unidad}" selected>{$comp.ind_descripcion}</option>
                                                                        {else}
                                                                            <option value="{$comp.pk_num_unidad}">{$comp.ind_descripcion}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="compra"><i class="md md-border-color"></i>Unidad Compra</label>
                                                            </div>
                                                            <div class="form-group col-lg-6" id="subfError">
                                                                <select id="subf" name="form[int][subf]" {if isset($ver) and $ver==1} disabled {/if}
                                                                        class="form-control select2-list select2">
                                                                    <option value=""></option>
                                                                    {foreach item=condicion from=$selectSubFamilia}
                                                                        {if isset($formDB.fk_lgb007_num_clase_subfamilia) and $formDB.fk_lgb007_num_clase_subfamilia == $condicion.pk_num_subfamilia}
                                                                            <option value="{$condicion.pk_num_subfamilia}" selected>{$condicion.ind_descripcion}</option>
                                                                        {else}
                                                                            <option value="{$condicion.pk_num_subfamilia}">{$condicion.ind_descripcion}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="subf"><i class="md md-border-color"></i>SubFamilia</label>
                                                            </div>

                                                            <div class="form-group col-lg-6" id="despachoError">
                                                                <select id="despacho" class="form-control select2-list select2" name="form[int][despacho]" {if isset($ver) and $ver==1} disabled {/if}>
                                                                    <option value=""></option>
                                                                    {foreach item=desp from=$unidades}
                                                                        {if isset($formDB.fk_lgb004_num_unidad_despacho) and $formDB.fk_lgb004_num_unidad_despacho == $desp.pk_num_unidad}
                                                                            <option value="{$desp.pk_num_unidad}" selected>{$desp.ind_descripcion}</option>
                                                                        {else}
                                                                            <option value="{$desp.pk_num_unidad}">{$desp.ind_descripcion}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="despacho"><i class="md md-border-color"></i>Unidad de Embalaje</label>
                                                            </div>


                                                            <div class="form-group col-lg-6" id="conversionError">
                                                                <select id="conversion" class="form-control select2-list select2" name="form[int][conversion]" {if isset($ver) and $ver==1} disabled {/if}>
                                                                    <option value=""></option>
                                                                    {foreach item=uni from=$unidad}
                                                                        {if isset($formDB.fk_lgc002_num_unidad_conversion) and $formDB.fk_lgc002_num_unidad_conversion == $uni.pk_num_unidad_conversion}
                                                                            <option value="{$uni.pk_num_unidad_conversion}" selected>{$uni.num_cantidad} {$uni.ind_descripcion}</option>
                                                                        {else}
                                                                            <option value="{$uni.pk_num_unidad_conversion}">{$uni.num_cantidad} {$uni.ind_descripcion}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="conversion"><i class="md md-border-color"></i>Unidad de Conversion</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--inicio de acordeon -->
                                            <div class="panel-group floating-label" id="accordion2">
                                                <div class="card panel">
                                                    <div class="card-head style-primary" data-toggle="collapse" data-parent="#accordion1" data-target="#accordion2-1">
                                                        <header>Características</header>
                                                        <div class="tools">
                                                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                                        </div>
                                                    </div>
                                                    <div id="accordion2-1">
                                                        <div class="card-body">
                                                            <div class="col-lg-6">
                                                                <div class="form-group floating-label col-lg-12">
                                                                    <label class="checkbox-styled">
                                                                        <input type="checkbox" name="form[int][lotes]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_lotes) and $formDB.num_flag_lotes==1}checked{/if}>
                                                                        <span>Se maneja por lotes</span>
                                                                    </label>
                                                                </div>
                                                                <div class="form-group floating-label col-lg-12">
                                                                    <label class="checkbox-styled">
                                                                        <input type="checkbox" name="form[int][kit]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_kit) and $formDB.num_flag_kit==1}checked{/if}>
                                                                        <span>Considerado como KIT</span>
                                                                    </label>
                                                                </div>
                                                                <div class="form-group floating-label col-lg-12">
                                                                    <label class="checkbox-styled">
                                                                        <input type="checkbox" name="form[int][estado]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_estatus)} {if $formDB.num_estatus==1}checked{/if}{else}checked{/if}>
                                                                        <span>Estatus</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="form-group floating-label col-lg-12">
                                                                    <label class="checkbox-styled">
                                                                        <input type="checkbox" name="form[int][impuesto]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_impuesto_venta) and $formDB.num_flag_impuesto_venta==1}checked{/if}>
                                                                        <span>Afecto Imp. Ventas </span>
                                                                    </label>
                                                                </div>
                                                                <div class="form-group floating-label col-lg-12">
                                                                    <label class="checkbox-styled">
                                                                        <input type="checkbox" name="form[int][auto]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_auto) and $formDB.num_flag_auto==1}checked{/if}>
                                                                        <span>Auto-Requisicionamiento </span>
                                                                    </label>
                                                                </div>
                                                                <div class="form-group floating-label col-lg-12">
                                                                    <label class="checkbox-styled">
                                                                        <input type="checkbox" name="form[int][disponible]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_disponible) and $formDB.num_flag_disponible==1}checked{/if}>
                                                                        <span>Disponible para Ventas</span>
                                                                    </label>
                                                                </div>
                                                                <div class="form-group floating-label col-lg-12">
                                                                    <label class="checkbox-styled">
                                                                        <input type="checkbox" name="form[int][presupuesto]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_verificado_presupuesto) and $formDB.num_flag_verificado_presupuesto==1}checked{/if}>
                                                                        <span>Verificado por Presupuesto</span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-lg-6" id="usuItemError">
                                                                <input type="text" class="form-control disabled" id="usuItem" disabled value="{if isset($formDB.usuario)}{$formDB.usuario}{/if}">
                                                                <label for="usuItem"><i class="md md-border-color"></i>Ultimo Usuario</label>
                                                            </div>
                                                            <div class="form-group col-lg-6" id="fecItemError">
                                                                <input type="text" class="form-control disabled" id="fecItem" disabled value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}">
                                                                <label for="fecItem"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane floating-label" id="tab2">
                                        <div class="col-lg-12">
                                            <!--inicio de acordeon -->
                                            <div class="panel-group floating-label" id="accordion3">
                                                <div class="card panel">
                                                    <div class="card-head style-primary" data-toggle="collapse" data-parent="#accordion4" data-target="#accordion3-1">
                                                        <header>Características</header>
                                                        <div class="tools">
                                                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                                        </div>
                                                    </div>
                                                    <div id="accordion3-1">
                                                        <div class="card-body">
                                                            <div class="col-lg-12">
                                                                <div class="form-group floating-label col-lg-4" id="minError">
                                                                    <input type="text" class="form-control" name="form[int][min]" id="min" {if isset($ver) and $ver==1} disabled {/if} value="{if isset($formDB.num_stock_minimo)}{$formDB.num_stock_minimo}{/if}">
                                                                    <label for="min"><i class="md md-border-color"></i>Stock Minimo</label>
                                                                </div>
                                                                <div class="form-group floating-label col-lg-4" id="maxError">
                                                                    <input type="text" class="form-control" name="form[int][max]" id="max" {if isset($ver) and $ver==1} disabled {/if} value="{if isset($formDB.num_stock_maximo)}{$formDB.num_stock_maximo}{/if}">
                                                                    <label for="max"><i class="md md-border-color"></i>Stock Maximo</label>
                                                                </div>
                                                                <div class="form-group floating-label col-lg-4" id="reordenError">
                                                                    <input type="text" class="form-control" name="form[int][reorden]" id="max" {if isset($ver) and $ver==1} disabled {/if} value="{if isset($formDB.num_punto_reorden)}{$formDB.num_punto_reorden}{/if}">
                                                                    <label for="reorden"><i class="md md-border-color"></i>Punto Reorden</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group floating-label col-lg-4">
                                                                </div>
                                                                <div class="form-group floating-label col-lg-4">
                                                                    <input type="text" class="form-control" disabled value="{if isset($formDB.num_stock_actual)}{$formDB.num_stock_actual}{/if}">
                                                                    <label><i class="md md-border-color"></i>Stock Actual</label>
                                                                </div>
                                                                <div class="form-group floating-label col-lg-4">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--inicio de acordeon -->
                                            <div class="panel-group floating-label" id="accordion4">
                                                <div class="card panel">
                                                    <div class="card-head style-primary" data-toggle="collapse" data-parent="#accordion3" data-target="#accordion4-1">
                                                        <header>Información Contable</header>
                                                        <div class="tools">
                                                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                                        </div>
                                                    </div>
                                                    <div id="accordion4-1">
                                                        <div class="card-body">
                                                            <div class="col-lg-12">
                                                                <div class="form-group col-lg-12" id="partidaError">
                                                                    <label for="partida"><i class="md md-border-color"></i>Partida Presupuestaria</label>
                                                                    <div class="col-lg-11">
                                                                        <input type="hidden" name="form[int][partida]" id="idPartida" value="{if isset($formDB.fk_prb002_num_partida_presupuestaria)}{$formDB.fk_prb002_num_partida_presupuestaria}{/if}">
                                                                        <input type="text" disabled class="form-control" id="nombrePartida" value="{if isset($formDB.nombrePartida)}{$formDB.nombrePartida}{/if}">
                                                                    </div>
                                                                    <div class="col-lg-1">
                                                                        <button {if isset($ver) and $ver==1} disabled {/if}
                                                                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                                type="button"
                                                                                title="Buscar Partida"
                                                                                data-toggle="modal" data-target="#formModal2"
                                                                                data-keyboard="false" data-backdrop="static"
                                                                                titulo="Buscar Partida"
                                                                                url="{$_Parametros.url}modLG/maestros/commodityCONTROL/partidaCuentaMET">
                                                                            <i class="md md-search" style="color: #ffffff;"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-lg-12" id="gastoOError">
                                                                    <label for="gastoO"><i class="md md-border-color"></i>Gasto Oncop</label>
                                                                    <input type="hidden" name="form[int][gastoO]" id="idCuenta1" value="{if isset($formDB.fk_cbb004_num_plan_cuenta_gasto_oncop)}{$formDB.fk_cbb004_num_plan_cuenta_gasto_oncop}{/if}">
                                                                    <input type="text" disabled class="form-control" id="nombreCuenta1" value="{if isset($formDB.nombreCuenta3)}{$formDB.nombreCuenta3}{/if}">
                                                                </div>
                                                                <div class="form-group col-lg-12" id="gasto2Error">
                                                                    <label for="gasto2"><i class="md md-border-color"></i>Gasto Pub. 20</label>
                                                                    <input type="hidden" name="form[int][gasto2]" id="idCuenta2" value="{if isset($formDB.fk_cbb004_num_plan_cuenta_gasto_pub_veinte)}{$formDB.fk_cbb004_num_plan_cuenta_gasto_pub_veinte}{/if}">
                                                                    <input type="text" disabled class="form-control" id="nombreCuenta2" value="{if isset($formDB.nombreCuenta4)}{$formDB.nombreCuenta4}{/if}">
                                                                </div>
                                                                <div class="form-group col-lg-12" id="inventarioOError">
                                                                    <label for="inventarioO"><i class="md md-border-color"></i>Inventario Oncop</label>
                                                                    <div class="col-lg-11">
                                                                        <input type="hidden" name="form[int][inventarioO]" id="idC1" value="{if isset($formDB.fk_cbb004_num_plan_cuenta_inventario_oncop)}{$formDB.fk_cbb004_num_plan_cuenta_inventario_oncop}{/if}">
                                                                        <input type="text" disabled class="form-control" id="nombreC1" value="{if isset($formDB.nombreCuenta1)}{$formDB.nombreCuenta1}{/if}">
                                                                    </div>
                                                                    <div class="col-lg-1">
                                                                        <button {if isset($ver) and $ver==1} disabled {/if}
                                                                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                                type="button"
                                                                                title="Buscar Cuenta"
                                                                                data-toggle="modal" data-target="#formModal2"
                                                                                data-keyboard="false" data-backdrop="static"
                                                                                titulo="Buscar Cuenta"
                                                                                url="{$_Parametros.url}modLG/maestros/itemsCONTROL/cuentaMET/1">
                                                                            <i class="md md-search" style="color: #ffffff;"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-lg-12" id="inventario2Error">
                                                                    <label for="inventario2"><i class="md md-border-color"></i>Inventario Pub. 20</label>
                                                                    <div class="col-lg-11">
                                                                        <input type="hidden" name="form[int][inventario2]" id="idC2" value="{if isset($formDB.fk_cbb004_num_plan_cuenta_inventario_pub_veinte)}{$formDB.fk_cbb004_num_plan_cuenta_inventario_pub_veinte}{/if}">
                                                                        <input type="text" disabled class="form-control" id="nombreC2" value="{if isset($formDB.nombreCuenta2)}{$formDB.nombreCuenta2}{/if}">
                                                                    </div>
                                                                    <div class="col-lg-1">
                                                                        <button {if isset($ver) and $ver==1} disabled {/if}
                                                                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                                type="button"
                                                                                title="Buscar Cuenta"
                                                                                data-toggle="modal" data-target="#formModal2"
                                                                                data-keyboard="false" data-backdrop="static"
                                                                                titulo="Buscar Cuenta"
                                                                                url="{$_Parametros.url}modLG/maestros/itemsCONTROL/cuentaMET/1">
                                                                            <i class="md md-search" style="color: #ffffff;"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ul class="pager wizard">
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                </ul>
                            </form>
                        </div>
                    </div>
                    {foreach item=alm from=$almacen}
                        {if $alm.cod_almacen=="ALCEA"}
                            <input type="hidden" name="form[int][almacen]" value="{$alm.pk_num_almacen}">
                        {/if}
                    {/foreach}
                </div>
            </div>
        </div>
        <!--end .col -->
    <!--end .row -->
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idItem)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $("#formAjax").submit(function(){
        return false;
    });
    $('#modalAncho').css("width", "60%");
    $('.select2').select2({ allowClear: true });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#fpre').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#modalAncho').css("width", "60%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Item fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='errorStock'){
                    app.metValidarError(dato,'Disculpa. hay cantidades erroneas en los campos de stock');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Item fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
    });
</script>