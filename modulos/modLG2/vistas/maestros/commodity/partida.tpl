<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Partidas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Denominación</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr>
                                <input type="hidden" class="persona"
                                       nombrePartida="{$i.cod_partida} {$i.ind_denominacion}"
                                       idPartida="{$i.pk_num_partida_presupuestaria}"
                                       nombreCuenta1="{$i.codCuenta1} {$i.cuenta1}"
                                       idCuenta1="{$i.fk_cbb004_num_plan_cuenta_onco}"
                                       nombreCuenta2="{$i.codCuenta2} {$i.cuenta2}"
                                       idCuenta2="{$i.fk_cbb004_num_plan_cuenta_pub20}"
                                       nombreCuenta3="{$i.codCuenta3} {$i.cuenta3}"
                                       idCuenta3="{$i.fk_cbb004_num_plan_cuenta_gastopub20}">
                                <td>{$i.cod_partida}</td>
                                <td>{$i.ind_denominacion}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2').on('click', 'tbody tr', function () {
//        $('#datatable2 tbody tr').click(function() {
            var input = $(this).find('input');

            $(document.getElementById('idPartida')).val(input.attr('idPartida'));
            $(document.getElementById('nombrePartida')).val(input.attr('nombrePartida'));

            $(document.getElementById('idCuenta1')).val(input.attr('idCuenta1'));
            $(document.getElementById('nombreCuenta1')).val(input.attr('nombreCuenta1'));

            $(document.getElementById('idCuenta2')).val(input.attr('idCuenta2'));
            $(document.getElementById('nombreCuenta2')).val(input.attr('nombreCuenta2'));

            $(document.getElementById('idCuenta3')).val(input.attr('idCuenta3'));
            $(document.getElementById('nombreCuenta3')).val(input.attr('nombreCuenta3'));

            $('#cerrarModal2').click();
        });
    });
</script>