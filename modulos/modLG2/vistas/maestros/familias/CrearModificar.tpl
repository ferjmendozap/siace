<form action="{$_Parametros.url}modLG/maestros/familiasCONTROL/crearModificarFamiliasMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idFamilias)}{$idFamilias}{else}0{/if}" name="idFamilias"/>
    <div class="modal-body">
        <div class="row">
            <div class="form-group col-lg-12">
                <div class="form-group col-lg-8" id="descError">
                    <input type="text" class="form-control" name="form[alphaNum][desc]" id="desc" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" {if isset($ver) and $ver==1} disabled {/if}>
                    <label for="desc"><i class="md md-border-color"></i>Descripcion</label>
                </div>
                <div class="form-group col-lg-3" id="codigoError">
                    <input type="text" class="form-control" name="form[alphaNum][codigo]" id="codigo" value="{if isset($formDB.ind_cod_subfamilia)}{$formDB.ind_cod_subfamilia}{/if}" {if isset($ver) and $ver==1} disabled {/if} maxlength="6">
                    <label for="codigo"><i class="md md-border-color"></i>Código</label>
                </div>
                <div class="form-group col-lg-1">
                    <label class="checkbox-styled">
                        <span>Estatus</span>
                        <input type="checkbox" name="form[int][estado]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_estatus)} {if $formDB.num_estatus==1}checked{/if}{else}checked{/if}>
                    </label>
                </div>
                <div class="form-group col-lg-6" id="lineasError">
                    <select id="lineas" name="form[int][lineas]" {if isset($ver) and $ver==1} disabled {/if}
                            class="form-control select2-list select2">
                        <option value=""></option>
                        {foreach item=condicion from=$lineas}
                            {if isset($formDB.fk_a006_num_miscelaneo_clase_linea) and $formDB.fk_a006_num_miscelaneo_clase_linea == $condicion.pk_num_miscelaneo_detalle}
                                <option value="{$condicion.pk_num_miscelaneo_detalle}" selected>{$condicion.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$condicion.pk_num_miscelaneo_detalle}">{$condicion.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="lineas"><i class="md md-border-color"></i>Lineas</label>
                </div>
                <div class="form-group col-lg-6" id="partidaError">
                    <select class="form-control select2-list select2" name="form[int][partida]" {if isset($ver) and $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=post from=$partida}
                            {if $post.pk_num_partida_presupuestaria==$formDB.fk_prb002_partida_presupuestaria}
                                <option value={$post.pk_num_partida_presupuestaria} selected>{$post.ind_denominacion}</option>
                            {else}
                                <option value={$post.pk_num_partida_presupuestaria}>{$post.ind_denominacion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="partida"><i class="md md-border-color"></i>Partida Presupuestaria</label>
                </div>
                <div class="form-group col-lg-6" id="inventarioError">
                    <select id="inventario" name="form[int][inventario]" {if isset($ver) and $ver==1} disabled {/if}
                            class="form-control select2-list select2">
                        <option value=""></option>
                        {foreach item=condicion from=$plan}
                            {if isset($formDB.fk_cbb004_cuenta_inventario) and $formDB.fk_cbb004_cuenta_inventario == $condicion.pk_num_cuenta}
                                <option value="{$condicion.pk_num_cuenta}" selected>{$condicion.ind_descripcion}</option>
                            {else}
                                <option value="{$condicion.pk_num_cuenta}">{$condicion.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="inventario"><i class="md md-border-color"></i>Cuenta Inventario</label>
                </div>
                <div class="form-group col-lg-6" id="gastoError">
                    <select id="gasto" name="form[int][gasto]" {if isset($ver) and $ver==1} disabled {/if}
                            class="form-control select2-list select2">
                        <option value=""></option>
                        {foreach item=condicion from=$plan}
                            {if isset($formDB.fk_cbb004_cuenta_gasto) and $formDB.fk_cbb004_cuenta_gasto == $condicion.pk_num_cuenta}
                                <option value="{$condicion.pk_num_cuenta}" selected>{$condicion.ind_descripcion}</option>
                            {else}
                                <option value="{$condicion.pk_num_cuenta}">{$condicion.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="gasto"><i class="md md-border-color"></i>Cuenta Gasto</label>
                </div>
                <div class="form-group col-lg-6" id="usuAlmacenError">
                    <input type="text" class="form-control disabled" id="usuAlmacen" disabled value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}">
                    <label for="usuAlmacen"><i class="md md-border-color"></i>Ultimo Usuario</label>
                </div>
                <div class="form-group col-lg-6" id="fecAlmacenError">
                    <input type="text" class="form-control disabled" id="fecAlmacen" disabled value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}">
                    <label for="fecAlmacen"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                </div>
            </div>
            <!--end .row -->
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idFamilias)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2({ allowClear: true });
        $('#modalAncho').css("width", "60%");
        var app = new  AppFunciones();
        $("#formAjax").submit(function () {
            return false;
        });
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La Familia fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'La Familia fue guardada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

    });
</script>