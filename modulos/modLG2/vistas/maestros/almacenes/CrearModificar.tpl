<form action="{$_Parametros.url}modLG/maestros/almacenesCONTROL/crearModificarAlmacenMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idAlmacen)}{$idAlmacen}{/if}" name="idAlmacen"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group floating-label col-lg-3" id="codAlmacenError">
                    <input type="text" class="form-control" name="form[alphaNum][codAlmacen]" value="{if isset($formDB.cod_almacen)}{$formDB.cod_almacen}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="codAlmacen" maxlength="6">
                    <label for="codAlmacen"><i class="md md-border-color"></i>Código de Almacén</label>
                </div>
                <div class="form-group col-lg-3" id="tipoAlmacenError">
                    <select class="form-control select2-list select2" name="form[int][tipoAlmacen]" {if isset($ver) and $ver==1} disabled {/if}>
                        {foreach item=condicion from=$selectAlm}
                            {if isset($formDB.ind_tipo_almacen) and $formDB.ind_tipo_almacen == $condicion.pk_num_miscelaneo_detalle}
                                <option value="{$condicion.pk_num_miscelaneo_detalle}" selected>{$condicion.ind_nombre_detalle}</option>
                            {else}
                                <option value="{$condicion.pk_num_miscelaneo_detalle}">{$condicion.ind_nombre_detalle}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="tipoAlmacen">Tipo de Almacen</label>
                </div>
                <div class="form-group floating-label col-lg-6" id="descAlmacenError">
                    <input type="text" class="form-control" name="form[txt][descAlmacen]" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="desAlmacen">
                    <label for="desAlmacen"><i class="md md-border-color"></i>Descripcion</label>
                </div>
                <div class="form-group floating-label col-lg-12" id="dirAlmacenError">
                    <textarea class="form-control" name="form[alphaNum][dirAlmacen]" id="dirAlmacen" {if isset($ver) and $ver==1} disabled {/if} >{if isset($formDB.ind_direccion)}{$formDB.ind_direccion}{/if}</textarea>
                    <label for="dirAlmacen"><i class="md md-border-color"></i>Direccion</label>
                </div>

                <div class="form-group col-lg-8" id="cuentaAlmacenError">
                    <label for="cuentaAlmacen"><i class="md md-border-color"></i>Cuenta Inventario</label>
                    <div class="col-lg-11">
                        <input type="hidden" name="form[int][cuentaAlmacen]" id="idC1" value="{if isset($formDB.fk_cbb004_num_cuenta_inventario)}{$formDB.fk_cbb004_num_cuenta_inventario}{/if}">
                        <input type="text" disabled class="form-control" id="nombreC1" value="{if isset($formDB.nombreCuenta1)}{$formDB.nombreCuenta1}{/if}">
                    </div>
                    <div class="col-lg-1">
                        <button {if isset($ver) and $ver==1} disabled {/if}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button"
                                title="Buscar Cuenta"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Cuenta"
                                url="{$_Parametros.url}modLG/maestros/itemsCONTROL/cuentaMET/1">
                            <i class="md md-search" style="color: #ffffff;"></i>
                    </div>
                </div>
                <div class="form-group floating-label col-lg-2">
                    <label class="checkbox-styled">
                        <span>Commodities</span>
                        <input type="checkbox" name="form[int][commAlmacen]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_commodity) and $formDB.num_flag_commodity==1}checked{/if}>
                    </label>
                </div>
                <div class="form-group floating-label col-lg-2">
                    <label class="checkbox-styled">
                        <span>Estatus</span>
                        <input type="checkbox" name="form[int][estadoAlmacen]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_estatus)} {if $formDB.num_estatus==1}checked{/if}{else}checked{/if}>
                    </label>
                </div>

                <div class="form-group floating-label col-sm-12" id="responAlmacenError">
                    <input type="hidden" class="form-control" name="form[int][responAlmacen]"
                           value="{if isset($formDB.pk_num_empleado)}{$formDB.pk_num_empleado}{/if}"
                           id="idEmpleado">
                    <input type="hidden" class="form-control" name="form[int][encargadoAlmacen]"
                           value="{if isset($formDB.pk_num_persona)}{$formDB.pk_num_persona}{/if}"
                           id="idPersona">
                    <div class="form-group floating-label col-sm-11">
                        <div class="form-group floating-label col-sm-8">
                            <input type="text" class="form-control" id="personaResp" value="{if isset($formDB.ind_nombre1) and isset($formDB.ind_apellido1)}{$formDB.ind_nombre1} {$formDB.ind_apellido1}{/if}" disabled>
                        </div>
                        <div class="form-group floating-label col-sm-4">
                            <input type="text" class="form-control" id="cedula" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" disabled>
                        </div>
                    </div>
                    <div class="form-group floating-label col-sm-1">
                        <button {if isset($ver) and $ver==1} disabled {/if}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button"
                                title="Buscar Empleado"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Empleado"
                                url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona/"
                                >
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                    <label for="responAlmacen"><i class="md md-people"></i>Empleado</label>
                </div>

                <div class="col-lg-12">
                    <div class="form-group floating-label col-lg-6">
                        <input type="text" class="form-control" disabled value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}">
                        <label for="usuAlmacen"><i class="md md-border-color"></i>Ultimo Usuario</label>
                    </div>
                    <div class="form-group floating-label col-lg-6">
                        <input type="text" class="form-control" disabled value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}">
                        <label for="fecAlmacen"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                    </div>
                </div>

            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .modal-doby-->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idAlmacen)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css("width", "60%");
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#modalAncho').css("width", "60%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Almacen fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Almacen fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
    });
</script>