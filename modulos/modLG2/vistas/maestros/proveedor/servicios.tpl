<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Servicios</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Descripcion</th>
                            <th>Código</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr class="servicio">
                                <input type="hidden"
                                       idServicios="{$i.pk_num_tipo_servico}"
                                       codigo="{$i.cod_tipo_servicio}"
                                       desc="{$i.ind_descripcion}">
                                <td>{$i.pk_num_tipo_servico}</td>
                                <td>{$i.ind_descripcion}</td>
                                <td>{$i.cod_tipo_servicio}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2 tbody').on( 'click', '.servicio', function () {
            var input = $(this).find('input');
            {if $servicio == 'servicios'}
            $(document.getElementById('idServicios')).val(input.attr('idServicios'));
            $(document.getElementById('codigo')).val(input.attr('codigo'));
            $(document.getElementById('desc')).val(input.attr('desc'));

            $(document.getElementById('contenidoTabla2')).append(
                    '<tr class="idServicio{$tr}">'+
                    '<td width="10px" style="vertical-align: middle;">{$tr}</td>' +
                    '<td width="10px"><input type="text" class="form-control" readonly value="'+input.attr('desc')+'" id="desc{$tr}" size="2"></td>' +
                    '<td width="10px"><input type="text" class="form-control" readonly value="'+input.attr('codigo')+'" id="sCodigo{$tr}" name="form[alphaNum][sCodigo{$tr}]" size="2"></td>' +
                    '<td style="vertical-align: middle;">' +
                    '<input type="hidden" value="{$tr}" name="form[int][sCant]" size="2">' +
                    '<input type="hidden" value="'+input.attr('idServicios')+'" name="form[int][sId{$tr}]" size="2">' +
                    '<button class="eliminar btn ink-reaction btn-raised btn-xs btn-danger" id="idServicio{$tr}">' +
                    '<i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                    '</tr>');
            {/if}
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>