<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Personas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nro Documento</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                        </tr>
                        </thead>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        /*
        $('#dataTablaJson2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        */
        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson2',
            '{$_Parametros.url}modLG/maestros/proveedorCONTROL/jsonDataTablaPersonaMET',
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "pk_num_persona" },
                { "data": "ind_cedula_documento" },
                { "data": "ind_nombre1" },
                { "data": "ind_apellido1" }
            ]
        );

        $('#dataTablaJson2').on('click', 'tbody tr', function () {
            var celdas = $(this).find('td');
            var c = 0;
            var idPersona = 0;
            var cedula = '';
            var nombre = '';
            celdas.each(function(){
                c = c+1;
                if(c==1){ idPersona = $(this).html(); }
                if(c==2){ cedula = $(this).html(); }
                if(c==3){ nombre = $(this).html(); }
                if(c==4){ nombre += $(this).html(); }
            });

            {if $persona == 'persona1'}
            $(document.getElementById('idPersona1')).val(idPersona);
            $(document.getElementById('personaResp1')).val(nombre);
            $(document.getElementById('cedula1')).val(cedula);
            {elseif ($persona == 'persona2')}
            $(document.getElementById('idPersona2')).val(idPersona);
            $(document.getElementById('personaResp2')).val(nombre);
            $(document.getElementById('cedula2')).val(cedula);
            {elseif ($persona == 'persona3')}
            $(document.getElementById('idPersona3')).val(idPersona);
            $(document.getElementById('personaResp3')).val(nombre);
            $(document.getElementById('cedula3')).val(cedula);
            {/if}
            $('#cerrarModal2').click();
        });
    });
</script>