<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Tipo de Documentos</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Descripción</th>
                            <th>Documento Fiscal</th>
                            <th>Tran. Sistema</th>
                            <th>Estatus</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="5">
                                {if in_array('LG-01-07-03-02-01-N',$_Parametros.perfil)}
                                    <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                            descipcion="el Usuario ha creado el Tipo de Documento Nro. " data-toggle="modal"
                                            data-target="#formModal" titulo="Registrar Nuevo Tipo de Documento" id="nuevo"><i class="md md-create"></i> Nuevo
                                        Tipo de Documento
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

    $(document).ready(function () {
        var url = '{$_Parametros.url}modLG/maestros/tipoDocumentoCONTROL/crearModificarTipoDocMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/maestros/tipoDocumentoCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_tipo_documento" },
                    { "data": "ind_descripcion" },
                    { "orderable": false,"data": "num_flag_documento_fiscal"},
                    { "orderable": false,"data": "num_flag_transaccion_sistema"},
                    { "orderable": false,"data": "num_estatus"},
                    { "orderable": false,"data": "acciones"}
                ]
        );

        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idTipoDoc: 0 }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idTipoDoc: $(this).attr('idTipoDoc')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idTipoDoc: $(this).attr('idTipoDoc'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idTipoDoc = $(this).attr('idTipoDoc');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modLG/maestros/tipoDocumentoCONTROL/eliminarTipoDocMET';
                $.post(url, { idTipoDoc: idTipoDoc },function(dato){
                    if(dato['status']=='ok'){
                        $('#dataTablaJson').dataTable().api().row().remove().draw(false);
                        swal("Eliminado!", "El Tipo de Documento fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

    });
</script>