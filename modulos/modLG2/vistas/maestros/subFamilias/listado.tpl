<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado del SubFamilias</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Código</th>
                                <th>Código familia</th>
                                <th>Descripción</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="4">
                                    {if in_array('LG-01-07-02-03-01-N',$_Parametros.perfil)}
                                        <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                                descipcion="el Usuario ha creado la subfamilia Nro. " data-toggle="modal"
                                                data-target="#formModal" titulo="Registrar Nueva SubFamilia" id="nuevo">
                                            <i class="md md-create"></i> Nueva SubFamilia
                                        </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var url = '{$_Parametros.url}modLG/maestros/subFamiliasCONTROL/crearModificarSubFamiliasMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/maestros/subFamiliasCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_subfamilia" },
                    { "data": "ind_cod_subfamilia" },
                    { "data": "ind_cod_familia" },
                    { "data": "ind_descripcion" },
                    { "orderable": false,"data": "num_estatus"},
                    { "orderable": false,"data": "acciones"}
                ]
        );
        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idSubFamilias: 0 }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idSubFamilias: $(this).attr('idSubFamilias')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idSubFamilias: $(this).attr('idSubFamilias'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idSubFamilias = $(this).attr('idSubFamilias');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modLG/maestros/subFamiliasCONTROL/eliminarSubFamiliasMET';
                $.post(url, { idSubFamilias: idSubFamilias },function(dato){
                    if(dato['status']=='ok'){
                        $('#dataTablaJson').dataTable().api().row().remove().draw(false);
                        swal("Eliminado!", "La SubFamilia fue eliminada satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
    });
</script>