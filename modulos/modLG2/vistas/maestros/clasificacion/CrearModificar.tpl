<form action="{$_Parametros.url}modLG/maestros/clasificacionCONTROL/crearModificarClasificacionMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idClasificacion)}{$idClasificacion}{/if}" name="idClasificacion"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-6">
                    <div class="form-group floating-label col-lg-4">
                        <label class="checkbox-styled">
                            <span>Estatus</span>
                            <input type="checkbox" name="form[alphaNum][estado]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_estatus)} {if $formDB.num_estatus==1}checked{/if}{else}checked{/if}>
                        </label>
                    </div>
                    <div class="form-group floating-label col-lg-8">
                        <label class="checkbox-styled">
                            <span>Usar Unidad de Compra</span>
                            <input type="checkbox" name="form[int][reposicion]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_reposicion) and $formDB.num_flag_reposicion=='1'}{"checked"}{/if}>
                        </label>
                    </div>
                    <div class="form-group floating-label col-lg-12">
                        <label class="checkbox-styled">
                            <span>Activo Fijo</span>
                            <input type="checkbox" name="form[int][activo]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_activo_fijo) and $formDB.num_flag_activo_fijo=='1'}{"checked"}{/if}>
                        </label>
                    </div>
                    <div class="form-group floating-label col-lg-12">
                        <label class="checkbox-styled">
                            <span>Trans. del Sistema</span>
                            <input type="checkbox" name="form[int][tran]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_transaccion) and $formDB.num_flag_transaccion=='1'}{"checked"}{/if}>
                        </label>
                    </div>
                    <div class="form-group floating-label col-lg-12">
                        <label class="checkbox-styled">
                            <span>Recepcionar en Almacén</span>
                            <input type="checkbox" name="form[int][recepcion]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_recepcion_almacen) and $formDB.num_flag_recepcion_almacen=='1'}{"checked"}{/if}>
                        </label>
                    </div>
                    <div class="form-group floating-label col-lg-12">
                        <label class="checkbox-styled">
                            <span>Disponible para Caja Chica</span>
                            <input type="checkbox" name="form[int][caja]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_caja_chica) and $formDB.num_flag_caja_chica=='1'}{"checked"}{/if}>
                        </label>
                    </div>
                    <div class="form-group floating-label col-lg-12">
                        <label class="checkbox-styled">
                            <span>La Requisición requiere Revisión</span>
                            <input type="checkbox" name="form[int][revision]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_flag_revision) and $formDB.num_flag_revision=='1'}{"checked"}{/if}>
                        </label>
                    </div>
                    <div class="form-group floating-label col-lg-12" id="usuClasError">
                        <input type="text" class="form-control disabled" id="usuClas" disabled value="{if isset($formDB.usuario)}{$formDB.usuario}{/if}">
                        <label for="usuClas"><i class="md md-border-color"></i>Ultimo Usuario</label>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group floating-label col-lg-12" id="descError">
                        <input type="text" class="form-control" name="form[alphaNum][desc]" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="desc">
                        <label for="desc"><i class="md md-border-color"></i>Descripcion</label>
                    </div>
                    <div class="form-group floating-label col-lg-8" id="almacenError">
                        <label for="almacen"><i class="md md-border-color"></i>Almacen</label>
                        <select class="form-control select2-list select2" name="form[int][almacen]" {if isset($ver) and $ver==1} disabled {/if}>
                            <option></option>
                            {foreach item=post from=$almacen}
                                {if $post.pk_num_almacen==$formDB.fk_lgb014_num_almacen}
                                    <option value={$post.pk_num_almacen} selected>{$post.ind_descripcion}</option>
                                {else}
                                    <option value={$post.pk_num_almacen}>{$post.ind_descripcion}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>

                    <div class="form-group floating-label col-lg-4" id="codigoError">
                        <input type="text" class="form-control" name="form[alphaNum][codigo]" value="{if isset($formDB.ind_cod_clasificacion)}{$formDB.ind_cod_clasificacion}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="codigo" maxlength="3">
                        <label for="codigo"><i class="md md-border-color"></i>Código</label>
                    </div>
                    <div class="form-group floating-label col-lg-12" id="tipoReqError">
                        <label for="tipoReq"><i class="md md-border-color"></i>Tipo requerimiento</label>
                        <select class="form-control select2-list select2" name="form[int][tipoReq]" {if isset($ver) and $ver==1} disabled {/if}>
                            <option></option>
                            {foreach item=post from=$tipoReq}
                                {if $post.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneos_tipo_requerimiento}
                                    <option value={$post.pk_num_miscelaneo_detalle} selected>{$post.ind_nombre_detalle}</option>
                                {else}
                                    <option value={$post.pk_num_miscelaneo_detalle}>{$post.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                    <div class="form-group floating-label col-lg-12" id="fecClasError">
                        <input type="text" class="form-control disabled" id="fecClas" disabled value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}">
                        <label for="fecClas"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                    </div>

                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idClasificacion)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "40%");
    $("#formAjax").submit(function(){
        return false;
    });
    $('.select2').select2({ allowClear: true });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('#modalAncho').css("width", "50%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Clasificacion fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Clasificacion fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
    });
</script>