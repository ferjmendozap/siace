<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Reporte de Selección de Contratistas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form action="{$_Parametros.url}modLG/reportes/reporteContratistaCONTROL/generarReporteMET/" id="formulario" method="post">

            <div class="card-head card-head-xs col-lg-2 style-primary" align="center">
                <header>F. Realización:</header>
            </div>
                <div class="col-lg-1 contain-lg">
                <div class="form-group" id="desdeError">
                    <label for="desde">Fecha Desde</label>
                    <input type="text" readonly name="desde" id="desde" value="{date('Y-m')}-01" class="form-control fecha">
                </div>
            </div>
            <div class="col-lg-1 contain-lg">
                <div class="form-group" id="hastaError">
                    <label for="hasta">Fecha Hasta</label>
                    <input type="text" readonly name="hasta" id="hasta" value="{date('Y-m-d')}" class="form-control fecha">
                </div>
            </div>
            <div class="col-lg-12" align="center">
                <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                    Buscar
                </button>
            </div>
        </div>

        </form>
        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        $('#accion').click(function () {
            var url2 = $('#formulario').attr('action');
            var desde = $('#desde').val();
            var hasta = $('#hasta').val();
            $('#respuestaPdf').html('<iframe frameborder="0" src="' + url2 +desde+'/'+hasta+'" width="100%" height="540px"></iframe>');

        });

        $('#desde').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#hasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

    });
</script>