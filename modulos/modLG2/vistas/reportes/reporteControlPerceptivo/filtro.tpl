<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Reporte de Control Perceptivo</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">

            <div class="col-lg-12">
                <div class="col-lg-2 contain-lg" align="right">
                    <span>Fecha Realización.:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-2 contain-lg" align="left">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkPeriodo" class="form-control"><span></span>
                        </label>
                    </div>
                    <div class="col-lg-5 contain-lg">
                        <div class="form-group" id="fdesdeError">
                            <input type="text" disabled name="fdesde" id="fdesde" value="{date('Y-m')}-01" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-5 contain-lg">
                        <div class="form-group" id="fhastaError">
                            <input type="text" disabled name="fhasta" id="fhasta" value="{date('Y-m-d')}" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" align="center">
                <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                    Buscar
                </button>
            </div>




        </div>

        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        $('#accion').click(function () {
            var url2 = '{$_Parametros.url}modLG/reportes/reporteControlPerceptivoCONTROL/generarReporteMET';
            var msj ='';
            if($('#checkPeriodo').attr('checked')=="checked"){
                var fdesde = $('#fdesde').val();
                var fhasta = $('#fhasta').val();
                if(fdesde>fhasta){
                    msj = 'Error de Fechas de Realizacion';
                }
            } else {
                fdesde = 'no';
                fhasta = 'no';
            }

            if(msj!=''){
                swal("Error!", msj , "error");
            } else {
                $('#respuestaPdf').html('<iframe frameborder="0" src="' + url2 +
                        '/'+fdesde+
                        '/'+fhasta
                        +'" width="100%" height="540px"></iframe>');
            }
        });

        $('#fdesde').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#fhasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('#checkPeriodo').change(function () {
            if($('#checkPeriodo').attr('checked')=="checked" ){
                $('#fdesde').attr('disabled',false);
                $('#fdesde').attr('readonly','readonly');
                $('#fhasta').attr('disabled',false);
                $('#fhasta').attr('readonly','readonly');
            } else {
                $('#fdesde').attr('readonly',false);
                $('#fdesde').attr('disabled',true);
                $('#fhasta').attr('readonly',false);
                $('#fhasta').attr('disabled',true);

                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth()+1;
                var yyyy = hoy.getFullYear();

                if(dd<10) { dd='0'+dd; }
                if(mm<10) { mm='0'+mm; }

                var fdesde = yyyy+'-'+mm+'-01';
                var fhasta = yyyy+'-'+mm+'-'+dd;
                $('#fdesde').val(fdesde);
                $('#fhasta').val(fhasta);
            }
        });
        $('#checkNroOrden').change(function () {
            if($('#checkNroOrden').attr('checked')=="checked" ){
                $('#nroOrden').attr('disabled',false);
            } else {
                $('#nroOrden').attr('disabled',false);
                $('#nroOrden').val('0000000001');
            }
        });

    });
</script>