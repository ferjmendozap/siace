<section class="style-default-bright">
        <h2 class="text-primary">Ubicaciones por Almacén de Item</h2>
    <div class="section-body contain-lg">
        <form action="{$_Parametros.url}modLG/reportes/reporteItemsCONTROL/generarReporteUbiAlmacenMET/" id="formulario" method="post">
            <div class="row">

                <!--div class="contain-lg col-lg-1" align="right">
                    <span>Ubicacion:</span>
                </div>
                <div class="col-lg-2 contain-lg">
                    <div class="form-group" id="ubicacionError">
                        <input type="text" name="ubicacion" id="ubicacion" class="form-control">
                    </div>
                </div-->

                <div class="col-lg-1" align="right">
                    <span>Linea:</span>
                </div>
                <div class="col-lg-3">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkLinea" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-1 contain-lg">
                        <button id="linea" disabled class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Ubicacion Linea"
                                url="{$_Parametros.url}modLG/reportes/reporteItemsCONTROL/lineasMET/">
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>

                    <div class="col-lg-8 contain-lg">
                        <div class="form-group" id="idLineaError">
                            <input type="hidden" readonly name="idLinea" id="idLinea" class="form-control">
                            <input type="text" readonly name="nombreLinea" id="nombreLinea" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 contain-lg">
                    <div class="contain-lg col-lg-3" align="right">
                        <span>Almacén:</span>
                    </div>
                    <div class="col-lg-9 contain-lg">
                        <select id="almacen" class="form-control select2">
                            {foreach item=alm from=$almacen}
                                <option value="{$alm.pk_num_almacen}">{$alm.ind_descripcion}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="col-lg-2 contain-lg">
                    <div class="contain-lg col-lg-6" align="right">
                        <span>Stock:</span>
                    </div>
                    <div class="col-lg-6 contain-lg">
                        <select id="stock" class="form-control select2">
                            <option value="1">Actual</option>
                            <option value="2"><>0</option>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12" align="center">
                    <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                        Buscar
                    </button>
                </div>
            </div>
        </form>

        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        //linea
        $('#checkLinea').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#linea').attr('disabled',false);
            } else {
                $('#linea').attr('disabled','disabled');
                $('#idLinea').val('');
                $('#nombreLinea').val('');
            }
        });

        //modal para ubicacion la linea
        $('#linea').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });


        $('#accion').click(function () {
            /*
            if($('#ubicacion').val().length>0){
                var ubicacion = $('#ubicacion').val();
            } else {
                ubicacion = 'no';
            }
            */
            ubicacion = 'no';

            if($('#checkLinea').attr('checked')=="checked" && $('#idLinea').val().length > 0){
                var linea = $('#idLinea').val();
            } else {
                linea = 'no';
            }


            //almacen
            var almacen = $('#almacen').val();
            var stock = $('#stock').val();
            var url = $('#formulario').attr('action');
            $('#respuestaPdf').html('<iframe frameborder="0" src="' + url +ubicacion+'/'+linea+'/'+almacen+'/'+stock+'" width="100%" height="540px"></iframe>');
        });



    });
</script>