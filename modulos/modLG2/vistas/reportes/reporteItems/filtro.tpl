<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Item</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form action="{$_Parametros.url}modLG/reportes/reporteItemsCONTROL/generarReporteMET/" id="formulario" method="post">


                <div class="contain-lg col-lg-1" align="right">
                    <span>Buscar:</span>
                </div>
                <div class="col-lg-2 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkBuscar" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-9 contain-lg">
                        <div class="form-group" id="buscarError">
                            <input type="text" disabled name="buscar" id="buscar" class="form-control">
                        </div>
                    </div>
                </div>



                <div class="contain-lg col-lg-1" align="right">
                    <span>Linea:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkLinea" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-1 contain-lg">
                        <button id="linea" disabled class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Linea"
                                url="{$_Parametros.url}modLG/reportes/reporteItemsCONTROL/lineasMET/">
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>

                    <div class="col-lg-8 contain-lg">
                        <div class="form-group" id="idLineaError">
                            <input type="hidden" readonly name="idLinea" id="idLinea" class="form-control">
                            <input type="text" readonly name="nombreLinea" id="nombreLinea" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="contain-lg col-lg-1" align="right">
                    <span>Ordenado por:</span>
                </div>
                <div class="col-lg-2 contain-lg">
                    <div class="col-lg-9 contain-lg">
                        <select id="orden" class="form-control select2">
                            <option value="pk_num_item">Item</option>
                            <option value="ind_descripcion">Descripcion</option>
                            <option value="fk_lgc002_num_unidad_conversion">Unidad</option>
                            <option value="pk_num_clase_familia">Familia</option>
                            <option value="fk_a006_num_miscelaneo_tipo_item">Tipo</option>
                            <option value="fk_prb002_num_partida_presupuestaria">Partida</option>
                            <option value="fk_cbb004_num_plan_cuenta_gasto_pub_veinte">Cta. Gasto</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-12" align="center">
                    <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                        Buscar
                    </button>
                </div>



        </div>

        </form>
        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        //buscar
        $('#checkBuscar').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#buscar').attr('disabled',false);
            } else {
                $('#buscar').attr('disabled','disabled');
                $('#buscar').val('');
            }
        });

        //linea
        $('#checkLinea').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#linea').attr('disabled',false);
            } else {
                $('#linea').attr('disabled','disabled');
                $('#idLinea').val('');
                $('#nombreLinea').val('');
            }
        });

        //modal para buscar la linea
        $('#linea').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });


        $('#accion').click(function () {
            if($('#checkBuscar').attr('checked')=="checked" && $('#buscar').val().length > 0){
                var buscar = $('#buscar').val();
            } else {
                buscar = 'no';
            }

            if($('#checkLinea').attr('checked')=="checked" && $('#idLinea').val().length > 0){
                var linea = $('#idLinea').val();
            } else {
                linea = 'no';
            }

            //orden
            var orden = $('#orden').val();
            var url = $('#formulario').attr('action');
            $('#respuestaPdf').html('<iframe frameborder="0" src="' + url +buscar+'/'+linea+'/'+orden+'" width="100%" height="540px"></iframe>');
        });



    });
</script>