<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Lineas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr class="linea">
                                <input type="hidden"
                                       idLinea="{$i.pk_num_miscelaneo_detalle}"
                                       nombreLinea="{$i.ind_nombre_detalle}">
                                <td>{$i.pk_num_miscelaneo_detalle}</td>
                                <td>{$i.ind_nombre_detalle}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2 tbody').on( 'click', '.linea', function () {
            var input = $(this).find('input');

            $('#idLinea').val(input.attr('idLinea'));
            $('#nombreLinea').val(input.attr('nombreLinea'));

            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>