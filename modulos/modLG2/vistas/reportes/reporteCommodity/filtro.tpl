<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Reporte de Commodities</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-6">
                <!--div class="contain-lg col-lg-1" align="right">
                    <span>Commodity:</span>
                </div-->
                <div class="col-lg-12 contain-lg">
                    <div class="col-lg-1 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkComm" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-10 form-group" id="commodityError">
                        <div class="col-lg-2">
                            <button disabled class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                    descripcion="el Usuario a Cargado el Inventario Nro. "
                                    id="commodity"
                                    title="Buscar Commoditie"
                                    titulo="Buscar Commoditie"
                                    data-toggle="modal" data-target="#formModal2"
                                    data-keyboard="false" data-backdrop="static"
                                    url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/commodity4/"
                                    >
                                <i class="md md-search" style="color: #ffffff;"></i>
                            </button>
                        </div>
                        <div class="col-lg-10">
                            <input type="hidden" readonly id="numComm" class="form-control" name="form[int][idComm]">
                            <input type="text" readonly id="nombreComm" class="form-control">
                        </div>
                    </div>


                </div>

            </div>
            <div class="col-lg-12" align="center">
                <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                    Buscar
                </button>
            </div>



        </div>

        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });
        $('.accionModal').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#accion').click(function () {
            var url2 = '{$_Parametros.url}modLG/reportes/reporteCommodityCONTROL/generarReporteMET/';

            if($('#checkComm').attr('checked')=="checked" && $('#numComm').val().length>0){
                var commodity = $('#numComm').val();
            } else {
                commodity = 'no';
            }

            $('#respuestaPdf').html('<iframe frameborder="0" src="' + url2 +commodity+'/" width="100%" height="540px"></iframe>');
        });


        $('#checkComm').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#commodity').attr('disabled',false);
            } else {
                $('#commodity').attr('disabled','disabled');
                $('#commodity').val('');
            }
        });


    });
</script>