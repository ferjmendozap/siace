<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Catálogo de Proveedores</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12">
                <div class="contain-lg col-lg-1">
                    <span>Nacionalidad:</span>
                </div>
                <div class="col-lg-2 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkNac" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-9 contain-lg">
                        <div class="form-group" id="nacionalidadError">
                            <select id="nacionalidad" disabled class="form-control select2">
                                <option value=""></option>
                                <option value="1">Nacional</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="contain-lg col-lg-4">
                    <div class="col-lg-6 contain-lg">
                    <span>Tipo de Servicio:</span>
                    <label class="checkbox-styled">
                        <input type="checkbox" id="checkServ" class="form-control">
                        <span></span>
                    </label>
                    </div>
                    <div class="col-lg-6 contain-lg">
                        <div class="form-group" id="servicioError">
                            <select id="servicio" disabled class="form-control select2">
                                {foreach item=ser from=$servicio}
                                    <option value="{$ser.pk_num_tipo_servico}">{$ser.ind_descripcion}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="contain-lg col-lg-1">
                    <span>Ordenado por:</span>
                </div>
                <div class="col-lg-2 contain-lg">
                    <div class="col-lg-9 contain-lg">
                        <select id="orden" class="form-control select2">
                            <option value="pk_num_proveedor">Cod. Proveedor</option>
                            <option value="ind_cedula_documento">Nro. Documento</option>
                            <option value="ind_documento_fiscal">Doc. Fiscal</option>
                            <option value="ind_tipo_persona">Nacionalidad</option>
                            <option value="ind_nombre1">Razón Social</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="col-lg-12" align="center">
                <div class="contain-lg col-lg-1">
                    <span>Ingresado el:</span>
                </div>
                <div class="col-lg-2 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkIngresado" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-9 contain-lg">
                        <div class="form-group" id="ingresadoError">
                            <input type="text" disabled name="ingresado" id="ingresado" value="{date('Y')}-01-01" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="contain-lg col-lg-1">
                    <span>Estado:</span>
                    <label class="checkbox-styled">
                        <input type="checkbox" id="checkEstado" class="form-control">
                        <span></span>
                    </label>
                </div>
                <div class="col-lg-2 contain-lg">
                    <div class="col-lg-9 contain-lg">
                        <select id="estado" disabled class="form-control select2">
                            <option value=""></option>
                            <option value="1">Activo</option>
                            <option value="2">Inactivo</option>
                        </select>
                    </div>
                </div>

                <div class="contain-lg col-lg-1">
                    <span>Buscar:</span>
                </div>
                <div class="col-lg-2 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkBuscar" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-9 contain-lg">
                        <div class="form-group" id="buscarError">
                            <input type="text" disabled name="buscar" id="buscar" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" align="center">
                <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                    Buscar
                </button>
            </div>
        </div>

        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        $('#accion').click(function () {
            var url2 = '{$_Parametros.url}modLG/reportes/reporteProveedoresCONTROL/generarReporteMET/';

            if($('#checkNac').attr('checked')=="checked" && $('#nacionalidad').val().length>0){
                var nacionalidad = $('#nacionalidad').val();
            } else {
                nacionalidad = 'no';
            }
            if($('#checkServ').attr('checked')=="checked" && $('#servicio').val().length>0){
                var servicio = $('#servicio').val();
            } else {
                servicio = 'no';
            }
            var orden = $('#orden').val();

            if($('#checkIngresado').attr('checked')=="checked" && $('#ingresado').val().length>0){
                var ingresado = $('#ingresado').val();
            } else {
                ingresado = 'no';
            }

            if($('#checkEstado').attr('checked')=="checked" && $('#estado').val().length>0){
                var estado = $('#estado').val();
            } else {
                estado = 'no';
            }

            if($('#checkBuscar').attr('checked')=="checked" && $('#buscar').val().length > 0){
                var buscar = $('#buscar').val();
            } else {
                buscar = 'no';
            }

            $('#respuestaPdf').html('<iframe frameborder="0" src="' + url2 +nacionalidad+'/'+servicio+'/'+orden+'/'+ingresado+'/'+estado+'/'+buscar+'" width="100%" height="540px"></iframe>');
        });

        $('#checkNac').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#nacionalidad').attr('disabled',false);
            } else {
                $('#nacionalidad').attr('disabled','disabled');
            }
        });

        $('#checkIngresado').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#ingresado').attr('disabled',false);
                $('#ingresado').attr('readonly','readonly');
            } else {
                $('#ingresado').attr('disabled','disabled');
            }
        });

        $('#checkServ').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#servicio').attr('disabled',false);
            } else {
                $('#servicio').attr('disabled','disabled');
            }
        });

        $('#checkEstado').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#estado').attr('disabled',false);
            } else {
                $('#estado').attr('disabled','disabled');
            }
        });

        $('#checkBuscar').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#buscar').attr('disabled',false);
            } else {
                $('#buscar').attr('disabled','disabled');
                $('#buscar').val('');
            }
        });

        $('#ingresado').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#hasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

    });
</script>