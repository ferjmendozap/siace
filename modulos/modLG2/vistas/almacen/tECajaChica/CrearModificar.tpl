<form action="{$_Parametros.url}modLG/almacen/tECajaChicaCONTROL/crearRecepcionMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idReq)}{$idReq}{/if}" name="idReq"/>
    <input type="hidden" value="{if isset($opcion)}{$opcion}{/if}" name="opcion"/>
    <input type="hidden" value="{if isset($idTran)}{$idTran}{/if}" name="idTran"/>
    <input type="hidden" value="{if isset($formDB.cod_requerimiento)}{$formDB.cod_requerimiento}{/if}" name="form[txt][docRef]"/>
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active">
                                        <a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span> </a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">ITEMS</span> </a></li>

                                </ul>
                            </div><!--end .form-wizard-nav -->
                            <div class="tab-content clearfix">
                                <div class="tab-pane floating-label active" id="tab1">
                                    <div class="card panel">
                                        <div class="card-head style-primary">
                                            <header>Información General</header>
                                        </div>
                                        <div class="card-body">
                                            <div class="col-lg-12">
                                                <div class="form-group col-lg-6" id="ccostoError">
                                                    <select id="ccosto" name="form[int][cCosto]" class="form-control select2 select2-list">
                                                        {foreach item=cc from=$centroCosto}
                                                            {if $formDB.fk_a004_num_dependencia==$idDepe AND $cc.fk_a004_num_dependencia==$idDepe}
                                                                <option value="{$cc.pk_num_centro_costo}" selected>{$cc.ind_descripcion_centro_costo}</option>
                                                            {else}
                                                                <option value="{$cc.pk_num_centro_costo}">{$cc.ind_descripcion_centro_costo}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="ccosto">Centro de Costo</label>
                                                </div>
                                                <div class="form-group col-lg-6" id="almacenError">
                                                    <select id="almacen" name="form[int][almacen]" class="form-control select2 select2-list">
                                                        {foreach item=alm from=$almacen}
                                                            {if $alm.num_flag_commodity!=1}
                                                                <option value="{$alm.pk_num_almacen}" selected>{$alm.ind_descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="almacen">Almacen</label>
                                                </div>
                                                <div class="form-group col-lg-6" id="tipoTranError">
                                                    <select id="tipoTran" name="form[int][tipoTran]" class="form-control select2 select2-list">
                                                        {foreach item=tran from=$transaccion}
                                                            {if $tran.ind_cod_tipo_transaccion=='RCJ' AND $opcion=="recepcionar"}
                                                                <option value="{$tran.pk_num_tipo_transaccion}" selected>{$tran.ind_descripcion}</option>
                                                            {elseif $tran.ind_cod_tipo_transaccion=='DRC' AND $opcion!="recepcionar"}
                                                                <option value="{$tran.pk_num_tipo_transaccion}" selected>{$tran.ind_descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="tipoTran">Tipo Transacción</label>
                                                </div>
                                                <div class="form-group col-lg-6" id="tipoDocError">
                                                    <select id="tipoDoc" name="form[int][tipoDoc]" class="form-control select2 select2-list">
                                                        {foreach item=doc from=$documento}
                                                            {if $doc.pk_num_tipo_documento==2}
                                                                <option value="{$doc.pk_num_tipo_documento}" selected>{$doc.ind_descripcion}</option>
                                                            {else}
                                                                <option value="{$doc.pk_num_tipo_documento}">{$doc.ind_descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="tipoDoc">Doc. Referencia</label>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="checkbox-inline checkbox-styled">
                                                        <input type="checkbox" checked readonly {if isset($formDB.num_flag_manual) and $formDB.num_flag_manual==1}checked{/if}>
                                                        <span>Valorización Manual</span>
                                                    </label>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label class="checkbox-inline checkbox-styled">
                                                        <input type="checkbox" readonly {if isset($formDB.num_flag_pendiente) and $formDB.num_flag_pendiente==1}checked{/if} >
                                                        <span>Valorización Pendiente</span>
                                                    </label>
                                                </div>
                                                <div class="form-group col-lg-3 floating-label" id="notaError">
                                                    <input type="text" name="form[alphaNum][nota]" {if isset($formDB.ind_nota_entrega_factura)} value="{$formDB.ind_nota_entrega_factura}" {/if} class="form-control">
                                                    <label for="nota">Nota de Entrega / Factura</label>
                                                </div>
                                                <div class="form-group col-lg-3 floating-label" id="fdocError">
                                                    <input type="text" class="form-control" name="form[txt][fdoc]" value="{date('Y-m-d')}" readonly>
                                                    <label for="fdoc">Fecha de Documento<i class="fa fa-calendar"></i></label>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group" id="comenError">
                                                        <input type="text" name="form[alphaNum][comen]" id="comen" class="form-control" value="{if isset($formDB.ind_comentario)}{$formDB.ind_comentario}{/if}{if isset($formDB.ind_comentarios)}{$formDB.ind_comentarios}{/if}">
                                                        <label for="comen"><i class="md md-border-color"></i>Comentario</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--end #tab1 -->
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="col-lg-12">
                                        <div class="card-head style-primary">
                                            <header>Items</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped no-margin" id="contenidoTabla">
                                                    <thead>
                                                    <tr>
                                                        <th width="15px">#</th>
                                                        <th width="10px">Código</th>
                                                        <th width="250px">Descripcion</th>
                                                        <th width="100px">Cantidad</th>
                                                        <th width="100px">Precio Unit.</th>
                                                        <th width="100px">Total</th>
                                                        <th width="10px">Seleccionar {if $opcion=="recepcionar"}Recepción{elseif $opcion=="aprobar"}Aprobado{else}Despacho{/if}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="item">
                                                    {if isset($formDBI)}
                                                        {$cant=0}
                                                        {foreach item=i from=$formDBI}
                                                            {$cantiRecibida=0}
                                                            {$cantiDespachada=0}
                                                            {$cantiPedida=$i.num_cantidad_pedida}
                                                            {foreach item=td from=$tranDet}
                                                                {if $td.fk_lgc001_num_requerimiento_detalle==$i.pk_num_requerimiento_detalle}
                                                                    {if $td.ind_cod_tipo_transaccion=='RCJ'}
                                                                        {$cantiRecibida=$cantiRecibida+$td.num_cantidad_transaccion}
                                                                    {elseif $td.ind_cod_tipo_transaccion=='DRC'}
                                                                        {$cantiDespachada=$cantiDespachada+$td.num_cantidad_transaccion}
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                            {$check=''}
                                                            {if $i.ind_estado=='PE' AND $opcion=="aprobar"}
                                                                {$check='checked'}
                                                            {/if}
                                                            {if $i.fk_lgb002_num_item}
                                                                {$id = $i.fk_lgb002_num_item}
                                                                {$idStock = $i.pk_num_item_stock}
                                                                {$descripcion = $i.ind_descripcion_item}
                                                                {$sAnt = $i.num_stock_actual}
                                                                {$ic="I"}
                                                            {else}
                                                                {$id = $i.fk_lgb003_num_commodity}
                                                                {$idStock = $i.pk_num_commodity_stock}
                                                                {$descripcion = $i.ind_descripcion_comm}
                                                                {$sAnt = $i.num_cantidad_comm}
                                                                {$ic="C"}
                                                            {/if}
                                                            {$readonly=""}
                                                            {$readonly2=""}
                                                            {if $opcion=="recepcionar"}
                                                                {$canti=$cantiPedida-$cantiRecibida}
                                                            {/if}
                                                            {if $opcion=="aprobar"}
                                                                {$readonly="readonly"}
                                                                {$canti=$cantiRecibida}
                                                            {/if}
                                                            {if $opcion=="despachar"}
                                                                {$readonly2="readonly"}
                                                                {$canti=$cantiRecibida-$cantiDespachada}
                                                                {if $i.ind_estado!='PE'}
                                                                    {$canti=0}
                                                                {/if}
                                                            {/if}
                                                            {if $canti!=0}
                                                                {$cant=$cant+1}
                                                                <tr id="item{$i.num_secuencia}">
                                                                    <input type="hidden" name="form[int][cantiDespachada][{$cant}]" value="{$cantiDespachada}">
                                                                    <input type="hidden" name="form[int][cantiRecibida][{$cant}]" value="{$cantiRecibida}">
                                                                    <input type="hidden" name="form[int][cantiPedida][{$cant}]" value="{$cantiPedida}">
                                                                    <input type="hidden" name="form[int][sAnt][{$cant}]" value="{$sAnt}">
                                                                    <input type="hidden" readonly class="form-control" name="form[int][idDetalleReq][{$cant}]" size="2" value="{$i.pk_num_requerimiento_detalle}">
                                                                    <td style="vertical-align: middle;">{$cant}</td>
                                                                    <td style="vertical-align: middle;"><input type="text" readonly class="form-control" name="form[int][numDet][{$cant}]" value="{$id}"></td>
                                                                    <td style="vertical-align: middle;"><input type="text" readonly class="form-control" value="{$descripcion}"></td>
                                                                    <td style="vertical-align: middle;">
                                                                        <input type="text" class="form-control cantidad" {$readonly} name="form[int][canti][{$cant}]"  id="cantidad{$cant}" size="6" value="{$canti}" secuencia="{$cant}">
                                                                    </td>
                                                                    <td style="vertical-align: middle;">
                                                                        <input type="text" class="form-control cantidad" {$readonly} {$readonly2} name="form[int][precio][{$cant}]" id="precio{$cant}"
                                                                               size=6" secuencia="{$cant}" value="{if isset($i.num_precio_unitario)}{$i.num_precio_unitario}{else}0{/if}">
                                                                    </td>
                                                                    <td style="vertical-align: middle;"><input type="text" readonly class="form-control" id="total{$cant}" size="6" value="0"></td>
                                                                    <td style="vertical-align: middle;">
                                                                        <div class="checkbox checkbox-styled" align="center">
                                                                            <label class="checkbox-inline checkbox-styled">
                                                                                <input type="checkbox" name="form[int][opcion][{$cant}]" class="checkbox checkbox-styled" value="1" id="check{$cant}" {$check}>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                    <input type="hidden" name="form[int][idStock][{$cant}]" value="{$idStock}">
                                                                    <input type="hidden" name="form[int][numUni][{$cant}]" value="{$cant}">
                                                                </tr>
                                                            {/if}
                                                        {/foreach}
                                                        <input type="hidden" name="form[int][cant]" value="{$cant}">
                                                        <input type="hidden" name="form[txt][cual]" value="{$ic}">
                                                    {/if}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--end #tab2 -->

                                <!--end .tab-content -->
                                <ul class="pager wizard">
                                    <!--<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>-->
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <!--<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>-->
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                </ul>
                            </div><!--END ClearFix-->
                        </div><!--end #rootwizard1 -->
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col -->
        </div><!--end .col -->
    </div><!--end .modal-body -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            {if isset($opcion)}
                <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                    {if $opcion=="recepcionar"}
                        <span class="fa fa-sign-in"></span> Recepcionar
                    {elseif $opcion=="aprobar"}
                        <span class="icm icm-rating3"></span> Aprobar
                    {elseif $opcion=="despachar"}
                        <span class="fa fa-sign-out"></span> Despachar
                    {/if}
                </button>
            {/if}
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "80%");
    $("#formAjax").submit(function(){
        return false;
    });
    $('.select2').select2({ allowClear: true });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('#fdoc').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('#tipoDoc').ready(function(){
            $('#num_ref').val($('#docRef').val());
        });

        $('#docRef').change(function(){
            $('#num_ref').val($('#docRef').val());
        });

        $('.cantidad').change(function(){
            var secuencia = $(this).attr('secuencia');
            var cantidad = $('#cantidad'+secuencia).val();
            var precio = $('#precio'+secuencia).val();
            var valor = cantidad*precio;
            $('#total'+secuencia).val(valor);
            if($('#check'+secuencia).attr('checked')==undefined){
                $('#check'+secuencia).attr('checked','checked');
            }
        });

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    $(document.getElementById('idReq'+dato['idReq'])).html(dato);
                    swal("Recepcionado!", "La Transaccion fue ejecutada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('idReq'+dato['idReq'])).remove();
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorCant'){
                    app.metValidarError(dato,'Disculpa. Las cantidades de los productos son erroneas');
                }else if(dato['status']=='errorOpcion'){
                    app.metValidarError(dato,'Disculpa. Debe seleccionar cual(es) detalle(s) desea efectuar la acción');
                }else if(dato['status']=='errorPrecio'){
                    app.metValidarError(dato,'Disculpa. Los Precios de los productos son erroneos');
                }else if(dato['status']=='errorStock'){
                    swal("Error!", "No hay suficiente stock para realizar la transacción.", "error");
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='periodo'){
                    swal("Error!", "No hay un periodo activo para realizar transacciones.", "error");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='nuevo'){
                    swal("Ejecutado!", "El Requerimiento fue Recibido satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('idReq'+dato['idReq'])).remove();
                }else if(dato['status']=='aprobar'){
                    swal("Ejecutado!", "El Requerimiento fue Aprobado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('aprobar'+dato['idReq'])).remove();
                }else if(dato['status']=='despachar'){
                    swal("Ejecutado!", "El Requerimiento fue Despachado satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('idReq2'+dato['idReq'])).remove();
                }
            }, 'json');
        });
    });
</script>