<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Recepción en Almacén</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nro. Orden</th>
                                <th>Proveedor</th>
                                <th>Razon Social</th>
                                <th>Fecha Entrega</th>
                                <th>Forma de Pago</th>
                                <th>Clasifición</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <!--tbody>
                            {foreach item=post from=$listado}
                                <tr id="idOrden{$post.pk_num_orden}">
                                    <td>{$post.ind_orden}</td>
                                    <td>{$post.pk_num_proveedor}</td>
                                    <td>{$post.ind_nombre1} {$post.ind_apellido1}</td>
                                    <td>{$post.fec_prometida}</td>
                                    <td>{$post.ind_nombre_detalle}</td>
                                    <td>{$post.clasificaionDesc}</td>
                                    <td>
                                        {if in_array('LG-01-02-03-01-R',$_Parametros.perfil)}
                                        <button accion="crear" title="Recepcionar"
                                                class="crear logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                descipcion="El Usuario ha Recepcionar en Almacen la Orden Nro. {$post.pk_num_orden}"
                                                idTran="{$post.pk_num_transaccion}" titulo="Recepcionar en Almacen"
                                                idDepe="{$post.fk_a004_num_dependencia}"
                                                idTran="{$post.pk_num_transaccion}"
                                                idOrden="{$post.pk_num_orden}"
                                                data-toggle="modal" data-target="#formModal" id="crear">
                                            <i class="fa fa-sign-in"></i>
                                        </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody-->
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var url = '{$_Parametros.url}modLG/almacen/recepcionCONTROL/crearRecepcionMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/almacen/recepcionCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_orden" },
                    { "data": "pk_num_proveedor" },
                    { "data": "nombreProveedor" },
                    { "data": "fec_prometida" },
                    { "data": "ind_nombre_detalle" },
                    { "data": "clasificaionDesc" },
                    { "orderable": false, "data": "acciones", width:150}
                ]
        );
        $('#dataTablaJson').on('click','.crear',function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idDepe: $(this).attr('idDepe'), idOrden: $(this).attr('idOrden') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
    });
</script>