<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">Ingreso / Despacho de Commodities</h2>
                    </div>
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <div class="row">
                            <div class="card">
                                <div class="card-body">
                                    <div id="rootwizard2" class="form-wizard form-wizard-horizontal">
                                        <form class="form floating-label form-validation" role="form" novalidate="novalidate">
                                            <div class="form-wizard-nav">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-primary"></div>
                                                </div>
                                                <ul class="nav nav-justified">
                                                    <li class="active">
                                                        <a href="#tab5" data-toggle="tab"><span class="step">1</span> <span class="title" style="font-weight:bold;">Ingreso de O/C</span> </a></li>
                                                    <li>
                                                        <a href="#tab6" data-toggle="tab"><span class="step">2</span> <span class="title" style="font-weight:bold;">Despacho de Req. x O/C</span> </a></li>
                                                </ul>
                                            </div>
                                            <div class="tab-content clearfix">
                                                <div class="tab-pane active" id="tab5">
                                                    <section class="style-default-bright">
                                                        <div class="section-body contain-lg">
                                                            <div class="row">
                                                                <div class="col-lg-12 contain-lg">
                                                                    <div class="table-responsive">
                                                            <table id="datatable1" class="table table-striped table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Nro. Orden</th>
                                                                    <th>Proveedor</th>
                                                                    <th>Fecha Entrega</th>
                                                                    <th>Forma de Pago</th>
                                                                    <th>Clasificación</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                {foreach item=post from=$listado}

                                                                    <!--detalles de las ordenes-->
                                                                    {$c1=0}
                                                                    <!--cuantos detalles estan recepcionados-->
                                                                    {$c2=0}
                                                                    {foreach item=det from=$listadoDet}
                                                                        {if $det.fk_lgb019_num_orden==$post.pk_num_orden}
                                                                            {if $det.num_cantidad==$det.cantidad_recibida}
                                                                                {$c2=$c2+1}
                                                                            {/if}
                                                                            {$c1=$c1+1}
                                                                        {/if}
                                                                    {/foreach}
                                                                    <!--si son disttintos debe recepcionar-->
                                                                    {if $c1!=$c2}
                                                                        <tr id="idOrden{$post.pk_num_orden}">
                                                                            <td>{$post.ind_orden}</td>
                                                                            <td>{$post.proveedor}</td>
                                                                            <td>{$post.fec_prometida}</td>
                                                                            <td>{$post.formaPago}</td>
                                                                            <td>{$post.clasificacion}</td>
                                                                            <td>
                                                                                {if in_array('LG-01-02-05-01-01-R',$_Parametros.perfil)}
                                                                                    <button title="Recepcionar"
                                                                                            class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info nuevo"
                                                                                            descipcion="El Usuario ha Recepcionado la orden Nro. {$post.pk_num_orden}"
                                                                                            idOrden="{$post.pk_num_orden}" titulo="Recepcionar Orden"
                                                                                            data-toggle="modal" data-target="#formModal"
                                                                                            opcion="recepcionar"
                                                                                            id="recepcionar">
                                                                                        <i class="fa fa-sign-in"></i>
                                                                                        <!--data-keyboard="false" data-backdrop="static"-->
                                                                                    </button>
                                                                                {/if}
                                                                            </td>
                                                                        </tr>
                                                                    {/if}
                                                                {/foreach}
                                                                </tbody>
                                                            </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                                <div class="tab-pane" id="tab6">
                                                    <section class="style-default-bright">
                                                        <div class="section-body contain-lg">
                                                            <div class="row">
                                                                <div class="col-lg-12 contain-lg">
                                                                    <div class="table-responsive">
                                                            <table id="datatable3" class="table table-striped table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Nro. Orden</th>
                                                                    <th>Proveedor</th>
                                                                    <th>Fecha Entrega</th>
                                                                    <th>Forma de Pago</th>
                                                                    <th>Clasificación</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                {foreach item=post from=$listado}
                                                                    {$cantPed=0}
                                                                    {$cantRec=0}
                                                                    {$cantDes=0}
                                                                    {$cantApr=0}
                                                                    {$cantApr2=0}
                                                                    {$cantPre=0}
                                                                    {$cant=0}
                                                                    {foreach item=det from=$listadoDet}
                                                                        {if $det.fk_lgb019_num_orden==$post.pk_num_orden}
                                                                            {$cant=$cant+1}
                                                                            {$cantPed=$cantPed+$det.num_cantidad}
                                                                            {$cantRec=$cantRec+$det.cantidad_recibida}
                                                                            {$cantDes=$cantDes+$det.cantidad_despachada}
                                                                            {if $det.ind_estado == 'AP' OR $det.ind_estado == 'CO'}
                                                                                {$cantApr=$cantApr+1}
                                                                            {elseif $det.cantidad_recibida != NULL}
                                                                                {$cantPre=$cantPre+1}
                                                                            {/if}
                                                                            {if $det.cantidad_despachada != NULL}
                                                                                {$cantApr2=$cantApr2+1}
                                                                            {/if}
                                                                        {/if}
                                                                    {/foreach}
                                                                    {foreach item=det2 from=$listadoDet2}
                                                                    {/foreach}
                                                                    {if $cantRec>0 AND $cantRec!=$cantDes}
                                                                        <tr id="2idOrden{$post.pk_num_orden}">
                                                                            <td>{$post.ind_orden}</td>
                                                                            <td>{$post.proveedor}</td>
                                                                            <td>{$post.fec_prometida}</td>
                                                                            <td>{$post.formaPago}</td>
                                                                            <td>{$post.clasificacion}</td>
                                                                            <td>
                                                                                {if in_array('LG-01-02-05-01-02-A',$_Parametros.perfil) AND $cantPre!=0}
                                                                                    <button title="Aprobar"
                                                                                            class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary nuevo"
                                                                                            descipcion="El Usuario ha Recepcionado la orden Nro. {$post.pk_num_orden}"
                                                                                            idOrden="{$post.pk_num_orden}" titulo="Aprobar Despacho"
                                                                                            data-toggle="modal" data-target="#formModal"
                                                                                            opcion="aprobar"
                                                                                            idTran="{$post.pk_num_transaccion}"
                                                                                            id="aprobar{$post.pk_num_orden}">
                                                                                        <i class="icm icm-rating3"></i>
                                                                                        <!--data-keyboard="false" data-backdrop="static"-->
                                                                                    </button>
                                                                                {/if}
                                                                                {if in_array('LG-01-02-05-01-03-D',$_Parametros.perfil) AND $cantApr>0 AND $cantDes<$cantRec}
                                                                                    <button title="Despachar"
                                                                                            class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info nuevo"
                                                                                            descipcion="El Usuario ha Recepcionado la orden Nro. {$post.pk_num_orden}"
                                                                                            idOrden="{$post.pk_num_orden}" titulo="Despachar Orden"
                                                                                            data-toggle="modal" data-target="#formModal"
                                                                                            opcion="despachar"
                                                                                            idTran="{$post.pk_num_transaccion}"
                                                                                            id="despachar">
                                                                                        <i class="fa fa-sign-out"></i>
                                                                                        <!--data-keyboard="false" data-backdrop="static"-->
                                                                                    </button>
                                                                                {/if}
                                                                            </td>
                                                                        </tr>
                                                                    {/if}
                                                                {/foreach}
                                                                </tbody>
                                                            </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div><!--END ClearFix-->
                                            <ul class="pager wizard">
                                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                                <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();
        $(".form").submit(function () {
            return false;
        });

        var url = '{$_Parametros.url}modLG/almacen/tECommodityCONTROL/crearModificarTransaccionMET';
        $('.table tbody').on( 'click', '.nuevo', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idOrden: $(this).attr('idOrden'), opcion: $(this).attr('opcion'), idTran: $(this).attr('idTran') },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable3').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }

        });

    });
</script>