<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Ubicaciones de Activos</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ubicación</th>
                            <th>Código</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr class="opciones">
                                    <input type="hidden"
                                           pk_num_ubicacion="{$i.pk_num_ubicacion}"
                                           ind_nombre_ubicacion="{$i.ind_nombre_ubicacion}"
                                            >
                                <td>{$i.pk_num_ubicacion}</td>
                                <td>{$i.ind_nombre_ubicacion}</td>
                                <td>{$i.ind_codigo}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {

        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });

        $('#datatable2 tbody').on( 'click', '.opciones', function () {
            var input = $(this).find('input');
            $(document.getElementById('pk_num_ubicacion'+{$id})).val(input.attr('pk_num_ubicacion'));
            $(document.getElementById('ind_nombre_ubicacion'+{$id})).val(input.attr('ind_nombre_ubicacion'));

            $('#cerrarModal2').click();
        });

    });
</script>