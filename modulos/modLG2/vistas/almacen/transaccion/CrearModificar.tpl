<form action="{$_Parametros.url}modLG/almacen/transaccionCONTROL/crearModificarTransaccionMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idTran)}{$idTran}{/if}" name="idTran"/>
    <input type="hidden" value="{if isset($formDB.fec_mes) AND isset($formDB.fec_anio)}{$formDB.fec_anio}-{$formDB.fec_mes}{/if}" name="periodo"/>
    <input type="hidden" value="{if isset($formDB.num_estatus)}{$formDB.num_estatus}{/if}" name="ejecutado"/>
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span> </a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">ITEMS</span> </a></li>
                                </ul>
                            </div><!--end .form-wizard-nav -->
                            <div class="tab-content clearfix">
                                <div class="tab-pane active floating-label" id="tab1">
                                    <div class="col-lg-12">
                                        <div class="panel-group">
                                        <div class="card panel">
                                            <div class="card-head style-primary">
                                                <header>Información General</header>
                                            </div>
                                            <div class="card-body">
                                                <div class="col-lg-12">
                                                    <div class="form-group col-lg-6" id="ccostoError">
                                                                <select id="ccosto" name="form[int][ccosto]" class="form-control select2 select2-list"{if isset($ver) and $ver==1} disabled {/if}>
                                                                    <option value=""></option>
                                                                    {foreach item=cc from=$centro}
                                                                        {if isset($formDB.fk_a023_num_centro_costo) and $formDB.fk_a023_num_centro_costo == $cc.pk_num_centro_costo}
                                                                            <option value="{$cc.pk_num_centro_costo}" selected>{$cc.ind_descripcion_centro_costo}</option>
                                                                        {else}
                                                                            <option value="{$cc.pk_num_centro_costo}">{$cc.ind_descripcion_centro_costo}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="ccosto">Centro de Costo</label>
                                                            </div>
                                                    <div class="form-group col-lg-6" id="almacenError">
                                                                <select id="almacen" name="form[int][almacen]" class="form-control select2 select2-list" {if isset($ver) and $ver==1} disabled {/if}>
                                                                    <option value=""></option>
                                                                    {foreach item=alm from=$almacen}
                                                                        {if isset($formDB.fk_lgb014_num_almacen) and $formDB.fk_lgb014_num_almacen == $alm.pk_num_almacen}
                                                                            <option value="{$alm.pk_num_almacen}" selected>{$alm.ind_descripcion}</option>
                                                                        {else}
                                                                            <option value="{$alm.pk_num_almacen}">{$alm.ind_descripcion}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="almacen">Almacen</label>
                                                            </div>
                                                    <div class="form-group col-lg-6" id="tipoTranError">
                                                                <select id="tipoTran" name="form[int][tipoTran]" class="form-control select2 select2-list" {if isset($ver) and $ver==1} disabled {/if}>
                                                                    <option value=""></option>
                                                                    {foreach item=tran from=$transaccion}
                                                                        {if isset($formDB.fk_lgb015_num_tipo_transaccion) and $formDB.fk_lgb015_num_tipo_transaccion == $tran.pk_num_tipo_transaccion}
                                                                            <option value="{$tran.pk_num_tipo_transaccion}" selected>{$tran.ind_descripcion}</option>
                                                                        {else}
                                                                            <option value="{$tran.pk_num_tipo_transaccion}">{$tran.ind_descripcion}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="tipoTran">Tipo Transacción</label>
                                                            </div>
                                                    <div class="form-group col-lg-6" id="tipoDocError">
                                                                <select id="tipoDoc" name="form[int][tipoDoc]" class="form-control select2 select2-list" {if isset($ver) and $ver==1} disabled {/if}>
                                                                    <option value=""></option>
                                                                    {foreach item=doc from=$documento}
                                                                        {if isset($formDB.fk_lgb016_num_tipo_documento) and $formDB.fk_lgb016_num_tipo_documento == $doc.pk_num_tipo_documento}
                                                                            <option value="{$doc.pk_num_tipo_documento}" selected>{$doc.ind_descripcion}</option>
                                                                        {else}
                                                                            <option value="{$doc.pk_num_tipo_documento}">{$doc.ind_descripcion}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="tipoDoc">Tipo Documento</label>
                                                            </div>
                                                    <div class="col-lg-6">
                                                                <label class="checkbox-inline checkbox-styled">
                                                                    <input type="checkbox" name="form[int][manual]" value="1" {if isset($formDB.num_flag_manual) and $formDB.num_flag_manual==1}checked{/if}{if isset($ver) and $ver==1} disabled {/if}>
                                                                    <span>Valorización Manual</span>
                                                                </label>
                                                            </div>
                                                    <div class="col-lg-6">
                                                                <label class="checkbox-inline checkbox-styled">
                                                                    <input type="checkbox" name="form[int][pendiente]" value="1" {if isset($formDB.num_flag_pendiente) and $formDB.num_flag_pendiente==1}checked{/if} {if isset($ver) and $ver==1} disabled {/if}>
                                                                    <span>Valorización Pendiente</span>
                                                                </label>
                                                            </div>

                                                    <div class="form-group col-lg-6 floating-label" id="nroError">
                                                                <input type="text" readonly name="form[int][nro]" maxlength="6" id="nro" {if isset($formDB.num_transaccion)} value="{$formDB.num_transaccion}" readonly {/if} class="form-control"{if isset($ver) and $ver==1} disabled {/if}>
                                                                <label for="nro">Nro. Transacción</label>
                                                            </div>
                                                    <div class="form-group col-lg-6" id="docRefError">
                                                                <input type="text" name="form[int][docRef]" id="docRef" maxlength="20" class="form-control" value="{if isset($formDB.ind_num_documento_referencia)}{$formDB.ind_num_documento_referencia}{/if}"{if isset($ver) and $ver==1} disabled {/if}>
                                                                <label for="docRef">Nro. Doc. Referencia</label>
                                                            </div>
                                                    <div class="form-group col-lg-6 floating-label" id="notaError">
                                                                <input type="text" name="form[alphaNum][nota]" id="nota" {if isset($formDB.ind_nota_entrega_factura)} value="{$formDB.ind_nota_entrega_factura}" {/if} class="form-control"{if isset($ver) and $ver==1} disabled {/if}>
                                                                <label for="nota">Nota de Entrega / Factura</label>
                                                            </div>
                                                    <div class="form-group col-lg-6 floating-label" id="fdocError">
                                                                <input type="text" readonly name="form[txt][fdoc]" class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{else}{date('Y-m-d')}{/if}">
                                                                <label for="fdoc">Fecha de Documento<i class="fa fa-calendar"></i></label>
                                                            </div>
                                                    <div class="col-lg-12">
                                                                <div class="form-group" id="comenError">
                                                                    <textarea name="form[alphaNum][comen]" id="comen" cols="30"  class="form-control" {if isset($ver) and $ver==1} disabled {/if}
                                                                              rows="5">{if isset($formDB.ind_comentario)}{$formDB.ind_comentario}{/if}</textarea>
                                                                    <label for="comen"><i class="md md-border-color"></i>Comentario</label>
                                                                </div>
                                                            </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div><!--end #tab1 -->
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="col-lg-12">
                                        <div class="card-head style-primary">
                                            <header>Items</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="col-lg-6">
                                                    <div class="form-group floating-label">
                                                        <label for="itemBuscar">Buscar Item</label>
                                                        <button {if isset($ver) and $ver==1} disabled {/if}
                                                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                type="button"
                                                                title="Buscar Item"
                                                                id="nuevoItem"
                                                                data-toggle="modal" data-target="#formModal2"
                                                                data-keyboard="false" data-backdrop="static"
                                                                titulo="Buscar Item"
                                                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/item2/"
                                                                >
                                                            <i class="md md-search" style="color: #ffffff;"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <table class="table table-striped no-margin" id="contenidoTabla" style="white-space:nowrap;overflow-x:scroll;overflow-y:scroll;width:100%;height:250px;display: inline-block;">
                                                    <thead>
                                                    <tr>
                                                        <th width="10px">#<i style="color: #ffffff">_______</i></th>
                                                        <th width="150px">Item<i style="color: #ffffff">______________________________________________________________</i></th>
                                                        <th width="50px">Stock anterior</th>
                                                        <th width="50px">Cant. Transacción<i style="color: #ffffff">_____</i></th>
                                                        <th width="50px">Precio Uni.<i style="color: #ffffff">___________</i></th>
                                                        <th width="50px">Num. Uni.</th>
                                                        <!--th width="5px">Num. Transacción</th-->
                                                        <th width="50px" align="right">Borrar</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="item">
                                                    {if isset($formDBI)}
                                                        {$cant=0}
                                                        {foreach item=i from=$formDBI}
                                                            {$cant=$cant+1}
                                                            <tr id="item{$i.fk_lgb002_num_item}">
                                                                <input type="hidden" name="form[int][numItem][]" value="{$i.fk_lgb002_num_item}">
                                                                <td width="1px"><input type="text" class="form-control" readonly value="{$i.num_secuencia}"></td>
                                                                <td width="15px"><input type="text" class="form-control" name="descripcion[]" readonly size="5" value="{$i.ind_descripcion_ic}"></td>
                                                                <td width="5px"><input type="text" class="form-control" name="form[int][sAnt][]" value="{$i.num_stock_anterior|number_format:2:'.':''}" readonly></td>
                                                                <td width="5px"><input type="text" class="form-control" name="form[int][canti][]" size="1" value="{$i.num_cantidad_transaccion|number_format:2:'.':''}"{if isset($ver) and $ver==1} disabled {/if}></td>
                                                                <td width="5px"><input type="text" class="form-control" name="form[int][precio][]" size="2" value="{$i.num_precio_unitario|number_format:2:'.':''}"{if isset($ver) and $ver==1} disabled {/if}></td>
                                                                <td width="7px"><input type="text" class="form-control" readonly name="form[alphaNum][unidadtxt][]" size="2" value="{$i.unidadItem}"></td>
                                                                <!--td width="10px"><input type="text" class="form-control" readonly size="1" value="{$i.fk_lgd001_num_transaccion}"></td-->
                                                                <td width="5px" class="text-center" style="vertical-align: middle;">
                                                                    <input type="hidden" name="form[int][idDet][]" value="{$i.pk_num_transaccion_detalle}">
                                                                    <input type="hidden" name="form[int][cant]" value="{$cant}">
                                                                    <input type="hidden"  name="form[int][numUni][]" value="{$i.fk_lgb004_num_unidad}">

                                                                <div>
                                                                        <button class="borrar btn ink-reaction btn-raised btn-xs btn-danger" {if isset($ver) and $ver==1} disabled {/if}
                                                                                url="{$_Parametros.url}modLG/almacen/transaccionCONTROL/eliminarDetalleMET/"
                                                                                id="{$i.pk_num_transaccion_detalle}"
                                                                                secuencia="item{$i.num_secuencia}">
                                                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                                                        </button>
                                                                    </div>
                                                                </td>

                                                            </tr>
                                                        {/foreach}
                                                    {/if}

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--end #tab2 -->
                                <!--end .tab-content -->
                                <ul class="pager wizard">
                                    <!--<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>-->
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <!--<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>-->
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                </ul>
                            </div><!--END ClearFix-->
                        </div><!--end #rootwizard1 -->
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .modal-body -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idTran) and !isset($cual)}
                    <span class="fa fa-edit"></span> Modificar
                {elseif isset($cual)}
                    <span class="md md-keyboard-return"></span> Reversar
                {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "80%");
    $("#formAjax").submit(function(){
        return false;
    });
    $('.select2').select2({ allowClear: true });
    $(document).ready(function () {
        $('#modalAncho').css("width", "80%");
        var app = new  AppFunciones();
        $('#fdoc').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0, tr:$("#contenidoTabla > tbody > tr").length+1 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson','La Transaccion fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorItem'){
                    app.metValidarError(dato,'Disculpa. Debe seleccionar items para la transacción');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='periodo'){
                    swal("Error!", "No hay un periodo activo para realizar transacciones.", "error");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson','La Transaccion fue guardada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

        $('#item').on('click', '.borrar' ,function () {
            $('.'+$(this).attr('secuencia')).remove();
            $.post($(this).attr('url'), { id: $(this).attr('id')}, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });


        $('#item').on('click', '.eliminar' ,function () {
            var id = $('#numItem'+$(this).attr('id')).attr('value');
            $('#item'+id).remove();
        });
    });
</script>