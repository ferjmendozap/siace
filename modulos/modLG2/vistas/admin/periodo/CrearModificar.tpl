<form action="{$_Parametros.url}modLG/admin/periodoCONTROL/crearModificarPeriodoMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idPeriodo)}{$idPeriodo}{/if}" name="idPeriodo"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group floating-label col-lg-3">
                    <label class="checkbox-inline checkbox-styled">
                        <span>Transaccion </span>
                        <input type="checkbox" name="form[int][tran]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDBPeriodo.num_flag_transaccion) and $formDBPeriodo.num_flag_transaccion==1}{"checked"}{/if}>
                    </label>
                </div>
                <div class="form-group floating-label col-lg-3">
                    <label class="checkbox-inline checkbox-styled">
                        <span>Estatus</span>
                        <input type="checkbox" name="form[txt][estado]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDBPeriodo.num_estatus) and $formDBPeriodo.num_estatus=='1'}{"checked"}{/if}>
                    </label>
                </div>
                <div class="form-group col-lg-6" id="periodoError">
                    <input type="text" readonly class="form-control" id="periodo" {if isset($ver) and $ver==1} disabled {/if} value="{if isset($formDBPeriodo.fec_mes) and isset($formDBPeriodo.fec_anio)}{$formDBPeriodo.fec_anio}-{$formDBPeriodo.fec_mes}{/if}" name="form[txt][periodo]">
                    <label for="periodo"><i class="md md-border-color"></i>Periodo</label>
                </div>
                <div class="form-group floating-label col-lg-6" id="usuPeriodoError">
                    <input type="text" class="form-control disabled" id="usuPeriodo" disabled value="{if isset($formDBPeriodo.ind_usuario)}{$formDBPeriodo.ind_usuario}{/if}">
                    <label for="usuPeriodo"><i class="md md-border-color"></i>Ultimo Usuario</label>
                </div>
                <div class="form-group floating-label col-lg-6" id="fecPeriodoError">
                    <input type="text" class="form-control disabled" id="fecPeriodo" disabled value="{if isset($formDBPeriodo.fec_ultima_modificacion)}{$formDBPeriodo.fec_ultima_modificacion}{/if}">
                    <label for="fecPeriodo"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idPeriodo)}
                    <span class="fa fa-edit"></span> Modificar
                {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $("#formAjax").submit(function(){
        return false;
    });
    $('#periodo').datepicker({ autoclose: true, minViewMode: 1, todayHighlight: true, format: "yyyy-mm", language:'es' });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('#modalAncho').css("width", "40%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Periodo fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. El periodo ya existe, coloque otro');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Periodo fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
    });
</script>