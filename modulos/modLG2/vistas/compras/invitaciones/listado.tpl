<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listar Invitaciones de Cotizaciones</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>F. Invitación</th>
                            <th># Invitación</th>
                            <th>Proveedor</th>
                            <th>Razón Social</th>
                            <th>Total Cotizado</th>
                            <th>Líneas</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <!--tbody>
                        {foreach item=post from=$listado}
                            {$nroLineas=0}

                            <tr id="">
                                <td>{$post.fec_invitacion}</td>
                                <td>{$post.ind_num_invitacion}</td>
                                <td>{$post.fk_lgb022_num_proveedor}</td>
                                <td>{$post.proveedor}</td>
                                <td>{$post.totalCotizado}</td>
                                <td>{$post.nroLineas}</td>
                                <td>{$post.ind_estado}</td>
                                <td>
                                    {if in_array('LG-01-01-10-01-I',$_Parametros.perfil)}
                                        <a href="{$_Parametros.url}modLG/compras/invitacionesCONTROL/generarInvitacionMET/{$post.pk_num_invitacion}" target="_blank">
                                            <button title="Imprimir Invitación"
                                                    class="accion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary-light"
                                                    titulo="Imprimir Invitación"
                                                    id="imprimir">
                                                <i class="md md-print"></i>
                                            </button>
                                        </a>
                                    {/if}
                                    {if in_array('LG-01-01-10-02-A',$_Parametros.perfil) AND $post.ind_estado=='PR' AND $post.pk_num_cotizacion==NULL}
                                        <button title="Anular Invitación" id="anular{$post.pk_num_invitacion}"
                                                class="anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                titulo="Anular Invitación"
                                                mensaje="Desea Anular esta Invitacion?"
                                                idInvitacion="{$post.pk_num_invitacion}">
                                            <i class="md md-block"></i>
                                        </button>
                                    {/if}
                                    {if in_array('LG-01-01-10-03-C',$_Parametros.perfil)}
                                        <button title="Cotizar" data-toggle="modal" data-target="#formModal"
                                                class="cotizar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                titulo="Cotizar por Invitación a Proveedor"
                                                numInvitacion="{$post.ind_num_invitacion}"
                                                anio="{$post.fec_anio}">
                                            <i class="md md-attach-money"></i>
                                        </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody-->
                    </table>

                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    $(document).ready(function () {
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/compras/invitacionesCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_invitacion" },
                    { "data": "fec_invitacion" },
                    { "data": "ind_num_invitacion" },
                    { "data": "fk_lgb022_num_proveedor" },
                    { "data": "proveedor"},
                    { "data": "totalCotizado"},
                    { "data": "nroLineas"},
                    { "data": "ind_estado"},
                    { "orderable": false,"data": "acciones", width:150}
                ]
        );

        $('#dataTablaJson tbody').on( 'click', '.anular', function () {
            var idInvitacion = $(this).attr('idInvitacion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){

                var url='{$_Parametros.url}modLG/compras/invitacionesCONTROL/anularInvitacionMET';
                $.post(url, { idInvitacion: idInvitacion },function(dato){
                    if(dato['status']=='ok'){
//                        $('#dataTablaJson').dataTable().api().row().remove().draw(false);
                        swal("Anulado!", "La Invitacion fue anulada satisfactoriamente.", "success");
                        $('#anular'+dato['id']).remove();
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });


        $('#dataTablaJson tbody').on( 'click', '.cotizar', function () {

            $('#formModalLabel').html($(this).attr('titulo'));

            var url = '{$_Parametros.url}modLG/compras/invitacionesCONTROL/cotizarMET';
            $.post(url, { numInvitacion: $(this).attr('numInvitacion'), anio: $(this).attr('anio')  } , function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
    });
</script>