<form action="{$_Parametros.url}modLG/compras/invitacionesCONTROL/cotizarMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($numInivtacion)}{$numInivtacion}{/if}" name="numInivtacion"/>
    <input type="hidden" value="{if isset($anio)}{$anio}{/if}" name="anio"/>
    <div class="modal-body">
        <div class="row">

            <div class="panel-group floating-label" id="accordion1">
                <div class="card panel">
                    <div class="card-head style-primary collapsed" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion1-1">
                        <header>Información General</header>
                        <div class="tools">
                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                        </div>
                    </div>
                    <div id="accordion1-1" class="collapse">
                        <div class="card-body">
                            <div class="col-lg-12">
                                <div class="form-group col-lg-7" id="proveedorError">
                                    <input disabled id="proveedor" class="form-control" value="{$cotizaciones[0].nombre}">
                                    <label for="proveedor"><i class="md md-border-color"></i> Proveedor:</label>
                                </div>
                                <div class="form-group col-lg-2" id="formaPagoError">
                                    <select id="formaPago" name="form[int][formaPago]" class="select2 select2-list">
                                        {foreach item=fp from=$formaPago}
                                            {if $cotizaciones[0].fk_a006_num_miscelaneo_detalle_forma_pago==$fp.pk_num_miscelaneo_detalle}
                                                <option value="{$fp.pk_num_miscelaneo_detalle}" selected>{$fp.ind_nombre_detalle}</option>
                                            {else}
                                                <option value="{$fp.pk_num_miscelaneo_detalle}">{$fp.ind_nombre_detalle}</option>
                                            {/if}
                                        {/foreach}
                                    </select>
                                    <label for="formaPago">Forma de Pago:</label>
                                </div>
                                <div class="form-group col-lg-3" id="fechaInviError">
                                    <input disabled id="fechaInvi" class="form-control" value="{$cotizaciones[0].fec_invitacion}">
                                    <label for="fechaInvi"><i class="fa fa-calendar"></i> Fecha Invitación:</label>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group col-lg-3" id="fentregaError">
                                    <input readonly id="fentrega" class="form-control fecha" name="form[txt][fentrega]" value="{$cotizaciones[0].entrega}">
                                    <label for="fentrega"><i class="fa fa-calendar"></i> Fecha Entrega:</label>
                                </div>
                                <div class="form-group col-lg-3" id="fechaAperturaError">
                                    <input readonly id="fechaApertura" class="form-control fecha" name="form[txt][fechaApertura]" value="{$cotizaciones[0].apertura }">
                                    <label for="fechaApertura"><i class="fa fa-calendar"></i> Fecha Apertura:</label>
                                </div>
                                <div class="form-group col-lg-3" id="frecepcionError">
                                    <input readonly id="frecepcion" class="form-control fecha" name="form[txt][frecepcion]" value="{if isset($cotizaciones[0].fec_recepcion)}{$cotizaciones[0].fec_recepcion}{else}{date('Y-m-d')}{/if}">
                                    <label for="frecepcion"><i class="fa fa-calendar"></i> Fecha Recepción:</label>
                                </div>
                                <div class="form-group col-lg-3" id="fechaLimiteError">
                                    <input disabled id="fechaLimite" class="form-control" value="{$cotizaciones[0].fec_limite}">
                                    <label for="fechaLimite"><i class="fa fa-calendar"></i> Fecha Límite:</label>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group col-lg-3" id="dPorError">
                                    <input id="dPor" class="form-control" name="form[int][dPor]" value="{number_format($cotizaciones[0].num_descuento_porcentaje,0)}" maxlength="3">
                                    <label for="dPor"><i class="md md-border-color"></i> Descuento (%):</label>
                                </div>
                                <div class="form-group col-lg-3" id="dFijoError">
                                    <input id="dFijo" class="form-control"  name="form[int][dFijo]" value="{number_format($cotizaciones[0].num_descuento_fijo,6)}" maxlength="18" >
                                    <label for="dFijo"><i class="md md-border-color"></i> Descuento (Monto):</label>
                                </div>
                                <div class="form-group col-lg-3" id="voferError">
                                    <input id="vofer" class="form-control"  name="form[int][vofer]" value="{$cotizaciones[0].num_oferta}">
                                    <label for="vofer"><i class="md md-border-color"></i> Validez Oferta:</label>
                                </div>
                                <div class="form-group col-lg-3" id="diasError">
                                    <input id="dias" class="form-control" name="form[int][dias]" value="{$cotizaciones[0].num_dias}">
                                    <label for="dias"><i class="md md-border-color"></i> Dias Entrega:</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-group floating-label" id="accordion2">
                <div class="card panel">
                    <div class="card-head style-primary collapsed" data-toggle="collapse" data-parent="#accordion1" data-target="#accordion2-1">
                        <header>Items / Commodities</header>
                        <div class="tools">
                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                        </div>
                    </div>
                    <div id="accordion2-1" class="collapse">
                        <div class="card-body">
                            <div class="card-body">
                                <table class="table table-striped table-bordered" style="white-space:nowrap;overflow-x:scroll;overflow-y:scroll;width:100%;height:250px;display: inline-block;" id="tabla">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item / Commodity</th>
                                        <th>Descripción<i style="color: #ffffff">________________________</i></th>
                                        <th>Unidad Recepción<i style="color: #ffffff">________________________</i></th>
                                        <th>Unidad Compra<i style="color: #ffffff">______</i></th>
                                        <th>Cantidad Comprada</th>
                                        <th>Precio Unitario<i style="color: #ffffff">___</i></th>
                                        <th>Asignado</th>
                                        <th>Exonerado IVA</th>
                                        <th>Precio con IVA<i style="color: #ffffff">____</i></th>
                                        <th>Descuento %<i style="color: #ffffff">_______</i></th>
                                        <th>Descuento Fijo<i style="color: #ffffff">____</i></th>
                                        <th>Total<i style="color: #ffffff">_____________</i></th>
                                        <th>Observación<i style="color: #ffffff">________________________________________________</i></th>
                                    </tr>
                                    </thead>
                                    <tbody id="cotizaciones">
                                    {$c=0}
                                    {foreach item=detalle from=$cotizaciones}
                                        {$unidad=''}
                                        {foreach item=uni from=$unidades}
                                            {if $uni.pk_num_unidad==$detalle.fk_lgb004_num_unidad_compra OR
                                            $uni.pk_num_unidad==$detalle.fk_lgb004_num_unidad}
                                                {$unidad=$uni.ind_descripcion}
                                            {/if}
                                        {/foreach}
                                        {$c=$c+1}
                                        {$asignado=''}
                                        {if $detalle.num_flag_asignado==1}
                                            {$asignado='checked'}
                                        {/if}
                                        {$exo=''}
                                        {if $detalle.exonerado==1}
                                            {$exo='checked'}
                                        {/if}
                                        {$sugerido=''}
                                        {if $detalle.num_flag_elegido_sistema==1}
                                            {$sugerido='checked'}
                                        {/if}
                                        <tr>
                                            {$idCotizacion = 0}
                                            {if isset($detalle.pk_num_cotizacion)}
                                                {$idCotizacion = $detalle.pk_num_cotizacion}
                                            {/if}
                                            {$idActaDet = 0}
                                            {if isset($detalle.pk_num_acta_detalle)}
                                                {$idActaDet = $detalle.pk_num_acta_detalle}
                                            {/if}
                                            <input type="hidden" name="form[int][idInvitacion][{$c}]" value="{$detalle.pk_num_invitacion}">
                                            <input type="hidden" name="form[int][idCotizacion][{$c}]" value="{$idCotizacion}">
                                            <input type="hidden" name="form[int][idActaDet][{$c}]" value="{$idActaDet}">
                                            <td style="vertical-align: top;">{$c}</td>
                                            <td><input type="text" class="form-control" disabled value="{if isset($detalle.fk_lgb002_num_item)}{$detalle.fk_lgb002_num_item}{else}{$detalle.fk_lgb003_num_commodity}{/if}"></td>
                                            <td><input type="text" class="form-control" disabled value="{$detalle.descripcion}"></td>
                                            <td><input type="text" class="form-control" disabled value="{$unidad}"></td>
                                            <td style="vertical-align:middle;">
                                                <select class="form-control select2 select2-list" name="form[int][uniCompra][{$c}]">
                                                    {foreach item=uni from=$unidades}
                                                        {if $uni.pk_num_unidad==$detalle.unidadCompra}
                                                            <option value="{$uni.pk_num_unidad}" selected>{$uni.ind_descripcion}</option>
                                                        {else}
                                                            <option value="{$uni.pk_num_unidad}">{$uni.ind_descripcion}</option>
                                                        {/if}
                                                    {/foreach}
                                                </select>
                                            </td>
                                            <td><input type="text" class="form-control" readonly id="total{$c}cantidad" name="form[int][cPedida][{$c}]" value="{$detalle.num_cantidad_pedida}"></td>
                                            <td><input type="text" class="form-control" onblur="calcular()" onclick="validarCampo({$c},1)" id="total{$c}pUni" name="form[int][pUni][{$c}]" value="{$detalle.num_precio_unitario}"></td>

                                            <td style="vertical-align: top;">
                                                <div align="center" class="checkbox checkbox-styled">
                                                    <label>
                                                        <input type="checkbox" {$asignado} name="form[int][asignado][{$c}]" c="{$c}" value="1"><span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td style="vertical-align: top;">
                                                <div align="center" class="checkbox checkbox-styled">
                                                    <label>
                                                        <input type="checkbox" class="exo" {$exo} onclick="calcular()" name="form[int][exo][{$c}]" id="total{$c}exo" value="1"><span></span>
                                                    </label>
                                                </div>
                                            </td><td><input readonly type="text" class="form-control" name="form[int][pIVA][{$c}]" id="total{$c}pIVA" value="{$detalle.num_precio_unitario_iva}"></td>
                                            <td><input type="text" class="form-control dpor" onblur="calcular()" onclick="validarCampo({$c},2)" name="form[int][dpor][{$c}]" id="total{$c}dpor" value="{$detalle.num_descuento_porcentaje}"></td>
                                            <td><input type="text" class="form-control dfijo" onblur="calcular()" onclick="validarCampo({$c},3)" name="form[int][dfijo][{$c}]" id="total{$c}dfijo" value="{$detalle.num_descuento_fijo}"></td>
                                            <td><input readonly type="text" class="form-control total" name="form[int][total][{$c}]" id="total{$c}total" sec="total{$c}" value="{$detalle.num_total}" c="{$c}"></td>
                                            <td><input type="text" class="form-control" name="form[alphaNum][observacion][{$c}]" value="{$detalle.ind_observacion}"></td>
                                        </tr>
                                    {/foreach}
                                    </tbody>
                                    <input type="hidden" name="form[int][secuencia]" value="{$c}">
                                </table>
                            </div><!--END card-body-->
                        </div>
                    </div>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">

    function cambiarValores(clase,valor) {
        $('.'+clase).attr('value',valor);
    }

    $(":input").inputmask();
    $("#dFijo").inputmask('999.999.999,999999',{ rightAlignNumerics: true,numericInput: true });
    $("#dPor").inputmask('999',{ rightAlignNumerics: true,numericInput: true });
    $("#vofer").inputmask('999',{ rightAlignNumerics: true,numericInput: true });
    $("#dias").inputmask('999',{ rightAlignNumerics: true,numericInput: true });
    $('.fecha').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

    function buscarSugerido() {
        var menor = 9999999999999999999999999999999999999999;
        $('.total').each(function () {
            var valor = $(this).val();
            var i = $(this).attr('c');
            if(parseFloat(valor)<parseFloat(menor)){
                menor = valor;
                $('.sugerido').attr('checked',false);
                $('#sugerido'+i).attr('checked',true);
            }
        });
    }

    function calcular() {
        $('.total').each(function( titulo, valor ){
            var pIVA = 0;
            var total = 0;
            var descuento = 0;
            var id=$(this).attr('sec');
            var cantidad = $('#'+id + 'cantidad').val();
            var dpor = $('#'+id + 'dpor').val();
            var dfijo = $('#'+id + 'dfijo').val();
            var precio = $('#'+id + 'pUni').val();

            if(precio==''){
                precio=0;
                $('#'+id + 'pUni').val(0);
            }

            if($('#'+ id + 'exo').is(':checked')){
                pIVA = precio;
            }else{
                pIVA = parseFloat(precio) + parseFloat(precio*0.12);
            }

            if (dpor!='0' && dfijo!='0.00') {
                dpor='0';
                dfijo='0.00';
            } else if (dpor!='0' && dpor < 100) {
                descuento = parseFloat(precio/100)*dpor;
            } else if (dfijo!='0.00' && dfijo < precio) {
                descuento = dfijo;
            } else {
                dpor='0';
                dfijo='0.00';
            }
            var subtotal = pIVA - descuento;
            if(cantidad==0){
                total = '0,00';
            } else {
                total = (parseFloat(cantidad) * subtotal).toFixed(2);
            }
            $('#'+id + 'pIVA').val(pIVA);
            $('#'+id + 'dpor').val(dpor);
            $('#'+id + 'dfijo').val(dfijo);
            $('#'+id + 'total').val(total);
        });
        buscarSugerido();
    }

    function validarCampo(nombre,cual) {

        if(cual==1){
            var id = '#total'+nombre+'pUni';
        } else if(cual==2){
            id = '#total'+nombre+'dpor';
            cual = 1;
        } else if(cual==3){
            id = '#total'+nombre+'dfijo';
            cual = 2;
        }

        if ($(id).val()=='' && cual==1){
            var cantidad = 0;
        } else if ($(id).val()=='' && cual==2){
            cantidad = '0.00';
        } else if($(id).val()!='') {
            cantidad = '';
        }
        $(id).val(cantidad);
    }
    $("#formAjax").submit(function(){
        return false;
    });
    $('.select2').select2({ allowClear: true });

    $(document).ready(function () {
        var app = new  AppFunciones();
        $('#modalAncho').css("width", "80%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    swal('Exito!','Se han registrado las Cotizaciones correctamente','success');
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
//                    app.metNuevoRegistroTabla(dato, dato['id'], 'idPeriodo', arrayCheck, arrayMostrarOrden, 'Se ha Creado el Desierto satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
        $('#dFijo').on('change',function () {
            var valor = $(this).val();
            var valor2 = '';
            for(var i=0; i<valor.length; i++) {
                if(valor.charAt(i)!='_' && valor.charAt(i)!='.'){
                    if(valor.charAt(i)==','){
                        valor2 += '.';
                    } else {
                        valor2 += valor.charAt(i);
                    }
                }
            }
            cambiarValores('dfijo',valor2);
            calcular();
        });
        $('#dPor').on('change',function () {
            var valor = $(this).val();
            var valor2 = '';
            for(var i=0; i<valor.length; i++) {
                if(valor.charAt(i)!='_'){
                    valor2 += valor.charAt(i);
                }
            }
            cambiarValores('dpor',valor2);
            calcular();
        });
    });
</script>