<form action="{$_Parametros.url}modLG/compras/requerimientoCONTROL/anularMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idReq)}{$idReq}{/if}" name="idReq"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                <div class="col-lg-12">
                    <div class="form-group floating-label col-lg-12" id="motivoError">
                        <textarea id="motivo" class="form-control" cols="50" rows="2" name="form[alphaNum][motivo]">{if isset($eval.ind_conclusion)}{$eval.ind_conclusion}{/if}</textarea>
                        <label for="motivo"><i class="md md-border-color"></i>
                            Motivo de la anulacion del Requerimiento Nro. {if isset($codigo)}{$codigo}{/if}
                        </label>
                    </div>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
            <span class="glyphicon glyphicon-floppy-disk"></span>
                Anular
        </button>
    </div>
</form>

<script type="text/javascript">
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        var app = new  AppFunciones();

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    swal("Anulado!", 'El Requerimiento fue Anulado satisfactoriamente.', "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('acta'+dato['idReq'])).attr('disabled',true);
                    $(document.getElementById('anular'+dato['idReq'])).attr('disabled',true);
                    $(document.getElementById('estado'+dato['idReq'])).attr('disabled',true);
                    $(document.getElementById('modificar'+dato['idReq'])).attr('disabled',true);
                }
            }, 'json');
        });
    });
</script>