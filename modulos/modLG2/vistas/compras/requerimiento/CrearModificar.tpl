<form action="{$_Parametros.url}modLG/compras/requerimientoCONTROL/crearModificarRequerimientoMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idReq)}{$idReq}{/if}" name="idReq"/>
    <input type="hidden" value="{if isset($estado)}{$estado}{/if}" name="estado" id="estado"/>
    <input type="hidden" value="{if isset($revisar)}{$revisar}{/if}" name="revisar"/>
    <input type="hidden" value="{if isset($formDBR.fk_a023_num_centro_costo)}{$formDBR.fk_a023_num_centro_costo}{else}{$predeterminado.fk_a023_num_centro_costo}{/if}" id="cc"/>
    <input type="hidden" value="{if isset($formDBR.fk_a004_num_dependencia)}{$formDBR.fk_a004_num_dependencia}{else}{$predeterminado.fk_a004_num_dependencia}{/if}" id="idDep"/>
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span>  <br/><span class="title">INFORMACIÓN GENERAL</span> </a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <br/> <span class="title">DETALLES</span> </a></li>
                                    <li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <br/> <span class="title">DISTRIBUCIÓN PRESUPUESTARIA/CONTABLES</span> </a></li>
                                    <!--li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <br/> <span class="title">COTIZACIONES</span> </a></li>
                                    <li><a href="#tab4" data-toggle="tab"><span class="step">4</span> <br/> <span class="title">AVANCES</span> </a></li-->
                                </ul>
                            </div><!--end .form-wizard-nav -->
                            <div class="tab-content clearfix">
                                <div class="tab-pane active floating-label" id="tab1">
                                    <div class="col-lg-12">
                                        <!--inicio de acordeon -->
                                        <div class="panel-group floating-label" id="accordion1">
                                            <div class="card panel">
                                                <div class="card-head style-primary" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion1-1">
                                                    <header>Información General</header>
                                                    <div class="tools">
                                                        <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                                    </div>
                                                </div>
                                                <div id="accordion1-1">
                                                    <div class="card-body">
                                                        <div class="col-lg-6">
                                                            <div class="form-group col-lg-12" id="dependenciaError">
                                                                <select id="dependencia" name="form[int][dependencia]" class="form-control select2-list select2" {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} disabled {/if}>
                                                                    <option value=""></option>
                                                                    {foreach item=dep from=$dependencia}
                                                                        {if $formDBR.fk_a004_num_dependencia==$dep.pk_num_dependencia}
                                                                            <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                                                        {elseif $predeterminado.fk_a004_num_dependencia==$dep.pk_num_dependencia AND !isset($formDBR.fk_a004_num_dependencia)}
                                                                            <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                                                        {else}
                                                                            <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="dependencia">Dependencia</label>
                                                            </div>
                                                            <div class="form-group col-lg-12" id="centro_costoError">
                                                                <label for="centro_costo">Centro de Costo</label>
                                                                <select id="centro_costo" name="form[int][centro_costo]" class="form-control select2-list select2" {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} disabled {/if}>
                                                                    <option value=""></option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-lg-12" id="clasificacionError">
                                                                <input type="hidden" id="clasificacion_id" value="{if isset($formDBR.fk_lgb017_num_clasificacion)}{$formDBR.fk_lgb017_num_clasificacion}{/if}">
                                                                <select id="clasif" name="form[int][clasificacion]" class="form-control select2-list select2" {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} disabled {/if}>
                                                                    {foreach item=clasificacion from=$clas}
                                                                        {if $clasificacion.ind_descripcion=='REQUERIMIENTO DE AUTO REPOSICION' AND isset($formDBR.fk_lgb017_num_clasificacion) and $formDBR.fk_lgb017_num_clasificacion == $clasificacion.pk_num_clasificacion}
                                                                            <option id="valor{$clasificacion.pk_num_clasificacion}" selected value="{$clasificacion.pk_num_clasificacion}" recepcionAlmacen="{$clasificacion.num_flag_recepcion_almacen}">{$clasificacion.ind_descripcion}</option>
                                                                        {elseif $clasificacion.ind_descripcion!='REQUERIMIENTO DE AUTO REPOSICION'}
                                                                            {if isset($formDBR.fk_lgb017_num_clasificacion) and $formDBR.fk_lgb017_num_clasificacion == $clasificacion.pk_num_clasificacion}
                                                                                <option id="valor{$clasificacion.pk_num_clasificacion}" value="{$clasificacion.pk_num_clasificacion}" recepcionAlmacen="{$clasificacion.num_flag_recepcion_almacen}" selected>{$clasificacion.ind_descripcion}</option>
                                                                            {elseif $clasificacion.ind_descripcion=='STOCK DE ALMACEN' AND !isset($formDBR.fk_lgb017_num_clasificacion)}
                                                                                <option id="valor{$clasificacion.pk_num_clasificacion}" value="{$clasificacion.pk_num_clasificacion}" recepcionAlmacen="{$clasificacion.num_flag_recepcion_almacen}" selected>{$clasificacion.ind_descripcion}</option>
                                                                            {else}
                                                                                <option id="valor{$clasificacion.pk_num_clasificacion}" value="{$clasificacion.pk_num_clasificacion}" recepcionAlmacen="{$clasificacion.num_flag_recepcion_almacen}">{$clasificacion.ind_descripcion}</option>
                                                                            {/if}
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="clasif">Clasificación</label>
                                                            </div>
                                                            <div class="form-group col-lg-12" id="almacenError">
                                                                <label for="almacen">Almacen</label>
                                                                <input type="hidden" name="form[int][almacen]" id="almacen_id" value="{if isset($formDBR.fk_a006_num_miscelaneos_almacen)}{$formDBR.fk_a006_num_miscelaneos_almacen}{/if}">
                                                                <select id="almacen" class="form-control select4-list select4" {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} disabled {/if}>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-lg-12" id="prioridadError">
                                                                <select id="prioridad" name="form[int][prioridad]" class="form-control select2-list select2" {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} disabled {/if}>
                                                                    <option value=""></option>
                                                                    {foreach item=p from=$prioridad}
                                                                        {if isset($formDBR.fk_a006_num_miscelaneos_prioridad) and $formDBR.fk_a006_num_miscelaneos_prioridad == $p.pk_num_miscelaneo_detalle}
                                                                            <option value="{$p.pk_num_miscelaneo_detalle}" selected>{$p.ind_nombre_detalle}</option>
                                                                        {elseif !isset($formDBR.fk_a006_num_miscelaneos_prioridad) AND $p.ind_nombre_detalle=="Normal"}
                                                                            <option value="{$p.pk_num_miscelaneo_detalle}" selected>{$p.ind_nombre_detalle}</option>
                                                                        {else}
                                                                            <option value="{$p.pk_num_miscelaneo_detalle}">{$p.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                </select>
                                                                <label for="prioridad">Prioridad</label>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group" id="comentarioRError">
                                                                    <textarea class="form-control" name="form[alphaNum][comentarioR]" id="comentarioR" cols="30" rows="7" {if $estado=='revisar' or $estado=='aprobar' or $estado=="ver"} disabled {/if}>{if isset($formDBR.ind_comentarios)}{$formDBR.ind_comentarios}{/if}</textarea>
                                                                    <label for="comentarioR"><i class="md md-border-color"></i>Comentario</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label class="checkbox-inline checkbox-styled">
                                                                    <input type="checkbox" value="1" name="form[int][fcajac]" {if isset($formDBR.ind_verificacion_caja_chica) and $formDBR.ind_verificacion_caja_chica==1}checked{/if} {if $revisar!=1 or $estado=="ver"} disabled {/if}>
                                                                    <span>Req. para Caja Chica </span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <label class="checkbox-inline checkbox-styled">
                                                                    <input type="checkbox" value="1" name="form[int][fpartida]" {if isset($formDBR.ind_verificacion_presupuestaria) and $formDBR.ind_verificacion_presupuestaria==1}checked{/if} {if $revisar!=1 or $estado=="ver"} disabled {/if}>
                                                                    <span>Revision de Partidas</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <input type="text" disabled id="fcaja" class="form-control" value="{if isset($formDBR.fec_verificacion_caja_chica)}{$formDBR.fec_verificacion_caja_chica}{/if}">
                                                                    <label for="fcaja">Fecha de Verif. Caja<i class="fa fa-calendar"></i></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <input type="text" disabled id="persona8" class="form-control disabled" value="{if isset($formDBR.despNom)}{$formDBR.despNom} {$formDBR.despApe}{/if}">
                                                                    <label for="preparado"><i class="md md-people"></i>Emp. Apr. Despacho</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <input type="text" disabled id="persona1" class="form-control disabled" value="">
                                                                    <label for="preparado"><i class="md md-people"></i>Emp. Verif. Caja Chica</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <input type="text" disabled id="persona2" class="form-control disabled" value="">
                                                                    <label for="preparado"><i class="md md-people"></i>Emp. Verif. Part. Pres.</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <div class="form-group">
                                                                        <input type="text" readonly value="{if isset($formDBR.cod_requerimiento)}{$formDBR.cod_requerimiento}{/if}" {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} disabled {/if} class="form-control">
                                                                        <label>Código</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12" id="preparadoError">
                                                                <div class="form-group">
                                                                    <input type="text" disabled id="persona3" class="form-control disabled" value="{if isset($empPrepara.ind_nombre1)}{$empPrepara.ind_nombre1} {$empPrepara.ind_apellido1}{/if}">
                                                                    <label for="preparado"><i class="md md-people"></i>Preparado por</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12" id="fprepError">
                                                                <div class="form-group">
                                                                    <input type="text" readonly id="fprep" class="form-control disabled" {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} disabled {/if} value="{if isset($formDBR.fec_preparacion)}{$formDBR.fec_preparacion}{/if}">
                                                                    <label for="fprep">Fecha de Prep.<i class="fa fa-calendar"></i></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12" id="revisadoError">
                                                                <div class="form-group">
                                                                    <input type="text" disabled id="persona4" class="form-control disabled" value="{if isset($empRevisa.ind_nombre1)}{$empRevisa.ind_nombre1} {$empRevisa.ind_apellido1}{/if}">
                                                                    <label for="revisado"><i class="md md-people"></i>Revisado por</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12" id="frevError">
                                                                <div class="form-group">
                                                                    <input type="text" readonly id="frev" class="form-control disabled" {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} disabled {/if} value="{if isset($formDBR.fec_revision)}{$formDBR.fec_revision}{/if}">
                                                                    <label for="frev">Fecha de Rev.<i class="fa fa-calendar"></i></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12" id="conforError">
                                                                <div class="form-group">
                                                                    <input type="text" disabled id="persona5" class="form-control disabled" value="{if isset($empConforma.ind_nombre1)}{$empConforma.ind_nombre1} {$empConforma.ind_apellido1}{/if}">
                                                                    <label for="confor"><i class="md md-people"></i>Conformado por</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12" id="fconforError">
                                                                <div class="form-group">
                                                                    <input type="text" readonly id="fconfor" class="form-control disabled" {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} disabled {/if} value="{if isset($formDBR.fec_conformacion)}{$formDBR.fec_conformacion}{/if}">
                                                                    <label for="fconfor">Fecha de Confor.<i class="fa fa-calendar"></i></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12" id="aprobadoError">
                                                                <div class="form-group">
                                                                    <input type="text" disabled id="persona6" class="form-control disabled" value="{if isset($empAprueba.ind_nombre1)}{$empAprueba.ind_nombre1} {$empAprueba.ind_apellido1}{/if}">
                                                                    <label for="aprobado"><i class="md md-people"></i>Aprobado por</label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <input type="text" readonly id="fapro" class="form-control disabled" value="{if isset($formDBR.fec_aprobacion)}{$formDBR.fec_aprobacion}{/if}">
                                                                    <label for="fapro">Fecha de Apro.<i class="fa fa-calendar"></i></label>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <textarea name="form[alphaNum][rechazo]" class="form-control" id="rechazo" cols="30" disabled
                                                                              rows="8">{if isset($formDBR.ind_razon_rechazo)}{$formDBR.ind_razon_rechazo}{/if}</textarea>
                                                                    <label for="rechazo">Razón Rechazo</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- end accordion1-->
                                        <!--inicio de acordeon -->
                                        <div class="panel-group floating-label" id="accordion2">
                                            <div class="card panel">
                                                <div class="card-head style-primary" data-toggle="collapse" data-parent="#accordion1" data-target="#accordion2-1">
                                                    <header>Información para Compras</header>
                                                    <div class="tools">
                                                        <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                                    </div>
                                                </div>
                                                <div id="accordion2-1">
                                                    <div class="card-body">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <div class="col-lg-12">
                                                                    <label for="proveedor" class="control-label">Proveedor Recomendado:</label>
                                                                </div>
                                                                <div class="col-lg-12" id="proveedorError">
                                                                    <div class="col-lg-8">
                                                                        <input type="hidden" name="form[int][proveedor]" id="idProveedor" class="form-control" value="{if isset($formDBR.fk_lgb022_num_proveedor_sugerido)}{$formDBR.fk_lgb022_num_proveedor_sugerido}{/if}">
                                                                        <input type="text" disabled id="persona" class="form-control" value="{if isset($formDBR.provNom)}{$formDBR.provNom} {$formDBR.provApe}{/if}">
                                                                    </div>
                                                                    <div class="col-lg-3">
                                                                        <input type="text" disabled id="cedula" class="form-control" value="{if isset($formDBR.provCed)}{$formDBR.provCed}{/if}">
                                                                    </div>
                                                                    <div class="col-lg-1">
                                                                        <button  {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} disabled {/if}
                                                                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                                type="button"
                                                                                data-toggle="modal" data-target="#formModal2"
                                                                                titulo="Buscar Proveedor"
                                                                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/proveedorMET/proveedor/"
                                                                        >
                                                                            <i class="md md-search"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- end accordion2-->
                                    </div>
                                </div><!--end #tab1 -->
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="col-lg-12">
                                        <div class="card-head style-primary collapsed" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion2-1">
                                            <header>Detalles</header>
                                            <div class="tools">
                                                <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="col-lg-4">
                                                    <div class="form-group floating-label">
                                                        <label for="itemBuscar">Buscar Item<i class="md md-computer"></i></label>
                                                        <button disabled
                                                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                type="button"
                                                                title="Buscar Item"
                                                                id="nuevoItem"
                                                                data-toggle="modal" data-target="#formModal2"
                                                                data-keyboard="false" data-backdrop="static"
                                                                titulo="Buscar Item"
                                                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/item/"
                                                        >
                                                            <i class="md md-search" style="color: #ffffff;"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group floating-label">
                                                        <label for="itemBuscar">Buscar Commodities <i class="md md-computer"></i></label>
                                                        <button disabled
                                                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                type="button"
                                                                title="Buscar Commodities"
                                                                id="nuevoComm"
                                                                data-toggle="modal" data-target="#formModal2"
                                                                data-keyboard="false" data-backdrop="static"
                                                                titulo="Buscar Commodities"
                                                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/commodity/"
                                                        >
                                                            <i class="md md-search" style="color: #ffffff;"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group floating-label">
                                                        <input type="hidden" id="idCC">
                                                        <label for="itemBuscar">Buscar Centro Costo <i class="md md-computer"></i></label>
                                                        <button class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                type="button"
                                                                title="Buscar Centro Costo"
                                                                id="botonCC" disabled
                                                                data-toggle="modal" data-target="#formModal2"
                                                                data-keyboard="false" data-backdrop="static"
                                                                titulo="Buscar Centro Costo"
                                                                url="{$_Parametros.url}modLG/reportes/reporteConsumoCONTROL/centroCostoMET/2">
                                                            <i class="md md-search" style="color: #ffffff;"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="cantidad" value="0">
                                                <table class="table table-striped no-margin" id="contenidoTabla" style="white-space:nowrap;overflow-x:scroll;overflow-y:scroll;width:100%;height:250px;display: inline-block;">
                                                    <thead>
                                                    <tr>
                                                        <th>Sec.</th>
                                                        <th>Cantidad Pedida<i style="color: #ffffff">______________</i></th>
                                                        <th>Comentarios<i style="color: #ffffff">________________________________________________</i></th>
                                                        <th>Especificación Técnica<i style="color: #ffffff">________________________________________________________</i></th>
                                                        <th>Unidad<i style="color: #ffffff">______________</i></th>
                                                        <th>Estado<i style="color: #ffffff">_______</i></th>
                                                        <th>Num. Centro Costo</th>
                                                        <th>Borrar</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="item" style="">
                                                    {if ($estado!='listado' or $estado=="ver") and $estado!="modificar"} {$disabled = 'disabled'}{else}{$disabled = ''}{/if}
                                                    {if isset($formDBD)}
                                                        {$cont=0}
                                                        {foreach item=det from=$formDBD}
                                                            {$cont=$cont+1}
                                                            {if $det.fk_lgb002_num_item}
                                                                {$id = $det.fk_lgb002_num_item}
                                                            {else}
                                                                {$id = $det.fk_lgb003_num_commodity}
                                                            {/if}
                                                            <tr id="idItem{$id}" idItem="{$id}">
                                                                <input type="hidden" id="tipo" value="{if $det.fk_lgb002_num_item}item{else}comm{/if}">
                                                                {if $det.fk_lgb002_num_item}
                                                                    {$idCuenta=$det.cuentaItem}
                                                                    {$codCuenta=$det.codCuentaItem}
                                                                    {$descCuenta=$det.descCuentaItem}

                                                                    {$idCuenta2=$det.cuentaItem2}
                                                                    {$codCuenta2=$det.codCuentaItem2}
                                                                    {$descCuenta2=$det.descCuentaItem2}

                                                                    {$idPartida=$det.partidaItem}
                                                                    {$codPartida=$det.codPartidaItem}
                                                                    {$descPartida=$det.descPartidaItem}
                                                                    <input type="hidden" value="{$det.fk_lgb002_num_item}" name="form[int][numItem][{$id}]">
                                                                {else}
                                                                    {$idCuenta=$det.cuentaComm}
                                                                    {$codCuenta=$det.codCuentaComm}
                                                                    {$descCuenta=$det.descCuentaComm}

                                                                    {$idCuenta2=$det.cuentaComm2}
                                                                    {$codCuenta2=$det.codCuentaComm2}
                                                                    {$descCuenta2=$det.descCuentaComm2}

                                                                    {$idPartida=$det.partidaComm}
                                                                    {$codPartida=$det.codPartidaComm}
                                                                    {$descPartida=$det.descPartidaComm}
                                                                    <input type="hidden" value="{$det.fk_lgb003_num_commodity}" name="form[int][numComm][{$id}]">
                                                                {/if}
                                                                <input type="hidden" class="cuenta" id="cuenta{$id}" value="{$idCuenta}" codCuenta="{$codCuenta}" descCuenta="{$descCuenta}">
                                                                <input type="hidden" class="cuenta2" id="cuenta2{$id}" value="{$idCuenta2}" codCuenta="{$codCuenta2}" descCuenta="{$descCuenta2}">
                                                                <input type="hidden" class="partida" id="partida{$id}" value="{$idPartida}" codPartida="{$codPartida}" descPartida="{$descPartida}">
                                                                <input type="hidden" value="{$det.pk_num_unidad}" name="form[int][pk_num_unidad][{$id}]">

                                                                <td width="10px"><input type="text" class="form-control" readonly name="form[int][sec]" size="2" value="{$cont}"></td>
                                                                <td width="10px"><input type="text" class="form-control cant" idItem="{$id}" {$disabled} name="form[int][canti][{$id}]" size="2" value="{$det.num_cantidad_pedida}"></td>

                                                                <td width="10px"><input type="text" class="form-control" name="form[alphaNum][comentarioI][{$id}]" readonly size="8" value="{$det.ind_comentarios}"></td>
                                                                <td width="10px"><input type="text" class="form-control" name="form[alphaNum][especificacionTecnica][{$id}]"  size="12" value="{$det.ind_especificacion_tecnica}"></td>
                                                                <td width="10px"><input type="text" class="form-control" disabled value="{$det.unidad}"></td>
                                                                <td width="10px"><input type="text" class="form-control" disabled size="2" value="{$det.ind_estado}"></td>
                                                                <td width="10px"><input type="text" class="form-control" {$disabled} readonly name="form[int][numCC][{$id}]" id="numCC{$id}" size="2" value="{$det.fk_a023_num_centro_costo}"></td>
                                                                <td width="10px" class="text-center" style="vertical-align: middle;">
                                                                    <input type="hidden" value="{$det.pk_num_requerimiento_detalle}" name="form[int][idItemSelect][{$id}]">
                                                                    <div class="checkbox checkbox-styled">

                                                                        <button class="borrar btn ink-reaction btn-raised btn-xs btn-danger"
                                                                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/eliminarDetalleMET/"
                                                                                {if $estado!='modificar'}disabled{/if}
                                                                                id="{$det.pk_num_requerimiento_detalle}"
                                                                                idCuenta="{$idCuenta}"
                                                                                idCuenta2="{$idCuenta2}"
                                                                                idPartida="{$idPartida}"
                                                                                idItem="{$id}">
                                                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                                                        </button>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        {/foreach}
                                                        <input type="hidden" name="form[int][cant]" value="{$cont}">
                                                    {/if}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end #tab2 -->

                                <div class="tab-pane floating-label" id="tab3">
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" align="center">
                                            <header>Distribución Contable</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped table-hover" style="width: 100%;display:block;overflow: auto;" id="cuenta">
                                                    <thead style="display: inline-block; width: 100%;">
                                                    <tr>
                                                        <th style="width:10%" class="codCuenta">Cuenta</th>
                                                        <th style="width:100%; text-align: center;">Descripción</th>
                                                        <th style="width:10%; text-align: center;"> %</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="contable" style="height: 100px;display: inline-block;width: 100%;overflow: auto;">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" align="center">
                                            <header>Distribución Contable (Pub. 20)</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped table-hover" style="width: 100%;display:block;overflow: auto;" id="cuenta2">
                                                    <thead style="display: inline-block; width: 100%;">
                                                    <tr>
                                                        <th style="width:10%" class="codCuenta2">Cuenta</th>
                                                        <th style="width:100%; text-align: center;">Descripción</th>
                                                        <th style="width:10%; text-align: center;"> %</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="contable2" style="height: 100px;display: inline-block;width: 100%;overflow: auto;">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="card-head card-head-xs style-primary" align="center">
                                            <header>Distribución Presupuestaria</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped table-hover" style="width: 100%;display:block;overflow: auto;" id="partida">
                                                    <thead style="display: inline-block; width: 100%;">
                                                    <tr>
                                                        <th style="width:10px" class="codPartida">Partida</th>
                                                        <th style="width:100%; text-align: center;">Descripción</th>
                                                        <th style="width:10px; text-align: center;"> %</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="presupuesto" style="height: 100px;display: inline-block;width: 100%;overflow: auto;">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--end #tab3 -->
                                <ul class="pager wizard">
                                    <!--<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>-->
                                    <li class="previous"><a class="btn-raised" href="javascript:void(3);">Anterior</a></li>
                                    <!--<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>-->
                                    <li class="next"><a class="btn-raised" href="javascript:void(3);">Siguiente</a></li>
                                </ul>
                            </div><!--END ClearFix-->
                        </div><!--end #rootwizard1 -->
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col-lg-12 -->
        </div><!--end .row -->
    </div><!--end .modal-body -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if $estado=="ver"}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idReq) AND $estado=='modificar'}
                    <span class="fa fa-edit"></span>
                    Modificar
                {elseif $estado=='listado'}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {elseif $revisar==1}
                    <span class="fa fa-check"></span>
                    Verificar
                {else}
                    {if $estado=='revisar'}
                        <span class="icm icm-rating"></span>
                    {elseif $estado=='conformar'}
                        <span class="icm icm-rating2"></span>
                    {elseif $estado=='aprobar'}
                        <span class="icm icm-rating3"></span>
                    {/if}
                    {$estado}
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css("width", "80%");
    $(document).ready(function() {
        $("#formAjax").submit(function(){
            return false;
        });
        var app = new  AppFunciones();
        $('.accionModal').click(function () {
            var url = $(this).attr('url');
            if($(this).attr('id')=='nuevoComm') {
                if($("select[name='form[int][clasificacion]']").val()!=''){
                    url = url+$("select[name='form[int][clasificacion]']").val();
                }
            }
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post(url, { cargar:0, tr:$("#contenidoTabla > tbody > tr").length+1 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['fec_aprobacion']==null){
                    dato['fec_aprobacion'] = "";
                }
                if(dato['status']=='modificar'){
                    var mensaje = 'Modificado';
                    var eliminar = "no";
                    if(dato['estado']=="revisar"){
                        mensaje = 'Revisado';
                        eliminar = "si";
                    }else if(dato['estado']=="revisar"){
                        mensaje = 'Verificado CC';
                        eliminar = "no";
                    }else if(dato['estado']=="conformar"){
                        mensaje = 'Conformado';
                        eliminar = "si";
                    }else if(dato['estado']=="aprobar"){
                        mensaje = 'Aprobado';
                        eliminar = "si";
                    }
                    swal("Registro Guardado!", 'El Requerimiento fue '+mensaje+' satisfactoriamente.', "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    if(eliminar == "si"){
                        $('#dataTablaJson').dataTable().api().row().remove().draw(false);
                    } else{
                        $(document.getElementById('verificar'+dato['idReq'])).remove();
                        app.metActualizarRegistroTablaJson('dataTablaJson', 'El Requerimiento fue '+mensaje+' satisfactoriamente.','cerrarModal','ContenidoModal');
                    }
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorCantidades'){
                    swal('Error!','Disculpa. hay cantidades erroneas en los items/commodities','error');
                }else if(dato['status']=='errorCC'){
                    swal('Error!','Disculpa. Debe realizar la Revision de Partidas para poder envíar a Caja Chica','error');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='errorIC'){
                    swal("Error!", 'Disculpa. No se han seleccionado items/commodities', "error");
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'Se ha creado el Requerimiento Nro '+dato['cod_requerimiento']+' satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            }, 'json');
        });

        function colocarAlmacen(idClasificacion){
            if(idClasificacion==''){
                idClasificacion = 7;
            }
            $('#almacen > option').remove();
            var url='{$_Parametros.url}modLG/compras/requerimientoCONTROL/buscarClasificacionMET';
            $.post(url, { idClas: idClasificacion }, function (dato) {
                if(dato['pk_num_almacen']==$('#almacen_id').val()) {
                    $('#almacen').append('<option value="'+dato['pk_num_almacen']+'" flag="'+dato['num_flag_commodity']+'" selected>'+dato['ind_descripcion']+'</option>');
                } else {
                    $('#almacen').append('<option value="'+dato['pk_num_almacen']+'" flag="'+dato['num_flag_commodity']+'">'+dato['ind_descripcion']+'</option>');
                }
                if($('#estado').val()=='listado' || $('#estado').val()=='modificar'){

                    if(dato['num_flag_commodity']==1){
                        $("#nuevoItem").attr('disabled', 'disabled');
                        $("#nuevoComm").removeAttr("disabled");
                    } else {
                        $("#nuevoItem").removeAttr("disabled");
                        $("#nuevoComm").attr('disabled', 'disabled');
                    }
                }
                $('#almacen_id').val($('#almacen option:selected').val());
                $('#almacen').select2().val($('#almacen option:selected').text());
            },'json');
        }
        $('#clasificacion_id').ready(function () {
            var id=$('#clasificacion_id').val();
            colocarAlmacen(id);
        });

        $('#clasif').on('change', function () {
            var id=$(this).val();
            $('#contenidoTabla > tbody >').remove();
            colocarAlmacen(id);
            $('#item').html('');
        });

        $('#dependenciaError').on('change', '.select2', function () {
            var url='{$_Parametros.url}modLG/compras/requerimientoCONTROL/buscarCentroCostoMET';
            $.post(url, { idDependencia: $(this).attr('value') }, function (dato) {
                $('#centro_costoError > select > option').remove();
                $('#centro_costo').append('<option value=""></option>');

                for(var i=0; i<dato.length; i++){
                    $('#centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                }
                $('#s2id_centro_costo .select2-chosen').html('');

            },'json');
        });

        $('#dependencia').ready(function () {
            var url='{$_Parametros.url}modLG/compras/requerimientoCONTROL/buscarCentroCostoMET';
            if($('#dependencia').val()!=''){
                var idDependencia = $('#dependencia').val();
            } else {
                idDependencia = $('#idDep').val();
            }
            $.post(url, { idDependencia: idDependencia }, function (dato) {
                $('#centro_costoError > select > option').remove();
                $('#centro_costo').append('<option value=""></option>');

                for(var i=0; i<dato.length; i++){
                    if($('#cc').val()==dato[i]['pk_num_centro_costo'] ){
                        $('#centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'" selected>'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                    } else {
                        $('#centro_costo').append('<option value="'+dato[i]['pk_num_centro_costo']+'">'+dato[i]['ind_descripcion_centro_costo']+'</option>');
                    }
                }
                $('#s2id_centro_costo .select2-chosen').html($('#centro_costo option:selected').text());
                $('#s2id_centro_costo a').removeClass('select2-default');
            },'json');
        });

//del listado
        $('#item').on('click', '.borrar' ,function () {
            var idItem = $(this).attr('idItem');
            var cuenta = $(this).attr('idCuenta');
            var cuenta2 = $(this).attr('idCuenta2');
            var partida = $(this).attr('idPartida');
            $('#idItem'+idItem).remove();
            /*
             $.post($(this).attr('url'), { id: $(this).attr('id')}, function (dato) {
             $('#ContenidoModal2').html(dato);
             });*/

            $('#botonCC').attr('disabled',true);
            if($('#catidadCuentaIC'+cuenta).val()==1){
                $('#idCuenta'+cuenta+'Error').remove();
            } else {
                $('#catidadCuentaIC'+cuenta).val($('#catidadCuentaIC'+cuenta).val()-1)
            }

            if($('#catidadCuentaIC2'+cuenta2).val()==1){
                $('#idCuenta2'+cuenta2+'Error').remove();
            } else {
                $('#catidadCuentaIC2'+cuenta2).val($('#catidadCuentaIC2'+cuenta2).val()-1)
            }

            if($('#catidadPartidaIC'+partida).val()==1){
                $('#idPartida'+partida+'Error').remove();
            } else {
                $('#catidadPartidaIC'+partida).val($('#catidadPartidaIC'+partida).val()-1)
            }
        });

//de la modal nueva
        $('#item').on('click', '.eliminar' ,function () {
            var id = $(this).attr('id');
            var cuenta = $('#'+id+'cuenta').attr('codigo');
            var cuenta2 = $('#'+id+'cuenta2').attr('codigo');
            var partida = $('#'+id+'partida').attr('codigo');

            $('#botonCC').attr('disabled',true);

            if($('#catidadCuentaIC'+cuenta).val()==1){
                $('#idCuenta'+cuenta+'Error').remove();
            } else {
                $('#catidadCuentaIC'+cuenta).val($('#catidadCuentaIC'+cuenta).val()-1)
            }

            if($('#catidadCuentaIC2'+cuenta2).val()==1){
                $('#idCuenta2'+cuenta2+'Error').remove();
            } else {
                $('#catidadCuentaIC2'+cuenta2).val($('#catidadCuentaIC2'+cuenta2).val()-1)
            }
            if($('#catidadPartidaIC'+partida).val()==1){
                $('#idPartida'+partida+'Error').remove();
            } else {
                $('#catidadPartidaIC'+partida).val($('#catidadPartidaIC'+partida).val()-1)
            }

            $('#'+id).remove();
        });

        $('#item').on('click', 'tr' ,function () {
            if($(this).attr('class')=='seleccionado'){
                $(this).removeClass('seleccionado');
                $(this).attr('style','');
                $('#idCC').val('');
                $('#botonCC').attr('disabled',true);
            } else {
                $('#idCC').val('');
                $('.seleccionado').attr('style','');
                $('.seleccionado').removeClass('seleccionado');

                $(this).attr('style','background-color:LightGreen;');
                $(this).attr('class','seleccionado');
                $('#idCC').val($(this).attr('idItem'));
                $('#botonCC').attr('disabled',false);
            }
        });

        function calcularPartidas() {
            $('.porcentajeCuenta').val(0);
            $('.porcentajeCuenta2').val(0);
            $('.porcentajePartida').val(0);

            var IC = $('#item tr').length;
            $('.catidadCuentaIC').each(function ( titulo, valor ) {
                var value = ($(this).val()*100/IC);
                var idCuenta = $(this).attr('idCuenta');
                $('#porcentajeCuenta'+idCuenta).val(value);
            });
            $('.catidadCuentaIC2').each(function ( titulo, valor ) {
                var value = ($(this).val()*100/IC);
                var idCuenta = $(this).attr('idCuenta');
                $('#porcentajeCuenta2'+idCuenta).val(value);
            });

            $('.catidadPartidaIC').each(function ( titulo, valor ) {
                var value = ($(this).val()*100/IC);
                var idPartida = $(this).attr('idPartida');
                $('#porcentajePartida'+idPartida).val(value);
            });

        }

        $('a').on('click', function () {
            calcularPartidas();
        });



        $('#item').ready(function () {

            $('.cuenta').each(function ( titulo, valor ) {
                var id = $(this).val();
                if($('#idCuenta'+id+'Error').length>0){
                    var contadorCuenta = parseFloat($('#catidadCuentaIC'+id).val())+parseFloat(1);
                    $('#catidadCuentaIC'+id).val(contadorCuenta);
                } else {
                    if(id!=''){
                        $(document.getElementById('contable')).append(
                            '<tr id="idCuenta'+id+'Error">' +
                            '<td style="width:20%"><input type="hidden" class="form-control catidadCuentaIC" id="catidadCuentaIC'+id+'" idCuenta="'+id+'" value="1">' +
                            '<input type="text" class="form-control" disabled value="'+$(this).attr('codCuenta')+'">'+
                            '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$(this).attr('descCuenta')+'"></td>'+
                            '<td style="width:10%"><input type="text" class="form-control porcentajeCuenta" disabled value="" id="porcentajeCuenta'+id+'"></td>'+
                            '</tr>');
                    }
                }
            });

            $('.cuenta2').each(function ( titulo, valor ) {
                var id = $(this).val();
                if($('#idCuenta2'+id+'Error').length>0){
                    var contadorCuenta = parseFloat($('#catidadCuentaIC2'+id).val())+parseFloat(1);
                    $('#catidadCuentaIC2'+id).val(contadorCuenta);
                } else {
                    if(id!=''){
                        $(document.getElementById('contable2')).append(
                            '<tr id="idCuenta2'+id+'Error">' +
                            '<td style="width:20%"><input type="hidden" class="form-control catidadCuentaIC2" id="catidadCuentaIC2'+id+'" idCuenta="'+id+'" value="1">' +
                            '<input type="text" class="form-control" disabled value="'+$(this).attr('codCuenta')+'">'+
                            '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$(this).attr('descCuenta')+'"></td>'+
                            '<td style="width:10%"><input type="text" class="form-control porcentajeCuenta2" disabled value="" id="porcentajeCuenta2'+id+'"></td>'+
                            '</tr>');
                    }
                }
            });

            $('.partida').each(function ( titulo, valor ) {
                var id = $(this).val();
                if($('#idPartida'+id+'Error').length>0){
                    var contadorPartida = parseFloat($('#catidadPartidaIC'+id).val())+parseFloat(1);
                    $('#catidadPartidaIC'+id).val(contadorPartida);
                } else {
                    if(id!=''){
                        $(document.getElementById('presupuesto')).append(
                            '<tr id="idPartida'+id+'Error">' +
                            '<td style="width:20%"><input type="hidden" class="form-control catidadPartidaIC" id="catidadPartidaIC'+id+'" idPartida="'+id+'" value="1">' +
                            '<input type="text" class="form-control" disabled value="'+$(this).attr('codPartida')+'">'+
                            '<td style="width:70%"><input type="text" class="form-control" disabled value="'+$(this).attr('descPartida')+'"></td>'+
                            '<td style="width:10%"><input type="text" class="form-control porcentajePartida" disabled value="" id="porcentajePartida'+id+'" value="100"></td>'+
                            '</tr>');
                    }
                }
            });
        });
    });


</script>
