<form action="{$_Parametros.url}modLG/compras/generarOrdenCONTROL/asignarOrdenMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idAdj)}{$idAdj}{/if}" name="idAdj"/>
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="col-lg-12">
                            <div class="form-group floating-label">
                                <label for="itemBuscar">Buscar Ordenes</label>
                                <button class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                        type="button"
                                        title="Buscar Ordenes"
                                        id="nuevaOrden"
                                        data-toggle="modal" data-target="#formModal2"
                                        data-keyboard="false" data-backdrop="static"
                                        titulo="Buscar Ordenes"
                                        url="{$_Parametros.url}modLG/compras/generarOrdenCONTROL/ordenesMET">
                                    <i class="md md-search" style="color: #ffffff;"></i>
                                </button>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="card-head card-head-xs style-primary" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion2-1">
                                <header>Ordenes</header>
                            </div>
                            <table class="table table-striped no-margin" style="white-space:nowrap;overflow-x:scroll;overflow-y:scroll;height:250px;display: inline-block;" id="contenidoTabla2">
                                <thead>
                                <tr>
                                    <th>#<i style="color: #ffffff">______</i></th>
                                    <th>Tipo Orden<i style="color: #ffffff">______</i></th>
                                    <th>Nro. Orden<i style="color: #ffffff">______</i></th>
                                    <th>Fecha Preparación<i style="color: #ffffff">______</i></th>
                                    <th>Proveedor<i style="color: #ffffff">________________________________________________________</i></th>
                                </tr>
                                </thead>
                                <tbody id="ordenes">

                                </tbody>
                            </table>
                        </div>

                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col-lg-12 -->
        </div><!--end .row -->
    </div><!--end .modal-body -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="fa fa-arrow-circle-down"></span>  Asignar
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "65%");
    $(document).ready(function() {
        $("#formAjax").submit(function(){
            return false;
        });
        var app = new  AppFunciones();

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'Las Ordenes fueron asignadas satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }
            }, 'json');
        });
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

    });


</script>