<form action="{$_Parametros.url}modLG/compras/actaInicioCONTROL/anularActaMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idActa)}{$idActa}{/if}" name="idActa"/>
    <input type="hidden" value="{if isset($estado)}{$estado}{/if}" name="estado"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                <div class="col-lg-12">
                    <div class="form-group floating-label col-lg-12" id="motivoError">
                        <textarea id="motivo" class="form-control" cols="50" rows="2" name="form[alphaNum][motivo]">{if isset($eval.ind_conclusion)}{$eval.ind_conclusion}{/if}</textarea>
                        <label for="motivo"><i class="md md-border-color"></i>
                            Motivo
                            {if $estado=="TE"}
                                del Termino
                            {else}
                                de la anulacion
                            {/if}
                        </label>
                    </div>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn {if $estado=="TE"}btn-info{else}btn-danger{/if} ink-reaction btn-raised" id="accion">

            {if $estado=="TE"}
                <span class="md md-done"></span>
                Terminar
            {else}
                <span class="icm icm-blocked"></span>
                Anular
            {/if}
        </button>
    </div>
</form>

<script type="text/javascript">
    $('#modalAncho').css("width", "60%");
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        var app = new  AppFunciones();

        $('#accion').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    swal("Realizado!", 'El Acta fue '+
                            {if $estado=="TE"}
                            'Terminado'+
                            {else}
                            'Anulado'+
                            {/if}
                            ' satisfactoriamente.', "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            }, 'json');
        });
    });
</script>