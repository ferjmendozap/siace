<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Informes {if $opcion=="revisar"}realizados{elseif $opcion=="aprobar"}revisados{/if}</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nro.</th>
                                <th>Nro. Evaluacion</th>
                                <th>Objeto</th>
                                <th>Fecha</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/compras/actaInicioCONTROL/jsonDataTablaInformeMET/{$opcion}",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_informe_recomendacion" },
                    { "data": "ind_cod_evaluacion", width:150 },
                    { "data": "ind_objeto_consulta" },
                    { "data": "fec_creacion", width:150 },
                    { "data": "estado" },
                    { "orderable": false,"data": "acciones", width:150}
                ]
        );


        var url = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/crearModificarInformeRecMET';

        $('#dataTablaJson tbody').on( 'click', '.accion', function () {
            $('#modalAncho').css("width", "50%");
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { t: $(this).attr('t'), ver: $(this).attr('ver'), idInfo: $(this).attr('idInfo'), idInfor: $(this).attr('idInfor'), objeto: $(this).attr('objeto'), conclusion: $(this).attr('conclusion'), recomendacion: $(this).attr('recomendacion'), idAsisA: $(this).attr('idAsisA'), idAsisB: $(this).attr('idAsisB'), idAsisC: $(this).attr('idAsisC'), opcion: $(this).attr('opcion') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.generarRecomendacionOdt', function () {
            var url = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/generarInformeRecOdtMET';
            $.post(url, { idRec: $(this).attr('idRec') }, function (dato) {
                window.open(dato['ruta']);
            }, 'json');
        });

        var url2 = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/crearModificarInformeRecMET';

        $('#dataTablaJson tbody').on( 'click', '.modificarRec', function () {
            $('#modalAncho').css("width", "50%");
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url2, { opcion: $(this).attr('opcion'),idInfor: $(this).attr('idInfor'),idAsisA: $(this).attr('idAsisA'), idAsisB: $(this).attr('idAsisB'), idAsisC: $(this).attr('idAsisC')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

    });
</script>