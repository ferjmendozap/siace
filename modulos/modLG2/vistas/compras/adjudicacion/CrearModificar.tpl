<form action="{$_Parametros.url}modLG/compras/adjudicacionCONTROL/crearModificarAdjudicacionMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idInfor)}{$idInfor}{/if}" name="idInfor"/>
    <input type="hidden" value="{if isset($idAdj)}{$idAdj}{/if}" name="idAdj"/>
    <input type="hidden" value="{if isset($informe[0].fk_a006_num_miscelaneo_detalle_tipo_adjudicacion)}{$informe[0].fk_a006_num_miscelaneo_detalle_tipo_adjudicacion}{/if}" name="tipoAdj"/>
    <input type="hidden" name="secuencia" value="{$secuencia}">
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                <div class="form-group col-lg-12">
                    <input type="hidden" name="form[int][proveedor]" value="{$proveedor.pk_num_proveedor}">
                    <input type="text" class="form-control" value="{$proveedor.nombre}" readonly>
                    <label>Proveedor Recomendado</label>
                </div>
                <div class="form-group col-lg-12" id="requerimientosError">
                    <div class="card-head card-head-xs style-primary" align="center">
                        <header>Requerimientos a adjudicar: </header>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <table>
                                <thead>
                                <tr style="background-color: #009999; color:white;">
                                    <td style="width: 100px;">Nro.</td>
                                    <td style="width: 200px;">Precio Cotizado</td>
                                    <td style="width: 200px;">Cantidad Pedida</td>
                                    <td style="width: 1000px;">Ítem</td>
                                    <td style="text-align:center;width: 100px;">Adjudicar</td>
                                </tr>
                                </thead>
                                <tbody>
                                {$contador=0}
                                {foreach item=infor from=$informe}
                                    <tr>
                                        {$monto = 0}
                                        {$check1=""}
                                        {if isset($adjudicacion) AND isset($idAdj)}
                                            {foreach item=adj from=$adjudicacion}
                                                {if $adj.fk_lgb023_num_acta_detalle == $infor.pk_num_acta_detalle}
                                                    {$check1="checked"}
                                                {/if}
                                            {/foreach}
                                        {/if}
                                        {foreach item=ic from=$inviCoti}
                                            {if $ic.fk_lgb022_num_proveedor==$proveedor.pk_num_proveedor AND
                                            $ic.fk_lgb023_num_acta_detalle==$infor.pk_num_acta_detalle}
                                                {$cotizacion = $ic.pk_num_cotizacion}
                                                {$monto = $ic.num_total}
                                                {$cantidad = $ic.num_cantidad}
                                                {if $ic.num_flag_asignado==1 AND !isset($idAdj)}
                                                    {$check1="checked"}
                                                {/if}
                                            {/if}
                                        {/foreach}
                                        <input type="hidden" name="form[int][reqDet][{$contador}]" value="{$infor.pk_num_acta_detalle}">
                                        <input type="hidden" name="form[int][coti][{$contador}]" value="{$cotizacion}">
                                        <td>{$contador+1}</td>
                                        <td>{$monto}</td>
                                        <td>{$cantidad}</td>
                                        <td>{$infor.ind_descripcion}</td>
                                        <td align="center">
                                            <div class="checkbox checkbox-styled">
                                                <label class="checkbox-inline checkbox-styled">
                                                    <input type="checkbox" class="form-control bloqueoCheck" name="form[int][adj][{$contador}]" id="adj{$contador}" value="1" {$check1}>
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    {$contador=$contador+1}
                                {/foreach}
                                <input type="hidden" name="form[int][sec]" value="{$contador}">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-warning ink-reaction btn-raised" id="informe"
                titulo="Ver Informe de Recomendacion" idRec="{$idInfor}">
            <span class="icm icm-eye"></span> Informe
        </button>
        {if isset($ver) and $ver==1}
        {else}

            <button type="button" class="btn btn-danger ink-reaction btn-raised" id="rechazar"
                    data-toggle="modal" data-target="#formModal2"
                    data-keyboard="false" data-backdrop="static"
                    titulo="Rechazar Informe de Recomendacion" idInfor="{$idInfor}">
                <span class="icm icm-blocked"></span> Rechazar
            </button>
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idAdj)}
                    <span class="fa fa-edit"></span> Modificar
                {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $("#formAjax").submit(function(){
        return false;
    });
    $('.bloqueoCheck').on('click',function () {
        var checked = $(this).attr('checked');
        if(checked=='checked'){
            $(this).attr('checked',false);
        } else {
            $(this).attr('checked',true);
        }
    });
     /**/

    $(document).ready(function () {
        var app = new  AppFunciones();
        $('#modalAncho').css("width", "80%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La Adjudicacion fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el Item/Commoditie seleccionado ya ha sido adjudicado');
                }else if(dato['status']=='nuevo'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La Adjudicacion fue guardada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

        $('#informe').click(function () {
            var url = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/generarInformeRecOdtMET';
            $.post(url, { idRec: $(this).attr('idRec') }, function (dato) {
                window.open(dato['ruta']);
            }, 'json');
        });

        $('#rechazar').click(function () {
            var url = '{$_Parametros.url}modLG/compras/adjudicacionCONTROL/rechazarMET';
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post(url, { idInfor: $(this).attr('idInfor') }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });
    });
</script>