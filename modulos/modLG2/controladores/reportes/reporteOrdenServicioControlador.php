<?php
class reporteOrdenServicioControlador extends Controlador
{
    public $atReporte;
    public $atOrdenCompraModelo;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = require_once ROOT . 'modulos' . DS . 'modLG' . DS . 'modelos' . DS . 'reportes' . DS . 'reporteOrdenServicioModelo.php';
        $this->atReporte = new reporteOrdenServicioModelo;
        Session::metAcceso();
        #se carga la Libreria.

    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('filtro');
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function estadoNombre($estado){
        $e="";
        if($estado=="PR"){
            $e="En Preparacion";
        } elseif($estado=="RV"){
            $e="Revisado";
        } elseif($estado=="CN"){
            $e="Conformado";
        } elseif($estado=="AN"){
            $e="Anulado";
        } elseif($estado=="CE"){
            $e="Cerrado";
        } elseif($estado=="TE"){
            $e="Terminado";
        } elseif($estado=="DS"){
            $e="Desierto";
        } elseif($estado=="PE"){
            $e="Pendiente";
        } elseif($estado=="AP"){
            $e="Aprobado";
        } elseif($estado=="CO"){
            $e="Completado";
        }
        return $e;
    }

    public function metGenerarReporte(
        $proveedor,
        $centroCosto,
        $estado,
        $fdesde,
        $fhasta,
        $fdesde2,
        $fhasta2)
    {

        define('DESDE_PREP',$fdesde);
        define('HASTA_PREP',$fhasta);
        define('DESDE_PREP2',$fdesde2);
        define('HASTA_PREP2',$fhasta2);

        $elaboradoPor = $this->atReporte->metMostrarEmpleado(Session::metObtener('idEmpleado'));
        $revisadoPor = $this->atReporte->metMostrarEmpleado(102);
        $conformadoPor = $this->atReporte->metMostrarEmpleado(95);
        $aprobadoPor = $this->atReporte->metMostrarEmpleado(148);

        define('ELABORADO_POR',$elaboradoPor['nombre']);
        define('ELABORADO_CARGO',$elaboradoPor['ind_descripcion_cargo']);
        define('REVISADO_POR',$revisadoPor['nombre']);
        define('REVISADO_CARGO',$revisadoPor['ind_descripcion_cargo']);
        define('CONFORMADO_POR',$conformadoPor['nombre']);
        define('CONFORMADO_CARGO',$conformadoPor['ind_descripcion_cargo']);
        define('APROBADO_POR',$aprobadoPor['nombre']);
        define('APROBADO_CARGO',$aprobadoPor['ind_descripcion_cargo']);

        $this->metObtenerLibreria('headerOrdenServicio','modLG');
        $this->atFPDF = new pdf('L', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5, 35);
        $this->atFPDF->AddPage();
        $where = "";
        if($proveedor!='no'){
            $where .= " AND orden.fk_lgb022_num_proveedor='$proveedor' ";
        }
        if($centroCosto!='no'){
            $where .= " AND orden.fk_a023_num_centro_costo='$centroCosto' ";
        }
        if($estado!='no'){
            $where .= " AND orden.ind_estado='$estado' ";
        }
        if($fdesde!='no' AND $fhasta!='no'){
            $where .= " AND (orden.fec_creacion>='$fdesde' AND orden.fec_creacion<='$fhasta') ";
        }
        if($fdesde2!='no' AND $fhasta2!='no'){
            $where .= " AND (orden.fec_aprobacion>='$fdesde2' AND orden.fec_aprobacion<='$fhasta2') ";
        }

        $ordenes = $this->atReporte->metListarOrdenServicio($where);


        foreach($ordenes AS $titulo=>$valor){

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 7);
            $this->atFPDF->Row(array($valor['ind_orden'],
                utf8_decode($valor['nombreProveedor']),
                $valor['fec_creacion'],
                $valor['fec_aprobacion'],
                $this->estadoNombre($valor['ind_estado']),
//                $this->estadoNombre($estado),
                $valor['cod_centro_costo'],
                $valor['ind_cod_commodity'],
                utf8_decode($valor['ind_descripcion'])));
            $this->atFPDF->Ln(1);
        }

        #salida del pdf
        $this->atFPDF->Output();
    }

}


?>