<?php
class reporteConfirmacionServicioControlador extends Controlador
{
    public $atReporte;
    public $atOrdenCompraModelo;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = require_once ROOT . 'modulos' . DS . 'modLG' . DS . 'modelos' . DS . 'reportes' . DS . 'reporteConfirmacionServicioModelo.php';
        $this->atReporte = new reporteConfirmacionServicioModelo;
        Session::metAcceso();
        #se carga la Libreria.

    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('filtro');
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function estadoNombre($estado){
        $e="";
        if($estado=="PR"){
            $e="En Preparacion";
        } elseif($estado=="RV"){
            $e="Revisado";
        } elseif($estado=="CN"){
            $e="Conformado";
        } elseif($estado=="AN"){
            $e="Anulado";
        } elseif($estado=="CE"){
            $e="Cerrado";
        } elseif($estado=="TE"){
            $e="Terminado";
        } elseif($estado=="DS"){
            $e="Desierto";
        } elseif($estado=="PE"){
            $e="Pendiente";
        } elseif($estado=="AP"){
            $e="Aprobado";
        } elseif($estado=="CO"){
            $e="Completado";
        }
        return $e;
    }

    public function metGenerarReporte($fdesde,$fhasta,$nroOrden)
    {


        $this->metObtenerLibreria('headerConfirmacionServicio','modLG');
        $this->atFPDF = new pdf('P', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5, 35);
        $where = "";
        $where2 = "";
        if($nroOrden!='no'){
            $where .= " AND orden.ind_orden='$nroOrden' ";
        }
        if($fdesde!='no' AND $fhasta!='no'){
            $where2 .= " AND (confirmacion.fec_confirmacion>='$fdesde 00:00:00' AND confirmacion.fec_confirmacion<='$fhasta 23:59:59') ";
        } else {
            $fdesde = '';
            $fhasta = '';
        }
        define('DESDE',$fdesde);
        define('HASTA',$fhasta);

        $elaboradoPor = $this->atReporte->metMostrarEmpleado(Session::metObtener('idEmpleado'));

        define('ELABORADO_POR',$elaboradoPor['nombre']);
        define('ELABORADO_CARGO',$elaboradoPor['ind_descripcion_cargo']);
        $this->atFPDF->AddPage();

        $ordenes = $this->atReporte->metListarOrdenesServicio($where,$where2);

        foreach($ordenes AS $titulo=>$valor){
            if($valor['confirm']>0){

                $codPrc = '';
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', 'B', 8);

                $this->atFPDF->Cell(20, 8, utf8_decode('Nº de Orden: '), 0, 0, 'L', 1);
                $this->atFPDF->Cell(25, 8, $valor['ind_orden'], 0, 0, 'L', 1);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', 'B', 8);
                $this->atFPDF->Cell(25, 8, utf8_decode('Fecha de La Orden: '), 0, 0, 'C', 1);
                $this->atFPDF->SetFont('Arial', '', 8);
                $this->atFPDF->Cell(22, 8, $valor['fec_creacion'], 0, 'C');
                $this->atFPDF->Ln(5);
                $this->atFPDF->SetFont('Arial', 'B', 8);
                $this->atFPDF->Cell(19, 8, utf8_decode('Descripcion:'), 0, 0, 'R', 1);
                $this->atFPDF->SetFont('Arial', '', 8);
                $this->atFPDF->MultiCell(185, 6, utf8_decode($valor['ind_descripcion']),  0, 'L', 1);
//                $this->atFPDF->Ln(1);
//                $this->atFPDF->SetFont('Arial', 'B', 8);
//                $this->atFPDF->Cell(23, 8, utf8_decode('Procedimiento: '), 0, 0, 'R', 0);
//                $this->atFPDF->SetFont('Arial', '', 8);
//                $this->atFPDF->MultiCell(185, 6, utf8_decode($codPrc),  0, 'L', 1);
                $this->atFPDF->Ln(10);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetFont('Arial', 'B', 8);
                $this->atFPDF->SetWidths(array(30, 30, 110, 20));
                $this->atFPDF->SetAligns(array('C', 'C', 'C','C'));
                $this->atFPDF->Row(array(utf8_decode('Nº de Confirmación'),
                    utf8_decode('Cod.Commoditie'),
                    utf8_decode('Descripción'),
                    utf8_decode('Cantidad')
                ));

                $confirmaciones = $this->atReporte->metListarConfirmaciones($valor['pk_num_orden'],$where2);

                foreach($confirmaciones AS $titulo2=>$valor2){
                    $this->atFPDF->SetFillColor(255, 255, 255);
                    $this->atFPDF->SetFont('Arial', '', 8);
                    $this->atFPDF->SetWidths(array(30, 30, 110, 20));
                    $this->atFPDF->SetAligns(array('C', 'C', 'L','C'));
                    $this->atFPDF->Row(array(
                        $valor2['ind_numero_confirmacion'],
                        utf8_decode($valor2['ind_cod_commodity']),
                        utf8_decode($valor2['ind_descripcion']),
                        $valor2['num_cantidad']
                    ));
                }

                $this->atFPDF->Ln(10);
            }
        }

        #salida del pdf
        $this->atFPDF->Output();
    }

}


?>