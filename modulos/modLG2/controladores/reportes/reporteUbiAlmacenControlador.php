<?php
class reporteUbiAlmacenControlador extends Controlador
{
    public $atReporte;
    public $atItemsModelo;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reporteUbiAlmacen', 'reportes');
        Session::metAcceso();
        #se carga la Libreria.
        $this->metObtenerLibreria('header','modLG');
        $this->atFPDF = new pdf('p','mm','letter');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('filtro');
    }

    public function metLineas()
    {
        $this->atVista->assign('lista',$this->atReporte->atMiscelaneoModelo->metMostrarSelect('LINEAS'));
        $this->atVista->metRenderizar('lineas','modales');
    }

    public function metFormatoFecha($fecha,$formato,$separador){
        #1  2016-01-01 a 01-01-2016
        #0  01-01-2016 a 2016-01-01
        if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
            $dia=substr($fecha,8,2);
            $mes=substr($fecha,5,2);
            $anio=substr($fecha,0,4);
        } elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
            $dia=substr($fecha,0,2);
            $mes=substr($fecha,3,2);
            $anio=substr($fecha,6,4);
        } else {
            $dia=0;
            $mes=0;
            $anio=0;
        }
        if (checkdate($mes, $dia, $anio)==false){
            return "la fecha es incorrecta";
            exit;
        }
        if($formato==1) {
            $fecha =$dia.$separador.$mes.$separador.$anio;
        } else {
            $fecha =$anio.$separador.$mes.$separador.$dia;
        }
        return $fecha;
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function estadoNombre($estado){
        if($estado=="PR"){
            $e="PREPARADO";
        } elseif($estado=="RV"){
            $e="REVISADO";
        } elseif($estado=="CN"){
            $e="CONFORMADO";
        } elseif($estado=="AN"){
            $e="ANULADO";
        } elseif($estado=="CE"){
            $e="CERRADO";
        } elseif($estado=="TE"){
            $e="TERMINADO";
        }
        return $e;
    }

    public function metGenerarReporte($buscar,$linea,$orden)
    {
        $filtro="WHERE 1 ";
        if($buscar!='no'){
            $filtro.="AND (i.ind_descripcion LIKE '%$buscar%' OR i.pk_num_item LIKE '%$buscar%') ";
        }
        if($linea!='no'){
            $filtro.="AND detalle.pk_num_miscelaneo_detalle LIKE '%$linea%' ";
        }
        $filtro.="ORDER BY $orden";

        $consulta = $this->atReporte->atItemsModelo->metListarItems($filtro);



        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(1, 35);
        $this->atFPDF->AddPage();

        $this->atFPDF->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(15, 5); $this->atFPDF->Cell(100, 5, 'NOMBRE_ORGANISMO_ACTUAL', 0, 1, 'L');
        $this->atFPDF->SetXY(15, 10); $this->atFPDF->Cell(100, 5, utf8_decode('DIVISIÓN DE ADMINISTRACIÓN Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetXY(230, 5); $this->atFPDF->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(60, 5, date("d-m-Y"), 0, 1, 'L');
        $this->atFPDF->SetXY(230, 10); $this->atFPDF->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->atFPDF->Cell(60, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 9);
        $this->atFPDF->SetXY(5, 20);
        $this->atFPDF->Cell(195, 5, utf8_decode('Listado de Items'), 0, 1, 'C', 0);
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetWidths(array(15, 12, 50, 10, 31, 18, 13, 13, 13, 15, 15));
        $this->atFPDF->SetAligns(array('C', 'C', 'L', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'));
        $this->atFPDF->Row(array(
            'Item',
            '# Interno',
            'Descripcion',
            'Und.',
            'Tipo',
            'Linea',
            'Familia',
            'S.Familia',
            'Partida',
            'Cta. Gasto'));
        $this->atFPDF->Ln(1);

        foreach ($consulta as $titulo=>$valor) {
            $codItem = $this->metRellenarCeros($consulta[$titulo]['pk_num_item'],10);
            $codItemInterno = $this->metRellenarCeros($consulta[$titulo]['pk_num_item'],3);

            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->Row(array(
                $codItem,
                'P'.$codItemInterno,
                utf8_decode($consulta[$titulo]['ind_descripcion']),
                $consulta[$titulo]['uni'],
                utf8_decode($consulta[$titulo]['tipo']),
                $consulta[$titulo]['cod_detalle'],
                $consulta[$titulo]['pk_num_clase_familia'],
                $consulta[$titulo]['pk_num_subfamilia'],
                $consulta[$titulo]['cod_partida'],
                $consulta[$titulo]['cod_cuenta']
                ));
            $this->atFPDF->Ln(1);

        }

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->Rect(9, 258, 197, 0.1, 'DF');

        #salida del pdf
        $this->atFPDF->Output();
    }
}


?>