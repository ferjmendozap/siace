<?php
class reporteContratistaControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reporteContratista', 'reportes');
        Session::metAcceso();
        #se carga la Libreria.
        $this->metObtenerLibreria('headerContratistas','modLG');
        $this->atFPDF = new pdf('L','mm','letter');
    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('filtro');
    }

    public function metFormatoFecha($fecha,$formato,$separador){
        #1  2016-01-01 a 01-01-2016
        #0  01-01-2016 a 2016-01-01
        if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
            $dia=substr($fecha,8,2);
            $mes=substr($fecha,5,2);
            $anio=substr($fecha,0,4);
        } elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
            $dia=substr($fecha,0,2);
            $mes=substr($fecha,3,2);
            $anio=substr($fecha,6,4);
        } else {
            $dia=0;
            $mes=0;
            $anio=0;
        }
        if (checkdate($mes, $dia, $anio)==false){
            return "la fecha es incorrecta";
            exit;
        }
        if($formato==1) {
            $fecha =$dia.$separador.$mes.$separador.$anio;
        } else {
            $fecha =$anio.$separador.$mes.$separador.$dia;
        }
        return $fecha;
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function estadoNombre($estado){
        $e="";
        if($estado=="PR"){
            $e="PREPARADO";
        } elseif($estado=="RV"){
            $e="REVISADO";
        } elseif($estado=="CN"){
            $e="CONFORMADO";
        } elseif($estado=="AN"){
            $e="ANULADO";
        } elseif($estado=="CE"){
            $e="CERRADO";
        } elseif($estado=="TE"){
            $e="TERMINADO";
        } elseif($estado=="DS"){
            $e="DESIERTO";
        }
        return $e;
    }

    public function metGenerarReporte($desde,$hasta)
    {
#        $desde = $this->metObtenerAlphaNumerico('desde');
#        $hasta = $this->metObtenerAlphaNumerico('hasta');
        define('DESDE',$desde);
        define('HASTA',$hasta);

        $acta = $this->atReporte->metListarActaFecha($desde,$hasta);
        $desde = $this->metFormatoFecha($desde,0,'-');
        $hasta = $this->metFormatoFecha($hasta,0,'-');

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5, 35);
        $this->atFPDF->AddPage();

        foreach($acta as $a){
            $codACT='0004-CPAI-'.$this->metRellenarCeros($a['pk_num_acta_inicio'],4).'-'.$a['num_anio'];
            $estado=$this->estadoNombre($a['ind_estado']);

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 7);
            $this->atFPDF->Row(array($a['pk_num_acta_inicio'],
                $a['ind_codigo_procedimiento'],
                utf8_decode($a['ind_nombre_procedimiento']),
                $a['ind_codigo_acta'],
                $a['fec_registro'],
                utf8_decode($estado)));
            $this->atFPDF->Ln(1);
        }

        #salida del pdf
        $this->atFPDF->Output();
    }
}


?>