<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

require_once ROOT. 'publico/procesoCompra/docxpresso/CreateDocument.php';

class controlPerceptivoControlador extends Controlador
{
    private $atConPer;
    private $atOrdenCompraModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atConPer = $this->metCargarModelo('controlPerceptivo', 'procesos');
    }

    //Método para listar los controles perceptivos
    public function metIndex($cual=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('cual',$cual);
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar los controles perceptivos
    public function metCrearModificarConPer()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idConPer = $this->metObtenerInt('idConPer');
        $idOrden = $this->metObtenerInt('idOrden');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $ExcceccionInt= array('cerrar','cantidad',
                'tipoFirmanteB','asisB',
                'tipoFirmanteC','asisC',
                'tipoFirmanteD','asisD',
                'tipoFirmanteE','asisE'
            );
            $ExcceccionText= array('comentario');
            $ind = $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum',$ExcceccionText);
            if ($ind!=null and $alphaNum==null){
                $validacion = $ind;
            } elseif($ind==null and $alphaNum!=null) {
                $validacion = $alphaNum;
            } else {
                $validacion = array_merge($ind,$alphaNum);
            }
/*
            $cerrar = '0';
            if ($validacion['cantidad']!=0 AND $idConPer===0){
                $cont=1;
                while($cont<=$validacion['cantidad']){
                    if ($validacion['idDetalle'][$cont] == 'error'){
                        $validacion['idDetalle'][$cont] = '0';
                    }
                    if ($validacion['comentario'][$cont] == false){
                        $validacion['comentario'][$cont] = '';
                    }
                    if($validacion['cantRecibida'][$cont]+$validacion['cantidadRecibida'][$cont]!=$validacion['cantPedida'][$cont]){
                        $cerrar = '1' ;
                    }
                    $cont=$cont+1;
                }
            }
            */
            if(!isset($validacion['cerrar'])){
                $validacion['cerrar'] = 1;
            } else {
                $validacion['cerrar'] = '0';
            }
//            $validacion['cerrar'] = $cerrar;

            $error =0;
            if($validacion['asisB']==0){
                $validacion['asisB'] = null;
                $validacion['tipoFirmanteB'] = null;
            } elseif($validacion['tipoFirmanteB']==0){
                $validacion['tipoFirmanteB'] = 'error';
            }
            if($validacion['asisC']==0){
                $validacion['asisC'] = null;
                $validacion['tipoFirmanteC'] = null;
            } elseif($validacion['tipoFirmanteC']==0){
                $validacion['tipoFirmanteC'] = 'error';
            }
            if($validacion['asisD']==0){
                $validacion['asisD'] = null;
                $validacion['tipoFirmanteD'] = null;
            } elseif($validacion['tipoFirmanteD']==0){
                $validacion['tipoFirmanteD'] = 'error';
            }
            if($validacion['asisE']==0){
                $validacion['asisE'] = null;
                $validacion['tipoFirmanteE'] = null;
            } elseif($validacion['tipoFirmanteE']==0){
                $validacion['tipoFirmanteE'] = 'error';
            }
            if($validacion['tipoFirmanteB']!=null){
                if($validacion['tipoFirmanteB']==$validacion['tipoFirmanteA']){
                    $validacion['tipoFirmanteB']='error';
                    $validacion['tipoFirmanteA']='error';
                    $error = 1;
                }
                if($validacion['tipoFirmanteB']==$validacion['tipoFirmanteC']){
                    $validacion['tipoFirmanteB']='error';
                    $validacion['tipoFirmanteC']='error';
                    $error = 1;
                }
                if($validacion['tipoFirmanteB']==$validacion['tipoFirmanteD']){
                    $validacion['tipoFirmanteB']='error';
                    $validacion['tipoFirmanteD']='error';
                    $error = 1;
                }
                if($validacion['tipoFirmanteB']==$validacion['tipoFirmanteE']){
                    $validacion['tipoFirmanteB']='error';
                    $validacion['tipoFirmanteE']='error';
                    $error = 1;
                }
            }
            if($validacion['tipoFirmanteC']!=null){
                if($validacion['tipoFirmanteC']==$validacion['tipoFirmanteA']){
                    $validacion['tipoFirmanteC']='error';
                    $validacion['tipoFirmanteA']='error';
                    $error = 1;
                }
                if($validacion['tipoFirmanteC']==$validacion['tipoFirmanteD']){
                    $validacion['tipoFirmanteC']='error';
                    $validacion['tipoFirmanteD']='error';
                    $error = 1;
                }
                if($validacion['tipoFirmanteC']==$validacion['tipoFirmanteE']){
                    $validacion['tipoFirmanteC']='error';
                    $validacion['tipoFirmanteE']='error';
                    $error = 1;
                }
            }
            if($validacion['tipoFirmanteD']!=null){
                if($validacion['tipoFirmanteD']==$validacion['tipoFirmanteA']){
                    $validacion['tipoFirmanteD']='error';
                    $validacion['tipoFirmanteA']='error';
                    $error = 1;
                }
                if($validacion['tipoFirmanteD']==$validacion['tipoFirmanteE']){
                    $validacion['tipoFirmanteD']='error';
                    $validacion['tipoFirmanteE']='error';
                    $error = 1;
                }
            }
            if($validacion['tipoFirmanteE']!=null){
                if($validacion['tipoFirmanteE']==$validacion['tipoFirmanteA']){
                    $validacion['tipoFirmanteE']='error';
                    $validacion['tipoFirmanteA']='error';
                    $error = 1;
                }
            }

            if($error==1){
                $validacion['status']='errorSQL2';
                echo json_encode($validacion);
                exit;
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            $datosOrden=$this->atConPer->atOrdenCompraModelo->metMostrarOrden($idOrden);
            $montoAfecto = 0;
            $montoNoAfecto = 0;
            $montoImpuesto = 0;
            $datosOrdenDetalle=$this->atConPer->atOrdenCompraModelo->metMostrarOrdenDetalle($idOrden);
            $validacion['datosOrdenDetalle'] = $datosOrdenDetalle;
            $cont = 0;
            if($idConPer===0){
                foreach ($datosOrdenDetalle as $key=>$value) {
                    $cont = $cont + 1;
                    if ($validacion['idDetalleOrden'][$cont]==$value['pk_num_orden_detalle']) {
                        $precioCantidad = $value['num_precio_unitario']*$validacion['cantRecibida'][$cont];
                        $montoIva = $precioCantidad * ($datosOrden['num_factor_porcentaje']/100);
                        if($value['num_flag_exonerado']==1){
                            $montoNoAfecto = $montoNoAfecto + $precioCantidad;
                        } else {
                            $montoAfecto = $montoAfecto + $precioCantidad;
                            $montoImpuesto = $montoImpuesto + $montoIva;
                        }
                    }
                }
            }


            $datosOrden['num_monto_afecto'] = $montoAfecto;
            $datosOrden['num_monto_no_afecto'] = $montoNoAfecto;
            $datosOrden['num_monto_igv'] = $montoImpuesto;
            $datosOrden['num_monto_total'] = $montoAfecto+$montoNoAfecto+$montoImpuesto;
            $datosOrden['num_monto_pendiente'] = $datosOrden['num_monto_total'];

            $validacion['datosOrden'] = $datosOrden;



            $orden=$this->atConPer->atOrdenCompraModelo->atRequerimientoModelo->metContar('lg_b020_control_perceptivo',date('y'),'fec_anio');
            $validacion['cant'] = $this->metRellenarCeros($orden['cantidad'] +1,4);
            $validacion['idOrden'] = $idOrden;
            $validacion['idConPer'] = $idConPer;

            $clasificacion=$this->atConPer->metBuscarClasificacionCxP('RCD');
            $validacion['clasificacion'] = $clasificacion['pk_num_documento_clasificacion'];

            $documento=$this->atConPer->metBuscarDocumento($idOrden);
            $validacion['idDoc'] = $documento['pk_num_documento'];




            if($idConPer===0){
               $id=$this->atConPer->metCrearConPer($validacion);

                $cerrar = 0;
                foreach ($datosOrdenDetalle as $key=>$value) {
                    $total = 0;
                    $percibido = 0;
                    $controlDetalle = $this->atConPer->metMostrarConPerDetalle($value['pk_num_orden_detalle'],true);
                    foreach ($controlDetalle as $key2=>$value2) {
                        $total = $value2['cantidadPedida'];
                        $percibido = $percibido + $value2['cantidadRecibida'];
                    }
                    if($total!=0 AND $total != $percibido){
                        $cerrar = 1;
                    }
                }
                if($cerrar==0){
                    $this->atConPer->metActualizarOrden($idOrden);
                }
                $validacion['status']='nuevo';
            }else{
                $id=$this->atConPer->metModificarConPer($validacion);

                $validacion['status']='modificar';
            }

            $validacion['id']=$id;
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idOrden']=$idOrden;

            echo json_encode($validacion);
            exit;
        }

        if($idConPer!=0){
            $formDB = $this->atConPer->metMostrarConPer($idConPer);
            $idAsisA = $formDB['fk_rhb001_num_empleado_conforme_a'];
            $idAsisB = $formDB['fk_rhb001_num_empleado_conforme_b'];
            $idAsisC = $formDB['fk_rhb001_num_empleado_conforme_c'];
            $idAsisD = $formDB['fk_rhb001_num_empleado_conforme_d'];
            $idAsisE = $formDB['fk_rhb001_num_empleado_conforme_e'];
            $this->atVista->assign('formDB',$formDB);
            $this->atVista->assign('AsisA',$this->atConPer->metMostrarEmpleado($idAsisA,'a'));
            $this->atVista->assign('AsisB',$this->atConPer->metMostrarEmpleado($idAsisB,'b'));
            $this->atVista->assign('AsisC',$this->atConPer->metMostrarEmpleado($idAsisC,'c'));
            $this->atVista->assign('AsisD',$this->atConPer->metMostrarEmpleado($idAsisD,'d'));
            $this->atVista->assign('AsisE',$this->atConPer->metMostrarEmpleado($idAsisE,'e'));
            $this->atVista->assign('percepDet2',$this->atConPer->metMostrarConPerDetalle($idConPer));
            $this->atVista->assign('idConPer',$idConPer);
            $this->atVista->assign('ver',$ver);
        }
        $this->atVista->assign('percepDet',$this->atConPer->metMostrarConPerDetalle());
        $this->atVista->assign('tipoFirmantes',$this->atConPer->atOrdenCompraModelo->atMiscelaneoModelo->metMostrarSelect('FCP'));
        $this->atVista->assign('idOrden',$idOrden);
        $this->atVista->assign('orden',$this->atConPer->atOrdenCompraModelo->metMostrarOrdenDetalle($idOrden));
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para modificar un dia a letras
    public function metDiasLetras($dia){

        if(strlen($dia)==1){
            $dia="0".$dia;
        }
        $diaLetras=array(
            '01'=>'uno',
            '02'=>'dos',
            '03'=>'tres',
            '04'=>'cuatro',
            '05'=>'cinco',
            '06'=>'seis',
            '07'=>'siete',
            '08'=>'ocho',
            '09'=>'nueve',
            '10'=>'diez',
            '11'=>'once',
            '12'=>'doce',
            '13'=>'trece',
            '14'=>'catorce',
            '15'=>'quince',
            '16'=>'dieciseis',
            '17'=>'diecisiete',
            '18'=>'dieciocho',
            '19'=>'diecinueve',
            '20'=>'veinte',
            '21'=>'veintiuno',
            '22'=>'veintidos',
            '23'=>'veintitres',
            '24'=>'veinticuatro',
            '25'=>'veinticinco',
            '26'=>'veintiseis',
            '27'=>'veintisiete',
            '28'=>'veintiocho',
            '29'=>'veintinueve',
            '30'=>'treinta',
            '31'=>'treinta y uno'
        );

        return $diaLetras[$dia];
    } // END FUNCTION

    //Método para modificar un mes a letras
    public function metMesLetras($mes){

        if(strlen($mes)==1){
            $mes="0".$mes;
        }
        $metMesLetras=array(
            '01'=>'Enero',
            '02'=>'Febrero',
            '03'=>'Marzo',
            '04'=>'Abril',
            '05'=>'Mayo',
            '06'=>'Junio',
            '07'=>'Julio',
            '08'=>'Agosto',
            '09'=>'Septiembre',
            '10'=>'Octubre',
            '11'=>'Noviembre',
            '12'=>'Diciembre'
        );

        return $metMesLetras[$mes];
    } // END FUNCTION

    //Método para generar el documento de los controles perceptivos
    public function metGenerar()
    {
        $idControl = $this->metObtenerInt('idControl');
        $this->atDocx = new Docxpresso\CreateDocument(array('template' => ROOT . 'publico' . DS . 'procesoCompra' . DS . 'control-perceptivo-plantilla.odt'));

        $control = $this->atConPer->metMostrarOrdenControlPer($idControl);
        $controlDetalle = $this->atConPer->metMostrarConPerDetalle($idControl);

        $codCP='CP-'.$this->metRellenarCeros($control['num_control'],4).'-'.$control['anio'];
        $diaNum =  substr($control['fec_registro'],8,2);
        $mesNum =  substr($control['fec_registro'],5,2);
        $anio =  substr($control['fec_registro'],0,4);
        
        $AsisA=$this->atConPer->metMostrarEmpleado($control['fk_rhb001_num_empleado_conforme_a'],'a',$control['pk_num_control_perceptivo']);
        $AsisB=$this->atConPer->metMostrarEmpleado($control['fk_rhb001_num_empleado_conforme_b'],'b',$control['pk_num_control_perceptivo']);
        $AsisC=$this->atConPer->metMostrarEmpleado($control['fk_rhb001_num_empleado_conforme_c'],'c',$control['pk_num_control_perceptivo']);
        $AsisD=$this->atConPer->metMostrarEmpleado($control['fk_rhb001_num_empleado_conforme_d'],'d',$control['pk_num_control_perceptivo']);
        $AsisE=$this->atConPer->metMostrarEmpleado($control['fk_rhb001_num_empleado_conforme_e'],'e',$control['pk_num_control_perceptivo']);

        $conformeA = $AsisA['ind_nombre1']." ".$AsisA['ind_apellido1'];
        $cedulaA = $AsisA['ind_documento_fiscal'];
        $cargoA = $AsisA['ind_descripcion_cargo'];
        $tipoA = $AsisA['ind_nombre_detalle'];

        $conformeB = $AsisB['ind_nombre1']." ".$AsisB['ind_apellido1'];
        $cedulaB = $AsisB['ind_documento_fiscal'];
        $cargoB = $AsisB['ind_descripcion_cargo'];
        $tipoB = $AsisB['ind_nombre_detalle'];

        $conformeC = $AsisC['ind_nombre1']." ".$AsisC['ind_apellido1'];
        $cedulaC = $AsisC['ind_documento_fiscal'];
        $cargoC = $AsisC['ind_descripcion_cargo'];
        $tipoC = $AsisC['ind_nombre_detalle'];

        $conformeD = $AsisD['ind_nombre1']." ".$AsisD['ind_apellido1'];
        $cedulaD = $AsisD['ind_documento_fiscal'];
        $cargoD = $AsisD['ind_descripcion_cargo'];
        $tipoD = $AsisD['ind_nombre_detalle'];

        $conformeE = $AsisE['ind_nombre1']." ".$AsisE['ind_apellido1'];
        $cedulaE = $AsisE['ind_documento_fiscal'];
        $cargoE = $AsisE['ind_descripcion_cargo'];
        $tipoE = $AsisE['ind_nombre_detalle'];

        $analistas = "<strong>$conformeA</strong>, titular de la cédula de identidad Nº <strong>$cedulaA</strong>, actuando en su caracter de <strong>$cargoA ($tipoA)</strong>";

        $firmas[0] = "$tipoA<br/>$conformeA<br/>$cargoA";

        if($AsisB['ind_nombre1']!=null){
            $analistas .= ", <strong>$conformeB</strong>, titular de la cédula de identidad Nº <strong>$cedulaB</strong>, actuando en su caracter de <strong>$cargoB ($tipoB)</strong>";
            $firmas[1] = "$tipoB<br/>$conformeB<br/>$cargoB";
        }

        if($AsisC['ind_nombre1']!=null){
            $analistas .= ", <strong>$conformeC</strong>, titular de la cédula de identidad Nº <strong>$cedulaC</strong>, actuando en su caracter de <strong>$cargoC ($tipoC)</strong>";
            $firmas[2] = "$tipoC<br/>$conformeC<br/>$cargoC";
        }

        if($AsisD['ind_nombre1']!=null){
            $analistas .= ", <strong>$conformeD</strong>, titular de la cédula de identidad Nº <strong>$cedulaD</strong>, actuando en su caracter de <strong>$cargoD ($tipoD)</strong>";
            $firmas[3] = "$tipoD<br/>$conformeD<br/>$cargoD";
        }

        if($AsisE['ind_nombre1']!=null){
            $analistas .= ", <strong>$conformeE</strong>, titular de la cédula de identidad Nº <strong>$cedulaE</strong>, actuando en su caracter de <strong>$cargoE ($tipoE)</strong>";
            $firmas[4] = "$tipoE<br/>$conformeE<br/>$cargoE";
        }
        $firmas[5] = "Proveedor";

        $firmantes = "<table style=' width: 590px; text-align: center; font-weight: bold;'>";

        $c=1;
        foreach ($firmas AS $key=>$value) {
            if(isset($value)){
                if($c==1) {
                    $firmantes .= "<tr>";
                }
                $firmantes .= "<td><pre style='font-family: Times; font-size: 11pt;'>".$value."</pre></td>";
                if($c==2) {
                    $c=1;
                    $firmantes .= "</tr>";
                } else {
                    $c=2;
                }
            }
        }
        if($c==2) {
            $firmantes .= "</tr>";
        }

        $firmantes .= "</table>";

        $ley='proceden de conformidad con lo dispuesto en el 127 del Decreto Nro. 1.399 con Rango Valor y Fuerza de la Ley de Contrataciones Públicas, publicada en Gaceta Oficial de la República Bolivariana de Venezuela Nº 6.154 de fecha 19/11/2014';

//Colocar logo en los odt
        $headerStyle = array('style' => 'min-height: 3cm');
        $urlImg = ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . 'CES.jpg';
        $urlImg2 = ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . 'LOGOSNCF.png';
        $rowStyle = array('style' => 'min-height: 25px');
        $cellStyle = array('style' => 'margin-left: 0px; vertical-align: middle;');
        $this->atDocx->header($headerStyle)
            ->table(array('grid' => array('295px','290px')))
            ->row($rowStyle)
            ->cell($cellStyle)->paragraph()->image(array('src' => $urlImg, 'style' => 'width:100px; height:80px;'))
            ->cell($cellStyle)->paragraph()->image(array('src' => $urlImg2, 'style' => 'margin-left: 190px; width:320px; height:320px;'))
        ;
//Colocar logo en los odt

        $this->metReemplazar('codCP', $codCP);
        $this->metReemplazar('dia', $this->metDiasLetras($diaNum));
        $this->metReemplazar('diaNumero', $diaNum);
        $this->metReemplazar('mes',$this->metMesLetras($mesNum));
        $this->metReemplazar('anio',$anio);
        $this->metReemplazar('analistas',$analistas);
        $this->metReemplazar('ley',$ley);

        $listaProveedor = array();
        for ($i = 0; $i < count($controlDetalle); $i++)
        {
            if($controlDetalle[$i]['descripcionItem']){
                $desc=$controlDetalle[$i]['descripcionItem'];
            } else {
                $desc=$controlDetalle[$i]['descripcionComm'];
            }

            $listaProveedor[$i] = array(
                'unidadCompra' => $controlDetalle[$i]['unidadCompra'],
                'CP' => $controlDetalle[$i]['cantidadPedida'],
                'CR' =>  $controlDetalle[$i]['cantidadRecibida'],
                'Desc' =>  $desc,
                'OB' =>  ''
            );
        }

        $tablaItems = "<table border='1' style='font-family: Times; font-size: 9pt; width: 590px;'>
                        <tr style='text-align: center;'>
                            <td style='width: 118px; vertical-align: middle;'><b>Unidad <br/>de Compra</b></td>
                            <td style='width: 118px; vertical-align: middle;'><b>Cantidad<br/>Solicitada</b></td>
                            <td style='width: 236px;'><b>Descripción</b><br/></td>
                            <td style='width: 118px; vertical-align: middle;'><b>Cantidad<br/>Recibida</b></td>
                        </tr>";

        foreach($listaProveedor AS $element) {
            $tablaItems .= "<tr>
                            <td style='text-align: center;' rowspan='2'>".$element['unidadCompra']."</td>
                            <td style='text-align: center;' rowspan='2'>".$element['CP']."</td>
                            <td style='border-bottom-color:#ffffff;'>".$element['Desc']."</td>
                            <td style='text-align: center;' rowspan='2'>".$element['CR']."</td>
                           </tr>
                           <tr>
                           <td style='border-top-color:#ffffff;'>Observación: ".$element['OB']."</td>
                            </tr>";
        }

        $tablaItems .="</table>";

        if(isset($control['ind_codigo_procedimiento'])){
            $this->metReemplazar('proc',' bajo el procedimiento de Consulta de Precios '.$control['ind_codigo_procedimiento'].', ');
        }
        else {
            $this->metReemplazar('proc',' '.$control['ind_comentario'].', ');
        }
        $this->metReemplazar('empresa',$control['proveedor']);
        $this->metReemplazar('tablaItems',$tablaItems,1);
        $this->metReemplazar('firmantes',$firmantes,1);

        $this->atDocx->render(ROOT . 'publico' .DS.'procesoCompra' .DS.'controlPerceptivo'.$idControl.'.odt');
        $validacion['idControl']=$idControl;
        $validacion['ruta']='.'.DS.'publico' .DS.'procesoCompra' .DS.'controlPerceptivo'.$idControl.'.odt';
        echo json_encode($validacion);
        exit;
    }

    //Método para reemplazar las variables en el documento a generar ODT
    public function metReemplazar($nombre,$valor,$extra=false){
        if($extra==1) {
            $this->atDocx->replace(array($nombre => array('value' => $valor)), array('block-type' => true));
        } else {
            $this->atDocx->replace(array($nombre => array('value' => $valor)));
        }
    }

    //Método para paginar los controles perceptivos
    public function metJsonDataTabla($cual=false)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        if($cual=='crear'){
            $sql = "SELECT
                        orden.*,
                        concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS proveedor
                    FROM
                        lg_b019_orden AS orden
                        INNER JOIN lg_b017_clasificacion AS clasificacion ON clasificacion.pk_num_clasificacion = orden.fk_lgb017_num_clasificacion
                        INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
                        INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                        INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = orden.fk_a004_num_dependencia
                        AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                        WHERE orden.ind_tipo_orden = 'OC' AND orden.ind_estado='AP' AND orden.num_flag_control_perceptivo='1'";
            $camposExtra = array();
            $campos = array('ind_orden','proveedor');
        } else {
            $sql = "SELECT
                        orden.*,
                        concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS proveedor,
                        perceptivo.pk_num_control_perceptivo,
                        perceptivo.num_estatus AS percEstado
                    FROM
                        lg_b020_control_perceptivo AS perceptivo
                        INNER JOIN lg_b019_orden AS orden ON orden.pk_num_orden = perceptivo.fk_lgb019_num_orden
                        INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
                        INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                        INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = orden.fk_a004_num_dependencia
                        AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                        WHERE orden.ind_tipo_orden = 'OC' AND (orden.ind_estado='AP' OR orden.ind_estado='CO') ";

            $camposExtra = array('pk_num_control_perceptivo');
            $campos = array('pk_num_control_perceptivo','ind_orden','proveedor');
        }

        if ($busqueda['value']) {
            $sql .= " AND
                        (orden.ind_orden LIKE '%$busqueda[value]%' OR
                        persona.ind_nombre1 LIKE '%$busqueda[value]%' OR
                        persona.ind_apellido1 LIKE '%$busqueda[value]%' 
                        ) 
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_orden';
        #construyo el listado de botones

        if (in_array('LG-01-06-01-01-01-N',$rol) AND $cual=='crear') {
            $campos['boton']['Nuevo'] = array("
                <button accion='nuevo' title='Nuevo Control Perceptivo'
                        class='hacer logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        descipcion='El Usuario ha Creado el Control Perceptivo Nro. $clavePrimaria'
                        titulo='Crear Control Perceptivo' estado='nuevo'
                        idOrden='$clavePrimaria'
                        data-toggle='modal' data-target='#formModal' id='crear$clavePrimaria'>
                    <i class='md md-create'></i>
                </button>
                ",
                'if( $i["num_flag_control_perceptivo"]==1) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Nuevo'] = false;
        }

        if (in_array('LG-01-06-01-02-01-M',$rol) AND $cual!='crear') {
            $campos['boton']['Editar'] = array("
                <button accion='modificar' title='Editar Control Perceptivo Realizado'
                        class='hacer logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        descipcion='El Usuario ha Modificado el Control Perceptivo Nro. $clavePrimaria'
                        idConPer='pk_num_control_perceptivo' titulo='Modificar Control Perceptivo Realizado'
                        estado='modificar'
                        idOrden='$clavePrimaria'
                        data-toggle='modal' data-target='#formModal' id='modificar$clavePrimaria'>
                    <i class='fa fa-edit'></i>
                </button>
                ",
                'if( $i["pk_num_control_perceptivo"] AND $i["percEstado"]==1 ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-06-01-02-02-V',$rol) AND $cual!='crear') {
            $campos['boton']['Consultar'] = array("
                <button accion='ver' title='Consultar'
                        class='ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning'
                        descipcion='El Usuario ha Consultado el Control Perceptivo Nro. $clavePrimaria'
                        idConPer='pk_num_control_perceptivo' titulo='Consultar Control Perceptivo'
                        estado='ver'
                        idOrden='$clavePrimaria'
                        data-toggle='modal' data-target='#formModal' id='ver'>
                    <i class='icm icm-eye2'></i>
                </button>
                ",
                'if( $i["pk_num_control_perceptivo"] ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Consultar'] = false;
        }

        if (in_array('LG-01-06-01-02-03-G',$rol) AND $cual!='crear') {
            $campos['boton']['Generar'] = array("
                <button title='Generar Reporte Control Perceptivo'
                        idControl='pk_num_control_perceptivo'
                        class='generar logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        titulo='Generar Reporte' id='generar'>
                    <i class='md md-assignment'></i>
                </button>
                ",
                'if( $i["pk_num_control_perceptivo"] ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Generar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);
    }
}
