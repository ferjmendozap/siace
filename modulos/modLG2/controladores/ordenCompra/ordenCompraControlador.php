<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class ordenCompraControlador extends Controlador
{
    private $atOrden;
    private $atMiscelaneoModelo;
    private $atRequerimientoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atOrden = $this->metCargarModelo('ordenCompra', 'ordenCompra');
        $this->metObtenerLibreria('headerOrdenCompraReporte','modLG');
    }

    //Método para listar las ordenes
    public function metIndex($opcion=false)
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($js);

        $estadosArray = array(
            0=>'',
            'revisar'=>'PR',
            'conformar'=>'RV',
            'aprobar'=>'CN',
            'generar'=>'AP');

        $this->atVista->assign('estadoSimbolo',$estadosArray[$opcion]);

        if($opcion=="revisar") {
            $this->atVista->assign('estado','Revisar');
            $this->atVista->assign('op','revisar');
        }  elseif($opcion=="conformar") {
            $this->atVista->assign('estado','Conformar');
            $this->atVista->assign('op','conformar');
        }  elseif($opcion=="aprobar") {
            $this->atVista->assign('estado','Aprobar');
            $this->atVista->assign('op','aprobar');
        } else {
            $this->atVista->assign('estado','Listado de');
            $this->atVista->assign('op','listado');
        }

        $this->atVista->assign('listadoEstado', $this->atOrden->atRequerimientoModelo->atMiscelaneoModelo->metMostrarSelect('EDOLG'));
        $this->atVista->assign('predeterminado',$this->atOrden->atRequerimientoModelo->metBuscarPredeterminado());
        $this->atVista->assign('dependencia',$this->atOrden->atRequerimientoModelo->metListarDependencias());
        $this->atVista->assign('clas',$this->atOrden->atRequerimientoModelo->atClasificacionModelo->metListarClasificacion());


        $this->atVista->metRenderizar('listado');
    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para modificar el formato de las fechas
    public function metFormatoFecha($fecha,$formato,$separador){
        #1  2016-01-01 a 01-01-2016
        #0  01-01-2016 a 2016-01-01
        if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
            $dia=substr($fecha,8,2);
            $mes=substr($fecha,5,2);
            $anio=substr($fecha,0,4);
        } elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
            $dia=substr($fecha,0,2);
            $mes=substr($fecha,3,2);
            $anio=substr($fecha,6,4);
        } else {
            $dia=0;
            $mes=0;
            $anio=0;
        }
        if($mes!='00' AND $dia!='00' AND $anio!='0000') {
            if (checkdate($mes, $dia, $anio)==false){
                return "la fecha es incorrecta";
                exit;
            }
        }
        if($formato==1) {
            $fecha =$dia.$separador.$mes.$separador.$anio;
        } else {
            $fecha =$anio.$separador.$mes.$separador.$dia;
        }
        return $fecha;
    } // END FUNCTION

    //Método para crear y/o modificar las ordenes
    public function metCrearModificarOrdenCompra()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idOrden = $this->metObtenerInt('idOrden');
        $estado = $this->metObtenerTexto('estado');
        $ver = $this->metObtenerInt('ver');
        $int = $this->metObtenerInt('form','int');
        if ($valido == 1) {
            $this->metValidarToken();

            if (isset($int['mAfecto']) and $int['mAfecto']!=0){
                $ExcceccionInt = array('mNoAfecto','impuestos','oCargos');
            } else {
                $ExcceccionInt = array('mAfecto','impuestos','oCargos');
            }
            $ExcceccionAlphaNum = array('comentario','rechazo');

            $ind = $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
            $txt = $this->metValidarFormArrayDatos('form','txt');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum',$ExcceccionAlphaNum);
            $formula = $this->metValidarFormArrayDatos('form','formula');

            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $txt != null) {
                $validacion = $txt;
            } elseif ($alphaNum == null && $ind != null && $txt != null) {
                $validacion = array_merge($txt, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null) {
                $validacion = array_merge($txt, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $txt);
            }
            if($formula) {
                $validacion = array_merge($validacion, $formula);
            }

            if (!isset($validacion['sec'])){
                $validacion['status']="errorIC";
                echo json_encode($validacion);
                exit;
            }
            $contador=1;
            if($ver!=1) {
                foreach ($validacion['cant'] as $key=>$value) {

                    if(!isset($validacion['exonerado'][$key])){
                        $validacion['exonerado'][$key]="0";
                    }

                    if($validacion['cant'][$key]=="error"){
                        $validacion['status'] = 'errorCantidades';
                        echo json_encode($validacion);
                        exit;
                    }

                    if($validacion['precio'][$key]=="error"){
                        $validacion['precio'][$key]="error";
                        $validacion['status'] = 'errorPrecio';
                        echo json_encode($validacion);
                        exit;
                    }
                    if($idOrden==0){
                        $idOrden="0";
                    }

                    if($validacion['descPorc'][$key]=="error"){
                        $validacion['descPorc'][$key]="0";
                    }

                    if($validacion['descFijo'][$key]=="error"){
                        $validacion['descFijo'][$key]="0";
                    }

                }
            }

            /*
            while ($contador<=$validacion['sec'] AND $ver!=1) {
                $contador=$contador+1;
            }
*/

            $contador=1;
            foreach ($validacion['montoPartida'] as $key=>$value) {
                #$key es el idPartida
                #value es el monto

                $idPresupuesto = $this->atOrden->metBuscarPresupuestoDetalle($key);

                if(!$idPresupuesto){
                    $validacion['idPartida'.$key] = 'error';
                    $validacion['status'] = 'errorPresupuesto';
                    echo json_encode($validacion);
                    exit;
                }
                if($estado=='revisar' OR $estado=="Listado de" ) {
                    if($idPresupuesto['num_monto_ajustado']-$idPresupuesto['compromiso_dist']<$value){
                        $validacion['cod_partida'] = $idPresupuesto['cod_partida'];
                        $validacion['status'] = 'errorPresupuesto2';
                        $validacion['idPartida'.$key] = 'error';
                        echo json_encode($validacion);
                        exit;
                    }
                }
                if ($estado=='revisar' and $idOrden!=null){

                    $id3=$this->atOrden->metCrearEstadoDistribucion(
                        $idPresupuesto['pk_num_presupuesto_det'],$key,
                        $idPresupuesto['fk_cbb004_num_plan_cuenta_onco'],
                        $idPresupuesto['fk_cbb004_num_plan_cuenta_pub20'],
                        $contador,$idOrden,$value);
                    $validacion['id3']=$id3;
                }
                    $contador=$contador+1;
            }

            $id3="";
            $validacion['estado']=$estado;
            $validacion['idOrden']=$idOrden;
            if ($estado=='revisar' and $idOrden!=null){
                $estatus='RV';
                $empleado='fk_rhb001_num_empleado_revisado_por';
                $campoFecha="fec_revision";
                $estatusDetalle="PR";
                $validacion['status']='modificar';
            } elseif ($estado=='conformar' and $idOrden!=null){
                $estatus='CN';
                $empleado='fk_rhb001_num_empleado_conformado_por';
                $campoFecha="fec_conformacion";
                $estatusDetalle='PR';
                $validacion['status']='modificar';
            } elseif ($estado=='aprobar' and $idOrden!=null){
                $estatus='AP';
                $empleado='fk_rhb001_num_empleado_aprobado_por';
                $campoFecha="fec_aprobacion";
                $estatusDetalle='PE';
                $validacion['status']='modificar';
            } elseif ($estado=='anular' and $idOrden!=null){
                $estatus='AN';
                $empleado=false;
                $campoFecha=false;
                $estatusDetalle='PR';

                $validacion['status']='ok';
            } elseif ($estado=='reverzar' and $idOrden!=null){
                $estatus='PR';
                $empleado=false;
                $campoFecha=false;
                $estatusDetalle='PR';
                $id3=$this->atOrden->metEliminarEstadoDistribucion($idOrden);
                $validacion['status']='ok';
            }
            if(isset($estatus)){
                $id=$this->atOrden->metActualizarOrden($estatus,$idOrden,$empleado,$campoFecha);
                $id2=$this->atOrden->metActualizarOrdenDetalle($estatusDetalle,$idOrden);
                $validacion['idactualizar']=$id;
                $validacion['idactualizadetalle']=$id2;
                $validacion['id3']=$id3;
                $validacion['ind_estado']=$estatus;
            }

            if(!isset($validacion['comentario'])){
                $validacion['comentario']=false;
            }
            if(!isset($validacion['rechazo'])){
                $validacion['rechazo']=false;
            }

            if($estado!="Listado de" AND $estado!='modificar'){
                echo json_encode($validacion);
                exit;
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            $validacion['item'] = null;
            $validacion['comm'] = null;
            if($validacion['tipo']=='item'){
                $validacion['item'] = $validacion['num'];
            } else {
                $validacion['comm'] = $validacion['num'];
            }
            

            if(!isset($validacion['idOrdenDetalle'])) {
                $validacion['idOrdenDetalle'] = null;
            }

            $orden=$this->atOrden->atRequerimientoModelo->metContar('lg_b019_orden',date('y'),'fec_anio','AND ind_tipo_orden="'.$validacion['tipoOrden'].'"');
            $orden['cantidad'] = $orden['cantidad'] +1;
            $orden=$this->metRellenarCeros($orden['cantidad'],10);
            $validacion['orden']=$orden;
            $predeterminado=$this->atOrden->atRequerimientoModelo->metBuscarPredeterminado();
            $validacion['cCosto'] = $predeterminado['fk_a023_num_centro_costo'];

            if ($idOrden == "0") {
                $validacion['status']='nuevo';
                $id=$this->atOrden->metNuevaOrden(
                    $validacion
                );

            } else {
                $validacion['status']='modificar';
                $id=$this->atOrden->metModificarOrden(
                    $validacion['dependencia'],$validacion['centro_costo'],$validacion['clasificacion'],$validacion['almacen'],
                    $validacion['proveedor'],
                    $validacion['tipo_servicio'],$validacion['mBruto'],$validacion['impuestos'],
                    $validacion['mTotal'],$validacion['mPendiente'],$validacion['mAfecto'],
                    $validacion['mNoAfecto'],$validacion['formaPago'],$validacion['plazo'],
                    $validacion['entregar'],$validacion['direccion'],$validacion['comentario'],
                    $validacion['descripcion'],$validacion['cuenta'],
                    $validacion['partida'],null,
                    $validacion['sec'],$validacion['cant'],$validacion['precio'],
                    $validacion['montoPU'],$validacion['descPorc'],$validacion['descFijo'],
                    $validacion['exonerado'],$validacion['total'],$validacion['cuentaDetalle'],
                    $validacion['partidaDetalle'],$validacion['unidad'],$validacion['cCosto'],
                    $idOrden,$validacion['idOrdenDetalle'],$validacion['item'],$validacion['comm']
                );
            }
            $validacion['id'] = $id;

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['id'] = $id;
                echo json_encode($validacion);
                exit;
            }

            $validacion['idOrden']=$id;
            echo json_encode($validacion);
            exit;
        }

        if($idOrden!=0){
            $orden = $this->atOrden->metMostrarOrden($idOrden);
            $this->atVista->assign('formBD',$orden);
            $this->atVista->assign('estadoOrden',$this->metEstadoNombre($orden['estadoOS']));
            $this->atVista->assign('formBDD',$this->atOrden->metMostrarOrdenDetalle($idOrden));
            $this->atVista->assign('idOrden',$idOrden);
            $this->atVista->assign('cotizaciones',$this->atOrden->metBuscarCotizaciones($orden['fk_lgb013_num_adjudicacion']));
            $this->atVista->assign('requerimientos',$this->atOrden->metBuscarRequerimientos($orden['fk_lgb013_num_adjudicacion']));
        }
        $this->atVista->assign('ver',$ver);
        $this->atVista->assign('predeterminado',$this->atOrden->atRequerimientoModelo->metBuscarPredeterminado());
        $this->atVista->assign('partidaCuenta',$this->atOrden->metBuscarPartidaCuenta('403.18.01.00'));
        $this->atVista->assign('almacen',$this->atOrden->atRequerimientoModelo->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('clas',$this->atOrden->atRequerimientoModelo->atClasificacionModelo->metListarClasificacion(false,'REQUERIMIENTO DE AUTO REPOSICION'));
        $this->atVista->assign('formaPago',$this->atOrden->atMiscelaneoModelo->metMostrarSelect('FDPLG'));
        $this->atVista->assign('tipoServicio',$this->atOrden->metListarTipoServicio());
        $this->atVista->assign('dependencia',$this->atOrden->metListarDependencias());
        $this->atVista->assign('estado',$estado);
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    //Método para eliminar los detalles
    public function metEliminarDetalle()
    {
        $id = $this->metObtenerInt('id');
        $query=$this->atOrden->metEliminarDetalle($id);
        if(is_array($query)){
            $valido=array(
                'status'=>'error',
                'mensaje'=>'Disculpa. Pero el Detalle se encuentra en uso y no se puede eliminar'
            );
        }else{
            $valido=array(
                'status'=>'ok',
                'id'=>$id
            );
        }

        echo json_encode($valido);
    }

    //Método para calcular los montos de los detalles
    public function metCalcularMontos(){
        $iva = $this->metObtenerTexto('iva');
        $idDetalle = $this->metObtenerInt('sec');
        $cantDetalle = $this->metObtenerFormulas('cant');
        $precioDetalle = $this->metObtenerFormulas('precio');
        $descPorcDetalle = $this->metObtenerFormulas('descPorc');
        $descFijoDetalle = $this->metObtenerFormulas('descFijo');
        $exoneradoDetalle = $this->metObtenerInt('exonerado');

        $resultado['iva']=$iva;
        $resultado['exoneradoDetalle']=$exoneradoDetalle;
        $resultado['precioDetalle']=$precioDetalle;
        $resultado['id']=$idDetalle;
        $resultado['cant']=$cantDetalle;
        if ($exoneradoDetalle==0) {
            $iva=$precioDetalle*($iva/100);
        } else {
            $iva=0;
        }

        $decuento=0;
        if($descFijoDetalle!=0) {
            $decuento=$descFijoDetalle;
            $resultado['descPorc']=0;
            $resultado['descFijo']=$descFijoDetalle;
        }
        if ($descPorcDetalle!=0) {
            $decuento=$precioDetalle*($descPorcDetalle/100);
            $resultado['descPorc']=$descPorcDetalle;
            $resultado['descFijo']=0;
        }
        if($descFijoDetalle==0 and $descPorcDetalle==0){
            $resultado['descPorc']=0;
            $resultado['descFijo']=0;
        }

        $resultado['precio']=$precioDetalle;
        $resultado['precioUnitario']=($precioDetalle+$iva)-$decuento;
        $resultado['total']=($resultado['precioUnitario']*$cantDetalle);

        echo json_encode($resultado);
        exit;
    }

    //Método para buscar el iva y la retención de los tipos de servicios del proveedor
    public function metBuscarIvaRetencion(){
        $id = $this->metObtenerTexto('id');
        $resultado = $this->atOrden->metBuscarImpuesto($id);
        echo json_encode($resultado);
        exit;
    }

    //Método para verificar las partidas asociadas a los detalles
    public function metVerificarPartida(){
        $idPartida = $this->metObtenerInt('idPartida');
        $idOrden = $this->metObtenerInt('idOrden');
        $monto = $this->metObtenerFormulas('monto');
        $idPresupuesto = $this->atOrden->metBuscarPresupuestoDetalle($idPartida);
        $idPresupuesto2 = $this->atOrden->metBuscarPresupuestoDetallePreparado($idPartida,$idOrden);

        $validacion['status'] = 'ok';
        if($idPresupuesto['num_monto_ajustado']-$idPresupuesto['compromiso_dist']-$idPresupuesto2['num_monto_base']<$monto){
            $validacion['status'] = 'ok2';
        }
        if(!$idPresupuesto OR ($idPresupuesto['num_monto_ajustado']-$idPresupuesto['compromiso_dist']<$monto)){
            $validacion['status'] = 'error';
        }
        $validacion['disponible1'] = $idPresupuesto['num_monto_ajustado']-$idPresupuesto['compromiso_dist']-$idPresupuesto2['num_monto_base'];
        $validacion['disponible2'] = $idPresupuesto['num_monto_ajustado']-$idPresupuesto['compromiso_dist'];
        $validacion['monto'] = $monto;
        $validacion['num_monto_base'] = $idPresupuesto2['num_monto_base'];
        $validacion['num_monto_ajustado'] = $idPresupuesto['num_monto_ajustado'];
        $validacion['compromiso_dist'] = $idPresupuesto['compromiso_dist'];
        echo json_encode($validacion);
        exit;
    }

    //Método para buscar los centros de costo de la dependencia
    public function metBuscarDependencia(){
        $id = $this->metObtenerInt('id');
        $resultado = $this->atOrden->metBuscarDependencia($id);
        echo json_encode($resultado);
        exit;
    }

    //Método para buscar los servicios del proveedor
    public function metBuscarServicios(){
        $id = $this->metObtenerInt('id');
        $resultado = $this->atOrden->metBuscarServicios($id);
        echo json_encode($resultado);
        exit;
    }

    //Método para cambiar los numeros por letras
    public function metNumLetras($num, $fem = true, $dec = true) {
//if (strlen($num) > 14) die("El número introducido es demasiado grande");
        $matuni[2]  = "dos";
        $matuni[3]  = "tres";
        $matuni[4]  = "cuatro";
        $matuni[5]  = "cinco";
        $matuni[6]  = "seis";
        $matuni[7]  = "siete";
        $matuni[8]  = "ocho";
        $matuni[9]  = "nueve";
        $matuni[10] = "diez";
        $matuni[11] = "once";
        $matuni[12] = "doce";
        $matuni[13] = "trece";
        $matuni[14] = "catorce";
        $matuni[15] = "quince";
        $matuni[16] = "dieciseis";
        $matuni[17] = "diecisiete";
        $matuni[18] = "dieciocho";
        $matuni[19] = "diecinueve";
        $matuni[20] = "veinte";
        $matunisub[2] = "dos";
        $matunisub[3] = "tres";
        $matunisub[4] = "cuatro";
        $matunisub[5] = "quin";
        $matunisub[6] = "seis";
        $matunisub[7] = "sete";
        $matunisub[8] = "ocho";
        $matunisub[9] = "nove";
        $matdec[2] = "veint";
        $matdec[3] = "treinta";
        $matdec[4] = "cuarenta";
        $matdec[5] = "cincuenta";
        $matdec[6] = "sesenta";
        $matdec[7] = "setenta";
        $matdec[8] = "ochenta";
        $matdec[9] = "noventa";
        $matsub[3]  = "mill";
        $matsub[5]  = "bill";
        $matsub[7]  = "mill";
        $matsub[9]  = "trill";
        $matsub[11] = "mill";
        $matsub[13] = "bill";
        $matsub[15] = "mill";
        $matmil[4]  = "millones";
        $matmil[6]  = "billones";
        $matmil[7]  = "de billones";
        $matmil[8]  = "millones de billones";
        $matmil[10] = "trillones";
        $matmil[11] = "de trillones";
        $matmil[12] = "millones de trillones";
        $matmil[13] = "de trillones";
        $matmil[14] = "billones de trillones";
        $matmil[15] = "de billones de trillones";
        $matmil[16] = "millones de billones de trillones";
        $num = trim((string)@$num);
        if($num!=0){

            if ($num[0] == "-") {
                $neg = "menos ";
                $num = substr($num, 1);
            }else
                $neg = "";
            while ($num[0] == "0") $num = substr($num, 1);
            if ($num[0] < "1" or $num[0] > 9) $num = "0" . $num;
            $zeros = true;
            $punt = false;
            $ent = "";
            $fra = "";
            for ($c = 0; $c < strlen($num); $c++) {
                $n = $num[$c];
                if (! (strpos(".,´´`", $n) === false)) {
                    if ($punt) break;
                    else{
                        $punt = true;
                        continue;
                    }
                }elseif (! (strpos("0123456789", $n) === false)) {
                    if ($punt) {
                        if ($n != "0") $zeros = false;
                        $fra .= $n;
                    }else
                        $ent .= $n;
                }else
                    break;
            }

            $ent = "     " . $ent;

            if ($dec and $fra and ! $zeros) {
                $fin = " coma";
                for ($n = 0; $n < strlen($fra); $n++) {
                    if (($s = $fra[$n]) == "0")
                        $fin .= " cero";
                    elseif ($s == "1")
                        $fin .= $fem ? " una" : " un";
                    else
                        $fin .= " " . $matuni[$s];
                }
            }else
                $fin = "";
            if ((int)$ent === 0) return "Cero " . $fin;
            $tex = "";
            $sub = 0;
            $mils = 0;
            $neutro = false;

            while ( ($num = substr($ent, -3)) != "   ") {

                $ent = substr($ent, 0, -3);
                if (++$sub < 3 and $fem) {
                    $matuni[1] = "una";
                    $subcent = "as";
                }else{
                    //$matuni[1] = $neutro ? "un" : "uno";
                    $matuni[1] = $neutro ? "un" : "un";
                    $subcent = "os";
                }
                $t = "";
                $n2 = substr($num, 1);
                if ($n2 == "00") {
                }elseif ($n2 < 21)
                    $t = " " . $matuni[(int)$n2];
                elseif ($n2 < 30) {
                    $n3 = $num[2];
                    if ($n3 != 0) $t = "i" . $matuni[$n3];
                    $n2 = $num[1];
                    $t = " " . $matdec[$n2] . $t;
                }else{
                    $n3 = $num[2];
                    if ($n3 != 0) $t = " y " . $matuni[$n3];
                    $n2 = $num[1];
                    $t = " " . $matdec[$n2] . $t;
                }

                $n = $num[0];
                if ($n == 1) {
                    if ($num == 100) $t = " cien" . $t; else $t = " ciento" . $t;
                }elseif ($n == 5){
                    $t = " " . $matunisub[$n] . "ient" . $subcent . $t;
                }elseif ($n != 0){
                    $t = " " . $matunisub[$n] . "cient" . $subcent . $t;
                }

                if ($sub == 1) {
                }elseif (! isset($matsub[$sub])) {
                    if ($num == 1) {
                        $t = " mil";
                    }elseif ($num > 1){
                        $t .= " mil";
                    }
                }elseif ($num == 1) {
                    $t .= " " . $matsub[$sub] . "ón";
                }elseif ($num > 1){
                    $t .= " " . $matsub[$sub] . "ones";
                }
                if ($num == "000") $mils ++;
                elseif ($mils != 0) {
                    if (isset($matmil[$sub])) $t .= " " . $matmil[$sub];
                    $mils = 0;
                }
                $neutro = true;
                $tex = $t . $tex;
            }
            $tex = $neg . substr($tex, 1) . $fin;
            return $tex;
        } else {
            return 'Cero';
        }
    }

    //Método para cambiar los numeros por letras
    public function metConvertirLetras($numero, $tipo) {
        list($e, $d) = explode('.', $numero);
        $d = substr($d,0,2);
        if ($tipo == "moneda")
            return $this->metNumLetras($e, false, false)." bolivares con ".$this->metNumLetras($d, false, false)." centimos";
        else if ($tipo == "decimal")
            return $this->metNumLetras($e, false, false)." con ".$this->metNumLetras($d, false, false);
        else if ($tipo == "entero")
            return $this->metNumLetras($e, false, false);
    }

    //Método para generar el documento de la orden PDF
    public function metImprimir($idOrden)
    {
        $orden = $this->atOrden->metMostrarOrden($idOrden);
        $partida = $this->atOrden->metBuscarPartidaCuenta('403.18.01.00');
        $telefono = $this->atOrden->atRequerimientoModelo->atProveedorModelo->metListarTelefonos($orden['pk_num_persona']);
        $telf = "";
        foreach ($telefono as $titulo => $valor) {
            $telf = $telefono[$titulo]['ind_telefono'];
        }
        if($orden['ind_tipo_orden']=='OC'){
            $tituloOS = 'ORDEN DE COMPRA';
        } elseif($orden['ind_tipo_orden']=='OS'){
            $tituloOS = 'ORDEN DE SERVICIO';
            define('NRO_REQUERIMIENTO',$orden['cod_requerimiento']);
            define('FECHA_REQUERIMIENTO',$orden['fecReq']);
        }
        define('NRO_ORDEN',$orden['ind_orden'] );
        define('FECHA_ORDEN',$this->metFormatoFecha(substr($orden['fec_creacion'],0,10),1,'-') );
        define('PROVEEDOR', $orden['proveedor']);
        define('PROVEEDOR_RIF', $orden['ind_documento_fiscal']);
        define('PROVEEDOR_DIRECCION', $orden['ind_direccion']);
        define('PROVEEDOR_SNC', $orden['ind_num_inscripcion_snc']);
        define('PROVEEDOR_ENTREGA', $orden['ind_entregar_en']);
        define('PROVEEDOR_TELF',$telf);
        define('PROVEEDOR_CORREO',$orden['ind_email']);
        define('PROVEEDOR_PAGO',$orden['ind_nombre_detalle']);
        define('OBSERVACIONES',$orden['ind_descripcion']);

        $aprueba = $this->atOrden->metBuscarCargo($orden['fk_rhb001_num_empleado_aprobado_por']);
        define('NOMBRE_APRUEBA',$aprueba['ind_nombre1']." ".$aprueba['ind_apellido1']);
        define('CARGO_APRUEBA',$aprueba['ind_descripcion_cargo']);
        $conforma = $this->atOrden->metBuscarCargo($orden['fk_rhb001_num_empleado_conformado_por']);
        define('NOMBRE_CONFORMA',$conforma['ind_nombre1']." ".$conforma['ind_apellido1']);
        define('CARGO_CONFORMA',$conforma['ind_descripcion_cargo']);

        $this->atFPDF = new pdf('p','mm','Letter');
        $this->atFPDF->titulo = $tituloOS;

        $this->atFPDF->SetTitle('orden');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);

        $this->atFPDF->SetAutoPageBreak(true, 38);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);




        $this->atFPDF->SetXY(6,93);
        $this->atFPDF->SetFont('Arial', '', 8);
        $detalles = $this->atOrden->metMostrarOrdenDetalle($idOrden);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetWidths(array(10, 100, 15, 20, 25, 25));
//        $this->atFPDF->Set
        $this->atFPDF->SetAligns(array('C', 'L', 'C', 'R', 'R', 'R'));
        $this->atFPDF->Row(array('Item',
            utf8_decode('Descripción'),
            'Uni.',
            'Cant.',
            'P. Unitario',
            'Total'));
        $this->atFPDF->Ln(1);
        $i=0;
        $this->atFPDF->SetDrawColor(255, 255, 255);
        foreach ($detalles as $titulo1 => $valor1) {
            $i++;
            $idDetalle = $detalles[$titulo1]['pk_num_orden_detalle'];
            if($detalles[$titulo1]['pk_num_item']!=null){
                $descripcion = $detalles[$titulo1]['ind_descripcion_item'];
                $unidad = $detalles[$titulo1]['uniItem'];
            } else {
                $descripcion = $detalles[$titulo1]['ind_descripcion_commodity'];
                $unidad = $detalles[$titulo1]['uniComm'];
            }

            $this->atFPDF->Row(array($i,
                utf8_decode($descripcion),
                ucwords(strtolower($unidad)),
                number_format($detalles[$titulo1]['num_cantidad'],2,',','.'),
                number_format($detalles[$titulo1]['num_precio_unitario'],2,',','.'),
                number_format($detalles[$titulo1]['num_cantidad']*$detalles[$titulo1]['num_precio_unitario'],2,'.',',')
            ));
        }

        if($this->atFPDF->titulo=="ORDEN DE COMPRA") {
            $this->atFPDF->titulo = 'LISTA DE PARTIDAS';
        }
        if ($this->atFPDF->GetY() < 200) $y = 200;
        else {
            $this->atFPDF->AddPage();
            $y = $this->atFPDF->GetY();
        }
        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetDrawColor(0, 0, 0); $this->atFPDF->SetFillColor(0, 0, 0);
        $this->atFPDF->Rect(10, $y, 195, 0.1, "DF");

        //---- Corregir
        $tipo_impuesto = "(Impuesto ".number_format(12, 2, ',', '')." %): ";

        $monto_total_en_letras = $this->metConvertirLetras($orden['num_monto_total'],'moneda');

        $this->atFPDF->SetFillColor(245, 245, 245);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetXY(10, $y+2); $this->atFPDF->Cell(120, 5, ('Monto total en letras: '), 0, 1, 'L', 1);
        $this->atFPDF->SetXY(10, $y+7); $this->atFPDF->MultiCell(120, 4, utf8_decode((strtoupper($monto_total_en_letras))), 0, 'J');

        $this->atFPDF->SetXY(135, $y+2);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(43, 5, 'Monto Afecto: ', 0, 0, 'R', 1);
        $this->atFPDF->Cell(27, 5, number_format($orden['num_monto_afecto'], 2, ',', '.'), 0, 0, 'R');

        $this->atFPDF->SetXY(135, $y+8);
        $this->atFPDF->Cell(43, 5, 'Monto No Afecto: ', 0, 0, 'R', 1);
        $this->atFPDF->Cell(27, 5, number_format($orden['num_monto_no_afecto'], 2, ',', '.'), 0, 0, 'R');

        $this->atFPDF->SetXY(135, $y+14);
        $this->atFPDF->Cell(43, 5, $tipo_impuesto, 0, 0, 'R', 1);
        $this->atFPDF->Cell(27, 5, number_format($orden['num_monto_igv'], 2, ',', '.'), 0, 0, 'R');

        $this->atFPDF->SetXY(135, $y+20);
        $this->atFPDF->SetFont('Arial', 'B', 10);
        $this->atFPDF->Cell(43, 5, 'Monto Total: ', 0, 0, 'R', 1);
        $this->atFPDF->Cell(27, 5, number_format($orden['num_monto_total'], 2, ',', '.'), 0, 0, 'R');






        if($i!=0) {
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->AddPage();
            $y = $this->atFPDF->GetY();

            $this->atFPDF->SetXY(10, $y);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->SetWidths(array(20, 150, 25));
            $this->atFPDF->SetAligns(array('C', 'L', 'R'));
            $this->atFPDF->Row(array('Partida',
                utf8_decode('Descripción'),
                'Monto'));
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->Ln(1);

        }
        $partida = $this->atOrden->metBuscarPartidaCuentaOrden($idOrden);
        $cuentasCodigo = array();
        $cuentasDescripcion = array();
        $cuentasMonto = array();
        foreach ($partida as $titulo2 => $valor2) {
            $this->atFPDF->Row(array(
                $valor2['cod_partida'],
                utf8_decode($valor2['ind_denominacion']),
                number_format($valor2['num_monto'],2,'.',',')
            ));
            $cuentasCodigo[$valor2['cod_partida']] = $valor2['cod_cuenta'];
            $cuentasDescripcion[$valor2['cod_partida']] = $valor2['ind_descripcion'];
            if(isset($cuentasMonto[$valor2['cod_partida']])){
                $cuentasMonto[$valor2['cod_partida']] = $cuentasMonto[$valor2['cod_partida']] +$valor2['num_monto'];
            } else {
                $cuentasMonto[$valor2['cod_partida']] = $valor2['num_monto'];
            }
        }

        $y = $this->atFPDF->GetY() + 20;

        $this->atFPDF->SetXY(10, $y);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', 'B', 12);
        $this->atFPDF->Cell(195, 20, 'LISTA DE CUENTAS CONTABLES', 0, 1, 'C');
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetWidths(array(20, 150, 25));
        $this->atFPDF->SetAligns(array('C', 'L', 'R'));
        $this->atFPDF->Row(array('Cuenta',
            utf8_decode('Descripción'),
            'Monto'));
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->Ln(1);


        foreach ($cuentasCodigo as $titulo2 => $valor2) {
            //$total_partida += $partida[$titulo2]['num_precio_unitario'];
            $this->atFPDF->Row(array(
                $valor2,
                utf8_decode($cuentasDescripcion[$titulo2]),
                number_format($cuentasMonto[$titulo2],2,'.',',')
            ));
        }


        #salida del pdf
        $pdfRecibo=$this->atFPDF->Output("",'I');
        echo $pdfRecibo;
    }

    //Método para anular la orden
    public function metAnular()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $valido = $this->metObtenerInt('valido');
        $idOrden = $this->metObtenerInt('idOrden');
        $estado = $this->metObtenerAlphaNumerico('estado');
        if($valido==1){
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');
            if ($alphaNum != null){
                $validacion = $alphaNum;
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }


            $cantidadDetalle = $this->atOrden->metMostrarOrdenDetalle($idOrden);
            $contador = 0;
            while($contador<count($cantidadDetalle)){
                if ($estado=='PR' and $idOrden!=null){
                    $estatus='AN';
                } elseif ($estado!='PR' and $idOrden!=null){
                    $estatus='PR';
                }
                $empleado='fk_a018_num_seguridad_usuario';
                $campoFecha='fec_ultima_modificacion';
                $validacion['status']='ok';
                $estatusDetalle='PR';

                if(isset($estatus)){
                    $id=$this->atOrden->metActualizarOrden($estatus,$idOrden,$empleado,$campoFecha,$validacion['motivo']);
                    $id2=$this->atOrden->metActualizarOrdenDetalle($estatusDetalle,$idOrden);
                    $id3=$this->atOrden->metEliminarEstadoDistribucion($idOrden);
                }
                $contador=$contador+1;
            }
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['id'] = $id;
                $validacion['id2'] = $id2;
                $validacion['id3'] = $id3;
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idOrden'] = $idOrden;

            echo json_encode($validacion);
            exit;
        }
        $this->atVista->assign('estado',$estado);
        $this->atVista->assign('idOrden',$idOrden);
        $this->atVista->metRenderizar('anular','modales');
    }

    //Método para cambiar los estados por el nombre
    public function metEstadoNombre($estado){
        $e="";
        if($estado=="PR"){
            $e="Preparado";
        } elseif($estado=="RV"){
            $e="Revisado";
        } elseif($estado=="CN"){
            $e="Conformado";
        } elseif($estado=="CO"){
            $e="Completado";
        } elseif($estado=="AN"){
            $e="Anulado";
        } elseif($estado=="CE"){
            $e="Cerrado";
        } elseif($estado=="TE"){
            $e="Terminado";
        } elseif($estado=="DS"){
            $e="Desierto";
        } elseif($estado=="PE"){
            $e="Pendiente";
        } elseif($estado=="AP"){
            $e="Aprobado";
        }
        return $e;
    }

    //Método para paginar las ordenes
    public function metJsonDataTabla($opcion,
                                     $dependencia = false,
                                     $clasificacion = false,
                                     $proveedor = false,
                                     $buscar = false,
                                     $desde = false,
                                     $hasta = false,
                                     $estadoFiltro = false)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  orden.*,
                  concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS proveedor,
                  case orden.ind_estado
                    when 'PR' then 'Preparado'
                    when 'RV' then 'Revisado'
                    when 'CN' then 'Conformado'
                    when 'AP' then 'Aprobado'
                    when 'AN' then 'Anulado'
                    when 'CE' then 'Cerrado'
                    when 'CO' then 'Completado'
                  end as ind_estado_nombre,
                  case orden.ind_tipo_orden
                    when 'OC' then 'Compra'
                    when 'OS' then 'Servicio'
                  end as ind_tipo_orden_nombre,
                  DATE_FORMAT(orden.fec_creacion, '%d-%m-%Y') AS fec_creacion
                FROM lg_b019_orden AS orden
                INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = orden.fk_a004_num_dependencia
                AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                WHERE 1 
                ";
        if ($busqueda['value']) {
            $sql .= " AND
                        ( 
                        orden.ind_orden LIKE '%$busqueda[value]%' OR 
                        orden.fec_creacion LIKE '%$busqueda[value]%' OR 
                        persona.ind_nombre1 LIKE '%$busqueda[value]%' OR 
                        persona.ind_apellido1 LIKE '%$busqueda[value]%' OR 
                        orden.num_monto_total LIKE '%$busqueda[value]%' OR 
                        orden.num_monto_pagado LIKE '%$busqueda[value]%' OR 
                        orden.num_monto_pendiente LIKE '%$busqueda[value]%' OR 
                        (
                            CASE 
                                when 'Preparado' LIKE '%$busqueda[value]%' THEN 'PR'
                                when 'Revisado' LIKE '%$busqueda[value]%' THEN 'RV'
                                when 'Conformado' LIKE '%$busqueda[value]%' THEN 'CN'
                                when 'Aprobado' LIKE '%$busqueda[value]%' THEN 'AP'
                                when 'Anulado' LIKE '%$busqueda[value]%' THEN 'AN'
                                when 'Cerrado' LIKE '%$busqueda[value]%' THEN 'CE'
                                when 'Completado' LIKE '%$busqueda[value]%' THEN 'CO'
                            END
                        ) LIKE orden.ind_estado OR 
                        (
                            CASE 
                                when 'Compra' LIKE '%$busqueda[value]%' THEN 'OC'
                                when 'Servicio' LIKE '%$busqueda[value]%' THEN 'OS'
                            END
                        ) LIKE orden.ind_tipo_orden 
                        ) 
                        ";
        }
        if($opcion!='listado'){

            $estado = [
                'revisar'=>'PR',
                'conformar'=>'RV',
                'aprobar'=>'CN',
                'asignar'=>'AP',
            ];

            $sql.= " AND orden.ind_estado='$estado[$opcion]' ";
        }

        if ($dependencia!='' AND $dependencia!='false') {
            $sql .= " AND orden.fk_a004_num_dependencia=$dependencia ";
        }

        if($clasificacion!='' AND $clasificacion!='false'){
            $sql .= " AND orden.ind_tipo_orden='$clasificacion' ";
        }

        if($proveedor!='' AND $proveedor!='false'){
            $sql .= " AND orden.fk_lgb022_num_proveedor=$proveedor ";
        }
        if($desde!='' AND $desde!='false' AND $hasta!='' AND $hasta!='false'){
            $sql .= " AND (orden.fec_creacion>='$desde 00:00:00' AND orden.fec_creacion<='$hasta 23:59:59') ";
        }
        if ($buscar!='' AND $buscar!='false') {
            $sql .= " AND
                        ( 
                        orden.ind_tipo_orden LIKE '%$buscar%' OR 
                        orden.ind_orden LIKE '%$buscar%' OR 
                        orden.fec_creacion LIKE '%$buscar%' OR 
                        persona.ind_nombre1 LIKE '%$buscar%' OR 
                        persona.ind_apellido1 LIKE '%$buscar%' OR 
                        orden.num_monto_total LIKE '%$buscar%' OR 
                        orden.num_monto_pagado LIKE '%$buscar%' OR 
                        orden.num_monto_pendiente LIKE '%$buscar%'  OR 
                        (
                            CASE 
                                when 'Preparado' LIKE '%$buscar%' THEN 'PR'
                                when 'Revisado' LIKE '%$buscar%' THEN 'RV'
                                when 'Conformado' LIKE '%$buscar%' THEN 'CN'
                                when 'Aprobado' LIKE '%$buscar%' THEN 'AP'
                                when 'Anulado' LIKE '%$buscar%' THEN 'AN'
                                when 'Cerrado' LIKE '%$buscar%' THEN 'CE'
                                when 'Completado' LIKE '%$buscar%' THEN 'CO'
                            END
                        ) LIKE orden.ind_estado OR 
                        (
                            CASE 
                                when 'Compra' LIKE '%$buscar%' THEN 'OC'
                                when 'Servicio' LIKE '%$buscar%' THEN 'OS'
                            END
                        ) LIKE orden.ind_tipo_orden
                        ) 
                        ";
        }

        if ($estadoFiltro!='' AND $estadoFiltro!='false') {
            $sql .= " AND orden.ind_estado = '$estadoFiltro' ";
        }
//        var_dump($sql);

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array(
            'pk_num_orden',
            'ind_tipo_orden_nombre',
            'ind_tipo_orden',
            'ind_orden',
            'fec_creacion',
            'proveedor',
            'num_monto_total',
            'num_monto_pagado',
            'num_monto_pendiente',
            'ind_estado_nombre');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_orden';
        #construyo el listado de botones
        $iconos = [
            'revisar'=>'',
            'conformar'=>'2',
            'aprobar'=>'3',
        ];

        if (in_array('LG-01-01-12-01-02-M',$rol) AND $opcion == 'listado') {
            $campos['boton']['Editar'] = array("
                <button accion='modificar' title='Editar Orden'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        idOrden='$clavePrimaria'
                        descipcion='El Usuario ha Modificado la Orden Nro. $clavePrimaria'
                        titulo='Modificar Orden'
                        estado='modificar'
                        data-toggle='modal' data-target='#formModal' id='modificar$clavePrimaria'>
                    <i class='fa fa-edit'></i>
                </button>
                ",
                'if( $i["ind_estado"] == "PR" AND $i["fk_lgb013_num_adjudicacion"] == null) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-01-12-01-03-R',$rol) AND $opcion == 'revisar') {
            $campos['boton']['Revisar'] = "
                <button accion='modificar' title='".ucwords($opcion)." Orden'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        idOrden='$clavePrimaria'
                        descipcion='El Usuario ha Modificado la Orden Nro. $clavePrimaria'
                        titulo='".ucwords($opcion)." Orden'
                        estado='$opcion'
                        ver='1'
                        data-toggle='modal' data-target='#formModal' id='modificar$clavePrimaria'>
                    <i class='icm icm-rating$iconos[$opcion]'></i>
                </button>
                ";
        } else {
            $campos['boton']['Revisar'] = false;
        }

        if (in_array('LG-01-01-12-01-04-C',$rol) AND $opcion == 'conformar') {
            $campos['boton']['Conformar'] = "
                <button accion='modificar' title='".ucwords($opcion)." Orden'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        idOrden='$clavePrimaria'
                        descipcion='El Usuario ha Modificado la Orden Nro. $clavePrimaria'
                        titulo='".ucwords($opcion)." Orden'
                        estado='$opcion'
                        ver='1'
                        data-toggle='modal' data-target='#formModal' id='modificar$clavePrimaria'>
                    <i class='icm icm-rating$iconos[$opcion]'></i>
                </button>
                ";
        } else {
            $campos['boton']['Conformar'] = false;
        }

        if (in_array('LG-01-01-12-01-05-A',$rol) AND $opcion == 'aprobar') {
            $campos['boton']['Aprobar'] = "
                <button accion='modificar' title='".ucwords($opcion)." Orden'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        idOrden='$clavePrimaria'
                        descipcion='El Usuario ha Modificado la Orden Nro. $clavePrimaria'
                        titulo='".ucwords($opcion)." Orden'
                        estado='$opcion'
                        ver='1'
                        data-toggle='modal' data-target='#formModal' id='modificar$clavePrimaria'>
                    <i class='icm icm-rating$iconos[$opcion]'></i>
                </button>
                ";
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('LG-01-01-12-01-06-V',$rol) AND $opcion == 'listado') {
            $campos['boton']['Ver'] = "
                <button accion='modificar' title='Consultar Orden'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-warning'
                        idOrden='$clavePrimaria'
                        descipcion='El Usuario ha Consultado la Orden Nro. $clavePrimaria'
                        titulo='Consultar Orden'
                        estado='$opcion'
                        ver='1'
                        data-toggle='modal' data-target='#formModal' id='consultar$clavePrimaria'>
                    <i class='icm icm-eye2'></i>
                </button>
                ";
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-01-12-01-07-E',$rol) AND $opcion == 'listado') {
            $campos['boton']['Anular'] = array("
                <button accion='anular' title='Anular/Reversar Orden'
                        class='anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger'
                        idOrden='$clavePrimaria'
                        descipcion='El Usuario ha Anulado/Reversado la Orden Nro. $clavePrimaria'
                        titulo='Anular/Reversar Orden'
                        estado='anular'
                        data-toggle='modal' data-target='#formModal' id='anular$clavePrimaria'>
                    <i class='md md-block'></i>
                </button>
                ",
                'if( $i["ind_estado"] != "AN" AND $i["ind_estado"] != "CO") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Anular'] = false;
        }

        if (in_array('LG-01-01-12-01-08-I',$rol) AND $opcion == 'listado') {
            $campos['boton']['Imprimir'] = "
                <button accion='imprimir' title='Imprimir Orden'
                        class='imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        descipcion='El Usuario ha Impreso la Orden Nro. $clavePrimaria'
                        titulo='Imprimir Orden'
                        idOrden='$clavePrimaria'
                        id='imprimir'>
                    <i class='md md-print'></i>
                </button>
                ";
        } else {
            $campos['boton']['Imprimir'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }
}

