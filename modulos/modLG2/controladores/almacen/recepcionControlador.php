<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class recepcionControlador extends Controlador
{
    private $atRecepcion;
    private $atOrdenCompraModelo;
    private $atTransaccionModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atRecepcion= $this->metCargarModelo('recepcion', 'almacen');
    }

    //Método para listar las ordenes a recepcionar
    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('donde', 'Recepción en');
        $this->atVista->assign('listado', $this->atRecepcion->metListarOrdenesAprobadas());
        $this->atVista->metRenderizar('listado');
    }

    //Método para recepcionar las ordenes
    public function metCrearRecepcion()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idOrden = $this->metObtenerInt('idOrden');
        if($valido==1){
            $this->metValidarToken();

            $ind = $this->metValidarFormArrayDatos('form','int');
            $txt = $this->metValidarFormArrayDatos('form','txt');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');

            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $txt != null) {
                $validacion = $txt;
            } elseif ($alphaNum == null && $ind != null && $txt != null) {
                $validacion = array_merge($txt, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null) {
                $validacion = array_merge($txt, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $txt);
            }

            if($validacion['cant']>0){
                $contador=1;
                $cant = 0;
                $errorOpcion=0;
                while($contador<=$validacion['cant']){
                    $cant = $cant + 1;
                    if (!isset($validacion['opcion'][$cant])){
                        $validacion['opcion'][$cant] = "0";
                        $validacion['canti'][$cant] = "0";
                        $errorOpcion = $errorOpcion + 1;
                    }
                    if($validacion['canti'][$contador]==0 AND $validacion['opcion'][$cant] != "0"){
                        $validacion['canti']='error';
                        $validacion['status']='errorCant';
                        echo json_encode($validacion);
                        exit;
                    }
                    $contador=$contador+1;
                }
                if($cant==$errorOpcion){
                    $validacion['cant'] = "error";
                    $validacion['status']='errorOpcion';
                    echo json_encode($validacion);
                    exit;
                }
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            $puedeTran = $this->atRecepcion->atTransaccionModelo->metBuscarPeriodo();
            $nro = $this->atRecepcion->atTransaccionModelo->metMostrarNro('E');
            $validacion['nro'] = $this->metRellenarCeros(($nro['nro'] + 1),6);


            if($idOrden!=0 AND $puedeTran['num_estatus']==1){
                $id=$this->atRecepcion->atTransaccionModelo->metCrearTransaccion(
                    $validacion['nro'],$validacion['fdoc'],$validacion['cCosto'],
                    $validacion['docRef'],$validacion['comen'],0
                    ,0,$validacion['nota'],'2',$validacion['tipoTran'],
                    $validacion['almacen'], $validacion['tipoDoc'],
                    $validacion['cant'],$validacion['sAnt'],$validacion['canti'],
                    $validacion['precio'],$validacion['numItem'],null,
                    $validacion['numUni'],$validacion['idDetalleOrden'],null
                );
                if($id != 0 AND !is_array($id)){
                    $contador=0;
                    while($contador<$validacion['cant']){
                        $contador=$contador+1;
                        if($validacion['opcion'][$contador]==1){
                            if($validacion['canti'][$contador]+$validacion['cantiRecibida'][$contador]==$validacion['cantiPedida'][$contador]){
                                $id=$this->atRecepcion->atTransaccionModelo->metActualizarEstadoDet($validacion['idDetalleOrden'][$contador],'CO');
                            }
                            $unidad = $this->atRecepcion->metBuscarUnidad($validacion['numItem'][$contador]);

                            $cantidad = $unidad['num_cantidad'] * $validacion['canti'][$contador];

                            $id2=$this->atRecepcion->atTransaccionModelo->metActualizarStock($cantidad,$validacion['numItem'][$contador],5);
                            $validacion['id2']=$id2;
                        }
                    }
                    $reqDet = $this->atRecepcion->atOrdenCompraModelo->metMostrarOrdenDetalle($idOrden);
                    $c1=0;
                    $c2=0;
                    foreach ($reqDet as $key=>$value) {
                        $c1=$c1+1;
                        if($reqDet[$key]['detalleEstatus']=='CO'){
                            $c2=$c2+1;
                        }
                    }
                    if($c1==$c2){
                        $id3=$this->atRecepcion->atOrdenCompraModelo->metActualizarOrden('CO',$idOrden,'fk_a018_num_seguridad_usuario','fec_ultima_modificacion');
                    }
                    $validacion['id2']=$id2;
                }

                $validacion['status']='modificar';

            } else {
                $validacion['status'] = 'periodo';
                echo json_encode($validacion);
                exit;
            }
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['id'] = $id;
                echo json_encode($validacion);
                exit;
            }

            $validacion['idOrden']=$idOrden;

            echo json_encode($validacion);
            exit;
        }

        $idDepe = '';
        if($idOrden!=0){
            $datosOrden = $this->atRecepcion->atOrdenCompraModelo->metMostrarOrden($idOrden,1);
            $this->atVista->assign('formDB',$datosOrden);
            $idDepe = $datosOrden['fk_a004_num_dependencia'];
            $this->atVista->assign('formDBI',$this->atRecepcion->metMostrarOrdenDetalle($idOrden));
            $this->atVista->assign('idOrden',$idOrden);
        }
        $this->atVista->assign('tranDet',$this->atRecepcion->atTransaccionModelo->metListarTransaccionesDetalles());
        $this->atVista->assign('centroCosto',$this->atRecepcion->atOrdenCompraModelo->atRequerimientoModelo->metListarCentroCosto($idDepe));
        $this->atVista->assign('almacen',$this->atRecepcion->atTransaccionModelo->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('documento',$this->atRecepcion->atTransaccionModelo->atTipoDocumentoModelo->metListarTipoDoc());
        $this->atVista->assign('transaccion',$this->atRecepcion->atTransaccionModelo->atTipoTransaccionModelo->metListarTipoTran());
        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para calcular los precios y montos
    public function metCalcular()
    {
        $cantidad = $this->metObtenerInt('cantidad');
        $precio = $this->metObtenerTexto('precio');
        $valor['contador'] = $this->metObtenerInt('contador');
        $valor['total']=$cantidad*$precio;
        echo json_encode($valor);
        exit;
    }

    //Método para paginar las ordenes a recepcionar
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
              orden.pk_num_orden,
              orden.fec_prometida,
              orden.ind_orden,
              proveedor.pk_num_proveedor,
              concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS nombreProveedor,
              detalle.ind_nombre_detalle,
              clasificacion.ind_descripcion AS clasificaionDesc
            FROM
                lg_b019_orden AS orden
            INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            INNER JOIN lg_b014_almacen AS almacen ON orden.fk_lgb014_num_almacen = almacen.pk_num_almacen
            INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = orden.fk_a006_num_miscelaneos_forma_pago
            INNER JOIN lg_b017_clasificacion AS clasificacion ON orden.fk_lgb017_num_clasificacion = clasificacion.pk_num_clasificacion
            WHERE
              orden.ind_estado='AP' AND orden.ind_tipo_orden='OC' AND almacen.num_flag_commodity=0 AND orden.num_flag_control_perceptivo = 0 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (
                        transaccion.pk_num_transaccion LIKE '%$busqueda[value]%' OR
                        transaccion.num_transaccion LIKE '%$busqueda[value]%' OR
                        tipo_transaccion.ind_descripcion LIKE '%$busqueda[value]%' OR
                        transaccion.fec_anio LIKE '%$busqueda[value]%' OR
                        transaccion.fec_mes LIKE '%$busqueda[value]%' OR
                        transaccion.ind_num_documento_referencia LIKE '%$busqueda[value]%' OR
                        0 LIKE '%$busqueda[value]%' OR
                        1 LIKE '%$busqueda[value]%' OR
                        2 LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_orden','pk_num_proveedor','nombreProveedor','fec_prometida','ind_nombre_detalle','clasificaionDesc');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_orden';
        #construyo el listado de botones
        $camposExtra = array();
        if (in_array('LG-01-02-03-01-R',$rol)) {
            $campos['boton']['Aprobar'] = "
                <button accion='crear' title='Recepcionar'
                        class='crear logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        descipcion='El Usuario ha Recepcionar en Almacen la Orden Nro. $clavePrimaria'
                        titulo='Recepcionar en Almacen'
                        idOrden='$clavePrimaria'
                        data-toggle='modal' data-target='#formModal' id='crear'>
                    <i class='fa fa-sign-in'></i>
                </button>";
        } else {
            $campos['boton']['Aprobar'] = false;
        }
        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);
    }

}
