<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class transaccionControlador extends Controlador
{
    private $atTransaccion;
    private $atAlmacenModelo;
    private $atTipoDocumentoModelo;
    private $atTipoTransaccionModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        $this->atTransaccion = $this->metCargarModelo('transaccion', 'almacen');

    }

    //Método para listar las transacciones
    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('tipoTran', $this->atTransaccion->atTipoTransaccionModelo->metListarTipoTran());
        $this->atVista->assign('listadoEstado', $this->atTransaccion->atAlmacenModelo->atMiscelaneoModelo->metMostrarSelect('EDOLG'));
        $this->atVista->assign('dependencia',$this->atTransaccion->metListarDependencias());
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar las transacciones
    public function metCrearModificarTransaccion()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idTran = $this->metObtenerInt('idTran');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $periodo = $this->metObtenerTexto('periodo');
            $ejecutado = $this->metObtenerTexto('ejecutado');
            $this->metValidarToken();

            $ExcceccionInt= array('manual','pendiente','nro');
            $ind = $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
            $txt = $this->metValidarFormArrayDatos('form','txt');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum',array('nota'));
            $descripcion= $this->metObtenerFormulas('descripcion');
            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $txt != null) {
                $validacion = $txt;
            } elseif ($alphaNum == null && $ind != null && $txt != null) {
                $validacion = array_merge($txt, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null) {
                $validacion = array_merge($txt, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $txt);
            }

            if(!isset($validacion['idDetalleOrden'])) {
                $validacion['idDetalleOrden']=null;
            }
            if(!isset($validacion['idDetalleReq'])){
                $validacion['idDetalleReq']=null;
            }
            if(!isset($validacion['nota'])){
                $validacion['nota']=null;
            }

            $validacion['descripcion']=$descripcion;
            if (isset($validacion['numItem'])) {
                $cant = 1;
                foreach ($validacion['numItem'] AS $key=>$value){
                    if ($validacion['precio'][$key]=="error") {
                        $validacion['precio'][$key] = "0";
                    }
                    if ($validacion['canti'][$key]=="error") {
                        $validacion['canti'][$key] = "0";
                    }
                    if ($validacion['sAnt'][$key]=="error") {
                        $validacion['sAnt'][$key] = "0";
                    }
                    if (!isset($validacion['numItem'][$key])) {
                        $validacion['numItem'][$key] = "0";
                    }
                    if (!isset($validacion['numUni'][$key])) {
                        $validacion['numUni'][$key] = "0";
                    }
                    if (!isset($validacion['idDet'][$key])) {
                        $validacion['idDet'][$key] = "0";
                    }

                    $cant = $cant + 1;
                }
            } else {
                $validacion['cant'] = "error";
                $validacion['status']='errorItem';
                echo json_encode($validacion);
                exit;
            }


            $validacion['borrar'] = "0";

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if (!isset($validacion['manual'])) {
                $validacion['manual'] = 0;
            }
            if (!isset($validacion['pendiente'])) {
                $validacion['pendiente'] = 0;
            }
            $puedeTran = $this->atTransaccion->metBuscarPeriodo();

            if($idTran===0 AND $puedeTran['num_estatus']==1){
                $tipoTran = $this->atTransaccion->metBuscarTipoTransaccion($validacion['tipoTran'],1);
                $nro = $this->atTransaccion->metMostrarNro($tipoTran['cod_detalle']);

                $validacion['nro'] = $this->metRellenarCeros(($nro['nro'] + 1),6);
                $id=$this->atTransaccion->metCrearTransaccion(
                    $validacion['nro'],$validacion['fdoc'],$validacion['ccosto'],
                    $validacion['docRef'],$validacion['comen'],$validacion['manual'],
                    $validacion['pendiente'],$validacion['nota'],'0',$validacion['tipoTran'],
                    $validacion['almacen'],$validacion['tipoDoc'],
                    0,$validacion['sAnt'],$validacion['canti'],
                    $validacion['precio'],$validacion['numItem'],null,
                    $validacion['numUni'],$validacion['idDetalleOrden'],$validacion['idDetalleReq']
                );
                $periodo = date('Y-m');
                $validacion['status']='nuevo';
            }elseif($puedeTran['num_estatus']==1){
                $id=$this->atTransaccion->metModificarTransaccion(
                    $validacion['fdoc'],$validacion['ccosto'],
                    $validacion['docRef'],$validacion['comen'],$validacion['manual'],
                    $validacion['pendiente'],$validacion['nota'],$validacion['tipoTran'],
                    $validacion['almacen'],$validacion['tipoDoc'],0,
                    $validacion['sAnt'],$validacion['canti'],$validacion['precio'],
                    $validacion['numItem'],$validacion['numUni'],
                    $idTran,$validacion['idDet'],$validacion['descripcion']);
                $validacion['status']='modificar';
            } else {
                $validacion['status'] = 'periodo';
                echo json_encode($validacion);
                exit;
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['id'] = $id;
                echo json_encode($validacion);
                exit;
            }

            $validacion['idTran']=$id;
            $validacion['num_transaccion']=$validacion['nro'];
            $validacion['periodo']=$periodo;
            $validacion['ejecutado']=$ejecutado;

            echo json_encode($validacion);
            exit;
        }
        if($idTran!=0){
            $this->atVista->assign('formDB',$this->atTransaccion->metMostrarTransaccion($idTran));
            $this->atVista->assign('formDBI',$this->atTransaccion->metMostrarDetalles($idTran));
            $this->atVista->assign('idTran',$idTran);
            $this->atVista->assign('ver',$ver);
        }
        $predeterminado = $this->atTransaccion->metBuscarPredeterminado();
        $this->atVista->assign('centro',$this->atTransaccion->metListarCC("WHERE 1"));
        $this->atVista->assign('almacen',$this->atTransaccion->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('documento',$this->atTransaccion->atTipoDocumentoModelo->metListarTipoDoc());
        $this->atVista->assign('transaccion',$this->atTransaccion->atTipoTransaccionModelo->metListarTipoTran());
        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para crear las reversas de las transacciones
    public function metReversarTransaccion()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $idTran = $this->metObtenerInt('idTran');
        $transaccion = $this->atTransaccion->metMostrarTransaccion($idTran);
        if($transaccion['ind_cod_tipo_transaccion']=='REQ') {
            $codigo = 'ARE';
        }elseif($transaccion['ind_cod_tipo_transaccion']=='ROC') {
            $codigo = 'ARO';
        }elseif($transaccion['ind_nombre_detalle']=='INGRESO') {
            $codigo = 'AIN';
        } elseif($transaccion['ind_nombre_detalle']=='EGRESO') {
            $codigo = 'AEG';
        }

        $tipoTran = $this->atTransaccion->metBuscarTipoTransaccion($codigo);
        $transaccion['fk_lgb015_num_tipo_transaccion'] = $tipoTran['pk_num_tipo_transaccion'];
        $transaccion['ind_num_documento_referencia'] = $transaccion['num_transaccion'];
        $transaccion['num_transaccion'] = '';

        $this->atVista->assign('formDB',$transaccion);
        $this->atVista->assign('formDBI',$this->atTransaccion->metMostrarDetalles($idTran));
//        $this->atVista->assign('idTran',$idTran);
        $this->atVista->assign('cual',1);
        $predeterminado = $this->atTransaccion->metBuscarPredeterminado();
        $this->atVista->assign('centro',$this->atTransaccion->metListarCC("WHERE fk_a004_num_dependencia = '".$predeterminado['fk_a004_num_dependencia']."'"));
        $this->atVista->assign('almacen',$this->atTransaccion->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('documento',$this->atTransaccion->atTipoDocumentoModelo->metListarTipoDoc());
        $this->atVista->assign('transaccion',$this->atTransaccion->atTipoTransaccionModelo->metListarTipoTran());
        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para eliminar las transacciones
    public function metEliminarTransaccion()
    {
        $idTran = $this->metObtenerInt('idTran');
        if($idTran!=0){
            $id=$this->atTransaccion->metEliminarTransaccion($idTran);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Transaccion se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idTran
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para eliminar los detalles
    public function metEliminarDetalle()
    {
        $idDet= $this->metObtenerInt('id');
        if($idDet!=0){
            $id=$this->atTransaccion->metEliminarDetalle($idDet);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Transaccion se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idDet
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para modificar el formato de las fechas
    public function metFormatoFecha($fecha,$formato,$separador){
        #1  2016-01-01 a 01-01-2016
        #0  01-01-2016 a 2016-01-01
        if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
            $dia=substr($fecha,8,2);
            $mes=substr($fecha,5,2);
            $anio=substr($fecha,0,4);
        } elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
            $dia=substr($fecha,0,2);
            $mes=substr($fecha,3,2);
            $anio=substr($fecha,6,4);
        } else {
            $dia=0;
            $mes=0;
            $anio=0;
        }
        if (checkdate($mes, $dia, $anio)==false){
            return "la fecha es incorrecta";
            exit;
        }
        if($formato==1) {
            $fecha =$dia.$separador.$mes.$separador.$anio;
        } else {
            $fecha =$anio.$separador.$mes.$separador.$dia;
        }
        return $fecha;
    }

    //Método para imprimir los datos de la transaccion a PDF
    public function metListadoImprimir($idTran)
    {
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $consulta= $this->atTransaccion->metMostrarTransaccion($idTran);
        $consultaDet= $this->atTransaccion->metMostrarDetalles($idTran);

        if(isset($consultaDet[0]['fk_lgb002_num_item'])) {
            define('IC','Item');
        } elseif(isset($consultaDet[0]['fk_lgb003_num_commodity'])) {
            define('IC','Commodity');
        } else {
            define('IC','');
        }

        $nro = $consulta['num_transaccion'];
        if($consulta['ind_nombre_detalle']=='EGRESO'){
            $consulta['ind_nombre_detalle'] = 'SALIDA';
        }

        define('NRO_INTERNO',$nro);
        define('FEC_DOC',$this->metFormatoFecha($consulta['fec_documento'],1,'-'));
        define('NOTA',ucwords(strtolower($consulta['ind_nombre_detalle'])));
        define('NOM_TRANSACCION',$consulta['nom_transaccion']);
        define('NRO_DOC_REF',$consulta['ind_num_documento_referencia']);
        define('NOM_PROV',$consulta['nomProveedor']);
        define('DOC_PROV',$consulta['ind_documento_fiscal']);
        define('TELF1',$consulta['ind_telefono']);
        define('DIRECCION',$consulta['ind_direccion']);
        define('TIPO_DOCUMENTO',$consulta['nom_documento']);
        define('NRO_DOCUMENTO',$consulta['ind_nota_entrega_factura']);
        define('COMENTARIO',$consulta['ind_comentario']);
        define('FEC_CREACION1',$this->metFormatoFecha(substr($consulta['fec_creacion1'],0,10),1,'-'));
        define('FEC_CREACION2',$this->metFormatoFecha(substr($consulta['fec_creacion2'],0,10),1,'-'));
        define('NOM_DESPACHO',$consulta['nomDespachadoPor']);
        define('CARGO_DESPACHO',$consulta['cargoDespachadoPor']);
        define('NRO_INTERNO_REFERENCIA',$consulta['doc_ref_ingreso']);
        define('DEP_SOLICITANTE',$consulta['dep_solicitante']);

        $aprobadoPor = $this->atTransaccion->metMostrarEmpleado($consulta['fk_rhb001_num_empleado_aprueba_despacho']);
        define('NOM_APRUEBA',$aprobadoPor ['ind_nombre1'].' '.$aprobadoPor ['ind_nombre2'].' '.$aprobadoPor ['ind_apellido1'].' '.$aprobadoPor ['ind_apellido2']);
        define('CARGO_APRUEBA',$aprobadoPor ['ind_descripcion_cargo']);

        ini_set('error_reporting', 'E_ALL & ~E_STRICT');
        $this->metObtenerLibreria('headerTransaccion','modLG');
        $this->atFPDF = new pdf('p','mm','letter');
        $this->atFPDF->SetTitle('Transaccion '.$nro);

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 10, 5);
        $this->atFPDF->SetAutoPageBreak(5, 40);
        $this->atFPDF->AddPage();

        ##
        $i=0;
        foreach($consultaDet AS $titulo => $valor){
            if($consultaDet[$titulo]['fk_lgb002_num_item']!=0){
                $codigoInterno = $consultaDet[$titulo]['ind_codigo_interno'];
                $descripcion = $consultaDet[$titulo]['descripcionItem'];
                $unidad =$consultaDet[$titulo]['unidadItem'];

                if($consulta['fec_documento']<'2017-09-13') {
                    $unidad2 =$consultaDet[$titulo]['unidadItem'];
                } else {
                    $unidad2 =$consultaDet[$titulo]['unidadItem2'];
                }
                $unidad2 =$consultaDet[$titulo]['unidadItem2'];

                $codigo =$consultaDet[$titulo]['fk_lgb002_num_item'];
            } else {
                $codigoInterno = $consultaDet[$titulo]['ind_cod_commodity'];
                $descripcion = $consultaDet[$titulo]['descripcionComm'];
                $unidad =$consultaDet[$titulo]['unidadComm'];
                $unidad2 =$consultaDet[$titulo]['unidadComm'];
                $codigo =$consultaDet[$titulo]['fk_lgb003_num_commodity'];
            }
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->Row(array(++$i,
                $codigoInterno,
                $codigo,
                utf8_decode($descripcion),
                utf8_decode($unidad),

                number_format($consultaDet[$titulo]['num_cantidad_transaccion'], 2, ',', '.'),
//                utf8_decode($unidad2),
                number_format($consultaDet[$titulo]['num_cantidad_transaccion'], 2, ',', '.'),
                number_format(0, 2, ',', '.')
            ));
            $this->atFPDF->Ln(1);
        }

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->Rect(10, $y, 200, 0.1, 'DF');
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(30, 5, utf8_decode('Nro. Items. '.$i), 0, 0, 'L', 0);
        $this->atFPDF->Output();

    }

    //Método para imprimir los datos de la transaccion de los activos a PDF
    public function metListadoImprimirActivos($idTran)
    {
        $consulta= $this->atTransaccion->metMostrarTransaccion($idTran);
        $consultaDet= $this->atTransaccion->metMostrarDetallesActivos($idTran);
        $nro = $consulta['num_transaccion'];
        if(strlen($consulta['num_transaccion'])<6){
            $c = 6-strlen($consulta['num_transaccion']);
            $nro = $consulta['num_transaccion'];
            for ($i=0; $i<$c; $i++){
                $nro = "0".$nro;
            }
        }
        if($consultaDet!=null){

            define('NRO_INTERNO',$nro);
            define('ANIO_ORDEN',$consulta['anioOrden']);
            define('NRO_ORDEN',$consulta['ind_orden']);
            define('NOM_PROV',$consulta['nomProveedor']);

            ini_set('error_reporting', 'E_ALL & ~E_STRICT');
            $this->metObtenerLibreria('headerTransaccionActivos','modLG');
            $this->atFPDF = new pdf('L','mm','letter');
            $this->atFPDF->SetTitle('Transaccion '.$nro);
            $this->atFPDF->AliasNbPages();
            $this->atFPDF->SetMargins(10, 10, 5);
            $this->atFPDF->SetAutoPageBreak(5, 40);
            $this->atFPDF->AddPage();

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->SetWidths(array(10, 20, 130, 20, 30, 30, 20));
            $this->atFPDF->SetAligns(array('C', 'C', 'L', 'C', 'C', 'C', 'C', 'L'));

            $c=0;
            foreach ($consultaDet as $key=>$value) {
                $c=$c+1;
                $this->atFPDF->Row(array($c,
                    $value['ind_cod_commodity'],
                    $value['ind_descripcion_ic'],
                    $value['ind_nro_serie'],
                    $value['ind_modelo'],
                    $value['marca'],
                    $value['color']
                ));
            }

            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(0, 0, 0);
            $y = $this->atFPDF->GetY();
            $this->atFPDF->Rect(10, $y, 260, 0.1, 'DF');
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(30, 5, utf8_decode('Nro. Activos. '.$c), 0, 0, 'L', 0);

            $this->atFPDF->Output();
        }

    }

    //Método para mostrar los PDF
    public function metWizardPdf()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementosCss = array(
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $idTran = $this->metObtenerInt('idTran');
        $this->atVista->assign('idTran',$idTran);
        $this->atVista->metRenderizar('wizardPdf');
    }

    //Método para paginar las transacciones
    public function metJsonDataTabla($dependencia = false,
                                    $tipoTran = false,
                                    $centroCosto = false,
                                    $estado = false,
                                    $busquedaL = false,
                                    $desde = false,
                                    $hasta = false)
    {

        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  transaccion.*,
                  tipo_transaccion.ind_descripcion AS transaccion,
                  tipo_transaccion.ind_cod_tipo_transaccion,
                  detalle.ind_nombre_detalle,
                  concat_ws('-',transaccion.fec_anio, transaccion.fec_mes) AS periodo,
                    case transaccion.num_estatus
                      when '0' then 'Pendiente'
                      when '1' then 'Aprobado'
                      when '2' then 'Ejecutado'
                    end as estatus
                FROM
                  lg_d001_transaccion AS transaccion
                INNER JOIN lg_b015_tipo_transaccion AS tipo_transaccion ON tipo_transaccion.pk_num_tipo_transaccion = transaccion.fk_lgb015_num_tipo_transaccion
                INNER JOIN a006_miscelaneo_detalle AS detalle ON tipo_transaccion.fk_a006_num_miscelaneo_detalle_tipo_documento = detalle.pk_num_miscelaneo_detalle
                INNER JOIN a023_centro_costo AS costo ON transaccion.fk_a023_num_centro_costo = costo.pk_num_centro_costo
                WHERE 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (
                        transaccion.num_transaccion LIKE '%$busqueda[value]%' OR
                        tipo_transaccion.ind_descripcion LIKE '%$busqueda[value]%' OR
                        transaccion.fec_anio LIKE '%$busqueda[value]%' OR
                        transaccion.fec_mes LIKE '%$busqueda[value]%' OR
                        transaccion.ind_num_documento_referencia LIKE '%$busqueda[value]%' OR
                            (
                            CASE 
                                WHEN 'Pendiente' LIKE '%$busqueda[value]%' THEN 0
                                WHEN 'Aprobado' LIKE '%$busqueda[value]%' THEN 1
                                WHEN 'Ejecutado' LIKE '%$busqueda[value]%' THEN 2
                            END
                            ) LIKE transaccion.num_estatus 
                        )
                        ";
        }
        if ($dependencia!='false') {
            $sql .= " AND costo.fk_a004_num_dependencia = '$dependencia' ";
        }
        if ($tipoTran!='false') {
            $sql .= " AND tipo_transaccion.pk_num_tipo_transaccion = '$tipoTran' ";
        }
        if ($centroCosto!='false') {
            $sql .= " AND costo.pk_num_centro_costo = '$centroCosto' ";
        }
        if ($estado!='false') {
            $sql .= " AND (
                            CASE 
                                WHEN 'Pendiente' LIKE '%$estado%' THEN 0
                                WHEN 'Aprobado' LIKE '%$estado%' THEN 1
                                WHEN 'Ejecutado' LIKE '%$estado%' THEN 2
                            END
                            ) LIKE transaccion.num_estatus ";
        }
        if ($busquedaL!='false') {
            $sql .= " AND
                        (
                        transaccion.num_transaccion LIKE '%$busquedaL%' OR
                        tipo_transaccion.ind_descripcion LIKE '%$busquedaL%' OR
                        transaccion.fec_anio LIKE '%$busquedaL%' OR
                        transaccion.fec_mes LIKE '%$busquedaL%' OR
                        transaccion.ind_num_documento_referencia LIKE '%$busquedaL%' OR 
                            (
                            CASE 
                                WHEN 'Pendiente' LIKE '%$busquedaL%' THEN 0
                                WHEN 'Aprobado' LIKE '%$busquedaL%' THEN 1
                                WHEN 'Ejecutado' LIKE '%$busquedaL%' THEN 2
                            END
                            ) LIKE transaccion.num_estatus 
                        ) ";
        }

        if ($desde!='false' AND $hasta!='false') {
            $sql .= " AND (transaccion.fec_documento >= '$desde' AND transaccion.fec_documento <= '$hasta') ";
        }


        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_transaccion','num_transaccion','transaccion','periodo','ind_num_documento_referencia','estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_transaccion';
        #construyo el listado de botones

        if (in_array('LG-01-02-01-02-M',$rol)) {
            $campos['boton']['Editar'] = array("
                <button accion='modificar' title='Editar'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        data-keyboard='false' data-backdrop='static'
                        descipcion='El Usuario ha Modificado la Transaccion Nro. $clavePrimaria'
                        idTran='$clavePrimaria' titulo='Modificar Transaccion'
                        data-toggle='modal' data-target='#formModal' id='modificar'>
                    <i class='fa fa-edit'></i>
                </button>
                ",
                'if( $i["estatus"] !="Ejecutado") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-02-01-03-V',$rol)) {
            $campos['boton']['Consultar'] = "
                <button accion='ver' title='Consultar'
                        class='ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning'
                        data-keyboard='false' data-backdrop='static'
                        descipcion='El Usuario ha Consultado la Transaccion Nro. $clavePrimaria'
                        idTran='$clavePrimaria' titulo='Consultar Transaccion'
                        data-toggle='modal' data-target='#formModal' id='modificar'>
                    <i class='icm icm-eye2'></i>
                </button>
                ";
        } else {
            $campos['boton']['Consultar'] = false;
        }

        if (in_array('LG-01-02-01-04-E',$rol)) {
            $campos['boton']['Eliminar'] = array("
                <button accion='eliminar' title='Eliminar'
                        class='eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger'
                        data-keyboard='false' data-backdrop='static'
                        descipcion='El Usuario ha Eliminado la Transaccion Nro. $clavePrimaria'
                        idTran='$clavePrimaria' titulo='Eliminar Transaccion'
                        mensaje='Estas seguro que desea eliminar la Transaccion!!' id='eliminar'>
                    <i class='md md-delete'></i>
                </button>
                ",
                'if( $i["estatus"] !="Ejecutado") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        if (in_array('LG-01-02-01-03-V',$rol)) {
            $campos['boton']['Imprimir'] = "
                <button title='Imprimir' class='imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        descipcion='El Usuario ha Impreso la Transaccion Nro. $clavePrimaria'
                        idTran='$clavePrimaria'
                        titulo='Imprimir Transaccion'
                        data-toggle='modal' data-target='#formModal'>
                    <i class='md md-print'></i>
                </button>
                ";
        } else {
            $campos['boton']['Imprimir'] = false;
        }


        if (in_array('LG-01-02-01-06-R',$rol)) {
            $campos['boton']['Reversar'] = array("
                <button accion='reversar' title='Reversar'
                        class='reversar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        data-keyboard='false' data-backdrop='static'
                        descipcion='El Usuario ha Reversado la Transaccion Nro. $clavePrimaria'
                        idTran='$clavePrimaria' titulo='Reversar Transaccion'
                        data-toggle='modal' data-target='#formModal' id='reversar'>
                    <i class='md md-keyboard-return'></i>
                </button>
                ",
                'if( $i["estatus"] =="Ejecutado" 
                AND $i["ind_cod_tipo_transaccion"]!="ARE" 
                AND $i["ind_cod_tipo_transaccion"]!="ARO" 
                AND $i["ind_cod_tipo_transaccion"]!="AIN" 
                AND $i["ind_cod_tipo_transaccion"]!="AEG") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Reversar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);
    }
}
