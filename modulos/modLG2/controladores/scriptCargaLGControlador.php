<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

require_once RUTA_Modulo . 'modLG' . DS . 'dataLG.php';

class scriptCargaLGControlador extends Controlador
{
    use dataLG;
    private $atScriptCarga;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atScriptCarga= $this->metCargarModelo('scriptCargaLG');
    }

    public function metIndex()
    {
        echo "INICIANDO<br>";
        #Cargar el tipo de documento
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE TIPO DE DOCUMENTO<br>';
        $this->atScriptCarga->metCargarTipoDocLG($this->metDataTipoDocumentoLG());
        /*
        SELECT
            CodTransaccion AS ind_cod_tipo_transaccion,
            Descripcion AS ind_descripcion,
            TipoMovimiento AS fk_a006_num_miscelaneo_detalle_tipo_documento,
            TipoDocGenerado AS fk_lgb016_num_tipo_documento_generado,
            TipoDocTransaccion AS fk_lgb016_num_tipo_documento_transaccion,
            FlagVoucherConsumo AS num_flag_voucher_consumo,
            FlagVoucherAjuste AS num_flag_voucher_ajuste,
            Estado AS num_estatus
            FROM `lg_tipotransaccion` WHERE 1
        */
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE TIPO DE TRANSACCION<br>';
        $this->atScriptCarga->metCargarTipoTranLG($this->metDataTipoTransaccionLG());
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE ALMACEN<br>';
        $this->atScriptCarga->metCargarAlmacenLG($this->metDataAlmacenLG());
        /*
        SELECT `CodUnidad` AS ind_cod_unidad, `Descripcion` AS ind_descripcion, `TipoMedida` AS fk_a006_num_miscelaneos_tipo_medida, `Estado` AS num_estatus FROM `mastunidades` WHERE 1
        */
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE UNIDADES<br>';
        $this->atScriptCarga->metCargarUnidadesLG($this->metDataUnidadesLG());
        /*
         SELECT `CodUnidad` AS ind_cod_unidad_conversion, `CodEquivalente` AS ind_cod_unidad, `Cantidad` AS num_cantidad, `Estado` AS num_estatus FROM `mastunidadesconv` WHERE 1
         */
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE UNIDADES CONVERSION<br>';
        $this->atScriptCarga->metCargarUnidadesConverisionLG($this->metDataUnidadesConversionLG());

        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE FAMILIAS<br>';
        $this->atScriptCarga->metCargarFamiliasLG($this->metDataFamiliasLG());
        /*
        SELECT
        `CodLinea` AS ind_cod_linea,
        `CodFamilia` AS ind_cod_familia,
        `CodSubFamilia` AS ind_cod_subfamilia,
        `Descripcion` AS ind_descripcion,
        `Estado` AS num_estatus
         FROM `lg_clasesubfamilia` WHERE 1
        */
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE SUBFAMILIAS<br>';
        $this->atScriptCarga->metCargarSubFamiliasLG($this->metDataSubFamiliasLG());
        /*
        SELECT
        `Clasificacion` AS ind_cod_clasificacion,
        `Descripcion` AS ind_descripcion,
        `CodAlmacen` AS fk_lgb014_num_almacen,
        `TipoRequerimiento` AS fk_a006_num_miscelaneos_tipo_requerimiento,
        `FlagRecepcionAlmacen` AS num_flag_recepcion_almacen,
        `FlagRevision` AS num_flag_revision,
        `ReqAlmacenCompra` AS ind_cod_clasificacion,
        `FlagTransaccion` AS num_flag_transaccion,
        `FlagCajaChica` AS num_flag_caja_chica,
        `Estado` AS num_estatus,
        `FlagActivoFijo` AS num_flag_activo_fijo
        FROM `lg_clasificacion` WHERE 1
        */
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE CLASIFICACIONES<br>';
        $this->atScriptCarga->metCargarClasificacionLG($this->metDataClasificacionLG());
        /*
        SELECT
        cualitativo.`ind_cod_aspecto`,
        cualitativo.`num_puntaje_maximo`,
        cualitativo.`ind_nombre`,
        cualitativo.`num_estatus`,
        detalle.`cod_detalle`
        FROM
        `lg_e007_aspecto_cualitativo` AS cualitativo
        INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = cualitativo.fk_a006_num_miscelaneo_detalle
        WHERE 1
        */
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE ASPECTOS<br>';
        $this->atScriptCarga->metCargarAspectoLG($this->metDataAspectoLG());
        /*
        SELECT
        `CodInterno` AS ind_codigo_interno,
        `Descripcion` AS ind_descripcion,
        `CodTipoItem` AS fk_a006_num_miscelaneo_tipo_item,
        `CodUnidad` AS fk_lgb004_num_unidad_despacho,
        `CodUnidadComp` AS fk_lgb004_num_unidad_compra,
        `CodUnidadEmb` AS fk_lgc002_num_unidad_conversion,
        `CodLinea` AS cod_linea,
        `CodFamilia` AS cod_familia,
        `CodSubFamilia` AS fk_lgb007_num_clase_subfamilia,
        `FlagLotes` AS num_flag_lotes ,
        `FlagKit` AS num_flag_kit ,
        `FlagImpuestoVentas` AS num_flag_impuesto_venta,
        `FlagAuto` AS num_flag_auto,
        `FlagDisponible` AS num_flag_disponible,
        `FlagPresupuesto` AS num_flag_verificado_presupuesto,
        `Imagen` AS ind_imagen,
        `StockMin` AS num_stock_minimo,
        `StockMax` AS num_stock_maximo,
        `CtaInventario` AS fk_cbb004_num_plan_cuenta_inventario_oncop,
        `CtaGasto` AS fk_cbb004_num_plan_cuenta_gasto_oncop,
        `CtaInventarioPub20` AS fk_cbb004_num_plan_cuenta_inventario_pub_veinte,
        `PartidaPresupuestal` AS fk_prb002_num_partida_presupuestaria,
        `Estado` AS num_estatus,
        `CtaGastoPub20` AS fk_cbb004_num_plan_cuenta_gasto_pub_veinte
        FROM `lg_itemmast` WHERE 1
        */
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE ITEMS<br>';
        $this->atScriptCarga->metCargarItemsLG($this->metDataItemsLG());
        /*
         SELECT
            mast.Clasificacion AS fk_lgb017_num_clasificacion,
            sub.`Codigo` AS ind_cod_commodity,
            sub.`Descripcion` AS ind_descripcion,
            sub.`CodUnidad` AS fk_lgb004_num_unidad,
            sub.`cod_partida` AS fk_prb002_num_partida_presupuestaria,
            sub.`CodCuenta` AS fk_cbb004_num_plan_cuenta,
            sub.`CodCuentaPub20` AS fk_cbb004_num_plan_cuenta_pub_veinte,
            sub.`Estado` AS num_estatus,
            sub.`FlagPresupuesto` AS num_flag_vericado_presupuesto
            FROM `lg_commoditysub` AS sub
            INNER JOIN lg_commoditymast AS mast ON mast.CommodityMast = sub.CommodityMast
            WHERE 1
        */
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE COMMODITY<br>';
        $this->atScriptCarga->metCargarCommodityLG($this->metDataCommodityLG());

        echo "TERMINADO";
    }

    public function metProveedores()
    {

        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE PROVEEDORES<br>';
        $con = mysqli_connect('192.168.0.6','root','$usMon$2015');
        mysqli_select_db($con,'pruebaDelSistema');

        $sql = "SELECT 
                    pers.Busqueda,
                    pers.DocFiscal AS ind_documento_fiscal,
                    
                    prov.CodProveedor AS fk_a003_num_persona_proveedor,
                    prov.CodTipoDocumento AS fk_lgb016_num_tipo_documento, 
                    prov.CodTipoPago AS cod_tipo_pago, 
                    prov.CodFormaPago AS cod_forma_pago, 
                    prov.CodTipoDocumento,
                    prov.CodTipoServicio,
                    prov.CodTipoServicio AS cod_tipo_servicio, 
                    prov.DiasPago AS num_dias_pago, 
                    prov.RegistroPublico AS ind_registro_publico, 
                    prov.LicenciaMunicipal AS ind_licencia_municipal, 
                    prov.FechaConstitucion AS fec_constitucion, 
                    prov.RepresentanteLegal AS fk_a003_num_persona_representante, 
                    prov.ContactoVendedor AS fk_a003_num_persona_vendedor, 
                    prov.FlagSNC AS ind_snc, 
                    prov.NroInscripcionSNC AS ind_num_inscripcion_snc, 
                    prov.FechaEmisionSNC AS fec_emision_snc, 
                    prov.FechaValidacionSNC AS fec_validacion_snc, 
                    prov.Nacionalidad AS ind_nacionalidad, 
                    prov.CondicionRNC AS ind_condicion_rcn, 
                    prov.Calificacion AS ind_calificacion, 
                    prov.Nivel AS ind_nivel, 
                    prov.Capacidad AS num_capacidad_financiera
                FROM mastproveedores AS prov 
                INNER JOIN mastpersonas AS pers ON pers.CodPersona = prov.CodProveedor
                INNER JOIN mastciudades AS ciudad ON ciudad.CodCiudad = pers.CiudadDomicilio
                WHERE 1
                LIMIT 0,10";

        //LIMIT 0,10
        $query = mysqli_query($con,$sql);

        $cual = true;
        foreach ($query AS $key=>$value){
            $value['ind_nombre1'] = $value['Busqueda'];

            $value['fk_a003_num_persona_proveedor'] = $this->metVerificarPersona($value['ind_documento_fiscal']);
#            $value['fk_a003_num_persona_representante'] = $this->metVerificarPersona($value['fk_a003_num_persona_representante']);
#            $value['fk_a003_num_persona_vendedor'] = $this->metVerificarPersona($value['fk_a003_num_persona_vendedor']);

            if($value['ind_snc']=='S'){
                $value['ind_snc'] = 1;
            } else {
                $value['ind_snc'] = 0;
            }

            $tipoDoc = $this->atScriptCarga->atScriptCargaModelo->metBusquedaSimple('cp_b002_tipo_documento','','cod_tipo_documento = "'.$value['CodTipoDocumento'].'"');
            $value['fk_cpb002_num_tipo_documento'] = $tipoDoc['pk_num_tipo_documento'];

            $tipoServ = $this->atScriptCarga->atScriptCargaModelo->metBusquedaSimple('cp_b017_tipo_servicio','','cod_tipo_servicio = "'.$value['CodTipoServicio'].'"');
            $value['fk_cpb017_num_tipo_servicio'] = $tipoServ['pk_num_tipo_servico'];

            $this->atScriptCarga->metCargarProveedorLG($value,$cual);
            /*
            echo $value['ind_nombre1'].'----';
            echo "<br>";
            */

            $cual = false;
        }
    }

    public function metVerificarPersona($documentoFiscal){
        $con = mysqli_connect('192.168.0.6','root','$usMon$2015');
        mysqli_select_db($con,'pruebaDelSistema');

        $sql = "SELECT 
                    pers.Busqueda,
                    pers.DocFiscal AS ind_documento_fiscal,
                    pers.Email AS ind_email,
                    pers.TipoPersona AS ind_tipo_persona,
                    pers.Direccion AS ind_direccion,
                    pers.Telefono1,
                    pers.Telefono2,
                    pers.TelefEmerg1,
                    pers.TelefEmerg2,
                    ciudad.Ciudad AS fk_a010_num_ciudad
                    FROM
                mastpersonas AS pers
                LEFT JOIN mastciudades AS ciudad ON ciudad.CodCiudad = pers.CiudadDomicilio
                WHERE pers.DocFiscal = '$documentoFiscal' OR pers.Ndocumento = '$documentoFiscal'
                ";
        $query = mysqli_query($con,$sql);
        $datos = mysqli_fetch_assoc($query);

        $datos['ind_nombre1'] = $datos['Busqueda'];
        $ciudad = $this->atScriptCarga->atScriptCargaModelo->metBusquedaSimple('a010_ciudad','','ind_ciudad = "'.$datos['fk_a010_num_ciudad'].'"');
        $datos['fk_a010_num_ciudad'] = $ciudad['pk_num_ciudad'];
        if(!$ciudad['pk_num_ciudad']){
            $datos['fk_a010_num_ciudad'] = 1;
        }

        $persona = $this->atScriptCarga->atScriptCargaModelo->metBusquedaSimple('a003_persona','','ind_documento_fiscal = "'.$documentoFiscal.'" OR ind_cedula_documento = "'.$documentoFiscal.'"');

        if($persona['pk_num_persona']!=null){
            return $persona['pk_num_persona'];
        } else {
            $idPersona = $this->atScriptCarga->metCargarPersonaLG($datos);
            if(!is_numeric($idPersona)){
                return 1;
            } else {
                return $idPersona;
            }
        }
    }

    public function metCargarPrestaciones()
    {
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE PRESTACIONES SOCIALES<br>';

        $query = $this->atScriptCarga->metPrestacionesSociales();
        $cont = 0;
        $cual = true;
        foreach ($query AS $key=>$value){
//          $value['persona'] = $this->metVerificarPersona($value['Ndocumento']);
            $persona = $this->atScriptCarga->atScriptCargaModelo->metBusquedaSimple('a003_persona','','ind_documento_fiscal = "'.$value['DocFiscal'].'" OR ind_cedula_documento = "'.$value['Ndocumento'].'"');

            $datosPersona = $this->atScriptCarga->metCargarPkEmpleado($persona['pk_num_persona']);

            if ($value['fk_nmb001_num_tipo_nomina'] == '01') {
                $value['fk_nmb001_num_tipo_nomina'] = 12;
            } elseif ($value['fk_nmb001_num_tipo_nomina'] == '02') {
                $value['fk_nmb001_num_tipo_nomina'] = 13;
            } elseif ($value['fk_nmb001_num_tipo_nomina'] == '03') {
                $value['fk_nmb001_num_tipo_nomina'] = 14;
            } elseif ($value['fk_nmb001_num_tipo_nomina'] == '04') {
                $value['fk_nmb001_num_tipo_nomina'] = 15;
            } elseif ($value['fk_nmb001_num_tipo_nomina'] == 'AF') {
                $value['fk_nmb001_num_tipo_nomina'] = 27;
            } elseif ($value['fk_nmb001_num_tipo_nomina'] == '10') {
                $value['fk_nmb001_num_tipo_nomina'] = 11;
            } else {
                $value['fk_nmb001_num_tipo_nomina'] = $datosPersona['fk_nmb001_num_tipo_nomina'];

            }
            $value['fk_rhb001_num_empleado'] = $datosPersona['pk_num_empleado'];

            if ($value['Estado'] == 'A') {
              /* echo $value['fk_rhb001_num_empleado'] . '-->';
                echo $value['Ndocumento'] . '---->';
                echo $value['fk_nmb001_num_tipo_nomina'] . '---->';
                echo $value['fec_anio'] . '---->';
                echo $value['fec_mes'] . '---->';
                echo $value['num_sueldo_base'] . '---->';
                echo $value['num_total_asignaciones'] . '---->';
                echo $value['num_sueldo_normal'] . '---->';
                echo $value['num_alicuota_vacacional'] . '---->';
                echo $value['num_alicuota_fin_anio'] . '---->';
                echo $value['num_bono_especial'] . '---->';
                echo $value['num_bono_fiscal'] . '---->';
                echo $value['num_dias_trimestre'] . '---->';
                echo $value['num_dias_anual'] . '---->';
                echo $value['num_remuneracion_mensual'] . '---->';
                echo $value['num_remuneracion_diaria'] . '---->';
                echo $value['num_monto_trimestral'] . '---->';
                echo $value['num_monto_anual'] . '---->';
                echo $value['num_estatus'] . '---->';
                echo $value['num_monto_acumulado'] . '---->';
*/
                echo "<br>";
                $this->atScriptCarga->metCargaPrestaciones($value);

           }

        } // fin foreach
    }

    public function metCargarRetroactivos()

    {
        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE PRESTACIONES SOCIALES<br>';

        $query = $this->atScriptCarga->metRetroactivoSociales();
        $cont = 0;
        $cual = true;
        foreach ($query AS $key=>$value){
//            $value['persona'] = $this->metVerificarPersona($value['Ndocumento']);
            $persona = $this->atScriptCarga->atScriptCargaModelo->metBusquedaSimple('a003_persona','','ind_documento_fiscal = "'.$value['DocFiscal'].'" OR ind_cedula_documento = "'.$value['Ndocumento'].'"');

            $datosPersona = $this->atScriptCarga->metCargarPkEmpleado($persona['pk_num_persona']);
            if ($value['fk_nmb001_num_tipo_nomina'] == '01') {
                $value['fk_nmb001_num_tipo_nomina'] = 12;
            } elseif ($value['fk_nmb001_num_tipo_nomina'] == '02') {
                $value['fk_nmb001_num_tipo_nomina'] = 13;
            } elseif ($value['fk_nmb001_num_tipo_nomina'] == '03') {
                $value['fk_nmb001_num_tipo_nomina'] = 14;
            } elseif ($value['fk_nmb001_num_tipo_nomina'] == '04') {
                $value['fk_nmb001_num_tipo_nomina'] = 15;
            } elseif ($value['fk_nmb001_num_tipo_nomina'] == 'AF') {
                $value['fk_nmb001_num_tipo_nomina'] = 27;
            } elseif ($value['fk_nmb001_num_tipo_nomina'] == '10') {
                $value['fk_nmb001_num_tipo_nomina'] = 11;
            } else {
                $value['fk_nmb001_num_tipo_nomina'] = $datosPersona['fk_nmb001_num_tipo_nomina'];

            }
            $value['fk_rhb001_num_empleado'] = $datosPersona['pk_num_empleado'];

            if ($value['Estado'] == 'A' ) {

                echo $value['fk_rhb001_num_empleado']. '-->';
                echo $value['Estado'] .'->';
                /*

                echo $value['Ndocumento'] . '---->';
                echo $value['fk_nmb001_num_tipo_nomina'] . '---->';
                echo $value['fec_anio'] . '---->';
                echo $value['fec_mes'] . '---->';
                echo $value['num_sueldo_base'] . '---->';
                echo $value['num_total_asignaciones'] . '---->';
                echo $value['num_sueldo_normal'] . '---->';
                echo $value['num_alicuota_vacacional'] . '---->';
                echo $value['num_alicuota_fin_anio'] . '---->';
                echo $value['num_bono_especial'] . '---->';
                echo $value['num_bono_fiscal'] . '---->';
                echo $value['num_dias_trimestre'] . '---->';
                echo $value['num_dias_anual'] . '---->';
                echo $value['num_remuneracion_mensual'] . '---->';
                echo $value['num_remuneracion_diaria'] . '---->';
                echo $value['num_monto_trimestral'] . '---->';
                echo $value['num_monto_anual'] . '---->';
                echo $value['num_estatus'] . '---->';
*/
                echo "<br>";
               $this->atScriptCarga->metCargaRetroactivos($value);

          }
        }// fin foreach
    }

    public function metCargarAnticipo()
    {
        /* PK's MISCELANEOS JUSTIFICATIVOS
         * pk = 4733 cod_detalle = 1 nombre_detalle= construccion adquisicion
         * pk = 4734 cod_detalle = 2 nombre_detalle= liberacion de hipoteca
         * pk = 4735 cod_detalle = 3 nombre_detalle= pensiones escolares
         * pk = 4736 cod_detalle = 4 nombre_detalle= gastos por atencion media y hospitalaria
         * pk = 5797 cod_detalle = 5 nombre_detalle= Ninguno
         */

        echo '&nbsp;&nbsp;&nbsp;->>INICIO CARGA DE PRESTACIONES SOCIALES<br>';
        $query = $this->atScriptCarga->metAnticipoSociales();

        foreach ($query AS $key=>$value){

            $persona = $this->atScriptCarga->atScriptCargaModelo->metBusquedaSimple('a003_persona','','ind_documento_fiscal = "'.$value['DocFiscal'].'" OR ind_cedula_documento = "'.$value['Ndocumento'].'"');
            $datosPersona = $this->atScriptCarga->metCargarPkEmpleado($persona['pk_num_persona']);


            if ($value['fk_a006_num_miscelaneo_detalle_justificativo'] == '1') {
                $value['fk_a006_num_miscelaneo_detalle_justificativo'] = 4733;
            } elseif ($value['fk_a006_num_miscelaneo_detalle_justificativo'] == '2') {
                $value['fk_a006_num_miscelaneo_detalle_justificativo'] = 4734;
            } elseif ($value['fk_a006_num_miscelaneo_detalle_justificativo'] == '3') {
                $value['fk_a006_num_miscelaneo_detalle_justificativo'] = 4735;
            } elseif ($value['fk_a006_num_miscelaneo_detalle_justificativo'] == '4') {
                $value['fk_a006_num_miscelaneo_detalle_justificativo'] = 4736;
            } elseif ($value['fk_a006_num_miscelaneo_detalle_justificativo'] == '') {
                $value['fk_a006_num_miscelaneo_detalle_justificativo'] = 5797;
            }
            $value['fk_rhb001_num_empleado'] = $datosPersona['pk_num_empleado'];

            if($value['fec_anticipo'] == ''){
                $value['fec_anticipo'] = $value['fec_ultima_modificacion'];
            }

             if ($value['Estado'] == 'A' && $value['fk_rhb001_num_empleado'] !=''){
                echo $value['fk_rhb001_num_empleado'].'-->';
                echo $value['fec_anio'] .'-->';
                echo $value['fec_mes'] .'-->';
                echo $value['num_anticipo'] .'-->';
                echo $value['fk_a006_num_miscelaneo_detalle_justificativo'] .'-->';
                echo $value['ind_porcentaje'] .'-->';
                echo $value['fec_anticipo'] .'-->';
                echo $value['fec_ultima_modificacion'] .'-->';
                echo $value['fk_a018_num_seguridad_usuario'] .'-->';
                echo "<br>";

                 $this->atScriptCarga->metCargaAnticipo($value);

             }
        }

    }

}