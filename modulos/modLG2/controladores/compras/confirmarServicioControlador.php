<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class confirmarServicioControlador extends Controlador
{
    private $atConfirmacion;
    private $atOrdenCompraModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atConfirmacion = $this->metCargarModelo('confirmarServicio', 'compras');
        $this->metObtenerLibreria('header','modLG');
        $this->atFPDF = new pdf('P', 'mm', 'letter');
    }

    //Método para listar las confirmaciones
    public function metIndex()
    {
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->atConfirmacion->metListarConfirmaciones());
        $this->atVista->assign('listado2', $this->atConfirmacion->metListarConfirmaciones());
        $this->atVista->assign('listadoDetalles1', $this->atConfirmacion->metListarConfirmacionesDetalles('PR'));
        $this->atVista->assign('listadoDetalles2', $this->atConfirmacion->metListarConfirmacionesDetalles('CE'));
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear las confirmaciones
    public function metCrearConfirmacion()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idOrden = $this->metObtenerInt('idOrden');
        $idDet = $this->metObtenerInt('idDet');
        $secuencia = $this->metObtenerInt('secuencia');
        if($valido==1){
            $this->metValidarToken();
            $exepcion=array('recibida');

            $ind = $this->metValidarFormArrayDatos('form','int',$exepcion);
            $text = $this->metValidarFormArrayDatos('form','txt');
            if ($ind!=null and $text!=null){
                $validacion = array_merge($ind,$text);
            } elseif ($text!=null){
                $validacion = $text;
            } else {
                $validacion = $ind;
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            $orden=$this->atConfirmacion->atOrdenCompraModelo->atRequerimientoModelo->metContar('lg_b021_confirmacion_servicio',date('y'),'fec_anio');
            $orden['cantidad'] = $orden['cantidad'] +1;
            if(strlen($orden['cantidad'])<10){
                $contador=strlen($orden['cantidad']);
                while($contador<(10-strlen($orden['cantidad']))){
                    $orden['cantidad']="0".$orden['cantidad'];
                    $contador++;
                }
            }

            $validacion['orden'] = $orden['cantidad'];

            if($validacion['recibir']+$validacion['recibida']==$validacion['total']){
                $estado='CE';
            } else {
                $estado='PE';
            }
            $validacion['estado'] = $estado;
            $validacion['idOrden'] = $idOrden;



            $clasificacion=$this->atConfirmacion->metBuscarClasificacionCxP('SER');
            $validacion['clasificacion'] = $clasificacion['pk_num_documento_clasificacion'];

            $datosOrden=$this->atConfirmacion->atOrdenCompraModelo->metMostrarOrden($idOrden);

            $datosOrdenDetalle=$this->atConfirmacion->atOrdenCompraModelo->metMostrarOrdenDetalle($idOrden);

            $validacion['datosOrdenDetalle'] = $datosOrdenDetalle;

            foreach ($datosOrdenDetalle as $key=>$value) {
                if($value['num_secuencia']==$validacion['secuencia']){
                    $precioCantidad = $value['num_precio_unitario']*$value['num_cantidad'];
                    $montoIva = $precioCantidad * 0.12;
                    if($value['num_flag_exonerado']==1){
                        $datosOrden['num_monto_afecto'] = '0';
                        $datosOrden['num_monto_no_afecto'] = $precioCantidad;
                    } else {
                        $datosOrden['num_monto_afecto'] = $precioCantidad;
                        $datosOrden['num_monto_no_afecto'] = '0';
                    }
                    $datosOrden['num_monto_igv'] = $montoIva;
                    $datosOrden['num_monto_total'] = $precioCantidad+$montoIva;
                    $datosOrden['num_monto_pendiente'] = $precioCantidad+$montoIva;
                }
            }

            $validacion['datosOrden'] = $datosOrden;
            if($idOrden!=0){
               $id=$this->atConfirmacion->metConfirmar($validacion);
                $validacion['status']='nuevo';

                $cantDetOrden1=$this->atConfirmacion->atOrdenCompraModelo->metMostrarOrdenDetalle($idOrden);
                $validacion['cantDetOrden'] = count($cantDetOrden1);

                $cantDetOrdenConfirm1=$this->atConfirmacion->metListarConfirmacionesDetallesRealizadas($idOrden,$secuencia);
                $validacion['cantDetOrdenConfirm'] = count($cantDetOrdenConfirm1);
                $validacion['quitarOrden'] = 0;

                if($validacion['cantDetOrden']==$validacion['cantDetOrdenConfirm']){
                    $estadoOrden=$this->atConfirmacion->atOrdenCompraModelo->metActualizarOrden('CO',$idOrden,'fk_a018_num_seguridad_usuario','fec_ultima_modificacion');
                    $validacion['quitarOrden'] = $idOrden;
                }
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['id'] = $id;
                echo json_encode($validacion);
                exit;
            }

            $validacion['idOrden']=$idOrden;
            $validacion['idDet']=$idDet;

            echo json_encode($validacion);
            exit;
        }
        if($idOrden!=0){
            $this->atVista->assign('formDB',$this->atConfirmacion->atOrdenCompraModelo->metMostrarOrden($idOrden));
            $this->atVista->assign('formDBDet',$this->atConfirmacion->metMostrarDetalle($idOrden,$secuencia));
            $this->atVista->assign('idOrden',$idOrden);
            $this->atVista->assign('idDet',$idDet);
            $this->atVista->assign('secuencia',$secuencia);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    //Método para desconfirmar
    public function metDesconfirmar()
    {
        $idOrden = $this->metObtenerInt('idOrden');
        $sec = $this->metObtenerInt('sec');
        if($idOrden!=0){
            $permiso=$this->atConfirmacion->metBuscarEstadoRegistro($idOrden,$sec);

            if($permiso['ind_estado']=='CE'){
                $permiso['ind_estado']='PE';
            }

            $id=$this->atConfirmacion->metDesconfirmar($idOrden,$sec,$permiso['ind_estado'],$permiso['pk_num_confirmacion']);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Confirmación se encuentra en uso y no se puede Desconfirmar',
                    'error'=>$id
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idOrden
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para imprimir las confirmaciones
    public function metImprimirConfirmacion($idOrden,$sec)
    {
        $this->atFPDF->SetTitle(utf8_decode('Confirmación de Servicios PDF'));
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();

        $datosOrden=$this->atConfirmacion->atOrdenCompraModelo->metMostrarOrden($idOrden);
        $datosServicio=$this->atConfirmacion->atOrdenCompraModelo->metMostrarOrdenDetalle($idOrden);
        $datosConfirmacion=$this->atConfirmacion->metMostrarDetalle($idOrden,$sec);
        $permiso=$this->atConfirmacion->metBuscarEstadoRegistro($idOrden,$sec);

        $nroVisual = substr($datosOrden['ind_orden'],6,4)."-".$datosOrden['fec_anio'];

        if($datosOrden['estadoOS']=="AP"){
            $estadoOS="Aprobada";
        } elseif($datosOrden['estadoOS']=="CE"){
            $estadoOS="Cerrada";
        } elseif($datosOrden['estadoOS']=="CO"){
            $estadoOS="Completada";
        }

        for($i=0;$i<count($datosServicio); $i++){
            if($datosServicio[$i]['num_secuencia']==$sec){
                $centroCosto = $datosServicio[$i]['ind_descripcion_centro_costo'];
                $cantidad=$datosServicio[$i]['num_cantidad'];
                $descripcion=$datosServicio[$i]['ind_descripcion_commodity'];
                $montoServicio=$datosServicio[$i]['num_precio_unitario'];
                $iva=$datosServicio[$i]['num_precio_unitario_iva']-$datosServicio[$i]['num_precio_unitario'];
                $montoTotal=$montoServicio+$iva;
            }
        }

        //	imprimo la cabecera
        $this->atFPDF->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 15, 11, 11, 12);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetXY(25, 10); $this->atFPDF->Cell(195, 5, utf8_decode('CONTRALORÍA DEL ESTADO MONGAGAS'), 0, 0, 'L');
        $this->atFPDF->SetXY(25, 13);
        $this->atFPDF->Cell(30, 5, utf8_decode('R.I.F. G-20001397-4'), 0, 0, 'L');
        $this->atFPDF->Cell(35, 5, utf8_decode('Telefono: 2916410441'), 0, 0, 'L');
        $this->atFPDF->Cell(35, 5, utf8_decode('Fax: '), 0, 0, 'L');
        $this->atFPDF->SetXY(25, 16); $this->atFPDF->Cell(195, 5, utf8_decode('Calle Sucre con calle Monagas, Edificio CONTRALORÍA DEL ESTADO SUCRE'), 0, 0, 'L');
        $this->atFPDF->SetXY(25, 19); $this->atFPDF->Cell(195, 5, utf8_decode('DIRECCION DE ADMINISTRACION Y PRESUPUESTO'), 0, 0, 'L');
        $this->atFPDF->SetFont('Arial', 'B', 12);
        $this->atFPDF->SetXY(10, 30);
        $this->atFPDF->Cell(195, 10, utf8_decode('CONFIRMACIÓN DE SERVICIOS Nº '.$nroVisual), 0, 1, 'C');
        $this->atFPDF->Ln(10);

        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetX(20); $this->atFPDF->SetFont('Arial', 'B', 12); $this->atFPDF->Cell(15, 8, 'De: ', 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial', '', 12); $this->atFPDF->Cell(15, 8, utf8_decode($centroCosto), 0, 1, 'L', 1);
        $this->atFPDF->SetX(20); $this->atFPDF->SetFont('Arial', 'B', 12); $this->atFPDF->Cell(15, 8, 'Para: ', 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial', '', 12); $this->atFPDF->Cell(15, 8, utf8_decode($datosOrden['ind_dependencia']), 0, 1, 'L', 1);
        $this->atFPDF->SetX(20); $this->atFPDF->SetFont('Arial', 'B', 12); $this->atFPDF->Cell(15, 8, 'Fecha: ', 0, 0, 'L', 1);
        $this->atFPDF->SetFont('Arial', '', 12); $this->atFPDF->Cell(15, 8, substr($datosOrden['fec_creacion'],0,10), 0, 1, 'L', 1);
        $this->atFPDF->SetX(20); $this->atFPDF->Cell(15, 8, 'Estado: ', 0, 0, 'L', 1);
        $this->atFPDF->Cell(15, 8, utf8_decode($estadoOS), 0, 1, 'L', 1);
        $this->atFPDF->Ln(10);

        $this->atFPDF->SetX(20);
        $this->atFPDF->MultiCell(175, 8, utf8_decode('Conste por el presente documento, que la CONTRALORÍA DEL ESTADO MONGAGAS ha recibido los servicios de '.
            $datosOrden['proveedor'].', correspondiente a '), 0, 'L',0);
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', 'B', 12);
        $this->atFPDF->SetWidths(array(20, 5, 170));
        $this->atFPDF->SetAligns(array('L', 'C', 'L'));
        $this->atFPDF->SetX(20);

        $this->atFPDF->Row(array(number_format($cantidad, 2, ',', '.'),
            '',
            $descripcion));

        $this->atFPDF->Ln(10);
        $this->atFPDF->SetX(20); $this->atFPDF->Cell(195, 8, utf8_decode('Según Contrato de Locación de Servicios O/S '), 0, 1, 'L', 1);
        $this->atFPDF->SetX(20); $this->atFPDF->Cell(195, 8, utf8_decode('Por lo tanto, solicito se proceda con el pago respectivo.'), 0, 1, 'L', 1);
        $this->atFPDF->Ln(10);

        $this->atFPDF->SetX(20);
        $this->atFPDF->Cell(60, 8, 'Monto Servicio: ', 0, 0, 'L', 1);
        $this->atFPDF->Cell(10, 8, 'Bs.', 0, 0, 'L', 1);
        $this->atFPDF->Cell(30, 8, number_format($montoServicio, 2, ',', '.'), 0, 1, 'R', 1);

        $this->atFPDF->SetX(20);
        $this->atFPDF->Cell(60, 8, 'I.V.A:  ', 0, 0, 'L', 1);
        $this->atFPDF->Cell(10, 8, 'Bs.', 0, 0, 'L', 1);
        $this->atFPDF->Cell(30, 8, number_format($iva, 2, ',', '.'), 0, 1, 'R', 1);

        $this->atFPDF->SetX(20);
        $this->atFPDF->Cell(60, 8, 'Monto Total a Pagar:  ', 0, 0, 'L', 1);
        $this->atFPDF->Cell(10, 8, 'Bs.', 0, 0, 'L', 1);
        $this->atFPDF->Cell(30, 8, number_format($montoTotal, 2, ',', '.'), 0, 1, 'R', 1);

        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->Rect(115, 239, 85, 0.1, "DF");
        $this->atFPDF->SetFont('Arial', 'B', 10);

        $this->atFPDF->SetXY(125, 240); $this->atFPDF->Cell(65, 5, 'El presente documento fue confirmado por', 0, 0, 'C');
        $this->atFPDF->SetXY(125, 245); $this->atFPDF->Cell(65, 5, utf8_decode($datosConfirmacion['conformador']), 0, 0, 'C');
        $this->atFPDF->SetXY(125, 250); $this->atFPDF->Cell(65, 5, substr($permiso['fec_confirmacion'],0,10), 0, 0, 'C');

        #salida del pdf
        $this->atFPDF->Output();
    }

}
