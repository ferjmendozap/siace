<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Guidmar Espinoza     | g.espinoza@contraloriamonagas.gob.ve    | 0414-1913443      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class requerimientoControlador extends Controlador
{
    private $atRequerimientoModelo;
    private $atMiscelaneoModelo;
    private $atAlmacenModelo;
    private $atClasificacionModelo;

    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atRequerimientoModelo = $this->metCargarModelo('requerimiento','compras');
        $this->metObtenerLibreria('headerRequerimiento','modLG');
        $this->atFPDF = new PDF('P', 'mm', 'Letter');
    }

    //Método para listar los requerimientos
    public function metIndex($opcion=false)
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($js);
        if(!$opcion){
            $opcion = 'listado';
        }
        $estadosArray = array(
            'listado'=>'',
            'revisar'=>'PR',
            'conformar'=>'RV',
            'aprobar'=>'CN',
            'generar'=>'AP');
        $this->atVista->assign('listadoEstado', $this->atRequerimientoModelo->atMiscelaneoModelo->metMostrarSelect('EDOLG'));
        $this->atVista->assign('predeterminado',$this->atRequerimientoModelo->metBuscarPredeterminado());
        $this->atVista->assign('dependencia',$this->atRequerimientoModelo->metListarDependencias());
        $this->atVista->assign('clas',$this->atRequerimientoModelo->atClasificacionModelo->metListarClasificacion());
        $this->atVista->assign('estado',$opcion);
        $this->atVista->assign('estadoSimbolo',$estadosArray[$opcion]);
        $this->atVista->metRenderizar('listado');
    }

    //Método para listar los item o commodity
    public function metItemCommodity($id,$clasif=false)
    {
        if ($id=="item" or $id=="item2" or $id=="item3" or $id=="item4") {
            #$this->atVista->assign('lista', $this->atRequerimientoModelo->atItemsModelo->metListarItems());
            $this->atVista->assign('lista', array());
            $this->atVista->assign('titulo', 'Items');

        } else {
            #$this->atVista->assign('lista', $this->atRequerimientoModelo->atCommodityModelo->metListarCommodity());
            $this->atVista->assign('lista', array());
            $this->atVista->assign('titulo', 'Commodities');
        }
        $tr = $this->metObtenerInt('tr');
        $this->atVista->assign('tr',$tr);
        $this->atVista->assign('id',$id);
        if($clasif){
            $this->atVista->assign('clasificacion',$clasif);
        }
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $this->atVista->metRenderizar('itemCommodity','modales');
    }

    //Método para listar los proveedores
    public function metProveedor($proveedor,$concepto = false)
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $tr = $this->metObtenerInt('tr');
        $this->atVista->assign('tr',$tr);
        $this->atVista->assign('proveedor', $proveedor);
        $this->atVista->assign('concepto', $concepto);
        $this->atVista->metRenderizar('proveedores','modales');
    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para crear y/o modificar requerimientos
    public function metCrearModificarRequerimiento()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $estado = $this->metObtenerAlphaNumerico('estado');
        $idReq = $this->metObtenerInt('idReq');
        $valido = $this->metObtenerInt('valido');
        $revisar = $this->metObtenerInt('revisar');

        if ($valido == 1) {
            $this->metValidarToken();
            $ExcceccionInt = array('fcajac','fpartida','costo','cant','proveedor');
            $int= $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');
            if ($int != null && $alphaNum == null) {
                $validacion = $int;
            } elseif ($int == null && $alphaNum != null) {
                $validacion = $alphaNum;
            } else {
                $validacion = array_merge($alphaNum, $int);
            }
            if (!isset($validacion['sec'])){
                $validacion['status']="errorIC";
                echo json_encode($validacion);
                exit;
            }
            if ($estado=='revisar' and $idReq!=null){
                $id=$this->atRequerimientoModelo->metActualizarRequerimiento('RV',$idReq,'fk_rhb001_num_empleado_revisado_por','fec_revision');
            } elseif ($revisar==1 and $idReq!=null){
                $estado="revisar";
#                $CuentaCC=$this->atRequerimientoModelo->metBuscarCuenta('111010101');
#                $id="";
                if(!isset($validacion['fcajac'])){
                    $validacion['fcajac'] = 0;
                    $validacion['fpartida'] = 0;
                } else {
                    if(!isset($validacion['fpartida']) ){
                        $validacion['fpartida'] = 'error';
                        $validacion['status']='errorCC';
                        echo json_encode($validacion);
                        exit;
                    }
                }



                $id=$this->atRequerimientoModelo->metActualizarRequerimiento('sin estado',$idReq,'caja',false,$validacion['fcajac']);
                $id=$this->atRequerimientoModelo->metActualizarRequerimiento('sin estado',$idReq,'partida',false,$validacion['fpartida']);
            }  elseif ($estado=='conformar' and $idReq!=null){
                $id=$this->atRequerimientoModelo->metActualizarRequerimiento('CN',$idReq,'fk_rhb001_num_empleado_conformado_por','fec_conformacion',$validacion['comentarioR']);
            } elseif ($estado=='aprobar' and $idReq!=null){
                $id=$this->atRequerimientoModelo->metActualizarRequerimiento('AP',$idReq,'fk_rhb001_num_empleado_aprobado_por','fec_aprobacion');
            } elseif ($estado=='anular' and $idReq!=null){
                $id=$this->atRequerimientoModelo->metActualizarRequerimiento('AN',$idReq);
            }
            if ($estado!="listado" AND $estado!="modificar") {
                $validacion['status']='modificar';
                if($idReq!=0){
                    $estado1 = $validacion['status'];
                    $validacion = $this->atRequerimientoModelo->metListarRequerimiento(1,1,$idReq);
                    $validacion['status']=$estado1;
                }
                $validacion['idReq']=$idReq;
                $validacion['error']=$id;
                $validacion['estado']=$estado;

                echo json_encode($validacion);
                exit;
            }
            if (isset($validacion['canti'])) {
                $cant = 1;
                foreach ($validacion['canti'] as $key=>$value) {

                    if(isset($value) AND $value=="error"){
                        $validacion['status'] = 'errorCantidades';
                        echo json_encode($validacion);
                        exit;
                    }

                    if (!isset($validacion['comentarioI'][$key])) {
                        $validacion['comentarioI'][$key] = "";
                    }
                    if (!isset($validacion['exonerado'][$key])) {
                        $validacion['exonerado'][$key] = "0";
                    }
                    if (!isset($validacion['numItem'][$key])) {
                        $validacion['numItem'][$key] = null;
                    }
                    if (!isset($validacion['numComm'][$key])) {
                        $validacion['numComm'][$key] = null;
                    }
                    if (!isset($validacion['numCC'][$key]) OR $validacion['numCC'][$key]=="error") {
                        $validacion['numCC'][$key] = $validacion['centro_costo'];
                    }
                    $cant = $cant + 1;
                }
                $validacion['secuencia'] = $cant;
            } else {
                $validacion['canti'] = "0";
                $validacion['comentarioI'] = "";
                $validacion['exonerado'] = "0";
                $validacion['numItem'] = "";
                $validacion['pk_num_unidad'] = "";
                $validacion['numComm'] = "";
                $validacion['cant'] ="0";
            }
            if (!isset($validacion['cajac'])) {
                $validacion['cajac'] = "0";
            }
            if (!isset($validacion['cant'])) {
                $validacion['cant'] = "0";
            }
            if (!isset($validacion['costo'])) {
                $validacion['costo'] = "0";
            }
            if (!isset($validacion['ppres'])) {
                $validacion['ppres'] = "0";
            }
            if (!isset($validacion['vcajac'])) {
                $validacion['vcajac'] = "0";
            }
            if (!isset($validacion['despacho'])) {
                $validacion['despacho'] = "0";
            }
            if ($validacion['proveedor']==0) {
                $validacion['proveedor'] = NULL;
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            $orden=$this->atRequerimientoModelo->metContar('lg_b001_requerimiento',date('y'),'fec_anio','AND fk_a004_num_dependencia='.$validacion['dependencia']);
            $orden['cantidad'] = $orden['cantidad'] + 1;

            $dependencia = $this->atRequerimientoModelo->metBuscarDependencia($validacion['dependencia']);

            $codigoReq=$dependencia['ind_codinterno']."-".$this->metRellenarCeros($orden['cantidad'],4)."-".date('Y');

            $val['codigo']=$codigoReq;
            if ($idReq == 0) {
                $validacion['rechazo'] = 'Ninguna';
                $validacion['status']='nuevo';
                $id=$this->atRequerimientoModelo->metNuevoRequerimiento(
                    $validacion['dependencia'],$validacion['centro_costo'],$validacion['clasificacion'],
                    $validacion['almacen'],$validacion['prioridad'],$validacion['cajac'],
                    $validacion['vcajac'],$validacion['ppres'],$validacion['proveedor'],
                    $codigoReq,$validacion['comentarioR'],$validacion['canti'],
                    $validacion['exonerado'],$validacion['comentarioI'],$validacion['especificacionTecnica'],$validacion['numItem'],
                    $validacion['numComm'],$validacion['numCC'],$validacion['despacho'],
                    $validacion['secuencia'],$validacion['pk_num_unidad']
                );
            } else {
                $validacion['status']='modificar';
                $id=$this->atRequerimientoModelo->metModificarRequerimiento(
                    $validacion['dependencia'],$validacion['centro_costo'],$validacion['clasificacion'],
                    $validacion['almacen'],$validacion['prioridad'],$validacion['cajac'],
                    $validacion['vcajac'],$validacion['ppres'],$validacion['proveedor'],
                    $codigoReq,$validacion['comentarioR'],$validacion['canti'],
                    $validacion['exonerado'],$validacion['comentarioI'],$validacion['especificacionTecnica'],
                    $validacion['numItem'],$validacion['numComm'],$validacion['numCC'],$validacion['despacho'],
                    $validacion['secuencia'],$validacion['pk_num_unidad'],$idReq
                );
            }
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['id'] = $id;
                echo json_encode($validacion);
                exit;
            }

            $validacion['idReq']=$id;
            $validacion['cod_requerimiento']=$codigoReq;
            echo json_encode($validacion);
            exit;
        }
        if ($idReq!=0) {
            $this->atVista->assign('formDBR',$this->atRequerimientoModelo->metBuscarRequerimiento($idReq));
            $this->atVista->assign('formDBD',$this->atRequerimientoModelo->metBuscarReqDetalle($idReq));
            $this->atVista->assign('empPrepara',$this->atRequerimientoModelo->metBuscarEmpleado($idReq,'fk_rhb001_num_empleado_preparado_por'));
            $this->atVista->assign('empRevisa',$this->atRequerimientoModelo->metBuscarEmpleado($idReq,'fk_rhb001_num_empleado_revisado_por'));
            $this->atVista->assign('empConforma',$this->atRequerimientoModelo->metBuscarEmpleado($idReq,'fk_rhb001_num_empleado_conformado_por'));
            $this->atVista->assign('empAprueba',$this->atRequerimientoModelo->metBuscarEmpleado($idReq,'fk_rhb001_num_empleado_aprobado_por'));
            $this->atVista->assign('idReq',$idReq);
        }
        $this->atVista->assign('predeterminado',$this->atRequerimientoModelo->metBuscarPredeterminado());
        $this->atVista->assign('estado',$estado);
        $this->atVista->assign('revisar',$revisar);
        $this->atVista->assign('dependencia',$this->atRequerimientoModelo->metListarDependencias());
        $this->atVista->assign('centroCosto',$this->atRequerimientoModelo->metListarCentroCosto());
        $this->atVista->assign('prioridad',$this->atRequerimientoModelo->atMiscelaneoModelo->metMostrarSelect('PRIOR'));
        $this->atVista->assign('almacen',$this->atRequerimientoModelo->atMiscelaneoModelo->metMostrarSelect('ALM'));
        $this->atVista->assign('clas',$this->atRequerimientoModelo->atClasificacionModelo->metListarClasificacion());
        $this->atVista->metRenderizar('CrearModificar', 'modales');
    }

    //Método para eliminar los detalles
    public function metEliminarDetalle()
    {
        $id = $this->metObtenerInt('id');
        $query=$this->atRequerimientoModelo->metEliminarDetalle($id);
        if(is_array($query)){
            $valido=array(
                'status'=>'error',
                'mensaje'=>'Disculpa. Pero el Requerimiento se encuentra en uso y no se puede eliminar'
            );
        }else{
            $valido=array(
                'status'=>'ok',
                'id'=>$id
            );
        }

        echo json_encode($valido);
    }

    //Método para buscar los centros de costo de la dependencia
    public function metBuscarCentroCosto()
    {
        $idDependencia = $this->metObtenerInt('idDependencia');
        $resultado = $this->atRequerimientoModelo->metListarCentroCosto($idDependencia);
        echo json_encode($resultado);
        exit;
    }

    //Método para buscar la clasificacion y su almacen
    public function metBuscarClasificacion()
    {
        $idClas = $this->metObtenerInt('idClas');
        $resultado = $this->atRequerimientoModelo->metBuscarClasificacion($idClas,1);
        echo json_encode($resultado);
        exit;
    }

    //Método para anular los requerimientos
    public function metAnular()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $idReq = $this->metObtenerInt('idReq');
        $valido = $this->metObtenerInt('valido');
        if($valido==1){
            $validacion = $this->metValidarFormArrayDatos('form','alphaNum');
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if($idReq!=0) {
                $id=$this->atRequerimientoModelo->metAnularRequerimiento($idReq,$validacion['motivo']);
                $validacion['status']='modificar';
            }
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($id);
                exit;
            }

            if($id!=0){
                $estado = $validacion['status'];
                $validacion = $this->atRequerimientoModelo->metListarRequerimiento(1,1,$id);
                $validacion['status']=$estado;
            }

            $validacion['idReq']=$id;
            echo json_encode($validacion);
            exit;
        }
        $req = $this->atRequerimientoModelo->metBuscarRequerimiento($idReq);

        $this->atVista->assign('codigo',$req['cod_requerimiento']);

        $this->atVista->assign('idReq',$idReq);
        $this->atVista->metRenderizar('anular','modales');
    }

    //Método para modificar el formato de las fechas
    public function metFormatoFecha($fecha,$formato,$separador,$mostrar=false){
        #1  2016-01-01 a 01-01-2016
        #0  01-01-2016 a 2016-01-01
        if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
            $dia=substr($fecha,8,2);
            $mes=substr($fecha,5,2);
            $anio=substr($fecha,0,4);
        } elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
            $dia=substr($fecha,0,2);
            $mes=substr($fecha,3,2);
            $anio=substr($fecha,6,4);
        } else {
            $dia=0;
            $mes=0;
            $anio=0;
        }
        if (checkdate($mes, $dia, $anio)==false){
            if($mostrar!=1){
                return "la fecha es incorrecta";
            } else {
                return "";
            }
            exit;
        }
        if($formato==1) {
            $fecha =$dia.$separador.$mes.$separador.$anio;
        } else {
            $fecha =$anio.$separador.$mes.$separador.$dia;
        }
        return $fecha;
    } // END FUNCTION

    //Método para generar el documento del requerimiento
    public function metImprimir($idReq)
    {
        $datos = $this->atRequerimientoModelo->metBuscarRequerimiento($idReq);
        $detalles = $this->atRequerimientoModelo->metBuscarReqDetalle($idReq);

        $preparado = $this->atRequerimientoModelo->metBuscarEmpleado($idReq,'fk_rhb001_num_empleado_preparado_por');
        $revisado = $this->atRequerimientoModelo->metBuscarEmpleado($idReq,'fk_rhb001_num_empleado_revisado_por');
        $conformado = $this->atRequerimientoModelo->metBuscarEmpleado($idReq,'fk_rhb001_num_empleado_conformado_por');
        $aprobado = $this->atRequerimientoModelo->metBuscarEmpleado($idReq,'fk_rhb001_num_empleado_aprobado_por');

        $estadosArray = array(
            'PR'=>'Preparado',
            'RV'=>'Revisado',
            'CN'=>'Conformado',
            'AP'=>'Aprobado',
            'AN'=>'Anulado',
            'CE'=>'Cerrado',
            'CO'=>'Completado');

        define('COD_REQ',$datos['cod_requerimiento']);
        define('DEPENDENCIA',$datos['ind_dependencia']);
        define('CENTRO_COSTO',$datos['ind_descripcion_centro_costo']);
        define('CLASIFICACION',$datos['clasf']);
        if($datos['clasf']!='STOCK DE ALMACEN'){
            define('TIPO','Compras');
        } else {
            define('TIPO','Almacen');
        }
        define('PRIORIDAD',$datos['ind_nombre_detalle']);
        define('FECHA_REQUERIDA',$this->metFormatoFecha($datos['fec_requerida'],1,'-'));
        define('ESTADO',$estadosArray[$datos['ind_estado']]);
        define('COMENTARIOS',$datos['ind_comentarios']);
        if($datos['num_flag_commodity']==0){
            define('IC','ITEM');
        } else {
            define('IC','COMMODITY');
        }

        define('NOMBRE_PREPARADO',$preparado['ind_nombre1']." ".$preparado['ind_apellido1']);
        define('CARGO_PREPARADO',$preparado['ind_descripcion_cargo']);
        define('FECHA_PREPARADO',$this->metFormatoFecha($datos['fec_preparacion'],1,'-',1));

        define('NOMBRE_REVISADO',$revisado['ind_nombre1']." ".$revisado['ind_apellido1']);
        define('CARGO_REVISADO',$revisado['ind_descripcion_cargo']);
        define('FECHA_REVISADO',$this->metFormatoFecha($datos['fec_revision'],1,'-',1));

        define('NOMBRE_CONFORMADO',$conformado['ind_nombre1']." ".$conformado['ind_apellido1']);
        define('CARGO_CONFORMADO',$conformado['ind_descripcion_cargo']);
        define('FECHA_CONFORMADO',$this->metFormatoFecha($datos['fec_conformacion'],1,'-',1));

        define('NOMBRE_APROBADO',$aprobado['ind_nombre1']." ".$aprobado['ind_apellido1']);
        define('CARGO_APROBADO',$aprobado['ind_descripcion_cargo']);
        define('FECHA_APROBADO',$this->metFormatoFecha($datos['fec_aprobacion'],1,'-',1));

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 10);
        $this->atFPDF->SetAutoPageBreak(1, 60);
        $this->atFPDF->AddPage();

        $this->atFPDF->primeraPagina();
        $this->atFPDF->campos();

        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);

        $i=0;
        foreach ($detalles as $key=>$value) {
            $i=$i+1;
            if(IC=='ITEM'){
                $codigo = $value['ind_codigo_interno'];
                $descripcion = $value['ind_descripcion_item'];
            } else {
                $codigo = $value['ind_cod_commodity'];
                $descripcion = $value['ind_descripcion_comm'];
            }
            $this->atFPDF->Ln(2);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->SetWidths(array(10, 20, 80, 20, 15,45));
            $this->atFPDF->SetAligns(array('R', 'C', 'L', 'C', 'R','L'));
            $this->atFPDF->Row(array($i,
                $codigo,
                utf8_decode($descripcion),
                $value['unidad'],
                number_format($value['num_cantidad_pedida'],2),$value['ind_especificacion_tecnica']));

            $y = $this->atFPDF->GetY();

            if($y>200){
                $this->atFPDF->AddPage();
                $this->atFPDF->campos();
            }

        }

        $this->atFPDF->Output();
    }

    //Método para paginar los requerimientos
    public function metJsonDataTabla($estado,
                                     $dependencia = false,
                                     $clasificacion = false,
                                     $centroCosto = false,
                                     $buscar = false,
                                     $desde = false,
                                     $hasta = false,
                                     $estadoFiltro = false
    )
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT 
                    req.*, 
                    CAST(req.fec_preparacion AS DATE) AS fec_preparacion,
                    CAST(req.fec_aprobacion AS DATE) AS fec_aprobacion,
                    case req.ind_estado
                      when 'PR' then 'Preparado'
                      when 'RV' then 'Revisado'
                      when 'CN' then 'Conformado'
                      when 'AP' then 'Aprobado'
                      when 'AN' then 'Anulado'
                      when 'CE' then 'Cerrado'
                      when 'CO' then 'Completado'
                    end as estatus,
                    c.*, 
                    c.num_flag_revision AS flagRevi, 
                    c.ind_descripcion AS clasificacion,
                    NULL AS acta1,
                    NULL AS acta2,
                    dependencia.ind_dependencia,
                    req.num_flag_caja_chica AS flagCajaReq,
                    c.num_flag_caja_chica AS flagCaja
                FROM 
                    lg_b001_requerimiento AS req 
                    INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = req.fk_a004_num_dependencia
                    INNER JOIN lg_b017_clasificacion AS c ON c.pk_num_clasificacion = req.fk_lgb017_num_clasificacion
                    INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = req.fk_a004_num_dependencia
                      AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                    WHERE 1
                     ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (req.cod_requerimiento LIKE '%$busqueda[value]%' OR
                        req.fec_preparacion LIKE '%$busqueda[value]%' OR
                        req.fec_aprobacion LIKE '%$busqueda[value]%' OR
                        req.ind_comentarios LIKE '%$busqueda[value]%' OR
                        req.ind_estado LIKE '%$busqueda[value]%' OR
                        c.ind_descripcion LIKE '%$busqueda[value]%' OR
                        dependencia.ind_dependencia LIKE '%$busqueda[value]%'
                        )
                        ";
        }
        $estadosArray = array(
            'revisar'=>'PR',
            'conformar'=>'RV',
            'aprobar'=>'CN',
            'generar'=>'AP');

        if($estado!='listado') {
            $sql.=" AND req.ind_estado LIKE '".$estadosArray[$estado]."'";
            if($estado=='generar'){
                $sql.=" AND c.ind_descripcion!='STOCK DE ALMACEN' AND req.ind_verificacion_caja_chica!=1 ";
            }
        }

        if ($estadoFiltro!='' AND $estadoFiltro!='false') {
            $sql .= " AND req.ind_estado = '$estadoFiltro' ";
        }

        if($dependencia!='' AND $dependencia!='false'){
            $sql .= " AND req.fk_a004_num_dependencia=$dependencia ";
        }

        if($clasificacion!='' AND $clasificacion!='false'){
            $sql .= " AND req.fk_lgb017_num_clasificacion=$clasificacion ";
        }

        if($centroCosto!='' AND $centroCosto!='false'){
            $sql .= " AND req.fk_a023_num_centro_costo=$centroCosto ";
        }
        if($desde!='' AND $desde!='false' AND $hasta!='' AND $hasta!='false'){
            $sql .= " AND (req.fec_requerida>='$desde 00:00:00' AND req.fec_requerida<='$hasta 23:59:59') ";
        }

        if($buscar!='' AND $buscar!='false'){
            $sql .= " AND
                            (req.cod_requerimiento LIKE '%$buscar%' OR
                            req.fec_preparacion LIKE '%$buscar%' OR
                            req.fec_aprobacion LIKE '%$buscar%' OR
                            req.ind_comentarios LIKE '%$buscar%' OR
                            req.ind_estado LIKE '%$buscar%' OR
                            c.ind_descripcion LIKE '%$buscar%' OR
                            dependencia.ind_dependencia LIKE '%$buscar%'
                            ) 
                            ";
        }
//        var_dump($sql);


        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_requerimiento','fec_preparacion','fec_aprobacion','ind_descripcion','ind_comentarios','estatus','ind_dependencia');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_requerimiento';
        #construyo el listado de botones
        $camposExtra = array('flagRevi');

        if (in_array('LG-01-01-01-01-02-M',$rol) AND $estado == "listado") {
            $campos['boton']['Editar'] = array("
                <button accion='modificar' title='Editar Requerimiento'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        descipcion='El Usuario ha Modificado el Requerimiento Nro. $clavePrimaria'
                        idReq='$clavePrimaria' titulo='Modificar Requerimiento'
                        estado='modificar'
                        data-toggle='modal' data-target='#formModal'
                        data-keyboard='false' data-backdrop='static' id='modificar$clavePrimaria'>
                        <i class='fa fa-edit'></i>
                </button>
                ",
                'if( $i["estatus"] =="Preparado") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-01-01-01-07-R',$rol) AND $estado == 'revisar') {
            $campos['boton']['Revisar'] = '
            <button accion="modificar" title="'.$estado.' Requerimiento"
                    class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                    descipcion="El Usuario ha Modificado el Requerimiento Nro. '.$clavePrimaria.'"
                    idReq="'.$clavePrimaria.'" titulo="'.$estado.' Requerimiento"
                    estado="'.$estado.'"
                    data-toggle="modal" data-target="#formModal"
                    data-keyboard="false" data-backdrop="static" id="estado'.$clavePrimaria.'">
                    <i class="icm icm-rating"></i>
            </button>
            ';
        } else {
            $campos['boton']['Revisar'] = false;
        }

        if (in_array('LG-01-01-01-01-08-C',$rol) AND $estado == 'conformar') {
            $campos['boton']['Conformar'] = array("
            <button accion='modificar' title='$estado Requerimiento'
                    class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                    descipcion='El Usuario ha Modificado el Requerimiento Nro. $clavePrimaria'
                    idReq='$clavePrimaria' titulo='$estado Requerimiento'
                    estado='$estado'
                    data-toggle='modal' data-target='#formModal'
                    data-keyboard='false' data-backdrop='static' id='estado$clavePrimaria'>
                    <i class='icm icm-rating2'></i>
            </button>
            ",
                'if($i["flagCaja"]==0 OR $i["flagCajaReq"]==1) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Conformar'] = false;
        }

        if (in_array('LG-01-01-01-01-09-A',$rol) AND $estado == 'aprobar') {
            $campos['boton']['Aprobar'] = '
            <button accion="modificar" title="'.$estado.' Requerimiento"
                    class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                    descipcion="El Usuario ha Modificado el Requerimiento Nro. '.$clavePrimaria.'"
                    idReq="'.$clavePrimaria.'" titulo="'.$estado.' Requerimiento"
                    estado="'.$estado.'"
                    data-toggle="modal" data-target="#formModal"
                    data-keyboard="false" data-backdrop="static" id="estado'.$clavePrimaria.'">
                    <i class="icm icm-rating3"></i>
            </button>
            ';
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('LG-01-01-01-01-06-C',$rol) AND $estado == 'conformar') {
            $campos['boton']['Verificar'] = array("
            <button accion='verificar' title='Verificar CC'
                    class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                    descipcion='El Usuario ha Modificado el Requerimiento Nro. $clavePrimaria'
                    idReq='$clavePrimaria' titulo='Verificar CC'
                    estado='verificar'
                    revisar='flagRevi'
                    data-toggle='modal' data-target='#formModal'
                    data-keyboard='false' data-backdrop='static' id='verificar$clavePrimaria'>
                <i class='fa fa-check'></i>
            </button>
            ",
                'if( $i["flagCajaReq"]==0 AND $i["flagCaja"]==1) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Verificar'] = false;
        }

        if (in_array('LG-01-01-01-01-03-V',$rol) AND $estado == "listado") {
            $campos['boton']['Ver'] = '
            <button accion="ver" title="Consultar Requerimiento"
                    class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                    descipcion="El Usuario ha Consultado el Requerimiento Nro. '.$clavePrimaria.'"
                    idReq="'.$clavePrimaria.'" titulo="Consultar Requerimiento"
                    estado="ver"
                    data-toggle="modal" data-target="#formModal"
                    data-keyboard="false" data-backdrop="static" id="ver">
                <i class="icm icm-eye2"></i>
            </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-01-01-01-04-E',$rol) AND $estado == "listado") {

            $campos['boton']['Anular'] = array("
            <button accion='anular' title='Anular Requerimiento' 
                    class='anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger'
                    descipcion='El Usuario ha Anulado el Requerimiento Nro. $clavePrimaria'
                    idReq='$clavePrimaria' titulo='Anular Requerimiento'
                    data-toggle='modal' data-target='#formModal'
                    data-keyboard='false' data-backdrop='static'
                    mensaje='Estas seguro que desea anular el Requerimiento!!' id='anular$clavePrimaria'>
                <i class='md md-not-interested'></i>
            </button>
                ",
                'if( $i["estatus"] !="Anulado" AND  $i["estatus"] !="Cerrado" AND $i["estatus"] !="Completado") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Anular'] = false;
        }

        if ($estado == "listado") {

            $campos['boton']['Imprimir'] = "
            <a href='modLG/compras/requerimientoCONTROL/imprimirMET/$clavePrimaria' target='_blank'>
                <button accion='imprimir' title='Imprimir Requerimiento' 
                        class='imprimir logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        descipcion='El Usuario ha Anulado el Requerimiento Nro. $clavePrimaria'
                        idReq='$clavePrimaria' titulo='Imprimir Requerimiento'>
                    <i class='md md-print'></i>
                </button>
            </a>";
        } else {
            $campos['boton']['Imprimir'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }

    //Método para paginar los proveedores
    public function metJsonDataTablaProveedor()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
              persona.*,
              empleado.pk_num_empleado,
              dependencia.ind_dependencia
            FROM
              a003_persona as persona 
              INNER JOIN rh_b001_empleado AS empleado ON persona.pk_num_persona = empleado.fk_a003_num_persona
              INNER JOIN rh_c076_empleado_organizacion AS organizacion ON organizacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
              INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
              and empleado.ind_estado_aprobacion = 'ap'
              and empleado.num_estatus = 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        ( 
                        empleado.pk_num_empleado LIKE '%$busqueda[value]%' OR
                        persona.pk_num_persona LIKE '%$busqueda[value]%' OR
                        persona.ind_cedula_documento LIKE '%$busqueda[value]%' OR
                        persona.ind_nombre1 LIKE '%$busqueda[value]%' OR
                        persona.ind_apellido1 LIKE '%$busqueda[value]%' OR
                        dependencia.ind_dependencia LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_persona','pk_num_empleado','ind_cedula_documento','ind_nombre1','ind_apellido1','ind_dependencia');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_empleado';
        #construyo el listado de botones
        $campos['boton']['Editar'] = false;

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }
}

