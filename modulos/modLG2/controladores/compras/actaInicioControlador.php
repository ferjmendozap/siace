<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once ROOT. 'publico/procesoCompra/docxpresso/CreateDocument.php';
class actaInicioControlador extends Controlador
{
	private $atActa;
	private $atMiscelaneoModelo;
	private $atRequerimientoModelo;
	public function __construct()
	{
		parent::__construct();
		#se carga el Modelo.
		Session::metAcceso();
		#se carga el Modelo.
		$this->atActa = $this->metCargarModelo('actaInicio', 'compras');
        $this->metObtenerLibreria('headerEvaluacion','modLG');
        $this->atFPDF = new pdf('l', 'mm', 'legal');
	}

    //Método para listar las actas de inicio
	public function metIndex()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);

		$this->atVista->metRenderizar('listado');
	}

    //Método para listar las evaluaciones cualitativas-cuantitativas
	public function metIndexEval()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);
		$this->atVista->metRenderizar('listadoEval');
	}

    //Método para listar los informes de recomendacion
	public function metIndexInforme()
	{
		$complementosCss = array(
			'DataTables/jquery.dataTables',
			'DataTables/extensions/dataTables.colVis941e',
			'DataTables/extensions/dataTables.tableTools4029',
		);
		$js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);
        $this->atVista->assign('opcion','todos');
		$this->atVista->metRenderizar('listadoInforme');
	}

    //Método para listar los informes de recomendacion para revisar
    public function metIndexEvalRevisar()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('opcion','revisar');
        /*
		$this->atVista->assign('acta',$this->atActa->atRequerimientoModelo->metListarActa());
		$this->atVista->assign('titulo','Revisar');
		$this->atVista->assign('ver',1);
		$this->atVista->assign('listado',$this->atActa->metListarInformeW('PR'));
        */
        $this->atVista->metRenderizar('listadoInforme');
    }

    //Método para listar los informes de recomendacion para aprobar
	public function metIndexEvalAprobar()
	{
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
		$this->atVista->metCargarCssComplemento($complementosCss);
		$this->atVista->metCargarJs($js);
        $this->atVista->assign('opcion','aprobar');
        /*
		$this->atVista->assign('acta',$this->atActa->atRequerimientoModelo->metListarActa());
		$this->atVista->assign('titulo','Aprobar');
		$this->atVista->assign('ver',1);
		$this->atVista->assign('listado',$this->atActa->metListarInformeW('RV'));
        */
		$this->atVista->metRenderizar('listadoInforme');
	}

    //Método para anular las actas de inicio
	public function metAnularActa()
	{
		$complementosJs = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min',
			'bootstrap-datepicker/bootstrap-datepicker',
			'select2/select2.min'
		);
		$complementoCss = array(
			'bootstrap-datepicker/datepicker',
			'select2/select201ef'
		);
		$js[] = 'Aplicacion/appFunciones';
		$this->atVista->metCargarCssComplemento($complementoCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$idActa = $this->metObtenerInt('idActa');
		$estado = $this->metObtenerTexto('estado');
		$valido = $this->metObtenerInt('valido');

		if($valido==1){

			$validacion = $this->metValidarFormArrayDatos('form','alphaNum');

			if(in_array('error',$validacion)){
				$validacion['status']='error';
				echo json_encode($validacion);
				exit;
			}

			if($idActa!=0) {
				$id=$this->atActa->metAnularActa(
					$idActa,$estado,$validacion['motivo']
				);
				$validacion['status']='modificar';
			}

			if (is_array($id)) {
				foreach ($validacion as $titulo => $valor) {
					if(!is_array($validacion[$titulo])) {
						if (strpos($id[2], $validacion[$titulo])) {
							$validacion[$titulo] = 'errorNo';
						}
					}
				}
				$validacion['status'] = 'errorSQL';
				echo json_encode($id);
				exit;
			}
			$validacion['idActa']=$id;
			echo json_encode($validacion);
			exit;

		}

		$this->atVista->assign('idActa',$idActa);
		$this->atVista->assign('estado',$estado);
		$this->atVista->metRenderizar('anularActa','modales');
	}

    //Método para calcular el presupuesto base
    public function metCalcularPresupuestoBase()
    {
        $modalidad = $this->metObtenerInt('modalidad');
        $valor = $this->metObtenerAlphaNumerico('valor');

        $cantidadNro = strlen($valor);
        $unidades = substr($valor,0,$cantidadNro-2);
        $centimos = substr($valor,-2,2);
        $valor = $unidades.','.$centimos;

        $ut = $this->atActa->metBuscarUT();
        $maestroModalidad = $this->atActa->atMiscelaneoModelo->metMostrarSelect('MOD');
        foreach ($maestroModalidad AS $key=>$value) {
            if($value['pk_num_miscelaneo_detalle']==$modalidad){
                $mod = $value['cod_detalle'];
            }
        }

        $datos = array(
            #bienes
            '1' =>
                array(
                    'desde' =>
                    #value [0] [1] [2] [3]
                        array(
//                            "20000",
                            "5000","0"
//                        ,"5000"
                        ),
                    'hasta' =>
                        array(
//                            "99999999999",
                            "20000","5000"
//                        ,"99999999999"
                        ),
                    'cual' =>
                        array(
//                            'CA',
                            'CC','CP'
//                        ,'CD'
                        )
                ),
            #servicios
            '2' =>
                array(
                    'desde' =>
                        array(
//                            "30000",
                            "10000","0"
//                        ,"10000"
                        ),
                    'hasta' =>
                        array(
//                            "99999999999",
                            "30000","10000"
//                        ,"99999999999"
                        ),
                    'cual' =>
                        array(
//                            'CA',
                            'CC','CP'
//                        ,'CD'
                        )
                ),
            #obras
            '3' =>
                array(
                    'desde' =>
                        array(
//                            "50000",
                            "20000","0"
//                        ,"20000"
                        ),
                    'hasta' =>
                        array(
//                            "99999999999",
                            "50000","20000"
//                        ,"99999999999"
                        ),
                    'cual' =>
                        array(
//                            'CA',
                            'CC','CP'
//                        ,'CD'
                        )
                )
        );
        $cual =array();
        $nombre =array();
        $nombreForm =array(
            'CA'=>'Concurso Abierto',
            'CC'=>'Concurso Cerrado',
            'CP'=>'Consulta de Precios',
            'CD'=>'Contratación Directa');
        $valor = str_replace(',','.',$valor);
        $valor = $valor/$ut['ind_valor'];
        for ($i=0;$i<2;$i++){
            $desde = $datos[$mod]['desde'][$i];
            $hasta = $datos[$mod]['hasta'][$i];
            if($desde<$valor AND $valor<=$hasta){
                $cual[$i] = $datos[$mod]['cual'][$i];
                $nombre[$i] = $nombreForm[$cual[$i]];
            }
        }

        $valores['modalidad'] = $modalidad;
        $valores['mod'] = $mod;
        $valores['valor'] = $valor;
        $valores['cual'] = $cual;
        $valores['nombre'] = $nombre;
        $valores['ut'] = $ut['ind_valor'];

        echo json_encode($valores);
        exit;

    }

    //Método para buscar los detalles de los requerimientos
    public function metBuscarDetalleReq(){
        $valor = $this->metObtenerInt('valor');
        $datos = $this->atActa->metBuscarDetalleReq($valor);
        echo json_encode($datos);
        exit;
    }

    //Método para buscar los requerimientos
    public function metBuscarReq(){
        $valor = $this->metObtenerFormulas('valor');
        $datos = $this->atActa->metBuscarReq($valor);
        $validacion['estadoActa'] = $datos['estadoActa'];
        echo json_encode($validacion);
        exit;
    }

    //Método para crear y/o modificar las actas
	public function metCrearModificarActa()
	{
        $complementoJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min',
            'inputmask/jquery.inputmask.bundle.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';

        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($js);
		$idAsisA = $this->metObtenerInt('idAsisA');
		$idAsisB = $this->metObtenerInt('idAsisB');
		$idAsisC = $this->metObtenerInt('idAsisC');
		$idAsisD = $this->metObtenerInt('idAsisD');
		$valido = $this->metObtenerInt('valido');
		$idActa = $this->metObtenerInt('idActa');
		$idReq = $this->metObtenerInt('idReq');
		$ver = $this->metObtenerInt('ver');
		$formula = $this->metValidarFormArrayDatos('form','formula');
		if($valido==1){
			$this->metValidarToken();
			$ExcceccionInt= array('asisC','contrato');
			$ind = $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
			$txt = $this->metValidarFormArrayDatos('form','txt');
			$alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');
			if ($alphaNum != null && $ind == null && $txt == null) {
				$validacion = $alphaNum;
			} elseif ($alphaNum == null && $ind != null && $txt == null) {
				$validacion = $ind;
			} elseif ($alphaNum == null && $ind == null && $txt != null) {
				$validacion = $txt;
			} elseif ($alphaNum == null && $ind != null && $txt != null) {
				$validacion = array_merge($txt, $ind);
			} elseif ($alphaNum != null && $ind == null && $txt != null) {
				$validacion = array_merge($txt, $alphaNum);
			} elseif ($alphaNum != null && $ind != null && $txt == null) {
				$validacion = array_merge($ind, $alphaNum);
			} else {
				$validacion = array_merge($alphaNum, $ind, $txt);
			}
            if ($validacion['asisC']==0) {
                $validacion['asisC'] = null;
            }if (!isset($validacion['contrato'])) {
                $validacion['contrato'] = '0';
            }

            $validacion['nombre'] = $this->metObtenerFormulas('nombre');

			#formateo el monto del presupuesto base
            $validacion['pres'] = str_replace('.','',$validacion['pres']);
            if (substr($validacion['pres'],0,1)==',') $validacion['pres'] = '0'.$validacion['pres'];
            $validacion['pres'] = str_replace(',','.',$validacion['pres']);

            #ajusto el formato de las horas
            $hora = substr($validacion['hreu'],0,2);
            $min = substr($validacion['hreu'],3,2);
            $am_pm = substr($validacion['hreu'],6,1);
            if($am_pm=='p'){
                $hora = $hora + 12;
            }
            if(!is_numeric($hora) OR !is_numeric($min)){
                $validacion['hreu'] = 'error';
            }
			$validacion['hreu'] = $hora.":".$min.":00";

            $proce=$this->atActa->atRequerimientoModelo->metContar('lg_b009_acta_inicio',date('y'),'num_anio',' AND ind_modalidad_contratacion="'.$validacion['contratacion'].'"');
            $validacion['proce'] = $proce['cantidad']+1;

			$modalidad = array(
			    'CP'=>'01',
			    'CA'=>'02',
			    'CAAI'=>'03',
			    'CC'=>'04',
                'CD'=>'05'
                );

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            #codigo del procedimiento el cual se crea mas no se actualiza
			$validacion['numActa'] = "0004-".$validacion['contratacion']."AI-".$this->metRellenarCeros($validacion['proce'],3)."-".date('Y');
            $validacion['numProce'] = Session::metObtener('SIGLASCE')."-PC-".$validacion['origen']."-".$modalidad[$validacion['contratacion']]."-".$this->metRellenarCeros($validacion['proce'],3)."-".date('Y');


            $validacion['idActa']=$idActa;
			if($idActa===0){
                if($validacion['tipo']=='i'){
                    $validacion['fk_lgb002_num_item'] = $validacion['id'];
                    $validacion['fk_lgb003_num_commodity'] = NULL;
                } elseif($validacion['tipo']=='c'){
                    $validacion['fk_lgb002_num_item'] = NULL;
                    $validacion['fk_lgb003_num_commodity'] = $validacion['id'];
                }
			   $id=$this->atActa->metCrearActa($validacion);
				$validacion['status']='nuevo';
			}else{
				$id=$this->atActa->metModificarActa($validacion);
				$validacion['status']='modificar';
			}
            $validacion['id'] = $id;
			if (is_array($id)) {
				foreach ($validacion as $titulo => $valor) {
					if(!is_array($validacion[$titulo])) {
						if (strpos($id[2], $validacion[$titulo])) {
							$validacion[$titulo] = 'error';
						}
					}
				}
				$validacion['status'] = 'errorSQL';
				$validacion['id2'] = 'error';
				echo json_encode($validacion);
				exit;
			}

			$validacion['idReq']=$idReq;
			echo json_encode($validacion);
			exit;


		}

		if($idActa!=0){

			$this->atVista->assign('AsisA',$this->atActa->metMostrarEmpleado($idAsisA));
			$this->atVista->assign('AsisB',$this->atActa->metMostrarEmpleado($idAsisB));
			$this->atVista->assign('AsisC',$this->atActa->metMostrarEmpleado($idAsisC));
			$this->atVista->assign('AsisD',$this->atActa->metMostrarEmpleado($idAsisD));
			$form = $this->atActa->metMostrarActa($idActa);
			$this->atVista->assign('formDB',$form);
            $len = strlen(Session::metObtener('SIGLASCE')."-PC-");
            $this->atVista->assign('origen2', substr($form['ind_codigo_procedimiento'], $len,2));
            $conglomerados = $this->atActa->metMostrarActaDetalle($idActa);
			$this->atVista->assign('idActa',$idActa);
            $requerimientos = $this->atActa->metBuscarReq($idActa,1);
			$this->atVista->assign('ver',$ver);
		} else {
		    foreach ($formula['req_acta'] AS $key=>$value){
                $req = $this->atActa->metBuscarReq($value);
                $requerimientos[$value] = $req;
            }
        }

        $detalles = array();
        foreach ($requerimientos AS $key=>$value){
            $det = $this->atActa->metBuscarDetalleReq($value['pk_num_requerimiento']);
            $detalles = array_merge($detalles,$det);
        }

        $conglomeradosReq = array();
        $c = 0;
        foreach ($detalles AS $key=>$value){
            $codigo = $value['fk_lgb002_num_item'];
            if ($value['fk_lgb003_num_commodity']!=null) {
                $codigo = $value['fk_lgb003_num_commodity'];
            }

            if(isset($conglomeradosReq[$codigo])) {
                $conglomeradosReq[$codigo]['num_cantidad_pedida'] = $conglomeradosReq[$codigo]['num_cantidad_pedida'] + $value['num_cantidad_pedida'];
            } else {
                $conglomeradosReq[$codigo]['num_cantidad_pedida'] = $value['num_cantidad_pedida'];
            }
            $c = $c+1;
            $conglomeradosReq[$codigo]['idDet'] = $value['pk_num_requerimiento_detalle'];
            $conglomeradosReq[$codigo]['num_secuencia'] = $c;
            $conglomeradosReq[$codigo]['fk_lgb002_num_item'] = $value['fk_lgb002_num_item'];
            $conglomeradosReq[$codigo]['fk_lgb003_num_commodity'] = $value['fk_lgb003_num_commodity'];
            $conglomeradosReq[$codigo]['ind_descripcion'] = $value['descItem'].$value['descComm'];
            $conglomeradosReq[$codigo]['unidad'] = $value['fk_lgb004_num_unidad'];
            $conglomeradosReq[$codigo]['cuenta'] = $value['cuentaItem'].$value['cuentaComm'];
            $conglomeradosReq[$codigo]['partida'] = $value['partidaItem'].$value['partidaComm'];
        }


        if($idActa==0){
            $conglomerados = $conglomeradosReq;
        }

        $this->atVista->assign('conglomerados',$conglomerados);
        $this->atVista->assign('requerimientos',$requerimientos);
        $this->atVista->assign('detalles',$detalles);
		$this->atVista->assign('idReq',$idReq);
        $this->atVista->assign('origen',$this->atActa->atMiscelaneoModelo->metMostrarSelect('ORG'));
        $this->atVista->assign('modalidad',$this->atActa->atMiscelaneoModelo->metMostrarSelect('MOD'));

		$this->atVista->assign('requerimiento',$this->atActa->atRequerimientoModelo->metListarRequerimiento());
		$this->atVista->metRenderizar('CrearModificar','modales');
	}

    //Método para modificar el formato de las fechas
	public function metFormatoFecha($fecha,$formato,$separador){
		#1  2016-01-01 a 01-01-2016
		#0  01-01-2016 a 2016-01-01
		if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
			$dia=substr($fecha,8,2);
			$mes=substr($fecha,5,2);
			$anio=substr($fecha,0,4);
		} elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
			$dia=substr($fecha,0,2);
			$mes=substr($fecha,3,2);
			$anio=substr($fecha,6,4);
		} else {
			$dia=0;
			$mes=0;
			$anio=0;
		}
		if (checkdate($mes, $dia, $anio)==false){
			return "la fecha es incorrecta";
			exit;
		}
		if($formato==1) {
			$fecha =$dia.$separador.$mes.$separador.$anio;
		} else {
			$fecha =$anio.$separador.$mes.$separador.$dia;
		}
		return $fecha;
	} // END FUNCTION

    //Método para buscar el subfijo de los numeros
	public function metSubfijo($xx)
	{ // esta función regresa un metSubfijo para la cifra
		$xx = trim($xx);
		$xstrlen = strlen($xx);
		if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
			$xsub = "";
		//
		if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
			$xsub = "MIL";
		//
		return $xsub;
	} // END FUNCTION

    //Método para modificar un monto a letras
	public function metNumtoletras($xcifra,$precio=false)
	{
		$xarray = array(0 => "Cero",
			1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
			"DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
			"VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
			100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
		);
//
		$xcifra = trim($xcifra);
		$xlength = strlen($xcifra);
		$xpos_punto = strpos($xcifra, ".");
		$xaux_int = $xcifra;
		$xdecimales = "00";
		if (!($xpos_punto === false)) {
			if ($xpos_punto == 0) {
				$xcifra = "0" . $xcifra;
				$xpos_punto = strpos($xcifra, ".");
			}
			$xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
			$xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
		}

		$XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
		$xcadena = "";
		for ($xz = 0; $xz < 3; $xz++) {
			$xaux = substr($XAUX, $xz * 6, 6);
			$xi = 0;
			$xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
			$xexit = true; // bandera para controlar el ciclo del While
			while ($xexit) {
				if ($xi == $xlimite) // si ya llegó al límite máximo de enteros
				{
					break; // termina el ciclo
				}

				$x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
				$xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
				for ($xy = 1; $xy < 4; $xy++) // ciclo para revisar centenas, decenas y unidades, en ese orden
				{
					switch ($xy) {
						case 1: // checa las centenas
							if (substr($xaux, 0, 3) < 100) // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
							{
							} else {
								if(substr($xaux, 1, 2)=='00'){
									$xseek = $xarray[substr($xaux, 0, 3)]; // busco si la centena es número redondo (100, 200, 300, 400, etc..)
								}else{
									$xseek=false;
								}

								if ($xseek) {
									$xsub = $this->metSubfijo($xaux); // devuelve el $this->metSubfijo correspondiente (Millón, Millones, Mil o nada)
									if (substr($xaux, 0, 3) == 100)
										$xcadena = " " . $xcadena . " CIEN " . $xsub;
									else
										$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
									$xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
								} else // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
								{
									$xseek = $xarray[substr($xaux, 0, 1) * 100]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
									$xcadena = " " . $xcadena . " " . $xseek;
								} // ENDIF ($xseek)
							} // ENDIF (substr($xaux, 0, 3) < 100)
							break;
						case 2: // checa las decenas (con la misma lógica que las centenas)
							if (substr($xaux, 1, 2) < 10) {
							} else {
								if(substr($xaux, 2, 1)=='0'){
									$xseek = $xarray[substr($xaux, 1, 2)]; // busco si la centena es número redondo (100, 200, 300, 400, etc..)
								}else{
									$xseek=false;
								}
								if ($xseek) {
									$xsub = $this->metSubfijo($xaux);
									if (substr($xaux, 1, 2) == 20)
										$xcadena = " " . $xcadena . " VEINTE " . $xsub;
									else
										$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
									$xy = 3;
								} else {
									$xseek = $xarray[substr($xaux, 1, 1) * 10];
									if (substr($xaux, 1, 1) * 10 == 20)
										$xcadena = " " . $xcadena . " " . $xseek;
									else
										$xcadena = " " . $xcadena . " " . $xseek . " Y ";
								} // ENDIF ($xseek)
							} // ENDIF (substr($xaux, 1, 2) < 10)
							break;
						case 3: // checa las unidades
							if (substr($xaux, 2, 1) < 1) // si la unidad es cero, ya no hace nada
							{
							} else {
								$xseek = $xarray[substr($xaux, 2, 1)]; // obtengo directamente el valor de la unidad (del uno al nueve)
								$xsub = $this->metSubfijo($xaux);
								$xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
							} // ENDIF (substr($xaux, 2, 1) < 1)
							break;
					} // END SWITCH
				} // END FOR
				$xi = $xi + 3;
			} // ENDDO

			if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
				$xcadena .= " DE";

			if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
				$xcadena .= " DE";

			// ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
			if (trim($xaux) != "" AND $precio==false) {
				switch ($xz) {
					case 0:
						if (trim(substr($XAUX, $xz * 6, 6)) == "1")
							$xcadena .= "UN BILLON ";
						else
							$xcadena .= " BILLONES ";
						break;
					case 1:
						if (trim(substr($XAUX, $xz * 6, 6)) == "1")
							$xcadena .= "UN MILLON ";
						else
							$xcadena .= " MILLONES ";
						break;
					case 2:
						if ($xcifra < 1) {
							$xcadena = "CERO BOLIVARES CON $xdecimales/100";
						}
						if ($xcifra >= 1 && $xcifra < 2) {
							$xcadena = "UN BOLIVAR CON $xdecimales/100";
						}
						if ($xcifra >= 2) {
							$xcadena .= " BOLIVARES CON $xdecimales/100"; //
						}
						break;
				} // endswitch ($xz)
			} // ENDIF (trim($xaux) != "")
			// ------------------      en este caso, para México se usa esta leyenda     ----------------
			$xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
			$xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
			$xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
			$xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
			$xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
			$xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
			$xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
		} // ENDFOR	($xz)
		return trim($xcadena);
	} // END FUNCTION

    //Método para modificar un mes a letras
	public function metMesLetras($mes){

		if(strlen($mes)==1){
			$mes="0".$mes;
		}
		$metMesLetras=array(
			'01'=>'Enero',
			'02'=>'Febrero',
			'03'=>'Marzo',
			'04'=>'Abril',
			'05'=>'Mayo',
			'06'=>'Junio',
			'07'=>'Julio',
			'08'=>'Agosto',
			'09'=>'Septiembre',
			'10'=>'Octubre',
			'11'=>'Noviembre',
			'12'=>'Diciembre'
		);

		return $metMesLetras[$mes];
	} // END FUNCTION

    //Método para modificar un dia a letras
	public function metDiasLetras($dia){

		if(strlen($dia)==1){
			$dia="0".$dia;
		}
		$diaLetras=array(
			'01'=>'uno',
			'02'=>'dos',
			'03'=>'tres',
			'04'=>'cuatro',
			'05'=>'cinco',
			'06'=>'seis',
			'07'=>'siete',
			'08'=>'ocho',
			'09'=>'nueve',
			'10'=>'diez',
			'11'=>'once',
			'12'=>'doce',
			'13'=>'trece',
			'14'=>'catorce',
			'15'=>'quince',
			'16'=>'dieciseis',
			'17'=>'diecisiete',
			'18'=>'dieciocho',
			'19'=>'diecinueve',
			'20'=>'veinte',
			'21'=>'veintiuno',
			'22'=>'veintidos',
			'23'=>'veintitres',
			'24'=>'veinticuatro',
			'25'=>'veinticinco',
			'26'=>'veintiseis',
			'27'=>'veintisiete',
			'28'=>'veintiocho',
			'29'=>'veintinueve',
			'30'=>'treinta',
			'31'=>'treinta y uno'
		);

		return $diaLetras[$dia];
	} // END FUNCTION

    //Método para rellenar con ceros a la izquierda
	public function metRellenarCeros($nro,$cantidad){
		$cont=strlen($nro);
		if($cont<$cantidad){
			while($cont<$cantidad){
				$nro="0".$nro;
				$cont=$cont+1;
			}
		}
		return $nro;
	} // END FUNCTION

    //Método para generar el documento del acta de inicio
    public function metGenerarActa()
    {
        $idActa = $this->metObtenerInt('idActa');
        #busco los datos
        $busquedaActa = $this->atActa->metMostrarActa($idActa);
        $busquedaProveedores = $this->atActa->metListarProveedores($idActa,1);
        //   $detalleRequerimiento= $this->atActa->metBuscarReq($idActa,1);
        #busco en la ruta el odt que me sirve de formato
        $this->atDocx = new Docxpresso\CreateDocument(array('template' => ROOT . 'publico' . DS . 'procesoCompra' . DS . 'actainicio-plantilla.odt'));

        $nroActa=$busquedaActa['ind_codigo_acta'];
        $tip_act=$busquedaActa['ind_nombre_detalle'];
        if ($tip_act='Adquisición de Bienes')
        {
            $actaTipo='Compra';
        }elseif ($tip_act='Prestación de Servicio')
        {
            $actaTipo='Servicio';
        }

        $diaNro = substr($busquedaActa['fec_reunion'],-2);
        $mesNro = substr($busquedaActa['fec_reunion'],5,2);
        $anio = substr($busquedaActa['fec_reunion'],0,4);

        $AsisA=$this->atActa->metMostrarEmpleado($busquedaActa['fk_rhb001_empleado_asistente_a']);
        $AsisB=$this->atActa->metMostrarEmpleado($busquedaActa['fk_rhb001_empleado_asistente_b']);
        $AsisC=$this->atActa->metMostrarEmpleado($busquedaActa['fk_rhb001_empleado_asistente_c']);

        $analistaA=$AsisA['nombre'];
        $cedulaAnalista_A='V-'.number_format($AsisA['ind_cedula_documento'],0,',','.');
        $fec_ingresoA=$AsisA['fec_ingreso'];
        $resolucion_ingresoA=$AsisA['ind_resolucion_ingreso'];


        $analistaB=$AsisB['nombre'];
        $cedulaAnalista_B='V-'.number_format($AsisB['ind_cedula_documento'],0,',','.');
        $fec_ingresoB=$AsisB['fec_ingreso'];
        $resolucion_ingresoB=$AsisB['ind_resolucion_ingreso'];


        $analistaC=$AsisC['nombre'];
        $cedulaAnalista_C='V-'.number_format($AsisC['ind_cedula_documento'],0,',','.');
        $fec_ingresoC=$AsisC['fec_ingreso'];
        $resolucion_ingresoC=$AsisC['ind_resolucion_ingreso'];

        $cargo='';
        $cargoAnalista1='';
        $cargoAnalista2='';
        if($AsisA['ind_descripcion_cargo']){
            $cargo=$AsisA['ind_descripcion_cargo'];
        }
        if($AsisB['ind_descripcion_cargo']){
            $cargoAnalista1=$AsisB['ind_descripcion_cargo'];
        }
        if($AsisC['ind_descripcion_cargo']){
            $cargoAnalista2=$AsisC['ind_descripcion_cargo'];
        }

        if($analistaC==null){
            //$analistas=$analistaA." y ".$analistaB;
            $analistas="y <b>".$analistaB."</b> titular  de  la cédula de identidad Nº <b>".$cedulaAnalista_B."</b> en su condición de ".$cargoAnalista1." según consta en Resolución Nº ".$resolucion_ingresoB." de fecha ".$fec_ingresoB;
        } else {
            $analistas="<b>".$analistaB."</b> titular  de  la cédula de identidad Nº <b>".$cedulaAnalista_B."</b> en su condición de ".$cargoAnalista1." según consta en Resolución Nº ".$resolucion_ingresoB." de fecha ".$fec_ingresoB."y <b>".$analistaC."</b> titular  de  la cédula de identidad Nº <b>".$cedulaAnalista_C."</b> en su condición de ".$cargoAnalista2." según consta en Resolución Nº ".$resolucion_ingresoC." de fecha ".$fec_ingresoC;
        }

        $nroProc=$busquedaActa['ind_codigo_procedimiento'];

        $requerimiento= $busquedaActa['ind_nombre_procedimiento'];



        $montoN=number_format($busquedaActa['num_presupuesto_base'],2,',','.');

        $montoL=$this->metNumtoletras($busquedaActa['num_presupuesto_base']);

        $proveedores = "";

        $cant=count($busquedaProveedores);
        $cont=0;
        while($cont<$cant){
            if($cont==0){
                $proveedores = "- ".$busquedaProveedores[$cont]['ind_nombre1'];
            } else {
                $proveedores = $proveedores."\t- ".$busquedaProveedores[$cont]['ind_nombre1'];
            }
            if($busquedaProveedores[$cont]['ind_apellido1']!=""){
                $proveedores = $proveedores." ".$busquedaProveedores[$cont]['ind_apellido1'];
            }
            if($busquedaProveedores[$cont]['ind_nombre_detalle']==null){
                $proveedores = $proveedores." No inscrito en el SNC \n";

            } else {
                $proveedores = $proveedores." Inscrito en el SNC , Empresa registrada en el R.N.C. \n";

            }
            $cont=$cont+1;
        }

        if($busquedaActa['cod_detalle']==1) {
            $tipoServicio = 'quince (15) días hábiles para adquisición de bienes';
            $tipoServicioDetallado = 'cuatro días (4)';
            $tipoServicioDetalladoFin = 'ocho días (8)';
            $DiaE = '2';
            $DiaRc = '2';
            $DiaAD = '2';
            $DiaNA = '2';
        } else if($busquedaActa['cod_detalle']==2) {
            $tipoServicio = 'diecisiete (17) días hábiles para la prestación de servicio';
            $tipoServicioDetallado = 'cinco días (5)';
            $tipoServicioDetalladoFin = 'nueve dias (9)';
            $DiaE = '2';
            $DiaRc = '3';
            $DiaAD = '2';
            $DiaNA = '2';
        } else if($busquedaActa['cod_detalle']==3) {
            $tipoServicio = 'diecinueve (19) días hábiles para ejecución de obras';
            $tipoServicioDetallado='06 días hábiles para ejecucion de obras';
            $tipoServicioDetalladoFin='10 dias para ejecusion de obras';
            $DiaE='3';
            $DiaRc='3';
            $DiaAD='2';
            $DiaNA='2';
        } else {
            $tipoServicio = '0';
            $tipoServicioDetallado='0';
            $tipoServicioDetalladoFin='0';
            $DiaE='0';
            $DiaRc='0';
            $DiaAD='0';
            $DiaNA='0';
        }

        #asignar a las variables los valores buscados
        $horaReunion = substr($busquedaActa['fec_hora_reunion'],0,2);
        $min = substr($busquedaActa['fec_hora_reunion'],3,2);
        if($horaReunion>12){
            $hora = ($horaReunion-12).":".$min." PM";
        } else {
            $hora = $horaReunion.":".$min." AM";
        }

        /*Colocar logo en los odt
                $headerStyle = array('style' => 'min-height: 3cm');
                $urlImg = ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . 'CES.jpg';
                $rowStyle = array('style' => 'min-height: 25px');
                $cellStyle = array('style' => 'margin-left: 200px; vertical-align: middle;');
                $pStyle = array('style' => 'text-align: center; font-weight: bold; font-family: bookma-old-style; font-size: 6pt; margin-right: 10pt;');
                $this->atDocx->header($headerStyle)
                    ->table(array('grid' => array('550px')))
                    ->row($rowStyle)
                    ->cell($cellStyle)->paragraph()->image(array('src' => $urlImg, 'style' => 'width:100px; height:80px;'))
                                      ->paragraph($pStyle)->text(array('text' => "DIRECCIÓN DE ADMINISTRACIÓN Y PRESUPUESTO"))
                                      ->paragraph($pStyle)->text(array('text' => "(UNIDAD CONTRATANTE)"))
                ;
        Colocar logo en los odt */
        $this->metReemplazar('nroActa',$nroActa);
        $this->metReemplazar('hora',$hora);
        $this->metReemplazar('diaR',$this->metDiasLetras($diaNro));
        $this->metReemplazar('diaNumeroR',$diaNro);
        $this->metReemplazar('mesR',$this->metMesLetras($mesNro));
        $this->metReemplazar('anioR',$anio);
        $this->metReemplazar('analistas',$analistas);
        $this->metReemplazar('nroProc',$nroProc);
        $this->metReemplazar('requerimiento',$requerimiento);
        $this->metReemplazar('montoL',$montoL);
        $this->metReemplazar('montoN',$montoN);
        $this->metReemplazar('proveedores',$proveedores);
        $this->metReemplazar('nombreDirector',$analistaA);
        $this->metReemplazar('cedula_Director',$cedulaAnalista_A);
        $this->metReemplazar('tipo',$actaTipo);
        $this->metReemplazar('tipoServicioDetallado',$tipoServicioDetallado);
        $this->metReemplazar('tipoServicioDetalladoFin',$tipoServicioDetalladoFin);

        $this->metReemplazar('nombreAnalista1',$analistaB);
        $this->metReemplazar('cedulaAnalista1',$cedulaAnalista_B);
        $this->metReemplazar('nombreAnalista2',$analistaC);
        $this->metReemplazar('cedulaAnalista2',$cedulaAnalista_C);
        $this->metReemplazar('cargo',$cargo);
        $this->metReemplazar('cargoAnalista1',$cargoAnalista1);
        $this->metReemplazar('cargoAnalista2',$cargoAnalista2);

        $tabla = '<table border="1" style="font-family: Arial; font-size: 9pt;">
                    <tr style="background-color: #a0a0a0; text-align: center;">
                    <td colspan="2"><b>CRONOGRAMA DE EJECUCIÓN</b></td>
                    </tr>
                    <tr style="text-align: center; tex">
                    <td><b>FASES</b></td>
                    <td><b>LAPSO</b></td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Recepción de Ofertas.</td>
                    <td>'.$tipoServicioDetallado.' contados a partir del dia hábil siguiente a la recepción de la oferta. Art. 67 del Decreto Nº 1.399 de la Ley de Contrataciones Públicas.</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Aclaratoria.</td>
                    <td>01 día hábil. Art. 70 del Decreto Nº 1.399 de la Ley de Contrataciones Públicas. Aclaratoria el día siguiente a la recepción de la invitación.</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Modificación por parte del Órgano Contratante.</td>
                    <td>02 días hábiles. Art. 68.del Decreto Nº 1.399 de la Ley de Contrataciones Públicas. Antes de la fecha límite de la recepción de la Oferta.</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Examen de las Ofertas. '.$DiaE.' días</td>
                    <td rowspan="4" style="vertical-align:middle;">'.$tipoServicioDetalladoFin.' Art. 98 del Decreto Nº 1.399  de la Ley de Contrataciones Públicas.</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Informe de Recomendación  '.$DiaRc.' días</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Adjudicación o Declaratoria Desierta. '.$DiaAD.' días</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Notificación de la Adjudicación. '.$DiaNA.' días</td>
                    </tr>
                    </table>';

        $this->metReemplazar('tabla', $tabla,1);

        $this->atDocx->render('.'.DS.'publico' .DS.'procesoCompra' .DS.'inicioCompra'.$idActa.'.odt');
        $validacion['idActa']=$idActa;
        $validacion['ruta']=BASE_URL.''.DS.'publico' .DS.'procesoCompra' .DS.'inicioCompra'.$idActa.'.odt';
        echo json_encode($validacion);
        exit;
    }

    //Método para crear y/o modificar las evaluaciones cualitativas-cuantitativas
	public function metCrearModificarEvaluacion()
	{
		$complementosJs = array(
			'jquery-validation/dist/jquery.validate.min',
			'jquery-validation/dist/additional-methods.min'
		);
		$js[] = 'Aplicacion/appFunciones';
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);
		$valido = $this->metObtenerInt('valido');
		$idEval = $this->metObtenerInt('idEval');
		$ver = $this->metObtenerInt('ver');
		$idActa = $this->metObtenerInt('idActa');
		$idPrimero = $this->atActa->atIdEmpleado;

		if($valido==1){
			$this->metValidarToken();
			$exepcion = array('tercero');
			$ind = $this->metValidarFormArrayDatos('form','int',$exepcion);
			$alphaNum = $this->metValidarFormArrayDatos('form','formula');
			if ($ind != null && $alphaNum == null) {
				$validacion = $ind;
			} elseif ($ind == null && $alphaNum != null) {
				$validacion = $alphaNum;
			} else {
				$validacion = array_merge($ind, $alphaNum);
			}

			if ($validacion['tercero'] == "0") {
				$validacion['tercero'] = NULL;
			}
			if(in_array('error',$validacion)){
				$validacion['status']='error';
				echo json_encode($validacion);
				exit;
			}

			$num=$this->atActa->atRequerimientoModelo->metContar('lg_b011_evaluacion',date('y'),'fec_anio');
            $validacion['cantidad'] = $num['cantidad'] +1;

            $validacion['idActa']=$idActa;
            $validacion['idEval']=$idEval;
            $validacion['ind_cod_evaluacion']='0004-CPECC-'.$this->metRellenarCeros($validacion['cantidad'],3).'-'.date('Y');

			if($idEval===0){
				$id=$this->atActa->metCrearEvaluacion($validacion);
				$validacion['status']='nuevo';
			} else {
				$id=$this->atActa->metModificarEvaluacion($validacion);
				$validacion['status']='modificar';
			}
			if (is_array($id)) {
				foreach ($validacion as $titulo => $valor) {
					if (strpos($id[2], $validacion[$titulo])) {
						$validacion[$titulo] = 'error';
					}
				}
				$validacion['status'] = 'errorSQL';
				echo json_encode($id);
				exit;
			}
			$validacion['idEvaluacion']=$id;
			echo json_encode($validacion);
			exit;
		}
		if($idEval!=0){
		    $eval = $this->atActa->metMostrarEvaluacion($idEval);
			$this->atVista->assign('eval',$eval);
            $this->atVista->assign('datosEvalCuali', $this->atActa->metBuscarDatosEvalCualitativa($idEval));
            $this->atVista->assign('idEval',$idEval);
            $this->atVista->assign('AsisB',$this->atActa->metMostrarEmpleado($eval['fk_rhb001_num_empleado_asistente_b']));
            $this->atVista->assign('AsisC',$this->atActa->metMostrarEmpleado($eval['fk_rhb001_num_empleado_asistente_c']));
		}

        $this->atVista->assign('ver',$ver);

        $requerimientos = $this->atActa->metListarActaDetalle($idActa);
        $menor2=array();
        foreach ($requerimientos AS $key => $value){
            $menor = $this->atActa->metBuscarCotizacion($value['pk_num_acta_detalle']);
            $menor2[$value['pk_num_acta_detalle']] = $menor['menor'];
        }

        $this->atVista->assign('evaluacionCC', $this->atActa->metBuscarDatosEvalCuantitativa());
        $this->atVista->assign('cotizaciones',$this->atActa->metListarCotizacion($idActa));
        $this->atVista->assign('cotiActa',$this->atActa->metCotizacionesActa($idActa));
        $this->atVista->assign('requerimientos',$requerimientos);
        $this->atVista->assign('detalles',$requerimientos);
        $this->atVista->assign('menor',$menor2);
        $this->atVista->assign('primero',$this->atActa->metMostrarEmpleado($idPrimero));
        $this->atVista->assign('idPrimero',$idPrimero);
        $this->atVista->assign('inviActa',$this->atActa->metInvitacionesActa($idActa));
        $this->atVista->assign('puntaje',$this->atActa->atAspectosCualitativosModelo->metMostrarParametro('PMEC'));
        $this->atVista->assign('aspectos',$this->atActa->atAspectosCualitativosModelo->metListarAspecto());
        $this->atVista->assign('idActa',$idActa);
		$this->atVista->metRenderizar('evaluar','modales');
	}

    //Método para crear y/o modificar los informes de recomendacion
	public function metCrearModificarInformeRec()
	{
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'select2/select2.min'
        );
		$complementoCss = array(
			'select2/select201ef'
		);
		$js[] = 'Aplicacion/appFunciones';
		$this->atVista->metCargarCssComplemento($complementoCss);
		$this->atVista->metCargarJsComplemento($complementosJs);
		$this->atVista->metCargarJs($js);

		$idInfor = $this->metObtenerInt('idInfor');
		$opcion = $this->metObtenerAlphaNumerico('opcion');
		$t = $this->metObtenerAlphaNumerico('t');

		$idActa = $this->metObtenerInt('idActa');
		$fecanio = $this->metObtenerInt('fecanio');

		$valido = $this->metObtenerInt('valido');
		$idEval = $this->metObtenerInt('idEval');
		$objeto = $this->metObtenerAlphaNumerico('objeto');
		$conclusion = $this->metObtenerAlphaNumerico('conclusion');
		$recomendacion = $this->metObtenerAlphaNumerico('recomendacion');
		$ver = $this->metObtenerInt('ver');
		if($valido==1){
			$this->metValidarToken();
			$ind = $this->metValidarFormArrayDatos('form','int');
			$formula = $this->metValidarFormArrayDatos('form','formula');
			if ($ind != null && $formula == null) {
				$validacion = $ind;
			} elseif ($ind == null && $formula != null) {
				$validacion = $formula;
			} else {
				$validacion = array_merge($ind, $formula);
			}
			if ($validacion['asisC'] == "error") {
				$validacion['asisC'] = NULL;
			}

			for ($i=1;$i<=$validacion['cantidadProvRec'];$i++){
                if ($validacion['proveedor1']!='error' AND $validacion['proveedor'.$i]=='error' AND $i>1){
                    $validacion['proveedor'.$i] = NULL;
                }
                $validacion['idProveedor'][] = $validacion['proveedor'.$i];
            }


			if(in_array('error',$validacion)){
				$validacion['status']='error';
				echo json_encode($validacion);
				exit;
			}

			$num=$this->atActa->atRequerimientoModelo->metContar('lg_b012_informe_recomendacion',date('y'),'fec_anio');
            $validacion['cantidad'] = $num['cantidad'] +1;

#            echo json_encode($validacion);
#            exit;

			if($idInfor==0){
				$id=$this->atActa->metCrearInforme(
					$validacion['conc'],$validacion['rec'],$validacion['asisA'],
					$validacion['asisB'],$validacion['asisC'],$validacion['objeto'],
					$validacion['asunto'],$validacion['artNum'],$validacion['adjudicacion'],
                    $validacion['idProveedor'],$idEval,
                    $validacion['cantidad']
				);
				$validacion['status']='nuevo';
			} else {
				$id=$this->atActa->metModificarInforme(
					$validacion['conc'],$validacion['rec'],$validacion['asisA'],
					$validacion['asisB'],$validacion['asisC'],$validacion['objeto'],
					$validacion['asunto'],$validacion['adjudicacion'],
					$validacion['artNum'],$validacion['idProveedor'],$idInfor
				);
				$validacion['status']='modificar';
			}
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['id'] = $id;
                echo json_encode($validacion);
                exit;
            }
			$validacion['idInfo']=$id;
			$validacion['idEval']=$idEval;
			echo json_encode($validacion);
			exit;
		}
        if($idEval!=0){
            $datosEval = $this->atActa->metMostrarEvaluacion($idEval);
            $idAsisA = $datosEval['fk_rhb001_num_empleado_asistente_a'];
            $idAsisB = $datosEval['fk_rhb001_num_empleado_asistente_b'];
            $idAsisC = $datosEval['fk_rhb001_num_empleado_asistente_c'];
            $this->atVista->assign('eval',$datosEval);
            $this->atVista->assign('idEval',$idEval);
        }
		if ($idInfor!=0) {
			$this->atVista->assign('idInfor',$idInfor);
            $informe = $this->atActa->metMostrarInforme($idInfor);
            $idAsisA = $informe['fk_rhb001_num_empleado_asistente_a'];
            $idAsisB = $informe['fk_rhb001_num_empleado_asistente_b'];
            $idAsisC = $informe['fk_rhb001_num_empleado_asistente_c'];
			$this->atVista->assign('informe',$informe);
            $this->atVista->assign('proveedores1',$this->atActa->metListarProveedores($informe['pk_num_acta_inicio'],1));
            $this->atVista->assign('proveedores2',$this->atActa->metListarProveedores($idInfor,2));
            $this->atVista->assign('cual',2);
        } else {
            $this->atVista->assign('proveedores1',$this->atActa->metListarProveedores($idActa,1));
            $this->atVista->assign('cual',1);
		}

		$this->atVista->assign('ver',$ver);
		$this->atVista->assign('AsisA',$this->atActa->metMostrarEmpleado($idAsisA));
		$this->atVista->assign('AsisB',$this->atActa->metMostrarEmpleado($idAsisB));
		$this->atVista->assign('AsisC',$this->atActa->metMostrarEmpleado($idAsisC));
		$this->atVista->assign('idActa',$idActa);
		$this->atVista->assign('fecanio',$fecanio);
		$this->atVista->assign('idAsisA',$idAsisA);
		$this->atVista->assign('idAsisB',$idAsisB);
		$this->atVista->assign('idAsisC',$idAsisC);
		$this->atVista->assign('opcion',$opcion);
		$this->atVista->assign('t',$t);
		$this->atVista->assign('objeto',$objeto);
		$this->atVista->assign('conclusion',$conclusion);
		$this->atVista->assign('recomendacion',$recomendacion);
		$this->atVista->assign('adjudicacion',$this->atActa->atMiscelaneoModelo->metMostrarSelect('TIPOADJ'));
		$this->atVista->metRenderizar('informe','modales');
	}

    //Método para generar el documento del informes de recomendacion
    public function metGenerarInformeRecOdt() {
        $idRec = $this->metObtenerInt('idRec');
        $this->atDocx = new Docxpresso\CreateDocument(array('template' => ROOT . 'publico' . DS . 'procesoCompra' . DS . 'informe-recomendacion-plantilla.odt'));

        $informe=$this->atActa->metMostrarInforme($idRec);
        $evaluacion=$this->atActa->metListarEvaluacionCC($informe['fk_lgb011_num_evaluacion']);
        $evaluacionCualitativa=$this->atActa->metBuscarDatosEvalCualitativa($informe['fk_lgb011_num_evaluacion']);
        $itemsComm=$this->atActa->metListarActaDetalle($informe['pk_num_acta_inicio']);

        $numRecomendacion=$this->metRellenarCeros($informe['num_recomendacion'],3);
        $diaNro = substr($informe['fec_creacion'],8,2);
        $mesNro = substr($informe['fec_creacion'],5,2);
        $numCotizacion=$this->metRellenarCeros($informe['ind_num_cotizacion'],4);
        $codigo = $informe['ind_codigo_procedimiento'];
        $nroRecomendacion = '0004-CPIR-'.$this->metRellenarCeros($numRecomendacion,3).'-'.$informe['fec_anio'];
        $diaInviNro = substr($informe['fec_invitacion'],-2);
        $mesInviNro = substr($informe['fec_invitacion'],5,2);
        $anioInviNro = substr($informe['fec_invitacion'],0,4);

        $AsisA=$this->atActa->metMostrarEmpleado($informe['fk_rhb001_num_empleado_asistente_a']);
        $AsisB=$this->atActa->metMostrarEmpleado($informe['fk_rhb001_num_empleado_asistente_b']);
        $AsisC=$this->atActa->metMostrarEmpleado($informe['fk_rhb001_num_empleado_asistente_c']);

        $analistaA=mb_strtoupper($AsisA['nombre']);
        $cargoA=mb_strtoupper($AsisA['ind_descripcion_cargo']);
        $analistaB=mb_strtoupper($AsisB['nombre']);
        $cargoB=mb_strtoupper($AsisA['ind_descripcion_cargo']);
        $analistaC=mb_strtoupper($AsisC['nombre']);
        $cargoC=mb_strtoupper($AsisC['ind_descripcion_cargo']);

        if(isset($AsisC['nombre'])){
            $analistas =
                $analistaA." titular de la cédula de identidad N° ".
                $AsisA['ind_documento_fiscal']." en su condición de ".$cargoA.", ".

                $analistaB." titular de la cédula de identidad N° ".
                $AsisB['ind_documento_fiscal']." en su condición de ".$cargoB." y ".

                $analistaC." titular de la cédula de identidad N° ".
                $AsisC['ind_documento_fiscal']." en su condición de ".$cargoC;
        } else {
            $analistas =
                $analistaA." titular de la cédula de identidad N° ".
                $AsisA['ind_documento_fiscal']." en su condición de ".$cargoA." y ".

                $analistaB." titular de la cédula de identidad N° ".
                $AsisB['ind_documento_fiscal']." en su condición de ".$cargoB;
        }
        $nroVisualEvaluacion = "0004-CPECC-".$this->metRellenarCeros($informe['num_recomendacion'],3)."-".$informe['fec_anio'];

//Colocar logo en los odt
        $headerStyle = array('style' => 'min-height: 3cm');
        $urlImg = ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . 'CES.jpg';
        $rowStyle = array('style' => 'min-height: 25px');
        $cellStyle = array('style' => 'margin-left: 230px; vertical-align: middle;');
        $this->atDocx->header($headerStyle)
            ->table(array('grid' => array('660px')))
            ->row($rowStyle)
            ->cell($cellStyle)->paragraph()->image(array('src' => $urlImg, 'style' => 'width:100px; height:80px;'))
        ;
//Colocar logo en los odt

        $this->metReemplazar('nroRecomendacion',$nroRecomendacion);
        $this->metReemplazar('diaNumero',$diaNro);
        $this->metReemplazar('mesLetras',$this->metMesLetras($mesNro));
        $this->metReemplazar('anio',$informe['fec_anio']);
        $this->metReemplazar('asunto',$informe['ind_asunto']);
        $this->metReemplazar('objeto',$informe['ind_objeto_consulta']);
        $this->metReemplazar('dia',$this->metDiasLetras($diaNro));
        $this->metReemplazar('nroProc',$codigo);
        $this->metReemplazar('numeral',$informe['ind_articulo_numeral']);
        $this->metReemplazar('diaInv',$this->metDiasLetras($diaInviNro));
        $this->metReemplazar('diaNumeroInv',$diaInviNro.') de '.$this->metMesLetras($mesInviNro).' de '.$anioInviNro);
        $this->metReemplazar('analistas',$analistas);
        $this->metReemplazar('nroVisualEvaluacion',$nroVisualEvaluacion);
        $this->metReemplazar('cargo1',$cargoA);
        $this->metReemplazar('cargo2',$cargoB);
        $this->metReemplazar('cargo3',$cargoC);
        $this->metReemplazar('persona1',$analistaA);
        $this->metReemplazar('persona2',$analistaB);
        $this->metReemplazar('persona3',$analistaC);

        $tablaEvaluacion = "<table border='1' style='font-family: Arial; font-size: 10pt; width: 600px; text-align: center;' cellspacing='0'>
                        <tr>
                            <td style='width: 264px; vertical-align: middle;' rowspan='2'><b>EMPRESA</b></td>
                            <td style='width: 264px; vertical-align: middle;' colspan='2'><b>PUNTUACIÓN CRITERIOS</b></td>
                            <td style='width: 132px; vertical-align: middle;' rowspan='2'><b>TOTAL <br/>PUNTUACIÓN</b></td>
                        </tr>
                        <tr>
                            <td style='width: 132px; vertical-align: middle;'><b>CUALITATIVOS</b></td>
                            <td style='width: 132px; vertical-align: middle;'><b>CUANTITATIVOS</b></td>
                        </tr>";
        $cantidadProveedores = array();
        $contadorProveedor = array();
        $nombreProveedores = array();
        foreach($evaluacion AS $element) {
            if($element['num_precio_unitario_iva']>0){
                if(isset($cantidadProveedores[$element['fk_lgb022_num_proveedor_recomendado']])){
                    $cantidadProveedores[$element['fk_lgb022_num_proveedor_recomendado']] = $cantidadProveedores[$element['fk_lgb022_num_proveedor_recomendado']] + $element['num_pp'];
                    $contadorProveedor[$element['fk_lgb022_num_proveedor_recomendado']] = $contadorProveedor[$element['fk_lgb022_num_proveedor_recomendado']]+1;
                } else {
                    $cantidadProveedores[$element['fk_lgb022_num_proveedor_recomendado']] = $element['num_pp'];
                    $contadorProveedor[$element['fk_lgb022_num_proveedor_recomendado']] = 1;
                }
                $nombreProveedores[$element['fk_lgb022_num_proveedor_recomendado']] = $element['nombre'];
            }
        }
        $proveedores = '';
        $analisis = '<ul>';
        foreach($nombreProveedores AS $key=>$value) {
            $cualitativa = 0;
            $cuantitativa = $cantidadProveedores[$key]/$contadorProveedor[$key];
            foreach($evaluacionCualitativa AS $element2) {
                if($element2['cod_detalle']=='01' AND $element2['fk_lgb022_num_proveedor_recomendado']==$key){
                    $cualitativa = $cualitativa+$element2['num_puntaje'];
                } elseif($element2['cod_detalle']=='02' AND $element2['fk_lgb022_num_proveedor_recomendado']==$key){
                    $cuantitativa = $cuantitativa+$element2['num_puntaje'];
                }
            }
            $total =  number_format($cualitativa + $cuantitativa,2,',','.');
            $tablaEvaluacion .="<tr>
                                <td>".$value."</td>
                                <td>".$cualitativa."</td>
                                <td>".$cuantitativa."</td>
                                <td>".$total."</td>
                                </tr>";
            $proveedores.="<p style='margin-left:30px;'><b>- ".$value."</b></p><br/>";
            $analisis.="<li>La empresa ".$value." obtuvo ".$total." puntos en la calificación final</li>";

        }


        $tablaRecomendaciones = "<table border='1' style='font-family: Arial; font-size: 10pt; width: 600px; text-align: center;' cellspacing='0'>
                        <tr>
                            <td style='width: 90px; vertical-align: middle;'><b>RENGLON</b><br/></td>
                            <td style='width: 220px; vertical-align: middle;'><b>PROVEEDOR</b><br/></td>
                            <td style='width: 220px; vertical-align: middle;'><b>DESCRIPCIÓN </b><br/></td>
                            <td style='width: 100px; vertical-align: middle;'><b>UNIDAD </b><br/></td>
                            <td style='width: 100px; vertical-align: middle;'><b>CANTIDAD </b><br/></td>
                        </tr>";
        $c=0;
        foreach ($itemsComm as $key=>$value) {
            $c=$c+1;
            # $recomendacionesCantidad[$value] -> cantidad
            $tablaRecomendaciones .="<tr>
                                    <td>".$c."</td>
                                    <td>".$value['nombre']."</td>
                                    <td>".$value['ind_descripcion']."</td>
                                    <td>".$value['unidad']."</td>
                                    <td>".$value['num_cantidad']."</td>
                                    </tr>";
        }
        $tablaRecomendaciones .="</table>";
        $analisis .= '</ul>';
        $this->metReemplazar('proveedores',$proveedores,1);
        $this->metReemplazar('tablaRecomendaciones',$tablaRecomendaciones,1);

        $tablaEvaluacion .="</table>";
        $this->metReemplazar('tablaEvaluacion',$tablaEvaluacion,1);
        $this->metReemplazar('analisis',$analisis,1);
        $this->metReemplazar('conclusiones',$informe['ind_conclusiones']);
        $this->metReemplazar('recomendaciones',$informe['rec']);

        $this->atDocx->render('.'.DS.'publico' .DS.'procesoCompra' .DS.'informeRecomendacion'.$idRec.'.odt');
        $validacion['idRec']=$idRec;
        $validacion['ruta']='.'.DS.'publico' .DS.'procesoCompra' .DS.'informeRecomendacion'.$idRec.'.odt';
        echo json_encode($validacion);
        exit;
    }

    //Método para actualizar el estado del informes de recomendacion
	public function metActualizarInformeRec() {
		$this->metValidarToken();
		$idInfor = $this->metObtenerAlphaNumerico('idInfor');
		$opcion = $this->metObtenerAlphaNumerico('opcion');

		if ($opcion=='revisar') {
			$validacion['status']='Revisado';
			$id=$this->atActa->metActualizarInforme($idInfor,'RV','fk_rhb001_num_empleado_revisado_por','fec_revision');
		} elseif ($opcion=='aprobar') {
			$validacion['status']='Aprobado';
			$id=$this->atActa->metActualizarInforme($idInfor,'AP','fk_rhb001_num_empleado_aprobado_por','fec_aprobacion');
		} else {
			$validacion['status']='Reversado';
			$id=$this->atActa->metActualizarInforme($idInfor,'PR','');
		}
		$validacion['idInfor']=$idInfor;
		echo json_encode($validacion);
		exit;
	}

    //Método para generar el documento de las evaluaciones cualitativas-cuantitativas PDF
    public function metGenerarEvaluacionPdf($idEval) {

        $eval=$this->atActa->metMostrarEvaluacion($idEval);
        $acta=$this->atActa->metInvitacionesActa($eval['fk_lgb009_num_acta_inicio']);
        $actaDet = $this->atActa->metMostrarActaDetalle($eval['fk_lgb009_num_acta_inicio']);
        $datosCuant=$this->atActa->metBuscarDatosEvalCuantitativa($idEval);

        $asisA = $this->atActa->metMostrarEmpleado($eval['fk_rhb001_num_empleado_asistente_a']);
        $asisB = $this->atActa->metMostrarEmpleado($eval['fk_rhb001_num_empleado_asistente_b']);
        $asisC = $this->atActa->metMostrarEmpleado($eval['fk_rhb001_num_empleado_asistente_c']);

        $fecha = $this->metFormatoFecha($eval['fec_evaluacion'],1,'-');
        define('FECHA',$fecha);
        define('NUM_EVAL',$idEval);
        define('NOMBRE_ASIS_A',$asisA['nombre']);
        define('CARGO_ASIS_A',$asisA['ind_descripcion_cargo']);
        define('NOMBRE_ASIS_B',$asisB['nombre']);
        define('CARGO_ASIS_B',$asisB['ind_descripcion_cargo']);
        define('NOMBRE_ASIS_C',$asisC['nombre']);
        define('CARGO_ASIS_C',$asisC['ind_descripcion_cargo']);

        $this->atFPDF->SetTitle('evaluacion_anexo');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();
        $this->atFPDF->Ln(5);

        $x=110;
        $y=55;
        $i=0;
        $totalPuntos = array();
        $totalMP = array();
        $totalMonto = array();
        $menor = array();
        foreach ($acta AS $key=>$value) {
            $proveedor = $this->atActa->atRequerimientoModelo->atProveedorModelo->metBuscarProveedor($value['fk_lgb022_num_proveedor']);
            $provNombre[$key]=$proveedor['nomProveedor'];
            $provId[$value['fk_lgb022_num_proveedor']]='si';
            $this->atFPDF->Rect($x, 40, 60, 15, "D");
            $this->atFPDF->SetFont('Arial', '', 8);
            $this->atFPDF->SetXY($x,40);
            $this->atFPDF->MultiCell(60, 5, utf8_decode($provNombre[$key]), 0, 'L');

            $this->atFPDF->SetXY($x, $y);
            $this->atFPDF->Cell(15, 5, 'PUNT. MP', 1, 0,  'C');
            $this->atFPDF->Cell(30, 5, 'OFERT. (Bs) POL', 1, 0, 'C');
            $this->atFPDF->Cell(15, 5, 'PUNTOS', 1, 0,'C');


            $i=$i+1;

            $y2=55;
            $pmoPoe = 0;
            $pP = 0;
            foreach ($datosCuant AS $key3=>$value3) {
                if ($value3['fk_lgb022_num_proveedor_recomendado']==$value['fk_lgb022_num_proveedor']){
                    $idProv = $value['fk_lgb022_num_proveedor'];
                    $precio = $value3['num_precio_unitario_iva'];
                    if($precio==0){
                        $nombreProv[$idProv] = $value['nombreProveedor'];
                        $nombreProvNo[$idProv] = '';
                    } else {
                        $nombreProv[$idProv] = '';
                        $nombreProvNo[$idProv] = $value['nombreProveedor'];
                    }
                    $y2=$y2+5;
                    $this->atFPDF->SetXY($x,$y2);
                    $this->atFPDF->Cell(15, 5, $value3['num_pmo_poe'], 1, 0,  'C');
                    $this->atFPDF->Cell(30, 5, $precio, 1, 0, 'C');
                    $this->atFPDF->Cell(15, 5, $value3['num_pp'], 1, 0,'C');
                    if(isset($menor[$value3['fk_lgb023_num_acta_detalle']])) {
                        if ($menor[$value3['fk_lgb023_num_acta_detalle']] > $precio ) {
                            $menor[$value3['fk_lgb023_num_acta_detalle']] = $precio;
                        }
                    } else {
                        $menor[$value3['fk_lgb023_num_acta_detalle']] = $precio;
                    }
                    $pmoPoe = $pmoPoe + $value3['num_pmo_poe'];
                    $pP = $pP +$value3['num_pp'];
                    $totalMP[$idProv] = $pmoPoe;
                    $totalPuntos[$idProv] = $pP;
                    $totalMonto[$idProv] = $precio;
//                    echo $pmoPoe.'--'.$totalMP[$idProv].'<br>';
                }
            }

            $x=$x+60;

        }

        $this->atFPDF->SetXY(5, 55);
        $this->atFPDF->Cell(80, 5, 'Item / Commodity', 1, 0, 'L');
        $this->atFPDF->Cell(25, 5, 'MEJOR (Bs) POV', 1, 0, 'C');


        $c = 0;
        $total = 0;
        foreach ($actaDet AS $key2=>$value2) {
            if($c==1){
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $c=0;
            } else {
                $this->atFPDF->SetDrawColor(200,200,200);
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $c=1;
            }

            $this->atFPDF->SetX(5);
            $y2 = $this->atFPDF->GetY();
            $y2=$y2+5;

            $this->atFPDF->SetY($y2);
            $this->atFPDF->Cell(80, 5, $value2['ind_descripcion'], 1, 0, 'L',true);
            $this->atFPDF->Cell(25, 5, $menor[$value2['pk_num_acta_detalle']], 1, 0, 'L',true);
            $total = $total + $menor[$value2['pk_num_acta_detalle']];
        }

        $y=$this->atFPDF->GetY();


        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->Ln(7);
        $this->atFPDF->Rect(5, $y+5, 342, 0.1, "FD");
        $this->atFPDF->Rect(5, $y+12.5, 342, 0.1, "FD");

        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(80, 5, 'Total', 1, 0, 'L');
        $this->atFPDF->Cell(25, 5, number_format($total, 2, ',', '.'), 1, 0, 'C');
        foreach ($totalMP AS $key=>$value) {
            $this->atFPDF->Cell(15, 5, $value, 1, 0, 'C');
            $this->atFPDF->Cell(30, 5, $totalMonto[$key], 1, 0, 'C');
            $this->atFPDF->Cell(15, 5, $totalPuntos[$key], 1, 0, 'C');
        }


        $y = $this->atFPDF->GetY() + 5;
        $this->atFPDF->SetY($y+10);
        $this->atFPDF->Cell(265, 5, 'Proveedores que NO cotizaron: ', 1, 1, 'L');

        foreach ($nombreProvNo AS $key=>$value) {
            if($value!=''){
                $this->atFPDF->Cell(265, 5, '     - '.utf8_decode($value), 1, 1, 'L');
            }
        }

        $this->atFPDF->Output();
    }

    //Método para generar el documento de las evaluaciones cualitativas-cuantitativas ODT
    public function metGenerarEvaluacionOdt(){
        $idEval = $this->metObtenerInt('idEval');
        $this->atDocx = new Docxpresso\CreateDocument(array('template' => ROOT . 'publico' . DS . 'procesoCompra' . DS . 'evaluacion-plantilla.odt'));
        $abc = array(
            'A','B','C','D','E',
            'F','G','H','I','J',
            'K','L','M','N','O',
            'P','Q','R','S','T',
            'U','V','W','X','Y',
            'Z');
        $eval = $this->atActa->metMostrarEvaluacion($idEval);
        $coti= $this->atActa->metMostrarECCId($idEval);
        $cuali= $this->atActa->metBuscarDatosEvalCualitativa($idEval);
        $coti2= $this->atActa->metProveedores($idEval);

        $AsisA=$this->atActa->metMostrarEmpleado($eval['fk_rhb001_num_empleado_asistente_a']);
        $AsisB=$this->atActa->metMostrarEmpleado($eval['fk_rhb001_num_empleado_asistente_b']);
        $AsisC=$this->atActa->metMostrarEmpleado($eval['fk_rhb001_num_empleado_asistente_c']);

        $nroEvaluacion = '<p>0004-CPECC-'.$this->metRellenarCeros($eval['num_evaluacion'],4).'-'.$eval['fec_anio'].'</p>';
        $diaNumero = substr($eval['fec_evaluacion'],-2);
        $mesNro = substr($eval['fec_evaluacion'],5,2);
        $anio = substr($eval['fec_evaluacion'],0,4);
        $nroProc = $eval['ind_codigo_procedimiento'];
        $FechaDocumento = $this->metFormatoFecha($eval['fec_evaluacion'],1,'-');
        $objeto = $eval['ind_objeto_evaluacion'];
        $conclusion = $eval['ind_conclusion'];
        $recomendacion = $eval['ind_recomendacion'];

        $analistas = $AsisA['nombre'].", titular de la cédula de identidad N° ".$AsisA['ind_documento_fiscal'].", en su condición de ".$AsisA['ind_descripcion_cargo'];

        if($AsisC['nombre']){
            $analistas.=", ".$AsisB['nombre'].", titular de la cédula de identidad N° ".$AsisB['ind_documento_fiscal'].", en su condición de ".$AsisB['ind_descripcion_cargo']." y ".
                $AsisC['nombre'].", titular de la cédula de identidad N° ".$AsisC['ind_documento_fiscal'].", en su condición de ".$AsisC['ind_descripcion_cargo'];
        } else {
            $analistas.="y ".$AsisB['nombre'].", titular de la cédula de identidad N° ".$AsisB['ind_documento_fiscal'].", en su condición de ".$AsisB['ind_descripcion_cargo'];
        }
        $proveedores = '';
        $i=0;
        foreach ($coti2 as $key=>$value) {
            $proveedores .="<b>- Oferta '".$abc[$i]."': ".$value['nombre']."</b>\n";
            $i=$i+1;
        }

        #cantidad de aspectos cualitativos mas 2 de la columna proveedor y total
        $countCuali = 0;
        foreach ($cuali as $key2=>$value2) {
            if($value2['cod_detalle']=='01'){
                $countCuali = $countCuali +1;
            }
        }
        $c2 = ($countCuali / count($coti2)) + 2;

        $tablaAspectos = "<table border='1' style='font-family: Arial; font-size: 10pt; width: 615px;'>
                <tr><td colspan='$c2' style='text-align: center;'><b>ASPECTOS CUALITATIVOS A EVALUAR/PUNTUACIÓN POR OFERTA<br/></b></td></tr>
                ";

        $tablaAspectos .="<tr style='text-align: center;'><td><b><br/>Proveedor<br/></b></td>";
        $tabla2 = array();
        $puntajeCualitativo = 0;
        $puntajeCuantitativo= 0;
        foreach ($cuali as $key2=>$value2) {
            if($value2['cod_detalle']=='01'){
                $nom = $value2['ind_nombre_aspecto'];
                $val = $value2['num_puntaje_maximo_evaluado'];
                $tabla2[$nom] = $val;
                $cuantitativo = 100-$value2['num_total_puntaje_evaluado'];
                $puntajeCualitativo = $this->metNumtoletras($value2['num_total_puntaje_evaluado'],1)." (".number_format($value2['num_total_puntaje_evaluado'],0).")";
                $puntajeCuantitativo = $this->metNumtoletras($cuantitativo,1)." (".number_format($cuantitativo,0).")";
            }
        }
        $aspectos = '';
        $c = count($tabla2)-1;
        $c3 = 0;
        foreach ($tabla2 as $key3=>$value3) {
            $c3 = $c3+1;
            $tablaAspectos .="<td><b><br/>".$key3."<br/></b></td>";

            $aspectos .= $this->metNumtoletras($value3,1)." (".number_format($value3,0).") puntos para  ".$key3;
            #$value3
            if($c3<$c){
                $aspectos .= ", ";
            } elseif($c3==$c){
                $aspectos .= " y ";
            }
            /*
            VEINTE (20) puntos para los Renglones Cotizados,
            VEINTE (20) puntos para el cumplimiento de las características técnicas,
            DIEZ (10) puntos para el tiempo de entrega y
            DIEZ (10) puntos para las condiciones de pago
            */
        }
        $tablaAspectos .="<td style='width: 50px'><b><br/>Total<br/></b></td></tr>";

        $aspectos .= ", ";

        $i=0;
        $totalCuali = array();
        $totalCuali2 = array();
        foreach ($coti2 as $key4=>$value4) {
            $total = '0.000000';
            $tablaAspectos .="<tr style='text-align: center;'><td>Oferta '".$abc[$i]."'<br/></td>";
            foreach ($cuali as $key2=>$value2) {
                if($value2['cod_detalle']=='01'){
                    $nom = "<td>".$value2['ind_nombre_aspecto']."</td>";
                    $tabla2[$nom] = 1;
                    if($value2['fk_lgb022_num_proveedor_recomendado']==$value4['fk_lgb022_num_proveedor_recomendado']){
                        $tablaAspectos .="<td>".number_format($value2['num_puntaje'],0)."<br/></td>";
                        $total = number_format($value2['num_total_puntaje_cualitativo'],0);
                        $totalCuali[$abc[$i]] = $total;
                    }
                } elseif($value2['cod_detalle']=='02'){
                    if($value2['fk_lgb022_num_proveedor_recomendado']==$value4['fk_lgb022_num_proveedor_recomendado']){
                        if(isset($totalCuali2[$value2['fk_lgb022_num_proveedor_recomendado']])){
                            $totalCuali2[$value2['fk_lgb022_num_proveedor_recomendado']] = $totalCuali2[$value2['fk_lgb022_num_proveedor_recomendado']]+$value2['num_puntaje'];
                        } else {
                            $totalCuali2[$value2['fk_lgb022_num_proveedor_recomendado']] = $value2['num_puntaje'];
                        }
                    }
                }
            }

            $i=$i+1;
            $tablaAspectos .="<td style='width: 50px'>".$total."<br/></td></tr>";
        }
        $tablaAspectos .="</table>";

//Colocar logo en los odt
        $headerStyle = array('style' => 'min-height: 3cm');
        $urlImg = ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . 'CES.jpg';
        $rowStyle = array('style' => 'min-height: 25px');
        $cellStyle = array('style' => 'margin-left: 250px; vertical-align: middle;');
        $this->atDocx->header($headerStyle)
            ->table(array('grid' => array('620px')))
            ->row($rowStyle)
            ->cell($cellStyle)->paragraph()->image(array('src' => $urlImg, 'style' => 'width:100px; height:80px;'))
        ;
//Colocar logo en los odt

//para los aspectos cuantitativos como el indicador financiero
        $formulaPuntutacion = '(POV/POL) * MP';
        $aspectoExtra = array();

        foreach ($cuali as $key2=>$value2) {
//            $iniciales2 = '';
            if($value2['cod_detalle']=='02'){
                /*
                $cantidadCar = strlen($value2['ind_nombre_aspecto']);
                $iniciales2 .=$value2['ind_nombre_aspecto'][0];
                for ($i=0;$i<$cantidadCar;$i++){
                    if($value2['ind_nombre_aspecto'][$i]==' '){
                        $iniciales2 .=$value2['ind_nombre_aspecto'][$i+1];
                    }
                }
                */
                $aspectoExtra[$value2['ind_nombre_aspecto']] = $value2['ind_cod_aspecto'];
            }
        }

        $extraPuntuacion = '';
        foreach ($aspectoExtra as $key=>$value) {
            $extraPuntuacion .= "<pre style='font-family: Arial; font-size: 10pt; margin-left: 95px; margin-top: 0px'>        ".$value."= ".$key."</pre><br/>";
            $formulaPuntutacion = '('.$formulaPuntutacion.') + '.$value;

        }

        $tablaMatriz = "<table border='1' style='font-family: Arial; font-size: 10pt; width: 615px; text-align: center;' cellspacing='0'>
                        <tr>
                            <td colspan='5'><b>MATRIZ DE EVALUACIÓN FINAL<br/></b></td></tr>
                        <tr>
                            <td style='width: 100px;' rowspan='2'><b><br/>OFERENTE</b></td>
                            <td style='width: 140px;' rowspan='2'><b><br/>EMPRESA</b></td>
                            <td style='width: 200px;' colspan='2'><b><br/>PUNTUACIÓN CRITERIOS</b></td>
                            <td style='width: 100px;' rowspan='2'><b><br/>TOTAL <br/>PUNTUACIÓN</b></td>
                        </tr>
                        <tr>
                            <td style='width: 100px;'><b>CUALITATIVOS<br/></b></td>
                            <td style='width: 100px;'><b>CUANTITATIVOS<br/></b></td>
                        </tr>
                        ";

        $i=0;
        $listaProvResultado = "<ul>";
        $trRecomendacion = array();
        $trRecomendacionCantidad = array();
        $trRecomendacionUnidad = array();
        $trRecomendacionDescripcion = array();
        foreach ($coti2 as $key=>$value) {

            $totalCuanti = 0;
            $j=0;
            foreach ($coti as $key2=>$value2) {
                if($value['fk_lgb022_num_proveedor_recomendado']==$value2['fk_lgb022_num_proveedor_recomendado']){
                    $j=$j+1;
                    $totalCuanti = $totalCuanti + $value2['num_pp'];
                    if ($value2['num_flag_asignado']==1){
                        $trRecomendacion[$value2['fk_lgb023_num_acta_detalle']] = $value['nombre'];
                        $trRecomendacionCantidad[$value2['fk_lgb023_num_acta_detalle']] = $value2['num_cantidad'];
                        $trRecomendacionUnidad[$value2['fk_lgb023_num_acta_detalle']] = $value2['unidadCompra'];
                        $trRecomendacionDescripcion[$value2['fk_lgb023_num_acta_detalle']] = $value2['ind_descripcion'];
                    }
                }
            }
            $totalCuanti = number_format((($totalCuanti/$j)+$totalCuali2[$value['fk_lgb022_num_proveedor_recomendado']]),2);

            $total = number_format(($totalCuanti + $totalCuali[$abc[$i]]),2);

            $tablaMatriz .="<tr style='height: 50px;'>
                            <td style='vertical-align: middle;'>'".$abc[$i]."'
                            </td>
                            <td>".$value['nombre']."
                            </td>
                            <td style='vertical-align: middle;'>".$totalCuali[$abc[$i]]."
                            </td>
                            <td style='vertical-align: middle;'>".$totalCuanti."
                            </td>
                            <td style='vertical-align: middle;'>".$total."
                            </td>
                            </tr>";

            $listaProvResultado .= "<li>La empresa ".$value['nombre']." obtuvo ".$total." puntos en la calificación final.</li>";

            $i=$i+1;
        }
        $tablaMatriz .="</table>";
        $listaProvResultado .= "</ul>";

        $tablaRecomendacion = "<table border='1' style='font-family: Arial; font-size: 10pt; width: 615px; text-align: center;' cellspacing='0'>
                        <tr>
                            <td style='width: 50px;'><b>RENGLON<br/></b></td>
                            <td style='width: 125px;'><b>PROVEEDOR<br/></b></td>
                            <td style='width: 125px;'><b>DESCRIPCIÓN<br/></b></td>
                            <td style='width: 50px;'><b>UNIDAD<br/></b></td>
                            <td style='width: 50px;'><b>CANTIDAD<br/></b></td>
                        </tr>";

        $c=0;
        foreach ($trRecomendacion as $key=>$value) {
            $c=$c+1;
            $cantidad = $trRecomendacionCantidad[$key];
            $unidad = $trRecomendacionUnidad[$key];
            $descripcion = $trRecomendacionDescripcion[$key];

            $tablaRecomendacion .="<tr style='height: 50px;'>
                                        <td>".$c."</td>
                                        <td>".$value."</td>
                                        <td>".$descripcion."</td>
                                        <td>".$unidad."</td>
                                        <td>".$cantidad."</td>
                                    </tr>";
        }
        $tablaRecomendacion .="</table>";

        $this->metReemplazar('nroEval',$nroEvaluacion);
        $this->metReemplazar('diaNumero',$diaNumero);
        $this->metReemplazar('mes',$this->metMesLetras($mesNro));
        $this->metReemplazar('anio',$anio);
        $this->metReemplazar('nroProc',$nroProc);
        $this->metReemplazar('FechaDocumento',$FechaDocumento);
        $this->metReemplazar('objeto',$objeto);
        $this->metReemplazar('conclusion',$conclusion);
        $this->metReemplazar('recomendacion',$recomendacion);
        $this->metReemplazar('analistas',$analistas);
        $this->metReemplazar('proveedores',$proveedores,1);
        $this->metReemplazar('puntajeCualitativo',$puntajeCualitativo);
        $this->metReemplazar('puntajeCuantitativo',$puntajeCuantitativo);
        $this->metReemplazar('aspectos',$aspectos);
        $this->metReemplazar('formulaPuntutacion',$formulaPuntutacion);
        $this->metReemplazar('extraPuntuacion',$extraPuntuacion,1);
        $this->metReemplazar('tablaAspectos',$tablaAspectos,1);
        $this->metReemplazar('tablaMatriz',$tablaMatriz,1);
        $this->metReemplazar('listaProvResultado',$listaProvResultado,1);
        $this->metReemplazar('tablaRecomendacion',$tablaRecomendacion,1);
        $this->metReemplazar('nombreDirector',$AsisA['nombre']);
        $this->metReemplazar('cargoDirector',$AsisA['ind_descripcion_cargo']);
        $this->metReemplazar('nombreAnalista',$AsisB['nombre']);
        $this->metReemplazar('cargoAnalista',$AsisB['ind_descripcion_cargo']);
        $this->metReemplazar('nombreAnalista2',$AsisC['nombre']);
        $this->metReemplazar('cargoAnalista2',$AsisC['ind_descripcion_cargo']);

        $this->atDocx->render('.'.DS.'publico' .DS.'procesoCompra' .DS.'evaluacionCualitativaCuantitativa'.$idEval.'.odt');
        $validacion['idEval']=$idEval;
        $validacion['ruta']='.'.DS.'publico' .DS.'procesoCompra' .DS.'evaluacionCualitativaCuantitativa'.$idEval.'.odt';
        echo json_encode($validacion);
        exit;
    }

    //Método para reemplazar las variables en el documento a generar ODT
    public function metReemplazar($nombre,$valor,$extra=false){
        if($extra==1) {
            $this->atDocx->replace(array($nombre => array('value' => $valor)), array('block-type' => true));
        } else {
            $this->atDocx->replace(array($nombre => array('value' => $valor)));
        }
    }

    //Método para paginar las actas de inicio
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT 
                    acta.*,
                    evaluacion.*,
                    cotizacion.ind_num_cotizacion,
                    (SELECT COUNT(*) FROM lg_b010_invitacion AS i,lg_b023_acta_detalle AS d 
                    WHERE d.pk_num_acta_detalle = i.fk_lgb023_num_acta_detalle
                    AND d.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio) AS permisoActa,
                    case acta.ind_modalidad_contratacion
                      when 'CA' then 'Concurso Abierto'
                      when 'CC' then 'Concurso Cerrado'
                      when 'CP' then 'Consulta de Precios'
                      when 'CD' then 'Contratación Directa'
                    end as ind_modalidad_contratacion,
                    case acta.ind_estado
                      when 'PR' then 'Preparado'
                      when 'AN' then 'Anulado'
                      when 'DS' then 'Desierto'
                      when 'CO' then 'Completado'
                    end as estado
                FROM 
                    lg_b009_acta_inicio AS acta 
                    LEFT JOIN lg_b023_acta_detalle AS detalle ON detalle.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
                    LEFT JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
                    LEFT JOIN lg_b010_invitacion AS invitacion ON invitacion.fk_lgb023_num_acta_detalle = detalle.pk_num_acta_detalle
                    LEFT JOIN lg_c003_cotizacion AS cotizacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
                    INNER JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON acta.pk_num_acta_inicio = acta_requerimiento.fk_lgb009_num_acta_inicio
                    INNER JOIN lg_b001_requerimiento AS requerimiento ON acta_requerimiento.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
                    INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = requerimiento.fk_a004_num_dependencia
                    AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                WHERE 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (acta.ind_codigo_acta LIKE '%$busqueda[value]%' OR
                        acta.ind_nombre_procedimiento LIKE '%$busqueda[value]%' OR
                        acta.ind_modalidad_contratacion LIKE '%$busqueda[value]%' OR
                        acta.ind_estado LIKE '%$busqueda[value]%')
                        ";
        }
        $sql .= " GROUP BY acta.pk_num_acta_inicio ";
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_codigo_acta','ind_nombre_procedimiento','ind_modalidad_contratacion','estado');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_acta_inicio';
        #construyo el listado de botones
        $camposExtra = array(
            'fk_rhb001_empleado_asistente_a',
            'fk_rhb001_empleado_asistente_b',
            'fk_rhb001_empleado_asistente_c',
            'fk_rhb001_empleado_anulado_termino',
            'pk_num_acta_inicio');

        if (in_array('LG-01-01-02-02-01-M',$rol)) {
            $campos['boton']['Editar'] = array("
                <button accion='modificar' title='Editar'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        descipcion='El Usuario ha Modificado el Acta de Inicio Nro. pk_num_acta_inicio'
                        idActa='pk_num_acta_inicio' titulo='Modificar Acta de Inicio'
                        idAsisA='fk_rhb001_empleado_asistente_a'
                        idAsisB='fk_rhb001_empleado_asistente_b'
                        idAsisC='fk_rhb001_empleado_asistente_c'
                        idAsisD='fk_rhb001_empleado_anulado_termino'
                        data-keyboard='false' data-backdrop='static'
                        data-toggle='modal' data-target='#formModal' id='modificar'>
                    <i class='fa fa-edit'></i>
                </button>
                ",
                'if( $i["ind_estado"] =="PR") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-01-02-02-02-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado el Acta de Inicio Nro. pk_num_acta_inicio"
                        idActa="pk_num_acta_inicio" titulo="Consultar Acta de Inicio"
                        idAsisA="fk_rhb001_empleado_asistente_a"
                        idAsisB="fk_rhb001_empleado_asistente_b"
                        idAsisC="fk_rhb001_empleado_asistente_c"
                        idAsisD="fk_rhb001_empleado_anulado_termino"
                        data-keyboard="false" data-backdrop="static"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-01-02-02-03-E',$rol)) {
            $campos['boton']['Anular'] = array("
                <button accion='anular' title='Anular'
                        class='anular logsUsuario btn ink-reaction btn-raised btn-xs btn-danger'
                        descipcion='El Usuario ha Anulado el Procedimiento Nro. pk_num_acta_inicio'
                        idActa='pk_num_acta_inicio'
                        estado='AN'
                        titulo='Anular Acta de Inicio'
                        data-toggle='modal' data-target='#formModal'
                        data-keyboard='false' data-backdrop='static'
                        mensaje='Estas seguro que desea anular el Procedimiento!!' id='anular'>
                    <i class='icm icm-blocked'></i>
                </button>
                ",
                'if( $i["ind_estado"] =="PR" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Anular'] = false;
        }

        if (in_array('LG-01-01-02-02-04-T',$rol)) {
            $campos['boton']['Terminar'] = array("
                <button accion='terminar' title='Terminar'
                        class='anular logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        descipcion='El Usuario ha Terminado el Procedimiento Nro. pk_num_acta_inicio'
                        idActa='pk_num_acta_inicio'
                        estado='TE'
                        titulo='Terminar Procedimiento'
                        data-toggle='modal' data-target='#formModal'
                        data-keyboard='false' data-backdrop='static'
                        mensaje='Estas seguro que desea terminar el Procedimiento!!' id='terminar'>
                    <i class='md md-check'></i>
                </button>
                ",
                'if( $i["ind_estado"] =="PR" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Terminar'] = false;
        }

        if (in_array('LG-01-01-02-02-05-E',$rol)) {
            $campos['boton']['Evaluar'] = array("
                <button accion='evaluar' title='Establecer Evaluacion Cualitativa-Cuantitativa'
                        titulo='Establecer Evaluacion Cualitativa-Cuantitativa'
                        class='evaluar logsUsuario btn ink-reaction btn-raised btn btn-xs btn-info'
                        descipcion='El Usuario ha Evaluado el Acta de Inicio Nro. pk_num_acta_inicio'
                        idActa='pk_num_acta_inicio'
                        idAsisA='fk_rhb001_empleado_asistente_a'
                        idAsisB='fk_rhb001_empleado_asistente_b'
                        idAsisC='fk_rhb001_empleado_asistente_c'
                        idAsisD='fk_rhb001_empleado_anulado_termino'
                        data-toggle='modal' data-target='#formModal'
                        data-keyboard='false' data-backdrop='static'
                        id='evaluarpk_num_acta_inicio'>
                    <i class='md md-pageview'></i>
                </button>
                ",
                'if( $i["num_flag_evaluacion"]==1 AND $i["pk_num_evaluacion"]==NULL AND $i["ind_estado"]!="AN") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Evaluar'] = false;
        }

        if (in_array('LG-01-01-02-02-06-G',$rol)) {
            $campos['boton']['Generar'] = array("
                <button title='Generar Acta' idActa='pk_num_acta_inicio'
                        class='generar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary-bright'>
                    <i class='md md-assignment'></i>
                </button>
            ",
            'if( $i["permisoActa"]>2 ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
        );
        } else {
            $campos['boton']['Generar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }

    //Método para paginar las evaluaciones cualitativas-cuantitativas
    public function metJsonDataTablaEval()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT 
                  evaluacion.*,
                  recomendacion.pk_num_informe_recomendacion,
                  recomendacion.ind_estado AS estadoInfor,
                  acta.ind_estado AS estadoActa,
                  ind_proveedores AS proveedores
                FROM lg_b011_evaluacion AS evaluacion 
                LEFT JOIN lg_b012_informe_recomendacion AS recomendacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
                AND recomendacion.ind_estado!='AN'
                INNER JOIN lg_b009_acta_inicio AS acta ON evaluacion.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
                INNER JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON acta.pk_num_acta_inicio = acta_requerimiento.fk_lgb009_num_acta_inicio
                INNER JOIN lg_b001_requerimiento AS requerimiento ON acta_requerimiento.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
                INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = requerimiento.fk_a004_num_dependencia
                AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                WHERE 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (evaluacion.ind_cod_evaluacion LIKE '%$busqueda[value]%' OR
                        evaluacion.fec_anio LIKE '%$busqueda[value]%'
                        )
                        ";
        }
        $sql .= " GROUP BY evaluacion.pk_num_evaluacion ";
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_cod_evaluacion','fec_anio','proveedores');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_evaluacion';
        #construyo el listado de botones
        $camposExtra = array(
            'ind_objeto_evaluacion',
            'ind_conclusion',
            'ind_recomendacion',
            'fk_lgb009_num_acta_inicio',
            'fec_anio',
            'pk_num_informe_recomendacion',
            'fk_rhb001_num_empleado_asistente_a',
            'fk_rhb001_num_empleado_asistente_b',
            'fk_rhb001_num_empleado_asistente_c');
        if (in_array('LG-01-01-03-02-01-M',$rol)) {
            $campos['boton']['Editar'] = array("
                <button accion='modificar' title='Editar Evaluación'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        descipcion='El Usuario ha Modificado el Evaluación Nro. $clavePrimaria'
                        idEval='$clavePrimaria' titulo='Modificar Evaluación'
                        idActa='fk_lgb009_num_acta_inicio'
                        data-toggle='modal' data-target='#formModal' id='modificar'>
                    <i class='fa fa-edit'></i>
                </button>
                ",
                'if( $i["estadoActa"]!="CO" AND $i["estadoActa"]!="AN" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-01-03-02-07-V',$rol)) {
            $campos['boton']['Ver'] = "
                <button title='Consultar Evaluación'
                        class='consultar logsUsuario btn ink-reaction btn-raised btn-xs btn-warning'
                        descipcion='El Usuario ha Consultado el Evaluación Nro. $clavePrimaria'
                        idEval='$clavePrimaria' titulo='Consultar Evaluación'
                        idActa='fk_lgb009_num_acta_inicio'
                        data-toggle='modal' data-target='#formModal' id='consultar'>
                    <i class='fa fa-eye'></i>
                </button>
                ";
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-01-03-02-05-G',$rol)) {
            $campos['boton']['GenerarPdf'] = "
                <a href='modLG/compras/actaInicioCONTROL/generarEvaluacionPdfMET/$clavePrimaria' target='_blank'>
                    <button title='Generar Evaluacion Pdf'
                            class='generar logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                            titulo='Generar Evaluacion Pdf' id='generar'>
                        <i class='md md-assignment'></i>
                    </button>
                </a>
                ";
        } else {
            $campos['boton']['GenerarPdf'] = false;
        }

        if (in_array('LG-01-01-03-02-06-G',$rol)) {
            $campos['boton']['GenerarOdt'] = "
                <button title='Generar Evaluacion Odt'
                        idEval='$clavePrimaria'
                        class='generarEvaluacionOdt logsUsuario btn ink-reaction btn-raised btn-xs btn-warning'
                        titulo='Generar Evaluacion Odt' id='generar'>
                    <i class='md md-assignment'></i>
                </button>
                ";
        } else {
            $campos['boton']['GenerarOdt'] = false;
        }

        if (in_array('LG-01-01-03-02-02-N',$rol)) {
            $campos['boton']['Crear1'] = array("
                <button accion='crear' title='Crear informe de Recomendacion'
                            class='crear logsUsuario btn ink-reaction btn-raised btn-xs btn-danger'
                            idEval='$clavePrimaria' titulo='Crear informe de Recomendacion'
                            objeto='ind_objeto_evaluacion'
                            conclusion='ind_conclusion'
                            recomendacion='ind_recomendacion'
                            idAsisA='fk_rhb001_num_empleado_asistente_a'
                            idAsisB='fk_rhb001_num_empleado_asistente_b'
                            idAsisC='fk_rhb001_num_empleado_asistente_c'
                            opcion='crear'
                            idActa='fk_lgb009_num_acta_inicio'
                            fecanio='fec_anio'
                            data-toggle='modal' data-target='#formModal' id='crear$clavePrimaria'>
                    <i class='md md-find-in-page'></i>
                </button>
                ",
                'if( !$i["pk_num_informe_recomendacion"] ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Crear1'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }

    //Método para paginar los informes de recomendacion
    public function metJsonDataTablaInforme($opcion)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  informe.*,
                    case informe.ind_estado
                      when 'PR' then 'Preparado'
                      when 'RV' then 'Revisado'
                      when 'AP' then 'Aprobado'
                      when 'AN' then 'Anulado'
                      when 'CO' then 'Completado'
                      when 'DS' then 'Desierto'
                    end as estado,
                  evaluacion.ind_cod_evaluacion
                FROM lg_b012_informe_recomendacion AS informe
                INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = informe.fk_lgb011_num_evaluacion
                INNER JOIN lg_b009_acta_inicio AS acta ON evaluacion.fk_lgb009_num_acta_inicio = acta.pk_num_acta_inicio
                INNER JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON acta.pk_num_acta_inicio = acta_requerimiento.fk_lgb009_num_acta_inicio
                INNER JOIN lg_b001_requerimiento AS requerimiento ON acta_requerimiento.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
                INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = requerimiento.fk_a004_num_dependencia
                AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                WHERE 1 
                  ";
        if($opcion=='revisar'){
            $sql .= " AND informe.ind_estado = 'PR' ";
        }elseif($opcion=='aprobar'){
            $sql .= " AND informe.ind_estado = 'RV' ";
        }
        if ($busqueda['value']) {
            $sql .= " AND
                        (evaluacion.ind_cod_evaluacion LIKE '%$busqueda[value]%' OR
                        informe.pk_num_informe_recomendacion LIKE '%$busqueda[value]%' OR
                        informe.ind_objeto_consulta LIKE '%$busqueda[value]%' OR
                        informe.fec_creacion LIKE '%$busqueda[value]%' OR
                        informe.ind_estado LIKE '%$busqueda[value]%'
                        )
                        ";
        }
        $sql .= " GROUP BY informe.pk_num_informe_recomendacion ";
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_informe_recomendacion','ind_cod_evaluacion','ind_objeto_consulta','fec_creacion','estado');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_informe_recomendacion';
        #construyo el listado de botones
        $camposExtra = array(
            'fk_rhb001_num_empleado_asistente_a',
            'fk_rhb001_num_empleado_asistente_b',
            'fk_rhb001_num_empleado_asistente_c');
        if (in_array('LG-01-01-03-04-01-R',$rol) AND $opcion=='revisar' AND $opcion!='todos') {
            $campos['boton']['Revisar'] = "
                <button accion='revisar' title='Revisar informe de Recomendacion'
                        class='accion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        idInfor='$clavePrimaria' titulo='Revisar Informe de Recomendacion'
                        opcion='revisar'
                        data-toggle='modal' data-target='#formModal'>
                    <i class='icm icm-rating'></i>
                </button>
                ";
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('LG-01-01-03-05-01-A',$rol) AND $opcion=='aprobar' AND $opcion!='todos') {
            $campos['boton']['Revisar'] = "
                <button accion='aprobar' title='Aprobar informe de Recomendacion'
                        class='accion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        idInfor='$clavePrimaria' titulo='Aprobar Informe de Recomendacion'
                        opcion='aprobar'
                        data-toggle='modal' data-target='#formModal'>
                    <i class='icm icm-rating3'></i>
                </button>
                ";
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('LG-01-01-03-02-03-M',$rol) AND $opcion=='todos') {
            $campos['boton']['Crear2'] = array("
                <button accion='modificarRec' title='Editar informe de Recomendacion'
                        class='modificarRec logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        idInfor='$clavePrimaria' titulo='Modificar informe de Recomendacion'
                        opcion='modificarInfo'
                        idAsisA='fk_rhb001_num_empleado_asistente_a'
                        idAsisB='fk_rhb001_num_empleado_asistente_b'
                        idAsisC='fk_rhb001_num_empleado_asistente_c'
                        data-toggle='modal' data-target='#formModal' id='modificarRec'>
                        <i class='fa fa-edit'></i>
                </button>
                ",
                'if( $i["pk_num_informe_recomendacion"]!=null AND $i["ind_estado"]=="PR" ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Crear2'] = false;
        }

        if (in_array('LG-01-01-03-02-03-M',$rol) AND $opcion=='todos') {
            $campos['boton']['Crear3'] = array("
                <button accion='modificarRec' title='Editar informe de Recomendacion'
                        class='modificarRec logsUsuario btn ink-reaction btn-raised btn-xs btn-warning'
                        idInfor='$clavePrimaria' titulo='Consultar informe de Recomendacion'
                        opcion='verInfo'
                        idAsisA='fk_rhb001_num_empleado_asistente_a'
                        idAsisB='fk_rhb001_num_empleado_asistente_b'
                        idAsisC='fk_rhb001_num_empleado_asistente_c'
                        data-toggle='modal' data-target='#formModal' id='modificarRec'>
                        <i class='fa fa-eye'></i>
                </button>
                ",
                'if( $i["pk_num_informe_recomendacion"]!=null ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Crear3'] = false;
        }

        if (in_array('LG-01-01-03-02-04-G',$rol) AND $opcion=='todos') {
            $campos['boton']['GenerarInforme'] = array("
                <button title='Generar informe de Recomendacion'
                        class='generarRecomendacionOdt logsUsuario btn ink-reaction btn-raised btn-xs btn-primary-light'
                        idRec='$clavePrimaria'
                        titulo='Generar informe de Recomendacion' id='generar'>
                    <i class='md md-assignment'></i>
                </button>
                ",
                'if( $i["pk_num_informe_recomendacion"] ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['GenerarInforme'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }

}
