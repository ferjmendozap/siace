<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class itemsControlador extends Controlador
{
    private $atItemsModelo;
    private $atMiscelaneoModelo;
    private $atSubFamiliasModelo;
    private $atUnidadesConversionModelo;
    private $atUnidadesModelo;
    private $atAlmacenModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atItemsModelo = $this->metCargarModelo('items', 'maestros');
    }

    //Método para listar los items
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para listar las cuentas
    public function metCuenta($cual)
    {
        $this->atVista->assign('cual', $cual);
        $this->atVista->assign('lista', $this->atItemsModelo->metListarPlanCuenta($cual));
        $this->atVista->metRenderizar('cuenta', 'modales');
    }

    //Método para crear y/o modificar los items
    public function metCrearModificarItems()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idItem = $this->metObtenerInt('idItem');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();
            $ExcceccionInt = array('estado','lotes','kit','impuesto','auto','disponible','presupuesto','partida','inventarioO','inventario2','gastoO','gasto2');
            $ind = $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
            $txt = $this->metValidarFormArrayDatos('form','txt');
            $codigo = $this->metObtenerFormulas('codigo');
            $descripcion=$this->metObtenerFormulas('descripcion');
            if ($txt != null && $ind == null) {
                $validacion = $txt;
            } elseif ($txt == null && $ind != null) {
                $validacion = $ind;
            } else {
                $validacion = array_merge($txt, $ind);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if (!isset($validacion['estado'])){
                $validacion['estado'] = 0;
            }
            if (!isset($validacion['lotes'])){
                $validacion['lotes'] = 0;
            }
            if (!isset($validacion['kit'])){
                $validacion['kit'] = 0;
            }
            if (!isset($validacion['impuesto'])){
                $validacion['impuesto'] = 0;
            }
            if (!isset($validacion['auto'])){
                $validacion['auto'] = 0;
            }
            if (!isset($validacion['disponible'])){
                $validacion['disponible'] = 0;
            }
            if (!isset($validacion['presupuesto'])){
                $validacion['presupuesto'] = 0;
            }
            if ($validacion['partida']=='0'){
                $validacion['partida'] = null;
            }
            if ($validacion['inventarioO']=='0'){
                $validacion['inventarioO'] = null;
            }
            if ($validacion['inventario2']=='0'){
                $validacion['inventario2'] = null;
            }
            if ($validacion['gastoO']=='0'){
                $validacion['gastoO'] = null;
            }
            if ($validacion['gasto2']=='0'){
                $validacion['gasto2'] = null;
            }

            if($validacion['min']<$validacion['reorden'] AND $validacion['max']>$validacion['reorden']) {
                #no hace nada
            } else {
                $validacion['min'] ="error";
                $validacion['reorden'] ="error";
                $validacion['max'] ="error";
                $validacion['status'] = 'errorStock';
                echo json_encode($validacion);
                exit;
            }

            $validacion['codigo'] = $codigo;
            $validacion['descripcion'] =  $descripcion;
            if($idItem==0){
                $id=$this->atItemsModelo->metCrearItems(
                    $validacion['descripcion'],$validacion['codigo'],$validacion['tipoItem'],$validacion['estado'],$validacion['lotes'],$validacion['kit'],
                    $validacion['impuesto'],$validacion['auto'],$validacion['disponible'],$validacion['presupuesto'],
                    $validacion['partida'],$validacion['inventarioO'],$validacion['gastoO'],$validacion['inventario2'],$validacion['gasto2'],
                    $validacion['min'],$validacion['max'],$validacion['reorden'],$validacion['subf'],$validacion['conversion'],
                    $validacion['compra'],$validacion['despacho'],1
                );
                $validacion['status']='nuevo';
            }else{
                $id=$this->atItemsModelo->metModificarItems(
                    $validacion['descripcion'],$validacion['codigo'],$validacion['tipoItem'],$validacion['estado'],$validacion['lotes'],$validacion['kit'],
                    $validacion['impuesto'],$validacion['auto'],$validacion['disponible'],$validacion['presupuesto'],
                    $validacion['partida'],$validacion['inventarioO'],$validacion['gastoO'],$validacion['inventario2'],$validacion['gasto2'],
                    $validacion['min'],$validacion['max'],$validacion['reorden'],$validacion['subf'],$validacion['conversion'],
                    $validacion['compra'],$validacion['despacho'],1,$idItem);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['id'] = $id;
                echo json_encode($validacion);
                exit;
            }

            if($id!=0){
                $estado = $validacion['status'];
                $validacion = $this->atItemsModelo->metListarItems($id);
                $validacion['status'] = $estado;
            }

            $validacion['idItem'] = $id;

            echo json_encode($validacion);
            exit;
        }
        if($idItem!=0){
            $this->atVista->assign('formDB',$this->atItemsModelo->metMostrarItem($idItem));
            $this->atVista->assign('idItem',$idItem);
            $this->atVista->assign('ver',$ver);
        }

        $this->atVista->assign('selectTipoItem',$this->atItemsModelo->atMiscelaneoModelo->metMostrarSelect('TIPOITM'));
        $this->atVista->assign('unidad',$this->atItemsModelo->atUnidadesConversionModelo->metListarUnidadesC());
        $this->atVista->assign('unidades',$this->atItemsModelo->atUnidadesModelo->metListarUnidades());
        $this->atVista->assign('selectSubFamilia',$this->atItemsModelo->atSubFamiliasModelo->metListarSubFamilias());
        $this->atVista->assign('almacen',$this->atItemsModelo->atAlmacenModelo->metListarAlmacen());

        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    //Método para eliminar los items
    public function metEliminarItems()
    {
        $idItem = $this->metObtenerInt('idItem');
        if($idItem!=0){
            $id=$this->atItemsModelo->metEliminarItems($idItem);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idItem
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para buscar un item
    public function metBuscarItem(){
        $idItem = $this->metObtenerInt('idIC');
        $datos = $this->atItemsModelo->metMostrarItem($idItem);
        echo json_encode($datos);
        exit;
    }

    //Método para paginar los items
    public function metJsonDataTabla($estado = false)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
              i.*,
              unidad.ind_descripcion AS uni,
              presupuestaria.cod_partida
              FROM lg_b002_item AS i
              INNER JOIN lg_b004_unidades AS unidad ON i.fk_lgb004_num_unidad_compra = unidad.pk_num_unidad 
              LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria ON presupuestaria.pk_num_partida_presupuestaria = i.fk_prb002_num_partida_presupuestaria 
              WHERE 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        ( 
                        i.pk_num_item LIKE '%$busqueda[value]%' OR 
                        i.ind_codigo_interno LIKE '%$busqueda[value]%' OR 
                        i.ind_descripcion LIKE '%$busqueda[value]%' OR 
                        unidad.ind_descripcion LIKE '%$busqueda[value]%'
                        )
                        ";
        }
        if($estado){
            $sql .= " AND i.num_estatus = 1 AND i.num_flag_verificado_presupuesto = 1 ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_item','ind_codigo_interno','ind_descripcion','uni','num_estatus','cod_partida');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_item';
        #construyo el listado de botones

        if (in_array('LG-01-07-02-01-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado el Item Nro. '.$clavePrimaria.'"
                        iditem="'.$clavePrimaria.'" titulo="Modificar Item"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-02-01-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado el Item Nro. '.$clavePrimaria.'"
                        idItem="'.$clavePrimaria.'" titulo="Consultar Item"
                        data-toggle="modal" data-target="#formModal" id="ver">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-02-01-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado el Item Nro. '.$clavePrimaria.'"
                        idItem="'.$clavePrimaria.'" titulo="Eliminar Item"
                        mensaje="Estas seguro que desea eliminar el Item!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }


}
