<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class clasificacionControlador extends Controlador
{
    private $atClasificacion;
    private $atMiscelaneoTipoReq;
    private $atAlmacenModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atClasificacion = $this->metCargarModelo('clasificacion', 'maestros');
    }

    //Método para listar las clasificaciones
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar las clasificaciones
    public function metCrearModificarClasificacion()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idClasificacion = $this->metObtenerInt('idClasificacion');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $ExcceccionInt= array('activo','tran','recepcion','caja','revision','reposicion');
            $ExcceccionAlphanum= array('estado');
            $ind = $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum',$ExcceccionAlphanum);

            if ($ind != null && $alphaNum == null) {
                $validacion = $ind;
            } elseif ($ind == null && $alphaNum != null) {
                $validacion = $alphaNum;
            } else {
                $validacion = array_merge($ind, $alphaNum);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['estado'])) {
                $validacion['estado'] = 0;
            }
            if (!isset($validacion['activo'])) {
                $validacion['activo'] = 0;
            }
            if (!isset($validacion['tran'])) {
                $validacion['tran'] = 0;
            }
            if (!isset($validacion['recepcion'])) {
                $validacion['recepcion'] = 0;
            }
            if (!isset($validacion['caja'])) {
                $validacion['caja'] = 0;
            }
            if (!isset($validacion['revision'])) {
                $validacion['revision'] = 0;
            }
            if (!isset($validacion['reposicion'])) {
                $validacion['reposicion'] = 0;
            }

            if($idClasificacion===0){
               $id=$this->atClasificacion->metCrearClasificacion(
                   $validacion['desc'],
                   $validacion['codigo'],
                   $validacion['tipoReq'],
                   $validacion['recepcion'],
                   $validacion['revision'],
                   $validacion['tran'],
                   $validacion['caja'],
                   $validacion['estado'],
                   $validacion['activo'],
                   $validacion['reposicion'],
                   $validacion['almacen']
               );
                $validacion['status']='nuevo';
            }else{
                $id=$this->atClasificacion->metModificarClasificacion(
                    $validacion['desc'],
                    $validacion['codigo'],
                    $validacion['tipoReq'],
                    $validacion['recepcion'],
                    $validacion['revision'],
                    $validacion['tran'],
                    $validacion['caja'],
                    $validacion['estado'],
                    $validacion['activo'],
                    $validacion['reposicion'],
                    $validacion['almacen'],$idClasificacion);
                $validacion['status']='modificar';
            }

            $validacion['idClasificacion']=$id;
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            echo json_encode($validacion);
            exit;
        }
        if($idClasificacion!=0){
            $this->atVista->assign('formDB',$this->atClasificacion->metMostrarClasificacion($idClasificacion));
            $this->atVista->assign('idClasificacion',$idClasificacion);
            $this->atVista->assign('ver',$ver);
        }

        $this->atVista->assign('tipoReq',$this->atClasificacion->atMiscelaneoTipoReq->metMostrarSelect('TIPOREQ'));
        $this->atVista->assign('almacen',$this->atClasificacion->atAlmacenModelo->metListarAlmacen());
        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para eliminar las clasificaciones
    public function metEliminarClasificacion()
    {
        $idClasificacion = $this->metObtenerInt('idClasificacion');
        if($idClasificacion!=0){
            $id=$this->atClasificacion->metEliminarClasificacion($idClasificacion);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Documento se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idClasificacion
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para paginar las clasificaciones
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
              cla.*,
              req.ind_nombre_detalle
              FROM lg_b017_clasificacion AS cla
              LEFT JOIN a006_miscelaneo_detalle AS req ON req.pk_num_miscelaneo_detalle = cla.fk_a006_num_miscelaneos_tipo_requerimiento ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        cla.pk_num_clasificacion LIKE '%$busqueda[value]%' OR 
                        cla.ind_descripcion LIKE '%$busqueda[value]%' OR 
                        cla.ind_cod_clasificacion LIKE '%$busqueda[value]%' OR 
                        req.ind_nombre_detalle LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_clasificacion','ind_cod_clasificacion','ind_descripcion','ind_nombre_detalle','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_clasificacion';
        #construyo el listado de botones

        if (in_array('LG-01-07-05-01-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado el Clasificacion Nro. '.$clavePrimaria.'"
                        idClasificacion="'.$clavePrimaria.'" titulo="Modificar Clasificacion"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-05-01-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado el Clasificacion Nro. '.$clavePrimaria.'"
                        idClasificacion="'.$clavePrimaria.'" titulo="Consultar Clasificacion"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-05-01-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado el Clasificacion Nro. '.$clavePrimaria.'"
                        idClasificacion="'.$clavePrimaria.'" titulo="Eliminar Clasificacion"
                        mensaje="Estas seguro que desea eliminar el Clasificacion!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }

}
