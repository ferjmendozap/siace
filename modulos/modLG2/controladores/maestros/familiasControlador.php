<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class familiasControlador extends Controlador
{
    private $atFamiliasModelo;
    private $atCommodityModelo;
    private $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atFamiliasModelo = $this->metCargarModelo('familias', 'maestros');
    }

    //Método para listar las familias
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar las familias
    public function metCrearModificarFamilias()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idFamilias = $this->metObtenerInt('idFamilias');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $Excepcion = array('estado','inventario','gasto','partida');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');
            $text = $this->metValidarFormArrayDatos('form','txt');
            $ind = $this->metValidarFormArrayDatos('form','int',$Excepcion);

            if ($text != null && $ind == null && $alphaNum == null) {
                $validacion = $text;
            } elseif ($text == null && $ind != null && $alphaNum == null) {
                $validacion = $ind;
            } elseif ($text == null && $ind == null && $alphaNum != null) {
                $validacion = $alphaNum;
            } elseif ($text == null && $ind != null && $alphaNum != null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif ($text != null && $ind != null && $alphaNum == null) {
                $validacion = array_merge($text, $ind);
            } elseif ($text != null && $ind == null && $alphaNum != null) {
                $validacion = array_merge($text, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $text);
            }

            if (!isset($validacion['estado'])) {
                $validacion['estado'] = 'I';
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if($validacion['inventario']=="0"){
                $validacion['inventario'] = null;
            }
            if($validacion['gasto']=="0"){
                $validacion['gasto'] = null;
            }
            if($validacion['partida']=="0"){
                $validacion['partida'] = null;
            }

            if($idFamilias==0){
                $id=$this->atFamiliasModelo->metCrearFamilias(
                    $validacion['desc'],
                    $validacion['codigo'],
                    $validacion['inventario'],
                    $validacion['gasto'],
                    $validacion['partida'],
                    $validacion['estado'],
                    $validacion['lineas']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atFamiliasModelo->metModificarFamilias(
                    $validacion['desc'],
                    $validacion['codigo'],
                    $validacion['inventario'],
                    $validacion['gasto'],
                    $validacion['partida'],
                    $validacion['estado'],
                    $validacion['lineas'],
                    $idFamilias);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['id'] = $id;
                echo json_encode($validacion);
                exit;
            }

            $validacion['idFamilias']=$id;

            echo json_encode($validacion);
            exit;
        }
        if($idFamilias!=0){
            $this->atVista->assign('formDB',$this->atFamiliasModelo->metMostrarFamilias($idFamilias));
            $this->atVista->assign('idFamilias',$idFamilias);
            $this->atVista->assign('ver',$ver);
        }

        $this->atVista->assign('partida',$this->atFamiliasModelo->atCommodityModelo->metListarPartida());
        $this->atVista->assign('plan',$this->atFamiliasModelo->atCommodityModelo->metListarPlanCuenta());
        $this->atVista->assign('lineas',$this->atFamiliasModelo->atMiscelaneoModelo->metMostrarSelect('LINEAS'));
        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para eliminar las familias
    public function metEliminarFamilias()
    {
        $idFamilias = $this->metObtenerInt('idFamilias');
        if($idFamilias!=0){
            $id=$this->atFamiliasModelo->metEliminarFamilias($idFamilias);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la Familia se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idFamilias
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para paginar las familias
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT familia.*,detalle.cod_detalle FROM lg_b006_clase_familia AS familia 
INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = familia.fk_a006_num_miscelaneo_clase_linea ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        familia.pk_num_clase_familia LIKE '%$busqueda[value]%' OR 
                        familia.ind_cod_familia LIKE '%$busqueda[value]%' OR 
                        detalle.cod_detalle LIKE '%$busqueda[value]%' OR 
                        familia.ind_descripcion LIKE '%$busqueda[value]%' 
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_clase_familia','cod_detalle','ind_cod_familia','ind_descripcion','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_clase_familia';
        #construyo el listado de botones

        if (in_array('LG-01-07-02-02-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado la Familia Nro. '.$clavePrimaria.'"
                        idFamilias="'.$clavePrimaria.'" titulo="Modificar Familia"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-02-02-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado la Familia Nro. '.$clavePrimaria.'"
                        idFamilias="'.$clavePrimaria.'" titulo="Consultar Familia"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-02-02-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado la Familia Nro. '.$clavePrimaria.'"
                        idFamilias="'.$clavePrimaria.'" titulo="Eliminar Familia"
                        mensaje="Estas seguro que desea eliminar la Familia!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }


}
