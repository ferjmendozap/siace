<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class unidadesConversionControlador extends Controlador
{
    private $atUnidades;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atUnidades = $this->metCargarModelo('unidadesConversion', 'maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    public function metCrearModificarUnidadesC()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idUnidades = $this->metObtenerInt('idUnidades');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $Excceccion = array('estadoUnidadC');
            $ind = $this->metValidarFormArrayDatos('formU','int');
            $texto = $this->metValidarFormArrayDatos('formU','txt',$Excceccion);

            if ($ind != null && $texto == null) {
                $validacion = $ind;
            } elseif ($ind == null && $texto != null) {
                $validacion = $texto;
            } else {
                $validacion = array_merge($texto, $ind);
            }

            if (!isset($validacion['estadoUnidadC'])) {
                $validacion['estadoUnidadC'] = 'I';
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if($idUnidades==0){
                $id=$this->atUnidades->metCrearUnidadesC($validacion['cantUnidades'],$validacion['unidad'],$validacion['estadoUnidadC']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atUnidades->metModificarUnidadesC($validacion['cantUnidades'],$validacion['unidad'],$validacion['estadoUnidadC'],$idUnidades);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            if($id!=0){
                $status = $validacion['status'];
                $validacion = $this->atUnidades->metMostrarUnidadesC($id);
                $validacion['status'] = $status;
            }

            $validacion['idUnidades']=$id;

            echo json_encode($validacion);
            exit;
        }

        $this->atVista->assign('unidades',$this->atUnidades->metListarUnidades());

        if($idUnidades!=0){
            $this->atVista->assign('formBDU',$this->atUnidades->metMostrarUnidadesC($idUnidades));
            $this->atVista->assign('idUnidades',$idUnidades);
            $this->atVista->assign('ver',$ver);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    public function metEliminarUnidadesC()
    {
        $idUnidades = $this->metObtenerInt('idUnidades');
        if($idUnidades!=0){
            $id=$this->atUnidades->metEliminarUnidadesC($idUnidades);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idUnidades
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT 
                    uc.*, 
                    u.ind_descripcion 
                FROM 
                    lg_c002_unidades_conversion AS uc 
                    INNER JOIN lg_b004_unidades AS u ON uc.fk_lgb004_num_unidad = u.pk_num_unidad ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        uc.num_cantidad LIKE '%$busqueda[value]%' OR 
                        u.ind_descripcion LIKE '%$busqueda[value]%' 
                        )
                        ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('num_cantidad','ind_descripcion','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_unidad_conversion';
        #construyo el listado de botones

        if (in_array('LG-01-07-02-05-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado la Unidad Nro. '.$clavePrimaria.'"
                        idUnidades="'.$clavePrimaria.'" titulo="Modificar Unidad"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-02-05-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado la Unidad Nro. '.$clavePrimaria.'"
                        idUnidades="'.$clavePrimaria.'" titulo="Consultar Unidad"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-02-05-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado la Unidad Nro. '.$clavePrimaria.'"
                        idUnidades="'.$clavePrimaria.'" titulo="Eliminar Unidad"
                        mensaje="Estas seguro que desea eliminar la Familia!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }


}