<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class unidadesControlador extends Controlador
{
    private $atUnidades;
    private $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atUnidades = $this->metCargarModelo('unidades', 'maestros');
    }

    //Método para listar las unidades
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar las unidades
    public function metCrearModificarUnidades()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idUnidades = $this->metObtenerInt('idUnidades');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $Excceccion = array('estadoUnidades');
            $ind = $this->metValidarFormArrayDatos('form','int');
            $texto = $this->metValidarFormArrayDatos('form','txt',$Excceccion);
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');

            if ($alphaNum != null && $ind == null && $texto == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $texto == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $texto != null) {
                $validacion = $texto;
            } elseif ($alphaNum == null && $ind != null && $texto != null) {
                $validacion = array_merge($texto, $ind);
            } elseif ($alphaNum != null && $ind == null && $texto != null) {
                $validacion = array_merge($texto, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $texto == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $texto);
            }
            if (!isset($validacion['estadoUnidades'])) {
                $validacion['estadoUnidades'] = '0';
            }
            if (!isset($validacion['tipoMedi'])) {
                $validacion['tipoMedi'] = 'error';
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if(isset($validacion['cantidadC'])){
                foreach ($validacion['cantidadC'] as $key=>$value) {
                    if($value=='error' OR $validacion['codigoC'][$key]=='error'){
                        $validacion['error']='error';
                        $validacion['status']='errorConversion';
                        echo json_encode($validacion);
                        exit;
                    }
                    if(!isset($validacion['estadoC'][$key])){
                        $validacion['estadoC'][$key] = 0;
                    }
                }
            } else {
                $validacion['error']='error';
                $validacion['status']='errorConversion';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idUnidades']=$idUnidades;
            if($idUnidades==0){
                $id=$this->atUnidades->metCrearUnidades($validacion);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atUnidades->metModificarUnidades($validacion);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }


            echo json_encode($validacion);
            exit;
        }

        if($idUnidades!=0){
            $this->atVista->assign('formBDU',$this->atUnidades->metMostrarUnidades($idUnidades));
            $this->atVista->assign('formBDUC',$this->atUnidades->metMostrarUnidadesC($idUnidades));
            $this->atVista->assign('idUnidades',$idUnidades);
            $this->atVista->assign('ver',$ver);
        }

        $this->atVista->assign('TIPOMED',$this->atUnidades->atMiscelaneoModelo->metMostrarSelect('TIPOMED'));
        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para eliminar las unidades
    public function metEliminarUnidades()
    {
        $idUnidades = $this->metObtenerInt('idUnidades');
        if($idUnidades!=0){
            $id=$this->atUnidades->metEliminarUnidades($idUnidades);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idUnidades
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para paginar las unidades
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT 
                  u.*,detalle.ind_nombre_detalle AS tipo_medida 
                FROM lg_b004_unidades AS u
                INNER JOIN a006_miscelaneo_detalle AS detalle 
                ON detalle.pk_num_miscelaneo_detalle = u.fk_a006_num_miscelaneos_tipo_medida ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        u.pk_num_unidad LIKE '%$busqueda[value]%' OR 
                        u.ind_descripcion LIKE '%$busqueda[value]%' OR 
                        detalle.ind_nombre_detalle LIKE '%$busqueda[value]%' 
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_unidad','ind_descripcion','tipo_medida','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_unidad';
        #construyo el listado de botones

        if (in_array('LG-01-07-02-04-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado la UnidadNro. '.$clavePrimaria.'"
                        idUnidades="'.$clavePrimaria.'" titulo="Modificar Unidad"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-02-04-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado la UnidadNro. '.$clavePrimaria.'"
                        idUnidades="'.$clavePrimaria.'" titulo="Consultar Unidad"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-02-04-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado la UnidadNro. '.$clavePrimaria.'"
                        idUnidades="'.$clavePrimaria.'" titulo="Eliminar Unidad"
                        mensaje="Estas seguro que desea eliminar la Familia!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }
}