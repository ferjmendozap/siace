<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">{if $opcion=="generar"}Procedimientos {else}Declaración de {/if}Desierto</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="15%">CÓDIGO</th>
                            <th width="30%">ASUNTO</th>
                            <th width="30%">OBJETO</th>
                            <th>ESTADO</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {

        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/compras/desiertoCONTROL/jsonDataTablaMET/{$opcion}",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_cod_evaluacion", width:200 },
                    { "data": "ind_asunto", width:200 },
                    { "data": "ind_objeto_consulta", width:200 },
                    { "data": "ind_estado" },
                    { "orderable": false,"data": "acciones", width:250}
                ]
        );
        var url = '{$_Parametros.url}modLG/compras/desiertoCONTROL/crearDesiertoMET';
        $('#dataTablaJson tbody').on( 'click', '.accion', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idInfor: $(this).attr('idInfor'), idActa: $(this).attr('idActa'), opcion: 'crear'}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.generar', function () {
            var url2='{$_Parametros.url}modLG/compras/desiertoCONTROL/generarMET';
            $.post(url2, { idInfor: $(this).attr('idInfor')}, function (dato) {
                window.open(dato['ruta']);
            }, 'json');
        });

    });
</script>