<form action="{$_Parametros.url}modLG/compras/desiertoCONTROL/crearDesiertoMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idInfor)}{$idInfor}{/if}" name="idInfor"/>
    <input type="hidden" value="{if isset($idActa)}{$idActa}{/if}" name="idActa"/>
    <input type="hidden" value="{if isset($opcion)}{$opcion}{/if}" name="opcion"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">

                <div class="form-group floating-label col-lg-12">
                    <textarea disabled class="form-control" cols="30" rows="10" {if isset($ver) and $ver==1} disabled {/if}>{if isset($informe.ind_asunto)}{$informe.ind_asunto}{/if}</textarea>
                    <label for="asunto"><i class="md md-border-color"></i>Asunto</label>
                </div>
                <div class="form-group floating-label col-lg-12">
                    <textarea disabled class="form-control" cols="30" rows="10" {if isset($ver) and $ver==1} disabled {/if}>{if isset($informe.ind_objeto_consulta)}{$informe.ind_objeto_consulta}{/if}</textarea>
                    <label for="objeto"><i class="md md-border-color"></i>Objeto de la Consulta de Precios</label>
                </div>
                <div class="form-group floating-label col-lg-12">
                    <textarea disabled class="form-control" cols="30" rows="10" {if isset($ver) and $ver==1} disabled {/if}>{if isset($informe.ind_conclusiones)}{$informe.ind_conclusiones}{/if}</textarea>
                    <label for="conc"><i class="md md-border-color"></i>Conclusiónes</label>
                </div>
                <div class="form-group floating-label col-lg-12">
                    <textarea disabled class="form-control" cols="30" rows="10" {if isset($ver) and $ver==1} disabled {/if}>{if isset($informe.ind_recomendacion)}{$informe.ind_recomendacion}{/if}</textarea>
                    <label for="rec"><i class="md md-border-color"></i>Recomendaciones</label>
                </div>
                <div class="form-group floating-label col-lg-12">
                    <input disabled type="text" class="form-control" value="{if isset($informe.ind_articulo_numeral)}{$informe.ind_articulo_numeral}{/if}" {if isset($ver) and $ver==1} disabled {/if}>
                    <label for="artNum"><i class="md md-border-color"></i>Numeral Articulo 113 de la Ley de Contrataciones Públicas</label>
                </div>

            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="icm icm-arrow-down"></span>  Declarar Desierto
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "60%");
    $("#formAjax").submit(function(){
        return false;
    });
    $('#fperiodo').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm" });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('#modalAncho').css("width", "40%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='generado'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'Se ha Generado el Desierto satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'Se ha Generado el Desierto satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
    });
</script>