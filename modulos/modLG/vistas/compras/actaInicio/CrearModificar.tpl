<form action="{$_Parametros.url}modLG/compras/actaInicioCONTROL/crearModificarActaMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idReq)}{$idReq}{/if}" name="idReq"/>
    <input type="hidden" value="{if isset($idActa)}{$idActa}{/if}" name="idActa"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-body">
                    <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                        <form class="form floating-label form-validation" role="form" novalidate="novalidate">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active">
                                        <a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title" style="font-weight:bold;">INFORMACION GENERAL</span> </a></li>
                                    <li>
                                        <a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title" style="font-weight:bold;">REQUERIMIENTOS</span> </a></li>
                                    <li>
                                        <a href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title" style="font-weight:bold;">CONGLOMERADOS</span> </a></li>
                                </ul>
                            </div>
                            <div class="tab-content clearfix">
                                <div class="tab-pane floating-label active" id="tab1">
                                    <div style="background-color: #0aa59b; color: #ffffff;">
                                        <center>Información General</center>
                                    </div>
                                    <div class="form-group floating-label col-lg-12" id="asisAError">
                                        <div class="col-lg-11">
                                            <div class="col-lg-3">
                                                <label for="asisA"><i class="md md-people"></i>
                                                    Primer Firmante
                                                </label>
                                            </div>
                                            <div class="col-lg-9">
                                                <input type="hidden" class="form-control" name="form[int][asisA]"
                                                       value="{if isset($formDB.fk_rhb001_empleado_asistente_a)}{$formDB.fk_rhb001_empleado_asistente_a}{/if}"
                                                       id="idEmpleado">
                                                <input type="text" class="form-control disabled" id="personaResp" value="{if isset($AsisA.nombre)}{$AsisA.nombre}{/if}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button {if isset($ver) and $ver==1} disabled {/if}
                                                    class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                    type="button"
                                                    title="Buscar Empleado"
                                                    data-toggle="modal" data-target="#formModal2"
                                                    data-keyboard="false" data-backdrop="static"
                                                    titulo="Buscar Empleado"
                                                    url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona1/"
                                            >
                                                <i class="md md-search" style="color: #ffffff;"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group floating-label col-lg-12" id="asisBError">
                                        <div class="col-lg-11">
                                            <div class="col-lg-3">
                                                <label for="asisB"><i class="md md-people"></i>
                                                    Segundo Firmante
                                                </label>
                                            </div>
                                            <div class="col-lg-9">
                                                <input type="hidden" class="form-control" name="form[int][asisB]"
                                                       value="{if isset($formDB.fk_rhb001_empleado_asistente_b)}{$formDB.fk_rhb001_empleado_asistente_b}{/if}"
                                                       id="idEmpleado2">
                                                <input type="text" class="form-control disabled" id="personaResp2" value="{if isset($AsisB.nombre)}{$AsisB.nombre} {/if}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button {if isset($ver) and $ver==1} disabled {/if}
                                                    class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                    type="button"
                                                    title="Buscar Empleado"
                                                    data-toggle="modal" data-target="#formModal2"
                                                    data-keyboard="false" data-backdrop="static"
                                                    titulo="Buscar Empleado"
                                                    url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona2/">
                                                <i class="md md-search" style="color: #ffffff;"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group floating-label col-lg-12" id="asisCError">
                                        <div class="col-lg-11">
                                            <div class="col-lg-3">
                                                <label for="asisC"><i class="md md-people"></i>
                                                    Tercer Firmante
                                                </label>
                                            </div>
                                            <div class="col-lg-9">
                                                <input type="hidden" class="form-control" name="form[int][asisC]"
                                                       value="{if isset($formDB.fk_rhb001_empleado_asistente_c)}{$formDB.fk_rhb001_empleado_asistente_c}{/if}"
                                                       id="idEmpleado3">
                                                <input type="text" class="form-control disabled" id="personaResp3" value="{if isset($AsisC.nombre)}{$AsisC.nombre} {/if}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button {if isset($ver) and $ver==1} disabled {/if}
                                                    class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                    type="button"
                                                    title="Buscar Empleado"
                                                    data-toggle="modal" data-target="#formModal2"
                                                    data-keyboard="false" data-backdrop="static"
                                                    titulo="Buscar Empleado"
                                                    url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona3/"
                                            >
                                                <i class="md md-search" style="color: #ffffff;"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group floating-label col-lg-4" id="freuError">
                                            <input readonly type="text" name="form[txt][freu]" id="freu" class="form-control" {if isset($ver) and $ver==1} disabled {/if}
                                                    {if isset($formDB.fec_reunion)} value="{$formDB.fec_reunion}"{/if}>
                                            <label for="freu"><i class="fa fa-calendar"></i>Fecha de la Reunión</label>
                                        </div>
                                        <div class="form-group floating-label col-lg-4" id="hreuError">
                                            {if isset($formDB.fec_hora_reunion)}
                                                {$hora = substr($formDB.fec_hora_reunion,0,2)}
                                                {$min = substr($formDB.fec_hora_reunion,3,2)}
                                                {if $hora<12}
                                                    {$hora = "$hora:$min am"}
                                                {elseif $hora<24 AND $hora>12}
                                                    {$hora = $hora-12}
                                                    {$hora = "$hora:$min pm"}
                                                {else}
                                                    {$hora = 12}
                                                    {$hora = "$hora:$min pm"}
                                                {/if}
                                            {else}
                                                {$hora=""}
                                            {/if}
                                            <label for="hreu"><i class="md md-av-timer"></i>Hora de la Reunión</label>
                                            <input type="text" name="form[txt][hreu]" id="hreu" class="form-control hora" value="{$hora}" {if isset($ver) and $ver==1} disabled {/if}>
                                        </div>
                                        <div class="form-group floating-label col-lg-4" id="origenError">
                                            <select id="origen" name="form[txt][origen]" class="form-control select2-list select2"{if isset($ver) and $ver==1} disabled {/if}>
                                                <option value=""></option>
                                                {foreach item=moda from=$origen}
                                                    {if isset($origen2) and $origen2 == $moda.cod_detalle}
                                                        <option value="{$moda.cod_detalle}" selected>{$moda.ind_nombre_detalle}</option>
                                                    {elseif !isset($origen2)}
                                                        <option value="{$moda.cod_detalle}">{$moda.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="origen"><i class="md md-developer-mode"></i>Quien realiza</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group floating-label col-lg-4" id="tipoError">
                                            <select id="tipo" name="form[int][tipoC]" class="form-control select2-list select2"{if isset($ver) and $ver==1} disabled {/if}>
                                                <option value=""></option>
                                                {foreach item=moda from=$modalidad}
                                                    {if isset($formDB.fk_a006_num_miscelaneos_modalidad) and $formDB.fk_a006_num_miscelaneos_modalidad == $moda.pk_num_miscelaneo_detalle}
                                                        <option value="{$moda.pk_num_miscelaneo_detalle}" selected>{$moda.ind_nombre_detalle}</option>
                                                    {else}
                                                        <option value="{$moda.pk_num_miscelaneo_detalle}">{$moda.ind_nombre_detalle}</option>
                                                    {/if}
                                                {/foreach}
                                            </select>
                                            <label for="mod"><i class="md md-developer-mode"></i>Tipo de Contratación (Casos)</label>
                                        </div>
                                        <div class="form-group floating-label col-lg-4" id="presError">
                                            <label for="pres"><i class="md md-border-color"></i>Presupuesto Base</label>
                                            <input type="text" class="form-control monto" id="pres" name="form[int][pres]" value="{if isset($formDB.num_presupuesto_base)}{number_format($formDB.num_presupuesto_base,2)}{/if}" {if isset($ver) and $ver==1} disabled {/if}">
                                        </div>
                                        <div class="form-group floating-label col-lg-4" id="contratacionError">
                                            <label for="pres"><i class="md md-border-color"></i>Modalidad de Contratación</label>
                                            <select name="form[txt][contratacion]" class="form-control select2-list select2" id="contratacion" {if isset($ver) and $ver==1} disabled {/if}>
                                                <option value=""></option>
                                                {if isset($idActa)}
                                                    {if $formDB.ind_modalidad_contratacion=='CP'}
                                                        <option value="CP" selected>Consulta de Precios</option>
                                                    {elseif $formDB.ind_modalidad_contratacion=='CA'}
                                                        <option value="CA" selected>Concurso Abierto</option>
                                                    {elseif $formDB.ind_modalidad_contratacion=='CC'}
                                                        <option value="CC" selected>Concurso Cerrado</option>
                                                    {elseif $formDB.ind_modalidad_contratacion=='CD'}
                                                        <option value="CD" selected>Contratación Directa</option>
                                                    {/if}
                                                {/if}
                                            </select>
                                            <!--input type="text" readonly class="form-control" id="" name="form[txt][contratacion]" value="{if isset($formDB.num_presupuesto_base)}{number_format($formDB.num_presupuesto_base,2)}{/if}" -->
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group floating-label col-lg-5" id="finiError">
                                            <input readonly type="text" name="form[txt][fini]" id="fini" class="form-control"
                                                   value="{if isset($formDB.fec_inicio)}{$formDB.fec_inicio}{/if}" readonly>
                                            <label for="fini"><i class="fa fa-calendar"></i>Fecha Inicio del Procedimiento</label>
                                        </div>
                                        <div class="form-group floating-label col-lg-5" id="ffinError">
                                            <input readonly type="text" name="form[txt][ffin]" id="ffin" class="form-control"
                                                   value="{if isset($formDB.fec_fin)}{$formDB.fec_fin}{/if}" readonly>
                                            <label for="ffin"><i class="fa fa-calendar"></i>Fecha de Fin del Procedimiento</label>
                                        </div>
                                        <div class="form-group floating-label col-lg-2" id="contratoError">
                                            <label class="checkbox-styled">
                                                <span>Cont. Marco</span>
                                                <input type="checkbox" name="form[int][contrato]" value="1" {if isset($ver) and $ver==1} disabled {/if} {if isset($formDB.num_flag_contrato) AND $formDB.num_flag_contrato==1}checked{/if}>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group floating-label col-lg-12" id="nombreError">
                                        <textarea class="form-control" name="nombre" {if isset($ver) and $ver==1} disabled {/if}>{if isset($formDB.ind_nombre_procedimiento)}{$formDB.ind_nombre_procedimiento}{/if}</textarea>
                                        <label for="ffin">Nombre Procedimiento</label>
                                    </div>
                                    <div class="form-group floating-label col-lg-12">
                                        <textarea class="form-control" disabled>{if isset($formDB.ind_motivo_anulado_termino)}{$formDB.ind_motivo_anulado_termino}{/if}</textarea>
                                        <label for="ffin">Motivo Anulación</label>
                                    </div>
                                </div>
                                <div class="tab-pane floating-label" id="tab2">
                                    <div style="background-color: #0aa59b; color: #ffffff;">
                                        <center>Requerimientos Asociados</center>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <table class="table table-striped no-margin" id="contenidoTabla" style="display: inline-block;width: 1400px; overflow: auto;">
                                            <thead>
                                            <tr>
                                                <th colspan="4"><center>Codigo Requerimiento</center></th>
                                            </tr>
                                            <tr>
                                                <th>Secuencia</th>
                                                <th width="500px">Descripción</th>
                                                <th width="100px">Cantidad Pedida</th>
                                            </tr>
                                            </thead>
                                            <tbody id="requerimientos">
                                            {if isset($requerimientos)}
                                                {foreach item=r from=$requerimientos}
                                                    <tr>
                                                        <td colspan="4"><center>{$r.cod_requerimiento}</center>
                                                            <input type="hidden" name="form[int][codReq][]" value="{$r.pk_num_requerimiento}"/>
                                                        </td>
                                                    </tr>
                                                    {if $r.estadoActa=='PR'}

                                                        <tr>
                                                        <td colspan="4">
                                                            <center><p style="background-color: #ff6100">Posee acta de inicio</p></center>
                                                            </td>
                                                        </tr>
                                                    {/if}
                                                    {foreach item=d from=$detalles}
                                                        {if $d.fk_lgb001_num_requerimiento==$r.pk_num_requerimiento}
                                                            <tr>
                                                                <td><input type="text" readonly class="form-control" value="{$d.num_secuencia}"></td>
                                                                <td><input type="text" readonly class="form-control" value="{$d.ind_comentarios}"></td>
                                                                <td><input type="text" readonly class="form-control" value="{$d.num_cantidad_pedida}" id="cantidad{$d.pk_num_requerimiento_detalle}"></td>
                                                            </tr>
                                                        {/if}
                                                    {/foreach}
                                                {/foreach}
                                            {/if}

                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="datosReq" class="form-group col-lg-6"></div>
                                </div>
                                <div class="tab-pane floating-label" id="tab3">
                                    <div style="background-color: #0aa59b; color: #ffffff;">
                                        <center>Requerimientos Conglomerados para el Procedimiento</center>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <table class="table table-striped no-margin" id="contenidoTabla2" style="display: inline-block;width: 1400px; overflow: auto;">
                                            <thead>
                                            <tr>
                                                <th>Secuencia</th>
                                                <th width="100px">Código</th>
                                                <th width="500px">Descripción</th>
                                                <th width="100px">Cantidad Solicitada</th>
                                            </tr>
                                            </thead>
                                            <tbody id="conglomerados">
                                            {if isset($conglomerados)}
                                                {foreach item=c from=$conglomerados}
                                                    {if isset($c.fk_lgb002_num_item)}
                                                        {$codigo=$c.fk_lgb002_num_item}
                                                        {$tipo = 'i'}
                                                    {else}
                                                        {$codigo=$c.fk_lgb003_num_commodity}
                                                        {$tipo = 'c'}
                                                    {/if}

                                                    <tr>
                                                        <input type="hidden" value="{$tipo}" name="form[txt][tipo]">
                                                        <input type="hidden" readonly value="{$c.ind_descripcion}" name="form[alphaNum][comentario][{$c.idDet}]">
                                                        <input type="hidden" readonly value="{$c.num_cantidad_pedida}" name="form[int][cantidad][{$c.idDet}]">
                                                        <input type="hidden" readonly value="{$codigo}" name="form[int][id][{$c.idDet}]">
                                                        <input type="hidden" readonly value="{$c.idDet}" name="form[int][idReqDet][{$codigo}]">
                                                        <input type="hidden" readonly value="{$c.unidad}" name="form[int][unidad][{$c.idDet}]">
                                                        <input type="hidden" readonly value="{$c.cuenta}" name="form[int][cuenta][{$c.idDet}]">
                                                        <input type="hidden" readonly value="{$c.partida}" name="form[int][partida][{$c.idDet}]">
                                                        <td><input type="text" readonly class="form-control" value="{$c.num_secuencia}"></td>
                                                        <td><input type="text" readonly class="form-control" value="{$codigo}"></td>
                                                        <td><input type="text" readonly class="form-control" value="{$c.ind_descripcion}"></td>
                                                        <td><input type="text" readonly class="form-control" value="{$c.num_cantidad_pedida}"></td>
                                                    </tr>
                                                {/foreach}
                                            {/if}

                                            </tbody>
                                        </table>
                                    </div>
                                    <div id="datosReq" class="form-group col-lg-6"></div>
                                </div>
                            </div>
                            <ul class="pager wizard">
                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idActa)}
                    <span class="fa fa-edit"></span>
                    Modificar
                {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">

    function buscarContratacion(valor) {

        var url = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/calcularPresupuestoBaseMET';
        var modalidad = $('#tipo').val();

        $.post(url, { modalidad: modalidad, valor: valor },function(dato) {

            var opciones = '';
            for (var c = 0; c <4; c = c+1) {
                if(dato['cual'][c]!=null){
                    opciones += '<option value='+dato['cual'][c]+'>'+dato['nombre'][c]+'</option>';
                }
            }
            $('#s2id_contratacion .select2-chosen').html('');
            $('#contratacion > option').remove();
            $('#contratacion').append(opciones);
        }, 'json');

    }
</script>
<script type="text/javascript">

    $('#modalAncho').css("width", "55%");
    $("#formAjax").submit(function(){
        return false;
    });
    $('.select2').select2({ allowClear: true });
    $(document).ready(function () {

        function verificarMaximo(id) {
            var cantidad = parseInt($('#cantidad'+id).val());
            var aprobado = parseInt($('#aprobado'+id).val());
            if(aprobado>cantidad){
                $('#aprobado'+id).val(cantidad);
            }
        }

        $('#freu').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#ffin').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#fini').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $(":input").inputmask();
        $(".hora").inputmask('hh:mm t');

        $(":input").inputmask();
        $(".monto").inputmask('999.999.999,99',{ rightAlignNumerics: true,numericInput: true });

        var app = new  AppFunciones();
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#pres').on('keyup',function(){
            var valor = $(this).val();
            buscarContratacion(valor);
        });

        $('#tipo').on('change',function(){
            var valor = $('#pres').val();
            buscarContratacion(valor);
        });


        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Acta fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    swal("Registro Guardado!", 'El Acta fue guardada satisfactoriamente.', "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('acta'+dato['idReq'])).remove();
                }
            }, 'json');
        });

        $('#requerimientos').on('change','.calcular',function () {
            verificarMaximo($(this).attr('valor'));
        });

        $('#requerimientos').on('click','.calcular',function () {
            var valor = $(this).attr('valor');
            var cantidad = $('#cantidad'+valor).val();
            if($(this).val()==''){
                $(this).val(cantidad);
            } else {
                $(this).val('');
            }
        });



    });
</script>