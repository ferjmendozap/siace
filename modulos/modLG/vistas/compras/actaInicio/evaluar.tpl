<form action="{$_Parametros.url}modLG/compras/actaInicioCONTROL/crearModificarEvaluacionMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idEval)}{$idEval}{/if}" name="idEval"/>
    <input type="hidden" value="{if isset($idActa)}{$idActa}{/if}" name="idActa"/>
    <input type="hidden" value="{if isset($idAsisB)}{$idAsisB}{/if}" name="idAsisB"/>
    {$disabled=""}
    {if $ver==1}
        {$disabled="disabled"}
    {/if}
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">

                    {$fuente = $inviActa}
                    {$fuente2 = $cotiActa}
                    <div class="form-group floating-label col-lg-12" id="objetoError">
                        <textarea id="objeto" class="form-control" cols="50" rows="2" name="form[formula][objeto]" {$disabled}>{if isset($eval.ind_objeto_evaluacion)}{$eval.ind_objeto_evaluacion}{else}{$inviActa[0].ind_nombre_procedimiento}{/if}</textarea>
                        <label for="objeto"><i class="md md-border-color"></i>Objeto</label>
                    </div>
                    <div class="form-group floating-label col-lg-12">
                        <table class="table table-striped no-margin border-black" id="contenidoTablaP">
                            {$oferta = array(
                            'A','B','C','D','E',
                            'F','G','H','I','J',
                            'K','L','M','N','O',
                            'P','Q','R','S','T',
                            'U','V','W','X','Y',
                            'Z')}
                            <thead>

                            {$cont=0}
                            {foreach item=invi from=$fuente}
                                <th style="vertical-align: middle; color: white; text-align: center; background-color: #008855;">
                                     Oferta {$oferta[$cont]}
                                </th>
                                {$cont=$cont+1}
                            {/foreach}
                            </thead>
                            <tbody>
                            <tr>
                                {$cont=0}
                                {$nomProv = ''}
                                {foreach item=p from=$fuente}
                                    <td style="vertical-align: middle; text-align: center;">
                                        {$nombre = $p.ind_nombre1}
                                        {$p.ind_nombre1} {$p.ind_nombre2} {$p.ind_apellido1} {$p.ind_apellido2}
                                        <input type="hidden" name="form[int][proveedores][{$cont}]" value="{$p.pk_num_proveedor}">
                                    </td>
                                    {$cont=$cont+1}
                                    {if $cont !=1}
                                        {$nomProv = "$nomProv, $nombre"}
                                    {else}
                                        {$nomProv = "$nombre"}
                                    {/if}
                                {/foreach}
                                <input type="hidden" value="{$nomProv}" name="form[formula][ind_proveedores]">
                            </tr>
                            </tbody>
                        </table>

                    </div>

                </div>
                <div class="col-lg-12">
                    <div class="form-control" style="text-align: center;">*Cuadro Cualitativo:</div>
                    <table class="table table-striped no-margin border-black" id="contenidoTabla1">
                        <thead>
                        <tr style="vertical-align: middle; color: white; text-align: center; background-color: #008855;">
                            <td class="border-black" rowspan="2" style="width: 300px;">ASPECTOS CUALITATIVOS A EVALUAR</td>
                            <td class="border-black" colspan="{count($fuente)}" style=" text-align: center;">PUNTUACIÓN OBTENIDA POR OFERTA</td>
                        </tr>
                        <tr style="vertical-align: middle; color: white; text-align: center; background-color: #008855;">
                            {$cont=0}
                            {foreach item=invi from=$fuente}
                                <td class="border-black">Oferta {$oferta[$cont]}</td>
                                {$cont=$cont+1}
                            {/foreach}
                        </tr>
                        </thead>
                        <tbody id="evaluacion">

                        {$cont=0}

                        {foreach item=asp from=$aspectos}
                            {if $asp.cod_detalle=='01'}

                                {$cont=$cont+1}
                                <input type="hidden" name="form[int][pm][{$asp.pk_num_aspecto}]" value="{$asp.num_puntaje_maximo}">
                                {if isset($datosEvalCuali)}
                                    {foreach item=cuali from=$datosEvalCuali}
                                        <!--para el id de la evaluacion cualitativa-->
                                        {if $cuali.fk_lge007_num_aspecto_cualitativo==$asp.pk_num_aspecto}
                                            <input type="hidden" name="form[int][idCuali][{$asp.pk_num_aspecto}][{$cuali.fk_lgb022_num_proveedor_recomendado}]" value="{if isset($cuali.pk_num_evaluacion_cualitativa)}{$cuali.pk_num_evaluacion_cualitativa}{else}0{/if}">
                                            {$valor[$asp.pk_num_aspecto][$cuali.fk_lgb022_num_proveedor_recomendado] = $cuali.num_puntaje}
                                            {$valor2[$cuali.fk_lgb022_num_proveedor_recomendado] = $cuali.num_total_puntaje_cualitativo}
                                            {$valor3[$asp.pk_num_aspecto] = $cuali.ind_nombre_aspecto}
                                            {$valor4[$asp.pk_num_aspecto] = $cuali.num_puntaje_maximo_evaluado}

                                        {/if}
                                    {/foreach}
                                {/if}

                                <tr>
                                    {if $ver==1}
                                        {if isset($valor4[$asp.pk_num_aspecto])}
                                            {$nombreAsp = $valor3[$asp.pk_num_aspecto]}
                                            {$puntajeMaximo1 = $valor4[$asp.pk_num_aspecto]}
                                            <td><input type="hidden" name="form[formula][nombreAsp][{$asp.pk_num_aspecto}]" value="{$asp.ind_nombre}">
                                                {$nombreAsp} P.M.({number_format($puntajeMaximo1,0)})</td>
                                        {/if}
                                    {else}
                                        {$nombreAsp = $asp.ind_nombre}
                                        {$puntajeMaximo1 = $asp.num_puntaje_maximo}
                                        <td><input type="hidden" name="form[formula][nombreAsp][{$asp.pk_num_aspecto}]" value="{$asp.ind_nombre}">
                                            {$nombreAsp} P.M.({number_format($puntajeMaximo1,0)})</td>
                                    {/if}
                                    {foreach item=prov from=$fuente}
                                        {if $ver==1}
                                            {if isset($valor4[$asp.pk_num_aspecto])}
                                                <td>
                                                    <input type="text" class="form-control calcular aspecto{$prov.pk_num_proveedor}" cual="1" name="form[int][asp][{$asp.pk_num_aspecto}][{$prov.pk_num_proveedor}]" onFocus="this.value=''" idProv="{$prov.pk_num_proveedor}" pm="{number_format($asp.num_puntaje_maximo,0)}" value="{if isset($valor[$asp.pk_num_aspecto][$prov.pk_num_proveedor])}{$valor[$asp.pk_num_aspecto][$prov.pk_num_proveedor]}{else}0{/if}" maxlength="2" {$disabled}>
                                                </td>
                                            {/if}
                                        {else}
                                            <td>
                                                <input type="text" class="form-control calcular aspecto{$prov.pk_num_proveedor}" cual="1" name="form[int][asp][{$asp.pk_num_aspecto}][{$prov.pk_num_proveedor}]" onFocus="this.value=''" idProv="{$prov.pk_num_proveedor}" pm="{number_format($asp.num_puntaje_maximo,0)}" value="{if isset($valor[$asp.pk_num_aspecto][$prov.pk_num_proveedor])}{$valor[$asp.pk_num_aspecto][$prov.pk_num_proveedor]}{else}0{/if}" maxlength="2" {$disabled}>
                                            </td>
                                        {/if}
                                    {/foreach}
                                </tr>
                            {/if}
                        {/foreach}
                        <tr>
                            {if isset($fuente[0].num_total_puntaje_cualitativo)}
                                {$puntajeMaximo = $fuente[0].num_total_puntaje_cualitativo}
                            {else}
                                {$puntajeMaximo = $puntaje.ind_valor_parametro}
                            {/if}
                            <td>Total de la Puntuación Técnica (PM: {$puntajeMaximo})</td>
                            <input type="hidden" name="form[int][totalPm]" value="{$puntaje.ind_valor_parametro}" id="totalPm">
                            {$cont=1}
                            {foreach item=prov from=$fuente}
                                <td><input type="text" class="form-control totalCuali" readonly name="form[int][total][{$prov.pk_num_proveedor}]" id="total{$prov.pk_num_proveedor}" idProv="{$prov.pk_num_proveedor}" value="{if isset($valor2[$prov.pk_num_proveedor])}{$valor2[$prov.pk_num_proveedor]}{else}0{/if}"></td>
                                {$cont=$cont+1}
                            {/foreach}
                        </tr>


                        <tr style="vertical-align: middle; color: white; text-align: center; background-color: #008855;">
                            <td class="border-black" rowspan="2" style="width: 300px;">ASPECTOS CUANTITATIVOS A EVALUAR</td>
                            <td class="border-black" colspan="{count($fuente)}" style=" text-align: center;">PUNTUACIÓN OBTENIDA POR OFERTA</td>
                        </tr>
                        <tr style="vertical-align: middle; color: white; text-align: center; background-color: #008855;">
                            {$cont=0}
                            {foreach item=invi from=$fuente}
                                <td class="border-black">Oferta {$oferta[$cont]}</td>
                                {$cont=$cont+1}
                            {/foreach}
                        </tr>
                        {$cont=0}

                        {foreach item=asp from=$aspectos}
                            {if $asp.cod_detalle=='02'}

                                {$cont=$cont+1}
                                <input type="hidden" name="form[int][pm][{$asp.pk_num_aspecto}]" value="{$asp.num_puntaje_maximo}">
                                {if isset($datosEvalCuali)}
                                    {foreach item=cuali from=$datosEvalCuali}
                                        <!--para el id de la evaluacion cualitativa-->
                                        {if $cuali.fk_lge007_num_aspecto_cualitativo==$asp.pk_num_aspecto}
                                            <input type="hidden" name="form[int][idCuali][{$asp.pk_num_aspecto}][{$cuali.fk_lgb022_num_proveedor_recomendado}]" value="{if isset($cuali.pk_num_evaluacion_cualitativa)}{$cuali.pk_num_evaluacion_cualitativa}{else}0{/if}">
                                            {$valor[$asp.pk_num_aspecto][$cuali.fk_lgb022_num_proveedor_recomendado] = $cuali.num_puntaje}
                                            {$valor2[$cuali.fk_lgb022_num_proveedor_recomendado] = $cuali.num_total_puntaje_cualitativo}
                                            {$valor3[$asp.pk_num_aspecto] = $cuali.ind_nombre_aspecto}
                                            {$valor4[$asp.pk_num_aspecto] = $cuali.num_puntaje_maximo_evaluado}
                                        {/if}
                                    {/foreach}
                                {/if}

                                <tr>
                                    {if $ver==1}
                                        {if isset($valor4[$asp.pk_num_aspecto])}
                                            {$nombreAsp = $valor3[$asp.pk_num_aspecto]}
                                            {$puntajeMaximo2 = $valor4[$asp.pk_num_aspecto]}
                                            <td><input type="hidden" name="form[formula][nombreAsp][{$asp.pk_num_aspecto}]" value="{$asp.ind_nombre}">
                                                {$nombreAsp} P.M.({number_format($puntajeMaximo2,0)})</td>
                                        {/if}
                                    {else}
                                        {$nombreAsp = $asp.ind_nombre}
                                        {$puntajeMaximo2 = number_format($asp.num_puntaje_maximo,0)}
                                        <td><input type="hidden" name="form[formula][nombreAsp][{$asp.pk_num_aspecto}]" value="{$asp.ind_nombre}">
                                            {$nombreAsp} P.M.({$puntajeMaximo2})</td>
                                    {/if}
                                    {$puntajeMaximo = ($puntajeMaximo + $puntajeMaximo2)}
                                    {foreach item=prov from=$fuente}
                                        {if $ver==1}
                                            {if isset($valor4[$asp.pk_num_aspecto])}
                                                <td>
                                                    <input type="text" class="form-control calcular aspecto{$prov.pk_num_proveedor}" cual="2" name="form[int][asp][{$asp.pk_num_aspecto}][{$prov.pk_num_proveedor}]" onFocus="this.value=''" idProv="{$prov.pk_num_proveedor}" pm="{number_format($asp.num_puntaje_maximo,0)}" value="{if isset($valor[$asp.pk_num_aspecto][$prov.pk_num_proveedor])}{$valor[$asp.pk_num_aspecto][$prov.pk_num_proveedor]}{else}0{/if}" maxlength="2" {$disabled}>
                                                </td>
                                            {/if}
                                        {else}
                                            <td>
                                                <input type="text" class="form-control calcular aspecto{$prov.pk_num_proveedor}" cual="2" name="form[int][asp][{$asp.pk_num_aspecto}][{$prov.pk_num_proveedor}]" onFocus="this.value=''" idProv="{$prov.pk_num_proveedor}" pm="{number_format($asp.num_puntaje_maximo,0)}" value="{if isset($valor[$asp.pk_num_aspecto][$prov.pk_num_proveedor])}{$valor[$asp.pk_num_aspecto][$prov.pk_num_proveedor]}{else}0{/if}" maxlength="2" {$disabled}>
                                            </td>
                                        {/if}
                                    {/foreach}
                                </tr>
                            {/if}
                        {/foreach}
                        </tbody>
                    </table>

                    <div class="form-control" style="text-align: center;">*Cuadro Cuantitativo:</div>
                    <table class="table table-striped no-margin" id="contenidoTabla2">
                        <thead>
                        <tr style="vertical-align: middle; color: white; text-align: center; background-color: #008855;">
                             <th class="border-black">OFERTA</th>
                             <th class="border-black">PROV. RECOMENDADO</th>
                             <th class="border-black">CONDICION</th>
                             <th class="border-black">Bs.</th>
                             <th class="border-black">PMO/POE</th>
                             <th class="border-black">PMP</th>
                             <th class="border-black">P.P.</th>
                        </tr>
                        </thead>
                        <tbody id="evaluacion2">
                        {$cont2=0}
                        {foreach item=r from=$detalles}
                            {$cont2=$cont2+1}
                            <tr>
                                <td colspan="7">
                                    <div class="form-control ic" style="vertical-align: middle; text-align: center; background-color: #1392e9;">{$r.ind_descripcion}</div>
                                </td>
                            </tr>

                            {$cont=0}
                            {foreach item=f from=$fuente2}
                                {$cont=$cont+1}
                                {if $r.pk_num_acta_detalle==$f.pk_num_acta_detalle}
                                    {$asignado = ''}
                                    {$monto=0}
                                    {$pp=0}
                                    {$idCotizacion = 0}
                                    {foreach item=c from=$cotizaciones}
                                        {if $c.fk_lgb023_num_acta_detalle==$r.pk_num_acta_detalle
                                        AND $c.fk_lgb022_num_proveedor==$f.pk_num_proveedor}
                                            {$monto = $c.num_precio_unitario_iva}
                                            {$idCotizacion = $c.pk_num_cotizacion}
                                            {if $c.num_flag_asignado==1}
                                                {$asignado = 'checked'}
                                            {/if}

                                            {foreach item=ecc from=$evaluacionCC}
                                                {if $ecc.fk_lgc003_num_cotizacion==$idCotizacion}
                                                    <input type="hidden" name="form[int][idEvalCC][{$f.pk_num_proveedor}][{$cont}]" value="{$ecc.pk_num_cuantitativa}">
                                                {/if}
                                            {/foreach}
                                        {/if}
                                    {/foreach}
                                    <tr>
                                        <!--349--180-->
                                        {foreach $menor AS $key=>$value}
                                            {$menor2[$key] = $menor[$key]}
                                        {/foreach}
                                        {$m = $menor2[$r.pk_num_acta_detalle]}
                                        {if $monto!=0}
                                            {$pmoPoe = number_format(($m/$monto), 6, '.', '')}
                                        {else}
                                            {$pmoPoe = number_format(0, 6, '.', '')}
                                        {/if}
                                        {$totalCuanti = 100-$puntajeMaximo}
                                        {$pp=$pmoPoe*$totalCuanti}
                                        <input type="hidden" name="form[int][pmoPoe][{$f.pk_num_proveedor}][{$cont}]" value="{$pmoPoe}">
                                        <input type="hidden" name="form[int][pp][{$f.pk_num_proveedor}][{$cont}]" value="{$pp}">
                                        <input type="hidden" name="form[int][idCotizacion][{$f.pk_num_proveedor}][{$cont}]" value="{$idCotizacion}">
                                        <td>{$f.ind_nombre1} {$f.ind_nombre2} {$f.ind_apellido1} {$f.ind_apellido2}</td>
                                        <td style="vertical-align: middle; text-align: center;">
                                            <div class="checkbox checkbox-styled ">
                                                <label class="checkbox-inline checkbox-styled">
                                                    <input type="checkbox" {$asignado} class="form-control bloqueoCheck">
                                                </label>
                                            </div>
                                        </td>
                                        <td width="200px">Oferta más Económica<br>Monto de Oferta</td>
                                        <td>
                                            <input disabled type="text" class="form-control" value="{$m}"><br>
                                            <input disabled type="text" class="form-control" value="{$monto}">
                                        </td>
                                        <td><input disabled type="text" class="form-control" value="{$pmoPoe}"></td>
                                        <td><input disabled type="text" class="form-control" value="{$totalCuanti}"></td>
                                        <td><input disabled type="text" class="form-control matriz" idProv="{$f.pk_num_proveedor}" value="{$pp}"></td>
                                    </tr>
                                {/if}

                            {/foreach} <!--foreach de los proveedores-->



                            <input type="hidden" name="form[int][contador]" value="{$cont}" id="contador">
                        {/foreach} <!--foreach de los requerimientos-->
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-12">
                    <div class="form-group floating-label col-lg-12" id="conclusionError">
                        <textarea id="conclusion" class="form-control" cols="50" rows="2" name="form[formula][conclusion]" {$disabled}>{if isset($eval.ind_conclusion)}{$eval.ind_conclusion}{/if}</textarea>
                        <label for="conclusion"><i class="md md-border-color"></i>Conclusion</label>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group floating-label col-lg-12" id="recomendacionError">
                        <textarea id="recomendacion" class="form-control" cols="50" rows="2" name="form[formula][recomendacion]" {$disabled}>{if isset($eval.ind_recomendacion)}{$eval.ind_recomendacion}{/if}</textarea>
                        <label for="recomendacion"><i class="md md-border-color"></i>Recomendacion</label>
                    </div>
                </div>
                <div class="col-lg-12">
                    <table class="table table-striped no-margin" id="contenidoTabla3">
                        <thead>
                        <tr style="vertical-align: middle; color: white; text-align: center; background-color: #008855;">
                            <th class="border-black" width="90" rowspan="2" align="center">OFERTA</th>
                            <th class="border-black" rowspan="2" align="center">EMPRESA</th>
                            <th class="border-black" width="150" height="14" colspan="2" align="center">PUNTUACIÓN CRITERIOS</th>
                            <th class="border-black" width="90" rowspan="2" align="center">TOTAL PUNTUACIÓN</th>
                        </tr>
                        <tr style="vertical-align: middle; color: white; text-align: center; background-color: #008855;">
                            <th class="border-black" width="150" height="15" align="center">CUALITATIVOS</th>
                            <th class="border-black" width="150" height="15" align="center">CUANTITATIVOS</th>
                        </tr>
                        </thead>
                        <tbody id="evaluacion3">
                        {$cont=0}
                        {foreach item=prov from=$fuente}
                            <tr>
                                <td>
                                    {$oferta[$cont]}
                                </td>
                                <td>
                                    {$prov.ind_nombre1} {$prov.ind_nombre2} {$prov.ind_apellido1} {$prov.ind_apellido2}
                                </td>
                                <td><input disabled type="text" class="form-control" id="cuali{$prov.pk_num_proveedor}"></td>
                                <td><input disabled type="text" class="form-control cuanti" id="cuanti{$prov.pk_num_proveedor}" idProv="{$prov.pk_num_proveedor}"></td>
                                <td><input disabled type="text" class="form-control totalm" id="totalm{$prov.pk_num_proveedor}" idProv="{$prov.pk_num_proveedor}"></td>
                            </tr>
                            {$cont=$cont+1}
                        {/foreach}
                        </tbody>

                    </table>
                </div>

                <div class="form-group floating-label col-lg-12" id="primerError">
                    <label for="primer"><i class="md md-people"></i>
                        Primer Firmante</label>
                    <input type="hidden" class="form-control" value="{$idPrimero}"
                           id="idEmpleado">
                    <div class="form-group floating-label col-lg-5">
                        <input type="text" class="form-control disabled" id="personaResp"
                               value="{if isset($primero.nombre)}{$primero.nombre}{/if}" disabled readonly>
                    </div>
                    <div class="form-group floating-label col-lg-6">
                        <input type="text" class="form-control disabled" id="cedula"
                               value="{if isset($primero.ind_documento_fiscal)}{$primero.ind_documento_fiscal}{elseif isset($primero.ind_cedula_documento)}{$primero.ind_cedula_documento}{/if}" disabled readonly>
                    </div>
                </div>

                <div class="form-group floating-label col-lg-12" id="segundoError">
                    <label for="segundo"><i class="md md-people"></i>
                        Segundo Firmante</label>
                    <input type="hidden" class="form-control" name="form[int][segundo]"
                           value="{if isset($eval.fk_rhb001_num_empleado_asistente_b)}{$eval.fk_rhb001_num_empleado_asistente_b}{/if}"
                           id="idEmpleado2">
                    <div class="form-group floating-label col-lg-5">
                        <input type="text" class="form-control disabled" id="personaResp2" value="{if isset($AsisB.nombre)}{$AsisB.nombre}{/if}" disabled>
                    </div>
                    <div class="form-group floating-label col-lg-6">
                        <input type="text" class="form-control disabled" id="cedula2" value="{if isset($AsisB.ind_documento_fiscal)}{$AsisB.ind_documento_fiscal}{elseif isset($AsisB.ind_cedula_documento)}{$AsisB.ind_cedula_documento}{/if}" disabled>
                    </div>

                    <div class="form-group floating-label col-lg-1">
                        <button {if isset($ver) and $ver==1} disabled {/if}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button" {$disabled}
                                title="Buscar Empleado"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Empleado"
                                url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona2/"
                                >
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>

                <div class="form-group floating-label col-lg-12" id="terceroError">
                    <label for="tercero"><i class="md md-people"></i>
                        Tercer Firmante</label>

                    <input type="hidden" class="form-control" name="form[int][tercero]"
                           value="{if isset($eval.fk_rhb001_num_empleado_asistente_c)}{$eval.fk_rhb001_num_empleado_asistente_c}{/if}"
                           id="idEmpleado3">
                    <div class="form-group floating-label col-lg-5">
                        <input type="text" class="form-control disabled" id="personaResp3" value="{if isset($AsisC.nombre)}{$AsisC.nombre}{/if}" disabled>
                    </div>
                    <div class="form-group floating-label col-lg-6">
                        <input type="text" class="form-control disabled" id="cedula3" value="{if isset($AsisC.ind_documento_fiscal)}{$AsisC.ind_documento_fiscal}{elseif isset($AsisC.ind_cedula_documento)}{$AsisC.ind_cedula_documento}{/if}" disabled>
                    </div>
                    <div class="form-group floating-label col-lg-1">
                        <button {if isset($ver) and $ver==1} disabled {/if}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button" {$disabled}
                                title="Buscar Empleado"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Empleado"
                                url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona3/"
                                >
                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idEvaluacion)}
                    <span class="fa fa-edit"></span>
                    Modificar
                {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}

            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $("#formAjax").submit(function(){
        return false;
    });

    function calcular(idProv) {
        var total = 0;
        var total2 = 0;
        $('.aspecto'+idProv).each(function (titulo, valor) {
            var pm = parseInt($(this).attr('pm'));
            if($(this).val()==''){
                $(this).val('0');
            } else if($(this).val() > pm){
                $(this).val(pm);
            }
            if($(this).attr('cual')==1){
                total = parseInt(total) + parseInt($(this).val());
            }

        });
        $(document.getElementById('total'+idProv)).val(total);
        calcularMatriz();
    }
    function calcularMatriz() {
        $('.totalm').val('0');
        $('.cuanti').val('0');
        var cuanti = 0;
        var ic = 0;
        var totalm = 0;
        $('.ic').each(function (titulo, valor) {
            ic = ic +1;
        });

        //total de las sumas de PP para la matriz
        $('.matriz').each(function (titulo, valor) {
            totalm = parseFloat($('#cuanti'+$(this).attr('idProv')).val())+parseFloat($(this).val());
            $('#cuanti'+$(this).attr('idProv')).val(totalm);
        });

        var totalCuali = 0;
        //total de la suma del resultado cualitativo
        $('.totalCuali').each(function (titulo, valor) {
            totalCuali = $(this).val();
            $('#cuali'+$(this).attr('idProv')).val(totalCuali);
        });

        totalm = 0;
        //total de las sumas de PP para la matriz entre la cantidad de items/commodities
        $('.cuanti').each(function (titulo, valor) {
            totalm = $(this).val()/ic;
            totalm = parseFloat(totalm).toFixed(2);
//            console.log($(this).attr('idProv'));
            var idProv = $(this).attr('idProv');
            $('.calcular').each(function (titulo, valor) {
                if($(this).attr('cual')==2 && $(this).attr('idProv')==idProv){
                    totalm = parseFloat(totalm) + parseFloat($(this).val());
                }
            });

            $(this).val(totalm);
        });

        totalm = 0;
        //total de cualitativo con cuantitativo para la matriz
        $('.totalm').each(function (titulo, valor) {
            var idProv = $(this).attr('idProv');
            var totalCuali = $('#cuali'+idProv).val();
            var totalCuanti = $('#cuanti'+idProv).val();
            totalm = parseFloat(totalCuali)+parseFloat(totalCuanti);
            $(this).val(totalm);
            //$(this).val(totalm.toFixed(2));
        });
    }
</script>

<script type="text/javascript">

    $('.calcular').on('blur', function () {
        calcular($(this).attr('idProv'));
    });

    $(document).ready(function () {
        var app = new  AppFunciones();
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        calcularMatriz();

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='errorNo'){
                    app.metValidarError(dato,'Disculpa. El procedimiento no puede anularse');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La Evaluacion fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La Evaluacion fue guardada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

        $('.bloqueoCheck').on('click',function () {
            var checked = $(this).attr('checked');
            if(checked=='checked'){
                $(this).attr('checked',false);
            } else {
                $(this).attr('checked',true);
            }
        });

    });
</script>