<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Evaluación Cualitativa-Cuantitativa - Acta Inicio de la Adquisición</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nro.</th>
                                <th>Proveedores</th>
                                <th width="20%">Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/compras/actaInicioCONTROL/jsonDataTablaEvalMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_cod_evaluacion", width:200 },
                    { "data": "proveedores" },
                    { "orderable": false,"data": "acciones", width:250}
                ]
        );

        var url = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/crearModificarEvaluacionMET';
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#modalAncho').css("width", "80%");
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idEval: $(this).attr('idEval'), idActa: $(this).attr('idActa')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        var url2 = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/crearModificarInformeRecMET';

        $('#dataTablaJson tbody').on( 'click', '.crear', function () {
            $('#modalAncho').css("width", "50%");
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url2, { idEval: $(this).attr('idEval'), objeto: $(this).attr('objeto'), conclusion: $(this).attr('conclusion'), recomendacion: $(this).attr('recomendacion'), idAsisA: $(this).attr('idAsisA'), idAsisB: $(this).attr('idAsisB'), idAsisC: $(this).attr('idAsisC'), opcion: $(this).attr('opcion'), idActa: $(this).attr('idActa'), fecanio: $(this).attr('fecanio') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.consultar', function () {
            $('#modalAncho').css("width", "80%");
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { ver:1, idReq: $(this).attr('idReq'), idCoti: $(this).attr('idCoti'), idEval: $(this).attr('idEval'), idAsisA: $(this).attr('idAsisA'), idAsisB: $(this).attr('idAsisB'), idAsisC: $(this).attr('idAsisC'), idActa: $(this).attr('idActa')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        var url3 = '{$_Parametros.url}modLG/compras/actaInicioCONTROL/generarEvaluacionOdtMET';
        $('#dataTablaJson tbody').on( 'click', '.generarEvaluacionOdt', function () {
            $.post(url3, { idEval: $(this).attr('idEval')}, function (dato) {
                window.open(dato['ruta']);
            }, 'json');
        });
    });

</script>
