<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">Confirmar Realización de Servicios </h2>
                    </div>
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <div class="form-wizard-nav">
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary"></div>
                            </div>
                            <ul class="nav nav-justified">
                                <li class="active">
                                    <a href="#tab1" data-toggle="tab"><span class="step">1</span>  <span class="title" style="font-weight:bold;">Ordenes Pendientes por Confirmar</span> </a></li>
                                <li>
                                    <a href="#tab2" data-toggle="tab"><span class="step">2</span>  <span class="title" style="font-weight:bold;">Confirmaciones ya Realizadas</span> </a></li>
                            </ul>
                        </div>
                        <div class="row">
                                <div class="card">
                                    <div class="card-body">
                                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                                            <div class="tab-content clearfix">
                                                <div class="tab-pane active floating-label" id="tab1">
                                                    <section class="style-default-bright">
                                                        <div class="section-body contain-lg">
                                                            <div class="row">
                                                                <div class="col-lg-12 contain-lg">
                                                                    <div class="table-responsive">
                                                                        <table id="datatable1" class="table table-striped table-hover">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>#</th>
                                                                                <th>Fecha</th>
                                                                                <th>Descripción</th>
                                                                                <th>C.C.</th>
                                                                                <th>Cantidad</th>
                                                                                <th>Acciones</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            {foreach item=post from=$listado}
                                                                                {if $post.cantidadDet!=$post.cantidadConfirm}
                                                                                    <tr id="idConfirmaciones{$post.pk_num_orden}" style="background-color: green; font-weight: bold;">
                                                                                        <td></td>
                                                                                        <td>{$post.ind_orden}</td>
                                                                                        <td>{$post.proveedorNom}</td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                    {foreach item=postdet from=$listadoDetalles1}
                                                                                        {if $postdet.fk_lgb019_num_orden==$post.pk_num_orden}
                                                                                            <tr id="idConfirmacionesdet{$postdet.pk_num_orden_detalle}">
                                                                                                <td>{$postdet.num_secuencia}</td>
                                                                                                <td>{$post.fec_creacion}</td>
                                                                                                <td>{$postdet.descripcion}</td>
                                                                                                <td>{$postdet.ind_abreviatura}</td>
                                                                                                <td>{$postdet.num_cantidad}</td>
                                                                                                <td>
                                                                                                    {if in_array('LG-01-01-12-05-01-C',$_Parametros.perfil)}
                                                                                                        <button accion="nuevo" title="Confirmar Servicio"
                                                                                                                class="nuevo logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                                                                                descipcion="El Usuario ha Confirmado el Servicio Nro. {$post.pk_num_orden}"
                                                                                                                idOrden="{$post.pk_num_orden}" idDet="{$postdet.pk_num_orden_detalle}"
                                                                                                                secuencia="{$postdet.num_secuencia}" titulo="Confirmar Servicio"
                                                                                                                data-toggle="modal" data-target="#formModal" id="nuevo">
                                                                                                            <i class="fa fa-check"></i>
                                                                                                        </button>
                                                                                                    {/if}
                                                                                                </td>
                                                                                            </tr>
                                                                                        {/if}
                                                                                    {/foreach}
                                                                                {/if}
                                                                            {/foreach}
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                                <div class="tab-pane floating-label" id="tab2">
                                                    <section class="style-default-bright">
                                                        <div class="section-body contain-lg">
                                                            <div class="row">
                                                                <div class="col-lg-12 contain-lg">
                                                                    <div class="table-responsive">
                                                                        <table id="datatable2" class="table table-striped table-hover">
                                                                            <thead>
                                                                            <tr>
                                                                                <th>#</th>
                                                                                <th>Fecha</th>
                                                                                <th>Commodities</th>
                                                                                <th>Descripción</th>
                                                                                <th>Cantidad</th>
                                                                                <th>Precio Unitario</th>
                                                                                <th>Acciones</th>
                                                                            </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            {foreach item=post from=$listado2}
                                                                                {if $post.cantidadConfirm!=0}
                                                                                    <tr id="idConfirmaciones{$post.pk_num_orden}" style="background-color: green; font-weight: bold;">
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td>{$post.ind_orden}</td>
                                                                                        <td>{$post.proveedorNom}</td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                        <td></td>
                                                                                    </tr>
                                                                                    {foreach item=postdet from=$listadoDetalles2}
                                                                                        {if $postdet.fk_lgb019_num_orden==$post.pk_num_orden AND $postdet.pk_num_confirmacion!=NULL}
                                                                                            <tr id="idConfirmacionesdet{$post.pk_num_orden}">
                                                                                                <td>{$postdet.num_secuencia}</td>
                                                                                                <td>{$post.fec_creacion}</td>
                                                                                                <td>{$postdet.pk_num_commodity}</td>
                                                                                                <td>{$postdet.descripcion}</td>
                                                                                                <td>{$postdet.num_cantidad}</td>
                                                                                                <td>{$postdet.num_precio_unitario}</td>
                                                                                                <td>
                                                                                                    {if in_array('LG-01-01-12-05-02-D',$_Parametros.perfil)}
                                                                                                        <button accion="desconfirmar" title="Desconfirmar Servicio"
                                                                                                                class="desconfirmar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                                                                                descipcion="El Usuario ha Desconfirmado el Servicio Nro. {$post.pk_num_orden}"
                                                                                                                idOrden="{$post.pk_num_orden}" sec="{$postdet.num_secuencia}" titulo="Desconfirmar Servicio"
                                                                                                                id="desconfirmar" mensaje="Estas seguro que desea Desconfirmar el Servicio!!" >
                                                                                                            <i class="md md-replay"></i>
                                                                                                        </button>
                                                                                                    {/if}
                                                                                                    {if in_array('LG-01-01-12-05-03-I',$_Parametros.perfil)}
                                                                                                        <a href="{$_Parametros.url}modLG/compras/confirmarServicioCONTROL/imprimirConfirmacionMET/{$post.pk_num_orden}/{$postdet.num_secuencia}" target="_blank">
                                                                                                            <button title="Imprimir Confirmación"
                                                                                                                    class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                                                                                    descipcion="El Usuario ha impreso la confirmacion Nro. {$post.pk_num_orden}"
                                                                                                                    idOrden="{$post.pk_num_orden}" titulo="imprimir"
                                                                                                                    id="imprimir">
                                                                                                                <i class="md md-print"></i>
                                                                                                            </button>
                                                                                                        </a>
                                                                                                    {/if}
                                                                                                </td>
                                                                                            </tr>
                                                                                        {/if}
                                                                                    {/foreach}
                                                                                {/if}
                                                                            {/foreach}
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </section>
                                                </div>
                                            </div><!--END ClearFix-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();

        var url='{$_Parametros.url}modLG/compras/confirmarServicioCONTROL/crearConfirmacionMET';
        $('.table tbody').on( 'click', '.nuevo', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idOrden: $(this).attr('idOrden'), idDet: $(this).attr('idDet'), secuencia: $(this).attr('secuencia') },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('.table tbody').on( 'click', '.desconfirmar', function () {
            var idOrden = $(this).attr('idOrden');
            var sec = $(this).attr('sec');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var $url='{$_Parametros.url}modLG/compras/confirmarServicioCONTROL/desconfirmarMET';
                $.post($url, { idOrden: idOrden, sec: sec },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idConfirmacionesdet'+dato['id'])).remove();
                        swal("Exito!", "Se desconfirmó satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }

        });
    });
</script>