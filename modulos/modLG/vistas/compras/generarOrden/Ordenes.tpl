<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Ordenes</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tipo Orden</th>
                            <th>Nro. Orden</th>
                            <th>Fecha Preparación</th>
                            <th>Proveedor</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {

        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson2',
                "{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/jsonDataTablaMET/asignar",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_orden"},
                    { "data": "ind_tipo_orden_nombre" },
                    { "data": "ind_orden" },
                    { "data": "fec_creacion"},
                    { "data": "proveedor"}
                ]
        );

        $('#dataTablaJson2 tbody').on( 'click', 'tr', function () {
            var celdas = $(this).find('td');
            var c = 0;
            var idOrden = 0;
            celdas.each(function(){
                c = c+1;
                if(c==1){
                    idOrden = $(this).html();
                }
            });

            var url = '{$_Parametros.url}modLG/compras/generarOrdenCONTROL/buscarOrdenMET';
            $.post(url, { idOrden: idOrden }, function (dato) {
                if($('#idOrden'+dato['pk_num_orden']).length){
                    swal("Error!", 'Orden ya seleccionada' , "error");
                } else {
                    $('#ordenes').append('<tr id="idOrden'+dato['pk_num_orden']+'">' +
                        '<input type="hidden" name="form[int][idOrden][]" value="'+dato['pk_num_orden']+'">' +
                        '<td>'+dato['pk_num_orden']+'</td>' +
                        '<td>'+dato['ind_tipo_orden_nombre']+'</td>' +
                        '<td>'+dato['ind_orden']+'</td>' +
                        '<td>'+dato['fec_creacion']+'</td>' +
                        '<td>'+dato['proveedor']+'</td>' +
                        '</tr>');
                }
            },'json');

            $('#cerrarModal2').click();
            $('#contenidoModal2').html('');

        });

    });
</script>