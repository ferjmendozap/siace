<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Proveedores</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson3" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre y Apellido</th>
                            <th>Nro. Documento</th>
                        </tr>
                        </thead>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson3',
            '{$_Parametros.url}modLG/maestros/proveedorCONTROL/jsonDataTablaMET/{$concepto}',
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "pk_proveedor" },
                { "data": "nomProveedor" },
                { "data": "ind_documento_fiscal" }
            ]
        );

        $('#dataTablaJson3').on('click', 'tbody tr', function () {
            var celdas = $(this).find('td');
            var c = 0;
            var idProveedor = 0;
            var nombre = '';
            var documento = '';
            celdas.each(function(){
                c = c+1;
                if(c==1){ idProveedor = $(this).html(); }
                if(c==2){ nombre = $(this).html(); }
                if(c==3){ documento= $(this).html(); }
            });

            {if $proveedor == 'proveedor'}
                var url='{$_Parametros.url}modLG/ordenCompra/ordenCompraCONTROL/buscarServiciosMET/';

                $('#tipo_servicio > option').remove();
                $.post(url, { id: idProveedor}, function (dato) {
                    var desde = 0;
                    var hasta = dato.length;
                    $('#tipo_servicio').append(
                            '<option value=""></option>'
                    );
                    while(desde<=hasta){
                        $('#tipo_servicio').append(
                                '<option value="'+dato[desde]['pk_num_tipo_servico']+'">'+dato[desde]['ind_descripcion']+'</option>'
                        );
                        desde++;
                    }
                },'json');

                $('#idProveedor').val(idProveedor);
                $('#persona').val(nombre);
                $('#cedula').val(documento);
            {elseif $proveedor == 'proveedor1'}
                $('#idProveedor'+{$tr}).val(idProveedor);
                $('#persona'+{$tr}).val(nombre);
                $('#cedula'+{$tr}).val(documento);

                $('#contenidoTabla').append(
                        '<tr class="idProveedor' + idProveedor + '">' +
                        '<input type="hidden" value="' + idProveedor + '" name="form[int][proveedor][]" class="tipoProcesoInput" proceso="' + idProveedor + '" />' +
                        '<input type="hidden" value="{$tr}" name="form[int][cant]" class="tipoProcesoInput">' +
                        '<td>{$tr}</td>' +
                        '<td>' + nombre + '</td>' +
                        '<td>' + documento + '</td>' +
                        '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger" id="idProveedor' + idProveedor + '"><i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                        '</tr>'
                );
            {elseif $proveedor == 'proveedor2'}
                $('#fk_nmb005_num_interfaz_cxp').val(idProveedor);
                $('#nombreProveedor').val(nombre);
            {/if}

            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>