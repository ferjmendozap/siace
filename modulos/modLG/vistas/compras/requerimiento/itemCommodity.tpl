<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de {$titulo}</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Código</th>
                            <th>Descripción</th>
                            <th>Partida</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {

        {if $titulo=='Items'}
        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson2',
            '{$_Parametros.url}modLG/maestros/itemsCONTROL/jsonDataTablaMET/1',
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "pk_num_item" },
                { "data": "ind_codigo_interno" },
                { "data": "ind_descripcion", width:500 },
                { "data": "cod_partida" }
            ]
        );
        {else}
        var app2 = new AppFunciones();
        var dt2 = app2.dataTable(
            '#dataTablaJson2',
            '{$_Parametros.url}modLG/maestros/commodityCONTROL/jsonDataTablaMET/{$clasificacion}/1/1',
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "pk_num_commodity" },
                { "data": "ind_cod_commodity" },
                { "data": "ind_descripcion", width:500 },
                { "data": "cod_partida" }
            ]
        );
        {/if}

        $('#dataTablaJson2 tbody').on( 'click', 'tr', function () {
            var celdas = $(this).find('td');
            var c = 0;
            var idIC = 0;
            var correcto = 0;
            var valor = 0;
            celdas.each(function(){
                c = c+1;
                if(c==1){
                    idIC = $(this).html();
                }
            });

            var url = '';
            {if $titulo=='Items'}
            url = '{$_Parametros.url}modLG/maestros/itemsCONTROL/buscarItemMET';
            {else}
            url = '{$_Parametros.url}modLG/maestros/commodityCONTROL/buscarCommodityMET';
            {/if}
            $.post(url, { idIC: idIC }, function (dato) {
                {if $id == 'item'}
                if($('#idItem'+dato['pk_num_item']).length){
                    swal("Error!", 'Item ya seleccionado' , "error");
                } else {
                    correcto = 1;
                    $(document.getElementById('item')).append(
                        '<tr id="idItem'+dato['pk_num_item']+'" idItem="'+dato['pk_num_item']+'"><td style="vertical-align: middle;">'+
                        '<input type="hidden" value="'+dato['pk_num_item']+'" name="form[int][numItem]['+dato['pk_num_item']+']">' +
                        '<input type="hidden" class="cuenta'+dato['codCuenta']+'" codigo="'+dato['idCuenta']+'" id="idItem'+dato['pk_num_item']+'cuenta">'+
                        '<input type="hidden" class="cuenta2'+dato['codCuenta2']+'" codigo="'+dato['idCuenta2']+'" id="idItem'+dato['pk_num_item']+'cuenta2">'+
                        '<input type="hidden" class="partida'+dato['codPartida']+'" codigo="'+dato['idPartida']+'" id="idItem'+dato['pk_num_item']+'partida">'+
                        '<input type="hidden" id="secidItem'+dato['pk_num_item']+'" name="form[int][sec]" value="{$tr}">'+
                        '<input type="hidden" value="'+dato['idUnidad']+'" name="form[int][pk_num_unidad]['+dato['pk_num_item']+']">'+
                        '{$tr}</td>' +
                        '<td><input type="text" class="form-control" value=1 name="form[int][canti]['+dato['pk_num_item']+']"></td>' +
                        '<td><input type="text" class="form-control" readonly value="'+dato['ind_descripcion']+'" id="comentarioI'+dato['pk_num_item']+'" name="form[alphaNum][comentarioI]['+dato['pk_num_item']+']"></td>' +
                        '<td><input type="text" class="form-control"  value="" name="form[alphaNum][especificacionTecnica]['+dato['pk_num_item']+']"></td>' +
                        '<td><input type="text" class="form-control" readonly value="'+dato['unidad']+'"></td>' +
                        '<td><input type="text" class="form-control" disabled id="estado'+dato['pk_num_item']+'" value="PR"></td>' +
                        '<td><input type="text" class="form-control" readonly id="numCC'+dato['pk_num_item']+'" name="form[int][numCC]['+dato['pk_num_item']+']" value=""></td>'+
                        '<td style="vertical-align: middle;">' +
                        '<button class="eliminar btn ink-reaction btn-raised btn-xs btn-danger" id="idItem'+dato['pk_num_item']+'">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                        '</tr>');
                    valor = 100;
                    $('#cantidad').val(parseInt($('#cantidad').val())+parseInt(1));
                }
                {/if}
                {if $id == 'item2'}
                if($('#item'+dato['pk_num_item']).length){
                    swal("Error!", 'Item ya seleccionado' , "error");
                } else {
                    $(document.getElementById('contenidoTabla')).append(
                        '<tr id="item'+dato['pk_num_item']+'">td width="10px">'+
                        '<input type="hidden" value="'+dato['pk_num_item']+'" id="numItem{$tr}" name="form[int][numItem][]">'+
                        '<input type="hidden" value="'+dato['idUnidad']+'" name="form[int][pk_num_unidad]['+dato['pk_num_item']+']">'+
                        '<td width="10px" style="vertical-align: middle;">{$tr}</td>' +
                        '<td width="10px"><input type="text" class="form-control" readonly name="descripcion[]" value="'+dato['ind_descripcion']+'" id="numItem{$tr}" size="5"></td>' +
                        '<td width="10px"><input type="text" class="form-control" value="'+dato['num_stock_actual']+'" name="form[int][sAnt][]" size="1" readonly></td>' +
                        '<td width="10px"><input type="text" class="form-control" value="1" name="form[int][canti][]" size="1"></td>' +
                        '<td width="10px"><input type="text" class="form-control" id="precio{$tr}" value="1" name="form[int][precio][]" size="2"></td>' +
                        '<td width="10px"><input type="text" class="form-control" readonly value="'+dato['idUnidad']+'" name="form[int][numUni][]" size="1"></td>' +
                        '<td width="10px" style="vertical-align: middle;">' +
                        '<input type="hidden" value="{$tr}" name="form[int][cant]" size="2">' +
                        '<button class="eliminar btn ink-reaction btn-raised btn-xs btn-danger" id="{$tr}">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                        '</tr>');
                }
                {/if}
                {if $id == 'item3'}
                if($('#item'+dato['pk_num_item']).length){
                    swal("Error!", 'Item ya seleccionado' , "error");
                } else {
                    correcto = 1;
                    $(document.getElementById('contenidoTabla')).append(
                        '<tr id="item'+dato['pk_num_item']+'" class="ic" idIC="'+dato['pk_num_item']+'">' +
                        '<input type="hidden" name="form[int][sec]" value="{$tr}">' +
                        '<input type="hidden" name="form[int][unidad]['+dato['pk_num_item']+']" value="' + dato['idUnidad'] + '">' +
                        '<input type="hidden" class="codCuenta" name="form[int][cuentaDetalle]['+dato['pk_num_item']+']" id="cuenta'+dato['pk_num_item']+'" value="' + dato['idCuenta'] + '">' +
                        '<input type="hidden" class="codCuenta2" name="form[int][cuenta2Detalle]['+dato['pk_num_item']+']" id="cuenta2'+dato['pk_num_item']+'" value="' + dato['idCuenta2'] + '">' +
                        '<input type="hidden" class="codPartida" name="form[int][partidaDetalle]['+dato['pk_num_item']+']" id="partida'+dato['pk_num_item']+'" value="' + dato['idPartida'] + '">' +
                        '<input type="hidden" name="form[txt][tipo]" value="item">' +
                        '<td width="10px" style="vertical-align: middle;">{$tr}</td>' +
                        '<td width="10px"><input type="text" readonly class="form-control" value="' + dato['pk_num_item'] + '" name="form[int][num]['+dato['pk_num_item']+']" size="4"></td>' +
                        '<td width="10px"><input type="text" readonly class="form-control" value="' + dato['ind_descripcion'] + '" size="6"></td>' +
                        '<td width="10px"><input type="text" class="form-control nuevoValor" value="1" name="form[int][cant]['+dato['pk_num_item']+']" id="cantidad'+dato['pk_num_item']+'" sec="'+dato['pk_num_item']+'" size="6"></td>' +
                        '<td width="10px"><input type="text" class="form-control nuevoValor" value="0" name="form[int][precio]['+dato['pk_num_item']+']" id="precio'+dato['pk_num_item']+'" sec="'+dato['pk_num_item']+'" size="4"></td>' +
                        '<td width="10px"><input type="text" class="form-control nuevoValor" value="0" name="form[int][descPorc]['+dato['pk_num_item']+']" id="descPorc'+dato['pk_num_item']+'" sec="'+dato['pk_num_item']+'"></td>' +
                        '<td width="10px"><input type="text" class="form-control nuevoValor" value="0" name="form[int][descFijo]['+dato['pk_num_item']+']" id="descFijo'+dato['pk_num_item']+'" sec="'+dato['pk_num_item']+'"></td>' +
                        '<td width="10px" style="vertical-align: middle;">' +
                        '<div class="checkbox checkbox-styled">' +
                        '<label class="checkbox-inline checkbox-styled">' +
                        '<input type="checkbox" class="form-control nuevoValor" name="form[int][exonerado]['+dato['pk_num_item']+']" id="exonerado' + dato['pk_num_item'] + '" idItem="' + dato['pk_num_item'] + '" value="1" sec="'+dato['pk_num_item']+'">' +
                        '<span></span> ' +
                        '</label>' +
                        '</div>' +
                        '</td>' +
                        '<td width="10px"><input type="text" class="form-control" readonly value="0" name="form[int][montoPU]['+dato['pk_num_item']+']" id="montoPU'+dato['pk_num_item']+'"></td>' +
                        '<td width="10px"><input type="text" class="form-control total" readonly value="0" name="form[int][total]['+dato['pk_num_item']+']" idItem="' + dato['pk_num_item'] + '" id="total' + dato['pk_num_item'] + '"></td>' +
                        '<td style="vertical-align: middle;"><input type="hidden" value="'+dato['pk_num_item']+'" name="form[int][cantidad]" size="2">' +
                        '<button class="eliminar btn ink-reaction btn-raised btn-xs btn-danger" id="'+dato['pk_num_item']+'">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                        '</tr>');
                    valor = 0;
                }
                {/if}
                {if $id == 'item4'}
                $(document.getElementById('nombreItem')).val(dato['ind_descripcion']);
                $(document.getElementById('nomUni')).val(dato['unidad']);
                $(document.getElementById('numItem')).val(dato['pk_num_item']);
                $(document.getElementById('codInt')).val(dato['ind_codigo_interno']);
                $(document.getElementById('slog')).val(dato['num_stock_actual']);
                $(document.getElementById('sreal')).val(0);
                {/if}

                {if $id == 'commodity'}
                if($('#idComm'+dato['pk_num_commodity']).length){
                    swal("Error!", 'Commoditie ya seleccionado' , "error");
                } else {
                    correcto = 1;
                    $(document.getElementById('contenidoTabla')).append(
                        '<tr id="idComm'+dato['pk_num_commodity']+'" idItem="'+dato['pk_num_commodity']+'">'+
                        '<input type="hidden" value="'+dato['pk_num_commodity']+'" name="form[int][numComm]['+dato['pk_num_commodity']+']">' +
                        '<input type="hidden" class="cuenta'+dato['codCuenta']+'" codigo="'+dato['idCuenta']+'" id="idComm'+dato['pk_num_commodity']+'cuenta" >'+
                        '<input type="hidden" class="partida'+dato['codCuenta']+'" codigo="'+dato['idPartida']+'" id="idComm'+dato['pk_num_commodity']+'partida">'+
                        '<input type="hidden" id="secidComm{$tr}" name="form[int][sec]" value="{$tr}">'+
                        '<input type="hidden" value="'+dato['idUnidad']+'" name="form[int][pk_num_unidad]['+dato['pk_num_commodity']+']">' +
                        '<td style="vertical-align: middle;">{$tr}</td>' +
                        '<td><input type="text" class="form-control" value=1 name="form[int][canti]['+dato['pk_num_commodity']+']" size="2"></td>' +
                        '<td><input type="text" class="form-control" readonly value="'+dato['ind_descripcion']+'" id="comentarioI{$tr}" name="form[alphaNum][comentarioI]['+dato['pk_num_commodity']+']" size="8"></td>' +
                        '<td><input type="text" class="form-control"  value="" name="form[alphaNum][especificacionTecnica]['+dato['pk_num_commodity']+']"></td>' +
                        '<td><input type="text" class="form-control" readonly value="'+dato['unidad']+'"></td>' +
                        '<td><input type="text" class="form-control" disabled id="estado{$tr}" size="2" value="PR"></td>' +
                        '<td><input type="text" class="form-control" readonly id="numCC'+dato['pk_num_commodity']+'" name="form[int][numCC]['+dato['pk_num_commodity']+']" value=""></td>'+
                        '<td style="vertical-align: middle;">' +
                        '<button class="eliminar btn ink-reaction btn-raised btn-xs btn-danger" id="idComm'+dato['pk_num_commodity']+'">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                        '</tr>');
                    valor = 100;
                    $('#cantidad').val(parseInt($('#cantidad').val())+parseInt(1));
                }
                {/if}
                {if $id == 'commodity3'}
                if($('#comm'+dato['pk_num_commodity']).length){
                    swal("Error!", 'Commoditie ya seleccionado' , "error");
                } else {
                    correcto = 1;
                    $(document.getElementById('contenidoTabla')).append(
                        '<tr id="item'+dato['pk_num_commodity']+'" class="ic" idIC="'+dato['pk_num_commodity']+'">'+
                        '<input type="hidden" name="form[int][sec]" value="{$tr}">'+
                        '<input type="hidden" name="form[int][unidad]['+dato['pk_num_commodity']+']" value="'+dato['idUnidad']+'">'+
                        '<input type="hidden" name="form[int][cuentaDetalle]['+dato['pk_num_commodity']+']" id="cuenta'+dato['pk_num_commodity']+'" value="'+dato['idCuenta']+'">'+
                        '<input type="hidden" name="form[int][partidaDetalle]['+dato['pk_num_commodity']+']" id="partida'+dato['pk_num_commodity']+'" value="'+dato['idPartida']+'">'+
                        '<input type="hidden" name="form[txt][tipo]" value="comm">'+
                        '<td width="10px" style="vertical-align: middle;">{$tr}</td>' +
                        '<td width="10px"><input type="text" class="form-control" value="'+dato['pk_num_commodity']+'" name="form[int][num]['+dato['pk_num_commodity']+']" size="4"></td>' +
                        '<td width="10px"><input readonly type="text" class="form-control" value="'+dato['ind_descripcion']+'" size="6"></td>' +
                        '<td width="10px"><input type="text" class="form-control nuevoValor" value="1" name="form[int][cant]['+dato['pk_num_commodity']+']" id="cantidad'+dato['pk_num_commodity']+'" sec="'+dato['pk_num_commodity']+'" size="6"></td>' +
                        '<td width="10px"><input type="text" class="form-control nuevoValor" value="0" name="form[int][precio]['+dato['pk_num_commodity']+']" id="precio'+dato['pk_num_commodity']+'" sec="'+dato['pk_num_commodity']+'" size="4"></td>' +
                        '<td width="10px"><input type="text" class="form-control nuevoValor" value="0" name="form[int][descPorc]['+dato['pk_num_commodity']+']" id="descPorc'+dato['pk_num_commodity']+'" sec="'+dato['pk_num_commodity']+'"></td>' +
                        '<td width="10px"><input type="text" class="form-control nuevoValor" value="0" name="form[int][descFijo]['+dato['pk_num_commodity']+']" id="descFijo'+dato['pk_num_commodity']+'" sec="'+dato['pk_num_commodity']+'"></td>' +
                        '<td width="10px" style="vertical-align: middle;">' +
                        '<div class="checkbox checkbox-styled">' +
                        '<label class="checkbox-inline checkbox-styled">' +
                        '<input type="checkbox" class="form-control nuevoValor" name="form[int][exonerado]['+dato['pk_num_commodity']+']" id="exonerado' + dato['pk_num_commodity'] + '" idItem="' + dato['pk_num_commodity'] + '" sec="'+dato['pk_num_commodity']+'" value="1">'+
                        '<span></span> ' +
                        '</label>'+
                        '</div>'+
                        '</td>' +
                        '<td width="10px"><input type="text" class="form-control" readonly value="0" name="form[int][montoPU]['+dato['pk_num_commodity']+']" id="montoPU'+dato['pk_num_commodity']+'"></td>' +
                        '<td width="10px"><input type="text" class="form-control total" readonly value="0" name="form[int][total]['+dato['pk_num_commodity']+']" idItem="' + dato['pk_num_commodity'] + '" id="total' + dato['pk_num_commodity'] + '"></td>' +
                        '<td style="vertical-align: middle;">' +
                        '<button class="eliminar btn ink-reaction btn-raised btn-xs btn-danger" id="' + dato['pk_num_commodity'] + '">' +
                        '<i class="md md-delete" style="color: #ffffff;"></i></button></td>' +
                        '</tr>');
                    valor = 0;
                }
                {/if}
                {if $id == 'commodity4'}
                $(document.getElementById('numComm')).val(dato['pk_num_commodity']);
                $(document.getElementById('nombreComm')).val(dato['ind_descripcion']);
                {/if}



                if(correcto ==1){
                    if($('#idCuenta'+dato['idCuenta']+'Error').length){
                        $('#catidadCuentaIC'+dato['idCuenta']).val(parseFloat($('#catidadCuentaIC'+dato['idCuenta']).val())+parseFloat(1));
                    } else {
                        if(dato['idCuenta']!=null){
                            $('#cuenta #contable').append(
                                '<tr id="idCuenta'+dato['idCuenta']+'Error">' +
                                '<td style="width:20%"><input type="hidden" class="form-control catidadCuentaIC" idCuenta="'+dato['idCuenta']+'" id="catidadCuentaIC'+dato['idCuenta']+'" value="1">' +
                                '<input type="text" class="form-control" disabled value="'+dato['codCuenta']+'">'+
                                '<td style="width:60%"><input type="text" class="form-control" disabled value="'+dato['descCuenta']+'"></td>'+
                                '<td style="width:20%"><input type="text" class="form-control porcentajeCuenta" disabled value="" id="porcentajeCuenta'+dato['idCuenta']+'"></td>'+
                                '</tr>');
                        }
                    }

                    if($('#idCuenta2'+dato['idCuenta2']+'Error').length){
                        $('#catidadCuentaIC2'+dato['idCuenta2']).val(parseFloat($('#catidadCuentaIC2'+dato['idCuenta2']).val())+parseFloat(1));
                    } else {
                        if(dato['idCuenta2']!=null){
                            $('#cuenta2 #contable2').append(
                                '<tr id="idCuenta2'+dato['idCuenta2']+'Error">' +
                                '<td style="width:20%"><input type="hidden" class="form-control catidadCuentaIC2" idCuenta="'+dato['idCuenta2']+'" id="catidadCuentaIC2'+dato['idCuenta2']+'" value="1">' +
                                '<input type="text" class="form-control" disabled value="'+dato['codCuenta2']+'">'+
                                '<td style="width:60%"><input type="text" class="form-control" disabled value="'+dato['descCuenta2']+'"></td>'+
                                '<td style="width:20%"><input type="text" class="form-control porcentajeCuenta2" disabled value="" id="porcentajeCuenta2'+dato['idCuenta2']+'"></td>'+
                                '</tr>');
                        }
                    }

                    if($('#idPartida'+dato['idPartida']+'Error').length){
                        $('#catidadPartidaIC'+dato['idPartida']).val(parseFloat($('#catidadPartidaIC'+dato['idPartida']).val())+parseFloat(1));
                    } else {
                        if(dato['idPartida']!=null){
                            $('#partida #presupuesto').append(
                                '<tr id="idPartida'+dato['idPartida']+'Error">' +
                                '<td style="width:20%"><input type="hidden" class="form-control catidadPartidaIC" idPartida="'+dato['idPartida']+'" id="catidadPartidaIC'+dato['idPartida']+'" value="1">' +
                                '<input type="text" class="form-control" disabled value="'+dato['codPartida']+'">'+
                                '<td style="width:60%"><input type="text" class="form-control" disabled value="'+dato['descPartida']+'"></td>'+
                                '<td style="width:20%"><input type="text" class="form-control porcentajePartida" readonly name="form[int][montoPartida]['+dato['idPartida']+']" value="" id="porcentajePartida'+dato['idPartida']+'"></td>'+
                                '</tr>');
                        }
                    }

                    $('.catidadPartidaIC').each(function ( titulo, valo ) {
                        var value = ($(this).val()*100/{$tr});
                        var idPartida = $(this).attr('idPartida');
                        if(valor==100){
                            $('#porcentajePartida'+idPartida).val(value);
                        } else {
                            $('#porcentajePartida'+idPartida).val(0);
                        }
                    });

                    $('.catidadCuentaIC').each(function ( titulo, valo ) {
                        var value = ($(this).val()*100/{$tr});
                        var idCuenta = $(this).attr('idCuenta');
                        if(valor==100){
                            $('#porcentajeCuenta'+idCuenta).val(value);
                        } else {
                            $('#porcentajeCuenta'+idCuenta).val(0);
                        }
                    });

                    $('.catidadCuentaIC2').each(function ( titulo, valo ) {
                        var value = ($(this).val()*100/{$tr});
                        var idCuenta = $(this).attr('idCuenta');
                        if(valor==100){
                            $('#porcentajeCuenta2'+idCuenta).val(value);
                        } else {
                            $('#porcentajeCuenta2'+idCuenta).val(0);
                        }
                    });
                }
            },'json');

            $('#cerrarModal2').click();
            $('#contenidoModal2').html('');

        });

    });
</script>