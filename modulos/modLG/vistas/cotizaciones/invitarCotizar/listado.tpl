<section class="modal-body style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Invitar/Cotizar Proveedores</h2>
    </div>

    <div class="section-body contain-lg">
        <div class="row">
            <div id="rootwizard2" class="form-wizard form-wizard-horizontal">
                <div class="form floating-label form-validation" role="form" id="form">
                    <div class="form-wizard-nav">
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary"></div>
                        </div>
                        <ul class="nav nav-justified">
                            <li class="active">
                                <a href="#tab5" data-toggle="tab"><span class="step">1</span> <span class="title" style="font-weight:bold;">ITEMS</span> </a></li>
                            <li>
                                <a href="#tab6" data-toggle="tab"><span class="step">2</span> <span class="title" style="font-weight:bold;">COMMODITIES</span> </a></li>
                        </ul>
                    </div>
                    <div class="tab-content clearfix">
                        <div class="tab-pane active" id="tab5">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="dataTablaJson1" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Código Acta</th>
                                            <th>#</th>
                                            <th>Item</th>
                                            <th>Descripcion</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab6">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="dataTablaJson2" class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Código Acta</th>
                                            <th>#</th>
                                            <th>Commoditie</th>
                                            <th>Descripcion</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div><!--END ClearFix-->
                    <ul class="pager wizard">
                        <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                        <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                    </ul>
                </div>
                <div>
                    {if in_array('LG-01-01-03-01-01-I',$_Parametros.perfil)}
                        <button class="boton_ic logsUsuario btn ink-reaction btn-raised btn-info" disabled
                                descipcion="El Usuario ha Invitado Proveedores. "
                                data-toggle="modal" data-target="#formModal"
                                url="crearInvitacionMET" titulo="Invitar Proveedores" id="invitar">
                            <i class="md md-insert-invitation"></i> Invitar Proveedores
                        </button>
                    {/if}
                    {if in_array('LG-01-01-03-01-02-C',$_Parametros.perfil)}
                        <button class="boton_ic logsUsuario btn ink-reaction btn-raised btn-primary" disabled
                                descipcion="El Usuario ha Cotizado la Invitacion Nro. "
                                data-toggle="modal" data-target="#formModal"
                                url="cotizarMET" titulo="Cotizaciones de los Proveedores Invitados" id="cotizar">
                            <i class="md md-attach-money"></i> Cotizar
                        </button>
                    {/if}
                    {if in_array('LG-01-01-03-01-03-C',$_Parametros.perfil)}
                        <button class='logsUsuario btn ink-reaction btn-raised btn-warning' disabled
                                titulo='Cuadro Comparativo' id="cuadro">
                            <i class='md md-compare'></i> Cuadro Comparativo
                        </button>
                    {/if}
                    {if in_array('LG-01-01-03-01-04-P',$_Parametros.perfil)}
                        <button class='logsUsuario btn ink-reaction btn-raised btn-primary-bright' disabled
                                titulo='Pliego de Condiciones' id="pliego">
                            <i class='md md-compare'></i> Pliego de Condiciones
                        </button>
                    {/if}
                </div>
            </div>
        </div>
    </div>

</section>
<form id="datos_ic"></form>

<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();

        var eventFired = function ( type ) {
            $('#cotizar').attr('disabled',true);
            $('#cuadro').attr('disabled',true);
            $('#pliego').attr('disabled',true);
            $('#invitar').attr('disabled',true);
            $('#datos_ic').html('');
        }

        var dt = app.dataTable(
                '#dataTablaJson1',
                "{$_Parametros.url}modLG/cotizaciones/invitarCotizarCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_acta_detalle", width:50 },
                    { "data": "ind_codigo_acta", width:150 },
                    { "data": "num_secuencia", width:50 },
                    { "data": "ind_codigo_interno", width:80 },
                    { "data": "ind_descripcion" }
                ]
        )
                .on( 'order.dt',  function () { eventFired( 'Order' ); } )
                .on( 'search.dt', function () { eventFired( 'Search' ); } )
                .on( 'page.dt',   function () { eventFired( 'Page' ); } )
                .on( 'length.dt',   function () { eventFired( 'lengthMenu' ); } );

        var dt2 = app.dataTable(
                '#dataTablaJson2',
                "{$_Parametros.url}modLG/cotizaciones/invitarCotizarCONTROL/jsonDataTabla1MET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_acta_detalle", width:50 },
                    { "data": "ind_codigo_acta", width:150 },
                    { "data": "num_secuencia", width:50 },
                    { "data": "ind_cod_commodity", width:80 },
                    { "data": "ind_descripcion" }
                ]
        )
                .on( 'order.dt',  function () { eventFired( 'Order' ); } )
                .on( 'search.dt', function () { eventFired( 'Search' ); } )
                .on( 'page.dt',   function () { eventFired( 'Page' ); } )
                .on( 'length.dt',   function () { eventFired( 'lengthMenu' ); } );


        $('.boton_ic').on( 'click', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            var url = '{$_Parametros.url}modLG/cotizaciones/invitarCotizarCONTROL/'+$(this).attr('url');
            $.post(url, $( "#datos_ic" ).serialize() , function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#cuadro').on( 'click', function () {
            if($( ".dataTablaJson1" ).val()){
                var idDetalle = $( ".dataTablaJson1" ).val();
            } else {
                idDetalle = $( ".dataTablaJson2" ).val();
            }
            window.open('{$_Parametros.url}modLG/cotizaciones/invitarCotizarCONTROL/cuadroMET/'+idDetalle);
        });
        $('#pliego').on( 'click', function () {
            var url = '{$_Parametros.url}modLG/cotizaciones/invitarCotizarCONTROL/generarPliegoOdtMET';
            $.post(url, $( "#datos_ic" ).serialize(), function (dato) {
                window.open(dato['ruta']);
            }, 'json');
        });
        function seleccionTr(celdas,id) {

            var cantidad = 0;
            var style = '';
            var idTr = '';
            var c = 0;
            var valor1 = 0;

            celdas.each(function(){
                c = c+1;
                if(c==1){
                    valor1 = $(this).html();
                    style = $(this).parent().attr('style');
                    idTr = 'ic'+valor1;
                    $(this).parent().attr('id',idTr);
                }
            });

            if(valor1!='No Hay Registros Disponibles'){

                if(id=='dataTablaJson1'){
                    $('#dataTablaJson2 .cambio').attr('style','');
                    $('#dataTablaJson2 .cambio').attr('class','');
                    $('#dataTablaJson2 .cambio').attr('valor1','');
                    $('.dataTablaJson2').remove();
                } else {
                    $('#dataTablaJson1 .cambio').attr('style','');
                    $('#dataTablaJson1 .cambio').attr('class','');
                    $('#dataTablaJson1 .cambio').attr('valor1','');
                    $('.dataTablaJson1').remove();
                }

                if(style == null || style == ''){
                    $('#'+idTr).attr('style','background-color:LightGreen;');
                    $('#'+idTr).attr('class','cambio');
                    $('#'+idTr).attr('valor1',valor1);
                    $('#datos_ic').append('<input type="hidden" name="datos_ic[]" class="'+id+'" value="'+valor1+'" id="datos_ic'+valor1+'">');
                } else {
                    $('#'+idTr).attr('style',null);
                    $('#'+idTr).attr('class',null);
                    $('#'+idTr).attr('valor1',null);
                    $('#datos_ic'+valor1).remove();
                }

                $('#'+id+' .cambio').each(function(){
                    cantidad = cantidad+1;
                });


                if(cantidad==0){
                    $('#invitar').attr('disabled',true);
                } else {
                    $('#invitar').attr('disabled',false);
                }
                if(cantidad==1){
                    $('#cotizar').attr('disabled',false);
                    $('#cuadro').attr('disabled',false);
                    $('#pliego').attr('disabled',false);
                } else {
                    $('#cotizar').attr('disabled',true);
                    $('#cuadro').attr('disabled',true);
                    $('#pliego').attr('disabled',true);
                }
            }

        }

        $("#dataTablaJson1 tbody").on('click', 'tr', function() {
            var celdas = $(this).find('td');
            seleccionTr(celdas,'dataTablaJson1');
        });

        $("#dataTablaJson2 tbody").on('click', 'tr', function() {
            var celdas = $(this).find('td');
            seleccionTr(celdas,'dataTablaJson2');
        });
    });
</script>