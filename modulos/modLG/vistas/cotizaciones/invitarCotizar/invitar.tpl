<form action="{$_Parametros.url}modLG/cotizaciones/invitarCotizarCONTROL/crearInvitacionMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($cantidad)}{$cantidad}{/if}" name="cantidad"/>
    <div class="modal-body">
        <div class="row">
            <div class="panel-group floating-label" id="accordion1">
                <div class="card panel">
                    <div class="card-head style-primary collapsed" data-toggle="collapse" data-parent="#accordion2" data-target="#accordion1-1">
                        <header>Información General</header>
                        <div class="tools">
                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                        </div>
                    </div>
                    <div id="accordion1-1" class="collapse">
                        <div class="card-body">
                            <div class="form-group floating-label col-lg-6" id="condicionError">
                                <textarea name="form[alphaNum][condicion]" class="form-control" id="condicion" cols="40" rows="2" {if isset($ver) and $ver==1} disabled {/if}></textarea>
                                <label for="condicion"><i class="md md-border-color"></i>Condiciones de Entrega</label>
                            </div>
                            <div class="form-group floating-label col-lg-6" id="observError">
                                <textarea name="form[alphaNum][observ]" class="form-control" id="observ" cols="40" rows="2" {if isset($ver) and $ver==1} disabled {/if}></textarea>
                                <label for="observ"><i class="md md-border-color"></i>Observaciones</label>
                            </div>
                            <div class="form-group floating-label col-lg-6" id="flimError">
                                <input type="text" readonly id="flim" name="form[txt][flim]" class="form-control" value="">
                                <label for="flim">Fecha Limite para Cotizar<i class="fa fa-calendar"></i></label>
                            </div>
                            <div class="form-group floating-label col-lg-6" id="diasError">
                                <input type="text" id="dias" name="form[int][dias]" class="form-control" value="">
                                <label for="dias">Días de Entrega<i class="fa fa-calendar"></i></label>
                            </div>
                        </div><!--END card-body-->
                    </div><!--END accordion1-1-->

                </div><!--END card-panel-->
            </div><!--END panel acordion1-->
            <div class="panel-group floating-label" id="accordion2">
                <div class="card panel">
                    <div class="card-head style-primary collapsed" data-toggle="collapse" data-parent="#accordion1" data-target="#accordion2-1">
                        <header>Proveedores</header>
                        <div class="tools">
                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                        </div>
                    </div>
                    <div id="accordion2-1" class="collapse">
                        <div class="card-body">
                            <div class="col-lg-6">
                                <div class="form-group floating-label" id="nuevoProvError">
                                    <label>Buscar Proveedor<i class="md md-people"></i></label>
                                    <button {if (isset($ver) and $ver==1)} disabled {/if}
                                            class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                            type="button"
                                            title="Buscar Proveedor"
                                            data-toggle="modal" data-target="#formModal2"
                                            data-keyboard="false" data-backdrop="static"
                                            titulo="Buscar Proveedor"
                                            url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/proveedorMET/proveedor1/"
                                            >
                                        <i class="md md-search" style="color: #ffffff;"></i>
                                    </button>
                                </div>
                            </div>
                            <table class="table table-striped no-margin" id="contenidoTabla">
                                <thead>
                                <tr>
                                    <th width="10px">Sec.</th>
                                    <th width="430px">Descripcion</th>
                                    <th>Forma de Pago</th>
                                    <th>Borrar</th>
                                </tr>
                                </thead>
                                <tbody id="proveedor">
                                </tbody>
                            </table>

                        </div><!--END card-body-->
                    </div><!--END acordion2-1-->
                </div><!--END card-panel-->
            </div><!--END panel acordion2-->
            <div class="panel-group floating-label" id="accordion3">
                <div class="card panel">
                    <div class="card-head style-primary collapsed" data-toggle="collapse" data-parent="#accordion4" data-target="#accordion3-1">
                        <header>Requerimientos</header>
                        <div class="tools">
                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                        </div>
                    </div>
                    <div id="accordion3-1" class="collapse">
                        <div class="card-body">

                            <table class="table table-striped no-margin">
                                <thead>
                                <tr>
                                    <th width="10px">Sec.</th>
                                    <th width="430px">Descripcion</th>
                                    <th>Unidad</th>
                                </tr>
                                </thead>
                                <tbody id="requerimie">
                                {$c=0}
                                {foreach item=r from=$requerimientos}
                                    {$c=$c+1}
                                    <tr>
                                        <input type="hidden" readonly name="datos_ic[]" value="{$r.pk_num_acta_detalle}">
                                        <td style="vertical-align: middle;">{$c}</td>
                                        <td>
                                            <input type="text" readonly value="{$r.ind_descripcion}" class="form-control">
                                        </td>
                                        <td>
                                            <input type="text" readonly value="{$r.unidad}" class="form-control">
                                        </td>
                                    </tr>
                                {/foreach}
                                </tbody>
                            </table>

                        </div><!--END card-body-->
                    </div><!--END accordion3-1-->

                </div><!--END card-panel-->
            </div><!--END panel acordion3-->
            <div class="panel-group floating-label" id="accordion4">
                <div class="card panel">
                    <div class="card-head style-primary collapsed" data-toggle="collapse" data-parent="#accordion3" data-target="#accordion4-1">
                        <header>Invitaciones ya Realizadas</header>
                        <div class="tools">
                            <a class="btn btn-icon-toggle"><i class="fa fa-angle-down"></i></a>
                        </div>
                    </div>
                    <div id="accordion4-1" class="collapse">
                        <div class="card-body">
                            <table class="table table-striped no-margin" id="contenidoTabla2">
                                <thead>
                                <tr>
                                    <th width="10px">Sec.</th>
                                    <th width="430px">Proveedor</th>
                                    <th>Fecha Invitación</th>
                                    <th>Estado</th>
                                </tr>
                                </thead>
                                <tbody id="invitaci">

                                {foreach item=r from=$requerimientos}
                                    <tr>
                                    <td style="vertical-align: middle; background-color: #0aa49a; text-align: center; color: #ffffff" colspan="4">{$r.ind_descripcion}</td>
                                    </tr>
                                    {$c=0}
                                    {foreach item=i from=$invitaciones}
                                        {if $i.fk_lgb023_num_acta_detalle==$r.pk_num_acta_detalle}
                                            {$c=$c+1}
                                            <tr>
                                                <td style="vertical-align: middle;">{$c}</td>
                                                <td>
                                                    <input type="text" readonly value="{$i.nombre}" class="form-control">
                                                </td>
                                                <td>
                                                    <input type="text" readonly value="{$i.fec_invitacion}" class="form-control">
                                                </td>
                                                <td>
                                                    <input type="text" readonly value="{$i.estadoInvi}" class="form-control">
                                                </td>
                                            </tr>
                                        {/if}
                                    {/foreach}
                                {/foreach}
                                </tbody>
                            </table>
                        </div><!--END card-body-->
                    </div><!--END accordion4-1-->

                </div><!--END card-panel-->
            </div><!--END panel acordion4->

            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="glyphicon glyphicon-floppy-disk"></span>  Guardar
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "80%");
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('#flim').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#modalAncho').css("width", "60%");

        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "50%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0, tr: $("#contenidoTabla > tbody > tr").length+1 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    swal("Registro Guardado!", 'La Invitacion fue modificada satisfactoriamente.', "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='errorEstado'){
                    app.metValidarError(dato,'Disculpa. No se puede Invitar para un procedimiento Completado o Anulado');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    swal("Registro Guardado!", 'La Invitacion fue guardada satisfactoriamente.', "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            }, 'json');
        });

        $('#proveedor').on('click', '.eliminar' ,function () {
            $('.'+$(this).attr('id')).remove();
        });
    });
</script>