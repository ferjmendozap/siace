<div class="form form-validate floating-label" novalidate="novalidate">
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="form-group floating-label col-lg-12" id="observacionError">
                        <input type="hidden" value="{if isset($id)}{$id}{/if}" id="idObservacion">
                        <textarea id="observacion2" class="form-control" cols="50" rows="2"></textarea>
                        <label for="observacion"><i class="md md-border-color"></i>Explique por qué no seleccionó el mejor Precio</label>
                    </div>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion2">
            <span class="glyphicon glyphicon-floppy-disk"></span> Aceptar
        </button>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new  AppFunciones();

        $('#accion2').click(function(){
            var dato = [];
            var id = $('#idObservacion').val();
            var observacion = $('#observacion2').val();

            if(observacion==''){
                dato['observacion'] = 'error';
                app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
            }else{
                $('#'+id).val(observacion);
                $('#cerrarModal2').click();
                $('#ContenidoModal2').html('');
            }
        });
    });
</script>