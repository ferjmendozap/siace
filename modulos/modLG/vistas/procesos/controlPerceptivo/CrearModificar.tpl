<form action="{$_Parametros.url}modLG/procesos/controlPerceptivoCONTROL/crearModificarConPerMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idConPer)}{$idConPer}{/if}" name="idConPer"/>
    <input type="hidden" value="{if isset($idOrden)}{$idOrden}{/if}" name="idOrden"/>
    <input type="hidden" value="{if isset($ver)}{$ver}{else}0{/if}" name="ver"/>
    {$disabled=""}
    {if isset($ver) and $ver==1}
        {$disabled="disabled"}
    {/if}
    {if isset($idConPer)}
        {$disabled2="disabled"}
    {/if}
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-sm-12">
                    <div class="col-sm-11">
                        <div class="form-group col-sm-2">
                            <i class="md md-people"></i>Firmante 1:
                        </div>
                        <div class="form-group col-sm-3" id="tipoFirmanteAError">
                            <select name="form[int][tipoFirmanteA]" class="form-control select2 select2-list" {$disabled}>
                                <option value=""></option>
                                {foreach item=tf from=$tipoFirmantes}
                                    {if $tf.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_tipo_firmante_a}
                                        <option value="{$tf.pk_num_miscelaneo_detalle}" selected>{$tf.ind_nombre_detalle}</option>
                                    {else}
                                        <option value="{$tf.pk_num_miscelaneo_detalle}">{$tf.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="tipoFirmanteA">Tipo </label>
                        </div>
                        <div class="form-group col-sm-7" id="asisAError">
                            <input type="hidden" class="form-control" name="form[int][asisA]"
                                   value="{if isset($formDB.fk_rhb001_num_empleado_conforme_a)}{$formDB.fk_rhb001_num_empleado_conforme_a}{/if}"
                                   id="idEmpleado">
                            <input type="text" class="form-control disabled" id="personaResp" value="{if isset($AsisA.ind_nombre1)}{$AsisA.ind_nombre1} {$AsisA.ind_apellido1}{/if}" disabled>
                        </div>
                    </div>
                    <div class="form-group col-sm-1">
                        <button {$disabled}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button"
                                title="Buscar Empleado"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Empleado"
                                url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona1/"
                                >
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-11">
                        <div class="form-group col-sm-2">
                            <i class="md md-people"></i>Firmante 2:
                        </div>
                        <div class="form-group col-sm-3" id="tipoFirmanteBError">
                            <select name="form[int][tipoFirmanteB]" class="form-control select2 select2-list" {$disabled}>
                                <option value=""></option>
                                {foreach item=tf from=$tipoFirmantes}
                                    {if $tf.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_tipo_firmante_b}
                                        <option value="{$tf.pk_num_miscelaneo_detalle}" selected>{$tf.ind_nombre_detalle}</option>
                                    {else}
                                        <option value="{$tf.pk_num_miscelaneo_detalle}">{$tf.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="tipoFirmanteB">Tipo </label>
                        </div>
                        <div class="form-group col-sm-7" id="asisBError">
                            <input type="hidden" class="form-control" name="form[int][asisB]"
                                   value="{if isset($formDB.fk_rhb001_num_empleado_conforme_b)}{$formDB.fk_rhb001_num_empleado_conforme_b}{/if}"
                                   id="idEmpleado2">
                            <input type="text" class="form-control disabled" id="personaResp2" value="{if isset($AsisB.ind_nombre1)}{$AsisB.ind_nombre1} {$AsisB.ind_apellido1}{/if}" disabled>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <button {$disabled}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button"
                                title="Buscar Empleado"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Empleado"
                                url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona2/">
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-11">
                        <div class="form-group col-sm-2">
                            <i class="md md-people"></i>Firmante 3:
                        </div>
                        <div class="form-group col-sm-3" id="tipoFirmanteCError">
                            <select name="form[int][tipoFirmanteC]" class="form-control select2 select2-list" {$disabled}>
                                <option value=""></option>
                                {foreach item=tf from=$tipoFirmantes}
                                    {if $tf.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_tipo_firmante_c}
                                        <option value="{$tf.pk_num_miscelaneo_detalle}" selected>{$tf.ind_nombre_detalle}</option>
                                    {else}
                                        <option value="{$tf.pk_num_miscelaneo_detalle}">{$tf.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="tipoFirmanteC">Tipo </label>
                        </div>
                        <div class="form-group col-sm-7" id="asisCError">
                            <input type="hidden" class="form-control" name="form[int][asisC]"
                                   value="{if isset($formDB.fk_rhb001_num_empleado_conforme_c)}{$formDB.fk_rhb001_num_empleado_conforme_c}{/if}"
                                   id="idEmpleado3">
                            <input type="text" class="form-control disabled" id="personaResp3" value="{if isset($AsisC.ind_nombre1)}{$AsisC.ind_nombre1} {$AsisC.ind_apellido1}{/if}" disabled>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <button {$disabled}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button"
                                title="Buscar Empleado"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Empleado"
                                url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona3/"
                                >
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-11">
                        <div class="form-group col-sm-2">
                            <i class="md md-people"></i>Firmante 4:
                        </div>
                        <div class="form-group col-sm-3" id="tipoFirmanteDError">
                            <select name="form[int][tipoFirmanteD]" class="form-control select2 select2-list" {$disabled}>
                                <option value=""></option>
                                {foreach item=tf from=$tipoFirmantes}
                                    {if $tf.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_tipo_firmante_d}
                                        <option value="{$tf.pk_num_miscelaneo_detalle}" selected>{$tf.ind_nombre_detalle}</option>
                                    {else}
                                        <option value="{$tf.pk_num_miscelaneo_detalle}">{$tf.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="tipoFirmanteD">Tipo </label>
                        </div>
                        <div class="form-group col-sm-7" id="asisDError">
                            <input type="hidden" class="form-control" name="form[int][asisD]"
                                   value="{if isset($formDB.fk_rhb001_num_empleado_conforme_d)}{$formDB.fk_rhb001_num_empleado_conforme_d}{/if}"
                                   id="idEmpleado4">
                            <input type="text" class="form-control disabled" id="personaResp4" value="{if isset($AsisD.ind_nombre1)}{$AsisD.ind_nombre1} {$AsisD.ind_apellido1}{/if}" disabled>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <button {$disabled}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button"
                                title="Buscar Empleado"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Empleado"
                                url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona4/"
                        >
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="col-sm-11">
                        <div class="form-group col-sm-2">
                            <i class="md md-people"></i>Firmante 5:
                        </div>
                        <div class="form-group col-sm-3" id="tipoFirmanteEError">
                            <select name="form[int][tipoFirmanteE]" class="form-control select2 select2-list" {$disabled}>
                                <option value=""></option>
                                {foreach item=tf from=$tipoFirmantes}
                                    {if $tf.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_tipo_firmante_e}
                                        <option value="{$tf.pk_num_miscelaneo_detalle}" selected>{$tf.ind_nombre_detalle}</option>
                                    {else}
                                        <option value="{$tf.pk_num_miscelaneo_detalle}">{$tf.ind_nombre_detalle}</option>
                                    {/if}
                                {/foreach}
                            </select>
                            <label for="tipoFirmanteE">Tipo </label>
                        </div>
                        <div class="form-group col-sm-7" id="asisEError">
                            <input type="hidden" class="form-control" name="form[int][asisE]"
                                   value="{if isset($formDB.fk_rhb001_num_empleado_conforme_e)}{$formDB.fk_rhb001_num_empleado_conforme_e}{/if}"
                                   id="idEmpleado5">
                            <input type="text" class="form-control disabled" id="personaResp5" value="{if isset($AsisE.ind_nombre1)}{$AsisE.ind_nombre1} {$AsisE.ind_apellido1}{/if}" disabled>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <button {$disabled}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button"
                                title="Buscar Empleado"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Empleado"
                                url="{$_Parametros.url}modLG/maestros/almacenesCONTROL/empleadoMET/persona9/"
                        >
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>
                <div class="col-sm-12">
                    {if isset($idConPer)}
                        <div class="col-sm-12">
                            <label class="checkbox-inline checkbox-styled">
                                <span>Cerrar Control Perceptivo </span>
                                <input type="checkbox" value="2" name="form[int][cerrar]" {$disabled} {if isset($formDB.num_estatus) AND $formDB.num_estatus==0} checked {/if}>
                            </label>
                        </div>
                    {/if}
                </div>

                <table class="table table-striped no-margin" id="contenidoTabla">
                    <thead>
                    <tr>
                        <th width="10px">Cantidad Pedida</th>
                        <th width="250px">Ítem</th>
                        <th width="10px">Cantidad {if isset($idConPer)}Recibida{else}Por Recibir{/if}</th>
                        <th width="10px">Observación</th>
                    </tr>
                    </thead>
                    <tbody id="item">

                    {$cont=0}
                    {foreach item=o from=$orden}
                        {$cont=$cont+1}

                        {if $o.fk_lgb002_num_item!=null}
                            {$descripcion=$o.ind_descripcion_item}
                        {else}
                            {$descripcion=$o.ind_descripcion_commodity}
                        {/if}

                        {$cantidadRecibida = 0}
                        {$cantidadPercibida = 0}
                        {$idDetalle = 0}

                        {foreach item=p from=$percepDet}
                            {if $o.pk_num_orden_detalle==$p.fk_lgc009_num_orden_detalle}
                                {$cantidadRecibida = $cantidadRecibida+$p.cantidadRecibida}
                            {/if}
                        {/foreach}

                        {$cantidadPendiente = $o.num_cantidad-$cantidadRecibida}
                        {$comentario = ''}
                        {if isset($idConPer)}
                            {foreach item=p2 from=$percepDet2}
                                {if $o.pk_num_orden_detalle==$p2.fk_lgc009_num_orden_detalle}
                                    {$cantidadPercibida = $p2.cantidadRecibida}
                                    {$comentario = $p2.ind_observacion}
                                {/if}
                            {/foreach}
                            {$cantidadPendiente = $cantidadPercibida}
                        {/if}
                        <input type="hidden" name="form[int][secuencia][{$cont}]" value="{$o.num_secuencia}">
                        <input type="hidden" name="form[int][precio][{$cont}]" value="{$o.num_precio_unitario_iva}">
                        <input type="hidden" name="form[int][idDetalle][{$cont}]" value="{$idDetalle}">
                        <input type="hidden" name="form[int][idDetalleOrden][{$cont}]" value="{$o.pk_num_orden_detalle}">
                        <input type="hidden" name="form[int][cantidadRecibida][{$cont}]" value="{$cantidadRecibida}">
                        <input type="hidden" id="cantidadPendiente{$cont}" value="{$cantidadPendiente}">
                        <tr class="item{$cont}">
                            <td width="10px"><input type="text" readonly class="form-control" name="form[int][cantPedida][{$cont}]" size="2" id="cantPedida{$cont}" value="{$o.num_cantidad}"></td>
                            <td width="10px"><input type="text" readonly class="form-control" value="{$descripcion}"></td>
                            <td width="10px"><input type="text" {$disabled2} class="form-control cantidadRecibida" name="form[int][cantRecibida][{$cont}]" id="cantRecibida{$cont}" cont="{$cont}" value="{$cantidadPendiente}" size="2"></td>
                            <td width="10px"><input type="text" {$disabled2} class="form-control" name="form[alphaNum][comentario][{$cont}]" value="{$comentario}"></td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
                <input type="hidden" name="form[int][cantidad]" id="cantidad" value="{$cont}">
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {elseif $disabled!='disabled'}
            <button {$disabled} type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idConPer)}
                    <span class="fa fa-edit"></span>
                    Modificar
                {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('.select2').select2({ allowClear: true });
    function validarCampo(valor,cont) {
        if(valor==''){
            $('#cantRecibida'+cont).val($('#cantidadPendiente'+cont).val());
        } else if(valor=='0.00') {
            $('#cantRecibida'+cont).val('');
        }
    }

    $("#formAjax").submit(function(){
        return false;
    });

    $(document).ready(function () {

        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0, tr:$("#contenidoTabla > tbody > tr").length+1 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        var app = new  AppFunciones();
        $('#modalAncho').css("width", "60%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Control Perceptivo fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. No se pudo guardar o modificar el Control \nPerceptivo, verifique los campos en rojo');
                }else if(dato['status']=='errorCantidad'){
                    app.metValidarError(dato,'Disculpa. La cantidad Recibida no puede ser mayor que la Cantidad Pedida');
                }else if(dato['status']=='errorSQL2'){
                    app.metValidarError(dato,'Disculpa. Hubo un error verifique los datos introducidos');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Control Perceptivo fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

        $('.cantidadRecibida').on('blur',function(){
            var cont = $(this).attr('cont');
            var valor = $(this).val();
            validarCampo(valor,cont);
        });

        $('.cantidadRecibida').change(function(){
            var cont = $(this).attr('cont');
            var cantidadRecibida = $(this).val();
            var cantidadPedida = $('#cantidadPendiente'+cont).val();
            var cp = parseFloat(cantidadPedida);
            var cr = parseFloat(cantidadRecibida);
            if(cr>cp){
                cantidadRecibida = cantidadPedida;
                $(this).val(cantidadRecibida);
                cr = parseFloat(cantidadRecibida);
            }
            if(cp==cr){
                $('#flag'+cont).attr('checked',true);
            } else {
                $('#flag'+cont).attr('checked',false);
            }
        });
    });
</script>