<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Obligaciones</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Documento</th>
                            <th>Proveedor</th>
                            <th>Fecha Doc.</th>
                            <th>Nro. Registro</th>
                        </tr>
                        </thead>
                        <tbody>
                        {$cont = 0}
                        {foreach item=i from=$lista}
                            {$cont = $cont + 1}
                            <tr>
                                <input type="hidden" class="obligacion"
                                   idObligacion="{$i.pk_num_obligacion}"
                                   codDocumento="{$i.ind_nro_control}-{$i.ind_nro_registro}"
                                   fechaDocumento="{$i.fec_documento}">
                                <td>{$cont}</td>
                                <td>{$i.ind_nro_control}-{$i.ind_nro_registro}</td>
                                <td>{$i.proveedor}</td>
                                <td>{$i.fec_documento}</td>
                                <td>{$i.ind_nro_registro}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2').on('click', 'tbody tr', function () {
            var input = $(this).find('input');
            $(document.getElementById('idObligacion')).val(input.attr('idObligacion'));
            $(document.getElementById('codDocumento')).val(input.attr('codDocumento'));
            $(document.getElementById('fechaDocumento')).val(input.attr('fechaDocumento'));
            $('#cerrarModal2').click();
        });
    });
</script>