<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Facturación de Activos</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Commodity</th>
                                <th>Descripción</th>
                                <th>Nro. Serie</th>
                                <th>Fecha Ingreso</th>

                                <th>Modelo</th>
                                <th>Cod. Barra</th>
                                <th>Ubicación</th>
                                <th>C. C.</th>
                                <th>Nro. Placa</th>

                                <th>Marca</th>
                                <th>Color</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        {$cont = 0}
                        {$orden = ''}
                        {foreach item=post from=$lista}
                            {if $orden!=$post.cod_orden}
                                {$cont = 0}
                                {$orden = $post.cod_orden}
                                <tr style="background-color: LightGreen;">
                                    <td><b>O/C: </b></td>
                                    <td><b>{$orden}</b></td>
                                    <td>
                                        <b> {$post.proveedor}</b>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            {/if}
                            {$cont = $cont +1}
                            <tr id="idActivo{$post.pk_num_activo_fijo}">
                                <td>{$cont}</td>
                                <td>{$post.ind_cod_commodity}</td>
                                <td>{$post.descripcion}</td>
                                <td>{$post.ind_nro_serie}</td>
                                <td>{$post.fec_ingreso}</td>

                                <td>{$post.ind_modelo}</td>
                                <td>{$post.ind_cod_barra}</td>
                                <td>{$post.ubicacion}</td>
                                <td>{$post.ind_descripcion_centro_costo}</td>
                                <td>{$post.ind_nro_placa}</td>

                                <td>{$post.marca}</td>
                                <td>{$post.color}</td>
                                <td>
                                    {if in_array('LG-01-06-03-01-N',$_Parametros.perfil)}
                                        <button title="Facturar Activo" titulo="Facturación de Activo"
                                                class="nuevo logsUsuario btn ink-reaction btn-raised btn-xs btn-info"
                                                idActivo="{$post.pk_num_activo_fijo}"
                                                data-toggle="modal" data-target="#formModal" id="nuevo">
                                            <i class="icm icm-subtract"></i>
                                        </button>
                                    {/if}
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var url = '{$_Parametros.url}modLG/procesos/facturacionActivoCONTROL/crearFacturacionActivoMET';
        $('#datatable1 tbody').on( 'click', '.nuevo', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idActivo: $(this).attr('idActivo') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        /*
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAlmacen: $(this).attr('idAlmacen')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idAlmacen: $(this).attr('idAlmacen'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var idAlmacen = $(this).attr('idAlmacen');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modLG/maestros/almacenesCONTROL/eliminarAlmacenMET';
                $.post(url, { idAlmacen: idAlmacen },function(dato){
                    if(dato['status']=='ok'){
                        $('#datatable1').dataTable().api().row().remove().draw(false);
                        swal("Eliminado!", "el Almacén fue eliminado satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });
        */
    });
</script>