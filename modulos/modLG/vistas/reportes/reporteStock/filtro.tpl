<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Stock</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12">

                <div class="col-lg-3 contain-lg">
                    <div class="contain-lg col-lg-5" align="right">
                        <span>Ordenar por:</span>
                    </div>
                    <div class="col-lg-7 contain-lg">
                        <select id="orden" class="form-control select2">
                            <option value="pk_num_item">Item</option>
                            <option value="ind_descripcion">Descripcion</option>
                        </select>
                    </div>
                </div>


                <div class="contain-lg col-lg-1" align="right">
                    <span>Item:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkItem" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-1 contain-lg">
                        <button id="item" disabled class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Listado de Items"
                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/item4/">
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>

                    <div class="col-lg-8 contain-lg">
                        <div class="form-group" id="numItemError">
                            <input type="hidden" readonly name="numItem" id="numItem" class="form-control">
                            <input type="text" readonly id="nombreItem" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 contain-lg">
                    <div class="contain-lg col-lg-3" align="right">
                        <span>Tipo de Item:</span>
                    </div>
                    <div class="col-lg-1 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkTipo" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-8 contain-lg">
                        <select id="tipo" class="form-control select2" disabled>
                            <option value=""></option>
                            {foreach item=tipo from=$selectTipoItem}
                                <option value="{$tipo.pk_num_miscelaneo_detalle}">{$tipo.ind_nombre_detalle}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>


            <div class="col-lg-12">
                <div class="contain-lg col-lg-1" align="right">
                    <span>Cantidad:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkCant" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-8 contain-lg">
                        <div class="form-group" id="cantidadError">
                            <input type="text" disabled name="cantidad" id="cantidad" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" align="center">
                <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                    Buscar
                </button>
            </div>

        </div>

        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        $('#checkItem').change(function () {
            if($('#checkItem').attr('checked')=="checked" ){
                $('#item').attr('disabled',false);
            } else {
                $('#item').attr('disabled','disabled');
                $('#numItem').val('');
                $('#nombreItem').val('');
            }
        });

        //modal para ubicacion la linea
        $('#item').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#checkTipo').change(function () {
            if($('#checkTipo').attr('checked')=="checked" ){
                $('#tipo').attr('disabled',false);
            } else {
                $('#tipo').attr('disabled','disabled');
                $('#tipo').attr('value','');
            }
        });

        $('#checkCant').change(function () {
            if($('#checkCant').attr('checked')=="checked" ){
                $('#cantidad').attr('disabled',false);
            } else {
                $('#cantidad').attr('disabled','disabled');
                $('#cantidad').attr('value','');
            }
        });

        $('#accion').click(function () {
            var url2 = '{$_Parametros.url}modLG/reportes/reporteStockCONTROL/generarReporteMET/';

            var orden = $('#orden').val();

            if($('#checkItem').attr('checked')=="checked" && $('#numItem').val().length>0){
                var item = $('#numItem').val();
            } else {
                item = 'no';
            }

            if($('#checkTipo').attr('checked')=="checked" && $('#tipo').val().length>0){
                var tipo = $('#tipo').val();
            } else {
                tipo = 'no';
            }

            if($('#checkCant').attr('checked')=="checked" && $('#cantidad').val().length>0){
                var cantidad = $('#cantidad').val();
            } else {
                cantidad = 'no';
            }

            $('#respuestaPdf').html('<iframe frameborder="0" src="' + url2 +orden+'/'+item+'/'+tipo+'/'+cantidad+'" width="100%" height="540px"></iframe>');
        });

    });
</script>