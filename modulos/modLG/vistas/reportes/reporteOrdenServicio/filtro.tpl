<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Reporte de Ordenes de Servicio</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12">
                <div class="contain-lg col-lg-1" align="right">
                    <span>Proveedor:</span>
                </div>
                <div class="col-lg-5 contain-lg">
                    <div class="col-lg-1 contain-lg" align="left">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkProv" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-10 contain-lg" align="left">
                        <div class="form-group" id="numCCError">
                            <input type="hidden" readonly name="idProveedor" id="idProveedor" class="form-control">
                            <input type="text" disabled id="persona" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-1 contain-lg">
                        <button id="proveedor" disabled class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Listado de Centro de Costo"
                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/proveedorMET/proveedor/">
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>


                <!--div class="contain-lg col-lg-1" align="right">
                    <span>Clasificación:</span>
                </div>
                <div class="col-lg-2 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkClas" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-9 contain-lg">
                        <div class="form-group" id="clasificacionError">
                            <select id="clasificacion" disabled class="form-control select2">
                                <option value=""></option>
                                <option value="l">O/C Local</option>
                                <option value="f">O/C Foraneo</option>
                            </select>
                        </div>
                    </div>
                </div-->


                <div class="contain-lg col-lg-1" align="right">
                    <span>Centro de Costo:</span>
                </div>
                <div class="col-lg-5 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkCC" class="form-control">
                            <span></span>
                        </label>
                    </div>

                    <div class="col-lg-8 contain-lg">
                        <div class="form-group" id="numCCError">
                            <input type="hidden" readonly name="numCC" id="numCC" class="form-control">
                            <input type="text" disabled id="nombreCC" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-1 contain-lg">
                        <button id="cc" disabled class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Listado de Centro de Costo"
                                url="{$_Parametros.url}modLG/reportes/reporteConsumoCONTROL/centroCostoMET/">
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="contain-lg col-lg-1" align="right">
                    <span>Fecha Prep.:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-2 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkPeriodo" class="form-control"><span></span>
                        </label>
                    </div>
                    <div class="col-lg-5 contain-lg">
                        <div class="form-group" id="fdesdeError">
                            <input type="text" disabled name="fdesde" id="fdesde" value="{date('Y-m')}-01" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-5 contain-lg">
                        <div class="form-group" id="fhastaError">
                            <input type="text" disabled name="fhasta" id="fhasta" value="{date('Y-m-d')}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="contain-lg col-lg-1" align="right">
                    <span>Fecha Aprob.:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-2 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkPeriodo2" class="form-control"><span></span>
                        </label>
                    </div>
                    <div class="col-lg-5 contain-lg">
                        <div class="form-group" id="fdesde2Error">
                            <input type="text" disabled name="fdesde2" id="fdesde2" value="{date('Y-m')}-01" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-5 contain-lg">
                        <div class="form-group" id="fhasta2Error">
                            <input type="text" disabled name="fhasta2" id="fhasta2" value="{date('Y-m-d')}" class="form-control">
                        </div>
                    </div>
                </div>


                <div class="contain-lg col-lg-1" align="right">
                    <span>Estado:</span>
                </div>
                <div class="col-lg-2 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkEst" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-lg-9 contain-lg">
                        <div class="form-group" id="estadoError">
                            <select id="estado" disabled class="form-control select2">
                                <option value=""></option>
                                <option value="PR">En Preparado</option>
                                <option value="RV">Revisado</option>
                                <option value="AP">Aprobado</option>
                                <option value="AN">Anulado</option>
                                <option value="RE">Rechazado</option>
                                <option value="CE">Cerrado</option>
                                <option value="CO">Completado</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" align="center">
                <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                    Buscar
                </button>
            </div>




        </div>

        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        $('#proveedor').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        //modal para ubicacion del centro de costo
        $('#cc').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#accion').click(function () {
            var url2 = '{$_Parametros.url}modLG/reportes/reporteOrdenServicioCONTROL/generarReporteMET/';

            if($('#checkProv').attr('checked')=="checked" && $('#idProveedor').val().length>0){
                var proveedor = $('#idProveedor').val();
            } else {
                proveedor = 'no';
            }
            if($('#checkCC').attr('checked')=="checked" && $('#numCC').val().length>0){
                var centroCosto = $('#numCC').val();
            } else {
                centroCosto = 'no';
            }
            if($('#checkEst').attr('checked')=="checked" && $('#estado').val().length>0){
                var estado = $('#estado').val();
            } else {
                estado = 'no';
            }
            var msj ='';
            if($('#checkPeriodo').attr('checked')=="checked"){
                var fdesde = $('#fdesde').val();
                var fhasta = $('#fhasta').val();
                if(fdesde>fhasta){
                    msj = 'Error de Fechas de Preparacion';
                }
            } else {
                fdesde = 'no';
                fhasta = 'no';
            }
            if($('#checkPeriodo2').attr('checked')=="checked"){
                var fdesde2 = $('#fdesde2').val();
                var fhasta2 = $('#fhasta2').val();
                if(fdesde2>fhasta2){
                    msj = 'Error de Fechas de Aprobacion';
                }
            } else {
                fdesde2 = 'no';
                fhasta2 = 'no';
            }

            if(msj!=''){
                swal("Error!", msj , "error");
            } else {
                $('#respuestaPdf').html('<iframe frameborder="0" src="' + url2 +
                        '/'+proveedor+
                        '/'+centroCosto+
                        '/'+estado+
                        '/'+fdesde+
                        '/'+fhasta+
                        '/'+fdesde2+
                        '/'+fhasta2

                        +'" width="100%" height="540px"></iframe>');
            }

        });

        $('#checkProv').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#proveedor').attr('disabled',false);
            } else {
                $('#proveedor').attr('disabled','disabled');
                $('#idProveedor').val('');
                $('#persona').val('');
            }
        });

        $('#checkCC').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#cc').attr('disabled',false);
            } else {
                $('#cc').attr('disabled','disabled');
                $('#numCC').val('');
                $('#nombreCC').val('');
            }
        });

        $('#checkEst').click(function () {
            if($(this).attr('checked')=="checked"){
                $('#estado').attr('disabled',false);
            } else {
                $('#estado').attr('disabled','disabled');
                $('#estado').val('');
            }
        });

        $('#fdesde').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#fdesde2').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#fhasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#fhasta2').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('#checkPeriodo').change(function () {
            if($('#checkPeriodo').attr('checked')=="checked" ){
                $('#fdesde').attr('disabled',false);
                $('#fdesde').attr('readonly','readonly');
                $('#fhasta').attr('disabled',false);
                $('#fhasta').attr('readonly','readonly');
            } else {
                $('#fdesde').attr('readonly',false);
                $('#fdesde').attr('disabled',true);
                $('#fhasta').attr('readonly',false);
                $('#fhasta').attr('disabled',true);

                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth()+1;
                var yyyy = hoy.getFullYear();

                if(dd<10) { dd='0'+dd; }
                if(mm<10) { mm='0'+mm; }

                var fdesde = yyyy+'-'+mm+'-01';
                var fhasta = yyyy+'-'+mm+'-'+dd;
                $('#fdesde').val(fdesde);
                $('#fhasta').val(fhasta);
            }
        });

        $('#checkPeriodo2').change(function () {
            if($('#checkPeriodo2').attr('checked')=="checked" ){
                $('#fdesde2').attr('disabled',false);
                $('#fdesde2').attr('readonly','readonly');
                $('#fhasta2').attr('disabled',false);
                $('#fhasta2').attr('readonly','readonly');
            } else {
                $('#fdesde2').attr('readonly',false);
                $('#fdesde2').attr('disabled',true);
                $('#fhasta2').attr('readonly',false);
                $('#fhasta2').attr('disabled',true);

                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth()+1;
                var yyyy = hoy.getFullYear();

                if(dd<10) { dd='0'+dd; }
                if(mm<10) { mm='0'+mm; }

                var fdesde2 = yyyy+'-'+mm+'-01';
                var fhasta2 = yyyy+'-'+mm+'-'+dd;
                $('#fdesde2').val(fdesde2);
                $('#fhasta2').val(fhasta2);
            }
        });

    });
</script>