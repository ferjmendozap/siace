<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Consumo por Dependencia</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12">

                <div class="contain-lg col-lg-1" align="right">
                    <span>Periodo:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-2 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkPeriodo" class="form-control"><span></span>
                        </label>
                    </div>
                    <div class="col-lg-5 contain-lg">
                        <div class="form-group" id="fdesdeError">
                            <input type="text" disabled name="fdesde" id="fdesde" value="{date('Y-m')}-01" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-5 contain-lg">
                        <div class="form-group" id="fhastaError">
                            <input type="text" disabled name="fhasta" id="fhasta" value="{date('Y-m-d')}" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="contain-lg col-lg-1" align="right">
                    <span>Dependencia:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkDep" class="form-control"><span></span>
                        </label>
                    </div>
                    <div class="col-lg-8 contain-lg">
                        <div class="form-group" id="depError">
                            <select id="dep" class="form-control select2" disabled>
                                <option value=""></option>
                                {foreach item=dep from=$dependencia}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/foreach}
                            </select>

                        </div>
                    </div>
                </div>

                <div class="contain-lg col-lg-1" align="right">
                    <span>Centro de Costo:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkCC" class="form-control">
                            <span></span>
                        </label>
                    </div>

                    <div class="col-lg-8 contain-lg">
                        <div class="form-group" id="numCCError">
                            <input type="hidden" readonly name="numCC" id="numCC" class="form-control">
                            <input type="text" disabled id="nombreCC" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-1 contain-lg">
                        <button id="cc" disabled class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Listado de Centro de Costo"
                                url="{$_Parametros.url}modLG/reportes/reporteConsumoCONTROL/centroCostoMET/">
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>

            </div>

            <div class="col-lg-12" align="center">
                <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                    Buscar
                </button>
            </div>

        </div>

        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        //modal para ubicacion del centro de costo
        $('#cc').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#fdesde').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#fhasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('#checkPeriodo').change(function () {
            if($('#checkPeriodo').attr('checked')=="checked" ){
                $('#fdesde').attr('disabled',false);
                $('#fdesde').attr('readonly','readonly');
                $('#fhasta').attr('disabled',false);
                $('#fhasta').attr('readonly','readonly');
            } else {
                $('#fdesde').attr('readonly',false);
                $('#fdesde').attr('disabled',true);
                $('#fhasta').attr('readonly',false);
                $('#fhasta').attr('disabled',true);

                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth()+1;
                var yyyy = hoy.getFullYear();

                if(dd<10) { dd='0'+dd; }
                if(mm<10) { mm='0'+mm; }

                var fdesde = yyyy+'-'+mm+'-01';
                var fhasta = yyyy+'-'+mm+'-'+dd;
                $('#fdesde').val(fdesde);
                $('#fhasta').val(fhasta);
            }
        });

        $('#checkDep').change(function () {
            if($('#checkDep').attr('checked')=="checked" ){
                $('#dep').attr('disabled',false);
            } else {
                $('#dep').attr('disabled','disabled');
                $('#dep').val('');
            }
        });

        $('#checkCC').change(function () {
            if($('#checkCC').attr('checked')=="checked" ){
                $('#cc').attr('disabled',false);
            } else {
                $('#cc').attr('disabled','disabled');
                $('#numCC').val('');
                $('#nombreCC').val('');
            }
        });

        $('#accion').click(function () {
            var url2 = '{$_Parametros.url}modLG/reportes/reporteConsumoCONTROL/generarReporteMET/';

            if($('#checkPeriodo').attr('checked')=="checked"){
                var fdesde = $('#fdesde').val();
                var fhasta = $('#fhasta').val();
            } else {
                fdesde = 'no';
                fhasta = 'no';
            }

            if($('#checkDep').attr('checked')=="checked" && $('#dep').val().length>0){
                var dep = $('#dep').val();
            } else {
                dep = 'no';
            }

            if($('#checkCC').attr('checked')=="checked" &&  $('#numCC').val().length>0){
                var cc = $('#numCC').val();
            } else {
                cc = 'no';
            }

            $('#respuestaPdf').html('<iframe frameborder="0" src="' + url2 +fdesde+'/' +fhasta+'/' +dep+'/' +cc+'/ " width="100%" height="540px"></iframe>');
        });

    });
</script>