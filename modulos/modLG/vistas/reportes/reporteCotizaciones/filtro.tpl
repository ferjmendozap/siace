<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Reporte de Cotizaciones</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
                <div class="contain-lg col-lg-1" align="right">
                    <span>Periodo:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-2 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkPeriodo" class="form-control"><span></span>
                        </label>
                    </div>
                    <div class="col-lg-5 contain-lg">
                        <div class="form-group" id="fdesdeError">
                            <input type="text" disabled name="fdesde" id="fdesde" value="{date('Y-m')}-01" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-5 contain-lg">
                        <div class="form-group" id="fhastaError">
                            <input type="text" disabled name="fhasta" id="fhasta" value="{date('Y-m-d')}" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="contain-lg col-lg-1" align="right">
                    <span>Item:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkI" class="form-control">
                            <span></span>
                        </label>
                    </div>

                    <div class="col-lg-8 contain-lg">
                        <div class="form-group" id="numCCError">
                            <input type="hidden" name="numItem" id="numItem" class="form-control">
                            <input type="text" disabled id="nombreItem" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-1 contain-lg">
                        <button id="item" disabled class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Listado de Items"
                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/item4/">
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>
                <div class="contain-lg col-lg-1" align="right">
                    <span>Commoditie:</span>
                </div>
                <div class="col-lg-3 contain-lg">
                    <div class="col-lg-3 contain-lg">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkC" class="form-control">
                            <span></span>
                        </label>
                    </div>

                    <div class="col-lg-8 contain-lg">
                        <div class="form-group" id="numCCError">
                            <input type="hidden" name="numComm" id="numComm" class="form-control">
                            <input type="text" disabled id="nombreComm" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-1 contain-lg">
                        <button id="comm" disabled class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Listado de Commoditie"
                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/itemCommodityMET/commodity4/">
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>
                </div>

                <div class="col-lg-12" align="center">
                    <button id="accion" class="btn btn-primary ink-reaction btn-raised">
                        Buscar
                    </button>
                </div>
        </div>

        <div id="respuestaPdf">

        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        $('#formulario').submit(function () {
            return false;
        });

        $('.accionModal').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#fdesde').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#fhasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('#checkPeriodo').change(function () {
            if($('#checkPeriodo').attr('checked')=="checked" ){
                $('#fdesde').attr('disabled',false);
                $('#fdesde').attr('readonly','readonly');
                $('#fhasta').attr('disabled',false);
                $('#fhasta').attr('readonly','readonly');
            } else {
                $('#fdesde').attr('readonly',false);
                $('#fdesde').attr('disabled',true);
                $('#fhasta').attr('readonly',false);
                $('#fhasta').attr('disabled',true);

                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth()+1;
                var yyyy = hoy.getFullYear();

                if(dd<10) { dd='0'+dd; }
                if(mm<10) { mm='0'+mm; }

                var fdesde = yyyy+'-'+mm+'-01';
                var fhasta = yyyy+'-'+mm+'-'+dd;
                $('#fdesde').val(fdesde);
                $('#fhasta').val(fhasta);
            }
        });


        $('#checkI').change(function () {
            if($('#checkI').attr('checked')=="checked" ){
                $('#item').attr('disabled',false);
            } else {
                $('#item').attr('disabled','disabled');
                $('#numItem').val('');
                $('#nombreItem').val('');
            }
        });


        $('#checkC').change(function () {
            if($('#checkC').attr('checked')=="checked" ){
                $('#comm').attr('disabled',false);
            } else {
                $('#comm').attr('disabled','disabled');
                $('#numComm').val('');
                $('#nombreComm').val('');
            }
        });

        $('#accion').click(function () {
            var url = "{$_Parametros.url}modLG/reportes/reporteCotizacionesCONTROL/generarReporteMET/";
            if($('#checkPeriodo').attr('checked')=="checked"){
                var fdesde = $('#fdesde').val();
                var fhasta = $('#fhasta').val();
            } else {
                fdesde = 'no';
                fhasta = 'no';
            }

            if($('#checkI').attr('checked')=="checked" && $('#numItem').val()!=''){
                var item = $('#numItem').val();
            } else {
                item = 'no';
            }

            if($('#checkC').attr('checked')=="checked" && $('#numComm').val()!=''){
                var commodity = $('#numComm').val();
            } else {
                commodity = 'no';
            }
            if(item == 'no' && commodity == 'no') {
                swal("Error!", 'Debe seleccionar un Item o Commoditie' , "error");
            } else if(item != 'no' && commodity != 'no') {
                swal("Error!", 'Debe seleccionar solo un Item o Commoditie' , "error");
            } else {
                $('#respuestaPdf').html('<iframe frameborder="0" src="' + url +fdesde+'/'+fhasta+'/'+item+'/'+commodity+'" width="100%" height="540px"></iframe>');
            }

        });
    });
</script>