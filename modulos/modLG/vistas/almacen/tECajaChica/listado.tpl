<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="section-header">
                        <h2 class="text-primary">Recepción / Despacho de Requerimientos de Caja Chica</h2>
                    </div>
                    <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                        <div class="row">
                            <div class="card">
                                <div class="card-body">
                                    <div id="rootwizard2" class="form-wizard form-wizard-horizontal">
                                        <form class="form floating-label form-validation" role="form" novalidate="novalidate">
                                            <div class="form-wizard-nav">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-primary"></div>
                                                </div>
                                                <ul class="nav nav-justified">
                                                    <li class="active">
                                                        <a href="#tab5" data-toggle="tab"><span class="step">1</span> <span class="title" style="font-weight:bold;">Recepción</span> </a></li>
                                                    <li>
                                                        <a href="#tab6" data-toggle="tab"><span class="step">2</span> <span class="title" style="font-weight:bold;">Despacho</span> </a></li>
                                                </ul>
                                            </div>
                                            <div class="tab-content clearfix">
                                                <div class="tab-pane active" id="tab5">
                                                    <div class="col-lg-12">
                                                        <div class="table-responsive">
                                                            <table id="datatable1" class="table table-striped table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Requerimiento</th>
                                                                    <th>C.C</th>
                                                                    <th>Fecha Requerida</th>
                                                                    <th>Comentarios</th>
                                                                    <th>Almacen</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                {foreach item=post from=$listado1}
                                                                    <!--detalles de los requerimientos-->
                                                                    {$c1=0}
                                                                    <!--cuantos detalles estan recepcionados-->
                                                                    {$c2=0}
                                                                    {foreach item=det from=$listado1Det}
                                                                        {if $det.fk_lgb001_num_requerimiento==$post.pk_num_requerimiento}
                                                                            {if $det.num_cantidad_pedida==$det.cantidad_recibida}
                                                                                {$c2=$c2+1}
                                                                            {/if}
                                                                            {$c1=$c1+1}
                                                                        {/if}
                                                                    {/foreach}
                                                                    <!--si son disttintos debe recepcionar-->
                                                                    {if $c1!=$c2}
                                                                        <tr id="idReq{$post.pk_num_requerimiento}">
                                                                            <td>{$post.cod_requerimiento}</td>
                                                                            <td>{$post.ind_descripcion_centro_costo}</td>
                                                                            <td>{$post.fec_requerida}</td>
                                                                            <td>{$post.ind_comentarios}</td>
                                                                            <td>{$post.almacen}</td>
                                                                            <td>
                                                                                {if in_array('LG-01-02-05-01-01-R',$_Parametros.perfil)}
                                                                                    <button title="Recepcionar"
                                                                                            class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info nuevo"
                                                                                            descipcion="El Usuario ha Recepcionado el Requerimiento Nro. {$post.pk_num_requerimiento}"
                                                                                            idReq="{$post.pk_num_requerimiento}" titulo="Recepcionar Requerimiento"
                                                                                            data-toggle="modal" data-target="#formModal"
                                                                                            opcion="recepcionar"
                                                                                            idDep="{$post.fk_a004_num_dependencia}"
                                                                                            id="recepcionar">
                                                                                        <i class="fa fa-sign-in"></i>
                                                                                        <!--data-keyboard="false" data-backdrop="static"-->
                                                                                    </button>
                                                                                {/if}
                                                                            </td>
                                                                        </tr>

                                                                    {/if}
                                                                {/foreach}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab6">
                                                    <div class="col-lg-12">
                                                        <div class="table-responsive">
                                                            <table id="datatable3" class="table table-striped table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th>Requerimiento</th>
                                                                    <th>C.C</th>
                                                                    <th>Fecha Requerida</th>
                                                                    <th>Comentarios</th>
                                                                    <th>Almacen</th>
                                                                    <th>Acciones</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                {foreach item=post2 from=$listado1}
                                                                    {$cantRe=0}
                                                                    {$cantDes=0}
                                                                    {$cantAp=0}
                                                                    {$cant=0}
                                                                    {foreach item=det from=$listado1Det}
                                                                        {if $det.fk_lgb001_num_requerimiento==$post2.pk_num_requerimiento}

                                                                            {$cant=$cant+1}
                                                                            {$cantRe=$cantRe+$det.cantidad_recibida}
                                                                            {$cantDes=$cantDes+$det.cantidad_despachada}

                                                                            {if $det.ind_estado == 'PE' OR $det.ind_estado == 'CO'}
                                                                                {$cantAp=$cantAp+1}
                                                                            {/if}
                                                                        {/if}
                                                                    {/foreach}
                                                                    {if $cantRe>0 AND $cantRe!=$cantDes}
                                                                        <tr id="idReq2{$post2.pk_num_requerimiento}">
                                                                            <td>{$post2.cod_requerimiento}</td>
                                                                            <td>{$post2.ind_descripcion_centro_costo}</td>
                                                                            <td>{$post2.fec_requerida}</td>
                                                                            <td>{$post2.ind_comentarios}</td>
                                                                            <td>{$post2.almacen}</td>
                                                                            <td>
                                                                                {if in_array('LG-01-02-05-01-01-R',$_Parametros.perfil) AND $cantAp<$cant}
                                                                                    <button title="Aprobar"
                                                                                            class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary nuevo"
                                                                                            descipcion="El Usuario ha Aprobado el Requerimiento Nro. {$post2.pk_num_requerimiento}"
                                                                                            idReq="{$post2.pk_num_requerimiento}" titulo="Aprobar Despacho"
                                                                                            data-toggle="modal" data-target="#formModal"
                                                                                            opcion="aprobar"
                                                                                            idDep="{$post2.fk_a004_num_dependencia}"
                                                                                            id="aprobar{$post2.pk_num_requerimiento}">
                                                                                        <i class="icm icm-rating3"></i>
                                                                                        <!--data-keyboard="false" data-backdrop="static"-->
                                                                                    </button>
                                                                                {/if}
                                                                                {if in_array('LG-01-02-05-01-01-R',$_Parametros.perfil) AND $cantRe!=$cantDes AND $cantAp!=0}
                                                                                    <button title="Despachar"
                                                                                            class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info nuevo"
                                                                                            descipcion="El Usuario ha Despachado el Requerimiento Nro. {$post2.pk_num_requerimiento}"
                                                                                            idReq="{$post2.pk_num_requerimiento}" titulo="Despachar Requerimiento"
                                                                                            data-toggle="modal" data-target="#formModal"
                                                                                            opcion="despachar"
                                                                                            idDep="{$post2.fk_a004_num_dependencia}"
                                                                                            id="despachar">
                                                                                        <i class="fa fa-sign-out"></i>
                                                                                        <!--data-keyboard="false" data-backdrop="static"-->
                                                                                    </button>
                                                                                {/if}
                                                                            </td>
                                                                        </tr>
                                                                    {/if}
                                                                {/foreach}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--END ClearFix-->
                                            <ul class="pager wizard">
                                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                                <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span class="clearfix"></span>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        app.metWizard();
        /*
        var dt = app.dataTable(
                '#dataTablaJson1',
                "{$_Parametros.url}modLG/almacen/tECajaChicaCONTROL/jsonDataTablaMET/1",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_requerimiento" },
                    { "data": "ind_descripcion_centro_costo" },
                    { "data": "fec_requerida" },
                    { "data": "ind_comentarios" },
                    { "data": "almacen" },
                    { "orderable": false,"data": "acciones", width:150}
                ]
        );
        dt = app.dataTable(
                '#dataTablaJson2',
                "{$_Parametros.url}modLG/almacen/tECajaChicaCONTROL/jsonDataTablaMET/2",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_requerimiento" },
                    { "data": "ind_descripcion_centro_costo" },
                    { "data": "fec_requerida" },
                    { "data": "ind_comentarios" },
                    { "data": "almacen" },
                    { "orderable": false,"data": "acciones", width:150}
                ]
        );
        */

        $(".form").submit(function () {
            return false;
        });
        var url = '{$_Parametros.url}modLG/almacen/tECajaChicaCONTROL/crearRecepcionMET';
        $('.table').on('click','.nuevo',function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idReq: $(this).attr('idReq'), opcion: $(this).attr('opcion'), idDep: $(this).attr('idDep'), idTran: $(this).attr('idTran') },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#datatable3').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }

        });

    });

</script>