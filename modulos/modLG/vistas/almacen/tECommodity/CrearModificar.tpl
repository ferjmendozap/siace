<form action="{$_Parametros.url}modLG/almacen/tECommodityCONTROL/crearModificarTransaccionMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idOrden)}{$idOrden}{/if}" name="idOrden"/>
    <input type="hidden" value="{if isset($idTran)}{$idTran}{/if}" name="idTran"/>
    <input type="hidden" value="{if isset($opcion)}{$opcion}{/if}" name="opcion"/>

    {if isset($formDBI) AND $formDB.clasificacion=='ACTIVOS FIJOS' AND $opcion=="recepcionar"}
        <input type="hidden" value="1" name="form[int][af]">
    {/if}
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active">
                                        <a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title" style="font-weight:bold;">INFORMACIÓN GENERAL</span> </a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title" style="font-weight:bold;">COMMODITIES</span> </a></li>
                                    <!--li><a href="#tab3" data-toggle="tab"><span class="step">3</span> <span class="title" style="font-weight:bold;">ACTIVOS</span> </a></li-->
                                </ul>
                            </div><!--end .form-wizard-nav -->
                            <div class="tab-content clearfix">
                                <div class="tab-pane floating-label active" id="tab1">
                                    <div class="card panel">
                                        <div class="card-head style-primary">
                                            <header>Información General</header>
                                        </div>
                                        <div class="card-body">
                                            <div class="col-lg-12">
                                                <div class="form-group col-lg-6" id="ccostoError">
                                                    <select id="ccosto" name="form[int][ccosto]" class="form-control select2 select2-list" readonly>
                                                        <option value=""></option>
                                                        {foreach item=cc from=$centro}
                                                            <!--option value="{$cc.pk_num_centro_costo}">{$cc.ind_descripcion_centro_costo}</option-->
                                                            {if $formDBI[0].fk_a023_num_centro_costo == $cc.pk_num_centro_costo}
                                                                <option value="{$cc.pk_num_centro_costo}" selected>{$cc.ind_descripcion_centro_costo}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="ccosto">Centro de Costo</label>
                                                </div>
                                                <div class="form-group col-lg-6" id="almacenError">
                                                    <select id="almacen" name="form[int][almacen]" class="form-control select2 select2-list" readonly>
                                                        <option value=""></option>
                                                        {foreach item=alm from=$almacen}
                                                            {if isset($formDB.fk_lgb014_num_almacen) and $formDB.fk_lgb014_num_almacen == $alm.pk_num_almacen}
                                                                <option value="{$alm.pk_num_almacen}" selected>{$alm.ind_descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="almacen">Almacen</label>
                                                </div>
                                                <div class="form-group col-lg-6" id="tipoTranError">
                                                    <select id="tipoTran" name="form[int][tipoTran]" class="form-control select2 select2-list" readonly>
                                                        <option value=""></option>
                                                        {foreach item=tran from=$transaccion}
                                                            {if $tran.ind_cod_tipo_transaccion=="ROC" AND $opcion!='despachar'}
                                                                <option value="{$tran.pk_num_tipo_transaccion}" selected>{$tran.ind_descripcion}</option>
                                                            {elseif $tran.ind_cod_tipo_transaccion=="DOC" AND $opcion=='despachar'}
                                                                <option value="{$tran.pk_num_tipo_transaccion}" selected>{$tran.ind_descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="tipoTran">Tipo Transacción</label>
                                                </div>
                                                <div class="form-group col-lg-6" id="tipoDocError">
                                                    <select id="tipoDoc" name="form[int][tipoDoc]" class="form-control select2 select2-list" readonly>
                                                        <option value=""></option>
                                                        {foreach item=doc from=$documento}
                                                            {if $formDB.fk_lgb016_num_tipo_documento==$doc.pk_num_tipo_documento OR $doc.cod_tipo_documento=="NI"}
                                                                <option value="{$doc.pk_num_tipo_documento}" selected>{$doc.ind_descripcion}</option>
                                                            {else}
                                                                <option value="{$doc.pk_num_tipo_documento}">{$doc.ind_descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                    <label for="tipoDoc">Tipo Documento</label>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="checkbox-inline checkbox-styled">
                                                        <input type="checkbox" name="form[int][manual]" value="1" {if isset($formDB.num_flag_manual) and $formDB.num_flag_manual==1}checked{/if}readonly>
                                                        <span>Valorización Manual</span>
                                                    </label>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="checkbox-inline checkbox-styled">
                                                        <input type="checkbox" name="form[int][pendiente]" value="1" {if isset($formDB.num_flag_pendiente) and $formDB.num_flag_pendiente==1}checked{/if} readonly>
                                                        <span>Valorización Pendiente</span>
                                                    </label>
                                                </div>

                                                <div class="form-group col-lg-6 floating-label" id="nroError">
                                                    <input type="text" name="form[int][nro]" maxlength="6" id="nro" {if isset($formDB.num_transaccion)} value="{$formDB.num_transaccion}" {/if} class="form-control" readonly>
                                                    <label for="nro">Nro. Transacción</label>
                                                </div>
                                                <div class="form-group col-lg-6" id="docRefError">
                                                    <input type="text" name="form[int][docRef]" id="docRef" maxlength="20" class="form-control" value="{if isset($formDB.ind_orden)}{$formDB.ind_orden}{/if}" readonly>
                                                    <label for="docRef">Nro. Doc. Referencia</label>
                                                </div>
                                                <div class="form-group col-lg-6 floating-label" id="notaError">
                                                    <input type="text" name="form[alphaNum][nota]" id="nota" {if isset($formDB.ind_nota_entrega_factura)} value="{$formDB.ind_nota_entrega_factura}" {/if} class="form-control" >
                                                    <label for="nota">Factura</label>
                                                </div>
                                                <div class="form-group col-lg-6 floating-label" id="fdocError">
                                                    <input type="text" name="form[alphaNum][fdoc]" class="form-control" value="{date('Y-m-d')}" readonly>
                                                    <label for="fdoc">Fecha de Documento<i class="fa fa-calendar"></i></label>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group" id="comenError">
                                                        <textarea name="form[alphaNum][comen]" id="comen" class="form-control">{if isset($formDB.ind_comentario)}{$formDB.ind_comentario}{/if}</textarea>
                                                        <label for="comen"><i class="md md-border-color"></i>Comentario</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!--end #tab1 -->
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="col-lg-12">
                                        <div class="card-head style-primary">
                                            <header>Commodities</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped no-margin" id="contenidoTabla">
                                                    <thead>
                                                    <tr>
                                                        <th width="15px">#</th>
                                                        <th width="250px">Commodities</th>
                                                        <th width="100px">Uni.</th>
                                                        <th width="100px">Cantidad</th>
                                                        <th width="100px">Precio Unit.</th>
                                                        <th width="100px">Total</th>
                                                        <th width="10px">Seleccionar {if $opcion=="recepcionar"}Recepción{elseif $opcion=="aprobar"}Aprobado{else}Despacho{/if}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="item">
                                                    {if isset($formDBI)}
                                                        {$cant=0}
                                                        {foreach item=i from=$formDBI}
                                                            {$cantiRecibida=0}
                                                            {$cantiDespachada=0}
                                                            {$cantiPedida=$i.num_cantidad}
                                                            {foreach item=td from=$tranDet}
                                                                {if $td.fk_lgc009_num_orden_detalle==$i.pk_num_orden_detalle}
                                                                    {if $td.ind_cod_tipo_transaccion=='ROC'}
                                                                        {$cantiRecibida=$cantiRecibida+$td.num_cantidad_transaccion}
                                                                    {elseif $td.ind_cod_tipo_transaccion=='DOC'}
                                                                        {$cantiDespachada=$cantiDespachada+$td.num_cantidad_transaccion}
                                                                    {/if}
                                                                {/if}
                                                            {/foreach}
                                                            {$check=''}
                                                            {if $i.detalleEstatus=='AP' AND $opcion=="aprobar"}
                                                                {$check='checked'}
                                                            {/if}
                                                            {$readonly=""}
                                                            {$readonly2=""}
                                                            {if $opcion=="recepcionar"}
                                                                {$canti=$cantiPedida-$cantiRecibida}
                                                            {/if}
                                                            {if $opcion=="aprobar"}
                                                                {$readonly="readonly"}
                                                                {$canti=$cantiRecibida}
                                                            {/if}
                                                            {if $opcion=="despachar"}
                                                                {$readonly2="readonly"}
                                                                {$canti=$cantiRecibida-$cantiDespachada}
                                                                {if $i.detalleEstatus!='AP'}
                                                                    {$canti=0}
                                                                {/if}
                                                            {/if}
                                                            {if $canti!=0}
                                                                {$cant=$cant+1}
                                                                <tr class="item{$cant}">
                                                                    <input type="hidden" name="form[int][cantiPedida][{$cant}]" value="{$cantiPedida}" id="cantiPedida{$cant}">
                                                                    <input type="hidden" name="form[int][cantiDespachada][{$cant}]" value="{$cantiDespachada}" id="cantiDespachada{$cant}">
                                                                    <input type="hidden" name="form[int][cantiRecibida][{$cant}]" value="{$cantiRecibida}" id="cantiRecibida{$cant}">
                                                                    <input type="hidden" name="form[int][idStock][{$cant}]" value="{$i.pk_num_commodity_stock}">
                                                                    <input type="hidden" name="form[int][numComm][{$cant}]" value="{$i.fk_lgb003_num_commodity}">
                                                                    <input type="hidden" name="form[int][idDet][{$cant}]" value="{$i.pk_num_orden_detalle}">
                                                                    <input type="hidden" name="form[int][idDetReq][{$cant}]" value="{if isset($i.fk_lgc001_num_requerimiento_detalle)}{$i.fk_lgc001_num_requerimiento_detalle}{else}null{/if}">
                                                                    <input type="hidden" name="form[int][unidad][{$cant}]" value="{$i.uniComm1}">
                                                                    <input type="hidden" name="form[int][sAct][{$cant}]" value="{if $i.num_stock_actual_comm==NULL}0{else}{$i.num_stock_actual_comm}{/if}">

                                                                    <td width="15px" style="vertical-align: middle;">{$cant}</td>
                                                                    <td width="250px" style="vertical-align: middle;">{$i.ind_descripcion_commodity}</td>
                                                                    <td width="100px" style="vertical-align: middle;">{$i.uniComm}</td>
                                                                    <td width="100px"><input type="text" class="form-control cantidad" name="form[int][canti][{$cant}]" size="1" value="{$canti}" id="cantidad{$cant}" secuencia="{$cant}" {if $opcion=="aprobar"} readonly{/if}></td>
                                                                    <td width="100px"><input type="text" class="form-control" readonly name="form[int][precio][{$cant}]" size="2" value="{$i.num_precio_unitario}" id="precio{$cant}"></td>
                                                                    <td width="10px"><input type="text" class="form-control" readonly name="form[int][total][{$cant}]" size="1" value="{$i.num_total}"></td>
                                                                    <td style="vertical-align: middle;">
                                                                        <div class="checkbox checkbox-styled" align="center">
                                                                            <label class="checkbox-inline checkbox-styled">
                                                                                <input type="checkbox" name="form[int][opcion][{$cant}]" class="checkbox checkbox-styled" value="1" id="check{$cant}" {$check}>
                                                                            </label>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            {/if}

                                                        {/foreach}
                                                        <input type="hidden" name="form[int][cant]" value="{$cant}">
                                                    {/if}

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div><!--end #tab2 -->
                                </div><!--end #tab2 -->
                                <!--div class="tab-pane floating-label" id="tab3">
                                    <div class="col-lg-12">
                                        <div class="card-head style-primary">
                                            <header>Activos Asociados</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">
                                                <table class="table table-striped no-margin" id="contenidoTabla3">
                                                    <thead>
                                                    <tr>
                                                        <th width="1px">#</th>
                                                        <th width="15px">Activo</th>
                                                        <th width="15px">Ubicación</th>
                                                        <th width="15px">Marca</th>
                                                        <th width="15px">Color</th>
                                                        <th width="15px">Nro Serie</th>
                                                        <th width="15px">Modelo</th>
                                                        <th width="15px">Cod Barra</th>
                                                        <th width="15px">Nro Placa</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="item2">
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div><!--end #tab3 ->
                                </div><!--end #tab3 -->
                                <!--end .tab-content -->

                                <ul class="pager wizard">
                                    <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                </ul>
                            </div><!--END ClearFix-->
                        </div><!--end #rootwizard1 -->
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col -->
        </div><!--end .row -->
    </div><!--end .modal-body -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            {if isset($opcion)}
                <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                    {if $opcion=="recepcionar"}
                        <span class="fa fa-sign-in"></span> Recepcionar
                    {elseif $opcion=="aprobar"}
                        <span class="icm icm-rating3"></span> Aprobar
                    {elseif $opcion=="despachar"}
                        <span class="fa fa-sign-out"></span> Despachar
                    {/if}
                </button>
            {/if}
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "80%");
    $("#formAjax").submit(function(){
        return false;
    });
    $('.select2').select2({ allowClear: true });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('#fdoc').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('.cantidad').change(function(){
            var secuencia = $(this).attr('secuencia');
            var cantidad = $('#cantidad'+secuencia).val();
            var cantiPedida = $('#cantiPedida'+secuencia).val();
            var cantiRecibida = $('#cantiRecibida'+secuencia).val();
            var cantiDespachada = $('#cantiDespachada'+secuencia).val();
            var precio = $('#precio'+secuencia).val();

            {if $opcion=="recepcionar"}
            if(parseFloat(cantidad)>(parseFloat(cantiPedida)-parseFloat(cantiRecibida))){
                cantidad = parseFloat(cantiPedida)-parseFloat(cantiRecibida);
                $('#cantidad'+secuencia).val(cantidad);
            }
            {/if}

            {if $opcion=="despachar"}
            if(parseFloat(cantidad)>(parseFloat(cantiPedida)-parseFloat(cantiDespachada))){
                cantidad = parseFloat(cantiPedida)-parseFloat(cantiDespachada);
                $('#cantidad'+secuencia).val(cantidad);
            }
            {/if}

            var valor = cantidad*precio;
            $('#total'+secuencia).val(valor);
            if($('#check'+secuencia).attr('checked')==undefined){
                $('#check'+secuencia).attr('checked','checked');
            }
        });
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='errorOpcion'){
                    app.metValidarError(dato,'Disculpa. Debe seleccionar cual(es) detalle(s) desea efectuar la acción');
                }else if(dato['status']=='errorCant'){
                    app.metValidarError(dato,'Disculpa. No se puede recepcionar/despachar la cantidad especificada');
                }else if(dato['status']=='errorStock'){
                    swal("Error!", "No hay suficiente stock para realizar la transacción.", "error");
                }else if(dato['status']=='periodo'){
                    swal("Error!", "No hay un periodo activo para realizar transacciones.", "error");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }else if(dato['status']=='nuevo'){
                    swal("Ejecutado!", "La Orden fue Recibida satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('idOrden'+dato['idOrden'])).remove();
                }else if(dato['status']=='aprobar'){
                    swal("Ejecutado!", "La Orden fue Aprobada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('aprobar'+dato['idOrden'])).remove();
                }else if(dato['status']=='despachar'){
                    swal("Ejecutado!", "La Orden fue Despachada satisfactoriamente.", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                    $(document.getElementById('2idOrden'+dato['idOrden'])).remove();
                }
            }, 'json');
        });
    });
</script>