<form action="{$_Parametros.url}modLG/almacen/ejecutarTranCONTROL/ModificarTransaccionMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idTran)}{$idTran}{/if}" name="idTran"/>
    <div class="modal-body">
        <!-- BEGIN FORM WIZARD -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="tab-pane floating-label" id="cabecera">
                        <br/><br/>
                    </div><!--end #cabecera -->
                    <div class="card-body ">
                        <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                            <div class="form-wizard-nav">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary"></div>
                                </div>
                                <ul class="nav nav-justified">
                                    <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">INFORMACIÓN GENERAL</span> </a></li>
                                    <li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">ITEMS</span> </a></li>
                                </ul>
                            </div><!--end .form-wizard-nav -->
                            <div class="tab-content clearfix">
                                <div class="tab-pane active floating-label" id="tab1">
                                        <div class="card panel">
                                            <div class="card-head style-primary">
                                                <header>Información General</header>
                                            </div>
                                                <div class="card-body">
                                                    <div class="col-lg-12">
                                                        <div class="form-group col-lg-6" id="ccostoError">
                                                            <select id="ccosto" disabled class="form-control">
                                                                <option value="500"></option>
                                                                {foreach item=cc from=$centro}
                                                                    {if isset($formDB.fk_a023_num_centro_costo) and $formDB.fk_a023_num_centro_costo == $cc.pk_num_centro_costo}
                                                                        <option value="{$cc.pk_num_centro_costo}" selected>{$cc.ind_descripcion_centro_costo}</option>
                                                                    {else}
                                                                        <option value="{$cc.pk_num_centro_costo}">{$cc.ind_descripcion_centro_costo}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="ccosto">Centro de Costo</label>
                                                        </div>
                                                        <div class="form-group col-lg-6" id="almacenError">
                                                            <select id="almacen" disabled class="form-control">
                                                                <option value=""></option>
                                                                {foreach item=alm from=$almacen}
                                                                    {if isset($formDB.fk_lgb014_num_almacen) and $formDB.fk_lgb014_num_almacen == $alm.pk_num_almacen}
                                                                        <option value="{$alm.pk_num_almacen}" selected>{$alm.ind_descripcion}</option>
                                                                    {else}
                                                                        <option value="{$alm.pk_num_almacen}">{$alm.ind_descripcion}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="almacen">Almacen</label>
                                                        </div>
                                                        <div class="form-group col-lg-6" id="tipoTranError">
                                                            <select id="tipoTran" disabled class="form-control" name="tipoTran">
                                                                <option value=""></option>
                                                                {foreach item=tran from=$transaccion}
                                                                    {if isset($formDB.fk_lgb015_num_tipo_transaccion) and $formDB.fk_lgb015_num_tipo_transaccion == $tran.pk_num_tipo_transaccion}
                                                                        <option value="{$tran.pk_num_tipo_transaccion}" selected>{$tran.ind_descripcion}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="tipoTran">Tipo Transacción</label>
                                                        </div>
                                                        <div class="form-group col-lg-6" id="tipoDocError">
                                                            <select id="tipoDoc" disabled class="form-control">
                                                                <option value=""></option>
                                                                {foreach item=doc from=$documento}
                                                                    {if isset($formDB.fk_lgb016_num_tipo_documento) and $formDB.fk_lgb016_num_tipo_documento == $doc.pk_num_tipo_documento}
                                                                        <option value="{$doc.pk_num_tipo_documento}" selected>{$doc.ind_descripcion}</option>
                                                                    {else}
                                                                        <option value="{$doc.pk_num_tipo_documento}">{$doc.ind_descripcion}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                            <label for="tipoDoc">Tipo Documento</label>
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <label class="checkbox-inline checkbox-styled">
                                                                <input type="checkbox" disabled {if isset($formDB.num_flag_manual) and $formDB.num_flag_manual==1}checked{/if}>
                                                                <span>Valorización Manual</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="checkbox-inline checkbox-styled">
                                                                <input type="checkbox" disabled {if isset($formDB.num_flag_pendiente) and $formDB.num_flag_pendiente==1}checked{/if} >
                                                                <span>Valorización Pendiente</span>
                                                            </label>
                                                        </div>
                                                        <div class="form-group col-lg-6 floating-label" id="nroError">
                                                            <input type="text" disabled maxlength="6" id="nro" {if isset($formDB.num_transaccion)} value="{$formDB.num_transaccion}" readonly {/if} class="form-control">
                                                            <label for="nro">Nro. Transacción</label>
                                                        </div>
                                                        <div class="form-group col-lg-6" id="docRefError">
                                                            <input type="text" disabled id="docRef" maxlength="20" class="form-control" value="{if isset($formDB.ind_num_documento_referencia)}{$formDB.ind_num_documento_referencia}{/if}">
                                                            <label for="docRef">Nro. Doc. Referencia</label>
                                                        </div>
                                                        <div class="form-group col-lg-6 floating-label" id="notaError">
                                                            <input type="text" disabled id="nota" {if isset($formDB.ind_nota_entrega_factura)} value="{$formDB.ind_nota_entrega_factura}" {/if} class="form-control">
                                                            <label for="nota">Nota de Entrega</label>
                                                        </div>
                                                        <div class="form-group col-lg-6 floating-label" id="fdocError">
                                                            <input type="text" disabled id="fdoc" class="form-control" value="{if isset($formDB.fec_documento)}{$formDB.fec_documento}{/if}">
                                                            <label for="fdoc">Fecha de Documento<i class="fa fa-calendar"></i></label>
                                                        </div>
                                                        <div class="col-lg-12">
                                                            <div class="form-group" id="comenError">
                                                                    <textarea disabled id="comen" cols="30"  class="form-control"
                                                                              rows="5">{if isset($formDB.ind_comentario)}{$formDB.ind_comentario}{/if}</textarea>
                                                                <!--input type="text" disabled id="comen" class="form-control" value="{if isset($formDB.ind_comentario)}{$formDB.ind_comentario}{/if}"-->
                                                                <label for="comen"><i class="md md-border-color"></i>Comentario</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                </div><!--end #tab1 -->
                                <div class="tab-pane floating-label" id="tab2">
                                    <div class="col-lg-12">
                                        <div class="card-head style-primary">
                                            <header>Items</header>
                                        </div>
                                        <div class="card">
                                            <div class="card-body">

                                                <table class="table table-striped no-margin" id="contenidoTabla">
                                                    <thead>
                                                    <tr>
                                                        <th width="10px">#</th>
                                                        <th width="150px">Descripción</th>
                                                        <th width="10px">Num. Stock anterior</th>
                                                        <th width="10px">Cant. Transacción</th>
                                                        <th width="10px">Precio Uni.</th>
                                                        <th width="10px">Num. Uni.</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="item">
                                                    {if isset($formDBI)}
                                                        {$cant=0}
                                                        {foreach item=i from=$formDBI}
                                                            {$cant=$cant+1}
                                                            <tr id="item{$i.num_secuencia}">
                                                                <td width="10px"><input type="text" class="form-control" disabled size="2" value="{$i.num_secuencia}"></td>
                                                                <td width="10px"><input type="text" class="form-control" disabled value="{$i.ind_descripcion_ic}"></td>
                                                                <td width="10px"><input type="text" class="form-control" disabled id="cant" {$i.num_secuencia} size="4" value="{$i.num_stock_anterior}"></td>
                                                                <td width="10px"><input type="text" class="form-control" disabled {$i.num_secuencia} size="6" value="{$i.num_cantidad_transaccion}"></td>
                                                                <td width="10px"><input type="text" class="form-control" disabled {$i.num_secuencia} size="4" value="{$i.num_precio_unitario}"></td>
                                                                <td width="10px"><input type="text" class="form-control" disabled {$i.num_secuencia} size="2" value="{$i.fk_lgb004_num_unidad}"></td>
                                                            </tr>
                                                        {/foreach}
                                                    {/if}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div><!--end #tab2 -->
                                </div>
                                <!--end .tab-content -->
                                <ul class="pager wizard">
                                    <!--<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>-->
                                    <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                    <!--<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>-->
                                    <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                                </ul>
                            </div><!--END ClearFix-->
                        </div><!--end #rootwizard1 -->
                    </div><!--end .card-body -->
                </div><!--end .card -->
            </div><!--end .col -->
        </div><!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                <span class="md md-send"></span>  Ejecutar
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('#modalAncho').css("width", "80%");
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        $('#modalAncho').css("width", "80%");
        var app = new  AppFunciones();

        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metNuevoRegistroTablaJson('dataTablaJson','La Transaccion fue ejecutada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorStock'){
                    swal("Error!", "No hay suficiente stock para realizar la transacción.", "error");
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='periodo'){
                    swal("Error!", "No hay un periodo activo para realizar transacciones.", "error");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            }, 'json');
        });
    });
</script>