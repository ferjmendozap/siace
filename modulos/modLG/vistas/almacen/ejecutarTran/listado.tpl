<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Ejecutar Transacciones del Almacén</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Num. Transaccion</th>
                                <th>Periodo</th>
                                <th>Valorización Manual</th>
                                <th>Valorización Pendiente</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        var dt = app.dataTable(
            '#dataTablaJson',
            "{$_Parametros.url}modLG/almacen/ejecutarTranCONTROL/jsonDataTablaMET",
            "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "num_transaccion" },
                { "data": "periodo" },
                { "orderable": false,"data": "num_flag_manual"},
                { "orderable": false,"data": "num_flag_pendiente"},
                { "orderable": false,"data": "acciones", width:150}
            ]
        );
        
        var url = '{$_Parametros.url}modLG/almacen/ejecutarTranCONTROL/modificarTransaccionMET';
        
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idTran: $(this).attr('idTran')}, function (dato) {
                $('#ContenidoModal').html(dato);
                $(document.getElementById('idTran'+dato['id'])).remove();
            });
        });
    });
</script>