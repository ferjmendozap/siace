<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Despacho del Almacén</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Nro. Requerimiento</th>
                                <th>Centro de Costo</th>
                                <th>Fecha Requerida</th>
                                <th>Comentarios</th>
                                <th>Almacen</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var url = '{$_Parametros.url}modLG/almacen/despachoCONTROL/crearDespachoMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modLG/almacen/despachoCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "cod_requerimiento" },
                    { "data": "ind_descripcion_centro_costo" },
                    { "data": "fec_requerida" },
                    { "data": "ind_comentarios" },
                    { "data": "ind_almacen"},
                    { "orderable": false, "data": "acciones", width:150}
                ]
        );
        $('#dataTablaJson tbody').on( 'click', '.accion', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idReq: $(this).attr('idReq'), idTran: $(this).attr('idTran'), idDepe: $(this).attr('idDepe'), estado: $(this).attr('estado')  }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
    });
</script>