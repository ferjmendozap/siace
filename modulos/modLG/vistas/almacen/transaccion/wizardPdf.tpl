<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div id="rootwizard1" class="form-wizard form-wizard-horizontal">
                        <div class="form-wizard-nav">
                            <div class="progress">
                                <div class="progress-bar progress-bar-primary"></div>
                            </div>
                            <ul class="nav nav-justified">
                                <li class="active"><a href="#tab1" data-toggle="tab"><span class="step">1</span> <span class="title">TRANSACCION</span> </a></li>
                                <!--li><a href="#tab2" data-toggle="tab"><span class="step">2</span> <span class="title">ACTIVOS FIJOS</span> </a></li-->
                            </ul>
                        </div>
                        <div class="tab-content clearfix">
                            <div class="tab-pane floating-label active" id="tab1">
                            </div>
                            <div class="tab-pane floating-label" id="tab2">
                            </div>
                            <ul class="pager wizard">
                                <!--<li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>-->
                                <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                                <!--<li class="next last"><a class="btn-raised" href="javascript:void(0);">Último</a></li>-->
                                <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function () {
        var url2 ="{$_Parametros.url}modLG/almacen/transaccionCONTROL/listadoImprimirMET/"+{$idTran};
        var url3 ="{$_Parametros.url}modLG/almacen/transaccionCONTROL/listadoImprimirActivosMET/"+{$idTran};
        $('#tab1').html('<iframe frameborder="0" src="' + url2 +'" width="100%" height="500px"></iframe>');
        $('#tab2').html('<iframe frameborder="0" src="' + url3 +'" width="100%" height="500px"></iframe>');
    });
</script>