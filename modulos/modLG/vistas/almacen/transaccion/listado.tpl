<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Transacciones del Almacén</h2>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkDependencia">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label for="dependencia"
                               class="control-label"> Dependencia: </label>
                    </div>
                    <div class="col-sm-9">
                        <select id="dependenciaL" class="form-control select2-list select3" disabled>
                            <option value="0">Seleccione...</option>
                            {foreach item=dep from=$dependencia}
                                {if $formDBR.fk_a004_num_dependencia==$dep.pk_num_dependencia}
                                    <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                {elseif $predeterminado.fk_a004_num_dependencia==$dep.pk_num_dependencia AND !isset($formDBR.fk_a004_num_dependencia)}
                                    <option value="{$dep.pk_num_dependencia}" selected>{$dep.ind_dependencia}</option>
                                {else}
                                    <option value="{$dep.pk_num_dependencia}">{$dep.ind_dependencia}</option>
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkTipoTran">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="tipoTranL"
                               class="control-label" style="margin-top: 10px;"> Tipo Transacción: </label>
                    </div>
                    <div class="col-sm-6">
                        <select id="tipoTranL" class="form-control select2-list select3" disabled>
                            <option value="0">Seleccione...</option>
                            {foreach item=tt from=$tipoTran}
                                <option value="{$tt.pk_num_tipo_transaccion}">{$tt.ind_descripcion}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <label class="checkbox-styled">
                            <input type="checkbox" id="checkCC" class="form-control">
                            <span></span>
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <label for="centroCosto"
                               class="control-label">C. Costo:</label>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group" id="numCCError">
                            <input type="hidden" readonly name="numCC" id="numCC" class="form-control">
                            <input type="text" disabled id="nombreCC" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <div class="form-group floating-label">
                            <button
                                    type="button"
                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                    data-toggle="modal"
                                    data-target="#formModal2"
                                    id="bcc"
                                    titulo="Listado Centro de Costo"
                                    disabled
                                    url="{$_Parametros.url}modLG/reportes/reporteConsumoCONTROL/centroCostoMET/">
                                <i class="md md-search"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkEstado" checked>
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="estado"
                               class="control-label" style="margin-top: 10px;"> Estado:</label>
                    </div>
                    <div class="col-sm-6">
                        <select id="estadoFiltro" class="form-control select2-list select3" disabled>
                            <option value="s">Seleccione...</option>
                            <option>Pendiente</option>
                            <option>Aprobado</option>
                            <option selected>Ejecutado</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">

                <div class="col-sm-6">
                    <div class="col-sm-1">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkBusqueda" >
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <label for="busqueda"
                               class="control-label"> Buscar:</label>
                    </div>
                    <div class="col-sm-10">
                        <input type="text" class="form-control text-center"
                               id="busqueda"
                               disabled>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="col-sm-1 text-right">
                        <div class="checkbox checkbox-styled">
                            <label>
                                <input type="checkbox" value="1" id="checkFechaDoc" checked="checked">
                                <span></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <label for="pago"
                               class="control-label" style="margin-top: 10px;"> F.Documento: </label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control text-center date"
                               id="desde"
                               style="text-align: center"
                               value="{date('Y-m')}-01"
                               placeholder="Desde"
                               disabled="disabled"
                               readonly>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control text-center date"
                               id="hasta"
                               style="text-align: center"
                               value="{date('Y-m-d')}"
                               placeholder="Hasta"
                               disabled="disabled"
                               readonly>
                    </div>
                </div>
            </div>

            <div align="center">
                <button class="buscar logsUsuario btn ink-reaction btn-raised btn-primary">
                    BUSCAR
                </button>
            </div>
        </div>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Num. Transaccion</th>
                                <th>Transaccion</th>
                                <th>Periodo</th>
                                <th>Doc. Referencia</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan="6">
                                    {if in_array('LG-01-02-01-01-N',$_Parametros.perfil)}
                                        <button class="nuevo logsUsuario btn ink-reaction btn-raised btn-info"
                                                descipcion="el Usuario ha creado el periodo Nro. " data-toggle="modal"
                                                data-keyboard="false" data-backdrop="static"
                                                data-target="#formModal" titulo="Registrar Nueva Transaccion" id="nuevo">
                                            <i class="md md-create"></i> Nueva Transaccion
                                    </button>
                                    {/if}
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        $('.select3').select2({ allowClear: true });
        $('#desde').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#hasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });

        $('#bcc').click(function () {
            $('#respuestaPdf').html('');
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });

        function verificarFechas(cual) {
            if(cual=='checked'){
                $('#desde').attr('disabled', false);
                $('#hasta').attr('disabled', false);
            }else{
                $('#desde').attr('disabled', true);
                $('#hasta').attr('disabled', true);

                var hoy = new Date();
                var dd = hoy.getDate();
                var mm = hoy.getMonth()+1;
                var yyyy = hoy.getFullYear();

                if(dd<10) { dd='0'+dd; }
                if(mm<10) { mm='0'+mm; }

                var fdesde = yyyy+'-'+mm+'-01';
                var fhasta = yyyy+'-'+mm+'-'+dd;
                $('#desde').val(fdesde);
                $('#hasta').val(fhasta);

            }
        }

        verificarFechas($('#checkFechaDoc').attr('checked'));

        $('#checkTipoTran').on('change', function () {
            if($(this).attr('checked')=='checked') {
                $('#tipoTranL').attr('disabled',false);
            } else {
                $('#tipoTranL').attr('disabled',true);
                $('#tipoTranL option[value=0]').attr("selected",true);
                $('#s2id_tipoTranL .select2-chosen').html($('#tipoTranL option:selected').text());
            }
        });

        $('#checkDependencia').on('change', function () {
            if($(this).attr('checked')=='checked') {
                $('#dependenciaL').attr('disabled',false);
            } else {
                $('#dependenciaL').attr('disabled',true);
                $('#dependenciaL option[value={$predeterminado.fk_a004_num_dependencia}]').attr("selected",true);
                $('#s2id_dependenciaL .select2-chosen').html($('#dependenciaL option:selected').text());
            }
        });

        $('#checkCC').change(function () {
            if($('#checkCC').attr('checked')=="checked" ){
                $('#bcc').attr('disabled',false);
            } else {
                $('#bcc').attr('disabled','disabled');
                $('#numCC').val('');
                $('#nombreCC').val('');
            }
        });

        $('#checkEstado').click(function () {
            if($(this).attr('checked')=='checked'){
                $('#estadoFiltro').attr('disabled', false);
            }else{
                $('#estadoFiltro').attr('disabled', true);
                $('#estadoFiltro option[value=s]').attr("selected",true);
                $('#s2id_estadoFiltro .select2-chosen').html($('#estadoFiltro option:selected').text());
            }
        });

        $('#checkBusqueda').click(function () {
            if($(this).attr('checked')=='checked'){
                $('#busqueda').attr('disabled', false);
            }else{
                $('#busqueda').attr('disabled', true);
                $('#busqueda').val("");
            }
        });

        $('#checkFechaDoc').click(function () {
            verificarFechas($('#checkFechaDoc').attr('checked'));
        });


        if($('#checkEstado').attr('checked')=='checked'){
            $('#estadoFiltro').attr('disabled', false);
        }else {
            $('#estadoFiltro').attr('disabled', true);
            $('#estadoFiltro option[value=1]').attr("selected",true);
            $('#s2id_estadoFiltro .select2-chosen').html($('#estadoFiltro option:selected').text());
        }

        function listado(cual) {

            var urlListado = "{$_Parametros.url}modLG/almacen/transaccionCONTROL/jsonDataTablaMET";
            var dependencia = 'false';
            var tipoTran = 'false';
            var centroCosto = 'false';
            var estado = 'false';
            var busqueda = 'false';
            var desde = 'false';
            var hasta = 'false';
            if($('#checkDependencia').attr('checked')=='checked') { dependencia = $('#dependenciaL').val(); }
            if($('#checkTipoTran').attr('checked')=='checked') { tipoTran = $('#tipoTranL').val(); }
            if($('#checkCC').attr('checked')=='checked') { centroCosto = $('#numCC').val(); }
            if($('#checkEstado').attr('checked')=='checked' && $('#estadoFiltro').val()!='s') { estado = $('#estadoFiltro').val(); }
            if($('#checkBusqueda').attr('checked')=='checked') { busqueda = $('#busqueda').val(); }
            if($('#checkFechaDoc').attr('checked')=='checked'){ desde = $('#desde').val(); hasta = $('#hasta').val(); }

            if(cual==1) {
                var dt = app.dataTable(
                        '#dataTablaJson',urlListado
                        +'/'+dependencia
                        +'/'+tipoTran
                        +'/'+centroCosto
                        +'/'+estado
                        +'/'+busqueda
                        +'/'+desde
                        +'/'+hasta,
                        "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                        [
                            { "data": "num_transaccion" },
                            { "data": "transaccion" },
                            { "data": "periodo" },
                            { "data": "ind_num_documento_referencia"},
                            { "data": "estatus"},
                            { "orderable": false,"data": "acciones", width:150}
                        ]
                );
            } else {

                $('#dataTablaJson').DataTable().ajax.url(urlListado
                    +'/'+dependencia
                    +'/'+tipoTran
                    +'/'+centroCosto
                    +'/'+estado
                    +'/'+busqueda
                    +'/'+desde
                    +'/'+hasta
                ).load();
            }
        }

        listado(1);

        var url = '{$_Parametros.url}modLG/almacen/transaccionCONTROL/crearModificarTransaccionMET';
        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idTran: 0 }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idTran: $(this).attr('idTran')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idTran: $(this).attr('idTran'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });
        $('#dataTablaJson tbody').on( 'click', '.eliminar', function () {
            var idTran = $(this).attr('idTran');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modLG/almacen/transaccionCONTROL/eliminarTransaccionMET';
                $.post(url, { idTran: idTran },function(dato){
                    if(dato['status']=='ok'){
                        $(document.getElementById('idTran'+dato['id'])).remove();
                        swal("Eliminado!", "la Transaccion fue eliminada satisfactoriamente.", "success");
                    }else{
                        swal("Error!", dato['mensaje'] , "error");
                    }
                },'json');
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.imprimir', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            var url ="{$_Parametros.url}modLG/almacen/transaccionCONTROL/wizardPdfMET";
            $.post(url, { idTran: $(this).attr('idTran'), ver: 1}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.reversar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post('{$_Parametros.url}modLG/almacen/transaccionCONTROL/reversarTransaccionMET', { idTran: $(this).attr('idTran')}, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('.buscar').click(function () {
            listado(2);
        });

    });
</script>