<form action="{$_Parametros.url}modLG/maestros/unidadesCONTROL/crearModificarUnidadesMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idUnidades)}{$idUnidades}{/if}" name="idUnidades"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="form-group col-lg-9" id="descUnidadesError">
                        <input type="text" class="form-control" name="form[alphaNum][descUnidades]" value="{if isset($formBDU.ind_descripcion)}{$formBDU.ind_descripcion}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="descUnidades">
                        <label for="descUnidades"><i class="md md-border-color"></i>Descripcion</label>
                    </div>
                    <div class="form-group col-lg-3" id="codigoError">
                        <input type="text" class="form-control" name="form[alphaNum][codigo]" value="{if isset($formBDU.ind_cod_unidad)}{$formBDU.ind_cod_unidad}{/if}" {if isset($ver) and $ver==1} disabled {/if} id="codigo">
                        <label for="codigo"><i class="md md-border-color"></i>Código</label>
                    </div>
                </div>
                    <div class="col-lg-12">
                    <div class="form-group floating-label col-lg-10" id="tipoMediError">
                        <select class="form-control select2-list select2"  id="tipoMedi" name="form[int][tipoMedi]" {if isset($ver) and $ver==1} disabled {/if}>
                            <option value=""></option>
                            {foreach item=tm from=$TIPOMED}
                                {if isset($formBDU.fk_a006_num_miscelaneos_tipo_medida) and $tm.pk_num_miscelaneo_detalle==$formBDU.fk_a006_num_miscelaneos_tipo_medida}
                                    <option value="{$tm.pk_num_miscelaneo_detalle}" selected>{$tm.ind_nombre_detalle}</option>
                                {else}
                                    <option value="{$tm.pk_num_miscelaneo_detalle}">{$tm.ind_nombre_detalle}</option>
                                {/if}
                            {/foreach}
                        </select>
                        <label for="tipoMedi"><i class="md md-border-color"></i>Tipo de Medida</label>
                    </div>
                    <div class="form-group floating-label col-lg-2">
                    <label class="checkbox-styled">
                        <span>Estatus</span>
                        <input type="checkbox" name="form[txt][estadoUnidades]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formBDU.num_estatus)} {if $formBDU.num_estatus==1}checked{/if}{else}checked{/if} >
                    </label>
                </div>
                </div>
                <div class="col-lg-12">
                    <div style="background-color: #0aa89e; border-color: #0aa89e; color: #ffffff; text-align: center;">
                        <header>Unidades de Conversión</header>
                    </div>
                    <table class="table table-striped no-margin" id="contenidoTabla" style="width: 100%;display:block;overflow: auto;">
                        <thead>
                        <tr>
                            <th style="width: 20%;">Código</th>
                            <th style="width: 60%;">Cantidad</th>
                            <th style="width: 10%;">Estado</th>
                            <th style="width: 10%;">Borrar</th>
                        </tr>
                        </thead>
                        <tbody id="conversion" style="height: 200px;display: inline-block;width: 100%;overflow: auto;">
                        {$cont = 0}
                        {foreach item=conversion from=$formBDUC}
                            <input type="hidden" name="form[alphaNum][idUC][{$cont}]" value="{$conversion.pk_num_unidad_conversion}">
                            <tr>
                                <td style="width: 20%;">
                                    <input type="text" name="form[alphaNum][codigoC][{$cont}]" placeholder="Código" class="form-control" maxlength="3" value="{$conversion.ind_cod_unidad_conversion}" {if isset($ver) and $ver==1} disabled {/if}>
                                </td>
                                <td style="width: 60%;">
                                    <input type="text" name="form[int][cantidadC][{$cont}]" placeholder="Cantidad (0.00)" class="form-control" value="{$conversion.num_cantidad}" {if isset($ver) and $ver==1} disabled {/if}>
                                </td>
                                <td style="width: 10%; vertical-align: middle;">
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                        <input type="checkbox" {if isset($conversion.num_estatus) and $conversion.num_estatus==1} checked {/if} value="1"
                                               name="form[int][estadoC][{$cont}]" {if isset($ver) and $ver==1} disabled {/if}>
                                            <span></span>
                                        </label>
                                    </div>
                                </td>
                                <td style="width: 10%;vertical-align: middle;">
                                    <button type="button" class="btn ink-reaction btn-raised btn-xs btn-danger" onclick="$(this).parent().parent().remove()" {if isset($ver) and $ver==1} disabled {/if}><i class="md md-delete"></i></button>
                                </td>
                            </tr>
                            {$cont = $cont + 1}
                        {/foreach}
                        <tfoot>
                        <tr>
                            <td colspan="3">
                                <div class="col-lg-offset-5">
                                    <button type="button" id="nuevoCampo" class="btn btn-info ink-reaction btn-raised" {if isset($ver) and $ver==1} disabled {/if}>
                                        <i class="md md-add"></i> Insertar Conversión
                                    </button>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                        </tbody>
                    </table>
                </div>
                <div class="form-group floating-label col-lg-12" id="usuUnidadesError">
                    <input type="text" class="form-control disabled" disabled value="{if isset($formBDU.ind_usuario)}{$formBDU.ind_usuario}{/if}">
                    <label for="usuUnidades"><i class="md md-border-color"></i>Ultimo Usuario</label>
                </div>
                <div class="form-group floating-label col-lg-12" id="fecUnidadesError">
                    <input type="text" class="form-control disabled" disabled value="{if isset($formBDU.fec_ultima_modificacion)}{$formBDU.fec_ultima_modificacion}{/if}">
                    <label for="fecUnidades"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                </div>
        </div>
        <!--end .col -->
    </div>
    <!--end .row -->
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idUnidades)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css("width", "40%");
    $("#formAjax").submit(function(){
        return false;
    });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Tipo de Transaccion fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='errorConversion'){
                    app.metValidarError(dato,'Disculpa. los datos en las unidades de conversión son incorrectos, por favor verifique');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Tipo de Transaccion fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
        $('#nuevoCampo').on('click',function () {
            var tr = $("#conversion > tr");
            var numTr = tr.length;
            var campo =
                    '<tr>' +
                    '<td style="width: 20%;"><input type="text" name="form[alphaNum][codigoC]['+numTr+']" placeholder="Código" class="form-control" maxlength="3">' +
                    '</td>' +
                    '<td style="width: 60%;"><input type="text" name="form[int][cantidadC]['+numTr+']" placeholder="Cantidad (0.00)" class="form-control">' +
                    '</td>' +
                    '<td style="width: 10%; vertical-align: middle;">' +
                    '<div class="checkbox checkbox-styled">' +
                    '<label>' +
                    '<input type="checkbox" value="1" name="form[int][estadoC]['+numTr+']" >' +
                    '<span></span>' +
                    '</label>' +
                    '</div>' +
                    '</td>' +
                    '<td style="width: 10%;vertical-align: middle;"><button type="button" class="btn ink-reaction btn-raised btn-xs btn-danger" onclick="$(this).parent().parent().remove()"><i class="md md-delete"></i></button></td>' +
                    '</tr>';
            $('#conversion').append(campo);
        });
    });
</script>