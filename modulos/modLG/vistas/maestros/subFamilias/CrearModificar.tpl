<form action="{$_Parametros.url}modLG/maestros/subFamiliasCONTROL/crearModificarSubFamiliasMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idSubFamilias)}{$idSubFamilias}{else}0{/if}" name="idSubFamilias"/>
    <div class="modal-body">
        <div class="row">
            <div class="form-group col-lg-12">
                <div class="form-group col-lg-8" id="descError">
                    <input type="text" class="form-control" name="form[alphaNum][desc]" id="desc" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" {if isset($ver) and $ver==1} disabled {/if}>
                    <label for="desc"><i class="md md-border-color"></i>Descripcion</label>
                </div>
                <div class="form-group col-lg-2" id="codigoError">
                    <input type="text" class="form-control" name="form[alphaNum][codigo]" id="codigo" value="{if isset($formDB.ind_cod_subfamilia)}{$formDB.ind_cod_subfamilia}{/if}" {if isset($ver) and $ver==1} disabled {/if} maxlength="6">
                    <label for="codigo"><i class="md md-border-color"></i>Código</label>
                </div>
                <div class="form-group col-lg-2">
                    <label class="checkbox-styled">
                        <span>Estatus</span>
                        <input type="checkbox" name="form[int][estado]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_estatus)} {if $formDB.num_estatus==1}checked{/if}{else}checked{/if}>
                    </label>
                </div>
                <div class="form-group col-lg-12" id="familiasError">
                    <select id="familias" name="form[int][familias]" {if isset($ver) and $ver==1} disabled {/if}
                            class="form-control select2-list select2">
                        <option value=""></option>
                        {foreach item=condicion from=$familias}
                            {if isset($formDB.fk_lgb006_num_clase_familia) and $formDB.fk_lgb006_num_clase_familia == $condicion.pk_num_clase_familia}
                                <option value="{$condicion.pk_num_clase_familia}" selected>{$condicion.ind_cod_familia}--{$condicion.ind_descripcion}</option>
                            {else}
                                <option value="{$condicion.pk_num_clase_familia}">{$condicion.ind_cod_familia}--{$condicion.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="inventario"><i class="md md-border-color"></i>Familias</label>
                </div>
                <div class="form-group floating-label col-lg-12" id="usuSubFamiliasError">
                    <input type="text" class="form-control disabled" id="usuSubFamilias" disabled value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}">
                    <label for="usuSubFamilias"><i class="md md-border-color"></i>Ultimo Usuario</label>
                </div>
                <div class="form-group floating-label col-lg-12" id="fecSubFamiliasError">
                    <input type="text" class="form-control disabled" id="fecSubFamilias" disabled value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}">
                    <label for="fecSubFamilias"><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                </div>
            </div>
            <!--end .row -->
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario ha Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idSubFamilias)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $('.select2').select2({ allowClear: true });
    $('#modalAncho').css("width", "50%");
    $(document).ready(function() {
        var app = new  AppFunciones();
        $("#formAjax").submit(function () {
            return false;
        });
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'La SubFamilia fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'La SubFamilia fue guardada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });

    });
</script>