<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Listado de Cuentas</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable2" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Denominación</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=i from=$lista}
                            <tr>
                                <input type="hidden" class="persona"
                                       nombreCuenta="{$i.cod_cuenta} {$i.ind_descripcion}"
                                       idCuenta="{$i.pk_num_cuenta}">
                                <td>{$i.cod_cuenta}</td>
                                <td>{$i.ind_descripcion}</td>
                            </tr>
                        {/foreach}
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function() {
        $('#datatable2').DataTable({
            "dom": 'lCfrtip',
            "order": [],
            "colVis": {
                "buttonText": "Columnas",
                "overlayFade": 0,
                "align": "right"
            },
            "language": {
                "lengthMenu": 'Mostrar _MENU_',
                "search": '<i class="fa fa-search"></i>',
                "paginate": {
                    "previous": '<i class="fa fa-angle-left"></i>',
                    "next": '<i class="fa fa-angle-right"></i>'
                }
            }
        });
        $('#datatable2').on('click', 'tbody tr', function () {
//        $('#datatable2 tbody tr').click(function() {
            var input = $(this).find('input');
            {if $cual==1}
                $(document.getElementById('idC1')).val(input.attr('idCuenta'));
                $(document.getElementById('nombreC1')).val(input.attr('nombreCuenta'));
            {else}
                $(document.getElementById('idC2')).val(input.attr('idCuenta'));
                $(document.getElementById('nombreC2')).val(input.attr('nombreCuenta'));
            {/if}
            $('#cerrarModal2').click();
        });
    });
</script>