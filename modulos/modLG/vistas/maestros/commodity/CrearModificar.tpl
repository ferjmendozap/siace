<form action="{$_Parametros.url}modLG/maestros/commodityCONTROL/crearModificarCommodityMET" id="formAjax" method="post" class="form form-validate floating-label" novalidate="novalidate">
    <input type="hidden" value="1" name="valido"/>
    <input type="hidden" value="{if isset($idCommodity)}{$idCommodity}{/if}" name="idCommodity"/>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group floating-label col-lg-7" id="descError">
                    <input type="text" class="form-control" id="desc" name="form[alphaNum][desc]" value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}" {if isset($ver) and $ver==1} disabled {/if}>
                    <label for="desc"><i class="md md-border-color"></i>Descripcion</label>
                </div>
                <div class="form-group floating-label col-lg-3" align="center">
                    <label class="checkbox-styled">
                        <span>Presupuesto</span>
                        <input type="checkbox" name="form[txt][pres]" value="1" {if isset($ver) and $ver==1} disabled {/if} {if isset($formDB.num_flag_vericado_presupuesto) and $formDB.num_flag_vericado_presupuesto==1}{"checked"}{/if}>
                    </label>
                </div>
                <div class="form-group floating-label col-lg-2">
                    <label class="checkbox-styled">
                        <span>Estatus</span>
                        <input type="checkbox" name="form[txt][estado]" value="1" {if isset($ver) and $ver==1} disabled {/if}  {if isset($formDB.num_estatus)} {if $formDB.num_estatus==1}checked{/if}{else}checked{/if}>
                    </label>
                </div>
                <div class="form-group col-lg-8" id="clasificacionError">
                    <select class="form-control select2-list select2" id="partida" name="form[int][clasificacion]" {if isset($ver) and $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=clasificacion from=$selectClasificacion}
                            {if isset($formDB.fk_lgb017_num_clasificacion) and $formDB.fk_lgb017_num_clasificacion == $clasificacion.pk_num_clasificacion}
                                <option value="{$clasificacion.pk_num_clasificacion}" selected>{$clasificacion.ind_descripcion}</option>
                            {else}
                                <option value="{$clasificacion.pk_num_clasificacion}">{$clasificacion.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="partida"><i class="md md-border-color"></i>Clasificación</label>
                </div>
                <div class="form-group floating-label col-lg-4" id="codigoError">
                    <input type="text" class="form-control" id="codigo" name="form[alphaNum][codigo]" value="{if isset($formDB.ind_cod_commodity)}{$formDB.ind_cod_commodity}{/if}" disabled>
                    <label for="codigo"><i class="md md-border-color"></i>Código</label>
                </div>
                <div class="form-group col-lg-12" id="partidaError">
                    <label for="partida"><i class="md md-border-color"></i>Partida Presupuestaria</label>
                    <div class="col-lg-11">
                        <input type="hidden" name="form[int][partida]" id="idPartida" value="{if isset($formDB.fk_prb002_num_partida_presupuestaria)}{$formDB.fk_prb002_num_partida_presupuestaria}{/if}">
                        <input type="text" disabled class="form-control" id="nombrePartida" value="{if isset($formDB.nombrePartida)}{$formDB.nombrePartida}{/if}">
                    </div>
                    <div class="col-lg-1">
                        <button {if isset($ver) and $ver==1} disabled {/if}
                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                type="button"
                                title="Buscar Partida"
                                data-toggle="modal" data-target="#formModal2"
                                data-keyboard="false" data-backdrop="static"
                                titulo="Buscar Partida"
                                url="{$_Parametros.url}modLG/maestros/commodityCONTROL/partidaCuentaMET">
                            <i class="md md-search" style="color: #ffffff;"></i>
                        </button>
                    </div>

                </div>
                <div class="form-group col-lg-12" id="planError">
                    <label for="plan"><i class="md md-border-color"></i>Plan Cuenta</label>
                    <input type="hidden" name="form[int][plan]" id="idCuenta1" value="{if isset($formDB.fk_cbb004_num_plan_cuenta)}{$formDB.fk_cbb004_num_plan_cuenta}{/if}">
                    <input type="text" disabled class="form-control" id="nombreCuenta1" value="{if isset($formDB.nombreCuenta1)}{$formDB.nombreCuenta1}{/if}">
                </div>
                <div class="form-group col-lg-12" id="plan20Error">
                    <label for="plan20"><i class="md md-border-color"></i>Plan Cuenta Pub. 20</label>
                    <input type="hidden" name="form[int][plan20]" id="idCuenta2" value="{if isset($formDB.fk_cbb004_num_plan_cuenta_pub_veinte)}{$formDB.fk_cbb004_num_plan_cuenta_pub_veinte}{/if}">
                    <input type="text" disabled class="form-control" id="nombreCuenta2" value="{if isset($formDB.nombreCuenta2)}{$formDB.nombreCuenta2}{/if}">
                </div>
                <div class="form-group col-lg-12" id="unidadError">
                    <select class="form-control select2-list select2" id="unidad" name="form[int][unidad]" {if isset($ver) and $ver==1} disabled {/if}>
                        <option value=""></option>
                        {foreach item=post from=$unidad}
                            {if $post.pk_num_unidad==$formDB.fk_lgb004_num_unidad}
                                <option value={$post.pk_num_unidad} selected>{$post.ind_descripcion}</option>
                            {else}
                                <option value={$post.pk_num_unidad}>{$post.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="unidad"><i class="md md-border-color"></i>Unidad</label>
                </div>
                <div class="form-group col-lg-12" id="almError">
                    <select class="form-control select2-list select2" id="alm" name="form[int][alm]" {if isset($ver) and $ver==1} disabled {/if}>
                        {foreach item=alm from=$almacen}
                            {if $alm.num_flag_commodity==1}
                                <option value={$alm.pk_num_almacen} selected>{$alm.ind_descripcion}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label for="alm"><i class="md md-border-color"></i>Almacén</label>
                </div>

                <div class="form-group col-lg-12" id="fecError">
                    <input type="text" disabled class="form-control" value="{if isset($formDB.fec_verificado_presupuesto)}{$formDB.fec_verificado_presupuesto}{/if}">
                    <label><i class="fa fa-calendar"></i>Fecha de Verif. de Pres.</label>
                </div>
                <div class="form-group floating-label col-sm-12">
                    <label><i class="md md-people"></i>
                        Empleado Verifica Presupuesto</label>
                    <div class="form-group floating-label col-sm-6">
                        <input type="text" class="form-control disabled" placeholder="Nombre" id="personaResp" value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1} {$formDB.ind_apellido1}{/if}" disabled>
                    </div>
                    <div class="form-group floating-label col-sm-6">
                        <input type="text" class="form-control disabled" placeholder="Cedula" id="cedula" value="{if isset($formDB.ind_cedula_documento)}{$formDB.ind_cedula_documento}{/if}" disabled>
                    </div>
                </div>
                <div class="form-group floating-label col-lg-6">
                    <input type="text" class="form-control disabled" disabled value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}">
                    <label><i class="fa fa-calendar"></i>Ultima Fecha de Modificacion</label>
                </div>
                <div class="form-group floating-label col-lg-6">
                    <input type="text" class="form-control disabled" disabled value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}">
                    <label><i class="md md-border-color"></i>Ultimo Usuario</label>
                </div>
            </div>
            <!--end .col -->
        </div>
        <!--end .row -->
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised"
                descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal">
            <span class="glyphicon glyphicon-floppy-remove"></span> Cancelar
        </button>
        {if isset($ver) and $ver==1}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised" id="accion">
                {if isset($idCommodity)}
                    <span class="fa fa-edit"></span>
                    Modificar {else}
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    Guardar
                {/if}
            </button>
        {/if}
    </div>
</form>
<script type="text/javascript">
    $("#formAjax").submit(function(){
        return false;
    });
    $('#modalAncho').css("width", "40%");
    $('.select2').select2({ allowClear: true });
    $(document).ready(function () {
        var app = new  AppFunciones();
        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "60%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function (dato) {
                $('#ContenidoModal2').html(dato);
            });
        });
        $('#modalAncho').css("width", "50%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato) {
                if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson', 'El Commoditie fue modificado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }else if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson', 'El Commoditie fue guardado satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                }
            }, 'json');
        });
    });
</script>