<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class cargaInventarioControlador extends Controlador
{
    private $atInventario;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atInventario = $this->metCargarModelo('cargaInventario', 'procesos');
    }

    //Método para mostrar el formulario para la carga del inventario fisico
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para listar los inventarios fisicos realizados
    public function metListarRealizadas()
    {
        $this->atVista->assign('lista', $this->atInventario->metListarRealizadas());
        $this->atVista->metRenderizar('realizadas','modales');
    }

    //Método para cargar el inventario fisico
    public function metCrearCarga()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $ind = $this->metValidarFormArrayDatos('form','int');
        if ($ind != null) {
            $validacion = $ind;
        }
//        $validacion['in_array'] = in_array('error',$validacion['sreal']);
        if($validacion['sreal']=='error') {
            $validacion['sreal'] = '0';
        }
        if(in_array('error',$validacion)){
            $validacion['status']='error';
            echo json_encode($validacion);
            exit;
        }

        $orden=$this->atInventario->metListarRealizadas();
        foreach ($orden as $titulo => $valor) {
            if($orden[$titulo]['fec_mes']!=date('m')){
                $cantidad=$orden[$titulo]['num_proceso']+1;
            } else {
                $cantidad=$orden[$titulo]['num_proceso'];
            }
        }
        if(!isset($cantidad)){
            $cantidad=1;
        }

        $busqueda = $this->atInventario->metBucarRealizada($validacion['idItem']);

        if($busqueda){
            $id=$this->atInventario->metActualizarCarga($busqueda['pk_num_registro'],$validacion['slog'],$validacion['sreal']);
        } else {
            $id=$this->atInventario->metCrearCarga($cantidad,$validacion['slog'],$validacion['sreal'],$validacion['idItem']);
        }

        $validacion['status']='nuevo';

        if (is_array($id)) {
            foreach ($validacion as $titulo => $valor) {
                if(!is_array($validacion[$titulo])) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
            }
            $validacion['status'] = 'error';
            $validacion['id'] = $id;
            echo json_encode($validacion);
            exit;
        }

        echo json_encode($validacion);
        exit;

    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para modificar un dia a letras
    public function metDiasLetras($dia){

        if(strlen($dia)==1){
            $dia="0".$dia;
        }
        $diaLetras=array(
            '01'=>'uno',
            '02'=>'dos',
            '03'=>'tres',
            '04'=>'cuatro',
            '05'=>'cinco',
            '06'=>'seis',
            '07'=>'siete',
            '08'=>'ocho',
            '09'=>'nueve',
            '10'=>'diez',
            '11'=>'once',
            '12'=>'doce',
            '13'=>'trece',
            '14'=>'catorce',
            '15'=>'quince',
            '16'=>'dieciseis',
            '17'=>'diecisiete',
            '18'=>'dieciocho',
            '19'=>'diecinueve',
            '20'=>'veinte',
            '21'=>'veintiuno',
            '22'=>'veintidos',
            '23'=>'veintitres',
            '24'=>'veinticuatro',
            '25'=>'veinticinco',
            '26'=>'veintiseis',
            '27'=>'veintisiete',
            '28'=>'veintiocho',
            '29'=>'veintinueve',
            '30'=>'treinta',
            '31'=>'treinta y uno'
        );

        return $diaLetras[$dia];
    } // END FUNCTION

    //Método para modificar un mes a letras
    public function metMesLetras($mes){

        if(strlen($mes)==1){
            $mes="0".$mes;
        }
        $metMesLetras=array(
            '01'=>'Enero',
            '02'=>'Febrero',
            '03'=>'Marzo',
            '04'=>'Abril',
            '05'=>'Mayo',
            '06'=>'Junio',
            '07'=>'Julio',
            '08'=>'Agosto',
            '09'=>'Septiembre',
            '10'=>'Octubre',
            '11'=>'Noviembre',
            '12'=>'Diciembre'
        );

        return $metMesLetras[$mes];
    } // END FUNCTION

    //Método para imprimir los inventarios fisicos realizados
    public function metImprimirRealizados($anio,$num)
    {
        $consulta = $this->atInventario->metBuscarRealizadas($anio,$num);
        foreach ($consulta as $titulo=>$valor) {
            $realizado = $consulta[$titulo]['fk_rhb001_num_empleado_realiza'];
            $fecha = $consulta[$titulo]['fec_registro'];
        }
        $empleado = $this->atInventario->metEmpleado($realizado);
        define('ANIO',$anio);
        define('NUM',$num);
        define('NOM_COMPLETO',$empleado['NomCompleto']);
        define('FECHA',$fecha);

        $this->metObtenerLibreria('headerCargaInventarioRealizados','modLG');
        $this->atFPDF = new pdf('p','mm','A4');

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(1, 40);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetFont('Arial', '', 6);

        foreach ($consulta as $titulo=>$valor) {

            $this->atFPDF->SetDrawColor(0, 0, 0); $this->atFPDF->SetFillColor(255, 255, 255); $this->atFPDF->SetTextColor(0, 0, 0);

            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->SetWidths(array(15, 80, 25, 20, 20, 20, 20));
            $this->atFPDF->SetAligns(array('C', 'L', 'C','C', 'R', 'C','C'));

            $this->atFPDF->Row(array(
                $consulta[$titulo]['pk_num_item'],
                utf8_decode($consulta[$titulo]['ind_descripcion']),
                $consulta[$titulo]['unidad'],
                $consulta[$titulo]['ind_codigo_interno'],
                $consulta[$titulo]['num_stock_logico'],
                $consulta[$titulo]['num_stock_fisico'],
                number_format($consulta[$titulo]['num_stock_logico']-$consulta[$titulo]['num_stock_fisico'], 2, ',', '.')
                ));
        }

        #salida del pdf
        $this->atFPDF->Output('InventarioRealizado-'.$anio.'-'.$num.'.pdf', 'I');

    }

    //Método para imprimir el formato de los items
    public function metFormato()
    {

        $consulta = $this->atInventario->atItemsModelo->metListarItems();
        $empleadoId = $this->atInventario->metUsuarioEmpleado($this->atInventario->atIdUsuario);
        $empleadoNombre = $this->atInventario->metEmpleado($empleadoId['pk_num_empleado']);
        define('IMPRESO_POR',$empleadoNombre['NomCompleto']);

        $this->metObtenerLibreria('headerCargaInventario','modLG');
        $this->atFPDF = new pdf('p','mm','A4');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(1, 20);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetFont('Arial', '', 6);

        foreach ($consulta as $titulo=>$valor) {

            $this->atFPDF->SetDrawColor(0, 0, 0); $this->atFPDF->SetFillColor(255, 255, 255); $this->atFPDF->SetTextColor(0, 0, 0);

            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->SetWidths(array(20, 90, 20, 20, 30));
            $this->atFPDF->SetAligns(array('C', 'L', 'C','C', 'L'));
            $codItem = $this->metRellenarCeros($consulta[$titulo]['pk_num_item'],10);
            $codItemInterno = $this->metRellenarCeros($consulta[$titulo]['pk_num_item'],3);

            $this->atFPDF->Row(array(
                $codItem,
                utf8_decode($consulta[$titulo]['ind_descripcion']),
                $consulta[$titulo]['uni'],
                'P'.$codItemInterno,
                ''
            ));
        }

        #salida del pdf
        $this->atFPDF->Output('InventarioRealizado.pdf', 'I');

    }

}
