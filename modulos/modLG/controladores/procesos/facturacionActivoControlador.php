<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class facturacionActivoControlador extends Controlador
{
    private $atFacturacionActivoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atFacturacionActivoModelo = $this->metCargarModelo('facturacionActivo', 'procesos');
    }

    //Método para listar los activos por facturar
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('lista', $this->atFacturacionActivoModelo->metListaActivosLG());
        $this->atVista->metRenderizar('listado');
    }

    public function metObligaciones()
    {
        $idOrden = $this->metObtenerInt('idOrden');
        $this->atVista->assign('lista', $this->atFacturacionActivoModelo->metListaObligaciones($idOrden));
        $this->atVista->metRenderizar('obligaciones', 'modales');
    }

    public function metCrearFacturacionActivo()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idActivo = $this->metObtenerInt('idActivo');
        if($valido==1){
            $this->metValidarToken();

            $ind = $this->metValidarFormArrayDatos('form','int');

            $validacion = $ind;

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idActivo']=$idActivo;
            if($idActivo!=0){
                $id=$this->atFacturacionActivoModelo->metFacturar($validacion);
                $validacion['status']='nuevo';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }


            echo json_encode($validacion);
            exit;
        }
        if($idActivo!=0){
            $this->atVista->assign('formDB',$this->atFacturacionActivoModelo->metListaActivosLG($idActivo));
            $this->atVista->assign('idActivo',$idActivo);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');

    }
}
