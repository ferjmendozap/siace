<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

require_once ROOT. 'publico/procesoCompra/docxpresso/CreateDocument.php';
class desiertoControlador extends Controlador
{
    private $atDesierto;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atDesierto = $this->metCargarModelo('desierto', 'compras');
    }

    //Método para listar los procedimientos a declarar desierto
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('opcion', "crear");
        $this->atVista->metRenderizar('listado');
    }

    //Método para listar los procedimientos desiertos
    public function metListadoDesierto()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('opcion', "generar");
        $this->atVista->metRenderizar('listado');
    }

    //Método para modificar un mes a letras
    public function metMesLetras($mes){

        if(strlen($mes)==1){
            $mes="0".$mes;
        }
        $metMesLetras=array(
            '01'=>'Enero',
            '02'=>'Febrero',
            '03'=>'Marzo',
            '04'=>'Abril',
            '05'=>'Mayo',
            '06'=>'Junio',
            '07'=>'Julio',
            '08'=>'Agosto',
            '09'=>'Septiembre',
            '10'=>'Octubre',
            '11'=>'Noviembre',
            '12'=>'Diciembre'
        );

        return $metMesLetras[$mes];
    } // END FUNCTION

    //Método para modificar un dia a letras
    public function metDiasLetras($dia){

        if(strlen($dia)==1){
            $dia="0".$dia;
        }
        $diaLetras=array(
            '01'=>'uno',
            '02'=>'dos',
            '03'=>'tres',
            '04'=>'cuatro',
            '05'=>'cinco',
            '06'=>'seis',
            '07'=>'siete',
            '08'=>'ocho',
            '09'=>'nueve',
            '10'=>'diez',
            '11'=>'once',
            '12'=>'doce',
            '13'=>'trece',
            '14'=>'catorce',
            '15'=>'quince',
            '16'=>'dieciseis',
            '17'=>'diecisiete',
            '18'=>'dieciocho',
            '19'=>'diecinueve',
            '20'=>'veinte',
            '21'=>'veintiuno',
            '22'=>'veintidos',
            '23'=>'veintitres',
            '24'=>'veinticuatro',
            '25'=>'veinticinco',
            '26'=>'veintiseis',
            '27'=>'veintisiete',
            '28'=>'veintiocho',
            '29'=>'veintinueve',
            '30'=>'treinta',
            '31'=>'treinta y uno'
        );

        return $diaLetras[$dia];
    } // END FUNCTION

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para declarar desierto
    public function metCrearDesierto()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idActa = $this->metObtenerInt('idActa');
        $idInfor = $this->metObtenerInt('idInfor');
        $opcion = $this->metObtenerAlphaNumerico('opcion');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            if($opcion=="crear"){
                $id=$this->atDesierto->metDeclararDesierto($idActa,$idInfor);
//                $id=1;
                $validacion['status']='nuevo';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idActa']=$idActa;
            $validacion['idInfor']=$idInfor;
            echo json_encode($validacion);
            exit;

        }
        if($idInfor!=0 && $opcion=="crear"){
            $this->atVista->assign('informe',$this->atDesierto->metMostrarInformeRecomendacion($idInfor));
            $this->atVista->assign('idInfor',$idInfor);
            $this->atVista->assign('idActa',$idActa);
            $this->atVista->assign('opcion',$opcion);
            $this->atVista->assign('ver',$ver);
            $this->atVista->metRenderizar('CrearModificar','modales');
        }

    }

    //Método para generar el documento del desierto
    public function metGenerar() {

        $idInfor = $this->metObtenerInt('idInfor');
        $this->atDocx = new Docxpresso\CreateDocument(array('template' => ROOT . 'publico' . DS . 'procesoCompra' . DS . 'informe-desierto-plantilla.odt'));
        #busco en la ruta el odt que me sirve de formato

        $informe= $this->atDesierto->metMostrarDatosGenerar($idInfor);

        $diaRecomen = substr($informe['fec_creacion'],8,2);
        $mesRecomen = $this->metMesLetras(substr($informe['fec_creacion'],5,2));
        $anioRecomen = substr($informe['fec_creacion'],0,4);


        $dia = substr($informe['fec_modif'],8,2);
        $mes = $this->metMesLetras(substr($informe['fec_modif'],5,2));
        $anio = substr($informe['fec_modif'],0,4);

        $contralorProvicional=$this->atDesierto->metBuscarCargo('CONTRALOR DEL ESTADO MONAGAS (P)');

        if($contralorProvicional['ind_cedula_documento']){
            $contralor = ucwords($contralorProvicional['ind_nombre1']." ".$contralorProvicional['ind_apellido1']);
            $cedula = $contralorProvicional['ind_cedula_documento'];
        } else {
            $contralor = '';
            $cedula = '';
        }
//Colocar logo en los odt
        $headerStyle = array('style' => 'min-height: 3cm');
        $urlImg = ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . 'CES.jpg';
        $urlImg2 = ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . 'LOGOSNCF.png';
        $rowStyle = array('style' => 'min-height: 25px');
        $cellStyle = array('style' => 'margin-left: 0px; vertical-align: middle;');
        $this->atDocx->header($headerStyle)
            ->table(array('grid' => array('270px','280px')))
            ->row($rowStyle)
            ->cell($cellStyle)->paragraph()->image(array('src' => $urlImg, 'style' => 'width:100px; height:80px;'))
            ->cell($cellStyle)->paragraph()->image(array('src' => $urlImg2, 'style' => 'margin-left: 170px; width:320px; height:320px;'))
        ;
//Colocar logo en los odt

        $this->metReemplazar('nroVisual',$this->metRellenarCeros($informe['num_procedimiento'],3).'-'.$informe['num_anio']);
        $this->metReemplazar('contralor',$contralor);
        $this->metReemplazar('cedula',$cedula);
        $this->metReemplazar('diaRecomen',$diaRecomen);
        $this->metReemplazar('mesRecomen',$mesRecomen);
        $this->metReemplazar('anioRecomen',$anioRecomen);
        $this->metReemplazar('objetoConsulta',$informe['ind_objeto_consulta']);
        $this->metReemplazar('numeral',$informe['ind_articulo_numeral']);
        $this->metReemplazar('dia',$this->metDiasLetras($dia));
        $this->metReemplazar('diaNumero',$dia);
        $this->metReemplazar('mes',$mes);
        $this->metReemplazar('anio',$anio);

        $this->atDocx->render(ROOT . 'publico' .DS.'procesoCompra' .DS.'declararDesierto'.$idInfor.'.odt');
        $validacion['idInfor']=$idInfor;
        $validacion['ruta']='.'.DS.'publico' .DS.'procesoCompra' .DS.'declararDesierto'.$idInfor.'.odt';
        echo json_encode($validacion);
        exit;
    }

    //Método para reemplazar las variables en el documento a generar ODT
    public function metReemplazar($nombre,$valor,$extra=false){
        if($extra==1) {
            $this->atDocx->replace(array($nombre => array('value' => $valor)), array('block-type' => true));
        } else {
            $this->atDocx->replace(array($nombre => array('value' => $valor)));
        }
    }

    //Método para paginar los desiertos
    public function metJsonDataTabla($opcion)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  recomendacion.*,
                  evaluacion.ind_cod_evaluacion,
                  acta.pk_num_acta_inicio,
                  case recomendacion.ind_estado
                      when 'PR' then 'Preparado'
                      when 'RV' then 'Revisado'
                      when 'AP' then 'Aprobado'
                      when 'AN' then 'Anulado'
                  end as ind_estado
                FROM
                  lg_b012_informe_recomendacion AS recomendacion
                INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
                INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
                INNER JOIN a006_miscelaneo_detalle AS detalle ON recomendacion.fk_a006_num_miscelaneo_detalle_tipo_adjudicacion = detalle.pk_num_miscelaneo_detalle
                INNER JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON acta.pk_num_acta_inicio = acta_requerimiento.fk_lgb009_num_acta_inicio
                INNER JOIN lg_b001_requerimiento AS requerimiento ON acta_requerimiento.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
                INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = requerimiento.fk_a004_num_dependencia
                AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                WHERE 1 
                  ";
        if($opcion=='crear'){
            $sql .= "
                    AND detalle.cod_detalle = 'DS' 
                    AND recomendacion.ind_estado = 'AP' 
                    AND acta.ind_estado <> 'DS' ";
        } elseif($opcion=='generar'){
            $sql .= " 
                    AND recomendacion.ind_estado = 'DS'
                    AND acta.ind_estado = 'DS' ";
        }
        if ($busqueda['value']) {
            $sql .= " AND
                        (evaluacion.ind_cod_evaluacion LIKE '%$busqueda[value]%' OR
                        recomendacion.ind_asunto LIKE '%$busqueda[value]%' OR
                        recomendacion.ind_objeto_consulta LIKE '%$busqueda[value]%' OR
                        recomendacion.ind_estado LIKE '%$busqueda[value]%'
                        )  
                        ";
        }
        $sql .= " GROUP BY recomendacion.pk_num_informe_recomendacion ";
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_cod_evaluacion','ind_asunto','ind_objeto_consulta','ind_estado');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_informe_recomendacion';
        #construyo el listado de botones

        $camposExtra = array(
            'pk_num_acta_inicio'
        );
        if (in_array('LG-01-01-03-07-01-N',$rol) AND $opcion=='crear') {
            $campos['boton']['Crear'] = "
                <button accion='crear' title='Declarar Desierto'
                        class='accion logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        idInfor='$clavePrimaria'
                        idActa='pk_num_acta_inicio'
                        titulo='Declarar Desierto'
                        data-toggle='modal' data-target='#formModal' id='crear'>
                    <i class='md md-create'></i>
                </button>
                "
            ;
        } else {
            $campos['boton']['Crear'] = false;
        }

        if (in_array('LG-01-01-03-07-02-G',$rol) AND $opcion=='generar') {
            $campos['boton']['Generar'] = "
                <button accion='generar' title='Generar Informe'
                        idInfor='$clavePrimaria'
                        class='generar logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        descipcion='El Usuario ha Generado el Desierto Nro. $clavePrimaria'
                        titulo='Generar Informe'
                        id='generar'>
                    <i class='fa fa-print'></i>
                </button>
                "
            ;
        } else {
            $campos['boton']['Generar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }
}
