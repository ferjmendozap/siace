<?php

/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Guidmar Espinoza     | g.espinoza@contraloriamonagas.gob.ve    | 0414-1913443      | MONAGAS
 * | 2 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
class proveedorControlador extends Controlador
{
    private $atProveedorModelo;
    private $atMiscelaneoModelo;
    private $atPaisModelo;
    private $atDocumentoModelo;
    private $atTipoServicioModelo;
    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        #se carga el Modelo.
        $this->atProveedorModelo = $this->metCargarModelo('proveedor', 'maestros');
    }

    //Método para listar los proveedores
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado', $this->atProveedorModelo->metListarProveedor());
        $this->atVista->metRenderizar('listado');
    }

    //Método para listar las personas
    public function metPersona($persona)
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('persona', $persona);
        $this->atVista->metRenderizar('personas', 'modales');
    }

    //Método para listar las personas
    public function metPersonaCrear()
    {
        $complementoJs =array(
            'inputmask/jquery.inputmask.bundle.min',
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementoJs);

        $this->atVista->metRenderizar('nuevaPersona','modales');
    }

    //Método para listar los servicios o documentos
    public function metServiciosDocumentos($servDoc)
    {
        if($servDoc=='servicios') {
            $this->atVista->assign('lista', $this->atProveedorModelo->atTipoServicioModelo->metServiciosListar());
            $tr = $this->metObtenerInt('tr2');
            $this->atVista->assign('servicio', $servDoc);
        } else if($servDoc=='documentos'){
            $this->atVista->assign('lista', $this->atProveedorModelo->atDocumentoModelo->metDocumentoListar());
            $tr = $this->metObtenerInt('tr1');
            $this->atVista->assign('doc', $servDoc);
        }
        $this->atVista->assign('tr', $tr);
        $this->atVista->metRenderizar($servDoc, 'modales');
    }

    //Método para crear y/o modificar los proveedores
    public function metCrearModificar()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'inputmask/jquery.inputmask.bundle.min',
            'mask/jquery.mask',
            'select2/select2.min',
            'JPEGCam/webcam'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js = array(
            'Aplicacion/appFunciones',
            'modRH/modRHFunciones',
            'materialSiace/core/demo/DemoFormWizard');
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $idProveedor = $this->metObtenerInt('idProveedor');
        $idPersona = $this->metObtenerInt('idPersona');
        $valido = $this->metObtenerInt('valido');
        $ver = $this->metObtenerInt('ver');
        if ($valido == 1) {
            $this->metValidarToken();
            $int = $this->metObtenerInt('form','int');
            $txt = $this->metObtenerTexto('form','txt');
            if (!isset($int['ind_snc'])) {
                $int['ind_snc'] = "0";
            }
            $dirE = $this->metObtenerFormulas('dirE');
            $Excceccion1 = array();
            if ($txt['tipopersona'] == 'J' or $txt['tipopersona'] == 'F') {
                $Excceccion1 = array('apellido1');
            }

            $Excceccion2 = array();
            if ($int['ind_snc'] == 0) { //indica que es persona natural
                $Excceccion2 = array('ind_snc', 'nroSNC', 'fSNC', 'fValSNC', 'condicion', 'nivel', 'calificacion', 'capacidad');
            }

            $ExcceccionGeneral = array('nombre2', 'apellido2', 'dirS', 'diasPago', 'registro', 'licencia', 'fecConst', 'representante', 'contacto', 'num_estatus', 'dirF', 'idDir');

            $Excceccion = array_merge($Excceccion1,$Excceccion2,$ExcceccionGeneral);

            $formTxt = $this->metValidarFormArrayDatos('form', 'txt', $Excceccion);
            $formInt = $this->metValidarFormArrayDatos('form', 'int', $Excceccion);
            $formAlphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum', $Excceccion);
            if ($formTxt != null && $formInt == null && $formAlphaNum == null) {
                $validacion = $formTxt;
            } elseif ($formTxt == null && $formInt != null && $formAlphaNum == null) {
                $validacion = $formInt;
            } elseif ($formTxt == null && $formInt == null && $formAlphaNum != null) {
                $validacion = $formAlphaNum;
            } elseif ($formTxt == null && $formInt != null && $formAlphaNum != null) {
                $validacion = array_merge($formAlphaNum, $formInt);
            } elseif ($formTxt != null && $formInt != null && $formAlphaNum == null) {
                $validacion = array_merge($formTxt, $formInt);
            } elseif ($formTxt != null && $formInt == null && $formAlphaNum != null) {
                $validacion = array_merge($formTxt, $formAlphaNum);
            } else {
                $validacion = array_merge($formAlphaNum, $formInt, $formTxt);
            }
            if ($validacion['tipopersona']=='J' or $validacion['tipopersona']=='F') {
                $validacion['ind_estado_civil'] = null;
            }
            if (!isset($validacion['num_estatus'])) {
                $validacion['num_estatus'] = "0";
            }
            if (!isset($validacion['nombre2'])) {
                $validacion['nombre2'] = null;
            }
            if (!isset($validacion['apellido1'])) {
                $validacion['apellido1'] = null;
            }
            if (!isset($validacion['apellido2'])) {
                $validacion['apellido2'] = null;
            }
            if ($validacion['fecConst']==false) {
                $validacion['fecConst'] = null;
            }
            if ($validacion['representante']=='0') {
                $validacion['representante'] = null;
            }
            if ($validacion['contacto']=='0') {
                $validacion['contacto'] = null;
            }
            if (!isset($validacion['ind_snc'])) {
                $validacion['ind_snc'] = "0";
                $validacion['nroSNC'] = null;
                $validacion['fSNC'] = null;
                $validacion['fValSNC'] = null;
                $validacion['calificacion'] = null;
                $validacion['condicion'] = null;
            }
            if (!isset($validacion['calificacion'])) {
                $validacion['calificacion'] = null;
            }
            if (!isset($validacion['calificacion'])) {
                $validacion['calificacion'] = null;
            }
            $validacion['nivel'] = null;
            $validacion['capacidad'] = null;
            if (isset($validacion['dCant'])) {
                $contador=1;
                while ($contador<=$validacion['dCant']) {
                    $nom1="dId".$contador;
                    $validacion['dId'][$contador] = $validacion[$nom1];
                    $contador=$contador+1;
                }
            } else {
                $validacion['dCant'] = "0";
                $validacion['dId'] = "0";
            }

            if (isset($validacion['sCant'])) {
                $contador=1;
                while ($contador<=$validacion['sCant']) {
                    $nom1="sId".$contador;
                    $validacion['sId'][$contador] = $validacion[$nom1];
                    $contador=$contador+1;
                }
            } else {
                $validacion['sCant'] = "0";
                $validacion['sId'] = "0";
            }

            if (isset($validacion['cant'])) {
                $contador=1;
                while ($contador<=$validacion['cant']) {
                    if(!isset($validacion['pk_num_telefono'][$contador])) $validacion['pk_num_telefono'][$contador]="0";
                    if(!isset($validacion['telEmer'][$contador])) $validacion['telEmer'][$contador]="0";
                    $contador=$contador+1;
                }
            } else {
                $validacion['ind_telefono'] = "0";
                $validacion['telEmer'] = "0";
                $validacion['cant'] = "0";
                $validacion['pk_num_telefono'] = "0";
            }

            $val = $this->metValidarCorreo($dirE);

            if($val==0 AND isset($dirE)){
                $validacion['dirE'] = 'error';
            } elseif(!isset($dirE)){
                $validacion['dirE'] = NULL;
            } else {
                $validacion['dirE'] = $dirE;
            }

            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            if ($idProveedor == 0) {
                $id = $this->atProveedorModelo->metCrearProveedor(
                    $validacion['docfiscal'],$validacion['nombre1'],$validacion['nombre2'],$validacion['apellido1'],
                    $validacion['apellido2'],$validacion['sexo'],$validacion['nacionalidad'],$validacion['ind_estado_civil'],
                    $validacion['dirE'],$validacion['tipopersona'],
                    $validacion['num_estatus'],$validacion['fk_a010_num_ciudad'],$validacion['ind_telefono'],$validacion['telEmer'],
                    $validacion['cant'],$validacion['dCant'],$validacion['sCant'],$validacion['tipoPago'],
                    $validacion['formaPago'],$validacion['dId'],$validacion['sId'],$validacion['diasPago'],
                    $validacion['registro'],$validacion['licencia'],$validacion['fecConst'],$validacion['ind_snc'],
                    $validacion['nroSNC'],$validacion['fSNC'],$validacion['fValSNC'],$validacion['condicion'],
                    $validacion['calificacion'],$validacion['nivel'],$validacion['capacidad'],$validacion['representante'],
                    $validacion['contacto'],$validacion['dirF']);
                $validacion['status'] = 'nuevo';
            } else {
                $id = $this->atProveedorModelo->metModificarProveedor(
                    $validacion['docfiscal'],$validacion['nombre1'],$validacion['nombre2'],$validacion['apellido1'],
                    $validacion['apellido2'],$validacion['sexo'],$validacion['nacionalidad'],$validacion['ind_estado_civil'],
                    $validacion['dirE'],$validacion['tipopersona'],
                    $validacion['num_estatus'],$validacion['fk_a010_num_ciudad'],$validacion['ind_telefono'],$validacion['telEmer'],
                    $validacion['cant'],$validacion['pk_num_telefono'],$validacion['dCant'],$validacion['sCant'],
                    $validacion['tipoPago'],$validacion['formaPago'],$validacion['dId'],$validacion['sId'],
                    $validacion['diasPago'],$validacion['registro'],$validacion['licencia'],$validacion['fecConst'],
                    $validacion['ind_snc'],$validacion['nroSNC'],$validacion['fSNC'],$validacion['fValSNC'],
                    $validacion['condicion'],$validacion['calificacion'],$validacion['nivel'],$validacion['capacidad'],
                    $validacion['representante'],$validacion['contacto'],$idProveedor,$idPersona,$validacion['idDir'],$validacion['dirF']
                );
                $validacion['status'] = 'modificar';
            }

            $validacion['id'] = $id;
            $validacion['idProveedor'] = $idProveedor;
            if($id=='error'){
                $validacion['id2'] = 'error';
                $validacion['status'] = 'errorPersona';
                echo json_encode($validacion);
                exit;
            }
            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            if($id!=0){
                $estado = $validacion['status'];
                $validacion = $this->atProveedorModelo->metBuscarProveedor($id);
                $validacion['status']=$estado;
            }
            $validacion['idProveedor'] = $id;
            echo json_encode($validacion);
            exit;
        }
        if ($idProveedor != 0) {
            $this->atVista->assign('formDB',$this->atProveedorModelo->metBuscarProveedor($idProveedor));
            $this->atVista->assign('telefono',$this->atProveedorModelo->metListarTelefonos($idPersona));
            $this->atVista->assign('tipoServicio', $this->atProveedorModelo->metServiciosProveedor($idProveedor));
            $this->atVista->assign('documento', $this->atProveedorModelo->metDocumentoProveedor($idProveedor));
            $this->atVista->assign('ver',$ver);
            $this->atVista->assign('idProveedor', $idProveedor);
        }

        $this->atVista->assign('selectEstadoCivil', $this->atProveedorModelo->atMiscelaneoModelo->metMostrarSelect('EDOCIVIL'));
        $this->atVista->assign('selectCondSNC', $this->atProveedorModelo->atMiscelaneoModelo->metMostrarSelect('IRCN'));
        $this->atVista->assign('selectTipoPers', $this->atProveedorModelo->atMiscelaneoModelo->metMostrarSelect('TipoPers'));
        $this->atVista->assign('selecttipoPago', $this->atProveedorModelo->atMiscelaneoModelo->metMostrarSelect('TDPLG'));
        $this->atVista->assign('selectformaPago', $this->atProveedorModelo->atMiscelaneoModelo->metMostrarSelect('FDPLG'));
        $this->atVista->assign('listadoPais', $this->atProveedorModelo->atPaisModelo->metListarPais());
        $this->atVista->metRenderizar('CrearModificar', 'modales');
    }

    //Método para eliminar los proveedores
    public function metEliminarProveedor()
    {
        $idProveedor = $this->metObtenerInt('idProveedor');
        $idPersona = $this->metObtenerInt('idPersona');
        if($idProveedor!=0){
            $id=$this->atProveedorModelo->metEliminarProveedor($idProveedor,$idPersona);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Proveedor se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idProveedor
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para eliminar los servicios
    public function metEliminarServicio()
    {
        $id1 = $this->metObtenerInt('id1');
        $id2 = $this->metObtenerInt('id2');
        if($id1!=0 and $id2!=0){
            $id=$this->atProveedorModelo->metEliminarServicio($id1,$id2);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Servicio se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para eliminar los documentos
    public function metEliminarDocumento()
    {
        $id1 = $this->metObtenerInt('id1');
        $id2 = $this->metObtenerInt('id2');
        if($id1!=0 and $id2!=0){
            $id=$this->atProveedorModelo->metEliminarDocumento($id1,$id2);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Documento se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para eliminar los telefonos
    public function metEliminarTelefono()
    {
        $idTelf = $this->metObtenerInt('id');
        if($idTelf!=0){
            $id=$this->atProveedorModelo->metEliminarTelefono($idTelf);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Teléfono se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para validar los correos
    public function metValidarCorreo($correo)
    {
        if (filter_var($correo, FILTER_VALIDATE_EMAIL)) {
            return 1;
        } else {
            return 0;
        }
    }

    //Método para validar con el seniat
    public function metObtenetInfoRifSeniat() {
        $rif = $this->metObtenerAlphaNumerico('docfiscal');
        $rif = str_replace('-', '', strtoupper($rif));
        $url = 'http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=';
        $validacion['nombre'] = '';
        $validacion['rif'] = $rif;
        if ($this->metValidarRif($rif)) {
            if(function_exists('curl_init')) {
                $url .= $rif;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $result = curl_exec ($ch);
                if ($result) {
                    if(substr($result,0,1)!= '<' ) {
                        $validacion['code_result'] = "5";
                    }
                    $xml = simplexml_load_string($result);
                    if(!is_bool($xml)) {
                        $elements = $xml->children('rif');
                        $seniat = array();
                        foreach($elements as $key => $node) {
                            $index = strtolower($node->getName());
                            $seniat[$index] = (string)$node;
                        }
                        $validacion['code_result'] = "4";
                        $validacion['nombre'] = $seniat['nombre'];
                    }
                } else {
                    $validacion['code_result'] = "3";
                }
            } else {
                $validacion['code_result'] = "2";
            }
        } else {
            $validacion['code_result'] = "1";
        }
        echo $validacion['code_result']."--".$validacion['nombre']."--".$validacion['rif'];
        exit;
    }

    //Método para validar el rif
    private function metValidarRif($rif) {
        $retorno = preg_match("/^([VEJPG]{1})([0-9]{9}$)/", $rif);

        if ($retorno) {
            $digitos = str_split($rif);

            $digitos[8] *= 2;
            $digitos[7] *= 3;
            $digitos[6] *= 4;
            $digitos[5] *= 5;
            $digitos[4] *= 6;
            $digitos[3] *= 7;
            $digitos[2] *= 2;
            $digitos[1] *= 3;

            // Determinar dígito especial según la inicial del RIF
            // Regla introducida por el SENIAT
            switch ($digitos[0]) {
                case 'V':
                    $digitoEspecial = 1;
                    break;
                case 'E':
                    $digitoEspecial = 2;
                    break;
                case 'J':
                    $digitoEspecial = 3;
                    break;
                case 'P':
                    $digitoEspecial = 4;
                    break;
                case 'G':
                    $digitoEspecial = 5;
                    break;
            }

            $suma = (array_sum($digitos) - $digitos[9]) + ($digitoEspecial*4);
            $residuo = $suma % 11;
            $resta = 11 - $residuo;

            $digitoVerificador = ($resta >= 10) ? 0 : $resta;

            if ($digitoVerificador != $digitos[9]) {
                $retorno = false;
            }
        }

        return $retorno;
    }

    //Método para paginar los proveedores
    public function metJsonDataTabla($concepto=false)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        if($concepto) {
            $sql = "SELECT
              proveedor.*,
              concat_ws(' ', persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2,documento.ind_descripcion) AS nomProveedor,
              persona.ind_documento_fiscal,
              persona.num_estatus,
              cxp.pk_num_interfaz_cxp AS pk_proveedor
          FROM
            lg_b022_proveedor AS proveedor
          INNER JOIN a003_persona AS persona ON persona.pk_num_persona =  proveedor.fk_a003_num_persona_proveedor
          INNER JOIN nm_b005_interfaz_cxp AS cxp ON cxp.fk_a003_num_persona_proveedor = persona.pk_num_persona
          INNER JOIN cp_b002_tipo_documento AS documento ON documento.pk_num_tipo_documento = cxp.fk_cpb002_num_tipo_documento ";
        } else {

            $sql = "SELECT
              proveedor.*,
              proveedor.pk_num_proveedor AS pk_proveedor,
              concat_ws(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nomProveedor,
              persona.ind_documento_fiscal,
              persona.num_estatus
          FROM
            lg_b022_proveedor AS proveedor
          INNER JOIN a003_persona AS persona ON persona.pk_num_persona =  proveedor.fk_a003_num_persona_proveedor
           ";
        }
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        proveedor.pk_num_proveedor LIKE '%$busqueda[value]%' OR 
                        persona.ind_documento_fiscal LIKE '%$busqueda[value]%' OR 
                        persona.ind_nombre1 LIKE '%$busqueda[value]%' OR 
                        persona.ind_nombre2 LIKE '%$busqueda[value]%' OR 
                        persona.ind_apellido1 LIKE '%$busqueda[value]%' OR 
                        persona.ind_apellido2 LIKE '%$busqueda[value]%' 
                        )
                        ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_proveedor','nomProveedor','ind_documento_fiscal','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_proveedor';
        #construyo el listado de botones
        $camposExtra = array('fk_a003_num_persona_proveedor');

        if (in_array('LG-01-07-01-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario a Modificado el Proveedor Nro. '.$clavePrimaria.'"
                        idPost="'.$clavePrimaria.'" titulo="Modificar Proveedor" idPersona="fk_a003_num_persona_proveedor"
                        data-toggle="modal" data-target="#formModal">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-01-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario a Visto el Proveedor Nro. '.$clavePrimaria.'"
                        idPost="'.$clavePrimaria.'" titulo="Visualizar Proveedor" idPersona="fk_a003_num_persona_proveedor"
                        data-toggle="modal" data-target="#formModal">
                    <i class="fa fa-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-01-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado el Proveedor Nro. '.$clavePrimaria.'"
                        idProveedor="'.$clavePrimaria.'" idPersona="fk_a003_num_persona_proveedor" titulo="Eliminar Proveedor"
                        mensaje="Estas seguro que desea eliminar el Proveedor!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);

    }

    //Método para paginar las personas
    public function metJsonDataTablaPersona()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
              persona.*
            FROM
              a003_persona as persona 
              WHERE 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        ( persona.pk_num_persona LIKE '%$busqueda[value]%' OR
                        persona.ind_cedula_documento LIKE '%$busqueda[value]%' OR
                        persona.ind_nombre1 LIKE '%$busqueda[value]%' OR
                        persona.ind_apellido1 LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_persona','ind_cedula_documento','ind_nombre1','ind_apellido1');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_empleado';

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }



}