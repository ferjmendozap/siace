<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class tipoDocumentoControlador extends Controlador
{
    private $atTipoDoc;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atTipoDoc = $this->metCargarModelo('tipoDocumento', 'maestros');
    }

    //Método para listar los tipos de documentos
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar los tipos de documentos
    public function metCrearModificarTipoDoc()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idTipoDoc = $this->metObtenerInt('idTipoDoc');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $ExcceccionInt = array('fiscalTipoDoc','tranTipoDoc');
            $ExcceccionTxt = array('estadoTipoDoc');
            $ind = $this->metValidarFormArrayDatos('formtipodoc','int',$ExcceccionInt);
            $texto = $this->metValidarFormArrayDatos('formtipodoc','txt',$ExcceccionTxt);
            $alphaNum = $this->metValidarFormArrayDatos('formtipodoc','alphaNum');

            if ($alphaNum != null && $ind == null && $texto == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $texto == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $texto != null) {
                $validacion = $texto;
            } elseif ($alphaNum == null && $ind != null && $texto != null) {
                $validacion = array_merge($texto, $ind);
            } elseif ($alphaNum != null && $ind == null && $texto != null) {
                $validacion = array_merge($texto, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $texto == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $texto);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['estadoTipoDoc'])) {
                $validacion['estadoTipoDoc'] = "I";
            }
            if (!isset($validacion['fiscalTipoDoc'])) {
                $validacion['fiscalTipoDoc'] = 0;
            }
            if (!isset($validacion['tranTipoDoc'])) {
                $validacion['tranTipoDoc'] = 0;
            }

            if($idTipoDoc==0){
               $id=$this->atTipoDoc->metCrearTipoDoc($validacion['codTipoDoc'],$validacion['descTipoDoc'],$validacion['tranTipoDoc'],$validacion['fiscalTipoDoc'],$validacion['estadoTipoDoc']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoDoc->metModificarTipoDoc($validacion['codTipoDoc'],$validacion['descTipoDoc'],$validacion['tranTipoDoc'],$validacion['fiscalTipoDoc'],$validacion['estadoTipoDoc'],$idTipoDoc);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idTipoDoc']=$id;

            echo json_encode($validacion);
            exit;
        }
        if($idTipoDoc!=0){
            $this->atVista->assign('formDBTipoDoc',$this->atTipoDoc->metMostrarTipoDoc($idTipoDoc));
            $this->atVista->assign('idTipoDoc',$idTipoDoc);
            $this->atVista->assign('ver',$ver);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para eliminar los tipos de documentos
    public function metEliminarTipoDoc()
    {
        $idTipoDoc = $this->metObtenerInt('idTipoDoc');
        if($idTipoDoc!=0){
            $id=$this->atTipoDoc->metEliminarTipoDoc($idTipoDoc);
            if(is_array($id)){
                $tdmvalido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Tipo de Documento se encuentra en uso y no se puede eliminar'
                );
            }else{
                $tdmvalido=array(
                    'status'=>'ok',
                    'id'=>$idTipoDoc
                );
            }
        }
        echo json_encode($tdmvalido);
        exit;
    }

    //Método para paginar los tipos de documentos
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT * FROM lg_b016_tipo_documento ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        cod_tipo_documento LIKE '%$busqueda[value]%' OR
                        ind_descripcion LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_tipo_documento','ind_descripcion','num_flag_transaccion_sistema','num_flag_documento_fiscal','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_tipo_documento';
        #construyo el listado de botones
        $flags = array('num_flag_documento_fiscal','num_flag_transaccion_sistema');

        if (in_array('LG-01-07-03-02-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado el Tipo de Documento Nro. '.$clavePrimaria.'"
                        idTipoDoc="'.$clavePrimaria.'" titulo="Modificar Tipo de Documento"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-03-02-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado el Tipo de Documento Nro. '.$clavePrimaria.'"
                        idTipoDoc="'.$clavePrimaria.'" titulo="Consultar Tipo de Documento"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-03-02-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado el Tipo de Documento Nro. '.$clavePrimaria.'"
                        idTipoDoc="'.$clavePrimaria.'" titulo="Eliminar Tipo de Documento"
                        mensaje="Estas seguro que desea eliminar el Tipo de Documento!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false,$flags);

    }


}
