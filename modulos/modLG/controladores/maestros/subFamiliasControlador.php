<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class subFamiliasControlador extends Controlador
{
    private $atSubFamiliasModelo;
    private $atFamiliasModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atSubFamiliasModelo = $this->metCargarModelo('subFamilias', 'maestros');
    }

    //Método para listar las subfamilias
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar las subfamilias
    public function metCrearModificarSubFamilias()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idSubFamilias = $this->metObtenerInt('idSubFamilias');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $Excepcion = array('estado');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');
            $text = $this->metValidarFormArrayDatos('form','txt',$Excepcion);
            $ind = $this->metValidarFormArrayDatos('form','int');

            if ($text != null && $ind == null && $alphaNum == null) {
                $validacion = $text;
            } elseif ($text == null && $ind != null && $alphaNum == null) {
                $validacion = $ind;
            } elseif ($text == null && $ind == null && $alphaNum != null) {
                $validacion = $alphaNum;
            } elseif ($text == null && $ind != null && $alphaNum != null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif ($text != null && $ind != null && $alphaNum == null) {
                $validacion = array_merge($text, $ind);
            } elseif ($text != null && $ind == null && $alphaNum != null) {
                $validacion = array_merge($text, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $text);
            }

            if (!isset($validacion['estado'])) {
                $validacion['estado'] = '0';
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if($idSubFamilias==0){
                $id=$this->atSubFamiliasModelo->metCrearSubFamilias(
                    $validacion['codigo'],
                    $validacion['desc'],
                    $validacion['estado'],
                    $validacion['familias']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atSubFamiliasModelo->metModificarSubFamilias(
                    $validacion['codigo'],
                    $validacion['desc'],
                    $validacion['estado'],
                    $validacion['familias'],
                    $idSubFamilias);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idSubFamilias']=$id;

            echo json_encode($validacion);
            exit;
        }
        if($idSubFamilias!=0){
            $this->atVista->assign('formDB',$this->atSubFamiliasModelo->metMostrarSubFamilias($idSubFamilias));
            $this->atVista->assign('idSubFamilias',$idSubFamilias);
            $this->atVista->assign('ver',$ver);
        }

        $this->atVista->assign('familias',$this->atSubFamiliasModelo->atFamiliasModelo->metListarFamilias());
        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para eliminar las subfamilias
    public function metEliminarSubFamilias()
    {
        $idSubFamilias = $this->metObtenerInt('idSubFamilias');
        if($idSubFamilias!=0){
            $id=$this->atSubFamiliasModelo->metEliminarSubFamilias($idSubFamilias);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero la SubFamilia se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idSubFamilias
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para paginar las subfamilias
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT subfamilia.*,
                  familia.ind_cod_familia
                FROM lg_b007_clase_subfamilia AS subfamilia 
                INNER JOIN lg_b006_clase_familia AS familia ON subfamilia.fk_lgb006_num_clase_familia = familia.pk_num_clase_familia
";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        subfamilia.pk_num_subfamilia LIKE '%$busqueda[value]%' OR 
                        subfamilia.ind_cod_subfamilia LIKE '%$busqueda[value]%' OR 
                        familia.ind_cod_familia LIKE '%$busqueda[value]%' OR 
                        subfamilia.ind_descripcion LIKE '%$busqueda[value]%' 
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_subfamilia','ind_cod_subfamilia','ind_cod_familia','ind_descripcion','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_subfamilia';
        #construyo el listado de botones

        if (in_array('LG-01-07-02-03-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado la SubFamiliaNro. '.$clavePrimaria.'"
                        idSubFamilias="'.$clavePrimaria.'" titulo="Modificar SubFamilia"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-02-03-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado la SubFamiliaNro. '.$clavePrimaria.'"
                        idSubFamilias="'.$clavePrimaria.'" titulo="Consultar SubFamilia"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-02-03-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado la SubFamiliaNro. '.$clavePrimaria.'"
                        idSubFamilias="'.$clavePrimaria.'" titulo="Eliminar SubFamilia"
                        mensaje="Estas seguro que desea eliminar la Familia!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }


}
