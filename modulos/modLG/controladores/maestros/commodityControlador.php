<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class commodityControlador extends Controlador
{
    private $atCommodityModelo;
    private $atAlmacenModelo;
    private $atClasificacionModelo;
    private $atUnidadesModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atCommodityModelo = $this->metCargarModelo('commodity', 'maestros');
    }

    //Método para listar los commodity
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para listar los partidas y cuentas
    public function metPartidaCuenta()
    {
        $this->atVista->assign('lista', $this->atCommodityModelo->metListarPartida());
        $this->atVista->metRenderizar('partida', 'modales');
    }

    //Método para crear y/o modificar los commodity
    public function metCrearModificarCommodity()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idCommodity = $this->metObtenerInt('idCommodity');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $ExcepcionTxt = array('estado','plan','plan20');
            $ind = $this->metValidarFormArrayDatos('form','int');
            $texto = $this->metValidarFormArrayDatos('form','txt',$ExcepcionTxt);
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');

            if ($alphaNum != null && $ind == null && $texto == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $texto == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $texto != null) {
                $validacion = $texto;
            } elseif ($alphaNum == null && $ind != null && $texto != null) {
                $validacion = array_merge($texto, $ind);
            } elseif ($alphaNum != null && $ind == null && $texto != null) {
                $validacion = array_merge($texto, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $texto == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $texto);
            }

            if ($validacion['plan']=="error") {
                $validacion['plan'] = NULL;
            }
            if ($validacion['plan20']=="error") {
                $validacion['plan20'] = NULL;
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['estado'])) {
                $validacion['estado'] = 0;
            }
            if (!isset($validacion['pres'])) {
                $validacion['pres'] = 0;
            }

            if($idCommodity==0){
                $cantidad = $this->atCommodityModelo->metBuscarCodigo($validacion['partida']);

                $validacion['codigo'] = $validacion['partida'].($cantidad['cantidad'] +1);

                $id=$this->atCommodityModelo->metCrearCommodity(
                    $validacion['desc'],$validacion['codigo'],$validacion['pres'],
                    $validacion['estado'],$validacion['partida'],
                    $validacion['plan'],$validacion['plan20'],
                    $validacion['unidad'],$validacion['alm'],
                    $validacion['clasificacion']
                );
                $validacion['status']='nuevo';
            }else{
                $id=$this->atCommodityModelo->metModificarCommodity(
                    $validacion['desc'],$validacion['pres'],
                    $validacion['estado'],$validacion['partida'],
                    $validacion['plan'],$validacion['plan20'],
                    $validacion['unidad'],$validacion['alm'],
                    $validacion['clasificacion'],$idCommodity
                );
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idCommodity']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idCommodity!=0){
            $this->atVista->assign('formDB',$this->atCommodityModelo->metMostrarCommodity($idCommodity));
            $this->atVista->assign('idCommodity',$idCommodity);
            $this->atVista->assign('ver',$ver);
        }
        $this->atVista->assign('selectClasificacion',$this->atCommodityModelo->atClasificacionModelo->metListarClasificacion());
//        $this->atVista->assign('selectPartida',$this->atCommodityModelo->atPartidaModelo->metListarPartida());
        $this->atVista->assign('unidad',$this->atCommodityModelo->atUnidadesModelo->metListarUnidades());
        $this->atVista->assign('almacen',$this->atCommodityModelo->atAlmacenModelo->metListarAlmacen());
//        $this->atVista->assign('plan',$this->atCommodityModelo->metListarPlanCuenta());
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    //Método para buscar un commodity
    public function metBuscarCommodity(){
        $idCommodity = $this->metObtenerInt('idIC');
        $datos = $this->atCommodityModelo->metMostrarCommodity($idCommodity);
        echo json_encode($datos);
        exit;
    }

    //Método para eliminar los commodity
    public function metEliminarCommodity()
    {
        $idCommodity = $this->metObtenerInt('idCommodity');
        if($idCommodity!=0){
            $id=$this->atCommodityModelo->metEliminarCommodity($idCommodity);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idCommodity
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para paginar los commodity
    public function metJsonDataTabla($clasif = false, $estado = false, $presu = false)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT 
                commodity.*, 
                clasificacion.ind_descripcion AS clasificacion,
                presupuestaria.cod_partida 
                FROM lg_b003_commodity AS commodity 
                INNER JOIN lg_b017_clasificacion AS clasificacion ON commodity.fk_lgb017_num_clasificacion = clasificacion.pk_num_clasificacion 
              INNER JOIN pr_b002_partida_presupuestaria AS presupuestaria ON presupuestaria.pk_num_partida_presupuestaria = commodity.fk_prb002_num_partida_presupuestaria 
              WHERE 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        ( 
                        commodity.pk_num_commodity LIKE '%$busqueda[value]%' OR
                        commodity.ind_cod_commodity LIKE '%$busqueda[value]%' OR
                        commodity.ind_descripcion LIKE '%$busqueda[value]%' OR
                        clasificacion.ind_descripcion LIKE '%$busqueda[value]%'
                        )
                        ";
        }
        if($clasif){
            $sql .= " AND clasificacion.pk_num_clasificacion = '$clasif' ";
        }
        if($presu){
            $sql .= " AND commodity.num_flag_vericado_presupuesto = 1 ";
        }
        if($estado){
            $sql .= " AND commodity.num_estatus = 1 ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_commodity','ind_cod_commodity','ind_descripcion','clasificacion','num_estatus','cod_partida');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_commodity';
        #construyo el listado de botones

        if (in_array('LG-01-07-04-01-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado el Commoditie Nro. '.$clavePrimaria.'"
                        idCommodity="'.$clavePrimaria.'" titulo="Modificar Commoditie"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-04-01-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado el Commoditie Nro. '.$clavePrimaria.'"
                        idCommodity="'.$clavePrimaria.'" titulo="Consultar Commoditie"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-04-01-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado el Commoditie Nro. '.$clavePrimaria.'"
                        idCommodity="'.$clavePrimaria.'" titulo="Eliminar Commoditie"
                        mensaje="Estas seguro que desea eliminar el Commoditie!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }


}
