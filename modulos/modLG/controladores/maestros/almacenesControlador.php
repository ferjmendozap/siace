<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class almacenesControlador extends Controlador
{
    private $atAlmacenesModelo;
    private $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atAlmacenesModelo = $this->metCargarModelo('almacen', 'maestros');
    }

    //Método para listar los almacenes
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para buscar los empleados
    public function metEmpleado($persona)
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('persona', $persona);
        $this->atVista->metRenderizar('personas', 'modales');
    }

    //Método para crear y/o modificar los almacenes
    public function metCrearModificarAlmacen()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idAlmacen = $this->metObtenerInt('idAlmacen');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $Excceccion = array('commAlmacen','estadoAlmacen','cuentaAlmacen');
            $ind = $this->metValidarFormArrayDatos('form','int',$Excceccion);
            $texto = $this->metValidarFormArrayDatos('form','txt');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');

            if ($alphaNum != null && $ind == null && $texto == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $texto == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $texto != null) {
                $validacion = $texto;
            } elseif ($alphaNum == null && $ind != null && $texto != null) {
                $validacion = array_merge($texto, $ind);
            } elseif ($alphaNum != null && $ind == null && $texto != null) {
                $validacion = array_merge($texto, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $texto == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $texto);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['estadoAlmacen'])) {
                $validacion['estadoAlmacen'] = 0;
            }
            if (!isset($validacion['commAlmacen'])) {
                $validacion['commAlmacen'] = 0;
            }
            if ($validacion['cuentaAlmacen']=='0') {
                $validacion['cuentaAlmacen'] = NULL;
            }

            if($idAlmacen==0){
                $id=$this->atAlmacenesModelo->metCrearAlmacen($validacion['codAlmacen'],$validacion['descAlmacen'],$validacion['dirAlmacen'],$validacion['estadoAlmacen'],$validacion['tipoAlmacen'],$validacion['commAlmacen'],$validacion['cuentaAlmacen'],$validacion['encargadoAlmacen'],$validacion['responAlmacen']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atAlmacenesModelo->metModificarAlmacen($validacion['codAlmacen'],$validacion['descAlmacen'],$validacion['dirAlmacen'],$validacion['estadoAlmacen'],$validacion['tipoAlmacen'],$validacion['commAlmacen'],$validacion['cuentaAlmacen'],$validacion['encargadoAlmacen'],$validacion['responAlmacen'],$idAlmacen);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idAlmacen']=$id;

            echo json_encode($validacion);
            exit;
        }
        if($idAlmacen!=0){
            $this->atVista->assign('formDB',$this->atAlmacenesModelo->metMostrarAlmacen($idAlmacen));
            $this->atVista->assign('idAlmacen',$idAlmacen);
            $this->atVista->assign('ver',$ver);
        }
        $this->atVista->assign('selectAlm',$this->atAlmacenesModelo->atMiscelaneoModelo->metMostrarSelect('ALM'));
        $this->atVista->assign('selectPlan',$this->atAlmacenesModelo->metSelectPlan());

        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para eliminar los almacenes
    public function metEliminarAlmacen()
    {
        $idAlmacen = $this->metObtenerInt('idAlmacen');
        if($idAlmacen!=0){
            $id=$this->atAlmacenesModelo->metEliminarAlmacen($idAlmacen);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idAlmacen
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para paginar los almacenes
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
              a.*,
              usu.ind_usuario
              FROM lg_b014_almacen AS a
              INNER JOIN a018_seguridad_usuario AS usu ON usu.pk_num_seguridad_usuario=a.fk_a018_num_seguridad_usuario ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        a.cod_almacen LIKE '%$busqueda[value]%' OR
                        a.ind_descripcion LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_almacen','ind_descripcion','ind_direccion','num_flag_commodity','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_almacen';
        #construyo el listado de botones
        $flags = array('num_flag_commodity');

        if (in_array('LG-01-07-03-01-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button accion="modificar" title="Editar"
                        class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        descipcion="El Usuario ha Modificado el Almacen Nro. '.$clavePrimaria.'"
                        idAlmacen="'.$clavePrimaria.'" titulo="Modificar Almacen"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="fa fa-edit"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-07-03-01-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button accion="ver" title="Consultar"
                        class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        descipcion="El Usuario ha Consultado el Almacen Nro. '.$clavePrimaria.'"
                        idAlmacen="'.$clavePrimaria.'" titulo="Consultar Almacen"
                        data-toggle="modal" data-target="#formModal" id="modificar">
                    <i class="icm icm-eye2"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        if (in_array('LG-01-07-03-01-04-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                <button accion="eliminar" title="Eliminar"
                        class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                        descipcion="El Usuario ha Eliminado el Almacen Nro. '.$clavePrimaria.'"
                        idAlmacen="'.$clavePrimaria.'" titulo="Eliminar Almacen"
                        mensaje="Estas seguro que desea eliminar el Almacen!!" id="eliminar">
                    <i class="md md-delete"></i>
                </button>
            ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false,$flags);

    }

    //Método para paginar los empleados
    public function metJsonDataTablaPersona()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
              persona.*,
              empleado.pk_num_empleado,
              dependencia.ind_dependencia
            FROM
              a003_persona as persona 
              INNER JOIN rh_b001_empleado AS empleado ON persona.pk_num_persona = empleado.fk_a003_num_persona
              INNER JOIN rh_c076_empleado_organizacion AS organizacion ON organizacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
              INNER JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
              and empleado.ind_estado_aprobacion = 'ap'
              and empleado.num_estatus = 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (empleado.pk_num_empleado LIKE '%$busqueda[value]%' OR
                        persona.pk_num_persona LIKE '%$busqueda[value]%' OR
                        persona.ind_cedula_documento LIKE '%$busqueda[value]%' OR
                        persona.ind_nombre1 LIKE '%$busqueda[value]%' OR
                        persona.ind_apellido1 LIKE '%$busqueda[value]%' OR
                        dependencia.ind_dependencia LIKE '%$busqueda[value]%') ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_persona','pk_num_empleado','ind_cedula_documento','ind_nombre1','ind_apellido1','ind_dependencia');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_empleado';

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }


}
