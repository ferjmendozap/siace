<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once ROOT. 'publico/procesoCompra/docxpresso/CreateDocument.php';

class invitarCotizarControlador extends Controlador
{
    private $atInvitar;
    private $atIdEmpleado;
    private $atMiscelaneoModelo;
    private $atActaInicioModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atInvitar = $this->metCargarModelo('invitarCotizar', 'cotizaciones');
        $this->metObtenerLibreria('headerCotizacion','modLG');
    }

    //Método para listar los requerimientos
    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min'
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para buscar el subfijo de los numeros
    public function metSubfijo($xx)
    { // esta función regresa un metSubfijo para la cifra
        $xx = trim($xx);
        $xstrlen = strlen($xx);
        if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
            $xsub = "";
        //
        if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
            $xsub = "MIL";
        //
        return $xsub;
    } // END FUNCTION

    //Método para modificar un monto a letras
    public function metNumtoletras($xcifra)
    {
        $xarray = array(0 => "Cero",
            1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
            "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
            "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
            100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
        );

        $xcifra = trim($xcifra);
        $xlength = strlen($xcifra);
        $xpos_punto = strpos($xcifra, ".");
        $xaux_int = $xcifra;
        $xdecimales = "00";
        if (!($xpos_punto === false)) {
            if ($xpos_punto == 0) {
                $xcifra = "0" . $xcifra;
                $xpos_punto = strpos($xcifra, ".");
            }
            $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
            $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
        }

        $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
        $xcadena = "";
        for ($xz = 0; $xz < 3; $xz++) {
            $xaux = substr($XAUX, $xz * 6, 6);
            $xi = 0;
            $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
            $xexit = true; // bandera para controlar el ciclo del While
            while ($xexit) {
                if ($xi == $xlimite) // si ya llegó al límite m&aacute;ximo de enteros
                {
                    break; // termina el ciclo
                }

                $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
                $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
                for ($xy = 1; $xy < 4; $xy++) // ciclo para revisar centenas, decenas y unidades, en ese orden
                {
                    switch ($xy) {
                        case 1: // checa las centenas
                            if (substr($xaux, 0, 3) < 100) // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
                            {
                            } else {
                                if(substr($xaux, 1, 2)=='00'){
                                    $xseek = $xarray[substr($xaux, 0, 3)]; // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                }else{
                                    $xseek=false;
                                }

                                if ($xseek) {
                                    $xsub = $this->metSubfijo($xaux); // devuelve el $this->metSubfijo correspondiente (Millón, Millones, Mil o nada)
                                    if (substr($xaux, 0, 3) == 100)
                                        $xcadena = " " . $xcadena . " CIEN " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
                                } else // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
                                {
                                    $xseek = $xarray[substr($xaux, 0, 1) * 100]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
                                    $xcadena = " " . $xcadena . " " . $xseek;
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 0, 3) < 100)
                            break;
                        case 2: // checa las decenas (con la misma lógica que las centenas)
                            if (substr($xaux, 1, 2) < 10) {
                            } else {
                                if(substr($xaux, 2, 1)=='0'){
                                    $xseek = $xarray[substr($xaux, 1, 2)]; // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                }else{
                                    $xseek=false;
                                }
                                if ($xseek) {
                                    $xsub = $this->metSubfijo($xaux);
                                    if (substr($xaux, 1, 2) == 20)
                                        $xcadena = " " . $xcadena . " VEINTE " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3;
                                } else {
                                    $xseek = $xarray[substr($xaux, 1, 1) * 10];
                                    if (substr($xaux, 1, 1) * 10 == 20)
                                        $xcadena = " " . $xcadena . " " . $xseek;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " Y ";
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 1, 2) < 10)
                            break;
                        case 3: // checa las unidades
                            if (substr($xaux, 2, 1) < 1) // si la unidad es cero, ya no hace nada
                            {
                            } else {
                                $xseek = $xarray[substr($xaux, 2, 1)]; // obtengo directamente el valor de la unidad (del uno al nueve)
                                $xsub = $this->metSubfijo($xaux);
                                $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                            } // ENDIF (substr($xaux, 2, 1) < 1)
                            break;
                    } // END SWITCH
                } // END FOR
                $xi = $xi + 3;
            } // ENDDO

            if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
                $xcadena .= " DE";

            if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
                $xcadena .= " DE";

            // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
            if (trim($xaux) != "") {
                switch ($xz) {
                    case 0:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena .= "UN BILLON ";
                        else
                            $xcadena .= " BILLONES ";
                        break;
                    case 1:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena .= "UN MILLON ";
                        else
                            $xcadena .= " MILLONES ";
                        break;
                    case 2:
                        if ($xcifra < 1) {
                            $xcadena = "CERO BOLIVARES CON $xdecimales/100";
                        }
                        if ($xcifra >= 1 && $xcifra < 2) {
                            $xcadena = "UN BOLIVAR CON $xdecimales/100";
                        }
                        if ($xcifra >= 2) {
                            $xcadena .= " BOLIVARES CON $xdecimales/100"; //
                        }
                        break;
                } // endswitch ($xz)
            } // ENDIF (trim($xaux) != "")
            // ------------------      en este caso, para México se usa esta leyenda     ----------------
            $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
        } // ENDFOR	($xz)
        return trim($xcadena);
    } // END FUNCTION

    //Método para modificar un dis de semana a letras
    public function metDiaSemLetras($dia)
    {
        if(strlen($dia)==1){
            $dia="0".$dia;
        }
        $metDiaSemLetras=array(
            '01'=>'Lunes',
            '02'=>'Martes',
            '03'=>'Miercoles',
            '04'=>'Jueves',
            '05'=>'Viernes',
            '06'=>'Sabado',
            '07'=>'Domingo'
        );
        return $metDiaSemLetras[$dia];
    } // END FUNCTION

    //Método para modificar el formato de las fechas
    public function metFormatoFecha($fecha,$formato,$separador){
        #1  2016-01-01 a 01-01-2016
        #0  01-01-2016 a 2016-01-01
        if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
            $dia=substr($fecha,8,2);
            $mes=substr($fecha,5,2);
            $anio=substr($fecha,0,4);
        } elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
            $dia=substr($fecha,0,2);
            $mes=substr($fecha,3,2);
            $anio=substr($fecha,6,4);
        } else {
            $dia=0;
            $mes=0;
            $anio=0;
        }
        if (checkdate($mes, $dia, $anio)==false){
            return "la fecha es incorrecta";
            exit;
        }
        if($formato==1) {
            $fecha =$dia.$separador.$mes.$separador.$anio;
        } else {
            $fecha =$anio.$separador.$mes.$separador.$dia;
        }
        return $fecha;
    } // END FUNCTION

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    //Método para crear la invitacion
    public function metCrearInvitacion()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $datos_ic = $this->metObtenerFormulas('datos_ic');

        if($valido==1) {
            $this->metValidarToken();

            $int = $this->metValidarFormArrayDatos('form', 'int');
            $txt = $this->metValidarFormArrayDatos('form', 'txt');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            if ($alphaNum != null && $int == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $int != null && $txt == null) {
                $validacion = $int;
            } elseif ($alphaNum == null && $int == null && $txt != null) {
                $validacion = $txt;
            } elseif ($alphaNum == null && $int != null && $txt != null) {
                $validacion = array_merge($txt, $int);
            } elseif ($alphaNum != null && $int == null && $txt != null) {
                $validacion = array_merge($txt, $alphaNum);
            } elseif ($alphaNum != null && $int != null && $txt == null) {
                $validacion = array_merge($int, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $int, $txt);
            }

            $validacion['validez'] = '0';

            if(isset($validacion['cant'])){
                $cant=1;
                foreach ($validacion['proveedor'] as $key=>$value) {
                    if (!isset($value)){
                        $validacion['proveedor'][$key] = "0";
                    } else {
                        $maximo = $this->atInvitar->metObtenerMaximaInvitacion();
                        $ini = 1;
                        for ($i=0;$i<10;$i++){
                            if ($maximo['maximo'][$i]!=0){
                                $ini = $i;
                                $i = 10;
                            }
                        }
                        $numero = substr($maximo['maximo'],$ini);

                        $numInvi = $numero+$cant;
                        $validacion['ind_num_invitacion'][$key] = $this->metRellenarCeros($numInvi,10);
                    }
                    $cant = $cant +1;
                }
            } else {
                $validacion['proveedor'] = 'error';
            }

            foreach ($datos_ic as $key=>$value) {

                $acta = $this->atInvitar->metBuscarActaDet($value);
                if($acta['estadoActa']=='CO' OR $acta['estadoActa']=='AN') {
                    $validacion['status']='errorEstado';
                    $validacion['proveedor'] = 'error';
                    echo json_encode($validacion);
                    exit;
                }
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            $id=$this->atInvitar->metCrearInvitacion(
                $validacion['ind_num_invitacion'],$validacion['validez'],
                $validacion['dias'],$validacion['flim'],$validacion['condicion'],
                $validacion['observ'],$datos_ic,$validacion['proveedor']
                ,$validacion['ind_num_invitacion']
            );
            $validacion['status']='nuevo';

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idInvitacion']=$id;

            echo json_encode($validacion);
            exit;
        }

        $requerimientos = array();
        $invitaciones = array();
        foreach ($datos_ic as $key=>$value) {
            $req = $this->atInvitar->metBuscarActaDet($value);
            $invi = $this->atInvitar->metMostrarInvitacionRealizada($value);
            $requerimientos[] = $req;
            $invitaciones = array_merge($invitaciones,$invi);
        }

        $this->atVista->assign('requerimientos',$requerimientos);
        $this->atVista->assign('invitaciones',$invitaciones);

        $this->atVista->metRenderizar('invitar','modales');
    }

    //Método para buscar los detalles del acta
    public function metBuscarActaDet(){
        $idActaDet = $this->metObtenerFormulas('idActaDet');

        $id = $this->atInvitar->metBuscarActaDet($idActaDet);
        $validacion['id'] = $id;
        $validacion['idActaDet'] = $idActaDet;

        echo json_encode($validacion);
        exit;
    }

    //Método para listar las unidades
    public function metListarUnidades(){
        $i = $this->metObtenerInt('i');
        $id = $this->atInvitar->metListarUnidades();
        $valores['i'] = $i;
        $valores['id'] = $id;
        echo json_encode($valores);
        exit;
    }

    //Método para crear la observacion referente a lo asignado y sugerido
    public function metObservacion(){
        $id = $this->metObtenerFormulas('id');
        $this->atVista->assign('id',$id);
        $this->atVista->metRenderizar('observacion','modales');
    }

    //Método para ver la disponibilidad presupuestaria
    public function metDisponibilidad(){

        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $id = $this->metObtenerInt('id');
        $acta = $this->atInvitar->metBuscarActaDet($id);
        $partidas = $this->atInvitar->metBuscarPartidasActa($acta['pk_num_acta_inicio']);
        $actaDet = $this->atInvitar->atActaInicioModelo->metListarActaDetalle($acta['pk_num_acta_inicio']);
        $this->atVista->assign('partidaIVA',$this->atInvitar->metBuscarPartidaCuenta('403.18.01.00'));

        $this->atVista->assign('partidas',$partidas);
        $this->atVista->assign('actaDet',$actaDet);
        $this->atVista->assign('id',$id);
        $this->atVista->metRenderizar('disponibilidad','modales');
    }

    //Método para ver las invitaciones realizadas
    public function metMostrarInvitacionRealizada(){
        $idActaDet = $this->metObtenerInt('idActaDet');
        $id = $this->atInvitar->metMostrarInvitacionRealizada($idActaDet);
        $validacion['id'] = $id;
        $validacion['idActaDet'] = $idActaDet;
        echo json_encode($validacion);
        exit;
    }

    //Método para crear y/o modificar la cotizacion
    public function metCotizar()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $datos_ic = $this->metObtenerFormulas('datos_ic');
        $idActaDet = $datos_ic[0];

        if($valido==1){
            $this->metValidarToken();

            $int = $this->metValidarFormArrayDatos('form','int');
            $txt = $this->metValidarFormArrayDatos('form','txt');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum');

            if ($alphaNum != null && $int == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $int != null && $txt == null) {
                $validacion = $int;
            } elseif ($alphaNum == null && $int == null && $txt != null) {
                $validacion = $txt;
            } elseif ($alphaNum == null && $int != null && $txt != null) {
                $validacion = array_merge($txt, $int);
            } elseif ($alphaNum != null && $int == null && $txt != null) {
                $validacion = array_merge($txt, $alphaNum);
            } elseif ($alphaNum != null && $int != null && $txt == null) {
                $validacion = array_merge($int, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $int, $txt);
            }

            if($validacion['estadoActa']=='CO' OR $validacion['estadoActa']=='AN'){
                $validacion['estado']='error';
                $validacion['status']='errorEstado';
                echo json_encode($validacion);
                exit;
            }

            if (isset($validacion['secuencia'])) {
                $cant = 1;
                while ($cant<=$validacion['secuencia']) {


                    $idCotizacion="idCotizacion".$cant;
                    $idActaDet="idActaDet".$cant;
                    $pUni="pUni".$cant;
                    $pIVA="pIVA".$cant;
                    $cantidad="cantidad".$cant;
                    $cantidadPedida="cantidadPedida".$cant;
                    $total="total".$cant;
                    $dpor="dpor".$cant;
                    $dfijo="dfijo".$cant;
                    $vofer="vofer".$cant;
                    $dias="dias".$cant;
                    $finvi="finvi".$cant;
                    $fentrega="fentrega".$cant;
                    $frecepcion="frecepcion".$cant;
                    $invi="invi".$cant;
                    $exo="exo".$cant;
                    $asignado="asignado".$cant;
                    $sugerido="sugerido".$cant;
                    $razon="razon".$cant;
                    $observacion="observacion".$cant;
                    $recomendacion="recomendacion".$cant;
                    $uniCompra="uniCompra".$cant;
                    $formaPago="formaPago".$cant;

                    if ($validacion[$idCotizacion] == 0) {
                        $validacion[$idCotizacion] = '0';
                    }
                    $validacion['idCotizacion'][$cant] = $validacion[$idCotizacion];
                    $validacion['idActaDet'][$cant] = $validacion[$idActaDet];

                    if($validacion[$pUni]=="error"){
                        $validacion[$pUni]="0";
                        $validacion[$pIVA]="0";
                        $validacion['pUni'][$cant] = 0;
                        $validacion['pIVA'][$cant] = 0;
                    } else {
                        $validacion['pUni'][$cant] = $validacion[$pUni];
                        $validacion['pIVA'][$cant] = $validacion[$pIVA];
                    }
                    if($validacion[$pIVA]=="error"){
                        $validacion['pIVA'][$cant] = "0";
                        $validacion[$pIVA] = "0";
                    }
                    $validacion['cantidad'][$cant] = $validacion[$cantidad];
                    $validacion['cantidadPedida'][$cant] = $validacion[$cantidadPedida];

                    if ($validacion[$dpor] == 0) {
                        $validacion[$dpor] = '0';
                    }
                    $validacion['dpor'][$cant] = $validacion[$dpor];
                    $validacion['dfijo'][$cant] = $validacion[$dfijo];
                    $validacion[$vofer] ="0";
                    $validacion['vofer'][$cant] = $validacion[$vofer];

                    if ($validacion[$dias] == 'error') {
                        $validacion[$dias] = '0';
                    }
                    $validacion['dias'][$cant] = $validacion[$dias];
                    $validacion['finvi'][$cant] = $validacion[$finvi];
                    $validacion['fentrega'][$cant] = $validacion[$fentrega];
                    $validacion['frecepcion'][$cant] = $validacion[$frecepcion];

                    if ($validacion[$total] == 0) {
                        $validacion[$total] = '0';
                    }
                    $validacion['total'][$cant] = $validacion[$total];
                    $validacion['invi'][$cant] = $validacion[$invi];

                    if (!isset($validacion[$exo])) {
                        $validacion[$exo] = '0';
                    }
                    $validacion['exo'][$cant] = $validacion[$exo];

                    if (!isset($validacion[$asignado])) {
                        $validacion[$asignado] = '0';
                    }
                    $validacion['asignado'][$cant] = $validacion[$asignado];

                    if (!isset($validacion[$sugerido])) {
                        $validacion[$sugerido] = '0';
                    }
                    $validacion['sugerido'][$cant] = $validacion[$sugerido];

                    if ($validacion[$observacion]=='error') {
                        $validacion[$observacion] = NULL;
                    }
                    $validacion['observacion'][$cant] = $validacion[$observacion];

                    if ($validacion[$razon]=='error') {
                        $validacion[$razon] = NULL;
                    }
                    $validacion['razon'][$cant] = $validacion[$razon];

                    if ($validacion[$recomendacion]=='error') {
                        $validacion[$recomendacion] = NULL;
                    }
                    $validacion['recomendacion'][$cant] = $validacion[$recomendacion];

                    $validacion['uniCompra'][$cant] = $validacion[$uniCompra];
                    $validacion['formaPago'][$cant] = $validacion[$formaPago];

                    $maximo = $this->atInvitar->metObtenerMaximaCotizacion();
                    $validacion['ind_num_cotizacion'][$cant] = $maximo['maximo'] +$cant+1;
                    $validacion['ind_num_cotizacion'][$cant] = $this->metRellenarCeros($validacion['ind_num_cotizacion'][$cant],10);
                    $cant = $cant+1;
                }
            } else {
                $validacion['pUni'] = "0";
                $validacion['pIVA'] = "0";
                $validacion['dpor'] = "0";
                $validacion['dfijo'] = "0";
                $validacion['vofer'] = "0";
                $validacion['dias'] = "0";
                $validacion['finvi'] = null;
                $validacion['invi'] = null;
                $validacion['total'] = "0";
            }
            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }


            $id=$this->atInvitar->metCotizar($validacion);
            $validacion['status'] = 'nuevo';
            foreach ($validacion['idActaDet'] AS $key=>$value){
                #detalle del requerimiento $value
                $acta=$this->atInvitar->metBuscarActaDetalle($value);
                $acta2=$this->atInvitar->metActualizarActa($acta['pk_num_acta_inicio']);
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['id'] = $id;
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            echo json_encode($validacion);
            exit;
        }

        $acta = $this->atInvitar->metBuscarActaDet($idActaDet);
        $invi = $this->atInvitar->metMostrarInvitacionRealizada($idActaDet);

        $this->atVista->assign('formaPago',$this->atInvitar->atMiscelaneoModelo->metMostrarSelect('FDPLG'));
        $this->atVista->assign('unidades',$this->atInvitar->metListarUnidades());
        $this->atVista->assign('acta',$acta);
        $this->atVista->assign('idActaDet',$idActaDet);
        $this->atVista->assign('invitaciones',$invi);
        $this->atVista->metRenderizar('cotizar','modales');
    }

    //Método para generar el documento del cuadro comparativo PDF
    public function metCuadro($idDetalle)
    {
        $invitaciones = $this->atInvitar->metMostrarInvitacionesProveedores($idDetalle);
        if($invitaciones!=null){

            $detalles = $this->atInvitar->atActaInicioModelo->metMostrarActaDetalle($invitaciones[0]['pk_num_acta_inicio']);
            $cotizaciones = $this->atInvitar->metMostrarCotizacionesProveedores($invitaciones[0]['pk_num_acta_inicio']);
            $elaborado = $this->atInvitar->atActaInicioModelo->metMostrarEmpleado($this->atInvitar->atIdEmpleado);

            $revisado=$this->atInvitar->atActaInicioModelo->metMostrarEmpleado(Session::metObtener('DIR_ADMIN'));

            $revisadoPor = $revisado['nombre'];
            $revisadoPorCargo = $revisado['ind_descripcion_cargo'];
            define('ELABORADO_POR', $elaborado['nombre']);
            define('CARGO_ELABORADO_POR', $elaborado['ind_descripcion_cargo']);
            define('REVISADO_POR', $revisadoPor);
            define('CARGO_REVISADO_POR', $revisadoPorCargo);

            $this->atFPDF = new pdf('L','mm','legal');
            $this->atFPDF->SetTitle('evaluacion_anexo');
            $this->atFPDF->AliasNbPages();
            $this->atFPDF->SetMargins(5, 1, 5);
            $this->atFPDF->SetAutoPageBreak(5, 1);
            $this->atFPDF->AddPage();
            $this->atFPDF->SetFont('Times', '', 8);

            $html = '<table align="center" width="100%" height="63" border="1">';
            $html .= '<tr>
<td width="120" rowspan="2" ><b><br/><br/>Item/Commodity</b></td>
<td width="50" rowspan="2" ><b><br/><br/>Unid.</b></td>';

            $c = count($invitaciones);
            $w = 150;
            if($c>=3){
                $w = 125;

            }
            foreach ($invitaciones as $key=>$value) {
                $html .= '<td width="'.$w.'" height="" colspan="3"><b>Cotización N°: '.$value['ind_num_cotizacion'].'</b> <br> '.$value['nombre'].' </td>';
            }
            $html .='</tr><tr>';
            foreach ($invitaciones as $key=>$value) {
                $html .= '<td><b>Cant.</b></td><td><b>Precio Unit.</b></td><td><b>Total</b></td>';
            }

            $html .='</tr>';

            $subtotal = array();
            foreach ($detalles as $key=>$value) {
                $html .= '<tr><td>'.$value['ind_descripcion'].'</td><td>'.$value['unidad'].'</td>';

                foreach ($cotizaciones as $key2=>$value2) {
                    if($value2['fk_lgb023_num_acta_detalle'] == $value['pk_num_acta_detalle']) {
                        $total = number_format($value2['num_precio_unitario'] * $value2['num_cantidad'],2,',','');
                        if(isset($subtotal[$value2['fk_lgb022_num_proveedor']])){
                            $subtotal[$value2['fk_lgb022_num_proveedor']] = $subtotal[$value2['fk_lgb022_num_proveedor']] + $total;
                        } else {
                            $subtotal[$value2['fk_lgb022_num_proveedor']] = $total;
                        }
                        $html .= '<td>'.number_format($value2['num_cantidad'],2,',','').'</td><td>'.number_format($value2['num_precio_unitario'],2,',','.').'</td><td>'.$total.'</td>';
                    }
                }
                $html .='</tr>';

            }

            $html .='<tr><td colspan="2" align="left"><b>Sub-Total</b></td>';
            foreach ($invitaciones as $key=>$value) {
                $sub=$subtotal[$value['fk_lgb022_num_proveedor']];
                $html .= '<td colspan="3" align="rigth">'.number_format($sub,2,',','.').'</td>';
            }

            $html .='</tr><tr><td colspan="2" align="left"><b>Impuesto</b></td>';
            foreach ($invitaciones as $key=>$value) {
                $impuesto = $subtotal[$value['fk_lgb022_num_proveedor']] * 0.12;
                $html .= '<td colspan="3" align="rigth">'.number_format($impuesto,2,',','.').'</td>';
            }

            $html .='</tr><tr><td colspan="2" align="left"><b>Total</b></td>';
            foreach ($invitaciones as $key=>$value) {
                $impuesto = $subtotal[$value['fk_lgb022_num_proveedor']] * 0.12;
                $total = $impuesto + $subtotal[$value['fk_lgb022_num_proveedor']];
                $html .= '<td colspan="3" align="rigth">'.number_format($total,2,',','.').'</td>';
            }

            $html .= '</tr></table>';
            $this->atFPDF->ln(50);
            $this->atFPDF->writeHTML($html);
        } else {
            define('ELABORADO_POR', '');
            define('CARGO_ELABORADO_POR', '');
            define('REVISADO_POR', '');
            define('CARGO_REVISADO_POR', '');

            $this->atFPDF = new pdf('L','mm','legal');
            $this->atFPDF->SetTitle('evaluacion_anexo');
            $this->atFPDF->AliasNbPages();
            $this->atFPDF->SetMargins(5, 1, 5);
            $this->atFPDF->SetAutoPageBreak(5, 1);
            $this->atFPDF->AddPage();
            $this->atFPDF->SetFont('Times', '', 12);
            $this->atFPDF->SetXY(125,60);
            $this->atFPDF->Cell(80, 5, 'No hay Invitaciones o Cotizaciones para este Item/Commodity', 0, 0, 'L');
        }
        $this->atFPDF->Output('comparativo.pdf', 'I');
    }

    //Método para generar el documento del pliego de condiciones ODT
    public function metGenerarPliegoOdt() {
        $datos_ic = $this->metObtenerFormulas('datos_ic');
        $idDet = $datos_ic[0];
        #busco en la ruta el odt que me sirve de formato
        $this->atDocx = new Docxpresso\CreateDocument(array('template' => ROOT . 'publico' . DS . 'procesoCompra' . DS . 'pliego-plantilla.odt'));

        $acta = $this->atInvitar->metBuscarActaDet($idDet);
        $idActa = $acta['pk_num_acta_inicio'];
        $detalles = $this->atInvitar->atActaInicioModelo->metMostrarActaDetalle($idActa);
        $asisA = $this->atInvitar->atActaInicioModelo->metMostrarEmpleado($acta['fk_rhb001_empleado_asistente_a']);
        $asisB = $this->atInvitar->atActaInicioModelo->metMostrarEmpleado($acta['fk_rhb001_empleado_asistente_b']);
        $asisC = $this->atInvitar->atActaInicioModelo->metMostrarEmpleado($acta['fk_rhb001_empleado_asistente_c']);

        if($acta['cod_detalle']==1) {
            $tipoServicio = 'quince (15) días hábiles para adquisición de bienes';
            $tipoServicioDetallado = '04 días hábiles para bienes';
            $tipoServicioDetalladoFin = '08 días para adquisiciòn de bienes';
            $DiaE = '2';
            $DiaRc = '2';
            $DiaAD = '2';
            $DiaNA = '2';
        } else if($acta['cod_detalle']==2) {
            $tipoServicio = 'diecisiete (17) días hábiles para la prestación de servicio';
            $tipoServicioDetallado = '05 días hábiles para la prestacion de servicio';
            $tipoServicioDetalladoFin = '09 dias  para la prestaciòn de servicio';
            $DiaE = '2';
            $DiaRc = '3';
            $DiaAD = '2';
            $DiaNA = '2';
        } else if($acta['cod_detalle']==3) {
            $tipoServicio = 'diecinueve (19) días hábiles para ejecución de obras';
            $tipoServicioDetallado='06 días hábiles para ejecucion de obras';
            $tipoServicioDetalladoFin='10 dias para ejecusion de obras';
            $DiaE='3';
            $DiaRc='3';
            $DiaAD='2';
            $DiaNA='2';
        } else {
            $tipoServicio = '0';
            $tipoServicioDetallado='0';
            $tipoServicioDetalladoFin='0';
            $DiaE='0';
            $DiaRc='0';
            $DiaAD='0';
            $DiaNA='0';
        }

/*Colocar logo en los odt
        $headerStyle = array('style' => 'min-height: 3cm');
        $urlImg = ROOT . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . 'CES.jpg';
        $rowStyle = array('style' => 'min-height: 25px');
        $cellStyle = array('style' => 'margin-left: 270px; vertical-align: middle;');
        $this->atDocx->header($headerStyle)
            ->table(array('grid' => array('660px')))
            ->row($rowStyle)
            ->cell($cellStyle)->paragraph()->image(array('src' => $urlImg, 'style' => 'width:100px; height:80px;'))
        ;
Colocar logo en los odt*/

        $tablaModalidad = '<table border="1" style="font-family: Arial; font-size: 9pt;">
                    <tr style="background-color: #a0a0a0; text-align: center;">
                    <td colspan="2"><b>CRONOGRAMA DE EJECUCIÓN</b></td>
                    </tr>
                    <tr style="text-align: center; tex">
                    <td><b>FASES</b></td>
                    <td><b>LAPSO</b></td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Recepción de Ofertas.</td>
                    <td>'.$tipoServicioDetallado.' contados a partir del dia hábil siguiente a la recepción de la oferta. Art. 67 del Decreto Nº 1.399 de la Ley de Contrataciones Públicas.</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Aclaratoria.</td>
                    <td>01 día hábil. Art. 70 del Decreto Nº 1.399 de la Ley de Contrataciones Públicas. Aclaratoria el día siguiente a la recepción de la invitación.</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Modificación por parte del Órgano Contratante.</td>
                    <td>02 días hábiles. Art. 68.del Decreto Nº 1.399 de la Ley de Contrataciones Públicas. Antes de la fecha límite de la recepción de la Oferta.</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Examen de las Ofertas. '.$DiaE.' días</td>
                    <td rowspan="4" style="vertical-align:middle;">'.$tipoServicioDetalladoFin.' Art. 98 del Decreto Nº 1.399  de la Ley de Contrataciones Públicas.</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Informe de Recomendación  '.$DiaRc.' días</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Adjudicación o Declaratoria Desierta. '.$DiaAD.' días</td>
                    </tr>
                    <tr style="text-align: justify;">
                    <td>Notificación de la Adjudicación. '.$DiaNA.' días</td>
                    </tr>
                    </table>';
        $this->metReemplazar('tablaModalidad', $tablaModalidad,1);

        $tablaReglon = '<table border="1" style="font-family: Arial; font-size: 10pt;">
                    <tr style="text-align: center;">
                    <td width="75px"><b>RENGLON<br/></b></td>
                    <td width="300px"><b>DESCRIPCIÓN<br/></b></td>
                    <td width="75px"><b>CANTIDAD<br/></b></td>
                    </tr>';
        foreach ($detalles as $key=>$value) {
            $tablaReglon .= '
                    <tr>
                    <td style="text-align: center;" width="75px"><br/>'.$value['num_secuencia'].'<br/></td>
                    <td width="300px"><br/>'.$value['ind_descripcion'].'<br/></td>
                    <td style="text-align: center;" width="75px"><br/>'.number_format($value['num_cantidad_pedida'],2,',','').'<br/></td>
                    </tr>';
        }
        $tablaReglon .= '</table>';

        $diaNro = substr($acta['fec_limite'],8,2);
        $mesNro = substr($acta['fec_limite'],5,2);
        $anioNro = substr($acta['fec_limite'],0,4);
        $diaSemNro = date("w", mktime(0, 0, 0,$mesNro, $diaNro,  $anioNro));

        $cargo='';
        $cargoAnalista1='';
        $cargoAnalista2='';
        if($asisA['ind_descripcion_cargo']){
            $cargo=$asisA['ind_descripcion_cargo'];
        }
        if($asisB['ind_descripcion_cargo']){
            $cargoAnalista1=$asisB['ind_descripcion_cargo'];
        }
        if($asisC['ind_descripcion_cargo']){
            $cargoAnalista2=$asisC['ind_descripcion_cargo'];
        }
        $this->metReemplazar('tablaR', $tablaReglon,1);
        $this->metReemplazar('fecha',$this->metDiaSemLetras($diaSemNro).' ('.$this->metFormatoFecha($acta['fec_limite'],1,'/').')');
        $this->metReemplazar('nroActa',$acta['ind_codigo_acta']);
        $this->metReemplazar('requerimiento',$acta['ind_nombre_procedimiento']);
        $this->metReemplazar('montoL',$this->metNumtoletras($acta['num_presupuesto_base']));
        $this->metReemplazar('pb',number_format($acta['num_presupuesto_base'],2,',','.'));
        $this->metReemplazar('nombreDirector',$asisA['nombre']);
        $this->metReemplazar('nombreAnalista1',$asisB['nombre']);
        $this->metReemplazar('nombreAnalista2',$asisC['nombre']);
        $this->metReemplazar('cargo',$cargo);
        $this->metReemplazar('cargoAnalista1',$cargoAnalista1);
        $this->metReemplazar('cargoAnalista2',$cargoAnalista2);

        $this->atDocx->render(ROOT . 'publico' .DS.'procesoCompra' .DS.'pliego'.$idActa.'.odt');
        $validacion['idActa']=$idActa;
        $validacion['ruta']='.'.DS.'publico' .DS.'procesoCompra' .DS.'pliego'.$idActa.'.odt';
        echo json_encode($validacion);
        exit;
    }

    //Método para reemplazar las variables en el documento a generar ODT
    public function metReemplazar($nombre,$valor,$extra=false){
        if($extra==1) {
            $this->atDocx->replace(array($nombre => array('value' => $valor)), array('block-type' => true));
        } else {
            $this->atDocx->replace(array($nombre => array('value' => $valor)));
        }
    }

    //Método para paginar los items
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  detalle.*,
                  inicio.ind_codigo_acta,
                  item.ind_codigo_interno
              FROM lg_b023_acta_detalle AS detalle
              INNER JOIN lg_b009_acta_inicio AS inicio ON inicio.pk_num_acta_inicio = detalle.fk_lgb009_num_acta_inicio
              INNER JOIN lg_b002_item AS item ON item.pk_num_item = detalle.fk_lgb002_num_item
              INNER JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON inicio.pk_num_acta_inicio = acta_requerimiento.fk_lgb009_num_acta_inicio
              INNER JOIN lg_b001_requerimiento AS requerimiento ON acta_requerimiento.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
              INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = requerimiento.fk_a004_num_dependencia
              AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
              WHERE inicio.ind_estado != 'AN' 
              ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (detalle.pk_num_acta_detalle LIKE '%$busqueda[value]%' OR
                        inicio.ind_codigo_acta LIKE '%$busqueda[value]%' OR
                        detalle.num_secuencia LIKE '%$busqueda[value]%' OR
                        item.ind_codigo_interno LIKE '%$busqueda[value]%' OR
                        detalle.ind_descripcion LIKE '%$busqueda[value]%') 
                         ";
        }
        $sql .= " GROUP BY detalle.pk_num_acta_detalle ";

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_acta_detalle','ind_codigo_acta','num_secuencia','ind_codigo_interno','ind_descripcion');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_acta_detalle';

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }

    //Método para paginar los commodities
    public function metJsonDataTabla1()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  detalle.*,
                  inicio.ind_codigo_acta,
                  commodity.ind_cod_commodity
              FROM lg_b023_acta_detalle AS detalle
              INNER JOIN lg_b009_acta_inicio AS inicio ON inicio.pk_num_acta_inicio = detalle.fk_lgb009_num_acta_inicio
              INNER JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle.fk_lgb003_num_commodity
              INNER JOIN lg_c011_acta_requerimiento AS acta_requerimiento ON inicio.pk_num_acta_inicio = acta_requerimiento.fk_lgb009_num_acta_inicio
              INNER JOIN lg_b001_requerimiento AS requerimiento ON acta_requerimiento.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
              INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = requerimiento.fk_a004_num_dependencia
              AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
              WHERE inicio.ind_estado != 'AN' 
              ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (detalle.pk_num_acta_detalle LIKE '%$busqueda[value]%' OR
                        inicio.ind_codigo_acta LIKE '%$busqueda[value]%' OR
                        detalle.num_secuencia LIKE '%$busqueda[value]%' OR
                        commodity.ind_cod_commodity LIKE '%$busqueda[value]%' OR
                        detalle.ind_descripcion LIKE '%$busqueda[value]%') 
                        ";
        }
        $sql .= " GROUP BY detalle.pk_num_acta_detalle ";
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_acta_detalle','ind_codigo_acta','num_secuencia','ind_cod_commodity','ind_descripcion');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_acta_detalle';

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }

}
