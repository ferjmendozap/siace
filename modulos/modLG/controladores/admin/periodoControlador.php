<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class periodoControlador extends Controlador
{
    private $atPeriodo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atPeriodo = $this->metCargarModelo('periodo', 'admin');
    }

    //Método para listar los periodos
    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear y/o modificar los periodos
    public function metCrearModificarPeriodo()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idPeriodo = $this->metObtenerInt('idPeriodo');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $ExcceccionInt= array('tran');
            $ExcceccionText= array('estado');
            $ind = $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
            $text = $this->metValidarFormArrayDatos('form','txt',$ExcceccionText);
            if ($ind!=null){
                $validacion = array_merge($ind,$text);
            } else {
                $validacion = $text;
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            $validacion['mes'] = substr($validacion['periodo'],-2);
            $validacion['anio'] = substr($validacion['periodo'],0,4);

            if (!isset($validacion['estado'])) {
                $validacion['estado'] = "I";
            }
            if (!isset($validacion['tran'])) {
                $validacion['tran'] = 0;
            }

            if($idPeriodo===0){
               $id=$this->atPeriodo->metCrearPeriodo($validacion['mes'],$validacion['anio'],$validacion['tran'],$validacion['estado']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atPeriodo->metModificarPeriodo($validacion['mes'],$validacion['anio'],$validacion['tran'],$validacion['estado'],$idPeriodo);
                $validacion['status']='modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                $validacion['periodo'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idPeriodo']=$id;

            echo json_encode($validacion);
            exit;
        }
        if($idPeriodo!=0){
            $this->atVista->assign('formDBPeriodo',$this->atPeriodo->metMostrarPeriodo($idPeriodo));
            $this->atVista->assign('idPeriodo',$idPeriodo);
            $this->atVista->assign('ver',$ver);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para eliminar los periodos
    public function metEliminarPeriodo()
    {
        $idPeriodo = $this->metObtenerInt('idPeriodo');
        if($idPeriodo!=0){
            $id=$this->atPeriodo->metEliminarPeriodo($idPeriodo);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el Periodo se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'id'=>$idPeriodo
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    //Método para paginar los periodos
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  *
                FROM lg_b018_control_periodo
                WHERE 1 ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (pk_num_periodo LIKE '%$busqueda[value]%' OR
                        fec_anio LIKE '%$busqueda[value]%' OR
                        fec_mes LIKE '%$busqueda[value]%' 
                        ) 
                        ";
        }
        $flag = array('num_flag_transaccion');

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('pk_num_periodo','fec_anio','fec_mes','num_flag_transaccion','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_periodo';
        #construyo el listado de botones

        if (in_array('LG-01-04-01-02-M',$rol)) {
            $campos['boton']['Editar'] = "
                <button accion='modificar' title='Editar'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        descipcion='El Usuario ha Modificado el Periodo Nro. $clavePrimaria'
                        idPeriodo='$clavePrimaria' titulo='Modificar Periodo'
                        data-toggle='modal' data-target='#formModal' id='modificar'>
                    <i class='fa fa-edit'></i>
                </button>
                "
            ;
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('LG-01-04-01-03-V',$rol)) {
            $campos['boton']['Consultar'] = "
                <button accion='ver' title='Consultar'
                        class='ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning'
                        descipcion='El Usuario ha Consultado el Periodo Nro. $clavePrimaria'
                        idPeriodo='$clavePrimaria' titulo='Consultar Periodo'
                        data-toggle='modal' data-target='#formModal' id='modificar'>
                    <i class='icm icm-eye2'></i>
                </button>
                "
            ;
        } else {
            $campos['boton']['Consultar'] = false;
        }

        if (in_array('LG-01-04-01-04-E',$rol)) {
            $campos['boton']['Eliminar'] = "
                <button accion='eliminar' title='Eliminar'
                        class='eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger'
                        descipcion='El Usuario ha Eliminado el Periodo Nro. $clavePrimaria'
                        idPeriodo='$clavePrimaria' titulo='Eliminar Periodo'
                        mensaje='Estas seguro que desea eliminar el Periodo!!' id='eliminar'>
                    <i class='md md-delete'></i>
                </button>
                "
            ;
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false,$flag);
    }
}
