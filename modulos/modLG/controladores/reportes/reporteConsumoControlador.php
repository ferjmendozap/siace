<?php
class reporteConsumoControlador extends Controlador
{
    public $atReporte;
    public $atTransaccionModelo;
    public $atMiscelaneoModelo;
    public $atOrdenCompraModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reporteConsumo', 'reportes');
        Session::metAcceso();
        #se carga la Libreria.
        $this->metObtenerLibreria('headerConsumo','modLG');
        $this->atFPDF = new pdf('L', 'mm', 'Letter');
    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('dependencia',$this->atReporte->atOrdenCompraModelo->metListarDependencias());
        $this->atVista->metRenderizar('filtro');
    }

    public function metCentroCosto($cual=false,$i=false)
    {
        $this->atVista->assign('cual',$cual);
        $this->atVista->assign('i',$i);
        $this->atVista->assign('centroCosto',$this->atReporte->atTransaccionModelo->metListarCC());
        $this->atVista->metRenderizar('centroCostos','modales');
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function metGenerarReporte($fdesde,$fhasta,$depen,$cCosto)
    {
        if($cCosto!='no'){ $cCosto = "WHERE pk_num_centro_costo = '".$cCosto."'"; } else { $cCosto = "WHERE 1"; }
        if($depen!='no'){ $depen = "AND pk_num_dependencia = '".$depen."'"; } else { $depen = ""; }
        if($fdesde!='no' AND $fhasta!='no'){ $fecha = "WHERE transaccion.fec_documento >= '".$fdesde."' AND transaccion.fec_documento <= '".$fhasta."' "; } else { $fecha = "WHERE 1"; }

        $dependencias = $this->atReporte->atOrdenCompraModelo->metListarDependencias($depen);
        $cc = $this->atReporte->atTransaccionModelo->metListarCC($cCosto);
        $transacciones = $this->atReporte->metListarTranDet($fecha);

        $this->atFPDF->SetTitle(utf8_decode('Consumo por Dependencias'));
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5, 35);
        $this->atFPDF->AddPage();

        $depCont=0;
        $i=array();
        foreach ($dependencias as $titulo1=>$valor1) {
            $ccCont=0;
            $totalItem=array();
            foreach ($cc as $titulo2=>$valor2) {
                $tranCont=0;
                foreach ($transacciones as $titulo3=>$valor3) {
                    if ($depCont == 0 AND $cc[$titulo2]['fk_a004_num_dependencia'] == $dependencias[$titulo1]['pk_num_dependencia'] AND $transacciones[$titulo3]['fk_a023_num_centro_costo'] == $cc[$titulo2]['pk_num_centro_costo']) {
                        $depCont++;
                        $this->atFPDF->SetDrawColor(255, 255, 255);
                        $this->atFPDF->SetFillColor(255, 255, 255);
                        $this->atFPDF->SetFont('Arial', 'BU', 6);
                        $this->atFPDF->Cell(20, 4, 'Dependencia: ', 0, 0, 'L');
                        $this->atFPDF->Cell(240, 4, utf8_decode($dependencias[$titulo1]['ind_dependencia']), 0, 1, 'L');
                    }
                    if ($ccCont == 0 AND $cc[$titulo2]['fk_a004_num_dependencia'] == $dependencias[$titulo1]['pk_num_dependencia'] AND $transacciones[$titulo3]['fk_a023_num_centro_costo'] == $cc[$titulo2]['pk_num_centro_costo']) {
                        $ccCont++;
                        $this->atFPDF->SetFont('Arial', 'B', 6);
                        $this->atFPDF->Cell(20, 4, 'Centro de Costo: ', 0, 0, 'L');
                        $this->atFPDF->Cell(240, 4, utf8_decode($cc[$titulo2]['ind_abreviatura'].' '.$cc[$titulo2]['ind_descripcion_centro_costo']), 0, 1, 'L');
                    }

                    if ($cc[$titulo2]['fk_a004_num_dependencia'] == $dependencias[$titulo1]['pk_num_dependencia'] AND $transacciones[$titulo3]['fk_a023_num_centro_costo'] == $cc[$titulo2]['pk_num_centro_costo']) {
                        $this->atFPDF->SetDrawColor(255, 255, 255);
                        $this->atFPDF->SetFillColor(255, 255, 255);
                        $this->atFPDF->SetTextColor(0, 0, 0);
                        $this->atFPDF->SetFont('Arial', '', 6);
                        $pk = $transacciones[$titulo3]['ind_codigo_interno'];


                        #total de items gastados por centro de costo
                        foreach ($transacciones as $titulo4=>$valor4) {
                            if(isset($totalItem[$pk]) AND
                                $transacciones[$titulo4]['fk_lgb002_num_item']==$pk AND
                                $transacciones[$titulo4]['fk_a023_num_centro_costo'] == $cc[$titulo2]['pk_num_centro_costo']
                            ){
                                $totalItem[$pk] = $totalItem[$pk] + $transacciones[$titulo3]['num_cantidad_transaccion'];
                            } else {
                                $totalItem[$pk] = $transacciones[$titulo3]['num_cantidad_transaccion'];
                            }
                        }


                        #para que no se repitan los items

                        if(isset($i[$pk]) AND $i[$pk]==1){
                            $i[$pk]=$i[$pk]+1;
                        } else {
                            $i[$pk]=1;
                            $empleado = $this->atReporte->atTransaccionModelo->metMostrarEmpleado($transacciones[$titulo3]['fk_rhb001_num_empleado_ejecutado_por']);
                            $this->atFPDF->Row(array($pk,
                                utf8_decode($transacciones[$titulo3]['descripcionItem']),
                                utf8_decode($transacciones[$titulo3]['num_transaccion']."-".$transacciones[$titulo3]['fec_anio']),
                                number_format($totalItem[$pk], 2, ',', '.'),
                                number_format($transacciones[$titulo3]['num_cantidad_transaccion']*$transacciones[$titulo3]['num_precio_unitario'], 2, ',', '.'),
                                utf8_decode($empleado['ind_nombre1']." ".$empleado['ind_apellido1'])
                            ));
                            $this->atFPDF->Ln(1);
                        }

                    }
                } #transacciones
            } #centro de costo
        } #dependencia

        #salida del pdf
        $this->atFPDF->Output('Consumo por Dependencias.pdf','I');
    }
}

?>