<?php
class reporteControlPerceptivoControlador extends Controlador
{
    public $atReporte;
    public $atOrdenCompraModelo;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = require_once ROOT . 'modulos' . DS . 'modLG' . DS . 'modelos' . DS . 'reportes' . DS . 'reporteControlPerceptivoModelo.php';
        $this->atReporte = new reporteControlPerceptivoModelo;
        Session::metAcceso();
        #se carga la Libreria.

    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('filtro');
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function estadoNombre($estado){
        $e="";
        if($estado=="PR"){
            $e="En Preparacion";
        } elseif($estado=="RV"){
            $e="Revisado";
        } elseif($estado=="CN"){
            $e="Conformado";
        } elseif($estado=="AN"){
            $e="Anulado";
        } elseif($estado=="CE"){
            $e="Cerrado";
        } elseif($estado=="TE"){
            $e="Terminado";
        } elseif($estado=="DS"){
            $e="Desierto";
        } elseif($estado=="PE"){
            $e="Pendiente";
        } elseif($estado=="AP"){
            $e="Aprobado";
        } elseif($estado=="CO"){
            $e="Completado";
        }
        return $e;
    }

    public function metGenerarReporte($fdesde,$fhasta)
    {


        $this->metObtenerLibreria('headerControlPerceptivo','modLG');
        $this->atFPDF = new pdf('L', 'mm', 'Letter');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5, 35);
        $where = "";
        if($fdesde!='no' AND $fhasta!='no'){
            $where .= " AND (perceptivo.fec_registro>='$fdesde 00:00:00' AND perceptivo.fec_registro<='$fhasta 23:59:59') ";
        } else {
            $fdesde = '';
            $fhasta = '';
        }
        define('DESDE',$fdesde);
        define('HASTA',$fhasta);

        $elaboradoPor = $this->atReporte->metMostrarEmpleado(Session::metObtener('idEmpleado'));

        define('ELABORADO_POR',$elaboradoPor['nombre']);
        define('ELABORADO_CARGO',$elaboradoPor['ind_descripcion_cargo']);
        $this->atFPDF->AddPage();

        $controles = $this->atReporte->metListarControlesPreceptivos($where);

        foreach ($controles as $control) {

            $codCP = 'CP-'.$this->metRellenarCeros($control['num_control'],4).'-'.$control['fec_anio'];

            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetFont('Arial', '', 7);
            $this->atFPDF->Row(array($codCP,
                $control['ind_codigo_acta'],
                utf8_decode($control['ind_comentario']),
                $control['ind_orden'],
                utf8_decode($control['nombreProveedor']),
                $control['fec_registro']));
            $this->atFPDF->Ln(1);
            
        }

        #salida del pdf
        $this->atFPDF->Output();
    }

}


?>