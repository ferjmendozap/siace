<?php
class reporteCotizacionesControlador extends Controlador
{
    public $atReporte;
    public $atItemsModelo;
    public $atCommodityModelo;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reporteCotizaciones', 'reportes');
        Session::metAcceso();
        #se carga la Libreria.
        $this->metObtenerLibreria('header','modLG');
        $this->atFPDF = new pdf('p','mm','letter');
    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('filtro');
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function metGenerarReporte($fdesde,$fhasta,$item,$commodity)
    {
        $filtro="WHERE 1 ";
        if($fdesde!='no' AND $fhasta!='no'){
            $filtro.="";
        }
        if($item!='no'){
            $filtro.="AND detalle.fk_lgb002_num_item = '".$item."'";
            $listaItem = $this->atReporte->atItemsModelo->metListarItems();
            foreach($listaItem AS $titulo=>$valor){
                if($listaItem[$titulo]['pk_num_item']==$item){
                    $codigo = $listaItem[$titulo]['ind_codigo_interno'];
                    $descripcion = $listaItem[$titulo]['descripcion'];
                    $unidad = $listaItem[$titulo]['uni'];
                }
            }
        }
        if($commodity!='no'){
            $filtro.="AND detalle.fk_lgb003_num_commodity = '".$commodity."'";
            $listaComm = $this->atReporte->atCommodityModelo->metListarCommodity();
            foreach($listaComm AS $titulo=>$valor){
                if($listaComm[$titulo]['pk_num_commodity']==$commodity){
                    $codigo = $listaItem[$titulo]['ind_cod_commodity'];
                    $descripcion = $listaComm[$titulo]['ind_descripcion'];
                    $unidad = $listaComm[$titulo]['unidad'];
                }
            }
        }

//        echo $filtro;

        $cotizaciones = $this->atReporte->metListarCotizaciones($filtro);

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5, 1);
        $this->atFPDF->AddPage();
    /*    $this->atFPDF->Image(ROOT.'publico'.DS.'imagenes'.DS.'logos'.DS."CES.jpg", 5, 5, 10, 10);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetXY(15, 5); $this->atFPDF->Cell(100, 5, 'NOMBRE_ORGANISMO_ACTUAL', 0, 1, 'L');
        $this->atFPDF->SetXY(15, 10); $this->atFPDF->Cell(100, 5, utf8_decode('Dirección de Administración y Presupuesto'), 0, 0, 'L');

        $this->atFPDF->SetXY(179, 5); $this->atFPDF->Cell(15, 5, utf8_decode('Fecha: '), 0, 0, 'R');
        $this->atFPDF->Cell(40, 5, date("d-m-Y"), 0, 1, 'L');*/
        $this->atFPDF->SetXY(180, 18); $this->atFPDF->Cell(15, 5, utf8_decode('Página: '), 0, 0, 'R');
        $this->atFPDF->Cell(40, 5, $this->atFPDF->PageNo().' de {nb}', 0, 1, 'L');

        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetXY(5, 20); $this->atFPDF->Cell(195, 5, utf8_decode('Cotizaciones'), 0, 1, 'C');
        $this->atFPDF->Ln(3);

        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->SetXY(5, 22);
        if($fdesde!='no'){
            $this->atFPDF->Cell(195, 5, utf8_decode(' Desde:  '.$fdesde.'  Hasta:  '.$fhasta), 0, 1, 'L');
        } else {
            $this->atFPDF->Cell(195, 5, '', 0, 1, 'L');
        }
        $this->atFPDF->Ln(2);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->SetWidths(array(30,30, 65, 30, 20,30));
        $this->atFPDF->SetAligns(array('C','C', 'C', 'C','C','C'));
        $this->atFPDF->Row(array(utf8_decode('Nº Cotización'),
            utf8_decode('Fecha de Cotización'),
            utf8_decode('Proveedor'),
            utf8_decode('Precio Unit.'),
            utf8_decode('Cant.'),
            utf8_decode('Total')
        ));

        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        
        $this->atFPDF->Ln(2);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(35, 8, 'Cod. Item / Commodity: ', 0, 0, 'R', 1);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(25, 8, $codigo, 0, 0, 'L', 1);
        $this->atFPDF->Cell(13, 8, 'Unidad: ', 0, 0, 'R', 1);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->Cell(13, 8, utf8_decode($unidad), 0, 1, 'L', 1);
        //$this->atFPDF->Ln(1);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(20, 8, utf8_decode('Descripción: '), 0, 0, 'R', 1);
        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->MultiCell(200, 5, utf8_decode($descripcion), 0, 'L');
        $this->atFPDF->Cell(13, 8, utf8_decode(''), 0, 1, 'L', 1);
        $this->atFPDF->Ln(3);

        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetWidths(array(30,30, 65, 30, 20,30));
        foreach($cotizaciones AS $titulo=>$valor){
            $this->atFPDF->Row(array($cotizaciones[$titulo]['pk_num_cotizacion'],
                $cotizaciones[$titulo]['fec_documento'],
                utf8_decode($cotizaciones[$titulo]['pk_num_cotizacion']),
                number_format($cotizaciones[$titulo]['pk_num_acta_detalle'],4, ',', '.'),
                number_format($cotizaciones[$titulo]['pk_num_acta_detalle'],4, ',', '.'),
                number_format($cotizaciones[$titulo]['pk_num_acta_detalle'], 4, ',', '.')
            ));

            $this->atFPDF->Ln(3);
        }


        #salida del pdf
        $this->atFPDF->Output();
    }

}


?>