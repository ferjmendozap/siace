<?php
class reporteOrdenCompraControlador extends Controlador
{
    public $atReporte;
    public $atOrdenCompraModelo;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = require_once ROOT . 'modulos' . DS . 'modLG' . DS . 'modelos' . DS . 'reportes' . DS . 'reporteOrdenCompraModelo.php';
        $this->atReporte = new reporteOrdenCompraModelo;
//        $this->atReporte = $this->metCargarModelo('reporteOrdenCompraModelo', 'reportes');
        Session::metAcceso();
        #se carga la Libreria.

    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('filtro');
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function estadoNombre($estado){
        $e="";
        if($estado=="PR"){
            $e="En Preparacion";
        } elseif($estado=="RV"){
            $e="Revisado";
        } elseif($estado=="CN"){
            $e="Conformado";
        } elseif($estado=="AN"){
            $e="Anulado";
        } elseif($estado=="CE"){
            $e="Cerrado";
        } elseif($estado=="TE"){
            $e="Terminado";
        } elseif($estado=="DS"){
            $e="Desierto";
        } elseif($estado=="PE"){
            $e="Pendiente";
        } elseif($estado=="AP"){
            $e="Aprobado";
        }
        return $e;
    }

    public function metGenerarReporte($proveedor,$clasificacion,$estado,$fdesde,$fhasta,$mmenor,$mmayor)
    {
        if($fdesde=='no'){
            $fdesde = "todos";
            $fhasta = "todos";
        }
        define('DESDE',$fdesde);
        define('HASTA',$fhasta);
       $this->metObtenerLibreria('headerOrdenCompra','modLG');
        $this->atFPDF = new pdf('P', 'mm', 'Letter');
        if($estado!='no'){
            $fordenes = $estado;
        } else {
            $fordenes = false;
        }
        $ordenes = $this->atReporte->atOrdenCompraModelo->metListarOrdenCompras($fordenes,false,false,'OC');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5,35);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $grupo="";
        
        foreach($ordenes AS $titulo=>$valor){

            if ($grupo != $ordenes[$titulo]['fk_lgb022_num_proveedor']) {
                $grupo = $ordenes[$titulo]['fk_lgb022_num_proveedor'];
                $grupo2 = "";
                $total = 0;

                $this->atFPDF->SetFillColor(245, 245, 245);
                $this->atFPDF->SetFont('Arial', 'B', 7);
                $this->atFPDF->Cell(17, 8, 'Proveedor: ', 0, 0, 'R', 1);
                $this->atFPDF->Cell(10, 8, $ordenes[$titulo]['fk_lgb022_num_proveedor'], 0, 0, 'L', 1);
                $this->atFPDF->Cell(140, 8, utf8_decode($ordenes[$titulo]['ind_nombre1'] . " " . $ordenes[$titulo]['ind_apellido1']), 0, 0, 'L', 1);
                $this->atFPDF->Cell(13, 8, 'Total Proveedor: ', 0, 0, 'R', 1);
                $this->atFPDF->Cell(25, 8, number_format($ordenes[$titulo]['TotalProveedor'], 2, ',', '.'), 0, 1, 'R', 1);
            }
                  if ($grupo2 !=  $ordenes[$titulo]['pk_num_orden']) {
                      $grupo2 = $ordenes[$titulo]['pk_num_orden'];
                      $this->atFPDF->SetFillColor(255, 255, 255);
                      $this->atFPDF->SetFont('Arial', 'B', 7);
                      $this->atFPDF->Cell(17, 8, 'NroOrden: ', 0, 0, 'R');
                      $this->atFPDF->Cell(15, 8, $ordenes[$titulo]['ind_orden'], 0, 0, 'L');
                      $this->atFPDF->Cell(30, 8, $ordenes[$titulo]['fec_creacion'], 0, 0, 'C');
                      $this->atFPDF->Cell(105, 8, $this->estadoNombre($ordenes[$titulo]['estatus']), 0, 0, 'L');
                      $this->atFPDF->Cell(13, 8, 'Total Orden: ', 0, 0, 'R');
                      $this->atFPDF->Cell(25, 8, number_format($ordenes[$titulo]['num_moo_total'], 2, ',', '.'), 0, 1, 'R');

                     $detalles = $this->atReporte->atOrdenCompraModelo->metMostrarOrdenDetalle($ordenes[$titulo]['pk_num_orden']);

                      foreach($detalles AS $titulo1=>$valor1){
                          $this->atFPDF->SetFillColor(255, 255, 255);
                          $this->atFPDF->SetFont('Arial', '', 7);
                          if($detalles[$titulo1]['pk_num_item']!=null){
                              $codigo = $this->metRellenarCeros($detalles[$titulo1]['pk_num_item'],10);
                              $descripcion = $detalles[$titulo1]['ind_descripcion_item'];
                              $unidad = $detalles[$titulo1]['uniItem'];
                          } else {
                              $codigo = $this->metRellenarCeros($detalles[$titulo1]['pk_num_commodity'],6);
                              $descripcion = $detalles[$titulo1]['ind_descripcion_commodity'];
                              $unidad = $detalles[$titulo1]['uniComm'];
                          }
                          $this->atFPDF->Row(array($codigo,
                              utf8_decode($descripcion),
                              utf8_decode($unidad),
                              number_format($detalles[$titulo1]['num_cantidad'], 2, ',', '.'),
                              number_format(1, 2, ',', '.'),
                              '0000-00-00',
                              1,
                              $this->estadoNombre($detalles[$titulo1]['detalleEstatus'])));
                      }
                  }
            $total += 1;
            $y = $this->atFPDF->GetY();
            /*
                        $this->atFPDF->Row(array(
                            $ordenes[$titulo]['ind_orden'],
                            $ordenes[$titulo]['fec_creacion'],
                            $this->estadoNombre($ordenes[$titulo]['num_estatus'])
                        ));
*/
            $this->atFPDF->Ln(1);

        }

        #salida del pdf
        $this->atFPDF->Output();
    }


    public function metGenerarReporte2($proveedor,$clasificacion,$estado,$fdesde,$fhasta,$mmenor,$mmayor)
    {
        if($fdesde=='no'){
            $fdesde = "todos";
            $fhasta = "todos";
        }
        define('DESDE',$fdesde);
        define('HASTA',$fhasta);
        $this->metObtenerLibreria('headerOrdenCompra','modLG');
        $this->atFPDF = new pdf('P', 'mm', 'Letter');
        if($estado!='no'){
            $fordenes = $estado;
        } else {
            $fordenes = false;
        }
        $ordenes = $this->atReporte->atOrdenCompraModelo->metListarOrdenCompras($fordenes,false,false,'OC');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5,35);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $grupo="";

        foreach($ordenes AS $titulo=>$valor){

            if ($grupo != $ordenes[$titulo]['fk_lgb022_num_proveedor']) {
                $grupo = $ordenes[$titulo]['fk_lgb022_num_proveedor'];
                $grupo2 = "";
                $total = 0;

                $this->atFPDF->SetFillColor(245, 245, 245);
                $this->atFPDF->SetFont('Arial', 'B', 7);
                $this->atFPDF->Cell(17, 8, 'Proveedor: ', 0, 0, 'R', 1);
                $this->atFPDF->Cell(10, 8, $ordenes[$titulo]['fk_lgb022_num_proveedor'], 0, 0, 'L', 1);
                $this->atFPDF->Cell(140, 8, utf8_decode($ordenes[$titulo]['ind_nombre1'] . " " . $ordenes[$titulo]['ind_apellido1']), 0, 0, 'L', 1);
                $this->atFPDF->Cell(13, 8, 'Total Proveedor: ', 0, 0, 'R', 1);
                $this->atFPDF->Cell(25, 8, number_format($ordenes[$titulo]['TotalProveedor'], 2, ',', '.'), 0, 1, 'R', 1);
            }
            if ($grupo2 !=  $ordenes[$titulo]['pk_num_orden']) {
                $grupo2 = $ordenes[$titulo]['pk_num_orden'];
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', 'B', 7);
                $this->atFPDF->Cell(17, 8, 'NroOrden: ', 0, 0, 'R');
                $this->atFPDF->Cell(15, 8, $ordenes[$titulo]['ind_orden'], 0, 0, 'L');
                $this->atFPDF->Cell(30, 8, $ordenes[$titulo]['fec_creacion'], 0, 0, 'C');
                $this->atFPDF->Cell(105, 8, $this->estadoNombre($ordenes[$titulo]['estatus']), 0, 0, 'L');
                $this->atFPDF->Cell(13, 8, 'Total Orden: ', 0, 0, 'R');
                $this->atFPDF->Cell(25, 8, number_format($ordenes[$titulo]['num_monto_total'], 2, ',', '.'), 0, 1, 'R');

                $detalles = $this->atReporte->atOrdenCompraModelo->metMostrarOrdenDetalle($ordenes[$titulo]['pk_num_orden']);

                foreach($detalles AS $titulo1=>$valor1){
                    $this->atFPDF->SetFillColor(255, 255, 255);
                    $this->atFPDF->SetFont('Arial', '', 7);
                    if($detalles[$titulo1]['pk_num_item']!=null){
                        $codigo = $this->metRellenarCeros($detalles[$titulo1]['pk_num_item'],10);
                        $descripcion = $detalles[$titulo1]['ind_descripcion_item'];
                        $unidad = $detalles[$titulo1]['uniItem'];
                    } else {
                        $codigo = $this->metRellenarCeros($detalles[$titulo1]['pk_num_commodity'],6);
                        $descripcion = $detalles[$titulo1]['ind_descripcion_commodity'];
                        $unidad = $detalles[$titulo1]['uniComm'];
                    }
                    $this->atFPDF->Row(array($codigo,
                        utf8_decode($descripcion),
                        utf8_decode($unidad),
                        number_format($detalles[$titulo1]['num_cantidad'], 2, ',', '.'),
                        number_format(1, 2, ',', '.'),
                        '0000-00-00',
                        1,
                        $this->estadoNombre($detalles[$titulo1]['detalleEstatus'])));
                }
            }
            $total += 1;
            $y = $this->atFPDF->GetY();
            /*
                        $this->atFPDF->Row(array(
                            $ordenes[$titulo]['ind_orden'],
                            $ordenes[$titulo]['fec_creacion'],
                            $this->estadoNombre($ordenes[$titulo]['num_estatus'])
                        ));
                        */
            $this->atFPDF->Ln(1);

        }

        #salida del pdf
        $this->atFPDF->Output();
    }

}


?>