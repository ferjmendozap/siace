<?php
class reporteItemsControlador extends Controlador
{
    public $atReporte;
    public $atItemsModelo;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reporteItems', 'reportes');
        Session::metAcceso();
        #se carga la Libreria.
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('filtro');
    }

    public function metIndexAlmacen()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('almacen',$this->atReporte->atItemsModelo->atAlmacenModelo->metListarAlmacen());
        $this->atVista->metRenderizar('filtroAlmacen');
    }

    public function metLineas()
    {
        $this->atVista->assign('lista',$this->atReporte->atMiscelaneoModelo->metMostrarSelect('LINEAS'));
        $this->atVista->metRenderizar('lineas','modales');
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function estadoNombre($estado){
        if($estado=="PR"){
            $e="PREPARADO";
        } elseif($estado=="RV"){
            $e="REVISADO";
        } elseif($estado=="CN"){
            $e="CONFORMADO";
        } elseif($estado=="AN"){
            $e="ANULADO";
        } elseif($estado=="CE"){
            $e="CERRADO";
        } elseif($estado=="TE"){
            $e="TERMINADO";
        }
        return $e;
    }

    public function metGenerarReporte($buscar,$linea,$orden)
    {
        $this->metObtenerLibreria('headerItem','modLG');
        $this->atFPDF1 = new pdf('P', 'mm', 'Letter');
        $filtro="WHERE 1 ";
        if($buscar!='no'){
            $filtro.="AND (i.ind_descripcion LIKE '%$buscar%' OR i.pk_num_item LIKE '%$buscar%') ";
        }
        if($linea!='no'){
            $filtro.="AND detalle.pk_num_miscelaneo_detalle LIKE '%$linea%' ";
        }
        $filtro.="ORDER BY $orden";

        $consulta = $this->atReporte->atItemsModelo->metListarItems('1',$filtro);

        $this->atFPDF1->AliasNbPages();
        $this->atFPDF1->SetMargins(5, 1, 1);
        $this->atFPDF1->SetAutoPageBreak(1, 20);
        $this->atFPDF1->AddPage();

        foreach ($consulta as $titulo=>$valor) {

            $this->atFPDF1->SetDrawColor(255, 255, 255);
            $this->atFPDF1->SetFillColor(255, 255, 255);
            $this->atFPDF1->SetTextColor(0, 0, 0);
            $this->atFPDF1->SetFont('Arial', '', 6);

            $this->atFPDF1->Row(array(
                $valor['pk_num_item'],
                $valor['ind_codigo_interno'],
                utf8_decode($valor['ind_descripcion']),
                $valor['uni'],
                utf8_decode($valor['tipo']),
                $valor['cod_detalle'],
                $valor['pk_num_clase_familia'],
                $valor['pk_num_subfamilia'],
                $valor['cod_partida'],
                $valor['cod_cuenta']
                ));
            $this->atFPDF1->Ln(1);
        }

        #salida del pdf
        $this->atFPDF1->Output();
    }

    public function metGenerarReporteUbiAlmacen($ubicacion,$linea,$almacen,$stock)
    {
        $this->metObtenerLibreria('headerUbiAlmacen','modLG');
        $this->atFPDF = new pdf('p','mm','letter');
        $filtro="WHERE 1 ";

        if($ubicacion!='no'){
            $filtro.="AND (almacen.ind_descripcion LIKE '%$ubicacion%') ";
        }

        if($linea!='no'){
            $filtro.="AND detalle.pk_num_miscelaneo_detalle LIKE '%$linea%' ";
        }

        if($stock==2){
            $filtro.="AND stock.num_stock_actual <> 0 ";
        }

        $filtro.="AND almacen.pk_num_almacen='$almacen' ORDER BY i.pk_num_item ";

        $consulta = $this->atReporte->atItemsModelo->metListarItems('1',$filtro);


        $elaboradoPor = $this->atReporte->metMostrarEmpleado(Session::metObtener('idEmpleado'));
        $revisadoPor = $this->atReporte->metMostrarEmpleado(102);
        $conformadoPor = $this->atReporte->metMostrarEmpleado(95);
        $aprobadoPor = $this->atReporte->metMostrarEmpleado(148);

        define('ELABORADO_POR',$elaboradoPor['nombre']);
        define('ELABORADO_CARGO',$elaboradoPor['ind_descripcion_cargo']);
        define('REVISADO_POR',$revisadoPor['nombre']);
        define('REVISADO_CARGO',$revisadoPor['ind_descripcion_cargo']);
        define('CONFORMADO_POR',$conformadoPor['nombre']);
        define('CONFORMADO_CARGO',$conformadoPor['ind_descripcion_cargo']);
        define('APROBADO_POR',$aprobadoPor['nombre']);
        define('APROBADO_CARGO',$aprobadoPor['ind_descripcion_cargo']);


        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(1, 35);
        $this->atFPDF->AddPage();

        $this->atFPDF->SetWidths(array(40, 25, 15, 80, 30, 18));
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C'));

        foreach ($consulta as $titulo=>$valor) {

            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFont('Arial', '', 8);

            $this->atFPDF->Row(array(
                utf8_decode($consulta[$titulo]['almacen']),
                $valor['ind_codigo_interno'],
                $valor['pk_num_item'],
                utf8_decode($consulta[$titulo]['descripcion']),
                $consulta[$titulo]['uni'],
                $consulta[$titulo]['num_stock_actual']
            ));

        }

        #salida del pdf
        $this->atFPDF->Output();
    }
}


?>