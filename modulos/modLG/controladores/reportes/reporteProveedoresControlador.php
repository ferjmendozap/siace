<?php
class reporteProveedoresControlador extends Controlador
{
    public $atReporte;
    public $atProveedorModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reporteProveedores', 'reportes');
        Session::metAcceso();
        #se carga la Libreria.
        $this->metObtenerLibreria('headerProveedores','modLG');
        $this->atFPDF = new pdf('L','mm','letter');
    }

    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef'
        );
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('dependencia',$this->atReporte->atOrdenCompraModelo->metListarDependencias());
        $this->atVista->assign('servicio',$this->atReporte->atOrdenCompraModelo->metListarTipoServicio());
        $this->atVista->metRenderizar('filtro');
    }

    public function metFormatoFecha($fecha,$formato,$separador){
        #1  2016-01-01 a 01-01-2016
        #0  01-01-2016 a 2016-01-01
        if(substr($fecha,4,1)=='-' or substr($fecha,4,1)=='/'){
            $dia=substr($fecha,8,2);
            $mes=substr($fecha,5,2);
            $anio=substr($fecha,0,4);
        } elseif(substr($fecha,2,1)=='-' or substr($fecha,2,1)=='/') {
            $dia=substr($fecha,0,2);
            $mes=substr($fecha,3,2);
            $anio=substr($fecha,6,4);
        } else {
            $dia=0;
            $mes=0;
            $anio=0;
        }
        if (checkdate($mes, $dia, $anio)==false){
            return "la fecha es incorrecta";
            exit;
        }
        if($formato==1) {
            $fecha =$dia.$separador.$mes.$separador.$anio;
        } else {
            $fecha =$anio.$separador.$mes.$separador.$dia;
        }
        return $fecha;
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function estadoNombre($estado){
        if($estado=="PR"){
            $e="PREPARADO";
        } elseif($estado=="RV"){
            $e="REVISADO";
        } elseif($estado=="CN"){
            $e="CONFORMADO";
        } elseif($estado=="AN"){
            $e="ANULADO";
        } elseif($estado=="CE"){
            $e="CERRADO";
        } elseif($estado=="TE"){
            $e="TERMINADO";
        }
        return $e;
    }

    public function metGenerarReporte($nac,$servicio,$orden,$ingresado,$estado,$buscar)
    {
        $filtro = "";
        if ($nac!='no') {
            $filtro .= "AND persona.fk_a006_num_miscelaneo_detalle_nacionalidad ='$nac' ";
        }
        if ($servicio!='no') {
            $filtro .= "AND servicio.fk_cpb017_num_tipo_servicio ='$servicio' ";
        }
        if ($ingresado!='no') {
            $filtro .= "AND proveedor.fec_constitucion >='$ingresado' ";
        }
        if ($estado!='no' and $estado==1) {
            $filtro .= "AND persona.num_estatus ='$estado' ";
        } elseif ($estado!='no' and $estado==2) {
            $filtro .= "AND persona.num_estatus ='0' ";
        }
        if ($buscar!='no') {

//            str_replace('%','',$buscar);

            $filtro .= "AND (proveedor.pk_num_proveedor LIKE '%".trim($buscar)."%' OR
            persona.ind_nombre1 LIKE '%".trim($buscar)."%' OR
            persona.ind_nombre2 LIKE '%".trim($buscar)."%' OR
            persona.ind_apellido1 LIKE '%".trim($buscar)."%' OR
            persona.ind_apellido2 LIKE '%".trim($buscar)."%' OR
            persona2.ind_nombre1 LIKE '%".trim($buscar)."%' OR
            persona2.ind_nombre2 LIKE '%".trim($buscar)."%' OR
            persona2.ind_apellido1 LIKE '%".trim($buscar)."%' OR
            persona2.ind_apellido2 LIKE '%".trim($buscar)."%' OR
            persona.ind_documento_fiscal LIKE '%".trim($buscar)."%' OR
            direccion.ind_direccion LIKE '%".trim($buscar)."%')";
        }
        if($orden=="pk_num_proveedor"){
            $filtro .= "ORDER BY proveedor.$orden";
        } else {
            $filtro .= "ORDER BY persona.$orden";
        }

        $proveedores = $this->atReporte->metBuscarProveedores($filtro);
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5, 35);
        $this->atFPDF->AddPage();

        foreach ($proveedores as $p) {
            $telefono = $this->atReporte->atProveedorModelo->metListarTelefonos($p['pk_num_persona']);

            if(isset($telefono[0]['ind_telefono'])){ $telefono1=$telefono[0]['ind_telefono']; } else { $telefono1=""; }
            if(isset($telefono[1]['ind_telefono'])){ $telefono2=$telefono[1]['ind_telefono']; } else { $telefono2=""; }

            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->Row(array($p['pk_num_proveedor'],
                utf8_decode($p['nombre']),
                $p['ind_documento_fiscal'],
                utf8_decode($p['ind_direccion']),
                $telefono1.' / '.$telefono2,
                $p['nombre2'],
                $p['ind_email']));
            $this->atFPDF->Ln(1);
        }


        #salida del pdf
        $this->atFPDF->Output();
    }
}


?>