<?php
class reporteStockControlador extends Controlador
{
    public $atReporte;
    public $atCommodityModelo;
    public $atClasificacionModelo;
    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        $this->atReporte = $this->metCargarModelo('reporteStock', 'reportes');
        Session::metAcceso();
        #se carga la Libreria.
        $this->metObtenerLibreria('headerStock','modLG');
        $this->atFPDF = new pdf('P', 'mm', 'Letter');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('selectTipoItem',$this->atReporte->atMiscelaneoModelo->metMostrarSelect('TIPOITM'));
        $this->atVista->assign('almacen',$this->atReporte->atCommodityModelo->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('clasificacion',$this->atReporte->atClasificacionModelo->metListarClasificacion());
        $this->atVista->metRenderizar('filtro');
    }

    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

    public function metGenerarReporte($orden,$item,$tipo,$cantidad)
    {
        $filtro = "";
        if($item!='no'){ $filtro .= "AND item.pk_num_item='$item' "; }
        if($tipo!='no'){ $filtro .= "AND detalle.pk_num_miscelaneo_detalle='$tipo' "; }
        if($cantidad!='no'){ $filtro .= "AND stock.num_stock_actual>='".number_format($cantidad,2,",",".")."' ";}

        $filtro .= "ORDER BY item.$orden ASC";

        $stock = $this->atReporte->metListarStock($filtro);

        $this->atFPDF->SetTitle(utf8_decode('Stock de Items'));
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(5, 1, 1);
        $this->atFPDF->SetAutoPageBreak(5, 35);
        $this->atFPDF->AddPage();

        foreach($stock AS $titulo=>$valor){
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFont('Arial', '', 6);
            $this->atFPDF->Row(array(
                //$stock[$titulo]['pk_num_item'],
                $stock[$titulo]['pk_num_item'],
                $stock[$titulo]['ind_codigo_interno'],
                utf8_decode($stock[$titulo]['descripcionItem']),
                $stock[$titulo]['unidad'],
                number_format($stock[$titulo]['num_stock_actual'], 2, ',', '.'),
                number_format($stock[$titulo]['num_stock_comprometido'], 2, ',', '.'),
                number_format($stock[$titulo]['num_stock_actual']-$stock[$titulo]['num_stock_comprometido'], 2, ',', '.')
            ));
            $this->atFPDF->Ln(1);
        }

        #salida del pdf
        $this->atFPDF->Output('Stock de Items','I');
    }
}

?>