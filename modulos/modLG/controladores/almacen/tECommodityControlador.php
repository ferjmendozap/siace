<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class tECommodityControlador extends Controlador
{
    private $atTECommodity;
    private $atOrdenCompraModelo;
    private $atTransaccionModelo;
    private $atTipoDocumentoModelo;
    private $atDespachoModelo;
    private $atEjecutarTranModelo;

    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        $this->atTECommodity = $this->metCargarModelo('tECommodity', 'almacen');
    }

    //Método para listar las ordenes a recepcionar y despachar
    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listado', $this->atTECommodity->metListarOrdenes());
        $this->atVista->assign('listadoDet', $this->atTECommodity->metListarOrdenDet());
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear las transacciones de recepción y despacho
    public function metCrearModificarTransaccion()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idOrden = $this->metObtenerInt('idOrden');
        $idTran = $this->metObtenerInt('idTran');
        $opcion = $this->metObtenerTexto('opcion');
        $ver = $this->metObtenerInt('ver');
        if($valido==1){
            $this->metValidarToken();

            $ExcceccionInt= array('manual','pendiente','nro','idDetReq');
            $ExcceccionAlphaNum= array('nota');
            $ind = $this->metValidarFormArrayDatos('form','int',$ExcceccionInt);
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum',$ExcceccionAlphaNum);

            if ($ind != null && $alphaNum == null) {
                $validacion = $ind;
            } elseif ($ind == null && $alphaNum != null) {
                $validacion = $alphaNum;
            } else {
                $validacion = array_merge($ind, $alphaNum);
            }

            if (isset($validacion['cant'])) {
                $cant = 0;
                $errorOpcion=0;
                while ($cant<$validacion['cant']) {
                    $cant = $cant + 1;
                    if (!isset($validacion['opcion'][$cant])){
                        $validacion['opcion'][$cant] = "0";
                        $validacion['canti'][$cant] = "0";
                        $errorOpcion = $errorOpcion + 1;
                    } elseif($validacion['canti'][$cant]==0){
                        $validacion['canti'][$cant]='error';
                        $validacion['status']='errorCant';
                        echo json_encode($validacion);
                        exit;
                    }
                    if ($validacion['cantiDespachada'][$cant]=='error'){
                        $validacion['cantiDespachada'][$cant] = "0";
                    }
                    if ($validacion['idDetReq'][$cant]=='0'){
                        $validacion['idDetReq'][$cant] = NULL;
                    }

                }
                if($cant==$errorOpcion){
                    $validacion['cant'] = "error";
                    $validacion['status']='errorOpcion';
                    echo json_encode($validacion);
                    exit;
                }
            } else {
                $validacion['cant'] = "error";
            }

            if(!isset($validacion['af'])) {
                $validacion['af'] = '0';
            }


            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if (!isset($validacion['manual'])) {
                $validacion['manual'] = 0;
            }
            if (!isset($validacion['pendiente'])) {
                $validacion['pendiente'] = 0;
            }

            if(!isset($validacion['nota'])){
                $validacion['nota']=null;
            }

            $puedeTran = $this->atTECommodity->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metBuscarPeriodo();
            $tipoTran = $this->atTECommodity->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metBuscarTipoTransaccion($validacion['tipoTran'],1);
            $nro = $this->atTECommodity->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metMostrarNro($tipoTran['cod_detalle']);
            $validacion['nro'] = $this->metRellenarCeros(($nro['nro'] + 1),6);

            if ($idOrden!=0 AND $puedeTran['num_estatus']==1 AND $opcion=="recepcionar") {
               $id=$this->atTECommodity->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metCrearTransaccion(
                   $validacion['nro'],$validacion['fdoc'],$validacion['ccosto'],
                   $validacion['docRef'],$validacion['comen'],$validacion['manual'],
                   $validacion['pendiente'],$validacion['nota'],'2',$validacion['tipoTran'],
                   $validacion['almacen'],$validacion['tipoDoc'],
                   $validacion['cant'],$validacion['sAct'],$validacion['canti'],
                   $validacion['precio'],null,$validacion['numComm'],
                   $validacion['unidad'],$validacion['idDet'],$validacion['idDetReq'],
                   $validacion['af']
               );

                $contador=0;
                while($contador<$validacion['cant']){
                    $contador=$contador+1;
                    if($validacion['opcion'][$contador]==1){
                        $id2=$this->atTECommodity->metActualizarStock($validacion['canti'][$contador],$validacion['numComm'][$contador],1);
                        $validacion['id2']=$id2;
                    }
                }

                $validacion['status']='nuevo';
            } elseif ($idTran!=0 AND $puedeTran['num_estatus']==1 AND $opcion=="aprobar") {
#                echo json_encode($validacion);
#                exit;

                $contador=0;
                while($contador<$validacion['cant']){
                    $contador=$contador+1;
                    if($validacion['opcion'][$contador]=="1"){
                        $id=$this->atTECommodity->metActualizarEstadoDet($validacion['idDet'][$contador],'AP');
                    }
                }
                $id2=$this->atTECommodity->metActualizarOrden($idOrden);
                $validacion['status']='aprobar';
            } elseif ($idTran!=0 AND $puedeTran['num_estatus']==1 AND $opcion=="despachar") {

                foreach ($validacion['canti'] as $key => $value) {
                    $stock = $this->atTECommodity->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metBuscarStock($validacion['idStock'][$key],'C');
                    if($stock['num_stock_actual']-$validacion['canti'][$key]<0){
                        $validacion['status']='errorStock';
                        $validacion['num_stock_actual'][$key]=$stock['num_stock_actual'];
                        $validacion['icCantidad']=$validacion['canti'][$key];
                        $validacion['stock'] = $stock;
                        echo json_encode($validacion);
                        exit;
                    }
                }

                $id=$this->atTECommodity->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metCrearTransaccion(
                    $validacion['nro'],$validacion['fdoc'],$validacion['ccosto'],
                    $validacion['docRef'],$validacion['comen'],$validacion['manual'],
                    $validacion['pendiente'],$validacion['nota'],'2',$validacion['tipoTran'],
                    $validacion['almacen'],$validacion['tipoDoc'],
                    $validacion['cant'],$validacion['sAct'],$validacion['canti'],
                    $validacion['precio'],null,$validacion['numComm'],
                    $validacion['unidad'],$validacion['idDet'],$validacion['idDetReq']
                );

                $contador=0;
                while($contador<$validacion['cant']){
                    $contador=$contador+1;
                    if($validacion['opcion'][$contador]=="1"){
                        if($validacion['canti'][$contador]+$validacion['cantiDespachada'][$contador]==$validacion['cantiPedida'][$contador]){
                            $reqDet = $this->atTECommodity->metBuscarReqDet($idOrden);
                            $id=$this->atTECommodity->metActualizarEstadoDet($validacion['idDet'][$contador],'CO',$reqDet);
                        }
                        $id2=$this->atTECommodity->metActualizarStock($validacion['canti'][$contador],$validacion['numComm'][$contador],2);
                    }
                }

                $reqDet = $this->atTECommodity->atDespachoModelo->atOrdenCompraModelo->metMostrarOrdenDetalle($idOrden);
                $c1=0;
                $c2=0;
                foreach ($reqDet as $key=>$value) {
                    $c1=$c1+1;
                    if($reqDet[$key]['detalleEstatus']=='CO'){
                        $c2=$c2+1;
                    }
                }
                if($c1==$c2){
                    $id3=$this->atTECommodity->atDespachoModelo->atOrdenCompraModelo->metActualizarOrden('CO',$idOrden,'fk_a018_num_seguridad_usuario','fec_ultima_modificacion');
                }

                $validacion['status']='despachar';
            } else {
                $validacion['status'] = 'periodo';
                echo json_encode($validacion);
                exit;
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['id'] = $id;
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idOrden']=$idOrden;
            $validacion['num_transaccion']=$validacion['nro'];
            $validacion['id']=$id;

            echo json_encode($validacion);
            exit;
        }
        if($idOrden!=0){
            $orden = $this->atTECommodity->atDespachoModelo->atOrdenCompraModelo->metMostrarOrden($idOrden);
            $transaccion = $this->atTECommodity->metMostrarTransaccion($orden['ind_orden'],$orden['fec_anio']);
            $orden['ind_nota_entrega_factura'] = $transaccion['ind_nota_entrega_factura'];
            $this->atVista->assign('formDB',$orden);
            $this->atVista->assign('formDBI',$this->atTECommodity->atDespachoModelo->atOrdenCompraModelo->metMostrarOrdenDetalle($idOrden));
            $this->atVista->assign('ver',$ver);
        }
        $this->atVista->assign('idOrden',$idOrden);
        $this->atVista->assign('opcion',$opcion);
        $this->atVista->assign('idTran',$idTran);
        $this->atVista->assign('tranDet',$this->atTECommodity->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metListarTransaccionesDetalles());
        $this->atVista->assign('centro',$this->atTECommodity->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metListarCC());
        $this->atVista->assign('almacen',$this->atTECommodity->atDespachoModelo->atOrdenCompraModelo->atRequerimientoModelo->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('transaccion',$this->atTECommodity->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->atTipoTransaccionModelo->metListarTipoTran());
        $this->atVista->assign('documento',$this->atTECommodity->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->atTipoDocumentoModelo->metListarTipoDoc());
        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION

}
