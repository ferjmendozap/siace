<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class ejecutarTranControlador extends Controlador
{
    private $atEjeTransaccion;
    private $atTransaccionModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        #se carga el Modelo.
        $this->atEjeTransaccion= $this->metCargarModelo('ejecutarTran', 'almacen');
    }

    //Método para listar las transacciones a ejecutar
    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);
//        $this->atVista->assign('listado', $this->atEjeTransaccion->metListarTransaccionesPendiente());
        $this->atVista->metRenderizar('listado');
    }

    //Método para ejecutar las transacciones
    public function metModificarTransaccion()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'wizard/jquery.bootstrap.wizard.min',
            'select2/select2.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idTran = $this->metObtenerInt('idTran');
        if($valido==1){
            $this->metValidarToken();
            $puedeTran = $this->atEjeTransaccion->atTransaccionModelo->metBuscarPeriodo();
            $tipoTranResul=$this->atEjeTransaccion->metBuscarTipoTran($idTran);

            $cual="0";
            $det = $this->atEjeTransaccion->atTransaccionModelo->metMostrarDetalles($idTran);
            $i=0;
            foreach ($det as $key => $value) {
                $i=$i+1;
                $validacion['canti'][$i] = $det[$key]['num_cantidad_transaccion'];
                if($det[$key]['fk_lgc007_num_item_stock']!=NULL){
                    $idIC[$i] = $det[$key]['fk_lgc007_num_item_stock'];
                    $ic='1';
                } else {
                    $idIC[$i] = $det[$key]['pk_num_commodity_stock'];
                    $ic='2';
                }
                $icCantidad[$i] = $det[$key]['num_cantidad_transaccion'];
            }

            $validacion['cant'] = $i;
            if($tipoTranResul['ind_nombre_detalle']=='INGRESO') {
                $cual="1";
            } elseif($tipoTranResul['ind_nombre_detalle']=='EGRESO') {
                $cual="2";
                if($ic==1){
                    foreach ($idIC as $key => $value) {
                        $stock = $this->atEjeTransaccion->atTransaccionModelo->metBuscarStock($idIC[$key],'I');
                        if($stock['num_stock_actual']-$icCantidad[$key]<0){
                            $validacion['status']='errorStock';
                            $validacion['num_stock_actual']=$stock['num_stock_actual'];
                            $validacion['icCantidad']=$icCantidad[$key];
                            echo json_encode($validacion);
                            exit;
                        }
                    }
                }
                if($ic==2){
                    foreach ($validacion['numComm'] as $key => $value) {
                        $stock = $this->atEjeTransaccion->atTransaccionModelo->metBuscarStock($idIC[$key]['pk_num_commodity_stock'],'C');
                        if($stock['num_stock_actual']-$icCantidad[$key]<0){
                            $validacion['status']='errorStock';
                            $validacion['num_stock_actual']=$stock['num_stock_actual'];
                            $validacion['icCantidad']=$icCantidad[$key];
                            echo json_encode($validacion);
                            exit;
                        }
                    }
                }
            }


            $validacion['id2'] = $tipoTranResul;

            if($idTran!=0 AND $puedeTran['num_estatus']==1){
                $id=$this->atEjeTransaccion->metEjecutarTransaccion($idTran);
                foreach ($idIC as $key => $value) {
                    $id3=$this->atEjeTransaccion->metActualizarStock(
                        $validacion['canti'][$key],$value,$cual
                    );
//                    var_dump($validacion['canti'][$key]."--".$value."--".$cual."--".$tipoTranResul['ind_nombre_detalle']);
                }

                $validacion['status']='modificar';
            } else {
                $validacion['status'] = 'periodo';
                echo json_encode($validacion);
                exit;
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idTran']=$idTran;

            echo json_encode($validacion);
            exit;
        }
        if($idTran!=0){
            $this->atVista->assign('formDB',$this->atEjeTransaccion->atTransaccionModelo->metMostrarTransaccion($idTran));
            $this->atVista->assign('formDBI',$this->atEjeTransaccion->atTransaccionModelo->metMostrarDetalles($idTran));
            $this->atVista->assign('idTran',$idTran);
        }

        $this->atVista->assign('centro',$this->atEjeTransaccion->atTransaccionModelo->metListarCC());
        $this->atVista->assign('almacen',$this->atEjeTransaccion->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('documento',$this->atEjeTransaccion->atTipoDocumentoModelo->metListarTipoDoc());
        $this->atVista->assign('transaccion',$this->atEjeTransaccion->atTipoTransaccionModelo->metListarTipoTran());
        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para paginar las transacciones a ejecutar
    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  *,
                  concat_ws('-', fec_anio,fec_mes) AS periodo
                FROM
                  lg_d001_transaccion
                WHERE
                  num_estatus='0' ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (
                        transaccion.pk_num_transaccion LIKE '%$busqueda[value]%' OR
                        transaccion.num_transaccion LIKE '%$busqueda[value]%' OR
                        tipo_transaccion.ind_descripcion LIKE '%$busqueda[value]%' OR
                        transaccion.fec_anio LIKE '%$busqueda[value]%' OR
                        transaccion.fec_mes LIKE '%$busqueda[value]%' OR
                        transaccion.ind_num_documento_referencia LIKE '%$busqueda[value]%' OR
                        0 LIKE '%$busqueda[value]%' OR
                        1 LIKE '%$busqueda[value]%' OR
                        2 LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('num_transaccion','periodo','num_flag_manual','num_flag_pendiente');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_transaccion';
        #construyo el listado de botones
        $flags = array('num_flag_manual','num_flag_pendiente');

        if (in_array('LG-01-02-02-01-E',$rol)) {
            $campos['boton']['Ejecutar'] = "
                <button accion='modificar' title='Ejecutar'
                        class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-info'
                        data-keyboard='false' data-backdrop='static'
                        descipcion='El Usuario ha Consultado la Transaccion Nro. $clavePrimaria'
                        idTran='$clavePrimaria' titulo='Ejecutar Transaccion'
                        data-toggle='modal' data-target='#formModal' id='modificar'>
                    <i class='md md-send'></i>
                </button>
                ";
        } else {
            $campos['boton']['Ejecutar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false,$flags);
    }

}
