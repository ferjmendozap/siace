<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

class tECajaChicaControlador extends Controlador
{
    private $atTECajaChica;
    private $atOrdenCompraModelo;
    private $atTransaccionModelo;
    private $atTipoDocumentoModelo;
    private $atDespachoModelo;
    private $atRequerimientoModelo;
    private $atEjecutarTranModelo;
    public function __construct()
    {
        parent::__construct();
        #se carga el Modelo.
        Session::metAcceso();
        $this->atTECajaChica = $this->metCargarModelo('tECajaChica', 'almacen');
    }

    //Método para listar los requerimientos a recepcionar y despachar
    public function metIndex()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'wizard/jquery.bootstrap.wizard.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min'
        );
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('listado1', $this->atTECajaChica->metListarReq());
        $this->atVista->assign('listado1Det', $this->atTECajaChica->metListarReqDet());
        $this->atVista->metRenderizar('listado');
    }

    //Método para crear las transacciones de recepción y despacho
    public function metCrearRecepcion()
    {
        $complementosJs = array(
            'jquery-validation/dist/jquery.validate.min',
            'jquery-validation/dist/additional-methods.min',
            'bootstrap-datepicker/bootstrap-datepicker',
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min'
        );
        $complementoCss = array(
            'bootstrap-datepicker/datepicker',
            'select2/select201ef',
            'wizard/wizardfa6c'
        );
        $js[] = 'materialSiace/core/demo/DemoFormWizard';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementoCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idDep = $this->metObtenerInt('idDep');
        $idReq = $this->metObtenerInt('idReq');
        $idTran = $this->metObtenerInt('idTran');
        $opcion = $this->metObtenerTexto('opcion');
        if($valido==1){
            $this->metValidarToken();

            $ind = $this->metValidarFormArrayDatos('form','int');
            $txt = $this->metValidarFormArrayDatos('form','txt');
            $alphaNum = $this->metValidarFormArrayDatos('form','alphaNum',array('nota'));

            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = $ind;
            } elseif ($alphaNum == null && $ind == null && $txt != null) {
                $validacion = $txt;
            } elseif ($alphaNum == null && $ind != null && $txt != null) {
                $validacion = array_merge($txt, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null) {
                $validacion = array_merge($txt, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $txt);
            }

            $contador=0;
            $c=0;
            if($validacion['cant']>0){
                while($contador<$validacion['cant']){

                    $contador=$contador+1;
                    if($validacion['canti'][$contador]>($validacion['cantiPedida'][$contador]-$validacion['cantiRecibida'][$contador]) AND $opcion=='recepcionar'){
                        $validacion['canti2'][$contador]='error';
                        $validacion['status']='errorCant';
                        echo json_encode($validacion);
                        exit;
                    }
                    if($validacion['cantiRecibida'][$contador]-$validacion['canti'][$contador]<0 AND $opcion=='despachar'){
                        $validacion['canti2'][$contador]='error';
                        $validacion['status']='errorCant';
                        echo json_encode($validacion);
                        exit;
                    }
                    if(!isset($validacion['opcion'][$contador])){
                        $validacion['opcion'][$contador] = 0;
                        $validacion['canti'][$contador] = 0;
                        $c=$c+1;
                    }
                    if($validacion['precio'][$contador]=="error" AND $opcion=="recepcionar" AND $validacion['opcion'][$contador]==1){
                        $validacion['canti']='error';
                        $validacion['status']='errorPrecio';
                        echo json_encode($validacion);
                        exit;
                    } elseif(!isset($validacion['precio'][$contador]) OR $validacion['precio'][$contador]=="error") {
                        $validacion['precio'][$contador] = 0;
                    }
                    if($validacion['cantiRecibida'][$contador]=="error"){
                        $validacion['cantiRecibida'][$contador] = 0;
                    }
                }
            }
            if($c==$contador){
                $validacion['canti']='error';
                $validacion['status']='errorOpcion';
                echo json_encode($validacion);
                exit;
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }

            if(!isset($validacion['nota'])){
                $validacion['nota']=null;
            }
            $cod_detalle = '';
            if ($opcion=="recepcionar") {
                $cod_detalle = 'I';
            } elseif ($opcion=="despachar") {
                $cod_detalle = 'E';
            }
            $puedeTran = $this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metBuscarPeriodo();
            $nro = $this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metMostrarNro($cod_detalle);
            $validacion['nro'] = $this->metRellenarCeros(($nro['nro'] + 1),6);

            if($validacion['cual']=='I'){
                $cual = 1;
                $item = $validacion['numDet'];
                $comm = NULL;
            } else {
                $item = NULL;
                $comm = $validacion['numDet'];
                $cual = 4;
            }
            if($idReq!=0 AND $puedeTran['num_estatus']==1 AND $opcion=="recepcionar"){
                $id=$this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metCrearTransaccion(
                    $validacion['nro'],$validacion['fdoc'],$validacion['cCosto'],
                    $validacion['docRef'],$validacion['comen'],0
                    ,0,$validacion['nota'],'2',
                    $validacion['tipoTran'],$validacion['almacen'], $validacion['tipoDoc'],
                    $validacion['cant'],$validacion['sAnt'],$validacion['canti'],
                    $validacion['precio'],$item,$comm,
                    $validacion['numUni'],null,$validacion['idDetalleReq']
                );
                $contador=0;
                while($contador<$validacion['cant']){
                    $contador=$contador+1;
                    if($validacion['opcion'][$contador]==1){
                        $id2=$this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->metActualizarStock(
                            $validacion['canti'][$contador],$validacion['idStock'][$contador],$cual
                        );
//                        $id=$this->atTECajaChica->atDespachoModelo->metActualizarEstadoDet($validacion['idDetalleReq'][$contador],'PR');
                        $validacion['id2']=$id2;
                    }
                }
                $validacion['status']='nuevo';
            } elseif ($puedeTran['num_estatus']==1 AND $opcion=="aprobar") {
                #$id=$this->atTECajaChica->metActualizarTransaccion($idTran);

                $contador=0;
                while($contador<$validacion['cant']){
                    $contador=$contador+1;
                    if($validacion['opcion'][$contador]=="1"){
                        $id=$this->atTECajaChica->atDespachoModelo->metActualizarEstadoDet($validacion['idDetalleReq'][$contador],'PE');
                        if($cual==1){
                            $id2=$this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->metActualizarStock($validacion['canti'][$contador],$validacion['idStock'][$contador],3);
                        }
                    }
                }
                $id3=$this->atTECajaChica->metActualizarRequerimiento($idReq);
                $validacion['id3']=$id3;

                $validacion['status']='aprobar';
            } elseif ($puedeTran['num_estatus']==1 AND $opcion=="despachar") {


                if($validacion['cual']=='I'){
                    $cual = 2;
                } else {
                    $cual = 5;
                }
                foreach ($validacion['canti'] as $key => $value) {
                    $stock = $this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metBuscarStock($validacion['idStock'][$key],$validacion['cual']);
                    if($stock['num_stock_actual']-$validacion['canti'][$key]<0){
                        $validacion['status']='errorStock';
                        $validacion['num_stock_actual'][$key]=$stock['num_stock_actual'];
                        $validacion['icCantidad']=$validacion['canti'][$key];
                        echo json_encode($validacion);
                        exit;
                    }
                }
                $id=$this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metCrearTransaccion(
                    $validacion['nro'],$validacion['fdoc'],$validacion['cCosto'],
                    $validacion['docRef'],$validacion['comen'],0
                    ,0,'','2',$validacion['tipoTran'],
                    $validacion['almacen'], $validacion['tipoDoc'],
                    $validacion['cant'],$validacion['sAnt'],$validacion['canti'],
                    $validacion['precio'],$item,$comm,
                    $validacion['numUni'],null,$validacion['idDetalleReq']
                );
                $contador=0;
                while($contador<$validacion['cant']){
                    $contador=$contador+1;
                    if($validacion['opcion'][$contador]=="1"){
                        if($validacion['canti'][$contador]+$validacion['cantiDespachada'][$contador]==$validacion['cantiPedida'][$contador]){
                            $id=$this->atTECajaChica->atDespachoModelo->metActualizarEstadoDet($validacion['idDetalleReq'][$contador],'CO');
                        }
                        if($cual==1){
                            $cual=3;
                        }
                        $id2=$this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->metActualizarStock($validacion['canti'][$contador],$validacion['idStock'][$contador],$cual);
                    }
                }
                $reqDet = $this->atTECajaChica->atDespachoModelo->atOrdenCompraModelo->atRequerimientoModelo->metBuscarReqDetalle($idReq);
                $c1=0;
                $c2=0;
                foreach ($reqDet as $key=>$value) {
                    $c1=$c1+1;
                    if($reqDet[$key]['ind_estado']=='CO'){
                        $c2=$c2+1;
                    }
                }
                if($c1==$c2){
                    $id3=$this->atTECajaChica->metActualizarRequerimiento('CO',$idReq,'fk_a018_num_seguridad_usuario','fec_ultima_modificacion');
                }
                $validacion['status']='despachar';
            } else {
                $validacion['status'] = 'periodo';
                echo json_encode($validacion);
                exit;
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['id'] = $id;
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            $validacion['idReq']=$idReq;

            echo json_encode($validacion);
            exit;
        }
        if($idReq!=0){
            $this->atVista->assign('formDB',$this->atTECajaChica->atDespachoModelo->atOrdenCompraModelo->atRequerimientoModelo->metBuscarRequerimiento($idReq));
            $this->atVista->assign('formDBI',$this->atTECajaChica->atDespachoModelo->atOrdenCompraModelo->atRequerimientoModelo->metBuscarReqDetalle($idReq));
            $this->atVista->assign('idReq',$idReq);
        }
        $this->atVista->assign('idDep',$idDep);
        $this->atVista->assign('opcion',$opcion);
        $this->atVista->assign('idTran',$idTran);
        $this->atVista->assign('centroCosto',$this->atTECajaChica->atDespachoModelo->atOrdenCompraModelo->atRequerimientoModelo->metListarCentroCosto($idDep));
        $this->atVista->assign('almacen',$this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->atAlmacenModelo->metListarAlmacen());
        $this->atVista->assign('documento',$this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->atTipoDocumentoModelo->metListarTipoDoc());
        $this->atVista->assign('transaccion',$this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->atTipoTransaccionModelo->metListarTipoTran());
        $this->atVista->assign('tranDet',$this->atTECajaChica->atDespachoModelo->atEjecutarTranModelo->atTransaccionModelo->metListarTransaccionesDetalles());
        $this->atVista->metRenderizar('CrearModificar','modales');

    }

    //Método para rellenar con ceros a la izquierda
    public function metRellenarCeros($nro,$cantidad){
        $cont=strlen($nro);
        if($cont<$cantidad){
            while($cont<$cantidad){
                $nro="0".$nro;
                $cont=$cont+1;
            }
        }
        return $nro;
    } // END FUNCTION
/*
    public function metJsonDataTabla($cual)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "
            SELECT
              requerimiento.*,
              costo.ind_descripcion_centro_costo,
              almacen.ind_descripcion AS almacen,
              
              (SELECT COUNT(*) FROM lg_c001_requerimiento_detalle AS detalle 
              WHERE detalle.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento) AS cantReq,
              
              (SELECT SUM(detalle.num_cantidad_pedida) FROM lg_c001_requerimiento_detalle AS detalle 
              WHERE detalle.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento) AS sumCantReqPed,
              
              (SELECT SUM(detalle2.num_cantidad_transaccion) FROM 
              lg_c001_requerimiento_detalle AS detalle,
              lg_d002_transaccion_detalle AS detalle2,
              lg_d001_transaccion AS transaccion,
              lg_b015_tipo_transaccion AS tipo
              WHERE detalle.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
              AND tipo.pk_num_tipo_transaccion = transaccion.fk_lgb015_num_tipo_transaccion
              AND tipo.ind_cod_tipo_transaccion = 'RCJ'
              AND detalle2.fk_lgd001_num_transaccion = transaccion.pk_num_transaccion
              AND transaccion.ind_num_documento_referencia = requerimiento.cod_requerimiento
              AND detalle2.fk_lgc001_num_requerimiento_detalle = detalle.pk_num_requerimiento_detalle
              ) AS sumCantReqReci,
              
              (SELECT SUM(detalle2.num_cantidad_transaccion) FROM 
              lg_c001_requerimiento_detalle AS detalle,
              lg_d002_transaccion_detalle AS detalle2,
              lg_d001_transaccion AS transaccion,
              lg_b015_tipo_transaccion AS tipo
              WHERE detalle.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
              AND tipo.pk_num_tipo_transaccion = transaccion.fk_lgb015_num_tipo_transaccion
              AND tipo.ind_cod_tipo_transaccion = 'DRC'
              AND detalle2.fk_lgd001_num_transaccion = transaccion.pk_num_transaccion
              AND transaccion.ind_num_documento_referencia = requerimiento.cod_requerimiento
              AND detalle2.fk_lgc001_num_requerimiento_detalle = detalle.pk_num_requerimiento_detalle
              ) AS sumCantReqDes,
              
              (SELECT COUNT(*) FROM 
              lg_c001_requerimiento_detalle AS detalle2, 
              lg_d002_transaccion_detalle AS detalle3,
              lg_d001_transaccion AS transaccion,
              lg_b015_tipo_transaccion AS tipo
              WHERE detalle2.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento
              AND tipo.pk_num_tipo_transaccion = transaccion.fk_lgb015_num_tipo_transaccion
              AND tipo.ind_cod_tipo_transaccion = 'RCJ'
              AND detalle3.fk_lgd001_num_transaccion = transaccion.pk_num_transaccion
              AND detalle2.pk_num_requerimiento_detalle = detalle3.fk_lgc001_num_requerimiento_detalle
              AND detalle2.fk_lgb001_num_requerimiento = requerimiento.pk_num_requerimiento AND detalle2.ind_estado='PE') AS cantReqAp
            FROM
              lg_b001_requerimiento AS requerimiento
              INNER JOIN a023_centro_costo AS costo ON costo.pk_num_centro_costo = requerimiento.fk_a023_num_centro_costo
              INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = requerimiento.fk_lgb014_num_almacen
              WHERE requerimiento.ind_verificacion_caja_chica = '1' AND requerimiento.ind_estado = 'AP' 
              ";
        if ($busqueda['value']) {
            $sql .= " AND
                        (
                        transaccion.pk_num_transaccion LIKE '%$busqueda[value]%' OR
                        transaccion.num_transaccion LIKE '%$busqueda[value]%' OR
                        tipo_transaccion.ind_descripcion LIKE '%$busqueda[value]%' OR
                        transaccion.fec_anio LIKE '%$busqueda[value]%' OR
                        transaccion.fec_mes LIKE '%$busqueda[value]%' OR
                        transaccion.ind_num_documento_referencia LIKE '%$busqueda[value]%' OR
                        0 LIKE '%$busqueda[value]%' OR
                        1 LIKE '%$busqueda[value]%' OR
                        2 LIKE '%$busqueda[value]%'
                        )
                        ";
        }


        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array(
            'cod_requerimiento',
            'ind_descripcion_centro_costo',
            'fec_requerida',
            'ind_comentarios',
            'almacen');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_requerimiento';
        #construyo el listado de botones
        $camposExtra = array();

        if (in_array('LG-01-02-05-01-01-R',$rol) AND $cual==1) {
            $campos['boton']['Recepcionar'] = array("
                <button title='Recepcionar'
                        class='logsUsuario btn ink-reaction btn-raised btn-xs btn-info nuevo'
                        descipcion='El Usuario ha Recepcionado el Requerimiento Nro. $clavePrimaria'
                        idReq='$clavePrimaria' titulo='Recepcionar Requerimiento'
                        data-toggle='modal' data-target='#formModal'
                        opcion='recepcionar'
                        id='recepcionar'>
                    <i class='fa fa-sign-in'></i>
                </button>
                ",
                'if( $i["sumCantReqReci"] < $i["sumCantReqPed"]) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Recepcionar'] = false;
        }

        if (in_array('LG-01-02-05-01-02-A',$rol) AND $cual==2) {
            $campos['boton']['Aprobar'] = array("
                <button title='Aprobar'
                        class='logsUsuario btn ink-reaction btn-raised btn-xs btn-primary nuevo'
                        descipcion='El Usuario ha Aprobado el Requerimiento Nro. $clavePrimaria'
                        idReq='$clavePrimaria' titulo='Aprobar Despacho'
                        data-toggle='modal' data-target='#formModal'
                        opcion='aprobar'
                        id='aprobar$clavePrimaria'>
                    <i class='icm icm-rating3'></i>
                </button>
                ",
                'if( $i["sumCantReqReci"] > 0 AND $i["cantReqAp"] != $i["cantReq"]) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('LG-01-02-05-01-03-D',$rol) AND $cual==2) {
            $campos['boton']['Despachar'] = array("
                <button title='Despachar'
                    class='logsUsuario btn ink-reaction btn-raised btn-xs btn-info nuevo'
                    descipcion='El Usuario ha Despachado el Requerimiento Nro. $clavePrimaria'
                    idReq='$clavePrimaria' titulo='Despachar Requerimiento'
                    data-toggle='modal' data-target='#formModal'
                    opcion='despachar'
                    id='despachar'>
                    <i class='fa fa-sign-out'></i>
                </button>
                ",
                'if( $i["cantReqAp"] > 0 ) { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Despachar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,$camposExtra);
    }
*/
}
