<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
class facturacionActivoModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
    }

    public function metListaActivosLG($idActivo = false)
    {
        $where = "";
        if ($idActivo){
            $where = "AND activo.pk_num_activo_fijo = '$idActivo'";
        }
        $personaPost = $this->_db->query("
            SELECT
              activo.*,
              commodity.ind_cod_commodity,
              commodity.ind_descripcion AS descripcion,
              1 AS ubicacion,
              2 AS ind_descripcion_centro_costo,
              3 AS marca,
              4 AS color,
              concat_ws('-',orden.fec_anio,orden.ind_orden) AS cod_orden,
              concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS proveedor,
              detalle.pk_num_orden_detalle,
              orden.pk_num_orden
            FROM
              lg_e004_activo_fijo as activo 
              INNER JOIN lg_c009_orden_detalle AS detalle ON detalle.pk_num_orden_detalle = activo.fk_lgc009_num_orden_detalle
              INNER JOIN lg_b003_commodity AS commodity ON detalle.fk_lgb003_num_commodity = commodity.pk_num_commodity
              INNER JOIN lg_b019_orden AS orden ON detalle.fk_lgb019_num_orden = orden.pk_num_orden
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor 
              INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = orden.fk_a004_num_dependencia
              AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
            WHERE
              activo.ind_estado = 'TR' AND activo.num_flag_facturado=0 $where
              ");
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        if($idActivo){
            return $personaPost ->fetch();
        } else{
            return $personaPost ->fetchAll();
        }
    }

    public function metListaObligaciones($idOrden)
    {
        $almacenPost = $this->_db->query(
            "SELECT
                obligacion.*,
                tipo.cod_tipo_documento,
                concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS proveedor
              FROM cp_d001_obligacion AS obligacion 
              INNER JOIN cp_b002_tipo_documento AS tipo ON obligacion.fk_cpb002_num_tipo_documento = tipo.pk_num_tipo_documento
              INNER JOIN cp_d002_documento_obligacion AS documento ON obligacion.pk_num_obligacion = documento.fk_cpd001_num_obligacion
              INNER JOIN lg_b019_orden AS orden ON documento.fk_lgb019_num_orden = orden.pk_num_orden
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor 
              WHERE documento.fk_lgb019_num_orden = '$idOrden' 
          ");
        $almacenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $almacenPost->fetchAll();
    }

    public function metFacturar($datos)
    {
        $this->_db->beginTransaction();
        $registroAlmacen = $this->_db->prepare("
          UPDATE
          lg_e004_activo_fijo
          SET
          fk_cpd001_num_obligacion=:fk_cpd001_num_obligacion,
          num_flag_facturado='1',
          fk_a018_num_seguridad_usuario='$this->atIdUsuario',
          fec_ultima_modificacion=NOW()
          WHERE pk_num_activo_fijo=:pk_num_activo_fijo
          ");

        $registroAlmacen->execute(array(
            'fk_cpd001_num_obligacion'=>$datos['idObligacion'],
            'pk_num_activo_fijo'=>$datos['idActivo']
        ));

        $fallaTansaccion = $registroAlmacen->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $datos['idActivo'];
        }

    }

}

?>