<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'itemsModelo.php';
class cargaInventarioModelo extends Modelo
{
    public $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atItemsModelo = new itemsModelo();
    }

    public function metUsuarioEmpleado($idUsuario)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              empleado.pk_num_empleado
            FROM
            rh_b001_empleado AS empleado
             INNER JOIN a018_seguridad_usuario AS usuario ON empleado.pk_num_empleado = usuario.fk_rhb001_num_empleado
             WHERE usuario.pk_num_seguridad_usuario = '$idUsuario'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metEmpleado($idEmp)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS NomCompleto
            FROM
            rh_b001_empleado AS empleado
             INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
             WHERE empleado.pk_num_empleado = '$idEmp'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metCrearCarga($numProceso,$slog,$sreal,$idItem)
    {
        $this->_db->beginTransaction();
        $registroInventario = $this->_db->prepare("
          INSERT INTO
              lg_e001_inventario_fisico
          SET
              num_proceso=:num_proceso,
              fec_registro=NOW(),
              num_stock_logico=:num_stock_logico,
              num_stock_fisico=:num_stock_fisico,
              fec_mes=:fec_mes,
              fec_anio=:fec_anio,
              fk_lgb002_num_item=:fk_lgb002_num_item,
              fk_rhb001_num_empleado_realiza='$this->atIdEmpleado',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
           ");


        $registroInventario->execute(array(
            'num_proceso'=>$numProceso,
            'num_stock_logico'=>$slog,
            'num_stock_fisico'=>$sreal,
            'fec_mes'=>date('m'),
            'fec_anio'=>date('Y'),
            'fk_lgb002_num_item'=>$idItem
        ));

        $idRegistro= $this->_db->lastInsertId();

        $fallaTansaccion = $registroInventario->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metActualizarCarga($idRegistro,$slog,$sreal)
    {
        $this->_db->beginTransaction();
        $registroInventario = $this->_db->prepare("
          UPDATE
              lg_e001_inventario_fisico
          SET
              num_stock_logico=:num_stock_logico,
              num_stock_fisico=:num_stock_fisico,
              fk_rhb001_num_empleado_realiza='$this->atIdEmpleado',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
          WHERE
              pk_num_registro=:pk_num_registro
           ");


        $registroInventario->execute(array(
            'num_stock_logico'=>$slog,
            'num_stock_fisico'=>$sreal,
            'pk_num_registro'=>$idRegistro
        ));

        $fallaTansaccion = $registroInventario->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metListarRealizadas()
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
              lg_e001_inventario_fisico
            GROUP BY
              fec_anio,fec_mes
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetchAll();
    }

    public function metBucarRealizada($idItem)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
              lg_e001_inventario_fisico
            WHERE fk_lgb002_num_item = '$idItem' AND 
            fec_anio = '".date('Y')."' AND 
            fec_mes = '".date('m')."'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metBuscarRealizadas($anio,$num)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              fisico.*,
              item.*,
              unidades.ind_descripcion AS unidad
            FROM
              lg_e001_inventario_fisico AS fisico
              INNER JOIN lg_b002_item AS item ON item.pk_num_item = fisico.fk_lgb002_num_item
              INNER JOIN lg_b004_unidades AS unidades ON item.fk_lgb004_num_unidad_compra = unidades.pk_num_unidad
            WHERE
              fisico.fec_anio = '$anio' AND
              fisico.num_proceso = '$num'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetchAll();
    }


}

?>