<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
class desiertoModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metListarDesiertos()
    {
        $desiertoPost = $this->_db->query("
            SELECT
              recomendacion.*,
              acta.pk_num_acta_inicio,
              acta.ind_estado AS estatu
            FROM
              lg_b012_informe_recomendacion AS recomendacion
              INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
              INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
              WHERE recomendacion.ind_estado <> 'AP'
              AND acta.ind_estado = 'DS'
              ");
        $desiertoPost->setFetchMode(PDO::FETCH_ASSOC);
        return $desiertoPost->fetchAll();
    }

    public function metListarDesiertosPendientes()
    {
        $desiertoPost = $this->_db->query("
            SELECT
              recomendacion.*,
              acta.pk_num_acta_inicio
            FROM
              lg_b012_informe_recomendacion AS recomendacion
            INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
            INNER JOIN lg_b009_acta_inicio AS acta ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
            WHERE 
              recomendacion.ind_estado <> 'AP' 
              AND recomendacion.ind_estado <> 'AN' 
              AND recomendacion.ind_estado <> 'DS'
              AND acta.ind_estado <> 'DS'
              ");
        $desiertoPost->setFetchMode(PDO::FETCH_ASSOC);
        return $desiertoPost->fetchAll();
    }

    public function metMostrarInformeRecomendacion($idInfo)
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              *
            FROM
              lg_b012_informe_recomendacion
            WHERE
              pk_num_informe_recomendacion = '$idInfo'
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetch();
    }

    public function metDeclararDesierto($idActa,$idInfor){
        $this->_db->beginTransaction();
        $declararDesierto= $this->_db->prepare("
            UPDATE
              lg_b009_acta_inicio
            SET
              ind_estado=:ind_estado,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
            WHERE
              pk_num_acta_inicio=:pk_num_acta_inicio
        ");
        $declararDesierto->execute(array(
            'ind_estado' => 'DS',
            'pk_num_acta_inicio' => $idActa
        ));
        $anularRecomendacion= $this->_db->prepare("
            UPDATE
              lg_b012_informe_recomendacion
            SET
              ind_estado=:ind_estado,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
            WHERE
              pk_num_informe_recomendacion=:pk_num_informe_recomendacion
        ");
        $anularRecomendacion->execute(array(
            'ind_estado' => 'DS',
            'pk_num_informe_recomendacion' => $idInfor
        ));
        $fallaTansaccion = $declararDesierto->errorInfo();
        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $declararDesierto;
        }
    }

    public function metMostrarDatosGenerar($idInfo)
    {
        $mostrarInforme= $this->_db->query("
            SELECT
              acta.*,
              recomendacion.*,
              recomendacion.fec_ultima_modificacion AS fec_modif
            FROM
            lg_b009_acta_inicio AS acta
            INNER JOIN lg_b011_evaluacion AS evaluacion ON acta.pk_num_acta_inicio = evaluacion.fk_lgb009_num_acta_inicio
            INNER JOIN lg_b012_informe_recomendacion AS recomendacion ON evaluacion.pk_num_evaluacion = recomendacion.fk_lgb011_num_evaluacion
            WHERE
            acta.ind_estado = 'DS' AND
              recomendacion.pk_num_informe_recomendacion = '$idInfo'
        ");
        $mostrarInforme->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarInforme->fetch();
    }

    public function metBuscarCargo($cargo)
    {
        $AdjudicacionPost = $this->_db->query("
            SELECT
                *
            FROM
              rh_b001_empleado AS empleado
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
              INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
              INNER JOIN rh_c006_tipo_cargo AS cargo ON cargo.pk_num_cargo = puesto.fk_rhc006_num_cargo
            WHERE
                puesto.ind_descripcion_cargo = '$cargo'
              ");
        $AdjudicacionPost->setFetchMode(PDO::FETCH_ASSOC);
        return $AdjudicacionPost->fetch();
    }

}

?>