<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Guidmar Espinoza     | g.espinoza@contraloriamonagas.gob.ve    | 0414-1913443      | MONAGAS
 * | 2 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'clasificacionModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'almacenModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'proveedorModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'itemsModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'commodityModelo.php';

class requerimientoModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atClasificacionModelo = new clasificacionModelo();
        $this->atAlmacenModelo = new almacenModelo();
        $this->atProveedorModelo = new proveedorModelo();
        $this->atItemsModelo = new itemsModelo();
        $this->atCommodityModelo = new commodityModelo();
    }

    public function metContar($tabla,$anio,$campo,$extra=false)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              count(*) AS cantidad
            FROM
            $tabla
            WHERE
            $campo='$anio' 
            $extra
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metBuscarDependencia($id)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
            a004_dependencia
            WHERE
            pk_num_dependencia='$id'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metBuscarPredeterminado()
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
            rh_c076_empleado_organizacion 
            WHERE
            fk_rhb001_num_empleado='$this->atIdEmpleado'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metBuscarClasificacion($id,$cual)
    {
        if($cual==1){
            $and = "clasificacion.pk_num_clasificacion='$id'";
        } else {
            $and = "clasificacion.ind_descripcion='$id'";
        }
        $sql = "SELECT
                  clasificacion.*,
                  clasificacion.ind_descripcion AS clasif,
                  almacen.*
                FROM
                lg_b014_almacen AS almacen
                INNER JOIN lg_b017_clasificacion AS clasificacion ON almacen.pk_num_almacen = clasificacion.fk_lgb014_num_almacen 
                AND $and";

        $generarOrdenPost = $this->_db->query($sql);
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);

        return $generarOrdenPost->fetch();
    }

    public function metListarRequerimiento($estatus=false,$clas=false,$idReq = false)
    {

        if($idReq){

            $where="WHERE req.pk_num_requerimiento='$idReq'";
        } else {
            if($estatus){
                $where="WHERE req.ind_estado='$estatus'";
            } else {
                $where="";
            }

            if($clas AND $clas =="3"){
                $where=$where." AND almacen.num_flag_commodity='0' AND c.ind_descripcion='STOCK DE ALMACEN'";
            } elseif($clas AND $clas =="2"){
                $where=$where." AND c.ind_descripcion='STOCK DE ALMACEN'";
            } elseif($clas AND $clas =="1"){
                $where=$where." AND req.fk_lgb014_num_almacen='1' AND c.num_flag_caja_chica='1'";
            } elseif($clas){
                $where=$where." AND req.fk_lgb014_num_almacen='1'";
            }
        }

        $requerimientoPost =  $this->_db->query("
            SELECT
              req.*,
              req.ind_estado AS estatus,
              c.*,
              c.num_flag_caja_chica AS flagCaja,
              c.num_flag_revision AS flagRevi,
              costo.pk_num_centro_costo,
              costo.ind_descripcion_centro_costo,
              almacen.ind_descripcion AS ind_nombre_detalle,
              almacen.num_flag_commodity
            FROM lg_b001_requerimiento AS req
            LEFT JOIN lg_b017_clasificacion AS c ON c.pk_num_clasificacion = req.fk_lgb017_num_clasificacion
            LEFT JOIN a023_centro_costo AS costo ON costo.pk_num_centro_costo = req.fk_a023_num_centro_costo
            LEFT JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = req.fk_lgb014_num_almacen
            $where
            ORDER BY req.pk_num_requerimiento ASC
             ");

        $requerimientoPost->setFetchMode(PDO::FETCH_ASSOC);
        if($idReq){
            return $requerimientoPost->fetch();
        } else {
            return $requerimientoPost->fetchAll();
        }
    }

    public function metListarRequerimientoDetalle()
    {
        $requerimientoPost =  $this->_db->query("
            SELECT
              req.*,
              req.ind_estado AS estatus,
              c.*,
              item.ind_descripcion AS descIem,
              commodity.ind_descripcion AS descComm,
              detalle.ind_estado AS estadoDet
            FROM lg_b001_requerimiento AS req
            INNER JOIN lg_b017_clasificacion AS c ON c.pk_num_clasificacion = req.fk_lgb017_num_clasificacion
            INNER JOIN lg_c001_requerimiento_detalle AS detalle ON req.pk_num_requerimiento = detalle.fk_lgb001_num_requerimiento
            LEFT JOIN lg_b002_item AS item ON item.pk_num_item = detalle.fk_lgb002_num_item
            LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle.fk_lgb003_num_commodity
            ORDER BY req.cod_requerimiento ASC
             ");

        $requerimientoPost->setFetchMode(PDO::FETCH_ASSOC);
        return $requerimientoPost->fetchAll();
    }

    public function metActualizarRequerimiento($estatus,$idReq,$empleado=false,$fecha=false,$comentario=false)
    {
        if($comentario){
            $c=$comentario;
        } else {
            $c=null;
        }
        if($fecha) {
            $set=" $empleado='$this->atIdEmpleado',
              $fecha=NOW(),
              ind_estado='$estatus'";
        } elseif($empleado=="caja") {
            $set="ind_verificacion_caja_chica='$comentario',
            fk_rhb001_num_empleado_verif_caja_chica='$this->atIdEmpleado',
            fec_verificacion_caja_chica=NOW(),
            num_flag_caja_chica='1'";
        } elseif($empleado=="partida") {
            $set="ind_verificacion_presupuestaria='$comentario',
            fk_rhb001_num_empleado_verif_presupuestaria='$this->atIdEmpleado',
            fec_verificacion_presupuestaria=NOW()";
        } else {
            $set="ind_estado='$estatus'";
        }

        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $NuevoPost=$this->_db->prepare(
            "UPDATE lg_b001_requerimiento
              SET 
              $set,
              fec_ultima_modificacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              WHERE pk_num_requerimiento =:pk_num_requerimiento
              ");

        $NuevoPost->execute(array(
            'pk_num_requerimiento'=>$idReq
        ));

        #commit — Consigna una transacción

        $fallaTansaccion = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idReq;
        }
    }

    public function metListarActa($idReq=false,$idActa=false)
    {
        if($idActa){
            $where="WHERE pk_num_acta_inicio = '$idActa'";
        } elseif ($idReq) {
            $where="WHERE fk_lgb001_num_requerimiento = '$idReq' AND ind_estado='PR'";
        } else {
            $where="";
        }
        $ActaPost = $this->_db->query(
            "SELECT
              *
              FROM lg_b009_acta_inicio
              $where
          ");
        $ActaPost->setFetchMode(PDO::FETCH_ASSOC);
        if ($idReq) {
            return $ActaPost->fetch();
        } elseif ($idActa) {
            return $ActaPost->fetch();
        } else {
            return $ActaPost->fetchAll();
        }
    }

    public function metListarDependencias()
    {
        $dependencias = $this->_db->query("
                SELECT
                  dependencia.*
                FROM
                  a004_dependencia AS dependencia
                INNER JOIN
                  a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = dependencia.pk_num_dependencia
                LEFT JOIN
                  rh_c076_empleado_organizacion AS organizacion ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
                WHERE 
                  (
                  seguridad.fk_a018_num_seguridad_usuario = '$this->atIdUsuario' 
                  AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
                  ) 
                  OR dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
                  GROUP BY dependencia.pk_num_dependencia
                ");
        $dependencias->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencias->fetchAll();
    }

    public function metListarCentroCosto($idDependencia=false,$idCC=false)
    {
        if ($idCC) {
            $where =" WHERE pk_num_centro_costo = '$idCC'";
        } elseif ($idDependencia) {
            $where =" WHERE fk_a004_num_dependencia = '$idDependencia'";
        } else {
            $where ="";
        }
        $dependencias = $this->_db->query("
              SELECT
                pk_num_centro_costo,
                ind_descripcion_centro_costo,
                fk_a004_num_dependencia
              FROM
                a023_centro_costo
                $where
              ");
        $dependencias->setFetchMode(PDO::FETCH_ASSOC);
        if ($idCC) {
            return $dependencias->fetch();
        } else{
            return $dependencias->fetchAll();
        }
    }

    public function metBuscarEmpleado($idReq,$campo)
    {
        $personaPost = $this->_db->query("
            SELECT
              p.pk_num_persona,
              p.ind_nombre1,
              p.ind_apellido1,
              p.ind_cedula_documento,
              p.ind_documento_fiscal,
              e.pk_num_empleado,
              puesto.ind_descripcion_cargo
            FROM a003_persona AS p
            INNER JOIN rh_b001_empleado AS e ON p.pk_num_persona=e.fk_a003_num_persona
            INNER JOIN lg_b001_requerimiento AS req ON e.pk_num_empleado = req.$campo
            LEFT JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = e.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            WHERE req.pk_num_requerimiento ='$idReq'
              ");
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        return $personaPost ->fetch();
    }

    public function metBuscarRequerimiento($idReq)
    {
        $requerimientoPost =  $this->_db->query("
            SELECT
            req.*,
            despacho.ind_nombre1 AS despNom,
            despacho.ind_apellido1 AS despApe,
            persona.ind_nombre1 AS provNom,
            persona.ind_apellido1 AS provApe,
            persona.ind_documento_fiscal AS provCed,
            transaccion.pk_num_transaccion,
            transaccion.ind_comentario,
            transaccion.num_estatus AS tranStatus,
            dependencia.ind_dependencia,
            costo.ind_descripcion_centro_costo,
            detalle.ind_nombre_detalle,
            clasificacion.ind_descripcion AS clasf,
            req.fec_requerida,
            almacen.num_flag_commodity
            FROM lg_b001_requerimiento AS req
            LEFT JOIN lg_b022_proveedor proveedor ON proveedor.pk_num_proveedor = req.fk_lgb022_num_proveedor_sugerido
            LEFT JOIN rh_b001_empleado AS rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = req.fk_rhb001_num_empleado_aprueba_despacho
            LEFT JOIN a003_persona AS despacho ON despacho.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            LEFT JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            LEFT JOIN lg_d001_transaccion AS transaccion ON transaccion.ind_num_documento_referencia = req.cod_requerimiento
            INNER JOIN a004_dependencia AS dependencia ON req.fk_a004_num_dependencia = dependencia.pk_num_dependencia
            INNER JOIN a023_centro_costo AS costo ON req.fk_a023_num_centro_costo = costo.pk_num_centro_costo
            INNER JOIN lg_b017_clasificacion AS clasificacion ON req.fk_lgb017_num_clasificacion = clasificacion.pk_num_clasificacion
            INNER JOIN a006_miscelaneo_detalle AS detalle ON req.fk_a006_num_miscelaneos_prioridad = detalle.pk_num_miscelaneo_detalle
            INNER JOIN lg_b014_almacen AS almacen ON req.fk_lgb014_num_almacen = almacen.pk_num_almacen
            WHERE req.pk_num_requerimiento = $idReq
            ");

        $requerimientoPost->setFetchMode(PDO::FETCH_ASSOC);
        return $requerimientoPost->fetch();
    }

    public function metBuscarReqDetalle($idReq)
    {
        $requerimientoPost =  $this->_db->query("
            SELECT
            detalle.*,
            
            item.ind_descripcion AS ind_descripcion_item,
            item.ind_codigo_interno,
            item.fk_cbb004_num_plan_cuenta_gasto_oncop AS cuentaItem,
            item.fk_cbb004_num_plan_cuenta_gasto_pub_veinte AS cuentaItem2,
            
            cuenta1.cod_cuenta AS codCuentaItem,
            cuenta1.ind_descripcion AS descCuentaItem,
            
            cuenta2.cod_cuenta AS codCuentaItem2,
            cuenta2.ind_descripcion AS descCuentaItem2,
            
            item.fk_prb002_num_partida_presupuestaria AS partidaItem,
            presupuestaria1.cod_partida AS codPartidaItem,
            presupuestaria1.ind_denominacion AS descPartidaItem,

            commodity.ind_descripcion AS ind_descripcion_comm,
            commodity.ind_cod_commodity,
            commodity.fk_cbb004_num_plan_cuenta AS cuentaComm,
            commodity.fk_cbb004_num_plan_cuenta_pub_veinte AS cuentaComm2,
            
            cuenta3.cod_cuenta AS codCuentaComm,
            cuenta3.ind_descripcion AS descCuentaComm,
            
            cuenta4.cod_cuenta AS codCuentaComm2,
            cuenta4.ind_descripcion AS descCuentaComm2,
            
            presupuestaria2.pk_num_partida_presupuestaria AS partidaComm,
            presupuestaria2.cod_partida AS codPartidaComm,
            presupuestaria2.ind_denominacion AS descPartidaComm,

            stock.pk_num_item_stock,
            stock.num_stock_actual,
            stock.num_precio_unitario,
            
            stock2.pk_num_commodity_stock,
            stock2.num_cantidad AS num_cantidad_comm,
            stock2.num_precio_unitario,
            
            unidades.ind_descripcion as unidad,
            unidades.pk_num_unidad,
            detalle2.num_cantidad_transaccion,
            (SELECT SUM(det.num_cantidad_transaccion) FROM lg_d002_transaccion_detalle AS det 
            WHERE det.fk_lgc001_num_requerimiento_detalle = detalle.pk_num_requerimiento_detalle) AS cantidadDespachada
            FROM lg_c001_requerimiento_detalle AS detalle
            LEFT JOIN lg_b002_item AS item ON item.pk_num_item = detalle.fk_lgb002_num_item
            LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle.fk_lgb003_num_commodity
            LEFT JOIN cb_b004_plan_cuenta AS cuenta1 ON cuenta1.pk_num_cuenta = item.fk_cbb004_num_plan_cuenta_gasto_oncop
            LEFT JOIN cb_b004_plan_cuenta AS cuenta2 ON cuenta2.pk_num_cuenta = item.fk_cbb004_num_plan_cuenta_gasto_pub_veinte
            LEFT JOIN cb_b004_plan_cuenta AS cuenta3 ON cuenta3.pk_num_cuenta = commodity.fk_cbb004_num_plan_cuenta
            LEFT JOIN cb_b004_plan_cuenta AS cuenta4 ON cuenta4.pk_num_cuenta = commodity.fk_cbb004_num_plan_cuenta_pub_veinte
            LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria1 ON presupuestaria1.pk_num_partida_presupuestaria = item.fk_prb002_num_partida_presupuestaria
            LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria2 ON presupuestaria2.pk_num_partida_presupuestaria = commodity.fk_prb002_num_partida_presupuestaria
            LEFT JOIN lg_c007_item_stock AS stock ON stock.pk_num_item_stock = item.fk_lgc007_num_item_stock
            LEFT JOIN lg_c008_commodity_stock AS stock2 ON commodity.pk_num_commodity = stock2.fk_lgb003_num_commodity
            LEFT JOIN lg_b004_unidades AS unidades ON unidades.pk_num_unidad = detalle.fk_lgb004_num_unidad
            LEFT JOIN lg_d002_transaccion_detalle AS detalle2 ON detalle2.fk_lgc001_num_requerimiento_detalle = detalle.pk_num_requerimiento_detalle
            WHERE fk_lgb001_num_requerimiento = '$idReq'
            GROUP BY detalle.pk_num_requerimiento_detalle
            ORDER BY detalle.pk_num_requerimiento_detalle ASC
            ");

        $requerimientoPost->setFetchMode(PDO::FETCH_ASSOC);
        return $requerimientoPost->fetchAll();
    }

    public function metNuevoRequerimiento(
        $dependencia,$centroCosto,$clasificacion,
        $almacen,$prioridad,$cajac,
        $vcajac,$ppres,$proveedor,
        $codigo,$comentarioR,$cant,
        $exonerado,$comentarioI,$especificacionTecnica,$numItem,
        $numComm,$numCC,$despacho,
        $secuencia,$idUnidad)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #prepare — Prepara una sentencia para su ejecución y devuelve un objeto sentencia
        $NuevoPost1=$this->_db->prepare(
            "INSERT INTO lg_b001_requerimiento
              SET
              cod_requerimiento=:cod_requerimiento,
              fk_a004_num_dependencia=:fk_a004_num_dependencia,
              fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
              fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
              fk_a006_num_miscelaneos_prioridad=:fk_a006_num_miscelaneos_prioridad,
              fk_rhb001_num_empleado_preparado_por='$this->atIdEmpleado',
              fec_preparacion=NOW(),
              fec_requerida=NOW(),
              ind_comentarios=:ind_comentarios,
              ind_razon_rechazo=:ind_razon_rechazo,
              num_flag_caja_chica=:num_flag_caja_chica,
              ind_estado=:ind_estado,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              ind_verificacion_caja_chica=:ind_verificacion_caja_chica,
              ind_verificacion_presupuestaria=:ind_verificacion_presupuestaria,
              num_flag_aprobacion_despacho=:num_flag_aprobacion_despacho,
              fec_anio=:fec_anio,
              fec_mes=:fec_mes,
              fk_lgb022_num_proveedor_sugerido=:fk_lgb022_num_proveedor_sugerido,
              fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion
              ");
        #execute — Ejecuta una sentencia preparada
        $NuevoPost1->execute(array(
            'cod_requerimiento'=> $codigo,
            'fk_a004_num_dependencia'=> $dependencia,
            'fk_a023_num_centro_costo'=> $centroCosto,
            'fk_lgb014_num_almacen'=> $almacen,
            'fk_a006_num_miscelaneos_prioridad'=> $prioridad,
            'ind_comentarios'=> $comentarioR,
            'ind_razon_rechazo'=> "",
            'num_flag_caja_chica'=> $cajac,
            'ind_estado'=> 'PR',
            'ind_verificacion_caja_chica'=> $vcajac,
            'ind_verificacion_presupuestaria'=> $ppres,
            'num_flag_aprobacion_despacho'=>$despacho,
            'fec_anio'=>date('Y'),
            'fec_mes'=>date('m'),
            'fk_lgb022_num_proveedor_sugerido'=> $proveedor,
            'fk_lgb017_num_clasificacion'=> $clasificacion
        ));
        $numReq=$this->_db->lastInsertId();

        $NuevoPost2=$this->_db->prepare("
            INSERT INTO
                lg_c001_requerimiento_detalle
            SET
                num_secuencia=:num_secuencia,
                num_cantidad_pedida=:num_cantidad_pedida,
                num_flag_exonerado=:num_flag_exonerado,
                ind_comentarios=:ind_comentarios,
                ind_especificacion_tecnica=:ind_especificacion_tecnica,
                ind_estado=:ind_estado,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_lgb002_num_item=:fk_lgb002_num_item,
                fk_lgb001_num_requerimiento=:fk_lgb001_num_requerimiento,
                fk_lgb003_num_commodity=:fk_lgb003_num_commodity,
                fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                fk_lgc003_num_cotizacion=:fk_lgc003_num_cotizacion,
                fk_lgb004_num_unidad=:fk_lgb004_num_unidad
            ");

        if ($secuencia!=0) {
            $cont=1;
            foreach ($cant as $key=>$value) {
                #execute — Ejecuta una sentencia preparada
                $NuevoPost2->execute(array(
                    'num_secuencia' => $cont,
                    'num_cantidad_pedida' => $cant[$key],
                    'num_flag_exonerado' => $exonerado[$key],
                    'ind_comentarios' => $comentarioI[$key],
                    'ind_especificacion_tecnica'=>$especificacionTecnica[$key],
                    'ind_estado' => 'PR',
                    'fk_lgb002_num_item' => $numItem[$key],
                    'fk_lgb001_num_requerimiento' => $numReq,
                    'fk_lgb003_num_commodity' => $numComm[$key],
                    'fk_a023_num_centro_costo' => $numCC[$key],
                    'fk_lgc003_num_cotizacion' => NULL,
                    'fk_lgb004_num_unidad' => $idUnidad[$key]
                ));
                $cont = $cont + 1;
            }
        }
        #commit — Consigna una transacción

        $fallaTansaccion1 = $NuevoPost1->errorInfo();
        $fallaTansaccion2 = $NuevoPost2->errorInfo();

        if(!empty($fallaTansaccion1[1]) && !empty($fallaTansaccion1[2])){
            $this->_db->rollBack();
            return $fallaTansaccion1;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }else{
            $this->_db->commit();
            return $numReq;
        }

    }

    public function metModificarRequerimiento(
        $dependencia,$centroCosto,$clasificacion,
        $almacen,$prioridad,$cajac,
        $vcajac,$ppres,$proveedor,
        $codigo,$comentarioR,$cant,
        $exonerado,$comentarioI,$especificacionTecnica,
        $numItem,$numComm,$numCC,$despacho,
        $secuencia,$idUnidad,$idReq)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $NuevoPost=$this->_db->prepare(
            "UPDATE lg_b001_requerimiento
              SET
              fk_a004_num_dependencia=:fk_a004_num_dependencia,
              fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
              fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
              fk_a006_num_miscelaneos_prioridad=:fk_a006_num_miscelaneos_prioridad,
              ind_comentarios=:ind_comentarios,
              num_flag_caja_chica=:num_flag_caja_chica,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              ind_verificacion_caja_chica=:ind_verificacion_caja_chica,
              ind_verificacion_presupuestaria=:ind_verificacion_presupuestaria,
              num_flag_aprobacion_despacho=:num_flag_aprobacion_despacho,
              fec_anio=:fec_anio,
              fec_mes=:fec_mes,
              fk_lgb022_num_proveedor_sugerido=:fk_lgb022_num_proveedor_sugerido,
              fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion
              WHERE pk_num_requerimiento = $idReq
              ");
        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            'fk_a004_num_dependencia'=> $dependencia,
            'fk_a023_num_centro_costo'=> $centroCosto,
            'fk_lgb014_num_almacen'=> $almacen,
            'fk_a006_num_miscelaneos_prioridad'=> $prioridad,
            'ind_comentarios'=> $comentarioR,
            'num_flag_caja_chica'=> $cajac,
            'ind_verificacion_caja_chica'=> $vcajac,
            'ind_verificacion_presupuestaria'=> $ppres,
            'num_flag_aprobacion_despacho'=>$despacho,
            'fec_anio'=>date('Y'),
            'fec_mes'=>date('m'),
            'fk_lgb022_num_proveedor_sugerido'=> $proveedor,
            'fk_lgb017_num_clasificacion'=> $clasificacion
        ));

        if ($secuencia!=0) {
            $this->_db->query("DELETE FROM lg_c001_requerimiento_detalle WHERE fk_lgb001_num_requerimiento='$idReq'");
            $this->_db->query("ALTER TABLE lg_c001_requerimiento_detalle AUTO_INCREMENT=1");
            foreach ($cant as $key=>$value) {

                $NuevoPostInsert=$this->_db->prepare("
                    INSERT INTO
                        lg_c001_requerimiento_detalle
                    SET
                        num_secuencia=:num_secuencia,
                        num_cantidad_pedida=:num_cantidad_pedida,
                        num_flag_exonerado=:num_flag_exonerado,
                        ind_comentarios=:ind_comentarios,
                        ind_especificacion_tecnica=:ind_especificacion_tecnica,
                        ind_estado=:ind_estado,
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                        fec_ultima_modificacion=NOW(),
                        fk_lgb002_num_item=:fk_lgb002_num_item,
                        fk_lgb003_num_commodity=:fk_lgb003_num_commodity,
                        fk_a023_num_centro_costo=:fk_a023_num_centro_costo,
                        fk_lgb001_num_requerimiento=:fk_lgb001_num_requerimiento,
                        fk_lgb004_num_unidad=:fk_lgb004_num_unidad
                    ");
                #execute — Ejecuta una sentencia preparada
                $NuevoPostInsert->execute(array(
                    'num_secuencia' => $secuencia,
                    'num_cantidad_pedida' => $cant[$key],
                    'num_flag_exonerado' => $exonerado[$key],
                    'ind_comentarios' => $comentarioI[$key],
                    'ind_especificacion_tecnica'=>$especificacionTecnica[$key],
                    'ind_estado' => 'PR',
                    'fk_lgb002_num_item' => $numItem[$key],
                    'fk_lgb003_num_commodity' => $numComm[$key],
                    'fk_a023_num_centro_costo' => $numCC[$key],
                    'fk_lgb001_num_requerimiento' => $idReq,
                    'fk_lgb004_num_unidad' => $idUnidad[$key]
                ));
                $fallaTansaccion = $NuevoPostInsert->errorInfo();
            }
        }

        #commit — Consigna una transacción
        $fallaTansaccion2 = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }else{
            $this->_db->commit();
            return $idReq;
        }
    }

    public function metAnularRequerimiento($idReq,$motivo)
    {
        $this->_db->beginTransaction();

        $query1=$this->_db->prepare("
            UPDATE
            lg_c001_requerimiento_detalle
            SET
            ind_estado='AN',
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
            WHERE fk_lgb001_num_requerimiento=:fk_lgb001_num_requerimiento
            ");

        $query1->execute(array(
            'fk_lgb001_num_requerimiento'=>$idReq
        ));

        $query2=$this->_db->prepare("
            UPDATE
              lg_b001_requerimiento
            SET
              ind_estado='AN',
              ind_razon_rechazo=:ind_razon_rechazo,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW()
            WHERE
              pk_num_requerimiento=:pk_num_requerimiento
            ");

        $query2->execute(array(
            'ind_razon_rechazo'=>$motivo,
            'pk_num_requerimiento'=>$idReq
        ));

        $fallaTansaccion = $query2->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idReq;
        }
    }

    public function metEliminarDetalle($id)
    {
        #beginTransaction — Inicia una transacción
        $this->_db->beginTransaction();
        #query — Ejecuta una sentencia SQL, devolviendo un conjunto de resultados como un objeto
        $query1=$this->_db->prepare("
            DELETE FROM
            lg_c001_requerimiento_detalle
            WHERE pk_num_requerimiento_detalle=:pk_num_requerimiento_detalle
            ");

        $query1->execute(array(
            'pk_num_requerimiento_detalle'=>$id
        ));

        $fallaTansaccion = $query1->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            #commit — Consigna una transacción
            $this->_db->commit();
            return $id;
        }
    }

    public function metListarDetalle($idEval){

        $dependencias = $this->_db->query("
              SELECT
                detalle.*
              FROM
                lg_c001_requerimiento_detalle AS detalle
              INNER JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.pk_num_cotizacion = detalle.fk_lgc003_num_cotizacion
              INNER JOIN lg_c004_evaluacion_cuantitativa AS cuantitativa ON cotizacion.pk_num_cotizacion = cuantitativa.fk_lgc003_num_cotizacion
              INNER JOIN lg_b011_evaluacion AS evaluacion ON evaluacion.pk_num_evaluacion = cuantitativa.fk_lgb011_num_evaluacion
              WHERE evaluacion.pk_num_evaluacion = '$idEval'
                ");
        $dependencias->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencias->fetchAll();
    }

    public function metBuscarCuenta($codCuenta){

        $dependencias = $this->_db->query("
              SELECT
                *
              FROM
                cb_b004_plan_cuenta AS cuenta
                INNER JOIN pr_b002_partida_presupuestaria AS presupuestaria ON cuenta.pk_num_cuenta = presupuestaria.fk_cbb004_num_plan_cuenta_pub20
              WHERE
                cuenta.cod_cuenta = '$codCuenta'
                ");
        $dependencias->setFetchMode(PDO::FETCH_ASSOC);
        return $dependencias->fetchAll();
    }

}
