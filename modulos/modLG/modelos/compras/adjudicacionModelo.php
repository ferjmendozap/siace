<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'compras' . DS . 'actaInicioModelo.php';
class adjudicacionModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atActaInicioModelo = new actaInicioModelo();
    }

    public function metListarProveedorRecomendado()
    {
        $AdjudicacionPost = $this->_db->query("
            SELECT
              recomendado.*,
              recomendado.num_secuencia AS num_secuencia_recomendado,
              recomendado.fk_lgb012_num_informe_recomendacion AS fkInfo,
              adjudicacion.*,
              persona.*,
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              cotizacion.*,
              direccion.*
            FROM
              lg_c006_proveedor_recomendado AS recomendado
            INNER JOIN lg_b012_informe_recomendacion AS recomendacion ON recomendacion.pk_num_informe_recomendacion = recomendado.fk_lgb012_num_informe_recomendacion
            INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = recomendado.fk_lgb022_num_proveedor
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            LEFT JOIN a036_persona_direccion AS direccion ON persona.pk_num_persona = direccion.fk_a003_num_persona
            LEFT JOIN lg_b013_adjudicacion AS adjudicacion ON adjudicacion.fk_lgb012_num_informe_recomendacion = recomendacion.pk_num_informe_recomendacion
            LEFT JOIN lg_c005_adjudicacion_detalle AS detalle ON adjudicacion.pk_num_adjudicacion = detalle.fk_lgb013_num_adjudicacion
            LEFT JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.pk_num_cotizacion = detalle.fk_lgc003_num_cotizacion
              ");
        $AdjudicacionPost->setFetchMode(PDO::FETCH_ASSOC);
        return $AdjudicacionPost->fetchAll();
    }

    public function metMostrarProveedor($idProveedor)
    {
        $AdjudicacionPost = $this->_db->query("
            SELECT
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              proveedor.pk_num_proveedor
            FROM
                lg_b022_proveedor AS proveedor
            INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
            WHERE proveedor.pk_num_proveedor = '$idProveedor'
              ");
        $AdjudicacionPost->setFetchMode(PDO::FETCH_ASSOC);
        return $AdjudicacionPost->fetch();
    }

    public function metMostrarAdjudicacion($idAdj){

        $mostrarAdjudicacion = $this->_db->query("
              SELECT
                CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
                adjudicacion.*,
                detalle.*,
                recomendacion.*
              FROM
                lg_b013_adjudicacion AS adjudicacion
                LEFT JOIN lg_c005_adjudicacion_detalle AS detalle ON adjudicacion.pk_num_adjudicacion = detalle.fk_lgb013_num_adjudicacion
                INNER JOIN lg_b012_informe_recomendacion AS recomendacion ON recomendacion.pk_num_informe_recomendacion = adjudicacion.fk_lgb012_num_informe_recomendacion
                INNER JOIN lg_c006_proveedor_recomendado AS recomendado ON recomendacion.pk_num_informe_recomendacion = recomendado.fk_lgb012_num_informe_recomendacion
                INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = recomendado.fk_lgb022_num_proveedor
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
              WHERE
                adjudicacion.pk_num_adjudicacion='$idAdj'
        ");
        $mostrarAdjudicacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarAdjudicacion->fetchAll();
    }

    public function metMostrarAdjudicacionReporte($idInfor){

        $mostrarAdjudicacion = $this->_db->query("
              SELECT
                CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
                persona.ind_documento_fiscal,
                direccion.ind_direccion,
                adjudicacion.*,
                detalle.*,
                recomendacion.*,
                cotizacion.num_total,
                SUM(cotizacion.num_total) AS total,
                adjudicacion.fec_creacion AS fec_adjudicacion
              FROM
                lg_b013_adjudicacion AS adjudicacion
                LEFT JOIN lg_c005_adjudicacion_detalle AS detalle ON adjudicacion.pk_num_adjudicacion = detalle.fk_lgb013_num_adjudicacion
                INNER JOIN lg_c003_cotizacion AS cotizacion ON cotizacion.pk_num_cotizacion = detalle.fk_lgc003_num_cotizacion
                INNER JOIN lg_b012_informe_recomendacion AS recomendacion ON recomendacion.pk_num_informe_recomendacion = adjudicacion.fk_lgb012_num_informe_recomendacion
                INNER JOIN lg_c006_proveedor_recomendado AS recomendado ON recomendacion.pk_num_informe_recomendacion = recomendado.fk_lgb012_num_informe_recomendacion
                AND recomendado.num_secuencia = adjudicacion.num_adjudicacion
                INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = recomendado.fk_lgb022_num_proveedor
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                LEFT JOIN a036_persona_direccion AS direccion ON persona.pk_num_persona = direccion.fk_a003_num_persona
              WHERE
                recomendacion.pk_num_informe_recomendacion='$idInfor'
                GROUP BY adjudicacion.pk_num_adjudicacion
        ");
        $mostrarAdjudicacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarAdjudicacion->fetchAll();
    }

    public function metCrearAdjudicacion($tipoAdj,$idInfor,$reqDet,$coti,$adjudicar,$proveedor,$sec,$num)
    {
        $this->_db->beginTransaction();
        $registroAdjudicacion = $this->_db->prepare("
          INSERT INTO
            lg_b013_adjudicacion
          SET
            num_adjudicacion=:num_adjudicacion,
			fk_a006_num_miscelaneo_detalle_tipo_adjudicacion=:fk_a006_num_miscelaneo_detalle_tipo_adjudicacion,
			fec_creacion=NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            ind_estado=:ind_estado,
            fec_anio=:fec_anio,
			fk_lgb012_num_informe_recomendacion=:fk_lgb012_num_informe_recomendacion
           ");

        $registroAdjudicacion->execute(array(
            'num_adjudicacion'=>$num,
            'fk_a006_num_miscelaneo_detalle_tipo_adjudicacion'=>$tipoAdj,
            'ind_estado'=>'PR',
            'fec_anio'=>date('Y'),
            'fk_lgb012_num_informe_recomendacion'=>$idInfor
        ));

        $idRegistro= $this->_db->lastInsertId();

        $registroDetalle = $this->_db->prepare("
          INSERT INTO
            lg_c005_adjudicacion_detalle
          SET
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fk_lgb023_num_acta_detalle=:fk_lgb023_num_acta_detalle,
			fk_lgb013_num_adjudicacion=$idRegistro,
			fk_lgb022_num_proveedor_adjudicado=$proveedor,
            fk_lgc003_num_cotizacion=:fk_lgc003_num_cotizacion
           ");
        $contador=0;
        while($contador<$sec){
            if($adjudicar[$contador]=="1"){
                $registroDetalle->execute(array(
                    'fk_lgc003_num_cotizacion'=>$coti[$contador],
                    'fk_lgb023_num_acta_detalle'=>$reqDet[$contador]
                ));

                $this->_db->query("
                  UPDATE
                    lg_b023_acta_detalle
                  SET
                    fk_lgc003_num_cotizacion = '$coti[$contador]'
                  WHERE
                    pk_num_acta_detalle = '$reqDet[$contador]'
                    ");
            }
            $contador=$contador+1;
        }

        $fallaTansaccion = $registroAdjudicacion->errorInfo();
        $fallaTansaccion2 = $registroDetalle->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarAdjudicacion(
        $reqDet,$coti,$adjudicar,
        $proveedor,$sec,$idAdj)
    {
        $this->_db->beginTransaction();

        $modificarRegistro=$this->_db->prepare("
          UPDATE
            lg_b013_adjudicacion
          SET
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
		  WHERE
		    pk_num_adjudicacion=:pk_num_adjudicacion
            ");
        $modificarRegistro->execute(array(
            'pk_num_adjudicacion'=>$idAdj
        ));

        $insertarDetalle = $this->_db->prepare("
          INSERT INTO
            lg_c005_adjudicacion_detalle
          SET
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            fk_lgb023_num_acta_detalle=:fk_lgb023_num_acta_detalle,
			fk_lgb013_num_adjudicacion=$idAdj,
			fk_lgb022_num_proveedor_adjudicado=$proveedor,
            fk_lgc003_num_cotizacion=:fk_lgc003_num_cotizacion
           ");

        $this->_db->query("
                  DELETE FROM
                    lg_c005_adjudicacion_detalle
                  WHERE
                    fk_lgb013_num_adjudicacion = '$idAdj'
                    ");

        $this->_db->query("ALTER TABLE lg_c005_adjudicacion_detalle auto_increment = 1 ROW_FORMAT = COMPACT");

        $contador=0;
        while($contador<$sec){
            if($adjudicar[$contador]=="1"){
                $insertarDetalle->execute(array(
                    'fk_lgb023_num_acta_detalle'=>$reqDet[$contador],
                    'fk_lgc003_num_cotizacion'=>$coti[$contador]
                ));

                $this->_db->query("
                  UPDATE
                    lg_b023_acta_detalle
                  SET
                    fk_lgc003_num_cotizacion = '$coti[$contador]'
                  WHERE
                    pk_num_acta_detalle = '$reqDet[$contador]'
                    ");
            }
            $contador=$contador+1;
        }

        $error1 = $modificarRegistro->errorInfo();
        $error2 = $insertarDetalle->errorInfo();

        if(!empty($error1[1]) && !empty($error1[2])){
            $this->_db->rollBack();
            return $error1;
        }elseif(!empty($error2[1]) && !empty($error2[2])){
            $this->_db->rollBack();
            return $error2;
        }else{
            $this->_db->commit();
            return $idAdj;
        }
    }

    public function metBuscarTotal($idAdj)
    {
        $AdjudicacionPost = $this->_db->query("
            SELECT
                cotizacion.num_total
            FROM
              lg_c003_cotizacion AS cotizacion
            INNER JOIN lg_c005_adjudicacion_detalle AS detalle ON detalle.fk_lgc003_num_cotizacion = cotizacion.pk_num_cotizacion
            WHERE 
              detalle.fk_lgb013_num_adjudicacion = '$idAdj'
              ");
        $AdjudicacionPost->setFetchMode(PDO::FETCH_ASSOC);
        return $AdjudicacionPost->fetchAll();
    }

    public function metBuscarCargo($cargo)
    {
        $AdjudicacionPost = $this->_db->query("
            SELECT
                *
            FROM
              rh_b001_empleado AS empleado
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
              INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
              INNER JOIN rh_c006_tipo_cargo AS cargo ON cargo.pk_num_cargo = puesto.fk_rhc006_num_cargo
            WHERE
                puesto.ind_descripcion_cargo = '$cargo'
              ");
        $AdjudicacionPost->setFetchMode(PDO::FETCH_ASSOC);
        return $AdjudicacionPost->fetch();
    }

    public function metListarInvitacionCotizacion()
    {
        $AdjudicacionPost = $this->_db->query("
            SELECT
                invitacion.*,
                cotizacion.pk_num_cotizacion,
                cotizacion.fk_lgb023_num_acta_detalle,
                cotizacion.num_total,
                cotizacion.num_flag_asignado,
                cotizacion.num_cantidad
            FROM
                lg_c003_cotizacion AS cotizacion 
            INNER JOIN lg_b010_invitacion AS invitacion ON invitacion.pk_num_invitacion = cotizacion.fk_lgb010_num_invitacion
              ");
        $AdjudicacionPost->setFetchMode(PDO::FETCH_ASSOC);
        return $AdjudicacionPost->fetchAll();
    }

    public function metAnularRecomendacion($datos)
    {
        $this->_db->beginTransaction();

        $modificarRegistro=$this->_db->prepare("
          UPDATE
            lg_b012_informe_recomendacion
          SET
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW(),
            ind_razon_rechazo=:ind_razon_rechazo,
            ind_estado = 'AN'
		  WHERE
		    pk_num_informe_recomendacion=:pk_num_informe_recomendacion
            ");
        $modificarRegistro->execute(array(
            'pk_num_informe_recomendacion'=>$datos['idInfor'],
            'ind_razon_rechazo'=>$datos['motivo']
        ));

        $modificarRegistro2=$this->_db->prepare("
          DELETE FROM
            lg_c005_adjudicacion_detalle
		  WHERE
		    fk_lgb013_num_adjudicacion=:fk_lgb013_num_adjudicacion
            ");

        $modificarRegistro3=$this->_db->prepare("
          DELETE FROM
            lg_b013_adjudicacion
		  WHERE
		    pk_num_adjudicacion=:pk_num_adjudicacion
            ");

        foreach ($datos['detalles'] as $key=>$value) {
            $modificarRegistro2->execute(array(
                'fk_lgb013_num_adjudicacion'=>$value['pk_num_adjudicacion']
            ));
            $modificarRegistro3->execute(array(
                'pk_num_adjudicacion'=>$value['pk_num_adjudicacion']
            ));
        }

        $error = $modificarRegistro->errorInfo();
        $error2 = $modificarRegistro2->errorInfo();
        $error2 = $modificarRegistro3->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }elseif(!empty($error2[1]) && !empty($error2[2])){
            $this->_db->rollBack();
            return $error2;
        }elseif(!empty($error3[1]) && !empty($error3[2])){
            $this->_db->rollBack();
            return $error3;
        }else{
            $this->_db->commit();
            return $datos['idInfor'];
        }
    }
}

?>