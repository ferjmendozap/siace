<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/

require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'ordenCompra' . DS . 'ordenCompraModelo.php';
class confirmarServicioModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atOrdenCompraModelo = new ordenCompraModelo();
    }

    public function metListarConfirmaciones()
    {

        $ConfirmacionPost = $this->_db->query("
            SELECT
                orden.*,
                concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS proveedorNom,
                (
                SELECT SUM(detalle.num_cantidad) FROM
                lg_c009_orden_detalle AS detalle
                WHERE detalle.fk_lgb019_num_orden = orden.pk_num_orden
                ) AS cantidadDet,
                (
                SELECT SUM(servicio.num_cantidad) FROM
                lg_b021_confirmacion_servicio AS servicio
                WHERE servicio.fk_lgb019_num_orden = orden.pk_num_orden
                ) AS cantidadConfirm
            FROM
                lg_b019_orden AS orden
                INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = orden.fk_lgb014_num_almacen
                INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
                INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
                INNER JOIN lg_b017_clasificacion AS clasificacion ON clasificacion.pk_num_clasificacion = orden.fk_lgb017_num_clasificacion
                INNER JOIN a019_seguridad_dependencia AS seguridad ON seguridad.fk_a004_num_dependencia = orden.fk_a004_num_dependencia
                AND seguridad.fk_a018_num_seguridad_usuario = '".Session::metObtener('idUsuario')."' AND seguridad.fk_a015_num_seguridad_aplicacion = '3'
            WHERE
              (orden.ind_estado = 'AP'
              OR orden.ind_estado = 'CO')
              AND orden.ind_tipo_orden = 'OS'
              GROUP BY orden.pk_num_orden
          ");
        $ConfirmacionPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ConfirmacionPost->fetchAll();
    }

    public function metListarConfirmacionesDetalles($estado)
    {
        $ConfirmacionPost = $this->_db->query("
                SELECT
                    detalle.*,
                    commodity.ind_descripcion AS descripcion,
                    commodity.pk_num_commodity,
                    costo.ind_abreviatura,
                    servicio.pk_num_confirmacion
                FROM
                    lg_c009_orden_detalle AS detalle
                    LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = detalle.fk_lgb003_num_commodity
                    LEFT JOIN a023_centro_costo AS costo ON costo.pk_num_centro_costo = detalle.fk_a023_num_centro_costo
                    LEFT JOIN lg_b021_confirmacion_servicio AS servicio ON detalle.num_secuencia = servicio.num_secuencia AND
                    detalle.fk_lgb019_num_orden = servicio.fk_lgb019_num_orden
                    WHERE detalle.ind_estado = '$estado' OR detalle.ind_estado = 'PE'
                    GROUP BY detalle.pk_num_orden_detalle

          ");
        $ConfirmacionPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ConfirmacionPost->fetchAll();
    }

    public function metListarConfirmacionesDetallesRealizadas($idOrden)
    {
        $ConfirmacionPost = $this->_db->query("
                SELECT
                    servicio.*
                FROM
                    lg_b021_confirmacion_servicio AS servicio
                    WHERE servicio.fk_lgb019_num_orden = '$idOrden'
          ");
        $ConfirmacionPost->setFetchMode(PDO::FETCH_ASSOC);
        return $ConfirmacionPost->fetchAll();
    }


    public function metMostrarDetalle($idOrden,$secuencia)
    {
        $buscarOrden=$this->_db->query("
            SELECT
              detalle.*,
              (SELECT sum(servicio.num_cantidad) FROM lg_b021_confirmacion_servicio AS servicio WHERE
              fk_lgb019_num_orden = '$idOrden' AND servicio.num_secuencia = '$secuencia') AS recibida,
              concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS conformador,
              persona.ind_documento_fiscal
            FROM
              lg_c009_orden_detalle AS detalle
              INNER JOIN a018_seguridad_usuario AS usuario ON usuario.pk_num_seguridad_usuario = detalle.fk_a018_num_seguridad_usuario
              INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = usuario.fk_rhb001_num_empleado
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
            WHERE
              detalle.fk_lgb019_num_orden = '$idOrden' AND detalle.num_secuencia = '$secuencia'
        ");
        $buscarOrden->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarOrden->fetch();
    }

    public function metConfirmar($datos
    //    $cantidad,$secuencia,$num,$idOrden,$estado
    )
    {
        $this->_db->beginTransaction();
        $registroConfirmacion = $this->_db->prepare("
          INSERT INTO
            lg_b021_confirmacion_servicio
          SET
              fec_mes=:fec_mes,
              fec_anio=:fec_anio,
              ind_numero_confirmacion=:ind_numero_confirmacion,
              num_secuencia=:num_secuencia,
              num_cantidad=:num_cantidad,
              fec_confirmacion=NOW(),
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              fk_lgb019_num_orden=:fk_lgb019_num_orden,
              fk_rhb001_num_empleado_conforma='$this->atIdEmpleado'
           ");

        $registroConfirmacion->execute(array(
            'fec_mes'=>date('m'),
            'fk_lgb019_num_orden'=>$datos['idOrden'],
            'fec_anio'=>date('Y'),
            'ind_numero_confirmacion'=>$datos['orden'],
            'num_secuencia'=>$datos['secuencia'],
            'num_cantidad'=>$datos['recibir']
        ));
        $idRegistro= $this->_db->lastInsertId();

        $NuevoPostDetalle=$this->_db->prepare(
            "UPDATE
                lg_c009_orden_detalle
              SET
                ind_estado=:ind_estado,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
				WHERE fk_lgb019_num_orden=:fk_lgb019_num_orden AND num_secuencia=:num_secuencia
              ");

        $NuevoPostDetalle->execute(array(
            'ind_estado'=>$datos['estado'],
            'fk_lgb019_num_orden'=>$datos['idOrden'],
            'num_secuencia'=>$datos['secuencia']
        ));

        $NuevoPostDetalle2=$this->_db->prepare(
            "INSERT INTO
                lg_e005_documento
              SET
                fk_cpb001_num_documento_clasificacion=:fk_cpb001_num_documento_clasificacion, 
                fk_lgb019_num_orden=:fk_lgb019_num_orden, 
                fec_documento=NOW(), 
                num_anio=:num_anio, 
                num_tipo_documento='OS', 
                num_monto_afecto=:num_monto_afecto, 
                num_monto_no_afecto=:num_monto_no_afecto, 
                num_monto_impuesto=:num_monto_impuesto, 
                num_monto_total=:num_monto_total, 
                num_monto_pendiente=:num_monto_pendiente, 
                num_monto_pagado=:num_monto_pagado, 
                ind_estado=:ind_estado,
                ind_comentarios=:ind_comentarios, 
                fec_ultima_modificacion=NOW(), 
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");

        $NuevoPostDetalle2->execute(array(
            'fk_cpb001_num_documento_clasificacion'=>$datos['clasificacion'],
            'fk_lgb019_num_orden'=>$datos['idOrden'],
            'num_anio'=>$datos['datosOrden']['fec_anio'],
            'num_monto_afecto'=>$datos['datosOrden']['num_monto_afecto'],
            'num_monto_no_afecto'=>$datos['datosOrden']['num_monto_no_afecto'],
            'num_monto_impuesto'=>$datos['datosOrden']['num_monto_igv'],
            'num_monto_total'=>$datos['datosOrden']['num_monto_total'],
            'num_monto_pendiente'=>$datos['datosOrden']['num_monto_pendiente'],
            'num_monto_pagado'=>$datos['datosOrden']['num_monto_pagado'],
            'ind_estado'=>'PR',
            'ind_comentarios'=>$datos['datosOrden']['ind_comentario']
        ));
        $idRegistro2= $this->_db->lastInsertId();

        $NuevoPostDetalle3=$this->_db->prepare(
            "INSERT INTO
                lg_e006_documento_detalle
              SET
                num_secuencia=:num_secuencia, 
                fk_lge005_num_documento=:fk_lge005_num_documento, 
                fk_lgb021_num_confirmacion_servicio=:fk_lgb021_num_confirmacion_servicio, 
                fk_lgc009_num_orden_detalle=:fk_lgc009_num_orden_detalle, 
                num_cantidad=:num_cantidad, 
                num_precio_unit=:num_precio_unit,
                num_precio_cantidad=:num_precio_cantidad,
                num_total=:num_total,
                fec_ultima_modificacion=NOW(), 
                fk_a018_num_seguridad_usuario='$this->atIdUsuario'
              ");
        foreach ($datos['datosOrdenDetalle'] as $key2=>$value2) {
            if($value2['num_secuencia']==$datos['secuencia']){
                $NuevoPostDetalle3->execute(array(
                    'num_secuencia'=>$value2['num_secuencia'],
                    'fk_lge005_num_documento'=>$idRegistro2,
                    'fk_lgc009_num_orden_detalle'=>$value2['pk_num_orden_detalle'],
                    'fk_lgb021_num_confirmacion_servicio'=>$idRegistro,
                    'num_cantidad'=>$value2['num_cantidad'],
                    'num_precio_unit'=>$value2['num_precio_unitario'],
                    'num_precio_cantidad'=>$value2['num_monto_base'],
                    'num_total'=>$value2['num_total']
                ));
            }
        }



        $fallaTansaccion = $registroConfirmacion->errorInfo();
        $fallaTansaccion2 = $NuevoPostDetalle2->errorInfo();
        $fallaTansaccion3 = $NuevoPostDetalle3->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }elseif(!empty($fallaTansaccion2[1]) && !empty($fallaTansaccion2[2])){
            $this->_db->rollBack();
            return $fallaTansaccion2;
        }elseif(!empty($fallaTansaccion3[1]) && !empty($fallaTansaccion3[2])){
            $this->_db->rollBack();
            return $fallaTansaccion3;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metContarOrdenes($idOrden)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              count(*) AS cantidad
            FROM
            lg_c009_orden_detalle
            WHERE
            fk_lgb019_num_orden='$idOrden' AND
            ind_estado='PE'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }


    public function metBuscarClasificacionCxP($codigo)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
              cp_b001_documento_clasificacion
            WHERE
              ind_documento_clasificacion='$codigo'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

    public function metDesconfirmar($idOrden,$sec,$permiso,$idConfirmacion){
        $this->_db->beginTransaction();
        $eliminarDco2=$this->_db->prepare("
            DELETE FROM
            lg_e006_documento_detalle
            WHERE
            fk_lgb021_num_confirmacion_servicio=:fk_lgb021_num_confirmacion_servicio AND
            num_secuencia=:num_secuencia
            ");
        $eliminarDco2->execute(array(
            'fk_lgb021_num_confirmacion_servicio'=>$idConfirmacion,
            'num_secuencia'=>$sec
        ));

        $eliminarDco1=$this->_db->prepare("
            DELETE FROM
            lg_e005_documento
            WHERE
            fk_lgb019_num_orden=:fk_lgb019_num_orden
            ");
        $eliminarDco1->execute(array(
            'fk_lgb019_num_orden'=>$idOrden
        ));

        $eliminaridConfirmacion=$this->_db->prepare("
            DELETE FROM
            lg_b021_confirmacion_servicio
            WHERE
            fk_lgb019_num_orden=:fk_lgb019_num_orden AND
            num_secuencia=:num_secuencia
            ");
        $eliminaridConfirmacion->execute(array(
            'fk_lgb019_num_orden'=>$idOrden,
            'num_secuencia'=>$sec
        ));

        $NuevoPostDetalle=$this->_db->prepare(
            "UPDATE
                lg_b019_orden
              SET
                ind_estado=:ind_estado,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
				WHERE pk_num_orden=:pk_num_orden
              ");
        if($permiso!=0){
            $NuevoPostDetalle->execute(array(
                'ind_estado'=>'CO',
                'pk_num_orden'=>$idOrden
            ));
        }

        $NuevoPostDetalle=$this->_db->prepare(
            "UPDATE
                lg_c009_orden_detalle
              SET
                ind_estado=:ind_estado,
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
				WHERE fk_lgb019_num_orden='$idOrden' AND num_secuencia='$sec'
              ");

        $NuevoPostDetalle->execute(array(
            'ind_estado'=>'PE'
        ));









        $error = $eliminaridConfirmacion->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idOrden;
        }
    }

    public function metBuscarEstadoRegistro($id,$sec)
    {
        $generarOrdenPost = $this->_db->query("
            SELECT
              *
            FROM
              lg_b019_orden AS orden
            INNER JOIN lg_b021_confirmacion_servicio AS servicio ON servicio.fk_lgb019_num_orden = orden.pk_num_orden
            INNER JOIN lg_c009_orden_detalle AS detalle ON detalle.fk_lgb019_num_orden = orden.pk_num_orden AND servicio.num_secuencia = detalle.num_secuencia
            WHERE
              orden.pk_num_orden = '$id'
              AND detalle.num_secuencia = '$sec'
              ");
        $generarOrdenPost->setFetchMode(PDO::FETCH_ASSOC);
        return $generarOrdenPost->fetch();
    }

}

?>