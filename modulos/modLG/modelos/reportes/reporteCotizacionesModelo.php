<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'itemsModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'commodityModelo.php';
class reporteCotizacionesModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atItemsModelo = new itemsModelo();
        $this->atCommodityModelo = new commodityModelo();
    }

    public function metListarCotizaciones($filtro=false){

        $actaPost =  $this->_db->query("
            SELECT
              *
            FROM lg_c003_cotizacion AS cotizacion
            INNER JOIN lg_b023_acta_detalle AS detalle ON detalle.pk_num_acta_detalle = cotizacion.fk_lgb023_num_acta_detalle
            INNER JOIN lg_b010_invitacion AS invitacion ON cotizacion.fk_lgb010_num_invitacion = invitacion.pk_num_invitacion
            INNER JOIN lg_b022_proveedor AS proveedor ON invitacion.fk_lgb022_num_proveedor = proveedor.pk_num_proveedor
            INNER JOIN a003_persona AS persona ON proveedor.fk_a003_num_persona_proveedor = persona.pk_num_persona
            $filtro
        ");

        $actaPost->setFetchMode(PDO::FETCH_ASSOC);
        return $actaPost->fetchAll();
    }

}

?>