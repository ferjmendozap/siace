<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0291-6531198      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'clasificacionModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'almacenModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'unidadesModelo.php';
class commodityModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atClasificacionModelo = new clasificacionModelo();
        $this->atAlmacenModelo = new almacenModelo();
        $this->atUnidadesModelo = new unidadesModelo();
    }

    public function metListarPartida()
    {
        $personaPost = $this->_db->query("
            SELECT
              presupuestaria.*,
              cuenta1.ind_descripcion AS cuenta1,
              cuenta1.cod_cuenta AS codCuenta1,
              cuenta2.ind_descripcion AS cuenta2,
              cuenta2.cod_cuenta AS codCuenta2,
              cuenta3.ind_descripcion AS cuenta3,
              cuenta3.cod_cuenta AS codCuenta3
            FROM
              pr_b002_partida_presupuestaria AS presupuestaria
               LEFT JOIN cb_b004_plan_cuenta AS cuenta1 ON cuenta1.pk_num_cuenta = fk_cbb004_num_plan_cuenta_onco
               LEFT JOIN cb_b004_plan_cuenta AS cuenta2 ON cuenta2.pk_num_cuenta = fk_cbb004_num_plan_cuenta_pub20
               LEFT JOIN cb_b004_plan_cuenta AS cuenta3 ON cuenta3.pk_num_cuenta = fk_cbb004_num_plan_cuenta_gastopub20
            WHERE
              presupuestaria.num_estatus='1'
              ");
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        return $personaPost ->fetchAll();
    }

    public function metListarPlanCuenta()
    {
        $personaPost = $this->_db->query("
            SELECT
              *
            FROM
              cb_b004_plan_cuenta
              ");
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        return $personaPost ->fetchAll();
    }

    public function metBuscarCodigo($idPartida)
    {
        $personaPost = $this->_db->query("
            SELECT
              count(*) AS cantidad
            FROM
              lg_b003_commodity
              WHERE fk_prb002_num_partida_presupuestaria = '$idPartida'
              ");
        $personaPost ->setFetchMode(PDO::FETCH_ASSOC);
        return $personaPost ->fetch();
    }

    public function metListarCommodity()
    {
        $commodityPost = $this->_db->query(
            "SELECT
              commodity.*,
              cuenta.pk_num_cuenta,
              cuenta.cod_cuenta,
              cuenta.ind_descripcion AS descripcionCuenta,
              presupuestaria.pk_num_partida_presupuestaria,
              presupuestaria.cod_partida,
              presupuestaria.ind_denominacion AS descripcionPartida,
              unidad.ind_descripcion AS unidad,
              unidad.pk_num_unidad AS idUnidad,
              stock.num_cantidad
              FROM lg_b003_commodity AS commodity
              LEFT JOIN lg_c008_commodity_stock AS stock ON commodity.pk_num_commodity = stock.fk_lgb003_num_commodity
              LEFT JOIN lg_b004_unidades AS unidad ON unidad.pk_num_unidad = commodity.fk_lgb004_num_unidad
              LEFT JOIN cb_b004_plan_cuenta AS cuenta ON cuenta.pk_num_cuenta = commodity.fk_cbb004_num_plan_cuenta
              LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria ON commodity.fk_prb002_num_partida_presupuestaria = presupuestaria.pk_num_partida_presupuestaria
              GROUP BY commodity.pk_num_commodity
          ");
        $commodityPost->setFetchMode(PDO::FETCH_ASSOC);
        return $commodityPost->fetchAll();
    }

    public function metMostrarCommodity($idCommodity){
        $mostrarCommodity = $this->_db->query("
            SELECT
                c.*,
                s.ind_usuario,
                concat_ws(' ',presupuestaria.cod_partida,presupuestaria.ind_denominacion) AS nombrePartida,
                presupuestaria.pk_num_partida_presupuestaria AS idPartida,
                presupuestaria.cod_partida AS codPartida,
                presupuestaria.ind_denominacion AS descPartida,
                concat_ws(' ',cuenta1.cod_cuenta,cuenta1.ind_descripcion) AS nombreCuenta1,
                cuenta1.pk_num_cuenta AS idCuenta,
                cuenta1.cod_cuenta AS codCuenta,
                cuenta1.ind_descripcion AS descCuenta,
                concat_ws(' ',cuenta2.cod_cuenta,cuenta2.ind_descripcion) AS nombreCuenta2,
                unidad.ind_descripcion AS unidad,
                unidad.pk_num_unidad AS idUnidad,
                persona.*
            FROM
                lg_b003_commodity AS c
                INNER JOIN a018_seguridad_usuario AS s ON c.fk_a018_num_seguridad_usuario = s.pk_num_seguridad_usuario
                LEFT JOIN pr_b002_partida_presupuestaria AS presupuestaria ON presupuestaria.pk_num_partida_presupuestaria = c.fk_prb002_num_partida_presupuestaria
                LEFT JOIN cb_b004_plan_cuenta AS cuenta1 ON cuenta1.pk_num_cuenta = c.fk_cbb004_num_plan_cuenta
                LEFT JOIN cb_b004_plan_cuenta AS cuenta2 ON cuenta2.pk_num_cuenta = c.fk_cbb004_num_plan_cuenta_pub_veinte
                INNER JOIN lg_b004_unidades AS unidad ON c.fk_lgb004_num_unidad = unidad.pk_num_unidad
                LEFT JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = c.fk_rhb001_num_empleado_verifica_presupuesto
                LEFT JOIN a003_persona AS persona ON persona.pk_num_persona = empleado.fk_a003_num_persona
                WHERE c.pk_num_commodity = '$idCommodity'
        ");
        $mostrarCommodity->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarCommodity->fetch();
    }

    public function metCrearCommodity($descComm,$codigo,$presCommodity,$estadoCommodity,$partidaComm,$planComm,$plan20Comm,$unidadComm,$almComm,$clasificacion)
    {
        $this->_db->beginTransaction();

        if($presCommodity==1){

            $registroCommodity = $this->_db->prepare("
          INSERT INTO
          lg_b003_commodity
          SET
          ind_cod_commodity=:ind_cod_commodity,
          ind_descripcion=:ind_descripcion,
          fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
          fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
          fk_cbb004_num_plan_cuenta_pub_veinte=:fk_cbb004_num_plan_cuenta_pub_veinte,
          num_estatus=:num_estatus,
          num_flag_vericado_presupuesto=:num_flag_vericado_presupuesto,
          fk_rhb001_num_empleado_verifica_presupuesto='$this->atIdEmpleado',
          fec_verificado_presupuesto=NOW(),
          fk_a018_num_seguridad_usuario='$this->atIdUsuario',
          fec_ultima_modificacion=NOW(),
          fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
          fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
          fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion
          ");
        } else {

            $registroCommodity = $this->_db->prepare("
          INSERT INTO
          lg_b003_commodity
          SET
          ind_cod_commodity=:ind_cod_commodity,
          ind_descripcion=:ind_descripcion,
          fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
          fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
          fk_cbb004_num_plan_cuenta_pub_veinte=:fk_cbb004_num_plan_cuenta_pub_veinte,
          num_estatus=:num_estatus,
          num_flag_vericado_presupuesto=:num_flag_vericado_presupuesto,
          fk_a018_num_seguridad_usuario='$this->atIdUsuario',
          fec_ultima_modificacion=NOW(),
          fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
          fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
          fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion
          ");
        }
        $registroCommodity->execute(array(
            'ind_cod_commodity'=>$codigo,
            'ind_descripcion'=>$descComm,
            'fk_prb002_num_partida_presupuestaria'=>$partidaComm,
            'fk_cbb004_num_plan_cuenta'=>$planComm,
            'fk_cbb004_num_plan_cuenta_pub_veinte'=>$plan20Comm,
            'num_estatus'=>$estadoCommodity,
            'num_flag_vericado_presupuesto'=>$presCommodity,
            'fk_lgb004_num_unidad'=>$unidadComm,
            'fk_lgb014_num_almacen'=>$almComm,
            'fk_lgb017_num_clasificacion'=>$clasificacion
        ));

        $idRegistro= $this->_db->lastInsertId();
        $registroCommodity = $this->_db->prepare("
            INSERT INTO
                lg_c008_commodity_stock
            SET
                num_stock_inicial=0, 
                num_cantidad=0, 
                fec_mes=:fec_mes, 
                fec_anio=:fec_anio, 
                num_precio_unitario=0, 
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW(),
                fk_rhb001_num_empleado_ingresadopor='$this->atIdEmpleado',
                fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
                fk_lgb003_num_commodity=$idRegistro
          ");

        $registroCommodity->execute(array(
            'fec_mes'=>date('m'),
            'fec_anio'=>date('Y'),
            'fk_lgb014_num_almacen'=>$almComm
        ));

        $fallaTansaccion = $registroCommodity->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metModificarCommodity($descComm,$presCommodity,$estadoCommodity,$partidaComm,$planComm,$plan20Comm,$unidadComm,$almComm,$clasificacion,$idCommodity)
    {
        $this->_db->beginTransaction();
        if($presCommodity==1){
        $modificarRegistro=$this->_db->prepare("
              UPDATE
                 lg_b003_commodity
              SET
                  ind_descripcion=:ind_descripcion,
                  fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                  fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                  fk_cbb004_num_plan_cuenta_pub_veinte=:fk_cbb004_num_plan_cuenta_pub_veinte,
                  num_estatus=:num_estatus,
                  num_flag_vericado_presupuesto=:num_flag_vericado_presupuesto,
                  fk_rhb001_num_empleado_verifica_presupuesto='$this->atIdEmpleado',
                  fec_verificado_presupuesto=NOW(),
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW(),
                  fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
                  fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
                  fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion
              WHERE
                  pk_num_commodity='$idCommodity'
            ");
        } else {
            $modificarRegistro=$this->_db->prepare("
              UPDATE
                 lg_b003_commodity
              SET
                  ind_descripcion=:ind_descripcion,
                  fk_prb002_num_partida_presupuestaria=:fk_prb002_num_partida_presupuestaria,
                  fk_cbb004_num_plan_cuenta=:fk_cbb004_num_plan_cuenta,
                  fk_cbb004_num_plan_cuenta_pub_veinte=:fk_cbb004_num_plan_cuenta_pub_veinte,
                  num_estatus=:num_estatus,
                  num_flag_vericado_presupuesto=:num_flag_vericado_presupuesto,
                  fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                  fec_ultima_modificacion=NOW(),
                  fk_lgb004_num_unidad=:fk_lgb004_num_unidad,
                  fk_lgb014_num_almacen=:fk_lgb014_num_almacen,
                  fk_lgb017_num_clasificacion=:fk_lgb017_num_clasificacion
              WHERE
                  pk_num_commodity='$idCommodity'
            ");
        }
        $modificarRegistro->execute(array(
            'ind_descripcion'=>$descComm,
            'fk_prb002_num_partida_presupuestaria'=>$partidaComm,
            'fk_cbb004_num_plan_cuenta'=>$planComm,
            'fk_cbb004_num_plan_cuenta_pub_veinte'=>$plan20Comm,
            'num_estatus'=>$estadoCommodity,
            'num_flag_vericado_presupuesto'=>$presCommodity,
            'fk_lgb004_num_unidad'=>$unidadComm,
            'fk_lgb014_num_almacen'=>$almComm,
            'fk_lgb017_num_clasificacion'=>$clasificacion
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCommodity;
        }
    }

    public function metEliminarCommodity($idCommodity){
        $this->_db->beginTransaction();
        $eliminarCommodity=$this->_db->prepare("
            DELETE FROM
            lg_c008_commodity_stock
            WHERE
            fk_lgb003_num_commodity=:fk_lgb003_num_commodity
            ");
        $eliminarCommodity->execute(array(
            'fk_lgb003_num_commodity'=>$idCommodity
        ));
        $eliminarCommodity=$this->_db->prepare("
            DELETE FROM
            lg_b003_commodity
            WHERE
            pk_num_commodity=:pk_num_commodity
            ");
        $eliminarCommodity->execute(array(
            'pk_num_commodity'=>$idCommodity
        ));

        $error = $eliminarCommodity->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idCommodity;
        }
    }

}

?>