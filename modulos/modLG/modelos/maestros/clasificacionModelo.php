<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'almacenModelo.php';
class clasificacionModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoTipoReq = new miscelaneoModelo();
        $this->atAlmacenModelo = new almacenModelo();
    }

    public function metListarClasificacion($idClasificacion = false,$cual = false)
    {
        if($idClasificacion){
            $where = "WHERE cla.pk_num_clasificacion = '$idClasificacion'";
        } else if($cual){
            $where = "WHERE cla.ind_descripcion <> '$cual'";
        } else {
            $where = "";
        }
        $ClasificacionPost = $this->_db->query(
            "SELECT
              cla.*,
              req.ind_nombre_detalle,
              almacen.num_flag_commodity
              FROM lg_b017_clasificacion AS cla
              INNER JOIN a006_miscelaneo_detalle AS req ON req.pk_num_miscelaneo_detalle = cla.fk_a006_num_miscelaneos_tipo_requerimiento
              LEFT JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = cla.fk_lgb014_num_almacen
              $where
              ORDER BY cla.pk_num_clasificacion
          ");
        $ClasificacionPost->setFetchMode(PDO::FETCH_ASSOC);
        if($idClasificacion){
            return $ClasificacionPost->fetch();
        } else {
            return $ClasificacionPost->fetchAll();
        }
    }

    public function metMostrarClasificacion($idClasificacion){
        $mostrarClasificacion = $this->_db->query("
              SELECT
              cla.*,
              req.pk_num_miscelaneo_detalle,
              req.ind_nombre_detalle,
              concat_ws(' ',persona.ind_nombre1,ind_apellido1) AS usuario
              FROM lg_b017_clasificacion AS cla
              LEFT JOIN a006_miscelaneo_detalle AS req ON req.pk_num_miscelaneo_detalle = cla.fk_a006_num_miscelaneos_tipo_requerimiento
              INNER JOIN a018_seguridad_usuario AS usuario ON usuario.pk_num_seguridad_usuario = cla.fk_a018_num_seguridad_usuario
              INNER JOIN rh_b001_empleado AS empleado ON empleado.pk_num_empleado = usuario.fk_rhb001_num_empleado
              INNER JOIN a003_persona AS persona ON empleado.fk_a003_num_persona = persona.pk_num_persona
              WHERE
                cla.pk_num_clasificacion='$idClasificacion'
        ");
        $mostrarClasificacion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarClasificacion->fetch();
    }

    public function metCrearClasificacion($desc,$codigo,$tipoReq,$recAlm,$revision,$transaccion,$cajaChica,$estatus,$activo,$reposicion,$almacen)
    {
        $this->_db->beginTransaction();
        $registroClasificacion = $this->_db->prepare("
          INSERT INTO
            lg_b017_clasificacion
          SET
              ind_descripcion=:ind_descripcion,
              ind_cod_clasificacion=:ind_cod_clasificacion,
              fk_a006_num_miscelaneos_tipo_requerimiento=:fk_a006_num_miscelaneos_tipo_requerimiento,
              num_flag_recepcion_almacen=:num_flag_recepcion_almacen,
              num_flag_revision=:num_flag_revision,
              num_flag_transaccion=:num_flag_transaccion,
              num_flag_caja_chica=:num_flag_caja_chica,
              num_estatus=:num_estatus,
              num_flag_activo_fijo=:num_flag_activo_fijo,
              num_flag_reposicion=:num_flag_reposicion,
              fk_a018_num_seguridad_usuario='$this->atIdUsuario',
              fec_ultima_modificacion=NOW(),
              fk_lgb014_num_almacen=:fk_lgb014_num_almacen
           ");

        $registroClasificacion->execute(array(
            'ind_descripcion'=>$desc,
            'ind_cod_clasificacion'=>$codigo,
            'fk_a006_num_miscelaneos_tipo_requerimiento'=>$tipoReq,
            'num_flag_recepcion_almacen'=>$recAlm,
            'num_flag_revision'=>$revision,
            'num_flag_transaccion'=>$transaccion,
            'num_flag_caja_chica'=>$cajaChica,
            'num_estatus'=>$estatus,
            'num_flag_activo_fijo'=>$activo,
            'num_flag_reposicion'=>$reposicion,
            'fk_lgb014_num_almacen'=>$almacen
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $registroClasificacion->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarClasificacion($desc,$codigo,$tipoReq,$recAlm,$revision,$transaccion,$cajaChica,$estatus,$activo,$reposicion,$almacen,$idClasificacion)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->prepare("
              UPDATE
                lg_b017_clasificacion
              SET
               ind_descripcion=:ind_descripcion,
               ind_cod_clasificacion=:ind_cod_clasificacion,
               fk_a006_num_miscelaneos_tipo_requerimiento=:fk_a006_num_miscelaneos_tipo_requerimiento,
               num_flag_recepcion_almacen=:num_flag_recepcion_almacen,
               num_flag_revision=:num_flag_revision,
               num_flag_transaccion=:num_flag_transaccion,
               num_flag_caja_chica=:num_flag_caja_chica,
               num_estatus=:num_estatus,
               num_flag_activo_fijo=:num_flag_activo_fijo,
              num_flag_reposicion=:num_flag_reposicion,
               fk_a018_num_seguridad_usuario='$this->atIdUsuario',
               fec_ultima_modificacion=NOW(),
               fk_lgb014_num_almacen=:fk_lgb014_num_almacen
              WHERE
                pk_num_clasificacion='$idClasificacion'
            ");
        $modificarRegistro->execute(array(
            'ind_descripcion'=>$desc,
            'ind_cod_clasificacion'=>$codigo,
            'fk_a006_num_miscelaneos_tipo_requerimiento'=>$tipoReq,
            'num_flag_recepcion_almacen'=>$recAlm,
            'num_flag_revision'=>$revision,
            'num_flag_transaccion'=>$transaccion,
            'num_flag_caja_chica'=>$cajaChica,
            'num_estatus'=>$estatus,
            'num_flag_activo_fijo'=>$activo,
            'num_flag_reposicion'=>$reposicion,
            'fk_lgb014_num_almacen'=>$almacen
        ));
        $error = $modificarRegistro->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idClasificacion;
        }
    }

    public function metEliminarClasificacion($idClasificacion){
        $this->_db->beginTransaction();
        $eliminarClas=$this->_db->prepare("
            DELETE FROM
            lg_b017_clasificacion
            WHERE
            pk_num_clasificacion=:pk_num_clasificacion
            ");
        $eliminarClas->execute(array(
            'pk_num_clasificacion'=>$idClasificacion
        ));

        $error = $eliminarClas->errorInfo();
        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idClasificacion;
        }
    }

}

?>