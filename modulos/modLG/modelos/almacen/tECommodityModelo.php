<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'almacen' . DS . 'despachoModelo.php';
class tECommodityModelo extends Modelo
{
    private $atIdUsuario;
    private $atIdEmpleado;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atDespachoModelo = new despachoModelo();
    }

    public function metListarOrdenes()
    {
        $tranPost = $this->_db->query("
            SELECT
              orden.*,
              concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS proveedor,
              detalle.ind_nombre_detalle AS formaPago,
              clasificacion.ind_descripcion AS clasificacion,
              transaccion.pk_num_transaccion
            FROM
              lg_b019_orden AS orden
              INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = orden.fk_lgb014_num_almacen
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
              INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = orden.fk_a006_num_miscelaneos_forma_pago
              INNER JOIN lg_b017_clasificacion AS clasificacion ON clasificacion.pk_num_clasificacion = orden.fk_lgb017_num_clasificacion
              INNER JOIN lg_b020_control_perceptivo AS perceptivo ON perceptivo.fk_lgb019_num_orden = orden.pk_num_orden
              LEFT JOIN lg_d001_transaccion AS transaccion ON (
              transaccion.ind_num_documento_referencia = orden.ind_orden
              AND orden.fec_anio = transaccion.fec_anio)
            WHERE 
              almacen.num_flag_commodity = '1' AND 
              clasificacion.ind_descripcion != 'SERVICIOS' AND 
              orden.ind_estado = 'AP' AND 
              perceptivo.num_estatus = '0'
            GROUP BY orden.pk_num_orden
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }

    public function metListarUbicacionesAF()
    {
        $tranPost = $this->_db->query("
            SELECT
              *
            FROM
              af_c005_ubicacion 
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }

    public function metListarEgresos()
    {
        $tranPost = $this->_db->query("
            SELECT
              orden.*,
              concat_ws(' ',persona.ind_nombre1,persona.ind_apellido1) AS proveedor,
              detalle.ind_nombre_detalle AS formaPago,
              clasificacion.ind_descripcion AS clasificacion,
              transaccion.pk_num_transaccion,
              transaccion.num_estatus AS estadoTran
            FROM
              lg_b019_orden AS orden
              INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = orden.fk_lgb014_num_almacen
              INNER JOIN lg_b022_proveedor AS proveedor ON proveedor.pk_num_proveedor = orden.fk_lgb022_num_proveedor
              INNER JOIN a003_persona AS persona ON persona.pk_num_persona = proveedor.fk_a003_num_persona_proveedor
              INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = orden.fk_a006_num_miscelaneos_forma_pago
              INNER JOIN lg_b017_clasificacion AS clasificacion ON clasificacion.pk_num_clasificacion = orden.fk_lgb017_num_clasificacion
              LEFT JOIN lg_d001_transaccion AS transaccion ON (
              transaccion.ind_num_documento_referencia = orden.ind_orden
              AND orden.fec_anio = transaccion.fec_anio)
              WHERE almacen.num_flag_commodity = '1' AND orden.ind_estado = 'AP'
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }

    public function metActualizarEstadoDet($idOrdenDet,$estado,$reqDet=false)
    {
        $this->_db->beginTransaction();

        $NuevoPost=$this->_db->prepare("
        UPDATE
            lg_c009_orden_detalle
        SET
            ind_estado='$estado',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_orden_detalle=:pk_num_orden_detalle
        ");
        $NuevoPost->execute(array(
            'pk_num_orden_detalle'=>$idOrdenDet
        ));

        $NuevoPost2=$this->_db->prepare("
        UPDATE
            lg_c001_requerimiento_detalle
        SET
            ind_estado='$estado',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_requerimiento_detalle=:pk_num_requerimiento_detalle
        ");

        $NuevoPost3=$this->_db->prepare("
        UPDATE
            lg_b001_requerimiento
        SET
            ind_estado='$estado',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_requerimiento=:pk_num_requerimiento
        ");
        if($reqDet){
            foreach ($reqDet as $item=>$value) {
                $NuevoPost2->execute(array(
                    'pk_num_requerimiento_detalle'=>$value['fk_lgc001_num_requerimiento_detalle']
                ));
                $NuevoPost3->execute(array(
                    'pk_num_requerimiento'=>$value['pk_num_requerimiento']
                ));
            }
        }
        $fallaTansaccion = $NuevoPost->errorInfo();


        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idOrdenDet;
        }
    }

    public function metActualizarStock($cantidad,$item,$cual)
    {
        if($cual==1) {
            $set = "num_cantidad=(num_cantidad + :num_cantidad),";
        } elseif ($cual==2){
            $set = "num_cantidad=(num_cantidad - :num_cantidad),";
        }

        $this->_db->beginTransaction();
        $NuevoPost=$this->_db->prepare("
        UPDATE
            lg_c008_commodity_stock
        SET
            $set
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            fk_lgb003_num_commodity=:fk_lgb003_num_commodity
        ");

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            'num_cantidad'=>$cantidad,
            'fk_lgb003_num_commodity'=>$item
        ));
        $fallaTansaccion1 = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion1[1]) && !empty($fallaTansaccion1[2])){
            $this->_db->rollBack();
            return $fallaTansaccion1;
        }else{
            $this->_db->commit();
            return true;
        }
    }

    public function metListarOrdenDet()
    {
        #"#el 2 es el tipo de transaccion recepcion de o/c
        $tranPost = $this->_db->query("
            SELECT
                detalle1.pk_num_orden_detalle,
                detalle1.num_cantidad,
                detalle1.fk_lgb019_num_orden,
                detalle1.ind_estado,
                (SELECT 
                    SUM(detalle2.num_cantidad_transaccion) 
                 FROM 
                    lg_d002_transaccion_detalle AS detalle2 
                 INNER JOIN lg_d001_transaccion AS transaccion ON 
                 transaccion.pk_num_transaccion = detalle2.fk_lgd001_num_transaccion
                 INNER JOIN lg_b015_tipo_transaccion AS tipo ON 
                 transaccion.fk_lgb015_num_tipo_transaccion = tipo.pk_num_tipo_transaccion
                WHERE 
                    detalle1.pk_num_orden_detalle = detalle2.fk_lgc009_num_orden_detalle 
                    AND tipo.ind_cod_tipo_transaccion = 'ROC') 
                AS cantidad_recibida ,
                (SELECT 
                    SUM(detalle2.num_cantidad_transaccion) 
                 FROM 
                    lg_d002_transaccion_detalle AS detalle2 
                 INNER JOIN lg_d001_transaccion AS transaccion ON 
                 transaccion.pk_num_transaccion = detalle2.fk_lgd001_num_transaccion
                 INNER JOIN lg_b015_tipo_transaccion AS tipo ON 
                 transaccion.fk_lgb015_num_tipo_transaccion = tipo.pk_num_tipo_transaccion
                WHERE 
                    detalle1.pk_num_orden_detalle = detalle2.fk_lgc009_num_orden_detalle 
                    AND tipo.ind_cod_tipo_transaccion = 'DOC') 
                AS cantidad_despachada
            FROM
                lg_c009_orden_detalle AS detalle1 
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }

    public function metBuscarReqDet($id)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              detalle2.*,
              requerimiento.pk_num_requerimiento
            FROM
              lg_b023_acta_detalle AS detalle 
              INNER JOIN lg_e009_acta_detalle_requerimiento_detalle AS detalle2 ON detalle.pk_num_acta_detalle = detalle2.fk_lgb023_num_acta_detalle
              INNER JOIN lg_c009_orden_detalle AS detalle3 ON detalle3.fk_lgb023_num_acta_detalle = detalle.pk_num_acta_detalle
              INNER JOIN lg_c001_requerimiento_detalle AS detalle4 ON detalle2.fk_lgc001_num_requerimiento_detalle = detalle4.pk_num_requerimiento_detalle
              INNER JOIN lg_b001_requerimiento AS requerimiento ON requerimiento.pk_num_requerimiento = detalle4.fk_lgb001_num_requerimiento
            WHERE
              detalle3.fk_lgb019_num_orden = '$id'
              ORDER BY detalle.fk_lgb002_num_item,detalle.fk_lgb003_num_commodity ASC
              ");
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetchAll();
    }

    public function metMostrarTransaccion($nroOrden,$anio)
    {
        $partidaCuenta = $this->_db->query("
            SELECT
              transaccion.ind_nota_entrega_factura
            FROM
              lg_d001_transaccion AS transaccion
              INNER JOIN lg_b015_tipo_transaccion AS tipo ON transaccion.fk_lgb015_num_tipo_transaccion = tipo.pk_num_tipo_transaccion
              INNER JOIN a006_miscelaneo_detalle AS detalle ON tipo.fk_a006_num_miscelaneo_detalle_tipo_documento = detalle.pk_num_miscelaneo_detalle
              WHERE 
              transaccion.ind_num_documento_referencia = '$nroOrden' AND 
              transaccion.fec_anio = '$anio' AND 
              detalle.cod_detalle = 'I'
              ");
        $partidaCuenta->setFetchMode(PDO::FETCH_ASSOC);
        return $partidaCuenta->fetch();
    }

    public function metActualizarOrden($idOrden)
    {
        $this->_db->beginTransaction();

        $NuevoPost=$this->_db->prepare("
        UPDATE
            lg_b019_orden
        SET
            fk_rhb001_num_empleado_aprueba_despacho='$this->atIdEmpleado',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_orden=:pk_num_orden
        ");
        $NuevoPost->execute(array(
            'pk_num_orden'=>$idOrden
        ));
        $fallaTansaccion = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idOrden;
        }
    }
}

?>