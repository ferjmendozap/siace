<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'almacen' . DS . 'despachoModelo.php';
class tECajaChicaModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atDespachoModelo = new despachoModelo();
    }

    public function metListarReq()
    {
        $tranPost = $this->_db->query("
            SELECT
              requerimiento.*,
              costo.ind_descripcion_centro_costo,
              almacen.ind_descripcion AS almacen
            FROM
              lg_b001_requerimiento AS requerimiento
              INNER JOIN a023_centro_costo AS costo ON costo.pk_num_centro_costo = requerimiento.fk_a023_num_centro_costo
              INNER JOIN lg_b014_almacen AS almacen ON almacen.pk_num_almacen = requerimiento.fk_lgb014_num_almacen
              WHERE requerimiento.ind_verificacion_caja_chica = '1' AND requerimiento.ind_estado = 'AP'
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }

    public function metListarReqDet()
    {
        #"#el 3 es el tipo de transaccion recepcion de caja chica
        $tranPost = $this->_db->query("
            SELECT
                detalle1.pk_num_requerimiento_detalle,
                detalle1.num_cantidad_pedida,
                detalle1.fk_lgb001_num_requerimiento,
                detalle1.ind_estado,
                (SELECT 
                    SUM(detalle2.num_cantidad_transaccion) 
                 FROM 
                    lg_d002_transaccion_detalle AS detalle2 
                 INNER JOIN lg_d001_transaccion AS transaccion ON 
                 transaccion.pk_num_transaccion = detalle2.fk_lgd001_num_transaccion
                 INNER JOIN lg_b015_tipo_transaccion AS tipo ON transaccion.fk_lgb015_num_tipo_transaccion = tipo.pk_num_tipo_transaccion
                WHERE 
                    detalle1.pk_num_requerimiento_detalle = detalle2.fk_lgc001_num_requerimiento_detalle 
                    AND tipo.ind_cod_tipo_transaccion = 'RCJ') 
                AS cantidad_recibida,
                (SELECT 
                    SUM(detalle2.num_cantidad_transaccion) 
                 FROM 
                    lg_d002_transaccion_detalle AS detalle2 
                 INNER JOIN lg_d001_transaccion AS transaccion ON 
                 transaccion.pk_num_transaccion = detalle2.fk_lgd001_num_transaccion
                 INNER JOIN lg_b015_tipo_transaccion AS tipo ON transaccion.fk_lgb015_num_tipo_transaccion = tipo.pk_num_tipo_transaccion
                WHERE 
                    detalle1.pk_num_requerimiento_detalle = detalle2.fk_lgc001_num_requerimiento_detalle 
                    AND tipo.ind_cod_tipo_transaccion = 'DRC') 
                AS cantidad_despachada 
            FROM
                lg_c001_requerimiento_detalle AS detalle1 
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }


    public function metActualizarTransaccion($idTran)
    {
        $this->_db->beginTransaction();
        $NuevoPost=$this->_db->prepare("
            UPDATE
                lg_d001_transaccion
            SET
                num_estatus = '1',
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            WHERE
                pk_num_transaccion=:pk_num_transaccion AND
                num_estatus = '0'
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            'pk_num_transaccion'=>$idTran,
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metActualizarReqDet($idReqDet,$estado)
    {
        $this->_db->beginTransaction();
        $NuevoPost=$this->_db->prepare("
            UPDATE
                lg_c001_requerimiento_detalle
            SET
                ind_estado = '$estado',
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            WHERE
                pk_num_requerimiento_detalle=:pk_num_requerimiento_detalle
            ");

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            'pk_num_requerimiento_detalle'=>$idReqDet,
        ));

        $fallaTansaccion = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idReqDet;
        }
    }

    public function metActualizarRequerimiento($idReq)
    {
        $this->_db->beginTransaction();

        $NuevoPost=$this->_db->prepare("
        UPDATE
            lg_b001_requerimiento
        SET
            fk_rhb001_num_empleado_aprueba_despacho='$this->atIdEmpleado',
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
        WHERE
            pk_num_requerimiento=:pk_num_requerimiento
        ");
        $NuevoPost->execute(array(
            'pk_num_requerimiento'=>$idReq
        ));
        $fallaTansaccion = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idReq;
        }
    }
    
}

?>