<?php
/************************************************************************************************************
 * DEV: CONTRALORIA DEL ESTADO.
 * MODULO: Logistica
 * PROGRAMADORES.____________________________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                                 | TELEFONO.         | CONTRALORIA
 * | 1 | Fernando Rosillo     | f.rosillo@contraloriamonagas.gob.ve     | 0414-8807980      | MONAGAS
 * | 2 |
 * |_________________________________________________________________________________________________________
 * **********************************************************************************************************/
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'almacenModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'tipoDocumentoModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'maestros' . DS . 'tipoTransaccionModelo.php';
require_once RUTA_Modulo . 'modLG' . DS . 'modelos' . DS . 'almacen' . DS . 'transaccionModelo.php';
class ejecutarTranModelo extends Modelo
{
    private $atIdUsuario;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
        $this->atAlmacenModelo = new almacenModelo();
        $this->atTipoDocumentoModelo = new tipoDocumentoModelo();
        $this->atTipoTransaccionModelo = new tipoTransaccionModelo();
        $this->atTransaccionModelo = new transaccionModelo();
    }

    public function metListarTransaccionesPendiente()
    {
        $tranPost = $this->_db->query("
            SELECT
              *
            FROM
              lg_d001_transaccion
            WHERE
              num_estatus='0'
          ");
        $tranPost->setFetchMode(PDO::FETCH_ASSOC);
        return $tranPost->fetchAll();
    }
/*
    public function metMostrarTransaccion($idTran){
        $mostrarTransaccion = $this->_db->query("
              SELECT
                tran.*,
                usu.ind_usuario
              FROM
                lg_d001_transaccion AS tran
                INNER JOIN a018_seguridad_usuario AS usu ON usu.pk_num_seguridad_usuario = tran.fk_a018_num_seguridad_usuario
              WHERE
                tran.pk_num_transaccion='$idTran'
        ");
        $mostrarTransaccion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTransaccion->fetch();
    }
    public function metMostrarDetalles($idTran){
        $mostrarTransaccion = $this->_db->query("
              SELECT
                det.*,
                item.*,
                commodity.*,
                stock.pk_num_commodity_stock
              FROM
                lg_d002_transaccion_detalle AS det
                INNER JOIN lg_d001_transaccion AS tran ON tran.pk_num_transaccion = det.fk_lgd001_num_transaccion
                LEFT JOIN lg_b002_item AS item ON item.pk_num_item = det.fk_lgb002_num_item
                LEFT JOIN lg_b003_commodity AS commodity ON commodity.pk_num_commodity = det.fk_lgb003_num_commodity
                LEFT JOIN lg_c008_commodity_stock AS stock ON stock.fk_lgb003_num_commodity = commodity.pk_num_commodity
              WHERE
                tran.pk_num_transaccion='$idTran'
        ");
        $mostrarTransaccion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTransaccion->fetchAll();
    }
    */
    public function metBuscarTipoTran($idTran){
        $mostrarTransaccion = $this->_db->query("
              SELECT
                  detalle.ind_nombre_detalle
              FROM
                  lg_d001_transaccion AS transaccion
              INNER JOIN lg_b015_tipo_transaccion AS tipo ON transaccion.fk_lgb015_num_tipo_transaccion = tipo.pk_num_tipo_transaccion 
              INNER JOIN a006_miscelaneo_detalle AS detalle ON detalle.pk_num_miscelaneo_detalle = tipo.fk_a006_num_miscelaneo_detalle_tipo_documento
              WHERE transaccion.pk_num_transaccion=$idTran
        ");
        $mostrarTransaccion->setFetchMode(PDO::FETCH_ASSOC);
        return $mostrarTransaccion->fetch();
    }

    public function metEjecutarTransaccion($idTran)
    {
        $this->_db->beginTransaction();
        $modificarRegistro=$this->_db->query("
          UPDATE
            lg_d001_transaccion
          SET
            fk_rhb001_num_empleado_ejecutado_por = '$this->atIdEmpleado',
            num_estatus = '2',
            fec_ejecucion = NOW(),
            fk_a018_num_seguridad_usuario='$this->atIdUsuario',
            fec_ultima_modificacion=NOW()
          WHERE
            pk_num_transaccion='$idTran'
            ");

        $error = $modificarRegistro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        } else{
            $this->_db->commit();
            return $idTran;
        }
    }
    public function metActualizarStock($cantidad,$item,$cual)
    {
        $this->_db->beginTransaction();
        if($cual==1) {
            #recepcion
            $NuevoPost=$this->_db->prepare("
            UPDATE
                lg_c007_item_stock
            SET
                num_stock_actual=(num_stock_actual + :num_stock_actual),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            WHERE
                pk_num_item_stock=:pk_num_item
            ");
        } elseif($cual==2) {
            #despacho
            $NuevoPost=$this->_db->prepare("
            UPDATE
                lg_c007_item_stock
            SET
                num_stock_actual=(num_stock_actual - :num_stock_actual),
                num_stock_comprometido=(num_stock_comprometido - :num_stock_actual),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            WHERE
                pk_num_item_stock=:pk_num_item
            ");
        } elseif($cual==3) {
            #aprobar
            $NuevoPost=$this->_db->prepare("
            UPDATE
                lg_c007_item_stock
            SET
                num_stock_comprometido=(num_stock_comprometido + :num_stock_actual),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            WHERE
                pk_num_item_stock=:pk_num_item
            ");
        } elseif($cual==4) {
            #recepcion
            $NuevoPost=$this->_db->prepare("
            UPDATE
                lg_c008_commodity_stock
            SET
                num_cantidad=(num_cantidad + :num_stock_actual),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            WHERE
                pk_num_commodity_stock=:pk_num_item
            ");
        } elseif($cual==5) {
            #despacho
            $NuevoPost=$this->_db->prepare("
            UPDATE
                lg_c008_commodity_stock
            SET
                num_cantidad=(num_cantidad - :num_stock_actual),
                fk_a018_num_seguridad_usuario='$this->atIdUsuario',
                fec_ultima_modificacion=NOW()
            WHERE
                pk_num_commodity_stock=:pk_num_item
            ");
        }

        #execute — Ejecuta una sentencia preparada
        $NuevoPost->execute(array(
            'num_stock_actual'=>$cantidad,
            'pk_num_item'=>$item
        ));

        $fallaTansaccion = $NuevoPost->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $item;
        }
    }
}

?>