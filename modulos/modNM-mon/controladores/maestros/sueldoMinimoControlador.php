<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class sueldoMinimoControlador extends Controlador
{
    private $atSueldoMinimo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atSueldoMinimo = $this->metCargarModelo('sueldoMinimo', 'maestros');
    }

    public function metIndex($estatus = false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('estatus', $estatus);
        $this->atVista->metRenderizar('listado');
    }

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idSueldoMinimo = $this->metObtenerInt('idSueldoMinimo');
        if ($valido == 1) {
            $this->metValidarToken();
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum');
            $ind = $this->metValidarFormArrayDatos('form', 'int');
            $txt = $this->metValidarFormArrayDatos('form', 'txt');
            if ($alphaNum != null && $ind == null && $txt==null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $validacion = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            }else{
                $validacion = array_merge($ind, $txt,$alphaNum);
            }
            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            if ($idSueldoMinimo == 0) {
                $id = $this->atSueldoMinimo->metCrearSueldoMinimo(
                    $validacion['ind_descripcion'], $validacion['ind_num_gaceta'],
                    $validacion['ind_num_resolucion'], $validacion['ind_periodo'],
                    number_format($validacion['num_sueldo_minimo'],6,'.','')
                );
                $validacion['ind_estado'] = 'PR';
                $validacion['status'] = 'nuevo';
            } else {
                $id = $this->atSueldoMinimo->metModificarSueldoMinimo(
                    $idSueldoMinimo, $validacion['ind_descripcion'], $validacion['ind_num_gaceta'],
                    $validacion['ind_num_resolucion'], $validacion['ind_periodo'],
                    number_format($validacion['num_sueldo_minimo'],6,'.','')
                );
                $validacion['ind_estado'] = 'PR';
                $validacion['status'] = 'modificar';
            }

            if (is_array($id)) {
                print_r($id);
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idSueldoMinimo'] = $id;
            echo json_encode($validacion);
            exit;
        }

        if($idSueldoMinimo!=0){
            $this->atVista->assign('formDB',$this->atSueldoMinimo->metBuscarSueldoMinimo($idSueldoMinimo));
        }
        $this->atVista->assign('idSueldoMinimo', $idSueldoMinimo);
        $this->atVista->metRenderizar('crearModificar', 'modales');
    }

    public function metVer()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $idSueldoMinimo = $this->metObtenerInt('idSueldoMinimo');

        $this->atVista->assign('formDB',$this->atSueldoMinimo->metBuscarSueldoMinimo($idSueldoMinimo));
        $this->atVista->assign('idSueldoMinimo', $idSueldoMinimo);
        $this->atVista->assign('ver', 1);
        $this->atVista->metRenderizar('crearModificar', 'modales');
    }

    public function metAprobar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $valido = $this->metObtenerInt('valido');
        $idSueldoMinimo = $this->metObtenerInt('idSueldoMinimo');
        if ($valido == 1) {
            if($this->metObtenerInt('idSueldoMinimoForm')!=null){
                $id = $this->atSueldoMinimo->metAprobarSueldominimo($this->metObtenerInt('idSueldoMinimoForm'),'AP');
                $validacion['idSueldoMinimo']=$id;
                $validacion['status']='aprobar';
                echo json_encode($validacion);
                exit;
            }else{
                $this->metValidarToken();
                $validacion=$this->metValidarFormArrayDatos('form','alphaNum');
                if(in_array('error',$validacion)){
                    $validacion['status']='error';
                    echo json_encode($validacion);
                    exit;
                }

                $id = $this->atSueldoMinimo->metAnularSueldominimo($idSueldoMinimo,$validacion['ind_motivo_anulado']);
                $validacion['idSueldoMinimo']=$id;
                $validacion['status']='anular';
                echo json_encode($validacion);
                exit;
            }
            exit;
        }
        if($idSueldoMinimo!=0){
            $this->atVista->assign('formDB',$this->atSueldoMinimo->metBuscarSueldoMinimo($idSueldoMinimo));
        }
        $this->atVista->assign('idSueldoMinimo', $idSueldoMinimo);
        $this->atVista->assign('ap', 1);
        $this->atVista->metRenderizar('crearModificar', 'modales');
    }

    public function metJsonDataTabla($estatus = false)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT * FROM nm_b006_sueldo_minimo WHERE 1 ";
        if ($estatus) {
            $sql .= " AND ind_estado = '$estatus'";
        }
        if ($busqueda['value']) {
            $sql .= " AND
                        ( 
                        ind_periodo LIKE '%$busqueda[value]%' OR
                        ind_num_resolucion LIKE '%$busqueda[value]%' OR
                        ind_num_gaceta LIKE '%$busqueda[value]%' OR
                        num_sueldo_minimo LIKE '%$busqueda[value]%' OR
                        ind_estado LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_periodo','ind_num_resolucion','ind_num_gaceta','num_sueldo_minimo','ind_estado');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_sueldo_minimo';
        #construyo el listado de botones

        if (in_array('NM-01-03-07-01-AP',$rol) AND $estatus == 'PR') {
            $campos['boton']['Aprobar'] = '
                <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static"
                        idSueldoMinimo="'.$clavePrimaria.'"
                        descipcion="El Usuario a Aprobado un Sueldo Minimo en el Sistema"
                        titulo="<i class=\'icm icm-coins\'></i> Aprobar Sueldo Minimo"
                        title="Aprobar">
                    <i class="icm icm-rating3" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('NM-01-03-06-02-M',$rol) AND $estatus != 'PR') {
            $campos['boton']['Editar'] = array(
                "<button class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        data-toggle='modal' data-target='#formModal'
                        data-keyboard='false' data-backdrop='static' idSueldoMinimo='$clavePrimaria'
                        descipcion='El Usuario a Modificado un Sueldo Minimo en el Sistema'
                        titulo='Modificar Sueldo Minimo'
                        title='Editar'>
                    <i class='fa fa-edit' style='color: #ffffff;'></i>
                </button>
                ",
                'if( $i["ind_estado"] =="PR") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
                );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('NM-01-03-04-02-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idSueldoMinimo="'.$clavePrimaria.'"
                        descipcion="El Usuario esta Viendo un Sueldo Minimo en el Sistema"
                        titulo="<i class=\'icm icm-coins\'></i> Ver Sueldo Minimo"
                        title="Ver">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }
}
