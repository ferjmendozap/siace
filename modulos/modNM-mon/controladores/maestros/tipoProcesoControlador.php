<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class tipoProcesoControlador extends Controlador
{
    private $atTipoProceso;

    public function __construct()
    {
        parent::__construct();
        $this->atTipoProceso=$this->metCargarModelo('tipoProceso','maestros');
    }

    public function metIndex($lista=false)
    {
        if(!$lista){
            $complementosCss = array(
                'DataTables/jquery.dataTables',
                'DataTables/extensions/dataTables.colVis941e',
                'DataTables/extensions/dataTables.tableTools4029',
            );
            $js[] = 'materialSiace/core/demo/DemoTableDynamic';
            $js[] = 'Aplicacion/appFunciones';
            $this->atVista->metCargarCssComplemento($complementosCss);
            $this->atVista->metCargarJs($js);
        }
        if($lista){
            $this->atVista->assign('listado',$this->atTipoProceso->metListarTipoProceso());
            $this->atVista->metRenderizar('listadoModal','modales');
        }else{
            $this->atVista->metRenderizar('listado');
        }
    }

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);

        $valido=$this->metObtenerInt('valido');
        $idProceso=$this->metObtenerInt('idProceso');
        if($valido==1){
            $this->metValidarToken();
            $Excceccion=array('num_flag_adelanto','num_estatus');
            $alphaNum=$this->metValidarFormArrayDatos('form','alphaNum');
            $ind=$this->metValidarFormArrayDatos('form','int',$Excceccion);
            if($alphaNum!=null && $ind==null){
                $validacion=$alphaNum;
            }elseif($alphaNum==null && $ind!=null){
                $validacion=$ind;
            }else{
                $validacion=array_merge($alphaNum,$ind);
            }

            if(in_array('error',$validacion)){
                $validacion['status']='error';
                echo json_encode($validacion);
                exit;
            }
            if(!isset($validacion['num_flag_adelanto'])){
                $validacion['num_flag_adelanto']=0;
            }
            if(!isset($validacion['num_flag_individual'])){
                $validacion['num_flag_individual']=0;
            }
            if(!isset($validacion['num_estatus'])){
                $validacion['num_estatus']=0;
            }

            if($idProceso==0){
                $id=$this->atTipoProceso->metCrearTipoProceso($validacion['cod_proceso'],$validacion['ind_nombre_proceso'],$validacion['num_flag_adelanto'],$validacion['num_estatus'],$validacion['num_flag_individual']);
                $validacion['status']='nuevo';
            }else{
                $id=$this->atTipoProceso->metModificarTipoPorceso($validacion['cod_proceso'],$validacion['ind_nombre_proceso'],$validacion['num_flag_adelanto'],$validacion['num_estatus'],$validacion['num_flag_individual'],$idProceso);
                $validacion['status']='modificar';
            }

            if(is_array($id)){
                foreach ($validacion as $titulo => $valor){
                    if(strpos($id[2],$validacion[$titulo])){
                        $validacion[$titulo]='error';
                    }
                }
                $validacion['status']='errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idProceso']=$id;

            echo json_encode($validacion);
            exit;
        }

        if($idProceso!=0){
            $this->atVista->assign('formDB',$this->atTipoProceso->metMostrarTipoProceso($idProceso));
            $this->atVista->assign('idProceso',$idProceso);
        }

        $this->atVista->metRenderizar('CrearModificar','modales');
    }

    public function metEliminar()
    {
        $idProceso = $this->metObtenerInt('idProceso');
        if($idProceso!=0){
            $id=$this->atTipoProceso->metEliminarTipoProceso($idProceso);
            if(is_array($id)){
                $valido=array(
                    'status'=>'error',
                    'mensaje'=>'Disculpa. Pero el proceso se encuentra en uso y no se puede eliminar'
                );
            }else{
                $valido=array(
                    'status'=>'ok',
                    'idProceso'=>$id
                );
            }
        }
        echo json_encode($valido);
        exit;
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT * FROM nm_b003_tipo_proceso ";

        if ($busqueda['value']) {
            $sql .= "WHERE
                    ( 
                      ind_nombre_proceso LIKE '%$busqueda[value]%' OR 
                      cod_proceso LIKE '%$busqueda[value]%'
                    )
                    ";
        }
        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_proceso','ind_nombre_proceso','num_flag_individual','num_flag_adelanto','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_tipo_proceso';
        #los valores del listado con flag 'md md-check'
        $flags = array('num_flag_adelanto','num_flag_individual');
        #construyo el listado de botones
        if (in_array('NM-01-03-01-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                    <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                            data-keyboard="false" data-backdrop="static" idProceso="'.$clavePrimaria.'" title="Editar"
                            descipcion="El Usuario a Modificado un Proceso de Nomina" titulo="Modificar Proceso">
                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Editar'] = false;
        }
        if (in_array('NM-01-03-01-03-E',$rol)) {
            $campos['boton']['Eliminar'] = '
                    <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idProceso="'.$clavePrimaria.'" boton="si, Eliminar" title="Eliminar"
                            descipcion="El usuario a eliminado un Proceso" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Proceso!!">
                        <i class="md md-delete" style="color: #ffffff;"></i>
                    </button>
                ';
        } else {
            $campos['boton']['Eliminar'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria,false,$flags);
    }

}
