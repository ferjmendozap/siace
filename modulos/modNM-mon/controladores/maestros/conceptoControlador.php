<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class Funciones
{
    use funcionesNomina;
}

class conceptoControlador extends Controlador
{
    private $atConceptoModelo;

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->atConceptoModelo = $this->metCargarModelo('concepto', 'maestros');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('listado');
    }

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $complementosCss = array(
            'select2/select201ef',
            'wizard/wizardfa6c',
            'menu-vertical/menu-vertical',
            'jquery-validation/dist/site-demos',
            'codeMirror/codemirror',
            'codeMirror/addon/hint/show-hint',
            'codeMirror/addon/fold/foldgutter',
            'codeMirror/addon/dialog/dialog',
            'codeMirror/addon/search/matchesonscrollbar',
            'codeMirror/addon/display/fullscreen',
        );
        $complementosJs = array(
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'codeMirror/codemirror',
            'codeMirror/addon/edit/matchbrackets',
            'codeMirror/addon/hint/show-hint',
            'codeMirror/addon/hint/javascript-hint',
            'codeMirror/addon/fold/foldcode',
            'codeMirror/addon/fold/foldgutter',
            'codeMirror/addon/fold/brace-fold',
            'codeMirror/addon/fold/xml-fold',
            'codeMirror/addon/fold/markdown-fold',
            'codeMirror/addon/fold/comment-fold',
            'codeMirror/addon/dialog/dialog',
            'codeMirror/addon/search/searchcursor',
            'codeMirror/addon/search/search',
            'codeMirror/addon/scroll/annotatescrollbar',
            'codeMirror/addon/search/matchesonscrollbar',
            'codeMirror/addon/display/fullscreen',
            'codeMirror/mode/htmlmixed/htmlmixed',
            'codeMirror/mode/xml/xml',
            'codeMirror/mode/javascript/javascript',
            'codeMirror/mode/css/css',
            'codeMirror/mode/clike/clike',
            'codeMirror/mode/php/php',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $idConcepto = $this->metObtenerInt('idConcepto');
        $valido = $this->metObtenerInt('valido');

        if ($valido == 1) {
            $this->metValidarToken();
            $Excceccion = array('num_estatus', 'ind_formula', 'num_flag_automatico', 'num_flag_bono', 'num_flag_incidencia', 'partida', 'debe', 'debepub20', 'haber', 'haberPub20','fk_nmb005_num_interfaz_cxp');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum', $Excceccion);
            $ind = $this->metValidarFormArrayDatos('form', 'int', $Excceccion);
            $txt = $this->metValidarFormArrayDatos('form', 'txt', $Excceccion);
            $formula = $this->metValidarFormArrayDatos('form', 'formula', $Excceccion);

            if ($alphaNum != null && $ind == null && $txt == null) {
                $validacion = array_merge($formula, $alphaNum);
            } elseif ($alphaNum == null && $ind != null && $txt == null) {
                $validacion = array_merge($formula, $ind);
            } elseif ($alphaNum == null && $ind == null && $txt != null) {
                $validacion = array_merge($formula, $txt);
            } elseif ($alphaNum == null && $ind != null && $txt != null) {
                $validacion = array_merge($formula, $txt, $ind);
            } elseif ($alphaNum != null && $ind == null && $txt != null) {
                $validacion = array_merge($formula, $txt, $alphaNum);
            } elseif ($alphaNum != null && $ind != null && $txt == null) {
                $validacion = array_merge($formula, $ind, $alphaNum);
            } else {
                $validacion = array_merge($alphaNum, $ind, $formula, $txt);
            }
            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            if (!isset($validacion['num_estatus'])) {
                $validacion['num_estatus'] = 0;
            }
            if (!isset($validacion['num_flag_automatico'])) {
                $validacion['num_flag_automatico'] = 0;
            }
            if (!isset($validacion['num_flag_bono'])) {
                $validacion['num_flag_bono'] = 0;
            }
            if (!isset($validacion['num_flag_incidencia'])) {
                $validacion['num_flag_incidencia'] = 0;
            }
            if ($validacion['ind_formula'] == '') {
                $validacion['ind_formula'] = null;
            }

            if (!isset($validacion['fk_nmb001_num_tipo_nomina'])) {
                $validacion['fk_nmb001_num_tipo_nomina'] = false;
            }
            if (!isset($validacion['fk_nmb005_num_interfaz_cxp']) OR $validacion['fk_nmb005_num_interfaz_cxp']==0) {
                $validacion['fk_nmb005_num_interfaz_cxp'] = NULL;
            }
            if (!isset($validacion['fk_nmb003_num_tipo_proceso'])) {
                $validacion['fk_nmb003_num_tipo_proceso'] = false;
            }
            if (!isset($validacion['partida'])) {
                $validacion['partida'] = false;
            }
            if (!isset($validacion['debe'])) {
                $validacion['debe'] = false;
            }
            if (!isset($validacion['haber'])) {
                $validacion['haber'] = false;
            }
            if (!isset($validacion['debepub20'])) {
                $validacion['debepub20'] = false;
            }
            if (!isset($validacion['haberPub20'])) {
                $validacion['haberPub20'] = false;
            } 

            if ($idConcepto == 0) {
                $id = $this->atConceptoModelo->metCrearConcepto(
                    $validacion['ind_formula'], $validacion['ind_abrebiatura'], $validacion['ind_descripcion'],
                    $validacion['ind_impresion'], $validacion['fk_a006_num_tipo_concepto'], $validacion['num_orden_planilla'],
                    $validacion['fk_nmb001_num_tipo_nomina'], $validacion['fk_nmb003_num_tipo_proceso'], $validacion['partida'], $validacion['debe'], $validacion['haber'],
                    $validacion['debepub20'], $validacion['haberPub20'], $validacion['num_flag_automatico'], $validacion['num_flag_bono'], $validacion['num_flag_incidencia'], $validacion['num_estatus'],$validacion['fk_nmb005_num_interfaz_cxp']
                );
                $validacion['status'] = 'nuevo';
            } else {
                $id = $this->atConceptoModelo->metModificarConcepto(
                    $validacion['ind_formula'], $validacion['ind_abrebiatura'], $validacion['ind_descripcion'],
                    $validacion['ind_impresion'], $validacion['fk_a006_num_tipo_concepto'], $validacion['num_orden_planilla'],
                    $validacion['fk_nmb001_num_tipo_nomina'], $validacion['fk_nmb003_num_tipo_proceso'], $validacion['partida'], $validacion['debe'], $validacion['haber'],
                    $validacion['debepub20'], $validacion['haberPub20'], $validacion['num_flag_automatico'], $validacion['num_flag_bono'], $validacion['num_flag_incidencia'], $validacion['num_estatus'],$validacion['fk_nmb005_num_interfaz_cxp'], $idConcepto
                );
                $validacion['status'] = 'modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])) {
                        if (strpos($id[2], $validacion[$titulo])) {
                            $validacion[$titulo] = 'errorNo';
                        }
                    }
                }
                $validacion['idConcepto'] = $idConcepto;
                $validacion['id'] = $id;
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idConcepto'] = $id;

            echo json_encode($validacion);
            exit;

        }
        if ($idConcepto != 0) {
            $db = $this->atConceptoModelo->metMostrarConcepto($idConcepto);
            $this->atVista->assign('formDB', $db);
        }

        //metodos de nomina
        $metodos = get_class_methods(new Funciones());
        array_shift($metodos);
        //echo print_r($metodos);
        $this->atVista->assign('metodos', $metodos);
        //atributos de nomina
        $at = array_keys(get_class_vars(get_class(new Funciones())));
        for($i=0;$i<count($at);$i++){
            if((strpos($at[$i],'Const')===false)){
                $atributos[]=$at[$i];
            }else{
                $constantes[]=$at[$i];
            }
        }

        if(!isset($atributos)){
            $atributos=false;
        }

        if(!isset($constantes)){
            $constantes=false;
        }

        $this->atVista->assign('atributos', $atributos);
        $this->atVista->assign('constantes', $constantes);

        if ($idConcepto != 0) {
            $this->atVista->assign('formDB', $this->atConceptoModelo->metMostrarConcepto($idConcepto));
            $conceptoDetalle = $this->atConceptoModelo->metMostrarConceptoDetalle($idConcepto);
            for ($i = 0; $i < count($conceptoDetalle); $i++) {

                $nominas[$conceptoDetalle[$i]['fk_nmb001_num_tipo_nomina']] = array(
                    'id' => $conceptoDetalle[$i]['fk_nmb001_num_tipo_nomina'],
                    'cod' => $conceptoDetalle[$i]['cod_tipo_nomina'],
                    'nombre' => $conceptoDetalle[$i]['ind_nombre_nomina'],
                );

                $procesos[$conceptoDetalle[$i]['fk_nmb003_num_tipo_proceso']] = array(
                    'id' => $conceptoDetalle[$i]['fk_nmb003_num_tipo_proceso'],
                    'cod' => $conceptoDetalle[$i]['cod_proceso'],
                    'nombre' => $conceptoDetalle[$i]['ind_nombre_proceso'],
                );
            }

            if (isset($nominas)) {
                $this->atVista->assign('nominas', $nominas);
            }
            if (isset($procesos)) {
                $this->atVista->assign('procesos', $procesos);
            }
            $this->atVista->assign('formDBDet', $conceptoDetalle);
        }

        $this->atVista->assign('tipoConcepto', $this->atConceptoModelo->metMostrarSelect('TDCNM'));
        $this->atVista->assign('idConcepto', $idConcepto);
        $this->atVista->metRenderizar('crear', 'modales');
    }

    public function metVer()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $complementosCss = array(
            'select2/select201ef',
            'wizard/wizardfa6c',
            'menu-vertical/menu-vertical',
            'jquery-validation/dist/site-demos',
            'codeMirror/codemirror',
            'codeMirror/addon/hint/show-hint',
            'codeMirror/addon/fold/foldgutter',
            'codeMirror/addon/dialog/dialog',
            'codeMirror/addon/search/matchesonscrollbar',
            'codeMirror/addon/display/fullscreen',
        );
        $complementosJs = array(
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'codeMirror/codemirror',
            'codeMirror/addon/edit/matchbrackets',
            'codeMirror/addon/hint/show-hint',
            'codeMirror/addon/hint/javascript-hint',
            'codeMirror/addon/fold/foldcode',
            'codeMirror/addon/fold/foldgutter',
            'codeMirror/addon/fold/brace-fold',
            'codeMirror/addon/fold/xml-fold',
            'codeMirror/addon/fold/markdown-fold',
            'codeMirror/addon/fold/comment-fold',
            'codeMirror/addon/dialog/dialog',
            'codeMirror/addon/search/searchcursor',
            'codeMirror/addon/search/search',
            'codeMirror/addon/scroll/annotatescrollbar',
            'codeMirror/addon/search/matchesonscrollbar',
            'codeMirror/addon/display/fullscreen',
            'codeMirror/mode/htmlmixed/htmlmixed',
            'codeMirror/mode/xml/xml',
            'codeMirror/mode/javascript/javascript',
            'codeMirror/mode/css/css',
            'codeMirror/mode/clike/clike',
            'codeMirror/mode/php/php',
        );

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $idConcepto = $this->metObtenerInt('idConcepto');
        //metodos de nomina
        $metodos = get_class_methods(new Funciones());
        array_shift($metodos);
        $this->atVista->assign('metodos', $metodos);
        //atributos de nomina
        $at = array_keys(get_class_vars(get_class(new Funciones())));
        for($i=0;$i<count($at);$i++){
            if((strpos($at[$i],'Const')===false)){
                $atributos[]=$at[$i];
            }else{
                $constantes[]=$at[$i];
            }
        }

        if(!isset($atributos)){
            $atributos=false;
        }

        if(!isset($constantes)){
            $constantes=false;
        }

        $this->atVista->assign('atributos', $atributos);
        $this->atVista->assign('constantes', $constantes);

        if ($idConcepto != 0) {
            $this->atVista->assign('formDB', $this->atConceptoModelo->metMostrarConcepto($idConcepto));
            $conceptoDetalle = $this->atConceptoModelo->metMostrarConceptoDetalle($idConcepto);
            for ($i = 0; $i < count($conceptoDetalle); $i++) {

                $nominas[$conceptoDetalle[$i]['fk_nmb001_num_tipo_nomina']] = array(
                    'id' => $conceptoDetalle[$i]['fk_nmb001_num_tipo_nomina'],
                    'cod' => $conceptoDetalle[$i]['cod_tipo_nomina'],
                    'nombre' => $conceptoDetalle[$i]['ind_nombre_nomina'],
                );

                $procesos[$conceptoDetalle[$i]['fk_nmb003_num_tipo_proceso']] = array(
                    'id' => $conceptoDetalle[$i]['fk_nmb003_num_tipo_proceso'],
                    'cod' => $conceptoDetalle[$i]['cod_proceso'],
                    'nombre' => $conceptoDetalle[$i]['ind_nombre_proceso'],
                );
            }

            if (isset($nominas)) {
                $this->atVista->assign('nominas', $nominas);
            }
            if (isset($procesos)) {
                $this->atVista->assign('procesos', $procesos);
            }
            $this->atVista->assign('formDBDet', $conceptoDetalle);
        }

        $this->atVista->assign('ver', 1);
        $this->atVista->assign('tipoConcepto', $this->atConceptoModelo->metMostrarSelect('TDCNM'));
        $this->atVista->assign('idConcepto', $idConcepto);
        $this->atVista->metRenderizar('crear', 'modales');
    }

    public function metJsonDataTabla()
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                      nm_b002.*,
                      a006.ind_nombre_detalle
                    FROM
                      nm_b002_concepto nm_b002
                    INNER JOIN
                      a006_miscelaneo_detalle a006 ON a006.pk_num_miscelaneo_detalle = nm_b002.fk_a006_num_tipo_concepto ";
        if ($busqueda['value']) {
            $sql .= " WHERE
                        ( 
                        nm_b002.cod_concepto LIKE '%$busqueda[value]%' OR
                        nm_b002.ind_descripcion LIKE '%$busqueda[value]%' OR
                        a006.ind_nombre_detalle LIKE '%$busqueda[value]%'
                        )
                        ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('cod_concepto','ind_descripcion','ind_nombre_detalle','num_estatus');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_concepto';
        #construyo el listado de botones

        if (in_array('NM-01-03-03-02-M',$rol)) {
            $campos['boton']['Editar'] = '
                <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idConcepto="'.$clavePrimaria.'" title="Editar"
                        descipcion="El Usuario a Modificado un Concepto" titulo="<i class=\'icm icm-calculate2\'></i> Modificar el Concepto">
                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('NM-01-03-03-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idConcepto="'.$clavePrimaria.'" title="Ver"
                        descipcion="El Usuario esta viendo un Concepto" titulo="<i class=\'icm icm-calculate2\'></i> Ver Concepto">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }
}
