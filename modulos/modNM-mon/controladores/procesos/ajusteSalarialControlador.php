<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class ajusteSalarialControlador extends Controlador
{

    private $atAjustes;

    public function __construct()
    {
        parent::__construct();
        $this->atAjustes = $this->metCargarModelo('ajusteSalarial', 'procesos');
    }

    public function metIndex($estatus=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

        $this->atVista->assign('estatus', $estatus);
        $this->atVista->metRenderizar('listado');
    }

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $complementosCss = array(
            'select2/select201ef',
            'wizard/wizardfa6c',
            'bootstrap-datepicker/datepicker',
            'jquery-validation/dist/site-demos',
        );
        $complementosJs = array(
            'select2/select2.min',
            'wizard/jquery.bootstrap.wizard.min',
            'jquery-validation/dist/jquery.validate.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );

        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $valido = $this->metObtenerInt('valido');
        $idAjusteSalarial = $this->metObtenerInt('idAjusteSalarial');
        $tipo = $this->metObtenerAlphaNumerico('tipo');
        $estado = $this->metObtenerAlphaNumerico('estado');

        if(isset($estado) && ($estado=='AN' || $estado=='CO' || $estado=='AP')){
            $validacion['ind_motivo_anulacion'] = $this->metObtenerAlphaNumerico('ind_motivo_anulacion');
            if($validacion['ind_motivo_anulacion']==null && $estado=='AN'){
                $validacion['ind_motivo_anulacion']="error";
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            $id=$this->atAjustes->metCambioSatus($idAjusteSalarial,$estado,$validacion['ind_motivo_anulacion']);
            $validacion['status'] = $estado;

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])){
                        if (strpos($id[2], (string)$validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }

            if($estado=='AN'){
                $validacion['Mensaje'] = array('titulo'=>'Anulado','contenido'=>'EL Ajuste fue Anulado Satisfactoriamente');
            }elseif($estado=='CO'){
                $validacion['Mensaje'] = array('titulo'=>'Conformado','contenido'=>'EL Ajuste fue Conformado Satisfactoriamente');
            }elseif($estado=='AP'){
                $validacion['Mensaje'] = array('titulo'=>'Aprobado','contenido'=>'EL Ajuste fue Aprobado Satisfactoriamente');
            }

            if($estado=='AP'){
                $grado=$this->atAjustes->metBuscarAjusteSalarialDet($idAjusteSalarial);
                $gradoPaso=$this->atAjustes->metBuscarAjusteSalarialDetPaso($idAjusteSalarial);
                $id=$this->atAjustes->metActualizarGrado($idAjusteSalarial,$grado,$gradoPaso);
                if (is_array($id)) {
                    foreach ($validacion as $titulo => $valor) {
                        if(!is_array($validacion[$titulo])){
                            if (strpos($id[2], (string)$validacion[$titulo])) {
                                $validacion[$titulo] = 'error';
                            }
                        }
                    }
                    $validacion['status'] = 'errorSQL';
                    echo json_encode($validacion);
                    exit;
                }
            }
            $validacion['idAjusteSalarial'] = $idAjusteSalarial;
            echo json_encode($validacion);
            exit;
        }
        if ($valido == 1) {
            $this->metValidarToken();
            $excepcion = array('num_estatus','ind_porcentaje','num_monto');
            $alphaNum = $this->metValidarFormArrayDatos('form', 'alphaNum',$excepcion);
            $ind = $this->metValidarFormArrayDatos('form', 'int',$excepcion);
            $txt = $this->metValidarFormArrayDatos('form', 'txt',$excepcion);
            if ($alphaNum != null && $ind == null && $txt==null) {
                $validacion = $alphaNum;
            } elseif ($alphaNum == null && $ind != null && $txt==null) {
                $validacion = $ind;
            } elseif($alphaNum != null && $ind != null && $txt==null) {
                $validacion = array_merge($alphaNum, $ind);
            } elseif($alphaNum != null && $txt != null &&$ind == null) {
                $validacion = array_merge($alphaNum, $txt);
            } elseif($ind != null && $txt != null && $alphaNum == null) {
                $validacion = array_merge($ind, $txt);
            }else{
                $validacion = array_merge($ind, $txt,$alphaNum);
            }
            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            $ajusteDet= $validacion['gradoSalarial'];
            if(isset($ajusteDet['checkBox'])) {
                $idGrados = $ajusteDet['checkBox'];
                foreach ($idGrados as $idGrado) {
                    if ($ajusteDet[$idGrado]['ind_porcentaje'] == '' && $ajusteDet[$idGrado]['num_monto'] == '') {
                        $validacion['status'] = 'errorCondicion';
                        $validacion['mensaje'] = 'error tiene que colocar almenos un tipo de aumento';
                        echo json_encode($validacion);
                        exit;
                    } elseif ($ajusteDet[$idGrado]['ind_porcentaje'] != '' && $ajusteDet[$idGrado]['num_monto'] != '') {
                        $validacion['mensaje'] = 'error no puede usar los 2 tipos de aumentos al mismo tiempo';
                        $validacion['status'] = 'errorCondicion';
                        echo json_encode($validacion);
                        exit;
                    } elseif ($ajusteDet[$idGrado]['ind_porcentaje'] != '') {
                        $sueldoNuevo = $ajusteDet[$idGrado]['num_sueldo_anterior'] + (($ajusteDet[$idGrado]['ind_porcentaje'] / 100) * $ajusteDet[$idGrado]['num_sueldo_anterior']);
                    } elseif ($ajusteDet[$idGrado]['num_monto'] != '') {
                        $sueldoNuevo = $ajusteDet[$idGrado]['num_sueldo_anterior'] + $ajusteDet[$idGrado]['num_monto'];
                    }
                    $ajusteDet[$idGrado]['sueldoNuevo'] = $sueldoNuevo;
                }
            }else{
                $validacion['status'] = 'errorCondicion';
                $validacion['mensaje'] = 'error le falta el ajuste 1';
                echo json_encode($validacion);
                exit;
            }

            $ajusteDetPaso= $validacion['gradoSalarialPaso'];
            if(isset($ajusteDetPaso['checkBox'])) {
                $idGrados = $ajusteDetPaso['checkBox'];
                foreach ($idGrados as $idGrado) {
                    if ($ajusteDetPaso[$idGrado]['ind_porcentaje'] == '' && $ajusteDetPaso[$idGrado]['num_monto'] == '') {
                        $validacion['status'] = 'errorCondicion';
                        $validacion['mensaje'] = 'error tiene que colocar almenos un tipo de aumento';
                        echo json_encode($validacion);
                        exit;
                    } elseif ($ajusteDetPaso[$idGrado]['ind_porcentaje'] != '' && $ajusteDetPaso[$idGrado]['num_monto'] != '') {
                        $validacion['mensaje'] = 'error no puede usar los 2 tipos de aumentos al mismo tiempo';
                        $validacion['status'] = 'errorCondicion';
                        echo json_encode($validacion);
                        exit;
                    } elseif ($ajusteDetPaso[$idGrado]['ind_porcentaje'] != '') {
                        $sueldoNuevo = $ajusteDetPaso[$idGrado]['num_sueldo_anterior'] + (($ajusteDetPaso[$idGrado]['ind_porcentaje'] / 100) * $ajusteDetPaso[$idGrado]['num_sueldo_anterior']);
                    } elseif ($ajusteDetPaso[$idGrado]['num_monto'] != '') {
                        $sueldoNuevo = $ajusteDetPaso[$idGrado]['num_sueldo_anterior'] + $ajusteDetPaso[$idGrado]['num_monto'];
                    }
                    $ajusteDetPaso[$idGrado]['sueldoNuevo'] = $sueldoNuevo;
                }
            }else{
                $validacion['status'] = 'errorCondicion';
                $validacion['mensaje'] = 'error le falta el ajuste 2';
                echo json_encode($validacion);
                exit;
            }

            if ($idAjusteSalarial == 0) {
                $id=$this->atAjustes->metCrearAjusteSalarial($validacion['ind_descripcion'], $validacion['ind_numero_gaceta'], $validacion['ind_numero_resolucion'], $validacion['ind_periodo'], $ajusteDet,$ajusteDetPaso);
                $validacion['status'] = 'nuevo';
            } else {
                $id=$this->atAjustes->metModificarAjusteSalarial($idAjusteSalarial,$validacion['ind_descripcion'], $validacion['ind_numero_gaceta'], $validacion['ind_numero_resolucion'], $validacion['ind_periodo'], $ajusteDet,$ajusteDetPaso);
                $validacion['status'] = 'modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if(!is_array($validacion[$titulo])){
                        if (strpos($id[2], (string)$validacion[$titulo])) {
                            $validacion[$titulo] = 'error';
                        }
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idAjusteSalarial'] = $id;
            echo json_encode($validacion);
            exit;
        }

        if($idAjusteSalarial!=0){
            $this->atVista->assign('formDB',$this->atAjustes->metBuscarAjusteSalarial($idAjusteSalarial));
            $this->atVista->assign('formDBDet',$this->atAjustes->metBuscarAjusteSalarialDet($idAjusteSalarial));
            $this->atVista->assign('formDBDetPaso',$this->atAjustes->metBuscarAjusteSalarialDetPaso($idAjusteSalarial));
        }

        $this->atVista->assign('tipo', $tipo);

        $listadoGrdos = array();
        foreach ($this->atAjustes->metListarGrados() AS $i){
            $pasos = $this->atAjustes->metListarGradosPaso($i['pk_num_grado']);
            if(count($pasos)==0){
                $pasos = null;
            }
            $listadoGrdos[] = array(
                'pk_num_grado'=>$i['pk_num_grado'],
                'fk_a006_num_miscelaneo_detalle'=>$i['fk_a006_num_miscelaneo_detalle'],
                'ind_grado'=>$i['ind_grado'],
                'ind_nombre_grado'=>$i['ind_nombre_grado'],
                'ind_nombre_detalle'=>$i['ind_nombre_detalle'],
                'flag_pasos'=>$i['flag_pasos'],
                'num_sueldo_minimo'=>$i['num_sueldo_minimo'],
                'num_sueldo_maximo'=>$i['num_sueldo_maximo'],
                'num_sueldo_promedio'=>$i['num_sueldo_promedio'],
                'num_estatus'=>$i['num_estatus'],
                'fk_a018_num_seguridad_usuario'=>$i['fk_a018_num_seguridad_usuario'],
                'fec_ultima_modificacion'=>$i['fec_ultima_modificacion'],
                'pasos'=>$pasos
            );
        }

        $this->atVista->assign('listadoGrados', $listadoGrdos);
        $this->atVista->assign('idAjusteSalarial', $idAjusteSalarial);
        $this->atVista->metRenderizar('crearModificar', 'modales');
    }

    public function metJsonDataTabla($estatus = false)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  *,
                  concat_ws('-',fec_mes,fec_anio) AS fec
                FROM
                  nm_b004_ajuste_salarial 
                WHERE 1 ";
        if ($estatus) {
            $sql .= " AND ind_estado='$estatus' ";
        }

        if ($busqueda['value']) {
            $sql .= " AND
                        ( 
                        fec LIKE '%$busqueda[value]%' OR
                        ind_descripcion LIKE '%$busqueda[value]%' OR
                        ind_numero_resolucion LIKE '%$busqueda[value]%' OR
                        ind_numero_gaceta LIKE '%$busqueda[value]%' OR
                        ind_estado LIKE '%$busqueda[value]%'
                        ) ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('fec','ind_descripcion','ind_numero_resolucion','ind_numero_gaceta','ind_estado');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_ajust_salarial';
        #construyo el listado de botones

        if (in_array('NM-01-01-05-01-05-AP',$rol) AND $estatus == 'CO') {
            $campos['boton']['Aprobar'] = '
                <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static"
                        idAjusteSalarial="'.$clavePrimaria.'"
                        descipcion="El Usuario a Aprobado un Ajuste Salarial"
                        titulo="<i class=\'icm icm-coin\'></i> Aprobar Ajuste Salarial"
                        title="Aprobar">
                    <i class="icm icm-rating3" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('NM-01-01-05-01-02-M',$rol) AND $estatus != 'PR') {
            $campos['boton']['Editar'] = array(
                "<button class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        data-toggle='modal' data-target='#formModal'
                        data-keyboard='false' data-backdrop='static'
                        idAjusteSalarial='$clavePrimaria'
                        descipcion='El Usuario a Modificado un Ajuste Salarial'
                        titulo=' Modificar Ajuste Salarial'
                        title='Editar'>
                    <i class='fa fa-edit' style='color: #ffffff;'></i>
                </button>
                ",
                'if( $i["ind_estado"] =="PR") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('NM-01-01-05-01-04-C',$rol) AND $estatus == 'PR') {
            $campos['boton']['Conformar'] =
                '<button class="conformar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static"
                        idAjusteSalarial="'.$clavePrimaria.'"
                        descipcion="El Usuario a Conformado un Ajuste Salarial"
                        titulo="<i class=\'icm icm-coin\'></i> Conformar Ajuste Salarial"
                        title="Conformar">
                    <i class="icm icm-rating2" style="color: #ffffff;"></i>
                </button>
                ';
        } else {
            $campos['boton']['Conformar'] = false;
        }

        if (in_array('NM-01-01-05-01-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static"
                        idAjusteSalarial="'.$clavePrimaria.'"
                        descipcion="El Usuario esta Viendo un Ajuste Salarial"
                        titulo="<i class=\'icm icm-coins\'></i> Ver Ajuste Salarial"
                        title="Ver">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }
}