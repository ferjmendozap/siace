<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class acumuladoControlador extends Controlador
{

    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atTipoNomina;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atAcumuladoPrestaciones = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        #periodos registrados con nominas ejecutadas
        if(null!=Session::metObtener('ENMS')){
            $filtro = Session::metObtener('ENMS');
        } else {
            $filtro = "a";
        }
        $periodos = $this->atAcumuladoPrestaciones->metListarPeriodos($filtro);

        $tiposNomina = array(); #total Periodos por Nomina
        $periodosRegistrados = array(); #total Periodos por Nomina
        $cantidadPeriodo = array(); #total Periodos por Nomina
        foreach ($periodos as $periodo) {
            if(!isset($tiposNomina[$periodo['fk_nmb001_num_tipo_nomina']])) {
                $tiposNomina[$periodo['fk_nmb001_num_tipo_nomina']] = 1;
            }
            if($periodo['ind_estado']!='CR') {
                $totalPeriodosAbiertos = 1;
            } else {
                $totalPeriodosAbiertos = 0;
            }
            if(!isset($periodosRegistrados[$periodo['fec_anio'].'-'.$periodo['fec_mes']])) {
                $periodosRegistrados[$periodo['fec_anio'].'-'.$periodo['fec_mes']] = $periodo['fec_anio'].'-'.$periodo['fec_mes'];
                $cantidadPeriodo[$periodo['fec_anio'].'-'.$periodo['fec_mes']] = $totalPeriodosAbiertos;
            } else {
                $cantidadPeriodo[$periodo['fec_anio'].'-'.$periodo['fec_mes']] += $totalPeriodosAbiertos;
            }
        }

        $periodos = array();
        foreach ($periodosRegistrados as $key=>$periodo) {
            if($cantidadPeriodo[$key]==0) {
                $periodos[]['periodo'] = $periodo;
            }
        }

        $this->atVista->assign('periodos', $periodos);
        $this->atVista->metRenderizar('index');
    }

    public function metListadoPersonas()
    {
        $js[] = 'Aplicacion/appFunciones';
        $complementosCss = array(
            'jquery-ui/jquery-ui-theme5e0a',
            'SelectMultipleTabla/css/ui.multiselect',
        );
        $complementosJs = array(
            'jquery-ui/jquery-ui.min',
            'SelectMultipleTabla/js/plugins/localisation/jquery.localisation',
            'SelectMultipleTabla/js/plugins/tmpl/jquery.tmpl.1.1.1',
            'SelectMultipleTabla/js/plugins/blockUI/jquery.blockUI',
            'SelectMultipleTabla/js/ui.multiselect',
        );
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $pk_num_tipo_nomina = $this->metObtenerInt('pk_num_tipo_nomina');
        $pk_num_proceso_periodo = $this->metObtenerInt('pk_num_proceso_periodo');
        $ind_cesados = $this->metObtenerInt('ind_cesados');
        $pk_num_tipo_nomina_periodo = $this->metObtenerInt('pk_num_tipo_nomina_periodo');

        if ($pk_num_tipo_nomina && $pk_num_proceso_periodo) {
            $procesos = $this->atAcumuladoPrestaciones->metListaEmpleadosEjecucionProcesos2($pk_num_tipo_nomina_periodo);
            $estado = count($procesos);

            $listadoEmpleados = $this->atAcumuladoPrestaciones->metListaEmpleadosNomina($pk_num_tipo_nomina, $ind_cesados);
            $codProceso = $this->atAcumuladoPrestaciones->metObtenerProceso($pk_num_proceso_periodo);
            for ($i = 0; $i < count($listadoEmpleados); $i++) {
                if ($codProceso['cod_proceso'] == 'GPA' || $codProceso['cod_proceso'] == 'RPA') {
                    $fecIngreso = str_getcsv($listadoEmpleados[$i]['fec_ingreso'], '-');
                    if ($fecIngreso[0] <= 1997) {
                        if ($fecIngreso[0] < 1997) {
                            $fecIngreso = '1997-06-19';
                        } else {
                            if ($fecIngreso[1] <= 06) {
                                if ($fecIngreso[2] <= 19) {
                                    $fecIngreso = '1997-06-19';
                                } else {
                                    $fecIngreso = '1997-06' . '-' . $fecIngreso[2];
                                }
                            } else {
                                $fecIngreso = '1997-' . $fecIngreso[1] . '-' . $fecIngreso[2];
                            }
                        }
                    } else {
                        $fecIngreso = "$fecIngreso[0]-$fecIngreso[1]-$fecIngreso[2]";
                    }
                    $periodo = strtotime($fecIngreso);
                    $periodo = strtotime('+1 month', $periodo);
                    $periodo = date('Y-m-d', $periodo);
                    $periodoNomina = str_getcsv($codProceso['fec_hasta'], '-');
                    $edad = $this->metCalcularEdad($periodo, $codProceso['fec_hasta']);
                    $periodo = str_getcsv($periodo, '-');
                    $año = $edad['A'] - 1;
                    if ($periodo[1] == $periodoNomina[1]) {
                        if ($año > 0) {
                            $listadoEmpleado[] = $listadoEmpleados[$i];
                        }
                    }
                } else {
                    $listadoEmpleado[] = $listadoEmpleados[$i];
                }
            }
            if (!isset($listadoEmpleado)) {
                $listadoEmpleado = false;
            }
            $this->atVista->assign('listaEmpleados', $listadoEmpleado);
            $this->atVista->assign('pk_num_proceso_periodo', $pk_num_proceso_periodo);
            $this->atVista->assign('pk_num_tipo_nomina_periodo', $pk_num_tipo_nomina_periodo);
            $this->atVista->assign('pk_num_tipo_nomina', $pk_num_tipo_nomina);
            $this->atVista->assign('estado', $estado);
            $this->atVista->metRenderizar('listaEmpleados', 'modales');
        }
    }

    public function metGenerarPrestaciones()
    {
        $periodo = $this->metObtenerFormulas('periodo');

        if($periodo != NULL AND $periodo != '0') {
            $validacion['estatus'] = 'ok';
            if(null!=Session::metObtener('ENMS')){
                $filtro = Session::metObtener('ENMS');
            } else {
                $filtro = "a";
            }

            $empleados = $this->atAcumuladoPrestaciones->metListarEmpleados(substr($periodo,0,4),substr($periodo,5,2),$filtro);
            foreach ($empleados as $empleado) {

                $this->atArgsIdEmpleado = $empleado['pk_num_empleado'];
                $this->atArgsPrediodoHasta = $periodo.'-31';

                $periodoAnterior = $this->metPeriodoAnterior($periodo);
                $consultaPeriodoAnterior = $this->atAcumuladoPrestaciones->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $periodoAnterior, 'GPS');

                if ($consultaPeriodoAnterior['num_dias_trimestre'] == '5') {
                    if ($consultaPeriodoAnterior['num_monto_trimestral'] == '0.000000' ) {
                        $diasTrimestre = 10;
                    } else {
                        $diasTrimestre = 5;
                    }
                } elseif ($consultaPeriodoAnterior['num_dias_trimestre'] == '10') {
                    if ($consultaPeriodoAnterior['num_monto_trimestral'] == '0.000000' ) {
                        $diasTrimestre = 15;
                    } else {
                        $diasTrimestre = 5;
                    }
                } elseif ($consultaPeriodoAnterior['num_dias_trimestre'] == '15') {
                    $diasTrimestre = 5;
                } else {
                    $diasTrimestre = 5;
                }

                $this->atArgsFecIngreso = $empleado['fec_ingreso'];
                $asignaciones = $this->atAcumuladoPrestaciones->metObtenerConceptosGeneradosPeriodo(substr($periodo,0,4),substr($periodo,5,2),$empleado['pk_num_empleado'],"OR nm_b003_tipo_proceso.cod_proceso = 'BE' OR nm_b003_tipo_proceso.cod_proceso = 'BF'");


                $totalAsignaciones = 0;
                $sueldoBasico = 0;
                $bonoEspecial = 0;
                $bonoFiscal = 0;

                $bonoEspecialR = 0;
                $bonoFiscalR = 0;

                $sueldoBasicoR = 0;
                $totalAsignacionesR = 0;
                $sueldoNormalR = 0;
                $alicVacacionalR = 0;
                $alicFinAñoR = 0;
                $totalRemuneracionMensualR = 0;
                $totalRemuneracionDiariaR = 0;

                foreach ($asignaciones as $asignacion) {
                    if($asignacion['cod_proceso']!='RTA') {
                        $cual = 1;

                        if($asignacion['ind_abrebiatura']=='SB' OR
                            $asignacion['ind_abrebiatura']=='DSB' OR
                            $asignacion['ind_abrebiatura']=='DIF AF' OR
                            $asignacion['ind_abrebiatura']=='R' OR
                            $asignacion['ind_abrebiatura']=='RTA') {
                            $sueldoBasico += $asignacion['num_monto'];
                        }
                        if(
                            $asignacion['ind_abrebiatura']=='PG' OR
                            $asignacion['ind_abrebiatura']=='PJERAEN' OR
                            $asignacion['ind_abrebiatura']=='PJR' OR
                            $asignacion['ind_abrebiatura']=='PH' OR
                            $asignacion['ind_abrebiatura']=='PAAF' OR
                            $asignacion['ind_abrebiatura']=='PP' OR
                            $asignacion['ind_abrebiatura']=='PA' OR
                            $asignacion['ind_abrebiatura']=='BP'
                        ) {
                            $totalAsignaciones += $asignacion['num_monto'];
//                        var_dump($asignacion['num_monto'].'--'.$asignacion['ind_abrebiatura'].'--'.$asignacion['cod_proceso']);
                        }
                        if($asignacion['ind_abrebiatura']=='BE') {
                            $bonoEspecial += $asignacion['num_monto'];
                        }
                        if($asignacion['ind_abrebiatura']=='BF') {
                            $bonoFiscal += $asignacion['num_monto'];
                        }

                    } else {

                        $cual = 2;

                        if($asignacion['ind_abrebiatura']=='RTA' OR
                            $asignacion['ind_abrebiatura']=='DSB') {
                            $sueldoBasicoR += $asignacion['num_monto'];
                        }
                        if(
                            $asignacion['ind_abrebiatura']=='PG' OR
                            $asignacion['ind_abrebiatura']=='PJERAEN' OR
                            $asignacion['ind_abrebiatura']=='PJR' OR
                            $asignacion['ind_abrebiatura']=='PH' OR
                            $asignacion['ind_abrebiatura']=='PP' OR
                            $asignacion['ind_abrebiatura']=='PA' OR
                            $asignacion['ind_abrebiatura']=='PAAF' OR
                            $asignacion['ind_abrebiatura']=='BP'
                        ) {
                            $totalAsignacionesR += $asignacion['num_monto'];
//                        var_dump($asignacion['num_monto'].'--'.$asignacion['ind_abrebiatura'].'--'.$asignacion['cod_proceso']);
                        }
                        if($asignacion['ind_abrebiatura']=='BE') {
                            $bonoEspecialR += $asignacion['num_monto'];
                        }
                        if($asignacion['ind_abrebiatura']=='BF') {
                            $bonoFiscalR += $asignacion['num_monto'];
                        }
                    }


                }

                $sueldoNormal = $sueldoBasico+$totalAsignaciones;
                $diasAlic = $this->metDiasAlicuota(1);
                $alicVacacional = (($sueldoNormal / 30) * $diasAlic) / 12;
                $alicFinAño = ((($sueldoNormal + $alicVacacional) / 30) * 120) / 12;

                $totalRemuneracionMensual = $sueldoNormal + $alicVacacional + $alicFinAño + $bonoEspecial + $bonoFiscal;
                $totalRemuneracionDiaria = $totalRemuneracionMensual / 30;

                $añosServicioContraloria = $this->metCalcularEdad($this->atArgsFecIngreso, date('Y-m-d'));

                $montoAnual = 0;
                $empleado['fec_ingreso'] = str_getcsv($empleado['fec_ingreso'], '-');
                $diasAnual = 0;

                $eliminar = $this->atAcumuladoPrestaciones->metEliminarDatosPrestacionesEmpleadoPeriodo(substr($periodo,0,4),substr($periodo,5,2),$empleado['pk_num_empleado']);
                $validacion['eliminar'] = $eliminar;
                $idPrestaciones = $this->atAcumuladoPrestaciones->metRegistrarPrestacionesSociales(
                    $this->atArgsIdEmpleado, $empleado['fk_nmb001_num_tipo_nomina'], $periodo,
                    $sueldoBasico, $totalAsignaciones, $sueldoNormal, $alicVacacional,
                    $alicFinAño, $bonoEspecial, $bonoFiscal, $totalRemuneracionMensual,
                    $totalRemuneracionDiaria, $diasTrimestre, 'FIN', $montoAnual, $diasAnual
                );
                $validacion['id'] = $idPrestaciones;
                if (is_array($idPrestaciones)) {
                    $validacion['status'] = 'errorSQL';
                    $validacion['mensaje'] = 'Error al Registrar Las Prestaciones Sociales';
                    echo json_encode($validacion);
                    exit;
                }


                if($cual==2) {

                    $sueldoNormalR = $sueldoBasicoR+$totalAsignacionesR;
                    $diasAlicR = $this->metDiasAlicuota(1);
                    $alicVacacionalR = (($sueldoNormalR / 30) * $diasAlicR) / 12;
                    $alicFinAñoR = ((($sueldoNormalR + $alicVacacionalR) / 30) * 120) / 12;

                    $totalRemuneracionMensualR = $sueldoNormalR + $alicVacacionalR + $alicFinAñoR + $bonoEspecialR + $bonoFiscalR;
                    $totalRemuneracionDiariaR = $totalRemuneracionMensualR / 30;

                    $añosServicioContraloria = $this->metCalcularEdad($this->atArgsFecIngreso, date('Y-m-d'));

                    $montoAnualR = 0;
                    $diasAnualR = 0;

                    $eliminar = $this->atAcumuladoPrestaciones->metEliminarDatosPrestacionesEmpleadoPeriodoRetroactivo(substr($periodo, 0, 4), substr($periodo, 5, 2), $empleado['pk_num_empleado']);
                    $validacion['eliminar'] = $eliminar;
                    $idPrestaciones = $this->atAcumuladoPrestaciones->metRegistrarPrestacionesSociales(
                        $this->atArgsIdEmpleado, $empleado['fk_nmb001_num_tipo_nomina'], $periodo,
                        $sueldoBasicoR, $totalAsignacionesR, $sueldoNormalR, $alicVacacionalR,
                        $alicFinAñoR, $bonoEspecialR, $bonoFiscalR, $totalRemuneracionMensualR,
                        $totalRemuneracionDiariaR, $diasTrimestre, 'RTA', $montoAnualR, $diasAnualR
                    );
                    $validacion['id'] = $idPrestaciones;
                    if (is_array($idPrestaciones)) {
                        $validacion['status'] = 'errorSQL';
                        $validacion['mensaje'] = 'Error al Registrar Las Prestaciones Sociales';
                        echo json_encode($validacion);
                        exit;
                    }
                }

                $validacion['idEmpleado'][$this->atArgsIdEmpleado]['nombre'] = $empleado['ind_nombre1'].' '.$empleado['ind_apellido1'];
                $validacion['idEmpleado'][$this->atArgsIdEmpleado]['sueldoBasico'] = number_format($sueldoBasico+$sueldoBasicoR,2,',','.');
                $validacion['idEmpleado'][$this->atArgsIdEmpleado]['totalAsignaciones'] = number_format($totalAsignaciones+$totalAsignacionesR,2,',','.');
                $validacion['idEmpleado'][$this->atArgsIdEmpleado]['sueldoNormal'] = number_format($sueldoNormal+$sueldoNormalR,2,',','.');
                $validacion['idEmpleado'][$this->atArgsIdEmpleado]['alicVacacional'] = number_format($alicVacacional+$alicVacacionalR,2,',','.');
                $validacion['idEmpleado'][$this->atArgsIdEmpleado]['alicFinAnio'] = number_format($alicFinAño+$alicFinAñoR,2,',','.');
                $validacion['idEmpleado'][$this->atArgsIdEmpleado]['totalRemuneracionMensual'] = number_format($totalRemuneracionMensual+$totalRemuneracionMensualR,2,',','.');
                $validacion['idEmpleado'][$this->atArgsIdEmpleado]['totalRemuneracionDiaria'] = number_format($totalRemuneracionDiaria+$totalRemuneracionDiariaR,2,',','.');
            }
        } else {
            $validacion['estatus'] = 'errorPeriodo';

        }

        echo json_encode($validacion);
        exit;

    }

    public function metGenerarPrestaciones2()
    {

        $valido = $this->metObtenerInt('valido');
        if ($valido == 1) {
            $validacion = $this->metValidarFormArrayDatos('form', 'int');
            $idEmpleados = $validacion['pk_num_empleado'];
            $estado = $this->metObtenerInt('estado');
            #Obtengo los Datos de la Nomina

            $datosNomina = $this->atAcumuladoPrestaciones->metObtenerDatosNomina($validacion['pk_num_tipo_nomina'], $validacion['pk_num_proceso_periodo']);

            if ($estado == 2) {

                #Cargo los datos de la Nomina
                $this->atArgsTipoNomina = $datosNomina['pk_num_tipo_nomina'];
                $this->atArgsPagoMensual = $datosNomina['num_flag_pago_mensual'];
                $this->atArgsCodTipoNomina = $datosNomina['cod_tipo_nomina'];
                $this->atArgsTipoProceso = $datosNomina['pk_num_tipo_proceso'];
                $this->atArgsCodTipoProceso = $datosNomina['cod_proceso'];
                $this->atArgsPeriodoNomina = $datosNomina['PERIODO'];
                $this->atArgsPrediodoDesde = $datosNomina['fec_desde'];
                $this->atArgsPrediodoHasta = $datosNomina['fec_hasta'];
                $this->atArgsProcesoPeriodo = $datosNomina['pk_num_proceso_periodo'];

                for ($i = 0; $i < count($idEmpleados); $i++) {
                    #Obtengo los Datos del Empleado
                    $empleado = $this->atAcumuladoPrestaciones->metObtenerEmpleado($idEmpleados[$i]);
                    $empleadoCesado = $this->atAcumuladoPrestaciones->metObtenerEmpleadoCesado($empleado['pk_num_empleado'], $empleado['fec_ingreso']);

                    #Cargo los Argumentos Con los Datos del Empleado
                    $this->atArgsIdEmpleado = $empleado['pk_num_empleado'];
                    $this->atArgsFecIngreso = $empleado['fec_ingreso'];
                    $this->atArgsFecEgreso = $empleadoCesado['fec_fecha'];

                        if (
                            $this->atArgsCodTipoProceso == 'FIN' || $this->atArgsCodTipoProceso == 'GPS' ||
                            $this->atArgsCodTipoProceso == 'GPA' || $this->atArgsCodTipoProceso == 'RPA' ||
                            $this->atArgsCodTipoProceso == 'RPS' || $this->atArgsCodTipoProceso == 'RTA'
                        ) {
                            if ($this->atArgsCodTipoProceso == 'FIN' || $this->atArgsCodTipoProceso == 'RTA') {

                                $periodoAnterior = $this->metPeriodoAnterior($this->atArgsPeriodoNomina);
                                $consultaPeriodoAnterior = $this->atAcumuladoPrestaciones->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $periodoAnterior, $this->atArgsCodTipoProceso);


                                if ($consultaPeriodoAnterior['num_dias_trimestre'] == '5') {
                                    if ($consultaPeriodoAnterior['num_monto_trimestral'] == '0.000000') {
                                        $diasTrimestre = 10;
                                    } else {
                                        $diasTrimestre = 5;
                                    }
                                } elseif ($consultaPeriodoAnterior['num_dias_trimestre'] == '10') {
                                    if ($consultaPeriodoAnterior['num_monto_trimestral'] == '0.000000') {
                                        $diasTrimestre = 15;
                                    } else {
                                        $diasTrimestre = 5;
                                    }
                                } elseif ($consultaPeriodoAnterior['num_dias_trimestre'] == '15') {
                                    $diasTrimestre = 5;
                                } else {
                                    $diasTrimestre = 5;
                                }

                                $consultaPeriodoActual = $this->atAcumuladoPrestaciones->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $this->atArgsCodTipoProceso);


                                $sueldoBasico = $this->metSueldoPrestaciones();

                                $asignaciones = $this->metObtenerTipoConceptoCalculados('I', 1, false, 1);

                                $sueldoNormal = $sueldoBasico + $asignaciones;
                                $diasAlic = $this->metDiasAlicuota(1);
                                $alicVacacional = (($sueldoNormal / 30) * $diasAlic) / 12;
                                $alicFinAño = ((($sueldoNormal + $alicVacacional) / 30) * 120) / 12;
                                $bonoEspecial = $this->metObtenerConcepto('BESP', $this->atArgsPeriodoNomina, 'BESP');
                                $bonoFiscal = $this->metObtenerConcepto('BFIS', $this->atArgsPeriodoNomina, 'BFIS');
                                $totalRemuneracionMensual = $sueldoNormal + $alicVacacional + $alicFinAño + $bonoEspecial + $bonoFiscal;
                                $totalRemuneracionDiaria = $totalRemuneracionMensual / 30;

                                if (!$consultaPeriodoActual) {
                                    $idPrestaciones = $this->atAcumuladoPrestaciones->metRegistrarPrestacionesSociales(
                                        $this->atArgsIdEmpleado, $this->atArgsTipoNomina, $this->atArgsPeriodoNomina,
                                        $sueldoBasico, $asignaciones, $sueldoNormal, $alicVacacional,
                                        $alicFinAño, $bonoEspecial, $bonoFiscal, $totalRemuneracionMensual,
                                        $totalRemuneracionDiaria, $diasTrimestre, $this->atArgsCodTipoProceso
                                    );
                                } else {
                                    $idPrestaciones = $this->atAcumuladoPrestaciones->metActualizarPrestacionesSociales(
                                        $this->atArgsIdEmpleado, $this->atArgsTipoNomina, $this->atArgsPeriodoNomina,
                                        $sueldoBasico, $asignaciones, $sueldoNormal, $alicVacacional,
                                        $alicFinAño, $bonoEspecial, $bonoFiscal, $totalRemuneracionMensual,
                                        $totalRemuneracionDiaria, $diasTrimestre, $this->atArgsCodTipoProceso,
                                        $consultaPeriodoActual['pk_num_prestaciones_sociales_calculo']
                                    );
                                }
                                if (is_array($idPrestaciones)) {
                                    $validacion['status'] = 'errorSQL';
                                    $validacion['mensaje'] = 'Error al Registrar Las Prestaciones Sociales';
                                    echo json_encode($validacion);
                                    exit;
                                }

                                $procentaje = $this->atAcumuladoPrestaciones->metUltimosIntereses();
                                $acumuladoPrestaciones = $this->atAcumuladoPrestaciones->metAcumuladoPrestacionesIntereses($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina);
                                $acumuladoPrestacionesRetro = $this->atAcumuladoPrestaciones->metAcumuladoPrestacionesRetroIntereses($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina);
                                $montoAcumulado =
                                    ($acumuladoPrestaciones['num_monto_trimestral'] + $acumuladoPrestaciones['num_monto_anual']) +
                                    ($acumuladoPrestacionesRetro['num_monto_trimestral'] + $acumuladoPrestacionesRetro['num_monto_anual']);
                                $fecha = new DateTime($this->atArgsPeriodoNomina);
                                $fecha->modify('last day of this month');
                                if (date('Y') % 4 == 0) {
                                    $diasAnno = 366;
                                } else {
                                    $diasAnno = 365;
                                }
                                $montoProcentaje = ((($montoAcumulado) * ($procentaje['num_activa'] / 100)) / $diasAnno) * $fecha->format('d');

                                $validarInteresMes = $this->atAcumuladoPrestaciones->metBuscarPrestacionesSocialesIntereses($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina);
                                if (!$validarInteresMes) {
                                    $idIntereses = $this->atAcumuladoPrestaciones->metRegistrarIntereses($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $montoAcumulado, $montoProcentaje, $procentaje['num_activa']);
                                } else {
                                    $idIntereses = $this->atAcumuladoPrestaciones->metActualizarIntereses($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $montoAcumulado, $montoProcentaje, $procentaje['num_activa'], $validarInteresMes['pk_num_prestaciones_sociales_intereses']);
                                }
                                if (is_array($idIntereses)) {
                                    $validacion['status'] = 'errorSQL';
                                    $validacion['mensaje'] = 'Error al Registrar Las Prestaciones Sociales';
                                    echo json_encode($validacion);
                                    exit;
                                }

                            }
                            elseif ($this->atArgsCodTipoProceso == 'GPA' || $this->atArgsCodTipoProceso == 'RPA') {
                                $consultaPeriodoActual = $this->atAcumuladoPrestaciones->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $this->atArgsCodTipoProceso);
                                $añosServicioContraloria = $this->metCalcularEdad($this->atArgsFecIngreso, $this->atArgsPrediodoHasta);
                                $años = $añosServicioContraloria['A'];
                                if ($años < 2) {
                                    $totalAños = 0;
                                } elseif (($años >= 2 && $años <= 16)) {
                                    $años = $años - 1;
                                    $totalAños = $años * 2;
                                } elseif ($años > 16) {
                                    $totalAños = 30;
                                }

                                if ($totalAños != 0) {

                                    $historico = $this->atAcumuladoPrestaciones->metHistoricoPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $this->atArgsCodTipoProceso);
                                    if ($this->atArgsCodTipoProceso != 'RPA') {
                                        $historicoRetroactivo = $this->atAcumuladoPrestaciones->metHistoricoPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, 'RPA');
                                        if (isset($historicoRetroactivo['num_remuneracion_diaria'])) {
                                            $montoAnualRPA = $historicoRetroactivo['num_remuneracion_diaria'];
                                        } else {
                                            $montoAnualRPA = 0;
                                        }
                                    } else {
                                        $montoAnualRPA = 0;
                                    }

                                    if (isset($historico['num_remuneracion_diaria'])) {
                                        $montoAnual = $historico['num_remuneracion_diaria'];
                                    } else {
                                        $montoAnual = 0;
                                    }
                                    $montoAnual = ($montoAnual + $montoAnualRPA) * $totalAños;
                                    $idPrestacionesSociales = $this->atAcumuladoPrestaciones->metActualizarPrestacionesSocialesAdicionales($this->atArgsIdEmpleado, $this->atArgsTipoNomina, $consultaPeriodoActual['pk_num_prestaciones_sociales_calculo'], $this->atArgsPeriodoNomina, $montoAnual, $totalAños, $this->atArgsCodTipoProceso);
                                    if (is_array($idPrestacionesSociales)) {
                                        $validacion['status'] = 'errorSQL';
                                        $validacion['mensaje'] = 'Error al Registrar Las Prestaciones Sociales';
                                        echo json_encode($validacion);
                                        exit;
                                    }
                                    if ($this->atArgsCodTipoProceso == 'GPA') {
                                        $idConcepto = GPA;
                                    } else {
                                        $idConcepto = RPA;
                                    }
                                    $registrarCalculos = $this->atAcumuladoPrestaciones->metRegistrarCalculos($this->atArgsIdEmpleado, $this->atArgsProcesoPeriodo, $idConcepto, $montoAnual, 1);
                                    if (is_array($registrarCalculos)) {
                                        $validacion['status'] = 'errorSQL';
                                        $validacion['mensaje'] = 'Error al Registrar el Concepto';
                                        echo json_encode($validacion);
                                        exit;
                                    }
                                }
                            } elseif ($this->atArgsCodTipoProceso == 'GPS' || $this->atArgsCodTipoProceso == 'RPS') {
                                $consultaPeriodoActual = $this->atAcumuladoPrestaciones->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, $this->atArgsCodTipoProceso);
                                if ($this->atArgsCodTipoProceso == 'GPS') {
                                    $consultaPeriodoActualRPS = $this->atAcumuladoPrestaciones->metBuscarPrestacionesSociales($this->atArgsIdEmpleado, $this->atArgsPeriodoNomina, 'RPS');
                                    $consultaPeriodoActualRPS['num_remuneracion_diaria'];
                                    if (isset($consultaPeriodoActualRPS['num_remuneracion_diaria'])) {
                                        $montoTrimestralRPS = $consultaPeriodoActualRPS['num_remuneracion_diaria'];
                                    } else {
                                        $montoTrimestralRPS = 0;
                                    }
                                } else {
                                    $montoTrimestralRPS = 0;
                                }
                                if (isset($consultaPeriodoActual['num_remuneracion_diaria'])) {
                                    $montoTrimestral = $consultaPeriodoActual['num_remuneracion_diaria'];
                                } else {
                                    $montoTrimestral = 0;
                                }
                                if ($montoTrimestral != 0) {

                                    $montoTrimestral = ($montoTrimestral + $montoTrimestralRPS) * $consultaPeriodoActual['num_dias_trimestre'];
                                    $idPrestacionesSociales = $this->atAcumuladoPrestaciones->metActualizarPrestacionesSocialesTrimestre($this->atArgsIdEmpleado, $this->atArgsTipoNomina, $consultaPeriodoActual['pk_num_prestaciones_sociales_calculo'], $this->atArgsPeriodoNomina, $montoTrimestral, $this->atArgsCodTipoProceso);
                                    if (is_array($idPrestacionesSociales)) {
                                        $validacion['status'] = 'errorSQL';
                                        $validacion['mensaje'] = 'Error al Registrar Las Prestaciones Sociales';
                                        echo json_encode($validacion);
                                        exit;
                                    }
                                    if ($this->atArgsCodTipoProceso == 'GPS') {
                                        $idConcepto = GPS;
                                    } else {
                                        $idConcepto = RPS;
                                    }
                                    $buscarPrestaciones = $this->atAcumuladoPrestaciones->metBuscarLimpiarPrestaciones($this->atArgsProcesoPeriodo, $this->atArgsIdEmpleado);
                                   // var_dump($buscarPrestaciones);
                                    foreach ($buscarPrestaciones AS $pk){
                                        $LimpiarNominaEmpleado = $this->atAcumuladoPrestaciones->metLimpiarPrestaciones($pk['pk_num_tiponomina_empleadoconcepto']);
                                    }
                                    $registrarCalculos = $this->atAcumuladoPrestaciones->metRegistrarCalculos($this->atArgsIdEmpleado, $this->atArgsProcesoPeriodo, $idConcepto, $montoTrimestral, 1);
                                    if (is_array($registrarCalculos)) {
                                        $validacion['status'] = 'errorSQL';
                                        $validacion['mensaje'] = 'Error al Registrar el Concepto';
                                        echo json_encode($validacion);
                                        exit;
                                    }
                                }
                            }
                        }
                    }
                $validacion['status'] = 'generada';
                $cambiarEstadoNomina = $this->atAcumuladoPrestaciones->metCambiarEstadoGenerado($this->atArgsProcesoPeriodo);
                if (is_array($cambiarEstadoNomina)) {
                    $validacion['status'] = 'errorSQL';
                    $validacion['mensaje'] = 'Error al cambiar el estado de la Nomina';
                    echo json_encode($validacion);
                    exit;
                }
                echo json_encode($validacion);
                exit;
            }elseif($datosNomina['ind_estado'] == 'GE') {
                $validacion['status'] = 'error';
                $validacion['mensaje'] = 'Disculpe pero no puede generar Prestaciones Sociales';
                echo json_encode($validacion);
                exit;
            }
        }
    }
}
