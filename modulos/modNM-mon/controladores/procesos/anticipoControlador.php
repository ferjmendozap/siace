<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class anticipoControlador extends Controlador
{

    private $atAnticipo;

    public function __construct()
    {
        parent::__construct();
        $this->atAnticipo = $this->metCargarModelo('anticipo', 'procesos');
    }

    public function metIndex($aprobarAnticipo = false)
    {
        if ($aprobarAnticipo) {
            $complementosCss = array(
                'DataTables/jquery.dataTables',
                'DataTables/extensions/dataTables.colVis941e',
                'DataTables/extensions/dataTables.tableTools4029',
            );
            $js[] = 'materialSiace/core/demo/DemoTableDynamic';
            $js[] = 'Aplicacion/appFunciones';
            $this->atVista->metCargarCssComplemento($complementosCss);
            $this->atVista->metCargarJs($js);
//            $this->atVista->assign('listado', $this->atAnticipo->metListarAnticipos(false, false, false, true));
            $this->atVista->metRenderizar('listado');
        } else {
            $this->atVista->metRenderizar('seleccion');
        }
    }

    public function metAnticipoEmpleado()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $this->atVista->assign('lista', $this->atAnticipo->metListarAnticipos($idEmpleado));
        $this->atVista->metRenderizar('listaAnticipo', 'modales');
    }

    public function metCrearModificar()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $valido = $this->metObtenerInt('valido');
        $idAnticipo = $this->metObtenerInt('idAnticipo');
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        if ($valido == 1) {
            $excepcion = array('num_estatus');
            $txt = $this->metValidarFormArrayDatos('form', 'txt');
            $ind = $this->metValidarFormArrayDatos('form', 'int', $excepcion);

            if ($txt != null && $ind == null) {
                $validacion = $txt;
            } elseif ($txt == null && $ind != null) {
                $validacion = $ind;
            } else {
                $validacion = array_merge($txt, $ind);
            }
            if (in_array('error', $validacion)) {
                $validacion['status'] = 'error';
                echo json_encode($validacion);
                exit;
            }

            $validacion['num_anticipo'] = ($validacion['ind_porcentaje'] / 100) * $validacion['num_monto_acumulado'];

            if ($idAnticipo == 0) {
                $id = $this->atAnticipo->metCrearAnticipo(
                    $idEmpleado, $validacion['fk_a006_num_miscelaneo_detalle_justificativo'],
                    date('Y-m'), $validacion['num_anticipo'], $validacion['ind_porcentaje'], $validacion['fec_anticipo']
                );
                $validacion['status'] = 'nuevo';
            } else {
                $id = $this->atAnticipo->metModificarAnticipo(
                    $idEmpleado, $validacion['fk_a006_num_miscelaneo_detalle_justificativo'],
                    date('Y-m'), $validacion['num_anticipo'], $validacion['ind_porcentaje'],
                    $validacion['fec_anticipo'], $idAnticipo
                );
                $validacion['status'] = 'modificar';
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idAnticipo'] = $id;
            $validacion['num_total'] = $validacion['num_monto_acumulado'] - $validacion['num_anticipo'];
            echo json_encode($validacion);
            exit;
        }
        $this->atVista->assign('idEmpleado', $idEmpleado);
        $this->atVista->assign('idAnticipo', $idAnticipo);
        if ($idAnticipo != 0) {
            $this->atVista->assign('formDB', $this->atAnticipo->metListarAnticipos($idEmpleado, $idAnticipo, true));
        } else {
            $periodo = $this->atAnticipo->metValidarAnticipos($idEmpleado);
            if (isset($periodo['num_estatus'])) {
                if ($periodo['num_estatus'] == 0) {
                    echo 'disculpa ya tiene un anticipo esperando por su aprobación por tal motivo
                            no puede cargar otro anticipo en el sistema ';
                    exit;
                } elseif ($periodo['fec_anticipo'] > date('Y-m-d')) {
                    echo 'disculpa ya tiene un anticipo cargado en el sistema, puede volver a cargar otro para el dia ' . $periodo['fec_anticipo'];
                    exit;
                }
            }
        }
        $this->atVista->assign('lista', $this->atAnticipo->metListarAnticipos($idEmpleado, false, true));
        $this->atVista->assign('justificativo', $this->atAnticipo->metMostrarSelect('JUSTANT'));
        $this->atVista->metRenderizar('crearModificar', 'modales');
    }

    public function metAprobarAnular()
    {
        $idAnticipo = $this->metObtenerInt('idAnticipo');
        $tipo = $this->metObtenerInt('tipo');
        if ($idAnticipo) {
            if ($tipo == 'AP') {
                $tipo = false;
                $validacion['mensaje'] = array('titulo' => 'Antcipo Aprobado!!', 'contenido' => 'El anticipo fue aprobado satisfactoriamente');
            } else {
                $tipo = true;
                $validacion['mensaje'] = array('titulo' => 'Antcipo Anulado!!', 'contenido' => 'El anticipo fue anulado satisfactoriamente');
            }

            $id = $this->atAnticipo->metAprobarAnularAnticipo($idAnticipo, $tipo);
            $validacion['status'] = 'OK';

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $validacion['idAnticipo'] = $id;
            echo json_encode($validacion);
            exit;
        }
    }

    private function numtoletras($xcifra)
    {
        $xarray = array(0 => "Cero",
            1 => "UN", "DOS", "TRES", "CUATRO", "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
            "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE", "QUINCE", "DIECISEIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
            "VEINTI", 30 => "TREINTA", 40 => "CUARENTA", 50 => "CINCUENTA", 60 => "SESENTA", 70 => "SETENTA", 80 => "OCHENTA", 90 => "NOVENTA",
            100 => "CIENTO", 200 => "DOSCIENTOS", 300 => "TRESCIENTOS", 400 => "CUATROCIENTOS", 500 => "QUINIENTOS", 600 => "SEISCIENTOS", 700 => "SETECIENTOS", 800 => "OCHOCIENTOS", 900 => "NOVECIENTOS"
        );
//
        $xcifra = trim($xcifra);
        $xlength = strlen($xcifra);
        $xpos_punto = strpos($xcifra, ".");
        $xaux_int = $xcifra;
        $xdecimales = "00";
        if (!($xpos_punto === false)) {
            if ($xpos_punto == 0) {
                $xcifra = "0" . $xcifra;
                $xpos_punto = strpos($xcifra, ".");
            }
            $xaux_int = substr($xcifra, 0, $xpos_punto); // obtengo el entero de la cifra a covertir
            $xdecimales = substr($xcifra . "00", $xpos_punto + 1, 2); // obtengo los valores decimales
        }

        $XAUX = str_pad($xaux_int, 18, " ", STR_PAD_LEFT); // ajusto la longitud de la cifra, para que sea divisible por centenas de miles (grupos de 6)
        $xcadena = "";
        for ($xz = 0; $xz < 3; $xz++) {
            $xaux = substr($XAUX, $xz * 6, 6);
            $xi = 0;
            $xlimite = 6; // inicializo el contador de centenas xi y establezco el límite a 6 dígitos en la parte entera
            $xexit = true; // bandera para controlar el ciclo del While
            while ($xexit) {
                if ($xi == $xlimite) // si ya llegó al límite m&aacute;ximo de enteros
                {
                    break; // termina el ciclo
                }

                $x3digitos = ($xlimite - $xi) * -1; // comienzo con los tres primeros digitos de la cifra, comenzando por la izquierda
                $xaux = substr($xaux, $x3digitos, abs($x3digitos)); // obtengo la centena (los tres dígitos)
                for ($xy = 1; $xy < 4; $xy++) // ciclo para revisar centenas, decenas y unidades, en ese orden
                {
                    switch ($xy) {
                        case 1: // checa las centenas
                            if (substr($xaux, 0, 3) < 100) // si el grupo de tres dígitos es menor a una centena ( < 99) no hace nada y pasa a revisar las decenas
                            {
                            } else {
                                if(substr($xaux, 1, 2)=='00'){
                                    $xseek = $xarray[substr($xaux, 0, 3)]; // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                }else{
                                    $xseek=false;
                                }

                                if ($xseek) {
                                    $xsub = $this->subfijo($xaux); // devuelve el $this->subfijo correspondiente (Millón, Millones, Mil o nada)
                                    if (substr($xaux, 0, 3) == 100)
                                        $xcadena = " " . $xcadena . " CIEN " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3; // la centena fue redonda, entonces termino el ciclo del for y ya no reviso decenas ni unidades
                                } else // entra aquí si la centena no fue numero redondo (101, 253, 120, 980, etc.)
                                {
                                    $xseek = $xarray[substr($xaux, 0, 1) * 100]; // toma el primer caracter de la centena y lo multiplica por cien y lo busca en el arreglo (para que busque 100,200,300, etc)
                                    $xcadena = " " . $xcadena . " " . $xseek;
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 0, 3) < 100)
                            break;
                        case 2: // checa las decenas (con la misma lógica que las centenas)
                            if (substr($xaux, 1, 2) < 10) {
                            } else {
                                if(substr($xaux, 2, 1)=='0'){
                                    $xseek = $xarray[substr($xaux, 1, 2)]; // busco si la centena es número redondo (100, 200, 300, 400, etc..)
                                }else{
                                    $xseek=false;
                                }
                                if ($xseek) {
                                    $xsub = $this->subfijo($xaux);
                                    if (substr($xaux, 1, 2) == 20)
                                        $xcadena = " " . $xcadena . " VEINTE " . $xsub;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                                    $xy = 3;
                                } else {
                                    $xseek = $xarray[substr($xaux, 1, 1) * 10];
                                    if (substr($xaux, 1, 1) * 10 == 20)
                                        $xcadena = " " . $xcadena . " " . $xseek;
                                    else
                                        $xcadena = " " . $xcadena . " " . $xseek . " Y ";
                                } // ENDIF ($xseek)
                            } // ENDIF (substr($xaux, 1, 2) < 10)
                            break;
                        case 3: // checa las unidades
                            if (substr($xaux, 2, 1) < 1) // si la unidad es cero, ya no hace nada
                            {
                            } else {
                                $xseek = $xarray[substr($xaux, 2, 1)]; // obtengo directamente el valor de la unidad (del uno al nueve)
                                $xsub = $this->subfijo($xaux);
                                $xcadena = " " . $xcadena . " " . $xseek . " " . $xsub;
                            } // ENDIF (substr($xaux, 2, 1) < 1)
                            break;
                    } // END SWITCH
                } // END FOR
                $xi = $xi + 3;
            } // ENDDO

            if (substr(trim($xcadena), -5, 5) == "ILLON") // si la cadena obtenida termina en MILLON o BILLON, entonces le agrega al final la conjuncion DE
                $xcadena .= " DE";

            if (substr(trim($xcadena), -7, 7) == "ILLONES") // si la cadena obtenida en MILLONES o BILLONES, entoncea le agrega al final la conjuncion DE
                $xcadena .= " DE";

            // ----------- esta línea la puedes cambiar de acuerdo a tus necesidades o a tu país -------
            if (trim($xaux) != "") {
                switch ($xz) {
                    case 0:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena .= "UN BILLON ";
                        else
                            $xcadena .= " BILLONES ";
                        break;
                    case 1:
                        if (trim(substr($XAUX, $xz * 6, 6)) == "1")
                            $xcadena .= "UN MILLON ";
                        else
                            $xcadena .= " MILLONES ";
                        break;
                    case 2:
                        if ($xcifra < 1) {
                            $xcadena = "CERO BOLIVARES CON $xdecimales/100";
                        }
                        if ($xcifra >= 1 && $xcifra < 2) {
                            $xcadena = "UN BOLIVAR CON $xdecimales/100";
                        }
                        if ($xcifra >= 2) {
                            $xcadena .= " BOLIVARES CON $xdecimales/100"; //
                        }
                        break;
                } // endswitch ($xz)
            } // ENDIF (trim($xaux) != "")
            // ------------------      en este caso, para México se usa esta leyenda     ----------------
            $xcadena = str_replace("VEINTI ", "VEINTI", $xcadena); // quito el espacio para el VEINTI, para que quede: VEINTICUATRO, VEINTIUN, VEINTIDOS, etc
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("UN UN", "UN", $xcadena); // quito la duplicidad
            $xcadena = str_replace("  ", " ", $xcadena); // quito espacios dobles
            $xcadena = str_replace("BILLON DE MILLONES", "BILLON DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("BILLONES DE MILLONES", "BILLONES DE", $xcadena); // corrigo la leyenda
            $xcadena = str_replace("DE UN", "UN", $xcadena); // corrigo la leyenda
        } // ENDFOR	($xz)
        return trim($xcadena);
    } // END FUNCTION

    private function subfijo($xx)
    { // esta función regresa un subfijo para la cifra
        $xx = trim($xx);
        $xstrlen = strlen($xx);
        if ($xstrlen == 1 || $xstrlen == 2 || $xstrlen == 3)
            $xsub = "";
        //
        if ($xstrlen == 4 || $xstrlen == 5 || $xstrlen == 6)
            $xsub = "MIL";
        //
        return $xsub;
    } // END FUNCTION

    public function metReporte($idAnticipo, $idEmpleado)
    {
        $this->metObtenerLibreria('fpdf', 'fpdf-V1.7');
        $anticipo=$this->atAnticipo->metListarAnticipos($idEmpleado, $idAnticipo, true);
        $pdf = new FPDF("P", "cm", "LETTER");
        $pdf->SetCreator("Creado automaticamente con FPDF");
        $pdf->SetAuthor("CEM");
        $pdf->SetTitle("HISTORICO DEL EMPLEADO");
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $pdf->SetLineWidth(0.05);
        $pdf->SetLeftMargin(1.0);
        //$pdf->Rect( 1.3, 1.0,18.5, 25.8, 'D');
        $pdf->image(ROOT . DS . 'publico' . DS . 'imagenes' . DS . 'logos' . DS . "CEM.jpg", 1.6, 0.6, 2.5, 2);

        $pdf->SetFont('Arial', '', 11);
        $pdf->SetLineWidth(0.00);
        $pdf->SetXY(2.9, 2.7);
        $pdf->write(0.5, 'SOLICITUD DE ANTICIPO AL FONDO FIDUCIARIO DE LOS TRABAJADORES DE:');

        $pdf->SetFont('Arial', 'B', 11);
        $pdf->SetLineWidth(0.00);
        $pdf->SetXY(6, 3.3);
        $pdf->write(0.5, '"CONTRALORIA  DEL ESTADO MONAGAS"');

        $pdf->SetFont('Arial', 'U', 11);
        $pdf->SetXY(5, 4.2);
        $pdf->write(0.5, '  Maturin  ');
        $Fecha = str_getcsv($anticipo['fec_anticipo'], '-');
        $pdf->SetXY(14, 4.2);
        $pdf->write(0.5, $Fecha[2] . '-' . $Fecha[1] . '-' . $Fecha[0]);

        $pdf->SetFont('Arial', 'B', 11);
        $pdf->SetXY(5, 4.7);
        $pdf->write(0.5, '  LUGAR  ');

        $pdf->SetXY(14, 4.7);
        $pdf->write(0.5, '  FECHA  ');

        $pdf->SetLineWidth(0.01);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetFillColor(127, 127, 127);


        $pdf->SetFont('Arial', '', 11);
        $pdf->SetXY(1.6, 5.6);
        $pdf->MultiCell(18.5, 0.7, utf8_decode('    Yo, '.$anticipo['ind_nombre1'].' '.$anticipo['ind_nombre2'].' '.$anticipo['ind_apellido1'].' '.$anticipo['ind_apellido2'].' '.'  Mayor  de  edad,   titular  de  la Cédula de Identidad '.$anticipo['ind_cedula_documento'].', Estado Civil [estadoCivil]   procediendo en mi carácter de Fideicomitente y Beneficiario del Fideicomiso Individual  que tengo constituido con el BANCO EXTERIOR, C.A. BANCO UNIVERSAL FIDUCIARIO, en virtud de haber suscrito el  "Contrato de Fideicomiso de Prestación de Antigüedad" para los Trabajadores de "CONTRALORIA DEL ESTADO MONAGAS" por medio de la presente solicito y declaro que recibiré en calidad de anticipo la suma de '.$this->numtoletras(round($anticipo['num_anticipo'],2)).' CTS (BSF.  '.number_format($anticipo['num_anticipo'],2,',','.').'). '), 0, 'J');

        $pdf->SetXY(3.6, 11);
        $pdf->write(0.5, utf8_decode($anticipo['ind_porcentaje'].'% de los que tengo depositado. Dicha cantidad la emplearé para:'));

        $pdf->SetXY(1.9, 11.7);
        if ($anticipo['cod_detalle'] == '1') {
            $pdf->MultiCell(1, 0.7, '( X )', 1, 'C');
        } else {
            $pdf->MultiCell(1, 0.7, '(   )', 1, 'C');
        }
        $pdf->SetXY(2.9, 11.7);
        $pdf->MultiCell(17, 0.7, utf8_decode('Construcción, adquisición, mejora o reparación de vivienda para mí y mi familia.'), 1, 'J');

        $pdf->SetXY(1.9, 12.4);
        if ($anticipo['cod_detalle'] == '2') {
            $pdf->MultiCell(1, 0.7, '( X )', 1, 'C');
        } else {
            $pdf->MultiCell(1, 0.7, '(   )', 1, 'C');
        }
        $pdf->SetXY(2.9, 12.4);
        $pdf->MultiCell(17, 0.7, utf8_decode('La liberación de hipoteca o de cualquier otro gravamen sobre vivienda de mi propiedad.'), 1, 'J');

        $pdf->SetXY(1.9, 13.1);
        if ($anticipo['cod_detalle'] == '3') {
            $pdf->MultiCell(1, 0.7, '( X )', 1, 'C');
        } else {
            $pdf->MultiCell(1, 0.7, '(   )', 1, 'C');
        }
        $pdf->SetXY(2.9, 13.1);
        $pdf->MultiCell(17, 0.7, utf8_decode('Las pensiones escolares para mí, mi conyugué, hijos o con quien haga vida marital.'), 1, 'J');

        $pdf->SetXY(1.9, 13.8);
        if ($anticipo['cod_detalle'] == '4') {
            $pdf->MultiCell(1, 0.7, '( X )', 1, 'C');
        } else {
            $pdf->MultiCell(1, 0.7, '(   )', 1, 'C');
        }
        $pdf->SetXY(2.9, 13.8);
        $pdf->MultiCell(17, 0.7, utf8_decode('Los gastos por atención médica y hospitalaria de las personas indicadas en el literal anterior.'), 1, 'J');


        $pdf->SetLineWidth(0.01);
        $pdf->SetXY(1.6, 14.9);
        $pdf->MultiCell(18.5, 0.7, utf8_decode('    Declaro conocer los límites del Artículo 144 de la Ley Orgánica del Trabajo, Los Trabajadores  y las Trabajadoras y el Contrato de Fideicomiso de los trabajadores de "CONTRALORIA DEL ESTADO MONAGAS", en consecuencia me comprometo a darle anticipo el destino señalado. Los recaudos de la justificación de este pago reposan en la empresa, de conformidad con la facultad que le confiere el Artículo 100 del Reglamento de la Ley Orgánica del Trabajo.'), 0, 'J');

        $pdf->SetLineWidth(0.01);
        $pdf->SetXY(1.6, 18.4);
        $pdf->MultiCell(18.5, 0.7, utf8_decode('    En caso de que mi liquidación por parte del Organismo, recibiré el saldo disponible de mi fondo fideicometido a mi entera satisfacción. Así mismo, doy fe que los datos solicitados en este documento son ciertos y exactos, con lo cual libero al BANCO EXTERIOR, C.A. BANCO UNIVERSAL FIDUCIARIO de toda responsabilidad, por la inexactitud de los mismos.'), 0, 'J');


        $pdf->SetLineWidth(0.01);
        $pdf->SetXY(1.6, 21.2);
        $pdf->MultiCell(18.5, 0.7, utf8_decode('    Para todos los efectos del presente documento, se elige como domicilio especial, exclusivo y excluyente, de cualquier otro, la ciudad de Caracas, a la jurisdicción de cuyos tribunales declaro someterme expresamente, sin prejuicio de que el BANCO EXTERIOR, C.A. BANCO UNIVERSAL FIDUCIARIO, pueda recurrir a otros Tribunales competentes de conformidad con las Leyes de la República.'), 0, 'J');

        $pdf->SetFont('Arial', 'UB', 11);
        $pdf->SetXY(4, 24.5);
        $pdf->write(0.5, '                                              ');

        $pdf->SetXY(13, 24.5);
        $pdf->write(0.5, '                                              ');
        $pdf->SetXY(4, 24.5);
        $pdf->write(0.5, '                                              ');

        $pdf->SetXY(13, 24.5);
        $pdf->write(0.5, '                                              ');

        $pdf->SetFont('Arial', 'B', 11);
        $pdf->SetXY(4, 25);
        $pdf->write(0.5, '  Firma del Solicitante  ');
        $pdf->SetXY(4, 25.4);
        $pdf->write(0.5, '  C.I. '.$anticipo['ind_cedula_documento']);

        $pdf->SetXY(13, 25);
        $pdf->write(0.5, '  Firma del Conyugue  ');
        $pdf->SetXY(13, 25.4);
        $pdf->write(0.5, '    ');
        $pdf->Output();
    }

    public function metJsonDataTabla($estatus = false)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  *,
                  concat_ws('-',fec_mes,fec_anio) AS fec
                FROM
                  nm_b004_ajuste_salarial 
                WHERE 1 ";
        if ($estatus) {
            $sql .= " AND ind_estado='$estatus' ";
        }

        if ($busqueda['value']) {
            $sql .= " AND
                        ( 
                        fec LIKE '%$busqueda[value]%' OR
                        ind_descripcion LIKE '%$busqueda[value]%' OR
                        ind_numero_resolucion LIKE '%$busqueda[value]%' OR
                        ind_numero_gaceta LIKE '%$busqueda[value]%' OR
                        ind_estado LIKE '%$busqueda[value]%'
                        ) ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('fec','ind_descripcion','ind_numero_resolucion','ind_numero_gaceta','ind_estado');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_ajust_salarial';
        #construyo el listado de botones

        if (in_array('NM-01-01-05-01-05-AP',$rol) AND $estatus == 'CO') {
            $campos['boton']['Aprobar'] = '
                <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static"
                        idAjusteSalarial="'.$clavePrimaria.'"
                        descipcion="El Usuario a Aprobado un Ajuste Salarial"
                        titulo="<i class=\'icm icm-coin\'></i> Aprobar Ajuste Salarial"
                        title="Aprobar">
                    <i class="icm icm-rating3" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('NM-01-01-05-01-02-M',$rol) AND $estatus != 'PR') {
            $campos['boton']['Editar'] = array(
                "<button class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary'
                        data-toggle='modal' data-target='#formModal'
                        data-keyboard='false' data-backdrop='static'
                        idAjusteSalarial='$clavePrimaria'
                        descipcion='El Usuario a Modificado un Ajuste Salarial'
                        titulo=' Modificar Ajuste Salarial'
                        title='Editar'>
                    <i class='fa fa-edit' style='color: #ffffff;'></i>
                </button>
                ",
                'if( $i["ind_estado"] =="PR") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('NM-01-01-05-01-04-C',$rol) AND $estatus == 'PR') {
            $campos['boton']['Conformar'] =
                '<button class="conformar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                        data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static"
                        idAjusteSalarial="'.$clavePrimaria.'"
                        descipcion="El Usuario a Conformado un Ajuste Salarial"
                        titulo="<i class=\'icm icm-coin\'></i> Conformar Ajuste Salarial"
                        title="Conformar">
                    <i class="icm icm-rating2" style="color: #ffffff;"></i>
                </button>
                ';
        } else {
            $campos['boton']['Conformar'] = false;
        }

        if (in_array('NM-01-01-05-01-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning"
                        data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static"
                        idAjusteSalarial="'.$clavePrimaria.'"
                        descipcion="El Usuario esta Viendo un Ajuste Salarial"
                        titulo="<i class=\'icm icm-coins\'></i> Ver Ajuste Salarial"
                        title="Ver">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }
}