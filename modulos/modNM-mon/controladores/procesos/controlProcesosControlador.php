<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class controlProcesosControlador extends Controlador
{

    private $atControlProcesos;
    private $atTipoNomina;

    public function __construct()
    {
        parent::__construct();
        $this->atControlProcesos = $this->metCargarModelo('controlProcesos', 'procesos');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
    }

    public function metIndex($status=false)
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);

//        $this->atVista->assign('listado', $this->atControlProcesos->metListarControlProcesos($status));
        $this->atVista->assign('status', $status);
        $this->atVista->metRenderizar('listado');
    }

    public function metIniciarModificarPeriodo()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $complementosCss = array(
            'select2/select201ef',
            'bootstrap-datepicker/datepicker'
        );
        $complementosJs = array(
            'select2/select2.min',
            'bootstrap-datepicker/bootstrap-datepicker'
        );
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);

        $valido = $this->metObtenerInt('valido');
        $ver = $this->metObtenerInt('ver');
        $status = $this->metObtenerAlphaNumerico('status');
        $idControlProceso = $this->metObtenerInt('idControlProceso');
        if ($valido == 1) {
            if($status == 'pr') {
                $excepcion = array('fk_nmc001_num_tipo_nomina_periodoTXT');
                $txt = $this->metValidarFormArrayDatos('form', 'txt', $excepcion);
                $ind = $this->metValidarFormArrayDatos('form', 'int');
                if ($txt != null && $ind == null) {
                    $validacion = $txt;
                } elseif ($txt == null && $ind != null) {
                    $validacion = $ind;
                } else {
                    $validacion = array_merge($txt, $ind);
                }

                if ($validacion['fk_nmc001_num_tipo_nomina_periodo'] == 'otros' && $validacion['fk_nmc001_num_tipo_nomina_periodoTXT'] == '') {
                    $validacion['fk_nmc001_num_tipo_nomina_periodoTXT'] = 'error';
                }

                if (in_array('error', $validacion)) {
                    $validacion['status'] = 'error';
                    echo json_encode($validacion);
                    exit;
                }

                if ($validacion['fk_nmc001_num_tipo_nomina_periodo'] == 'otros' && $validacion['fk_nmc001_num_tipo_nomina_periodoTXT'] != '') {
                    $idPeriodo = $this->atControlProcesos->metAperturarPeriodo($validacion['fk_nmb001_num_tipo_nomina'], $validacion['fk_nmc001_num_tipo_nomina_periodoTXT']);
                    $validacion['fk_nmc001_num_tipo_nomina_periodo'] = $idPeriodo;
                } else {
                    $idPeriodo = 0;
                }

                if (is_array($idPeriodo)) {
                    $validacion['fk_nmc001_num_tipo_nomina_periodoTXT'] = 'error';
                    $validacion['fk_nmc001_num_tipo_nomina_periodo'] = 'otros';
                    $validacion['status'] = 'errorSQL';
                    echo json_encode($validacion);
                    exit;
                }

                if ($idControlProceso == 0) {
                    $id = $this->atControlProcesos->metAperturarProceso($validacion['fk_nmb003_num_tipo_proceso'], $validacion['fk_nmc001_num_tipo_nomina_periodo'], $validacion['fec_desde'], $validacion['fec_hasta']);
                    $validacion['status'] = 'nuevo';
                } else {
                    $id = $this->atControlProcesos->metModificarAperturarProceso($validacion['fk_nmb003_num_tipo_proceso'], $validacion['fk_nmc001_num_tipo_nomina_periodo'], $validacion['fec_desde'], $validacion['fec_hasta'], $idControlProceso);
                    $validacion['status'] = 'modificar';
                }
            }elseif($status=='AP' || $status=='CR'){
                $id = $this->atControlProcesos->metCambioStatusAperturarProceso($idControlProceso,$status);
                if($status=='AP'){
                    $validacion['status'] = 'aprobado';
                }else{
                    $validacion['status'] = 'cerrado';
                }
            }

            if (is_array($id)) {
                foreach ($validacion as $titulo => $valor) {
                    if (strpos($id[2], $validacion[$titulo])) {
                        $validacion[$titulo] = 'error';
                    }
                }
                $validacion['status'] = 'errorSQL';
                echo json_encode($validacion);
                exit;
            }
            $registro=$this->atControlProcesos->metBuscarControlProcesos($idControlProceso);
            $validacion['fk_nmb003_num_tipo_proceso']=$registro['ind_nombre_proceso'];
            $validacion['fk_nmb001_num_tipo_nomina']=$registro['ind_nombre_nomina'];
            $validacion['fec_anio']=$registro['fec_anio'];
            $validacion['fec_mes']=$registro['fec_mes'];
            $validacion['ind_estado']=$registro['ind_estado'];

            $validacion['idControlProceso'] = $id;
            echo json_encode($validacion);
            exit;
        }

        $this->atVista->assign('tipoNomina',$this->atTipoNomina->metListarTipoNomina(1));
        $this->atVista->assign('idControlProceso',$idControlProceso);

        if($idControlProceso!=0){
            $this->atVista->assign('formDB',$this->atControlProcesos->metBuscarControlProcesos($idControlProceso));
        }

        $this->atVista->assign('ver',$ver);
        $this->atVista->assign('status',$status);
        $this->atVista->metRenderizar('IniciarModificar', 'modales');
    }

    public function metProcesosNomina()
    {
        $idNomina = $this->metObtenerInt('idNomina');
        $procesos = $this->atControlProcesos->metListarProcesosNomina($idNomina);
        echo json_encode($procesos);
        exit;
    }

    public function metPeriodosDisponibles()
    {
        $idNomina = $this->metObtenerInt('idNomina');
        $periodos = $this->atControlProcesos->metListarPeriodosNomina($idNomina);
        echo json_encode($periodos);
        exit;
    }

    public function metProcesosDisponibles()
    {
        $idPeriodoNomina = $this->metObtenerInt('idPeriodoProceso');
        $todos = $this->metObtenerInt('todos');
        $tipo = $this->metObtenerAlphaNumerico('tipo');
        if($todos==0){
            $todos=false;
        }
        $procesos = $this->atControlProcesos->metListarProcesosPeriodoNomina($idPeriodoNomina,$todos,$tipo);

        echo json_encode($procesos);
        exit;
    }

    public function metJsonDataTabla($estatus = false)
    {
        #obtengo los rodes de usuario, Nota: esto es obligatorio
        $rol=Session::metObtener('perfil');
        #cacturo la busqueda enviada por la datatabla, Nota: esto es obligatorio
        $busqueda = $this->metObtenerFormulas('search');
        #construyo el sql, Nota: esto es obligatorio
        $sql = "SELECT
                  nm_c002_proceso_periodo.*,
                  nm_c001_tipo_nomina_periodo.fec_anio,
                  nm_c001_tipo_nomina_periodo.fec_mes,
                  nm_b001_tipo_nomina.ind_nombre_nomina,
                  nm_b003_tipo_proceso.ind_nombre_proceso
                FROM
                  nm_c002_proceso_periodo
                INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
                INNER JOIN nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina
                INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso 
                WHERE 1 ";
        if ($estatus) {
            $sql .= " AND nm_c002_proceso_periodo.ind_estado='$estatus' ";
        }

        if ($busqueda['value']) {
            $sql .= " AND
                        ( 
                        nm_b001_tipo_nomina.ind_nombre_nomina LIKE '%$busqueda[value]%' OR
                        nm_c001_tipo_nomina_periodo.fec_anio LIKE '%$busqueda[value]%' OR
                        nm_c001_tipo_nomina_periodo.fec_mes LIKE '%$busqueda[value]%' OR
                        nm_b003_tipo_proceso.ind_nombre_proceso LIKE '%$busqueda[value]%' OR
                        nm_c002_proceso_periodo.ind_estado LIKE '%$busqueda[value]%'
                        ) ";
        }

        #creo un arreglo de los campos a mostrar, Nota: esto es obligatorio
        $campos = array('ind_nombre_nomina','fec_anio','fec_mes','ind_nombre_proceso','ind_estado');
        #campo primario de la tabla, Nota: esto es obligatorio
        $clavePrimaria = 'pk_num_proceso_periodo';
        #construyo el listado de botones

        if (in_array('NM-01-01-03-01-AP',$rol) AND $estatus == 'PR') {
            $campos['boton']['Aprobar'] = '
                <button class="aprobar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idControlProceso="'.$clavePrimaria.'" title="Aprobar Proceso"
                        descipcion="El Usuario a Aprobado un Control de Procesos" titulo="Aprobar Control de Procesos">
                    <i class="icm icm-rating3" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Aprobar'] = false;
        }

        if (in_array('NM-01-01-03-02-M',$rol) AND $estatus != 'PR') {
            $campos['boton']['Editar'] = array(
                "<button class='modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary' data-toggle='modal' data-target='#formModal'
                        data-keyboard='false' data-backdrop='static' idControlProceso='$clavePrimaria' title='Editar'
                        descipcion='El Usuario a Modificado el Control de Procesos' titulo='Modificar Control de Procesos'>
                    <i class='fa fa-edit' style='color: #ffffff;'></i>
                </button>
                ",
                'if( $i["ind_estado"] =="PR") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Editar'] = false;
        }

        if (in_array('NM-01-01-03-04-C',$rol) AND $estatus != 'PR') {
            $campos['boton']['Generar'] = array(
                "<button class='cerrar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary' data-toggle='modal' data-target='#formModal'
                        data-keyboard='false' data-backdrop='static' idControlProceso='$clavePrimaria' title='Cerrar Proceso'
                        descipcion='El Usuario a Modificado el Control de Procesos' titulo='Cerrar Control de Procesos'>
                    <i class='md md-timer-off' style='color: #ffffff;'></i>
                </button>
                ",
                'if( $i["ind_estado"] =="GE") { $valor2 = "#botonEval"; } else { $valor2 = ""; }'
            );
        } else {
            $campos['boton']['Generar'] = false;
        }

        if (in_array('NM-01-01-03-03-V',$rol)) {
            $campos['boton']['Ver'] = '
                <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                        data-keyboard="false" data-backdrop="static" idControlProceso="'.$clavePrimaria.'" title="Ver Proceso"
                        descipcion="El Usuario esta viendo un Control de Procesos" titulo="<i class=\'icm icm-calculate2\'></i> Ver Control de Procesos">
                    <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                </button>
            ';
        } else {
            $campos['boton']['Ver'] = false;
        }

        #hago el llamado de la datatabla del controlador principal.
        $this->metDataTabla($sql,$campos,$clavePrimaria);

    }

}