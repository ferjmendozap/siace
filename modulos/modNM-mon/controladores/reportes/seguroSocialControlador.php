<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modNM' . DS . 'controladores' . DS . 'FuncionesNomina' . DS . 'funcionesNominaControlador.php';

class seguroSocialControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }
    private $seguroSocial;
    private $atConsultasComunes;
    private $atTipoNomina;
    private $atFPDF;
    private $atFuncionesModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atFuncionesModelo = new funcionesNominaModelo();
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');

        $this->metObtenerLibreria('headerSeguroSocial', 'modNM');
        #$this->atFPDF = new pdfseguroSocial('P', 'mm', array(200, 279));
        $this->atFPDF = new pdfSeguroSocial('P', 'mm', 'A4');
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina());
        $this->atVista->metRenderizar('index');
    }

    public function metMesLetras($mes){

        if(strlen($mes)==1){
            $mes="0".$mes;
        }
        $metMesLetras=array(
            '01'=>'ENERO',
            '02'=>'FEBRERO',
            '03'=>'MARZO',
            '04'=>'ABRIL',
            '05'=>'MAYO',
            '06'=>'JUNIO',
            '07'=>'JULIO',
            '08'=>'AGOSTO',
            '09'=>'SEPTIEMBRE',
            '10'=>'OCTUBRE',
            '11'=>'NOVIEMBRE',
            '12'=>'DICIEMBRE'
        );

        return $metMesLetras[$mes];
    } // END FUNCTION

    public function metPdfSeguroSocial($idProcesoPeriodo)
    {
        define('PROCESO','ok');

        $empleados = $this->atConsultasComunes->metEmpleadosNomina($idProcesoPeriodo);
        $nomina=$this->atEjecucionProcesos->metObtenerProceso($idProcesoPeriodo);

        $this->atFPDF->SetTipoNom(utf8_decode($nomina['ind_titulo_boleta']));
        $this->atFPDF->SetProceso(utf8_decode($nomina['ind_nombre_proceso']));
        $this->atFPDF->setPeriodo('MES DE '.$this->metMesLetras($empleados[0]['fec_mes']).' '.$empleados[0]['fec_anio']);

        $this->atFPDF->setDesde(date('d-m-Y', strtotime($nomina['fec_desde'])));
        $this->atFPDF->setHasta(date('d-m-Y', strtotime($nomina['fec_hasta'])));

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 15, 10);
        $this->atFPDF->SetAutoPageBreak(5, 20);

        $this->atFPDF->AddPage();
        $this->atFPDF->SetFont('Arial', '', 7);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array(18, 65, 23, 23, 23, 30));

        $totalRetencion = 0;
        $totalAporte = 0;
        $totalMonto = 0;
        $totalEnterar = 0;

        foreach ($empleados AS $empleado) {
            $deducciones = $this->atConsultasComunes->metEmpleadosDeducciones($idProcesoPeriodo, $empleado['pk_num_empleado']);
            $asignacion = $this->atConsultasComunes->metEmpleadosAsignaciones($idProcesoPeriodo, $empleado['pk_num_empleado']);
            $aportes = $this->atConsultasComunes->metEmpleadosAportes($idProcesoPeriodo, $empleado['pk_num_empleado']);

            //SALTAR PAGINA
            $y = $this->atFPDF->GetY();

            if ($y > 200){

                $this->atFPDF->AddPage();
                $this->atFPDF->SetDrawColor(0, 0, 0);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                //$this->atFPDF->SetWidths(array(8, 20, 85, 24, 40));
                $this->atFPDF->SetWidths(array(18, 65, 23, 23, 23, 30));
                $this->atFPDF->SetAligns(array('R', 'L', 'R', 'R', 'R', 'R'));
            }
            $this->atArgsIdEmpleado = $empleado['pk_num_empleado'];
            $this->atArgsPrediodoDesde = $empleado['fec_anio'].'-'.$empleado['fec_mes'];
            if(PROCESO=='RTA'){
                $monto = $this->metSueldoBasico();
            } else {
                $monto = $this->metSueldoBasico(true);
            }

            $retencion = 0;
            $aporte = 0;

            foreach ($deducciones as $key=>$value)
            {
                if($value['ind_abrebiatura']=='RETSSO'){
                    $retencion = $retencion + $value['num_monto'];
                }
            }
            foreach ($aportes as $key=>$value) {
                if ($value['ind_abrebiatura'] == 'APSSO') {
                    $aporte = $aporte + $value['num_monto'];
                }
            }
            $total = $retencion + $aporte;

            //$total = $total + (number_format($a,2,'.','') - number_format($d,2,'.',''));
            $this->atFPDF->Row(array(
                number_format($empleado['ind_cedula_documento'], 0, '', '.') ,
                utf8_decode($empleado['nombre']),
                number_format($monto,2,',','.'),
                number_format($retencion,2,',','.'),
                number_format($aporte,2,',','.'),
                number_format($total,2,',','.')

            ));

            $totalRetencion = $totalRetencion + $retencion;
            $totalAporte = $totalAporte + $aporte;
            $totalEnterar = $totalEnterar + $total;
            $totalMonto = $totalMonto + $monto;
        }


        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($this->atConsultasComunes->atIdEmpleado);
        $revisadoPor = $this->atConsultasComunes->metMostrarEmpleadoJefe($preparadoPor['pk_num_dependencia']);
        $conformadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');


        $this->atFPDF->SetFont('Arial', 'B', 8);

        $this->atFPDF->Row(array(
            '',
            'TOTAL',
            number_format($totalMonto,2,',','.'),
            number_format($totalRetencion,2,',','.'),
            number_format($totalAporte,2,',','.'),
            number_format($totalEnterar,2,',','.'),
        ));
        $this->atFPDF->Ln(5);



        $this->atFPDF->SetDrawColor(255, 255, 255); $this->atFPDF->SetFillColor(255, 255, 255); $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array(10,100, 100));
        $this->atFPDF->SetAligns(array('','L', 'L'));
        $this->atFPDF->Row(array('','ELABORADO POR:','CONFORMADO POR:'));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['nombre']),utf8_decode($revisadoPor['nombre'])));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['ind_descripcion_cargo']),utf8_decode($revisadoPor['ind_descripcion_cargo'])));

        $this->atFPDF->Ln(8);
        $this->atFPDF->Row(array('','APROBADO POR: '));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($aprobadoPor['nombre'])));

        $this->atFPDF->Row(array('',utf8_decode($aprobadoPor['ind_descripcion_cargo'])));

        $this->atFPDF->Output();
    }

}