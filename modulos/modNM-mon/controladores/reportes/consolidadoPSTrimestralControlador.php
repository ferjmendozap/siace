<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class consolidadoPSTrimestralControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;
    private $atConsultasComunes;
    private $atTipoNomina;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->metObtenerLibreria('headerNominaConsolidada', 'modNM');
        $this->atFPDF = new pdfNominaConsolidada("L", "mm", "LEGAL");
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('lista', $this->atConsultasComunes->metPrestacionesSocialesAños());
        $this->atVista->metRenderizar('index');
    }


    public function metPdfConsolidadoPSTrimestral($fecPrestacionesSociales,$trimestre)
    {
        $empleadoPrestacionesSociales = $this->atConsultasComunes->metConsolidadoPSTrimestral($fecPrestacionesSociales);
        $this->atFPDF->setTipoHeader('prestacionesSocialesTrimestralConsolidado');
        $this->atFPDF->setNumAnio($fecPrestacionesSociales);
        $this->atFPDF->setNumTri($trimestre);
        if ($trimestre == '1') {
            $this->atFPDF->setDesde('ENERO');
            $this->atFPDF->sethasta('MARZO');
        }
        if ($trimestre == '2') {
            $this->atFPDF->setDesde('ABRIL');
            $this->atFPDF->sethasta('JUNIO');
        }
        if ($trimestre == '3') {
            $this->atFPDF->setDesde('JULIO');
            $this->atFPDF->sethasta('SEPTIEMBRE');
        }
        if ($trimestre == '4') {
            $this->atFPDF->setDesde('OCTUBRE');
            $this->atFPDF->sethasta('DICIEMBRE');
        }
        $n = 1;
        $this->atFPDF->AddPage();
        $totalPrestaciones = 0;
        $totalPago = 0;
        foreach ($empleadoPrestacionesSociales AS $empleadoPrestaciones) {
                $prestaciones = ($empleadoPrestaciones['TRIMESTRE' . $trimestre] + $empleadoPrestaciones['ANUAL' . $trimestre]);
                $totalPrestaciones = $totalPrestaciones + $prestaciones;
                $explode = $empleadoPrestaciones['fec_ingreso'];
                $fecha_ingreso = explode("-", $explode);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetX(15);
                $this->atFPDF->SetWidths(array(8, 20, 80, 30, 50, 30, 30, 30, 30));
                if ($prestaciones > 0) {

                    $this->atFPDF->SetAligns(array('C', 'C', 'L', 'C', 'C', 'C', 'R', 'R', 'R'));
                    $this->atFPDF->Row(array(
                        $n++,
                        number_format($empleadoPrestaciones['ind_cedula_documento'], 0, '', '.'),
                        utf8_decode($empleadoPrestaciones['ind_apellido1'] . ' ' . $empleadoPrestaciones['ind_apellido2'] . ' ' . $empleadoPrestaciones['ind_nombre1'] . ' ' . $empleadoPrestaciones['ind_nombre2']),
                        $fecha_ingreso[2] . '/' . $fecha_ingreso[1] . '/' . $fecha_ingreso[0],
                        $empleadoPrestaciones['ind_cuenta'],
                        $empleadoPrestaciones['dias_anual' . $trimestre],
                        number_format($empleadoPrestaciones['ANUAL' . $trimestre], 2, ',', '.'),
                        number_format($empleadoPrestaciones['TRIMESTRE' . $trimestre], 2, ',', '.'),
                        number_format($prestaciones, 2, ',', '.')
                    ));

                }

            //SALTAR PAGINA
            $y = $this->atFPDF->GetY();
            if ($y > 300) {
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetX(15);
                $this->atFPDF->SetWidths(array(278, 30));
                $this->atFPDF->SetAligns(array('R', 'R'));
                $this->atFPDF->Row(array('Total', number_format($totalPrestaciones, 2, ',', '.')));
                $totalPago = $totalPago + $totalPrestaciones;
                $totalPrestaciones = 0;
                $this->atFPDF->AddPage();
            }

        }


        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetX(15);
        $this->atFPDF->SetWidths(array(278, 30));
        $this->atFPDF->SetAligns(array('R', 'R'));
        $this->atFPDF->Row(array('Total', number_format($totalPrestaciones, 2, ',', '.')));

        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetX(15);
        $this->atFPDF->SetWidths(array(278, 30));
        $this->atFPDF->SetAligns(array('R', 'R'));
        $this->atFPDF->Row(array('Total Prestaciones', number_format($totalPago + $totalPrestaciones, 2, ',', '.')));


        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($this->atConsultasComunes->atIdEmpleado);
        //$aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');

        $this->atFPDF->SetXY(55, 161);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 9);
        $this->atFPDF->Cell(200, 3, '______________________________________________                                     ___________________________________________', 0, 1, 'L');

        $this->atFPDF->SetFont('Arial', '', 9);
        $this->atFPDF->SetXY(35, 160);
        $this->atFPDF->Cell(60, 3, ('Elaborado Por:                                                                                                       Aprobado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(60, 3, (''), 0, 1, 'L');
        $this->atFPDF->SetXY(60, 166);
        $this->atFPDF->MultiCell(150, 3, utf8_decode($preparadoPor['nombre']), 0, 'L');//nombre de quien elabora
        $this->atFPDF->SetXY(170, 166);
        $this->atFPDF->Cell(150, 3, utf8_decode($aprobadoPor['nombre']), 0, 1, 'L');//nombre de quien revisa
        $this->atFPDF->SetXY(60, 170);
        $this->atFPDF->MultiCell(100, 3, utf8_decode($preparadoPor['ind_descripcion_cargo']), 0, 'L');//cargo de quien elabora
        $this->atFPDF->SetXY(170, 170);
        $this->atFPDF->Cell(100, 3, utf8_decode($aprobadoPor['ind_descripcion_cargo']), 0, 1, 'L');//Cargo de quien revisa

        $this->atFPDF->Output();
    }

}