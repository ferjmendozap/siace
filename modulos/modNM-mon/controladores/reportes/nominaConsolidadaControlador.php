<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class nominaConsolidadaControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;
    private $atConsultasComunes;
    private $atTipoNomina;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->metObtenerLibreria('headerNominaConsolidada', 'modNM');
        $this->atFPDF = new pdfNominaConsolidada('L', 'mm', array(216, 595));
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina(1));
        $this->atVista->metRenderizar('index');
    }


    public function metPdfNominaConsolidada($idPeriodoProceso)
    {
        $nomina=$this->atEjecucionProcesos->metObtenerProceso($idPeriodoProceso);
        $empleado = $this->atConsultasComunes->metEmpleadosNomina($idPeriodoProceso);
        for ($i = 0; $i < count($empleado); $i++) {
            $empleados[$i] = array(
                'idEmpleado' => $empleado[$i]['pk_num_empleado'],
                'ci' => $empleado[$i]['ind_cedula_documento'],
                'nombre' => $empleado[$i]['ind_nombre1'] . ' ' . $empleado[$i]['ind_nombre2'] . ' ' . $empleado[$i]['ind_apellido1'] . ' ' . $empleado[$i]['ind_apellido2'],
            );
            $empleados[$i]['asignaciones'] = $this->atConsultasComunes->metEmpleadosAsignaciones($idPeriodoProceso, $empleado[$i]['pk_num_empleado']);
            $codAsignaciones = $empleados[$i]['asignaciones'];
            $totalAsignacionesEmpleado = 0;
            foreach ($codAsignaciones AS $codAsignacion) {
                $empleados[$i]['codAsignaciones'][] = $codAsignacion['cod_concepto'];
                $totalAsignacionesEmpleado = $totalAsignacionesEmpleado + $codAsignacion['num_monto'];
            }
            $empleados[$i]['totalAsignacionesEmpleado'] = $totalAsignacionesEmpleado;

            $empleados[$i]['deducciones'] = $this->atConsultasComunes->metEmpleadosDeducciones($idPeriodoProceso, $empleado[$i]['pk_num_empleado']);
            $codDeducciones = $empleados[$i]['deducciones'];
            $totalDeduccionesEmpleado = 0;
            foreach ($codDeducciones AS $codDeduccion) {
                $empleados[$i]['codDeducciones'][] = $codDeduccion['cod_concepto'];
                $totalDeduccionesEmpleado = $totalDeduccionesEmpleado + $codDeduccion['num_monto'];
            }
            $empleados[$i]['totalDeduccionesEmpleado'] = $totalDeduccionesEmpleado;

            $empleados[$i]['aportes'] = $this->atConsultasComunes->metEmpleadosAportes($idPeriodoProceso, $empleado[$i]['pk_num_empleado']);
            $codAportes = $empleados[$i]['aportes'];
            $totalAportesEmpleado = 0;
            foreach ($codAportes AS $codAporte) {
                $empleados[$i]['codAporte'][] = $codAporte['cod_concepto'];
                $totalAportesEmpleado = $totalAportesEmpleado + $codAporte['num_monto'];
            }
            $empleados[$i]['totalAportesEmpleado'] = $totalAportesEmpleado;
        }
        $tituloAsignaciones = $this->atConsultasComunes->metTituloAsignaciones($idPeriodoProceso);
        $tituloDeducciones = $this->atConsultasComunes->metTituloDeducciones($idPeriodoProceso);
        $tituloAportes = $this->atConsultasComunes->metTituloAportes($idPeriodoProceso);

        #### PDF ####
        $this->atFPDF->SetTipoNom(utf8_decode($nomina['ind_titulo_boleta']));
        $this->atFPDF->SetProceso(utf8_decode($nomina['ind_nombre_proceso']));

        $this->atFPDF->setDesde(date('d-m-Y', strtotime($nomina['fec_desde'])));
        $this->atFPDF->setHasta(date('d-m-Y', strtotime($nomina['fec_hasta'])));
        $this->atFPDF->setTipoHeader('nomina');

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 5);
        $this->atFPDF->SetAutoPageBreak(10);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);

####### ASIGNACIONES ########
        if ($tituloAsignaciones) {
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(190, 5, ("ASIGNACIONES"), 0, 1, 'C');
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
            foreach ($tituloAsignaciones AS $titulos) {
                $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, ('T.ASIG.'), 1, 1, 'R', 1);
            $ln = 0;
            $N = 1;
            foreach ($empleados AS $empleado) {
                if ($empleado['asignaciones']) {
                    $ln++;
                    $y = $this->atFPDF->GetY();
                    if ($y > 170) {
                        $this->atFPDF->AddPage();
                        $this->atFPDF->SetFont('Arial', 'B', 8);
                        $this->atFPDF->Cell(190, 5, ("ASIGNACIONES"), 0, 1, 'C');
                        $this->atFPDF->SetFillColor(200, 200, 200);
                        $this->atFPDF->SetFont('Arial', 'B', 6);
                        $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
                        $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
                        $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
                        foreach ($tituloAsignaciones AS $titulos) {
                            $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
                        }
                        $this->atFPDF->Cell(21, 6, ('T.ASIG.'), 1, 1, 'R', 1);
                    } else {
                        $this->atFPDF->SetY($y);
                    }
                    if ($ln % 2 == 0) {
                        $this->atFPDF->SetFillColor(240, 240, 240);
                    } else {
                        $this->atFPDF->SetFillColor(255, 255, 255);
                    }
                    $this->atFPDF->SetFont('Arial', '', 7);
                    $this->atFPDF->Cell(7, 6, $N++, 1, 0, 'C', 1);
                    $this->atFPDF->Cell(15, 6, number_format($empleado['ci'], 0, '', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Cell(60, 6, utf8_decode($empleado['nombre']), 1, 0, 'L', 1);
                    $asignaciones = $empleado['asignaciones'];
                    $codAsignaciones = $empleado['codAsignaciones'];
                    $total = 0;

                    foreach ($tituloAsignaciones AS $titulos) {
                        if (in_array($titulos['cod_concepto'], $codAsignaciones)) {
                            foreach ($asignaciones AS $asignacion) {
                                if ($titulos['cod_concepto'] == $asignacion['cod_concepto']) {
                                    $total = $total + $asignacion['num_monto'];
                                    $this->atFPDF->Cell(21, 6, number_format($asignacion['num_monto'], 2, ',', '.'), 1, 0, 'R', 1);

                                }

                            }

                        } else {
                            $this->atFPDF->Cell(21, 6, number_format(0, 2, ',', '.'), 1, 0, 'R', 1);
                        }
                    }


                    $this->atFPDF->SetFont('Arial', 'B', 7);
                    $this->atFPDF->Cell(21, 6, number_format($total, 2, ',', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Ln();

                }


            }
            ### Totales ###
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->Cell(7, 6, '', 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ' ', 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, 'Totales: ', 1, 0, 'R', 1);
            $this->atFPDF->SetFont('Arial', 'B', 7);

            $totalAsignaciones = 0;
            foreach ($tituloAsignaciones AS $titulos) {
                $totalAsignaciones = $totalAsignaciones + $titulos['TOTAL'];
                $this->atFPDF->Cell(21, 6, number_format($titulos['TOTAL'], 2, ',', '.'), 1, 0, 'R', 1);
            }

            $this->atFPDF->Cell(21, 6, number_format($totalAsignaciones, 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Ln(6);
            ### Leyenda ###
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 4);
            $this->atFPDF->Cell(30, 5, 'LEYENDA', 1, 0, 'C', 1);
            $this->atFPDF->Cell(50, 5, 'DESCRIPCION', 1, 1, 'C', 1);

            foreach ($tituloAsignaciones AS $titulos) {
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetFont('Arial', 'B', 4);
                $this->atFPDF->Cell(30, 5, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'L', 1);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 4);
                $this->atFPDF->Cell(50, 5, utf8_decode($titulos['ind_impresion']), 1, 1, 'L', 1);
            }

            $this->atFPDF->AddPage();
        }

####### FIN ########
####### DEDUCCIONES ########
        if ($tituloDeducciones) {
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(190, 5, ("DEDUCCIONES"), 0, 1, 'C');
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
            foreach ($tituloDeducciones AS $titulos) {
                $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, ('T.DEDUC.'), 1, 1, 'R', 1);
            $ln = 0;
            $N = 1;
            foreach ($empleados AS $empleado) {
                if ($empleado['deducciones']) {
                    $ln++;
                    $y = $this->atFPDF->GetY();
                    if ($y > 170) {
                        $this->atFPDF->AddPage();
                        $this->atFPDF->SetFont('Arial', 'B', 8);
                        $this->atFPDF->Cell(190, 5, ("DEDUCCIONES"), 0, 1, 'C');
                        $this->atFPDF->SetFillColor(200, 200, 200);
                        $this->atFPDF->SetFont('Arial', 'B', 6);
                        $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
                        $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
                        $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
                        foreach ($tituloDeducciones AS $titulos) {
                            $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
                        }
                        $this->atFPDF->Cell(21, 6, ('T.DEDUC.'), 1, 1, 'R', 1);
                    } else {
                        $this->atFPDF->SetY($y);
                    }
                    if ($ln % 2 == 0) {
                        $this->atFPDF->SetFillColor(240, 240, 240);
                    } else {
                        $this->atFPDF->SetFillColor(255, 255, 255);
                    }
                    $this->atFPDF->SetFont('Arial', '', 7);
                    $this->atFPDF->Cell(7, 6, $N++, 1, 0, 'C', 1);
                    $this->atFPDF->Cell(15, 6, number_format($empleado['ci'], 0, '', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Cell(60, 6, utf8_decode($empleado['nombre']), 1, 0, 'L', 1);
                    $deducciones = $empleado['deducciones'];
                    $codDeducciones = $empleado['codDeducciones'];
                    $total = 0;
                    foreach ($tituloDeducciones AS $titulos) {
                        if (in_array($titulos['cod_concepto'], $codDeducciones)) {
                            foreach ($deducciones AS $deduccion) {
                                if ($titulos['cod_concepto'] == $deduccion['cod_concepto']) {
                                    $total = $total + $deduccion['num_monto'];
                                    $this->atFPDF->Cell(21, 6, number_format($deduccion['num_monto'], 2, ',', '.'), 1, 0, 'R', 1);
                                }
                            }
                        } else {
                            $this->atFPDF->Cell(21, 6, number_format(0, 2, ',', '.'), 1, 0, 'R', 1);
                        }
                    }

                    $this->atFPDF->SetFont('Arial', 'B', 7);
                    $this->atFPDF->Cell(21, 6, number_format($total, 2, ',', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Ln();
                }
            }
            ### Totales ###
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->Cell(7, 6, '', 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ' ', 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, 'Totales: ', 1, 0, 'R', 1);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $totalDeducciones = 0;
            foreach ($tituloDeducciones AS $titulos) {
                $totalDeducciones = $totalDeducciones + $titulos['TOTAL'];
                $this->atFPDF->Cell(21, 6, utf8_decode($titulos['TOTAL']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, number_format($totalDeducciones, 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Ln(6);
            ### Leyenda ###
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 4);
            $this->atFPDF->Cell(30, 5, 'LEYENDA', 1, 0, 'C', 1);
            $this->atFPDF->Cell(50, 5, 'DESCRIPCION', 1, 1, 'C', 1);
            foreach ($tituloDeducciones AS $titulos) {
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetFont('Arial', 'B', 4);
                $this->atFPDF->Cell(30, 5, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'L', 1);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 4);
                $this->atFPDF->Cell(50, 5, utf8_decode($titulos['ind_impresion']), 1, 1, 'L', 1);
            }
            $this->atFPDF->AddPage();
        }
####### FIN ########
####### APORTES ########
        if ($tituloAportes) {
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(190, 5, ("APORTES"), 0, 1, 'C');
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
            foreach ($tituloAportes AS $titulos) {
                $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, ('T.APORTES.'), 1, 1, 'R', 1);
            $ln = 0;
            $N = 1;
            foreach ($empleados AS $empleado) {
                if ($empleado['aportes']) {
                    $ln++;
                    $y = $this->atFPDF->GetY();
                    if ($y > 170) {
                        $this->atFPDF->AddPage();
                        $this->atFPDF->SetFont('Arial', 'B', 8);
                        $this->atFPDF->Cell(190, 5, ("APORTES"), 0, 1, 'C');
                        $this->atFPDF->SetFillColor(200, 200, 200);
                        $this->atFPDF->SetFont('Arial', 'B', 6);
                        $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
                        $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
                        $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
                        foreach ($tituloAportes AS $titulos) {
                            $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
                        }
                        $this->atFPDF->Cell(21, 6, ('T.APORTES.'), 1, 1, 'R', 1);
                    } else {
                        $this->atFPDF->SetY($y);
                    }
                    if ($ln % 2 == 0) {
                        $this->atFPDF->SetFillColor(240, 240, 240);
                    } else {
                        $this->atFPDF->SetFillColor(255, 255, 255);
                    }
                    $this->atFPDF->SetFont('Arial', '', 7);
                    $this->atFPDF->Cell(7, 6, $N++, 1, 0, 'C', 1);
                    $this->atFPDF->Cell(15, 6, number_format($empleado['ci'], 0, '', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Cell(60, 6, utf8_decode($empleado['nombre']), 1, 0, 'L', 1);
                    $aportes = $empleado['aportes'];
                    $codAporte = $empleado['codAporte'];
                    $total = 0;
                    foreach ($tituloAportes AS $titulos) {
                        if (in_array($titulos['cod_concepto'], $codAporte)) {
                            foreach ($aportes AS $aporte) {
                                if ($titulos['cod_concepto'] == $aporte['cod_concepto']) {
                                    $total = $total + $aporte['num_monto'];
                                    $this->atFPDF->Cell(21, 6, number_format($aporte['num_monto'], 2, ',', '.'), 1, 0, 'R', 1);
                                }
                            }
                        } else {
                            $this->atFPDF->Cell(21, 6, number_format(0, 2, ',', '.'), 1, 0, 'R', 1);
                        }
                    }

                    $this->atFPDF->SetFont('Arial', 'B', 7);
                    $this->atFPDF->Cell(21, 6, number_format($total, 2, ',', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Ln();
                }
            }
            ### Totales ###
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->Cell(7, 6, '', 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ' ', 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, 'Totales: ', 1, 0, 'R', 1);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $totalAportes = 0;
            foreach ($tituloAportes AS $titulos) {
                $totalAportes = $totalAportes + $titulos['TOTAL'];
                $this->atFPDF->Cell(21, 6, utf8_decode($titulos['TOTAL']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, number_format($totalAportes, 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Ln(6);
            ### Leyenda ###
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 4);
            $this->atFPDF->Cell(30, 5, 'LEYENDA', 1, 0, 'C', 1);
            $this->atFPDF->Cell(50, 5, 'DESCRIPCION', 1, 1, 'C', 1);
            foreach ($tituloAportes AS $titulos) {
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetFont('Arial', 'B', 4);
                $this->atFPDF->Cell(30, 5, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'L', 1);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 4);
                $this->atFPDF->Cell(50, 5, utf8_decode($titulos['ind_impresion']), 1, 1, 'L', 1);
            }
            $this->atFPDF->AddPage();
        }
####### FIN ########
####### REMUNERACION ########
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(190, 5, ("TOTAL REMUNERACION"), 0, 1, 'C');
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
        $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
        $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
        $this->atFPDF->Cell(21, 6, 'T.ASIG', 1, 0, 'R', 1);
        $this->atFPDF->Cell(21, 6, 'T.DEDUC.', 1, 0, 'R', 1);
        $this->atFPDF->Cell(21, 6, 'T.PAGAR.', 1, 0, 'R', 1);
        $this->atFPDF->Cell(30, 6, ('FIRMA'), 1, 1, 'R', 1);
        $ln = 0;
        $N = 1;

        $asignacionEmpleados = 0;
        $deduccionEmpleados = 0;

        foreach ($empleados AS $empleado) {
            $asignacionEmpleados = $asignacionEmpleados + $empleado['totalAsignacionesEmpleado'];
            $deduccionEmpleados = $deduccionEmpleados + $empleado['totalDeduccionesEmpleado'];
            $ln++;
            $y = $this->atFPDF->GetY();
            if ($y > 170) {
                $this->atFPDF->AddPage();
                $this->atFPDF->SetFont('Arial', 'B', 8);
                $this->atFPDF->Cell(190, 5, ("TOTAL REMUNERACION"), 0, 1, 'C');
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetFont('Arial', 'B', 6);
                $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
                $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
                $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
                $this->atFPDF->Cell(21, 6, 'T.ASIG', 1, 0, 'R', 1);
                $this->atFPDF->Cell(21, 6, 'T.DEDUC.', 1, 0, 'R', 1);
                $this->atFPDF->Cell(21, 6, 'T.PAGAR.', 1, 0, 'R', 1);
                $this->atFPDF->Cell(30, 6, ('FIRMA'), 1, 1, 'R', 1);
            } else {
                $this->atFPDF->SetY($y);
            }
            if ($ln % 2 == 0) {
                $this->atFPDF->SetFillColor(240, 240, 240);
            } else {
                $this->atFPDF->SetFillColor(255, 255, 255);
            }
            $this->atFPDF->SetFont('Arial', '', 7);
            $this->atFPDF->Cell(7, 6, $N++, 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, number_format($empleado['ci'], 0, '', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, utf8_decode($empleado['nombre']), 1, 0, 'L', 1);
            $this->atFPDF->Cell(21, 6, number_format($empleado['totalAsignacionesEmpleado'], 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(21, 6, number_format($empleado['totalDeduccionesEmpleado'], 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(21, 6, number_format(($empleado['totalAsignacionesEmpleado'] - $empleado['totalDeduccionesEmpleado']), 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(30, 6, '', 1, 0, 'R', 1);
            $this->atFPDF->Ln();
        }
        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($nomina['fk_rhb001_num_empleado_crea']);
        $revisadoPor = $this->atConsultasComunes->metMostrarEmpleadoJefe($preparadoPor['pk_num_dependencia']);
        $conformadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');


        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(7, 6, '', 1, 0, 'C', 1);
        $this->atFPDF->Cell(15, 6, ' ', 1, 0, 'R', 1);
        $this->atFPDF->Cell(60, 6, 'Totales: ', 1, 0, 'R', 1);
        $this->atFPDF->Cell(21, 6, number_format($totalAsignaciones, 2, ',', '.'), 1, 0, 'R', 1);
        $this->atFPDF->Cell(21, 6, number_format($deduccionEmpleados, 2, ',', '.'), 1, 0, 'R', 1);
        $this->atFPDF->Cell(21, 6, number_format($totalAsignaciones - $deduccionEmpleados, 2, ',', '.'), 1, 0, 'R', 1);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Ln(6);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetFont('Arial', 'B', 5);
        $this->atFPDF->Cell(30, 5, 'LEYENDA', 1, 0, 'C', 1);
        $this->atFPDF->Cell(50, 5, 'DESCRIPCION', 1, 1, 'C', 1);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetFont('Arial', 'B', 5);
        $this->atFPDF->Cell(30, 5, utf8_decode('T.ASIG'), 1, 0, 'L', 1);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 5);
        $this->atFPDF->Cell(150, 5, utf8_decode('TOTAL ASIGNACIÓN'), 1, 1, 'L', 1);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->Cell(30, 5, utf8_decode('T.DEDUC'), 1, 0, 'L', 1);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 5);
        $this->atFPDF->Cell(150, 5, utf8_decode('TOTAL DEDUCCIÓN'), 1, 1, 'L', 1);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->Cell(30, 5, utf8_decode('T.PAGAR'), 1, 0, 'L', 1);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 5);
        $this->atFPDF->Cell(150, 5, utf8_decode('TOTAL PAGAR'), 1, 1, 'L', 1);
        $this->atFPDF->Ln();

        $yf = $this->atFPDF->GetY();
        if($yf>160) $yf = 160;
        $this->atFPDF->Rect(10, $y + 6, 70, 0.1, "DF");

        $this->atFPDF->SetXY(200, $yf + 10);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->Cell(200, 3, '______________________________________________                                    ___________________________________________', 0, 1, 'L');

        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(200, $yf + 15);
        $this->atFPDF->Cell(100, 3, ('Elaborado Por:                                                                                                         Conformado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(100, 3, (''), 0, 1, 'L');
        $this->atFPDF->SetXY(200, $yf + 18);
        $this->atFPDF->MultiCell(50, 3, utf8_decode($preparadoPor['nombre']), 0, 'L');//nombre de quien elabora
        $this->atFPDF->SetXY(276, $yf + 18);
        $this->atFPDF->Cell(100, 3, utf8_decode($revisadoPor['nombre']), 0, 1, 'L');//nombre de quien revisa
        $this->atFPDF->SetXY(200, $yf + 20);
        $this->atFPDF->MultiCell(50, 3, utf8_decode($preparadoPor['ind_descripcion_cargo']), 0, 'L');//cargo de quien elabora
        $this->atFPDF->SetXY(276, $yf + 20);
        $this->atFPDF->Cell(100, 3, utf8_decode($revisadoPor['ind_descripcion_cargo']), 0, 1, 'L');//Cargo de quien revisa

        $this->atFPDF->Rect(10, $y + 20, 70, 0.1, "DF");
        $this->atFPDF->SetXY(200, $yf + 35);
        $this->atFPDF->Cell(100, 3, ' ____________________________________________', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(200, $yf + 40);
        $this->atFPDF->Cell(100, 3, ('Aprobado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(100, 3, (''), 0, 1, 'L');
        $this->atFPDF->SetXY(200, $yf + 43);
        $this->atFPDF->Cell(100, 3, utf8_decode($aprobadoPor['nombre']), 0, 0, 'L');//nombre de quien conforma
        $this->atFPDF->SetXY(200, $yf + 45);
        $this->atFPDF->Cell(100, 3, utf8_decode($aprobadoPor['ind_descripcion_cargo']), 0, 0, 'L');//cargo de quien conforma

        $this->atFPDF->Output();
    }
}
