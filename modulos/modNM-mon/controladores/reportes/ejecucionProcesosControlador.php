<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class ejecucionProcesosControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->metObtenerLibreria('header', 'modNM');
        $this->atFPDF = new pdfEjecucion('P', 'mm', 'Letter');
    }

    public function metIndex()
    {
        exit;
    }


    public function metVerNomina($idJsonEmpleados, $idTipoNominaPeriodo, $idPeriodoProceso, $tipo)
    {
        $idEmpleado = str_getcsv($idJsonEmpleados, ',');

        if ($idTipoNominaPeriodo && $idPeriodoProceso) {
            $datosNomina = $this->atEjecucionProcesos->metObtenerDatosNomina($idTipoNominaPeriodo, $idPeriodoProceso);

            $codProceso = $this->atEjecucionProcesos->metObtenerProceso($idPeriodoProceso);
            $this->atFPDF->setDesde(date('d-m-Y', strtotime($codProceso['fec_desde'])));
            $this->atFPDF->setHasta(date('d-m-Y', strtotime($codProceso['fec_hasta'])));
            $this->atFPDF->setNomina(utf8_decode($codProceso['ind_titulo_boleta']));
            $this->atFPDF->setNombreProceso(utf8_decode($codProceso['ind_nombre_proceso']));
            $this->atFPDF->AddPage();

            $totales = 0;
            for ($i = 0; $i < count($idEmpleado); $i++) {
                $this->atArgsIdEmpleado = $idEmpleado[$i];
                $this->atArgsTipoNomina = $datosNomina['pk_num_tipo_nomina'];

                /*if($this->metObtenerConcepto('SB',$datosNomina['PERIODO'],'FIN')==0) {
                    $sueldoBasico = $this->metObtenerConcepto('SB',$datosNomina['PERIODO'],'PRQ') +
                        $this->metObtenerConcepto('SB',$datosNomina['PERIODO'],'PRQ') +
                        $this->metObtenerConcepto('DIF AF',$datosNomina['PERIODO'],'PRQ') +
                        $this->metObtenerConcepto('DIF AF',$datosNomina['PERIODO'],'FIN') +
                        $this->metObtenerConcepto('DSB',$datosNomina['PERIODO'],'PRQ') +
                        $this->metObtenerConcepto('DSB',$datosNomina['PERIODO'],'PRQ');
                } else {
                    $sueldoBasico = $this->metObtenerConcepto('SB',$datosNomina['PERIODO'],'PRQ') +
                        $this->metObtenerConcepto('SB',$datosNomina['PERIODO'],'FIN') +
                        $this->metObtenerConcepto('R',$datosNomina['PERIODO'],'FIN') +
                        $this->metObtenerConcepto('DIF AF',$datosNomina['PERIODO'],'PRQ') +
                        $this->metObtenerConcepto('DIF AF',$datosNomina['PERIODO'],'FIN');
                        ;

                }*/



                if ($i % 2 == 0 && $i != 0) {
                    $this->atFPDF->AddPage();
                }


                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetWidths(array(23, 30, 110, 30));
                $this->atFPDF->SetAligns(array('L', 'L', 'L', 'L'));
                $this->atFPDF->Row(array(utf8_decode('Código'), utf8_decode('Cédula'), 'Empleado', 'Fecha Ing.'));
                $this->atFPDF->Ln(2);
                $datosEmpleado = $this->atEjecucionProcesos->metObtenerEmpleado($idEmpleado[$i]);
                $this->atFPDF->Row(array(str_pad($datosEmpleado['pk_num_empleado'], 6, "0", STR_PAD_LEFT), utf8_decode($datosEmpleado['ind_cedula_documento']), utf8_decode($datosEmpleado['ind_nombre1'] . ' ' . $datosEmpleado['ind_nombre2'] . ' ' . $datosEmpleado['ind_apellido1'] . ' ' . $datosEmpleado['ind_apellido2']), $datosEmpleado['fec_ingreso']));
                $this->atFPDF->Ln(2);
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetWidths(array(23, 140));
                $this->atFPDF->SetAligns(array('L', 'L'));
                if($datosEmpleado['fk_nmb001_num_tipo_nomina']==NOJ || $datosEmpleado['fk_nmb001_num_tipo_nomina']==NOP ){
                    if($datosEmpleado['fk_nmb001_num_tipo_nomina']==NOJ){
                        $cargo = 'Jubilado';
                    }else{
                        $cargo = 'Pensionado';
                    }
                    $this->atFPDF->Row(array('Cargo:', utf8_decode($cargo)));
                }else{
                    $this->atFPDF->Row(array('Cargo:', utf8_decode($datosEmpleado['ind_descripcion_cargo'])));
                }
                $this->atFPDF->SetWidths(array(55, 120));
                $this->atFPDF->SetAligns(array('L', 'L'));
                $this->atFPDF->Ln(2);
                if($datosEmpleado['fk_nmb001_num_tipo_nomina']==NOJ || $datosEmpleado['fk_nmb001_num_tipo_nomina']==NOP ){
                  $this->atFPDF->Row(array('Sueldo Basico Mensual:', number_format($this->metSueldoBasicoJP(1), 2, ',', '.')));  
                }else{
                  $this->atFPDF->Row(array('Sueldo Basico Mensual:', number_format($this->metSueldoBasico(1), 2, ',', '.')));  
                }
                $this->atFPDF->Ln(2);
                //	Imprimo los conceptos
                $this->atFPDF->SetDrawColor(0, 0, 0);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetFont('Courier', 'B', 11);
                $y = $this->atFPDF->GetY();
                $this->atFPDF->Rect(10, $y, 195, 0.1);
                $this->atFPDF->Cell(104, 5, 'A S I G N A C I O N E S', 0, 0, 'C');
                $this->atFPDF->Cell(104, 5, 'D E D U C C I O N E S', 0, 1, 'C');
                $y = $this->atFPDF->GetY();
                $this->atFPDF->Rect(10, $y, 195, 0.1);
                $this->atFPDF->Ln(1);
                $y = $this->atFPDF->GetY();
                $yi = $y;
                $yd = $y;
                $contador_conceptos = 0;
                $linead = 0;
                $lineai = 0;
                $asignaciones = 0;
                $deducciones = 0;
                $conceptosEmpleado = $this->atEjecucionProcesos->metObtenerConceptosGeneradosEmpleado($idPeriodoProceso, $idEmpleado[$i]);
                foreach ($conceptosEmpleado AS $conceptoEmpleado) {
                    $this->atFPDF->SetDrawColor(255, 255, 255);
                    $this->atFPDF->SetFillColor(255, 255, 255);
                    $this->atFPDF->SetTextColor(0, 0, 0);
                    $this->atFPDF->SetFont('Courier', 'B', 11);
                    $this->atFPDF->SetWidths(array(65, 30, 1));
                    $this->atFPDF->SetAligns(array('L', 'R', 'R'));
                    if (
                        $conceptoEmpleado['cod_detalle'] == "I" || $conceptoEmpleado['cod_detalle'] == "AS" ||
                        $conceptoEmpleado['cod_detalle'] == "B" || $conceptoEmpleado['cod_detalle'] == "T"
                    ) {
                        $asignaciones = $asignaciones + $conceptoEmpleado['num_monto'];
                        $this->atFPDF->SetXY(10, $yi);
                        /*if(strlen($conceptoEmpleado['ind_impresion'])>27) {
                            $yi += 5;
                        }*/
                        $this->atFPDF->Row(array(utf8_decode(substr($conceptoEmpleado['ind_impresion'],0,27)), number_format($conceptoEmpleado['num_monto'], 2, ',', '.'), ''));
                        $yi += 5;
                        $lineai++;
                    } elseif (
                        $conceptoEmpleado['cod_detalle'] == "R" || $conceptoEmpleado['cod_detalle'] == "D"
                    ) {
                        $deducciones = $deducciones + $conceptoEmpleado['num_monto'];
                        $this->atFPDF->SetXY(107, $yd);
                        /*if(strlen($conceptoEmpleado['ind_impresion'])>27) {
                            $yd += 5;
                        }*/
                        $this->atFPDF->Row(array(utf8_decode(substr($conceptoEmpleado['ind_impresion'],0,27)), number_format($conceptoEmpleado['num_monto'], 2, ',', '.'), ''));
                        $yd += 5;
                        $linead++;
                    }
                }

                if ($lineai > $linead) {
                    $contador_conceptos = $lineai;
                } else {
                    $contador_conceptos = $linead;
                }

                if ($yi > $yd) {
                    $y = $yi + 2;
                } else {
                    $y = $yd + 2;
                }
                $this->atFPDF->SetDrawColor(0, 0, 0);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->Rect(10, $y, 195, 0.1);
                $this->atFPDF->Ln(1);
                $this->atFPDF->SetFont('Courier', 'B', 10);
                $this->atFPDF->SetY($y);
                $this->atFPDF->Cell(70, 5, 'TOTAL ASIGNACIONES', 0, 0, 'L');
                $this->atFPDF->Cell(25, 5, number_format($asignaciones, 2, ',', '.'), 0, 0, 'R');
                $this->atFPDF->Cell(1, 5);
                $this->atFPDF->Cell(70, 5, 'TOTAL DEDUCCIONES', 0, 0, 'L');
                $this->atFPDF->Cell(25, 5, number_format($deducciones, 2, ',', '.'), 0, 1, 'R');
                $this->atFPDF->Cell(97, 5);
                $this->atFPDF->Cell(70, 5, 'TOTAL A PAGAR', 0, 0, 'L');
                $totales = $totales + ($asignaciones - $deducciones);
                $this->atFPDF->Cell(25, 5, number_format($asignaciones - $deducciones, 2, ',', '.'), 0, 1, 'R');

                $this->atFPDF->SetFont('Courier', 'B', 10);
                $this->atFPDF->Ln(5);
                $this->atFPDF->SetX(20);
                $this->atFPDF->Cell(80, 5, '____________________', 0, 1, 'C');
                $this->atFPDF->SetX(20);
                $this->atFPDF->Cell(80, 5, 'RECIBI CONFORME', 0, 0, 'C');
                $this->atFPDF->Ln(4);
                $this->atFPDF->SetX(43.5);
                $this->atFPDF->Cell(50, 5, 'C.I: ' . $datosEmpleado['ind_cedula_documento'], 0, 0, 'L');
                $this->atFPDF->Ln(20);
                if ($tipo == 'verPayRoll') {
                    if ($i % 2 == 0) {
                        $this->atFPDF->SetDrawColor(255, 255, 255);
                        $this->atFPDF->SetFillColor(255, 255, 255);
                        $this->atFPDF->SetTextColor(0, 0, 0);
                        $this->atFPDF->SetFont('Courier', 'B', 12);
                        $this->atFPDF->Cell(190, 5, utf8_decode(APP_ORGANISMO), 0, 1, 'L');
                        $this->atFPDF->SetX(15);
                        $this->atFPDF->SetFont('Courier', 'B', 10);
                        $this->atFPDF->Cell(190, 5, 'Fecha de Emision:' . date('Y-m-d H:i:s'), 0, 1, 'R');

                        $this->atFPDF->Ln(5);
                        $this->atFPDF->SetDrawColor(0, 0, 0);
                        $this->atFPDF->SetFillColor(255, 255, 255);
                        $this->atFPDF->SetTextColor(0, 0, 0);
                        $this->atFPDF->SetFont('Courier', 'B', 11);

                        $this->atFPDF->SetWidths(array(70, 65, 60));
                        $this->atFPDF->SetAligns(array('L', 'L', 'L', 'L'));
                        $this->atFPDF->Row(array('Nomina', 'Nombre del Proceso', 'Periodo'));
                        $this->atFPDF->Row(array(utf8_decode($codProceso['ind_titulo_boleta']), utf8_decode($codProceso['ind_nombre_proceso']), date('d-m-Y', strtotime($codProceso['fec_desde'])) . ' A: ' . date('d-m-Y', strtotime($codProceso['fec_hasta']))));
                        $this->atFPDF->Ln(5);
                    }
                }

            }
            $this->atFPDF->Ln(5);
            if ($tipo != 'verPayRoll') {
                $this->atFPDF->Cell(70, 5, 'TOTAL NETO A PAGAR: ' . number_format($totales, 2, ',', '.'), 0, 0, 'L');
            }
            if (preg_match("/MSIE/i", $_SERVER["HTTP_USER_AGENT"])) {
                header("Content-type: application/PDF");
            } else {
                header("Content-type: application/PDF");
                header("Content-Type: application/pdf");
            }
            $this->atFPDF->Output();
            exit;
        }
    }


    public function metEnviarPayRoll($idEmpleado, $idTipoNominaPeriodo, $idPeriodoProceso)
    {
        //exit;
        if ($idTipoNominaPeriodo && $idPeriodoProceso) {
            $datosNomina = $this->atEjecucionProcesos->metObtenerDatosNomina($idTipoNominaPeriodo, $idPeriodoProceso);

            $codProceso = $this->atEjecucionProcesos->metObtenerProceso($idPeriodoProceso);
            $this->atFPDF->setDesde(date('d-m-Y', strtotime($codProceso['fec_desde'])));
            $this->atFPDF->setHasta(date('d-m-Y', strtotime($codProceso['fec_hasta'])));
            $this->atFPDF->setNomina(utf8_decode($codProceso['ind_titulo_boleta']));
            $this->atFPDF->setNombreProceso(utf8_decode($codProceso['ind_nombre_proceso']));
            $this->atFPDF->AddPage();

            $totales = 0;
            $this->atArgsIdEmpleado = $idEmpleado;
            $this->atArgsTipoNomina = $datosNomina['pk_num_tipo_nomina'];

            /*if($this->metObtenerConcepto('SB',$datosNomina['PERIODO'],'FIN')==0) {
                $sueldoBasico = $this->metObtenerConcepto('SB',$datosNomina['PERIODO'],'PRQ') +
                    $this->metObtenerConcepto('SB',$datosNomina['PERIODO'],'PRQ') +
                    $this->metObtenerConcepto('DIF AF',$datosNomina['PERIODO'],'PRQ') +
                    $this->metObtenerConcepto('DIF AF',$datosNomina['PERIODO'],'FIN') +
                    $this->metObtenerConcepto('DSB',$datosNomina['PERIODO'],'PRQ') +
                    $this->metObtenerConcepto('DSB',$datosNomina['PERIODO'],'FIN');
            } else {
                $sueldoBasico = $this->metObtenerConcepto('SB',$datosNomina['PERIODO'],'PRQ') +
                    $this->metObtenerConcepto('SB',$datosNomina['PERIODO'],'FIN') +
                    $this->metObtenerConcepto('R',$datosNomina['PERIODO'],'FIN') +
                    $this->metObtenerConcepto('DIF AF',$datosNomina['PERIODO'],'PRQ') +
                    $this->metObtenerConcepto('DIF AF',$datosNomina['PERIODO'],'FIN');
                    ;

            }*/

            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetWidths(array(23, 30, 110, 30));
            $this->atFPDF->SetAligns(array('L', 'L', 'L', 'L'));
            $this->atFPDF->Row(array(utf8_decode('Código'), utf8_decode('Cédula'), 'Empleado', 'Fecha Ing.'));
            $this->atFPDF->Ln(2);
            $datosEmpleado = $this->atEjecucionProcesos->metObtenerEmpleado($idEmpleado);
            $this->atFPDF->Row(array(str_pad($datosEmpleado['pk_num_empleado'], 6, "0", STR_PAD_LEFT), utf8_decode($datosEmpleado['ind_cedula_documento']), utf8_decode($datosEmpleado['ind_nombre1'] . ' ' . $datosEmpleado['ind_nombre2'] . ' ' . $datosEmpleado['ind_apellido1'] . ' ' . $datosEmpleado['ind_apellido2']), $datosEmpleado['fec_ingreso']));
            $this->atFPDF->Ln(2);
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetWidths(array(23, 140));
            $this->atFPDF->SetAligns(array('L', 'L'));
                if($datosEmpleado['fk_nmb001_num_tipo_nomina']==NOJ || $datosEmpleado['fk_nmb001_num_tipo_nomina']==NOP ){
                    if($datosEmpleado['fk_nmb001_num_tipo_nomina']==NOJ){
                        $cargo = 'Jubilado';
                    }else{
                        $cargo = 'Pensionado';
                    }
                    $this->atFPDF->Row(array('Cargo:', utf8_decode($cargo)));
                }else{
                    $this->atFPDF->Row(array('Cargo:', utf8_decode($datosEmpleado['ind_descripcion_cargo'])));
                }
            $this->atFPDF->SetWidths(array(55, 120));
            $this->atFPDF->SetAligns(array('L', 'L'));
            $this->atFPDF->Ln(2);
            if($datosEmpleado['fk_nmb001_num_tipo_nomina']==NOJ || $datosEmpleado['fk_nmb001_num_tipo_nomina']==NOP ){
              $this->atFPDF->Row(array('Sueldo Basico Mensual:', number_format($this->metSueldoBasicoJP(1), 2, ',', '.')));  
            }else{
              $this->atFPDF->Row(array('Sueldo Basico Mensual:', number_format($this->metSueldoBasico(1), 2, ',', '.')));  
            }
            $this->atFPDF->Ln(2);
            //	Imprimo los conceptos
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFont('Courier', 'B', 11);
            $y = $this->atFPDF->GetY();
            $this->atFPDF->Rect(10, $y, 195, 0.1);
            $this->atFPDF->Cell(104, 5, 'A S I G N A C I O N E S', 0, 0, 'C');
            $this->atFPDF->Cell(104, 5, 'D E D U C C I O N E S', 0, 1, 'C');
            $y = $this->atFPDF->GetY();
            $this->atFPDF->Rect(10, $y, 195, 0.1);
            $this->atFPDF->Ln(1);
            $y = $this->atFPDF->GetY();
            $yi = $y;
            $yd = $y;
            $contador_conceptos = 0;
            $linead = 0;
            $lineai = 0;
            $asignaciones = 0;
            $deducciones = 0;
            $conceptosEmpleado = $this->atEjecucionProcesos->metObtenerConceptosGeneradosEmpleado($idPeriodoProceso, $idEmpleado);
            foreach ($conceptosEmpleado AS $conceptoEmpleado) {
                $this->atFPDF->SetDrawColor(255, 255, 255);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetFont('Courier', 'B', 11);
                $this->atFPDF->SetWidths(array(65, 30, 1));
                $this->atFPDF->SetAligns(array('L', 'R', 'R'));
                if (
                    $conceptoEmpleado['cod_detalle'] == "I" || $conceptoEmpleado['cod_detalle'] == "AS" ||
                    $conceptoEmpleado['cod_detalle'] == "B" || $conceptoEmpleado['cod_detalle'] == "T"
                ) {
                    $asignaciones = $asignaciones + $conceptoEmpleado['num_monto'];
                    $this->atFPDF->SetXY(10, $yi);
                    /*if(strlen($conceptoEmpleado['ind_impresion'])>27) {
                        $yi += 5;
                    }*/
                    $this->atFPDF->Row(array(utf8_decode(substr($conceptoEmpleado['ind_impresion'],0,27)), number_format($conceptoEmpleado['num_monto'], 2, ',', '.'), ''));
                    $yi += 5;
                    $lineai++;
                } elseif (
                    $conceptoEmpleado['cod_detalle'] == "R" || $conceptoEmpleado['cod_detalle'] == "D"
                ) {
                    $deducciones = $deducciones + $conceptoEmpleado['num_monto'];
                    $this->atFPDF->SetXY(107, $yd);
                    /*if(strlen($conceptoEmpleado['ind_impresion'])>27) {
                        $yd += 5;
                    }*/
                    $this->atFPDF->Row(array(utf8_decode(substr($conceptoEmpleado['ind_impresion'],0,27)), number_format($conceptoEmpleado['num_monto'], 2, ',', '.'), ''));
                    $yd += 5;
                    $linead++;
                }
            }

            if ($lineai > $linead) {
                $contador_conceptos = $lineai;
            } else {
                $contador_conceptos = $linead;
            }

            if ($yi > $yd) {
                $y = $yi + 2;
            } else {
                $y = $yd + 2;
            }
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->SetFillColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->Rect(10, $y, 195, 0.1);
            $this->atFPDF->Ln(1);
            $this->atFPDF->SetFont('Courier', 'B', 10);
            $this->atFPDF->SetY($y);
            $this->atFPDF->Cell(70, 5, 'TOTAL ASIGNACIONES', 0, 0, 'L');
            $this->atFPDF->Cell(25, 5, number_format($asignaciones, 2, ',', '.'), 0, 0, 'R');
            $this->atFPDF->Cell(1, 5);
            $this->atFPDF->Cell(70, 5, 'TOTAL DEDUCCIONES', 0, 0, 'L');
            $this->atFPDF->Cell(25, 5, number_format($deducciones, 2, ',', '.'), 0, 1, 'R');
            $this->atFPDF->Cell(97, 5);
            $this->atFPDF->Cell(70, 5, 'TOTAL A PAGAR', 0, 0, 'L');
            $totales = $totales + ($asignaciones - $deducciones);
            $this->atFPDF->Cell(25, 5, number_format($asignaciones - $deducciones, 2, ',', '.'), 0, 1, 'R');

            $this->atFPDF->SetFont('Courier', 'B', 10);
            $this->atFPDF->Ln(5);
            $this->atFPDF->SetX(20);
            $this->atFPDF->Cell(80, 5, '____________________', 0, 1, 'C');
            $this->atFPDF->SetX(20);
            $this->atFPDF->Cell(80, 5, 'RECIBI CONFORME', 0, 0, 'C');
            $this->atFPDF->Ln(4);
            $this->atFPDF->SetX(43.5);
            $this->atFPDF->Cell(50, 5, 'C.I: ' . $datosEmpleado['ind_cedula_documento'], 0, 0, 'L');
            $this->atFPDF->Ln(20);

            $this->atFPDF->Ln(5);
            if (preg_match("/MSIE/i", $_SERVER["HTTP_USER_AGENT"])) {
                header("Content-type: application/PDF");
            } else {
                header("Content-type: application/PDF");
                header("Content-Type: application/pdf");
            }
            $archivo = ROOT.'sistema'.DS.'temporales'.DS.'compilados'.DS.'payroll'.$idEmpleado.'.pdf';

            $datos['idEmpleado'] = $idEmpleado;

            $this->atFPDF->Output($archivo,'F');
            echo json_encode($datos);
            exit;

//            $this->metConfigurarCorreo($idEmpleado);
        }
    }

    public function metConfigurarCorreo()
    {
        $idEmpleado = $this->metObtenerInt('idEmpleado');
        $nomina = $this->metObtenerInt('nomina');
        $proceso = $this->metObtenerInt('proceso');

        $datosNomina = $this->atEjecucionProcesos->metObtenerDatosNomina($nomina, $proceso);
        if($datosNomina['cod_proceso']=='FIN'){
            $ftproceso='SEGUNDA QUINCENA';
        }

        if($datosNomina['cod_proceso']=='PRQ'){
            $ftproceso='PRIMERA QUINCENA';
        }

        $aux = explode("-",$datosNomina['PERIODO']);
        if($aux[1]=='01'){$mes='ENERO';}
        if($aux[1]=='02'){$mes='FEBRERO';}
        if($aux[1]=='03'){$mes='MARZO';}
        if($aux[1]=='04'){$mes='ABRIL';}
        if($aux[1]=='05'){$mes='MAYO';}
        if($aux[1]=='06'){$mes='JUNIO';}
        if($aux[1]=='07'){$mes='JULIO';}
        if($aux[1]=='08'){$mes='AGOSTO';}
        if($aux[1]=='09'){$mes='SEPTIEMBRE';}
        if($aux[1]=='10'){$mes='OCTUBRE';}
        if($aux[1]=='11'){$mes='NOVIEMBRE';}
        if($aux[1]=='12'){$mes='DICIEMBRE';}

        $datosEmpleado = $this->atEjecucionProcesos->metObtenerEmpleado($idEmpleado);
        $archivo = ROOT.'sistema'.DS.'temporales'.DS.'compilados'.DS.'payroll'.$idEmpleado.'.pdf';
        $correo = $datosEmpleado['ind_email'];
        $nombrePersona = $datosEmpleado['ind_nombre1'] . ' ' . $datosEmpleado['ind_nombre2'] . ' ' . $datosEmpleado['ind_apellido1'] . ' ' . $datosEmpleado['ind_apellido2'];
        //$cuerpoCorreo = 'PayRoll de Pago de '.$nombrePersona;
        $cuerpoCorreo = '<b>Buen '.utf8_decode('día').'</b>
                          <br><br>  
                          <b>Tengo el agrado de dirigirme a usted en la oportunidad de saludarle y a su vez remitirle el Pay Roll de Pago correspondiente a la '.$ftproceso.' de '.$mes.' del '.utf8_decode('año').' '.$aux[0].', '.utf8_decode('remisión').' que se '.utf8_decode('efectúa').' con la finalidad de aprovechar el uso de las '.utf8_decode('Tecnologías').' de '.utf8_decode('Información').' y '.utf8_decode('Comunicación').' TIC para disminuir el alto consumo de papel y contribuir en gran medida con el medio ambiente.</b>

                          <br><br>

                          <b>Saludos</b><br><br>
                          <b>Lcdo. '.utf8_decode('Leonardo Linares').' </b><br>
                          <b>DIRECTOR DE RECURSOS HUMANOS</b><br>
                          <b>CONTRALORIA DEL ESTADO SUCRE</b>';

        $this->metObtenerLibreria('PHPMailerAutoload', 'PHPMailer');
        $mail = new PHPMailer();
        //Definir que vamos a usar SMTP
        $mail->IsSMTP();

        $mail->SMTPOptions = array(
            'ssl' => array (
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
        //Esto es para activar el modo depuración. En entorno de pruebas lo mejor es 2, en producción siempre 0
        // 0 = off (producción)
        // 1 = client messages
        // 2 = client and server messages
        $mail->SMTPDebug  = 0;
        //Ahora definimos gmail como servidor que aloja nuestro SMTP
        $mail->Host       = 'cesmail.cgesucre.gob.ve';
        //$mail->Host       = 'smtp.gmail.com';
        //El puerto será el 587 ya que usamos encriptación TLS
        $mail->Port       = 587;
        //Definmos la seguridad como TLS
        $mail->SMTPSecure = 'TLS';
        //Tenemos que usar gmail autenticados, así que esto a TRUE
        $mail->SMTPAuth   = true;
        //Definimos la cuenta que vamos a usar. Dirección completa de la misma
        $mail->Username   = "direccion.recursos.humanos@cgesucre.gob.ve";
        //Introducimos nuestra contraseña de gmail
        $mail->Password   = "llsiete";
        //Definimos el remitente (dirección y, opcionalmente, nombre)
        //$mail->SetFrom('fundicem@gmail.com', 'FUNDICEM');
        $mail->SetFrom('direccion.recursos.humanos@cgesucre.gob.ve', 'DIRECCION DE RECURSOS HUMANOS DE LA CONTRALORIA DEL ESTADO SUCRE');
        //Esta línea es por si queréis enviar copia a alguien (dirección y, opcionalmente, nombre)
        //$mail->AddReplyTo('i.lezama@contraloriamonagas.gob.ve','El de la réplica');
        //Y, ahora sí, definimos el destinatario (dirección y, opcionalmente, nombre)
        $mail->isHTML(true);
        $mail->AddAddress($correo, $nombrePersona);
        //Definimos el tema del email
        $mail->Subject = 'PayRoll de Pago';
        //Y por si nos bloquean el contenido HTML (algunos correos lo hacen por seguridad) una versión alternativa en texto plano (también será válida para lectores de pantalla)
        $mail->Body = $cuerpoCorreo;
        //Enviamos el correo
        $mail->addAttachment($archivo);
        // Envio el correo
        $result = $mail->Send();

        if(!$result) {
            //echo "error ".$mail->ErrorInfo;
            $datos['mensaje'] = 'no';
            $datos['error'] = $mail->ErrorInfo;
            echo json_encode($datos);
        } else {
            $datos['mensaje'] = 'ok';
            echo json_encode($datos);
        }
    }


}
