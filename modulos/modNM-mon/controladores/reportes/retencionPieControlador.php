<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          xxxxxxxxxxxxxx                   |                                    |                                |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |                                       |                         |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class retencionPieControlador extends Controlador

{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;
    private $atConsultasComunes;
    private $atTipoNomina;


    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->metObtenerLibreria('headerRetencionPie', 'modNM');
        $this->atFPDF = new pdfRetencionPie('P', 'mm', array(200, 279));
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina());
        $this->atVista->metRenderizar('index');
    }


    public function metPdfRetencionPie($idProcesoPeriodo)
    {
        $nomina =$this->atEjecucionProcesos->metObtenerProceso($idProcesoPeriodo);
        $empleados = $this->atConsultasComunes->metEmpleadosNomina($idProcesoPeriodo);

        $this->atFPDF->SetTipoNom(utf8_decode($nomina['ind_titulo_boleta']));
        $this->atFPDF->SetProceso(utf8_decode($nomina['ind_nombre_proceso']));
        $this->atFPDF->setPeriodo('MES DE '.$this->metMesLetras($empleados[0]['fec_mes']).' '.$empleados[0]['fec_anio']);

        $this->atFPDF->setDesde(date('d-m-Y', strtotime($nomina['fec_desde'])));
        $this->atFPDF->setHasta(date('d-m-Y', strtotime($nomina['fec_hasta'])));

        $this->atFPDF->setTipoHeader('nomina');


        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 15, 10);
        $this->atFPDF->SetAutoPageBreak(1, 20);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetFont('Arial', '', 6);


        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array( 16, 70, 18, 18, 18, 30));
        //$this->atFPDF->SetWidths(array(8, 20, 85, 24, 40));
        //$this->atFPDF->SetAligns(array('C', 'L', 'C', 'C', 'C', 'C','C','C','C'));
        //$this->atFPDF->SetAligns(array('C', 'C', 'L', 'C', 'C'));

        $this->atFPDF->SetFont('Arial', '', 5);
        $totalSalario = 0;
        $totalRetencion = 0;
        $totalAporte = 0;
        $totalEnterar = 0;

        foreach ($empleados AS $empleado) {
            if(  $empleado['pk_num_empleado'] != '89'){
                $aportes = $this->atConsultasComunes->metEmpleadosAportes($idProcesoPeriodo, $empleado['pk_num_empleado']);
                $asignaciones = $this->atConsultasComunes->metEmpleadosAsignaciones($idProcesoPeriodo, $empleado['pk_num_empleado']);
                $decucciones = $this->atConsultasComunes->metEmpleadosDeducciones($idProcesoPeriodo, $empleado['pk_num_empleado']);
                $this->atArgsIdEmpleado = $empleado['pk_num_empleado'];
                $sueldo = $this->metSueldoBasico(1);

                $primas = 0;
                foreach ($asignaciones AS $asignacion) {
                    if ($asignacion['ind_abrebiatura'] == 'PA' || $asignacion['ind_abrebiatura'] == 'PP' ||
                        $asignacion['ind_abrebiatura'] == 'PH' || $asignacion['ind_abrebiatura'] == 'PJR' ||
                        $asignacion['ind_abrebiatura'] == 'DIF AF' || $asignacion['ind_abrebiatura'] == 'R'
                    ) {
                        $primas = $primas + $asignacion['num_monto'];
                    }
                }

                $retencion = 0;
                foreach ($decucciones AS $decuccion) {
                    if ($decuccion['ind_abrebiatura'] == 'RETENPIE') {
                        $retencion = $decuccion['num_monto'];
                    }
                }

                $aporte = 0;
                foreach ($aportes AS $a) {
                    if ($a['ind_abrebiatura'] == 'APPIE') {
                        $aporte = $a['num_monto'];
                    }
                }

                $salario =  ($sueldo/2)+$primas;//TOTAL SUELDO QUINCENAL + PRIMAS

                $enterar = $retencion + $aporte; //TOTAL DE RETENCION + APORTE

                $totalSalario = $totalSalario + $salario;
                $totalRetencion = $totalRetencion + $retencion;
                $totalAporte = $totalAporte + $aporte;
                $totalEnterar = $totalEnterar + $enterar;

                $this->atFPDF->SetAligns(array('C', 'L', 'C', 'C', 'C', 'C'));

                $this->atFPDF->Row(array(
                    number_format($empleado['ind_cedula_documento'], 0, '', '.'),
                    utf8_decode($empleado['nombre']),
                    number_format($salario, 2, ',', '.'),
                    number_format($retencion, 2, ',', '.'),
                    number_format($aporte, 2, ',', '.'),
                    number_format($enterar,2,',','.')
                ));

                //SALTAR PAGINA
                $y = $this->atFPDF->GetY();
                if ($y > 300) {
                    $this->atFPDF->AddPage();

                }
            }
        }

        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($nomina['fk_rhb001_num_empleado_crea']);
        $revisadoPor = $this->atConsultasComunes->metMostrarEmpleadoJefe($preparadoPor['pk_num_dependencia']);
        $conformadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');


        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->SetAligns(array('C', 'C', 'C', 'C', 'C', 'C'));

        $this->atFPDF->Row(array(
                                    '',
                                    'TOTAL',
                                    number_format($totalSalario,2,',','.'),
                                    number_format($totalRetencion,2,',','.'),
                                    number_format($totalAporte,2,',','.'),
                                    number_format($totalEnterar,2,',','.')
        ));
        $this->atFPDF->Ln(5);

        $this->atFPDF->SetDrawColor(255, 255, 255); $this->atFPDF->SetFillColor(255, 255, 255); $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array(10,100, 100));
        $this->atFPDF->SetAligns(array('','L', 'L'));
        $this->atFPDF->Row(array('','ELABORADO POR:','CONFORMADO POR:'));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['nombre']),utf8_decode($revisadoPor['nombre'])));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['ind_descripcion_cargo']),utf8_decode($revisadoPor['ind_descripcion_cargo'])));

        $this->atFPDF->Ln(8);
        $this->atFPDF->Row(array('','APROBADO POR: '));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($aprobadoPor['nombre'])));

        $this->atFPDF->Row(array('',utf8_decode($aprobadoPor['ind_descripcion_cargo'])));


        $this->atFPDF->Output();
    }
}