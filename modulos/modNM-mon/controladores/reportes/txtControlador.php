<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class txtControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;
    private $atConsultasComunes;
    private $atTipoNomina;
    private $atTxtModelo;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->atTxtModelo = $this->metCargarModelo('txt','reportes');
        $this->metObtenerLibreria('headerNominaConsolidada', 'modNM');
        $this->atFPDF = new pdfNominaConsolidada('L', 'mm', array(216, 356));
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $complementosCss = array(
            'select2/select201ef'
        );
        $complementoJs = array(
            'select2/select2.min'
        );        
        $js= array('materialSiace/App','materialSiace/core/demo/DemoFormComponents','Aplicacion/appFunciones');
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->metRenderizar('index');
    }


    public function metPeriodosDisponiblesTxt()
    {
        $periodos = $this->atTxtModelo->metGenerarPeriodosTxt();
        echo json_encode($periodos);
        exit;
    } 

    public function metProcesosDisponiblesTxt()
    {
        $procesos = $this->atTxtModelo->metProcesosDisponiblesTxt();
        echo json_encode($procesos);
        exit;
    }     


    public function metGenerarTxt()
    {
        
        $LF = 0x0A;
        $CR = 0x0D;
        $nl = sprintf("%c%c",$CR,$LF);
        $fl = sprintf("%c",$CR);
        $txt_nombre = $this->metObtenerTexto("txt_nombre");
        $idPeriodoProceso = $this->metObtenerInt("proceso");
        $texto ="";//texto interno del txt
        $fecha = date("d/m/y");//fecha del txt
        $archivo=fopen($txt_nombre.".txt", "w+");   

        $empleadoR = $this->atTxtModelo->metEmpleadosNomina($idPeriodoProceso);
        //TIPO CUENTA - CUENTA - MONTO - RELLENO - NOMBRE - CEDULA - RELLENO
        $cantidadFilas = count($empleadoR);

        $i = 0;
        $TOTAL_N = 0;
	$TOTAL_L = 0;

        foreach ($empleadoR as $empleado) {
            
            $i++;

            $asignacionesR = $this->atTxtModelo->metEmpleadosAsignaciones($idPeriodoProceso, $empleado['pk_num_empleado']);
            $deduccionesR  = $this->atTxtModelo->metEmpleadosDeducciones($idPeriodoProceso, $empleado['pk_num_empleado']);

            //asignaciones
            if(count($asignacionesR) != 0){
                $totalAsignacionesEmpleado = 0;
                foreach ($asignacionesR AS $asignaciones) {
                    $totalAsignacionesEmpleado =  $totalAsignacionesEmpleado + $asignaciones['num_monto'];
                }
            }else{
                $totalAsignacionesEmpleado = 0;
            }
            //deducciones
            if(count($deduccionesR) != 0){
                $totalDeduccionesEmpleado = 0;
                foreach ($deduccionesR AS $deducciones) {
                    $totalDeduccionesEmpleado =  $totalDeduccionesEmpleado + $deducciones['num_monto'];
                }
            }else{
                $totalDeduccionesEmpleado = 0;
            }

            $TOTAL_L = $totalAsignacionesEmpleado - $totalDeduccionesEmpleado;

	    

	    $TOTAL_L = number_format($TOTAL_L, 2, '.', '');

$TOTAL_N += $TOTAL_L; 

	    //$neto = $this->atTxtModelo->metEmpleadoTotalNeto($idPeriodoProceso, $empleado['pk_num_empleado']);

	

            //$neto['total_neto'] = number_format($neto['total_neto'], 2, '.', '');

	    //$TOTAL_N += $neto['total_neto'];

	    
	   
            $x = explode('.', $TOTAL_L); 
            $int = $x[0];
            $dec = $x[1];
            $field_monto = "$int$dec";
            $monto = (string) str_repeat("0", 11-strlen($field_monto)).$field_monto;


            //tipo de cuenta
            if(strcmp($empleado['tipo_cuenta'],'CO') == 0 ){
                $tipo_cuenta = 0;
            }else{
                $tipo_cuenta = 1;
            }

            //numero de cuenta
            $nrocuenta = (string) str_repeat("0", 20-strlen($empleado['ind_cuenta'])).$empleado['ind_cuenta'];

            //relleno 1
            $relleno_1 = "0770";
            //nombre
            $nombre = preg_replace("/[^A-Za-z 0-9]/", "", $empleado['nombre']);
            $nombre = (string) $nombre.str_repeat(" ", 40-strlen($nombre));

            //cedula
            $cedula = $empleado['ind_cedula_documento'];
            $cedula = (string) str_repeat("0", 10-strlen($cedula)).$cedula;

            //relleno 2
            $relleno_2 = "003291  ";

            if($i != $cantidadFilas)
            {
                $texto.=$tipo_cuenta.$nrocuenta.$monto.$relleno_1.$nombre.$cedula.$relleno_2."$nl";

            } else {

                $texto.=$tipo_cuenta.$nrocuenta.$monto.$relleno_1.$nombre.$cedula.$relleno_2."$fl";

            }


        }

	//$TOTAL_N = number_format($TOTAL_N, 2, '.', '');


        $w = explode('.', $TOTAL_N); 
        $int = $w[0];
        $dec = $w[1];
        if(strlen($dec) == 1)
        {
            $dec = "".$dec."0";
        }

        if(strlen($dec) == 0)
        {
            $dec = "".$dec."00";
        }

        $total = "$int$dec";
        $total_neto = (string) str_repeat("0", 13-strlen($total)).$total;


        $titulo = "HContraloria del Estado                  0102066004000019069301".$fecha.$total_neto."03291 ".$nl;

        fwrite($archivo, $titulo.$texto);
        fclose($archivo);     

        echo json_encode(1);
    }

    public function metDescargarTxt($nombre_archivo)
    {
        $nombre_archivo = strtr($nombre_archivo, " ", "_");
        $enlace = $nombre_archivo.".txt"; 
        header ("Content-Disposition: attachment; filename=".$enlace."\n\n");
        header ("Content-Type: application/octet-stream; charset=iso-8859-1");
        readfile($enlace);
        unlink($enlace);
    }

    public function metPdfNominaConsolidada($idPeriodoProceso)
    {
        $nomina=$this->atEjecucionProcesos->metObtenerProceso($idPeriodoProceso);
        $empleado = $this->atConsultasComunes->metEmpleadosNomina($idPeriodoProceso);
        for ($i = 0; $i < count($empleado); $i++) {
            $empleados[$i] = array(
                'idEmpleado' => $empleado[$i]['pk_num_empleado'],
                'ci' => $empleado[$i]['ind_cedula_documento'],
                'nombre' => $empleado[$i]['ind_nombre1'] . ' ' . $empleado[$i]['ind_nombre2'] . ' ' . $empleado[$i]['ind_apellido1'] . ' ' . $empleado[$i]['ind_apellido2'],
            );
            $empleados[$i]['asignaciones'] = $this->atConsultasComunes->metEmpleadosAsignaciones($idPeriodoProceso, $empleado[$i]['pk_num_empleado']);
            $codAsignaciones = $empleados[$i]['asignaciones'];
            $totalAsignacionesEmpleado = 0;
            foreach ($codAsignaciones AS $codAsignacion) {
                $empleados[$i]['codAsignaciones'][] = $codAsignacion['cod_concepto'];
                $totalAsignacionesEmpleado = $totalAsignacionesEmpleado + $codAsignacion['num_monto'];
            }
            $empleados[$i]['totalAsignacionesEmpleado'] = $totalAsignacionesEmpleado;

            $empleados[$i]['deducciones'] = $this->atConsultasComunes->metEmpleadosDeducciones($idPeriodoProceso, $empleado[$i]['pk_num_empleado']);
            $codDeducciones = $empleados[$i]['deducciones'];
            $totalDeduccionesEmpleado = 0;
            foreach ($codDeducciones AS $codDeduccion) {
                $empleados[$i]['codDeducciones'][] = $codDeduccion['cod_concepto'];
                $totalDeduccionesEmpleado = $totalDeduccionesEmpleado + $codDeduccion['num_monto'];
            }
            $empleados[$i]['totalDeduccionesEmpleado'] = $totalDeduccionesEmpleado;

            $empleados[$i]['aportes'] = $this->atConsultasComunes->metEmpleadosAportes($idPeriodoProceso, $empleado[$i]['pk_num_empleado']);
            $codAportes = $empleados[$i]['aportes'];
            $totalAportesEmpleado = 0;
            foreach ($codAportes AS $codAporte) {
                $empleados[$i]['codAporte'][] = $codAporte['cod_concepto'];
                $totalAportesEmpleado = $totalAportesEmpleado + $codAporte['num_monto'];
            }
            $empleados[$i]['totalAportesEmpleado'] = $totalAportesEmpleado;
        }
        $tituloAsignaciones = $this->atConsultasComunes->metTituloAsignaciones($idPeriodoProceso);
        $tituloDeducciones = $this->atConsultasComunes->metTituloDeducciones($idPeriodoProceso);
        $tituloAportes = $this->atConsultasComunes->metTituloAportes($idPeriodoProceso);

        #### PDF ####
        $this->atFPDF->SetTipoNom(utf8_decode($nomina['ind_titulo_boleta']));
        $this->atFPDF->SetProceso(utf8_decode($nomina['ind_nombre_proceso']));

        $this->atFPDF->setDesde(date('d-m-Y', strtotime($nomina['fec_desde'])));
        $this->atFPDF->setHasta(date('d-m-Y', strtotime($nomina['fec_hasta'])));
        $this->atFPDF->setTipoHeader('nomina');

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 5);
        $this->atFPDF->SetAutoPageBreak(10);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);

####### ASIGNACIONES ########
        if ($tituloAsignaciones) {
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(190, 5, ("ASIGNACIONES"), 0, 1, 'C');
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
            foreach ($tituloAsignaciones AS $titulos) {
                $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, ('T.ASIG.'), 1, 1, 'R', 1);
            $ln = 0;
            $N = 1;
            foreach ($empleados AS $empleado) {
                if ($empleado['asignaciones']) {
                    $ln++;
                    $y = $this->atFPDF->GetY();
                    if ($y > 170) {
                        $this->atFPDF->AddPage();
                        $this->atFPDF->SetFont('Arial', 'B', 8);
                        $this->atFPDF->Cell(190, 5, ("ASIGNACIONES"), 0, 1, 'C');
                        $this->atFPDF->SetFillColor(200, 200, 200);
                        $this->atFPDF->SetFont('Arial', 'B', 6);
                        $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
                        $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
                        $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
                        foreach ($tituloAsignaciones AS $titulos) {
                            $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
                        }
                        $this->atFPDF->Cell(21, 6, ('T.ASIG.'), 1, 1, 'R', 1);
                    } else {
                        $this->atFPDF->SetY($y);
                    }
                    if ($ln % 2 == 0) {
                        $this->atFPDF->SetFillColor(240, 240, 240);
                    } else {
                        $this->atFPDF->SetFillColor(255, 255, 255);
                    }
                    $this->atFPDF->SetFont('Arial', '', 7);
                    $this->atFPDF->Cell(7, 6, $N++, 1, 0, 'C', 1);
                    $this->atFPDF->Cell(15, 6, number_format($empleado['ci'], 0, '', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Cell(60, 6, utf8_decode($empleado['nombre']), 1, 0, 'L', 1);
                    $asignaciones = $empleado['asignaciones'];
                    $codAsignaciones = $empleado['codAsignaciones'];
                    $total = 0;
                    foreach ($tituloAsignaciones AS $titulos) {
                        if (in_array($titulos['cod_concepto'], $codAsignaciones)) {
                            foreach ($asignaciones AS $asignacion) {
                                if ($titulos['cod_concepto'] == $asignacion['cod_concepto']) {
                                    $total = $total + $asignacion['num_monto'];
                                    $this->atFPDF->Cell(21, 6, number_format($asignacion['num_monto'], 2, ',', '.'), 1, 0, 'R', 1);
                                }
                            }
                        } else {
                            $this->atFPDF->Cell(21, 6, number_format(0, 2, ',', '.'), 1, 0, 'R', 1);
                        }
                    }

                    $this->atFPDF->SetFont('Arial', 'B', 7);
                    $this->atFPDF->Cell(21, 6, number_format($total, 2, ',', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Ln();
                }
            }
            ### Totales ###
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->Cell(7, 6, '', 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ' ', 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, 'Totales: ', 1, 0, 'R', 1);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $totalAsignaciones = 0;
            foreach ($tituloAsignaciones AS $titulos) {
                $totalAsignaciones = $totalAsignaciones + $titulos['TOTAL'];
                $this->atFPDF->Cell(21, 6, number_format($titulos['TOTAL'], 2, ',', '.'), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, number_format($totalAsignaciones, 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Ln(6);
            ### Leyenda ###
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 4);
            $this->atFPDF->Cell(30, 5, 'LEYENDA', 1, 0, 'C', 1);
            $this->atFPDF->Cell(50, 5, 'DESCRIPCION', 1, 1, 'C', 1);
            foreach ($tituloAsignaciones AS $titulos) {
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetFont('Arial', 'B', 4);
                $this->atFPDF->Cell(30, 5, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'L', 1);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 4);
                $this->atFPDF->Cell(50, 5, utf8_decode($titulos['ind_impresion']), 1, 1, 'L', 1);
            }
            $this->atFPDF->AddPage();
        }
####### FIN ########
####### DEDUCCIONES ########
        if ($tituloDeducciones) {
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(190, 5, ("DEDUCCIONES"), 0, 1, 'C');
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
            foreach ($tituloDeducciones AS $titulos) {
                $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, ('T.DEDUC.'), 1, 1, 'R', 1);
            $ln = 0;
            $N = 1;
            foreach ($empleados AS $empleado) {
                if ($empleado['deducciones']) {
                    $ln++;
                    $y = $this->atFPDF->GetY();
                    if ($y > 170) {
                        $this->atFPDF->AddPage();
                        $this->atFPDF->SetFont('Arial', 'B', 8);
                        $this->atFPDF->Cell(190, 5, ("DEDUCCIONES"), 0, 1, 'C');
                        $this->atFPDF->SetFillColor(200, 200, 200);
                        $this->atFPDF->SetFont('Arial', 'B', 6);
                        $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
                        $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
                        $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
                        foreach ($tituloDeducciones AS $titulos) {
                            $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
                        }
                        $this->atFPDF->Cell(21, 6, ('T.DEDUC.'), 1, 1, 'R', 1);
                    } else {
                        $this->atFPDF->SetY($y);
                    }
                    if ($ln % 2 == 0) {
                        $this->atFPDF->SetFillColor(240, 240, 240);
                    } else {
                        $this->atFPDF->SetFillColor(255, 255, 255);
                    }
                    $this->atFPDF->SetFont('Arial', '', 7);
                    $this->atFPDF->Cell(7, 6, $N++, 1, 0, 'C', 1);
                    $this->atFPDF->Cell(15, 6, number_format($empleado['ci'], 0, '', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Cell(60, 6, utf8_decode($empleado['nombre']), 1, 0, 'L', 1);
                    $deducciones = $empleado['deducciones'];
                    $codDeducciones = $empleado['codDeducciones'];
                    $total = 0;
                    foreach ($tituloDeducciones AS $titulos) {
                        if (in_array($titulos['cod_concepto'], $codDeducciones)) {
                            foreach ($deducciones AS $deduccion) {
                                if ($titulos['cod_concepto'] == $deduccion['cod_concepto']) {
                                    $total = $total + $deduccion['num_monto'];
                                    $this->atFPDF->Cell(21, 6, number_format($deduccion['num_monto'], 2, ',', '.'), 1, 0, 'R', 1);
                                }
                            }
                        } else {
                            $this->atFPDF->Cell(21, 6, number_format(0, 2, ',', '.'), 1, 0, 'R', 1);
                        }
                    }

                    $this->atFPDF->SetFont('Arial', 'B', 7);
                    $this->atFPDF->Cell(21, 6, number_format($total, 2, ',', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Ln();
                }
            }
            ### Totales ###
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->Cell(7, 6, '', 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ' ', 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, 'Totales: ', 1, 0, 'R', 1);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $totalDeducciones = 0;
            foreach ($tituloDeducciones AS $titulos) {
                $totalDeducciones = $totalDeducciones + $titulos['TOTAL'];
                $this->atFPDF->Cell(21, 6, utf8_decode($titulos['TOTAL']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, number_format($totalDeducciones, 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Ln(6);
            ### Leyenda ###
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 4);
            $this->atFPDF->Cell(30, 5, 'LEYENDA', 1, 0, 'C', 1);
            $this->atFPDF->Cell(50, 5, 'DESCRIPCION', 1, 1, 'C', 1);
            foreach ($tituloDeducciones AS $titulos) {
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetFont('Arial', 'B', 4);
                $this->atFPDF->Cell(30, 5, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'L', 1);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 4);
                $this->atFPDF->Cell(50, 5, utf8_decode($titulos['ind_impresion']), 1, 1, 'L', 1);
            }
            $this->atFPDF->AddPage();
        }
####### FIN ########
####### APORTES ########
        if ($tituloAportes) {
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Cell(190, 5, ("APORTES"), 0, 1, 'C');
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
            foreach ($tituloAportes AS $titulos) {
                $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, ('T.APORTES.'), 1, 1, 'R', 1);
            $ln = 0;
            $N = 1;
            foreach ($empleados AS $empleado) {
                if ($empleado['aportes']) {
                    $ln++;
                    $y = $this->atFPDF->GetY();
                    if ($y > 170) {
                        $this->atFPDF->AddPage();
                        $this->atFPDF->SetFont('Arial', 'B', 8);
                        $this->atFPDF->Cell(190, 5, ("APORTES"), 0, 1, 'C');
                        $this->atFPDF->SetFillColor(200, 200, 200);
                        $this->atFPDF->SetFont('Arial', 'B', 6);
                        $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
                        $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
                        $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
                        foreach ($tituloAportes AS $titulos) {
                            $this->atFPDF->Cell(21, 6, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'R', 1);
                        }
                        $this->atFPDF->Cell(21, 6, ('T.APORTES.'), 1, 1, 'R', 1);
                    } else {
                        $this->atFPDF->SetY($y);
                    }
                    if ($ln % 2 == 0) {
                        $this->atFPDF->SetFillColor(240, 240, 240);
                    } else {
                        $this->atFPDF->SetFillColor(255, 255, 255);
                    }
                    $this->atFPDF->SetFont('Arial', '', 7);
                    $this->atFPDF->Cell(7, 6, $N++, 1, 0, 'C', 1);
                    $this->atFPDF->Cell(15, 6, number_format($empleado['ci'], 0, '', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Cell(60, 6, utf8_decode($empleado['nombre']), 1, 0, 'L', 1);
                    $aportes = $empleado['aportes'];
                    $codAporte = $empleado['codAporte'];
                    $total = 0;
                    foreach ($tituloAportes AS $titulos) {
                        if (in_array($titulos['cod_concepto'], $codAporte)) {
                            foreach ($aportes AS $aporte) {
                                if ($titulos['cod_concepto'] == $aporte['cod_concepto']) {
                                    $total = $total + $aporte['num_monto'];
                                    $this->atFPDF->Cell(21, 6, number_format($aporte['num_monto'], 2, ',', '.'), 1, 0, 'R', 1);
                                }
                            }
                        } else {
                            $this->atFPDF->Cell(21, 6, number_format(0, 2, ',', '.'), 1, 0, 'R', 1);
                        }
                    }

                    $this->atFPDF->SetFont('Arial', 'B', 7);
                    $this->atFPDF->Cell(21, 6, number_format($total, 2, ',', '.'), 1, 0, 'R', 1);
                    $this->atFPDF->Ln();
                }
            }
            ### Totales ###
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->Cell(7, 6, '', 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, ' ', 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, 'Totales: ', 1, 0, 'R', 1);
            $this->atFPDF->SetFont('Arial', 'B', 7);
            $totalAportes = 0;
            foreach ($tituloAportes AS $titulos) {
                $totalAportes = $totalAportes + $titulos['TOTAL'];
                $this->atFPDF->Cell(21, 6, utf8_decode($titulos['TOTAL']), 1, 0, 'R', 1);
            }
            $this->atFPDF->Cell(21, 6, number_format($totalAportes, 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Ln(6);
            ### Leyenda ###
            $this->atFPDF->SetDrawColor(255, 255, 255);
            $this->atFPDF->SetTextColor(0, 0, 0);
            $this->atFPDF->SetFillColor(200, 200, 200);
            $this->atFPDF->SetFont('Arial', 'B', 4);
            $this->atFPDF->Cell(30, 5, 'LEYENDA', 1, 0, 'C', 1);
            $this->atFPDF->Cell(50, 5, 'DESCRIPCION', 1, 1, 'C', 1);
            foreach ($tituloAportes AS $titulos) {
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetFont('Arial', 'B', 4);
                $this->atFPDF->Cell(30, 5, utf8_decode($titulos['ind_abrebiatura']), 1, 0, 'L', 1);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetFont('Arial', '', 4);
                $this->atFPDF->Cell(50, 5, utf8_decode($titulos['ind_impresion']), 1, 1, 'L', 1);
            }
            $this->atFPDF->AddPage();
        }
####### FIN ########
####### REMUNERACION ########
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Cell(190, 5, ("TOTAL REMUNERACION"), 0, 1, 'C');
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetFont('Arial', 'B', 6);
        $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
        $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
        $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
        $this->atFPDF->Cell(21, 6, 'T.ASIG', 1, 0, 'R', 1);
        $this->atFPDF->Cell(21, 6, 'T.DEDUC.', 1, 0, 'R', 1);
        $this->atFPDF->Cell(21, 6, 'T.PAGAR.', 1, 0, 'R', 1);
        $this->atFPDF->Cell(30, 6, ('FIRMA'), 1, 1, 'R', 1);
        $ln = 0;
        $N = 1;
        $asignacionEmpleados = 0;
        $deduccionEmpleados = 0;

        foreach ($empleados AS $empleado) {
            $asignacionEmpleados = $asignacionEmpleados + $empleado['totalAsignacionesEmpleado'];
            $deduccionEmpleados = $deduccionEmpleados + $empleado['totalDeduccionesEmpleado'];
            $ln++;
            $y = $this->atFPDF->GetY();
            if ($y > 170) {
                $this->atFPDF->AddPage();
                $this->atFPDF->SetFont('Arial', 'B', 8);
                $this->atFPDF->Cell(190, 5, ("TOTAL REMUNERACION"), 0, 1, 'C');
                $this->atFPDF->SetFillColor(200, 200, 200);
                $this->atFPDF->SetFont('Arial', 'B', 6);
                $this->atFPDF->Cell(7, 6, utf8_decode('Nº'), 1, 0, 'C', 1);
                $this->atFPDF->Cell(15, 6, ('CEDULA'), 1, 0, 'R', 1);
                $this->atFPDF->Cell(60, 6, ('NOMBRES Y APELLIDOS'), 1, 0, 'L', 1);
                $this->atFPDF->Cell(21, 6, 'T.ASIG', 1, 0, 'R', 1);
                $this->atFPDF->Cell(21, 6, 'T.DEDUC.', 1, 0, 'R', 1);
                $this->atFPDF->Cell(21, 6, 'T.PAGAR.', 1, 0, 'R', 1);
                $this->atFPDF->Cell(30, 6, ('FIRMA'), 1, 1, 'R', 1);
            } else {
                $this->atFPDF->SetY($y);
            }
            if ($ln % 2 == 0) {
                $this->atFPDF->SetFillColor(240, 240, 240);
            } else {
                $this->atFPDF->SetFillColor(255, 255, 255);
            }
            $this->atFPDF->SetFont('Arial', '', 7);
            $this->atFPDF->Cell(7, 6, $N++, 1, 0, 'C', 1);
            $this->atFPDF->Cell(15, 6, number_format($empleado['ci'], 0, '', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(60, 6, utf8_decode($empleado['nombre']), 1, 0, 'L', 1);
            $this->atFPDF->Cell(21, 6, number_format($empleado['totalAsignacionesEmpleado'], 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(21, 6, number_format($empleado['totalDeduccionesEmpleado'], 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(21, 6, number_format(($empleado['totalAsignacionesEmpleado'] - $empleado['totalDeduccionesEmpleado']), 2, ',', '.'), 1, 0, 'R', 1);
            $this->atFPDF->Cell(30, 6, '', 1, 0, 'R', 1);
            $this->atFPDF->Ln();
        }

        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($nomina['fk_rhb001_num_empleado_crea']);
        $revisadoPor = $this->atConsultasComunes->metMostrarEmpleadoJefe($preparadoPor['pk_num_dependencia']);
        $conformadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');


        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetFont('Arial', 'B', 7);
        $this->atFPDF->Cell(7, 6, '', 1, 0, 'C', 1);
        $this->atFPDF->Cell(15, 6, ' ', 1, 0, 'R', 1);
        $this->atFPDF->Cell(60, 6, 'Totales: ', 1, 0, 'R', 1);
        $this->atFPDF->Cell(21, 6, number_format($totalAsignaciones, 2, ',', '.'), 1, 0, 'R', 1);
        $this->atFPDF->Cell(21, 6, number_format($deduccionEmpleados, 2, ',', '.'), 1, 0, 'R', 1);
        $this->atFPDF->Cell(21, 6, number_format($totalAsignaciones - $deduccionEmpleados, 2, ',', '.'), 1, 0, 'R', 1);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Ln(6);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetFont('Arial', 'B', 5);
        $this->atFPDF->Cell(30, 5, 'LEYENDA', 1, 0, 'C', 1);
        $this->atFPDF->Cell(50, 5, 'DESCRIPCION', 1, 1, 'C', 1);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->SetFont('Arial', 'B', 5);
        $this->atFPDF->Cell(30, 5, utf8_decode('T.ASIG'), 1, 0, 'L', 1);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 5);
        $this->atFPDF->Cell(150, 5, utf8_decode('TOTAL ASIGNACIÓN'), 1, 1, 'L', 1);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->Cell(30, 5, utf8_decode('T.DEDUC'), 1, 0, 'L', 1);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 5);
        $this->atFPDF->Cell(150, 5, utf8_decode('TOTAL DEDUCCIÓN'), 1, 1, 'L', 1);
        $this->atFPDF->SetFillColor(200, 200, 200);
        $this->atFPDF->Cell(30, 5, utf8_decode('T.PAGAR'), 1, 0, 'L', 1);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetFont('Arial', '', 5);
        $this->atFPDF->Cell(150, 5, utf8_decode('TOTAL PAGAR'), 1, 1, 'L', 1);
        $this->atFPDF->Ln(6);

        $yf = $this->atFPDF->GetY();
        $this->atFPDF->Rect(10, $y + 6, 70, 0.1, "DF");

        $this->atFPDF->SetXY(200, $yf + 10);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->Cell(200, 3, '______________________________________________                                    ___________________________________________', 0, 1, 'L');

        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(200, $yf + 15);
        $this->atFPDF->Cell(100, 3, ('Elaborado Por:                                                                                                         Revisado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(100, 3, (''), 0, 1, 'L');
        $this->atFPDF->SetXY(200, $yf + 18);
        $this->atFPDF->MultiCell(50, 3, utf8_decode($preparadoPor['nombre']), 0, 'L');//nombre de quien elabora
        $this->atFPDF->SetXY(276, $yf + 18);
        $this->atFPDF->Cell(100, 3, utf8_decode($revisadoPor['nombre']), 0, 1, 'L');//nombre de quien revisa
        $this->atFPDF->SetXY(200, $yf + 20);
        $this->atFPDF->MultiCell(50, 3, utf8_decode($preparadoPor['ind_descripcion_cargo']), 0, 'L');//cargo de quien elabora
        $this->atFPDF->SetXY(276, $yf + 20);
        $this->atFPDF->Cell(100, 3, utf8_decode($revisadoPor['ind_descripcion_cargo']), 0, 1, 'L');//Cargo de quien revisa

        $this->atFPDF->Rect(10, $y + 20, 70, 0.1, "DF");
        $this->atFPDF->SetXY(200, $yf + 35);
        $this->atFPDF->Cell(100, 3, ' _____________________________________________                                        ___________________________________________', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(200, $yf + 40);
        $this->atFPDF->Cell(100, 3, ('Conformado Por:                                                                                                      Aprobado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(100, 3, (''), 0, 1, 'L');
        $this->atFPDF->SetXY(200, $yf + 43);
        $this->atFPDF->Cell(100, 3, utf8_decode($conformadoPor['nombre']), 0, 0, 'L');//nombre de quien conforma
        $this->atFPDF->SetXY(276, $yf + 43);
        $this->atFPDF->Cell(100, 3, utf8_decode($aprobadoPor['nombre']), 0, 1, 'L');//nombre de quien aprueba
        $this->atFPDF->SetXY(200, $yf + 45);
        $this->atFPDF->Cell(100, 3, utf8_decode($conformadoPor['ind_descripcion_cargo']), 0, 0, 'L');//cargo de quien conforma
        $this->atFPDF->SetXY(276, $yf + 45);
        $this->atFPDF->Cell(100, 3, utf8_decode($aprobadoPor['ind_descripcion_cargo']), 0, 1, 'L');//Cargo de quien aprueba

        $this->atFPDF->Output();
    }
}
