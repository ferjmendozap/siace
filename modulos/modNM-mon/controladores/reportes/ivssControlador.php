<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ivssControlador extends Controlador
{
    private $atFPDF;
    private $atIvss;

    public function __construct()
    {
        parent::__construct();
        $this->atIvss = $this->metCargarModelo('acumuladoPrestacionesSociales', 'reportes');
        $this->atIvss = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->metObtenerLibreria('headerIvss', 'modNM');
        $this->atFPDF = new pdfIvss('P', 'mm', array(200, 279));
    }

    public function metIndex()
    {
        $this->atVista->metRenderizar('seleccion');
    }

    public function metObtenerMes($mes)
    {
        switch($mes){
            case 1:
                $nombreMes = 'ENERO';
                break;
            case 2:
                $nombreMes = 'FEBRERO';
                break;
            case 3:
                $nombreMes = 'MARZO';
                break;
            case 4:
                $nombreMes = 'ABRIL';
                break;
            case 5:
                $nombreMes = 'MAYO';
                break;
            case 6:
                $nombreMes = 'JUNIO';
                break;
            case 7:
                $nombreMes = 'JULIO';
                break;
            case 8:
                $nombreMes = 'AGOSTO';
                break;
            case 9:
                $nombreMes = 'SEPTIEMBRE';
                break;
            case 10:
                $nombreMes = 'OCTUBRE';
                break;
            case 11:
                $nombreMes = 'NOVIEMBRE';
                break;
            case 12:
                $nombreMes = 'DICIEMBRE';
                break;
            case 13:
                $nombreMes = 'TOTALES';
                break;
        }
        return $nombreMes;
    }

    public function metPdfIvss($idEmpleado)
    {
        $js[] = 'Aplicacion/appFunciones';
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';

        $Empleado = $this->atIvss->metNombreEmpleado($idEmpleado);
        $Organismo = $this->atIvss->metOrganismo();
        $this->atFPDF->setEmpleado($Empleado['nombre']);
        $this->atFPDF->setCedula(number_format($Empleado['ind_cedula_documento'],0,',','.'));
        $this->atFPDF->setFechaIngreso($Empleado['fec_ingreso']);
        $this->atFPDF->setFechaEgreso($Empleado['fec_egreso']);
        $this->atFPDF->setOrganismo($Organismo['ind_descripcion_empresa']);
        $this->atFPDF->setRifOrganismo($Organismo['ind_documento_fiscal']);
        $this->atFPDF->setDirOrganismo($Organismo['ind_direccion']);
        $this->atFPDF->setRepresentante($Organismo['nombre']);
        $this->atFPDF->setCiRepresentante(number_format($Organismo['ind_cedula_documento'],0,'','.'));
        $this->atFPDF->setTlfOrganismo($Organismo['ind_telefono']);
        $this->atFPDF->setCargoRepresentante($Organismo['ind_descripcion_cargo']);
        $this->atFPDF->setLugarOrganismo($Organismo['ind_ciudad']);

        $this->atFPDF->SetMargins(8, 5, 5);


        $contAnio = explode("-",$Empleado['fec_ingreso']);
        $difAnio = (date('Y') - $contAnio[0]) + 1;

        $pag = (int)($difAnio/6);
        if($difAnio%6!=0){
            $pag +=1;
        }
        $anioI = $contAnio[0];

        for ($i=0; $i<$pag; $i++){
            $this->atFPDF->AddPage();
            $this->atFPDF->SetFont('Arial', 'B', 6);
            $this->atFPDF->SetXY(8,91);
            $this->atFPDF->Cell(22, 7, '', 1, 0, 'C');
            $this->atFPDF->SetXY(15,91);
            $this->atFPDF->Cell(15, 4, utf8_decode('AÑOS'), 0, 0, 'C');
            $this->atFPDF->SetXY(6,93);
            $this->atFPDF->Cell(15, 4, ('MESES'), 0, 0, 'C');

            $this->atFPDF->SetY(98);

            for ($meses=1; $meses<=13; $meses++) {
                $this->atFPDF->SetFont('Arial', 'B', 7);
                $this->atFPDF->SetX(8);
                $this->atFPDF->Cell(22, 7, $this->metObtenerMes($meses), 1, 1, 'L');
            }
            $this->atFPDF->SetXY(30,91);
            for ($anios=$anioI+($i*6); $anios<$anioI+6+($i*6); $anios++){
                $acum[$anios] = 0;
                $x = $this->atFPDF->GetX();
                $y = $this->atFPDF->GetY();
                if($anios<=date('Y')) {
                    $this->atFPDF->Cell(26.7, 7, $anios, 1, 0, 'R');
                    for ($meses=1; $meses<=13; $meses++) {
                        if(strlen($meses)==1) {
                            $mes = '0'.$meses;
                        } else {
                            $mes = $meses;
                        }
                        $periodo = $this->atIvss->metPeriodos($idEmpleado,$mes,$anios);
                        $this->atFPDF->SetXY($x,$y+(7*$meses));
                        if($meses == 13){
                            $this->atFPDF->Cell(26.7, 7, number_format($acum[$anios],2,',','.'), 1, 0, 'R');
                        }elseif($periodo['num_sueldo_base'] == ''){
                            $this->atFPDF->Cell(26.7, 7, '0,00', 1, 0, 'R');
                        }else{
                            if(isset($acum[$anios])){
                                $this->atFPDF->SetFont('Arial', 'B', 7);
                                $acum[$anios]+= $periodo['num_sueldo_base'];
                            }
                            $this->atFPDF->Cell(26.7, 7, number_format($periodo['num_sueldo_base'],2,',','.'), 1, 0, 'R');
                        }
                    }
                    $this->atFPDF->SetXY($x+26.7,$y);

                } else {

                    $this->atFPDF->Cell(26.7, 7, '', 1, 0, 'R');
                    for ($meses=1; $meses<=13; $meses++) {
                        $this->atFPDF->SetXY($x,$y+(7*$meses));
                        $this->atFPDF->Cell(26.7, 7, '', 1, 0, 'R');
                        $this->atFPDF->SetXY($x+26.7,$y);
                    }
                }
            }
        }

        $this->atFPDF->Output();

    }


}