<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          xxxxxxxxxxxxxx                   |                                    |                                |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |                                       |                         |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class prestamosCajaDeAhorroControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;
    private $atConsultasComunes;
    private $atTipoNomina;
    private $atConcepto;


    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->atConcepto = $this->metCargarModelo('concepto', 'maestros');
        $this->metObtenerLibreria('headerPrestamoCajaDeAhorro', 'modNM');
        $this->atFPDF = new pdfPrestamosCajaDeAhorro('P', 'mm', array(200, 279));
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina());
        $this->atVista->metRenderizar('index');
    }

    public function metIndexOtro()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina());
        $this->atVista->assign('conceptos', $this->atConcepto->metListarConcepto(true));
        $this->atVista->metRenderizar('indexOtro');
    }


    public function metPdfPrestamosCajaDeAhorro($idProcesoPeriodo,$concepto = false)
    {


        $nomina=$this->atEjecucionProcesos->metObtenerProceso($idProcesoPeriodo);
        $empleados = $this->atConsultasComunes->metEmpleadosNomina($idProcesoPeriodo);


        $this->atFPDF->SetTipoNom(utf8_decode($nomina['ind_titulo_boleta']));
        $this->atFPDF->SetProceso(utf8_decode($nomina['ind_nombre_proceso']));
        $this->atFPDF->setPeriodo('MES DE '.$this->metMesLetras($empleados[0]['fec_mes']).' '.$empleados[0]['fec_anio']);

        $this->atFPDF->setDesde(date('d-m-Y', strtotime($nomina['fec_desde'])));
        $this->atFPDF->setHasta(date('d-m-Y', strtotime($nomina['fec_hasta'])));

        $this->atFPDF->setTipoHeader('nomina');

        if(!$concepto){
            $concepto = 'PCAJAH';
        }

        $prestamo=$this->atConsultasComunes->metPrestamoCajaDeAhorro($idProcesoPeriodo,$concepto);


        if(!isset($prestamo[0])) {
            $crea = 0;
            $nombreConcepto = '';
        } else {
            $nombreConcepto = $prestamo[0]['ind_descripcion'];
            $crea = $prestamo[0]['fk_rhb001_num_empleado_crea'];
        }
        if(!$concepto){
            $this->atFPDF->setNombreProceso(utf8_decode('PRESTAMOS CAJA DE AHORRO'));
        } else {
            $this->atFPDF->setNombreProceso(utf8_decode('REPORTE DE '.$nombreConcepto));
        }


        $this->atFPDF->setTipoHeader('nomina');

        #### PDF ####
        /*
                $this->atFPDF->SetMargins(10, 15, 10);
                $this->atFPDF->AliasNbPages();
        */
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 15, 10);
        $this->atFPDF->SetAutoPageBreak(1, 20);
        $this->atFPDF->AddPage();

        $this->atFPDF->SetFont('Arial', '', 8);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array( 20, 85, 24));
        $this->atFPDF->SetAligns(array('C', 'L', 'C'));

        $total = 0;
        if(is_array($prestamo)) {
            foreach ($prestamo as $n) {
                $total = $total + $n['num_monto'];
                $this->atFPDF->Row(array(number_format($n['ind_cedula_documento'],0,'','.'  ),$n['nombre'],number_format($n['num_monto'],2,',','.')));
            }
        }



        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($crea);
        $revisadoPor = $this->atConsultasComunes->metMostrarEmpleadoJefe($preparadoPor['pk_num_dependencia']);
        $conformadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');


        $this->atFPDF->SetFont('Arial', 'B', 8);

        $this->atFPDF->Row(array('', 'TOTAL', number_format($total,2,',','.')));
        $this->atFPDF->Ln(5);



        $this->atFPDF->SetDrawColor(255, 255, 255); $this->atFPDF->SetFillColor(255, 255, 255); $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array(10,90, 100));
        $this->atFPDF->SetAligns(array('','L', 'L'));
        $this->atFPDF->Row(array('','ELABORADO POR:','CONFORMADO POR:'));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['nombre']),utf8_decode($revisadoPor['nombre'])));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['ind_descripcion_cargo']),utf8_decode($revisadoPor['ind_descripcion_cargo'])));

        $this->atFPDF->Ln(8);
        $this->atFPDF->Row(array('','APROBADO POR: '));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($aprobadoPor['nombre'])));

        $this->atFPDF->Row(array('',utf8_decode($aprobadoPor['ind_descripcion_cargo'])));


        $this->atFPDF->Output();
    }
}