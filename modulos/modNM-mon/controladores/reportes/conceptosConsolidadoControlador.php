<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . DS . 'modNM/controladores/FuncionesNomina/funcionesNominaControlador.php';

class conceptosConsolidadoControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }

    private $atFPDF;
    private $atEjecucionProcesos;
    private $atConsultasComunes;
    private $atTipoNomina;
    private $atInterfazCxP;

    public function __construct()
    {
        parent::__construct();
        $this->__fnConstruct();
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->atInterfazCxP = $this->metCargarModelo('interfazCxP', 'procesos');
        $this->metObtenerLibreria('headerNominaConsolidada', 'modNM');
        $this->atFPDF = new pdfNominaConsolidada('L', 'mm', array(200, 375));
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina(1));
        $this->atVista->metRenderizar('index');
    }


    public function metPdfConceptosConsolidado($idPeriodoProceso)
    {
        $nomina = $this->atEjecucionProcesos->metObtenerProceso($idPeriodoProceso);
        $ingresos = $this->atConsultasComunes->metConceptoConsolidadosIngresos($idPeriodoProceso);
        $deducciones = $this->atConsultasComunes->metConceptosConsolidadosDeducciones2($idPeriodoProceso);
        $aportes = $this->atConsultasComunes->metConceptoConsolidadosAportes($idPeriodoProceso);


        #### PDF ####
        $this->atFPDF->SetTipoNom(utf8_decode($nomina['ind_titulo_boleta']));
        $this->atFPDF->SetProceso(utf8_decode($nomina['ind_nombre_proceso']));
        $this->atFPDF->setDesde(date('d-m-Y', strtotime($nomina['fec_desde'])));
        $this->atFPDF->setHasta(date('d-m-Y', strtotime($nomina['fec_hasta'])));
        $this->atFPDF->setTipoHeader('conceptos');
        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 5, 5);
        $this->atFPDF->SetAutoPageBreak(10);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $totalIngresos = 0;
        foreach ($ingresos AS $ingreso) {

            if (
                $ingreso['cod_detalle'] == 'I' || $ingreso['cod_detalle'] == 'T' ||
                $ingreso['cod_detalle'] == 'B' || $ingreso['cod_detalle'] == 'AS'
            ) {

                $totalIngresos = $totalIngresos + $ingreso['num_monto'];
                $this->atFPDF->SetFont('Arial', '', 8);
                $this->atFPDF->SetWidths(array(30, 70, 30, 30, 30));
                $this->atFPDF->SetAligns(array('C', 'L', 'R', 'R', 'R'));
                $this->atFPDF->Row(array($ingreso['cod_partida'], utf8_decode($ingreso['ind_impresion']), number_format($ingreso['num_monto'], 2, ',', '.'), '', ''));
            }
        }
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Ln(5);
        $totalDeducciones = 0;
        foreach ($deducciones AS $deduccion) {
            if (
                $deduccion['cod_detalle'] == 'D' || $deduccion['cod_detalle'] == 'R'
            ) {
                $totalDeducciones = $totalDeducciones + $deduccion['num_monto'];
                $this->atFPDF->SetFont('Arial', '', 8);
                $this->atFPDF->SetWidths(array(30, 70, 30, 30, 30));
                $this->atFPDF->SetAligns(array('C', 'L', 'R', 'R', 'R'));
                $this->atFPDF->Row(array('', utf8_decode($deduccion['ind_impresion']), '', number_format($deduccion['num_monto'], 2, ',', '.'), ''));
            }
        }
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Ln(5);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->Ln(2);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Row(array((''), ('TOTAL NETO'), number_format($totalIngresos, 2, ',', '.'), number_format($totalDeducciones, 2, ',', '.'), ''));
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $y = $this->atFPDF->GetY();
        $this->atFPDF->Ln(2);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(0, 0, 0);
        $this->atFPDF->Rect(10, $y, 190, 0.1, "DF");
        $this->atFPDF->Ln(2);
        $this->atFPDF->SetDrawColor(255, 255, 255);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', 'B', 8);
        $this->atFPDF->Row(array((''), ('TOTAL NETO  A PAGAR'), '', '', number_format($totalIngresos - $totalDeducciones, 2, ',', '.')));

        $impresion_partida = count($ingresos) + count($deducciones);
        if ($aportes) {
            $this->atFPDF->Ln(8);
            $yr = $this->atFPDF->GetY();
            $f = 90;
            if ($impresion_partida <= 8)
                $impresion_partida2 = $impresion_partida;
            else
                $impresion_partida2 = $impresion_partida - 8;
            $h = $f;
            $this->atFPDF->SetXY(220, $h);
            $this->atFPDF->SetWidths(array(30, 70, 30));
            $this->atFPDF->SetAligns(array('C', 'L', 'R'));
            $this->atFPDF->SetDrawColor(0, 0, 0);
            $this->atFPDF->Row(array('PARTIDA', 'CONCEPTO', 'MONTO'));
            $y = $this->atFPDF->GetY();
            $this->atFPDF->SetXY(220, $h);
            $totalApotes = 0;
            foreach ($aportes AS $aporte) {
                $h = $h + 3;
                $totalApotes = $totalApotes + $aporte['num_monto'];
                $this->atFPDF->SetXY(220, $h);
                $this->atFPDF->SetFont('Arial', '', 8);
                $this->atFPDF->SetWidths(array(30, 70, 30));
                $this->atFPDF->SetAligns(array('C', 'L', 'R'));
                $this->atFPDF->Row(array($aporte['cod_partida'], utf8_decode($aporte['ind_denominacion']), number_format($aporte['num_monto'], 2, ',', '.')));
            }
            $this->atFPDF->SetXY(220, $h + 5);
            $this->atFPDF->SetFont('Arial', 'B', 8);
            $this->atFPDF->Row(array((''), ('TOTAL NETO  A PAGAR'), number_format($totalApotes, 2, ',', '.')));
            $this->atFPDF->Ln(5);
        }
        $yf = $this->atFPDF->GetY();

        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($nomina['fk_rhb001_num_empleado_crea']);
        $revisadoPor = $this->atConsultasComunes->metMostrarEmpleadoJefe($preparadoPor['pk_num_dependencia']);
        $conformadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');

        $this->atFPDF->SetXY(200, $yf + 10);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->Cell(200, 3, '______________________________________________                                    ___________________________________________', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(200, $yf + 15);
        $this->atFPDF->Cell(100, 3, ('Elaborado Por:                                                                                                         Conformado Por:'), 0, 0, 'L');
        $this->atFPDF->Cell(100, 3, (''), 0, 1, 'L');
        $this->atFPDF->SetXY(200, $yf + 18);
        $this->atFPDF->MultiCell(50, 3, utf8_decode($preparadoPor['nombre']), 0, 'L');//nombre de quien elabora
        $this->atFPDF->SetXY(276, $yf + 18);
        $this->atFPDF->Cell(100, 3, utf8_decode($revisadoPor['nombre']), 0, 1, 'L');//nombre de quien revisa
        $this->atFPDF->SetXY(200, $yf + 20);
        $this->atFPDF->MultiCell(50, 3, utf8_decode($preparadoPor['ind_descripcion_cargo']), 0, 'L');//cargo de quien elabora
        $this->atFPDF->SetXY(276, $yf + 20);
        $this->atFPDF->Cell(100, 3, utf8_decode($revisadoPor['ind_descripcion_cargo']), 0, 1, 'L');//Cargo de quien revisa
        $this->atFPDF->SetXY(200, $yf + 35);
        $this->atFPDF->Cell(100, 3, ' _____________________________________________', 0, 1, 'L');
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetXY(200, $yf + 40);
        $this->atFPDF->Cell(100, 3, ('Aprobado Por: '), 0, 0, 'L');
        $this->atFPDF->Cell(100, 3, (''), 0, 1, 'L');
        $this->atFPDF->SetXY(200, $yf + 43);
        $this->atFPDF->Cell(100, 3, utf8_decode($aprobadoPor['nombre']), 0, 0, 'L');//nombre de quien conforma
        $this->atFPDF->SetXY(200, $yf + 45);
        $this->atFPDF->Cell(100, 3, utf8_decode($aprobadoPor['ind_descripcion_cargo']), 0, 0, 'L');//cargo de quien conforma
        $this->atFPDF->Output();
    }
}