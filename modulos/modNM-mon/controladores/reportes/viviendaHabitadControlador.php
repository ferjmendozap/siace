<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modNM' . DS . 'controladores' . DS . 'FuncionesNomina' . DS . 'funcionesNominaControlador.php';

class viviendaHabitadControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }
    private $viviendaHabitad;
    private $atConsultasComunes;
    private $atTipoNomina;
    private $atFPDF;
    private $atFuncionesModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atFuncionesModelo = new funcionesNominaModelo();
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');

        $this->metObtenerLibreria('headerViviendaHabitad', 'modNM');
        #$this->atFPDF = new pdfViviendaHabitad('P', 'mm', array(200, 279));
        $this->atFPDF = new pdfViviendaHabitad('P', 'mm', 'A4');
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina());
        $this->atVista->metRenderizar('index');
    }

    public function metMesLetras($mes){

        if(strlen($mes)==1){
            $mes="0".$mes;
        }
        $metMesLetras=array(
            '01'=>'ENERO',
            '02'=>'FEBRERO',
            '03'=>'MARZO',
            '04'=>'ABRIL',
            '05'=>'MAYO',
            '06'=>'JUNIO',
            '07'=>'JULIO',
            '08'=>'AGOSTO',
            '09'=>'SEPTIEMBRE',
            '10'=>'OCTUBRE',
            '11'=>'NOVIEMBRE',
            '12'=>'DICIEMBRE'
        );

        return $metMesLetras[$mes];
    } // END FUNCTION

    public function metPdfViviendaHabitad($idProcesoPeriodo)
    {
        define('PROCESO','ok');

        $empleados = $this->atConsultasComunes->metEmpleadosNomina($idProcesoPeriodo);
        $nomina=$this->atEjecucionProcesos->metObtenerProceso($idProcesoPeriodo);

        $this->atFPDF->SetTipoNom(utf8_decode($nomina['ind_titulo_boleta']));
        $this->atFPDF->SetProceso(utf8_decode($nomina['ind_nombre_proceso']));
        $this->atFPDF->setPeriodo('MES DE '.$this->metMesLetras($empleados[0]['fec_mes']).' '.$empleados[0]['fec_anio']);

        $this->atFPDF->setDesde(date('d-m-Y', strtotime($nomina['fec_desde'])));
        $this->atFPDF->setHasta(date('d-m-Y', strtotime($nomina['fec_hasta'])));

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 15, 10);
        $this->atFPDF->SetAutoPageBreak(1, 20);
        $this->atFPDF->AddPage();
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array(16, 55, 17, 17, 17, 17, 17, 17, 20));

        $totalDif = 0;
        $totalFaov = 0;
        $totalDfaov = 0;
        $totalAporte = 0;
        $totalDaporte = 0;
        $totalEnterar = 0;
        $totalSalario = 0;

        foreach ($empleados AS $empleado) {
            $deducciones = $this->atConsultasComunes->metEmpleadosDeducciones($idProcesoPeriodo, $empleado['pk_num_empleado']);
            $asignaciones = $this->atConsultasComunes->metEmpleadosAsignaciones($idProcesoPeriodo, $empleado['pk_num_empleado']);
            $aportes = $this->atConsultasComunes->metEmpleadosAportes($idProcesoPeriodo, $empleado['pk_num_empleado']);

            //SALTAR PAGINA
            $y = $this->atFPDF->GetY();

            if ($y > 300){
                $this->atFPDF->AddPage();
                $this->atFPDF->SetDrawColor(0, 0, 0);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                //$this->atFPDF->SetWidths(array(8, 20, 85, 24, 40));
                $this->atFPDF->SetWidths(array(16, 55, 17, 17, 17, 17, 17, 17, 20));
                $this->atFPDF->SetAligns(array('C', 'L', 'R', 'C', 'C', 'C','C','C','C'));
            }
            $this->atArgsIdEmpleado = $empleado['pk_num_empleado'];
            $this->atArgsPrediodoDesde = $empleado['fec_anio'].'-'.$empleado['fec_mes'];
            if(PROCESO=='RTA'){
                $monto = $this->metSueldoBasico();
            } else {
                $monto = $this->metSueldoBasico(true);
            }
            $diferencia = 0;
            $faov = 0;
            $dfaov = 0;
            $aporte = 0;
            $daporte = 0;

            foreach ($asignaciones as $key=>$value)
            {
                if($value['ind_abrebiatura']=='DSB'){
                    $diferencia = $diferencia + $value['num_monto'];
                }
		if($value['ind_abrebiatura']=='PJERAEN' && (
		    $empleado['pk_num_empleado'] == '8' ||
		    $empleado['pk_num_empleado'] == '122' ||
		    $empleado['pk_num_empleado'] == '82'
			) ){

		$diferencia =  $value['num_monto'];
                }
            
            }

            foreach ($deducciones as $key=>$value)
            {
                if($value['ind_abrebiatura']=='RTFAOV'){
                    $faov = $faov + $value['num_monto'];
                }
                if($value['ind_abrebiatura']=='DF'){
                    $dfaov = $dfaov + $value['num_monto'];
                }

            }
            foreach ($aportes as $key=>$value) {
                if ($value['ind_abrebiatura'] == 'AFAOV') {
                    $aporte = $aporte + $value['num_monto'];
                }
                if ($value['ind_abrebiatura'] == 'DIFEFAOV') {
                    $daporte = $daporte + $value['num_monto'];
                }
            }


            $total = $diferencia + $dfaov + $faov + $aporte + $daporte;

            //$total = $total + (number_format($a,2,'.','') - number_format($d,2,'.',''));
            $this->atFPDF->Row(array(
                number_format($empleado['ind_cedula_documento'], 0, '', '.') ,
                utf8_decode($empleado['nombre']),
                number_format($monto,2,',','.'),
                number_format($diferencia,2,',','.'),
                number_format($faov,2,',','.'),
                number_format($dfaov,2,',','.'),
                number_format($aporte,2,',','.'),
                number_format($daporte,2,',','.'),
                number_format($total,2,',','.')
            ));

            $totalDif = $totalDif + $diferencia;
            $totalFaov = $totalFaov + $faov;
            $totalDfaov = $totalDfaov + $dfaov;
            $totalAporte = $totalAporte + $aporte;
            $totalDaporte = $totalDaporte + $daporte;
            $totalEnterar = $totalEnterar + $total;
            $totalSalario = $totalSalario + $monto;
        }


        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($this->atConsultasComunes->atIdEmpleado);
        $revisadoPor = $this->atConsultasComunes->metMostrarEmpleadoJefe($preparadoPor['pk_num_dependencia']);
        $conformadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');


        $this->atFPDF->SetFont('Arial', 'B', 6);

        $this->atFPDF->Row(array(
            '', 'TOTAL',
            number_format($totalSalario,2,',','.'),
            number_format($totalDif,2,',','.'),
            number_format($totalFaov,2,',','.'),
            number_format($totalDfaov,2,',','.'),
            number_format($totalAporte,2,',','.'),
            number_format($totalDaporte,2,',','.'),
            number_format($totalEnterar,2,',','.')
        ));
        $this->atFPDF->Ln(5);



        $this->atFPDF->SetDrawColor(255, 255, 255); $this->atFPDF->SetFillColor(255, 255, 255); $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array(10,100, 100));
        $this->atFPDF->SetAligns(array('','L', 'L'));
        $this->atFPDF->Row(array('','ELABORADO POR:','CONFORMADO POR:'));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['nombre']),utf8_decode($revisadoPor['nombre'])));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['ind_descripcion_cargo']),utf8_decode($revisadoPor['ind_descripcion_cargo'])));

        $this->atFPDF->Ln(8);
        $this->atFPDF->Row(array('','APROBADO POR: '));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($aprobadoPor['nombre'])));

        $this->atFPDF->Row(array('',utf8_decode($aprobadoPor['ind_descripcion_cargo'])));

        $this->atFPDF->Output();

    }

}
