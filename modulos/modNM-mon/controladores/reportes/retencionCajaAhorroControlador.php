<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        27-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_Modulo . 'modNM' . DS . 'controladores' . DS . 'FuncionesNomina' . DS . 'funcionesNominaControlador.php';

class retencionCajaAhorroControlador extends Controlador
{
    use funcionesNomina {
        funcionesNomina::__construct as private __fnConstruct;
    }
    private $retencionCajaAhorro;
    private $atConsultasComunes;
    private $atTipoNomina;
    private $atFPDF;
    private $atFuncionesModelo;

    public function __construct()
    {
        parent::__construct();
        $this->atFuncionesModelo = new funcionesNominaModelo();
        $this->atTipoNomina = $this->metCargarModelo('tipoNomina', 'maestros');
        $this->atEjecucionProcesos = $this->metCargarModelo('ejecucionProcesos', 'procesos');
        $this->atConsultasComunes = $this->metCargarModelo('consultasComunes', 'reportes');

        $this->metObtenerLibreria('headerRetencionCajaAhorro', 'modNM');
        $this->atFPDF = new pdfRetCajaAhorro('P', 'mm', 'A4');
    }

    public function metIndex()
    {
        $js[] = 'Aplicacion/appFunciones';
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('tipoNomina', $this->atTipoNomina->metListarTipoNomina());
        $this->atVista->metRenderizar('index');
    }

    public function metMesLetras($mes){

        if(strlen($mes)==1){
            $mes="0".$mes;
        }
        $metMesLetras=array(
            '01'=>'ENERO',
            '02'=>'FEBRERO',
            '03'=>'MARZO',
            '04'=>'ABRIL',
            '05'=>'MAYO',
            '06'=>'JUNIO',
            '07'=>'JULIO',
            '08'=>'AGOSTO',
            '09'=>'SEPTIEMBRE',
            '10'=>'OCTUBRE',
            '11'=>'NOVIEMBRE',
            '12'=>'DICIEMBRE'
        );

        return $metMesLetras[$mes];
    } // END FUNCTION

    public function metPdfRetencionCajaAhorro($idProcesoPeriodo)
    {
        define('PROCESO','ok');

        $empleados = $this->atConsultasComunes->metEmpleadosNomina($idProcesoPeriodo);
        $nomina=$this->atEjecucionProcesos->metObtenerProceso($idProcesoPeriodo);

        $this->atFPDF->SetTipoNom(utf8_decode($nomina['ind_titulo_boleta']));
        $this->atFPDF->SetProceso(utf8_decode($nomina['ind_nombre_proceso']));
        $this->atFPDF->setPeriodo('MES DE '.$this->metMesLetras($empleados[0]['fec_mes']).' '.$empleados[0]['fec_anio']);

        $this->atFPDF->setDesde(date('d-m-Y', strtotime($nomina['fec_desde'])));
        $this->atFPDF->setHasta(date('d-m-Y', strtotime($nomina['fec_hasta'])));

        $this->atFPDF->AliasNbPages();
        $this->atFPDF->SetMargins(10, 15, 10);
        $this->atFPDF->SetAutoPageBreak(5, 20);

        $this->atFPDF->AddPage();
        $this->atFPDF->SetFont('Arial', '', 6);
        $this->atFPDF->SetDrawColor(0, 0, 0);
        $this->atFPDF->SetFillColor(255, 255, 255);
        $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array(14, 53, 18, 18, 18, 18, 18, 18, 17));
        $this->atFPDF->SetAligns(array('R', 'L', 'R', 'R', 'R', 'R'));

        $totalRetencion = 0;
        $totalDifRetencion = 0;
        $totalAporte = 0;
        $totalDifAporte = 0;
        $totalMonto = 0;
        $totalDifMonto = 0;
        $totalEnterar = 0;

        foreach ($empleados AS $empleado) {
            $deducciones = $this->atConsultasComunes->metEmpleadosDeducciones($idProcesoPeriodo, $empleado['pk_num_empleado']);
            $asignaciones = $this->atConsultasComunes->metEmpleadosAsignaciones($idProcesoPeriodo, $empleado['pk_num_empleado']);
            $aportes = $this->atConsultasComunes->metEmpleadosAportes($idProcesoPeriodo, $empleado['pk_num_empleado']);

            //SALTAR PAGINA
            $y = $this->atFPDF->GetY();

            if ($y > 200){

                $this->atFPDF->SetFont('Arial', '', 6);
                $this->atFPDF->SetDrawColor(0, 0, 0);
                $this->atFPDF->SetFillColor(255, 255, 255);
                $this->atFPDF->SetTextColor(0, 0, 0);
                $this->atFPDF->SetWidths(array(14, 53, 18, 18, 18, 18, 18, 18, 17));
                $this->atFPDF->SetAligns(array('R', 'L', 'R', 'R', 'R', 'R'));
            }
            $this->atArgsIdEmpleado = $empleado['pk_num_empleado'];
            $this->atArgsPrediodoDesde = $empleado['fec_anio'].'-'.$empleado['fec_mes'];

            if(PROCESO=='RTA'){
                $monto = $this->metSueldoBasico();
            } else {
                $monto = $this->metSueldoBasico(true);
            }

            $retencion = 0;
            $aporte = 0;
            $difMonto = 0;
            $difRetencion = 0;
            $difAporte = 0;

            foreach ($asignaciones as $key=>$value)
            {
                if($value['ind_abrebiatura']=='DSB'){
                    $difMonto = $value['num_monto'];
                }
            }

            foreach ($deducciones as $key=>$value)
            {
                if($value['ind_abrebiatura']=='RETCA'){
                    $retencion = $value['num_monto'];
                }
                if($value['ind_abrebiatura']=='RETECA'){
                    $difRetencion = $value['num_monto'];
                }
            }
            foreach ($aportes as $key=>$value) {
                if ($value['ind_abrebiatura'] == 'APCA') {
                    $aporte = $value['num_monto'];
                }
                if ($value['ind_abrebiatura'] == 'DIFECA') {
                    $difAporte = $value['num_monto'];
                }
            }

            if($retencion != 0){

                $total = $retencion + $aporte;

                $this->atFPDF->Row(array(
                    number_format($empleado['ind_cedula_documento'], 0, '', '.') ,
                    utf8_decode($empleado['nombre']),
                    number_format($monto,2,',','.'),
                    number_format($difMonto,2,',','.'),
                    number_format($retencion,2,',','.'),
                    number_format($difRetencion,2,',','.'),
                    number_format($aporte,2,',','.'),
                    number_format($difAporte,2,',','.'),
                    number_format($total,2,',','.')
                ));

                $totalRetencion = $totalRetencion + $retencion;
                $totalDifRetencion = $totalDifRetencion + $difRetencion;
                $totalAporte = $totalAporte + $aporte;
                $totalDifAporte = $totalDifAporte + $difAporte;
                $totalEnterar = $totalEnterar + $total;
                $totalMonto = $totalMonto + $monto;
                $totalDifMonto = $totalDifMonto + $difMonto;
            }
        }


        $preparadoPor = $this->atConsultasComunes->metMostrarEmpleado($this->atConsultasComunes->atIdEmpleado);
        $revisadoPor = $this->atConsultasComunes->metMostrarEmpleadoJefe($preparadoPor['pk_num_dependencia']);
        $conformadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) DE RECURSOS HUMANOS ');
        $aprobadoPor = $this->atConsultasComunes->metBuscarCargo('DIRECTOR (A) GENERAL');


        $this->atFPDF->SetFont('Arial', 'B', 7);

        $this->atFPDF->Row(array(
            '',
            'TOTAL',
            number_format($totalMonto,2,',','.'),
            number_format($totalDifMonto,2,',','.'),
            number_format($totalRetencion,2,',','.'),
            number_format($totalDifRetencion,2,',','.'),
            number_format($totalAporte,2,',','.'),
            number_format($totalDifAporte,2,',','.'),
            number_format($totalEnterar,2,',','.')

        ));
        $this->atFPDF->Ln(5);



        $this->atFPDF->SetDrawColor(255, 255, 255); $this->atFPDF->SetFillColor(255, 255, 255); $this->atFPDF->SetTextColor(0, 0, 0);
        $this->atFPDF->SetWidths(array(10,100, 100));
        $this->atFPDF->SetAligns(array('','L', 'L'));
        $this->atFPDF->Row(array('','ELABORADO POR:','CONFORMADO POR:'));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['nombre']),utf8_decode($revisadoPor['nombre'])));
        $this->atFPDF->Row(array('',utf8_decode($preparadoPor['ind_descripcion_cargo']),utf8_decode($revisadoPor['ind_descripcion_cargo'])));

        $this->atFPDF->Ln(8);
        $this->atFPDF->Row(array('','APROBADO POR: '));$this->atFPDF->Ln(3);
        $this->atFPDF->Row(array('','__________________'));
        $this->atFPDF->Row(array('',utf8_decode($aprobadoPor['nombre'])));

        $this->atFPDF->Row(array('',utf8_decode($aprobadoPor['ind_descripcion_cargo'])));

        $this->atFPDF->Output();
    }

}