<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class consultasComunesModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metEmpleadosNomina($idProcesoPeriodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              *,
              CONCAT_WS(' ',a003_persona.ind_nombre1,a003_persona.ind_nombre2,a003_persona.ind_apellido1,a003_persona.ind_apellido2) AS nombre,
              a003_persona.ind_documento_fiscal,
              rh_c063_puestos.ind_descripcion_cargo,
              rh_c076_empleado_organizacion.fk_a004_num_dependencia,
              rh_c012_empleado_banco.ind_cuenta,
              nm_c001_tipo_nomina_periodo.fec_anio,
              nm_c001_tipo_nomina_periodo.fec_mes
            FROM
              rh_b001_empleado
            INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN nm_d005_tipo_nomina_empleado_concepto ON rh_b001_empleado.pk_num_empleado = nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
            INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
            
            INNER JOIN rh_c059_empleado_nivelacion ON rh_c059_empleado_nivelacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c059_empleado_nivelacion.fk_rhc063_num_puestos
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
            LEFT JOIN rh_c012_empleado_banco ON rh_b001_empleado.pk_num_empleado = rh_c012_empleado_banco.fk_rhb001_num_empleado
            WHERE
              nm_c002_proceso_periodo.pk_num_proceso_periodo='$idProcesoPeriodo'
            GROUP BY rh_b001_empleado.pk_num_empleado
            ORDER BY  a003_persona.ind_cedula_documento ASC 
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metEmpleadosAsignaciones($idProcesoPeriodo,$idEmpleado)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              nm_d005_tipo_nomina_empleado_concepto.num_monto AS num_monto,
              nm_b002_concepto.ind_abrebiatura,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              nm_b002_concepto.ind_impresion,
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
              pr_b002_partida_presupuestaria.cod_partida,
              pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria,
              IF( debe.pk_num_cuenta,debe.pk_num_cuenta,0 ) AS debe,
              IF( debe20.pk_num_cuenta,debe20.pk_num_cuenta,0 ) AS debe20,
              IF( haber.pk_num_cuenta,haber.pk_num_cuenta,0 ) AS haber,
              IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta,0 ) AS haber20
            FROM
             nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
            INNER JOIN nm_d001_concepto_perfil_detalle ON (
              nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
              nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto AND
              nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
            )
            LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.cod_partida = nm_d001_concepto_perfil_detalle.ind_cod_partida
            LEFT JOIN cb_b004_plan_cuenta AS debe ON debe.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe
            LEFT JOIN cb_b004_plan_cuenta AS debe20 ON debe20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20
            LEFT JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
            LEFT JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
            WHERE
              fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              fk_rhb001_num_empleado='$idEmpleado' AND
              (
                a006_miscelaneo_detalle.cod_detalle='I' OR
                a006_miscelaneo_detalle.cod_detalle='T' OR
                a006_miscelaneo_detalle.cod_detalle='AS' OR
                a006_miscelaneo_detalle.cod_detalle='B'
              )
            ORDER BY nm_b002_concepto.ind_abrebiatura ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metEmpleadosDeducciones($idProcesoPeriodo,$idEmpleado)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              nm_d005_tipo_nomina_empleado_concepto.num_monto AS num_monto,
              nm_b002_concepto.ind_abrebiatura,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              nm_b002_concepto.ind_impresion
            FROM
             nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            WHERE
              fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              fk_rhb001_num_empleado='$idEmpleado' AND
              (
                a006_miscelaneo_detalle.cod_detalle='D' OR
                a006_miscelaneo_detalle.cod_detalle='R'
              )
            ORDER BY nm_b002_concepto.ind_abrebiatura ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metEmpleadosAportes($idProcesoPeriodo,$idEmpleado)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              nm_d005_tipo_nomina_empleado_concepto.num_monto AS num_monto,
              nm_b002_concepto.ind_abrebiatura,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              nm_b002_concepto.ind_impresion
            FROM
             nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            WHERE
              fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              fk_rhb001_num_empleado='$idEmpleado' AND
              a006_miscelaneo_detalle.cod_detalle='A'
            ORDER BY nm_b002_concepto.ind_abrebiatura ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metTituloAsignaciones($idProcesoPeriodo)
    {

        $obligacionListar = $this->_db->query("
            SELECT
              SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS TOTAL,
              nm_b002_concepto.ind_abrebiatura,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              nm_b002_concepto.ind_impresion,
              nm_b005_interfaz_cxp.fk_a003_num_persona_proveedor
            FROM
             nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            LEFT JOIN nm_b005_interfaz_cxp ON nm_b005_interfaz_cxp.pk_num_interfaz_cxp = nm_b002_concepto.fk_nmb005_num_interfaz_cxp
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              (
                a006_miscelaneo_detalle.cod_detalle='I' OR
                a006_miscelaneo_detalle.cod_detalle='T' OR
                a006_miscelaneo_detalle.cod_detalle='AS' OR
                a006_miscelaneo_detalle.cod_detalle='B'
              )
            GROUP BY nm_b002_concepto.pk_num_concepto
            ORDER BY nm_b002_concepto.ind_abrebiatura ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metTituloDeducciones($idProcesoPeriodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS TOTAL,
              nm_b002_concepto.ind_abrebiatura,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              nm_b002_concepto.ind_impresion,
              nm_b005_interfaz_cxp.fk_a003_num_persona_proveedor
            FROM
             nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            LEFT JOIN nm_b005_interfaz_cxp ON nm_b005_interfaz_cxp.pk_num_interfaz_cxp = nm_b002_concepto.fk_nmb005_num_interfaz_cxp
            WHERE
              fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              (
                a006_miscelaneo_detalle.cod_detalle='D' OR
                a006_miscelaneo_detalle.cod_detalle='R'
              )
            GROUP BY nm_b002_concepto.pk_num_concepto
            ORDER BY nm_b002_concepto.ind_abrebiatura ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metTituloAportes($idProcesoPeriodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS TOTAL,
              nm_d005_tipo_nomina_empleado_concepto.num_monto,
              nm_b002_concepto.ind_abrebiatura,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              nm_b002_concepto.ind_impresion
            FROM
             nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            WHERE
              fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              a006_miscelaneo_detalle.cod_detalle='A'
            GROUP BY nm_b002_concepto.pk_num_concepto
            ORDER BY nm_b002_concepto.ind_abrebiatura ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metAportes($idProcesoPeriodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS num_monto,
              pr_b002_partida_presupuestaria.cod_partida,
              a006_miscelaneo_detalle.cod_detalle,
              IF( debe.pk_num_cuenta,debe.pk_num_cuenta,0 ) AS debe,
              IF( debe20.pk_num_cuenta,debe20.pk_num_cuenta,0 ) AS debe20,
              IF( haber.pk_num_cuenta,haber.pk_num_cuenta,0 ) AS haber,
              IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta,0 ) AS haber20
            FROM
              nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
              INNER JOIN nm_d001_concepto_perfil_detalle ON (
                nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto
              )
              INNER JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.cod_partida = nm_d001_concepto_perfil_detalle.ind_cod_partida
              INNER JOIN cb_b004_plan_cuenta AS debe ON debe.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe
              INNER JOIN cb_b004_plan_cuenta AS debe20 ON debe20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20
              INNER JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
              INNER JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              a006_miscelaneo_detalle.cod_detalle='A'
            GROUP BY pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria, nm_b002_concepto.cod_concepto 
            ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metConceptoConsolidadosIngresos($idProcesoPeriodo)
    {

        $obligacionListar = $this->_db->query("
            SELECT
              SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS num_monto,
              pr_b002_partida_presupuestaria.cod_partida,
              pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria,
              nm_b002_concepto.ind_impresion,
              a006_miscelaneo_detalle.cod_detalle,
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
              nm_b005_interfaz_cxp.pk_num_interfaz_cxp,
              nm_b005_interfaz_cxp.fk_a003_num_persona_proveedor,
              nm_b005_interfaz_cxp.fk_a006_num_miscelaneo_det_tipo_pago,
              nm_b005_interfaz_cxp.fk_cpb002_num_tipo_documento,
              IF( debe.pk_num_cuenta,debe.pk_num_cuenta,0 ) AS debe,
              IF( debe20.pk_num_cuenta,debe20.pk_num_cuenta,0 ) AS debe20,
              IF( haber.pk_num_cuenta,haber.pk_num_cuenta,0 ) AS haber,
              IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta,0 ) AS haber20
            FROM
              nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
              INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
              INNER JOIN nm_d001_concepto_perfil_detalle ON (
                nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto AND
                nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
              )
              LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.cod_partida = nm_d001_concepto_perfil_detalle.ind_cod_partida
              LEFT JOIN cb_b004_plan_cuenta AS debe ON debe.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe
              LEFT JOIN cb_b004_plan_cuenta AS debe20 ON debe20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20
              LEFT JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
              LEFT JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
              LEFT JOIN nm_b005_interfaz_cxp ON nm_b005_interfaz_cxp.pk_num_interfaz_cxp = nm_b002_concepto.fk_nmb005_num_interfaz_cxp
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$idProcesoPeriodo'  AND
              (
                a006_miscelaneo_detalle.cod_detalle='I' OR
                a006_miscelaneo_detalle.cod_detalle='T' OR
                a006_miscelaneo_detalle.cod_detalle='AS' OR
                a006_miscelaneo_detalle.cod_detalle='B'
              )
            GROUP BY pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria, nm_b002_concepto.cod_concepto 
            ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metConceptoConsolidadosAportes($idProcesoPeriodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS num_monto,
              pr_b002_partida_presupuestaria.cod_partida,
              pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria,
              pr_b002_partida_presupuestaria.ind_denominacion,
              nm_b002_concepto.ind_impresion,
              a006_miscelaneo_detalle.cod_detalle,
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
              nm_b005_interfaz_cxp.pk_num_interfaz_cxp,
              nm_b005_interfaz_cxp.fk_a003_num_persona_proveedor,
              nm_b005_interfaz_cxp.fk_000_tipo_servicio,
              nm_b005_interfaz_cxp.ind_num_cuenta,
              nm_b005_interfaz_cxp.fk_a006_num_miscelaneo_det_tipo_pago,
              nm_b005_interfaz_cxp.fk_cpb002_num_tipo_documento,
              nm_b005_interfaz_cxp.ind_comentario,
              IF( debe.pk_num_cuenta,debe.pk_num_cuenta,0 ) AS debe,
              IF( debe20.pk_num_cuenta,debe20.pk_num_cuenta,0 ) AS debe20,
              IF( haber.pk_num_cuenta,haber.pk_num_cuenta,0 ) AS haber,
              IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta,0 ) AS haber20
            FROM
              nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
              INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
              INNER JOIN nm_d001_concepto_perfil_detalle ON (
                nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto AND
                nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
              )
              LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.cod_partida = nm_d001_concepto_perfil_detalle.ind_cod_partida
              LEFT JOIN cb_b004_plan_cuenta AS debe ON debe.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe
              LEFT JOIN cb_b004_plan_cuenta AS debe20 ON debe20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20
              LEFT JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
              LEFT JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
              LEFT JOIN nm_b005_interfaz_cxp ON nm_b005_interfaz_cxp.pk_num_interfaz_cxp = nm_b002_concepto.fk_nmb005_num_interfaz_cxp
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$idProcesoPeriodo'  AND
              (
                a006_miscelaneo_detalle.cod_detalle='A'
              )
            GROUP BY pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria, nm_b002_concepto.cod_concepto 
            ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metConceptosConsolidadosDeducciones2($idProcesoPeriodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS num_monto,
              pr_b002_partida_presupuestaria.cod_partida,
              nm_b002_concepto.ind_impresion,
              nm_b002_concepto.pk_num_concepto,
              a006_miscelaneo_detalle.cod_detalle,
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
              nm_b005_interfaz_cxp.fk_a003_num_persona_proveedor,
              nm_b005_interfaz_cxp.fk_a006_num_miscelaneo_det_tipo_pago,
              nm_b005_interfaz_cxp.fk_000_tipo_servicio,
              nm_b005_interfaz_cxp.ind_num_cuenta,
              nm_b005_interfaz_cxp.fk_cpb002_num_tipo_documento,
              nm_b005_interfaz_cxp.ind_comentario,
              nm_b005_interfaz_cxp.pk_num_interfaz_cxp,
              IF( debe.pk_num_cuenta,debe.pk_num_cuenta,0 ) AS debe,
              IF( debe20.pk_num_cuenta,debe20.pk_num_cuenta,0 ) AS debe20,
              IF( haber.pk_num_cuenta,haber.pk_num_cuenta,0 ) AS haber,
              IF( haber20.pk_num_cuenta,haber20.pk_num_cuenta,0 ) AS haber20
            FROM
              nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
              INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
              INNER JOIN nm_d001_concepto_perfil_detalle ON (
                nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto AND
                nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina = nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina
              )
              LEFT JOIN pr_b002_partida_presupuestaria ON pr_b002_partida_presupuestaria.cod_partida = nm_d001_concepto_perfil_detalle.ind_cod_partida
              LEFT JOIN cb_b004_plan_cuenta AS debe ON debe.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe
              LEFT JOIN cb_b004_plan_cuenta AS debe20 ON debe20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20
              LEFT JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
              LEFT JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
              LEFT JOIN nm_b005_interfaz_cxp ON nm_b005_interfaz_cxp.pk_num_interfaz_cxp = nm_b002_concepto.fk_nmb005_num_interfaz_cxp
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$idProcesoPeriodo'  AND
              (
                a006_miscelaneo_detalle.cod_detalle='D' OR
                a006_miscelaneo_detalle.cod_detalle='R'
              )
            GROUP BY pr_b002_partida_presupuestaria.pk_num_partida_presupuestaria, nm_b002_concepto.cod_concepto 
            ORDER BY nm_b002_concepto.pk_num_concepto ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metConceptosConsolidadosDeducciones($idProcesoPeriodo)
    {
        $obligacionListar = $this->_db->query("
            SELECT
              SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS num_monto,
              a006_miscelaneo_detalle.cod_detalle,
              nm_b002_concepto.ind_impresion
            FROM
              nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
              INNER JOIN nm_d001_concepto_perfil_detalle ON (
                nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso AND
                nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto
              )
              INNER JOIN cb_b004_plan_cuenta AS haber ON haber.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber
              INNER JOIN cb_b004_plan_cuenta AS haber20 ON haber20.cod_cuenta=nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_haber_pub20
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              nm_d001_concepto_perfil_detalle.ind_cod_partida IS NULL AND
              nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe  IS NULL AND
              nm_d001_concepto_perfil_detalle.ind_cod_cuenta_contable_debe_pub20  IS NULL AND
              (
                a006_miscelaneo_detalle.cod_detalle='D' OR
                a006_miscelaneo_detalle.cod_detalle='R'
              )
            GROUP BY haber.cod_cuenta, nm_b002_concepto.pk_num_concepto
            ORDER BY a006_miscelaneo_detalle.cod_detalle ASC
        ");
        $obligacionListar->setFetchMode(PDO::FETCH_ASSOC);
        return $obligacionListar->fetchAll();
    }

    public function metPrestacionesSocialesAños()
    {
        $añosPrestacionesSociales = $this->_db->query("
            SELECT
              fec_anio
            FROM
              nm_e002_prestaciones_sociales_calculo
            GROUP BY fec_anio ORDER BY fec_anio DESC

        ");
        $añosPrestacionesSociales->setFetchMode(PDO::FETCH_ASSOC);
        return $añosPrestacionesSociales->fetchAll();
    }

    public function metConsolidadoPrestacionesSociales($fecAño)
    {
        $añosPrestacionesSociales = $this->_db->query("
            SELECT
              rh_b001_empleado.pk_num_empleado AS idEmpleado,
              a003_persona.ind_nombre1,
              a003_persona.ind_nombre2,
              a003_persona.ind_apellido1,
              a003_persona.ind_apellido2,
              a003_persona.ind_cedula_documento,
              rh_c005_empleado_laboral.fec_ingreso,
              rh_c012_empleado_banco.ind_cuenta,
              (
                (
                    SELECT
                      if( ROUND(SUM(num_monto_trimestral),2), ROUND(SUM(num_monto_trimestral),2),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      (
                        nm_e002_prestaciones_sociales_calculo.fec_anio>='$fecAño' AND nm_e002_prestaciones_sociales_calculo.fec_anio<='$fecAño'
                      ) AND
                      (
                        nm_e002_prestaciones_sociales_calculo.fec_mes>='01' AND nm_e002_prestaciones_sociales_calculo.fec_mes<='03'
                      )
                ) + (
                  SELECT
                      if( ROUND(SUM(num_monto_trimestral),2), ROUND(SUM(num_monto_trimestral),2),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      (
                        nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio>='$fecAño' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio<='$fecAño'
                      ) AND
                      (
                        nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes>='01' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes<='03'
                      )
                )
              ) AS TRIMESTRE1,
              (
                (
                    SELECT
                      if( ROUND(SUM(num_monto_trimestral),2), ROUND(SUM(num_monto_trimestral),2),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      (
                        nm_e002_prestaciones_sociales_calculo.fec_anio>='$fecAño' AND nm_e002_prestaciones_sociales_calculo.fec_anio<='$fecAño'
                      ) AND
                      (
                        nm_e002_prestaciones_sociales_calculo.fec_mes>='04' AND nm_e002_prestaciones_sociales_calculo.fec_mes<='06'
                      )
                ) + (
                  SELECT
                      if( ROUND(SUM(num_monto_trimestral),2), ROUND(SUM(num_monto_trimestral),2),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      (
                        nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio>='$fecAño' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio<='$fecAño'
                      ) AND
                      (
                        nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes>='04' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes<='06'
                      )
                )
              ) AS TRIMESTRE2,
              (
                (
                    SELECT
                      if( ROUND(SUM(num_monto_trimestral),2), ROUND(SUM(num_monto_trimestral),2),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      (
                        nm_e002_prestaciones_sociales_calculo.fec_anio>='$fecAño' AND nm_e002_prestaciones_sociales_calculo.fec_anio<='$fecAño'
                      ) AND
                      (
                        nm_e002_prestaciones_sociales_calculo.fec_mes>='07' AND nm_e002_prestaciones_sociales_calculo.fec_mes<='09'
                      )
                ) + (
                  SELECT
                      if( ROUND(SUM(num_monto_trimestral),2), ROUND(SUM(num_monto_trimestral),2),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      (
                        nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio>='$fecAño' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio<='$fecAño'
                      ) AND
                      (
                        nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes>='07' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes<='09'
                      )
                )
              ) AS TRIMESTRE3,
              (
                (
                    SELECT
                      if( ROUND(SUM(num_monto_trimestral),2), ROUND(SUM(num_monto_trimestral),2),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      (
                        nm_e002_prestaciones_sociales_calculo.fec_anio>='$fecAño' AND nm_e002_prestaciones_sociales_calculo.fec_anio<='$fecAño'
                      ) AND
                      (
                        nm_e002_prestaciones_sociales_calculo.fec_mes>='10' AND nm_e002_prestaciones_sociales_calculo.fec_mes<='12'
                      )
                ) + (
                  SELECT
                      if( ROUND(SUM(num_monto_trimestral),2), ROUND(SUM(num_monto_trimestral),2),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      (
                        nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio>='$fecAño' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio<='$fecAño'
                      ) AND
                      (
                        nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes>='10' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes<='12'
                      )
                )
              ) AS TRIMESTRE4,

              (
                (
                    SELECT
                      if( ROUND(SUM(num_monto_anual),2), ROUND(SUM(num_monto_anual),2),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      nm_e002_prestaciones_sociales_calculo.fec_anio='$fecAño'
                ) + (
                  SELECT
                      if( ROUND(SUM(num_monto_anual),2), ROUND(SUM(num_monto_anual),2),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio='$fecAño'
                )
              ) AS ANUAL,
              IF(SUM(nm_e002_prestaciones_sociales_calculo.num_dias_anual),SUM(nm_e002_prestaciones_sociales_calculo.num_dias_anual),0) AS num_dias_anual,
              (
                    SELECT
                      if(SUM(num_anticipo),SUM(num_anticipo),0)
                    FROM
                        rh_b001_empleado
                        INNER JOIN nm_e003_prestaciones_sociales_anticipo ON rh_b001_empleado.pk_num_empleado = nm_e003_prestaciones_sociales_anticipo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado=idEmpleado AND
                      nm_e003_prestaciones_sociales_anticipo.fec_anio='$fecAño'
                ) AS ANTICIPO
            FROM
              a003_persona
              INNER JOIN rh_b001_empleado ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
              INNER JOIN rh_c012_empleado_banco ON rh_b001_empleado.pk_num_empleado = rh_c012_empleado_banco.fk_rhb001_num_empleado
              INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
              INNER JOIN rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado
            WHERE
              nm_e002_prestaciones_sociales_calculo.fec_anio='$fecAño'
            GROUP BY rh_b001_empleado.pk_num_empleado, nm_e002_prestaciones_sociales_calculo.fec_anio
            ORDER BY a003_persona.ind_cedula_documento

        ");

        $añosPrestacionesSociales->setFetchMode(PDO::FETCH_ASSOC);
        return $añosPrestacionesSociales->fetchAll();
    }
    public function metConsolidadoPSTrimestral($fecAño)
    {
        $sql = "
            SELECT
              rh_b001_empleado.pk_num_empleado AS idEmpleado,
              a003_persona.ind_nombre1,
              a003_persona.ind_nombre2,
              a003_persona.ind_apellido1,
              a003_persona.ind_apellido2,
              a003_persona.ind_cedula_documento,
              rh_c005_empleado_laboral.fec_ingreso,
              rh_c012_empleado_banco.ind_cuenta,
              (
                (
                SELECT
                  IF(
                     ROUND(SUM(num_monto_trimestral),2),
                     ROUND(SUM(num_monto_trimestral),2),
                    0
                  )
                FROM
                  rh_b001_empleado
                INNER JOIN
                  nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado = idEmpleado AND(
                    nm_e002_prestaciones_sociales_calculo.fec_anio >= '$fecAño' AND nm_e002_prestaciones_sociales_calculo.fec_anio <= '$fecAño'
                  ) AND(
                    nm_e002_prestaciones_sociales_calculo.fec_mes >= '01' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '03'
                  )
              ) +(
              SELECT
                IF(
                   ROUND(SUM(num_monto_trimestral),2),
                   ROUND(SUM(num_monto_trimestral),2),
                  0
                )
              FROM
                rh_b001_empleado
              INNER JOIN
                nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
              WHERE
                rh_b001_empleado.pk_num_empleado = idEmpleado AND(
                  nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio >= '$fecAño' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio <= '$fecAño'
                ) AND(
                  nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '01' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '03'
                )
            )
              ) AS TRIMESTRE1,
              (
                (
                SELECT
                  IF(
                     ROUND(SUM(num_monto_trimestral),2),
                     ROUND(SUM(num_monto_trimestral),2),
                    0
                  )
                FROM
                  rh_b001_empleado
                INNER JOIN
                  nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado = idEmpleado AND(
                    nm_e002_prestaciones_sociales_calculo.fec_anio >= '$fecAño' AND nm_e002_prestaciones_sociales_calculo.fec_anio <= '$fecAño'
                  ) AND(
                    nm_e002_prestaciones_sociales_calculo.fec_mes >= '04' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '06'
                  )
              ) +(
              SELECT
                IF(
                   ROUND(SUM(num_monto_trimestral),2),
                   ROUND(SUM(num_monto_trimestral),2),
                  0
                )
              FROM
                rh_b001_empleado
              INNER JOIN
                nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
              WHERE
                rh_b001_empleado.pk_num_empleado = idEmpleado AND(
                  nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio >= '$fecAño' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio <= '$fecAño'
                ) AND(
                  nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '04' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '06'
                )
            )
              ) AS TRIMESTRE2,
              (
                (
                SELECT
                  IF(
                     ROUND(SUM(num_monto_trimestral),2),
                     ROUND(SUM(num_monto_trimestral),2),
                    0
                  )
                FROM
                  rh_b001_empleado
                INNER JOIN
                  nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado = idEmpleado AND(
                    nm_e002_prestaciones_sociales_calculo.fec_anio >= '$fecAño' AND nm_e002_prestaciones_sociales_calculo.fec_anio <= '$fecAño'
                  ) AND(
                    nm_e002_prestaciones_sociales_calculo.fec_mes >= '07' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '09'
                  )
              ) +(
              SELECT
                IF(
                   ROUND(SUM(num_monto_trimestral),2),
                   ROUND(SUM(num_monto_trimestral),2),
                  0
                )
              FROM
                rh_b001_empleado
              INNER JOIN
                nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
              WHERE
                rh_b001_empleado.pk_num_empleado = idEmpleado AND(
                  nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio >= '$fecAño' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio <= '$fecAño'
                ) AND(
                  nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '07' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '09'
                )
            )
              ) AS TRIMESTRE3,
              (
                (
                SELECT
                  IF(
                     ROUND(SUM(num_monto_trimestral),2),
                     ROUND(SUM(num_monto_trimestral),2),
                    0
                  )
                FROM
                  rh_b001_empleado
                INNER JOIN
                  nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado = idEmpleado AND(
                    nm_e002_prestaciones_sociales_calculo.fec_anio >= '$fecAño' AND nm_e002_prestaciones_sociales_calculo.fec_anio <= '$fecAño'
                  ) AND(
                    nm_e002_prestaciones_sociales_calculo.fec_mes >= '10' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '12'
                  )
              ) +(
              SELECT
                IF(
                   ROUND(SUM(num_monto_trimestral),2),
                   ROUND(SUM(num_monto_trimestral),2),
                  0
                )
              FROM
                rh_b001_empleado
              INNER JOIN
                nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
              WHERE
                rh_b001_empleado.pk_num_empleado = idEmpleado AND(
                  nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio >= '$fecAño' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio <= '$fecAño'
                ) AND(
                  nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '10' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '12'
                )
                )       
              ) AS TRIMESTRE4,
               (
                  (
                    SELECT
                      IF(
                         ROUND(SUM(num_monto_anual),2),
                         ROUND(SUM(num_monto_anual),2),
                        0
                      )
                    FROM
                      rh_b001_empleado
                    INNER JOIN
                      nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e002_prestaciones_sociales_calculo.fec_anio = '$fecAño'  AND(
                        (
                          nm_e002_prestaciones_sociales_calculo.fec_mes >= '01' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '03'
                        )
                      )
                  ) +(
                  SELECT
                    IF(
                       ROUND(SUM(num_monto_anual),2),
                       ROUND(SUM(num_monto_anual),2),
                      0
                    )
                  FROM
                    rh_b001_empleado
                  INNER JOIN
                    nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                  WHERE
                    rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$fecAño'  AND(
                        (
                          nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '01' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '03'
                        )
                      )
                    )
                  ) AS ANUAL1,
                  
                  (
                    (
                    SELECT
                      IF(
                         ROUND(SUM(num_monto_anual),2),
                         ROUND(SUM(num_monto_anual),2),
                        0
                      )
                    FROM
                      rh_b001_empleado
                    INNER JOIN
                      nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e002_prestaciones_sociales_calculo.fec_anio = '$fecAño'  AND(
                        (
                          nm_e002_prestaciones_sociales_calculo.fec_mes >= '04' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '06'
                        )
                      )
                  ) +(
                  SELECT
                    IF(
                       ROUND(SUM(num_monto_anual),2),
                       ROUND(SUM(num_monto_anual),2),
                      0
                    )
                  FROM
                    rh_b001_empleado
                  INNER JOIN
                    nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                  WHERE
                    rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$fecAño'  AND(
                        (
                          nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '04' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '06'
                        )
                      )
                    )
                  ) AS ANUAL2,(
                    (
                    SELECT
                      IF(
                         ROUND(SUM(num_monto_anual),2),
                         ROUND(SUM(num_monto_anual),2),
                        0
                      )
                    FROM
                      rh_b001_empleado
                    INNER JOIN
                      nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e002_prestaciones_sociales_calculo.fec_anio = '$fecAño'  AND(
                        (
                          nm_e002_prestaciones_sociales_calculo.fec_mes >= '07' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '09'
                        )
                      )
                  ) +(
                  SELECT
                    IF(
                       ROUND(SUM(num_monto_anual),2),
                       ROUND(SUM(num_monto_anual),2),
                      0
                    )
                  FROM
                    rh_b001_empleado
                  INNER JOIN
                    nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                  WHERE
                    rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$fecAño'  AND(
                        (
                          nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '07' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '09'
                        )
                      )
                    )
                  ) AS ANUAL3,(
                    (
                    SELECT
                      IF(
                         ROUND(SUM(num_monto_anual),2),
                         ROUND(SUM(num_monto_anual),2),
                        0
                      )
                    FROM
                      rh_b001_empleado
                    INNER JOIN
                      nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                    WHERE
                      rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e002_prestaciones_sociales_calculo.fec_anio = '$fecAño'  AND(
                        (
                          nm_e002_prestaciones_sociales_calculo.fec_mes >= '10' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '12'
                        )
                      )
                  ) +(
                  SELECT
                    IF(
                       ROUND(SUM(num_monto_anual),2),
                       ROUND(SUM(num_monto_anual),2),
                      0
                    )
                  FROM
                    rh_b001_empleado
                  INNER JOIN
                    nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                  WHERE
                    rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$fecAño'  AND(
                        (
                          nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '10' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '12'
                        )
                      )
                    )
                  ) AS ANUAL4,
              (
                (
                SELECT
                  IF(
                    SUM(num_dias_anual),
                    SUM(num_dias_anual),
                    0
                  )
                FROM
                  rh_b001_empleado
                INNER JOIN
                  nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e002_prestaciones_sociales_calculo.fec_anio = '$fecAño' AND(
                    (
                      nm_e002_prestaciones_sociales_calculo.fec_mes >= '01' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '03'
                    )
                  )
              ) +(
              SELECT
                IF(
                  SUM(num_dias_anual),
                  SUM(num_dias_anual),
                  0
                )
              FROM
                rh_b001_empleado
              INNER JOIN
                nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
              WHERE
                rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$fecAño' AND(
                  (
                    nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '01' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '03'
                  )
                )
            )
              ) AS dias_anual1,
              (
                (
                SELECT
                  IF(
                    SUM(num_dias_anual),
                    SUM(num_dias_anual),
                    0
                  )
                FROM
                  rh_b001_empleado
                INNER JOIN
                  nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e002_prestaciones_sociales_calculo.fec_anio = '$fecAño' AND(
                    (
                      nm_e002_prestaciones_sociales_calculo.fec_mes >= '04' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '06'
                    )
                  )
              ) +(
              SELECT
                IF(
                  SUM(num_dias_anual),
                  SUM(num_dias_anual),
                  0
                )
              FROM
                rh_b001_empleado
              INNER JOIN
                nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
              WHERE
                rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$fecAño' AND(
                  (
                    nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '04' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '06'
                  )
                )
            )
              ) AS dias_anual2,
              (
                (
                SELECT
                  IF(
                    SUM(num_dias_anual),
                    SUM(num_dias_anual),
                    0
                  )
                FROM
                  rh_b001_empleado
                INNER JOIN
                  nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e002_prestaciones_sociales_calculo.fec_anio = '$fecAño' AND(
                    (
                      nm_e002_prestaciones_sociales_calculo.fec_mes >= '07' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '09'
                    )
                  )
              ) +(
              SELECT
                IF(
                  SUM(num_dias_anual),
                  SUM(num_dias_anual),
                  0
                )
              FROM
                rh_b001_empleado
              INNER JOIN
                nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
              WHERE
                rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$fecAño' AND(
                  (
                    nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '07' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '09'
                  )
                )
            )
              ) AS dias_anual3,
              (
                (
                SELECT
                  IF(
                    SUM(num_dias_anual),
                    SUM(num_dias_anual),
                    0
                  )
                FROM
                  rh_b001_empleado
                INNER JOIN
                  nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e002_prestaciones_sociales_calculo.fec_anio = '$fecAño' AND(
                    (
                      nm_e002_prestaciones_sociales_calculo.fec_mes >= '07' AND nm_e002_prestaciones_sociales_calculo.fec_mes <= '09'
                    )
                  )
              ) +(
              SELECT
                IF(
                  SUM(num_dias_anual),
                  SUM(num_dias_anual),
                  0
                )
              FROM
                rh_b001_empleado
              INNER JOIN
                nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
              WHERE
                rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$fecAño' AND(
                  (
                    nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '10' AND nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '12'
                  )
                )
            )
              ) AS dias_anual4,
             
              (
              SELECT
                IF(
                  SUM(num_anticipo),
                  SUM(num_anticipo),
                  0
                )
              FROM
                rh_b001_empleado
              INNER JOIN
                nm_e003_prestaciones_sociales_anticipo ON rh_b001_empleado.pk_num_empleado = nm_e003_prestaciones_sociales_anticipo.fk_rhb001_num_empleado
              WHERE
                rh_b001_empleado.pk_num_empleado = idEmpleado AND nm_e003_prestaciones_sociales_anticipo.fec_anio = '$fecAño'
            ) AS ANTICIPO
            FROM
              a003_persona
            INNER JOIN
              rh_b001_empleado ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN
              rh_c012_empleado_banco ON rh_b001_empleado.pk_num_empleado = rh_c012_empleado_banco.fk_rhb001_num_empleado
            INNER JOIN
              nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
            INNER JOIN
              rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado = rh_c005_empleado_laboral.fk_rhb001_num_empleado
            WHERE            
              
              nm_e002_prestaciones_sociales_calculo.fec_anio = '$fecAño' AND
              rh_c012_empleado_banco.fk_a006_num_miscelaneo_detalle_banco = '4760'
            GROUP BY
              rh_b001_empleado.pk_num_empleado,
              nm_e002_prestaciones_sociales_calculo.fec_anio
            ORDER BY
              a003_persona.ind_cedula_documento

        ";
        $añosPrestacionesSociales = $this->_db->query($sql);
        $añosPrestacionesSociales->setFetchMode(PDO::FETCH_ASSOC);
        return $añosPrestacionesSociales->fetchAll();
    }

    public function metBuscarCargo($cargo)
    {
        $Firmantes = $this->_db->query("
            SELECT
              CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre,
              puesto.ind_descripcion_cargo
            FROM
              rh_b001_empleado AS empleado
              INNER JOIN a003_persona AS per ON per.pk_num_persona = empleado.fk_a003_num_persona
              INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            WHERE
                puesto.ind_descripcion_cargo = '$cargo'
              ");

        $Firmantes->setFetchMode(PDO::FETCH_ASSOC);
        return $Firmantes->fetch();
    }

    public function metMostrarEmpleado($idEmpleado){
        $persona = $this->_db->query("
            SELECT
              CONCAT_WS(' ',per.ind_nombre1,per.ind_nombre2,per.ind_apellido1,per.ind_apellido2) AS nombre,
              per.ind_documento_fiscal,
              puesto.ind_descripcion_cargo,
              dependencia.pk_num_dependencia
            FROM rh_b001_empleado AS emp
            INNER JOIN a003_persona AS per ON per.pk_num_persona = emp.fk_a003_num_persona
            INNER JOIN rh_c059_empleado_nivelacion AS nivelacion ON nivelacion.fk_rhb001_num_empleado = emp.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = nivelacion.fk_rhc063_num_puestos
            LEFT JOIN rh_c076_empleado_organizacion AS organizacion ON organizacion.fk_rhb001_num_empleado = emp.pk_num_empleado
            LEFT JOIN a004_dependencia AS dependencia ON dependencia.pk_num_dependencia = organizacion.fk_a004_num_dependencia
            WHERE
              emp.pk_num_empleado='$idEmpleado'
              ORDER BY nivelacion.pk_num_empleado_nivelacion DESC
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metMostrarEmpleadoJefe($idDependencia){
        $persona = $this->_db->query("
            SELECT
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              persona.ind_documento_fiscal,
              puesto.ind_descripcion_cargo,
              organizacion.fk_a004_num_dependencia
            FROM
            a004_dependencia AS dependencia 
            LEFT JOIN a003_persona AS persona ON persona.pk_num_persona = dependencia.fk_a003_num_persona_responsable
            LEFT JOIN rh_b001_empleado AS empleado ON empleado.fk_a003_num_persona = persona.pk_num_persona
            LEFT JOIN rh_c059_empleado_nivelacion AS nivelacion ON nivelacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
            
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = nivelacion.fk_rhc063_num_puestos
            LEFT JOIN rh_c076_empleado_organizacion AS organizacion ON organizacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
            WHERE dependencia.pk_num_dependencia='$idDependencia'
              ORDER BY nivelacion.pk_num_empleado_nivelacion DESC
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metMostrarEmpleadoConsolida($idDependencia){
        $persona = $this->_db->query("
            SELECT
              CONCAT_WS(' ',persona.ind_nombre1,persona.ind_nombre2,persona.ind_apellido1,persona.ind_apellido2) AS nombre,
              persona.ind_documento_fiscal,
              puesto.ind_descripcion_cargo,
              organizacion.fk_a004_num_dependencia
            FROM
            a004_dependencia AS dependencia 
            LEFT JOIN a003_persona AS persona ON persona.pk_num_persona = dependencia.fk_a003_num_persona_responsable
            LEFT JOIN rh_b001_empleado AS empleado ON empleado.fk_a003_num_persona = persona.pk_num_persona
            
            LEFT JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado
            LEFT JOIN rh_c063_puestos AS puesto ON puesto.pk_num_puestos = laboral.fk_rhc063_num_puestos_cargo
            LEFT JOIN rh_c076_empleado_organizacion AS organizacion ON organizacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
            WHERE dependencia.pk_num_dependencia='$idDependencia'
        ");
        $persona->setFetchMode(PDO::FETCH_ASSOC);
        return $persona->fetch();
    }

    public function metPrestamoCajaDeAhorro($idProcesoPeriodo,$abreviatura)
    {
        $prestamo = $this->_db->query("
            SELECT
			CONCAT_WS(' ',a003_persona.ind_nombre1,a003_persona.ind_nombre2,a003_persona.ind_apellido1,a003_persona.ind_apellido2) AS nombre,
			a003_persona.ind_cedula_documento,
              nm_d005_tipo_nomina_empleado_concepto.num_monto AS num_monto,
              nm_b002_concepto.ind_abrebiatura,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              nm_b002_concepto.ind_impresion,
              nm_c002_proceso_periodo.fk_rhb001_num_empleado_crea
            FROM
             nm_d005_tipo_nomina_empleado_concepto
			 INNER JOIN rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
             INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
            
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            WHERE
              fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              nm_b002_concepto.ind_abrebiatura = '$abreviatura' AND
              (
                a006_miscelaneo_detalle.cod_detalle='D' OR
                a006_miscelaneo_detalle.cod_detalle='R'
              )
            ORDER BY a003_persona.ind_cedula_documento ASC
        ");
        $prestamo->setFetchMode(PDO::FETCH_ASSOC);
        return $prestamo->fetchAll();
    }

    public function metNombreEmpleado($idEmpleado)
    {
        $nombreEmpleado = $this->_db->query("
            SELECT
                CONCAT_WS(' ',persona.ind_apellido1,persona.ind_apellido2,persona.ind_nombre1,persona.ind_nombre2) AS nombre,
                persona.ind_cedula_documento,
                laboral.fec_ingreso,
                laboral.fec_egreso
            FROM
              rh_b001_empleado AS empleado 
            INNER JOIN rh_c005_empleado_laboral AS laboral ON laboral.fk_rhb001_num_empleado = empleado.pk_num_empleado 
            INNER JOIN a003_persona AS persona ON empleado.fk_a003_num_persona = persona.pk_num_persona
            WHERE
              empleado.pk_num_empleado = '$idEmpleado'
        ");
        $nombreEmpleado->setFetchMode(PDO::FETCH_ASSOC);
        return $nombreEmpleado->fetch();

    }

    public function metPeriodos($idEmpleado, $mes, $anio)
    {
        $listarPeriodos = $this->_db->query("
            SELECT
              fec_anio,
              fec_mes,
              num_sueldo_normal
            FROM
              nm_e002_prestaciones_sociales_calculo AS nm_e002          
            WHERE 
             fk_rhb001_num_empleado = '$idEmpleado' AND 
             fec_anio = '$anio' AND 
             fec_mes = '$mes'
            GROUP BY fec_anio
        ");
        // ORDER BY periodo ASC
        $listarPeriodos->setFetchMode(PDO::FETCH_ASSOC);
        return $listarPeriodos->fetch();
    }

    public function metOrganismo(){

        $listarOrganismo = $this->_db->query("
                SELECT
                  CONCAT_WS(ind_apellido1,ind_apellido2,',',ind_nombre1,ind_nombre2) AS nombre,                                
                    ind_descripcion_empresa,
                  a001.ind_documento_fiscal,
                  ind_direccion,
                  ind_cedula_documento,
                  ind_telefono,
                  ind_descripcion_cargo,
                  ind_ciudad
                FROM
                  a001_organismo AS a001
                INNER JOIN
                  a002_organismo_detalle AS a002 ON a001.pk_num_organismo = a002.fk_a001_num_organismo
                INNER JOIN
                  a010_ciudad AS a010 ON a002.fk_a010_num_ciudad = a010.pk_num_ciudad
                INNER JOIN
                  a003_persona AS a003 ON a003.pk_num_persona = a002.fk_a003_num_persona_representante
                INNER JOIN
                  rh_b001_empleado AS b001 ON a003.pk_num_persona = b001.fk_a003_num_persona
                INNER JOIN
                  rh_c005_empleado_laboral AS c005 ON b001.pk_num_empleado = c005.fk_rhb001_num_empleado
                INNER JOIN
                  rh_c063_puestos AS c063 ON c063.pk_num_puestos = c005.fk_rhc063_num_puestos_cargo
                INNER JOIN
                  a029_organismo_telefono AS a029 ON a001.pk_num_organismo = a029.fk_a001_num_organismo
                WHERE
                  pk_num_organismo = '1'
                                  
        ");
        $listarOrganismo->setFetchMode(PDO::FETCH_ASSOC);
        return $listarOrganismo->fetch();
    }

}
