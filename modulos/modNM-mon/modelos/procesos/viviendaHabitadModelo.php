<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class viviendaHabitadModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metListarConceptosEmpleado($idEmpleado)
    {
        $conceptoAsignado = $this->_db->query("
            SELECT
              nm_d002_empleado_concepto.*,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_descripcion,
              GROUP_CONCAT(nm_b003_tipo_proceso.cod_proceso SEPARATOR ', ') AS nm_b003_tipo_proceso
            FROM
              a003_persona
            INNER JOIN rh_b001_empleado on a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN nm_d002_empleado_concepto ON rh_b001_empleado.pk_num_empleado = nm_d002_empleado_concepto.fk_rhb001_num_empleado
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d002_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN nm_d003_proceso_empleado_concepto ON nm_d002_empleado_concepto.pk_empleado_concepto = nm_d003_proceso_empleado_concepto.fk_nmd002_num_empleado_concepto
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d003_proceso_empleado_concepto.fk_nmb005_num_tipo_proceso
            WHERE
              pk_num_empleado='$idEmpleado'
            GROUP BY nm_d002_empleado_concepto.pk_empleado_concepto
        ");
        $conceptoAsignado->setFetchMode(PDO::FETCH_ASSOC);
        return $conceptoAsignado->fetchAll();
    }

    public function metBuscarConceptoAsignado($idEmpleado, $idConceptoAsignado)
    {
        $conceptoAsignado = $this->_db->query("
            SELECT
              nm_d002_empleado_concepto.*,
              a018_seguridad_usuario.ind_usuario
            FROM
              a003_persona
            INNER JOIN rh_b001_empleado on a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN nm_d002_empleado_concepto ON rh_b001_empleado.pk_num_empleado = nm_d002_empleado_concepto.fk_rhb001_num_empleado
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = nm_d002_empleado_concepto.fk_a018_num_seguridad_usuario
            WHERE
              pk_num_empleado='$idEmpleado' and pk_empleado_concepto='$idConceptoAsignado'
        ");
        $conceptoAsignado->setFetchMode(PDO::FETCH_ASSOC);
        return $conceptoAsignado->fetch();
    }

    public function metBuscarConceptoAsignadoProcesos($idEmpleado, $idConceptoAsignado)
    {
        $conceptoAsignado = $this->_db->query("
            SELECT
              nm_b003_tipo_proceso.cod_proceso,
              nm_b003_tipo_proceso.ind_nombre_proceso,
              nm_d003_proceso_empleado_concepto.fk_nmb005_num_tipo_proceso
            FROM
              nm_d002_empleado_concepto
            INNER JOIN nm_d003_proceso_empleado_concepto ON nm_d002_empleado_concepto.pk_empleado_concepto = nm_d003_proceso_empleado_concepto.fk_nmd002_num_empleado_concepto
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d003_proceso_empleado_concepto.fk_nmb005_num_tipo_proceso
            WHERE
              nm_d002_empleado_concepto.fk_rhb001_num_empleado='$idEmpleado' and pk_empleado_concepto='$idConceptoAsignado'
        ");
        $conceptoAsignado->setFetchMode(PDO::FETCH_ASSOC);
        return $conceptoAsignado->fetchAll();
    }

    public function metAsignarConceptoEmpleado($idEmpleado, $idConcepto, $monto, $cantidad, $fecDesde, $fecHasta, $status, $procesos)
    {
        $this->_db->beginTransaction();
        $asignarConcepto = $this->_db->prepare("
                      INSERT INTO
                        nm_d002_empleado_concepto
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), fec_periodo_desde=:fec_periodo_desde,
                        fec_periodo_hasta=:fec_periodo_hasta, fk_nmb002_num_concepto=:fk_nmb002_num_concepto, fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                        num_estatus=:num_estatus, num_cantidad=:num_cantidad, num_monto=:num_monto
            ");
        $asignarConcepto->execute(array(
            'fk_rhb001_num_empleado' => $idEmpleado,
            'fk_nmb002_num_concepto' => $idConcepto,
            'num_monto' => $monto,
            'num_cantidad' => $cantidad,
            'fec_periodo_desde' => $fecDesde,
            'fec_periodo_hasta' => $fecHasta,
            'num_estatus' => $status
        ));
        $idRegistro = $this->_db->lastInsertId();

        $conceptoAsignadoProceso = $this->_db->prepare("
                      INSERT INTO
                        nm_d003_proceso_empleado_concepto
                      SET
                        fk_nmb005_num_tipo_proceso=:fk_nmb005_num_tipo_proceso, fk_nmd002_num_empleado_concepto='$idRegistro'
            ");
        for ($i = 0; $i < count($procesos); $i++) {
            $conceptoAsignadoProceso->execute(array(
                'fk_nmb005_num_tipo_proceso' => $procesos[$i]
            ));
        }

        $error = $asignarConcepto->errorInfo();
        $error2 = $conceptoAsignadoProceso->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) && !empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            $errores = array_merge($error, $error2);
            return $errores;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarConceptoEmpleado($idEmpleado, $idConcepto, $monto, $cantidad, $fecDesde, $fecHasta, $status, $procesos, $idConceptoAsignado)
    {
        $this->_db->beginTransaction();
        $asignarConcepto = $this->_db->prepare("
                      UPDATE
                        nm_d002_empleado_concepto
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), fec_periodo_desde=:fec_periodo_desde,
                        fec_periodo_hasta=:fec_periodo_hasta, fk_nmb002_num_concepto=:fk_nmb002_num_concepto, num_estatus=:num_estatus,
                        num_cantidad=:num_cantidad, num_monto=:num_monto
                      WHERE
                        fk_rhb001_num_empleado='$idEmpleado' AND pk_empleado_concepto='$idConceptoAsignado'
            ");
        $asignarConcepto->execute(array(
            'fk_nmb002_num_concepto' => $idConcepto,
            'num_monto' => $monto,
            'num_cantidad' => $cantidad,
            'fec_periodo_desde' => $fecDesde,
            'fec_periodo_hasta' => $fecHasta,
            'num_estatus' => $status
        ));

        $this->_db->query("
                      DELETE FROM
                        nm_d003_proceso_empleado_concepto
                      WHERE
                        fk_nmd002_num_empleado_concepto='$idConceptoAsignado'
        ");

        $conceptoAsignadoProceso = $this->_db->prepare("
                      INSERT INTO
                        nm_d003_proceso_empleado_concepto
                      SET
                        fk_nmb005_num_tipo_proceso=:fk_nmb005_num_tipo_proceso, fk_nmd002_num_empleado_concepto='$idConceptoAsignado'
            ");
        for ($i = 0; $i < count($procesos); $i++) {
            $conceptoAsignadoProceso->execute(array(
                'fk_nmb005_num_tipo_proceso' => $procesos[$i]
            ));
        }

        $error = $asignarConcepto->errorInfo();
        $error2 = $conceptoAsignadoProceso->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) && !empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            $errores = array_merge($error, $error2);
            return $errores;
        } else {
            $this->_db->commit();
            return $idConceptoAsignado;
        }
    }

    public function metEliminarConceptoEmpleado($idConceptoAsignado)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "  delete from nm_d003_proceso_empleado_concepto where fk_nmd002_num_empleado_concepto ='$idConceptoAsignado'"
        );
        $this->_db->query(
            "  delete from nm_d002_empleado_concepto where pk_empleado_concepto ='$idConceptoAsignado'"
        );
        $this->_db->commit();
    }
}
