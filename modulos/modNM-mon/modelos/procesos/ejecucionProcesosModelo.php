<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ejecucionProcesosModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListaEmpleadosEjecucionProcesos($idProcesosPeriodo)
    {
        $listaEmpleados = $this->_db->query("
            SELECT
              rh_b001_empleado.pk_num_empleado,
              nm_c002_proceso_periodo.ind_estado
            FROM
              a003_persona
            INNER JOIN rh_b001_empleado ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado
            INNER JOIN nm_d005_tipo_nomina_empleado_concepto ON rh_b001_empleado.pk_num_empleado = nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado
            INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
            WHERE
              pk_num_proceso_periodo='$idProcesosPeriodo'
            GROUP BY pk_num_empleado
        ");
        $listaEmpleados->setFetchMode(PDO::FETCH_ASSOC);
        return $listaEmpleados->fetchAll();
    }
    public function metListaEmpleadosEjecucionProcesos2($idProcesosPeriodo)
    {
        $listaEmpleados = $this->_db->query("
            SELECT
              ind_estado,
              nm_b003_tipo_proceso.cod_proceso
            FROM
              nm_c002_proceso_periodo
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
            WHERE
              fk_nmc001_num_tipo_nomina_periodo = '$idProcesosPeriodo' AND
              ( nm_b003_tipo_proceso.cod_proceso = 'PRQ' OR
              nm_b003_tipo_proceso.cod_proceso = 'FIN' ) AND
              nm_c002_proceso_periodo.ind_estado = 'CR'
        ");
        $listaEmpleados->setFetchMode(PDO::FETCH_ASSOC);
        return $listaEmpleados->fetchAll();
    }

    public function metListaEmpleadosNomina($idNomina, $cesado)
    {
        if ($cesado) {
            $cesado = "LEFT JOIN rh_c077_empleado_cese_reingreso ON rh_b001_empleado.pk_num_empleado=rh_c077_empleado_cese_reingreso.fk_rhb001_num_empleado";
            $status = "";
        } else {
            $status = "AND rh_b001_empleado.num_estatus=1";
        }
        $listaEmpleados = $this->_db->query("
            SELECT
              a003_persona.ind_nombre1,
              a003_persona.ind_nombre2,
              a003_persona.ind_apellido1,
              a003_persona.ind_apellido2,
              pk_num_empleado,
              a003_persona.ind_cedula_documento,
              rh_c005_empleado_laboral.fec_ingreso,
              (SELECT
                  nm_e002_prestaciones_sociales_calculo.fec_mes
                FROM
                  nm_e002_prestaciones_sociales_calculo
                WHERE
                  nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado AND nm_e002_prestaciones_sociales_calculo.num_dias_anual !=0
                ORDER BY
                  nm_e002_prestaciones_sociales_calculo.fec_anio DESC,
                  nm_e002_prestaciones_sociales_calculo.fec_mes DESC
                  LIMIT 0,1) AS mes_pda
            FROM
              a003_persona
            INNER JOIN rh_b001_empleado ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado=rh_c005_empleado_laboral.fk_rhb001_num_empleado
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado
            $cesado
            WHERE
              rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina='$idNomina'
              $status
        ");
        $listaEmpleados->setFetchMode(PDO::FETCH_ASSOC);
        return $listaEmpleados->fetchAll();
    }

    public function metObtenerConceptosGeneradosEmpleado($idProcesoPeriodo, $idEmpleado)
    {
        $listaEmpleados = $this->_db->query("
            SELECT
              nm_d005_tipo_nomina_empleado_concepto.num_monto,
              nm_b002_concepto.ind_impresion,
              a006_miscelaneo_detalle.cod_detalle
            FROM
              nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo='$idProcesoPeriodo' AND
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado='$idEmpleado'

        ");
        $listaEmpleados->setFetchMode(PDO::FETCH_ASSOC);
        return $listaEmpleados->fetchAll();
    }

    public function metObtenerProceso($idProcesosPeriodo)
    {
        $listaEmpleados = $this->_db->query("
            SELECT
              nm_b003_tipo_proceso.cod_proceso,
              nm_c002_proceso_periodo.fec_hasta,
              nm_b003_tipo_proceso.ind_nombre_proceso,
              nm_c002_proceso_periodo.fec_desde,
              nm_b001_tipo_nomina.ind_titulo_boleta,
              nm_b001_tipo_nomina.pk_num_tipo_nomina,
              nm_c002_proceso_periodo.fk_rhb001_num_empleado_crea
            FROM
              nm_c002_proceso_periodo
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
            INNER JOIN nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina
            WHERE
              pk_num_proceso_periodo='$idProcesosPeriodo'
        ");
        $listaEmpleados->setFetchMode(PDO::FETCH_ASSOC);
        return $listaEmpleados->fetch();
    }

    public function metCambiarEstadoGenerado($idControlProceso)
    {
        $this->_db->beginTransaction();
        $nominaPeriodo = $this->_db->prepare("
              UPDATE
                nm_c002_proceso_periodo
              SET
                fk_a018_num_seguridadusuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), num_flag_cerrado=0,
                fk_rhb001_num_empleado_procesa='$this->atIdEmpleado', fec_procesado=NOW(), ind_estado=:ind_estado
              WHERE
                pk_num_proceso_periodo='$idControlProceso'
        ");
        $nominaPeriodo->execute(array(
            'ind_estado' => 'GE'
        ));
        $error = $nominaPeriodo->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idControlProceso;
        }
    }

    public function metObtenerEmpleado($idEmpleado)
    {
        $empleado = $this->_db->query("
            SELECT
              pk_num_empleado,
              rh_c005_empleado_laboral.fec_ingreso,
              rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo,
              rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargotmp,
              rh_c063_puestos.txt_descripcion_gen,
              rh_c063_puestos.ind_descripcion_cargo,
              a003_persona.ind_apellido1,
              a003_persona.ind_apellido2,
              a003_persona.ind_nombre1,
              a003_persona.ind_nombre2,
              a003_persona.ind_cedula_documento,
              a003_persona.ind_email,
              rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina,
              (SELECT
                  nm_e002_prestaciones_sociales_calculo.fec_mes
                FROM
                  nm_e002_prestaciones_sociales_calculo
                WHERE
                  nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado AND nm_e002_prestaciones_sociales_calculo.num_dias_anual !=0
                ORDER BY
                  nm_e002_prestaciones_sociales_calculo.fec_anio DESC,
                  nm_e002_prestaciones_sociales_calculo.fec_mes DESC
                  LIMIT 0,1) AS mes_pda
            FROM
              a003_persona
            INNER JOIN rh_b001_empleado ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            INNER JOIN rh_c005_empleado_laboral ON rh_b001_empleado.pk_num_empleado=rh_c005_empleado_laboral.fk_rhb001_num_empleado
            INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado=rh_c076_empleado_organizacion.fk_rhb001_num_empleado
            INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos=rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
            WHERE
              pk_num_empleado='$idEmpleado'

            ORDER BY pk_num_empleado_laboral DESC
              LIMIT 1
        ");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metObtenerEmpleadoCesado($idEmpleado, $fecIngreso)
    {
        $empleado = $this->_db->query("
            SELECT
              fec_fecha
            FROM
              rh_c077_empleado_cese_reingreso
            WHERE
              fk_rhb001_num_empleado='$idEmpleado' AND
              fec_fecha >= '$fecIngreso'
            ORDER BY pk_num_empleado_cese_reingreso DESC
              LIMIT 1
        ");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metObtenerDatosNomina($idNomina, $idProcesoPeriodo)
    {
        $empleado = $this->_db->query("
            SELECT
              nm_b003_tipo_proceso.cod_proceso,
              nm_b003_tipo_proceso.pk_num_tipo_proceso,
              nm_b001_tipo_nomina.cod_tipo_nomina,
              nm_b001_tipo_nomina.num_flag_pago_mensual,
              nm_b001_tipo_nomina.pk_num_tipo_nomina,
              nm_c002_proceso_periodo.fec_desde,
              nm_c002_proceso_periodo.fec_hasta,
              CONCAT(nm_c001_tipo_nomina_periodo.fec_anio,'-',nm_c001_tipo_nomina_periodo.fec_mes) AS PERIODO,
              nm_c002_proceso_periodo.pk_num_proceso_periodo,
              nm_c002_proceso_periodo.ind_estado
            FROM
              nm_b001_tipo_nomina
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_b001_tipo_nomina.pk_num_tipo_nomina = nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina
            INNER JOIN nm_c002_proceso_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
            WHERE
              pk_num_tipo_nomina='$idNomina' AND
              pk_num_proceso_periodo='$idProcesoPeriodo'
        ");
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metObtenerConceptos($idNomina, $idTipoProceso, $idEmpleado, $periodo)
    {
        $conceptos = $this->_db->query("

            SELECT
              num_monto,
              num_cantidad,
              ind_descripcion,
              cod_concepto,
              ind_formula,
              pk_num_concepto,
              num_orden_planilla
            FROM
              (
                SELECT
                  nm_d002_empleado_concepto.num_monto,
                  nm_d002_empleado_concepto.num_cantidad,
                  nm_b002_concepto.ind_descripcion,
                  nm_b002_concepto.cod_concepto,
                  nm_b002_concepto.ind_formula,
                  nm_b002_concepto.pk_num_concepto,
                  nm_b002_concepto.num_orden_planilla

                FROM
                  nm_b002_concepto
                INNER JOIN nm_d002_empleado_concepto ON nm_b002_concepto.pk_num_concepto = nm_d002_empleado_concepto.fk_nmb002_num_concepto
                INNER JOIN nm_d003_proceso_empleado_concepto ON nm_d002_empleado_concepto.pk_empleado_concepto = nm_d003_proceso_empleado_concepto.fk_nmd002_num_empleado_concepto
                WHERE
                  nm_d003_proceso_empleado_concepto.fk_nmb005_num_tipo_proceso='$idTipoProceso' AND
                  nm_d002_empleado_concepto.fk_rhb001_num_empleado='$idEmpleado' AND
                  nm_b002_concepto.num_estatus='1' AND
                  nm_d002_empleado_concepto.num_estatus='1' AND
                  ((nm_d002_empleado_concepto.fec_periodo_hasta >= '$periodo' AND nm_d002_empleado_concepto.fec_periodo_desde <= '$periodo') OR (nm_d002_empleado_concepto.fec_periodo_desde <= '$periodo'))

                UNION
                SELECT
                  CONCAT('0') AS num_monto,
                  CONCAT('0') AS num_cantidad,
                  nm_b002_concepto.ind_descripcion,
                  nm_b002_concepto.cod_concepto,
                  nm_b002_concepto.ind_formula,
                  nm_b002_concepto.pk_num_concepto,
                  nm_b002_concepto.num_orden_planilla
                FROM
                  nm_b002_concepto
                INNER JOIN nm_d001_concepto_perfil_detalle ON nm_b002_concepto.pk_num_concepto = nm_d001_concepto_perfil_detalle.fk_nmb002_num_concepto

                WHERE
                  nm_d001_concepto_perfil_detalle.fk_nmb003_num_tipo_proceso='$idTipoProceso' AND
                  nm_d001_concepto_perfil_detalle.fk_nmb001_num_tipo_nomina='$idNomina' AND
                  nm_b002_concepto.num_estatus='1' AND
                  nm_b002_concepto.num_flag_automatico='1'
              ) concepto
            GROUP BY cod_concepto
            ORDER BY num_orden_planilla ASC
        ");

        $conceptos->setFetchMode(PDO::FETCH_ASSOC);
        return $conceptos->fetchAll();
    }

    public function metLimpiarNominaEmpleado($idProcesoPeriodo)
    {
        $this->_db->beginTransaction();
        $limpiarNominaEmpleado = $this->_db->prepare("
                  DELETE FROM
                    nm_d005_tipo_nomina_empleado_concepto
                  WHERE
                    fk_nmc002_num_proceso_periodo=:fk_nmc002_num_proceso_periodo
        ");
        $limpiarNominaEmpleado->execute(array(
            'fk_nmc002_num_proceso_periodo' => $idProcesoPeriodo
        ));
        $error = $limpiarNominaEmpleado->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idProcesoPeriodo;
        }
    }

    public function metBuscarLimpiarPrestaciones($idProcesoPeriodo, $pkEmpleado)
    {
        $busqueda = $this->_db->query("
            SELECT
              nm_d005_tipo_nomina_empleado_concepto.pk_num_tiponomina_empleadoconcepto
            FROM
              nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN
              nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
            INNER JOIN
              nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
            WHERE
              fk_nmc002_num_proceso_periodo ='$idProcesoPeriodo' AND 
              fk_rhb001_num_empleado = '$pkEmpleado' AND
              nm_b003_tipo_proceso.cod_proceso = 'GPS' 
        ");
        $busqueda->setFetchMode(PDO::FETCH_ASSOC);
        return $busqueda->fetchAll();
    }

    public function metLimpiarPrestaciones($pk)
    {
        $this->_db->beginTransaction();
        $limpiarNominaEmpleado = $this->_db->prepare("
                  DELETE FROM
                    nm_d005_tipo_nomina_empleado_concepto
                  WHERE
                    pk_num_tiponomina_empleadoconcepto=:pk_num_tiponomina_empleadoconcepto
        ");
        $limpiarNominaEmpleado->execute(array(
            'pk_num_tiponomina_empleadoconcepto' => $pk
        ));
        $error = $limpiarNominaEmpleado->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $pk;
        }
    }

    public function metRegistrarCalculos($idEmpleado, $idProcesoPeriodo, $idConcepto, $monto, $cantidad)
    {
        $this->_db->beginTransaction();
        $registrarCalculos = $this->_db->prepare("
                  INSERT INTO
                    nm_d005_tipo_nomina_empleado_concepto
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_monto=:num_monto, num_cantidad=:num_cantidad, fk_rhb001_num_empleado=:fk_rhb001_num_empleado,
                    fk_nmb002_num_concepto=:fk_nmb002_num_concepto, fk_nmc002_num_proceso_periodo=:fk_nmc002_num_proceso_periodo
        ");
        $registrarCalculos->execute(array(
            'fk_rhb001_num_empleado' => $idEmpleado,
            'fk_nmc002_num_proceso_periodo' => $idProcesoPeriodo,
            'fk_nmb002_num_concepto' => $idConcepto,
            'num_monto' => $monto,
            'num_cantidad' => $cantidad
        ));
        $idRegistro = $this->_db->lastInsertId();
        $error = $registrarCalculos->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metBuscarPrestacionesSociales($idEmpleado, $periodo, $codTipoProceso)
    {
        $periodo = str_getcsv($periodo, '-');
        if ($codTipoProceso == 'GPA' || $codTipoProceso == 'GPS') {
            $sql = "
                SELECT
                  nm_e002_prestaciones_sociales_calculo.num_dias_trimestre,
                  nm_e002_prestaciones_sociales_calculo.num_monto_trimestral,
                  nm_e002_prestaciones_sociales_calculo.num_remuneracion_diaria,
                  nm_e002_prestaciones_sociales_calculo.pk_num_prestaciones_sociales_calculo
                FROM
                  rh_b001_empleado
                INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado='$idEmpleado' AND
                  nm_e002_prestaciones_sociales_calculo.fec_anio='$periodo[0]' AND
                  nm_e002_prestaciones_sociales_calculo.fec_mes='$periodo[1]'
            ";
        } else {
            $sql = "
                SELECT
                  nm_e001_prestaciones_sociales_calculo_retroactivo.num_dias_trimestre,
                  nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_trimestral,
                  nm_e001_prestaciones_sociales_calculo_retroactivo.num_remuneracion_diaria,
                  nm_e001_prestaciones_sociales_calculo_retroactivo.pk_num_prestaciones_sociales_calculo_retroactivo AS pk_num_prestaciones_sociales_calculo
                FROM
                  rh_b001_empleado
                INNER JOIN nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado='$idEmpleado' AND
                  nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio='$periodo[0]' AND
                  nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes='$periodo[1]'
            ";
        }

        $empleado = $this->_db->query($sql);
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();

    }

    public function metHistoricoPrestacionesSociales($idEmpleado, $periodo,$codTipoProceso)
    {
        $periodoAnterior = date("$periodo-01");
        $periodoAnterior = strtotime ( '-1 year ' , strtotime ( $periodoAnterior ) );
        $periodoAnterior = strtotime ( '+1 month' , $periodoAnterior ) ;
        $periodoAnterior = date ( 'Y-m' , $periodoAnterior );
        $periodoAnterior = str_getcsv($periodoAnterior,'-');
        $periodo = str_getcsv($periodo, '-');

        if ($codTipoProceso == 'FIN' || $codTipoProceso == 'GPA') {
            $sql = "
                SELECT
                  (SUM(nm_e002_prestaciones_sociales_calculo.num_remuneracion_diaria)/12) AS num_remuneracion_diaria
                FROM
                  rh_b001_empleado
                INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado='$idEmpleado' AND
                  (
                    nm_e002_prestaciones_sociales_calculo.fec_anio = '$periodoAnterior[0]' AND 
                    nm_e002_prestaciones_sociales_calculo.fec_mes >= '$periodoAnterior[1]' AND
                    nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado = '$idEmpleado'
                  ) OR
                  (
                    nm_e002_prestaciones_sociales_calculo.fec_anio = '$periodo[0]' AND 
                    nm_e002_prestaciones_sociales_calculo.fec_mes <= '$periodo[1]' AND
                    nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado = '$idEmpleado'
                  )
            ";
        } else {
            $sql = "SELECT
                          (
                            SUM(
                              nm_e001_prestaciones_sociales_calculo_retroactivo.num_remuneracion_diaria
                            ) / 12
                          ) AS num_remuneracion_diaria
                        FROM
                          rh_b001_empleado
                        INNER JOIN
                          nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
                        WHERE
                          rh_b001_empleado.pk_num_empleado = '$idEmpleado' AND(
                            nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$periodoAnterior[0]' AND 
                            nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes >= '$periodoAnterior[1]' AND 
                            nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado = '$idEmpleado'
                          ) OR(
                            nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio = '$periodo[0]' AND 
                            nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes <= '$periodo[1]' AND 
                            nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado = '$idEmpleado'
                          )
            
             ";
        }
        $empleado = $this->_db->query($sql);
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metAcumuladoPrestacionesIntereses($idEmpleado, $periodo)
    {
        $sql = "
            SELECT
              SUM(nm_e002_prestaciones_sociales_calculo.num_monto_trimestral) AS num_monto_trimestral,
              SUM(nm_e002_prestaciones_sociales_calculo.num_monto_anual) AS num_monto_anual,
              CONCAT(nm_e002_prestaciones_sociales_calculo.fec_anio,'-',nm_e002_prestaciones_sociales_calculo.fec_mes) AS periodo
            FROM
              rh_b001_empleado
            INNER JOIN nm_e002_prestaciones_sociales_calculo ON rh_b001_empleado.pk_num_empleado = nm_e002_prestaciones_sociales_calculo.fk_rhb001_num_empleado
            WHERE
              rh_b001_empleado.pk_num_empleado='$idEmpleado' AND
              CONCAT(nm_e002_prestaciones_sociales_calculo.fec_anio,'-',nm_e002_prestaciones_sociales_calculo.fec_mes) <= '$periodo' AND 
              nm_e002_prestaciones_sociales_calculo.num_estatus = 0
            ORDER BY periodo ASC
        ";
        $empleado = $this->_db->query($sql);
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metAcumuladoPrestacionesRetroIntereses($idEmpleado, $periodo)
    {
        $sql = "
            SELECT
              SUM(nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_trimestral) AS num_monto_trimestral,
              SUM(nm_e001_prestaciones_sociales_calculo_retroactivo.num_monto_anual) AS num_monto_anual,
              CONCAT(nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio,'-',nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes) AS periodo
            FROM
              rh_b001_empleado
            INNER JOIN nm_e001_prestaciones_sociales_calculo_retroactivo ON rh_b001_empleado.pk_num_empleado = nm_e001_prestaciones_sociales_calculo_retroactivo.fk_rhb001_num_empleado
            WHERE
              rh_b001_empleado.pk_num_empleado='$idEmpleado' AND
              CONCAT(nm_e001_prestaciones_sociales_calculo_retroactivo.fec_anio,'-',nm_e001_prestaciones_sociales_calculo_retroactivo.fec_mes) <= '$periodo' AND 
              nm_e001_prestaciones_sociales_calculo_retroactivo.num_estatus = 0
            ORDER BY periodo ASC
        ";
        $empleado = $this->_db->query($sql);
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metUltimosIntereses()
    {
        $sql = "
            SELECT 
              * 
            FROM 
              nm_b007_tasa_interes 
            ORDER BY nm_b007_tasa_interes.periodo DESC
            LIMIT 1
        ";
        $interes = $this->_db->query($sql);
        $interes->setFetchMode(PDO::FETCH_ASSOC);
        return $interes->fetch();
    }

    public function metRegistrarPrestacionesSociales(
        $idEmpleado, $idNomina, $periodo, $sueldoBasico, $asignaciones, $sueldoNormal, $alicVacacional,
        $alicFinAño, $bonoEspecial, $bonoFiscal, $remuneracionMensual, $remuneracionDiaria, $diasTrimestre,
        $codProceso,$montoAnual,$diasAnual
    )
    {
        $periodo = str_getcsv($periodo, '-');
        $this->_db->beginTransaction();
        if ($codProceso == 'FIN') {
            $sql = "
                  INSERT INTO
                    nm_e002_prestaciones_sociales_calculo
                  SET
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado, 
                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
                    fec_ultima_modificacion=NOW(), 
                    fec_anio=:fec_anio,
                    fec_mes=:fec_mes,
                    num_estatus=1,
                    num_sueldo_base=:num_sueldo_base, 
                    num_total_asignaciones=:num_total_asignaciones,
                    num_sueldo_normal=:num_sueldo_normal, 
                    num_alicuota_vacacional=:num_alicuota_vacacional,
                    num_alicuota_fin_anio=:num_alicuota_fin_anio, 
                    num_bono_especial=:num_bono_especial, 
                    num_bono_fiscal=:num_bono_fiscal,
                    num_remuneracion_mensual=:num_remuneracion_mensual, 
                    num_remuneracion_diaria=:num_remuneracion_diaria,
                    num_dias_trimestre=:num_dias_trimestre, 
                    num_monto_anual=:num_monto_anual, 
                    num_dias_anual=:num_dias_anual,
                    num_monto_acumulado=0
          ";
        } else {
            $sql = "
                  INSERT INTO
                    nm_e001_prestaciones_sociales_calculo_retroactivo
                  SET
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado, 
                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                    fk_a018_num_seguridadusuario='$this->atIdUsuario', 
                    fec_ultima_modificacion=NOW(), 
                    fec_anio=:fec_anio,
                    fec_mes=:fec_mes,
                    num_estatus=1,
                    num_sueldo_base=:num_sueldo_base, 
                    num_total_asignaciones=:num_total_asignaciones,
                    num_sueldo_normal=:num_sueldo_normal, 
                    num_alicuota_vacacional=:num_alicuota_vacacional,
                    num_alicuota_fin_anio=:num_alicuota_fin_anio, 
                    num_bono_especial=:num_bono_especial, 
                    num_bono_fiscal=:num_bono_fiscal,
                    num_remuneracion_mensual=:num_remuneracion_mensual, 
                    num_remuneracion_diaria=:num_remuneracion_diaria,
                    num_dias_trimestre=:num_dias_trimestre, 
                    num_monto_anual=:num_monto_anual, 
                    num_dias_anual=:num_dias_anual, 
                    num_monto_acumulado=0
          ";
        }
        $registrarCalculos = $this->_db->prepare($sql);
        $registrarCalculos->execute(array(
            'fk_rhb001_num_empleado' => $idEmpleado,
            'fk_nmb001_num_tipo_nomina' => $idNomina,
            'fec_anio' => $periodo[0],
            'fec_mes' => $periodo[1],
            'num_sueldo_base' => $sueldoBasico,
            'num_total_asignaciones' => $asignaciones,
            'num_sueldo_normal' => $sueldoNormal,
            'num_alicuota_vacacional' => $alicVacacional,
            'num_alicuota_fin_anio' => $alicFinAño,
            'num_bono_especial' => $bonoEspecial,
            'num_bono_fiscal' => $bonoFiscal,
            'num_remuneracion_mensual' => $remuneracionMensual,
            'num_remuneracion_diaria' => $remuneracionDiaria,
            'num_dias_trimestre' => $diasTrimestre,
            'num_monto_anual' => $montoAnual,
            'num_dias_anual' => $diasAnual,
        ));
        $idRegistro = $this->_db->lastInsertId();
        $error = $registrarCalculos->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metActualizarPrestacionesSociales(
        $idEmpleado, $idNomina, $periodo, $sueldoBasico, $asignaciones, $sueldoNormal, $alicVacacional,
        $alicFinAño, $bonoEspecial, $bonoFiscal, $remuneracionMensual, $remuneracionDiaria, $diasTrimestre,
        $codProceso,$idPrestaciones
    )
    {
        $periodo = str_getcsv($periodo, '-');
        $this->_db->beginTransaction();
        if ($codProceso == 'FIN') {
            $sql = "
                  UPDATE
                    nm_e002_prestaciones_sociales_calculo
                  SET
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado, fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), fec_anio=:fec_anio,
                    fec_mes=:fec_mes,
                    num_estatus=0,num_sueldo_base=:num_sueldo_base, num_total_asignaciones=:num_total_asignaciones,
                    num_sueldo_normal=:num_sueldo_normal, num_alicuota_vacacional=:num_alicuota_vacacional,
                    num_alicuota_fin_anio=:num_alicuota_fin_anio, num_bono_especial=:num_bono_especial, num_bono_fiscal=:num_bono_fiscal,
                    num_remuneracion_mensual=:num_remuneracion_mensual, num_remuneracion_diaria=:num_remuneracion_diaria,
                    num_dias_trimestre=:num_dias_trimestre
                  WHERE
                    pk_num_prestaciones_sociales_calculo='$idPrestaciones'
          ";
        } else {
            $sql = "
                  UPDATE
                    nm_e001_prestaciones_sociales_calculo_retroactivo
                  SET
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado, fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina,
                    fk_a018_num_seguridadusuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), fec_anio=:fec_anio,
                    fec_mes=:fec_mes,
                    num_estatus=0,num_sueldo_base=:num_sueldo_base, num_total_asignaciones=:num_total_asignaciones,
                    num_sueldo_normal=:num_sueldo_normal, num_alicuota_vacacional=:num_alicuota_vacacional,
                    num_alicuota_fin_anio=:num_alicuota_fin_anio, num_bono_especial=:num_bono_especial, num_bono_fiscal=:num_bono_fiscal,
                    num_remuneracion_mensual=:num_remuneracion_mensual, num_remuneracion_diaria=:num_remuneracion_diaria,
                    num_dias_trimestre=:num_dias_trimestre
                  WHERE
                    pk_num_prestaciones_sociales_calculo_retroactivo='$idPrestaciones'
          ";
        }
        $registrarCalculos = $this->_db->prepare($sql);
        $registrarCalculos->execute(array(
            'fk_rhb001_num_empleado' => $idEmpleado,
            'fk_nmb001_num_tipo_nomina' => $idNomina,
            'fec_anio' => $periodo[0],
            'fec_mes' => $periodo[1],
            'num_sueldo_base' => $sueldoBasico,
            'num_total_asignaciones' => $asignaciones,
            'num_sueldo_normal' => $sueldoNormal,
            'num_alicuota_vacacional' => $alicVacacional,
            'num_alicuota_fin_anio' => $alicFinAño,
            'num_bono_especial' => $bonoEspecial,
            'num_bono_fiscal' => $bonoFiscal,
            'num_remuneracion_mensual' => $remuneracionMensual,
            'num_remuneracion_diaria' => $remuneracionDiaria,
            'num_dias_trimestre' => $diasTrimestre,
        ));
        $error = $registrarCalculos->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idPrestaciones;
        }
    }

    public function metBuscarPrestacionesSocialesIntereses($idEmpleado, $periodo)
    {
        $periodo = str_getcsv($periodo, '-');
            $sql = "
                SELECT
                  nm_e004_prestaciones_sociales_intereses.pk_num_prestaciones_sociales_intereses
                FROM
                  rh_b001_empleado
                INNER JOIN nm_e004_prestaciones_sociales_intereses ON rh_b001_empleado.pk_num_empleado = nm_e004_prestaciones_sociales_intereses.fk_rhb001_num_empleado
                WHERE
                  rh_b001_empleado.pk_num_empleado='$idEmpleado' AND
                  nm_e004_prestaciones_sociales_intereses.fec_anio='$periodo[0]' AND
                  nm_e004_prestaciones_sociales_intereses.fec_mes='$periodo[1]'
            ";
        $empleado = $this->_db->query($sql);
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metRegistrarIntereses(
        $idEmpleado, $periodo, $acumulado, $intereses, $porcentaje
    )
    {
        $periodo = str_getcsv($periodo, '-');
        $this->_db->beginTransaction();

        $sql = "
                  INSERT INTO
                    nm_e004_prestaciones_sociales_intereses
                  SET
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado, fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
                    fec_anio=:fec_anio, fec_mes=:fec_mes, num_monto_acumulado=:num_monto_acumulado, num_monto_interes=:num_monto_interes,
                    ind_porcentaje_aplicado=:ind_porcentaje_aplicado, num_estatus=:num_estatus
          ";

        $registrarCalculos = $this->_db->prepare($sql);
        $registrarCalculos->execute(array(
            'fk_rhb001_num_empleado' => $idEmpleado,
            'fec_anio' => $periodo[0],
            'fec_mes' => $periodo[1],
            'num_monto_acumulado' => $acumulado,
            'num_monto_interes' => $intereses,
            'ind_porcentaje_aplicado' => $porcentaje,
            'num_estatus' => 1
        ));
        $idRegistro = $this->_db->lastInsertId();
        $error = $registrarCalculos->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metActualizarIntereses(
        $idEmpleado, $periodo, $acumulado, $intereses, $porcentaje, $idIntereses
    )
    {
        $periodo = str_getcsv($periodo, '-');
        $this->_db->beginTransaction();

        $sql = "
                  UPDATE
                    nm_e004_prestaciones_sociales_intereses
                  SET
                    fk_rhb001_num_empleado=:fk_rhb001_num_empleado, fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
                    fec_anio=:fec_anio, fec_mes=:fec_mes, num_monto_acumulado=:num_monto_acumulado, num_monto_interes=:num_monto_interes,
                    ind_porcentaje_aplicado=:ind_porcentaje_aplicado, num_estatus=:num_estatus
                  WHERE 
                    pk_num_prestaciones_sociales_intereses = '$idIntereses'
          ";

        $registrarCalculos = $this->_db->prepare($sql);
        $registrarCalculos->execute(array(
            'fk_rhb001_num_empleado' => $idEmpleado,
            'fec_anio' => $periodo[0],
            'fec_mes' => $periodo[1],
            'num_monto_acumulado' => $acumulado,
            'num_monto_interes' => $intereses,
            'ind_porcentaje_aplicado' => $porcentaje,
            'num_estatus' => 1
        ));
        $idRegistro = $this->_db->lastInsertId();
        $error = $registrarCalculos->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metActualizarPrestacionesSocialesAdicionales($idEmpleado, $idNomina,$idPrestaciones, $periodo, $montoAnual, $dias, $codProceso)
    {
        $periodo = str_getcsv($periodo, '-');
        $this->_db->beginTransaction();
        if ($codProceso == 'GPA') {
            $sql = "
                  UPDATE
                    nm_e002_prestaciones_sociales_calculo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
                    fec_ultima_modificacion=NOW(),
                    num_monto_anual=:num_monto_anual, 
                    num_dias_anual=:num_dias_anual
                  WHERE
                    pk_num_prestaciones_sociales_calculo='$idPrestaciones' AND
                    fk_nmb001_num_tipo_nomina='$idNomina' AND
                    fk_rhb001_num_empleado='$idEmpleado' AND
                    fec_anio='$periodo[0]' AND
                    fec_mes='$periodo[1]'
          ";
        } else {
            $sql = "
                  UPDATE
                    nm_e001_prestaciones_sociales_calculo_retroactivo
                  SET
                    fk_a018_num_seguridadusuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_monto_anual=:num_monto_anual, num_dias_anual=:num_dias_anual
                  WHERE
                    pk_num_prestaciones_sociales_calculo_retroactivo='$idPrestaciones' AND
                    fk_nmb001_num_tipo_nomina='$idNomina' AND
                    fk_rhb001_num_empleado='$idEmpleado' AND
                    fec_anio='$periodo[0]' AND
                    fec_mes='$periodo[1]'
          ";
        }
        $registrarCalculos = $this->_db->prepare($sql);
        $registrarCalculos->execute(array(
            'num_monto_anual' => $montoAnual,
            'num_dias_anual' => $dias,
        ));
        $error = $registrarCalculos->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idPrestaciones;
        }
    }

    public function metActualizarPrestacionesSocialesTrimestre($idEmpleado, $idNomina,$idPrestaciones, $periodo, $montoTrimestral, $codProceso)
    {
        $periodo = str_getcsv($periodo, '-');
        $this->_db->beginTransaction();
        if ($codProceso == 'GPS') {
            $sql = "
                  UPDATE
                    nm_e002_prestaciones_sociales_calculo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_monto_trimestral=:num_monto_trimestral
                  WHERE
                    pk_num_prestaciones_sociales_calculo='$idPrestaciones' AND
                    fk_nmb001_num_tipo_nomina='$idNomina' AND
                    fk_rhb001_num_empleado='$idEmpleado' AND
                    fec_anio='$periodo[0]' AND
                    fec_mes='$periodo[1]'
          ";
        } else {
            $sql = "
                  UPDATE
                    nm_e001_prestaciones_sociales_calculo_retroactivo
                  SET
                    fk_a018_num_seguridadusuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_monto_trimestral=:num_monto_trimestral
                  WHERE
                    pk_num_prestaciones_sociales_calculo_retroactivo='$idPrestaciones' AND
                    fk_nmb001_num_tipo_nomina='$idNomina' AND
                    fk_rhb001_num_empleado='$idEmpleado' AND
                    fec_anio='$periodo[0]' AND
                    fec_mes='$periodo[1]'
          ";
        }
        $registrarCalculos = $this->_db->prepare($sql);
        $registrarCalculos->execute(array(
            'num_monto_trimestral' => $montoTrimestral
        ));
        $error = $registrarCalculos->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idPrestaciones;
        }
    }

    public function metListarPeriodos($filtro)
    {
        $sql = "
            SELECT
              nomina.fk_nmc002_num_proceso_periodo,
              nomina_periodo.*,
              periodo.ind_estado,
              tipo.cod_proceso
            FROM
              nm_d005_tipo_nomina_empleado_concepto AS nomina
            INNER JOIN
              nm_c002_proceso_periodo AS periodo ON nomina.fk_nmc002_num_proceso_periodo = periodo.pk_num_proceso_periodo
            INNER JOIN
              nm_c001_tipo_nomina_periodo AS nomina_periodo ON periodo.fk_nmc001_num_tipo_nomina_periodo = nomina_periodo.pk_num_tipo_nomina_periodo AND 
              nomina_periodo.fk_nmb001_num_tipo_nomina NOT IN ($filtro)
            INNER JOIN
              nm_b003_tipo_proceso AS tipo ON periodo.fk_nmb003_num_tipo_proceso = tipo.pk_num_tipo_proceso
            WHERE
              tipo.cod_proceso = 'FIN' OR tipo.cod_proceso = 'PRQ' AND 
              nomina_periodo.fk_nmb001_num_tipo_nomina NOT IN ($filtro)
            GROUP BY
              nomina_periodo.pk_num_tipo_nomina_periodo,tipo.cod_proceso
            ORDER BY
              nomina_periodo.fec_anio,
              nomina_periodo.fec_mes ASC
        ";

        $interes = $this->_db->query($sql);
        $interes->setFetchMode(PDO::FETCH_ASSOC);
        return $interes->fetchAll();
    }

    public function metObtenerConceptosGeneradosPeriodo($anio,$mes,$idEmpleado,$otros = false)
    {
        if(!$otros) {
            $otros = '';
        }
        $listaEmpleados = $this->_db->query("
            SELECT
              nm_d005_tipo_nomina_empleado_concepto.num_monto,
              nm_b002_concepto.ind_impresion,
              nm_b002_concepto.cod_concepto,
              nm_b002_concepto.ind_abrebiatura,
              a006_miscelaneo_detalle.cod_detalle,
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado,
              nm_c001_tipo_nomina_periodo.fec_anio,
              nm_c001_tipo_nomina_periodo.fec_mes,
              nm_b003_tipo_proceso.cod_proceso
            FROM
              nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
            INNER JOIN nm_c002_proceso_periodo ON nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo = nm_c002_proceso_periodo.pk_num_proceso_periodo
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo = nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo
            INNER JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
            WHERE
              nm_c001_tipo_nomina_periodo.fec_anio = '$anio' AND 
              nm_c001_tipo_nomina_periodo.fec_mes = '$mes' AND 
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado = '$idEmpleado' AND 
              a006_miscelaneo_detalle.cod_detalle = 'I' AND 
              (
              nm_b003_tipo_proceso.cod_proceso = 'PRQ'
              OR nm_b003_tipo_proceso.cod_proceso = 'FIN'
              OR nm_b003_tipo_proceso.cod_proceso = 'RTA' 
              $otros
              )
        ");
        $listaEmpleados->setFetchMode(PDO::FETCH_ASSOC);
        return $listaEmpleados->fetchAll();
    }

    public function metListarEmpleados($anio,$mes,$filtro)
    {
        $sql = "
            SELECT
              *
            FROM
              rh_b001_empleado AS empleado
            INNER JOIN
              rh_c005_empleado_laboral AS laboral ON empleado.pk_num_empleado = laboral.fk_rhb001_num_empleado
            INNER JOIN
              rh_c076_empleado_organizacion AS organizacion ON organizacion.fk_rhb001_num_empleado = empleado.pk_num_empleado
            INNER JOIN 
              a003_persona AS persona ON empleado.fk_a003_num_persona = persona.pk_num_persona
            INNER JOIN 
              nm_d005_tipo_nomina_empleado_concepto AS empleado_concepto ON empleado.pk_num_empleado = empleado_concepto.fk_rhb001_num_empleado
            INNER JOIN 
              nm_c002_proceso_periodo AS periodo ON empleado_concepto.fk_nmc002_num_proceso_periodo = periodo.pk_num_proceso_periodo
            INNER JOIN 
              nm_c001_tipo_nomina_periodo AS nomina_periodo ON periodo.fk_nmc001_num_tipo_nomina_periodo = nomina_periodo.pk_num_tipo_nomina_periodo
            WHERE empleado.pk_num_empleado !=1 AND
              nomina_periodo.fec_anio = '$anio' AND 
              nomina_periodo.fec_mes = '$mes' AND 
              organizacion.fk_nmb001_num_tipo_nomina NOT IN ($filtro)
            GROUP BY empleado.pk_num_empleado
        ";
        $interes = $this->_db->query($sql);
        $interes->setFetchMode(PDO::FETCH_ASSOC);
        return $interes->fetchAll();
    }

    public function metEliminarDatosPrestacionesEmpleadoPeriodo($anio,$mes,$empleado)
    {
        $this->_db->beginTransaction();
        $sql = "DELETE FROM nm_e002_prestaciones_sociales_calculo WHERE 
                  fec_anio = '$anio' AND 
                  fec_mes = '$mes' AND 
                  fk_rhb001_num_empleado = '$empleado' 
        ";
        $interes = $this->_db->query($sql);

        $error = $interes->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return 'ok';
        }
    }

    public function metEliminarDatosPrestacionesEmpleadoPeriodoRetroactivo($anio,$mes,$empleado)
    {
        $this->_db->beginTransaction();
        $sql = "DELETE FROM nm_e001_prestaciones_sociales_calculo_retroactivo WHERE 
                  fec_anio = '$anio' AND 
                  fec_mes = '$mes' AND 
                  fk_rhb001_num_empleado = '$empleado' 
        ";
        $interes = $this->_db->query($sql);

        $error = $interes->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return 'ok';
        }
    }
}
