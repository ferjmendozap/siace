<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class controlProcesosModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListarControlProcesos($status = false)
    {
        if ($status) {
            $status = "WHERE nm_c002_proceso_periodo.ind_estado='$status'";
        }
        $controlProcesos = $this->_db->query("
            SELECT
              nm_c002_proceso_periodo.*,
              nm_c001_tipo_nomina_periodo.fec_anio,
              nm_c001_tipo_nomina_periodo.fec_mes,
              nm_b001_tipo_nomina.ind_nombre_nomina,
              nm_b003_tipo_proceso.ind_nombre_proceso
            FROM
              nm_c002_proceso_periodo
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
            INNER JOIN nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
            $status
            ORDER BY nm_b001_tipo_nomina.ind_nombre_nomina DESC
        ");
        $controlProcesos->setFetchMode(PDO::FETCH_ASSOC);
        return $controlProcesos->fetchAll();
    }

    public function metBuscarControlProcesos($idControlProcesos)
    {
        $controlProcesos = $this->_db->query("
            SELECT
              *,
              nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina,
              a018_seguridad_usuario.ind_usuario,
              nm_b001_tipo_nomina.ind_nombre_nomina,
              nm_b003_tipo_proceso.ind_nombre_proceso,
              nm_c001_tipo_nomina_periodo.fec_anio,
              nm_c001_tipo_nomina_periodo.fec_mes,
              (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_c002_proceso_periodo ON rh_b001_empleado.pk_num_empleado = nm_c002_proceso_periodo.fk_rhb001_num_empleado_crea
                    WHERE
                        pk_num_proceso_periodo = '$idControlProcesos'
              ) AS fk_rhb001_num_empleado_crea,
              (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_c002_proceso_periodo ON rh_b001_empleado.pk_num_empleado = nm_c002_proceso_periodo.fk_rhb001_num_empleado_aprueba
                    WHERE
                        pk_num_proceso_periodo = '$idControlProcesos'
              ) AS fk_rhb001_num_empleado_aprueba,
              (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_c002_proceso_periodo ON rh_b001_empleado.pk_num_empleado = nm_c002_proceso_periodo.fk_rhb001_num_empleado_procesa
                    WHERE
                        pk_num_proceso_periodo = '$idControlProcesos'
              ) AS fk_rhb001_num_empleado_procesa,
              (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_c002_proceso_periodo ON rh_b001_empleado.pk_num_empleado = nm_c002_proceso_periodo.fk_rhb001_num_empleado_cierra
                    WHERE
                        pk_num_proceso_periodo = '$idControlProcesos'
              ) AS fk_rhb001_num_empleado_cierra
            FROM
              nm_c002_proceso_periodo
            INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = nm_c002_proceso_periodo.fk_a018_num_seguridadusuario
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
            LEFT JOIN nm_b001_tipo_nomina ON nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo = nm_b001_tipo_nomina.pk_num_tipo_nomina
            LEFT JOIN nm_b003_tipo_proceso ON nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso = nm_b003_tipo_proceso.pk_num_tipo_proceso
            WHERE
              pk_num_proceso_periodo='$idControlProcesos'
        ");
        //var_dump($controlProcesos);
        $controlProcesos->setFetchMode(PDO::FETCH_ASSOC);
        return $controlProcesos->fetch();
    }

    public function metListarProcesosNomina($idNomina)
    {
        $procesosNomina = $this->_db->query("
            SELECT
              pk_num_tipo_proceso,
              ind_nombre_proceso
            FROM
              nm_d004_proceso_tipo_nomina
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_d004_proceso_tipo_nomina.fk_nmb003_num_tipo_proceso
            WHERE
              fk_nmb001_num_tipo_nomina='$idNomina'
        ");
        $procesosNomina->setFetchMode(PDO::FETCH_ASSOC);
        return $procesosNomina->fetchAll();
    }

    public function metListarPeriodosNomina($idNomina)
    {
        $periodosNomina = $this->_db->query("
            SELECT
              pk_num_tipo_nomina_periodo,
              fec_anio,
              fec_mes
            FROM
              nm_c001_tipo_nomina_periodo
            WHERE
              fk_nmb001_num_tipo_nomina='$idNomina'
        ");
        $periodosNomina->setFetchMode(PDO::FETCH_ASSOC);
        return $periodosNomina->fetchAll();
    }

    public function metListarProcesosPeriodoNomina($idPeriodoNomina,$todos,$tipo)
    {
        if($todos){
            $todos=" OR nm_c002_proceso_periodo.ind_estado='CR'";
        }

        if($tipo=='procesos'){
            $sql="
                SELECT
                  nm_c002_proceso_periodo.pk_num_proceso_periodo,
                  nm_b003_tipo_proceso.ind_nombre_proceso
                FROM
                  nm_c002_proceso_periodo
                INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
                WHERE
                  nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo='$idPeriodoNomina'
                AND (nm_c002_proceso_periodo.ind_estado='AP' OR nm_c002_proceso_periodo.ind_estado='GE' OR nm_c002_proceso_periodo.ind_estado='CR' $todos)
            ";
        }else{
            $sql="
                SELECT
                  nm_c002_proceso_periodo.pk_num_proceso_periodo,
                  nm_b003_tipo_proceso.ind_nombre_proceso,
                  concat(nm_c001_tipo_nomina_periodo.fec_anio,'-',nm_c001_tipo_nomina_periodo.fec_mes) AS fk_nmc001_num_tipo_nomina_periodo,
                  nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo
                FROM
                  nm_c002_proceso_periodo
                INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
                INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo

                WHERE
                  nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina='$idPeriodoNomina'
                AND (nm_c002_proceso_periodo.ind_estado='AP' OR nm_c002_proceso_periodo.ind_estado='GE' OR nm_c002_proceso_periodo.ind_estado='CR' $todos)
                GROUP BY nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo
            ";
        }

        $periodosNomina = $this->_db->query($sql);


        $periodosNomina->setFetchMode(PDO::FETCH_ASSOC);
        return $periodosNomina->fetchAll();
    }

    public function metAperturarPeriodo($idNomina, $periodo)
    {
        $periodo = str_getcsv($periodo, '-');
        $this->_db->beginTransaction();
        $nominaPeriodo = $this->_db->prepare("
                  INSERT INTO
                    nm_c001_tipo_nomina_periodo
                  SET
                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina, fec_anio=:fec_anio, fec_mes=:fec_mes
        ");
        $nominaPeriodo->execute(array(
            'fk_nmb001_num_tipo_nomina' => $idNomina,
            'fec_anio' => $periodo[0],
            'fec_mes' => $periodo[1]
        ));
        $idRegistro = $this->_db->lastInsertId();
        $error = $nominaPeriodo->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metAperturarProceso($idProceso, $idPeriodoNomina, $fecDesde, $fecHasta)
    {
        $this->_db->beginTransaction();
        $nominaPeriodo = $this->_db->prepare("
                  INSERT INTO
                    nm_c002_proceso_periodo
                  SET
                    fk_a018_num_seguridadusuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), fec_creado=NOW(),fk_rhb001_num_empleado_crea='$this->atIdEmpleado',
                    fec_desde=:fec_desde, fec_hasta=:fec_hasta, ind_estado='PR', fk_nmb003_num_tipo_proceso=:fk_nmb003_num_tipo_proceso,
                    fk_nmc001_num_tipo_nomina_periodo=:fk_nmc001_num_tipo_nomina_periodo
        ");
        $nominaPeriodo->execute(array(
            'fk_nmb003_num_tipo_proceso' => $idProceso,
            'fk_nmc001_num_tipo_nomina_periodo' => $idPeriodoNomina,
            'fec_desde' => $fecDesde,
            'fec_hasta' => $fecHasta
        ));
        $idRegistro = $this->_db->lastInsertId();
        $error = $nominaPeriodo->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarAperturarProceso($idProceso, $idPeriodoNomina, $fecDesde, $fecHasta, $idControlProceso)
    {
        $this->_db->beginTransaction();
        $nominaPeriodo = $this->_db->prepare("
                  UPDATE
                    nm_c002_proceso_periodo
                  SET
                    fk_a018_num_seguridadusuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fec_desde=:fec_desde, fec_hasta=:fec_hasta, fk_nmb003_num_tipo_proceso=:fk_nmb003_num_tipo_proceso,
                    fk_nmc001_num_tipo_nomina_periodo=:fk_nmc001_num_tipo_nomina_periodo
                  WHERE
                    pk_num_proceso_periodo='$idControlProceso'
        ");
        $nominaPeriodo->execute(array(
            'fk_nmb003_num_tipo_proceso' => $idProceso,
            'fk_nmc001_num_tipo_nomina_periodo' => $idPeriodoNomina,
            'fec_desde' => $fecDesde,
            'fec_hasta' => $fecHasta
        ));
        $error = $nominaPeriodo->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idControlProceso;
        }
    }

    public function metCambioStatusAperturarProceso($idControlProceso, $status)
    {
        $this->_db->beginTransaction();
        if ($status=='AP') {
            $nominaPeriodo = $this->_db->prepare("
                  UPDATE
                    nm_c002_proceso_periodo
                  SET
                    fk_a018_num_seguridadusuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), num_flag_aprobado=1,
                    fk_rhb001_num_empleado_aprueba='$this->atIdEmpleado', fec_aprobado=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_proceso_periodo='$idControlProceso'
            ");
        } elseif ($status=='CR') {
            $nominaPeriodo = $this->_db->prepare("
                  UPDATE
                    nm_c002_proceso_periodo
                  SET
                    fk_a018_num_seguridadusuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), num_flag_cerrado=1,
                    fk_rhb001_num_empleado_cierra='$this->atIdEmpleado', fec_cerrado=NOW(), ind_estado=:ind_estado
                  WHERE
                    pk_num_proceso_periodo='$idControlProceso'
            ");
        }

        $nominaPeriodo->execute(array(
            'ind_estado' => $status
        ));

        if ($status=='CR') {
            $datos = $this->metBuscarControlProcesos($idControlProceso);
            $fec_anio = $datos['fec_anio'];
            $fec_mes = $datos['fec_mes'];
            $cod_proceso = $datos['cod_proceso'];
            $fk_nmb001_num_tipo_nomina = $datos['fk_nmb001_num_tipo_nomina'];
            if($cod_proceso == 'FIN'){
                $prestacionesSociales = $this->_db->prepare("
                      UPDATE
                        nm_e002_prestaciones_sociales_calculo
                      SET
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                        num_estatus = 1
                      WHERE
                        fec_anio = :fec_anio AND 
                        fec_mes  = :fec_mes AND 
                        fk_nmb001_num_tipo_nomina = :fk_nmb001_num_tipo_nomina
                ");
                $prestacionesSociales->execute(array(
                    'fec_anio' => $fec_anio,
                    'fec_mes' => $fec_mes,
                    'fk_nmb001_num_tipo_nomina' => $fk_nmb001_num_tipo_nomina
                ));
            }elseif ($cod_proceso == 'RTA'){
                $prestacionesSociales = $this->_db->prepare("
                      UPDATE
                        nm_e001_prestaciones_sociales_calculo_retroactivo
                      SET
                        fk_a018_num_seguridadusuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                        num_estatus = 1
                      WHERE
                        fec_anio = :fec_anio AND 
                        fec_mes  = :fec_mes AND 
                        fk_nmb001_num_tipo_nomina = :fk_nmb001_num_tipo_nomina
                ");
                $prestacionesSociales->execute(array(
                    'fec_anio' => $fec_anio,
                    'fec_mes' => $fec_mes,
                    'fk_nmb001_num_tipo_nomina' => $fk_nmb001_num_tipo_nomina
                ));
            }
            if($cod_proceso == 'FIN' || $cod_proceso == 'RTA'){
                $error2 = $prestacionesSociales->errorInfo();
                if (!empty($error2[1]) && !empty($error2[2])) {
                    $this->_db->rollBack();
                    return $error2;
                }
            }
        }


        $error = $nominaPeriodo->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idControlProceso;
        }
    }
}
