<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ajusteSalarialModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListaAjustes($estatus = false)
    {
        if ($estatus) {
            $estatus = "WHERE ind_estado='$estatus'";
        } else {
            $estatus = "";
        }
        $listaEmpleados = $this->_db->query("
            SELECT
              *
            FROM
              nm_b004_ajuste_salarial
              $estatus
        ");
        $listaEmpleados->setFetchMode(PDO::FETCH_ASSOC);
        return $listaEmpleados->fetchAll();
    }

    public function metListarGrados()
    {
        $listaGrados = $this->_db->query("
            SELECT
              rh_c007_grado_salarial.*,
              a006_miscelaneo_detalle.ind_nombre_detalle
            FROM
              rh_c007_grado_salarial
              INNER JOIN a006_miscelaneo_detalle ON rh_c007_grado_salarial.fk_a006_num_miscelaneo_detalle = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            ORDER BY fk_a006_num_miscelaneo_detalle,ind_grado ASC
              
        ");
        $listaGrados->setFetchMode(PDO::FETCH_ASSOC);
        return $listaGrados->fetchAll();
    }

    public function metListarGradosPaso($pk_grado_salarial)
    {
        $listaGrados = $this->_db->query("
            SELECT
              *
            FROM
              rh_c027_grado_salarial_pasos
              INNER JOIN a006_miscelaneo_detalle ON rh_c027_grado_salarial_pasos.fk_a006_num_miscelaneo_detalle_paso = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            WHERE 
              fk_rhc007_num_grado = '$pk_grado_salarial'
        ");
        $listaGrados->setFetchMode(PDO::FETCH_ASSOC);
        return $listaGrados->fetchAll();
    }

    public function metBuscarAjusteSalarial($idAjuste)
    {
        $ajuste = $this->_db->query("
            SELECT
              nm_b004_ajuste_salarial.*,
                a018_seguridad_usuario.ind_usuario,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_b004_ajuste_salarial ON rh_b001_empleado.pk_num_empleado = nm_b004_ajuste_salarial.fk_rhb001_num_empleado_crea
                    WHERE
                        pk_num_ajust_salarial = '$idAjuste'
                ) AS EMPLEADO_PREPARADO,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_b004_ajuste_salarial ON rh_b001_empleado.pk_num_empleado = nm_b004_ajuste_salarial.fk_rhb001_num_empleado_aprueba
                    WHERE
                        pk_num_ajust_salarial = '$idAjuste'
                ) AS EMPLEADO_APROBADO,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_b004_ajuste_salarial ON rh_b001_empleado.pk_num_empleado = nm_b004_ajuste_salarial.fk_rhb001_num_empleado_conforma
                    WHERE
                        pk_num_ajust_salarial = '$idAjuste'
                ) AS EMPLEADO_CONFORMADO,
                (
                    SELECT
                        CONCAT(a003_persona.ind_nombre1,' ',a003_persona.ind_apellido1)
                    FROM
                        rh_b001_empleado
                        INNER JOIN a003_persona ON a003_persona.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
                        INNER JOIN nm_b004_ajuste_salarial ON rh_b001_empleado.pk_num_empleado = nm_b004_ajuste_salarial.fk_rhb001_num_empleado_anula
                    WHERE
                        pk_num_ajust_salarial = '$idAjuste'
                ) AS EMPLEADO_ANULADO
            FROM
              nm_b004_ajuste_salarial
              INNER JOIN a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = nm_b004_ajuste_salarial.fk_a018_num_seguridad_usuario

            WHERE
              pk_num_ajust_salarial='$idAjuste'
        ");
        $ajuste->setFetchMode(PDO::FETCH_ASSOC);
        return $ajuste->fetch();
    }

    public function metBuscarAjusteSalarialDet($idAjuste)
    {
        $ajuste = $this->_db->query("
            SELECT
              *
            FROM
              nm_c003_ajust_salarial_det
            WHERE
              fk_nmb004_num_ajuste_salarial='$idAjuste';
        ");
        $ajuste->setFetchMode(PDO::FETCH_ASSOC);
        return $ajuste->fetchAll();
    }

    public function metBuscarAjusteSalarialDetPaso($idAjuste)
    {
        $ajuste = $this->_db->query("
            SELECT
              nm_c005_ajust_salarial_det_paso.*,
              a006_miscelaneo_detalle.*
            FROM
              nm_c005_ajust_salarial_det_paso
              INNER JOIN rh_c027_grado_salarial_pasos ON nm_c005_ajust_salarial_det_paso.fk_rhc027_num_grados_pasos = rh_c027_grado_salarial_pasos.pk_num_grados_pasos
              INNER JOIN a006_miscelaneo_detalle ON rh_c027_grado_salarial_pasos.fk_a006_num_miscelaneo_detalle_paso = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            WHERE
              fk_nmb004_num_ajuste_salarial='$idAjuste';
        ");
        $ajuste->setFetchMode(PDO::FETCH_ASSOC);
        return $ajuste->fetchAll();
    }

    public function metCrearAjusteSalarial($descripcion, $numGaceta, $numResolucion, $periodo, $ajusteDet, $ajusteDetPaso)
    {
        $periodo = str_getcsv($periodo, '-');
        $this->_db->beginTransaction();
        $registrarAjuste = $this->_db->prepare("
                  INSERT INTO
                    nm_b004_ajuste_salarial
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), fec_preparado=NOW(),
                    fec_anio=:fec_anio, fec_mes=:fec_mes, fk_rhb001_num_empleado_crea='$this->atIdEmpleado',
                    ind_descripcion=:ind_descripcion, ind_estado='PR', ind_numero_gaceta=:ind_numero_gaceta,
                    ind_numero_resolucion=:ind_numero_resolucion

        ");
        $registrarAjuste->execute(array(
            'fec_anio' => $periodo[0],
            'fec_mes' => $periodo[1],
            'ind_descripcion' => $descripcion,
            'ind_numero_gaceta' => $numGaceta,
            'ind_numero_resolucion' => $numResolucion,
        ));
        $idRegistro = $this->_db->lastInsertId();

        $registrarAjusteDet = $this->_db->prepare("
                  INSERT INTO
                    nm_c003_ajust_salarial_det
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fk_nmb004_num_ajuste_salarial='$idRegistro', fk_rhc007_pk_num_grado=:fk_rhc007_pk_num_grado,
                    num_porcentaje=:num_porcentaje, num_monto=:num_monto, num_sueldo_promedio=:num_sueldo_promedio
                    , num_sueldo_anterior=:num_sueldo_anterior
        ");
        $idGrados = $ajusteDet['checkBox'];
        foreach ($idGrados as $idGrado) {
            if ($ajusteDet[$idGrado]['ind_porcentaje'] == '') {
                $porcentaje = null;
                $monto = $ajusteDet[$idGrado]['num_monto'];
            } else {
                $porcentaje = $ajusteDet[$idGrado]['ind_porcentaje'];
                $monto = null;
            }
            $registrarAjusteDet->execute(array(
                'fk_rhc007_pk_num_grado' => $idGrado,
                'num_porcentaje' => $porcentaje,
                'num_monto' => $monto,
                'num_sueldo_promedio' => $ajusteDet[$idGrado]['sueldoNuevo'],
                'num_sueldo_anterior' => $ajusteDet[$idGrado]['num_sueldo_anterior']
            ));
        }


        $registrarAjusteDetPaso = $this->_db->prepare("
                  INSERT INTO
                    nm_c005_ajust_salarial_det_paso
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fk_nmb004_num_ajuste_salarial='$idRegistro', fk_rhc007_num_grado=:fk_rhc007_num_grado,
                    num_porcentaje=:num_porcentaje, num_monto=:num_monto, num_sueldo_promedio=:num_sueldo_promedio,
                    num_sueldo_anterior=:num_sueldo_anterior, fk_rhc027_num_grados_pasos=:fk_rhc027_num_grados_pasos
        ");
        $idGradoPasos = $ajusteDetPaso['checkBox'];
        foreach ($idGradoPasos as $idGradoPaso) {
            if ($ajusteDetPaso[$idGradoPaso]['ind_porcentaje'] == '') {
                $porcentaje = null;
                $monto = $ajusteDetPaso[$idGradoPaso]['num_monto'];
            } else {
                $porcentaje = $ajusteDetPaso[$idGradoPaso]['ind_porcentaje'];
                $monto = null;
            }
            $registrarAjusteDetPaso->execute(array(
                'fk_rhc027_num_grados_pasos' => $idGradoPaso,
                'fk_rhc007_num_grado' => $ajusteDetPaso[$idGradoPaso]['idGrado'],
                'num_porcentaje' => $porcentaje,
                'num_monto' => $monto,
                'num_sueldo_promedio' => $ajusteDetPaso[$idGradoPaso]['sueldoNuevo'],
                'num_sueldo_anterior' => $ajusteDetPaso[$idGradoPaso]['num_sueldo_anterior']
            ));
        }


        $error = $registrarAjuste->errorInfo();
        $error2 = $registrarAjusteDet->errorInfo();
        $error3 = $registrarAjusteDetPaso->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } elseif (!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        } elseif (!empty($error3[1]) && !empty($error3[2])) {
            $this->_db->rollBack();
            var_dump($error3);
            return $error3;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarAjusteSalarial($idAjuste, $descripcion, $numGaceta, $numResolucion, $periodo, $ajusteDet, $ajusteDetPaso)
    {
        $periodo = str_getcsv($periodo, '-');
        $this->_db->beginTransaction();
        $registrarAjuste = $this->_db->prepare("
                  UPDATE
                    nm_b004_ajuste_salarial
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    fec_anio=:fec_anio, fec_mes=:fec_mes,
                    ind_descripcion=:ind_descripcion, ind_estado='PR', ind_numero_gaceta=:ind_numero_gaceta,
                    ind_numero_resolucion=:ind_numero_resolucion
                  WHERE
                    pk_num_ajust_salarial='$idAjuste'

        ");
        $registrarAjuste->execute(array(
            'fec_anio' => $periodo[0],
            'fec_mes' => $periodo[1],
            'ind_descripcion' => $descripcion,
            'ind_numero_gaceta' => $numGaceta,
            'ind_numero_resolucion' => $numResolucion,
        ));

        $registrarAjusteDet = $this->_db->prepare("
                  UPDATE
                    nm_c003_ajust_salarial_det
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_porcentaje=:num_porcentaje, num_monto=:num_monto, num_sueldo_promedio=:num_sueldo_promedio,
                    num_sueldo_anterior=:num_sueldo_anterior
                  WHERE
                    fk_nmb004_num_ajuste_salarial='$idAjuste' AND pk_num_ajuste_salarial_det=:pk_num_ajuste_salarial_det
        ");
        $idGrados = $ajusteDet['checkBox'];
        foreach ($idGrados as $idGrado) {
            if ($ajusteDet[$idGrado]['ind_porcentaje'] == '') {
                $porcentaje = null;
                $monto = $ajusteDet[$idGrado]['num_monto'];
            } else {
                $porcentaje = $ajusteDet[$idGrado]['ind_porcentaje'];
                $monto = null;
            }
            $registrarAjusteDet->execute(array(
                'pk_num_ajuste_salarial_det' => $ajusteDet[$idGrado]['idAjusteDet'],
                'num_porcentaje' => $porcentaje,
                'num_monto' => $monto,
                'num_sueldo_promedio' => $ajusteDet[$idGrado]['sueldoNuevo'],
                'num_sueldo_anterior' => $ajusteDet[$idGrado]['num_sueldo_anterior']
            ));
        }

        $registrarAjusteDetPaso = $this->_db->prepare("
                  UPDATE
                    nm_c005_ajust_salarial_det_paso
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_porcentaje=:num_porcentaje, num_monto=:num_monto, num_sueldo_promedio=:num_sueldo_promedio,
                    num_sueldo_anterior=:num_sueldo_anterior
                  WHERE
                    fk_nmb004_num_ajuste_salarial='$idAjuste' AND pk_num_ajuste_salarial_det_paso=:pk_num_ajuste_salarial_det_paso
        ");
        $idGradoPasos = $ajusteDetPaso['checkBox'];
        foreach ($idGradoPasos as $idGradoPaso) {
            if ($ajusteDetPaso[$idGradoPaso]['ind_porcentaje'] == '') {
                $porcentaje = null;
                $monto = $ajusteDetPaso[$idGradoPaso]['num_monto'];
            } else {
                $porcentaje = $ajusteDetPaso[$idGradoPaso]['ind_porcentaje'];
                $monto = null;
            }
            $registrarAjusteDetPaso->execute(array(
                'pk_num_ajuste_salarial_det_paso' => $ajusteDetPaso[$idGradoPaso]['idAjusteDetPaso'],
                'num_porcentaje' => $porcentaje,
                'num_monto' => $monto,
                'num_sueldo_promedio' => $ajusteDetPaso[$idGradoPaso]['sueldoNuevo'],
                'num_sueldo_anterior' => $ajusteDetPaso[$idGradoPaso]['num_sueldo_anterior']
            ));
        }


        $error = $registrarAjuste->errorInfo();
        $error2 = $registrarAjusteDet->errorInfo();
        $error3 = $registrarAjusteDetPaso->errorInfo();
        if (!empty($error[1]) && !empty($error[2]) && !empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idAjuste;
        }
    }

    public function metCambioSatus($idAjuste, $estado, $anulacion = null)
    {
        if ($estado == 'AN') {
            $responsable = ", fk_rhb001_num_empleado_anula='$this->atIdEmpleado', fec_anulado=NOW()";
        } elseif ($estado == 'CO') {
            $responsable = ", fk_rhb001_num_empleado_conforma='$this->atIdEmpleado', fec_conformado=NOW()";
        } elseif ($estado == 'AP') {
            $responsable = ", fk_rhb001_num_empleado_aprueba='$this->atIdEmpleado', fec_aprobado=NOW()";
        }
        $this->_db->beginTransaction();
        $registrarAjuste = $this->_db->prepare("
                  UPDATE
                    nm_b004_ajuste_salarial
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    ind_estado=:ind_estado,ind_motivo_anulacion=:ind_motivo_anulacion $responsable
                  WHERE
                    pk_num_ajust_salarial='$idAjuste'

        ");
        $registrarAjuste->execute(array(
            'ind_estado' => $estado,
            'ind_motivo_anulacion' => $anulacion,
        ));

        $error = $registrarAjuste->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idAjuste;
        }
    }

    public function metActualizarGrado($idAjuste, $grados, $gradosPaso)
    {
        $this->_db->beginTransaction();
        $gradoSalarial = $this->_db->prepare("
                  UPDATE
                    rh_c007_grado_salarial
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_sueldo_minimo=:num_sueldo_minimo, num_sueldo_promedio=:num_sueldo_promedio,
                    num_sueldo_maximo=:num_sueldo_maximo
                  WHERE
                    pk_num_grado=:pk_num_grado

        ");
        foreach ($grados as $grado) {
            $gradoSalarial->execute(array(
                'pk_num_grado' => $grado['fk_rhc007_pk_num_grado'],
                'num_sueldo_minimo' => $grado['num_sueldo_promedio'],
                'num_sueldo_promedio' => $grado['num_sueldo_promedio'],
                'num_sueldo_maximo' => $grado['num_sueldo_promedio']
            ));
        }
        $gradoSalarialPaso = $this->_db->prepare("
                  UPDATE
                    rh_c027_grado_salarial_pasos
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(),
                    num_sueldo_minimo=:num_sueldo_minimo, num_sueldo_promedio=:num_sueldo_promedio,
                    num_sueldo_maximo=:num_sueldo_maximo
                  WHERE
                    pk_num_grados_pasos=:pk_num_grados_pasos

        ");
        foreach ($gradosPaso as $gradoPaso) {
            $gradoSalarialPaso->execute(array(
                'pk_num_grados_pasos' => $gradoPaso['fk_rhc027_num_grados_pasos'],
                'num_sueldo_minimo' => $gradoPaso['num_sueldo_promedio'],
                'num_sueldo_promedio' => $gradoPaso['num_sueldo_promedio'],
                'num_sueldo_maximo' => $gradoPaso['num_sueldo_promedio']
            ));
        }

        $error = $gradoSalarial->errorInfo();
        $error2 = $gradoSalarialPaso->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } elseif(!empty($error2[1]) && !empty($error2[2])) {
            $this->_db->rollBack();
            return $error2;
        } else {
            $this->_db->commit();
            return $idAjuste;
        }
    }
}
