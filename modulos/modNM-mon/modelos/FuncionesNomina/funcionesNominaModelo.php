<?php

/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        18-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class funcionesNominaModelo extends Modelo
{
    private $atIdUsuario;

    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metFechaNacimiento($idEmpleado)
    {
        $sueldoMinimo = $this->_db->query(
            "
              SELECT
                fec_nacimiento
              FROM
                a003_persona
              INNER JOIN rh_b001_empleado ON  rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
              WHERE 
              rh_b001_empleado.pk_num_empleado = '$idEmpleado'
            "
        );
        $sueldoMinimo->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoMinimo->fetch();
    }

    public function metSueldoMinimo()
    {
        $sueldoMinimo = $this->_db->query(
            "
              SELECT
                num_sueldo_minimo
              FROM
                nm_b006_sueldo_minimo
              WHERE ind_estado='AP'
              ORDER BY pk_num_sueldo_minimo DESC
              LIMIT 1
            "
        );
        $sueldoMinimo->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoMinimo->fetch();
    }

    public function metSueldoBasico($idEmpleado)
    {
        $sueldoBasico = $this->_db->query(
            "
              SELECT
                num_sueldo_promedio
              FROM
                rh_b001_empleado
              INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos=rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
              INNER JOIN rh_c007_grado_salarial ON rh_c007_grado_salarial.pk_num_grado = rh_c063_puestos.fk_rhc007_num_grado
              WHERE
                rh_b001_empleado.pk_num_empleado='$idEmpleado'
              ORDER BY pk_num_empleado_laboral DESC
              LIMIT 1
            "
        );
        $sueldoBasico->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoBasico->fetch();
    }

    public function metSueldoBasicoAnterior($idEmpleado)
    {
        $sueldoBasico = $this->_db->query(
            "
             SELECT
              num_monto
            FROM
              nm_d005_tipo_nomina_empleado_concepto
            WHERE
              fk_rhb001_num_empleado = '$idEmpleado' AND 
              (fk_nmb002_num_concepto = '26' or fk_nmb002_num_concepto = '60')
              AND fk_nmc002_num_proceso_periodo = '115'
            ORDER BY fec_ultima_modificacion ASC
            LIMIT 1
            "
        );
        $sueldoBasico->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoBasico->fetch();
    }

    public function metPorcentajeRetroactivo()
    {
        $sueldoBasico = $this->_db->query(
            "
             SELECT
              ind_valor_parametro
            FROM
              a035_parametros
            WHERE
              ind_parametro_clave = 'PCRETRO'
            "
        );
        $sueldoBasico->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoBasico->fetch();
    }

    public function metDiferenciaSueldoBasico($idEmpleado)
    {
        $sueldoBasico = $this->_db->query(
            "
              SELECT
                num_sueldo_promedio
              FROM
                rh_b001_empleado
              INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos=rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargotmp
              INNER JOIN rh_c007_grado_salarial ON rh_c007_grado_salarial.pk_num_grado = rh_c063_puestos.fk_rhc007_num_grado
              WHERE
                rh_b001_empleado.pk_num_empleado='$idEmpleado'
              ORDER BY pk_num_empleado_laboral DESC
              LIMIT 1
            "
        );
        $sueldoBasico->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoBasico->fetch();
    }

    public function metSueldoBasicoPaso($idEmpleado)
    {
        $sueldoBasico = $this->_db->query(
            "
              SELECT
                rh_c027_grado_salarial_pasos.num_sueldo_promedio
              FROM
                rh_b001_empleado
              INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos=rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
              INNER JOIN rh_c007_grado_salarial ON rh_c007_grado_salarial.pk_num_grado = rh_c063_puestos.fk_rhc007_num_grado
              INNER JOIN rh_c027_grado_salarial_pasos ON (
                  rh_c007_grado_salarial.pk_num_grado = rh_c027_grado_salarial_pasos.fk_rhc007_num_grado AND 
                  rh_c027_grado_salarial_pasos.fk_a006_num_miscelaneo_detalle_paso = rh_c005_empleado_laboral.fk_a006_num_miscelaneo_detalle_paso
              )
              WHERE
                rh_b001_empleado.pk_num_empleado='$idEmpleado'
              ORDER BY pk_num_empleado_laboral DESC
              LIMIT 1
            "
        );
        $sueldoBasico->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoBasico->fetch();
    }

    public function metDiferenciaSueldoBasicoPaso($idEmpleado)
    {
        $sueldoBasico = $this->_db->query(
            "
              SELECT
                rh_c027_grado_salarial_pasos.num_sueldo_promedio
              FROM
                rh_b001_empleado
              INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado
              INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos=rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargotmp
              INNER JOIN rh_c007_grado_salarial ON rh_c007_grado_salarial.pk_num_grado = rh_c063_puestos.fk_rhc007_num_grado
              INNER JOIN rh_c027_grado_salarial_pasos ON (
                  rh_c007_grado_salarial.pk_num_grado = rh_c027_grado_salarial_pasos.fk_rhc007_num_grado AND 
                  rh_c027_grado_salarial_pasos.fk_a006_num_miscelaneo_detalle_paso = rh_c005_empleado_laboral.fk_a006_num_miscelaneo_detalle_paso
              )
              WHERE
                rh_b001_empleado.pk_num_empleado='$idEmpleado'
              ORDER BY pk_num_empleado_laboral DESC
              LIMIT 1
            "
        );
        $sueldoBasico->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoBasico->fetch();
    }

    public function metSueldoBasicoJP($idEmpleado)
    {
        $sueldoBasico = $this->_db->query(
            "
              SELECT
                num_sueldo
              FROM
                rh_c005_empleado_laboral
              WHERE
                fk_rhb001_num_empleado='$idEmpleado'
            "
        );
        $sueldoBasico->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoBasico->fetch();
    }    

    public function metObtenerAñosAdministracionPublica($idEmpleado)
    {
        $añosAdministracionPublica = $this->_db->query(
            "
              SELECT
                fec_ingreso,
                fec_egreso
              FROM
                rh_c011_experiencia
              INNER JOIN a006_miscelaneo_detalle ON rh_c011_experiencia.fk_a006_num_miscelaneo_detalle_tipo_empresa=a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
              WHERE
                rh_c011_experiencia.fk_rhb001_num_empleado='$idEmpleado' AND
                a006_miscelaneo_detalle.cod_detalle='G'
              ORDER BY fec_ingreso DESC
            "
        );
        $añosAdministracionPublica->setFetchMode(PDO::FETCH_ASSOC);
        return $añosAdministracionPublica->fetchAll();
    }

    private function metMostrarSelect($codMaestro, $codDetalle)
    {
        $miscelaneoDetalle = $this->_db->query("
            SELECT
              a006.pk_num_miscelaneo_detalle
            FROM
              a006_miscelaneo_detalle AS a006
            INNER JOIN
              a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
            WHERE
              a005.cod_maestro='$codMaestro' AND
              a006.cod_detalle='$codDetalle'
        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetch();
    }

    public function metObtenerHijos($idEmpleado)
    {
        $idSelect=$this->metMostrarSelect('PARENT','HI');
        $sueldoBasico = $this->_db->query(
            "
              SELECT
                *
              FROM
                rh_b001_empleado
              INNER JOIN rh_c015_carga_familiar ON rh_c015_carga_familiar.fk_rhb001_num_empleado=rh_b001_empleado.pk_num_empleado
              INNER JOIN a006_miscelaneo_detalle ON rh_c015_carga_familiar.fk_a006_num_miscelaneo_detalle_parentezco=a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
              INNER JOIN a003_persona ON rh_c015_carga_familiar.fk_a003_num_persona=a003_persona.pk_num_persona
              WHERE
                rh_b001_empleado.pk_num_empleado='$idEmpleado' AND a006_miscelaneo_detalle.pk_num_miscelaneo_detalle='$idSelect[pk_num_miscelaneo_detalle]'
            "
        );
        $sueldoBasico->setFetchMode(PDO::FETCH_ASSOC);
        return $sueldoBasico->fetchAll();
    }

    public function metObtenerTipoConceptoCalculados($idEmpleado,$idNomina,$codProceso,$periodo,$tipo,$flagIncidencia=0)
    {
        $periodo=str_getcsv($periodo,'-');
        $asignacionesAutomaticas = $this->_db->query(
            "
              SELECT
                SUM(nm_d005_tipo_nomina_empleado_concepto.num_monto) AS num_monto
              FROM
                nm_d005_tipo_nomina_empleado_concepto
              INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
              INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = nm_b002_concepto.fk_a006_num_tipo_concepto
              INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
              INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
              INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
              INNER JOIN nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina

              WHERE
                a006_miscelaneo_detalle.cod_detalle='$tipo' AND
                nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado='$idEmpleado' AND
                nm_c001_tipo_nomina_periodo.fec_anio='$periodo[0]' AND
                nm_c001_tipo_nomina_periodo.fec_mes='$periodo[1]' AND
                nm_b001_tipo_nomina.pk_num_tipo_nomina='$idNomina' AND
                nm_b003_tipo_proceso.cod_proceso='$codProceso' AND
                nm_b002_concepto.num_flag_incidencia='$flagIncidencia'
            "
        );
        $asignacionesAutomaticas->setFetchMode(PDO::FETCH_ASSOC);
        return $asignacionesAutomaticas->fetch();
    }

    public function metObtenerConceptos($idEmpleado,$idNomina,$abrevConcepto, $CodProceso, $periodo)
    {
        $periodo=str_getcsv($periodo,'-');
        $conceptos = $this->_db->query("
            SELECT
              nm_d005_tipo_nomina_empleado_concepto.num_monto
            FROM
              nm_d005_tipo_nomina_empleado_concepto
            INNER JOIN nm_c002_proceso_periodo ON nm_c002_proceso_periodo.pk_num_proceso_periodo = nm_d005_tipo_nomina_empleado_concepto.fk_nmc002_num_proceso_periodo
            INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d005_tipo_nomina_empleado_concepto.fk_nmb002_num_concepto
            INNER JOIN nm_c001_tipo_nomina_periodo ON nm_c001_tipo_nomina_periodo.pk_num_tipo_nomina_periodo = nm_c002_proceso_periodo.fk_nmc001_num_tipo_nomina_periodo
            INNER JOIN nm_b001_tipo_nomina ON nm_b001_tipo_nomina.pk_num_tipo_nomina = nm_c001_tipo_nomina_periodo.fk_nmb001_num_tipo_nomina
            INNER JOIN nm_b003_tipo_proceso ON nm_b003_tipo_proceso.pk_num_tipo_proceso = nm_c002_proceso_periodo.fk_nmb003_num_tipo_proceso
            WHERE
              nm_d005_tipo_nomina_empleado_concepto.fk_rhb001_num_empleado='$idEmpleado' AND
              nm_b001_tipo_nomina.pk_num_tipo_nomina='$idNomina' AND
              nm_b003_tipo_proceso.cod_proceso='$CodProceso' AND
              nm_b002_concepto.ind_abrebiatura='$abrevConcepto' AND
              nm_c001_tipo_nomina_periodo.fec_anio='$periodo[0]' AND
              nm_c001_tipo_nomina_periodo.fec_mes='$periodo[1]'

        ");
        $conceptos->setFetchMode(PDO::FETCH_ASSOC);
        return $conceptos->fetch();
    }

    public function metObtenerUnidadTributaria()
    {
           $anio = Session::metObtener('UTANIO');
     $consulta = $this->_db->query(
            "
              SELECT
                ind_valor
              FROM
                cp_b018_unidad_tributaria
              WHERE
                ind_anio = '$anio'
              ORDER BY num_secuencia DESC
              LIMIT 1
            "
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metObtenerCantidadDias($idEmpleado = false, $periodo = false, $tipoNomina = false)
    {
        $consulta = $this->_db->query(
            "
              SELECT
              rh_c039_beneficio_alimentacion_detalle.num_dias_pago,
              rh_c039_beneficio_alimentacion_detalle.num_dias_descuento
            FROM
              rh_c019_beneficio_alimentacion
            INNER JOIN
              rh_c039_beneficio_alimentacion_detalle ON(
                rh_c019_beneficio_alimentacion.fec_anio = rh_c039_beneficio_alimentacion_detalle.fec_anio AND rh_c019_beneficio_alimentacion.pk_num_beneficio = rh_c039_beneficio_alimentacion_detalle.fk_rhc019_num_beneficio
              )
            WHERE
              rh_c039_beneficio_alimentacion_detalle.fk_rhb001_num_empleado = '$idEmpleado' 
              AND rh_c019_beneficio_alimentacion.ind_periodo = '$periodo' 
              AND rh_c019_beneficio_alimentacion.fk_nmb001_num_tipo_nomina = '$tipoNomina'
            "
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metObtenerGradoInstruccion($idEmpleado)
    {
        $consulta = $this->_db->query(
            "
              SELECT
                *
              FROM
                rh_c018_empleado_instruccion
                INNER JOIN rh_c064_nivel_grado_instruccion ON rh_c018_empleado_instruccion.fk_rhc064_num_nivel_grado = rh_c064_nivel_grado_instruccion.pk_num_nivel_grado
                INNER JOIN a006_miscelaneo_detalle ON rh_c018_empleado_instruccion.fk_a006_num_miscelaneo_detalle_gradoinst = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
              WHERE
                rh_c018_empleado_instruccion.fk_rhb001_num_empleado='$idEmpleado'
              ORDER BY a006_miscelaneo_detalle.pk_num_miscelaneo_detalle DESC
            "
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metObtenerJerarquia($idEmpleado)
    {
        $consulta = $this->_db->query(
            "
              SELECT
                ind_nombre_cargo,
                rh_c063_puestos.ind_descripcion_cargo
              FROM
                rh_b001_empleado
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
                INNER JOIN rh_c010_serie ON rh_c010_serie.pk_num_serie = rh_c063_puestos.fk_rhc010_num_serie
                INNER JOIN rh_c006_tipo_cargo ON rh_c006_tipo_cargo.pk_num_cargo = rh_c063_puestos.fk_rhc006_num_cargo
              WHERE
                rh_b001_empleado.pk_num_empleado='$idEmpleado'
              ORDER BY pk_num_empleado DESC 
              LIMIT 1
            "
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metObtenerJerarquia2($idEmpleado)
    {
        $consulta = $this->_db->query(
            "
              SELECT
                ind_nombre_cargo,
                rh_c063_puestos.ind_descripcion_cargo
              FROM
                rh_b001_empleado
                INNER JOIN rh_c005_empleado_laboral ON rh_c005_empleado_laboral.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
                INNER JOIN rh_c063_puestos ON rh_c063_puestos.pk_num_puestos = rh_c005_empleado_laboral.fk_rhc063_num_puestos_cargo
                INNER JOIN rh_c010_serie ON rh_c010_serie.pk_num_serie = rh_c063_puestos.fk_rhc010_num_serie
                INNER JOIN rh_c006_tipo_cargo ON rh_c006_tipo_cargo.pk_num_cargo = rh_c063_puestos.fk_rhc006_num_cargo
              WHERE 
                rh_b001_empleado.pk_num_empleado='$idEmpleado'
              ORDER BY pk_num_empleado DESC 
              LIMIT 1
            "
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metEjecutarConceptoCodigo($codigo)
    {
        $consulta = $this->_db->query(
            "
              SELECT
                ind_formula
              FROM
                nm_b002_concepto
                WHERE ind_abrebiatura = '$codigo'
            "
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metEjecutarConceptoMonto($codigo,$pk_num_empleado)
    {
        $consulta = $this->_db->query(
            "
              SELECT
                nm_d002_empleado_concepto.num_monto
              FROM
                nm_d002_empleado_concepto
                INNER JOIN nm_b002_concepto ON nm_b002_concepto.pk_num_concepto = nm_d002_empleado_concepto.fk_nmb002_num_concepto
                WHERE nm_b002_concepto.ind_abrebiatura = '$codigo' AND nm_d002_empleado_concepto.fk_rhb001_num_empleado = '$pk_num_empleado'
            "
        );

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }


    /*
     * AGREGADA EL 08-08-2017
     * FUNCION PARA OBTENER LA ULTIMA PRIMA POR AÑOS DE SERVICIOS ASIGNADA ATRAVES DE CONCEPTO
     */
    public function metObtenerUltimaPrimaAntiguedaConcepto($idEmpleado,$concepto)
    {
        $consulta = $this->_db->query(
            "
              SELECT
                num_monto
              FROM
                nm_d002_empleado_concepto
                WHERE fk_rhb001_num_empleado = '$idEmpleado' and fk_nmb002_num_concepto=$concepto
            "
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    /*
 * AGREGADA EL 08-08-2017
 * FUNCION PARA OBTENER LA ULTIMA PRIMA POR AÑOS DE SERVICIOS EN NOMINA CALCULADA
 */
    public function metObtenerUltimaPrimaAntigueda($idEmpleado,$concepto,$periodo)
    {

        $periodo = explode("-",$periodo);

        $consulta = $this->_db->query(
            "
                SELECT
                    MAX(fec_anio),
                    MAX(fec_mes),
                    pk_num_concepto,
                    ind_descripcion,
                    num_monto
                FROM
                    vl_nm_ultima_prima_anio_servicios
                WHERE
                    fk_rhb001_num_empleado = $idEmpleado
                AND pk_num_concepto = $concepto
                AND fec_anio = '$periodo[0]'
                AND fec_mes < '$periodo[1]'
            "
        );

        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        $datos = $consulta->fetch();
        return $datos['num_monto'];

    }

    public function metObtenerUltimoPeriodoDesde($idEmpleado,$concepto)
    {
        $consulta = $this->_db->query(
            "
              SELECT
                fec_periodo_desde
              FROM
                nm_d002_empleado_concepto
                WHERE fk_rhb001_num_empleado = '$idEmpleado' and fk_nmb002_num_concepto=$concepto
            "
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        return $consulta->fetch();
    }

    public function metEncargaduriaTemporal($idEmpleado)
    {
        $consulta = $this->_db->query(
            "
              SELECT
                    a006_miscelaneo_detalle.cod_detalle
                FROM
                    rh_c059_empleado_nivelacion
                INNER JOIN a006_miscelaneo_detalle ON a006_miscelaneo_detalle.pk_num_miscelaneo_detalle = rh_c059_empleado_nivelacion.fk_a006_num_miscelaneo_detalle_tipoaccion
                WHERE
                    rh_c059_empleado_nivelacion.fk_rhb001_num_empleado = '$idEmpleado'
                ORDER BY
                    pk_num_empleado_nivelacion DESC  LIMIT 1
            "
        );
        $consulta->setFetchMode(PDO::FETCH_ASSOC);
        $datos = $consulta->fetch();
        return $datos['cod_detalle'];
    }

    public function metObtenerAltoNivel($idEmpleado)
    {
        $consulta = $this->_db->prepare(
            "
              SELECT
                rh_b001_empleado.pk_num_empleado,
                a003_persona.pk_num_persona,
                a003_persona.ind_nombre1,
                a003_persona.ind_nombre2,
                a003_persona.ind_apellido1,
                a003_persona.ind_apellido2,
                nm_b001_tipo_nomina.pk_num_tipo_nomina,
                nm_b001_tipo_nomina.cod_tipo_nomina,
                nm_b001_tipo_nomina.ind_nombre_nomina
            FROM
                rh_b001_empleado
            INNER JOIN a003_persona ON rh_b001_empleado.fk_a003_num_persona = a003_persona.pk_num_persona
            INNER JOIN rh_c076_empleado_organizacion ON rh_b001_empleado.pk_num_empleado = rh_c076_empleado_organizacion.fk_rhb001_num_empleado
            INNER JOIN nm_b001_tipo_nomina ON rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina = nm_b001_tipo_nomina.pk_num_tipo_nomina
            WHERE
                pk_num_tipo_nomina = 6 and pk_num_empleado='$idEmpleado'
            "
        );
        $consulta->execute();
        $num = $consulta->rowCount();
        return $num;
    }

    
}
