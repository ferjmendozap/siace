<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class tasaInteresesModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metMostrarTasaInteres($idTasa)
    {
        $tipoProceso = $this->_db->query("
          SELECT
            nm_b007_tasa_interes.*,
            a018_seguridad_usuario.ind_usuario
          FROM
            nm_b007_tasa_interes
          INNER JOIN
            a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = nm_b007_tasa_interes.fk_a018_num_seguridad_usuario
          WHERE
            nm_b007_tasa_interes.pk_num_tasa_interes='$idTasa'
        ");
        $tipoProceso->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoProceso->fetch();
    }

    public function metListarTasaInteres()
    {
        $tipoProceso = $this->_db->query(
            "SELECT * FROM nm_b007_tasa_interes"
        );
        $tipoProceso->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoProceso->fetchAll();
    }

    public function metCrearTasaInteres($registro)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  INSERT INTO
                    nm_b007_tasa_interes
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                    fec_gaceta=:fec_gaceta, num_gaceta=:num_gaceta, num_prom_act_pasiv=:num_prom_act_pasiv,
                    num_activa=:num_activa, num_estatus=:num_estatus  , periodo=:periodo
                ");
            $nuevoRegistro->execute(array(
                'fec_gaceta'=>$registro['fec_gaceta'],
                'num_gaceta'=>$registro['num_gaceta'],
                'num_prom_act_pasiv'=>$registro['num_prom_act_pasiv'],
                'num_activa'=>$registro['num_activa'],
                'num_estatus'=>$registro['num_estatus'],
                'periodo'=>$registro['periodo']
            ));
            $idRegistro= $this->_db->lastInsertId();
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idRegistro;
            }
    }

    public function metModificarTasaInteres($registro)
    {
        $this->_db->beginTransaction();
            $nuevoRegistro=$this->_db->prepare("
                  UPDATE
                    nm_b007_tasa_interes
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), 
                    fec_gaceta=:fec_gaceta, num_gaceta=:num_gaceta, num_prom_act_pasiv=:num_prom_act_pasiv,
                    num_activa=:num_activa, num_estatus=:num_estatus  , periodo=:periodo
                  WHERE
                    pk_num_tasa_interes = '$registro[pk_num_tasa_interes]'
            ");
            $nuevoRegistro->execute(array(
                'fec_gaceta'=>$registro['fec_gaceta'],
                'num_gaceta'=>$registro['num_gaceta'],
                'num_prom_act_pasiv'=>$registro['num_prom_act_pasiv'],
                'num_activa'=>$registro['num_activa'],
                'num_estatus'=>$registro['num_estatus'],
                'periodo'=>$registro['periodo']
            ));
            $error = $nuevoRegistro->errorInfo();
            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $registro['pk_num_tasa_interes'];
            }
    }

    public function metEliminarTasaInteres($idTasa)
    {
        $this->_db->beginTransaction();
            $elimar=$this->_db->prepare("
                DELETE FROM nm_b007_tasa_interes WHERE pk_num_tasa_interes=:pk_num_tasa_interes
            ");
            $elimar->execute(array(
                'pk_num_tasa_interes'=>$idTasa
            ));
            $error=$elimar->errorInfo();

            if(!empty($error[1]) && !empty($error[2])){
                $this->_db->rollBack();
                return $error;
            }else{
                $this->_db->commit();
                return $idTasa;
            }
    }

}
