<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

class cargarCsvPrestacionesModelo extends Modelo
{
    public function __construct() 
    {
        parent::__construct();
        $this->atIdUsuario=Session::metObtener('idUsuario');
    }

    public function metBuscarEmpleadoCsv($idEmpleado)
    {
        $tipoProceso = $this->_db->query("
          SELECT
              rh_c076_empleado_organizacion.fk_nmb001_num_tipo_nomina,
              rh_b001_empleado.pk_num_empleado
          FROM 
              rh_b001_empleado
            INNER JOIN rh_c076_empleado_organizacion ON rh_c076_empleado_organizacion.fk_rhb001_num_empleado = rh_b001_empleado.pk_num_empleado
          WHERE 
            rh_b001_empleado.pk_num_empleado = '$idEmpleado'
        ");
        $tipoProceso->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoProceso->fetch();
    }

    public function metCrearModificar($form)
    {
        $this->_db->beginTransaction();
        $registrarNomina=$this->_db->prepare("
                  INSERT INTO
                    nm_e002_prestaciones_sociales_calculo
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), num_estatus=:num_estatus,
                    fk_nmb001_num_tipo_nomina=:fk_nmb001_num_tipo_nomina, fec_anio=:fec_anio, fec_mes=:fec_mes, num_sueldo_base=:num_sueldo_base,
                    num_total_asignaciones=:num_total_asignaciones, num_sueldo_normal=:num_sueldo_normal, num_alicuota_vacacional=:num_alicuota_vacacional,
                    num_alicuota_fin_anio=:num_alicuota_fin_anio, num_bono_especial=:num_bono_especial, num_bono_fiscal=:num_bono_fiscal,
                    num_dias_trimestre=:num_dias_trimestre, num_dias_anual=:num_dias_anual, num_remuneracion_mensual=:num_remuneracion_mensual,
                    num_remuneracion_diaria=:num_remuneracion_diaria, num_monto_trimestral=:num_monto_trimestral, num_monto_anual=:num_monto_anual,
                    num_monto_acumulado=:num_monto_acumulado, fk_rhb001_num_empleado=:fk_rhb001_num_empleado
        ");
        for ($i=0;$i<count($form['fk_rhb001_num_empleado']);$i++) {
            if($form['num_dias_anual'][$i]=='NULL'){
                $form['num_dias_anual'][$i]=null;
            }
            $registrarNomina->execute(array(
                'num_estatus'=>1,
                'fk_nmb001_num_tipo_nomina'=>$form['fk_nmb001_num_tipo_nomina'][$i],
                'fec_anio'=>$form['fec_anio'][$i],
                'fec_mes'=>$form['fec_mes'][$i],
                'num_sueldo_base'=>$form['num_sueldo_base'][$i],
                'num_total_asignaciones'=>$form['num_total_asignaciones'][$i],
                'num_sueldo_normal'=>$form['num_sueldo_normal'][$i],
                'num_alicuota_vacacional'=>$form['num_alicuota_vacacional'][$i],
                'num_alicuota_fin_anio'=>$form['num_alicuota_fin_anio'][$i],
                'num_bono_especial'=>$form['num_bono_especial'][$i],
                'num_bono_fiscal'=>$form['num_bono_fiscal'][$i],
                'num_dias_trimestre'=>$form['num_dias_trimestre'][$i],
                'num_dias_anual'=>$form['num_dias_anual'][$i],
                'num_remuneracion_mensual'=>$form['num_remuneracion_mensual'][$i],
                'num_remuneracion_diaria'=>$form['num_remuneracion_diaria'][$i],
                'num_monto_trimestral'=>$form['num_monto_trimestral'][$i],
                'num_monto_anual'=>$form['num_monto_anual'][$i],
                'num_monto_acumulado'=>$form['num_monto_acumulado'][$i],
                'fk_rhb001_num_empleado'=>$form['fk_rhb001_num_empleado'][$i]
            ));
        }
        $error = $registrarNomina->errorInfo();
        if(!empty($error[1]) && !empty($error[2]) ){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $form['fk_rhb001_num_empleado'][0];
        }
    }

}
