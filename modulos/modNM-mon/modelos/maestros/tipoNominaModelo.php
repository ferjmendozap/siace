<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Daniel Muñoz                     |d.munoz@contraloriamonagas.gob.ve   |         0412-8358676           |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        03-08-2015       |         1.0        |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/

require_once 'tipoProcesoModelo.php';

class tipoNominaModelo extends tipoProcesoModelo
{
    public $atDocumento;

    public function __construct() 
    {
        parent::__construct();
    }

    public function metMostrarTipoNomina($idTipoNomina)
    {
        $tipoNomina = $this->_db->query("
          SELECT
            nm_b001.*,
            a018.ind_usuario
          FROM
            nm_b001_tipo_nomina AS nm_b001
          INNER JOIN
            a018_seguridad_usuario AS a018 ON a018.pk_num_seguridad_usuario = nm_b001.fk_a018_num_seguridad_usuario
          WHERE
            nm_b001.pk_num_tipo_nomina='$idTipoNomina'
        ");
        $tipoNomina->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoNomina->fetch();
    }

    public function metMostrarDetTipoNomina($idTipoNomina)
    {
        $det = $this->_db->query("
          SELECT
            *
          FROM
            nm_d004_proceso_tipo_nomina AS nm_d004
          INNER JOIN
            nm_b003_tipo_proceso AS nm_b003 ON nm_b003.pk_num_tipo_proceso = nm_d004.fk_nmb003_num_tipo_proceso
          INNER JOIN
            cp_b002_tipo_documento AS cp_b002 ON cp_b002.pk_num_tipo_documento = nm_d004.fk_000_num_tipo_documento
          WHERE
            nm_d004.fk_nmb001_num_tipo_nomina = '$idTipoNomina'
        ");
        $det->setFetchMode(PDO::FETCH_ASSOC);
        return $det->fetchAll();
    }

    public function metListarTipoNomina($status=false)
    {
        if($status){
            $status="WHERE num_estatus = '1'";
        }
        $tipoNomina = $this->_db->query(
            "SELECT * FROM nm_b001_tipo_nomina $status"
        );
        $tipoNomina->setFetchMode(PDO::FETCH_ASSOC);
        return $tipoNomina->fetchAll();
    }

    public function metCrearTipoNomina($codNomina,$nombre,$titulo,$flagPagoMensual=0,$status=0,$arrayProceso=false,$ArrayDocumento=false)
    {
        $this->_db->beginTransaction();
        $registrarNomina=$this->_db->prepare("
                  INSERT INTO
                    nm_b001_tipo_nomina
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), num_estatus=:num_estatus,
                    cod_tipo_nomina=:cod_tipo_nomina, ind_nombre_nomina=:ind_nombre_nomina, ind_titulo_boleta=:ind_titulo_boleta,
                    num_flag_pago_mensual=:num_flag_pago_mensual
        ");
        $registrarNomina->execute(array(
            'num_estatus'=>$status,
            'cod_tipo_nomina'=>$codNomina,
            'ind_nombre_nomina'=>$nombre,
            'ind_titulo_boleta'=>$titulo,
            'num_flag_pago_mensual'=>$flagPagoMensual,
        ));
        $idRegistro= $this->_db->lastInsertId();
        if($arrayProceso && $ArrayDocumento) {
            $unirNominaConProceso = $this->_db->prepare("
                  INSERT INTO
                    nm_d004_proceso_tipo_nomina
                  SET
                    fk_000_num_tipo_documento=:fk_000_num_tipo_documento, fk_nmb001_num_tipo_nomina='$idRegistro',
                    fk_nmb003_num_tipo_proceso=:fk_nmb003_num_tipo_proceso
            ");
            for($i=0;$i<count($arrayProceso);$i++) {
                $unirNominaConProceso->execute(array(
                    'fk_000_num_tipo_documento' => $ArrayDocumento[$i],
                    'fk_nmb003_num_tipo_proceso' => $arrayProceso[$i]
                ));
            }

        }
        $error = $registrarNomina->errorInfo();
        if(!isset($unirNominaConProceso)){
            $error2=$unirNominaConProceso->errorInfo();
        }else{
            $error2=array(1=>null,2=>null);
        }

        if(!empty($error[1]) && !empty($error[2]) && !empty($error2[1]) && !empty($error2[2])){
            $this->_db->rollBack();
            $error=array_merge($error,$error2);
            return $error;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metModificarTipoNomina($codNomina,$nombre,$titulo,$flagPagoMensual=0,$status=0,$arrayProceso=false,$ArrayDocumento=false,$idNomina)
    {
        $this->_db->beginTransaction();
        $registrarNomina=$this->_db->prepare("
                  UPDATE
                    nm_b001_tipo_nomina
                  SET
                    fk_a018_num_seguridad_usuario='$this->atIdUsuario', fec_ultima_modificacion=NOW(), num_estatus=:num_estatus,
                    cod_tipo_nomina=:cod_tipo_nomina, ind_nombre_nomina=:ind_nombre_nomina, ind_titulo_boleta=:ind_titulo_boleta,
                    num_flag_pago_mensual=:num_flag_pago_mensual
                  WHERE
                    pk_num_tipo_nomina='$idNomina'
        ");
        $registrarNomina->execute(array(
            'num_estatus'=>$status,
            'cod_tipo_nomina'=>$codNomina,
            'ind_nombre_nomina'=>$nombre,
            'ind_titulo_boleta'=>$titulo,
            'num_flag_pago_mensual'=>$flagPagoMensual,
        ));

        $this->_db->query("DELETE FROM nm_d004_proceso_tipo_nomina WHERE fk_nmb001_num_tipo_nomina='$idNomina'");

        if($arrayProceso && $ArrayDocumento) {
            $unirNominaConProceso = $this->_db->prepare("
                  INSERT INTO
                    nm_d004_proceso_tipo_nomina
                  SET
                    fk_000_num_tipo_documento=:fk_000_num_tipo_documento, fk_nmb001_num_tipo_nomina='$idNomina',
                    fk_nmb003_num_tipo_proceso=:fk_nmb003_num_tipo_proceso
            ");
            for($i=0;$i<count($arrayProceso);$i++) {
                $unirNominaConProceso->execute(array(
                    'fk_000_num_tipo_documento' => $ArrayDocumento[$i],
                    'fk_nmb003_num_tipo_proceso' => $arrayProceso[$i]
                ));
            }

        }
        $error = $registrarNomina->errorInfo();
        if(!isset($unirNominaConProceso)){
            $error2=$unirNominaConProceso->errorInfo();
        }else{
            $error2=array(1=>null,2=>null);
        }

        if(!empty($error[1]) && !empty($error[2]) && !empty($error2[1]) && !empty($error2[2])){
            $this->_db->rollBack();
            $error=array_merge($error,$error2);
            return $error;
        }else{
            $this->_db->commit();
            return $idNomina;
        }
    }

    public function metEliminarTipoNomina($idNomina)
    {
        $this->_db->beginTransaction();
        $elimar=$this->_db->prepare("
                DELETE FROM nm_b001_tipo_nomina WHERE pk_num_tipo_nomina=:pk_num_tipo_nomina
            ");
        $elimar->execute(array(
            'pk_num_tipo_nomina'=>$idNomina
        ));
        $error=$elimar->errorInfo();

        if(!empty($error[1]) && !empty($error[2])){
            $this->_db->rollBack();
            return $error;
        }else{
            $this->_db->commit();
            return $idNomina;
        }

    }
}
