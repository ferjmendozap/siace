<form action="{$_Parametros.url}modNM/maestros/tasaInteresesCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idTasa) }
            <input type="hidden" value="{$idTasa}" name="idTasa"/>
        {/if}
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group control-width-normal" id="fec_gacetaError">
                    <div class="input-group date" id="fec_gaceta">
                        <div class="input-group-content">
                            <input {if $ver==1}disabled{/if} type="text" id="fec_gaceta" class="form-control" name="form[txt][fec_gaceta]" value="{if isset($formDB.fec_gaceta)}{$formDB.fec_gaceta}{/if}">
                            <label>Fecha de Gaceta</label>
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="num_gacetaError">
                    <label for="num_gaceta"><i class="fa fa-code"></i>N&ordm; Gaceta <b style="color: red;font-weight: bold;">*</b></label>
                    <input type="text" class="form-control" value="{if isset($formDB.num_gaceta)}{$formDB.num_gaceta}{/if}" name="form[txt][num_gaceta]" id="num_gaceta" {if isset($ap) || isset($ver) }disabled{/if}>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="num_prom_act_pasivError">
                    <label for="num_prom_act_pasiv"><i class="fa fa-code"></i>Promedio entre activo y pasivo <b style="color: red;font-weight: bold;">*</b></label>
                    <input type="text" class="form-control" value="{if isset($formDB.num_prom_act_pasiv)}{$formDB.num_prom_act_pasiv}{/if}" name="form[int][num_prom_act_pasiv]" id="num_prom_act_pasiv" {if isset($ap) || isset($ver) }disabled{/if}>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="num_activaError">
                    <label for="num_activa"><i class="fa fa-code"></i>Promedio Activo <b style="color: red;font-weight: bold;">*</b></label>
                    <input type="text" class="form-control" value="{if isset($formDB.num_activa)}{$formDB.num_activa}{/if}" name="form[int][num_activa]" id="num_activa" {if isset($ap) || isset($ver) }disabled{/if}>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group control-width-normal" id="periodoError">
                    <div class="input-group date" id="periodo">
                        <div class="input-group-content">
                            <input {if $ver==1}disabled{/if} type="text" id="periodo" class="form-control" name="form[txt][periodo]" value="{if isset($formDB.periodo)}{$formDB.periodo}{/if}">
                            <label>Periodo</label>
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="checkbox checkbox-styled">
                    <label>
                        <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                        <span>estatus </span>
                    </label>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new AppFunciones();

        $('#fec_gaceta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
        $('#periodo').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm", language:'es' });
        function validarError(dato,mensaje){
            for (var item in dato) {
                if(dato[item]=='error'){
                    $(document.getElementById(item+'Error')).removeClass('has-success has-feedback');
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).append('<span id="'+item+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    swal("Error!", mensaje, "error");
                }else{
                    $(document.getElementById(item+'Error')).removeClass('has-error has-feedback');
                    $(document.getElementById(item+'Error')).addClass('has-success has-feedback');
                    $(document.getElementById(item+'Icono')).removeClass('glyphicon glyphicon-remove form-control-feedback');
                    $(document.getElementById(item+'Icono')).addClass('glyphicon glyphicon-ok form-control-feedback');
                }
            }
        }
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css("width","35%");
        $('#accion').click(function(){
            /*swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });*/
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    validarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    validarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson','"El Proceso fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson','El Proceso fue modificado satisfactoriament.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>