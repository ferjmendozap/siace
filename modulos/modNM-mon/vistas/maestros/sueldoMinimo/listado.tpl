<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">
            {if $estatus == 'PR'}
                Sueldo Minimo - Aprobar
            {else}
                Sueldo Minimo - Listado
            {/if}
        </h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Periodo</th>
                            <th>Numero Resolucion</th>
                            <th>Numero Gaceta</th>
                            <th>Monto</th>
                            <th>Estatus</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        {if $estatus != 'PR'}
                            <tfoot>
                            <tr>
                                <th colspan="6">
                                    {if in_array('NM-01-03-06-01-N',$_Parametros.perfil)}
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info"
                                                data-toggle="modal" data-target="#formModal" data-keyboard="false"
                                                data-backdrop="static"
                                                descipcion="el Usuario a Registrado un Sueldo Minimo Nuevo"
                                                titulo="<i class='icm icm-coins'></i> Crear Sueldo Minimo" id="nuevo">
                                            <i class="md md-create"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nuevo Sueldo Minimo
                                        </button>
                                    {/if}
                                </th>
                            </tr>
                            </tfoot>
                        {/if}
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {
        var url = '{$_Parametros.url}modNM/maestros/sueldoMinimoCONTROL/crearModificarMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modNM/maestros/sueldoMinimoCONTROL/jsonDataTablaMET/{$estatus}",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "ind_periodo" },
                    { "data": "ind_num_resolucion" },
                    { "data": "ind_num_gaceta" },
                    { "data": "num_sueldo_minimo" },
                    { "data": "ind_estado" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );
        $('#nuevo').click(function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idSueldoMinimo: 0 }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on('click', '.aprobar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post('{$_Parametros.url}modNM/maestros/sueldoMinimoCONTROL/aprobarMET', { idSueldoMinimo: $(this).attr('idSueldoMinimo') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on('click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url, { idSueldoMinimo: $(this).attr('idSueldoMinimo') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

        $('#dataTablaJson tbody').on('click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post('{$_Parametros.url}modNM/maestros/sueldoMinimoCONTROL/verMET', { idSueldoMinimo: $(this).attr('idSueldoMinimo') }, function (dato) {
                $('#ContenidoModal').html(dato);
            });
        });

    });
</script>