<form action="{$_Parametros.url}modNM/maestros/sueldoMinimoCONTROL/{if isset($ap) || isset($ver) }aprobarMET{else}crearModificarMET{/if}" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idSueldoMinimo) }
            <input type="hidden" value="{$idSueldoMinimo}" name="idSueldoMinimo" id="idSueldoMinimo"/>
        {/if}
        <div class="row">
            <div class="col-sm-5">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_num_resolucionError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_num_resolucion)}{$formDB.ind_num_resolucion}{/if}" name="form[alphaNum][ind_num_resolucion]" id="ind_num_resolucion" {if isset($ap) || isset($ver) }disabled{/if}>
                        <label for="ind_num_resolucion"><i class="fa fa-code"></i> N&ordm; Resolucion <b style="color: red;font-weight: bold;">*</b></label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_num_gacetaError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_num_gaceta)}{$formDB.ind_num_gaceta}{/if}" name="form[alphaNum][ind_num_gaceta]" id="ind_num_gaceta" {if isset($ap) || isset($ver) }disabled{/if}>
                        <label for="ind_num_gaceta"><i class="fa fa-code"></i>N&ordm; Gaceta <b style="color: red;font-weight: bold;">*</b></label>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_periodoError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_periodo)}{$formDB.ind_periodo}{/if}" name="form[txt][ind_periodo]" id="ind_periodo" {if isset($ap) || isset($ver) }disabled{/if}>
                        <label for="ind_periodo"><i class="fa fa-calendar"></i> Periodo <b style="color: red;font-weight: bold;">*</b></label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="ind_estadoError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_estado)}{$formDB.ind_estado}{/if}" disabled name="form[alphaNum][ind_estado]" id="ind_estado">
                        <label for="ind_estado"><i class="fa fa-eye"></i> Estado:</label>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="form-group floating-label" id="num_sueldo_minimoError">
                        <input type="text" class="form-control" value="{if isset($formDB.num_sueldo_minimo)}{$formDB.num_sueldo_minimo}{/if}" name="form[int][num_sueldo_minimo]" id="num_sueldo_minimo" {if isset($ap) || isset($ver) }disabled{/if}>
                        <label for="num_sueldo_minimo"><i class="icm icm-coins"></i> Monto del Sueldo Minimo <b style="color: red;font-weight: bold;">*</b></label>
                    </div>
                </div>
            </div>
            <div class="col-sm-7">
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="fk_rhb001_num_empleado_creaError">
                        <input type="text" class="form-control" value="{if isset($formDB.EMPLEADO_PREPARADO)}{$formDB.EMPLEADO_PREPARADO}{/if}" name="form[int][fk_rhb001_num_empleado_crea]" disabled id="fk_rhb001_num_empleado_crea">
                        <label for="fk_rhb001_num_empleado_crea"><i class="md md-person"></i> Creado Por</label>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="fec_preparadoError">
                        <input type="text" class="form-control" value="{if isset($formDB.fec_preparado)}{$formDB.fec_preparado}{/if}" disabled name="form[alphaNum][fec_preparado]" id="fec_preparado">
                        <label for="fec_preparado"><i class="md md-today"></i> Fecha de Creacion</label>
                    </div>
                </div>
                {if isset($formDB.ind_motivo_anulado) }
                        <div class="col-sm-6">
                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                                <input type="text" class="form-control" value="{if isset($formDB.EMPLEADO_ANULADO)}{$formDB.EMPLEADO_ANULADO}{/if}" disabled id="fk_rhb001_num_empleado_aprueba">
                                <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Anulado Por</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group floating-label" id="fec_preparadoError">
                                <input type="text" class="form-control" value="{if isset($formDB.fec_anulado)}{$formDB.fec_anulado}{/if}" disabled  id="fec_preparado">
                                <label for="fec_preparado"><i class="md md-today"></i> Fecha de Anulado</label>
                            </div>
                        </div>
                    {else}
                        <div class="col-sm-6">
                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                                <input type="text" class="form-control" value="{if isset($formDB.EMPLEADO_APROBADO)}{$formDB.EMPLEADO_APROBADO}{/if}" name="form[int][fk_rhb001_num_empleado_aprueba]" disabled id="fk_rhb001_num_empleado_aprueba">
                                <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Aprobado Por</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group floating-label" id="fec_preparadoError">
                                <input type="text" class="form-control" value="{if isset($formDB.fec_aprobado)}{$formDB.fec_aprobado}{/if}" disabled name="form[alphaNum][fec_preparado]" id="fec_preparado">
                                <label for="fec_preparado"><i class="md md-today"></i> Fecha de Aprobacion</label>
                            </div>
                        </div>
                {/if}
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_descripcionError">
                        <textarea id="ind_descripcion" name="form[alphaNum][ind_descripcion]" class="form-control" rows="3" placeholder="" {if isset($ap) || isset($ver) }disabled{/if}>{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}</textarea>
                        <label for="ind_descripcion"><i class="md md-comment"></i> Descripcion del Aumento del Sueldo Minimo <b style="color: red;font-weight: bold;">*</b></label>
                    </div>
                </div>
                {if isset($ver)}
                    {if isset($formDB.ind_motivo_anulado) }
                        <div class="col-sm-12">
                            <div class="form-group floating-label" id="ind_motivo_anuladoError">
                                <textarea id="ind_motivo_anulado" name="form[alphaNum][ind_motivo_anulado]" class="form-control" rows="3" placeholder="" {if isset($formDB.ind_motivo_anulado)}disabled{/if}>{if isset($formDB.ind_motivo_anulado)}{$formDB.ind_motivo_anulado}{/if}</textarea>
                                <label for="ind_motivo_anulado"><i class="md md-comment"></i> Motivo de la Anulacion del Aumento del Sueldo Minimo</label>
                            </div>
                        </div>
                    {/if}
                {else}
                    {if isset($ap) || isset($formDB.ind_motivo_anulado) }
                        <div class="col-sm-12">
                            <div class="form-group floating-label" id="ind_motivo_anuladoError">
                                <textarea id="ind_motivo_anulado" name="form[alphaNum][ind_motivo_anulado]" class="form-control" rows="3" placeholder="" {if isset($formDB.ind_motivo_anulado)}disabled{/if}>{if isset($formDB.ind_motivo_anulado)}{$formDB.ind_motivo_anulado}{/if}</textarea>
                                <label for="ind_motivo_anulado"><i class="md md-comment"></i> Motivo de la Anulacion del Aumento del Sueldo Minimo</label>
                            </div>
                        </div>
                    {/if}
                {/if}
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    {if isset($ver)}

    {else}
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if isset($ap)}
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="anular"><i class="icm icm-blocked"></i>&nbsp;Anular</button>
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="aprobar"><i class="icm icm-rating3"></i>&nbsp;Aprobar</button>
            {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
        {/if}
    </div>
    {/if}
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css("width","70%");
        $('#anular').click(function(){
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='anular'){
                    $(document.getElementById('idSueldoMinimo'+dato['idSueldoMinimo'])).remove();
                    swal("Anulado!", "EL Suedo Minimo fue Anulado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
        $('#aprobar').click(function(){
            $.post($("#formAjax").attr("action"), { idSueldoMinimoForm: $('#idSueldoMinimo').val(),valido:1 },function(dato){
                if(dato['status']=='aprobar'){
                    $(document.getElementById('idSueldoMinimo'+dato['idSueldoMinimo'])).remove();
                    swal("Aprobado!", "EL Suedo Minimo fue Aprobado Satisfactoriamente", "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');
                }
            },'json');
        });
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = false;
                var arrayMostrarOrden = ['ind_periodo','ind_num_resolucion','ind_num_gaceta','num_sueldo_minimo','ind_estado'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson','La Nomina fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                } else if (dato['status'] == 'nuevo') {
                    app.metNuevoRegistroTablaJson('dataTablaJson','La Nomina fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>