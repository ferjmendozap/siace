<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Tipo de Nomina - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Check</th>
                            <th>Cod</th>
                            <th>Nomina</th>
                            <th>Pago Mensual</th>
                            <th>Estatus</th>
                        </tr>
                        </thead>
                        <tbody>
                        {foreach item=nomina from=$listado}
                            <tr id="idNomina{$nomina.pk_num_tipo_nomina}">
                                <td>
                                    <div class="checkbox checkbox-styled">
                                        <label>
                                            <input type="checkbox" class="valores" idNomina="{$nomina.pk_num_tipo_nomina}" nomina="{$nomina.ind_nombre_nomina}" cod="{$nomina.cod_tipo_nomina}">
                                        </label>
                                    </div>
                                </td>
                                <td><label>{$nomina.cod_tipo_nomina}</label></td>
                                <td><label>{$nomina.ind_nombre_nomina}</label></td>
                                <td><i class="{if $nomina.num_flag_pago_mensual==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                <td>
                                    <i class="{if $nomina.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<span class="clearfix"></span>
<div class="modal-footer">
    <button type="button" class="btn btn-default ink-reaction btn-raised"  data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-primary ink-reaction btn-raised" id="agregarNominaSeleccionada">Agregar</button>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#agregarNominaSeleccionada').click(function(){
            var input = $('.valores');
            for(i=0;i<input.length;i++){
                if(input[i].checked == true ){
                    $(document.getElementsByClassName('idNomina'+input[i].getAttribute('idNomina'))).remove();
                    $(document.getElementById('tipoNomina')).append(
                            '<tr class="idNomina'+input[i].getAttribute('idNomina')+'">'+
                            '<input type="hidden" value="'+input[i].getAttribute('idNomina')+'" class="tipoNominaInput" name="form[int][fk_nmb001_num_tipo_nomina][]" nomina="'+input[i].getAttribute('nomina')+'" />'+
                            '<td>'+input[i].getAttribute('cod')+'</td>'+
                            '<td>'+input[i].getAttribute('nomina')+'</td>'+
                            '<td><button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idNomina'+input[i].getAttribute('idNomina')+'"><i class="md md-delete" style="color: #ffffff;"></i></button></td>'+
                            '</tr>'
                    );
                }
            }
            $('#ContenidoModal2').html('');
            $('#cerrarModal2').click();
        });
    });
</script>