<form action="{$_Parametros.url}modNM/maestros/tipoNominaCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idNomina) }
            <input type="hidden" value="{$idNomina}" name="idNomina"/>
        {/if}
        <div class="row">
            <div class="col-sm-4">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="cod_tipo_nominaError">
                        <input type="text" class="form-control" value="{if isset($formDB.cod_tipo_nomina)}{$formDB.cod_tipo_nomina}{/if}" name="form[alphaNum][cod_tipo_nomina]" id="cod_tipo_nomina">
                        <label for="cod_tipo_nomina"><i class="fa fa-code"></i> Cod Nomina</label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_nombre_nominaError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_nombre_nomina)}{$formDB.ind_nombre_nomina}{/if}" name="form[alphaNum][ind_nombre_nomina]" id="ind_nombre_nomina">
                        <label for="ind_nombre_nomina"><i class="fa fa-list-alt"></i> Nombre de la Nomina</label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="ind_titulo_boletaError">
                        <input type="text" class="form-control" value="{if isset($formDB.ind_titulo_boleta)}{$formDB.ind_titulo_boleta}{/if}" name="form[alphaNum][ind_titulo_boleta]" id="ind_titulo_boleta">
                        <label for="ind_titulo_boleta"><i class="md md-insert-comment"></i> Titulo de la Boleta</label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_flag_pago_mensual) and $formDB.num_flag_pago_mensual==1} checked{/if} value="1" name="form[int][num_flag_pago_mensual]">
                            <span>Pago Mensual </span>
                        </label>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="checkbox checkbox-styled">
                        <label>
                            <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if} value="1" name="form[int][num_estatus]">
                            <span>estatus </span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="col-lg-12">
                    <div class="table-responsive no-margin">
                        <table class="table table-striped no-margin" id="contenidoTabla">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 10%;">#</th>
                                    <th style="width: 50%;">Proceso</th>
                                    <th style="width: 40%;">Tipo de Documento</th>
                                </tr>
                            </thead>
                            <tbody>
                            {if isset($formDBDet)}
                                {foreach item=det from=$formDBDet}
                                    <tr>
                                        <td class="text-right" style="vertical-align: middle;">
                                            {$numero++}
                                        </td>
                                        <td style="vertical-align: middle;">
                                            <div class="col-sm-12">
                                                <div class="form-group" id="pk_num_tipo_proceso{$n}Error">
                                                    <select name="form[int][pk_num_tipo_proceso][{$n}]" class="form-control select2-list select2" data-placeholder="Seleccione el Proceso">'+
                                                        <option value="">Seleccione</option>
                                                        {foreach item=proceso from=$listadoProceso}
                                                            {if $det.pk_num_tipo_proceso==$proceso.pk_num_tipo_proceso}
                                                                <option selected value="{$proceso.pk_num_tipo_proceso}">{$proceso.ind_nombre_proceso}</option>
                                                                {else}
                                                                <option value="{$proceso.pk_num_tipo_proceso}">{$proceso.ind_nombre_proceso}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </div>
                                        </td>
                                        <td  class="text-center" style="vertical-align: middle;">
                                            <div class="col-sm-12">
                                                <div class="form-group" id="pk_num_tipo_documento{$n}Error">
                                                    <select name="form[int][pk_num_tipo_documento][{$n++}]" class="form-control select2-list select2" data-placeholder="Seleccione el Documento">'+
                                                        <option value="">Seleccione</option>
                                                        {foreach item=documento from=$listadoDocumentos}
                                                            {if $det.pk_num_tipo_documento==$documento.pk_num_tipo_documento}
                                                                <option selected value="{$documento.pk_num_tipo_documento}">{$documento.ind_descripcion}</option>
                                                                {else}
                                                                <option value="{$documento.pk_num_tipo_documento}">{$documento.ind_descripcion}</option>
                                                            {/if}
                                                        {/foreach}
                                                    </select>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                {/foreach}
                            {/if}
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="4" align="center">
                                    <button type="button" id="nuevoCampo" class="btn btn-info ink-reaction btn-raised">
                                        <i class="md md-add"></i> Nuevo Proceso
                                    </button>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="cod_tipo_nominaError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="cod_tipo_nomina"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('.select2').select2({ allowClear: true });
        $('#modalAncho').css("width","80%");
        $("#nuevoCampo").click(function() {
            var idtabla= $("#contenidoTabla");
            var tr= $("#contenidoTabla > tbody > tr");
            var numTr=tr.length - 1;
            var nuevoTr=numTr+1;

            var numero=nuevoTr+1;
            var tipoProceso='<div class="form-group" id="pk_num_tipo_proceso'+nuevoTr+'Error">'+
                                '<select name="form[int][pk_num_tipo_proceso]['+nuevoTr+']" class="form-control" data-placeholder="Seleccione el Proceso">'+
                                '<option value="">Seleccione</option>'+
                                '{foreach item=proceso from=$listadoProceso}'+
                                '<option value="{$proceso.pk_num_tipo_proceso}">{$proceso.ind_nombre_proceso}</option>'+
                                '{/foreach}'+
                                '</select>'+
                            '</div>';
            var tipoDocumento='<div class="form-group" id="pk_num_tipo_documento'+nuevoTr+'Error">'+
                                        '<select name="form[int][pk_num_tipo_documento]['+nuevoTr+']" class="form-control" data-placeholder="Seleccione el Documento">'+
                                        '<option value="">Seleccione</option>'+
                                        '{foreach item=documento from=$listadoDocumentos}'+
                                        '<option value="{$documento.pk_num_tipo_documento}">{$documento.ind_descripcion}</option>'+
                                        '{/foreach}'+
                                        '</select>'+
                                '</div>';

            idtabla.append('<tr>'+
                    '<td class="text-right" style="vertical-align: middle;">'+numero+'</td>'+
                    '<td style="vertical-align: middle;"><div class="col-sm-12">'+tipoProceso+'</div></td>'+
                    '<td  class="text-center" style="vertical-align: middle;"><div class="col-sm-12">'+tipoDocumento+'</div></td>'+
                    '</tr>');

        });
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ["num_estatus","num_flag_pago_mensual"];
                var arrayMostrarOrden = ['cod_tipo_nomina','ind_nombre_nomina','num_flag_pago_mensual','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTablaJson('dataTablaJson','El Tipo de Nomina fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTablaJson('dataTablaJson','El Tipo de Nomina fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>