<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                <form action="{$_Parametros.url}modNM/maestros/conceptoCONTROL/crearModificarMET" id="formAjax"
                      class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                    <input type="hidden" value="1" name="valido"/>
                    <input type="hidden" value="{$idConcepto}" name="idConcepto"/>

                    <div class="form-wizard-nav">
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary"></div>
                        </div>
                        <ul class="nav nav-justified">
                            <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span
                                            class="title">Formula </span></a></li>
                            <li><a href="#step2" data-toggle="tab" id="tabFormula"><span class="step">2</span> <span
                                            class="title">Informaci&oacute;n General</span></a></li>
                            <li><a href="#step3" data-toggle="tab"><span class="step">3</span> <span class="title">Informaci&oacute;n Contable</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content clearfix">
                        <div class="tab-pane active" id="step1">
                            <div class="row">
                                <div class="col-sm-9">
                                    <div class="card">
                                        <div class="card-head card-head-xs style-primary">
                                            <header>Formula</header>
                                        </div>
                                        <div class="card-body" style="padding: 4px;">
                                            <textarea id="ind_formula" name="form[formula][ind_formula]" {if isset($ver)}readonly{/if}>{if isset($formDB.ind_formula)}{$formDB.ind_formula}{else}//aqui tu Codigo{/if}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="card">
                                        <div class="card-head card-head-xs style-primary">
                                            <header>Teclado Numerico</header>
                                        </div>
                                        <div class="card-body" style="padding: 4px;" id="accionesNM">
                                            <div class="col-sm-12">
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="7">7
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="8">8
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="9">9
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="+">+
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="4">4
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="5">5
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="6">6
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="-">-
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="1">1
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="2">2
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="3">3
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="*">*
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="=">=
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="0">0
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role=".">.
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="/">/
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="(">(
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role=")">)
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="$">$
                                                    </button>
                                                </div>
                                                <div class="col-sm-3" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role=";">;
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="col-sm-6" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="$this->atMonto = ">MONTO
                                                    </button>
                                                </div>
                                                <div class="col-sm-6" style="padding: 5px;">
                                                    <button type="button"
                                                            class="btn btn-xs btn-primary btn-block ink-reaction accionesNM"
                                                            role="$this->atCantidad = ">CANTIDAD
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="step2">
                            <div class="row">
                                <div class="col-sm-offset-1 col-sm-10">
                                    <div class="card">
                                        <div class="card-head card-head-xs style-primary">
                                            <header>Datos del Concepto</header>
                                        </div>
                                        <div class="card-body" style="padding: 4px;">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-sm-6">
                                                        <div class="form-group floating-label" id="cod_conceptoError">
                                                            <input type="text" class="form-control"
                                                                   value="{if isset($formDB.cod_concepto)}{$formDB.cod_concepto}{/if}"
                                                                   name="form[int][cod_concepto]" id="cod_tipo_nomina"
                                                                   disabled readonly>
                                                            <label for="cod_concepto"><i class="icm icm-calculate2"></i>
                                                                Concepto N&ring;</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group floating-label"
                                                             id="ind_abrebiaturaError">
                                                            <input type="text" class="form-control"
                                                                   value="{if isset($formDB.ind_abrebiatura)}{$formDB.ind_abrebiatura}{/if}"
                                                                   name="form[alphaNum][ind_abrebiatura]" id="ind_abrebiatura"
                                                                   required>
                                                            <label for="ind_abrebiatura"><i
                                                                        class="icm icm-calculate2"></i>
                                                                Abreviatura</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="col-sm-6">
                                                        <div class="form-group" id="fk_a006_num_tipo_conceptoError">
                                                            <label for="fk_a006_num_tipo_concepto"><i
                                                                        class="md md-map"></i> Tipo Concepto</label>
                                                            <select id="fk_a006_num_tipo_concepto" required  {if isset($ver)}disabled{/if}
                                                                    name="form[int][fk_a006_num_tipo_concepto]"
                                                                    class="form-control select2-list select2">
                                                                <option value="">Seleccione</option>
                                                                {if isset($tipoConcepto)}
                                                                    {foreach item=i from=$tipoConcepto}
                                                                        {if isset($formDB.fk_a006_num_tipo_concepto)}
                                                                            {if $i.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_tipo_concepto}
                                                                                <option selected
                                                                                        value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                            {else}
                                                                                <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                            {/if}
                                                                        {else}
                                                                            <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                {/if}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group" id="num_orden_planillaError">
                                                            <label for="num_orden_planilla"><i class="md md-map"></i>
                                                                Orden en Boleta</label>
                                                            <select id="num_orden_planilla" required  {if isset($ver)}disabled{/if}
                                                                    name="form[int][num_orden_planilla]"
                                                                    class="form-control select2-list select2">
                                                                <option value="">Seleccione</option>
                                                                {if isset($formDB.num_orden_planilla)}
                                                                        {if $formDB.num_orden_planilla == 1} <option value="1" selected>N&ring; 1</option>{else} <option value="1">N&ring; 1</option>{/if}
                                                                        {if $formDB.num_orden_planilla == 2} <option value="2" selected>N&ring; 2</option>{else} <option value="2">N&ring; 2</option>{/if}
                                                                        {if $formDB.num_orden_planilla == 3} <option value="3" selected>N&ring; 3</option>{else} <option value="3">N&ring; 3</option>{/if}
                                                                        {if $formDB.num_orden_planilla == 4} <option value="4" selected>N&ring; 4</option>{else} <option value="4">N&ring; 4</option>{/if}
                                                                        {if $formDB.num_orden_planilla == 5} <option value="5" selected>N&ring; 5</option>{else} <option value="5">N&ring; 5</option>{/if}
                                                                        {if $formDB.num_orden_planilla == 6} <option value="6" selected>N&ring; 6</option>{else} <option value="6">N&ring; 6</option>{/if}
                                                                    {else}
                                                                        <option value="1">N&ring; 1</option>
                                                                        <option value="2">N&ring; 2</option>
                                                                        <option value="3">N&ring; 3</option>
                                                                        <option value="4">N&ring; 4</option>
                                                                        <option value="5">N&ring; 5</option>
                                                                        <option value="6">N&ring; 6</option>
                                                                {/if}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-sm-12">
                                                        <div class="form-group floating-label"
                                                             id="ind_descripcionError">
                                                            <input type="text" class="form-control" {if isset($ver)}readonly{/if}
                                                                   value="{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}"
                                                                   name="form[alphaNum][ind_descripcion]"
                                                                   id="ind_descripcion" required>
                                                            <label for="ind_descripcion"><i
                                                                        class="icm icm-calculate2"></i> Nombre del
                                                                Concepto</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="col-sm-12">
                                                        <div class="form-group floating-label" id="ind_impresionError">
                                                            <input type="text" class="form-control" {if isset($ver)}readonly{/if}
                                                                   value="{if isset($formDB.ind_impresion)}{$formDB.ind_impresion}{/if}"
                                                                   name="form[alphaNum][ind_impresion]"
                                                                   id="ind_impresion" required>
                                                            <label for="ind_impresion"><i
                                                                        class="icm icm-calculate2"></i> Titulo de la
                                                                Boleta</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-sm-6">
                                                        <div class="checkbox checkbox-styled">
                                                            <label>
                                                                <input type="checkbox" {if isset($formDB.num_flag_automatico) and $formDB.num_flag_automatico==1} checked{/if}
                                                                       value="1" name="form[int][num_flag_automatico]" {if isset($ver)}disabled{/if}>
                                                                <span>Automatico</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="checkbox checkbox-styled">
                                                            <label>
                                                                <input type="checkbox" {if isset($formDB.num_flag_bono) and $formDB.num_flag_bono==1} checked{/if}
                                                                       value="1" name="form[int][num_flag_bono]" {if isset($ver)}disabled{/if}>
                                                                <span>Bonificacion</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="col-sm-6">
                                                        <div class="checkbox checkbox-styled">
                                                            <label>
                                                                <input type="checkbox" {if isset($formDB.num_flag_incidencia) and $formDB.num_flag_incidencia==1} checked{/if}
                                                                       value="1" name="form[int][num_flag_incidencia]" {if isset($ver)}disabled{/if}>
                                                                <span>Incidencia Salarial</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="checkbox checkbox-styled">
                                                            <label>
                                                                <input type="checkbox" {if isset($formDB.num_estatus) and $formDB.num_estatus==1} checked{/if}
                                                                       value="1" name="form[int][num_estatus]" {if isset($ver)}disabled{/if}>
                                                                <span>Estatus</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-offset-1 col-sm-10">
                                    <div class="col-sm-10">
                                        <div class="form-group floating-label" id="fk_nmb005_num_interfaz_cxpError">
                                            <label for="fk_nmb005_num_interfaz_cxp"><i class="icm icm-calculate2"></i> Obligación de Cuentas por Pagar</label>
                                            <input type="hidden" name="form[int][fk_nmb005_num_interfaz_cxp]" id="fk_nmb005_num_interfaz_cxp" value="{if isset($formDB.fk_nmb005_num_interfaz_cxp)}{$formDB.fk_nmb005_num_interfaz_cxp}{/if}">
                                            <input type="text" class="form-control" readonly
                                                   value="{if isset($formDB.nombreProveedor)}{$formDB.nombreProveedor}{/if}"
                                                   id="nombreProveedor" >
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <button {if isset($ver)}disabled{/if}
                                                class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                type="button"
                                                title="Buscar Proveedor"
                                                data-toggle="modal" data-target="#formModal2"
                                                data-keyboard="false" data-backdrop="static"
                                                titulo="Buscar Proveedor"
                                                url="{$_Parametros.url}modLG/compras/requerimientoCONTROL/proveedorMET/proveedor2/true"
                                        >
                                            <i class="md md-search" style="color: #ffffff;"></i>
                                        </button>
                                        <button class="btn ink-reaction btn-raised btn-xs btn-danger" type="button" title="Limpiar Proveedor" id="limpiar">
                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-offset-1 col-sm-10">

                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-head card-head-xs style-primary">
                                                <header>Nominas</header>
                                            </div>
                                            <div class="card-body" style="padding: 4px;">
                                                <div class="table-responsive">
                                                    <table class="table no-margin" id="tipoNomina">
                                                        <thead>
                                                        <tr>
                                                            <th>Cod</th>
                                                            <th>Nominas</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                            {if isset($nominas)}
                                                                {foreach item=i from=$nominas}
                                                                    <tr class="idNomina{$i.id}">
                                                                    <input type="hidden" value="{$i.id}" class="tipoNominaInput" name="form[int][fk_nmb001_num_tipo_nomina][]" nomina="{$i.nombre}" />
                                                                    <td>{$i.cod}</td>
                                                                    <td>{$i.nombre}</td>
                                                                    <td> {if isset($ver)} {else}<button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idNomina{$i.id}"><i class="md md-delete" style="color: #ffffff;"></i></button> {/if}</td>
                                                                    </tr>
                                                                {/foreach}
                                                            {/if}
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="3" align="center">
                                                                {if isset($ver)} {else}
                                                                    <button type="button"
                                                                            class="btn btn-primary ink-reaction btn-raised"
                                                                            id="agregarNomina"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Nomina"
                                                                            url="{$_Parametros.url}modNM/maestros/tipoNominaCONTROL/indexMET/MODAL"
                                                                            >Agregar Nomina
                                                                    </button>
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-head card-head-xs style-primary">
                                                <header>Procesos</header>
                                            </div>
                                            <div class="card-body" style="padding: 4px;">
                                                <div class="table-responsive">
                                                    <table class="table no-margin" id="tipoProcesoNomina">
                                                        <thead>
                                                        <tr>
                                                            <th>Cod</th>
                                                            <th>Procesos</th>
                                                            <th>Acciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {if isset($nominas)}
                                                            {foreach item=i from=$procesos}
                                                                <tr class="idProceso{$i.id}">
                                                                    <input type="hidden" value="{$i.id}" name="form[int][fk_nmb003_num_tipo_proceso][]" class="tipoProcesoInput" proceso="{$i.cod}" />
                                                                    <td>{$i.cod}</td>
                                                                    <td>{$i.nombre}</td>
                                                                    <td>{if isset($ver)} {else} <button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idProceso{$i.id}"><i class="md md-delete" style="color: #ffffff;"></i></button>{/if}</td>
                                                                </tr>
                                                            {/foreach}
                                                        {/if}
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="3" align="center">
                                                                {if isset($ver)} {else}
                                                                    <button type="button"
                                                                            class="btn btn-primary ink-reaction btn-raised"
                                                                            id="agregarProceso"
                                                                            data-toggle="modal" data-target="#formModal2"
                                                                            data-keyboard="false" data-backdrop="static"
                                                                            titulo="Agregar Procesos"
                                                                            url="{$_Parametros.url}modNM/maestros/tipoProcesoCONTROL/indexMET/MODAL"
                                                                            >Agregar Proceso
                                                                    </button>
                                                                {/if}
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="step3">
                            <div class="row">
                                <div class="col-sm-offset-1 col-sm-10">
                                    <div class="card">
                                        <div class="card-head card-head-xs style-primary">
                                            <header>Informacion Contable</header>
                                        </div>
                                        <div class="card-body" style="padding: 4px;">
                                            <div class="table-responsive">
                                                <table class="table no-margin" id="informacionContable">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 180px;" class="text-center">Tipo Nomina</th>
                                                        <th style="width: 180px;" class="text-center" valign="middle">
                                                            Tipo Proceso
                                                        </th>
                                                        <th class="text-center" valign="middle">Partida Presupuestaria
                                                        </th>
                                                        <th class="text-center" valign="middle">Debe</th>
                                                        <th class="text-center" valign="middle">Debe Pub20</th>
                                                        <th class="text-center" valign="middle">Haber</th>
                                                        <th class="text-center" valign="middle">Haber Pub20</th>
                                                        <th>Acciones</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    {if isset($formDBDet)}
                                                        {foreach item=i from=$formDBDet}
                                                            <tr id="idNM{$i.fk_nmb001_num_tipo_nomina}idPRO{$i.fk_nmb003_num_tipo_proceso}List">
                                                            <input type="hidden" proceso="{$i.ind_nombre_proceso}" nomina="{$i.ind_nombre_nomina}" idNomina="{$i.fk_nmb001_num_tipo_nomina}" idProceso="{$i.fk_nmb003_num_tipo_proceso}" partida="{$i.ind_cod_partida}" debe="{$i.ind_cod_cuenta_contable_debe}" debePub20="{$i.ind_cod_cuenta_contable_debe_pub20}" haber="{$i.ind_cod_cuenta_contable_haber}" haberPub20="{$i.ind_cod_cuenta_contable_haber_pub20}" class="trInformacion">
                                                            <input type="hidden" name="form[txt][partida][{$i.fk_nmb001_num_tipo_nomina}{$i.fk_nmb003_num_tipo_proceso}]" value="{$i.ind_cod_partida}">
                                                            <input type="hidden" name="form[txt][debe][{$i.fk_nmb001_num_tipo_nomina}{$i.fk_nmb003_num_tipo_proceso}]" value="{$i.ind_cod_cuenta_contable_debe}">
                                                            <input type="hidden" name="form[txt][debePub20][{$i.fk_nmb001_num_tipo_nomina}{$i.fk_nmb003_num_tipo_proceso}]" value="{$i.ind_cod_cuenta_contable_debe_pub20}">
                                                            <input type="hidden" name="form[txt][haber][{$i.fk_nmb001_num_tipo_nomina}{$i.fk_nmb003_num_tipo_proceso}]" value="{$i.ind_cod_cuenta_contable_haber}">
                                                            <input type="hidden" name="form[txt][haberPub20][{$i.fk_nmb001_num_tipo_nomina}{$i.fk_nmb003_num_tipo_proceso}]" value="{$i.ind_cod_cuenta_contable_haber_pub20}">
                                                            <td>{$i.ind_nombre_nomina}</td>
                                                            <td>{$i.ind_nombre_proceso}</td>
                                                            <td><input type="text" size="10"  class="form-control partida" style="height: 20px;margin-bottom: 0px;" readonly value="{$i.ind_cod_partida}" /></td>
                                                            <td><input type="text" size="10"  class="form-control debe" style="height: 20px;margin-bottom: 0px;" readonly value="{$i.ind_cod_cuenta_contable_debe}" /></td>
                                                            <td><input type="text" size="10"  class="form-control debePub20" style="height: 20px;margin-bottom: 0px;" readonly value="{$i.ind_cod_cuenta_contable_debe_pub20}" /></td>
                                                            <td><input type="text" size="10"  class="form-control haber" style="height: 20px;margin-bottom: 0px;" readonly value="{$i.ind_cod_cuenta_contable_haber}" /></td>
                                                            <td><input type="text" size="10"  class="form-control haberPub20" style="height: 20px;margin-bottom: 0px;" readonly value="{$i.ind_cod_cuenta_contable_haber_pub20}" /></td>
                                                            <td>{if isset($ver)} {else}<button class="eliminar  btn ink-reaction btn-raised btn-xs btn-danger borrar" borrar="idNM{$i.fk_nmb001_num_tipo_nomina}idPRO{$i.fk_nmb003_num_tipo_proceso}List"><i class="md md-delete" style="color: #ffffff;"></i></button> {/if}</td>
                                                            </tr>
                                                        {/foreach}
                                                    {/if}
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="8" align="center">
                                                            {if isset($ver)} {else}
                                                                <button type="button"
                                                                        class="btn btn-xs btn-primary ink-reaction btn-raised"
                                                                        id="agergarInformacionContable"
                                                                        idTabla="informacionContable"
                                                                        data-toggle="modal" data-target="#formModal2"
                                                                        data-keyboard="false" data-backdrop="static"
                                                                        titulo="Agregar Informacion Contable"
                                                                        url="{$_Parametros.url}modNM/maestros/perfilConceptoCONTROL/perfilDetalleNominaProcesoMET"
                                                                        >Agregar Campo
                                                                </button>
                                                            {/if}
                                                        </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <ul class="pager wizard">
                            <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                            <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                            <li class="next last"><a class="btn-raised" href="javascript:void(0);">Ultimo</a></li>
                            <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                        </ul>
                        <div class="row">
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group floating-label">
                                            <input type="text" disabled class="form-control disabled"
                                                   value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}"
                                                   id="ind_usuario">
                                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="col-sm-12">
                                    <div class="form-group floating-label" id="cod_tipo_nominaError">
                                        <input type="text" disabled class="form-control disabled"
                                               value="{if isset($formDB.fec_ultima_moficacion)}{$formDB.fec_ultima_moficacion}{/if}"
                                               id="fec_ultima_modificacion">
                                        <label for="cod_tipo_nomina"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
            <!--end #rootwizard -->
        </div>
        <!--end .col -->
    </div>
</div>
<span class="clearfix"></span>
</div>
{if isset($ver)} {else}
<div class="modal-footer">
    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal"
            descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar
    </button>
    <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
</div>
{/if}

<div id="nav-menu-vertical" style="display: none;">
    <ul>
        <li class="current"><a href="#"><span></span>Opciones de Nomina</a></li>
        <li><a href="#"><span></span>Funciones</a>
            <ul>
                {foreach item=i from=$metodos}
                    <li><a role="$this->{$i}()" tabindex="-1" href="javascript:void(0);"
                           class="funcionesNomina">{$i}()</a></li>
                {/foreach}
            </ul>
        </li>
        <li><a href="#"><span></span>Variables Disponibles</a>
            <ul>
                {foreach item=i from=$atributos}
                    <li><a role="$this->{$i}" tabindex="-1" href="javascript:void(0);"
                           class="funcionesNomina">$this->{$i}</a></li>
                {/foreach}
            </ul>
        </li>
        <li><a href="#"><span></span>Constantes Disponibles</a>
            <ul>
                {foreach item=i from=$constantes}
                    <li><a role="$this->{$i}" tabindex="-1" href="javascript:void(0);"
                           class="funcionesNomina">$this->{$i}</a></li>
                {/foreach}
            </ul>
        </li>
    </ul>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var app = new AppFunciones();
        //CodeMirror y Menu de seleccion
        $("#nav-menu-vertical").hide(10);
        var formula = "<?php\n"+$('#ind_formula').val()+"\n?>";

        $('#ind_formula').val(formula);
        $(document).bind("contextmenu", function (e) {
            return false;
        });
        $('#step1').on('bind contextmenu', '.card .card-body .CodeMirror', function (e) {
            if (e.button == 2) {
                $("#nav-menu-vertical").css("top", e.pageY - 150);
                $("#nav-menu-vertical").css("left", e.pageX - 150);
                $("#nav-menu-vertical").show('fast');
            }
        });
        $('#step1').on('click', '.card .card-body .CodeMirror', function () {
            $("#nav-menu-vertical").hide(1000);
        });
        app.metWizard();
        var cm = app.metCodeMirror('ind_formula');
        $('#nav-menu-vertical').on('click', 'ul li ul li .funcionesNomina', function () {
            var select = cm.getSelection
            cm.replaceSelection($(this).attr('role'), select);
            $("#nav-menu-vertical").hide(1000);
        });

        $('#accionesNM').on('click', '.accionesNM', function () {

            cm.replaceSelection($(this).attr('role'));
        });
        // Ajax
        $("#formAjax").submit(function () {
            return false;
        });
        $('.select2').select2({ allowClear: true });
        $('#modalAncho').css("width", "80%");
        $('#accion').click(function () {
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $('#ind_formula').val(cm.getValue().replace('<?php\n', '').replace('\n?>', '').replace(' \t//aqui tu Codigo ', ''));
            $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                var arrayCheck = ["num_estatus"];
                var arrayMostrarOrden = ['ind_abrebiatura','ind_descripcion','fk_a006_num_tipo_concepto', 'num_estatus'];
                if (dato['status'] == 'errorSQL') {
                    app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                } else if (dato['status'] == 'error') {
                    app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                } else if (dato['status'] == 'modificar') {
                    app.metActualizarRegistroTablaJson('dataTablaJson','El Concepto fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                } else if (dato['status'] == 'nuevo') {
                    app.metNuevoRegistroTablaJson('dataTablaJson','El Concepto fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            }, 'json');
        });
        //modales..
        $('#agregarProceso').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#agregarNomina').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });
        $('#agergarInformacionContable').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), {
                cargar: 0,
                tr: $("#" + $(this).attr('informacionContable') + " > tbody > tr").length + 1
            }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        $('.accionModal').click(function () {
            $('#modalAncho2').css("width", "50%");
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar:0, tr: $("#contenidoTabla > tbody > tr").length+1 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#tipoNomina').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
        $('#tipoProcesoNomina').on('click', '.borrar', function () {
            var borrar = $(this);
            $(document.getElementsByClassName(borrar.attr('borrar'))).remove();
        });
        $('#informacionContable').on('click', '.borrar', function () {
            var borrar = $(this);
            console.log(borrar);
            $(document.getElementById(borrar.attr('borrar'))).remove();
        });
        $('#limpiar').on('click', function () {
            $(document.getElementById('nombreProveedor')).val('');
            $(document.getElementById('fk_nmb005_num_interfaz_cxp')).val('');
        });

    });
</script>