<form action="{$_Parametros.url}modNM/maestros/perfilConceptoCONTROL/modificarPerfilDetalleMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="1" name="valido" />
        {if isset($idNomina) }
            <input type="hidden" value="{$idNomina}" name="idNomina"/>
        {/if}
        <div class="row">
            <div class="col-md-3 col-sm-offset-1"><h3>Cod. Nomina: {$nomina.cod_tipo_nomina}</h3></div>
            <div class="col-md-3"><h3>Nomina: {$nomina.ind_nombre_nomina}</h3></div>
            <div class="col-md-5"><h3>Titulo de Boleta: {$nomina.ind_titulo_boleta}</h3></div>
            <div class="col-md-12">
                <div class="card tabs-left style-default-light">
                    <ul class="card-head nav nav-tabs" data-toggle="tabs">
                        {foreach item=i from=$perfilConcepto}
                            <li {if $n == 1}class="active {$n++}"{/if}><a href="#proceso{$i.id}"><i class="icm icm-cog3"></i> {$i.nombre}</a></li>
                            <input type="hidden" name="form[int][fk_nmb003_num_tipo_proceso][]" value="{$i.id}">
                        {/foreach}
                    </ul>
                    <div class="card-body tab-content style-default-bright">
                        {foreach item=i from=$perfilConcepto}
                            <div class="tab-pane {if $n2 == 1}active {$n2++}{/if}" id="proceso{$i.id}">
                                Proceso: {$i.nombre}
                                <div class="table-responsive">
                                    <table class="table no-margin">
                                        <thead>
                                            <tr>
                                                <th width="100" class="text-center">Cod Concepto</th>
                                                <th width="200" class="text-center">Concepto</th>
                                                <th width="150" class="text-center">Partida Presupuestaria</th>
                                                <th class="text-center">Debe</th>
                                                <th class="text-center">Debe Pub20</th>
                                                <th class="text-center">Haber</th>
                                                <th class="text-center">Haber Pub20</th>
                                            </tr>
                                        </thead>
                                        <tbody class="table-hover">
                                        {foreach item=ii from=$i.array}
                                            <tr>
                                                <td colspan="7" class="bg-primary">{$ii.tipo}</td>
                                            </tr>
                                            {foreach item=iii from=$ii.detalle}
                                                <input type="hidden" name="form[int][fk_nmb002_num_concepto][{$idNomina}{$i.id}][]" value="{$iii.pk_num_concepto}">
                                                <tr>
                                                    <td class="text-center">{$iii.cod_concepto}</td>
                                                    <td>{$iii.ind_descripcion}</td>
                                                    <td><input type="text" size="10" {if $ver ==1}disabled readonly{/if} maxlength="12" name="form[int][ind_cod_partida][{$idNomina}{$i.id}{$iii.pk_num_concepto}]"  class="form-control text-center" style="height: 20px;margin-bottom: 0px;" value="{$iii.ind_cod_partida}" /></td>
                                                    <td><input type="text" size="10" {if $ver ==1}disabled readonly{/if} name="form[int][ind_cod_cuenta_contable_debe][{$idNomina}{$i.id}{$iii.pk_num_concepto}]" class="form-control text-right" style="height: 20px;margin-bottom: 0px;" value="{$iii.ind_cod_cuenta_contable_debe}" /></td>
                                                    <td><input type="text" size="10" {if $ver ==1}disabled readonly{/if} name="form[int][ind_cod_cuenta_contable_debe_pub20][{$idNomina}{$i.id}{$iii.pk_num_concepto}]" class="form-control text-right" style="height: 20px;margin-bottom: 0px;" value="{$iii.ind_cod_cuenta_contable_debe_pub20}" /></td>
                                                    <td><input type="text" size="10" {if $ver ==1}disabled readonly{/if} name="form[int][ind_cod_cuenta_contable_haber][{$idNomina}{$i.id}{$iii.pk_num_concepto}]" class="form-control text-right" style="height: 20px;margin-bottom: 0px;" value="{$iii.ind_cod_cuenta_contable_haber}" /></td>
                                                    <td><input type="text" size="10" {if $ver ==1}disabled readonly{/if} name="form[int][ind_cod_cuenta_contable_haber_pub20][{$idNomina}{$i.id}{$iii.pk_num_concepto}]" class="form-control text-right" style="height: 20px;margin-bottom: 0px;" value="{$iii.ind_cod_cuenta_contable_haber_pub20}" /></td>
                                                </tr>
                                            {/foreach}
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    {if $ver==0}
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
    </div>
    {/if}
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css("width","80%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificacion'){
                    app.metActualizarRegistroTablaJson('dataTablaJson','Registro modificado satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>