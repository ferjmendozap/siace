<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Obligaciones Cuentas por Pagar - Listado</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Cod Documento</th>
                            <th>Proveedor</th>
                            <th>Pagar A</th>
                            <th>Titulo</th>
                            <th>Tipo</th>
                            <th>Accion</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th colspan="5">
                                {if in_array('NM-01-03-02-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                            descipcion="el Usuario a creado una Nueva Obligacion De i"  titulo="<i class='icm icm-cog3'></i> Crear Obligacion" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nueva Obligacion
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        var $url='{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/crearModificarMET';
        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/jsonDataTablaMET",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "pk_num_interfaz_cxp" },
                    { "data": "cod_tipo_documento" },
                    { "data": "proveedor" },
                    { "data": "pagarA" },
                    { "data": "tipoObligacion" },
                    { "data": "ind_titulo" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idObligacionCxp:0 },function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url,{ idObligacionCxp: $(this).attr('idObligacionCxp')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        
    });
</script>