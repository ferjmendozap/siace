<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body ">
                    <form action="{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/crearModificarMET" autocomplete="off" id="formAjax" class="form" role="form" method="post">
                        <input type="hidden" value="1" name="valido" />
                        {if isset($idObligacionCxp) }
                            <input type="hidden" value="{$idObligacionCxp}" name="idObligacionCxp"/>
                        {/if}
                        <div class="row">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-head card-head-xs style-primary text-center">
                                            <header>Informaci&oacute;n del Proveedor</header>
                                        </div>
                                        <div class="card-body" style="padding: 4px;">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="docProveedor"><i class="icm icm-calculate2"></i>
                                                                Doc. Fiscal</label>
                                                            <input type="text" class="form-control"
                                                                   value="{if isset($formDB.ind_documento_fiscal)}{$formDB.ind_documento_fiscal}{/if}"
                                                                   id="docProveedor" disabled readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group" id="fk_a003_num_persona_proveedorError">
                                                            <label for="nombreProveedor"><i class="icm icm-calculate2"></i>
                                                                Nombre del Proveedor</label>
                                                            <input type="text" class="form-control"
                                                                   value="{if isset($formDB.ind_nombre1)}{$formDB.ind_nombre1}{/if}"
                                                                   id="nombreProveedor" disabled readonly>

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <div class="form-group">
                                                            <input id="fk_a003_num_persona_proveedor" type="hidden" value="{if isset($formDB.fk_a003_num_persona_proveedor)}{$formDB.fk_a003_num_persona_proveedor}{/if}" name="form[int][fk_a003_num_persona_proveedor]">
                                                            <button
                                                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                    type="button"
                                                                    title="Buscar Proveedor"
                                                                    data-toggle="modal" data-target="#formModal2"
                                                                    data-keyboard="false" data-backdrop="static"
                                                                    titulo="Buscar Proveedor"
                                                                    url="{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/proveedorMET/proveedor/"
                                                                    >
                                                                <i class="md md-search" style="color: #ffffff;"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label for="docPagar"><i class="icm icm-calculate2"></i>
                                                                Doc. Fiscal</label>
                                                            <input type="text" class="form-control"
                                                                   value="{if isset($formDB.ind_documento_fiscal11)}{$formDB.ind_documento_fiscal11}{/if}"
                                                                   id="docPagar" disabled readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group" id="fk_a003_num_persona_pagarError">
                                                            <label for="nombrePagar"><i class="icm icm-calculate2"></i>
                                                                pagar a.</label>
                                                            <input type="text" class="form-control"
                                                                   value="{if isset($formDB.ind_nombre11)}{$formDB.ind_nombre11}{/if}"
                                                                   id="nombrePagar" disabled readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-1">
                                                        <div class="form-group">
                                                            <input id="fk_a003_num_persona_pagar" type="hidden" value="{if isset($formDB.fk_a003_num_persona_pagar)}{$formDB.fk_a003_num_persona_pagar}{/if}" name="form[int][fk_a003_num_persona_pagar]">
                                                            <button
                                                                    class="btn ink-reaction btn-raised btn-xs btn-info accionModal"
                                                                    type="button"
                                                                    title="Buscar Proveedor"
                                                                    data-toggle="modal" data-target="#formModal2"
                                                                    data-keyboard="false" data-backdrop="static"
                                                                    titulo="Buscar Proveedor"
                                                                    url="{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/proveedorMET/pagarA/"
                                                                    >
                                                                <i class="md md-search" style="color: #ffffff;"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="col-sm-12">
                                                        <div class="form-group" id="fk_a006_num_miscelaneo_det_tipo_obligacionError">
                                                            <label for="fk_a006_num_miscelaneo_det_tipo_obligacion"><i
                                                                        class="md md-map"></i> Tipo de Obligacion</label>
                                                            <select id="fk_a006_num_miscelaneo_det_tipo_obligacion" required  {if isset($ver)}disabled{/if}
                                                                    name="form[int][fk_a006_num_miscelaneo_det_tipo_obligacion]"
                                                                    class="form-control select2-list select2">
                                                                <option value="">Seleccione</option>
                                                                {if isset($tipoObligacion)}
                                                                    {foreach item=i from=$tipoObligacion}
                                                                        {if isset($formDB.fk_a006_num_miscelaneo_det_tipo_obligacion)}
                                                                            {if $i.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_det_tipo_obligacion}
                                                                                <option selected
                                                                                        value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                            {else}
                                                                                <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                            {/if}
                                                                        {else}
                                                                            <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                {/if}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="col-sm-6">
                                                        <div class="form-group" id="fk_cpb002_num_tipo_documentoError">
                                                            <label for="fk_cpb002_num_tipo_documento"><i
                                                                        class="md md-map"></i> Tipo de Documento</label>
                                                            <select id="fk_cpb002_num_tipo_documento"
                                                                    name="form[int][fk_cpb002_num_tipo_documento]"
                                                                    class="form-control select2-list select2">
                                                                <option value="">Seleccione</option>
                                                                {if isset($tipoDocumento)}
                                                                    {foreach item=i from=$tipoDocumento}
                                                                        {if isset($formDB.fk_cpb002_num_tipo_documento)}
                                                                            {if $i.pk_num_tipo_documento==$formDB.fk_cpb002_num_tipo_documento}
                                                                                <option selected
                                                                                        value="{$i.pk_num_tipo_documento}">{$i.ind_descripcion}</option>
                                                                            {else}
                                                                                <option value="{$i.pk_num_tipo_documento}">{$i.ind_descripcion}</option>
                                                                            {/if}
                                                                        {else}
                                                                            <option value="{$i.pk_num_tipo_documento}">{$i.ind_descripcion}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                {/if}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group" id="fk_000_tipo_servicioError">
                                                            <label for="fk_000_tipo_servicio">
                                                                Tipo de Servicio:
                                                            </label>
                                                            <select name="form[int][fk_000_tipo_servicio]"
                                                                    class="form-control select2-list select2" required
                                                                    data-placeholder="Seleccione el Tipo de Servicio"
                                                                    id="fk_000_tipo_servicio">
                                                                <option value="">Seleccione..</option>
                                                                {foreach item=proceso from=$listadoServicio}
                                                                    {if isset($formDB.fk_000_tipo_servicio) and $formDB.fk_000_tipo_servicio == $proceso.pk_num_tipo_servico }
                                                                        <option value="{$proceso.pk_num_tipo_servico}"
                                                                                selected>{$proceso.ind_descripcion}</option>
                                                                    {else}
                                                                        <option value="{$proceso.pk_num_tipo_servico}">{$proceso.ind_descripcion}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">

                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-head card-head-xs style-primary text-center">
                                                <header>Informaci&oacute;n del Pago</header>
                                            </div>
                                            <div class="card-body" style="padding: 4px;">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group floating-label"
                                                             id="ind_tituloError">
                                                            <input type="text" class="form-control"
                                                                   value="{if isset($formDB.ind_titulo)}{$formDB.ind_titulo}{/if}"
                                                                   name="form[alphaNum][ind_titulo]" id="ind_titulo" maxlength="7"
                                                                   required>
                                                            <label for="ind_titulo"><i
                                                                        class="icm icm-calculate2"></i>
                                                                Titulo de la Obligacion </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="checkbox checkbox-styled">
                                                            <label>
                                                                <input type="checkbox" {if isset($formDB.num_flag_prq) and $formDB.num_flag_prq==1} checked{/if}
                                                                       value="1" name="form[int][num_flag_prq]">
                                                                <span>Flag Primera Quincena</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group floating-label"
                                                             id="ind_comentarioError">
                                                            <input type="text" class="form-control"
                                                                   value="{if isset($formDB.ind_comentario)}{$formDB.ind_comentario}{/if}"
                                                                   name="form[alphaNum][ind_comentario]" id="ind_comentario"
                                                                   required>
                                                            <label for="ind_comentario"><i
                                                                        class="icm icm-calculate2"></i>
                                                                Comentario de la Obligacion</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group" id="fk_a006_num_miscelaneo_det_tipo_pagoError">
                                                            <label for="fk_a006_num_miscelaneo_det_tipo_pago"><i
                                                                        class="md md-map"></i> Tipo de Pago</label>
                                                            <select id="fk_a006_num_miscelaneo_det_tipo_pago"
                                                                    name="form[int][fk_a006_num_miscelaneo_det_tipo_pago]"
                                                                    class="form-control select2-list select2">
                                                                <option value="">Seleccione</option>
                                                                {if isset($tipoPago)}
                                                                    {foreach item=i from=$tipoPago}
                                                                        {if isset($formDB.fk_a006_num_miscelaneo_det_tipo_pago)}
                                                                            {if $i.pk_num_miscelaneo_detalle==$formDB.fk_a006_num_miscelaneo_det_tipo_pago}
                                                                                <option selected
                                                                                        value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                            {else}
                                                                                <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                            {/if}
                                                                        {else}
                                                                            <option value="{$i.pk_num_miscelaneo_detalle}">{$i.ind_nombre_detalle}</option>
                                                                        {/if}
                                                                    {/foreach}
                                                                {/if}
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6">
                                                        <div class="form-group" id="ind_num_cuentaError">
                                                            <label for="ind_num_cuenta"><i
                                                                        class="md md-map"></i> Cuenta Bancaria</label>
                                                            <select id="ind_num_cuenta"
                                                                    name="form[int][ind_num_cuenta]"
                                                                    class="form-control select2-list select2">
                                                                <option value="">Seleccione</option>
                                                                {foreach item=proceso from=$listadoCuentas}
                                                                    {if isset($formDB.ind_num_cuenta) and $formDB.ind_num_cuenta == $proceso.pk_num_cuenta }
                                                                        <option value="{$proceso.pk_num_cuenta}" selected >{$proceso.ind_num_cuenta}</option>
                                                                    {else}
                                                                        <option value="{$proceso.pk_num_cuenta}">{$proceso.ind_num_cuenta}</option>
                                                                    {/if}
                                                                {/foreach}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="card">
                                            <div class="card-head card-head-xs style-primary text-center">
                                                <header>Partida Presupuestaria o Cuenta Contable</header>
                                            </div>
                                            <div class="card-body" style="padding: 4px;">
                                                <div class="table-responsive">
                                                    <table class="table table-striped" id="partidasCuenta">
                                                        <thead>
                                                        <tr>
                                                            <th>Descripcion</th>
                                                            <th width="165" colspan="2">Partida o Cuenta</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {if isset($formDBDetPartida)}
                                                            {foreach item=i from=$formDBDetPartida}
                                                                <tr id="P{$i.pk_num_partida_presupuestaria}">
                                                                    <input type="hidden" value="{$i.pk_num_partida_presupuestaria}" name="form[txt][cod][]">
                                                                    <input type="hidden" value="P" name="form[txt][tipo][]">
                                                                    <td>{$i.ind_denominacion}</td>
                                                                    <td width="130">{$i.cod_partida}</td>
                                                                    <td width="35" width="10px" class="text-center" style="vertical-align: middle;">
                                                                        <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" idTr="P{$i.pk_num_partida_presupuestaria}">
                                                                            <i class="md md-delete"></i>
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                            {/foreach}
                                                        {/if}
                                                        {if isset($formDBDetCuenta)}
                                                            {foreach item=i from=$formDBDetCuenta}
                                                                <tr id="C{$i.pk_num_cuenta}">
                                                                    <input type="hidden" value="{$i.pk_num_cuenta}" name="form[txt][cod][]">
                                                                    <input type="hidden" value="C" name="form[txt][tipo][]">
                                                                    <td>{$i.ind_descripcion}</td>
                                                                    <td width="130">{$i.cod_cuenta}</td>
                                                                    <td width="35" width="10px" class="text-center" style="vertical-align: middle;">
                                                                        <button class="btn ink-reaction btn-raised btn-xs btn-danger delete" idTr="C{$i.pk_num_cuenta}">
                                                                            <i class="md md-delete"></i>
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                            {/foreach}
                                                        {/if}
                                                        </tbody>
                                                        <tfoot>
                                                        <tr>
                                                            <td colspan="2" align="right">
                                                                <button
                                                                        class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                        type="button"
                                                                        title="Buscar Partidas"
                                                                        data-toggle="modal" data-target="#formModal2"
                                                                        data-keyboard="false" data-backdrop="static"
                                                                        titulo="Buscar Partidas"
                                                                        url="{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/partidasCuentasMET/obligacionesCxp/P/">
                                                                    Agregar Partida
                                                                </button>
                                                                <button
                                                                        class="btn ink-reaction btn-raised btn-xs btn-primary accionModal"
                                                                        type="button"
                                                                        title="Buscar Cuenta"
                                                                        data-toggle="modal" data-target="#formModal2"
                                                                        data-keyboard="false" data-backdrop="static"
                                                                        titulo="Buscar Partidas"
                                                                        url="{$_Parametros.url}modNM/maestros/obligacionCuentasPorPagarCONTROL/partidasCuentasMET/obligacionesCxp/C/">
                                                                    Agregar Cuentas
                                                                </button>
                                                            </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<span class="clearfix"></span>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
    <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2({ allowClear: true });
        var app = new  AppFunciones();
        $("#formAjax").submit(function(){
            return false;
        });
        $("#fk_a006_num_miscelaneo_det_tipo_obligacion").change(function(){
            if($("#fk_a006_num_miscelaneo_det_tipo_obligacion").val()==''){
                console.log('hola.');
            }else{
                console.log('hola.');
            }
        });

        $('.accionModal').click(function () {
            $('#formModalLabel2').html($(this).attr('titulo'));
            $.post($(this).attr('url'), { cargar: 0 }, function ($dato) {
                $('#ContenidoModal2').html($dato);
            });
        });

        $('#partidasCuenta').on('click', '.delete', function () {
            var tr = $(this).attr('idTr');
            $('#'+tr).remove();
        });

        $('#modalAncho').css("width","80%");
        $('#accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
                var arrayCheck = ["num_estatus","num_flag_pago_mensual"];
                var arrayMostrarOrden = ['ind_documento_fiscal','ind_nombre_nomina','num_flag_pago_mensual','num_estatus'];
                if(dato['status']=='error'){
                    app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
                }else if(dato['status']=='errorSQL'){
                    app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                }else if(dato['status']=='modificar'){
                    app.metActualizarRegistroTabla(dato,dato['idObligacionCxp'],'idObligacionCxp',arrayCheck,arrayMostrarOrden,'La Nomina fue modificada satisfactoriamente.','cerrarModal','ContenidoModal');
                }else if(dato['status']=='nuevo'){
                    app.metNuevoRegistroTabla(dato,dato['idObligacionCxp'],'idObligacionCxp',arrayCheck,arrayMostrarOrden,'La Nomina fue guardada satisfactoriamente.','cerrarModal','ContenidoModal');
                }
            },'json');
        });
    });
</script>