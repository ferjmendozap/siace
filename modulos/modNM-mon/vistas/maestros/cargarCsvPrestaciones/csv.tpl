<form enctype="multipart/form-data" id="formAjax" method="post">
    <input type="hidden" name="validoCsv" value="1">
<table class="table table-striped no-margin" id="conceptoAsignadoEmpleado">
    <thead>
    <tr>
        <th width="20"> Año </th>
        <th> Mes </th>
        <th> Sueldo Base </th>
        <th> Total Asignaciones </th>
        <th> Sueldo Normal </th>
        <th> Alic. Vacacional </th>
        <th> Alic. Fin Año </th>
        <th> Bono Especial </th>
        <th> Bono Fiscal </th>
        <th> Dias Trimestre </th>
        <th> Dias Anual </th>
        <th> Remuneración Mensual </th>
        <th> Remuneración Diaria </th>
        <th> Monto Trimestal </th>
        <th> Monto Anual </th>
        <th> Acumulado </th>
    </tr>
    </thead>
    <tbody>
    {foreach item=i from=$csv}
        <tr>
            <td>
                <input type="hidden" name="form[int][fk_rhb001_num_empleado][]" value="{$idEmpleado}">
                <input type="hidden" name="form[int][fk_nmb001_num_tipo_nomina][]" value="{$idNomina}">
                <input type="text" name="form[int][fec_anio][]" value="{$i[0]}" class="from-control">
            </td>
            <td><input type="text" name="form[int][fec_mes][]" value="{$i[1]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_sueldo_base][]" value="{$i[2]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_total_asignaciones][]" value="{$i[3]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_sueldo_normal][]" value="{$i[4]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_alicuota_vacacional][]" value="{$i[5]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_alicuota_fin_anio][]" value="{$i[6]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_bono_especial][]" value="{$i[7]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_bono_fiscal][]" value="{$i[8]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_dias_trimestre][]" value="{$i[9]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_dias_anual][]" value="{$i[10]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_remuneracion_mensual][]" value="{$i[11]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_remuneracion_diaria][]" value="{$i[12]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_monto_trimestral][]" value="{$i[13]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_monto_anual][]" value="{$i[14]}" class="from-control"></td>
            <td><input type="text" name="form[int][num_monto_acumulado][]" value="{$i[15]}" class="from-control"></td>
        </tr>
    {/foreach}
    </tbody>
    <tfoot>
        <tr>
            <td colspan="16">
                <button class="logsUsuario btn ink-reaction btn-raised btn-info pull-right" type="button"
                        descipcion=""  titulo="" id="nuevo" >
                    <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Guardar Registro
                </button>
            </td>
        </tr>
    </tfoot>
</table>
</form>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}modNM/maestros/cargarCsvPrestacionesCONTROL/csvMET';
        $('#nuevo').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            $.post(url, $("#formAjax").serialize(),function(dato){
                console.log(dato['status']);
                if(dato['status'] == 'OK'){
                    swal("Registro Guardado!", 'Registro Guardado satisfactoriamente', "success");
                    $('#contenidoCsv').html('');
                }
            },'json');
        });
    });
</script>