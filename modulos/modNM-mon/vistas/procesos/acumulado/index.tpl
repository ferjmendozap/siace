<section class="style-default-bright">
    <div class="section-header">
        <h3 class="text-primary">Acumulado de prestaciones Sociales</h3>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form class="form-horizontal" role="form">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="fk_nmc001_num_tipo_nomina_periodo">Periodos</label>
                            <div class="col-sm-9">
                                <select id="fk_nmc001_num_tipo_nomina_periodo" name="form[txt][fk_nmc001_num_tipo_nomina_periodo]" class="form-control select2-list select2">
                                    <option value="0">Seleccione el Periodo</option>
                                    {foreach item=i from=$periodos}
                                        <option>{$i.periodo}</option>
                                    {/foreach}
                                </select>
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6" style="color:red;">
                        *Solo se listan los periodos con todas las nominas cerradas
                    </div>
                    <div class="form-control-line"></div>
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-xs btn-primary" id="selectEmpleado" titulo="seleccionar Empleados">REALIZAR CALCULOS</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <br>
    <table id="datatable1" class="table table-striped table-hover">
        <thead>
        <tr>
            <th> # </th>
            <th> Empleado </th>
            <th style="width: 350px;">Nombre</th>
            <th>SUELDO BASICO</th>
            <th>TOTAL ASIGNACIONES</th>
            <th>SUELDO NORMAL</th>
            <th>ALI. VAC.</th>
            <th>ALI. FIN AÑO</th>
            <th>REMUNERACION MENSUAL</th>
            <th>REMUNERACION DIARIA</th>
        </tr>
        </thead>
        <tbody id="listado">

        </tbody>

    </table>
</section>
<div class="row" id="listadoPersonas">

</div>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new  AppFunciones();
        //Periodos
        app.metJsonPeriodoProcesosDisponibles('{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/procesosDisponiblesMET','fk_nmb001_num_tipo_nomina','fk_nmc001_num_tipo_nomina_periodo','fk_nmb001_num_tipo_nomina','pk_num_tipo_nomina_periodo','fk_nmc001_num_tipo_nomina_periodo','Seleccione El Periodo',false,false);
        //Procesos
        app.metJsonPeriodoProcesosDisponibles('{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/procesosDisponiblesMET','fk_nmc001_num_tipo_nomina_periodo','fk_nmb003_num_tipo_proceso','fk_nmb001_num_tipo_nomina','pk_num_proceso_periodo','ind_nombre_proceso','Seleccione El Proceso',false,'procesos');
        var $url='{$_Parametros.url}modNM/procesos/acumuladoCONTROL/generarPrestacionesMET';

        $('#selectEmpleado').click(function(){
            swal({
                title: 'Ciudado!!',
                text: 'Esta seguro de realizar los calculos??',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post($url,{ periodo:$('#fk_nmc001_num_tipo_nomina_periodo').val() },function(dato){
                    if(dato['estatus']=='errorPeriodo') {
                        swal('Error!','Debe seleccionar un Periodo','error');
                    } else if(dato['estatus']=='errorSQL') {
                        swal('Error!','No se pudo registrar el acumulado.','error');
                    } else if(dato['estatus']=='ok') {
                        swal('Exito!', 'Registro Exitoso', 'success');
                        var empleados = dato['idEmpleado'];

                        var tr = '';
                        var c=0;
                        $.each(empleados,function( key,value ) {
                            c = c+1;
                            tr = tr + '<tr>' +
                                '<td>'+c+'</td>'+
                                '<td>'+key+'</td>'+
                                '<td>'+value['nombre']+'</td>'+
                                '<td>'+value['sueldoBasico']+'</td>'+
                                '<td>'+value['totalAsignaciones']+'</td>'+
                                '<td>'+value['sueldoNormal']+'</td>'+
                                '<td>'+value['alicVacacional']+'</td>'+
                                '<td>'+value['alicFinAnio']+'</td>'+
                                '<td>'+value['totalRemuneracionMensual']+'</td>'+
                                '<td>'+value['totalRemuneracionDiaria']+'</td>'+
                                '</tr>';
                        });
                        $('#listado').html('');
                        $('#listado').append(tr);

                    }
                },'json');
            });
        });
    });
</script>