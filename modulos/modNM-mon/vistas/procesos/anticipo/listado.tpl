<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">
                Anticipo Prestaciones Sociales - Aprobar
        </h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th width="70" class="text-center"> PERIODO </th>
                                <th width="90" class="text-center"> CEDULA </th>
                                <th class="text-center"> NOMBRE Y APELLIDO </th>
                                <th width="220" class="text-center"> JUSTIFICATIVO </th>
                                <th width="120" class="text-center"> MONTO ACUMULADO </th>
                                <th width="120" class="text-center"> ANTICIPO </th>
                                <th width="120" class="text-center"> TOTAL </th>
                                <th width="80" class="text-center"> ACCIONES </th>
                            </tr>
                        </thead>
                        <!--tbody>
                        {foreach item=i from=$listado}
                            <tr id="idAnticipo{$i.pk_num_prestaciones_sociales_anticipo}">
                                <td>{$i.ind_periodo}</td>
                                <td>{$i.ind_cedula_documento}</td>
                                <td>{$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</td>
                                <td class="text-justify"> {$i.ind_nombre_detalle} </td>
                                <td class="text-right"> {$i.num_monto_acumulado} </td>
                                <td class="text-right"> {$i.num_anticipo} </td>
                                <td class="text-right"> {$i.num_monto_acumulado - $i.num_anticipo} </td>
                                <td align="center">
                                    <button class="accion logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" idAnticipo="{$i.pk_num_prestaciones_sociales_anticipo}"  boton="si, Aprueba" tipo="AP"
                                            descipcion="El usuario esta aprobando un Anticipo" titulo="Revisaste toda la Documentación?" mensaje="Usted va a aprobar un anticipo tenga en cuenta que una vez aprobado este no podrá ser anulado si reviso bien toda la documentación apruebe!!">
                                        <i class="icm icm-rating3" style="color: #ffffff;"></i>
                                    </button>
                                    <button class="accion logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idAnticipo="{$i.pk_num_prestaciones_sociales_anticipo}"  boton="si, Anula"
                                            descipcion="El usuario esta anulando un Anticipo" titulo="Estas Seguro?" mensaje="Estas seguro que desea anular el Anticipo!!" tipo="AN">
                                        <i class="icm icm-blocked" style="color: #ffffff;"></i>
                                    </button>
                                </td>
                            </tr>
                        {/foreach}
                        </tbody-->
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function () {

        var app = new AppFunciones();
        var dt = app.dataTable(
                '#dataTablaJson',
                "{$_Parametros.url}modNM/procesos/anticipoCONTROL/jsonDataTablaMET/{$estatus}",
                "{$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
                [
                    { "data": "fec" },
                    { "data": "ind_descripcion" },
                    { "data": "ind_numero_resolucion" },
                    { "data": "ind_numero_gaceta" },
                    { "data": "ind_estado" },
                    { "orderable": false,"data": "acciones",'width':100 }
                ]
        );
        $('#dataTablaJson tbody').on( 'click', '.accion', function () {
            var idAnticipo=$(this).attr('idAnticipo');
            var tipo=$(this).attr('tipo');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modNM/procesos/anticipoCONTROL/aprobarAnularMET';
                $.post(url, { idAnticipo: idAnticipo, tipo:tipo },function(dato){
                    if(dato['status']=='OK'){
                        $(document.getElementById('idAnticipo'+dato['idAnticipo'])).remove();
                        swal(dato['mensaje']['titulo'], dato['mensaje']['contenido'], "success");
                    }
                },'json');
            });
        });
    });
</script>