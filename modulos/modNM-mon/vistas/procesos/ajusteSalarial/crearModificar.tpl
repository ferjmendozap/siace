<div class="modal-body">
    <div class="row">
        <div class="col-lg-12">
            <div id="rootWizard" class="form-wizard form-wizard-horizontal">
                <form action="{$_Parametros.url}modNM/procesos/ajusteSalarialCONTROL/crearModificarMET" id="formAjax"
                      class="form floating-label form-validation" role="form" method="post" novalidate="novalidate">
                    <input type="hidden" value="1" name="valido"/>
                    <input type="hidden" value="{$idAjusteSalarial}" name="idAjusteSalarial" id="idAjusteSalarial"/>

                    <div class="form-wizard-nav">
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary"></div>
                        </div>
                        <ul class="nav nav-justified">
                            <li class="active"><a href="#step1" data-toggle="tab"><span class="step">1</span> <span
                                            class="title">Informaci&oacute;n General</span></a></li>
                            <li><a href="#step2" data-toggle="tab" id="tabFormula"><span class="step">2</span> <span
                                            class="title">Cálculos</span></a></li>
                        </ul>
                    </div>
                    <div class="tab-content clearfix">
                        <div class="tab-pane active" id="step1">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="col-sm-6">
                                        <div class="form-group floating-label" id="ind_numero_resolucionError">
                                            <input type="text" class="form-control" value="{if isset($formDB.ind_numero_resolucion)}{$formDB.ind_numero_resolucion}{/if}" name="form[alphaNum][ind_numero_resolucion]" id="ind_numero_resolucion" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}>
                                            <label for="ind_numero_resolucion"><i class="fa fa-code"></i> Nro. Resolución <b style="color: red;font-weight: bold;">*</b></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group floating-label" id="ind_numero_gacetaError">
                                            <input type="text" class="form-control" value="{if isset($formDB.ind_numero_gaceta)}{$formDB.ind_numero_gaceta}{/if}" name="form[alphaNum][ind_numero_gaceta]" id="ind_numero_gaceta" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}>
                                            <label for="ind_numero_gaceta"><i class="fa fa-code"></i>Nro. Gaceta <b style="color: red;font-weight: bold;">*</b></label>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group floating-label" id="ind_periodoError">
                                            <input type="text" class="form-control" value="{if isset($formDB.fec_anio)}{$formDB.fec_anio}-{$formDB.fec_mes}{/if}" name="form[txt][ind_periodo]" id="ind_periodo" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}>
                                            <label for="ind_periodo"><i class="fa fa-calendar"></i> Periodo <b style="color: red;font-weight: bold;">*</b></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group floating-label" id="ind_estadoError">
                                            <input type="text" class="form-control" value="{if isset($formDB.ind_estado)}{$formDB.ind_estado}{/if}" disabled name="form[alphaNum][ind_estado]" id="ind_estado">
                                            <label for="ind_estado"><i class="fa fa-eye"></i> Estado:</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    {if isset($formDB.EMPLEADO_PREPARADO) }
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_creaError">
                                                <input type="text" class="form-control" value="{if isset($formDB.EMPLEADO_PREPARADO)}{$formDB.EMPLEADO_PREPARADO}{/if}" name="form[int][fk_rhb001_num_empleado_crea]" disabled id="fk_rhb001_num_empleado_crea">
                                                <label for="fk_rhb001_num_empleado_crea"><i class="md md-person"></i> Creado Por</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label" id="fec_preparadoError">
                                                <input type="text" class="form-control" value="{if isset($formDB.fec_preparado)}{$formDB.fec_preparado}{/if}" disabled name="form[alphaNum][fec_preparado]" id="fec_preparado">
                                                <label for="fec_preparado"><i class="md md-today"></i> Fecha de Creacion</label>
                                            </div>
                                        </div>
                                    {/if}
                                    {if isset($formDB.EMPLEADO_CONFORMADO) }
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                                                <input type="text" class="form-control" value="{if isset($formDB.EMPLEADO_CONFORMADO)}{$formDB.EMPLEADO_CONFORMADO}{/if}" name="form[int][fk_rhb001_num_empleado_aprueba]" disabled id="fk_rhb001_num_empleado_aprueba">
                                                <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Conformado Por</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label" id="fec_preparadoError">
                                                <input type="text" class="form-control" value="{if isset($formDB.fec_conformado)}{$formDB.fec_conformado}{/if}" disabled name="form[alphaNum][fec_conformado]" id="fec_preparado">
                                                <label for="fec_preparado"><i class="md md-today"></i> Fecha de Conformacion</label>
                                            </div>
                                        </div>
                                    {/if}
                                    {if isset($formDB.EMPLEADO_APROBADO) }
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                                                <input type="text" class="form-control" value="{if isset($formDB.EMPLEADO_APROBADO)}{$formDB.EMPLEADO_APROBADO}{/if}" name="form[int][fk_rhb001_num_empleado_aprueba]" disabled id="fk_rhb001_num_empleado_aprueba">
                                                <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Aprobado Por</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label" id="fec_preparadoError">
                                                <input type="text" class="form-control" value="{if isset($formDB.fec_aprobado)}{$formDB.fec_aprobado}{/if}" disabled name="form[alphaNum][fec_preparado]" id="fec_preparado">
                                                <label for="fec_preparado"><i class="md md-today"></i> Fecha de Aprobacion</label>
                                            </div>
                                        </div>
                                    {/if}
                                    {if isset($formDB.EMPLEADO_ANULADO) }
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                                                <input type="text" class="form-control" value="{if isset($formDB.EMPLEADO_ANULADO)}{$formDB.EMPLEADO_ANULADO}{/if}" disabled id="fk_rhb001_num_empleado_aprueba">
                                                <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Anulado Por</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group floating-label" id="fec_preparadoError">
                                                <input type="text" class="form-control" value="{if isset($formDB.fec_anulado)}{$formDB.fec_anulado}{/if}" disabled  id="fec_preparado">
                                                <label for="fec_preparado"><i class="md md-today"></i> Fecha de Anulado</label>
                                            </div>
                                        </div>
                                    {/if}

                                    <div class="col-sm-12">
                                        <div class="form-group floating-label" id="ind_descripcionError">
                                            <textarea id="ind_descripcion" name="form[alphaNum][ind_descripcion]" class="form-control" rows="3" placeholder="" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}>{if isset($formDB.ind_descripcion)}{$formDB.ind_descripcion}{/if}</textarea>
                                            <label for="ind_descripcion"><i class="md md-comment"></i> Descripcion del Aumento del Sueldo Minimo <b style="color: red;font-weight: bold;">*</b></label>
                                        </div>
                                    </div>
                                    {if isset($tipo) && ($tipo == 'VER')}
                                        {if isset($formDB.ind_motivo_anulacion) }
                                            <div class="col-sm-12">
                                                <div class="form-group floating-label" id="ind_motivo_anulacionError">
                                                    <textarea id="ind_motivo_anulacion" name="form[alphaNum][ind_motivo_anulacion]" class="form-control" rows="3" placeholder="" {if isset($formDB.ind_motivo_anulacion)}disabled{/if}>{if isset($formDB.ind_motivo_anulacion)}{$formDB.ind_motivo_anulacion}{/if}</textarea>
                                                    <label for="ind_motivo_anulacion"><i class="md md-comment"></i> Motivo de la Anulacion del Aumento del Sueldo Minimo</label>
                                                </div>
                                            </div>
                                        {/if}
                                    {else}
                                        {if (isset($tipo) && ($tipo == 'PR' || $tipo == 'CO') || isset($formDB.ind_motivo_anulacion))}
                                            <div class="col-sm-12">
                                                <div class="form-group floating-label" id="ind_motivo_anulacionError">
                                                    <textarea id="ind_motivo_anulacion" name="form[alphaNum][ind_motivo_anulacion]" class="form-control" rows="3" placeholder="" {if isset($formDB.ind_motivo_anulacion)}disabled{/if}>{if isset($formDB.ind_motivo_anulacion)}{$formDB.ind_motivo_anulacion}{/if}</textarea>
                                                    <label for="ind_motivo_anulacion"><i class="md md-comment"></i> Motivo de la Anulacion del Aumento del Sueldo Minimo</label>
                                                </div>
                                            </div>
                                        {/if}
                                    {/if}
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="step2">
                                <div class="table-responsive no-margin">
                                    <table class="table table-striped no-margin">
                                        <thead>
                                            <tr>
                                                <td></td>
                                                <td>Grado</td>
                                                <td>Descripción</td>
                                                <td width="150">Sueldo Actual</td>
                                                <td width="50">Porcentaje</td>
                                                <td width="150">Monto</td>
                                                <td width="150">Sueldo Nuevo</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {assign var="gradoDet" value="0"}
                                        {assign var="titulos" value=""}
                                        {foreach item=i from=$listadoGrados}
                                            {if $titulos != $i.ind_nombre_detalle}
                                                <tr class="success" style="background-color: #dff0d8;">
                                                    <td colspan="7">{$i.ind_nombre_detalle}</td>
                                                </tr>
                                                {assign var="titulos" value=$i.ind_nombre_detalle}
                                            {/if}
                                            {if isset($formDBDet)}
                                                {foreach item=ii from=$formDBDet}
                                                    {if $i.pk_num_grado == $ii.fk_rhc007_pk_num_grado}
                                                        {assign var="gradoDet" value="1"}
                                                        <tr id="trGrados{$i.pk_num_grado}">
                                                            <input type="hidden" name="form[int][gradoSalarial][{$i.pk_num_grado}][idAjusteDet]" value="{$ii.pk_num_ajuste_salarial_det}">
                                                            <td><input type="checkbox" class="checkGrados" checked value="{$i.pk_num_grado}" name="form[int][gradoSalarial][checkBox][]" id="checkGrado{$i.pk_num_grado}" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}></td>
                                                            <td>{$i.ind_grado}</td>
                                                            <td>{$i.ind_nombre_grado}</td>
                                                            <td>
                                                                <input type="text" value="{number_format($ii.num_sueldo_anterior,2, ',', '.')}"  class="form-control text-center" id="checkGrado{$i.pk_num_grado}Anterior" disabled style="height: 20px;margin-bottom: 0px;">
                                                                <input type="hidden" name="form[int][gradoSalarial][{$i.pk_num_grado}][num_sueldo_anterior]" value="{number_format($ii.num_sueldo_anterior,2,',','.')}">
                                                            </td>
                                                            <td><input type="number" name="form[int][gradoSalarial][{$i.pk_num_grado}][ind_porcentaje]"class="form-control text-center checkGrado{$i.pk_num_grado} cambio" clase="checkGrado{$i.pk_num_grado}" value="{$ii.num_porcentaje}" tipo="porcentaje" style="height: 20px;margin-bottom: 0px;" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}></td>
                                                            <td><input type="text" name="form[int][gradoSalarial][{$i.pk_num_grado}][num_monto]"class="form-control text-right checkGrado{$i.pk_num_grado} cambio" clase="checkGrado{$i.pk_num_grado}" value="{number_format($ii.num_monto,2,',','.')}" tipo="monto" style="height: 20px;margin-bottom: 0px;" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}></td>
                                                            <td><input type="text" name="form[int][gradoSalarial][{$i.pk_num_grado}][num_sueldo_promedio]" class="form-control text-right" id="checkGrado{$i.pk_num_grado}Sueldo" style="height: 20px;margin-bottom: 0px;" value="{number_format($ii.num_sueldo_promedio,2,',','.')}"{if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}></td>
                                                        </tr>
                                                    {/if}
                                                {/foreach}
                                            {/if}
                                            {if isset($formDBDetPaso)}
                                                {if $i.pasos != null}
                                                    {foreach item=ii from=$formDBDetPaso}
                                                        {if $i.pk_num_grado == $ii.fk_rhc007_num_grado}
                                                            {assign var="gradoDet" value="1"}
                                                            <tr id="trGradosPaso{$ii.fk_rhc027_num_grados_pasos}">
                                                                <input type="hidden" name="form[int][gradoSalarialPaso][{$ii.fk_rhc027_num_grados_pasos}][idAjusteDetPaso]" value="{$ii.pk_num_ajuste_salarial_det_paso}">
                                                                <td><input type="checkbox" class="checkGrados" checked value="{$ii.fk_rhc027_num_grados_pasos}" name="form[int][gradoSalarialPaso][checkBox][]" id="checkGradoPaso{$ii.fk_rhc027_num_grados_pasos}" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}></td>
                                                                <td>{$i.ind_grado}</td>
                                                                <td>{$i.ind_nombre_grado} {$ii.ind_nombre_detalle}</td>
                                                                <td>
                                                                    <input type="text" value="{number_format($ii.num_sueldo_anterior,2, ',', '.')}"  class="form-control text-center" id="checkGradoPaso{$ii.fk_rhc027_num_grados_pasos}Anterior" disabled style="height: 20px;margin-bottom: 0px;">
                                                                    <input type="hidden" name="form[int][gradoSalarialPaso][{$ii.fk_rhc027_num_grados_pasos}][num_sueldo_anterior]" value="{number_format($ii.num_sueldo_anterior,2,',','.')}">
                                                                    <input type="hidden" name="form[int][gradoSalarialPaso][{$ii.fk_rhc027_num_grados_pasos}][idGrado]" value="{$i.pk_num_grado}">
                                                                </td>
                                                                <td><input type="number" name="form[int][gradoSalarialPaso][{$ii.fk_rhc027_num_grados_pasos}][ind_porcentaje]" value="{$ii.num_porcentaje}" class="form-control text-center checkGradoPaso{$ii.fk_rhc027_num_grados_pasos} cambio" clase="checkGradoPaso{$ii.fk_rhc027_num_grados_pasos}" tipo="porcentaje" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if} style="height: 20px;margin-bottom: 0px;"></td>
                                                                <td><input type="text" name="form[int][gradoSalarialPaso][{$ii.fk_rhc027_num_grados_pasos}][num_monto]" value="{number_format($ii.num_monto,2,',','.')}" class="form-control text-right checkGradoPaso{$ii.fk_rhc027_num_grados_pasos} cambio" clase="checkGradoPaso{$ii.fk_rhc027_num_grados_pasos}" tipo="monto" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if} style="height: 20px;margin-bottom: 0px;"></td>
                                                                <td><input type="text" name="form[int][gradoSalarialPaso][{$ii.fk_rhc027_num_grados_pasos}][num_sueldo_promedio]" class="form-control text-right" id="checkGradoPaso{$ii.fk_rhc027_num_grados_pasos}Sueldo"  value="{number_format($ii.num_sueldo_promedio,2,',','.')}" style="height: 20px;margin-bottom: 0px;" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}></td>
                                                            </tr>
                                                        {/if}
                                                    {/foreach}
                                                {/if}
                                            {/if}
                                            {if isset($gradoDet) && $gradoDet==0}
                                                {if $i.pasos != null}
                                                    {foreach item=ii from=$i.pasos}
                                                        <tr id="trGradosPaso{$ii.pk_num_grados_pasos}">
                                                            <td><input type="checkbox" class="checkGrados" value="{$ii.pk_num_grados_pasos}" name="form[int][gradoSalarialPaso][checkBox][]" id="checkGradoPaso{$ii.pk_num_grados_pasos}" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}></td>
                                                            <td>{$i.ind_grado}</td>
                                                            <td>{$i.ind_nombre_grado} {$ii.ind_nombre_detalle}</td>
                                                            <td>
                                                                <input type="text" value="{number_format($ii.num_sueldo_promedio,2,',','.')}"  class="form-control text-center" id="checkGradoPaso{$ii.pk_num_grados_pasos}Anterior" disabled style="height: 20px;margin-bottom: 0px;">
                                                                <input type="hidden" name="form[int][gradoSalarialPaso][{$ii.pk_num_grados_pasos}][num_sueldo_anterior]" value="{$ii.num_sueldo_promedio}">
                                                                <input type="hidden" name="form[int][gradoSalarialPaso][{$ii.pk_num_grados_pasos}][idGrado]" value="{$i.pk_num_grado}">
                                                            </td>
                                                            <td><input type="number" name="form[int][gradoSalarialPaso][{$ii.pk_num_grados_pasos}][ind_porcentaje]" class="form-control text-center checkGradoPaso{$ii.pk_num_grados_pasos} cambio" clase="checkGradoPaso{$ii.pk_num_grados_pasos}" tipo="porcentaje" disabled style="height: 20px;margin-bottom: 0px;"></td>
                                                            <td><input type="text" name="form[int][gradoSalarialPaso][{$ii.pk_num_grados_pasos}][num_monto]" class="form-control text-right checkGradoPaso{$ii.pk_num_grados_pasos} cambio" clase="checkGradoPaso{$ii.pk_num_grados_pasos}" tipo="monto" id="checkGradoPaso{$ii.pk_num_grados_pasos}diferencia" disabled style="height: 20px;margin-bottom: 0px;"></td>
                                                            <td><input type="text" name="form[int][gradoSalarialPaso][{$ii.pk_num_grados_pasos}][num_sueldo_promedio]" class="form-control text-right checkGradoPaso{$ii.pk_num_grados_pasos} inversa" clase="checkGradoPaso{$ii.pk_num_grados_pasos}" id="checkGradoPaso{$ii.pk_num_grados_pasos}Sueldo" style="height: 20px;margin-bottom: 0px;" disabled></td>
                                                        </tr>
                                                    {/foreach}
                                                {else}
                                                    <tr id="trGrados{$i.pk_num_grado}">
                                                        <td><input type="checkbox" class="checkGrados" value="{$i.pk_num_grado}" name="form[int][gradoSalarial][checkBox][]" id="checkGrado{$i.pk_num_grado}" {if isset($tipo) && ($tipo == 'VER' || $tipo == 'PR' || $tipo == 'CO')}disabled{/if}></td>
                                                        <td>{$i.ind_grado}</td>
                                                        <td>{$i.ind_nombre_grado}</td>
                                                        <td>
                                                            <input type="text" value="{number_format($i.num_sueldo_promedio,2,',','.')}"  class="form-control text-center" id="checkGrado{$i.pk_num_grado}Anterior" disabled style="height: 20px;margin-bottom: 0px;">
                                                            <input type="hidden" name="form[int][gradoSalarial][{$i.pk_num_grado}][num_sueldo_anterior]" value="{$i.num_sueldo_promedio}">
                                                        </td>
                                                        <td><input type="number" name="form[int][gradoSalarial][{$i.pk_num_grado}][ind_porcentaje]"class="form-control text-center checkGrado{$i.pk_num_grado} cambio" clase="checkGrado{$i.pk_num_grado}" tipo="porcentaje" disabled style="height: 20px;margin-bottom: 0px;"></td>
                                                        <td><input type="text" name="form[int][gradoSalarial][{$i.pk_num_grado}][num_monto]"class="form-control text-right checkGrado{$i.pk_num_grado} cambio" clase="checkGrado{$i.pk_num_grado}" tipo="monto" id="checkGrado{$i.pk_num_grado}diferencia" disabled style="height: 20px;margin-bottom: 0px;"></td>
                                                        <td><input type="text" name="form[int][gradoSalarial][{$i.pk_num_grado}][num_sueldo_promedio]" class="form-control text-right checkGrado{$i.pk_num_grado} inversa" clase="checkGrado{$i.pk_num_grado}" id="checkGrado{$i.pk_num_grado}Sueldo" style="height: 20px;margin-bottom: 0px;" disabled></td>
                                                    </tr>
                                                {/if}
                                            {/if}
                                            {assign var="gradoDet" value="0"}
                                        {/foreach}
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                        <ul class="pager wizard">
                            <li class="previous first"><a class="btn-raised" href="javascript:void(0);">Primero</a></li>
                            <li class="previous"><a class="btn-raised" href="javascript:void(0);">Anterior</a></li>
                            <li class="next last"><a class="btn-raised" href="javascript:void(0);">Ultimo</a></li>
                            <li class="next"><a class="btn-raised" href="javascript:void(0);">Siguiente</a></li>
                        </ul>
                        <div class="row">
                            <div class="col-sm-5 col-sm-offset-1">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group floating-label">
                                            <input type="text" disabled class="form-control disabled"
                                                   value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}"
                                                   id="ind_usuario">
                                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="col-sm-12">
                                    <div class="form-group floating-label" id="cod_tipo_nominaError">
                                        <input type="text" disabled class="form-control disabled"
                                               value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}"
                                               id="fec_ultima_modificacion">
                                        <label for="cod_tipo_nomina"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                </form>
            </div>
            <!--end #rootwizard -->
        </div>
        <!--end .col -->
    </div>
</div>
<span class="clearfix"></span>
</div>
{if isset($tipo) && ($tipo == 'VER')}

{else}
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if isset($tipo) && ($tipo == 'PR' || $tipo == 'CO')}
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal accion" tipo="anular"><i class="icm icm-blocked"></i>&nbsp;Anular</button>
            {if $tipo == 'CO'}
                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal accion" tipo="aprobar"><i class="icm icm-rating3"></i>&nbsp;Aprobar</button>
            {else}
                <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal accion" tipo="conformar"><i class="icm icm-rating2"></i>&nbsp;Conformar</button></button>
            {/if}
        {else}
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal accion" tipo="registrar"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
        {/if}
    </div>
{/if}

<script type="text/javascript">
    $(document).ready(function() {
        var app = new  AppFunciones();
        app.metWizard();
        $('#ind_periodo').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es' });
        $("#formAjax").submit(function(){
            return false;
        });
        $('#modalAncho').css("width","70%");
        $('.checkGrados').click(function(){
            var id = $(this).attr('id');
            var input = $('.'+id);
            for(i=0;i<input.length;i++){
                if(input[i].getAttribute('disabled')==''){
                    input[i].removeAttribute('disabled');
                }else{
                    input[i].setAttribute('disabled','');
                }
            }
        });
        $('.cambio').change(function(){
            var id = $(this).attr('clase');
            var sueldoA = $("#"+id+"Anterior").val();
            var sueldoP =  sueldoA.replace('.', '');
            var sueldoAnterior =  parseFloat(sueldoP.replace(',', '.'));
            var num = parseFloat($(this).val());
            if($(this).attr('tipo')=='porcentaje'){
                var porcentaje = num/100;
                var sueldoNuevo = sueldoAnterior+(sueldoAnterior*porcentaje);
            }else{
                var sueldoNuevo = sueldoAnterior+num;
            }
            $("#"+id+"Sueldo").val(sueldoNuevo.toFixed(2));
        });
        $('.inversa').change(function(){
            var id = $(this).attr('clase');
            var sueldoA = $("#"+id+"Anterior").val();
            var sueldoP =  sueldoA.replace('.', '');
            var sueldoAnterior =  parseFloat(sueldoP.replace(',', '.'));
            var num = parseFloat($(this).val());
            var sueldoNuevo = num-sueldoAnterior;
            $("#"+id+"diferencia").val(sueldoNuevo);
        });
        $('.accion').click(function(){
            swal({
                title: "¡Por favor espere!",
                text: "Se esta procesando su solicitud, puede demorar un poco.",
                timer: 50000000,
                showConfirmButton: false
            });
            var tipo = $(this).attr('tipo');
            if(tipo == 'registrar') {
                $.post($("#formAjax").attr("action"), $("#formAjax").serialize(), function (dato) {
                    var arrayCheck = false;
                    var arrayMostrarOrden = ['ind_periodo', 'ind_descripcion', 'ind_numero_resolucion', 'ind_numero_gaceta', 'ind_estado'];
                    if (dato['status'] == 'error') {
                        app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                    } else if (dato['status'] == 'errorCondicion') {
                        swal("Error!", dato['mensaje'], "error");
                    } else if (dato['status'] == 'errorSQL') {
                        app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                    } else if (dato['status'] == 'modificar') {
                        app.metActualizarRegistroTablaJson('dataTablaJson','La Nomina fue modificada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                    } else if (dato['status'] == 'nuevo') {
                        app.metNuevoRegistroTablaJson('', 'La Nomina fue guardada satisfactoriamente.', 'cerrarModal', 'ContenidoModal');
                    }
                }, 'json');
            }else{
                if(tipo == 'anular'){
                    var estado='AN';
                }else if(tipo == 'aprobar'){
                    var estado='AP';
                }else if(tipo == 'conformar'){
                    var estado='CO';
                }

                $.post($("#formAjax").attr("action"), { idAjusteSalarial: $('#idAjusteSalarial').val(), ind_motivo_anulacion: $('#ind_motivo_anulacion').val(), estado:estado }, function (dato) {
                    if (dato['status'] == 'error') {
                        app.metValidarError(dato, 'Disculpa. los campos marcados con X en rojo son obligatorios');
                    } else if (dato['status'] == 'errorSQL') {
                        app.metValidarError(dato, 'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
                    } else if (dato['status'] == 'AN' || dato['status'] == 'CO' || dato['status'] == 'AP') {
                        $(document.getElementById('idAjusteSalarial'+dato['idAjusteSalarial'])).remove();
                        swal(dato['Mensaje']['titulo']+'!!', dato['Mensaje']['contenido'], "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');
                    }
                }, 'json');
            }
        });


    });
</script>