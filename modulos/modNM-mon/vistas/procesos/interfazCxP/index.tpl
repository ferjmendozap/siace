<section class="style-default-bright">
    <div class="section-header">
        <h3 class="text-primary">Interfaz Cuentas Por Pagar</h3>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form class="form-horizontal" role="form">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="fk_nmb001_num_tipo_nomina">Nominas</label>
                                <div class="col-sm-9">
                                    <select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control select2-list">
                                        <option value="">Seleccione La Nomina</option>
                                        {foreach item=i from=$tipoNomina}
                                            <option value="{$i.pk_num_tipo_nomina}">{$i.ind_nombre_nomina}</option>
                                        {/foreach}
                                    </select>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="fk_nmb003_num_tipo_proceso">Procesos</label>
                                <div class="col-sm-9">
                                    <select id="fk_nmb003_num_tipo_proceso" name="form[int][fk_nmb003_num_tipo_proceso]" class="form-control select2-list">
                                        <option value="">Seleccione el Proceso</option>
                                    </select>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="fk_nmc001_num_tipo_nomina_periodo">Periodos</label>
                            <div class="col-sm-9">
                                <select id="fk_nmc001_num_tipo_nomina_periodo" name="form[txt][fk_nmc001_num_tipo_nomina_periodo]" class="form-control select2-list select2">
                                    <option value="">Seleccione el Periodo</option>
                                </select>
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-5">

                            </div>
                        </div>
                    </div>
                    <div class="form-control-line"></div>
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-xs btn-primary" id="selectEmpleado" titulo="Buscar Obligaciones">Buscar Obligaciones</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="dataTablaJson" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th width="10" class="text-center"> # </th>
                            <th width="100" class="text-center"> TIPO PAGO </th>
                            <th width="85" class="text-center"> PROVEEDOR </th>
                            <th class="text-center"> NOMBRE DEL PROVEEDOR </th>
                            <th width="50" class="text-center"> DOC. </th>
                            <th width="140" class="text-center"> NRO. REGISTRO </th>
                            <th width="40" class="text-center"> VERF. </th>
                            <th width="35" class="text-center"> TRF. </th>
                            <th width="200" class="text-center"> MONTO OBLIGACION </th>
                            <th width="10" class="text-center"> ESTADO </th>
                            <th width="100" class="text-center"> ACCIONES </th>
                        </tr>
                        </thead>
                        <tbody style="font-weight: bold; text-align: center;">
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="11" class="text-left">
                                {if in_array('NM-01-01-05-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" id="calcular"
                                            data-toggle='modal' data-target='#formModal'
                                            data-keyboard='false' data-backdrop='static'
                                            titulo="Calcular Obligaciones"
                                            mensaje="Esta Seguro que desea Calcular las obligaciones??"
                                            descipcion="el Usuario a calculado las Obligaciones de Nomina">
                                        Previsualizar Cálculo de Obligaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </button>
                                {/if}
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new  AppFunciones();
        var url='{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/';
        var dt = app.dataTable(
            '#dataTablaJson',
            url+'jsonDataTablaMET',
            "$_Parametros.ruta_Js}Aplicacion/datatableSpanish.json",
            [
                { "data": "pk_num_obligacion" },
                { "data": "ind_nombre_detalle" },
                { "data": "fk_a003_num_persona_proveedor"},
                { "data": "proveedor"},
                { "data": "cod_tipo_documento"},
                { "data": "ind_nro_factura"},
                { "data": "num_flag_presupuesto"},
                { "data": "num_flag_transferido"},
                { "data": "num_monto_obligacion"},
                { "data": "ind_estado"},
                { "orderable": false,"data": "acciones"}
            ]
        );

        //Periodos
        app.metJsonPeriodoProcesosDisponibles('{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/procesosDisponiblesMET','fk_nmb001_num_tipo_nomina','fk_nmc001_num_tipo_nomina_periodo','fk_nmb001_num_tipo_nomina','pk_num_tipo_nomina_periodo','fk_nmc001_num_tipo_nomina_periodo','Seleccione El Periodo',false,false);
        //Procesos
        app.metJsonPeriodoProcesosDisponibles('{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/procesosDisponiblesMET','fk_nmc001_num_tipo_nomina_periodo','fk_nmb003_num_tipo_proceso','fk_nmb001_num_tipo_nomina','pk_num_proceso_periodo','ind_nombre_proceso','Seleccione El Proceso',false,'procesos');

        $('#selectEmpleado').click(function(){
            if($('#fk_nmb003_num_tipo_proceso').val()=='' || $('#fk_nmb001_num_tipo_nomina').val()=='' || $('#fk_nmc001_num_tipo_nomina_periodo').val()==''){
                if($('#fk_nmb001_num_tipo_nomina').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar una Nomina', "error");
                }else if($('#fk_nmc001_num_tipo_nomina_periodo').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar EL Periodo', "error");
                }else{
                    swal("Error!", 'Disculpa Debes Seleccionar un Proceso', "error");
                }
            }else{
                $('#dataTablaJson').DataTable().ajax.url(url+'jsonDataTablaMET/'+$('#fk_nmb003_num_tipo_proceso').val()).load();
            }
        });

        $('#calcular').on( 'click', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            if($('#fk_nmb003_num_tipo_proceso').val()=='' || $('#fk_nmb001_num_tipo_nomina').val()=='' || $('#fk_nmc001_num_tipo_nomina_periodo').val()==''){
                var mensaje = '';
                if($('#fk_nmb001_num_tipo_nomina').val()==''){
                    mensaje = 'Disculpa Debes Seleccionar una Nomina';
                }else if($('#fk_nmc001_num_tipo_nomina_periodo').val()==''){
                    mensaje = 'Disculpa Debes Seleccionar EL Periodo';
                }else{
                    mensaje = 'Disculpa Debes Seleccionar un Proceso';
                }

                swal({
                    title: 'Error!',
                    text: mensaje,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: true
                }, function(){
                    $('#cerrarModal').click();
                    $('#ContenidoModal').html('');
                });
            }else{
                $('#modalAncho').css("width", "80%");
                $.post(url+'previsualizarObligacionesMET',{ pk_num_proceso_periodo:$('#fk_nmb003_num_tipo_proceso').val(),pk_num_tipo_nomina:$('#fk_nmb001_num_tipo_nomina').val() },function(dato){
                    $('#ContenidoModal').html(dato);
                });
            }
        });

        $('#dataTablaJson tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post( '{$_Parametros.url}modCP/obligaciones/operaciones/obligacionCONTROL/nuevaObligacionMET',
                { idObligacion: $(this).attr('idObligacion') , estado: $(this).attr('id'), ver:1 },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        var $url='{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/';
        $('#dataTablaJson tbody').on( 'click', '.verificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($url+'verificarObligacionMET',{ idObligacion: $(this).attr('idObligacion')},function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#dataTablaJson tbody').on( 'click', '.transferir', function () {
            var idObligacion = $(this).attr('idObligacion');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                $.post($url+'transferirObligacionMET', { idObligacion: idObligacion},function(dato){
                    if (dato['status'] == 'ok') {
                        swal("Obligacion Transferida!", 'La Obligacion fue transferida satisfactoriamente', "success");
                        $('#dataTablaJson').dataTable().api().row().remove().draw(false);
                        $('#cerrarModal').click();
                        $('#ContenidoModal').html('');
                    }
                },'json');
            });

        });
    });
</script>