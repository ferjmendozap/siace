<div class="col-sm-12">
    <section class="style-default-bright">
        <div class="section-header">
            <h3 class="text-primary">{$datosNomina.ind_titulo_boleta} {$datosNomina.PERIODO} {$datosNomina.ind_nombre_proceso}</h3>
        </div>
        <div class="checkbox checkbox-styled">
            <label>
                <input type="checkbox" id="checkCalcular"><span>Calcular Todas</span>
            </label>
            <label>
                <input type="checkbox" id="checkModificar"><span>Modificar Todas</span>
            </label>
            <label>
                <input type="checkbox" id="checkConglomerar"><span>Conglomerar Todas</span>
            </label>
        </div>
        <div class="section-body contain-lg">
            <div class="row">
                <div class="col-lg-12 contain-lg">
                    <div class="table-responsive">
                        <form action="" id="datosObligaciones">
                            <input type="hidden" name="form[formula][datosObligaciones][procesoPeriodo]" value="{$datosNomina.procesoPeriodo}">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th width="50" class="text-center"> # </th>
                                    <th width="50" class="text-center"> DOC. </th>
                                    <th class="text-center"> PROVEEDOR </th>
                                    <th width="100" class="text-center"> MONTO CALCULADO </th>
                                    <th width="100" class="text-center"> CALCULAR </th>
                                    <th width="100" class="text-center"> MODIFICAR </th>
                                    <th width="100" class="text-center"> CONGLOMERAR </th>
                                    <th width="100" class="text-center"> OBLIGACION PARA CONGLOMERAR/MODIFICAR </th>
                                </tr>
                                </thead>
                                <tbody style="font-weight: bold; text-align: center;">
                                {foreach item=i from=$proveedores}
                                    {if $i.num_monto_obligacion>0}
                                        <tr>
                                            <input type="hidden" name="form[formula][datosObligaciones][fk_a003_num_persona_proveedor][{$i.pk_num_interfaz_cxp}]" value="{$i.fk_a003_num_persona_proveedor}">
                                            <input type="hidden" name="form[formula][datosObligaciones][fk_a003_num_persona_proveedor_a_pagar][{$i.pk_num_interfaz_cxp}]" value="{$i.fk_a003_num_persona_proveedor_a_pagar}">
                                            <input type="hidden" name="form[formula][datosObligaciones][fk_a023_num_centro_de_costo][{$i.pk_num_interfaz_cxp}]" value="{$i.fk_a023_num_centro_de_costo}">
                                            <input type="hidden" name="form[formula][datosObligaciones][fk_cpb002_num_tipo_documento][{$i.pk_num_interfaz_cxp}]" value="{$i.fk_cpb002_num_tipo_documento}">
                                            <input type="hidden" name="form[formula][datosObligaciones][fk_cpb017_num_tipo_servicio][{$i.pk_num_interfaz_cxp}]" value="{$i.fk_cpb017_num_tipo_servicio}">
                                            <input type="hidden" name="form[formula][datosObligaciones][fk_a006_num_miscelaneo_tipo_pago][{$i.pk_num_interfaz_cxp}]" value="{$i.fk_a006_num_miscelaneo_tipo_pago}">
                                            <input type="hidden" name="form[formula][datosObligaciones][fk_cpb014_num_cuenta][{$i.pk_num_interfaz_cxp}]" value="{$i.fk_cpb014_num_cuenta}">
                                            <input type="hidden" name="form[formula][datosObligaciones][ind_nro_control][{$i.pk_num_interfaz_cxp}]" value="{$i.ind_nro_control}">
                                            <input type="hidden" name="form[formula][datosObligaciones][ind_nro_factura][{$i.pk_num_interfaz_cxp}]" value="{$i.ind_nro_factura}">
                                            <input type="hidden" name="form[formula][datosObligaciones][ind_comentarios][{$i.pk_num_interfaz_cxp}]" value="{$i.ind_comentarios}">
                                            <input type="hidden" name="form[formula][datosObligaciones][ind_comentarios_adicional][{$i.pk_num_interfaz_cxp}]" value="{$i.ind_comentarios_adicional}">
                                            <input type="hidden" name="form[formula][datosObligaciones][num_flag_compromiso][{$i.pk_num_interfaz_cxp}]" value="{$i.num_flag_compromiso}">
                                            <input type="hidden" name="form[formula][datosObligaciones][num_flag_obligacion_directa][{$i.pk_num_interfaz_cxp}]" value="{$i.num_flag_obligacion_directa}">
                                            <input type="hidden" name="form[formula][datosObligaciones][fk_nmc002_num_proceso_periodo][{$i.pk_num_interfaz_cxp}]" value="{$i.fk_nmc002_num_proceso_periodo}">
                                            <input type="hidden" name="form[formula][datosObligaciones][pk_num_tipo_nomina][{$i.pk_num_interfaz_cxp}]" value="{$i.pk_num_tipo_nomina}">

                                            <input type="hidden" name="form[formula][datosObligaciones][num_monto_obligacion][{$i.pk_num_interfaz_cxp}]" value="{$i.num_monto_obligacion}">
                                            <input type="hidden" name="form[formula][datosObligaciones][num_monto_impuesto_otros][{$i.pk_num_interfaz_cxp}]" value="{$i.num_monto_impuesto_otros}">
                                            <input type="hidden" name="form[formula][datosObligaciones][num_monto_no_afecto][{$i.pk_num_interfaz_cxp}]" value="{$i.num_monto_no_afecto}">

                                            {$c=0}
                                            {foreach item=j from=$i.partidaCuenta}
                                                {if isset($j.num_monto)}
                                                    {$c=$c+1}
                                                    <input type="hidden" name="form[formula][partidaCuenta][{$i.pk_num_interfaz_cxp}][{$c}][fk_a003_num_persona_proveedor]" value="{$j.fk_a003_num_persona_proveedor}">
                                                    <input type="hidden" name="form[formula][partidaCuenta][{$i.pk_num_interfaz_cxp}][{$c}][fk_a023_num_centro_costo]" value="{$j.fk_a023_num_centro_costo}">
                                                    <input type="hidden" name="form[formula][partidaCuenta][{$i.pk_num_interfaz_cxp}][{$c}][fk_cbb004_num_cuenta]" value="{$j.fk_cbb004_num_cuenta}">
                                                    <input type="hidden" name="form[formula][partidaCuenta][{$i.pk_num_interfaz_cxp}][{$c}][fk_cbb004_num_cuenta_pub20]" value="{$j.fk_cbb004_num_cuenta_pub20}">
                                                    <input type="hidden" name="form[formula][partidaCuenta][{$i.pk_num_interfaz_cxp}][{$c}][fk_prb002_num_partida_presupuestaria]" value="{$j.fk_prb002_num_partida_presupuestaria}">
                                                    <input type="hidden" name="form[formula][partidaCuenta][{$i.pk_num_interfaz_cxp}][{$c}][ind_descripcion]" value="{$j.ind_descripcion}">
                                                    <input type="hidden" name="form[formula][partidaCuenta][{$i.pk_num_interfaz_cxp}][{$c}][num_flag_no_afecto]" value="{$j.num_flag_no_afecto}">
                                                    <input type="hidden" name="form[formula][partidaCuenta][{$i.pk_num_interfaz_cxp}][{$c}][num_monto]" value="{$j.num_monto}">
                                                {/if}
                                            {/foreach}
                                            <input type="hidden" name="form[formula][partidaCuenta][{$i.pk_num_interfaz_cxp}][ind_secuencia]" value="{$c}">

                                            {$c=0}
                                            {foreach item=k from=$i.impuestosRetenciones}
                                                {if isset($k.num_monto_afecto)}
                                                    {$c=$c+1}
                                                    <input type="hidden" name="form[formula][impuestosRetenciones][{$i.pk_num_interfaz_cxp}][{$c}][fk_a003_num_persona_proveedor]" value="{$k.fk_a003_num_persona_proveedor}">
                                                    <input type="hidden" name="form[formula][impuestosRetenciones][{$i.pk_num_interfaz_cxp}][{$c}][fk_a023_num_centro_costo]" value="{$k.fk_a023_num_centro_costo}">
                                                    <input type="hidden" name="form[formula][impuestosRetenciones][{$i.pk_num_interfaz_cxp}][{$c}][fk_cbb004_num_cuenta]" value="{$k.fk_cbb004_num_cuenta}">
                                                    <input type="hidden" name="form[formula][impuestosRetenciones][{$i.pk_num_interfaz_cxp}][{$c}][fk_cbb004_num_cuenta_pub20]" value="{$k.fk_cbb004_num_cuenta_pub20}">
                                                    <input type="hidden" name="form[formula][impuestosRetenciones][{$i.pk_num_interfaz_cxp}][{$c}][fk_nmb002_num_concepto]" value="{$k.fk_nmb002_num_concepto}">
                                                    <input type="hidden" name="form[formula][impuestosRetenciones][{$i.pk_num_interfaz_cxp}][{$c}][num_monto_impuesto]" value="{$k.num_monto_impuesto}">
                                                    <input type="hidden" name="form[formula][impuestosRetenciones][{$i.pk_num_interfaz_cxp}][{$c}][num_monto_afecto]" value="{$k.num_monto_afecto}">
                                                    <input type="hidden" name="form[formula][impuestosRetenciones][{$i.pk_num_interfaz_cxp}][{$c}][ind_impresion]" value="{$k.ind_impresion}">
                                                {/if}

                                            {/foreach}
                                            <input type="hidden" name="form[formula][impuestosRetenciones][{$i.pk_num_interfaz_cxp}][ind_secuencia]" value="{$c}">

                                            <td>{$i.pk_num_interfaz_cxp}</td>
                                            <td>{$i.cod_tipo_documento}</td>
                                            <td>{$i.proveedor}</td>
                                            <td>{number_format($i.num_monto_obligacion,2,',','.')}</td>
                                            <td>
                                                <div class="checkbox checkbox-styled">
                                                    <label>
                                                        <input type="checkbox" class="checkCalcular" value="1" name="form[int][calcular][{$i.pk_num_interfaz_cxp}]" idCxp="{$i.pk_num_interfaz_cxp}">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="checkbox checkbox-styled">
                                                    <label>
                                                        <input type="checkbox" disabled class="checkModificar" value="1" name="form[int][modificar][{$i.pk_num_interfaz_cxp}]" id="modificar{$i.pk_num_interfaz_cxp}" idCxp="{$i.pk_num_interfaz_cxp}">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="checkbox checkbox-styled">
                                                    <label>
                                                        <input type="checkbox" disabled class="checkConglomerar" value="1" name="form[int][conglomerar][{$i.pk_num_interfaz_cxp}]" id="conglomerar{$i.pk_num_interfaz_cxp}" idCxp="{$i.pk_num_interfaz_cxp}">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>{if isset($i.obligacionConglomerar)}<input type="hidden" name="form[formula][datosObligaciones][obligacionConglomerar][{$i.pk_num_interfaz_cxp}]" value="{$i.obligacionConglomerar}" id="obligacionConglomerar{$i.pk_num_interfaz_cxp}">{$i.obligacionConglomerar}{else}
                                                    <input type="hidden" value="Ninguna" id="obligacionConglomerar{$i.pk_num_interfaz_cxp}">
                                                    Ninguna{/if}</td>
                                        </tr>

                                    {/if}
                                {/foreach}
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="3">
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-info" id="calcularPrevisualizado"
                                                titulo="Calcular Obligaciones"
                                                mensaje="Esta Seguro que desea Calcular las obligaciones??"
                                                descipcion="el Usuario a calculado las Obligaciones de Nomina">
                                            Calcular Obligaciones &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </button>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#datosObligaciones").submit(function () {
            return false;
        });
        $('#checkCalcular').on('click',function () {
           if(this.checked){
               $('.checkCalcular').attr('checked',true);

               $('.checkModificar').each(function () {
                   if($('#obligacionConglomerar'+$(this).attr('idCxp')).val() == 'Ninguna' ) {
                       $(this).attr('checked',false);
                       $(this).attr('disabled',true);
                   } else {
                       $(this).attr('disabled',false);
                   }
               });
               $('.checkConglomerar').each(function () {
                   if($('#obligacionConglomerar'+$(this).attr('idCxp')).val() == 'Ninguna' ) {
                       $(this).attr('checked',false);
                       $(this).attr('disabled',true);
                   } else {
                       $(this).attr('disabled',false);
                   }
               });
           } else {
               $('.checkCalcular').attr('checked',false);
               $('.checkModificar').attr('disabled',true);
               $('.checkConglomerar').attr('disabled',true);
               $('.checkModificar').attr('checked',false);
               $('.checkConglomerar').attr('checked',false);
               $('#checkModificar').attr('checked',false);
               $('#checkConglomerar').attr('checked',false);
           }
        });
        $('#checkModificar').on('click',function () {
           if(this.checked){
               $('.checkModificar').each(function () {
                   if($(this).attr('disabled')!='disabled' && $('#obligacionConglomerar'+$(this).attr('idCxp')).val() != 'Ninguna' ) {
                       $(this).attr('checked',true);
                   }
               });
               $('.checkConglomerar').attr('checked',false);
               $('#checkConglomerar').attr('checked',false);
           } else {
               $('.checkModificar').attr('checked',false);
           }
        });
        $('#checkConglomerar').on('click',function () {
           if(this.checked){
               $('.checkConglomerar').each(function () {
                   if($(this).attr('disabled')!='disabled' && $('#obligacionConglomerar'+$(this).attr('idCxp')).val() != 'Ninguna' ) {
                       $(this).attr('checked',true);
                   }
               });
               $('.checkModificar').attr('checked',false);
               $('#checkModificar').attr('checked',false);
           } else {
               $('.checkConglomerar').attr('checked',false);
           }
        });
        $('.checkCalcular').on('click',function () {
           if(this.checked && $('#obligacionConglomerar'+$(this).attr('idCxp')).val() != 'Ninguna' ){
               $('#modificar'+$(this).attr('idCxp')).attr('disabled',false);
               $('#conglomerar'+$(this).attr('idCxp')).attr('disabled',false);
           } else {
               $('#modificar'+$(this).attr('idCxp')).attr('checked',false);
               $('#modificar'+$(this).attr('idCxp')).attr('disabled',true);
               $('#conglomerar'+$(this).attr('idCxp')).attr('checked',false);
               $('#conglomerar'+$(this).attr('idCxp')).attr('disabled',true);
           }
        });
        $('.checkConglomerar').on('click',function () {
           if(this.checked){
               $('#modificar'+$(this).attr('idCxp')).attr('checked',false);
           }
        });
        $('.checkModificar').on('click',function () {
           if(this.checked){
               $('#conglomerar'+$(this).attr('idCxp')).attr('checked',false);
           }
        });

        $('#calcularPrevisualizado').on( 'click', function () {
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                swal({
                    title: "¡Por favor espere!",
                    text: "Se esta procesando su solicitud, puede demorar un poco.",
                    timer: 50000000,
                    showConfirmButton: false
                });
                var url = '{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/calcularObligacionesMET';
                $.post(url, $('#datosObligaciones').serialize() ,function(dato){
                    if (dato['status'] == 'listo') {
                        swal("Obligacion Calculada!", 'La Obligacion fue calculada satisfactoriamente', "success");
                        $('#dataTablaJson').dataTable().api().row().remove().draw(false);
                        $('#cerrarModal').click();
                        $('#ContenidoModal').html('');
                    }
                },'json');
            });
        });
    });
</script>