<form action="{$_Parametros.url}modNM/procesos/ejecucionProcesosCONTROL/" autocomplete="off" id="formAjaxGenerarNomina" class="form" role="form" method="post">
    <input type="hidden" id="valido" name="valido" value="1">
    <input type="hidden" id="pk_num_proceso_periodo" name="form[int][pk_num_proceso_periodo]" value="{$pk_num_proceso_periodo}">
    <input type="hidden" id="pk_num_tipo_nomina" name="form[int][pk_num_tipo_nomina]" value="{$pk_num_tipo_nomina}">
    <div class="col-sm-offset-1 col-sm-10">
        <div class="card">
            <div class="card-head">
                <header></header>
                <div class="tools">
                    <div class="btn-group">
                        <a class="btn ink-reaction btn-floating-action btn-sm md-2x btn-info accionesNomina2" tipo="enviarPayRoll" title="Enviar PayRoll"><i class="fa fa-envelope"></i></a>
                        <a class="btn ink-reaction btn-floating-action btn-sm md-2x btn-primary accionesNomina" tipo="verPayRoll" title="Ver PayRoll de Pago"><i class="md md-my-library-books"></i></a>
                        <a class="btn ink-reaction btn-floating-action btn-sm md-2x btn-primary accionesNomina" tipo="verCalculos" title="Ver Calculos Generados"><i class="md md-insert-drive-file"></i></a>
                        <a class="btn ink-reaction btn-floating-action btn-sm md-2x btn-danger accionesNomina" tipo="generar" estado="{$estado}" title="Generar Nomina"><i class="md md-sync-problem"></i></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="pull-right">
                        <button id="seleccionarTodos" type="button">Seleccionar Todos</button>
                    </div>
                    <div class="pull-right">
                        <button id="deseleccionarTodos" type="button">Deseleccionar Todos</button>
                    </div>
                </div>
                <div class="col-sm-12" style="left: -20px;">
                    <select id="optgroup" checkbox="1" class="multiselect" multiple="multiple" name="form[int][pk_num_empleado][]" style="height: 400px;">
                        {if $listaEmpleados}
                            {foreach item=i from=$listaEmpleados}
                                {if in_array($i.pk_num_empleado,$listaEmpleadosSeleccionados)}
                                    <option value="{$i.pk_num_empleado}" selected>{$i.ind_cedula_documento} - {$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                {else}
                                    <option value="{$i.pk_num_empleado}">{$i.ind_cedula_documento} - {$i.ind_nombre1} {$i.ind_nombre2} {$i.ind_apellido1} {$i.ind_apellido2}</option>
                                {/if}
                            {/foreach}
                        {/if}
                    </select>
                </div>
                <span class="clearfix"></span>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $(function(){
            $.localise('ui.multiselect', { path: '{$_Parametros.ruta_Complementos}SelectMultipleTabla/js/locale/' });
            $("#optgroup").multiselect();
        });
        $("#seleccionarTodos").click(function () {
            $(".selected .empleadoSeleccionado").prop('checked', true); //solo los del objeto #diasHabilitados
        });
        $("#deseleccionarTodos").click(function () {
            $(".selected .empleadoSeleccionado").prop('checked', false); //solo los del objeto #diasHabilitados
        });

        $('.accionesNomina').click(function(){
            $('#modalAncho').css("width", "70%");
            var input = $('.selected .empleadoSeleccionado');
            var alerta =0;
            var idEmpleado = [];
            var cound=0;
            for(i=0;i<input.length;i++){
                if(input[i].checked == true ){
                    alerta = 1;
                    idEmpleado[cound]=input[i].getAttribute('idEmpleado');
                    cound++;
                }
            }

            if($(this).attr('tipo')!='generar'){
                if(alerta==0){
                    swal("Error!", 'Disculpa Debes Seleccionar por lo menos un Empleado poder Generar El Reporte', "error");
                }else{
                    $('#formModal').modal({
                        show: 'false'
                    });
                    if($(this).attr('tipo')=='verPayRoll'){
                        var titulo='PayRoll de Pago';
                    }else{
                        var titulo='Nomina';
                    }
                    $('#formModalLabel').html(titulo);
                    $('#ContenidoModal').html('<iframe frameborder="0" src="{$_Parametros.url}modNM/reportes/ejecucionProcesosCONTROL/verNominaMET/'+idEmpleado+'/'+$('#fk_nmb001_num_tipo_nomina').val()+'/'+$('#fk_nmb003_num_tipo_proceso').val()+'/'+$(this).attr('tipo')+'"  width="100%" height="540px"></iframe>');
                }
            } else {
                if(alerta==0){
                    swal("Error!", 'Disculpa no tienes empleados seleccionados para Generar la nomina', "error");
                } else {
                    if($(this).attr('estado')!='CR'){
                        swal({
                            title: 'Generar Nomina',
                            text: 'Usted esta seguro(a) que desea generar la Nomina???',
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: 'Si, Generala',
                            closeOnConfirm: false
                        }, function(){
                            swal({
                                title: "¡Por favor espere!",
                                text: "Se esta procesando su solicitud, puede demorar un poco.",
                                timer: 50000000,
                                showConfirmButton: false
                            });
                            var input = $('.selected .empleadoSeleccionado');
                            var alerta =0;
                            var idEmpleado = [];
                            var cound=0;
                            for(i=0;i<input.length;i++){
                                if(input[i].checked == true ){
                                    alerta = 1;
                                    idEmpleado[cound]=input[i].getAttribute('idEmpleado');
                                    cound++;
                                }
                            }
                            var form = {
                                int:
                                    {
                                        pk_num_proceso_periodo: $('#pk_num_proceso_periodo').val(),
                                        pk_num_tipo_nomina: $('#pk_num_tipo_nomina').val(),
                                        pk_num_empleado: idEmpleado
                                    }
                            };
                            $.post($("#formAjaxGenerarNomina").attr("action") + 'generarNominaMET',
                                { form: form, valido: 1 }
                                , function (dato) {
                                    if (dato['status'] == 'errorSQL') {
                                        swal("Error!", dato['mensaje'], "error");
                                    } else if (dato['status'] == 'generada') {
                                        swal("Nomina Generada!", "La Nomina fue Generada Satisfactoriamente.", "success");
                                    }
                                }, 'json');
                        });
                    } else {
                        swal("Error!", 'Disculpa el Periodo se encuentra cerrado', "error");
                    }
                }
            }

        });
        $('.accionesNomina2').click(function(){
            var input = $('.selected .empleadoSeleccionado');
            var alerta =0;
            var idEmpleado = [];
            var cound=0;
            for(i=0;i<input.length;i++){
                if(input[i].checked == true ){
                    alerta = 1;
                    idEmpleado[cound]=input[i].getAttribute('idEmpleado');
                    cound++;
                }
            }
            if(alerta==0){
                swal("Error!", 'Disculpa Debes Seleccionar por lo menos un Empleado poder Generar El Reporte', "error");
            }else{
                c=0;
                for(i=0;i<idEmpleado.length;i++){
                    $('#formModalLabel').html('Enviar PayRoll');
                    $('#ContenidoModal').html('');
                    $.post('{$_Parametros.url}modNM/reportes/ejecucionProcesosCONTROL/enviarPayRollMET/'+idEmpleado[i]+'/'+$('#fk_nmb001_num_tipo_nomina').val()+'/'+$('#fk_nmb003_num_tipo_proceso').val(), { valido: 1 }, function (dato) {
                        var id = dato['idEmpleado'];
                        var nomina = $('#fk_nmb001_num_tipo_nomina').val();
                        var proceso = $('#fk_nmb003_num_tipo_proceso').val();
                        if(id!='') {
                            $.post('{$_Parametros.url}modNM/reportes/ejecucionProcesosCONTROL/configurarCorreoMET', { idEmpleado: id, nomina: nomina, proceso: proceso }, function (dato2) {
                                if(dato2['mensaje']=='ok') {
                                    c=c+1;

                                }
                            }, 'json');
                        }
                    }, 'json');
                    if(c==i){
                        swal("Exito!", 'Envios Completados', "success");
                    } else {
                        swal("Error!", 'Disculpa hubo un error al enviar', "error");
                    }
                }
            }
        });
    });
</script>