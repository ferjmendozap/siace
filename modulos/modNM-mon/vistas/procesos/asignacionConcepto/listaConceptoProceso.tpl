<div class="col-sm-offset-1 col-sm-10">
    <div class="card">
        <div class="card-body">
            <span class="clearfix"></span>
            <div class="table-responsive no-margin">
                <table class="table table-striped no-margin" id="conceptoAsignadoEmpleado">
                    <thead>
                        <tr>
                            <th> # </th>
                            <th> Concepto </th>
                            <th> Desde </th>
                            <th> Hasta </th>
                            <th> Monto </th>
                            <th> Cantidad </th>
                            <th> Procesos </th>
                            <th> Estado </th>
                            <th> Acciones </th>
                        </tr>
                    </thead>
                    <tbody>
                    {if isset($lista)}
                        {foreach item=i from=$lista}
                            {if $i.pk_empleado_concepto != null}
                                <tr id="idConceptoAsignado{$i.pk_empleado_concepto}">
                                    <td> {$i.cod_concepto} </td>
                                    <td> {$i.ind_descripcion} </td>
                                    <td> {$i.fec_periodo_desde} </td>
                                    <td> {$i.fec_periodo_hasta} </td>
                                    <td> {$i.num_monto} </td>
                                    <td> {$i.num_cantidad} </td>
                                    <td> {$i.nm_b003_tipo_proceso} </td>
                                    <td> {$i.num_estatus} </td>
                                    <td>
                                        {if in_array('NM-01-01-01-02-M',$_Parametros.perfil)}
                                        <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idConceptoAsignado="{$i.pk_empleado_concepto}" title="Editar"
                                                descipcion="El Usuario a Modificado Un Concepto al empleado" titulo="<i class='fa fa-exchange'></i>Modificar el Concepto al Empleado">
                                            <i class="fa fa-edit" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                        &nbsp;&nbsp;
                                        {if in_array('NM-01-01-01-03-E',$_Parametros.perfil)}
                                        <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idConceptoAsignado="{$i.pk_empleado_concepto}"  boton="si, Eliminar" title="Eliminar"
                                                descipcion="El usuario a eliminado un Concepto Asignado" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar la el Concepto Asignado!!">
                                            <i class="md md-delete" style="color: #ffffff;"></i>
                                        </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/if}
                        {/foreach}
                    {/if}
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="9">
                                {if in_array('NM-01-01-01-01-N',$_Parametros.perfil)}
                                <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"
                                        descipcion="el Usuario a Agregado Un Nuevo Concepto al empleado"  titulo="<i class='fa fa-exchange'></i> Agregar Concepto al Empleado" id="nuevo" >
                                    <i class="md md-create"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Agregar Concepto
                                </button>
                                {/if}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var url='{$_Parametros.url}modNM/procesos/asignacionConceptoCONTROL/crearModificarMET';
        $('#nuevo').click(function(){
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idConceptoAsignado:0, idEmpleado:$('#codEmpleado').val() },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#conceptoAsignadoEmpleado tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post(url,{ idConceptoAsignado: $(this).attr('idConceptoAsignado'), idEmpleado:$('#codEmpleado').val() },function(dato){
                $('#ContenidoModal').html(dato);
            });
        });

        $('#conceptoAsignadoEmpleado tbody').on( 'click', '.eliminar', function () {

            var idConceptoAsignado=$(this).attr('idConceptoAsignado');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                var url='{$_Parametros.url}modNM/procesos/asignacionConceptoCONTROL/eliminarMET';
                $.post(url, { idConceptoAsignado: idConceptoAsignado },function(dato){
                    if(dato['status']=='OK'){
                        $(document.getElementById('idConceptoAsignado'+dato['idConceptoAsignado'])).remove();
                        swal("Eliminado!", "el Concepto Asignado fue eliminado satisfactoriamente.", "success");
                    }
                },'json');
            });
        });
    });
</script>