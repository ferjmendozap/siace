<form action="{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/iniciarModificarPeriodoMET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        {if $status =='AP' || $status =='CR'}
            <input type="hidden" value="1" name="ver" />
        {/if}
        <input type="hidden" value="{$status}" name="status" />
        <input type="hidden" value="1" name="valido" />
        <input type="hidden" value="{$idControlProceso}" name="idControlProceso"/>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group" id="fk_nmb001_num_tipo_nominaError">
                    <label for="fk_nmb001_num_tipo_nomina"><i class="md md-map"></i> Nomina</label>
                    <select {if $ver==1}disabled{/if} id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control select2-list select2">
                        <option value="">Seleccione La Nomina</option>
                        {foreach item=i from=$tipoNomina}
                            {if isset($formDB.fk_nmb001_num_tipo_nomina)}
                                {if $i.pk_num_tipo_nomina==$formDB.fk_nmb001_num_tipo_nomina}
                                    <option selected value="{$i.pk_num_tipo_nomina}">{$i.ind_nombre_nomina}</option>
                                {else}
                                    <option value="{$i.pk_num_tipo_nomina}">{$i.ind_nombre_nomina}</option>
                                {/if}
                            {else}
                                <option value="{$i.pk_num_tipo_nomina}">{$i.ind_nombre_nomina}</option>
                            {/if}
                        {/foreach}
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group" id="fk_nmb003_num_tipo_procesoError">
                    <label for="fk_nmb003_num_tipo_proceso"><i class="md md-map"></i> Tipo de Proceso</label>
                    <select {if $ver==1}disabled{/if} id="fk_nmb003_num_tipo_proceso" name="form[int][fk_nmb003_num_tipo_proceso]" class="form-control select2-list select2">
                        <option value="">Seleccione el Proceso</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group" id="fk_nmc001_num_tipo_nomina_periodoError">
                    <label for="fk_nmc001_num_tipo_nomina_periodo"><i class="md md-map"></i> Periodo</label>
                    <select {if $ver==1}disabled{/if} id="fk_nmc001_num_tipo_nomina_periodo" name="form[txt][fk_nmc001_num_tipo_nomina_periodo]" class="form-control select2-list select2">
                        <option value="">Seleccione el Periodo</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-8 col-sm-offset-2">
                <div class="form-group control-width-normal" id="fk_nmc001_num_tipo_nomina_periodoTXTError" style="display: none;">
                    <div class="input-group date" id="fk_nmc001_num_tipo_nomina_periodoTXT">
                        <div class="input-group-content">
                            <input {if $ver==1}disabled{/if} type="text" id="fk_nmc001_num_tipo_nomina_periodoTXT" class="form-control text-center" name="form[txt][fk_nmc001_num_tipo_nomina_periodoTXT]">
                            <label>Periodo</label>
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group control-width-normal" id="fec_desdeError">
                    <div class="input-group date" id="fec_desde">
                        <div class="input-group-content">
                            <input {if $ver==1}disabled{/if} type="text" id="fec_desde" class="form-control" name="form[txt][fec_desde]" value="{if isset($formDB.fec_desde)}{$formDB.fec_desde}{/if}">
                            <label>Periodo Desde</label>
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group control-width-normal" id="fec_hastaError">
                    <div class="input-group date" id="fec_hasta">
                        <div class="input-group-content">
                            <input {if $ver==1}disabled{/if} type="text" id="fec_hasta" class="form-control" name="form[txt][fec_hasta]" value="{if isset($formDB.fec_hasta)}{$formDB.fec_hasta}{/if}">
                            <label>Periodo Hasta</label>
                        </div>
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
            </div>
            {if isset($formDB.fk_rhb001_num_empleado_crea)}
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fk_rhb001_num_empleado_creaError">
                    <input type="text" class="form-control" value="{if isset($formDB.fk_rhb001_num_empleado_crea)}{$formDB.fk_rhb001_num_empleado_crea}{/if}" name="form[int][fk_rhb001_num_empleado_crea]" disabled id="fk_rhb001_num_empleado_crea">
                    <label for="fk_rhb001_num_empleado_crea"><i class="md md-person"></i> Creado Por</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fec_creadoError">
                    <input type="text" class="form-control" value="{if isset($formDB.fec_creado)}{$formDB.fec_creado}{/if}" disabled name="form[alphaNum][fec_creado]" id="fec_creado">
                    <label for="fec_creado"><i class="md md-today"></i> Fecha de Creacion</label>
                </div>
            </div>
            {/if}
            {if isset($formDB.fk_rhb001_num_empleado_aprueba)}
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fk_rhb001_num_empleado_apruebaError">
                    <input type="text" class="form-control" value="{if isset($formDB.fk_rhb001_num_empleado_aprueba)}{$formDB.fk_rhb001_num_empleado_aprueba}{/if}" name="form[int][fk_rhb001_num_empleado_aprueba]" disabled id="fk_rhb001_num_empleado_aprueba">
                    <label for="fk_rhb001_num_empleado_aprueba"><i class="md md-person"></i> Aprobado Por</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fec_aprobadoError">
                    <input type="text" class="form-control" value="{if isset($formDB.fec_aprobado)}{$formDB.fec_aprobado}{/if}" disabled name="form[alphaNum][fec_aprobado]" id="fec_aprobado">
                    <label for="fec_aprobado"><i class="md md-today"></i> Fecha Aprobado</label>
                </div>
            </div>
            {/if}
            {if isset($formDB.fk_rhb001_num_empleado_procesa)}
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fk_rhb001_num_empleado_procesaError">
                    <input type="text" class="form-control" value="{if isset($formDB.fk_rhb001_num_empleado_procesa)}{$formDB.fk_rhb001_num_empleado_procesa}{/if}" name="form[int][fk_rhb001_num_empleado_procesa]" disabled id="fk_rhb001_num_empleado_procesa">
                    <label for="fk_rhb001_num_empleado_procesa"><i class="md md-person"></i> Procesado Por</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fec_procesadoError">
                    <input type="text" class="form-control" value="{if isset($formDB.fec_procesado)}{$formDB.fec_procesado}{/if}" disabled name="form[alphaNum][fec_procesado]" id="fec_procesado">
                    <label for="fec_procesado"><i class="md md-today"></i> Fecha de Proceso</label>
                </div>
            </div>
            {/if}
            {if isset($formDB.fk_rhb001_num_empleado_cierra)}
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fk_rhb001_num_empleado_cierraError">
                    <input type="text" class="form-control" value="{if isset($formDB.fk_rhb001_num_empleado_cierra)}{$formDB.fk_rhb001_num_empleado_cierra}{/if}" name="form[int][fk_rhb001_num_empleado_cierra]" disabled id="fk_rhb001_num_empleado_cierra">
                    <label for="fk_rhb001_num_empleado_cierra"><i class="md md-person"></i> Cerrado Por</label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group floating-label" id="fec_cerradoError">
                    <input type="text" class="form-control" value="{if isset($formDB.fec_cerrado)}{$formDB.fec_cerrado}{/if}" disabled name="form[alphaNum][fec_cerrado]" id="fec_cerrado">
                    <label for="fec_cerrado"><i class="md md-today"></i> Fecha del Cierre</label>
                </div>
            </div>
            {/if}
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group floating-label">
                            <input type="text" disabled class="form-control disabled" value="{if isset($formDB.ind_usuario)}{$formDB.ind_usuario}{/if}" id="ind_usuario">
                            <label for="ind_usuario"><i class="md md-person"></i> Ultimo Usuario</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="col-sm-12">
                    <div class="form-group floating-label" id="fec_ultima_modificacionError">
                        <input type="text" disabled class="form-control disabled" value="{if isset($formDB.fec_ultima_modificacion)}{$formDB.fec_ultima_modificacion}{/if}" id="fec_ultima_modificacion">
                        <label for="fec_ultima_modificacion"><i class="fa fa-calendar"></i> Ultima Modificacion</label>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>
    </div>
    {if $ver==0 || $status=='AP' || $status=='CR'}
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
            {if $status=='AP'}
                <i class="icm icm-rating3"></i>&nbsp;Aprobar
            {elseif $status=='CR'}
                Cerrar
            {elseif $idControlProceso !=0}
                <i class="fa fa-edit"></i>&nbsp;Modificar
            {else}
                <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar
            {/if}
        </button>
    </div>
    {/if}
</form>

<script type="text/javascript">
$(document).ready(function() {
    var app = new  AppFunciones();
    $("#formAjax").submit(function(){
        return false;
    });

    $('#modalAncho').css( "width", "39%" );
    $('#fec_desde').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
    $('#fec_hasta').datepicker({ autoclose: true, todayHighlight: true, format: "yyyy-mm-dd", language:'es' });
    $('#fk_nmc001_num_tipo_nomina_periodoTXT').datepicker({ autoclose: true, todayHighlight: true, minViewMode: 1, format: "yyyy-mm", language:'es' });
    $('.select2').select2({ allowClear: true });
    $('#fk_nmc001_num_tipo_nomina_periodo').change(function(){
        if($('#fk_nmc001_num_tipo_nomina_periodo').val()=='otros'){
            $('#fk_nmc001_num_tipo_nomina_periodoError').css('display','none');
            $('#fk_nmc001_num_tipo_nomina_periodoTXTError').css('display','block');
        }
    });
    app.metJsonProcesoNomina('{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/procesosNominaMET','{if isset($formDB.fk_nmb001_num_tipo_nomina)}{$formDB.fk_nmb001_num_tipo_nomina}{/if}','{if isset($formDB.fk_nmb003_num_tipo_proceso)}{$formDB.fk_nmb003_num_tipo_proceso}{/if}');
    app.metJsonPeriodoNomina('{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/periodosDisponiblesMET','{if isset($formDB.fk_nmb001_num_tipo_nomina)}{$formDB.fk_nmb001_num_tipo_nomina}{/if}','{if isset($formDB.fk_nmc001_num_tipo_nomina_periodo)}{$formDB.fk_nmc001_num_tipo_nomina_periodo}{/if}');
    $('#accion').click(function(){
        swal({
            title: "¡Por favor espere!",
            text: "Se esta procesando su solicitud, puede demorar un poco.",
            timer: 50000000,
            showConfirmButton: false
        });
        $.post($("#formAjax").attr("action"), $( "#formAjax" ).serialize(),function(dato){
            var arrayCheck = ["num_estatus"];
            var arrayMostrarOrden = ['fk_nmb001_num_tipo_nomina','fec_anio','fec_mes','fk_nmb003_num_tipo_proceso','ind_estado'];
            if(dato['status']=='error'){
                app.metValidarError(dato,'Disculpa. los campos marcados con X en rojo son obligatorios');
            }else if(dato['status']=='errorSQL'){
                app.metValidarError(dato,'Disculpa. el(los) campo(s) seleccionados con una X son UNICOS coloque otro valor');
            }else if(dato['status']=='modificar'){
                app.metActualizarRegistroTablaJson('dataTablaJson','El Proceso fue modificado satisfactoriamente.','cerrarModal','ContenidoModal');
            }else if(dato['status']=='nuevo'){
                app.metNuevoRegistroTablaJson('dataTablaJson','El Proceso fue guardado satisfactoriamente.','cerrarModal','ContenidoModal');
            }else if(dato['status']=='aprobado'){
                app.metAprobarRegistroJson('dataTablaJson','El Proceso fue aprobado satisfactoriamente.','cerrarModal','ContenidoModal');
            }else if(dato['status']=='cerrado'){
                app.metActualizarRegistroTablaJson('dataTablaJson','El Proceso fue cerrado satisfactoriamente.','cerrarModal','ContenidoModal');
            }
        },'json');
    });





});
</script>