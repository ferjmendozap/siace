<section class="style-default-bright">
    <div class="section-header">
        <h3 class="text-primary">Anticipo Prestaciones Sociales</h3>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form class="form-horizontal" role="form">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label for="codEmpleado" class="col-sm-6 control-label">Cod. Empleado</label>
                                    <div class="col-sm-6">
                                        {$empleado.cod_empleado}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="nomApe" class="col-sm-4 control-label">Nombre y Apellido</label>
                                    <div class="col-sm-8">
                                        {$empleado.ind_nombre1} {$empleado.ind_apellido1}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="tipoNomina" class="col-sm-4 control-label">Tipo de Nomina</label>
                                    <div class="col-sm-8">
                                        {$empleado.ind_nombre_nomina}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="col-sm-7">
                                <div class="form-group">
                                    <label for="fecIngreso" class="col-sm-6 control-label">Fecha de Ingreso</label>
                                    <div class="col-sm-6">
                                        {$empleado.fec_ingreso}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<div class="col-sm-12">
    <div class="card">
        <div class="card-body">
            <span class="clearfix"></span>
            <div class="table-responsive no-margin">
                <table class="table table-striped no-margin" style="overflow:scroll; width:1900px;" id="datatable2">
                    <thead>
                    <tr>
                        <th width="100" scope="col" class="text-center">PERIODO</th>
                        <th width="150" scope="col" class="text-center">SUELDO BASICO</th>
                        <th width="80" scope="col" class="text-center">TOTAL ASIGNACIONES</th>
                        <th width="60" scope="col" class="text-center">SUELDO NORMAL</th>
                        <th width="60" scope="col" class="text-center">ALI. VAC.</th>
                        <th width="60" scope="col" class="text-center">ALI. FIN AÑO</th>
                        <th width="60" scope="col" class="text-center">BONO FISCAL</th>
                        <th width="60" scope="col" class="text-center">BONO ESPECIAL</th>
                        <th width="80" scope="col" class="text-center">REMUNERACION MENSUAL</th>
                        <th width="80" scope="col" class="text-center">REMUNERACION DIARIA</th>
                        <th width="35" scope="col" class="text-center">DIAS TRIMESTRAL</th>
                        <th width="80" scope="col" class="text-center">DIAS ANUALES</th>
                        <th width="80" scope="col" class="text-center">MONTO TRIMESTRAL</th>
                        <th width="80" scope="col" class="text-center">MONTO ANUAL</th>
                        <th width="50" scope="col" class="text-center">COMPLEMENTO</th>
                        <th width="50" scope="col" class="text-center">CAPITALIZACION</th>
                        <th width="80" scope="col" class="text-center">APORTES</th>
                        <th width="80" scope="col" class="text-center">ANTICIPO</th>
                        <th width="80" scope="col" class="text-center">INTERESES</th>
                        <th width="80" scope="col" class="text-center">ACUMULADO</th>
                    </tr>
                    </thead>
                    <tbody>
                    {foreach item=i from=$lista}
                        <tr>
                            <th width="100" scope="col" class="text-center">{$i.periodo}</th>
                            <th width="150" scope="col" class="text-right">{$i.num_sueldo_base}</th>
                            <th width="80" scope="col" class="text-right">{$i.num_total_asignaciones}</th>
                            <th width="60" scope="col" class="text-right">{$i.num_sueldo_normal}</th>
                            <th width="60" scope="col" class="text-right">{$i.num_alicuota_vacacional}</th>
                            <th width="60" scope="col" class="text-right">{$i.num_alicuota_fin_anio}</th>
                            <th width="60" scope="col" class="text-right">{$i.num_bono_especial}</th>
                            <th width="60" scope="col" class="text-right">{$i.num_bono_fiscal}</th>
                            <th width="80" scope="col" class="text-right">{$i.num_remuneracion_mensual}</th>
                            <th width="80" scope="col" class="text-right">{$i.num_remuneracion_diaria}</th>
                            <th width="35" scope="col" class="text-center">{$i.num_dias_trimestre}</th>
                            <th width="80" scope="col" class="text-center">{$i.num_dias_anual}</th>
                            <th width="80" scope="col" class="text-right">{$i.num_monto_trimestral}</th>
                            <th width="80" scope="col" class="text-right">{$i.num_monto_anual}</th>
                            <th width="50" scope="col" class="text-right"></th>
                            <th width="50" scope="col" class="text-right"></th>
                            <th width="50" scope="col" class="text-right">{$i.num_aportes}</th>
                            <th width="80" scope="col" class="text-right">{$i.num_anticipo}</th>
                            <th width="80" scope="col" class="text-right">{$i.num_monto_interes}</th>
                            <th width="80" scope="col" class="text-right">{$i.num_monto_acumulado}</th>
                        </tr>
                    {/foreach}
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="15" class="text-right">Detalles</th>
                        <th colspan="2" class="text-center">Pagado</th>
                        <th colspan="2" class="text-center">Por Pagar</th>
                    </tr>
                    <tr>
                        <th colspan="15" class="text-right">Acumulado:</th>
                        <th colspan="2" class="text-right">{$pagado}</th>
                        <th colspan="2" class="text-right">{$porPagar}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>