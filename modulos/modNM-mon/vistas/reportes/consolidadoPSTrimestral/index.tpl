<section class="style-default-bright">
    <div class="section-header">
        <h3 class="text-primary">Consolidado Prestaciones Sociales Trimestrales</h3>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form class="form-horizontal" role="form">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-7 control-label" for="fec_anio">Año</label>
                                <div class="col-sm-4">
                                    <select id="fec_anio" name="form[int][fec_anio]" class="form-control select2-list">
                                        <option value="">Seleccione el año</option>
                                        {foreach item=i from=$lista}
                                            <option value="{$i.fec_anio}">{$i.fec_anio}</option>
                                        {/foreach}
                                    </select>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="trimestre">Trimestre</label>
                                <div class="col-sm-4">
                                    <select id="trimestre"  class="form-control select2-list">
                                        <option value=""></option>
                                        {for $i=1;$i<=4;$i++}
                                            <option value="{$i}">Trimestre {$i}</option>
                                        {/for}
                                    </select>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    <div class="form-control-line"></div>
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-xs btn-primary" id="selectEmpleado" titulo="Ver Reporte">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<br>
<div class="row" id="pdfConsolidadoPSTrimestral">

</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#selectEmpleado').click(function(){
            if($('#fec_anio').val()=='' || $('#trimestre').val()==''){
                swal("Error!", 'Disculpa Debes Seleccionar el Año y Trimestre a Buscar!!', "error");
            }else{
                $('#pdfConsolidadoPSTrimestral').html('<iframe frameborder="0" src="{$_Parametros.url}modNM/reportes/consolidadoPSTrimestralCONTROL/PdfConsolidadoPSTrimestralMET/'+$('#fec_anio').val()+'/'+$('#trimestre').val()+'"  width="100%" height="540px"></iframe>');
            }
        });
    });
</script>