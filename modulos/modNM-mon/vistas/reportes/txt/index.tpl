<section class="style-default-bright">
    <div class="section-header">
        <h3 class="text-primary">TXT de Nómina</h3>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form class="form-horizontal" role="form">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="fk_nmc001_num_tipo_nomina_periodo">Periodos</label>
                            <div class="col-sm-9">
                                <select id="fk_nmc001_num_tipo_nomina_periodo" name="form[txt][fk_nmc001_num_tipo_nomina_periodo]" class="form-control select2-list select2">
                                    <option value="">Seleccione el Periodo</option>
                                </select>
                                <div class="form-control-line"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="fk_nmb003_num_tipo_proceso">Procesos</label>
                                <div class="col-sm-9">
                                    <select id="fk_nmb003_num_tipo_proceso" name="form[int][fk_nmb003_num_tipo_proceso]" class="form-control input-sm select2-list select2">
                                        <option value="0">Seleccione el Proceso</option>
                                    </select>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div>    
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="col-sm-12">
                         <div class="form-group">
                               <label class="col-sm-2 control-label" for="txt_nombre">Nombre del Archivo</label>
                               <div class="col-sm-9">
                                    <input type="text" class="form-control" value="" name="form[txt][txt_nombre]" id="txt_nombre">
                               </div> 
                         </div>   
                     </div>   
                    <div class="form-control-line"></div>
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-xs btn-primary" id="btnGenerar" titulo="Generar txt">Generar TXT</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<br>
<div class="row" id="pdfNominaConsolidada">

</div>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new  AppFunciones();
        //Periodos
        $.post('{$_Parametros.url}modNM/reportes/txtCONTROL/periodosDisponiblesTxtMET', { }, function (dato) {
                    for (var i = 0; i < dato.length; i++) {
                        $('#fk_nmc001_num_tipo_nomina_periodo').append('<option value="' + dato[i]['pk_num_proceso_periodo'] + '">' + dato[i]['periodo']+'</option>');
                    }
                }, 'json');
        //Procesos
        $.post('{$_Parametros.url}modNM/reportes/txtCONTROL/ProcesosDisponiblesTxtMET', { }, function (dato) {
            for (var i = 0; i < dato.length; i++) {
                $('#fk_nmb003_num_tipo_proceso').append('<option value="' + dato[i]['pk_num_tipo_proceso'] + '">' + dato[i]['ind_nombre_proceso']+'</option>');
            }
        }, 'json');
        /*app.metJsonPeriodoProcesosDisponibles('{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/procesosDisponiblesMET','fk_nmc001_num_tipo_nomina_periodo','fk_nmb003_num_tipo_proceso','fk_nmb001_num_tipo_nomina','pk_num_proceso_periodo','ind_nombre_proceso','Seleccione El Proceso',false,'procesos');*/


        
        $('#btnGenerar').click(function(){
            if($('#fk_nmc001_num_tipo_nomina_periodo').val()=='' || $('#fk_nmb003_num_tipo_proceso').val()=='' || $('#txt_nombre').val()==''){
                if($('#fk_nmc001_num_tipo_nomina_periodo').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar un Periodo', "error");
                }else if($('#fk_nmb003_num_tipo_proceso').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar un Proceso', "error");
                }else{
                    swal("Error!", 'Disculpa Indicar el Nombre del TXT', "error");
                }
            }else{
                var url='{$_Parametros.url}modNM/reportes/txtCONTROL/GenerarTxtMET';
                $.post(url, { txt_nombre: $("#txt_nombre").val(), proceso: $("#fk_nmb003_num_tipo_proceso").val()  }, function (dato) {

                    if(dato==1){

                        window.open("{$_Parametros.url}modNM/reportes/txtCONTROL/DescargarTxtMET/"+$("#txt_nombre").val(), "wPrincipal", "toolbar=no, menubar=no, location=no, scrollbars=yes, height=800, width=800, left=200, top=200, resizable=yes");
                        
                    }
                    
                },'json');

                /*$('#pdfNominaConsolidada').html('<iframe frameborder="0" src="{$_Parametros.url}modNM/reportes/nominaConsolidadaCONTROL/pdfNominaConsolidadaMET/'+$('#fk_nmb003_num_tipo_proceso').val()+'"  width="100%" height="540px"></iframe>');*/
            }
        });
    });
</script>