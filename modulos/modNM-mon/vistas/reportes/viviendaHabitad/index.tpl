<section class="style-default-bright">
    <div class="section-header">
        <h3 class="text-primary">Reporte de Retenciones de Vivienda y Hábitat</h3>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <form class="form-horizontal" role="form">
                <div class="col-sm-offset-1 col-sm-10">
                    <div class="col-sm-6">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="fk_nmb001_num_tipo_nomina">Nominas</label>
                                <div class="col-sm-9">
                                    <select id="fk_nmb001_num_tipo_nomina" name="form[int][fk_nmb001_num_tipo_nomina]" class="form-control select2-list">
                                        <option value="">Seleccione La Nomina</option>
                                        {foreach item=i from=$tipoNomina}
                                            <option value="{$i.pk_num_tipo_nomina}">{$i.ind_nombre_nomina}</option>
                                        {/foreach}
                                    </select>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="fk_nmb003_num_tipo_proceso">Procesos</label>
                                <div class="col-sm-9">
                                    <select id="fk_nmb003_num_tipo_proceso" name="form[int][fk_nmb003_num_tipo_proceso]" class="form-control select2-list">
                                        <option value="">Seleccione el Proceso</option>
                                    </select>
                                    <div class="form-control-line"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="fk_nmc001_num_tipo_nomina_periodo">Periodos</label>
                            <div class="col-sm-9">
                                <select id="fk_nmc001_num_tipo_nomina_periodo" name="form[txt][fk_nmc001_num_tipo_nomina_periodo]" class="form-control select2-list select2">
                                    <option value="">Seleccione el Periodo</option>
                                </select>
                                <div class="form-control-line"></div>
                            </div>
                        </div>

                    </div>
                    <div class="form-control-line"></div>
                    <div class="col-sm-12 text-center">
                        <button type="button" class="btn btn-xs btn-primary" id="selectEmpleado" titulo="Ver Reporte">Buscar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<br>
<div class="row" id="pdfDiv">

</div>
<script type="text/javascript">
    $(document).ready(function () {
        var app = new  AppFunciones();
        //Periodos
        app.metJsonPeriodoProcesosDisponibles('{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/procesosDisponiblesMET','fk_nmb001_num_tipo_nomina','fk_nmc001_num_tipo_nomina_periodo','fk_nmb001_num_tipo_nomina','pk_num_tipo_nomina_periodo','fk_nmc001_num_tipo_nomina_periodo','Seleccione El Periodo',false,false);
        //Procesos
        app.metJsonPeriodoProcesosDisponibles('{$_Parametros.url}modNM/procesos/controlProcesosCONTROL/procesosDisponiblesMET','fk_nmc001_num_tipo_nomina_periodo','fk_nmb003_num_tipo_proceso','fk_nmb001_num_tipo_nomina','pk_num_proceso_periodo','ind_nombre_proceso','Seleccione El Proceso',false,'procesos');
        var $url='{$_Parametros.url}modNM/procesos/interfazCxPCONTROL/listadoObligacionesMET';
        $('#selectEmpleado').click(function(){
            if($('#fk_nmb003_num_tipo_proceso').val()=='' || $('#fk_nmb001_num_tipo_nomina').val()=='' || $('#fk_nmc001_num_tipo_nomina_periodo').val()==''){
                if($('#fk_nmb001_num_tipo_nomina').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar una Nomina', "error");
                }else if($('#fk_nmc001_num_tipo_nomina_periodo').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar EL Periodo', "error");
                }else if($('#fk_nmb002_num_concepto').val()==''){
                    swal("Error!", 'Disculpa Debes Seleccionar EL Concepto', "error");
                }else{
                    swal("Error!", 'Disculpa Debes Seleccionar un Proceso', "error");
                }
            }else{
                $('#pdfDiv').html('<iframe frameborder="0" src="{$_Parametros.url}modNM/reportes/viviendaHabitadCONTROL/pdfViviendaHabitadMET/'+$('#fk_nmb003_num_tipo_proceso').val()+'"  width="100%" height="540px"></iframe>');
            }
        });
    });
</script>