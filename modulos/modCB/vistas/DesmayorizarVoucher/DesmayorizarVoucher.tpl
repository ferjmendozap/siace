<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Desmayorizar Voucher</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id.</th>
                                <th>Período</th>
                                <th>Nro. Voucher</th>
                                <th>Clasificación</th>
                                <th>Descripción</th>
                                <th>Estado</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=registro from=$listado}
                                <tr id="id{$registro.pk_num_voucher_mast}">
                                    <td><label>{$registro.pk_num_voucher_mast}</label></td>
                                    <td><label>{$registro.ind_anio}-{$registro.ind_mes}</label></td>
                                    <td><label>{$registro.ind_voucher}</label></td>
                                    <td><label>{ConsultarDescripcion::consultar($registro.fk_cbc003_num_voucher,'pk_num_voucher','cb_c003_tipo_voucher')}</label></td>
                                    <td><label>{$registro.txt_titulo_voucher}</label></td>
                                    <td><label>{ConsultarDescripcion::options('cb_estado_voucher',$registro.ind_estatus,'0')}</label></td>
                                    <td align="center">
                                        <button class="logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" idRegistro="{$registro.pk_num_voucher_mast}"  boton="Desmayorizar" title="Desmayorizar"
                                                    descipcion="El usuario mayorizó un post" titulo="Estas Seguro?" mensaje="Esta seguro que desea desmayorizar el Voucher!!" id="desmayorizar_voucher" title="Desmayorizar">
                                                <i class="md md-swap-horiz" style="color: #ffffff;"></i>
                                        </button>
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
   $(document).ready(function() {
     
        //  base
        var $base = '{$_Parametros.url}modCB/DesmayorizarVoucherCONTROL/';

        // Desmayorizar Voucher
        $('#datatable1 tbody').on( 'click', '#desmayorizar_voucher', function () {
            var id = $(this).attr('idRegistro');
                swal({
                    title: $(this).attr('titulo'),
                    text: $(this).attr('mensaje'),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $(this).attr('boton'),
                    closeOnConfirm: false
                }, function(){
                    $.post($base+'desmayorizarVoucherMET', { id: id }, function(dato){
                            $(document.getElementById('id'+id)).remove();
                            swal("Operación Exitosa!", dato['mensaje'], "success");
                            $('#cerrar').click();
                   },'json');
                });
        });

    });
</script>
