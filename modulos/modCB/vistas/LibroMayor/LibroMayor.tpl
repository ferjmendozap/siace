<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary"> Libro Mayor </h2></header>
    </div>

    <div class="card-head">
        <header></header>
    </div>

    <form class="form" action="{$_Parametros.url}modCB/LibroMayorCONTROL/libroMayorReportePdfMET" method="post" target="_blank">
        <div class="col-lg-12">
            <div class="col-lg-1"></div>
            <div class="col-lg-2">
              <div class="form-group" id="fk_cbb005_num_contabilidadesError">
                <select id="fk_cbb005_num_contabilidades" name="fk_cbb005_num_contabilidades" class="form-control">
                     {Select::lista('cb_b005_contabilidades', 'pk_num_contabilidades', 'ind_descripcion', $form.fk_cbb005_num_contabilidades)}
                </select>
                <label for="fk_cbb005_num_contabilidades">Contabilidad</label>
             </div>
            </div>

            <div class="col-lg-1"></div>

            <div class="form-group col-lg-1">
                <div id="fechas" class="input-daterange input-group">
                    <div class="input-group-content">
                        <input class="form-control" type="text" id="periodo" name="periodo" value="{date("Y-m")}" style="text-align: center;" readonly >
                        <label>Período</label>
                    </div>
                </div>
            </div>

            <div class="col-lg-1"></div>

            <div class="col-lg-2">
                <div class="form-group">
                      <select id="tipo_cuenta" name="tipo_cuenta" class="form-control" >
                        {Select::options('Tipo_Cuenta','','0')}
                     </select>
                     <label for="tipo_cuenta">Tipo Cuenta</label>
                </div>
            </div>

        </div>

        <div class="col_lg_12">

            <div class="col-lg-1"></div>
            <div class="col-lg-2">
              <div class="form-group" id="fk_cbb003_num_libro_contabilidadError">
                <select id="fk_cbb002_num_libro_contabilidad" name="fk_cbb002_num_libro_contabilidad" class="form-control">
                     {Select::lista('cb_b002_libro_contable', 'pk_num_libro_contable', 'ind_descripcion', $form.pk_num_libro_contable)}
                </select>
                <label for="fk_cbb002_num_libro_contabilidad">Libro Contable</label>
             </div>
            </div>

            <div class="col-lg-1"></div>

            <div class="col-lg-2">
                <div class="form-group">
                    <input type="hidden" name="fk_cbb004_num_cuenta0" id="fk_cbb004_num_cuenta0">
                    <input type="text" name="num_cuenta0" id="num_cuenta0" class="form-control" maxlength="15" onchange="obtenerCuenta($(this));" readonly>
                    <label for="num_cuenta">Cuenta Desde:</label>
                </div>
            </div>

            <div class="col-lg-1">
                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                       style="margin: 0px 0px -45px -10px;"
                       data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                    onclick="selector($(this),'{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta0','num_cuenta0'], 'Contabilidad='+$('#fk_cbb005_num_contabilidades').val());">
                    <i class="md md-search"></i>
                </button>
            </div>

            <div class="col-lg-2">
                <div class="form-group ">
                    <input type="hidden" name="fk_cbb004_num_cuenta1" id="fk_cbb004_num_cuenta1">
                    <input type="text" name="num_cuenta1" id="num_cuenta1" class="form-control" maxlength="15" onchange="obtenerCuenta($(this));" readonly>
                    <label for="num_cuenta1">Cuenta Hasta:</label>
                </div>
            </div>

            <div class="col-lg-1">
                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                       style="margin: 0px 0px -45px -10px;"
                       data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                    onclick="selector($(this),'{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['fk_cbb004_num_cuenta1','num_cuenta1'], 'Contabilidad='+$('#fk_cbb005_num_contabilidades').val());">
                    <i class="md md-search"></i>
                </button>
            </div>

        </div>


        <div class="col-lg-12" style="height: 100px;">
            <div class="col-lg-1"></div>
             <div align="center">
                 <button type="submit" class="btn ink-reaction btn-raised btn-primary" id="btnConsultar" style="margin-top:7px">Buscar</button>
             </div>
        </div>


    </form>
</div>

<script type="text/javascript">

    $('#periodo').datepicker({
        format: "yyyy-mm",
        minViewMode: 1,
        maxViewMode: 2,
        language: "es"
    });

    $(document).ready(function() {
        $('#modalAncho').css("width", "85%");
        //$('#fechas').datepicker({ format: 'dd/mm/yyyy' });
    });

</script>
