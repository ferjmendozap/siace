<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Grupo Centro Costo</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id.</th>
                                <th>Código</th>
                                <th class="col-sm-6">Descripción</th>
                                <th>Estado</th>
                                <th>Acción</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=registro from=$listado}
                                <tr id="id{$registro.pk_num_grupo_centro_costo}">
                                    <td><label>{$registro.pk_num_grupo_centro_costo}</label></td>
                                    <td><label>{$registro.cod_grupo_centro_costo}</label></td>
                                    <td><label>{$registro.ind_descripcion}</label></td>
                                    <td><i class="{if $registro.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    <td align="center">
                                        {if in_array('CB-01-90-06-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_grupo_centro_costo}" title="Editar"
                                                    descipcion="El Usuario a Modificado un post" titulo="Editar Grupo Centro Costo">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}

                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_grupo_centro_costo}" title="Ver"
                                                descipcion="El Usuario esta viendo Plan de Cuentas" titulo="Ver Grupo Centro Costo">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                                        {if in_array('CB-01-90-06-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idRegistro="{$registro.pk_num_grupo_centro_costo}"  boton="si, Eliminar" title="Eliminar"
                                                    descipcion="El usuario a eliminado un post" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="5">
                                {if in_array('CB-01-90-06-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"  descipcion="el Usuario a creado un post"  titulo="Nuevo Grupo Centro Costo" id="nuevo" ><i class="md md-create"></i>&nbsp;Nuevo Grupo Centro Costo</button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

   $(document).ready(function() {

        //  base
        var $base = '{$_Parametros.url}modCB/GrupoCentroCostoCONTROL/';

        //  nuevo registro
        $('#nuevo').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
                $.post($base+'nuevoMET', '', function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        // modificar
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'editarMET', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        // ver
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'verMET', { id: $(this).attr('idRegistro'), estado: 'ver' }, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var id = $(this).attr('idRegistro');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                $.post($base+'eliminarMET', { id: id }, function(dato){
                    //  mensaje
                    if (dato['valor']=="1") {
                        $(document.getElementById('id'+id)).remove();
                        swal("Registro Eliminado!", dato['mensaje'], "success");
                        $('#cerrar').click();
                    }else{
                        swal("Registro No Eliminado!", dato['mensaje'], "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });

    });
</script>
