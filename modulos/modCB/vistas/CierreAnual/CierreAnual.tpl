<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary"> Cierre Anual </h2></header>
    </div>

    <div class="card-head">
        <header></header>
    </div>

    <form class="form" action="{$_Parametros.url}modCB/CierreAnualCONTROL/CierreAnualPdfMET" method="post" target="_blank">
        <div class="col-lg-12"> 
            <div class="col-lg-1"></div>
            <div class="col-lg-2">
              <div class="form-group" id="fk_cbb005_num_contabilidadesError">
                <select id="fk_cbb005_num_contabilidades" name="fk_cbb005_num_contabilidades" class="form-control">
                     {Select::lista('cb_b005_contabilidades', 'pk_num_contabilidades', 'ind_descripcion', $form.fk_cbb005_num_contabilidades)}
                </select>
                <label for="fk_cbb005_num_contabilidades">Contabilidad</label>
             </div>
            </div>

            <div class="col-lg-1"></div>

            <div class="col-lg-1">
                    <div class="form-group" id="fk_cbc005_num_control_cierreError">
                        <select id="fk_cbc005_num_control_cierre" name="fk_cbc005_num_control_cierre" class="form-control">
                            {Select::lista3('cb_c005_control_cierre_mensual', 'pk_num_control_cierre', 'fec_anio', $form.fk_cbc005_num_control_cierre)}
                        </select>
                        <label>Período</label>
                    </div>
            </div>

            <div class="col-lg-1">
               <button type="button" class="btn btn-default ink-reaction btn-raised" id="btnEjecutar" style="margin-top:7px">Ejecutar Cierre</button>
            </div>

        </div>


        <div class="col-lg-12" style="height: 100px;">
            <div class="col-lg-1"></div>
              <!--
                 <button type="submit" class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:7px">Consultar</button>
                 </button>

             <div class="modal-footer">
                <button type="submit" class="btn ink-reaction btn-raised btn-info" id="btnConsultar" style="margin-top:7px">Consultar</button>
                 </button>
             </div>-->
             <div align="center">
                <button type="submit" class="btn ink-reaction btn-raised btn-primary" id="btnConsultar" style="margin-top:7px">Buscar</button>
             </div>
             <!--
             <div align="center">
                <button type="submit" class="btn btn-default ink-reaction btn-raised" id="btnEjecutar" style="margin-top:7px">Ejecutar Cierre</button>
             </div>
             -->
        </div>


    </form>
</div>

<script type="text/javascript">
    /*
    $('#periodo').datepicker({
        format: "yyyy-mm",
        minViewMode: 1,
        maxViewMode: 2,
        language: "es"
    });
    */

    $(document).ready(function() {
      //  base
      var $base = '{$_Parametros.url}modCB/CierreAnualCONTROL/';

        $('#modalAncho').css("width", "85%");
        $('#fechas').datepicker({ format: 'dd/mm/yyyy' });

        $('#btnEjecutar').click(function() {  alert($base+'ejecutarCierreMET', "fperiodo="+$("#fk_cbc005_num_control_cierre").val()+"&fcontabilidad="+$("#fk_cbb005_num_contabilidades").val());//alert('si no'+ $("#fk_cbc005_num_control_cierre").val());
           //$.post($base+'ejecutarCierreMET'+"&fperiodo="+$("#fk_cbc005_num_control_cierre").val()+"&fcontabilidad="+$("#fk_cbb005_num_contabilidades").val(), function(dato){
           $.post($base+'ejecutarCierreMET', "fperiodo="+$("#fk_cbc005_num_control_cierre").val()+"&fcontabilidad="+$("#fk_cbb005_num_contabilidades").val(),function(dato){
                alert("datos="+dato);
                swal("Operación Exitosa!", "Cierre Anual Creado", "success");
                $('#cerrar').click();
           //},'json');
           });
        });
    });

</script>
