<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary"> Estado Resultado </h2></header>
    </div>

    <div class="card-head">
        <header></header>
    </div>

    <form class="form" action="{$_Parametros.url}modCB/EstadoResultadoCONTROL/balanceReportePdfMET" method="post" target="_blank">
        <div class="col-lg-12">
            <div class="col-lg-2">
              <div class="form-group" id="fk_cbb005_num_contabilidadesError">
                <select id="fk_cbb005_num_contabilidades" name="fk_cbb005_num_contabilidades" class="form-control">
                     {Select::lista('cb_b005_contabilidades', 'pk_num_contabilidades', 'ind_descripcion', $form.fk_cbb005_num_contabilidades)}
                </select>
                <label for="fk_cbb005_num_contabilidades">Contabilidad</label>
             </div>
            </div>

            <div class="col-lg-1"></div>

            <div class="form-group col-lg-2">
                <div class="form-control">
                    <input class="form-control" type="text" id="periodo_desde" name="periodo_desde" value="{date("Y-m")}" style="text-align: center;" readonly />
                    <label>P.Desde</label>
                </div>
            </div>

            <div class="col-lg-1"></div>

            <div class="col-lg-2">
                 <div class="form-group">
                     <input class="form-control" type="text" id="periodo_hasta" name="periodo_hasta" value="{date("Y-m")}" style="text-align: center;" readonly />
                     <label for="periodo_hasta">P. Hasta</label>
                 </div>
            </div>

            <div class="col-lg-1"></div>

            <div class="col-lg-2">
                <div class="form-group">
                      <select id="tipo_cuenta" name="tipo_cuenta" class="form-control" >
                        {Select::options('Tipo_Cuenta','','0')}
                     </select>
                     <label for="tipo_cuenta">Tipo Cuenta</label>
                </div>
            </div>
        </div>

        <div class="col-lg-12" style="height: 100px;">
            <div class="col-lg-1"></div>
             <div align="center">
                <button type="submit" class="btn ink-reaction btn-raised btn-primary" id="btnConsultar" style="margin-top:7px">Buscar</button>
             </div>

        </div>

    </form>
</div>

<script type="text/javascript">

    $('#periodo_desde').datepicker({
        format: "yyyy-mm",
        minViewMode: 1,
        maxViewMode: 2,
        language: "es"
    });

    $('#periodo_hasta').datepicker({
        format: "yyyy-mm",
        minViewMode: 1,
        maxViewMode: 2,
        language: "es"
    });

    $(document).ready(function() {
        $('#modalAncho').css("width", "85%");
        //$('#fechas').datepicker({ format: 'dd/mm/yyyy' });
    });

</script>
