<div class="card">
    <div class="card-head">
        <header> <h2 class="text-primary"> Libro Diario </h2></header>
    </div>

    <div class="card-head">
        <header></header>
    </div>

    <form class="form" action="{$_Parametros.url}modCB/LibroDiarioCONTROL/libroDiarioReportePdfMET" method="post" target="_blank">
        <div class="col-lg-12">
            <div class="col-lg-1"></div>
            <div class="col-lg-2">
              <div class="form-group" id="fk_cbb005_num_contabilidadesError">
                <select id="fk_cbb005_num_contabilidades" name="fk_cbb005_num_contabilidades" class="form-control">
                     {Select::lista('cb_b005_contabilidades', 'pk_num_contabilidades', 'ind_descripcion', $form.fk_cbb005_num_contabilidades)}
                </select>
                <label for="fk_cbb005_num_contabilidades">Contabilidad</label>
             </div>
            </div>

            <div class="col-lg-1"></div>

            <div class="form-group col-lg-1">
                <div id="fechas" class="input-daterange input-group">
                    <div class="input-group-content">
                        <input class="form-control" type="text" id="periodo" name="periodo" value="{date("Y-m")}" style="text-align: center;" readonly />
                        <input type="hidden" id="anio_fecha" name="anio_fecha" value="{date("Y-m")}" >
                        <label>Período</label>
                    </div>
                </div>
            </div>

            <div class="col-lg-1"></div>

            <div class="form-group col-lg-1">
                <div id="vouchers" class="input-daterange input-group">
                    <div class="input-group-content floating-label">
                        <input class="form-control" type="text" id="voucher" name="voucher"  style="text-align: center;" >
                        <label>Voucher</label>
                    </div>
                </div>
            </div>

        </div>


        <div class="col-lg-12" style="height: 100px;">
            <div class="col-lg-1"></div>
             <div align="center">
                <button type="submit" class="btn ink-reaction btn-raised btn-primary" id="btnConsultar" style="margin-top:7px">Buscar</button>
             </div>
        </div>

    </form>
</div>

<script type="text/javascript">

    $('#periodo').datepicker({
        format: "yyyy-mm",
        minViewMode: 1,
        maxViewMode: 2,
        language: "es"
    });

    $(document).ready(function() {
        $('#modalAncho').css("width", "85%");
        $('#fechas').datepicker({ format: 'dd/mm/yyyy' });
    });

</script>
