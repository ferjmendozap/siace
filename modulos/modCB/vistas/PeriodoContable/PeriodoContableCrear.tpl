<form action="{$_Parametros.url}modCB/PeriodoContableCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
    <input type="hidden" value="{$_Parametros.url}modCB/PeriodoContableCONTROL/{$form.metodo}MET"/>
        <input type="hidden" value="{if isset($form.pk_num_control_cierre)}{$form.pk_num_control_cierre}{/if}" name="pk_num_control_cierre"/>

        <div class="col-sm-12">
            <div class="row">

                <!-- Nivel -->
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="pk_num_libro_contableError">
                        <select id="pk_num_libro_contable" name="pk_num_libro_contable" class="form-control">
                            {Select::lista('cb_b002_libro_contable', 'pk_num_libro_contable', 'ind_descripcion', $form.pk_num_libro_contable)}
                        </select>
                        <label for="pk_num_libro_contable">Libro Contable</label>
                    </div>
                </div>

                <!--Periodo Actualizar -->
                <div class="col-sm-4">
                    <div class="form-group floating-label " id="periodoError">
                        <input type="text" id="periodo" name="periodo" class="form-control" value="{if isset($form.ind_mes)}{$form.periodo}{/if}" style="text-align:center" maxlength="7" data-provide="datepicker" readonly>
                        <label for="periodo">Período Agregar</label>
                    </div>
                </div>

            </div>

            <div class="row">

                <!-- Nivel -->
                <div class="col-sm-6">
                    <div class="form-group floating-label" id="num_nivelError">
                        <select id="ind_tipo_registro" name="ind_tipo_registro" class="form-control">
                            {Select::options('tipo_registro', $form.ind_tipo_registro, 0)}
                        </select>
                        <label for="ind_tipo_registro">Tipo Registro</label>
                    </div>
                </div>
                 <!--Estado-->
                <div class="col-sm-3">
                     <div class="checkbox checkbox-styled" style="visibility: hidden;">
                         <label>
                           <input type="checkbox" {if isset($form.ind_estatus) and $form.ind_estatus==A} checked{/if} value="A" name="ind_estatus" id="ind_estatus">
                           <span>Estado</span>
                          </label>
                     </div>
                </div>

            </div>
        </div>

        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
            <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $form.estado != "ver"}
            <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
        {/if}

    </div>
</form>

<script type="text/javascript">

    $('#periodo').datepicker({
        format: "yyyy-mm",
        minViewMode: 1,
        maxViewMode: 2,
        language: "es"
    });

    $(document).ready(function(){

        $("#formAjax").submit(function(){
            return false;
        });

        $('#modalAncho').css( "width", "55%" );

        //  envio formulario
        $('#accion').click(function() {

            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('CB-01-01-01-02-M',$_Parametros.perfil)}&perfilE={in_array('CB-01-01-01-03-E',$_Parametros.perfil)}", function(dato) {

                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                        $(document.getElementById(dato+'Error')).append('<span id="'+dato+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    }
                    if(dato['flag_mensaje']==1) swal("Registro previamente creado!", dato['mensaje'], "warning");
                } else {
                    //  mensaje
                    swal("Registro Guardado!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                        $('#datatable1 tbody').append(dato['tr']);
                    }
                    else if (dato['status'] == 'modificar') {
                        $('#id'+dato['id']).html(dato['tr']);
                    }
                }
           },'json');
        });
    });
</script>
