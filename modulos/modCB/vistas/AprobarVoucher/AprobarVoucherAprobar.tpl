<form action="{$_Parametros.url}modCB/AprobarVoucherCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="{if isset($form.pk_num_voucher_mast)}{$form.pk_num_voucher_mast}{/if}" name="pk_num_voucher_mast"/>
        <input type="hidden" value="{$form.fk_a003_num_preparado_por}"/>
        <input type="hidden" value="{$form.fk_a003_num_aprobado_por}"/>

        <div class="col-sm-12">
            <div class="row">

                <div class="col-sm-2">
                    <div class="form-group floating-label" id="ind_periodoError">
                        <input type="text" id="periodo" name="periodo" class="form-control" value="{if isset($form.ind_mes)}{$form.periodo}{/if}" style="text-align:center;" maxlength="10" />
                        <label for="periodo">Período</label>
                    </div>
                </div>

                <div class="col-sm-4">

                </div>

                <!-- Voucher -->
                <div class="col-sm-3">
                    <div class="form-group floating-label"  id="ind_voucherError">
                        <select id="ind_voucher" name="ind_voucher" class="form-control">
                            {Select::lista('cb_c003_tipo_voucher', 'pk_num_voucher', 'ind_descripcion', $form.ind_voucher)}
                        </select>
                        <label for="ind_descripcion">Voucher</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group floating-label"  id="ind_nro_voucherError">
                            <input type="text" id="ind_nro_voucher" name="ind_nro_voucher" readonly class="form-control" value="{if isset($form.ind_nro_voucher)}{$form.ind_nro_voucher}{/if}" >
                            <label for="ind_nro_voucher">Nro.Voucher</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="row">
                <!-- Organismo -->
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="fk_a001_num_organismoError">
                        <select id="fk_a001_num_organismo" name="fk_a001_num_organismo" class="form-control">
                            {Select::lista('a001_organismo', 'pk_num_organismo', 'ind_descripcion_empresa', $form.fk_a001_num_organismo)}
                        </select>
                        <label for="fk_a001_num_organismo">Organismo</label>
                    </div>
                </div>

                <!-- Preparado Por -->
                <div class="col-sm-5">
                    <div class="form-group floating-label"  id="fk_a003_num_preparado_porError">
                        <input type="text" id="fk_a003_num_preparado_por" name="fk_a003_num_preparado_por" class="form-control" value="{ConsultarDescripcion::consultar2($form.fk_a003_num_preparado_por,'pk_num_persona','a003_persona')}" readonly >
                        <label for="fk_a003_num_preparado_por">Preparado Por</label>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-12">
            <div class="row">
                <!-- Dependencia -->
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="fk_a004_num_dependenciaError">
                        <select id="fk_a004_num_dependencia" name="fk_a004_num_dependencia" class="form-control">
                            <option value="">&nbsp;</option>
                             {Select::lista('a004_dependencia', 'pk_num_dependencia', 'ind_dependencia', $form.fk_a004_num_dependencia)}
                        </select>
                        <label for="fk_a004_num_Dependencia">Dependencia</label>
                    </div>
                </div>

                <!-- Preparado Por -->
                <div class="col-sm-5">
                    <div class="form-group floating-label"  id="fk_a003_num_aprobado_porError">
                        <input type="text" id="fk_a003_num_aprobado_por" name="fk_a003_num_aprobado_por"class="form-control" value="{if isset($form.fk_a003_num_aprobado_por)}{ConsultarDescripcion::consultar2($form.fk_a003_num_aprobado_por,'pk_num_persona','a003_persona')}{/if}" readonly >
                        <label for="fk_a003_num_aprobado_por">Aprobado Por</label>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-12">
            <div class="row">
                <!-- Dependencia -->
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="fk_cbb005_num_contabilidadError">
                        <select id="fk_cbb005_num_contabilidad" name="fk_cbb005_num_contabilidad" class="form-control">
                             {Select::lista('cb_b005_contabilidades', 'pk_num_contabilidad', 'ind_descripcion', $form.fk_cbb005_num_contabilidad)}
                        </select>
                        <label for="fk_cbb005_num_contabilidad">Contabilidad</label>
                    </div>
                </div>

                <!-- Preparado Por -->
                <div class="col-sm-7">
                    <div class="form-group floating-label"  id="fk_a003_num_aprobado_porError">
                        <input type="text" id="txt_titulo_voucher" name="txt_titulo_voucher"class="form-control" value="{if isset($form.txt_titulo_voucher)}{$form.txt_titulo_voucher}{/if}" >
                        <label for="txt_titulo_voucher">Descripción</label>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-10">
            <div class="row">

                <div class="col-sm-4">
                    <div class="form-group floating-label"  id="fec_fecha_preparacionError">
                        <input type="text" id="fec_fecha_preparacion" name="fec_fecha_preparacion"class="form-control" value="{if isset($form.fec_fecha_preparacion)}{$form.fec_fecha_preparacion}{/if}" style="text-align:center;" maxlength="10" />
                        <label for="fec_fecha_preparacion">Fecha</label>
                    </div>
                </div>

                <div class="col-sm-6"> </div>

            </div>
        </div>

        <div class="col-sm-12">
            <div class="table-responsive">
                <table>
                    <tr>
                        <td><input type="button" id="tbPub20" name="tbPub20" value="PUB20">
                            <input type="button" id="tbOnco" name="tbOnco" value="ONCOP">
                            <input type="button" id="tbPersona" name="tbPersona" value="PERSONA">
                            <input type="button" id="tbCentroCosto" name="tbCentroCosto" value="C. COSTO">
                        </td>
                    </tr>
                </table>
                <table class="table table-bordered">
                        <thead>
                            <tr >
                                <th class="text-center" width="40">#</th>
                                <th class="text-center" width="100">Cuenta</th>
                                <th class="text-center" width="100">Persona</th>
                                <th class="text-center" width="100">Documento</th>
                                <th class="text-center" width="150">Fecha</th>
                                <th class="text-center" width="150">Monto</th>
                                <th class="text-center" width="150">CCosto</th>
                                <th class="text-center" width="400">Descripcion</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id='addr0'>
                                <td>1</td>
                                <td><input type="text" name='cuenta0'   class="form-control" readonly /></td>
                                <td><input type="text" name='persona0'  class="form-control" readonly /></td>
                                <td><input type="text" name='documento0'  class="form-control"/></td>
                                <td><input type="text" name='fecha0'  class="form-control"/></td>
                                <td><input type="text" name='monto0'  class="form-control"/></td>
                                <td><input type="text" name='ccosto0'  class="form-control" readonly/></td>
                                <td><input type="text" name='fecha0'  class="form-control"/></td>
                            </tr>
                            <tr id='addr1'>
                                <td> 2 </td>
                                <td><input type="text" name='cuenta1'   class="form-control" readonly /></td>
                                <td><input type="text" name='persona1'  class="form-control" readonly /></td>
                                <td><input type="text" name='documento1'  class="form-control"/></td>
                                <td><input type="text" name='fecha1'  class="form-control"/></td>
                                <td><input type="text" name='monto1'  class="form-control"/></td>
                                <td><input type="text" name='ccosto1'  class="form-control" readonly /></td>
                                <td><input type="text" name='descripcion1'  class="form-control"/></td>
                            </tr>
                        </tbody>
                </table>
            </div>
        </div>

        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">Guardar</button>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function(){

        $('#modalAncho').css( "width", "70%" );

          var i=1;
          $("#add_row").click(function(){
             $('#addr'+i).html("<td>"+ (i) +"</td><td><input name='name"+i+"' type='text' placeholder='Name' class='form-control input-md'  /> </td><td><input  name='mail"+i+"' type='text' placeholder='Mail'  class='form-control input-md'></td><td><input  name='mobile"+i+"' type='text' placeholder='Mobile'  class='form-control input-md'></td>");

             $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
             i++;
          });

          $("#delete_row").click(function(){
             if(i>1){
               $("#addr"+(i-1)).html('');
               i--;
             }
          });
    });

</script>
