<style type="text/css">
    .form-control2 {
    padding: 0;
    height: 20px;
    /*width: 70px;*/
    border-left: none;
    border-right: none;
    border-top: none;
    border-bottom-color: rgba(12, 12, 12, 0.12);
    /*background: transparent;*/
    color: #0c0c0c;
    font-size: 16px;
    -webkit-box-shadow: none;
    box-shadow: none;
    }
</style>
<form action="{$_Parametros.url}modCB/ContabilidadCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="{if isset($form.pk_num_contabilidades)}{$form.pk_num_contabilidades}{/if}" name="pk_num_contabilidades"/>

        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group floating-label" id="ind_tipo_contabilidadError">
                        <input type="text" id="ind_tipo_contabilidad" name="ind_tipo_contabilidad" class="form-control" value="{if isset($form.ind_tipo_contabilidad)}{$form.ind_tipo_contabilidad}{/if}" >
                        <label for="ind_tipo_contabilidad">Tipo Cont.</label>
                    </div>
                </div>

                <div class="col-sm-10">
                    <div class="form-group floating-label"  id="ind_descripcionError">
                        <input type="text" id="ind_descripcion" name="ind_descripcion" class="form-control" value="{if isset($form.ind_descripcion)}{$form.ind_descripcion}{/if}" maxlength="30" >
                        <label for="ind_descripcion">Descripción</label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group floating-label" id="ind_contabilidad_acronimoError">
                        <input type="text" id="ind_contabilidad_acronimo" name="ind_contabilidad_acronimo" class="form-control" value="{if isset($form.ind_contabilidad_acronimo)}{$form.ind_contabilidad_acronimo}{/if}" maxlength="10" >
                        <label for="ind_contabilidad_acronimo">Acrónimo</label>
                    </div>
                </div>

                <div class="col-sm-4">
                     <div class="checkbox checkbox-styled">
                         <label>
                           <input type="checkbox" {if isset($form.num_estatus) and $form.num_estatus==1} checked{/if} value="1" name="num_estatus" id="num_estatus">
                           <span>Estatus</span>
                          </label>
                     </div>
                </div>

            </div>

            <!-- Libros contables -->
            <div class="row">
                <div class="col-sm-12">
                     <div class="card card-underline">
                         <div class="card-head"><label><b>Libros Válidos</b></label>
                             <div class="tools">
                                 <button type="button" id="insertar" class="btn ink-reaction btn-raised btn-info btn-xs" data-lista="detalle">
                                 <i class="md md-add"></i>
                                 </button>
                             </div>
                         </div>
                         <div class="card-body">
                             <div class="table-responsive" style="height: 170px;">
                             <table id="datatable1" class="table table-striped table-hover table-condensed" style="min-width: 320px;">
                                 <thead>
                                     <tr>
                                       <!-- <th class="text-center" width="100">Id.</th>-->
                                        <th class="text-center" width="100">Código</th>
                                        <th class="text-center" width="200">Descripción</th>
                                        <th width="25">Eliminar</th>
                                     </tr>
                                 </thead>
                                 <tbody id="lista_detalle">
                                {$numero= 0}
                                {foreach item=registro from=$form.listado_detalle}
                                    {$numero=$numero+1}
                                    <tr id="detalle{$numero}">
                                        <td>
                                            <div class="col-sm-10">
                                                <div class="form-group form-group-sm floating-label">
                                                    <input type="hidden" name="pk_num_libro_contabilidad" value="{$registro.pk_num_libro_contabilidad}">
                                                    <input type="text" name="detalle_cod_libro[]" id="detalle_cod_libro{$numero}" value="{$registro.cod_libro}" class="form-control input-sm" style="text-align:center;" disabled>
                                                    <label for="detalle_cod_libro">&nbsp;</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-sm floating-label">
                                                    <select name="fk_cbb002_num_libro_contable[]" id="detalle_num_libro_contable{$numero}" data-id="{$numero}" class="form-control" onchange="obtenerCodLibro($(this));" >
                                                        <option value="">&nbsp;</option>
                                                            {Select::lista('cb_b002_libro_contable', 'pk_num_libro_contabLe', 'ind_descripcion', $registro.fk_cbb002_num_libro_contable)}
                                                    </select>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-2">
                                              <button class="btn ink-reaction btn-raised btn-xs btn-danger"  onclick="$('#detalle{$numero}').remove();">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                             </button>
                                           </div>
                                        </td>
                                    </tr>
                                {/foreach}
                                 </tbody>
                             </table>
                             </div>
                         </div>
                     </div>
                </div>
            </div>

        </div>
        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $form.estado != "ver"}
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function(){

        $("#formAjax").submit(function(){
            return false;
        });

        $('#modalAncho').css( "width", "50%" );

        //  envio formulario
        $('#accion').click(function() {

            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('CB-01-90-05-02-M',$_Parametros.perfil)}&perfilE={in_array('CB-01-90-05-03-E',$_Parametros.perfil)}", function(dato) {

                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                         $(document.getElementById(dato+'Error')).append('<span id="'+dato+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    }
                } else {
                    //  mensaje
                    //swal("Registro Guardado!", dato['mensaje'], "success");
                    swal(dato['swal'], dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                    //  actualizo tabla
                    if(dato['status']=='crear') {
                        $('#datatable1 tbody').append(dato['tr']);
                    }else if (dato['status'] == 'modificar') {
                        $('#id'+dato['id']).html(dato['tr']);
                    }
                }
            },'json');
        });

        //  insertar linea
        $('#insertar').click(function() {
            var tbody = $('#lista_' + $(this).data('lista'));
            var numero = $('#lista_' + $(this).data('lista') + ' tr').length + 1;

            $.post("{$_Parametros.url}modCB/ContabilidadCONTROL/insertarMET", "numero="+numero, function(dato) {
                tbody.append(dato);
            }, '');
        });

    });
    
    // Obtiene el código del libro
    function obtenerCodLibro(detalle_num_libro) {

        var detalle_cod_libro= $('#detalle_cod_libro' + detalle_num_libro.data('id'));
        var input = detalle_num_libro.attr('id');

        $.post("{$_Parametros.url}modCB/ContabilidadCONTROL/obtenerCodLibroMET", "detalle_num_libro="+detalle_num_libro.val(), function(dato) {

            if (dato == '') {
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
            }
            else {
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
            }
            detalle_cod_libro.val(dato);
        }, '');
    }


</script>
