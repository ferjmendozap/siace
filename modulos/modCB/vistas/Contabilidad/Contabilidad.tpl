<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary">Contabilidad</h2>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="table-responsive">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id.</th>
                                <th>TipoCont.</th>
                                <th>Descripción</th>
                                <th>Acrónimo</th>
                                <th>Estado</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=registro from=$listado}
                                <tr id="id{$registro.pk_num_contabilidades}">
                                    <td><label>{$registro.pk_num_contabilidades}</label></td>
                                    <td><label>{$registro.ind_tipo_contabilidad}</label></td>
                                    <td><label>{$registro.ind_descripcion}</label></td>
                                    <td><label>{$registro.ind_contabilidad_acronimo}</label></td>
                                    <td><i class="{if $registro.num_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    <td align="center">
                                        {if in_array('CB-01-90-05-02-M',$_Parametros.perfil)}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal"
                                                    data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_contabilidades}" title="Editar"
                                                    descipcion="El Usuario a Modificado un post" titulo="Editar Contabilidad">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}

                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_contabilidades}" title="Ver"
                                                descipcion="El Usuario esta viendo Contabilidades" titulo="Ver Contabilidad">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                                        {if in_array('CB-01-90-05-03-E',$_Parametros.perfil)}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idRegistro="{$registro.pk_num_contabilidades}"  boton="si, Eliminar" title="Eliminar"
                                                    descipcion="El usuario a eliminado un post" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="6">
                                {if in_array('CB-01-90-05-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"  descipcion="el Usuario a creado un post"  titulo="Nuevo Registro" id="nuevo" >
                                        <i class="md md-create"></i>&nbsp;Nueva Contabilidad</button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
   $(document).ready(function() {
        //  base
        var $base = '{$_Parametros.url}modCB/ContabilidadCONTROL/';

        //  nuevo registro
        $('#nuevo').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'nuevoMET', '', function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });
        // modificar
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'editarMET', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        // ver
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'verMET', { id: $(this).attr('idRegistro'), estado: 'ver' }, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });
        // eliminar
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var id = $(this).attr('idRegistro');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                $.post($base+'eliminarMET', { id: id }, function(dato){
                    //  mensaje
                    if (dato['valor']=="1") {
                        $(document.getElementById('id'+id)).remove();
                        swal("Registro Eliminado!", dato['mensaje'], "success");
                        $('#cerrar').click();
                    }else{
                        //$(document.getElementById('id'+id)).remove();
                        swal("Registro No Eliminado!", dato['mensaje'], "success");
                        $('#cerrar').click();
                    }
                },'json');
            });
        });
    });
</script>
