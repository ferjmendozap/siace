<form action="{$_Parametros.url}modCB/SistemaFuenteCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="{if isset($form.pk_num_sistema_fuente)}{$form.pk_num_sistema_fuente}{/if}" name="pk_num_sistema_fuente"/>

        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group floating-label" id="cod_sistema_fuenteError">
                        <input type="text" id="cod_sistema_fuente" name="cod_sistema_fuente" class="form-control" value="{if isset($form.cod_sistema_fuente)}{$form.cod_sistema_fuente}{/if}" >
                        <label for="cod_sistema_fuente">Cód. Fuente</label>
                    </div>
                </div>

                <div class="col-sm-7">
                    <div class="form-group floating-label"  id="ind_descripcionError">
                        <input type="text" id="ind_descripcion" name="ind_descripcion"class="form-control" value="{if isset($form.ind_descripcion)}{$form.ind_descripcion}{/if}" >
                        <label for="ind_descripcion">Descripción</label>
                    </div>
                </div>

                <div class="col-sm-3">
                     <div class="checkbox checkbox-styled">
                         <label>
                           <input type="checkbox" {if isset($form.num_estatus) and $form.num_estatus==1} checked{/if} value="1" name="num_estatus" id="num_estatus">
                           <span>Estatus</span>
                          </label>
                     </div>
                </div>
            </div>
        </div>

        <span class="clearfix"></span>
    </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Canselado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $form.estado != "ver"}
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion"><i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
        {/if}
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function(){

        $("#formAjax").submit(function(){
            return false;
        });

        $('#modalAncho').css( "width", "75%" );

        //  envio formulario
        $('#accion').click(function() {

            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('CB-01-90-02-02-M',$_Parametros.perfil)}&perfilE={in_array('CB-01-90-02-03-E',$_Parametros.perfil)}", function(dato) {

                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                         $(document.getElementById(dato+'Error')).append('<span id="'+dato+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    }
                } else {
                    //  mensaje
                    swal("Registro Guardado!", dato['mensaje'], "success");
                    $(document.getElementById('cerrarModal')).click();
                    $(document.getElementById('ContenidoModal')).html('');

                    //  actualizo tabla
                    if (dato['status'] == 'crear') {
                        $('#datatable1 tbody').append(dato['tr']);
                    }
                    else if (dato['status'] == 'modificar') {
                        $('#id'+dato['id']).html(dato['tr']);
                    }
                }
            },'json');
        });
    });
</script>
