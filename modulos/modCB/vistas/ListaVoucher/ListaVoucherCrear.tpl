<style type="text/css">
    .form-control2 {
    padding: 0;
    height: 10px;
    /*width: 70px;*/
    border-left: none;
    border-right: none;
    border-top: none;
    border-bottom-color: rgba(12, 12, 12, 0.12);
    /*background: transparent;*/
    color: #0c0c0c;
    font-size: 16px;
    -webkit-box-shadow: none;
    box-shadow: none;
    }
</style>

<form action="{$_Parametros.url}modCB/ListaVoucherCONTROL/{$form.metodo}MET" id="formAjax" class="form" role="form" method="post">
    <div class="modal-body">
        <input type="hidden" value="{if isset($form.pk_num_voucher_mast)}{$form.pk_num_voucher_mast}{/if}" name="pk_num_voucher_mast"/>

        <div class="col-sm-12">
            <div class="row">

                <div class="col-sm-2">
                    <div class="form-group floating-label" id="periodoError">
                        <input type="text" id="periodo" name="periodo" class="form-control" value="{if isset($form.ind_mes)}{$form.periodo}{/if}" style="text-align:center;" maxlength="10" readonly />
                        <label for="periodo">Período</label>
                    </div>
                </div>

                <div class="col-sm-4">

                </div>

                <!-- Voucher -->
                <div class="col-sm-3">
                    <div class="form-group floating-label"  id="fk_cbc003_num_voucherError">
                        <select id="fk_cbc003_num_voucher" name="fk_cbc003_num_voucher" class="form-control">
                            {Select::lista('cb_c003_tipo_voucher', 'pk_num_voucher', 'ind_descripcion', $form.fk_cbc003_num_voucher)}
                        </select>
                        <label for="fk_cbc003_num_voucher">Voucher</label>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group floating-label"  id="ind_nro_voucherError">
                            <input type="text" id="ind_nro_voucher" name="ind_nro_voucher" readonly class="form-control" value="{if isset($form.ind_nro_voucher)}{$form.ind_nro_voucher}{/if}" >
                            <label for="ind_nro_voucher">Nro.Voucher</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="row">
                <!-- Organismo -->
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="fk_a001_num_organismoError">
                        <select id="fk_a001_num_organismo" name="fk_a001_num_organismo" class="form-control">
                            {Select::lista('a001_organismo', 'pk_num_organismo', 'ind_descripcion_empresa', $form.fk_a001_num_organismo)}
                        </select>
                        <label for="fk_a001_num_organismo">Organismo</label>
                    </div>
                </div>

                <!-- Preparado Por -->
                <div class="col-sm-5">
                    <div class="form-group floating-label"  id="fk_a003_num_preparado_porError">
                        <input type="text" id="fk_a003_num_preparado_por" name="fk_a003_num_preparado_por" class="form-control" value="{ConsultarDescripcion::consultar2($form.fk_a003_num_preparado_por,'pk_num_persona','a003_persona')}" readonly >
                        <label for="fk_a003_num_preparado_por">Preparado Por</label>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-12">
            <div class="row">
                <!-- Dependencia -->
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="fk_a004_num_dependenciaError">
                        <select id="fk_a004_num_dependencia" name="fk_a004_num_dependencia" class="form-control">
                             {Select::lista('a004_dependencia', 'pk_num_dependencia', 'ind_dependencia', $form.fk_a004_num_dependencia)}
                        </select>
                        <label for="fk_a004_num_Dependencia">Dependencia</label>
                    </div>
                </div>

                <!-- Preparado Por -->
                <div class="col-sm-5">
                    <div class="form-group floating-label"  id="fk_a003_num_aprobado_porError">
                        <input type="text" id="fk_a003_num_aprobado_por" name="fk_a003_num_aprobado_por"class="form-control" value="{if isset($form.fk_a003_num_aprobado_por)}{ConsultarDescripcion::consultar2($form.fk_a003_num_aprobado_por,'pk_num_persona','a003_persona')}{/if}" readonly >
                        <label for="fk_a003_num_aprobado_por">Aprobado Por</label>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-12">
            <div class="row">
                <!-- Contabilidad -->
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="fk_cbb005_num_contabilidadesError">
                        <select id="fk_cbb005_num_contabilidades" name="fk_cbb005_num_contabilidades" class="form-control">
                             {Select::lista('cb_b005_contabilidades', 'pk_num_contabilidades', 'ind_descripcion', $form.fk_cbb005_num_contabilidades)}
                        </select>
                        <label for="fk_cbb005_num_contabilidad">Contabilidad</label>
                    </div>
                </div>

                <!-- Libro Contable -->
                <div class="col-sm-5">
                    <div class="form-group floating-label" id="fk_cbb002_num_libro_contableError">
                        <select id="fk_cbb002_num_libro_contable" name="fk_cbb002_num_libro_contable" class="form-control">
                            {Select::lista('cb_b002_libro_contable', 'pk_num_libro_contabLe', 'ind_descripcion', $form1.fk_cbb002_num_libro_contable)}
                        </select>
                        <label for="fk_cbb002_num_libro_contable">Libro Contable</label>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-sm-10">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group floating-label"  id="fec_fecha_voucherError">
                        <input type="text" id="fec_fecha_voucher" name="fec_fecha_voucher"class="form-control" value="{if isset($form.fec_fecha_voucher)}{$form.fec_fecha_voucher}{/if}" style="text-align:center;" maxlength="10" readonly />
                        <label for="fec_fecha_voucher">Fecha</label>
                    </div>
                </div>


                <div class="col-sm-7">
                    <div class="form-group floating-label"  id="txt_titulo_voucherError">
                        <input type="text" id="txt_titulo_voucher" name="txt_titulo_voucher" class="form-control" value="{if isset($form.txt_titulo_voucher)}{$form.txt_titulo_voucher}{/if}" >
                        <label for="txt_titulo_voucher">Descripción</label>
                    </div>
                </div>

            </div>
        </div>

        <!-- prueba -->
        <div class="col-sm-12">
            <div class="card card-underline">
                <div class="card-head">
                    <div class="tools">
                        <button type="button" id="insertar" class="btn ink-reaction btn-raised btn-info btn-xs" data-lista="detalle">
                            <i class="md md-add"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive" style="height: 200px;">
                      <table id="datatable1" class="table table-striped table-hover table-condensed" style="min-width: 1400px;">
                            <thead>
                                <tr>
                                    <th class="text-center" width="200">Cuenta</th>
                                    <th class="text-center" width="200">Persona</th>
                                    <th class="text-center" width="200">Debe</th>
                                    <th class="text-center" width="200">Haber</th>
                                    <th class="text-center" width="300">CCosto</th>
                                    <th class="text-center">Descripcion</th>
                                    <th width="25">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody id="lista_detalle">
                                {$numero = 0}
                                {foreach item=registro from=$form.listado_detalle}
                                    {$numero=$numero+1}
                                    {$total_debe= $registro.num_debe + $total_debe}
                                    {$total_haber= $registro.num_haber + $total_haber}
                                    <tr id="detalle{$numero}">
                                        <td>
                                            <div class="col-sm-10">
                                                <div class="form-group form-group-sm floating-label" id="detalle_num_cuenta{$numero}Error">
                                                    <input type="hidden" name="detalle_fk_cbb004_num_cuenta[]" id="detalle_fk_cbb004_num_cuenta{$numero}" value="{$registro.fk_cbb004_num_cuenta}">
                                                    <input type="text" name="detalle_num_cuenta[]" id="detalle_num_cuenta{$numero}" value="{$registro.num_cuenta}-{$registro.descripcion_cuenta}" class="form-control input-sm" data-id="{$numero}" onchange="obtenerCuenta($(this));" readonly>
                                                    <label for="detalle_num_cuenta">&nbsp;</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                               <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                                                       style="margin: 0px 0px -45px -10px;"
                                                       data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Plan de Cuentas"
                                                    onclick="selector($(this),'{$_Parametros.url}modCB/PlanCuentaCONTROL/selectorCuentasContablesMET', ['detalle_fk_cbb004_num_cuenta{$numero}','detalle_num_cuenta{$numero}'],'Contabilidad='+$('#fk_cbb005_num_contabilidades').val());">
                                                    <i class="md md-search"></i>
                                               </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-10">
                                                <div class="form-group form-group-sm floating-label" id="detalle_num_persona{$numero}Error">
                                                    <input type="hidden" name="detalle_fk_a003_num_persona[]" id="detalle_fk_a003_num_persona{$numero}" value="{$registro.fk_a003_num_persona}">
                                                    <input type="text" name="detalle_num_persona[]" id="detalle_num_persona{$numero}" value="{$registro.num_persona}" class="form-control input-sm" maxlength="15" data-id="{$numero}" onchange="obtenerPersona($(this));" readonly>
                                                    <label for="detalle_num_persona">&nbsp;</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                                                        style="margin: 0px 0px -45px -10px;"
                                                        data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado de Personas"
                                                        onclick="selector($(this),'{$_Parametros.url}modCB/ListaVoucherCONTROL/selectorPersonaMET', ['detalle_fk_a003_num_persona{$numero}','detalle_num_persona{$numero}']);">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-8">
                                                <div class="form-group form-group-sm floating-label">
                                                    <input type="text" name="detalle_num_debe[]" id="detalle_num_debe{$numero}" value="{number_format($registro.num_debe,2,',','.')}" class="form-control input-sm" style="text-align:right;" onclick="monto_anterior($(this));" onchange="actualizarMonto($(this),'debe');">
                                                    <label for="detalle_num_debe">&nbsp;</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-8">
                                                <div class="form-group form-group-sm floating-label">
                                                    <input type="text" name="detalle_num_haber[]" id="detalle_num_haber{$numero}" value="{number_format($registro.num_haber,2,',','.')}" class="form-control input-sm" style="text-align:right;" onclick="monto_anterior($(this));" onchange="actualizarMonto($(this),'haber');">
                                                    <label for="detalle_num_haber">&nbsp;</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-10">
                                                <div class="form-group form-group-sm floating-label" id="detalle_num_centro_costo{$numero}Error">
                                                    <input type="hidden" name="detalle_fk_a023_num_centro_costo[]" id="detalle_fk_a023_num_centro_costo{$numero}" value="{$registro.fk_a023_num_centro_costo}">
                                                    <input type="text" name="detalle_num_centro_costo[]" id="detalle_num_centro_costo{$numero}" value="{$registro.num_centro_costo}" class="form-control input-sm" maxlength="15" data-id="{$numero}" onchange="obtenerCcosto($(this));" readonly>
                                                    <label for="detalle_num_centro_costo">&nbsp;</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <button type="button" class="btn ink-reaction btn-raised btn-info btn-xs selector"
                                                        style="margin: 0px 0px -45px -10px;"
                                                        data-toggle="modal" data-target="#formModal2" data-keyboard="false" data-backdrop="static" data-titulo="Listado Centro Costos"
                                                        onclick="selector($(this),'{$_Parametros.url}modCB/ListaVoucherCONTROL/selectorCcostoMET', ['detalle_fk_a023_num_centro_costo{$numero}','detalle_num_centro_costo{$numero}']);">
                                                    <i class="md md-search"></i>
                                                </button>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="form-group form-group-sm floating-label">
                                                    <input type="text" name="detalle_ind_descripcion[]" value="{$registro.ind_descripcion}" class="form-control input-sm">
                                                    <label for="detalle_ind_descripcion">&nbsp;</label>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <button class="btn ink-reaction btn-raised btn-xs btn-danger"  onclick="monto_menos($('#detalle_num_debe{$numero}'),$('#detalle_num_haber{$numero}'))|$('#detalle{$numero}').remove();">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                        <div style="top: 0px; left: 300px; width: 435px;" class="col-sm-12">
                                 <div class="form-group col-sm-2">
                                    <label>Total x Columnas: </label>
                                    <input type="hidden" id="monto_ant" name="monto_ant" >
                                  </div>
                                 <div class="form-group col-sm-4">
                                    <input type="text" style="text-align:right;" id="monto_debe" name="monto_debe" class="form-control" value="{number_format($total_debe,2,',','.')}" readonly>
                                    <label for="monto_debe">&nbsp;</label>
                                </div>
                                <div class="form-group col-sm-2"></div>
                                <div class="form-group col-sm-4">
                                    <input type="text" style="text-align:right;" name="monto_haber" id="monto_haber" class="form-control" value="{number_format($total_haber,2,',','.')}" readonly>
                                    <label for="monto_haber">&nbsp;</label>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- fin prueba -->
        <span class="clearfix"></span>
    </div>
    <div class="modal-footer">

        <button type="button" class="btn btn-default ink-reaction btn-raised logsUsuarioModal" descipcionModal="El usuario a Cancelado el Registro" data-dismiss="modal"><i class="glyphicon glyphicon-floppy-remove"></i>&nbsp;Cancelar</button>
        {if $form.estado != "ver"}
        <button type="button" class="btn btn-primary ink-reaction btn-raised logsUsuarioModal" id="accion">
          <i class="glyphicon glyphicon-floppy-disk"></i>&nbsp;Guardar</button>
        {/if}
    </div>
</form>

<script type="text/javascript">

    $('#periodo').datepicker({
        format: "yyyy-mm",
        minViewMode: 1,
        maxViewMode: 2,
        language: "es"
    });

    $("#fec_fecha_voucher").datepicker({
        format: "dd-mm-yyyy",
        language: "es"
    });

    $(document).ready(function(){

        $("#formAjax").submit(function(){
            return false;
        });

        $('#modalAncho').css( "width", "70%" );

        //  envio formulario
        $('#accion').click(function() {

            $.post($("#formAjax").attr("action"), $("#formAjax").serialize()+"&perfilM={in_array('CB-01-02-01-02-M',$_Parametros.perfil)}&perfilE={in_array('CB-01-02-01-03-E',$_Parametros.perfil)}", function(dato) {

                if (dato['status'] == 'error') {
                    for (var i in dato.input) {
                        $(document.getElementById(dato.input[i].field+'Error')).removeClass('has-error has-feedback');
                        $(document.getElementById(dato.input[i].field+'Error')).addClass('has-error has-feedback');
                        $(document.getElementById(dato+'Error')).append('<span id="'+dato+'Icono" class="glyphicon glyphicon-remove form-control-feedback"></span>');
                    }
                } else{

                    if (dato['status'] == 'notificacion') {
                        swal("El período no existe o está cerrado!", dato['mensaje'], "warning");
                    }else if(dato['status'] == 'no-igual') {
                        swal("Diferencias entre Montos Totales!", dato['mensaje'], "warning");
                    }else if(dato['status'] == 'no-estatus-permitido'){
                        swal("No puede ser Modificado!", dato['mensaje'], "warning");
                    }else{
                        //  mensaje
                        swal("Registro Guardado!", dato['mensaje'], "success");
                        $(document.getElementById('cerrarModal')).click();
                        $(document.getElementById('ContenidoModal')).html('');

                        //  actualizo tabla
                        if (dato['status'] == 'crear') {
                            $('#datatable1 tbody').append(dato['tr']);
                        }
                        else if (dato['status'] == 'modificar') {
                            $('#id'+dato['id']).html(dato['tr']);
                        }
                    }
                }
            },'json');
        });

        //  insertar linea
        $('#insertar').click(function() {
            var tbody = $('#lista_' + $(this).data('lista'));
            var numero = $('#lista_' + $(this).data('lista') + ' tr').length + 1;

            $.post("{$_Parametros.url}modCB/ListaVoucherCONTROL/insertarMET", "numero="+numero+"&Contabilidad="+$('#fk_cbb005_num_contabilidades').val(), function(dato) {
                tbody.append(dato);
            }, '');
        });

        //
        $('.listado_cuentas').click(function() {
            alert($(this).data('field'));
        });

    });

    /*
    *Función que permite obtener cuenta
    */
    function obtenerCuenta(detalle_num_cuenta) {
        var detalle_fk_cbb004_num_cuenta = $('#detalle_fk_cbb004_num_cuenta' + detalle_num_cuenta.data('id'));
        var input = detalle_num_cuenta.attr('id');

        $.post("{$_Parametros.url}modCB/ListaVoucherCONTROL/ObtenerCuentaMET", "detalle_num_cuenta="+detalle_num_cuenta.val(), function(dato) {
            if (dato == '') {
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
            }
            else {
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
            }
            detalle_fk_cbb004_num_cuenta.val(dato);
        }, '');
    }

    /*
    *Función que permite obtener cuenta
    */
    function obtenerPersona(detalle_num_persona) {
        var detalle_fk_a003_num_persona = $('#detalle_fk_a003_num_persona' + detalle_num_persona.data('id'));
        var input = detalle_num_persona.attr('id');

        $.post("{$_Parametros.url}modCB/ListVoucherCONTROL/ObtenerPersonaMET", "detalle_num_persona="+detalle_num_persona.val(), function(dato) {
            if (dato == '') {
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
            }
            else {
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
            }
            detalle_fk_a003_num_persona.val(dato);
        }, '');
    }

    /*
    *Función que permite obtener centro costo
    */
    function obtenerCcosto(detalle_num_centro_costo) {
        var detalle_fk_a023_num_centro_costo = $('#detalle_fk_a023_num_centro_costo' + detalle_num_centro_costo.data('id'));
        var input = detalle_num_centro_costo.attr('id');

        $.post("{$_Parametros.url}modCB/ListVoucherCONTROL/ObtenerCentroCostoMET", "detalle_num_centro_costo="+detalle_num_centro_costo.val(), function(dato) {
            if (dato == '') {
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
            }
            else {
                $(document.getElementById(input+'Error')).addClass('has-error has-feedback');
                $(document.getElementById(input+'Error')).removeClass('has-error has-feedback');
            }
            detalle_fk_a023_num_centro_costo.val(dato);
        }, '');
    }

    /*
    *Solicito el cambio de formato número
    */
    function actualizarMonto(id, nomb_campo){
        var valor="";
        var monto_new = setNumero($(id).val());

        // calculando totales "debe - haber"
        if (nomb_campo=="debe") {
           var mont_debe= setNumero($('#monto_debe').val());
           var mont_ant= setNumero($('#monto_ant').val());

           if(monto_new>mont_ant){
             valor= monto_new - mont_ant + mont_debe;
           }else{
             valor= mont_debe - mont_ant + monto_new;
           }

           $("#monto_debe").val(valor).formatCurrency();

        }else{

          var mont_haber= setNumero($('#monto_haber').val());
          var mont_ant= setNumero($('#monto_ant').val());

          if(monto_new>mont_ant){
            valor= monto_new - mont_ant + mont_haber;
          }else{
            valor= mont_haber - mont_ant + monto_new;
          }

          $("#monto_haber").val(valor).formatCurrency();

        }

        $(id).val(monto_new).formatCurrency();
    }

    /*
    *Organiza el número ingresado
    */
    function setNumero(num_formateado) {
        var num = num_formateado.toString();
        num = num.replace(/[.]/gi, "");
        num = num.replace(/[,]/gi, ".");

        var numero = new Number(num);
        return numero;
    }

    /*
    *Captura el monto de un campo ()
    */
    function monto_anterior(id){
      var monto = setNumero($(id).val());
      $("#monto_ant").val(monto);
    }

    /*
    *
    */
    function monto_menos(valor_debe, valor_haber){
        
        var monto_d= setNumero($(valor_debe).val()); 
        var monto_h= setNumero($(valor_haber).val()); 

        if(monto_d!="0"){
             var monto_debe= setNumero($("#monto_debe").val()); 
                 monto_debe= monto_debe - monto_d;
             $("#monto_debe").val(monto_debe).formatCurrency();     
        }else{
            var monto_haber= setNumero($("#monto_haber").val()); 
                monto_haber= monto_haber - monto_h;
             $("#monto_haber").val(monto_haber).formatCurrency();
        }
    }

</script>
