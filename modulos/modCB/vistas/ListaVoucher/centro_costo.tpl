<section class="style-default-bright">
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
                <div class="card card-underline">
                    <div class="card-body">
                            <div class="table-responsive">
                                <table id="" class="table table-striped table-hover datatable1">
                                    <thead>
                                        <tr>
                                            <th>Id.</th>
                                            <th>Centro de Costo</th>
                                            <th>Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody> 
                                           {foreach item=registro from=$listado_ccosto}
                                           <tr id="id{$registro.pk_num_centro_costo}" onclick="seleccionar(['{$campos.campo1}','{$campos.campo2}'], ['{$registro.pk_num_centro_costo}','{$registro.ind_descripcion_centro_costo}']);">
                                               <td><label>{$registro.pk_num_centro_costo}</label></td>
                                               <td><label>{$registro.ind_descripcion_centro_costo}</label></td>
                                               <td><i class="{if $registro.num_estatus=='1'}md md-check{else}md md-not-interested{/if}"></i></td>
                                            </tr>
                                          {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        inicializar();
    });
</script>
