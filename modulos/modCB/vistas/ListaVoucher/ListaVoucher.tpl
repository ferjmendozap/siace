<section class="style-default-bright">
    <div class="section-header">
        <h2 class="text-primary pull-left">Listar Voucher</h2>
         <a data-toggle="offcanvas" title="Filtro" class="btn ink-reaction btn-floating-action btn-sm btn-info pull-right" href="#offcanvas-filtro"><i class="fa fa-filter"></i></a>
    </div>
    <div class="section-body contain-lg">
        <div class="row">
            <div class="col-lg-12 contain-lg">
             <div class="card card-underline">
                <div class="table-responsive">
                 <div class="card-body">
                    <table id="datatable1" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>Id.</th>
                                <th>Período</th>
                                <th>Nro. Voucher</th>
                                <th>Clasificación</th>
                                <th>Descripción</th>
                                <th>Estado</th>
                                <th>Accion</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach item=registro from=$listado}
                                <tr id="id{$registro.pk_num_voucher_mast}">
                                    <td><label>{$registro.pk_num_voucher_mast}</label></td>
                                    <td><label>{$registro.ind_anio}-{$registro.ind_mes}</label></td>
                                    <td><label>{$registro.ind_voucher}</label></td>
                                    <td><label>{ConsultarDescripcion::consultar($registro.fk_cbc003_num_voucher,'pk_num_voucher','cb_c003_tipo_voucher')}</label></td>
                                    <td><label>{$registro.txt_titulo_voucher}</label></td>
                                    <td><label>{ConsultarDescripcion::options('cb_estado_voucher',$registro.ind_estatus,'0')}</label></td>
                                    <!--
                                    <td><i class="{if $registro.ind_estatus==1}md md-check{else}md md-not-interested{/if}"></i></td>
                                    -->
                                    <td width="120" align="center">
                                        {if in_array('CB-01-02-01-02-M',$_Parametros.perfil) and $registro.ind_estatus == "AB"}
                                            <button class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" title="Editar"
                                                    data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_voucher_mast}"
                                                    descipcion="El Usuario a Modificado un post" titulo="Editar Registro">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}

                                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_voucher_mast}" title="Ver"
                                                descipcion="El Usuario esta viendo Voucher" titulo="Ver Voucher">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                                        {if in_array('CB-01-02-01-03-E',$_Parametros.perfil) and $registro.ind_estatus == "AB"}
                                            <button class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idRegistro="{$registro.pk_num_voucher_mast}" title="Eliminar"  boton="si, Eliminar"
                                                    descipcion="El usuario a eliminado un post" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el registro!!">
                                                <i class="md md-delete" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}

                                        {if $registro.ind_estatus != "AN"}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" idRegistro="{$registro.pk_num_voucher_mast}"  title="Anular" boton="Anular"
                                                        descipcion="El usuario anuló un post" titulo="Estas Seguro?" mensaje="Esta seguro que desea anular el Voucher!!" id="anular_voucher">
                                                    <i class="md md-not-interested"></i>
                                                </button>
                                        {/if}

                                        <!--<button class="logsUsuario btn ink-reaction btn-raised btn-xs btn-success" idRegistro="{$registro.pk_num_voucher_mast}"  boton="Imprimir" id="imprimir_voucher" action="{$_Parametros.url}modCB/ListaVoucherCONTROL/imprimirVoucherMET" target="_blank">
                                                <i class="icm icm-print" style="color: #ffffff;"></i>
                                        </button> -->
                                        {if $registro.ind_estatus == "MA"}
                                            <button class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info" idRegistro="{$registro.pk_num_voucher_mast}" title="Imprimir"  boton="Imprimir" id="imprimir_voucher" >
                                                    <i class="icm icm-print" style="color: #ffffff;"></i>
                                            </button>
                                        {/if}

                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="7">
                                {if in_array('CB-01-02-01-01-N',$_Parametros.perfil)}
                                    <button class="logsUsuario btn ink-reaction btn-raised btn-info" data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static"  descipcion="el Usuario a creado un post"  titulo="Registrar Nuevo Voucher" id="nuevo" >
                                       <i class="md md-create"></i>&nbsp;Nuevo Voucher
                                    </button>
                                {/if}
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                 </div>
                </div>
             </div>
            </div>
        </div>
    </div>
</section>

<div class="offcanvas">
    <div id="offcanvas-filtro" class="offcanvas-pane width-7">
        <div class="offcanvas-head">
            <header>Filtro</header>
            <div class="offcanvas-tools">
                <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                    <i class="md md-close"></i>
                </a>
            </div>
        </div>

        <div class="offcanvas-body">
            <form action="{$_Parametros.url}modCB/ListaVoucherCONTROL/indexMET/{$data.lista}" id="formFiltro" class="form" role="form" method="post" autocomplete="off" onsubmit="return cargarPagina();">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                            <select name="fnum_contabilidades" id="fnum_contabilidades" class="form-control input-sm">
                                <option value="">&nbsp;</option>
                                {Select::lista('cb_b005_contabilidades','pk_num_contabilidades','ind_descripcion',{$filtro.fnum_contabilidades})}
                            </select>
                            <label for="fnum_contabilidades">Tipo Contabilidad</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                           <select name="fnum_voucher" id="fnum_voucher" class="form-control input-sm">
                               <option value="">&nbsp;</option>
                                   {Select::lista('cb_c003_tipo_voucher', 'pk_num_voucher', 'ind_descripcion', {$filtro.fnum_voucher})}
                           </select>
                           <label for="fnum_voucher">Tipo Voucher</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm">
                            <input type="text" id="fperiodo" name="fperiodo" class="form-control" value="{if isset($filtro.fperiodo)}{$filtro.fperiodo}{/if}" style="text-align:center;" maxlength="10" readonly />
                            <label for="fperiodo">Período</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group form-group-sm ">
                           <select name="festado_voucher" id="festado_voucher" class="form-control input-sm">
                               <option value="">&nbsp;</option>
                                   {ConsultarDescripcion::options('cb_estado_voucher',$filtro.festado_voucher,'1')}
                           </select>
                           <label for="festado_voucher"> Estado </label>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-xs ink-reaction btn-raised btn-info" id="botonFiltro">
                            Filtrar
                        </button>
                    </div>
                </div>


            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

$('#fperiodo').datepicker({
        format: "yyyy-mm",
        minViewMode: 1,
        maxViewMode: 2,
        language: "es"
    });

   $(document).ready(function() {
        //  base
        var $base = '{$_Parametros.url}modCB/ListaVoucherCONTROL/';

        //  nuevo registro
        $('#nuevo').click(function() {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'nuevoMET', '', function($dato) {
                $('#ContenidoModal').html($dato);
            });
        });

        // Modificar Voucher
        $('#datatable1 tbody').on( 'click', '.modificar', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'editarMET', { id: $(this).attr('idRegistro')}, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        // Ver Voucher
        $('#datatable1 tbody').on( 'click', '.ver', function () {
            $('#formModalLabel').html($(this).attr('titulo'));
            $.post($base+'verMET', { id: $(this).attr('idRegistro'), estado: 'ver' }, function($dato){
                $('#ContenidoModal').html($dato);
            });
        });

        // Eliminar Voucher
        $('#datatable1 tbody').on( 'click', '.eliminar', function () {
            var id = $(this).attr('idRegistro');
            swal({
                title: $(this).attr('titulo'),
                text: $(this).attr('mensaje'),
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: $(this).attr('boton'),
                closeOnConfirm: false
            }, function(){
                $.post($base+'eliminarMET', { id: id }, function(dato){
                    //  mensaje
                    $(document.getElementById('id'+id)).remove();
                    swal("Registro Eliminado!", dato['mensaje'], "success");
                    $('#cerrar').click();
                },'json');
            });
        });

        // Anular Voucher
        $('#datatable1 tbody').on( 'click', '#anular_voucher', function () {
                var id = $(this).attr('idRegistro');
                    swal({
                        title: $(this).attr('titulo'),
                        text: $(this).attr('mensaje'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $(this).attr('boton'),
                        closeOnConfirm: false
                    }, function(){
                        $.post($base+'anularVoucherMET', { id: id, perfilM: "{in_array('CB-01-02-01-02-M',$_Parametros.perfil)}", perfilE: "{in_array('CB-01-02-01-03-E',$_Parametros.perfil)}" }, function(dato){
                            if (dato['status'] == 'error') {
                                swal("No puede ser Anulado!", dato['mensaje'], "success");
                                $('#cerrar').click();
                            }else{
                                $('#id'+dato['id']).html(dato['tr']);
                                swal("Operación Exitosa!", dato['mensaje'], "success");
                                $('#cerrar').click();
                            }
                       },'json');
                    });
        });

        // Imprimir Voucher
        $('#datatable1 tbody').on( 'click', '#imprimir_voucher', function () {
                var id = $(this).attr('idRegistro');
                $.post($("#imprimir_voucher").attr("action"), function(dato) {
                  //window.open($("#imprimir_voucher").attr("action")+"&id="+id, '_blank');
                  window.open($base+'imprimirVoucherMET'+"&id="+id, '_blank');
                });
        });


    });
</script>
