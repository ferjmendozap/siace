<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';

class PlanCuentaControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->PlanCuenta = $this->metCargarModelo('PlanCuenta');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';

        $this->atVista->metCargarCssComplemento($complementosCss);
        //$this->atVista->metCargarJsComplemento($complementoJs);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->PlanCuenta->metListar($_POST));
        $this->atVista->metRenderizar('PlanCuentaLista');
    }

    /**
    * Función que permite cargar formulario para el ingreso del nuevo registro
    */
    public function metNuevo()
    {

      ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'pk_num_cuenta' => null,
            'cod_cuenta' => null,
            'ind_grupo' => null,
            'ind_sub_grupo' => null,
            'ind_rubro' => null,
            'ind_cuenta' => null,
            'ind_sub_cuenta1' => null,
            'ind_sub_cuenta2' => null,
            'ind_sub_cuenta3' => null,
            'ind_descripcion' => null,
            'num_nivel' => null,
            'num_flag_tipo' => null,
            'fk_a006_num_miscelaneo_det' => null,
            'ind_tipo_saldo' => null,
            'ind_naturaleza' => null,
            'num_estatus' => null,
            'num_flag_tipo_cuenta' => '1',
            'estado' => 'nuevo',
        ];

        $this->atVista->assign('form',$form);
        $this->atVista->metRenderizar('PlanCuentaCrear','modales');
    }

    /**
    * Función que permite cargar el proceso para el ingreso del nuevo registro
    */
    public function metCrear()
    {
        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'cod_cuenta' => 'required|numeric|max_len,15',
            'ind_descripcion' => 'required|max_len,255',
            'fk_cbb005_num_contabilidades' => 'required|alpha_space|max_len,255',
        ]);

        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";
        if (!isset($_POST['num_flag_req_ccosto'])) $_POST['num_flag_req_ccosto']= "0";
        if (!isset($_POST['num_flag_req_activo'])) $_POST['num_flag_req_activo']= "0";

        ##  validación exitosa
        if($validate === TRUE) {

            if($_POST['num_flag_tipo_cuenta']=='1'){

                ## Obteniendo y armando datos segun nivel seleccionado para Pub20
                $_POST['ind_grupo']=  substr($_POST['cod_cuenta'], 0, 1);
                $_POST['ind_subgrupo']=  substr($_POST['cod_cuenta'], 1, 1);
                $_POST['ind_rubro']=  substr($_POST['cod_cuenta'], 2, 3);
                $_POST['ind_cuenta']=  substr($_POST['cod_cuenta'], 5, 2);
                $_POST['ind_sub_cuenta1']=  substr($_POST['cod_cuenta'], 7, 2);
                $_POST['ind_sub_cuenta2']=  substr($_POST['cod_cuenta'], 9, 2);
                $_POST['ind_sub_cuenta3']=  substr($_POST['cod_cuenta'], 11, 2);

            }
            else{

                ## Obteniendo y armando datos segun nivel seleccionado
                $_POST['ind_grupo']=  substr($_POST['cod_cuenta'], 0, 1);
                $_POST['ind_subgrupo']=  substr($_POST['cod_cuenta'], 1, 1);
                $_POST['ind_rubro']=  substr($_POST['cod_cuenta'], 2, 1);
                $_POST['ind_cuenta']=  substr($_POST['cod_cuenta'], 3, 2);
                $_POST['ind_sub_cuenta1']=  substr($_POST['cod_cuenta'], 5, 2);
                $_POST['ind_sub_cuenta2']=  substr($_POST['cod_cuenta'], 7, 2);
                $_POST['ind_sub_cuenta3']=  substr($_POST['cod_cuenta'], 9, 3);
            }

            ## crear registro
            $id = $this->PlanCuenta->metNuevo($_POST);
            ## consultar tipo tipo de cuenta
            $tipo_cuenta= $this->PlanCuenta->metMostrar($id);
            ## devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' =>'<tr id="id'.$id.'">
                          <td><label>'.$id.'</label></td>
                          <td><label>'.($tipo_cuenta['num_flag_tipo_cuenta']=="1"?"PUB20":"ONCOP").'</label></td>
                          <td><label>'.$_POST['cod_cuenta'].'</label></td>
                          <td><label>'.$_POST['ind_descripcion'].'</label></td>
                          <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Cuenta" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                            <button idRegistro="'.$id.'" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static"  title="Ver" descipcion="El Usuario esta viendo Plan Cuenta" titulo="Ver Cuenta" >
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                            </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                          </td>
                       </tr>
                   ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el formulario para la actualización de un registro
    */
    public function metEditar()
    {
         $db= $this->PlanCuenta->metMostrar($_POST['id']);
         $db['metodo'] = 'modificar';
         $db['estado'] = 'editar';

         $this->atVista->assign('form', $db);
         $this->atVista->metRenderizar('PlanCuentaCrear', 'modales');
    }

    /**
    * Función que permite cargar el formulario mostrando datos del registro
    */
    public function metVer()
    {
        ## datos formulario
         $db= $this->PlanCuenta->metMostrar($_POST['id']);
         $db['metodo'] = 'modificar';
         $db['estado'] = $_POST['estado'];

        ## vista
        $this->atVista->assign('form', $db);
        $this->atVista->metRenderizar('PlanCuentaCrear', 'modales');
    }

    /**
    * Función que permite cargar el proceso para el editado de un registro
    */
    public function metModificar()
    {
        $id = $_POST['pk_num_cuenta'];

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'cod_cuenta' => 'required|numeric|max_len,15',
            'ind_descripcion' => 'required|max_len,255',
            'fk_cbb005_num_contabilidades' => 'required|alpha_space|max_len,255',
        ]);

        ##  validación exitosa
        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";
        if (!isset($_POST['num_flag_req_ccosto'])) $_POST['num_flag_req_ccosto']= "0";
        if (!isset($_POST['num_flag_req_activo'])) $_POST['num_flag_req_activo']= "0";

        if($validate === TRUE) {

            if($_POST['num_flag_tipo_cuenta']=='1'){

                ## Obteniendo y armando datos segun nivel seleccionado para Pub20
                $_POST['ind_grupo']=  substr($_POST['cod_cuenta'], 0, 1);
                $_POST['ind_subgrupo']=  substr($_POST['cod_cuenta'], 1, 1);
                $_POST['ind_rubro']=  substr($_POST['cod_cuenta'], 2, 3);
                $_POST['ind_cuenta']=  substr($_POST['cod_cuenta'], 5, 2);
                $_POST['ind_sub_cuenta1']=  substr($_POST['cod_cuenta'], 7, 2);
                $_POST['ind_sub_cuenta2']=  substr($_POST['cod_cuenta'], 9, 2);
                $_POST['ind_sub_cuenta3']=  substr($_POST['cod_cuenta'], 11, 2);

            }
            else{

                ## Obteniendo y armando datos segun nivel seleccionado
                $_POST['ind_grupo']=  substr($_POST['cod_cuenta'], 0, 1);
                $_POST['ind_subgrupo']=  substr($_POST['cod_cuenta'], 1, 1);
                $_POST['ind_rubro']=  substr($_POST['cod_cuenta'], 2, 1);
                $_POST['ind_cuenta']=  substr($_POST['cod_cuenta'], 3, 2);
                $_POST['ind_sub_cuenta1']=  substr($_POST['cod_cuenta'], 5, 2);
                $_POST['ind_sub_cuenta2']=  substr($_POST['cod_cuenta'], 7, 2);
                $_POST['ind_sub_cuenta3']=  substr($_POST['cod_cuenta'], 9, 3);
            }

           ##  crear registro
           $this->PlanCuenta->metEditar($_POST);
           ## consultar tipo tipo de cuenta
           $tipo_cuenta= $this->PlanCuenta->metMostrar($id);
           ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => '
                            <td><label>'.$id.'</label></td>
                            <td><label>'.($tipo_cuenta['num_flag_tipo_cuenta']=="1"?"PUB20":"ONCOP").'</label></td>
                            <td><label>'.$_POST['cod_cuenta'].'</label></td>
                            <td><label>'.$_POST['ind_descripcion'].'</label></td>
                            <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                            <td align="center">
                        '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                        data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Modificar Registro">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>':'').'

                         <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Consultar"
                                                descipcion="El Usuario esta viendo Plan de Cuentas" titulo="Consultar Plan de Cuentas">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                         </button>

                        '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                        boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>':'').'
                           </td>
                    ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el proceso de eliminado de un registro
    */
    public function metEliminar()
    {

        $this->PlanCuenta->metEliminar(['pk_num_cuenta' => $_POST['id']]);

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro eliminado exitosamente',
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

    /**
     * Permite cargar datos para un selector
     */
    public function metSelectorCuentasContables()
    {
        $campos = $_POST;
        $this->atVista->assign('campos', $campos);
        $this->atVista->assign('listado',$this->PlanCuenta->metListar($_POST));
        $this->atVista->metRenderizar('selectorCuentasContables','modales');
    }

    /**
    * obtengo datos para cargar el tipo de cuenta
    * según la contabilidad
    */
    public function metCargarTipoCuenta()
    {
        if($_POST['v_tipo_cuenta']==1){
            $miscelaneo_tc= "CUENTPUB20";
        }else{
            $miscelaneo_tc= "CUENTONCOP";
        }
        echo json_encode(['valor' => Select::miscelaneo($miscelaneo_tc)]);
        exit();
    }

}
