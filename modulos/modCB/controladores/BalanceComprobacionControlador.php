<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'ConsultarDescripcion.php';

class BalanceComprobacionControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->BalanceComprobacion = $this->metCargarModelo('BalanceComprobacion');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
        );

        $complementosJs = array(
            'mask/jquery.mask',
            'bootstrap-datepicker/bootstrap-datepicker',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->Contabilidades = Select::parametros('CONTABILIDAD');
        $id = $this->BalanceComprobacion->metContabilidades($this->Contabilidades);
        $form['fk_cbb005_num_contabilidades'] = $id;
        $this->atVista->assign('form',$form);

        $this->atVista->metRenderizar('BalanceComprobacion');
    }

  
    public function metBalanceReportePdf()
    {
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        //$obtenerUsuario= $this->atBusquedaEquipo->metUsuarioReporte($usuario);
        $this->metObtenerLibreria('CabecerasReportePdf','modCB');
        /*$pdf=new ReporteBalanceSaldoAnteriorAcumulado('P','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();*/

        $filtro_cuenta= '';

        $valor_x= explode("-", $_POST['periodo']);
            $anio= $valor_x[0];
            $mes=  $valor_x[1];

        if ($_POST['num_cuenta0']=="" and $_POST['num_cuenta1']=="") {
           $filtro_cuenta= " AND a.fk_cbb004_num_cuenta>= '1' AND  a.fk_cbb004_num_cuenta<= '999999999999' ";
        }else{
           $filtro_cuenta= " AND a.fk_cbb004_num_cuenta>= '".$_POST['fk_cbb004_num_cuenta0']."' AND
                                 a.fk_cbb004_num_cuenta<= '".$_POST['fk_cbb004_num_cuenta1']."' ";
        }

        //# Inicialización de Variables Utilizadas
        $saldo_anterior_debe= 0; $saldo_anterior_haber= 0; $total_debe_mayor= 0; $total_debe= 0; $total_haber_mayor= 0; $total_haber= 0;
        $total_saldo_anterior_debe= 0; $total_saldo_anterior_haber= 0; $total_mov_mes_debe= 0; $total_mov_mes_haber= 0;
        $total_sueldo_actual_debe= 0; $total_sueldo_actual_haber= 0; $valor2= 0; $valor_capturado= 0; $contador= 0;
        $saldo_anterior_debe_ag= 0; $saldo_anterior_haber_ag= 0; $saldo_actual_haber_ag= 0; $mov_mes_debe_ag= 0;
        $mov_mes_haber= 0; $mov_mes_haber_ag= 0; $saldo_actual_debe_ag= 0; $saldo_actual_haber= 0; $mov_mes_debe= 0;
        $SALDO_ACTUAL_DEBE_TOTAL= 0; $SALDO_ACTUAL_HABER= 0; $SALDO_ACTUAL_HABER_TOTAL= 0; $saldo_anterior_detalle_ag= 0;
        $saldo_anterior_total= 0; $neto_mes_detalle_ag= 0; $neto_mes_total= 0; $saldo_actual_debe_haber_detalle_ag= 0;
        $saldo_actual_total= 0; $saldo_actual_debe= 0; $SALDO_ACTUAL_DEBE= 0; $cont_cuenta=0;

        //echo('/// fk_cbb005_num_contabilidades='.$_POST['fk_cbb005_num_contabilidades'].' -- tipo_cuenta='.$_POST['tipo_cuenta'].' -- Anio='.$anio.' -- filtro_cuenta='.$filtro_cuenta);
        $consulta= $this->BalanceComprobacion->metBalanceReportePdf($_POST['fk_cbb005_num_contabilidades'], $_POST['tipo_cuenta'], $anio,
            $filtro_cuenta);

        //foreach ($consulta as $f) {
            //if(count($f['pk_num_balance_cuenta']>0)){


                  if ($_POST['reporte']=='AC') {
                     $pdf=new ReporteBalanceAcumulado('P','mm','Letter');
                     $pdf->AliasNbPages();
                     $pdf->AddPage();

                      $cont=""; $paso="";
                     //for ($i=0; $i < count($f['pk_num_balance_cuenta']); $i++) {
                     foreach ($consulta as $f) {

                        switch ($mes) {
                                  case "01": $valor = "$f[num_saldo_balance01]"; break;
                                  case "02": $valor = "$f[num_saldo_balance02]"; break;
                                  case "03": $valor = "$f[num_saldo_balance03]"; break;
                                  case "04": $valor = "$f[num_saldo_balance04]"; break;
                                  case "05": $valor = "$f[num_saldo_balance05]"; break;
                                  case "06": $valor = "$f[num_saldo_balance06]"; break;
                                  case "07": $valor = "$f[num_saldo_balance07]"; break;
                                  case "08": $valor = "$f[num_saldo_balance08]"; break;
                                  case "09": $valor = "$f[num_saldo_balance09]"; break;
                                  case "10": $valor = "$f[num_saldo_balance10]"; break;
                                  case "11": $valor = "$f[num_saldo_balance11]"; break;
                                  case "12": $valor = "$f[num_saldo_balance12]"; break;
                                }

                        //formulas
                          if($mes != '')
                              if($f['num_saldo_inicial']>0) $saldo_anterior_debe+= $f['num_saldo_inicial'];else  $saldo_anterior_haber+= $f['num_saldo_inicial'];

                          if($mes >='01')
                              if($f['num_saldo_balance01']>0) $saldo_anterior_debe+= $f['num_saldo_balance01'];else $saldo_anterior_haber+= $f['num_saldo_balance01'];

                          if($mes >='02')
                              if($f['num_saldo_balance02']>0) $saldo_anterior_debe+= $f['num_saldo_balance02'];else $saldo_anterior_haber+= $f['num_saldo_balance02'];

                          if($mes >='03')
                              if($f['num_saldo_balance03']>0) $saldo_anterior_debe+= $f['num_saldo_balance03'];else $saldo_anterior_haber+= $f['num_saldo_balance03'];

                          if($mes >='04')
                              if($f['num_saldo_balance04']>0) $saldo_anterior_debe+= $f['num_saldo_balance04'];else $saldo_anterior_haber+= $f['num_saldo_balance04'];

                          if($mes >='05')
                              if($f['num_saldo_balance05']>0) $saldo_anterior_debe+= $f['num_saldo_balance05'];else $saldo_anterior_haber+= $f['num_saldo_balance05'];

                          if($mes >='06')
                              if($f['num_saldo_balance06']>0) $saldo_anterior_debe+= $f['num_saldo_balance06'];else $saldo_anterior_haber+= $f['num_saldo_balance06'];

                          if($mes >='07')
                              if($f['num_saldo_balance07']>0) $saldo_anterior_debe+= $f['num_saldo_balance07'];else $saldo_anterior_haber+= $f['num_saldo_balance07'];

                          if($mes >='08')
                              if($f['num_saldo_balance08']>0) $saldo_anterior_debe+= $f['num_saldo_balance08'];else $saldo_anterior_haber+= $f['num_saldo_balance08'];

                          if($mes >='09')
                              if($f['num_saldo_balance09']>0) $saldo_anterior_debe+= $f['num_saldo_balance09'];else $saldo_anterior_haber+= $f['num_saldo_balance09'];

                          if($mes >='10')
                              if($f['num_saldo_balance10']>0) $saldo_anterior_debe+= $f['num_saldo_balance10'];else $saldo_anterior_haber+= $f['num_saldo_balance10'];

                          if($mes >='11')
                              if($f['num_saldo_balance11']>0) $saldo_anterior_debe+= $f['num_saldo_balance11'];else $saldo_anterior_haber+= $f['num_saldo_balance11'];

                          if($mes =='12')
                              if($f['num_saldo_balance12']>0) $saldo_anterior_debe+= $f['num_saldo_balance12'];else $saldo_anterior_haber+= $f['num_saldo_balance12'];

                          ////------------------------------------------------------------------------ */
                          ////      Muestra total por agrupadores
                          ////------------------------------------------------------------------------ */
                          $CuentaCapt= $f['cod_cuenta'];

                          if($f['ind_tipo_contabilidad']=='T'){
                              if($f['num_nivel']=='3')$valor2 = substr($CuentaCapt, 0, -1);
                              else if($f['num_nivel']=='4')$valor2 = substr($CuentaCapt, 0, -3);
                              else if($f['num_nivel']=='5')$valor2 = substr($CuentaCapt, 0, -5);
                              else if($f['num_nivel']=='6')$valor2 = substr($CuentaCapt, 0, -7);
                              else if($f['num_nivel']=='7')$valor2 = substr($CuentaCapt, 0, -10);
                          }elseif($f['ind_tipo_contabilidad']=='F'){
                              if($f['num_nivel']=='3')$valor2 = substr($CuentaCapt, 0, -3);
                              else if($f['num_nivel']=='4')$valor2 = substr($CuentaCapt, 0, -5);
                              else if($f['num_nivel']=='5')$valor2 = substr($CuentaCapt, 0, -7);
                              else if($f['num_nivel']=='6')$valor2 = substr($CuentaCapt, 0, -9);
                              else if($f['num_nivel']=='7')$valor2 = substr($CuentaCapt, 0, -11);
                          }

                          if($valor_capturado==$valor2) $cont_cuenta=0;
                          else{
                              $cont = $cont + 1;
                              if(($cont==2)and($paso=='')){ $cont_cuenta=1; $cont=0;}
                              elseif($paso!='paso'){
                                 $cont_cuenta=0;
                                 $valor_capturado = $valor2;
                                 $paso='paso';
                                 $cont= $cont+1;
                              }else $cont_cuenta=1;
                          }

                          $contador+=1;

                          ////---------------------------------------------------- */
                          ////                  SALDO ACTUAL
                          ////---------------------------------------------------- */
                          $saldo_anterior_debe = round($saldo_anterior_debe, 2);
                          $saldo_anterior_haber = round($saldo_anterior_haber, 2);
                          $SALDO = $saldo_anterior_debe + $saldo_anterior_haber;

                          if($SALDO > 0){
                            $SALDO_ACTUAL_DEBE = $SALDO;
                            $SALDO_ACTUAL_DEBE_TOTAL+= $SALDO_ACTUAL_DEBE;
                          }elseif($SALDO < 0){
                            $SALDO_ACTUAL_HABER = $SALDO;
                            $SALDO_ACTUAL_HABER_TOTAL+= $SALDO_ACTUAL_HABER;
                          }

                          ////---------------------------------------------------- */
                          ////  MOVIMIENTO DEL MES = "sumatoria del SaldoBalance segun periodo"
                          ////---------------------------------------------------- */
                          if($valor>0) $mov_mes_debe= $valor; else $mov_mes_haber= $valor;

                          //// Informacion agrupadores movimiento mes
                          $mov_mes_debe_ag+= $mov_mes_debe;
                          $mov_mes_haber_ag+= $mov_mes_haber;

                          //// Totalizador movimiento del mes
                          $total_mov_mes_debe+= $mov_mes_debe;
                          $total_mov_mes_haber+= $mov_mes_haber;

                          ////------------------------------------------------------------------------ */
                          ////  SALDO ACTUAL = "sumatoria de los debe y haber de saldo anterior y
                          ////              movimiento del mes"
                          ////------------------------------------------------------------------------ */
                          $saldo_actual = ($mov_mes_debe + $saldo_anterior_debe) + ($mov_mes_haber + $saldo_anterior_haber);
                          if($saldo_actual>0)$saldo_actual_debe = $saldo_actual; else $saldo_actual_haber = $saldo_actual;

                          //// Informacion agrupadores saldo actual
                          $saldo_actual_debe_ag+= $saldo_actual_debe;
                          $saldo_actual_haber_ag+= $saldo_actual_haber;

                          //// Totalizador sueldo actual
                          $total_sueldo_actual_debe+= $saldo_actual_debe;
                          $total_sueldo_actual_haber+= $saldo_actual_haber;

                          ////------------------------------------------------------------------------ */
                          //// IMPRIMIR DATOS POR PANTALLA: "Muestra la informacion de detalle de las cuentas"
                          ////------------------------------------------------------------------------ */
                          if(($SALDO_ACTUAL_DEBE>0)or($SALDO_ACTUAL_HABER<0)){

                             $pdf->SetFillColor(202, 202, 202);
                             $pdf->SetFont('Arial', '', 7);
                             $pdf->Cell(20,4,$f['cod_cuenta'],0,0,'L');
                             $pdf->Cell(100,4,utf8_decode(substr($f['ind_descripcion'],0,80)),0,0,'L');
                             $pdf->Cell(20,4,number_format($SALDO_ACTUAL_DEBE,2,',','.'),'','','R');
                             $pdf->Cell(20,4,number_format(-1*$SALDO_ACTUAL_HABER,2,',','.'),'', 1,'R');
                          }
                            $SALDO_ACTUAL_DEBE=0;
                            $SALDO_ACTUAL_HABER=0;
                            $saldo_anterior_debe=0;
                            $saldo_anterior_haber=0;
                            $SALDO= 0;
                     }

                      $pdf->Cell(120, 4, 'Total: ', 1, 0, 'R');
                      $pdf->Cell(20, 4, number_format($SALDO_ACTUAL_DEBE_TOTAL, 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(20, 4, number_format((-1*$SALDO_ACTUAL_HABER_TOTAL), 2, ',', '.'), 1, 1, 'R');

                      $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(202, 202, 202); $pdf->SetTextColor(0, 0, 0);
                      $pdf->SetFont('Arial', 'B', 7);
                      $pdf->Cell(100,3,'',0,1,'L');
                      $pdf->Cell(60,3,'PREPARADO POR:',0,0,'L');
                      $pdf->Cell(90,3,'REVISADO POR:',0,0,'L');
                      $pdf->Cell(100,3,'CONFORMADO POR:',0,1,'L');

                      $pdf->Cell(100,5,'',0,0,'L');
                      $pdf->Cell(120,5,'',0,0,'L');
                      $pdf->Cell(100,5,'',0,1,'L');

                      // Nombres
                      $pdf->Cell(60,5,'LCDA. XXXX XXXX',0,0,'L');
                      $pdf->Cell(90,5,'LCDA. XXXX XXXX',0,0,'L');
                      $pdf->Cell(90,5,'LCDA. XXXX XXXX',0,1,'L');

                      // Cargos
                      $pdf->Cell(60,2,'ANALISTA CONTABLE I',0,0,'L');
                      $pdf->Cell(90,2,'DIRECTORA DE AMINISTRACION',0,0,'L');
                      $pdf->Cell(100,2,'DIRECTORA DE AMINISTRACION',0,1,'L');

                      //salida
                      $pdf->Output();
                  }
                  elseif ($_POST['reporte']=="SAC") {
                     $pdf=new ReporteBalanceSaldoAnteriorAcumulado('P','mm','Letter');
                     $pdf->AliasNbPages();
                     $pdf->AddPage();

                      $cont="";
                       //for ($i=0; $i < count($f['pk_num_balance_cuenta']); $i++) {
                      foreach ($consulta as $f) {

                        switch ($mes) {
                                  case "01": $valor = "$f[num_saldo_balance01]"; break;
                                  case "02": $valor = "$f[num_saldo_balance02]"; break;
                                  case "03": $valor = "$f[num_saldo_balance03]"; break;
                                  case "04": $valor = "$f[num_saldo_balance04]"; break;
                                  case "05": $valor = "$f[num_saldo_balance05]"; break;
                                  case "06": $valor = "$f[num_saldo_balance06]"; break;
                                  case "07": $valor = "$f[num_saldo_balance07]"; break;
                                  case "08": $valor = "$f[num_saldo_balance08]"; break;
                                  case "09": $valor = "$f[num_saldo_balance09]"; break;
                                  case "10": $valor = "$f[num_saldo_balance10]"; break;
                                  case "11": $valor = "$f[num_saldo_balance11]"; break;
                                  case "12": $valor = "$f[num_saldo_balance12]"; break;
                                }

                          //formulas
                          if($mes >='01')
                              if($f['num_saldo_inicial']>0) $saldo_anterior_debe+= $f['num_saldo_inicial'];else  $saldo_anterior_haber+= $f['num_saldo_inicial'];

                          if($mes >='02')
                            if($f['num_saldo_balance01']>0) $saldo_anterior_debe+= $f['num_saldo_balance01'];else $saldo_anterior_haber+= $f['num_saldo_balance01'];

                          if($mes >='03')
                            if($f['num_saldo_balance02']>0) $saldo_anterior_debe+= $f['num_saldo_balance02'];else $saldo_anterior_haber+= $f['num_saldo_balance02'];

                          if($mes >='04')
                            if($f['num_saldo_balance03']>0) $saldo_anterior_debe+= $f['num_saldo_balance03'];else $saldo_anterior_haber+= $f['num_saldo_balance03'];

                          if($mes >='05')
                            if($f['num_saldo_balance04']>0) $saldo_anterior_debe+= $f['num_saldo_balance04'];else $saldo_anterior_haber+= $f['num_saldo_balance04'];

                          if($mes >='06')
                            if($f['num_saldo_balance05']>0) $saldo_anterior_debe+= $f['num_saldo_balance05'];else $saldo_anterior_haber+= $f['num_saldo_balance05'];

                          if($mes >='07')
                            if($f['num_saldo_balance06']>0) $saldo_anterior_debe+= $f['num_saldo_balance06'];else $saldo_anterior_haber+= $f['num_saldo_balance06'];

                          if($mes >='08')
                            if($f['num_saldo_balance07']>0) $saldo_anterior_debe+= $f['num_saldo_balance07'];else $saldo_anterior_haber+= $f['num_saldo_balance07'];

                          if($mes >='09')
                            if($f['num_saldo_balance08']>0) $saldo_anterior_debe+= $f['num_saldo_balance08'];else $saldo_anterior_haber+= $f['num_saldo_balance08'];

                          if($mes >='10')
                            if($f['num_saldo_balance09']>0) $saldo_anterior_debe+= $f['num_saldo_balance09'];else $saldo_anterior_haber+= $f['num_saldo_balance09'];

                          if($mes >='11')
                            if($f['num_saldo_balance10']>0) $saldo_anterior_debe+= $f['num_saldo_balance10'];else $saldo_anterior_haber+= $f['num_saldo_balance10'];

                          if($mes =='12')
                            if($f['num_saldo_balance11']>0) $saldo_anterior_debe+= $f['num_saldo_balance11'];else $saldo_anterior_haber+= $f['num_saldo_balance11'];

                          ////------------------------------------------------------------------------ */
                          ////            Muestra total por agrupadores
                          ////------------------------------------------------------------------------ */
                          $CuentaCapt= $f['cod_cuenta'];

                          if($f['ind_tipo_contabilidad']=='T'){
                              if($f['num_nivel']=='3')$valor2 = substr($CuentaCapt, 0, -1);
                              else if($f['num_nivel']=='4')$valor2 = substr($CuentaCapt, 0, -3);
                              else if($f['num_nivel']=='5')$valor2 = substr($CuentaCapt, 0, -5);
                              else if($f['num_nivel']=='6')$valor2 = substr($CuentaCapt, 0, -7);
                              else if($f['num_nivel']=='7')$valor2 = substr($CuentaCapt, 0, -10);
                          }elseif($f['ind_tipo_contabilidad']=='F'){
                              if($f['num_nivel']=='3')$valor2 = substr($CuentaCapt, 0, -3);
                              else if($f['num_nivel']=='4')$valor2 = substr($CuentaCapt, 0, -5);
                              else if($f['num_nivel']=='5')$valor2 = substr($CuentaCapt, 0, -7);
                              else if($f['num_nivel']=='6')$valor2 = substr($CuentaCapt, 0, -9);
                              else if($f['num_nivel']=='7')$valor2 = substr($CuentaCapt, 0, -11);
                          }

                          if($valor_capturado==$valor2) $cont_cuenta=0;
                          elseif($cont>=1){
                                $valor_capturado2 = $valor_capturado;
                                $cont_cuenta=1;
                          }else{
                                $cont= $cont+1;
                                $valor_capturado = $valor2;
                                $cont_cuenta = 0;
                          }

                          //$contador+=1;
                          if($cont_cuenta==1){
                                $consulta2= $this->BalanceComprobacion->metConsulta2Pdf($valor_capturado2, $_POST['tipo_cuenta']);
/*
                                foreach ($consulta2 as $f2) {
                                  $pdf->SetFillColor(202, 202, 202);
                                  $pdf->SetFont('Arial', 'B', 7);
                                  $pdf->Cell(20,4,$f2['cod_cuenta'],0,0,'L');
                                  $pdf->Cell(60,4,utf8_decode(substr($f2['ind_descripcion'],0,40)),0,0,'L');
                                  $pdf->Cell(20,4,number_format($saldo_anterior_debe_ag,2,',','.'),'','','R');
                                  $pdf->Cell(20,4,number_format(-1*$saldo_anterior_haber_ag,2,',','.'),'','','R');
                                  $pdf->Cell(20,4,number_format($mov_mes_debe_ag,2,',','.'),'','','R');
                                  $pdf->Cell(20,4,number_format(-1*$mov_mes_haber_ag,2,',','.'),'','','R');
                                  $pdf->Cell(20,4,number_format($saldo_actual_debe_ag,2,',','.'),0,0,'R');
                                  $pdf->Cell(20,4,number_format(-1*$saldo_actual_haber_ag,2,',','.'),0,1,'R');
                                }
*/
                                $paso='';
                                $cont_cuenta= 0;
                                $saldo_anterior_debe_ag= 0; $saldo_anterior_haber_ag= 0;
                                $mov_mes_debe_ag = 0;       $mov_mes_haber_ag= 0;
                                $saldo_actual_debe_ag= 0;   $saldo_actual_haber_ag= 0;

                                $valor_capturado = $valor2;
                          }

                          ////------------------------------------------------------------------------ */
                          ////                                SALDO ANTERIOR
                          ////------------------------------------------------------------------------ */

                          //// Informacion para agrupadores
                          $saldo_anterior_debe_ag+=$saldo_anterior_debe ;
                          $saldo_anterior_haber_ag+=$saldo_anterior_haber;

                          //// Totalizador de saldo anterior
                          $total_saldo_anterior_debe+= $saldo_anterior_debe;
                          $total_saldo_anterior_haber+= $saldo_anterior_haber;

                          ////------------------------------------------------------------------------ */
                          ////    MOVIMIENTO DEL MES = "sumatoria del SaldoBalance segun periodo"
                          ////------------------------------------------------------------------------ */
                          if($valor>0)$mov_mes_debe = $valor;else $mov_mes_haber = $valor; //echo $f['cod_cuenta'].'-'.$valor.'*****';

                          //// Informacion agrupadores movimiento mes
                          $mov_mes_debe_ag+= $mov_mes_debe;
                          $mov_mes_haber_ag+= $mov_mes_haber;

                          //// Totalizador movimiento del mes
                          $total_mov_mes_debe+= $mov_mes_debe;
                          $total_mov_mes_haber+= $mov_mes_haber;

                          ////------------------------------------------------------------------------ */
                          ////    SALDO ACTUAL = "sumatoria de los debe y haber de saldo anterior y
                          ////                            movimiento del mes"
                          ////------------------------------------------------------------------------ */
                          $saldo_actual = ($mov_mes_debe + $saldo_anterior_debe) + ($mov_mes_haber + $saldo_anterior_haber);
                          if($saldo_actual>0)$saldo_actual_debe = $saldo_actual; else $saldo_actual_haber = $saldo_actual;

                          //// Informacion agrupadores saldo actual
                          $saldo_actual_debe_ag+= $saldo_actual_debe;
                          $saldo_actual_haber_ag+= $saldo_actual_haber;

                          //// Totalizador sueldo actual
                          $total_sueldo_actual_debe+= $saldo_actual_debe;
                          $total_sueldo_actual_haber+= $saldo_actual_haber;

                          ////------------------------------------------------------------------------ */
                          ////                    IMPRIMIR DATOS POR PANTALLA
                          ////            Muestra la informacion de detalle de las cuentas
                          ////------------------------------------------------------------------------ */
                         if($saldo_anterior_debe=='')$saldo_anterior_debe='0';
                         if($mov_mes_debe=='') $mov_mes_debe='0';
                         if($saldo_actual_debe=='') $saldo_actual_debe='0';

                         $pdf->SetFillColor(202, 202, 202);
                         $pdf->SetFont('Arial', '', 7);
                         $pdf->Cell(20,4,$f['cod_cuenta'],0,0,'L');
                         $pdf->Cell(60,4,utf8_decode(substr($f['ind_descripcion'],0,35)),0,0,'L');
                         $pdf->Cell(20,4,number_format($saldo_anterior_debe,2,',','.'),'','','R');
                         $pdf->Cell(20,4,number_format(-1*$saldo_anterior_haber,2,',','.'),'','','R');
                         $pdf->Cell(20,4,number_format($mov_mes_debe,2,',','.'),'','','R');
                         $pdf->Cell(20,4,number_format(-1*$mov_mes_haber,2,',','.'),'','','R');
                         $pdf->Cell(20,4,number_format($saldo_actual_debe,2,',','.'),'',0,'R');
                         $pdf->Cell(20,4,number_format(-1*$saldo_actual_haber,2,',','.'),'',1,'R');

                         $saldo_anterior_debe=''; $saldo_anterior_haber='';
                         $mov_mes_debe=''; $mov_mes_haber='';
                         $saldo_actual_debe=''; $saldo_actual_haber='';


                         if($contador==count($f['pk_num_balance_cuenta'])){

                            $consulta3= $this->BalanceComprobacion->metConsulta2Pdf($valor_capturado, $_POST['tipo_cuenta']);
/*
                            foreach ($consulta3 as $f3) {
                              $pdf->SetFillColor(202, 202, 202);
                              $pdf->SetFont('Arial', 'B', 7);
                              $pdf->Cell(20,4,$f3['cod_cuenta'],0,0,'L');
                              $pdf->Cell(60,4,substr($f3['ind_descripcion'],0,40),0,0,'L');
                              $pdf->Cell(20,4,number_format($saldo_anterior_debe_ag,2,',','.'),'','','R');
                              $pdf->Cell(20,4,number_format(-1*$saldo_anterior_haber_ag,2,',','.'),'','','R');
                              $pdf->Cell(20,4,number_format($mov_mes_debe_ag,2,',','.'),'','','R');
                              $pdf->Cell(20,4,number_format(-1*$mov_mes_haber_ag,2,',','.'),'','','R');
                              $pdf->Cell(20,4,number_format($saldo_actual_debe_ag,2,',','.'),0,0,'R');
                              $pdf->Cell(20,4,number_format(-1*$saldo_actual_haber_ag,2,',','.'),0,1,'R');
                            }
*/
                         }

                         $contador+=1;

                       }

                       /// Limpiando variables utilizadas para calculo
                       $Tmonto_mayor_debito =''; $Tmonto_mayor_creditos='';
                       $saldo_inicial_debe=''; $saldo_inicial_haber='';
                       $Total_mayor_creditos=''; $Total_mayor_debito='';


                       $total_debe_saldos = $total_debe + $total_debe_mayor;
                       $total_haber_saldos = $total_haber + $total_haber_mayor;

                       $pdf->Cell(50, 4, 'Total: ', 1, 0, 'R');
                       $pdf->Cell(20, 4, number_format($total_saldo_anterior_debe, 2, ',', '.'), 1, 0, 'R');
                       $pdf->Cell(20, 4, number_format((-1*$total_saldo_anterior_haber), 2, ',', '.'), 1, 0, 'R');
                       $pdf->Cell(20, 4, number_format($total_mov_mes_debe, 2, ',', '.'), 1, 0, 'R');
                       $pdf->Cell(20, 4, number_format((-1*$total_mov_mes_haber), 2, ',', '.'), 1, 0, 'R');
                       $pdf->Cell(20, 4, number_format($total_sueldo_actual_debe, 2, ',', '.'), 1, 0, 'R');
                       $pdf->Cell(20, 4, number_format((-1*$total_sueldo_actual_haber), 2, ',', '.'), 1, 0, 'R');

                     //salida
                     $pdf->Output();
                  }
                  elseif ($_POST['reporte']=='DH') {
                     $pdf=new ReporteBalanceDebeHaber('P','mm','Letter');
                     $pdf->AliasNbPages();
                     $pdf->AddPage();

                      $cont="";
                     //for ($i=0; $i < count($f['pk_num_balance_cuenta']); $i++) {
                     foreach ($consulta as $f) {
                       //echo " // siii=".count($f['pk_num_balance_cuenta']);
                       //echo " /// cod_cuenta=".$f['cod_cuenta'];
                        switch ($mes) {
                                  case "01": $valor = "$f[num_saldo_balance01]"; break;
                                  case "02": $valor = "$f[num_saldo_balance02]"; break;
                                  case "03": $valor = "$f[num_saldo_balance03]"; break;
                                  case "04": $valor = "$f[num_saldo_balance04]"; break;
                                  case "05": $valor = "$f[num_saldo_balance05]"; break;
                                  case "06": $valor = "$f[num_saldo_balance06]"; break;
                                  case "07": $valor = "$f[num_saldo_balance07]"; break;
                                  case "08": $valor = "$f[num_saldo_balance08]"; break;
                                  case "09": $valor = "$f[num_saldo_balance09]"; break;
                                  case "10": $valor = "$f[num_saldo_balance10]"; break;
                                  case "11": $valor = "$f[num_saldo_balance11]"; break;
                                  case "12": $valor = "$f[num_saldo_balance12]"; break;
                                }

                          //formulas
                          if($mes >='01')
                            if($f['num_saldo_inicial']>0) $saldo_anterior_debe+= $f['num_saldo_inicial'];else $saldo_anterior_haber+= $f['num_saldo_inicial'];

                          if($mes >='02')
                            if($f['num_saldo_balance01']>0) $saldo_anterior_debe+= $f['num_saldo_balance01'];else $saldo_anterior_haber+= $f['num_saldo_balance01'];

                          if($mes >='03')
                            if($f['num_saldo_balance02']>0) $saldo_anterior_debe+= $f['num_saldo_balance02'];else $saldo_anterior_haber+= $f['num_saldo_balance02'];

                          if($mes >='04')
                            if($f['num_saldo_balance03']>0) $saldo_anterior_debe+= $f['num_saldo_balance03'];else $saldo_anterior_haber+= $f['num_saldo_balance03'];

                          if($mes >='05')
                            if($f['num_saldo_balance04']>0) $saldo_anterior_debe+= $f['num_saldo_balance04'];else $saldo_anterior_haber+= $f['num_saldo_balance04'];

                          if($mes >='06')
                            if($f['num_saldo_balance05']>0) $saldo_anterior_debe+= $f['num_saldo_balance05'];else $saldo_anterior_haber+= $f['num_saldo_balance05'];

                          if($mes >='07')
                            if($f['num_saldo_balance06']>0) $saldo_anterior_debe+= $f['num_saldo_balance06'];else $saldo_anterior_haber+= $f['num_saldo_balance06'];

                          if($mes >='08')
                            if($f['num_saldo_balance07']>0) $saldo_anterior_debe+= $f['num_saldo_balance07'];else $saldo_anterior_haber+= $f['num_saldo_balance07'];

                          if($mes >='09')
                            if($f['num_saldo_balance08']>0) $saldo_anterior_debe+= $f['num_saldo_balance08'];else $saldo_anterior_haber+= $f['num_saldo_balance08'];

                          if($mes >='10')
                            if($f['num_saldo_balance09']>0) $saldo_anterior_debe+= $f['num_saldo_balance09'];else $saldo_anterior_haber+= $f['num_saldo_balance09'];

                          if($mes >='11')
                            if($f['num_saldo_balance10']>0) $saldo_anterior_debe+= $f['num_saldo_balance10'];else $saldo_anterior_haber+= $f['num_saldo_balance10'];

                          if($mes =='12')
                            if($f['num_saldo_balance11']>0) $saldo_anterior_debe+= $f['num_saldo_balance11'];else $saldo_anterior_haber+= $f['num_saldo_balance11'];


                          ////------------------------------------------------------------------------ */
                          ////            Muestra total por agrupadores
                          ////------------------------------------------------------------------------ */
                          $CuentaCapt= $f['cod_cuenta'];

                          if($f['ind_tipo_contabilidad']=='T'){
                              if($f['num_nivel']=='3')$valor2 = substr($CuentaCapt, 0, -1);
                              else if($f['num_nivel']=='4')$valor2 = substr($CuentaCapt, 0, -3);
                              else if($f['num_nivel']=='5')$valor2 = substr($CuentaCapt, 0, -5);
                              else if($f['num_nivel']=='6')$valor2 = substr($CuentaCapt, 0, -7);
                              else if($f['num_nivel']=='7')$valor2 = substr($CuentaCapt, 0, -10);
                          }elseif($f['ind_tipo_contabilidad']=='F'){
                              if($f['num_nivel']=='3')$valor2 = substr($CuentaCapt, 0, -3);
                              else if($f['num_nivel']=='4')$valor2 = substr($CuentaCapt, 0, -5);
                              else if($f['num_nivel']=='5')$valor2 = substr($CuentaCapt, 0, -7);
                              else if($f['num_nivel']=='6')$valor2 = substr($CuentaCapt, 0, -9);
                              else if($f['num_nivel']=='7')$valor2 = substr($CuentaCapt, 0, -11);
                          }

                          if($valor_capturado==$valor2) $cont_cuenta=0;
                          elseif($cont>=1){
                                $valor_capturado2 = $valor_capturado;
                                $cont_cuenta=1;
                          }else{
                                $cont= $cont+1;
                                $valor_capturado = $valor2;
                          }

                          //$contador+=1;

                          if($cont_cuenta==1){
                                $consulta2= $this->BalanceComprobacion->metConsulta2Pdf($valor_capturado2, $_POST['tipo_cuenta']);

                                if($saldo_anterior_debe_ag=='')$saldo_anterior_debe_ag='0';
                                if($saldo_actual_debe_ag=='')$saldo_actual_debe_ag='0';

                                foreach ($consulta2 as $f2) {
                                    $pdf->SetFillColor(202, 202, 202);
                                    $pdf->SetFont('Arial', 'B', 7);
                                    $pdf->Cell(20,4,$f2['cod_cuenta'],0,0,'L');
                                    $pdf->Cell(60,4,utf8_decode(substr($f2['ind_descripcion'],0,40)),0,0,'L');
                                    $pdf->Cell(25,4,number_format($saldo_anterior_detalle_ag,2,',','.'),'','','R');
                                    $pdf->Cell(20,4,number_format($mov_mes_debe_ag,2,',','.'),'','','R');
                                    $pdf->Cell(20,4,number_format(-1*$mov_mes_haber_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format($neto_mes_detalle_ag,2,',','.'),0,0,'R');
                                    $pdf->Cell(25,4,number_format($saldo_actual_debe_haber_detalle_ag,2,',','.'),0,1,'R');
                                }
                                $paso='';
                                $cont_cuenta= 0;
                                $saldo_anterior_detalle_ag= 0;
                                $mov_mes_debe_ag= 0;
                                $mov_mes_haber_ag= 0;
                                $neto_mes_detalle_ag= 0;
                                $saldo_actual_debe_haber_detalle_ag= 0;

                                $valor_capturado = $valor2;
                          }


                            ////------------------------------------------------------------------------ */
                            ////                  SALDO ANTERIOR
                            ////------------------------------------------------------------------------ */

                            //// SALDO ANTERIOR DETALLE PARA BALANCE COMPROBACION DEBE - HABER
                            $saldo_anterior_detalle = $saldo_anterior_debe + $saldo_anterior_haber;

                            //// AGRUPADORES PARA BALANCE COMPROBACION DEBE - HABER
                            $saldo_anterior_detalle_ag+= $saldo_anterior_detalle;

                            //// TOTALIZADOR PARA BALANCE COMPROBACION DEBE - HABER
                            $saldo_anterior_total+= $saldo_anterior_detalle;

                            //// Informacion para agrupadores
                            $saldo_anterior_debe_ag+=$saldo_anterior_debe ;
                            $saldo_anterior_haber_ag+=$saldo_anterior_haber;

                            //// Totalizador de saldo anterior
                            $total_saldo_anterior_debe+= $saldo_anterior_debe;
                            $total_saldo_anterior_haber+= $saldo_anterior_haber;

                            ////------------------------------------------------------------------------ */
                            ////  MOVIMIENTO DEL MES = "sumatoria del SaldoBalance segun periodo"
                            ////------------------------------------------------------------------------ */

                            //// Movimiento del Mes en la tabla ac_voucherdet
                            $consultar3 = $this->BalanceComprobacion->metConsulta3Pdf($mes, $anio, $f['fk_cbb004_num_cuenta'], $_POST['fk_cbb005_num_contabilidades'], $_POST['tipo_cuenta']);
                            $contador_ciclo= 0;
                            foreach ($consultar3 as $f3) {
                               $contador_ciclo+= 1;
                               if ($contador_ciclo==1) {
                                    $mov_mes_debe= $f3['Deudor'];
                                    $mov_mes_haber= $f3['Acreedor'];
                               }
                            }

                            //// Informacion agrupadores movimiento mes
                            $mov_mes_debe_ag+= $mov_mes_debe;
                            $mov_mes_haber_ag+= $mov_mes_haber;

                            //// Totalizador movimiento del mes
                            $total_mov_mes_debe+= $mov_mes_debe;
                            $total_mov_mes_haber+= $mov_mes_haber;

                            ////------------------------------------------------------------------------ */
                            ////      NETO DEL MES PARA BALANCE COMPROBACION DEBE-HABER
                            ////------------------------------------------------------------------------ */
                            $neto_mes_detalle = $mov_mes_debe - $mov_mes_haber;

                            //// AGRUPADOR NETO MES
                            $neto_mes_detalle_ag+= $neto_mes_detalle;

                            //// TOTALIZADOR PARA BALANCE COMPROBACION DEBE-HABER
                            $neto_mes_total+= $neto_mes_detalle;

                            ////------------------------------------------------------------------------ */
                            ////  SALDO ACTUAL = "sumatoria de los debe y haber de saldo anterior y
                            ////              movimiento del mes"
                            ////------------------------------------------------------------------------ */
                            $saldo_actual = ($mov_mes_debe + $saldo_anterior_debe) + ($mov_mes_haber + $saldo_anterior_haber);
                            if($saldo_actual>0)$saldo_actual_debe = $saldo_actual; else $saldo_actual_haber = $saldo_actual;

                            //// Informacion agrupadores saldo actual
                            $saldo_actual_debe_ag+= $saldo_actual_debe;
                            $saldo_actual_haber_ag+= $saldo_actual_haber;

                            //// Totalizador sueldo actual
                            $total_sueldo_actual_debe+= $saldo_actual_debe;
                            $total_sueldo_actual_haber+= $saldo_actual_haber;

                            //// SALDO ACTUAL PARA BALANCE COMPROBACION DEBE-HABER
                            $saldo_actual_debe_haber_detalle = $neto_mes_detalle + $saldo_anterior_detalle;

                            //// AGRUPADOR SALDO ACTUAL PARABALANCE COMPROBACION DEBE-HABER
                            $saldo_actual_debe_haber_detalle_ag+=$saldo_actual_debe_haber_detalle;

                            //// TOTALIZADOR PARA BALANCE COMPROBACION DEBE-HABER
                            $saldo_actual_total+=$saldo_actual_debe_haber_detalle;

                            ////------------------------------------------------------------------------ */
                            ////          IMPRIMIR DATOS POR PANTALLA
                            ////      Muestra la informacion de detalle de las cuentas
                            ////------------------------------------------------------------------------ */

                               if($saldo_anterior_debe=='') $saldo_anterior_debe='0';
                               if($mov_mes_debe=='') $mov_mes_debe='0';
                               if($saldo_actual_debe=='') $saldo_actual_debe='0';

                               $pdf->SetFillColor(202, 202, 202);
                               $pdf->SetFont('Arial', '', 7);
                               $pdf->Cell(20,4,$f['cod_cuenta'],0,0,'L');
                               $pdf->Cell(60,4,utf8_decode(substr($f['ind_descripcion'],0,35)),0,0,'L');
                               $pdf->Cell(25,4,number_format($saldo_anterior_detalle,2,',','.'),'','','R');
                               $pdf->Cell(20,4,number_format($mov_mes_debe,2,',','.'),'','','R');
                               $pdf->Cell(20,4,number_format(-1*$mov_mes_haber,2,',','.'),'','','R');
                               $pdf->Cell(25,4,number_format($neto_mes_detalle,2,',','.'),'',0,'R');
                               $pdf->Cell(25,4,number_format($saldo_actual_debe_haber_detalle,2,',','.'),'',1,'R');

                               $saldo_anterior_debe=''; $saldo_anterior_haber='';
                               $mov_mes_debe=''; $mov_mes_haber='';
                               $saldo_actual_debe=''; $saldo_actual_haber='';
                               //echo ' // contador='.$contador.' -- pk_num_balance_cuenta='.count($f['pk_num_balance_cuenta']);
                                if($contador==count($f['pk_num_balance_cuenta'])){

                                  $consulta2= $this->BalanceComprobacion->metConsulta2Pdf($valor_capturado, $_POST['tipo_cuenta']);

                                   foreach ($consulta2 as $f2) {
                                      $pdf->SetFillColor(202, 202, 202);
                                      $pdf->SetFont('Arial', 'B', 7);
                                      $pdf->Cell(20,4,$f2['cod_cuenta'],0,0,'L');
                                      $pdf->Cell(60,4,utf8_decode(substr($f2['ind_descripcion'],0,40)),0,0,'L');
                                      $pdf->Cell(25,4,number_format($saldo_anterior_detalle_ag,2,',','.'),'','','R');
                                      $pdf->Cell(20,4,number_format($mov_mes_debe_ag,2,',','.'),'','','R');
                                      $pdf->Cell(20,4,number_format(-1*$mov_mes_haber_ag,2,',','.'),'','','R');
                                      $pdf->Cell(25,4,number_format($neto_mes_detalle_ag,2,',','.'),0,0,'R');
                                      $pdf->Cell(25,4,number_format($saldo_actual_debe_haber_detalle_ag,2,',','.'),0,1,'R');
                                    }
                                }
                                $contador+=1;
                     }

                      /// Limpiando variables utilizadas para calculo
                      $Tmonto_mayor_debito =''; $Tmonto_mayor_creditos='';
                      $saldo_inicial_debe=''; $saldo_inicial_haber='';
                      $Total_mayor_creditos=''; $Total_mayor_debito='';

                      $total_debe_saldos = $total_debe + $total_debe_mayor;
                      $total_haber_saldos = $total_haber + $total_haber_mayor;

                      $pdf->Cell(50, 4, 'Total: ', 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format($saldo_anterior_total, 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(20, 4, number_format($total_mov_mes_debe, 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(20, 4, number_format((-1*$total_mov_mes_haber), 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format($neto_mes_total, 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format($saldo_actual_total, 2, ',', '.'), 1, 0, 'R');

                     $pdf->Output();
                  }
                  elseif ($_POST['reporte']=='DHAC') {

                      $pdf=new ReporteBalanceDebeHaberAcumulado('L','mm','Legal');
                      $pdf->AliasNbPages();
                      $pdf->AddPage();

                      //variables
                      $total_sumas_acumuladas_debe= 0;
                      $total_sumas_acumuladas_haber= 0;
                      $cont="";

                      //for ($i=0; $i < count($f['pk_num_balance_cuenta']); $i++) {
                      foreach ($consulta as $f) {

                        switch ($mes) {
                                  case "01": $valor = "$f[num_saldo_balance01]"; break;
                                  case "02": $valor = "$f[num_saldo_balance02]"; break;
                                  case "03": $valor = "$f[num_saldo_balance03]"; break;
                                  case "04": $valor = "$f[num_saldo_balance04]"; break;
                                  case "05": $valor = "$f[num_saldo_balance05]"; break;
                                  case "06": $valor = "$f[num_saldo_balance06]"; break;
                                  case "07": $valor = "$f[num_saldo_balance07]"; break;
                                  case "08": $valor = "$f[num_saldo_balance08]"; break;
                                  case "09": $valor = "$f[num_saldo_balance09]"; break;
                                  case "10": $valor = "$f[num_saldo_balance10]"; break;
                                  case "11": $valor = "$f[num_saldo_balance11]"; break;
                                  case "12": $valor = "$f[num_saldo_balance12]"; break;
                                }

                          //formulas
                          if($mes >='01')
                              if($f['num_saldo_inicial']>0) $saldo_anterior_debe+= $f['num_saldo_inicial'];else  $saldo_anterior_haber+= $f['num_saldo_inicial'];

                          if($mes >='02')
                            if($f['num_saldo_balance01']>0) $saldo_anterior_debe+= $f['num_saldo_balance01'];else $saldo_anterior_haber+= $f['num_saldo_balance01'];

                          if($mes >='03')
                            if($f['num_saldo_balance02']>0) $saldo_anterior_debe+= $f['num_saldo_balance02'];else $saldo_anterior_haber+= $f['num_saldo_balance02'];

                          if($mes >='04')
                            if($f['num_saldo_balance03']>0) $saldo_anterior_debe+= $f['num_saldo_balance03'];else $saldo_anterior_haber+= $f['num_saldo_balance03'];

                          if($mes >='05')
                            if($f['num_saldo_balance04']>0) $saldo_anterior_debe+= $f['num_saldo_balance04'];else $saldo_anterior_haber+= $f['num_saldo_balance04'];

                          if($mes >='06')
                            if($f['num_saldo_balance05']>0) $saldo_anterior_debe+= $f['num_saldo_balance05'];else $saldo_anterior_haber+= $f['num_saldo_balance05'];

                          if($mes >='07')
                            if($f['num_saldo_balance06']>0) $saldo_anterior_debe+= $f['num_saldo_balance06'];else $saldo_anterior_haber+= $f['num_saldo_balance06'];

                          if($mes >='08')
                            if($f['num_saldo_balance07']>0) $saldo_anterior_debe+= $f['num_saldo_balance07'];else $saldo_anterior_haber+= $f['num_saldo_balance07'];

                          if($mes >='09')
                            if($f['num_saldo_balance08']>0) $saldo_anterior_debe+= $f['num_saldo_balance08'];else $saldo_anterior_haber+= $f['num_saldo_balance08'];

                          if($mes >='10')
                            if($f['num_saldo_balance09']>0) $saldo_anterior_debe+= $f['num_saldo_balance09'];else $saldo_anterior_haber+= $f['num_saldo_balance09'];

                          if($mes >='11')
                            if($f['num_saldo_balance10']>0) $saldo_anterior_debe+= $f['num_saldo_balance10'];else $saldo_anterior_haber+= $f['num_saldo_balance10'];

                          if($mes =='12')
                            if($f['num_saldo_balance11']>0) $saldo_anterior_debe+= $f['num_saldo_balance11'];else $saldo_anterior_haber+= $f['num_saldo_balance11'];


                          ////------------------------------------------------------------------------ */
                          ////            Muestra total por agrupadores
                          ////------------------------------------------------------------------------ */
                          $CuentaCapt= $f['cod_cuenta'];

                          if($f['ind_tipo_contabilidad']=='T'){
                              if($f['num_nivel']=='3')$valor2 = substr($CuentaCapt, 0, -1);
                              else if($f['num_nivel']=='4')$valor2 = substr($CuentaCapt, 0, -3);
                              else if($f['num_nivel']=='5')$valor2 = substr($CuentaCapt, 0, -5);
                              else if($f['num_nivel']=='6')$valor2 = substr($CuentaCapt, 0, -7);
                              else if($f['num_nivel']=='7')$valor2 = substr($CuentaCapt, 0, -10);
                          }elseif($f['ind_tipo_contabilidad']=='F'){
                              if($f['num_nivel']=='3')$valor2 = substr($CuentaCapt, 0, -3);
                              else if($f['num_nivel']=='4')$valor2 = substr($CuentaCapt, 0, -5);
                              else if($f['num_nivel']=='5')$valor2 = substr($CuentaCapt, 0, -7);
                              else if($f['num_nivel']=='6')$valor2 = substr($CuentaCapt, 0, -9);
                              else if($f['num_nivel']=='7')$valor2 = substr($CuentaCapt, 0, -11);
                          }

                          if($valor_capturado==$valor2) $cont_cuenta=0;
                          elseif($cont>=1){
                                $valor_capturado2 = $valor_capturado;
                                $cont_cuenta=1;
                          }else{
                                $cont= $cont+1;
                                $valor_capturado = $valor2;
                          }

                          //$contador+=1;

                          if($cont_cuenta==1){
                                $consulta2= $this->BalanceComprobacion->metConsulta2Pdf($valor_capturado2, $_POST['tipo_cuenta']);

                                if($saldo_anterior_debe_ag=='')$saldo_anterior_debe_ag='0';
                                if($saldo_actual_debe_ag=='')$saldo_actual_debe_ag='0';

                                foreach ($consulta2 as $f2) {
                                    $pdf->SetFillColor(202, 202, 202);
                                    $pdf->SetFont('Arial', 'B', 7);
                                    $pdf->Cell(20,4,$f2['cod_cuenta'],0,0,'L');
                                    $pdf->Cell(80,4,utf8_decode(substr($f2['ind_descripcion'],0,40)),0,0,'L');
                                    $pdf->Cell(25,4,number_format($saldo_anterior_debe_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format(-1*$saldo_anterior_haber_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format($mov_mes_debe_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format(-1*$mov_mes_haber_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format($mov_mes_debe_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format(-1*$mov_mes_haber_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format($saldo_actual_debe_ag,2,',','.'),0,0,'R');
                                    $pdf->Cell(25,4,number_format(-1*$saldo_actual_haber_ag,2,',','.'),0,1,'R');
                                }
                                $paso='';

                                $cont_cuenta= 0;
                                $saldo_anterior_debe_ag= 0;
                                $saldo_anterior_haber_ag= 0;
                                $mov_mes_debe_ag= 0;
                                $mov_mes_haber_ag= 0;
                                $saldo_actual_debe_ag= 0;
                                $saldo_actual_haber_ag= 0;

                                $valor_capturado = $valor2;
                          }


                            ////------------------------------------------------------------------------ */
                            ////                  SALDO ANTERIOR
                            ////------------------------------------------------------------------------ */

                            //// Informacion para agrupadores
                            $saldo_anterior_debe_ag+=$saldo_anterior_debe ;
                            $saldo_anterior_haber_ag+=$saldo_anterior_haber;

                            //// Totalizador de saldo anterior
                            $total_saldo_anterior_debe+= $saldo_anterior_debe;
                            $total_saldo_anterior_haber+= $saldo_anterior_haber;

                            ////------------------------------------------------------------------------ */
                            ////  SUMAS ACUMULADAS = "sumatoria de los debe y haber de saldo anterior y
                            ////              movimiento del mes en tabla ac_voucherdet"
                            ////------------------------------------------------------------------------ */

                            //// Movimiento del Mes en la tabla ac_voucherdet
                            $consultar3 = $this->BalanceComprobacion->metConsulta3Pdf($mes, $anio, $f['fk_cbb004_num_cuenta'], $_POST['fk_cbb005_num_contabilidades'], $_POST['tipo_cuenta']);

                            /*
                            foreach ($consultar3 as $f3) {

                               if (count($f3['Deudor'])>0) {
                                    $mov_mes_debe= $f3['Deudor'];
                                    $mov_mes_haber= $f3['Acreedor'];
                               }
                            }
                            */

                            $contador_ciclo= 0;
                            foreach ($consultar3 as $f3) {
                               $contador_ciclo+= 1;
                               if ($contador_ciclo==1) {
                                    $mov_mes_debe= $f3['Deudor'];
                                    $mov_mes_haber= -$f3['Acreedor'];
                               }
                            }

                            //// Informacion agrupadores movimiento mes
                            $mov_mes_debe_ag+= $mov_mes_debe;
                            $mov_mes_haber_ag+= $mov_mes_haber;

                            //// Totalizador movimiento del mes
                            $total_mov_mes_debe+= $mov_mes_debe;
                            $total_mov_mes_haber+= $mov_mes_haber;

                            //// SUMAS ACUMULADAS DETALLE
                            $sumas_acumuladas_detalle_debe = $mov_mes_debe + $saldo_anterior_debe;
                            $sumas_acumuladas_detalle_haber = $mov_mes_haber + $saldo_anterior_haber;

                            //// TOTALIZADOR SUMAS ACUMULADAS
                            $total_sumas_acumuladas_debe+= $sumas_acumuladas_detalle_debe;
                            $total_sumas_acumuladas_haber+= $sumas_acumuladas_detalle_haber;


                            ////------------------------------------------------------------------------ */
                            ////  SALDO ACTUAL = "sumatoria de los debe y haber de saldo anterior y
                            ////              movimiento del mes"
                            ////------------------------------------------------------------------------ */
                            $saldo_actual = ($mov_mes_debe + $saldo_anterior_debe) + ($mov_mes_haber + $saldo_anterior_haber);
                            if($saldo_actual>0)$saldo_actual_debe = $saldo_actual; else $saldo_actual_haber = $saldo_actual;

                            //// Informacion agrupadores saldo actual
                            $saldo_actual_debe_ag+= $saldo_actual_debe;
                            $saldo_actual_haber_ag+= $saldo_actual_haber;

                            //// Totalizador sueldo actual
                            $total_sueldo_actual_debe+= $saldo_actual_debe;
                            $total_sueldo_actual_haber+= $saldo_actual_haber;

                            ////------------------------------------------------------------------------ */
                            ////          IMPRIMIR DATOS POR PANTALLA
                            ////      Muestra la informacion de detalle de las cuentas
                            ////------------------------------------------------------------------------ */

                             if($saldo_anterior_debe=='')$saldo_anterior_debe='0';
                             if($mov_mes_debe=='') $mov_mes_debe='0';
                             if($saldo_actual_debe=='') $saldo_actual_debe='0';

                             $pdf->SetFillColor(202, 202, 202);
                             $pdf->SetFont('Arial', '', 7);
                             $pdf->Cell(20,4,$f['cod_cuenta'],0,0,'L');
                             $pdf->Cell(80,4,utf8_decode(substr($f['ind_descripcion'],0,35)),0,0,'L');
                             $pdf->Cell(25,4,number_format($saldo_anterior_debe,2,',','.'),'','','R');
                             $pdf->Cell(25,4,number_format(-1*$saldo_anterior_haber,2,',','.'),'','','R');
                             $pdf->Cell(25,4,number_format($mov_mes_debe,2,',','.'),'','','R');
                             $pdf->Cell(25,4,number_format(-1*$mov_mes_haber,2,',','.'),'','','R');
                             $pdf->Cell(25,4,number_format($sumas_acumuladas_detalle_debe,2,',','.'),'','','R');
                             $pdf->Cell(25,4,number_format(-1*$sumas_acumuladas_detalle_haber,2,',','.'),'','','R');
                             $pdf->Cell(25,4,number_format($saldo_actual_debe,2,',','.'),'',0,'R');
                             $pdf->Cell(25,4,number_format(-1*$saldo_actual_haber,2,',','.'),'',1,'R');

                             $saldo_anterior_debe=''; $saldo_anterior_haber='';
                             $mov_mes_debe=''; $mov_mes_haber='';
                             $saldo_actual_debe=''; $saldo_actual_haber='';

                             if($contador==count($f['pk_num_balance_cuenta'])){

                                $consulta2= $this->BalanceComprobacion->metConsulta2Pdf($valor_capturado, $_POST['tipo_cuenta']);

                                 foreach ($consulta2 as $f2) {

                                    $pdf->SetFillColor(202, 202, 202);
                                    $pdf->SetFont('Arial', 'B', 7);
                                    $pdf->Cell(20,4,$f2['cod_cuenta'],0,0,'L');
                                    $pdf->Cell(80,4,utf8_decode(substr($f2['ind_descripcion'],0,40)),0,0,'L');
                                    $pdf->Cell(25,4,number_format($saldo_anterior_debe_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format(-1*$saldo_anterior_haber_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format($mov_mes_debe_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format(-1*$mov_mes_haber_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format($mov_mes_debe_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format(-1*$mov_mes_haber_ag,2,',','.'),'','','R');
                                    $pdf->Cell(25,4,number_format($saldo_actual_debe_ag,2,',','.'),0,0,'R');
                                    $pdf->Cell(25,4,number_format(-1*$saldo_actual_haber_ag,2,',','.'),0,1,'R');
                                 }
                             }
                             $contador+=1;

                      }

                      /// Limpiando variables utilizadas para calculo
                      $Tmonto_mayor_debito=0;  $Tmonto_mayor_creditos=0;
                      $saldo_inicial_debe=0;   $saldo_inicial_haber=0;
                      $Total_mayor_creditos=0; $Total_mayor_debito=0;

                      $total_debe_saldos = $total_debe + $total_debe_mayor;
                      $total_haber_saldos = $total_haber + $total_haber_mayor;

                      $pdf->Cell(70, 4, 'Total: ', 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format($total_saldo_anterior_debe, 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format((-1*$total_saldo_anterior_haber), 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format($total_mov_mes_debe, 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format((-1*$total_mov_mes_haber), 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format($total_sumas_acumuladas_debe, 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format((-1*$total_sumas_acumuladas_haber), 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format($total_sueldo_actual_debe, 2, ',', '.'), 1, 0, 'R');
                      $pdf->Cell(25, 4, number_format((-1*$total_sueldo_actual_haber), 2, ',', '.'), 1, 0, 'R');

                     //salida
                     $pdf->Output();
                  }
            //}
        //}
    }

}
