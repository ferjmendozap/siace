 <?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'ConsultarDescripcion.php';

class LibroMayorControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->LibroMayor = $this->metCargarModelo('LibroMayor');
    }

    public function metIndex()
    {
        $complementosCss = array(
          'DataTables/jquery.dataTables',
          'DataTables/extensions/dataTables.colVis941e',
          'DataTables/extensions/dataTables.tableTools4029',
          'bootstrap-datepicker/datepicker',
        );

         $complementosJs = array(
           'mask/jquery.mask',
           'bootstrap-datepicker/bootstrap-datepicker',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->Contabilidades = Select::parametros('CONTABILIDAD');
        $id = $this->LibroMayor->metContabilidades($this->Contabilidades);
        $form['fk_cbb005_num_contabilidades'] = $id;
        $this->atVista->assign('form',$form);

        $this->atVista->metRenderizar('LibroMayor');
    }

    public function metLibroMayorReportePdf()
    {
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('CabecerasReportePdf','modCB');
        $pdf=new ReporteLibroMayor('P','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        $filtro_cuenta= ''; $valorCuentaSubgrupo= '';

        $valor_x= explode("-", $_POST['periodo']);
            $anio= $valor_x[0];
            $mes=  $valor_x[1];

        if ($_POST['num_cuenta0']=="" and $_POST['num_cuenta1']=="") {
           $filtro_cuenta= " AND (a.fk_cbb004_num_cuenta>= '1' AND  a.fk_cbb004_num_cuenta<= '999999999999') ";
        }else{
           $filtro_cuenta= " AND (a.fk_cbb004_num_cuenta>= '".$_POST['fk_cbb004_num_cuenta0']."' AND
                                 a.fk_cbb004_num_cuenta<= '".$_POST['fk_cbb004_num_cuenta1']."') ";
        }

        $consulta= $this->LibroMayor->metLibroMayorReportePdf($_POST['fk_cbb002_num_libro_contabilidad'], $_POST['tipo_cuenta'], $anio, $mes, $filtro_cuenta, $_POST['fk_cbb005_num_contabilidades']);

        $codCuentaCapturada= 0; $CuentaCapt= 0; $t_debe= 0; $t_haber= 0; $debeAnterior= 0; $haberAnterior= 0;
        $valorSubGrupo = '';
        foreach ($consulta as $f) {

            if($f['ind_tipo_contabilidad']=='T' and $f['num_flag_tipo_cuenta']=$_POST['tipo_cuenta']){

                if($f['num_nivel']=='3') $valorCuentaSubgrupo = substr($f['cod_cuenta'], 0, -1);
                elseif($f['num_nivel']=='4') $valorCuentaSubgrupo = substr($f['cod_cuenta'], 0, -3);
                elseif($f['num_nivel']=='5') $valorCuentaSubgrupo = substr($f['cod_cuenta'], 0, -5);
                elseif($f['num_nivel']=='6') $valorCuentaSubgrupo = substr($f['cod_cuenta'], 0, -7);
                elseif($f['num_nivel']=='7') $valorCuentaSubgrupo = substr($f['cod_cuenta'], 0, -10);

            }elseif($f['ind_tipo_contabilidad']=='F' and $f['num_flag_tipo_cuenta']=$_POST['tipo_cuenta']){
                if($f['num_nivel']=='3')$valorCuentaSubgrupo = substr($f['cod_cuenta'], 0, -3);
                elseif($f['num_nivel']=='4')$valorCuentaSubgrupo = substr($f['cod_cuenta'], 0, -5);
                elseif($f['num_nivel']=='5')$valorCuentaSubgrupo = substr($f['cod_cuenta'], 0, -7);
                elseif($f['num_nivel']=='6')$valorCuentaSubgrupo = substr($f['cod_cuenta'], 0, -9);
                elseif($f['num_nivel']=='7')$valorCuentaSubgrupo = substr($f['cod_cuenta'], 0, -11);
            }
            ///  Obteniendo Descripción de SubGrupo
            $consulta2= $this->LibroMayor->metConsultaSubGrupo($valorCuentaSubgrupo, $_POST['tipo_cuenta']);
            foreach ($consulta2 as $f2) {

                if($CuentaCapt != $f2['cod_cuenta']){ /// condición para mostrar cuenta SubGrupo
                   $pdf->SetFillColor(202, 202, 202);
                   $pdf->SetFont('Arial', 'B', 8);
                   $pdf->Cell(10,6,$f2['cod_cuenta'],0,0,'L');
                   $pdf->Cell(25,6,$f2['ind_descripcion'],0,1,'L');
                   $CuentaCapt = $f2['cod_cuenta'];
                }
            }

            /// Obteniendo Saldo Anterior
            if($f['cod_cuenta'] != $codCuentaCapturada){
                 $codCuentaCapturada = $f['cod_cuenta'];

                 if($mes=='01' or $mes=='07'){
                      $sa_debe= '0,00'; $sa_haber = '0,00';
                      $pdf->SetFillColor(202, 202, 202);
                      $pdf->SetFont('Arial', 'B', 8);
                      $pdf->Cell(25,6,$f['cod_cuenta'],0,0,'L');
                      $pdf->Cell(115,6,utf8_decode(substr($f['ind_descripcion'], 0, 70)),0,0,'L');
                      $pdf->Cell(20,6,'SALDO ANTERIOR ->',0,0,'R');
                      $pdf->Cell(18,6,$sa_debe,0,0,'R');
                      $pdf->Cell(18,6,$sa_haber,0,1,'R');

                 }else{
                      $mes_anterior = $mes - 1 ;
                      if($mes_anterior<10) $mes_anterior= '0'.$mes_anterior;

                      $consulta3= $this->LibroMayor->metLibroMayorReportePdf2($f['pk_num_libro_contabilidad'], $_POST['tipo_cuenta'], $anio, $mes_anterior, $f['pk_num_cuenta']);
                      $consultaAterior = $this->LibroMayor->metDetalleReporte2($mes_anterior, $anio, $f['pk_num_libro_contabilidad'], $f['pk_num_cuenta']);
                      foreach ($consulta3 as $f3) {
                        # code...
                          $saldoAnterior = $consultaAterior[0]['num_debe'] - $consultaAterior[0]['num_haber'];
                          if($saldoAnterior >= 0){
                              $sa_debe = $saldoAnterior;
                              $sa_haber = '0';

                           }else{
                              $sa_debe = '0';
                              $sa_haber = $saldoAnterior;
                           }

                            $pdf->SetFillColor(202, 202, 202);
                            $pdf->SetFont('Arial', 'B', 8);
                            $pdf->Cell(25,6,$f3['cod_cuenta'],0,0,'L');
                            $pdf->Cell(20,6,utf8_decode(substr($f3['ind_descripcion'], 0, 70)),0,0,'L');
                            $pdf->Cell(110,6,'SALDO ANTERIOR ->',0,0,'R');
                            $pdf->Cell(18,6,number_format($sa_debe,2,',','.'),0,0,'R');
                            $pdf->Cell(18,6,number_format($sa_haber,2,',','.'),0,1,'R');
                      }
                }
            }

            // CONSULTO TABLA "cb_b001_voucher" Y "cb_c001_voucher_det"
            $consulta4= $this->LibroMayor->metDetalleReporte($mes, $anio, $f['pk_num_libro_contabilidad'], $f['pk_num_cuenta']);

            foreach ($consulta4 as $f4) {
              # code...
              ## Obteniendo datos del Periodo
              $valor_x= explode(" ", $f4['fec_fecha_voucher']);
              $valor_y= explode("-", $valor_x[0]);
              $fecha_Voucher= $valor_y[2].'-'.$valor_y[1].'-'.$valor_y[0];

              $t_debe+= $f4['num_debe'];
              $t_haber+= $f4['num_haber']; if($f4['num_haber']>0) $signo= "-"; else $signo="";

              $pdf->SetFillColor(255, 255, 255);
              $pdf->SetFont('Arial', '', 7);
              $pdf->SetWidths(array(18, 10, 18, 80, 15, 18, 18, 18));
              $pdf->SetAligns(array('C','C','C','L','R','R','R','R'));
              $pdf->Row(array($f4['ind_voucher'], $f4['num_linea'], $fecha_Voucher, $f4['txt_titulo_voucher'], $f4['num_persona'], '', number_format($f4['num_debe'],2,',','.'), $signo.number_format($f4['num_haber'],2,',','.')));
            }
            if($t_haber>0) $s= "-"; else $s="";
            $t_saldoActualCuenta = ($t_debe + $sa_debe) - ($t_haber - $sa_haber);
            $t_saldoActualCuenta = number_format($t_saldoActualCuenta,2,',','.');

            $t_debe = number_format($t_debe,2,',','.');
            $t_haber = number_format($t_haber,2,',','.');
            $pdf->SetFillColor(202, 202, 202);
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(100,4, '',0,0,'L');
            $pdf->Cell(48,4,'TOTAL MOVIMIENTO DEL MES->',0,0,'R');
            $pdf->Cell(30,4,$t_debe,0,0,'R');
            $pdf->Cell(18,4,$s.$t_haber,0,1,'R');

            if($t_saldoActualCuenta>0){
               $pdf->SetFont('Arial', 'B', 8);
               $pdf->Cell(100,4, '',0,0,'L');
               $pdf->Cell(48,4,'SALDO ACTUAL CUENTA '.''.$f['cod_cuenta'],0,0,'R');
               $pdf->Cell(30,4,$t_saldoActualCuenta,0,0,'R');
               $pdf->Cell(18,4,'',0,1,'R');
               $pdf->ln(2);
            }else{
               $pdf->SetFont('Arial', 'B', 8);
               $pdf->Cell(100,4, '',0,0,'L');
               $pdf->Cell(48,4,'SALDO ACTUAL CUENTA '.''.$f['cod_cuenta'],0,0,'R');
               $pdf->Cell(30,4,'',0,0,'R');
               $pdf->Cell(18,4,$t_saldoActualCuenta,0,1,'R');
               $pdf->ln(2);
            }

            $t_debe=0; $t_haber=0;


        }

       //salida
       $pdf->Output();
    }

}
