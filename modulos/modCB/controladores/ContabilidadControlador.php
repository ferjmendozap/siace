<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'ConsultarDescripcion.php';

class ContabilidadControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->Contabilidad = $this->metCargarModelo('Contabilidad');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->Contabilidad->metListar());
        $this->atVista->metRenderizar('Contabilidad');
    }

    /**
    * Función que permite cargar el proceso para el ingreso del nuevo registro
    */
    public function metNuevo()
    {
      ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'ind_tipo_contabilidad' => null,
            'ind_descripcion' => null,
            'ind_contabilidad_acronimo' => null,
            'num_estatus' => null,
            'pk_num_contabilidades' => null,
            'fec_ultima_modificacion' => null,
            'fk_a018_num_seguridad_usuario' => null,
            'listado_detalle' => [],
            'estado' => 'nuevo',
        ];

        $this->atVista->assign('form',$form);
        $this->atVista->metRenderizar('ContabilidadCrear','modales');
    }

    /**
    * Función que permite iniciar el proceso de ingreso del nuevo registro
    */
    public function metCrear()
    {
        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_tipo_contabilidad' => 'required|alpha_numeric|max_len,2',
            'ind_descripcion' => 'required|alpha_space|max_len,50',
            'ind_contabilidad_acronimo' => 'required|alpha_space|max_len,10',
        ]);

        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id= $this->Contabilidad->metNuevo($_POST);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' =>'<tr id="id'.$id.'">
                          <td><label>'.$id.'</label></td>
                          <td><label>'.$_POST['ind_tipo_contabilidad'].'</label></td>
                          <td><label>'.$_POST['ind_descripcion'].'</label></td>
                          <td><label>'.$_POST['ind_contabilidad_acronimo'].'</label></td>
                          <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Contabilidad" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                             <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Contabilidades" titulo="Ver Contabilidad">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                          </td>
                       </tr>
                   ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el proceso de editado de un registro
    */
    public function metEditar()
    {
         $form= $this->Contabilidad->metMostrar($_POST['id']);
         $form['listado_detalle']= $this->Contabilidad->metLibroContabilidad($form['pk_num_contabilidades']);
         $form['metodo'] = 'modificar';
         $form['estado'] = 'editar';

         $this->atVista->assign('form', $form);
         $this->atVista->metRenderizar('ContabilidadCrear', 'modales');
    }

    /**
    * Función que permite cargar y mostrar datos de un registro
    */
    public function metVer()
    {
        ## datos formulario
         $form= $this->Contabilidad->metMostrar($_POST['id']);
         $form['listado_detalle']= $this->Contabilidad->metLibroContabilidad($form['pk_num_contabilidades']);
         $form['metodo'] = 'ver';
         $form['estado'] = $_POST['estado'];

        ## vista
         $this->atVista->assign('form', $form);
         $this->atVista->metRenderizar('ContabilidadCrear', 'modales');
    }

    /**
    * Función que permite iniciar el proceso de editado de un registro
    */
    public function metModificar()
    {
        $id = $_POST['pk_num_contabilidades'];

        ##  valido formulario
        $gump = new GUMP();

        $validate = $gump->validate($_POST, [
            'ind_tipo_contabilidad' => 'required|alpha_numeric|max_len,2',
            'ind_descripcion' => 'required|alpha_space|max_len,50',
        ]);

        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $mensaje= $this->Contabilidad->metEditar($_POST);

            $valor_x= explode("|", $mensaje);
            $mensaje= $valor_x[0];
            $swal= $valor_x[1];
            //$this->Contabilidad->metEditar($_POST);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                //'mensaje' => 'Registro actualizado exitosamente',
                'mensaje' => $mensaje,
                'swal' => $swal,
                'id' => $id,
                'tr' => '
                          <td><label>'.$id.'</label></td>
                          <td><label>'.$_POST['ind_tipo_contabilidad'].'</label></td>
                          <td><label>'.$_POST['ind_descripcion'].'</label></td>
                          <td><label>'.$_POST['ind_contabilidad_acronimo'].'</label></td>
                          <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Registro" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Contabilidades" titulo="Ver Contabilidad">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!" title="Eliminar">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                          </td>
                ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar la acción de eliminar un registro
    */
    public function metEliminar()
    {

        $mensaje= $this->Contabilidad->metEliminar(['pk_num_contabilidades' => $_POST['id']]);

         $valor_x= explode("|", $mensaje);
            $mensaje= $valor_x[0];
            $valor= $valor_x[1];

        $jsondata = [
            'status' => 'OK',
            //'mensaje' => 'Registro eliminado exitosamente',
            'mensaje' =>  $mensaje,
            'valor'=> $valor,
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

    /**
    * Inserta lineas de detalle
    */
    public function metInsertar()
    {
      $tr='
        <tr id="detalle'.$_POST['numero'].'" >
           <td align="center">
                <div class="col-sm-10">
                    <div class="form-group form-group-sm floating-label">
                        <input type="text" name="detalle_cod_libro[]"  id="detalle_cod_libro'.$_POST['numero'].'" class="form-control input-sm" style="text-align:center;" disabled >
                        <label for="detalle_cod_libro">&nbsp;</label>
                    </div>
                </div>
           </td>
           <td>
                <div class="col-sm-12">
                    <div class="form-group form-group-sm floating-label">
                        <select name="fk_cbb002_num_libro_contable[]" id="detalle_num_libro_contable'.$_POST['numero'].'" data-id="'.$_POST['numero'].'" class="form-control" onchange="obtenerCodLibro($(this));" >
                        <option value="">&nbsp;</option>'.
                        Select::lista("cb_b002_libro_contable", "pk_num_libro_contabLe", "ind_descripcion").'
                        </select>
                    </div>
                </div>
           </td>
           <td>
                <div class="col-sm-2">
                 <button class="btn ink-reaction btn-raised btn-xs btn-danger"  onclick="$(\'#detalle'.$_POST['numero'].'\').remove();">
                    <i class="md md-delete" style="color: #ffffff;"></i>
                 </button>
                </div>
           </td>
        </tr>';

        echo $tr;
    }

    /**
    * Obtengo datos libro contable
    */
    public function metObtenerCodLibro()
    {
        $db = new db();
        $sql = "SELECT * FROM cb_b002_libro_contable WHERE pk_num_libro_contabLe= '".$_POST['detalle_num_libro']."'";
        $row = $db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        echo $field['cod_libro'];
    }

}
