<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'ConsultarDescripcion.php';

class BalanceGeneralControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->BalanceGeneral = $this->metCargarModelo('BalanceGeneral');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
        );

        $complementosJs = array(
                'mask/jquery.mask',
                'bootstrap-datepicker/bootstrap-datepicker',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->Contabilidades = Select::parametros('CONTABILIDAD');
        $id = $this->BalanceGeneral->metContabilidades($this->Contabilidades);
        $form['fk_cbb005_num_contabilidades'] = $id;
        $this->atVista->assign('form',$form);

        $this->atVista->metRenderizar('BalanceGeneral');
    }

    /**
    * Carga Reporte Balance General
    */
    public function metBalanceReportePdf()
    {
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('CabecerasReportePdf','modCB');

        $filtro_cuenta= '';
        $TM_ctactivo= '';
        $TM_ctpasivo= '';

        $valor_x= explode("-", $_POST['periodo_hasta']);
            $anio= $valor_x[0];
            $mes=  $valor_x[1];

        if ($_POST['num_cuenta0']=="" and $_POST['num_cuenta1']=="") {
           $filtro_cuenta= " AND a.fk_cbb004_num_cuenta>= '1' AND  a.fk_cbb004_num_cuenta<= '999999999999' ";
        }else{
           $filtro_cuenta= " AND a.fk_cbb004_num_cuenta>= '$_POST[fk_cbb004_num_cuenta0]' AND
                                 a.fk_cbb004_num_cuenta<= '$_POST[fk_cbb004_num_cuenta1]' ";
        }

        //# Inicialización de Variables Utilizadas
        $saldo_anterior_debe= 0; $saldo_anterior_haber= 0; $total_debe_mayor= 0; $total_debe= 0; $total_haber_mayor= 0; $total_haber= 0;
        $total_saldo_anterior_debe= 0; $total_saldo_anterior_haber= 0; $total_mov_mes_debe= 0; $total_mov_mes_haber= 0;
        $total_sueldo_actual_debe= 0; $total_sueldo_actual_haber= 0; $valor2= 0; $valor_capturado= 0; $contador= 0;
        $saldo_anterior_debe_ag= 0; $saldo_anterior_haber_ag= 0; $saldo_actual_haber_ag= 0; $mov_mes_debe_ag= 0;
        $mov_mes_haber= 0; $mov_mes_haber_ag= 0; $saldo_actual_debe_ag= 0; $saldo_actual_haber= 0; $mov_mes_debe= 0;
        $SALDO_ACTUAL_DEBE_TOTAL= 0; $SALDO_ACTUAL_HABER= 0; $SALDO_ACTUAL_HABER_TOTAL= 0; $saldo_anterior_detalle_ag= 0;
        $saldo_anterior_total= 0; $neto_mes_detalle_ag= 0; $neto_mes_total= 0; $saldo_actual_debe_haber_detalle_ag= 0;
        $saldo_actual_total= 0; $saldo_actual_debe= 0; $SALDO_ACTUAL_DEBE= 0; $cont_cuenta=0;


        $consulta= $this->BalanceGeneral->metBalanceReportePdf($_POST['fk_cbb005_num_contabilidades'], $_POST['tipo_cuenta'], $anio,
            $filtro_cuenta);


        $pdf=new ReporteBalanceGeneral('P','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        //------------------------------------------------------//
        $pdf->ln(5);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(180,2, utf8_decode('CUENTAS DEL TESORO'), 0, 1, 'C');
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(180,0,"_______________________________",0,1,'C');
        $pdf->ln(6);
        //------------------------------------------------------//

        //----------------------------- ## CUENTAS DEL TESORO ## ----------------------------//
        $niveles='3';
        $grupo='1';
        $sgrupo='1';   //ACTIVO CUENTAS DEL TESORO
        $sgrupo02='2'; //PASIVO CUENTAS DEL TESORO

        //ACTIVO CUENTAS DEL TESORO
        $consulta_2= $this->BalanceGeneral->metPlanCuenta($_POST['tipo_cuenta'], $niveles, $grupo, $sgrupo);

        if($consulta_2!=""){

          $y1 = 60;
          $pdf->SetFont('Arial', 'B', 7); $pdf->Cell(50,4,"ACTIVO",0,0,'R'); $pdf->Cell(100,4,"PASIVO",0,1,'R');

          foreach ($consulta_2 as $f_2) {

             $saldo= $this->BalanceGeneral->metObtenerSaldo($f_2['ind_rubro'], $_POST['fk_cbb005_num_contabilidades'], $anio, $mes, $filtro_cuenta);

             $TM_ctactivo= $saldo + $TM_ctactivo;


             $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
             $pdf->SetFont('Arial', '', 8);
             $pdf->SetWidths(array(12, 60, 21));
             $pdf->SetAligns(array('C', 'L', 'R'));
             $pdf->SetXY(10, $y1);$pdf->Row(array($f_2['ind_rubro'], utf8_decode($f_2['ind_descripcion']), number_format($saldo,2,',','.')));

             $y1+= 5;

             if($f_2['ind_rubro']=='132'){
               $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
               $pdf->SetFont('Arial', 'B', 8);
               $pdf->SetWidths(array(11, 60, 21));
               $pdf->SetAligns(array('C', 'R', 'R'));

               $pdf->SetXY(10, $y1); $pdf->Row(array('', '', '-------------------'));
               $y1+= 5;
               $MontoTesoroActivo = $TM_ctactivo;
               $pdf->SetXY(10, $y1); $pdf->Row(array('','Sub Totales - Cuentas del Tesoro=',number_format($TM_ctactivo,2,',','.')));
             }
          }
        }

        //PASIVO CUENTAS DEL TESORO
        $consulta_3= $this->BalanceGeneral->metPlanCuenta($_POST['tipo_cuenta'], $niveles, $grupo, $sgrupo02);

        if($consulta_3!=""){
          $y2 = 60;
          $saldo="0";

          foreach ($consulta_3 as $f_3) {

               $saldo= $this->BalanceGeneral->metObtenerSaldo($f_3['ind_rubro'], $_POST['fk_cbb005_num_contabilidades'], $anio, $mes, $filtro_cuenta);

               if($f_3['ind_rubro']=='199'){
                  $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                  $pdf->SetFont('Arial', '', 8);
                  $pdf->SetWidths(array(15, 60, 20, 20));
                  $pdf->SetAligns(array('C', 'R', 'R'));

                  $pdf->SetXY(105, $y2); $pdf->Row(array('', 'Sub Total=', number_format($TM_ctpasivo,2,',','.')));
                  $y2+= 5;
               }

               $TM_ctpasivo= $saldo + $TM_ctpasivo;

               //
               $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
               $pdf->SetFont('Arial', '', 8);
               $pdf->SetWidths(array(15, 60, 20));
               $pdf->SetAligns(array('C', 'L', 'R'));

               $pdf->SetXY(105, $y2); $pdf->Row(array($f_3['ind_rubro'], utf8_decode($f_3['ind_descripcion']), number_format($saldo,2,',','.')));

               $y2+= 5;

               if($f_3['ind_rubro']=='199'){
                $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->SetWidths(array(15, 60, 20));
                $pdf->SetAligns(array('C', 'R', 'R'));

                $pdf->SetXY(105, $y2); $pdf->Row(array('', '', '-------------------'));
                $y2+= 5;
                $MontoTesoroPasivo = $TM_ctpasivo;
                $pdf->SetXY(105, $y2); $pdf->Row(array('', '', number_format($TM_ctpasivo,2,',','.')));

               }
          }
        }

        //--------------------------- ## CUENTAS DE LA HACIENDA ## ---------------------------//
        $pdf->ln(8);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(180,2, utf8_decode('CUENTAS DE LA HACIENDA'), 0, 1, 'C');

        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(180,0,"_______________________________",0,1,'C');
        $pdf->ln(5);
        //------------------------------------------------------//

        $niveles='3';
        $grupo='2';
        $sgrupo='1';

        //ACTIVO CUENTAS DE LA HACIENDA
        $consulta_4= $this->BalanceGeneral->metPlanCuenta($_POST['tipo_cuenta'], $niveles, $grupo, $sgrupo);

        if($consulta_4!=""){
             $y1 = 129;
             $TM_ctactivo="";

             foreach ($consulta_4 as $f_4) {

                $saldo= $this->BalanceGeneral->metObtenerSaldo($f_4['ind_rubro'], $_POST['fk_cbb005_num_contabilidades'], $anio, $mes, $filtro_cuenta);

                $TM_ctactivo= $saldo + $TM_ctactivo;

                $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFont('Arial', '', 8);
                $pdf->SetWidths(array(12, 60, 20));
                $pdf->SetAligns(array('C', 'L', 'R'));

                $pdf->SetXY(10, $y1); $pdf->Row(array($f_4['ind_rubro'], utf8_decode($f_4['ind_descripcion']), number_format($saldo,2,',','.')));

                $y1+= 5;

                if($f_4['ind_rubro']=='240'){
                    $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->SetWidths(array(12, 60, 21));
                    $pdf->SetAligns(array('C', 'R', 'R'));

                    $pdf->SetXY(10, $y1); $pdf->Row(array('', '', '-------------------'));
                    $y1+= 5;
                    $MontoHaciendaActivo = $TM_ctactivo;
                    $pdf->SetXY(10, $y1); $pdf->Row(array('','Sub Totales - Cuentas de la Hacienda=',number_format($TM_ctactivo,2,',','.')));
                }
             }
        }

        //PASIVO CUENTAS DE LA HACIENDA
        $sgrupo02[0]='2'; $Rubro[0]='203';
        $sgrupo02[1]='2'; $Rubro[1]='221';
        $sgrupo02[2]='3'; $Rubro[2]='299';

        $fveces=3;

        $y2 = 130;

        for($i=0; $i<$fveces; $i++){

            $consulta_5= $this->BalanceGeneral->metPlanCuenta2($_POST['tipo_cuenta'], $niveles, $grupo, $sgrupo02[$i], $Rubro[$i]);

            if($consulta_5!=""){

              $TM_ctpasivo="";

              foreach ($consulta_5 as $f_5) {

                  $saldo= $this->BalanceGeneral->metObtenerSaldo($f_5['ind_rubro'], $_POST['fk_cbb005_num_contabilidades'], $anio, $mes, $filtro_cuenta);

                  $TM_ctpasivo= $saldo + $TM_ctpasivo;

                  //
                  $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                  $pdf->SetFont('Arial', '', 8);
                  $pdf->SetWidths(array(15, 60, 20));
                  $pdf->SetAligns(array('C', 'L', 'R'));

                  $pdf->SetXY(105, $y2); $pdf->Row(array($f_5['ind_rubro'], utf8_decode($f_5['ind_descripcion']), number_format($saldo,2,',','.')));

                  $y2+= 22;

                  if($f_5['ind_rubro']=='299'){
                    $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->SetWidths(array(15, 60, 20));
                    $pdf->SetAligns(array('C', 'R', 'R'));

                    $pdf->SetXY(105, $y2-16); $pdf->Row(array('', '', '-------------------'));
                    $y1+= 5;
                    $MontoHaciendaPasivo = $TM_ctpasivo;
                    $pdf->SetXY(105, $y2-12); $pdf->Row(array('','',number_format($TM_ctpasivo,2,',','.')));
                  }
              }
           }
        }

        //--------------------------- ## CUENTAS DEL PRESUPUESTO ## ---------------------------//
        $pdf->ln(8);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(180,2, utf8_decode('CUENTAS DEL PRESUPUESTO'), 0, 1, 'C');

        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(180,0,"_______________________________",0,1,'C');
        $pdf->ln(5);
        //------------------------------------------------------//

        $niveles='3';
        $grupo='6';
        $sgrupo='1';  //ACTIVO
        $grupo02='5'; //PASIVO

        //ACTIVO CUENTAS DEL PRESUPUESTO
        $consulta_6= $this->BalanceGeneral->metPlanCuenta($_POST['tipo_cuenta'], $niveles, $grupo, $sgrupo);

        if($consulta_6!=""){

          $y1 = 205;
          $TM_ctactivo="";

          foreach ($consulta_6 as $f_6) {

            $saldo= $this->BalanceGeneral->metObtenerSaldo($f_6['ind_rubro'], $_POST['fk_cbb005_num_contabilidades'], $anio, $mes, $filtro_cuenta);

            $TM_ctactivo= $saldo + $TM_ctactivo;

            $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Arial', '', 8);
            $pdf->SetWidths(array(12, 60, 22));
            $pdf->SetAligns(array('C', 'L', 'R'));

            $pdf->SetXY(10, $y1); $pdf->Row(array($f_6['ind_rubro'], utf8_decode($f_6['ind_descripcion']), number_format($saldo,2,',','.')));

            $y1+= 10;

            if($f_6['ind_rubro']=='300'){
                $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->SetWidths(array(12, 60, 22));
                $pdf->SetAligns(array('C', 'R', 'R'));

                $pdf->SetXY(10, $y1); $pdf->Row(array('', '', '-------------------'));
                $y1+= 5;
                $MontoActivo= $TM_ctactivo + $MontoHaciendaActivo + $MontoTesoroActivo;

                $pdf->SetXY(10, $y1); $pdf->Row(array('','Total=',number_format($MontoActivo,2,',','.')));
            }
          }
        }

        //PASIVO CUENTAS DEL PRESUPUESTO
        $consulta_7= $this->BalanceGeneral->metPlanCuenta($_POST['tipo_cuenta'], $niveles, $grupo02, $sgrupo);

        $TM_ctpasivo="";

        if ($consulta_7!="") {
            $y2 = 205;

            foreach ($consulta_7 as $f_7) {

                $saldo= $this->BalanceGeneral->metObtenerSaldo($f_7['ind_rubro'], $_POST['fk_cbb005_num_contabilidades'], $anio, $mes, $filtro_cuenta);

                $TM_ctpasivo= $saldo + $TM_ctpasivo;

                  //
                $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                $pdf->SetFont('Arial', '', 8);
                $pdf->SetWidths(array(15, 60, 22));
                $pdf->SetAligns(array('C', 'L', 'R'));

                  $pdf->SetXY(105, $y2); $pdf->Row(array($f_7['ind_rubro'], utf8_decode($f_7['ind_descripcion']), number_format($saldo,2,',','.')));


                  $y2+= 5;

                 if($f_7['ind_rubro']=='303'){
                      $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                      $pdf->SetFont('Arial', 'B', 8);
                      $pdf->SetWidths(array(15, 60, 22));
                      $pdf->SetAligns(array('C', 'R', 'R'));

                      $pdf->SetXY(105, $y2); $pdf->Row(array('', '', '-------------------'));
                      $y2+= 5; $MontoPasivo = $TM_ctpasivo + $MontoHaciendaPasivo + $MontoTesoroPasivo;

                      $pdf->SetXY(105, $y2); $pdf->Row(array('', '', number_format($MontoPasivo,2,',','.')));
                 }
            }
        }


        $pdf->Ln(5);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(202, 202, 202); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(100,3,'',0,1,'L');
        $pdf->Cell(75,3,'PREPARADO POR:',0,0,'L');
        $pdf->Cell(90,3,'REVISADO POR:',0,0,'L');
        $pdf->Cell(100,3,'CONFORMADO POR:',0,1,'L');

        $pdf->Cell(100,5,'',0,0,'L');
        $pdf->Cell(120,5,'',0,0,'L');
        $pdf->Cell(100,5,'',0,1,'L');

        $pdf->Cell(75,5,'LCDA. AAAAAAA AAAAAA',0,0,'L');
        $pdf->Cell(90,5,'LCDA. BBBBBBB BBBBBB',0,0,'L');
        $pdf->Cell(100,5,'ING. CCCCCCC CCCCCC ',0,1,'L');


        $pdf->Cell(75,2,'ANALISTA CONTABLE I',0,0,'L');
        $pdf->Cell(90,2,'DIRECTORA DE AMINISTRACION',0,0,'L');
        $pdf->Cell(100,2,'DIRECTOR GENERAL',0,1,'L');

      //salida
      $pdf->Output();
    }

}
