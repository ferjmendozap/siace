<?php
require_once ROOT.'librerias'.DS.'Select.php';
require_once 'datosCarga/dataBasicaInicial.php';
require_once 'datosCarga/dataBasicaInicialMenu.php';
require_once 'datosCarga/dataBasicaInicialMiscelaneos.php';
//require_once 'datosCarga/dataBasicaInicialParametros.php';

class scriptCargaControlador extends Controlador
{
    use dataBasicaInicial;
    use dataBasicaInicialMenu;
    use dataBasicaInicialMiscelaneos;
    //use dataBasicaInicialParametros;

    private $atScriptCarga;

    public function __construct()
    {
    	parent:: __construct();
    	$this->atScriptCarga = $this->metCargarModelo('scriptCarga');
    }

    public function metIndex()
    {
        echo "INICIANDO <br>";

        echo "MENU CONTABILIDAD<br>";
        $this->atScriptCarga->metCargarMenu($this->metDataMenu());

        echo "MISCELANEOS CONTABILIDAD<br>";
        $this->atScriptCarga->metCargarMiscelaneos($this->metDataMiscelaneos());

        //echo "PARAMETROS CONTABILIDAD<br>";
        //$this->atScriptCarga->metCargarParametros($this->metDataParametros());

        echo "DATOS CONTABILIDAD<br>";

        $this->metContabilidad();

        echo "TERMINADO";
    }

    public function metContabilidad()
    {
        echo "MAESTROS -> SISTEMA FUENTE<br>";
        $this->atScriptCarga->metCargarMaestroSistemaFuente($this->metDataSistemaFuente());

        echo "MAESTROS -> TIPO VOUCHER<br>";
        $this->atScriptCarga->metCargarMaestroTipoVoucher($this->metDataTipoVoucher());

		echo "MAESTROS -> PLAN DE CUENTA<br>";
        $this->atScriptCarga->metCargarMaestroPlanCuenta($this->metDataPlanCuenta());

        echo "MAESTROS -> LIBRO CONTABLE<br>";
        $this->atScriptCarga->metCargarMaestroLibroContable($this->metLibroContable());

        echo "MAESTROS -> CONTABILIDAD<br>";
        $this->atScriptCarga->metCargarMaestroContabilidad($this->metDataContabilidad());

        echo "MAESTROS -> LIBRO CONTABILIDAD<br>";
        $this->atScriptCarga ->metCargaMaestroLibroContabilidad($this->metDataLibroContabilidad());
    }

    public function metCBMenu()
    {
        //
        $this->atScriptCarga->metCargarMenuCB($this->metDataMenu());

    }


}
