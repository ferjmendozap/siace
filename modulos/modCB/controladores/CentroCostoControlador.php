<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';

class CentroCostoControlador extends Controlador
{

    function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->CentroCosto= $this->metCargarModelo('CentroCosto');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';

        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->CentroCosto->metListar());
        $this->atVista->metRenderizar('CentroCostoLista');
    }

    /**
    * Función que permite cargar el proceso para el ingreso del nuevo registro
    */
    public function metNuevo()
    {
      ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'pk_num_centro_costo' => null,
            'ind_descripcion_centro_costo' => null,
            'ind_abreviatura' => null,
            'num_flag_administrativo' => null,
            'num_flag_ventas' => null,
            'num_flag_financiera' => null,
            'num_flag_produccion' => null,
            'num_flag_centro_ingresos' => null,
            'num_estatus' => 1,
            'fk_a003_num_persona' => null,
            'fk_a025_num_subgrupo_centro_costo' => null,
            'fk_a004_num_dependencia' => null,
            'fk_a018_num_seguridad_usuario' => null,
            'fec_ultima_modificacion' => null,
            'pk_num_persona' => null,
            'ind_empleado' => null,
            'apellido' => null,
            'nombre' => null,
            'listado_detalle' => [],
            'estado' => 'nuevo',
            'cod_centro_costo' => null,
        ];

        $this->atVista->assign('form',$form);
        $this->atVista->metRenderizar('CentroCostoCrear','modales');
    }

    /**
    * Función que permite iniciar el proceso de ingreso del nuevo registro
    */
    public function metCrear()
    {
        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_abreviatura' => 'required|alpha|max_len,10',
            'ind_descripcion_centro_costo' => 'required|alpha_space|max_len,100',
            'cod_centro_costo' => 'required|alpha_numeric|max_len,4',
        ]);

        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";
        if (!isset($_POST['num_flag_administrativo'])) $_POST['num_flag_administrativo']= "0";
        if (!isset($_POST['num_flag_ventas'])) $_POST['num_flag_ventas']= "0";
        if (!isset($_POST['num_flag_financiera'])) $_POST['num_flag_financiera']= "0";
        if (!isset($_POST['num_flag_produccion'])) $_POST['num_flag_produccion']= "0";
        if (!isset($_POST['num_flag_centro_ingresos'])) $_POST['num_flag_centro_ingresos']= "0";

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id= $this->CentroCosto->metNuevo($_POST);
            $swal= "Registro Guardado!";
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'swal' => $swal,
                'id' => $id,
                'tr' =>'<tr id="id'.$id.'">
                          <td><label>'.$id.'</label></td>
                          <td><label>'.$_POST['ind_abreviatura'].'</label></td>
                          <td><label>'.$_POST['ind_descripcion_centro_costo'].'</label></td>
                          <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Grupo Centro Costo" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                             <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Grupo Centro Costo" titulo="Ver Grupo Centro Costo">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                          </td>
                       </tr>
                   ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el proceso de editado de un registro
    */
    public function metEditar()
    {
         $form= $this->CentroCosto->metMostrar($_POST['id']);
         $form['metodo'] = 'modificar';
         $form['estado'] = 'editar';

         $this->atVista->assign('form', $form);
         $this->atVista->metRenderizar('CentroCostoCrear', 'modales');
    }

    /**
    * Función que permite iniciar el proceso de editado de un registro
    */
    public function metModificar()
    {
        $id = $_POST['pk_num_centro_costo'];

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'ind_abreviatura' => 'required|alpha_numeric|max_len,10',
            'ind_descripcion_centro_costo' => 'required|alpha_space|max_len,100',
            'cod_centro_costo' => 'required|alpha_numeric|max_len,4',
        ]);

        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";
        if (!isset($_POST['num_flag_administrativo'])) $_POST['num_flag_administrativo']= "0";
        if (!isset($_POST['num_flag_ventas'])) $_POST['num_flag_ventas']= "0";
        if (!isset($_POST['num_flag_financiera'])) $_POST['num_flag_financiera']= "0";
        if (!isset($_POST['num_flag_produccion'])) $_POST['num_flag_produccion']= "0";
        if (!isset($_POST['num_flag_centro_ingresos'])) $_POST['num_flag_centro_ingresos']= "0";


        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $valores= $this->CentroCosto->metEditar($_POST);

            $valor_x= explode("|", $valores);
            $mensaje= $valor_x[0];
            $swal= $valor_x[1];
            $status= $valor_x[2];
            ##  devolver registro creado
             $jsondata = [
                'status' => $status,
                'mensaje' => $mensaje,
                'swal' => $swal,
                'id' => $id,
                'tr' =>'
                          <td><label>'.$id.'</label></td>
                          <td><label>'.$_POST['ind_abreviatura'].'</label></td>
                          <td><label>'.$_POST['ind_descripcion_centro_costo'].'</label></td>
                          <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Grupo Centro Costo" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                             <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Grupo Centro Costo" titulo="Ver Grupo Centro Costo">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                          </td>
                   ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar y mostrar datos de un registro
    */
    public function metVer()
    {
        ## datos formulario
         $form= $this->CentroCosto->metMostrar($_POST['id']);
         $form['metodo'] = 'ver';
         $form['estado'] = $_POST['estado'];

        ## vista
         $this->atVista->assign('form', $form);
         $this->atVista->metRenderizar('CentroCostoCrear', 'modales');
    }

    /**
    * Función que permite cargar la acción de eliminar un registro
    */
    public function metEliminar()
    {
        $mensaje= $this->CentroCosto->metEliminar(['pk_num_centro_costo' => $_POST['id']]);

         $valor_x= explode("|", $mensaje);
            $mensaje= $valor_x[0];
            $valor= $valor_x[1];

        $jsondata = [
            'status' => 'OK',
            'mensaje' =>  $mensaje,
            'valor'=> $valor,
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

    /**
    * Consulta de información en tabla para mostrar registro de persona
    */
    public function metObtenerPersona()
    {
        $db = new db();
        $sql = "SELECT a.*
                  FROM a003_persona a
                       INNER JOIN rh_b001_empleado b ON (b.fk_a003_num_persona=a.pk_num_persona AND b.num_estatus='1')
                 WHERE a.pk_num_persona= '".$_POST['pk_num_persona']."' ";
        $row = $db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        echo $field['pk_num_persona'];
    }

    /**
    * Función que permite cargar ventana con listado
    */
    public function metSelectorPersona()
    {
        $campos = $_POST;
        $this->atVista->assign('campos', $campos);
        $this->atVista->assign('listado_persona',$this->CentroCosto->metMostrar_persona());
        $this->atVista->metRenderizar('empleado','modales');
    }

    /**
    * Carga select de SubGrupo Centro Costo
    */
    public function metCargarSubGrupoCentroCosto()
    {
        $grupo_centro_costo= $_POST['grupo_centro_costo'];
        echo json_encode(['valor' => Select::Lista2('a025_subgrupo_centro_costo', 'pk_num_subgrupo_centro_costo', 'ind_descripcion', 'fk_a024_num_grupo_centro_costo', $grupo_centro_costo, '1')]);

        exit();
    }

    /**
    * Permite Consulta donde se obtienen datos para muestra en campo EMPLEADO
    */
    public function metCargarResponsableDependencia()
    {

        $data = '';
        $nombre_persona='';

        $db= new db();
        $sql= "SELECT a.*,
                      b.pk_num_persona,
                      b.ind_nombre1,
                      b.ind_apellido1
                 FROM a004_dependencia a
                      INNER JOIN a003_persona b on (b.pk_num_persona= a.fk_a003_num_persona_responsable)
                WHERE a.pk_num_dependencia= '".$_POST['pk_num_dependencia']."' ";
        $row= $db->query($sql);
        $row->setFetchMode(PDO::FETCH_ASSOC);
        $field = $row->fetch();

        $nombre_persona= $field['ind_nombre1'].' '.$field['ind_apellido1'];

        $jsondata = [
            'valor_pk_num_persona' => $field['pk_num_persona'],
            'nombre_persona' =>  $nombre_persona,
        ];
        echo json_encode($jsondata);
        exit();
    }

}
