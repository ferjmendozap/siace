<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';

class SistemaFuenteControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->SistemaFuente = $this->metCargarModelo('SistemaFuente');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->SistemaFuente->metListar());
        $this->atVista->metRenderizar('SistemaFuente');
    }

    /**
    * Función que permite cargar formulario para el ingreso del nuevo registro
    */
    public function metNuevo()
    {
      ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'cod_sistema_fuente' => null,
            'ind_descripcion' => null,
            'num_estatus' => null,
            'pk_num_sistema_fuente' => null,
            'estado' => 'nuevo',

        ];

        $this->atVista->assign('form',$form);
        $this->atVista->metRenderizar('SistemaFuenteCrear','modales');
    }

    /**
    * Función que permite cargar el proceso para el ingreso del nuevo registro
    */
    public function metCrear()
    {
        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'cod_sistema_fuente' => 'required|alpha_numeric|max_len,10',
            'ind_descripcion' => 'required|alpha_space|max_len,50',
        ]);

        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $this->SistemaFuente->metNuevo($_POST);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' =>'<tr id="id'.$id.'">
                          <td><label>'.$id.'</label></td>
                          <td><label>'.$_POST['cod_sistema_fuente'].'</label></td>
                          <td><label>'.$_POST['ind_descripcion'].'</label></td>
                          <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Sistema Fuente" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                             <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Libro Contable" titulo="Ver Sistema Fuente">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                          </td>
                       </tr>
                   ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el formulario para la actualización de un registro
    */
    public function metEditar()
    {

         $db= $this->SistemaFuente->metMostrar($_POST['id']);
         $db['metodo'] = 'modificar';
         $db['estado'] = 'editar';

         $this->atVista->assign('form', $db);
         $this->atVista->metRenderizar('SistemaFuenteCrear', 'modales');
    }

    /**
    * Función que permite cargar el formulario mostrando datos del registro
    */
    public function metVer()
    {
        ##  valores del formulario
        $db= $this->SistemaFuente->metMostrar($_POST['id']);
        $db['metodo'] = 'ver';
        $db['estado']= $_POST['estado'];
        ## vista
        $this->atVista->assign('form', $db);
        $this->atVista->metRenderizar('SistemaFuenteCrear', 'modales');
    }

    /**
    * Función que permite cargar el proceso para el editado de un registro
    */
    public function metModificar()
    {
        $id = $_POST['pk_num_sistema_fuente'];

        ##  valido formulario
        $gump = new GUMP();

        $validate = $gump->validate($_POST, [
            'cod_sistema_fuente' => 'required|alpha_numeric|max_len,10',
            'ind_descripcion' => 'required|alpha_space|max_len,50',
        ]);

        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $this->SistemaFuente->metEditar($_POST);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => '
                      <td><label>'.$id.'</label></td>
                      <td><label>'.$_POST['cod_sistema_fuente'].'</label></td>
                      <td><label>'.$_POST['ind_descripcion'].'</label></td>
                      <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                      <td align="center">
                        '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                        data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Sistema Fuente" title="Editar">
                                                    <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                </button>':'').'

                        <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" idRegistro="'.$id.'" title="Ver"
                                                descipcion="El Usuario esta viendo Sistema Fuente" titulo="Ver Sistema Fuente">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                        </button>

                        '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                        boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                    <i class="md md-delete" style="color: #ffffff;"></i>
                                                </button>':'').'
                      </td>
                ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el proceso de eliminado de un registro
    */
    public function metEliminar()
    {

        $this->SistemaFuente->metEliminar(['pk_num_sistema_fuente' => $_POST['id']]);

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro eliminado exitosamente',
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

}
