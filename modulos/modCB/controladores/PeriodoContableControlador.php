<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'ConsultarDescripcion.php';

class PeriodoContableControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->PeriodoContable = $this->metCargarModelo('PeriodoContable');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
            'bootstrap-datepicker/datepicker',
        );

        $complementosJs = array(
                'bootstrap-datepicker/bootstrap-datepicker',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->assign('listado',$this->PeriodoContable->metListar());
        $this->atVista->metRenderizar('PeriodoContable');
    }

    /**
    * Función que permite cargar formulario para el ingreso del nuevo registro
    */
    public function metNuevo()
    {

        ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'pk_num_control_cierre' => null,
            'periodo' => null,
            'estado' => 'nuevo',
        ];
        
        $this->atVista->assign('form',$form);
        $this->atVista->metRenderizar('PeriodoContableCrear','modales');
    }

    /**
    * Función que permite cargar el proceso para el ingreso del nuevo registro
    */
    public function metCrear()
    {

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'periodo' => 'required|alpha_dash|max_len,7',
        ]);

        $_POST['ind_estatus']= "A";

        ##  validación exitosa
        if($validate === TRUE){

           ## Obteniendo datos del Periodo
           $valor_x= explode("-", $_POST['periodo']);

            $_POST['ind_mes']=  $valor_x[1];
            $_POST['fec_anio']=  $valor_x[0];

           $libro_contable= ConsultarDescripcion::consultar($_POST['pk_num_libro_contable'],'pk_num_libro_contable','cb_b002_libro_contable');

            ##  crear registro
            $id = $this->PeriodoContable->metNuevo($_POST);

            $periodo= $_POST['fec_anio'].'-'.$_POST['ind_mes'];

            ##  devolver registro creado
            if($id=="0"){
                ##  devolver errores
                $jsondata = [
                    'status' => 'error',
                    'flag_mensaje' => '1',
                    'input' => $validate
                ];
            }else{
                $jsondata = [
                    'status' => 'crear',
                    'mensaje' => 'Registro creado exitosamente',
                    'id' => $id,
                    'tr' =>'<tr id="id'.$id.'">
                              <td><label>'.$id.'</label></td>
                              <td><label>'.$_POST['ind_tipo_registro'].'</label></td>
                              <td><label>'.$periodo.'</label></td>
                              <td><label>'.$libro_contable.'</label></td>
                              <td><i class="'.($_POST['ind_estatus']=="A"?"md md-check":"md md-not-interested").'"></i></td>
                              <td align="center">
                                   '.(($_POST['perfilM'] and $_POST['ind_estatus']=="A")?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" title="Editar"
                                                    data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_control_cierre}"
                                                    descipcion="El Usuario a Modificado un post" titulo="Editar Registro">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                          </button>':'').'

                                    '.(($_POST['ind_estatus']=="C")?'<button idRegistro="'.$id.'" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info" boton="Abrir"
                                                    descipcion="El usuario abrió un Período" titulo="Estas Seguro?" mensaje="Esta seguro que desea abrir el Período!!" id="abrir_periodo" title="Abrir"> <i class="md md-open-in-browser" style="color: #ffffff;"></i>
                                         </button>':'').'

                                    '.(($_POST['ind_estatus']=="A")?'<button idRegistro="'.$id.'" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="Cerrar"
                                                    descipcion="El usuario cerró un Período" titulo="Estas Seguro?" mensaje="Estas seguro que desea cerrar el Periodo!!" id="cerrar_periodo" title="Cerrar"> <i class="md md-close" style="color: #ffffff;"></i>
                                            </button>':'').'

                                    <button idRegistro="'.$id.'" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                            data-keyboard="false" data-backdrop="static"  title="Ver"
                                            descipcion="El Usuario esta viendo Periodo" titulo="Ver Período">
                                        <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                    </button>



                              </td>
                           </tr>
                       ',
                ];
            }

        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el formulario para la actualización de un registro
    */
    public function metEditar()
    {
         $db= $this->PeriodoContable->metMostrar($_POST['id']);
         $db['metodo'] = 'modificar';
         $db['periodo'] = $db['fec_anio'].'-'.$db['ind_mes'];
         $db['estado'] = "editar";
         $this->atVista->assign('form', $db);
         $this->atVista->metRenderizar('PeriodoContableCrear', 'modales');
    }

    /**
    * Función que permite cargar el formulario mostrando datos del registro
    */
    public function metVer()
    {
         $db= $this->PeriodoContable->metMostrar($_POST['id']);
         $db['metodo'] = 'ver';
         $db['periodo'] = $db['fec_anio'].'-'.$db['ind_mes'];
         $db['estado'] = $_POST['estado'];
         $this->atVista->assign('form', $db);
         $this->atVista->metRenderizar('PeriodoContableCrear', 'modales');
    }

    /**
    * Función que permite cargar el proceso para el editado de un registro
    */
    public function metModificar()
    {
        $id = $_POST['pk_num_control_cierre'];

        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'periodo' => 'required|alpha_dash|max_len,7',
        ]);

        if (!isset($_POST['ind_estatus'])) $_POST['ind_estatus']= "C";


        ##  validación exitosa
        if($validate === TRUE) {

            $valor_x= explode("-", $_POST['periodo']);
            $_POST['ind_mes']=  $valor_x[1];
            $_POST['fec_anio']=  $valor_x[0];

            $libro_contable= ConsultarDescripcion::consultar($_POST['pk_num_libro_contable'],'pk_num_libro_contable','cb_b002_libro_contable');

            ##  crear registro
            $this->PeriodoContable->metEditar($_POST);

            $periodo= $_POST['fec_anio'].'-'.$_POST['ind_mes'];

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => '
                          <td><label>'.$id.'</label></td>
                          <td><label>'.$_POST['ind_tipo_registro'].'</label></td>
                          <td><label>'.$periodo.'</label></td>
                          <td><label>'.$libro_contable.'</label></td>
                          <td><i class="'.($_POST['ind_estatus']=="A"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">

                            '.(($_POST['perfilM'] and $_POST['ind_estatus']=="A")?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary" data-toggle="modal" data-target="#formModal" title="Editar"
                                                    data-keyboard="false" data-backdrop="static" idRegistro="{$registro.pk_num_control_cierre}"
                                                    descipcion="El Usuario a Modificado un post" titulo="Editar Registro">
                                                <i class="fa fa-edit" style="color: #ffffff;"></i>
                                          </button>':'').'

                            '.(($_POST['ind_estatus']=="C")?'<button idRegistro="'.$id.'" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info" boton="Abrir"
                                                    descipcion="El usuario abrió un Período" titulo="Estas Seguro?" mensaje="Esta seguro que desea abrir el Período!!" id="abrir_periodo" title="Abrir"> <i class="md md-open-in-browser" style="color: #ffffff;"></i>
                                         </button>':'').'

                            '.(($_POST['ind_estatus']=="A")?'<button idRegistro="'.$id.'" class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger" boton="Cerrar"
                                                    descipcion="El usuario cerró un Período" titulo="Estas Seguro?" mensaje="Estas seguro que desea cerrar el Periodo!!" id="cerrar_periodo" title="Cerrar"> <i class="md md-close" style="color: #ffffff;"></i>
                                            </button>':'').'

                            <button idRegistro="'.$id.'" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                    data-keyboard="false" data-backdrop="static"  title="Ver"
                                    descipcion="El Usuario esta viendo Periodo" titulo="Ver Período">
                                <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                            </button>

                          </td>
                ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    // NO UTILIZADO
    public function metEliminar()
    {

        $this->PeriodoContable->metEliminar(['pk_num_control_cierre' => $_POST['id']]);

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro eliminado exitosamente',
            'id' => $_POST['id'],
        ];
        echo json_encode($jsondata);
        exit();
    }

    /**
    * Inicia el proceso de cerrar período
    */
    public function metCerrarPeriodo()
    {

        $this->PeriodoContable->metCerrarPeriodo(['pk_num_control_cierre' => $_POST['id']]);

        $db= $this->PeriodoContable->metMostrar($_POST['id']);
        $periodo= $db['fec_anio'].'-'.$db['ind_mes'];

        $libro_contable= ConsultarDescripcion::consultar($db['fk_cbb002_cod_libro_contable'],'pk_num_libro_contable','cb_b002_libro_contable');

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro cerrado exitosamente',
            'id' => $_POST['id'],
            'tr' => '
                  <td><label>'.$_POST['id'].'</label></td>
                  <td><label>'.$db['ind_tipo_registro'].'</label></td>
                  <td><label>'.$periodo.'</label></td>
                  <td><label>'.$libro_contable.'</label></td>
                  <td><i class="'.($db['ind_estatus']=="A"?"md md-check":"md md-not-interested").'"></i></td>
                  <td align="center">
                            '.(($_POST['perfilM'] and $db['ind_estatus']=="A")?'<button idRegistro="'.$_POST['id'].'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Registro" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            <!--&nbsp;&nbsp;
                            '.(($_POST['perfilE'])?'<button idRegistro="'.$_POST['id'].'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!" title="Eliminar">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            &nbsp;&nbsp;
                            -->
                            '.(($db['ind_estatus']=="C")?'<button  idRegistro="'.$_POST['id'].'"  class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info"  boton="Abrir"
                                                    titulo="Estas Seguro?" mensaje="Estas seguro que desea abrir el Período!!" id="abrir_periodo" title="Abrir">
                                                <i class="md md-open-in-browser" style="color: #ffffff;"></i>
                                            </button>':'').'


                            '.(($db['ind_estatus']=="A")?'<button  idRegistro="'.$_POST['id'].'"  class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  boton="Cerrar"
                                                    titulo="Estas Seguro?" mensaje="Estas seguro que desea cerrar el Periodo!!" id="cerrar_periodo" title="Cerrar">
                                                <i class="md md-close" style="color: #ffffff;"></i>
                                            </button>':'').'

                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" id="ver" idRegistro="'.$_POST['id'].'" title="Ver"
                                                descipcion="El Usuario esta viendo Periodo" titulo="Ver Periodo">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>
                </td>
                ',
            ];
        echo json_encode($jsondata);
        exit();
    }

    /**
    * Inicia el proceso de abrir período
    */
    public function metAbrirPeriodo()
    {
        $this->PeriodoContable->metAbrirPeriodo(['pk_num_control_cierre' => $_POST['id']]);

        $db= $this->PeriodoContable->metMostrar($_POST['id']);
        $periodo= $db['fec_anio'].'-'.$db['ind_mes'];

        $libro_contable= ConsultarDescripcion::consultar($db['fk_cbb002_cod_libro_contable'],'pk_num_libro_contable','cb_b002_libro_contable');

        $jsondata = [
            'status' => 'OK',
            'mensaje' => 'Registro cerrado exitosamente',
            'id' => $_POST['id'],
            'tr' => '
                  <td><label>'.$_POST['id'].'</label></td>
                  <td><label>'.$db['ind_tipo_registro'].'</label></td>
                  <td><label>'.$periodo.'</label></td>
                  <td><label>'.$libro_contable.'</label></td>
                  <td><i class="'.($db['ind_estatus']=="A"?"md md-check":"md md-not-interested").'"></i></td>
                  <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$_POST['id'].'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Registro" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            <!--&nbsp;&nbsp;
                            '.(($_POST['perfilE'])?'<button idRegistro="'.$_POST['id'].'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Estas seguro que desea eliminar el Registro!!" title="Eliminar" >
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                            &nbsp;&nbsp;
                            -->
                            '.(($db['ind_estatus']=="C")?'<button  idRegistro="'.$_POST['id'].'"  class="logsUsuario btn ink-reaction btn-raised btn-xs btn-info"  boton="Abrir"
                                            titulo="Estas Seguro?" mensaje="Estas seguro que desea abrir el Período!!" id="abrir_periodo" title="Abrir">
                                                <i class="md md-open-in-browser" style="color: #ffffff;"></i>
                                            </button>':'').'

                            '.(($db['ind_estatus']=="A")?'<button  idRegistro="'.$_POST['id'].'"  class="logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"  boton="Cerrar"
                                            titulo="Estas Seguro?" mensaje="Estas seguro que desea cerrar el Periodo!!" id="cerrar_periodo" title="Cerrar">
                                                <i class="md md-close" style="color: #ffffff;"></i>
                                            </button>':'').'

                            <button class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static" id="ver" idRegistro="'.$_POST['id'].'" title="Ver"
                                                descipcion="El Usuario esta viendo Periodo" titulo="Ver Periodo">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                </td>
                ',
            ];
        echo json_encode($jsondata);
        exit();
    }

}
