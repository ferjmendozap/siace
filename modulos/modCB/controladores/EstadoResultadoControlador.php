<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';
require_once ROOT.'librerias'.DS.'Select.php';
require_once ROOT.'librerias'.DS.'ConsultarDescripcion.php';

class EstadoResultadoControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->EstadoResultado = $this->metCargarModelo('EstadoResultado');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'bootstrap-datepicker/datepicker',
        );

        $complementosJs = array(
                'bootstrap-datepicker/bootstrap-datepicker',
        );

        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $js[] = 'Scripts/Form';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJsComplemento($complementosJs);
        $this->atVista->metCargarJs($js);

        $this->Contabilidades = Select::parametros('CONTABILIDAD');
        $id = $this->EstadoResultado->metContabilidades($this->Contabilidades);
        $form['fk_cbb005_num_contabilidades'] = $id;
        $this->atVista->assign('form',$form);

        $this->atVista->metRenderizar('EstadoResultado');
    }

    /**
    * Carga Reporte Estado de Resultado
    */
    public function metBalanceReportePdf()
    {
        ini_set("max_execution_time","9000");
        $usuario= Session::metObtener('idUsuario');
        $this->metObtenerLibreria('CabecerasReportePdf','modCB');

        $filtro_cuenta= '';
        $TM_ctactivo= '';
        $TM_ctpasivo= '';

        $valor_x= explode("-", $_POST['periodo_hasta']);
            $anio= $valor_x[0];
            $mes=  $valor_x[1];

        $filtro_cuenta= " AND a.fk_cbb004_num_cuenta>= '1' AND  a.fk_cbb004_num_cuenta<= '999999999999' ";

        //# Inicialización de Variables Utilizadas
        $saldo_anterior_debe= 0; $saldo_anterior_haber= 0; $total_debe_mayor= 0; $total_debe= 0; $total_haber_mayor= 0; $total_haber= 0;
        $total_saldo_anterior_debe= 0; $total_saldo_anterior_haber= 0; $total_mov_mes_debe= 0; $total_mov_mes_haber= 0;
        $total_sueldo_actual_debe= 0; $total_sueldo_actual_haber= 0; $valor2= 0; $valor_capturado= 0; $contador= 0;
        $saldo_anterior_debe_ag= 0; $saldo_anterior_haber_ag= 0; $saldo_actual_haber_ag= 0; $mov_mes_debe_ag= 0;
        $mov_mes_haber= 0; $mov_mes_haber_ag= 0; $saldo_actual_debe_ag= 0; $saldo_actual_haber= 0; $mov_mes_debe= 0;
        $SALDO_ACTUAL_DEBE_TOTAL= 0; $SALDO_ACTUAL_HABER= 0; $SALDO_ACTUAL_HABER_TOTAL= 0; $saldo_anterior_detalle_ag= 0;
        $saldo_anterior_total= 0; $neto_mes_detalle_ag= 0; $neto_mes_total= 0; $saldo_actual_debe_haber_detalle_ag= 0;
        $saldo_actual_total= 0; $saldo_actual_debe= 0; $SALDO_ACTUAL_DEBE= 0; $cont_cuenta=0;


        $pdf=new ReporteEstadoResultado('P','mm','Letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        //------------------------------------------------------//
        $pdf->ln(5);

        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(30,2,'5', 0, 0, 'L');
        $pdf->Cell(100,2, utf8_decode('INGRESOS'), 0, 1, 'L');

        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(30,2,'', 0, 0, 'C');
        $pdf->Cell(100,0,"___________",0,1,'L');
        $pdf->ln(4);

        $niveles='3';
        $sgrupo='1';
        $grupo02='5';
        $TM_ctpasivo="";
        $totalIngresos="";
        $y2="";
        $MontoHaciendaPasivo="";
        $MontoTesoroPasivo="";

        //INGRESO
        $consulta_2= $this->EstadoResultado->metPlanCuenta($_POST['tipo_cuenta'], $niveles, $grupo02, $sgrupo);

        if($consulta_2!=""){

            foreach ($consulta_2 as $f_2) {

                 $saldo= $this->EstadoResultado->metObtenerSaldo($f_2['ind_rubro'], $_POST['fk_cbb005_num_contabilidades'], $anio, $mes, $filtro_cuenta);

                 $totalIngresos= $saldo + $totalIngresos;

                 $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                 $pdf->SetFont('Arial', '', 8);
                 $pdf->SetWidths(array(30, 120, 40));
                 $pdf->SetAligns(array('L', 'L','L'));
                 $pdf->Row(array($f_2['ind_grupo'].'-'.$f_2['ind_subgrupo'].'-'.$f_2['ind_rubro'], utf8_decode($f_2['ind_descripcion']), number_format(-1*$saldo,2,',','.')));

                 if($f_2['ind_rubro']=='303'){

                     $pdf->Cell(169,1,"---------------------",0,1,'R');

                     $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                     $pdf->SetFont('Arial', 'B', 8);

                     $pdf->SetWidths(array(30, 120, 40));
                     $pdf->SetAligns(array('C', 'L', 'R'));
                     $pdf->Row(array('', utf8_decode('TOTAL INGRESOS'), number_format(-1*$totalIngresos,2,',','.')));


                     $pdf->Cell(190,0,"---------------------",0,1,'R');
                     $pdf->Cell(190,2,"---------------------",0,1,'R');

                     $y2+= 5; $MontoPasivo = $TM_ctpasivo + $MontoHaciendaPasivo + $MontoTesoroPasivo;
                     $pdf->SetXY(105, $y2); $pdf->Row(array('', '', number_format($MontoPasivo,2,',','.')));
                 }
            }
        }

        //------------------------------------------------------//
        //                 GASTOS PRESUPUESTARIOS               //
        //------------------------------------------------------//
        $pdf->ln(60);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(30,2,'6-1-300', 0, 0, 'L');
        $pdf->Cell(100,2, utf8_decode('GASTOS PRESUPUESTARIOS'), 0, 1, 'L');

        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(30,2,'', 0, 0, 'C');
        $pdf->Cell(100,0,"______________________________",0,1,'L');
        $pdf->ln(6);

        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(30,2,'6-1-300-01', 0, 0, 'L');
        $pdf->Cell(100,3, utf8_decode('GASTOS PERSONAL'), 0, 1, 'L');

        /// -------------------------------------------------- //
        $gpgrupo= '6';
        $gpSubGrupo= '1';
        $Rubro= '300';
        $gpCuenta= '01';
        $gpnivel= '4';
        $TM_ctpasivo="";
        $totalIngresos2="";

        $consulta_3= $this->EstadoResultado->metPlanCuenta2($_POST['tipo_cuenta'], $gpnivel, $gpgrupo, $gpSubGrupo, $gpCuenta);

        if($consulta_3!=""){

            foreach ($consulta_3 as $f_3) {

               $saldo= $this->EstadoResultado->metObtenerSaldo2($f_3['ind_rubro'], $_POST['fk_cbb005_num_contabilidades'], $anio, $mes, $filtro_cuenta, $f_3['Cod_Cuenta']);

               $totalIngresos2+=$saldo;
               //
               if ($saldo>'0') {
                  # code...
                    $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                  $pdf->SetFont('Arial', '', 8);
                  $pdf->SetWidths(array(30, 98, 40));
                  $pdf->SetAligns(array('L', 'L', 'R'));
                  $pdf->Row(array($f_3['ind_grupo'].'-'.$f_3['ind_subgrupo'].'-'.$f_3['ind_rubro'].'-'.$f_3['cod_cuenta'].'-'.$f_3['ind_sub_cuenta1'].$f_3['ind_sub_cuenta2'], utf8_decode($f_3['ind_descripcion']), number_format($saldo,2,',','.')));
               }
            }
        }


        /// Incluyendo las cuentas de Transferencias y Donaciones (6-1-300-07)
        $gpgrupo= '6';
        $gpSubGrupo= '1';
        $Rubro= '300';
        $gpCuenta= '07';
        $gpnivel= '5';
        $totalIngresos2="0";

        $consulta_4= $this->EstadoResultado->metPlanCuenta2($_POST['tipo_cuenta'], $gpnivel, $gpgrupo, $gpSubGrupo, $gpCuenta);

        if($consulta_4!=""){

            foreach ($consulta_4 as $f_4) {

               $saldo= $this->EstadoResultado->metObtenerSaldo2($f_4['ind_rubro'], $_POST['fk_cbb005_num_contabilidades'], $anio, $mes, $filtro_cuenta, $f_4['Cod_Cuenta']);
               //echo "saldo=".$saldo;
               $totalIngresos2+=$saldo;
               //
               if ($saldo>'0') {
                  # code...
                    $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                  $pdf->SetFont('Arial', '', 8);
                  $pdf->SetWidths(array(30, 98, 40));
                  $pdf->SetAligns(array('L', 'L', 'R'));
                  $pdf->Row(array($f_4['ind_grupo'].'-'.$f_4['ind_subgrupo'].'-'.$f_4['ind_grupo'].'-'.$f_4['Cod_Cuenta'].'-'.$f_4['ind_sub_cuenta1'].$f_4['ind_sub_cuenta2'].$f_4['ind_sub_cuenta3'], utf8_decode($f_4['ind_descripcion']), number_format($saldo,2,',','.')));
               }
            }
        }

        $pdf->ln(2);
        $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->SetWidths(array(30, 118, 40));
        $pdf->SetAligns(array('L', 'L', 'R'));
        $pdf->Row(array('', utf8_decode('TOTAL GASTOS DE PERSONAL'), number_format($totalIngresos2,2,',','.')));
        $pdf->Ln(3);


        //------------------------------------------------------//
        //                 GASTOS DE FUNCIONAMIENTO             //
        //------------------------------------------------------//
        $gpgrupo= '6';
        $gpSubGrupo= '1';
        $Rubro= '300';
        $gpCuenta= '02';
        $gpnivel= '4';
        $valorParametro=" AND Cod_Cuenta<>'07' ";
        $TM_ctpasivo="";
        $totalIngresos3="0";
        $totalXcuenta="";
        $montosip="";
        $nro_cuenta="";
        $pase="";

        $consulta_5= $this->EstadoResultado->metPlanCuenta3($_POST['tipo_cuenta'], $gpgrupo, $gpSubGrupo, $gpCuenta, $valorParametro);

        if($consulta_5!=""){

            foreach ($consulta_5 as $f_5) {
               /// para mostrar totalizador por cuenta
               if ($f_5['Cod_Cuenta']!=$nro_cuenta and $pase==1 and $montosip==1) {
                      # code...
                      $pdf->ln(2);
                      $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                      $pdf->SetFont('Arial', 'B', 8);
                      $pdf->SetWidths(array(30, 118, 40));
                      $pdf->SetAligns(array('L', 'L', 'R'));
                      $pdf->Row(array('', 'TOTAL '.utf8_decode(strtr(strtoupper($Descripcion2), "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÜÚ", "àáâãäåæçèéêëìíîïðñòóôõöøùüú")), number_format($totalXcuenta,2,',','.')));
                      $pdf->Ln(3);

                      $totalXcuenta="";
                      $montosip="";
               }
               /// ----------------------------

               if($f_5['num_nivel']=='4'){
                     $nro_cuenta= $f_5['Cod_Cuenta'];
                     $cuentaMostrar= $f_5['ind_grupo'].'-'.$f_5['ind_subgrupo'].'-'.$f_5['ind_rubro'].'-'.$f_5['Cod_Cuenta'];
                     $Descripcion= $f_5['ind_descripcion'];
                     $Descripcion2= $f_5['ind_descripcion'];
                     $pase=1;
               }
               /// ----------------------------

               $saldo= $this->EstadoResultado->metObtenerSaldo2($f_5['ind_rubro'], $_POST['fk_cbb005_num_contabilidades'], $anio, $mes, $filtro_cuenta, $f_5['Cod_Cuenta']);

               $totalIngresos3+= $saldo;
               $totalXcuenta+= $saldo;
               //
               if ($saldo>'0') {
                  $montosip=1;
                  if($Descripcion!=""){

                     $pdf->Ln(2);
                     $pdf->SetFont('Arial', 'B', 8);
                     $pdf->Cell(30,2, $cuentaMostrar, 0, 0, 'L');
                     $pdf->Cell(100,2,  utf8_decode(strtr(strtoupper($Descripcion), "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÜÚ", "àáâãäåæçèéêëìíîïðñòóôõöøùüú")), 0, 1, 'L');
                    $pdf->Ln(2);

                    $cuentaMostrar= "";
                    $Descripcion= "";

                  }

                  $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
                  $pdf->SetFont('Arial', '', 8);
                  $pdf->SetWidths(array(30, 98, 40));
                  $pdf->SetAligns(array('L', 'L', 'R'));
                  $pdf->Row(array($f_5['ind_grupo'].'-'.$f_5['ind_subgrupo'].'-'.$f_5['ind_grupo'].'-'.$f_5['Cod_Cuenta'].'-'.$f_5['ind_sub_cuenta1'].$f_5['ind_sub_cuenta2'],
                         utf8_decode($f_5['ind_descripcion']), number_format($saldo,2,',','.')));
               }

            }

            $pdf->ln(2);
            $pdf->SetDrawColor(255, 255, 255); $pdf->SetFillColor(255, 255, 255); $pdf->SetTextColor(0, 0, 0);
            $pdf->SetFont('Arial', 'B', 8);
            $pdf->SetWidths(array(30, 118, 40));
            $pdf->SetAligns(array('L', 'L', 'R'));
            $pdf->Row(array('', utf8_decode('TOTAL GASTOS DE FUNCIONAMIENTO'), number_format($totalIngresos3,2,',','.')));

            $pdf->Ln(2);

            /// -------------------------------------------------- //
            $TotalGastosPresup= $totalIngresos3 + $totalIngresos2;

            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(30,2,'', 0, 0, 'L');
            $pdf->Cell(118,2, utf8_decode('TOTAL GASTOS PRESUPUESTARIOS'), 0, 0, 'L');
            $pdf->Cell(40,2, number_format($TotalGastosPresup,2,',','.'), 0, 1, 'R');
            $pdf->Cell(188,1,'--------------------', 0, 1, 'R');
            $pdf->Ln(3);
            /// -------------------------------------------------- //


           /// EJECUCION DEL PRESUPUESTO
           $ejecucionPresup= (-1*$totalIngresos) - $TotalGastosPresup;

            $pdf->SetFont('Arial', 'B', 8);
            $pdf->Cell(30,2,'2-3-309', 0, 0, 'L');
            $pdf->Cell(118,2, utf8_decode('EJECUCION DEL PRESUPUESTO'), 0, 0, 'L');
            $pdf->Cell(40,2, number_format($ejecucionPresup,2,',','.'), 0, 1, 'R');
            $pdf->Cell(188,1,'--------------------', 0, 1, 'R');
            $pdf->Cell(188,1,'--------------------', 0, 1, 'R');
            $pdf->Ln(2);

        }


        $pdf->Ln(5);
        $pdf->SetDrawColor(0, 0, 0); $pdf->SetFillColor(202, 202, 202); $pdf->SetTextColor(0, 0, 0);
        $pdf->SetFont('Arial', 'B', 7);
        $pdf->Cell(100,3,'',0,1,'L');
        $pdf->Cell(75,3,'PREPARADO POR:',0,0,'L');
        $pdf->Cell(90,3,'REVISADO POR:',0,0,'L');
        $pdf->Cell(100,3,'APROBADO POR:',0,1,'L');

        $pdf->Cell(100,5,'',0,0,'L');
        $pdf->Cell(120,5,'',0,0,'L');
        $pdf->Cell(100,5,'',0,1,'L');

        $pdf->Cell(75,5,'LCDA. AAAAAA AAAAAA',0,0,'L');
        $pdf->Cell(90,5,'LCDA. AAAAAA AAAAAA',0,0,'L');
        $pdf->Cell(100,5,'ING. BBBBBB BBBBBB',0,1,'L');

        $pdf->Cell(75,2,'ANALISTA CONTABLE I',0,0,'L');
        $pdf->Cell(90,2,'DIRECTORA DE AMINISTRACION',0,0,'L');
        $pdf->Cell(100,2,'DIRECTOR GENERAL',0,1,'L');

      //salida
      $pdf->Output();
    }

}
