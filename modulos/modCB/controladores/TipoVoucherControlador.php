<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once ROOT.'librerias'.DS.'gump.class.php';

class TipoVoucherControlador extends Controlador
{

    public function __construct()
    {
        parent::__construct();
        Session::metAcceso();
        $this->TipoVoucher = $this->metCargarModelo('TipoVoucher');
    }

    public function metIndex()
    {
        $complementosCss = array(
            'DataTables/jquery.dataTables',
            'DataTables/extensions/dataTables.colVis941e',
            'DataTables/extensions/dataTables.tableTools4029',
        );
        $js[] = 'materialSiace/core/demo/DemoTableDynamic';
        $this->atVista->metCargarCssComplemento($complementosCss);
        $this->atVista->metCargarJs($js);
        $this->atVista->assign('listado',$this->TipoVoucher->metListar());
        $this->atVista->metRenderizar('TipoVoucher');
    }

    /**
    * Función que permite cargar formulario para el ingreso del nuevo registro
    */
    public function metNuevo()
    {

      ##  valores del formulario
        $form = [
            'metodo' => 'crear',
            'cod_voucher' => null,
            'ind_descripcion' => null,
            'num_flag_manual' => null,
            'num_estatus' => null,
            'pk_num_voucher_mast' => null,
            'estado' => 'nuevo',

        ];

        $this->atVista->assign('form',$form);
        $this->atVista->metRenderizar('TipoVoucherCrear','modales');
    }

    /**
    * Función que permite cargar el proceso para el ingreso del nuevo registro
    */
    public function metCrear()
    {
        ##  valido formulario
        $gump = new GUMP();
        $validate = $gump->validate($_POST, [
            'cod_voucher' => 'required|alpha_numeric|max_len,2',
            'ind_descripcion' => 'required|alpha_space_full|max_len,50',
        ]);


        if (!isset($_POST['num_estatus'])) $_POST['num_estatus'] = "0";

        if (!isset($_POST['num_flag_manual'])) $_POST['num_flag_manual']= "0";

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $id = $this->TipoVoucher->metNuevo($_POST);
            ##  devolver registro creado
            $jsondata = [
                'status' => 'crear',
                'mensaje' => 'Registro creado exitosamente',
                'id' => $id,
                'tr' =>'<tr id="id'.$id.'">
                          <td><label>'.$id.'</label></td>
                          <td><label>'.$_POST['cod_voucher'].'</label></td>
                          <td><label>'.$_POST['ind_descripcion'].'</label></td>
                          <td><i class="'.($_POST['num_flag_manual']==1?"md md-check":"md md-not-interested").'"></i></td>
                          <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Tipo Voucher" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                            <button idRegistro="'.$id.'" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static"  title="Ver"
                                                descipcion="El Usuario esta viendo Tipo Voucher" titulo="Ver Tipo Voucher">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                                        </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                          </td>
                       </tr>
                   ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el formulario para la actualización de un registro
    */
    public function metEditar()
    {
         $db= $this->TipoVoucher->metMostrar($_POST['id']);
         $db['metodo'] = 'modificar';
         $db['estado'] = 'editar';

         $this->atVista->assign('form', $db);
         $this->atVista->metRenderizar('TipoVoucherCrear', 'modales');
    }

    /**
    * Función que permite cargar el formulario mostrando datos del registro
    */
    public function metVer()
    {
         ## Datos formulario
         $db= $this->TipoVoucher->metMostrar($_POST['id']);
         $db['metodo'] = 'ver';
         $db['estado'] = $_POST['estado'];
         ## Vista
         $this->atVista->assign('form', $db);
         $this->atVista->metRenderizar('TipoVoucherCrear', 'modales');
    }

    /**
    * Función que permite cargar el proceso para el editado de un registro
    */
    public function metModificar()
    {
        $id = $_POST['pk_num_voucher'];

        ##  valido formulario
        $gump = new GUMP();

        $validate = $gump->validate($_POST, [
            'cod_voucher' => 'required|alpha_numeric|max_len,2',
            'ind_descripcion' => 'required|alpha_space_full|max_len,50',
        ]);

        if (!isset($_POST['num_estatus'])) $_POST['num_estatus']= "0";

        if (!isset($_POST['num_flag_manual'])) $_POST['num_flag_manual']= "0";

        ##  validación exitosa
        if($validate === TRUE) {
            ##  crear registro
            $this->TipoVoucher->metEditar($_POST);

            ##  devolver registro creado
            $jsondata = [
                'status' => 'modificar',
                'mensaje' => 'Registro actualizado exitosamente',
                'id' => $id,
                'tr' => '
                          <td><label>'.$id.'</label></td>
                          <td><label>'.$_POST['cod_voucher'].'</label></td>
                          <td><label>'.$_POST['ind_descripcion'].'</label></td>
                          <td><i class="'.($_POST['num_flag_manual']==1?"md md-check":"md md-not-interested").'"></i></td>
                          <td><i class="'.($_POST['num_estatus']=="1"?"md md-check":"md md-not-interested").'"></i></td>
                          <td align="center">
                            '.(($_POST['perfilM'])?'<button idRegistro="'.$id.'" class="modificar logsUsuario btn ink-reaction btn-raised btn-xs btn-primary"
                                                            data-toggle="modal" data-target="#formModal" data-keyboard="false" data-backdrop="static" titulo="Editar Tipo Voucher" title="Editar">
                                                        <i class="fa fa-edit" style="color: #ffffff;"></i>
                                                    </button>':'').'

                            <button idRegistro="'.$id.'" class="ver logsUsuario btn ink-reaction btn-raised btn-xs btn-warning" data-toggle="modal" data-target="#formModal"
                                                data-keyboard="false" data-backdrop="static"  title="Ver" descipcion="El Usuario esta viendo Tipo Voucher" titulo="Ver Tipo Voucher">
                                            <i class="md md-remove-red-eye" style="color: #ffffff;"></i>
                            </button>

                            '.(($_POST['perfilE'])?'<button idRegistro="'.$id.'" class="eliminar logsUsuario btn ink-reaction btn-raised btn-xs btn-danger"
                                                            boton="si, Eliminar" titulo="Estas Seguro?" mensaje="Esta seguro que desea eliminar el Registro!!" title="Eliminar">
                                                        <i class="md md-delete" style="color: #ffffff;"></i>
                                                    </button>':'').'
                          </td>
                ',
            ];
        }
        else {
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'input' => $validate
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

    /**
    * Función que permite cargar el proceso de eliminado de un registro
    */
    public function metEliminar()
    {

        $id= $this->TipoVoucher->metEliminar(['pk_num_voucher' => $_POST['id']]);

        ##  devolver registro creado
        if($id=="1"){
            ##  devolver errores
            $jsondata = [
                'status' => 'error',
                'mensaje' => '',
                'flag_mensaje' => '1'
            ];
        }else{
            $jsondata = [
                'status' => 'OK',
                'mensaje' => 'Registro eliminado exitosamente',
                'id' => $_POST['id'],
            ];
        }

        echo json_encode($jsondata);
        exit();
    }

}
