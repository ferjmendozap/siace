<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ContabilidadModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /*
    * Obtengo datos para mostrar en el listado
    */
    public function metListar()
    {
        $db = $this->_db->query("SELECT * FROM cb_b005_contabilidades");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Ejecuta la acción de nuevo registro
    */
    public function metNuevo($data)
    {
        $this->_db->beginTransaction();

            $sql ="INSERT INTO cb_b005_contabilidades
                             SET
                                ind_tipo_contabilidad='$data[ind_tipo_contabilidad]',
                                ind_descripcion='$data[ind_descripcion]',
                                ind_contabilidad_acronimo= '$data[ind_contabilidad_acronimo]',
                                num_estatus='$data[num_estatus]',
                                fk_a018_num_seguridad_usuario = '$this->idUsuario',
                                fec_ultima_modificacion = NOW() ";
            $db = $this->_db->prepare($sql);
            $db->execute();

            $id = $this->_db->lastInsertId();


            if (isset($data['fk_cbb002_num_libro_contable'])) {

                    for($i=0; $i<count($data['fk_cbb002_num_libro_contable']); $i++){

                        $sql= "INSERT INTO cb_b003_libro_contabilidad
                                       SET ind_tipo_contabilidad= '$data[ind_tipo_contabilidad]',
                                           fk_cbb002_num_libro_contable= '".$data['fk_cbb002_num_libro_contable'][$i]."',
                                           fk_cbb005_num_contabilidades= '$id',
                                           fk_a018_num_seguridad_usuario= '$this->idUsuario',
                                           fec_ultima_modificacion= NOW() ";
                        $db= $this->_db->prepare($sql);
                        $db->execute();
                        //$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
                    }
            }

            ##  Consigna una transacción
            $this->_db->commit();

        return $id;
    }

    /*
    * Obtengo datos de registro específico
    */
    public function metMostrar($id)
    {
        $db = $this->_db->query("SELECT * FROM cb_b005_contabilidades WHERE pk_num_contabilidades= '$id' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetch();
    }

    /*
    * Ejecuta la acción editar registro
    */
    public function metEditar($data)
    {
        $this->_db->beginTransaction();

            $modificar=""; $mensaje="";

            $sql= $this->_db->query("SELECT *
                                       FROM cb_b001_voucher
                                      WHERE fk_cbb005_num_contabilidades= '$data[pk_num_contabilidades]' ");
            $sql->setFetchMode(PDO::FETCH_ASSOC);
            $field= $sql->fetchAll();

            foreach ($field as $f) {
                if (isset($f['pk_num_voucher_mast'])) {
                    $mensaje= "No puede ser modificado";
                    $swal= "Registro no puede ser Modificado!";
                }
            }

            if ($mensaje==""){
                $mensaje= "Registro actualizado exitosamente";
                $modificar= "modificar";
                $swal= "Registro Guardado!";
            }

            if ($modificar=="modificar") {

                //actualizo datos en contabilidades
                $sql=  "UPDATE cb_b005_contabilidades
                           SET
                               ind_tipo_contabilidad= '$data[ind_tipo_contabilidad]',
                               ind_descripcion= '$data[ind_descripcion]',
                               ind_contabilidad_acronimo= '$data[ind_contabilidad_acronimo]',
                               num_estatus= '$data[num_estatus]',
                               fk_a018_num_seguridad_usuario = '$this->idUsuario',
                               fec_ultima_modificacion = NOW()
                         WHERE
                               pk_num_contabilidades= '$data[pk_num_contabilidades]' ";
                $db= $this->_db->prepare($sql);
                $db->execute();

                //elimino datos realacionados a la contabilidad
                $sql = "DELETE FROM cb_b003_libro_contabilidad WHERE fk_cbb005_num_contabilidades= '$data[pk_num_contabilidades]'";
                $db = $this->_db->prepare($sql);
                $db->execute();


                if (isset($data['fk_cbb002_num_libro_contable'])) {

                    for($i=0; $i<count($data['fk_cbb002_num_libro_contable']); $i++){

                        $sql= "INSERT INTO cb_b003_libro_contabilidad
                                       SET ind_tipo_contabilidad= '$data[ind_tipo_contabilidad]',
                                           fk_cbb002_num_libro_contable= '".$data['fk_cbb002_num_libro_contable'][$i]."',
                                           fk_cbb005_num_contabilidades= '$data[pk_num_contabilidades]',
                                           fk_a018_num_seguridad_usuario= '$this->idUsuario',
                                           fec_ultima_modificacion= NOW() ";
                        $db= $this->_db->prepare($sql);
                        $db->execute();
                        //$db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
                    }
                }
            }



         ##  Consigna una transacción
         $this->_db->commit();

         return $mensaje.'|'.$swal;
    }

    /*
    * Ejecuta acción de eliminar el registro
    */
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql= $this->_db->query("SELECT *
                                   FROM cb_b001_voucher
                                  WHERE fk_cbb005_num_contabilidades= '$data[pk_num_contabilidades]' ");
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        $field= $sql->fetch();

        if (isset($field['pk_num_voucher_mast'])){
            $mensaje= "No puede ser Eliminado";
            $valor= "2";
        }else{

            $sql = "DELETE FROM cb_b003_libro_contabilidad WHERE fk_cbb005_num_contabilidades= '$data[pk_num_contabilidades]'";
            $db = $this->_db->prepare($sql);
            $db->execute();

            $sql = "DELETE FROM cb_b005_contabilidades WHERE pk_num_contabilidades= '$data[pk_num_contabilidades]'";
            $db = $this->_db->prepare($sql);
            $db->execute();

            $mensaje= "Registro eliminado exitosamente";
            $valor= "1";

        }

        ##  Consigna una transacción
        $this->_db->commit();

        return $mensaje.'|'.$valor;
    }

    /*
    * Obtengo datos de registro en particular para cargar formulario
    */
    public function metLibroContabilidad($id)
    {
      $db= $this->_db->query("SELECT a.*,
                                     b.cod_libro,
                                     b.ind_descripcion,
                                     b.pk_num_libro_contable
                                FROM cb_b003_libro_contabilidad a
                                     INNER JOIN cb_b002_libro_contable b on (b.pk_num_libro_contable=a.fk_cbb002_num_libro_contable)
                               WHERE a.fk_cbb005_num_contabilidades= '$id' ");
      $db->setFetchMode(PDO::FETCH_ASSOC);
      return $db->fetchAll();
    }

}
