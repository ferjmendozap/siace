<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class PlanCuentaModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /*
    * Obtengo datos de tabla para cargar listado de registros
    */
    public function metListar($campo)
    {
        $where="";
        if(!empty($campo['Contabilidad'])) $where= " WHERE fk_cbb005_num_contabilidades= '$campo[Contabilidad]' ";
        $db = $this->_db->query("SELECT * FROM cb_b004_plan_cuenta $where ORDER BY num_flag_tipo_cuenta, pk_num_cuenta ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Ejecuta el proceso de nuevo registro de una cuenta
    */
    public function metNuevo($data)
    {
        $this->_db->beginTransaction();

            $sql ="INSERT INTO cb_b004_plan_cuenta
                             SET
                                cod_cuenta = '$data[cod_cuenta]',
                                ind_grupo = '$data[ind_grupo]',
                                ind_subgrupo = '$data[ind_subgrupo]',
                                ind_rubro = '$data[ind_rubro]',
                                ind_cuenta = '$data[ind_cuenta]',
                                ind_sub_cuenta1 = '$data[ind_sub_cuenta1]',
                                ind_sub_cuenta2 = '$data[ind_sub_cuenta2]',
                                ind_sub_cuenta3 = '$data[ind_sub_cuenta3]',
                                ind_descripcion = '$data[ind_descripcion]',
                                num_nivel = '$data[num_nivel]',
                                num_flag_tipo = '$data[num_flag_tipo]',
                                fk_a006_num_miscelaneo_detalle = '$data[fk_a006_num_miscelaneo_detalle]',
                                ind_tipo_saldo = '$data[ind_tipo_saldo]',
                                num_estatus = '$data[num_estatus]',
                                fec_ultima_modificacion = NOW(),
                                fk_a018_num_seguridad_usuario = '$this->idUsuario',
                                num_flag_tipo_cuenta = '$data[num_flag_tipo_cuenta]',
                                num_flag_req_ccosto = '$data[num_flag_req_ccosto]',
                                num_flag_req_activo = '$data[num_flag_req_activo]',
                                fk_cbb005_num_contabilidades = '$data[fk_cbb005_num_contabilidades]' ";
             $db = $this->_db->prepare($sql);
             $db->execute();

             $id = $this->_db->lastInsertId();

             ##  Consigna una transacción
             $this->_db->commit();

             return $id;
    }

    /*
    * Obtengo datos de tabla con búsqueda específica del plan cuenta
    */
    public function metMostrar($id)
    {
        $db = $this->_db->query("SELECT * FROM cb_b004_plan_cuenta WHERE pk_num_cuenta='$id' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetch();
    }

    /*
    * Ejecuta el proceso de editado de un registro del plan cuenta
    */
    public function metEditar($data)
    {
        $this->_db->beginTransaction();

         $sql ="UPDATE cb_b004_plan_cuenta
                   SET
                        cod_cuenta = '$data[cod_cuenta]',
                        ind_grupo = '$data[ind_grupo]',
                        ind_subgrupo = '$data[ind_subgrupo]',
                        ind_rubro = '$data[ind_rubro]',
                        ind_cuenta = '$data[ind_cuenta]',
                        ind_sub_cuenta1 = '$data[ind_sub_cuenta1]',
                        ind_sub_cuenta2 = '$data[ind_sub_cuenta2]',
                        ind_sub_cuenta3 = '$data[ind_sub_cuenta3]',
                        ind_descripcion = '$data[ind_descripcion]',
                        num_nivel = '$data[num_nivel]',
                        num_flag_tipo = '$data[num_flag_tipo]',
                        fk_a006_num_miscelaneo_detalle = '$data[fk_a006_num_miscelaneo_detalle]',
                        ind_tipo_saldo = '$data[ind_tipo_saldo]',
                        num_estatus = '$data[num_estatus]',
                        fec_ultima_modificacion = NOW(),
                        fk_a018_num_seguridad_usuario = '$this->idUsuario',
                        num_flag_tipo_cuenta = '$data[num_flag_tipo_cuenta]',
                        num_flag_req_ccosto = '$data[num_flag_req_ccosto]',
                        num_flag_req_activo = '$data[num_flag_req_activo]',
                        fk_cbb005_num_contabilidades = '$data[fk_cbb005_num_contabilidades]'
                WHERE
                      pk_num_cuenta= '$data[pk_num_cuenta]' "; //echo $sql;

         $db = $this->_db->prepare($sql);
         $db->execute();

         ##  Consigna una transacción
         $this->_db->commit();
    }

    /*
    * Ejecuta el proceso de anular un registro del plan cuenta
    */
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "DELETE FROM cb_b004_plan_cuenta WHERE pk_num_cuenta = '$data[pk_num_cuenta]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacción
        $this->_db->commit();
    }

}
