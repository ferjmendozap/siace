<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        08-03-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class CierreAnualModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
    * Realiza las operaiones de consulta e insertado en tablas del cierre anual
    */
    public function metEjecutar($fanio, $fCodContabilidad, $param_dependencia, $param_ccosto)
    {
      $filtro=" AND vd.fk_cbb005_num_contabilidades= '$fCodContabilidad'  AND  vd.fec_anio= '$fanio' "; echo $filtro." / ";
      $CodLibroCont = '1';

      $this->_db->beginTransaction();

      $param_dependencia= $this->metValor("SELECT pk_num_dependencia FROM a004_dependencia WHERE ind_codinterno='$param_dependencia' AND num_estatus='1' ", "A");
      $param_ccosto= $this->metValor("SELECT pk_num_centro_costo FROM a023_centro_costo WHERE cod_centro_costo='$param_ccosto' AND num_estatus='1' ", "C"); echo "/".$param_ccosto."/";
      $Descripcion23309= $this->metValor("SELECT ind_descripcion FROM cb_b004_plan_cuenta WHERE cod_cuenta = '23309' AND fk_cbb005_num_contabilidades= '$fCodContabilidad' ", "B");
      $Descripcion232990101= $this->metValor("SELECT ind_descripcion FROM cb_b004_plan_cuenta WHERE cod_cuenta = '232990101' AND fk_cbb005_num_contabilidades= '$fCodContabilidad' ", "B");

      //## Validación de existencia previa de cierre anual
      $db= $this->_db->query("SELECT *
                                FROM cb_c008_cierre_anual
                               WHERE fec_anio= '$fanio' AND
                                     fk_cbb005_num_contabilidades= '$fCodContabilidad' ");
      $db->setFetchMode(PDO::FETCH_ASSOC);
      $field= $db->fetch();

      //if(count($field)) die('El Periodo seleccionado ya se encuentra cerrado.');

      //## ASIENTO 1
      if ($fCodContabilidad == '1') {
          $sql= $this->_db->query("SELECT pc.pk_num_cuenta,
                                          pc.ind_descripcion,
                                          SUM(vd.num_debe) AS Monto_debe,
                                          SUM(vd.num_haber) AS Monto_haber
                                     FROM
                                          cb_c001_voucher_det vd
                                          INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = vd.fk_cbb004_num_cuenta)
                                    WHERE vd.ind_estatus = 'MA' AND vd.fk_cbb004_num_cuenta = '510' $filtro
                                    ORDER BY vd.fk_cbb004_num_cuenta");
      }
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      $field= $sql->fetchAll();
      $Monto51301 = 0;
      foreach($field as $f) {
            $TituloVoucher = "Para cancelar el saldo de la cuenta Ingresos, por transferencia a la cuenta No. 309-Ejecución del Presupuesto";

            $Monto= ABS($f['Monto_debe'] - $f['Monto_haber']);

            // Insertamos datos a la tabla
            $sql ="INSERT INTO cb_b001_voucher
                           SET
                                ind_mes= '12',
                                ind_anio= '$fanio',
                                ind_voucher='33-0001',
                                ind_nro_voucher='0001',
                                num_creditos= '".floatval($Monto)."',
                                num_debitos= '".floatval($Monto)."',
                                ind_estatus= 'MA',
                                fk_a003_num_preparado_por= '$this->idUsuario',
                                fec_fecha_preparacion= NOW(),
                                fk_a003_num_aprobado_por= '$this->idUsuario',
                                fec_fecha_aprobacion= NOW(),
                                txt_titulo_voucher= '".utf8_decode($TituloVoucher)."',
                                fec_fecha_voucher= NOW(),
                                fk_cbc003_num_voucher= '32',
                                fk_cbb005_num_contabilidades= '$fCodContabilidad',
                                fk_a001_num_organismo= '1',
                                fk_a004_num_dependencia= '$param_dependencia',
                                fk_cbb003_num_libro_contabilidad= '1',
                                fk_cbc002_num_sistema_fuente='6',
                                fec_ultima_modificacion= NOW(),
                                fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
            $db = $this->_db->prepare($sql);
            $db->execute();
            $id = $this->_db->lastInsertId();

            ##	detalles
            $sql = "INSERT INTO cb_c001_voucher_det
                            SET
                                ind_mes = '12',
                                fec_anio = '$fanio',
                                num_linea = '1',
                                num_debe = '".floatval($Monto)."',
                                ind_descripcion = '".$f['ind_descripcion']."',
                                ind_estatus = 'MA',
                                fk_a023_num_centro_costo = '$param_ccosto',
                                fk_cbb001_num_voucher_mast = '$id',
                                fk_cbb004_num_cuenta = '".$f['pk_num_cuenta']."',
                                fk_cbb003_num_libro_contabilidad = '1',
                                fk_cbb005_num_contabilidades= '$fCodContabilidad',
                                fec_ultima_modificacion = NOW(),
                                fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
            $db = $this->_db->prepare($sql);
            $db->execute();

			      $sql = "INSERT INTO cb_c001_voucher_det
                  					SET
                                ind_mes = '12',
                                fec_anio = '$fanio',
                                num_linea = '2',
                                num_haber = '".floatval($Monto)."',
                                ind_descripcion = '".$f['ind_descripcion']."',
                                ind_estatus = 'MA',
                                fk_a023_num_centro_costo = '$param_ccosto',
                                fk_cbb001_num_voucher_mast = '$id',
                                fk_cbb004_num_cuenta = '507',
                                fk_cbb003_num_libro_contabilidad = '1',
                                fk_cbb005_num_contabilidades= '$fCodContabilidad',
                                fec_ultima_modificacion = NOW(),
                                fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
            $db = $this->_db->prepare($sql);
            $db->execute();

            ##
            $Monto51301 += $Monto;
            $_CIERRE[] = array(
                'fec_anio' => $fanio,
                'fk_cbb005_num_contabilidades' => $fCodContabilidad,
                'num_asiento' => 1,
                'fk_cbb004_num_cuenta' => $f['pk_num_cuenta'],
                'num_monto' => floatval($Monto),
                'fec_ultima_modificacion'=> date("Y-m-d H:i:s"),
                'fk_a018_num_seguridad_usuario' => $this->idUsuario
            );
            $_CIERRE[] = array(
                'fec_anio' => $fanio,
                'fk_cbb005_num_contabilidades' => $fCodContabilidad,
                'num_asiento' => 1,
                'fk_cbb004_num_cuenta' => 507,
                'num_monto' => floatval(-$Monto),
                'fec_ultima_modificacion' => date("Y-m-d H:i:s"),
                'fk_a018_num_seguridad_usuario' => $this->idUsuario
            );
        }

      //## ASIENTO 2
      if ($fCodContabilidad == '1') {
          $sql= $this->_db->query("SELECT pc.pk_num_cuenta,
                                          pc.ind_descripcion,
                                          ABS(SUM(vd.num_debe)) AS Monto_debe,
                                          ABS(SUM(vd.num_haber)) AS Monto_haber
                                     FROM
                                          cb_c001_voucher_det vd
                                          INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = vd.fk_cbb004_num_cuenta)
                                    WHERE vd.ind_estatus = 'MA' AND vd.fk_cbb004_num_cuenta = '511' $filtro
                                    ORDER BY vd.fk_cbb004_num_cuenta");
      }
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      $field= $sql->fetchAll();
      $Monto51303= 0;
      foreach($field as $f){
          $TituloVoucher= "Para cancelar el saldo de la cuenta Gastos Presupuestarios, por transferencia a la cuenta No. 309-Ejecución del Presupuesto";
          //$NroInterno = $this->codigo('cb_b001_voucher', 'NroInterno', 10);

          $Monto= ABS($f['Monto_debe'] - $f['Monto_haber']);

          // Insertamos datos a la tabla
          $sql ="INSERT INTO cb_b001_voucher
                         SET
                              ind_mes= '12',
                              ind_anio= '$fanio',
                              ind_voucher='33-0002',
                              ind_nro_voucher='0002',
                              num_creditos= '".floatval($Monto)."',
                              num_debitos= '".floatval($Monto)."',
                              ind_estatus= 'MA',
                              fk_a003_num_preparado_por= '$this->idUsuario',
                              fec_fecha_preparacion= NOW(),
                              fk_a003_num_aprobado_por= '$this->idUsuario',
                              fec_fecha_aprobacion= NOW(),
                              txt_titulo_voucher= '".utf8_decode($TituloVoucher)."',
                              fec_fecha_voucher= NOW(),
                              fk_cbc003_num_voucher= '32',
                              fk_cbb005_num_contabilidades= '$fCodContabilidad',
                              fk_a001_num_organismo= '1',
                              fk_a004_num_dependencia= '$param_dependencia',
                              fk_cbb003_num_libro_contabilidad= '1',
                              fk_cbc002_num_sistema_fuente='6',
                              fec_ultima_modificacion= NOW(),
                              fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
          $db = $this->_db->prepare($sql);
          $db->execute();
          $id = $this->_db->lastInsertId();

          ##	detalles
          $sql = "INSERT INTO cb_c001_voucher_det
                          SET
                              ind_mes = '12',
                              fec_anio = '$fanio',
                              num_linea = '1',
                              num_debe = '".floatval($Monto)."',
                              ind_descripcion = '".$f['ind_descripcion']."',
                              ind_estatus = 'MA',
                              fk_a023_num_centro_costo = '$param_ccosto',
                              fk_cbb001_num_voucher_mast = '$id',
                              fk_cbb004_num_cuenta= '".$f['pk_num_cuenta']."',
                              fk_cbb003_num_libro_contabilidad = '1',
                              fk_cbb005_num_contabilidades= '$fCodContabilidad',
                              fec_ultima_modificacion = NOW(),
                              fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
          $db = $this->_db->prepare($sql);
          $db->execute();

          $sql = "INSERT INTO cb_c001_voucher_det
                          SET
                              ind_mes = '12',
                              fec_anio = '$fanio',
                              num_linea = '2',
                              num_haber = '".floatval($Monto)."',
                              ind_descripcion = '".utf8_decode($Descripcion23309)."',
                              ind_estatus = 'MA',
                              fk_a023_num_centro_costo = '$param_ccosto',
                              fk_cbb001_num_voucher_mast = '$id',
                              fk_cbb004_num_cuenta = '507',
                              fk_cbb003_num_libro_contabilidad = '1',
                              fk_cbb005_num_contabilidades= '$fCodContabilidad',
                              fec_ultima_modificacion = NOW(),
                              fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
          $db = $this->_db->prepare($sql);
          $db->execute();

          ##
          $Monto51303 += $Monto;
          $_CIERRE[] = array(
              'fec_anio' => $fanio,
              'fk_cbb005_num_contabilidades' => $fCodContabilidad,
              'num_asiento' => 2,
              'fk_cbb004_num_cuenta' => $f['pk_num_cuenta'],
              'num_monto' => floatval($Monto),
              'fec_ultima_modificacion' => date("Y-m-d H:i:s"),
              'fk_a018_num_seguridad_usuario' => $this->idUsuario
          );
          $_CIERRE[] = array(
              'fec_anio' => $fanio,
              'fk_cbb005_num_contabilidades' => $fCodContabilidad,
              'num_asiento' => 2,
              'fk_cbb004_num_cuenta' => 507,
              'num_monto' => floatval(-$Monto),
              'fec_ultima_modificacion' => date("Y-m-d H:i:s"),
              'fk_a018_num_seguridad_usuario' => $this->idUsuario
          );
      }

      //## ASIENTO 3
      $Monto_asiento3= 0;
      if ($fCodContabilidad == '1') {
          $sql= $this->_db->query("SELECT
                                           ABS(SUM(vd.num_debe)) AS Monto_debe,
                                           ABS(SUM(vd.num_haber)) AS Monto_haber
                                      FROM
                                           cb_c001_voucher_det vd
                                           INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = vd.fk_cbb004_num_cuenta AND pc.cod_cuenta LIKE '61300%')
                                     WHERE vd.ind_estatus='MA' $filtro
                                  ORDER BY vd.fk_cbb004_num_cuenta");
      }
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      $field= $sql->fetchAll();

      //foreach($field as $f) {
        $Monto_asiento3= ABS($f['Monto_debe'] - $f['Monto_haber']);
        //$Monto_debe+= $Monto_asiento3;
      //}

      $TituloVoucher = "Para cancelar el saldo de la cuenta Ingresos Extraordinarios, por transferencia a la cuenta No. 309-Ejecución del Presupuesto";
      //$Monto= $f['Monto_debe'] + $f['Monto_haber'];

      // Insertamos datos a la tabla
      $sql ="INSERT INTO cb_b001_voucher
                     SET
                          ind_mes= '12',
                          ind_anio= '$fanio',
                          ind_voucher='33-0003',
                          ind_nro_voucher='0003',
                          num_creditos= '".floatval($Monto_asiento3)."',
                          num_debitos= '".floatval($Monto_asiento3)."',
                          ind_estatus= 'MA',
                          fk_a003_num_preparado_por= '$this->idUsuario',
                          fec_fecha_preparacion= NOW(),
                          fk_a003_num_aprobado_por= '$this->idUsuario',
                          fec_fecha_aprobacion= NOW(),
                          txt_titulo_voucher= '".utf8_decode($TituloVoucher)."',
                          fec_fecha_voucher= NOW(),
                          fk_cbc003_num_voucher= '32',
                          fk_cbb005_num_contabilidades= '$fCodContabilidad',
                          fk_a001_num_organismo= '1',
                          fk_a004_num_dependencia= '$param_dependencia',
                          fk_cbb003_num_libro_contabilidad= '1',
                          fk_cbc002_num_sistema_fuente='6',
                          fec_ultima_modificacion= NOW(),
                          fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
      $db = $this->_db->prepare($sql);
      $db->execute();
      $id = $this->_db->lastInsertId();

      ##	detalles
      $sql = "INSERT INTO cb_c001_voucher_det
                      SET
                          ind_mes = '12',
                          fec_anio = '$fanio',
                          num_linea = '1',
                          num_debe = '".floatval($Monto_asiento3)."',
                          ind_descripcion = '".utf8_decode($Descripcion23309)."',
                          ind_estatus = 'MA',
                          fk_a023_num_centro_costo = '$param_ccosto',
                          fk_cbb001_num_voucher_mast = '$id',
                          fk_cbb004_num_cuenta = '507',
                          fk_a003_num_persona = '$this->idUsuario',
                          fk_cbb003_num_libro_contabilidad = '1',
                          fk_cbb005_num_contabilidades= '$fCodContabilidad',
                          fec_ultima_modificacion = NOW(),
                          fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
      $db = $this->_db->prepare($sql);
      $db->execute();

      if ($fCodContabilidad == '1') {
          $sql= $this->_db->query("SELECT  pc.pk_num_cuenta,
                                           pc.ind_descripcion,
                                           SUM(vd.num_debe) AS Monto_debe,
                                           SUM(vd.num_haber) AS Monto_haber
                                      FROM
                                           cb_c001_voucher_det vd
                                           INNER JOIN cb_b004_plan_cuenta pc ON (pc.pk_num_cuenta = vd.fk_cbb004_num_cuenta AND pc.cod_cuenta LIKE '61300%')
                                     WHERE vd.ind_estatus='MA'  $filtro
                                  GROUP BY vd.fk_cbb004_num_cuenta
                                  ORDER BY vd.fk_cbb004_num_cuenta");
      }
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      $field= $sql->fetchAll();

      ##
      $_CIERRE[] = array(
          'fec_anio' => $fanio,
          'fk_cbb005_num_contabilidades' => $fCodContabilidad,
          'num_asiento' => 3,
          'fk_cbb004_num_cuenta' => 507,
          'num_monto' => floatval($Monto_asiento3),
          'fec_ultima_modificacion' => date("Y-m-d H:i:s"),
          'fk_a018_num_seguridad_usuario' => $this->idUsuario
      );
      /**
      *Creo lineas que afectan el monto_haber de la contabilidad
      */
      $Monto61300 = 0;
		  $Linea = 1;
      foreach($field as $f) {
          $Monto= ABS($f['Monto_debe'] - $f['Monto_haber']);
          ##	detalles
          $sql = "INSERT INTO cb_c001_voucher_det
                          SET
                              ind_mes = '12',
                              fec_anio = '$fanio',
                              num_linea = ".++$Linea.",
                              num_haber = '".floatval($Monto)."',
                              ind_descripcion = '".utf8_decode($f['ind_descripcion'])."',
                              ind_estatus = 'MA',
                              fk_a023_num_centro_costo = '$param_ccosto',
                              fk_cbb001_num_voucher_mast = '$id',
                              fk_cbb004_num_cuenta = '".$f['pk_num_cuenta']."',
                              fk_cbb003_num_libro_contabilidad = '1',
                              fk_cbb005_num_contabilidades= '".$fCodContabilidad."',
                              fec_ultima_modificacion = NOW(),
                              fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
          $db = $this->_db->prepare($sql);
          $db->execute();

          ##
          $Monto61300 += $Monto;

          $_CIERRE[] = array(
              'fec_anio' => $fanio,
              'fk_cbb005_num_contabilidades' => $fCodContabilidad,
              'num_asiento' => 3,
              'fk_cbb004_num_cuenta' => $f['pk_num_cuenta'],
              'num_monto' => floatval(-$Monto),
              'fec_ultima_modificacion' => date("Y-m-d H:i:s"),
              'fk_a018_num_seguridad_usuario' => $this->idUsuario
          );
      }

      $sql= $this->_db->query("UPDATE cb_b001_voucher
                          		  SET Lineas= ".intval($Linea)."
                          		WHERE
                                      Periodo= '".$fanio."-12' AND
                                      CodContabilidad= '".$fCodContabilidad."' AND
                                      Voucher= '33-0003' ");
      $db = $this->_db->prepare($sql);
      $db->execute();

      //##	ASIENTO 4
  		$MontoDebe = $Monto51301 + $Monto51303 - $Monto61300;
  		$MontoHaber = $MontoDebe;

      ##
  		$TituloVoucher = "Para cancelar la cuenta No. 309-Ejecución del Presupuesto";
  		//$NroInterno = codigo('ac_vouchermast', 'NroInterno', 10);

      ##	general
      // Insertamos datos a la tabla
      $sql ="INSERT INTO cb_b001_voucher
                     SET
                          ind_mes= '12',
                          ind_anio= '$fanio',
                          ind_voucher='33-0004',
                          ind_nro_voucher='0004',
                          num_creditos= '".floatval($MontoDebe)."',
                          num_debitos= '".floatval($MontoDebe)."',
                          ind_estatus= 'MA',
                          fk_a003_num_preparado_por= '$this->idUsuario',
                          fec_fecha_preparacion= NOW(),
                          fk_a003_num_aprobado_por= '$this->idUsuario',
                          fec_fecha_aprobacion= NOW(),
                          txt_titulo_voucher= '".utf8_decode($TituloVoucher)."',
                          fec_fecha_voucher= NOW(),
                          fk_cbc003_num_voucher= '33',
                          fk_cbb005_num_contabilidades= '$fCodContabilidad',
                          fk_a001_num_organismo= '1',
                          fk_a004_num_dependencia= '$param_dependencia',
                          fk_cbb003_num_libro_contabilidad= '1',
                          fk_cbc002_num_sistema_fuente='6',
                          fec_ultima_modificacion= NOW(),
                          fk_a018_num_seguridad_usuario= '$this->idUsuario' "; ////echo $sql."\n\n";
      $db = $this->_db->prepare($sql);
      $db->execute();
      $id = $this->_db->lastInsertId();

      ##	detalles 1
      $sql = "INSERT INTO cb_c001_voucher_det
                      SET
                          ind_mes = '12',
                          fec_anio = '$fanio',
                          num_linea = '1',
                          num_debe = '$MontoDebe',
                          ind_descripcion = '".utf8_decode($Descripcion23309)."',
                          ind_estatus = 'MA',
                          fk_a023_num_centro_costo = '$param_ccosto',
                          fk_cbb001_num_voucher_mast = '$id',
                          fk_cbb004_num_cuenta = '507',
                          fk_cbb003_num_libro_contabilidad = '1',
                          fk_cbb005_num_contabilidades= '".$fCodContabilidad."',
                          fec_ultima_modificacion = NOW(),
                          fk_a018_num_seguridad_usuario= '$this->idUsuario' "; ////echo $sql."\n\n";
      $db = $this->_db->prepare($sql);
      $db->execute();

      ## detalles 2
      $sql = "INSERT INTO cb_c001_voucher_det
                      SET
                          ind_mes = '12',
                          fec_anio = '$fanio',
                          num_linea = '2',
                          num_haber = '$MontoHaber',
                          ind_descripcion = '".utf8_decode($Descripcion232990101)."',
                          ind_estatus = 'MA',
                          fk_a023_num_centro_costo = '$param_ccosto',
                          fk_cbb001_num_voucher_mast = '$id',
                          fk_cbb004_num_cuenta = '506',
                          fk_cbb003_num_libro_contabilidad = '1',
                          fk_cbb005_num_contabilidades= '".$fCodContabilidad."',
                          fec_ultima_modificacion = NOW(),
                          fk_a018_num_seguridad_usuario= '$this->idUsuario' "; ////echo $sql."\n\n";
      $db = $this->_db->prepare($sql);
      $db->execute();

      $_CIERRE[] = array(
          'fec_anio' => $fanio,
          'fk_cbb005_num_contabilidades' => $fCodContabilidad,
          'num_asiento' => 4,
          'fk_cbb004_num_cuenta' => 507,
          'num_monto' => $MontoDebe,
          'fec_ultima_modificacion' => date("Y-m-d H:i:s"),
          'fk_a018_num_seguridad_usuario' => $this->idUsuario
      );
      $_CIERRE[] = array(
          'fec_anio' => $fanio,
          'fk_cbb005_num_contabilidades' => $fCodContabilidad,
          'num_asiento' => 4,
          'fk_cbb004_num_cuenta' => 506,
          'num_monto' => $MontoHaber,
          'fec_ultima_modificacion' => date("Y-m-d H:i:s"),
          'fk_a018_num_seguridad_usuario' => $this->idUsuario
      );

      ##	INSERTO EL CIERRE
  		foreach ($_CIERRE as $c) {
  			$sql = "INSERT INTO cb_c008_cierre_anual
              				SET
                                fec_anio= '".$c['fec_anio']."',
                                fk_cbb005_num_contabilidades=  '".$c['fk_cbb005_num_contabilidades']."',
                                num_asiento=  '".$c['num_asiento']."',
                                fk_cbb004_num_cuenta=  '".$c['fk_cbb004_num_cuenta']."',
                                num_monto=  '".$c['num_monto']."',
                                fec_ultima_modificacion=  NOW(),
                                fk_a018_num_seguridad_usuario=  $this->idUsuario ";
        $db = $this->_db->prepare($sql);
        $db->execute();
  		}

      ##	INSERTO SALDOS INICIALES
      $sql= $this->_db->query("SELECT bc.*,
                                      (bc.num_saldo_inicial + bc.num_saldo_balance01 + bc.num_saldo_balance02 + bc.num_saldo_balance03 + bc.num_saldo_balance04 + bc.num_saldo_balance05 + bc.num_saldo_balance06 + bc.num_saldo_balance07 + bc.num_saldo_balance08 + bc.num_saldo_balance09 + bc.num_saldo_balance10 + bc.num_saldo_balance11 + bc.num_saldo_balance12) AS SaldoInicial
                                 FROM cb_c007_balance_cuenta bc
                                WHERE fk_cbb005_num_contabilidades= '$fCodContabilidad' AND
                                      fec_anio= '$fanio' AND
                                      (bc.num_saldo_inicial + bc.num_saldo_balance01 + bc.num_saldo_balance02 + bc.num_saldo_balance03 + bc.num_saldo_balance04 + bc.num_saldo_balance05 + bc.num_saldo_balance06 + bc.num_saldo_balance07 + bc.num_saldo_balance08 + bc.num_saldo_balance09 + bc.num_saldo_balance10 + bc.num_saldo_balance11 + bc.num_saldo_balance12) <> 0
                              ");
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      $field_saldos= $sql->fetchAll();

		  foreach ($field_saldos as $fs) {
    			$sql = "UPDATE cb_c007_balance_cuenta
            			   SET num_saldo_final= '".$fs['SaldoInicial']."',
                               fec_ultima_modificacion=  NOW(),
                               fk_a018_num_seguridad_usuario=  $this->idUsuario
            			 WHERE
                                fec_anio = '".$fanio."' AND
                                fk_cbb005_num_contabilidades = '".$fs['fk_cbb005_num_contabilidades']."' AND
                                fk_cbb004_num_cuenta = '".$fs['fk_cbb004_num_cuenta']."' ";
    			$db = $this->_db->prepare($sql);
                $db->execute();

                ##
                $sql = "INSERT INTO cb_c007_balance_cuenta
                                    SET fec_anio = '".($fanio+1)."',
                                        fk_cbb005_num_contabilidades = '".$fs['fk_cbb005_num_contabilidades']."',
                                        fk_cbb004_num_cuenta = '".$fs['fk_cbb004_num_cuenta']."',
                                        num_saldo_inicial = '".$fs['SaldoInicial']."',
                                        fec_ultima_modificacion=  NOW(),
                                        fk_a018_num_seguridad_usuario=  $this->idUsuario
                        ON DUPLICATE KEY UPDATE
                                        num_saldo_inicial = '".$fs['SaldoInicial']."' ";
                $db = $this->_db->prepare($sql);
                $db->execute();
		  }

      ## Consigna una transacción
      $this->_db->commit();
    }

    /**
    *Permite realizar consulta generalizada
    *@return devuelve un valor de donde fue realizada la llamada
    */
    public function metValor($valor, $opc)
    {
        $db= $this->_db->query("$valor");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        $field= $db->fetch();
        if($opc=="A") return $field['pk_num_dependencia'];
        elseif($opc=="B") return $field['ind_descripcion'];
        elseif($opc=="C") return $field['pk_num_centro_costo'];
    }

    /**
    * Obtengo información de un voucher en específico
    */
    public function metConsultaVoucher($ind_voucher)
    {
        $db= $this->_db->query("SELECT *
                                  FROM cb_b001_voucher
                                 WHERE ind_voucher= '$ind_voucher' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
    * permite consultar el contabilidad del voucher
    * @return pk_num_contabilidades
    */
    public function metContabilidades($data)
    {
       $db= $this->_db->query("SELECT pk_num_contabilidades FROM cb_b005_contabilidades WHERE  ind_contabilidad_acronimo='$data' ");
       $db->setFetchMode(PDO::FETCH_ASSOC);
       $field= $db->fetch();
       return $field['pk_num_contabilidades'];
    }

}
