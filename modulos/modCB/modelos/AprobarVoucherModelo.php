<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class AprobarVoucherModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
    * Se obtiene información para ser mostrada en el listar
    */
    public function metListar()
    {
        $db = $this->_db->query("SELECT
                                        vm.*,
                                        o.pk_num_organismo,
                                        d.pk_num_dependencia,
                                        -- lc.Descripcion AS NomLibroContable,
                                        acv.ind_descripcion
                                  FROM
                                        cb_b001_voucher vm
                                        INNER JOIN a001_organismo o ON (vm.fk_a001_num_organismo = o.pk_num_organismo)
                                        LEFT JOIN a004_dependencia d ON (vm.fk_a004_num_dependencia = d.pk_num_dependencia)
                                        -- LEFT JOIN cb_b002_libro_contable lc ON (vm.CodLibroCont = lc.pk_num_libro_contable)
                                        INNER JOIN cb_c003_tipo_voucher acv ON (vm.fk_cbc003_num_voucher = acv.pk_num_voucher)
                                  WHERE
                                        vm.ind_estatus= 'AB'
                               ORDER BY
                                        vm.ind_anio, vm.ind_mes, vm.ind_nro_voucher");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
    * Ejecuta la acción de aprobar el voucher
    */
    public function metAprobarVoucher($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

          $sql= "UPDATE cb_c001_voucher_det
                    SET ind_estatus= 'AP',
                        fk_a018_num_seguridad_usuario= '$this->idUsuario',
                        fec_ultima_modificacion= NOW()
                  WHERE fk_cbb001_num_voucher_mast= '$data[pk_num_voucher_mast]' ";
          $db= $this->_db->prepare($sql);
          $db->execute();

          $sql= "UPDATE cb_b001_voucher
                    SET ind_estatus= 'AP',
                        fk_a018_num_seguridad_usuario= '$this->idUsuario',
                        fec_ultima_modificacion= NOW()
                  WHERE pk_num_voucher_mast= '$data[pk_num_voucher_mast]' ";
          $db= $this->_db->prepare($sql);
          $db->execute();

          ##  Consigna una transacción
          $this->_db->commit();
    }

    public function metTipoVoucher($id)
    {
       $db2= $this->_db->query("SELECT ind_descripcion FROM cb_c003_tipo_voucher WHERE pk_num_voucher='$id' ");
       $db2->setFetchMode(PDO::FETCH_ASSOC);
       return $db2->fetch();
    }

}
