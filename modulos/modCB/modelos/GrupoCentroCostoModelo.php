<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class GrupoCentroCostoModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /*
    * Obtengo datos de tabla para cargar listado de registros
    */
    public function metListar()
    {
        $db = $this->_db->query("SELECT * FROM a024_grupo_centro_costo");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Obtengo datos de tabla con búsqueda específica de GrupoCentroCosto
    */
    public function metMostrar($id)
    {
        $db = $this->_db->query("SELECT * FROM a024_grupo_centro_costo WHERE pk_num_grupo_centro_costo= '$id' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetch();
    }

    /*
    * Obtengo información a ser mostrada en formulario para editado y ver GrupoCentroCosto
    */
    public function metSubGrupoCentroCosto($id)
    {
        $db= $this->_db->query("SELECT a.*,
                                     b.ind_descripcion,
                                     b.pk_num_subgrupo_centro_costo,
                                     b.num_estatus
                                FROM a024_grupo_centro_costo a
                                     INNER JOIN a025_subgrupo_centro_costo b on (b.fk_a024_num_grupo_centro_costo=a.pk_num_grupo_centro_costo)
                               WHERE a.pk_num_grupo_centro_costo= '$id' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Ejecuta el proceso de registro del nuevo GrupoCentroCosto
    */
    public function metNuevo($data)
    {
        $this->_db->beginTransaction();

        $sql ="INSERT INTO a024_grupo_centro_costo
                           SET
                               cod_grupo_centro_costo='$data[cod_grupo_centro_costo]',
                               ind_descripcion='$data[ind_descripcion]',
                               num_estatus='$data[num_estatus]',
                               fk_a018_num_seguridad_usuario = '$this->idUsuario',
                               fec_ultima_modificacion = NOW() ";
        $db = $this->_db->prepare($sql);
        $db->execute();

        $id = $this->_db->lastInsertId();


        if (isset($data['detalle_ind_descripcion'])) {
            for($i=0; $i<count($data['detalle_ind_descripcion']); $i++){
                $sql= "INSERT INTO a025_subgrupo_centro_costo
                                SET ind_descripcion= '".$data['detalle_ind_descripcion'][$i]."',
                                    num_estatus= '".$data['detalle_num_estatus'][$i]."',
                                    fk_a024_num_grupo_centro_costo= '$id',
                                    fk_a018_num_seguridad_usuario = '$this->idUsuario',
                                    fec_ultima_modificacion = NOW() ";
                $db= $this->_db->prepare($sql);
                $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
            }
        }

        ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

    /*
    * Ejecuta el proceso de editado de un GrupoCentroCosto
    */
    public function metEditar($data)
    {
        $this->_db->beginTransaction();

        $modificar=""; $mensaje="";
        /* 24-10-2017
        $sql= $this->_db->query("SELECT a.*,
                                        c.pk_num_centro_costo
                                   FROM a024_grupo_centro_costo a
                                        INNER JOIN a025_subgrupo_centro_costo b on (b.fk_a024_num_grupo_centro_costo=a.pk_num_grupo_centro_costo)
                                        INNER JOIN a023_centro_costo c on (c.fk_a025_num_subgrupo_centro_costo=b.pk_num_subgrupo_centro_costo)
                                  WHERE a.pk_num_grupo_centro_costo= '$data[pk_num_grupo_centro_costo]' ");
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        $field= $sql->fetchAll();

        foreach ($field as $f) {
            if (isset($f['pk_num_grupo_centro_costo'])) {
                $mensaje= "Esta siendo utilizado";
                $swal= "Registro no puede ser Modificado!";
                $modificar= "nomodificar";
            }
        }
        */
        //if ($mensaje==""){
        $mensaje= "Registro actualizado exitosamente";
        $modificar= "modificar";
        $swal= "Registro Guardado!";
        //}

        if ($modificar=="modificar") {

            // actualizo grupo centro costo
            $sql= "UPDATE a024_grupo_centro_costo
                          SET
                              cod_grupo_centro_costo= '$data[cod_grupo_centro_costo]',
                              ind_descripcion= '$data[ind_descripcion]',
                              num_estatus= '$data[num_estatus]',
                              fk_a018_num_seguridad_usuario = '$this->idUsuario',
                              fec_ultima_modificacion = NOW()
                        WHERE
                              pk_num_grupo_centro_costo= '$data[pk_num_grupo_centro_costo]' ";
            $db= $this->_db->prepare($sql);
            $db->execute();

            //elimino datos realacionados a la contabilidad
            $sql = "DELETE FROM a025_subgrupo_centro_costo WHERE fk_a024_num_grupo_centro_costo= '$data[pk_num_grupo_centro_costo]' ";
            $db = $this->_db->prepare($sql);
            $db->execute();


            if (isset($data['detalle_ind_descripcion'])) {

                for($i=0; $i<count($data['detalle_ind_descripcion']); $i++){

                    if (!isset($data['detalle_num_estatus'][$i])) $data['detalle_num_estatus'][$i]= "0";

                    $sql= "INSERT INTO a025_subgrupo_centro_costo
                                       SET ind_descripcion= '".$data['detalle_ind_descripcion'][$i]."',
                                           num_estatus= '".$data['detalle_num_estatus'][$i]."',
                                           fk_a024_num_grupo_centro_costo= '$data[pk_num_grupo_centro_costo]',
                                           fk_a018_num_seguridad_usuario= '$this->idUsuario',
                                           fec_ultima_modificacion= NOW() ";
                    $db= $this->_db->prepare($sql);
                    $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
                }
            }
        }

        ##  Consigna una transacción
        $this->_db->commit();

        return $mensaje.'|'.$swal.'|'.$modificar;
    }

    /*
    * Ejecuta el proceso de eliminar un GrupoCentroCosto
    */
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql= $this->_db->query("SELECT a.*
                                       FROM a024_grupo_centro_costo a
                                            inner join a025_subgrupo_centro_costo b on (b.fk_a024_num_grupo_centro_costo=a.pk_num_grupo_centro_costo)
                                            inner join a023_centro_costo c on (c.fk_a025_num_subgrupo_centro_costo=b.pk_num_subgrupo_centro_costo)
                                      WHERE pk_num_grupo_centro_costo= '$data[pk_num_grupo_centro_costo]' ");
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        $field= $sql->fetch();

        if (isset($field['pk_num_grupo_centro_costo'])){
            $mensaje= "No puede ser Eliminado";
            $valor= "2";
        }else{

            $sql = "DELETE FROM a025_subgrupo_centro_costo WHERE fk_a024_num_grupo_centro_costo= '$data[pk_num_grupo_centro_costo]'";
            $db = $this->_db->prepare($sql);
            $db->execute();

            $sql = "DELETE FROM a024_grupo_centro_costo WHERE pk_num_grupo_centro_costo= '$data[pk_num_grupo_centro_costo]'";
            $db = $this->_db->prepare($sql);
            $db->execute();

            $mensaje= "Registro eliminado exitosamente";
            $valor= "1";

        }

        ##  Consigna una transacción
        $this->_db->commit();

        return $mensaje.'|'.$valor;
    }

}