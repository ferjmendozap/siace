<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION

 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *****************************************************************************************************************************************/
class scriptCargaModelo extends Modelo{

    public function __construct()
    {
	  parent:: __construct();
    }

	  public function metBusquedaSimple($tabla, $innerJoin, $where)
    {
        $busquedaSimple = $this->_db->query(" SELECT * FROM $tabla $innerJoin WHERE $where ");
        $busquedaSimple->setFetchMode(PDO::FETCH_ASSOC);
        return $busquedaSimple->fetch();
    }

    #MENU CB
    public function metCargarMenu($arrays)
    {
        $this->_db->beginTransaction();

        $registro = $this->_db->prepare(
            "INSERT INTO a027_seguridad_menu
                    SET  fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                         ind_descripcion=:ind_descripcion,
                         fec_ultima_modificacion=NOW(),
                         ind_estatus=1,
                         cod_interno=:cod_interno,
                         cod_padre=:cod_padre,
                         fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion,
                         ind_icono=:ind_icono,
                         ind_nombre=:ind_nombre,
                         ind_rol=:ind_rol,
                         ind_ruta=:ind_ruta,
                         num_nivel=:num_nivel
        ");

        $registroMenuPerfil = $this->_db->prepare(
            "INSERT INTO a028_seguridad_menupermiso
                    SET  fk_a017_num_seguridad_perfil=1,
                         fk_a027_num_seguridad_menu=:fk_a027_num_seguridad_menu
       ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a027_seguridad_menu', false, "cod_interno = '" . $array['cod_interno'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':num_nivel' => $array['num_nivel'],
                    ':ind_ruta' => $array['ind_ruta'],
                    ':ind_rol' => $array['ind_rol'],
                    ':ind_nombre' => mb_strtoupper($array['ind_nombre'], 'utf-8'),
                    ':ind_icono' => $array['ind_icono'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':cod_padre' => mb_strtoupper($array['cod_padre'], 'utf-8'),
                    ':cod_interno' => mb_strtoupper($array['cod_interno'], 'utf-8'),
                    ':ind_descripcion' => mb_strtoupper($array['ind_descripcion'], 'utf-8'),
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
                $idMenu = $this->_db->lastInsertId();
                $registroMenuPerfil->execute(array(
                    ':fk_a027_num_seguridad_menu' => $idMenu
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #MISCELANEOS CB
    public function metCargarMiscelaneos($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO a005_miscelaneo_maestro
                    SET  fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                         ind_descripcion=:ind_descripcion,
                         fec_ultima_modificacion=NOW(),
                         ind_estatus=1,
                         cod_maestro=:cod_maestro,
                         fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion,
                         ind_nombre_maestro=:ind_nombre_maestro
       ");

        $registroDetalle = $this->_db->prepare(
            "INSERT INTO a006_miscelaneo_detalle
                    SET  cod_detalle=:cod_detalle,
                         fk_a005_num_miscelaneo_maestro=:fk_a005_num_miscelaneo_maestro,
                         ind_estatus=:ind_estatus,
                         ind_nombre_detalle=:ind_nombre_detalle
        ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('a005_miscelaneo_maestro', false, "cod_maestro = '" . $array['cod_maestro'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':cod_maestro' => $array['cod_maestro'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':ind_nombre_maestro' => $array['ind_nombre_maestro']
                ));
                $idMiscelanneo = $this->_db->lastInsertId();
                foreach ($array['Det'] as $arrayDet) {
                    $registroDetalle->execute(array(
                        ':fk_a005_num_miscelaneo_maestro' => $idMiscelanneo,
                        ':cod_detalle' => $arrayDet['cod_detalle'],
                        ':ind_estatus' => $arrayDet['ind_estatus'],
                        ':ind_nombre_detalle' => $arrayDet['ind_nombre_detalle']
                    ));
                }
            }
        }
        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    /*
    #PARAMETROS CB
    public function metCargarParametros($arrays)
    {
        $this->_db->beginTransaction();

        $registro = $this->_db->prepare(
            "INSERT INTO a035_parametros
                    SET  ind_descripcion=:ind_descripcion,
                         ind_explicacion=:ind_explicacion,
                         ind_parametro_clave=:ind_parametro_clave,
                         ind_valor_parametro=:ind_valor_parametro,
                         ind_estatus=:ind_estatus,
                         fec_ultima_modificacion=:fec_ultima_modificacion,
                         fk_a015_num_seguridad_aplicacion=:fk_a015_num_seguridad_aplicacion,
                         fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                         fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle
            ");

        foreach ($arrays as $array) {

            $busqueda = $this->metBusquedaSimple('a035_parametros', false, "pk_num_parametros = '" . $array['pk_num_parametros'] . "'");

            if (!$busqueda) {
                $idMiscelaneosDet = $this->metMostrarSelect('TPPARAMET', $array['cod_detalle']);
                $registro->execute(array(
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':ind_explicacion' => $array['ind_explicacion'],
                    ':ind_parametro_clave' => $array['ind_parametro_clave'],
                    ':ind_valor_parametro' => $array['ind_valor_parametro'],
                    ':ind_estatus' => $array['ind_estatus'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a015_num_seguridad_aplicacion' => $array['fk_a015_num_seguridad_aplicacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':fk_a006_num_miscelaneo_detalle' => $idMiscelaneosDet['pk_num_miscelaneo_detalle']
                ));
            }

        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }
    */

    #MAESTRO SISTEMA FUENTE
    public function metCargarMaestroSistemaFuente($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO cb_c002_sistema_fuente
                    SET  pk_num_sistema_fuente=:pk_num_sistema_fuente,
                         cod_sistema_fuente=:cod_sistema_fuente,
                         ind_descripcion=:ind_descripcion,
                         num_estatus=:num_estatus,
                         fec_ultima_modificacion=:fec_ultima_modificacion,
                         fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cb_c002_sistema_fuente', false, "pk_num_sistema_fuente= '" . $array['pk_num_sistema_fuente'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_sistema_fuente' => $array['pk_num_sistema_fuente'],
                    ':cod_sistema_fuente' => $array['cod_sistema_fuente'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #MAESTRO TIPO VOUCHER
    public function metCargarMaestroTipoVoucher($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO cb_c003_tipo_voucher
                    SET  pk_num_voucher=:pk_num_voucher,
                         cod_voucher=:cod_voucher,
                         ind_descripcion=:ind_descripcion,
                         num_estatus=:num_estatus,
                         fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cb_c003_tipo_voucher', false, "pk_num_voucher= '" . $array['pk_num_voucher'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_voucher' => $array['pk_num_voucher'],
                    ':cod_voucher' => $array['cod_voucher'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #MAESTRO PLAN DE CUENTA
    public function metCargarMaestroPlanCuenta($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO cb_b004_plan_cuenta
                    SET  pk_num_cuenta=:pk_num_cuenta,
                         cod_cuenta=:cod_cuenta,
                         ind_grupo=:ind_grupo,
                         ind_subgrupo=:ind_subgrupo,
                         ind_rubro=:ind_rubro,
                         ind_cuenta=:ind_cuenta,
                         ind_sub_cuenta1=:ind_sub_cuenta1,
                         ind_sub_cuenta2=:ind_sub_cuenta2,
                         ind_sub_cuenta3=:ind_sub_cuenta3,
                         ind_descripcion=:ind_descripcion,
                         num_nivel=:num_nivel,
                         num_flag_tipo=:num_flag_tipo,
                         fk_a006_num_miscelaneo_detalle=:fk_a006_num_miscelaneo_detalle,
                         ind_tipo_saldo=:ind_tipo_saldo,
                         ind_naturaleza=:ind_naturaleza,
                         num_estatus=:num_estatus,
                         fec_ultima_modificacion=:fec_ultima_modificacion,
                         fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario,
                         num_flag_tipo_cuenta=:num_flag_tipo_cuenta,
                         num_flag_req_ccosto=:num_flag_req_ccosto,
                         num_flag_req_activo=:num_flag_req_activo
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cb_b004_plan_cuenta', false, "pk_num_cuenta= '" . $array['pk_num_cuenta'] . "'");
            if (!$busqueda) {

                $registro->execute(array(
                    ':pk_num_cuenta' => $array['pk_num_cuenta'],
                    ':cod_cuenta' => $array['cod_cuenta'],
                    ':ind_grupo' => $array['ind_grupo'],
                    ':ind_subgrupo' => $array['ind_subgrupo'],
                    ':ind_rubro' => $array['ind_rubro'],
                    ':ind_cuenta' => $array['ind_cuenta'],
                    ':ind_sub_cuenta1' => $array['ind_sub_cuenta1'],
                    ':ind_sub_cuenta2' => $array['ind_sub_cuenta2'],
                    ':ind_sub_cuenta3' => $array['ind_sub_cuenta3'],
                    ':ind_descripcion' => $array['ind_descripcion'],
                    ':num_nivel' => $array['num_nivel'],
                    ':num_flag_tipo' => $array['num_flag_tipo'],
                    ':fk_a006_num_miscelaneo_detalle' => $array['fk_a006_num_miscelaneo_detalle'],
                    ':ind_tipo_saldo' => $array['ind_tipo_saldo'],
                    ':ind_naturaleza' => $array['ind_naturaleza'],
                    ':num_estatus' => $array['num_estatus'],
                    ':fec_ultima_modificacion' => $array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario' => $array['fk_a018_num_seguridad_usuario'],
                    ':num_flag_tipo_cuenta' => $array['num_flag_tipo_cuenta'],
                    ':num_flag_req_ccosto' => $array['num_flag_req_ccosto'],
                    ':num_flag_req_activo' => $array['num_flag_req_activo']
                ));

            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #MAESTRO LIBRO CONTABLE
    public function metCargarMaestroLibroContable($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO cb_b002_libro_contable
                    SET  pk_num_libro_contable=:pk_num_libro_contable,
                         cod_libro=:cod_libro,
                         ind_descripcion=:ind_descripcion,
                         num_estatus=:num_estatus,
                         fec_ultima_modificacion=:fec_ultima_modificacion,
                         fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cb_b002_libro_contable', false, "pk_num_libro_contable= '" . $array['pk_num_libro_contable'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_libro_contable'=>$array['pk_num_libro_contable'],
                    ':cod_libro'=>$array['cod_libro'],
                    ':ind_descripcion'=>$array['ind_descripcion'],
                    ':num_estatus'=>$array['num_estatus'],
                    ':fec_ultima_modificacion'=>$array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario'=>$array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #MAESTRO CONTABILIDAD
    public function metCargarMaestroContabilidad($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO cb_b005_contabilidades
                    SET  pk_num_contabilidades=:pk_num_contabilidades,
                         ind_tipo_contabilidad=:ind_tipo_contabilidad,
                         ind_descripcion=:ind_descripcion,
                         num_estatus=:num_estatus,
                         fec_ultima_modificacion=:fec_ultima_modificacion,
                         fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cb_b005_contabilidades', false, "pk_num_contabilidades= '" . $array['pk_num_contabilidades'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_contabilidades'=>$array['pk_num_contabilidades'],
                    ':ind_tipo_contabilidad'=>$array['ind_tipo_contabilidad'],
                    ':ind_descripcion'=>$array['ind_descripcion'],
                    ':num_estatus'=>$array['num_estatus'],
                    ':fec_ultima_modificacion'=>$array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario'=>$array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #MAESTRO LIBRO CONTABILIDAD
    public function metCargaMaestroLibroContabilidad($arrays)
    {
        $this->_db->beginTransaction();
        $registro = $this->_db->prepare(
            "INSERT INTO cb_b003_libro_contabilidad
                    SET  pk_num_libro_contabilidad=:pk_num_libro_contabilidad,
                         ind_tipo_contabilidad=:ind_tipo_contabilidad,
                         fk_cbb002_num_libro_contable=:fk_cbb002_num_libro_contable,
                         fk_cbb005_num_contabilidades=:fk_cbb005_num_contabilidades,
                         fec_ultima_modificacion=:fec_ultima_modificacion,
                         fk_a018_num_seguridad_usuario=:fk_a018_num_seguridad_usuario
            ");

        foreach ($arrays as $array) {
            $busqueda = $this->metBusquedaSimple('cb_b003_libro_contabilidad', false, "pk_num_libro_contabilidad= '" . $array['pk_num_libro_contabilidad'] . "'");
            if (!$busqueda) {
                $registro->execute(array(
                    ':pk_num_libro_contabilidad'=>$array['pk_num_libro_contabilidad'],
                    ':ind_tipo_contabilidad'=>$array['ind_tipo_contabilidad'],
                    ':fk_cbb002_num_libro_contable'=>$array['fk_cbb002_num_libro_contable'],
                    ':fk_cbb005_num_contabilidades'=>$array['fk_cbb005_num_contabilidades'],
                    ':fec_ultima_modificacion'=>$array['fec_ultima_modificacion'],
                    ':fk_a018_num_seguridad_usuario'=>$array['fk_a018_num_seguridad_usuario']
                ));
            }
        }

        $error = $registro->errorInfo();
        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            var_dump($error);
            echo '<br><br>';
            return $error;
        } else {
            $idRegistro = $this->_db->lastInsertId();
            $this->_db->commit();
            return $idRegistro;
        }
    }

    #FUNCIÓN DE BUSQUEDA PARA OBTENER DATOS MISCELANEO
    public function metMostrarSelect($codMaestro, $codDetalle)
    {
        $miscelaneoDetalle = $this->_db->query("
            SELECT a006.pk_num_miscelaneo_detalle
              FROM a006_miscelaneo_detalle AS a006
                   INNER JOIN a005_miscelaneo_maestro AS a005 ON a006.fk_a005_num_miscelaneo_maestro=a005.pk_num_miscelaneo_maestro
             WHERE
                   a005.cod_maestro='$codMaestro' AND
                   a006.cod_detalle='$codDetalle'
        ");
        $miscelaneoDetalle->setFetchMode(PDO::FETCH_ASSOC);
        return $miscelaneoDetalle->fetch();
    }

    #FUNCIÓN CARGAR MENU
    public function metCargarMenuCB($array)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();
        ##
        $sql = "DELETE FROM a028_seguridad_menupermiso
                WHERE fk_a027_num_seguridad_menu IN (SELECT pk_num_seguridad_menu FROM a027_seguridad_menu WHERE fk_a015_num_seguridad_aplicacion = 10)";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        $sql = "DELETE FROM a027_seguridad_menu WHERE fk_a015_num_seguridad_aplicacion = 10";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##
        foreach ($array as $f) {
            $sql = "INSERT INTO a027_seguridad_menu
                    SET
                        ind_nombre='$f[ind_nombre]',
                        ind_descripcion='$f[ind_descripcion]',
                        cod_interno='$f[cod_interno]',
                        cod_padre='$f[cod_padre]',
                        num_nivel='$f[num_nivel]',
                        ind_ruta='$f[ind_ruta]',
                        ind_rol='$f[ind_rol]',
                        ind_icono='$f[ind_icono]',
                        num_estatus='$f[num_estatus]',
                        fk_a018_num_seguridad_usuario='$f[fk_a018_num_seguridad_usuario]',
                        fk_a015_num_seguridad_aplicacion='$f[fk_a015_num_seguridad_aplicacion]',
                        fec_ultima_modificacion = NOW()";
            $db = $this->_db->prepare($sql);
            $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        }
        ##
        $sql = "INSERT INTO a028_seguridad_menupermiso (fk_a027_num_seguridad_menu, fk_a017_num_seguridad_perfil)
                SELECT pk_num_seguridad_menu AS fk_a027_num_seguridad_menu, '1' AS fk_a017_num_seguridad_perfil
                FROM a027_seguridad_menu
                WHERE fk_a015_num_seguridad_aplicacion = 10";
        $db = $this->_db->prepare($sql);
        $db->execute() or die($db->errorInfo()[2].'<br>Query: '.$sql);
        ##  Consigna una transacción
        $this->_db->commit();
    }

}
