<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class ListaVoucherModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
    * función que permite obtener información de los vouchers registrados en sistema
    * en forma de lista
    */
    public function metListar($filtro=null)
    {
        $filtrar = "";

        if (isset($filtro['fnum_contabilidades'])) if ($filtro['fnum_contabilidades'] != "") $filtrar.= " AND vm.fk_cbb005_num_contabilidades = '$filtro[fnum_contabilidades]'";

        if (isset($filtro['fperiodo'])){
          if ($filtro['fperiodo'] != ""){
             $valor_f= explode("-", $filtro['fperiodo']);
             $ind_anio= $valor_f[0];
             $ind_mes= $valor_f[1];

             $filtrar.= " AND vm.ind_mes='$ind_mes' AND vm.ind_anio='$ind_anio' ";
          }/*else{
             $ind_anio= date("Y");
             $ind_mes= date("m");

             $filtrar.= " AND vm.ind_mes='$ind_mes' AND vm.ind_anio='$ind_anio' ";
          }*/
        }

        if (isset($filtro['fnum_voucher'])) if ($filtro['fnum_voucher'] != "") $filtrar.= " AND  vm.fk_cbc003_num_voucher= '$filtro[fnum_voucher]' ";

        if (isset($filtro['festado_voucher'])) if($filtro['festado_voucher'] != "") $filtrar.= " AND vm.ind_estatus= '$filtro[festado_voucher]' ";

        $db = $this->_db->query("SELECT
                                        vm.*,
                                        o.pk_num_organismo,
                                        d.pk_num_dependencia,
                                        -- lc.Descripcion AS NomLibroContable,
                                        acv.ind_descripcion
                                  FROM
                                        cb_b001_voucher vm
                                        INNER JOIN a001_organismo o ON (vm.fk_a001_num_organismo = o.pk_num_organismo)
                                        LEFT JOIN a004_dependencia d ON (vm.fk_a004_num_dependencia = d.pk_num_dependencia)
                                        -- LEFT JOIN cb_b002_libro_contable lc ON (vm.CodLibroCont = lc.pk_num_libro_contable)
                                        INNER JOIN cb_c003_tipo_voucher acv ON (vm.fk_cbc003_num_voucher = acv.pk_num_voucher)
                                 WHERE 1 $filtrar
                              ORDER BY
                                        vm.pk_num_voucher_mast, vm.ind_anio, vm.ind_mes, vm.ind_nro_voucher, vm.ind_estatus");

        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
    * permite consultar el período del voucher
    * @return valor obtenido
    */
    public function metPeriodo($ind_mes, $fec_anio)
    {
      $sql= $this->_db->query("SELECT *
                                 FROM cb_c005_control_cierre_mensual
                                WHERE ind_mes='$ind_mes' and fec_anio='$fec_anio' and ind_estatus='A' ");
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      $field= $sql->fetch();

      return $field;
    }

    /**
    * permite consultar el contabilidad del voucher
    * @return pk_num_contabilidades
    */
    public function metContabilidades($data)
    {
       $db= $this->_db->query("SELECT pk_num_contabilidades FROM cb_b005_contabilidades WHERE  ind_contabilidad_acronimo='$data' ");
       $db->setFetchMode(PDO::FETCH_ASSOC);
       $field= $db->fetch();
       return $field['pk_num_contabilidades'];
    }

    /**
    * realiza el proceso de registro de los datos del nuevo voucher
    */
    public function metNuevo($data)
    {
       // consulto para verificar si el período a ingresar se encuentra creado y abierto
       $c_periodo= $this->metPeriodo($data['ind_mes'], $data['ind_anio']);

       if (isset($c_periodo['pk_num_control_cierre'])) {

          if($data['monto_debe']==$data['monto_haber']){

              $this->_db->beginTransaction();

               $tipo_voucher= $this->_db->query("SELECT cod_voucher
                                                   FROM cb_c003_tipo_voucher
                                                  WHERE pk_num_voucher= '$data[fk_cbc003_num_voucher]' ");
               $tipo_voucher->setFetchMode(PDO::FETCH_ASSOC);
               $f_tipo_voucher= $tipo_voucher->fetch();

               // Busco nro voucher
               $nro_voucher= $this->_db->query("SELECT max(ind_nro_voucher) as nro_max_voucher
                                                   from cb_b001_voucher
                                                  where ind_mes= '$data[ind_mes]' and
                                                        ind_anio= '$data[ind_anio]' and
                                                        fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]' and
                                                        fk_a001_num_organismo= '$data[fk_a001_num_organismo]' and
                                                        fk_cbc003_num_voucher= '$data[fk_cbc003_num_voucher]' ");
                $nro_voucher->setFetchMode(PDO::FETCH_ASSOC);
                $f_nro_voucher= $nro_voucher->fetch();

                if ($f_nro_voucher['nro_max_voucher']!=""){
                    $codigo=(int) ($f_nro_voucher['nro_max_voucher']+1);
                    $codigo=(string) str_repeat("0", 4-strlen($codigo)).$codigo;
                    $valor_ind_voucher= $f_tipo_voucher['cod_voucher']."-".$codigo;
                 }else{
                    $valor_ind_voucher= $f_tipo_voucher['cod_voucher']."-0001";
                    $codigo= '0001';
                 }

                // Busco PK Libro Contabilidad
                $l_contabilidad= $this->_db->query("SELECT pk_num_libro_contabilidad
                                                      FROM cb_b003_libro_contabilidad
                                                     WHERE fk_cbb002_num_libro_contable= '$data[fk_cbb002_num_libro_contable]' AND
                                                           fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]' ");
                $l_contabilidad->setFetchMode(PDO::FETCH_ASSOC);
                $f_l_contabilidad= $l_contabilidad->fetch();

                // Insertamos datos a la tabla
                $sql ="INSERT INTO cb_b001_voucher
                               SET
                                    ind_mes= '$data[ind_mes]',
                                    ind_anio= '$data[ind_anio]',
                                    ind_voucher='$valor_ind_voucher',
                                    ind_nro_voucher='$codigo',
                                    fk_a003_num_preparado_por= '$data[fk_a003_num_preparado_por]',
                                    fec_fecha_preparacion= NOW(),
                                    txt_titulo_voucher= '$data[txt_titulo_voucher]',
                                    fec_fecha_voucher= '$data[fec_fecha_voucher]',
                                    num_flag_transferencia= '$data[num_flag_transferencia]',
                                    fk_cbc002_num_sistema_fuente= '$data[fk_cbc002_num_sistema_fuente]',
                                    fk_cbc003_num_voucher= '$data[fk_cbc003_num_voucher]',
                                    fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]',
                                    fk_a001_num_organismo= '$data[fk_a001_num_organismo]',
                                    fk_a004_num_dependencia= '$data[fk_a004_num_dependencia]',
                                    fk_cbb003_num_libro_contabilidad= '".$f_l_contabilidad['pk_num_libro_contabilidad']."',
                                    fec_ultima_modificacion= NOW(),
                                    fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
                $db = $this->_db->prepare($sql);
                $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));
                $id = $this->_db->lastInsertId();

                //cuentas
                if (isset($data['detalle_fk_cbb004_num_cuenta'])) {
                  $debitos=0;
                  $creditos=0;

                  for ($i=0; $i < count($data['detalle_fk_cbb004_num_cuenta']); $i++) {

                    $linea = $i;
                    $linea++;

                    $formato_debitos= Number:: format($data['detalle_num_debe'][$i]);
                    $formato_creditos= Number:: format($data['detalle_num_haber'][$i]);

                    $debitos+= $formato_debitos;
                    $creditos+= $formato_creditos;

                    ##  registro
                    $sql = "INSERT INTO cb_c001_voucher_det
                                    SET
                                        ind_mes = '$data[ind_mes]',
                                        fec_anio = '$data[ind_anio]',
                                        num_linea = '$linea',
                                        num_debe = '$formato_debitos',
                                        num_haber = '$formato_creditos',
                                        ind_descripcion = '".$data['detalle_ind_descripcion'][$i]."',
                                        ind_estatus = 'AB',
                                        fk_a023_num_centro_costo = '".$data['detalle_fk_a023_num_centro_costo'][$i]."',
                                        fk_cbb001_num_voucher_mast = '$id',
                                        fk_cbb004_num_cuenta = '".$data['detalle_fk_cbb004_num_cuenta'][$i]."',
                                        fk_a003_num_persona = '".$data['detalle_fk_a003_num_persona'][$i]."',
                                        fk_cbb003_num_libro_contabilidad = '".$f_l_contabilidad['pk_num_libro_contabilidad']."',
                                        fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]',
                                        fec_ultima_modificacion = NOW(),
                                        fk_a018_num_seguridad_usuario= '$this->idUsuario' "; //echo $sql."\n\n";
                    $db = $this->_db->prepare($sql);
                    $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]]));

                    $formato_creditos= 0;
                    $formato_debitos= 0;
                  }
                }

                // actualizo campos
                $sql= "UPDATE cb_b001_voucher
                          SET num_creditos= '$creditos',
                              num_debitos= '$debitos'
                        WHERE pk_num_voucher_mast= '$id' ";
                $db = $this->_db->prepare($sql);
                $db->execute();


                ##  Consigna una transacción
                $this->_db->commit();

                return $id;

            }else {
                $id= "no-igual";
                return $id;
            }

        }else{

            $id= "no-periodo";
            return $id;
        }
    }

    /**
    * realiza consulta de registro específico
    * @return valor obtenido
    */
    public function metMostrar($id)
    {
        $db = $this->_db->query("SELECT * FROM cb_b001_voucher WHERE pk_num_voucher_mast='$id' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetch();
    }

    /**
    * realiza consulta de registro específico
    * @return ind_descripcion
    */
    public function metTipoVoucher($id)
    {
       $db= $this->_db->query("SELECT ind_descripcion FROM cb_c003_tipo_voucher WHERE pk_num_voucher='$id' ");
       $db->setFetchMode(PDO::FETCH_ASSOC);
       return $db->fetch();
    }

    /**
    * realiza consulta de registro específico
    * @return fk_cbb002_num_libro_contable
    */
    public function metLibroContabilidad($id)
    {
      $db= $this->_db->query("SELECT fk_cbb002_num_libro_contable FROM cb_b003_libro_contabilidad WHERE pk_num_libro_contabilidad= '$id' ");
      $db->setFetchMode(PDO::FETCH_ASSOC);
      return $db->fetch();
    }

    /**
    * utilizada para cargar información de voucher específico
    * para los procesos de ver y editar
    */
    public function metListarVoucher($id)
    {
        $db = $this->_db->query("SELECT
                                        b.*,
                                        c.pk_num_cuenta as fk_cbb004_num_cuenta,
                                        c.cod_cuenta as num_cuenta,
                                        c.ind_descripcion as descripcion_cuenta,
                                        d.ind_descripcion_centro_costo as num_centro_costo,
                                        d.pk_num_centro_costo as fk_a023_num_centro_costo,
                                        --CONCAT(e.ind_nombre1,' ',e.ind_apellido1) AS num_persona,
                                        e.ind_apellido1 as apellido,
                                        e.ind_nombre1 as nombre,
                                        e.ind_cedula_documento as num_persona,
                                        e.pk_num_persona as fk_a003_num_persona,
                                        a.fk_cbb003_num_libro_contabilidad

                                  FROM
                                        cb_b001_voucher a
                                        INNER JOIN cb_c001_voucher_det b ON (b.fk_cbb001_num_voucher_mast = a.pk_num_voucher_mast)
                                        INNER JOIN cb_b004_plan_cuenta c ON (c.pk_num_cuenta = b.fk_cbb004_num_cuenta)
                                        INNER JOIN a023_centro_costo d ON (d.pk_num_centro_costo = b.fk_a023_num_centro_costo)
                                        INNER JOIN a003_persona e ON (e.pk_num_persona = b.fk_a003_num_persona)
                                  where
                                        a.pk_num_voucher_mast= '$id'  ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
    *
    */
    /*public function indVoucher($data)
    {

       $tipo_voucher= $this->_db->query("SELECT cod_voucher
                                           FROM cb_c003_tipo_voucher
                                          WHERE pk_num_voucher= '$data[fk_cbc003_num_voucher]' ");
       $tipo_voucher->setFetchMode(PDO::FETCH_ASSOC);
       $f_tipo_voucher= $tipo_voucher->fetch();

       // Busco nro voucher
       $nro_voucher= $this->_db->query("SELECT max(ind_nro_voucher) as nro_max_voucher
                                           from cb_b001_voucher
                                          where ind_mes= '$data[ind_mes]' and
                                                ind_anio= '$data[fec_anio]' and
                                                fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]' and
                                                fk_a001_num_organismo= '$data[fk_a001_num_organismo]' and
                                                fk_cbc003_num_voucher= '$data[fk_cbc003_num_voucher]' ");
        $nro_voucher->setFetchMode(PDO::FETCH_ASSOC);
        $f_nro_voucher= $nro_voucher->fetch();

        if ($f_nro_voucher['nro_max_voucher']!=""){
            $codigo=(int) ($f_nro_voucher['nro_max_voucher']+1);
            $codigo=(string) str_repeat("0", 4-strlen($codigo)).$codigo;
            $valor_ind_voucher= $f_tipo_voucher['cod_voucher']."-".$codigo;
         }else{
            $valor_ind_voucher= $f_tipo_voucher['cod_voucher']."-0001";
            $codigo= '0001';
         }

         return $valor_ind_voucher.'|'.$codigo;
    } */

    /**
    * realiza el proceso de editado de voucher específico
    */
    public function metEditar($data)
    {
          //consulto para obtener valor estado del voucher
          $sql= $this->_db->query("SELECT *
                                     FROM cb_b001_voucher
                                    WHERE pk_num_voucher_mast= '$data[pk_num_voucher_mast]' and
                                          ind_estatus='AB' ");
          $sql->setFetchMode(PDO::FETCH_ASSOC);
          $field= $sql->fetch();

          if (count($field['pk_num_voucher_mast'])!=0){

             if($data['monto_debe']==$data['monto_haber']){

                  $this->_db->beginTransaction();

                  // eliminar datos de voucher_det
                  $sql= "DELETE FROM cb_c001_voucher_det WHERE fk_cbb001_num_voucher_mast= '$data[pk_num_voucher_mast]' "; //echo $sql."\n\n";
                  $db= $this->_db->prepare($sql);
                  $db->execute();

                  // Busco PK Libro Contabilidad
                  $l_contabilidad= $this->_db->query("SELECT pk_num_libro_contabilidad
                                                        FROM cb_b003_libro_contabilidad
                                                       WHERE fk_cbb002_num_libro_contable= '$data[fk_cbb002_num_libro_contable]' AND
                                                             fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]' ");
                  $l_contabilidad->setFetchMode(PDO::FETCH_ASSOC);
                  $f_l_contabilidad= $l_contabilidad->fetch();


                  //  cargando detalle del Voucher
                  if (isset($data['detalle_fk_cbb004_num_cuenta'])) {
                      $debitos=0;
                      $creditos=0;

                      for ($i=0; $i < count($data['detalle_fk_cbb004_num_cuenta']); $i++) {

                        $linea = $i;
                        $linea++;

                        //conversión formato de número
                        $formato_debitos= Number:: format($data['detalle_num_debe'][$i]);
                        $formato_creditos= Number:: format($data['detalle_num_haber'][$i]);

                        $debitos+= $formato_debitos;
                        $creditos+= $formato_creditos;

                        ##  registro
                        $sql = "INSERT INTO cb_c001_voucher_det
                                        SET
                                            ind_mes = '$data[ind_mes]',
                                            fec_anio = '$data[fec_anio]',
                                            num_linea = '$linea',
                                            num_debe = '$formato_debitos',
                                            num_haber = '$formato_creditos',
                                            ind_descripcion = '".$data['detalle_ind_descripcion'][$i]."',
                                            ind_estatus = 'AB',
                                            fk_a023_num_centro_costo = '".$data['detalle_fk_a023_num_centro_costo'][$i]."',
                                            fk_cbb001_num_voucher_mast = '$data[pk_num_voucher_mast]',
                                            fk_cbb004_num_cuenta = '".$data['detalle_fk_cbb004_num_cuenta'][$i]."',
                                            fk_a003_num_persona = '".$data['detalle_fk_a003_num_persona'][$i]."',
                                            fk_cbb003_num_libro_contabilidad = '".$f_l_contabilidad['pk_num_libro_contabilidad']."',
                                            fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]',
                                            fec_ultima_modificacion = NOW(),
                                            fk_a018_num_seguridad_usuario = '$this->idUsuario' ";
                        $db = $this->_db->prepare($sql);
                        $db->execute() or die(json_encode(['status'=>'error', 'mensaje'=>[$db->errorInfo()[2]]])); //echo $sql."\n\n";

                        $formato_creditos= 0;
                        $formato_debitos= 0;
                      }
                  }

                  // Actualizamos tabla cb_b001_voucher
                  $sql =" UPDATE cb_b001_voucher
                             SET
                                ind_mes= '$data[ind_mes]',
                                ind_anio= '$data[fec_anio]',
                                txt_titulo_voucher= '$data[txt_titulo_voucher]',
                                fec_fecha_voucher= '$data[fec_fecha_voucher]',
                                fk_cbb005_num_contabilidades= '$data[fk_cbb005_num_contabilidades]',
                                fk_a001_num_organismo= '$data[fk_a001_num_organismo]',
                                fk_a004_num_dependencia= '$data[fk_a004_num_dependencia]',
                                fk_cbb003_num_libro_contabilidad= '".$f_l_contabilidad['pk_num_libro_contabilidad']."',
                                fec_ultima_modificacion= NOW(),
                                fk_a018_num_seguridad_usuario= '$this->idUsuario',
                                num_creditos= '$creditos',
                                num_debitos= '$debitos'
                          WHERE
                                pk_num_voucher_mast= '$data[pk_num_voucher_mast]' AND
                                ind_estatus= 'AB' "; //echo $sql."\n\n";
                   $db = $this->_db->prepare($sql);
                   $db->execute();

                  ##  Consigna una transacción
                  $this->_db->commit();

                  $id= "si-modificar";
                  return $id;

             }else{
                $id= "no-igual";
                return $id;
             }

          }else{
             $id= "no-estatus-permitido";
             return $id;
          }
    }

    /**
    * realiza el proceso de eliminado de voucher específico
    */
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql= $this->_db->query("SELECT *
                                   FROM cb_b001_voucher
                                  WHERE pk_num_voucher_mast= '$data[pk_num_voucher_mast]' AND
                                        ind_estatus= 'MA' ");
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        $field= $sql->fetch();

        if (count($field['pk_num_voucher_mast'])==0) {

              $sql= "DELETE FROM cb_c001_voucher_det WHERE fk_cbb001_num_voucher_mast= '$data[pk_num_voucher_mast]' ";
              $db= $this->_db->prepare($sql);
              $db->execute();

              $sql = "DELETE FROM cb_b001_voucher WHERE pk_num_voucher_mast= '$data[pk_num_voucher_mast]'";
              $db = $this->_db->prepare($sql);
              $db->execute();
        }

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /**
    * realiza el consulta de datos de persona para cargar listado de personas
    */
    public function metMostrar_persona()
    {
        $db = $this->_db->query("SELECT * FROM a003_persona ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
    * realiza el consulta de datos de persona para cargar listado de centro de costo
    */
    public function metMostrar_ccosto()
    {
        $db = $this->_db->query("SELECT * FROM a023_centro_costo WHERE num_estatus='1' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
    * Anular Voucher
    */
    public function metAnularVoucher($data)
    {
       ##  Inicia una transacción
        $this->_db->beginTransaction();

         $ind_estatus=""; $monto_cuenta=0;
         //consulto para obtener valor estado del voucher
          $sql= $this->_db->query("SELECT *
                                     FROM cb_b001_voucher
                                    WHERE pk_num_voucher_mast= '$data[pk_num_voucher_mast]' ");
          $sql->setFetchMode(PDO::FETCH_ASSOC);
          $field= $sql->fetch();

          if ($field['ind_estatus']=="AB") $ind_estatus= "AN";
          elseif($field['ind_estatus']=="AP") $ind_estatus= "AB";
          elseif($field['ind_estatus']=="MA") $ind_estatus= "AP";
          elseif($field['ind_estatus']=="AN") $ind_estatus= "AN";

          if ($field['ind_estatus']=="MA") {
              # code...

              $sql= $this->_db->query("SELECT b.*
                                         FROM cb_b001_voucher a
                                              INNER JOIN cb_c001_voucher_det b on (b.fk_cbb001_num_voucher_mast=a.pk_num_voucher_mast)
                                        WHERE a.pk_num_voucher_mast= '$data[pk_num_voucher_mast]' ");
              $sql->setFetchMode(PDO::FETCH_ASSOC);
              $field_1= $sql->fetch();

              foreach ($field_1 as $f_anular) {

                if ($f_anular['num_debe']>0) $monto_cuenta= $f_anular['num_debe'];
                elseif($f_anular['num_haber']>0) $monto_cuenta= $f_anular['num_haber'];

                $sql= "UPDATE cb_c004_voucher_balance
                          SET num_saldo_balance= (num_saldo_balance - $monto_cuenta)
                        WHERE fk_cbb004_num_cuenta= '".$f_anular['fk_cbb004_num_cuenta']."' AND
                              ind_mes= '".$f_anular['ind_mes']."' AND
                              fec_anio= '".$f_anular['fec_anio']."' ";
                $db= $this->_db->prepare($sql);
                $db->execute();
              }

          }

         $sql ="UPDATE cb_b001_voucher
                   SET ind_estatus= '$ind_estatus',
                       fk_a018_num_seguridad_usuario= '$this->idUsuario',
                       fec_ultima_modificacion= NOW()
                 WHERE pk_num_voucher_mast= '$data[pk_num_voucher_mast]' ";
         $db = $this->_db->prepare($sql);
         $db->execute();

         $sql ="UPDATE cb_c001_voucher_det
                   SET ind_estatus= '$ind_estatus',
                       fk_a018_num_seguridad_usuario= '$this->idUsuario',
                       fec_ultima_modificacion= NOW()
                 WHERE fk_cbb001_num_voucher_mast= '$data[pk_num_voucher_mast]' ";
         $db = $this->_db->prepare($sql);
         $db->execute();

         ##  Consigna una transacción
         $this->_db->commit();
    }

    /**
    * Obtiene información de persona y organismo según voucher
    * consultado para imprimir
    */
    public function metConsulta($pk_num_voucher_mast)
    {
      $db= $this->_db->query("SELECT a.*,
                                     b.ind_nombre1 as Nombre,
                                     b.ind_apellido1 as Apellido,
                                     c.ind_descripcion_empresa as NombEmpresa
                                FROM cb_b001_voucher a
                                     INNER JOIN a003_persona b on (b.pk_num_persona = a.fk_a003_num_preparado_por)
                                     INNER JOIN a001_organismo c ON (a.fk_a001_num_organismo=c.pk_num_organismo)
                               WHERE a.pk_num_voucher_mast= '$pk_num_voucher_mast' ");
      $db->setFetchMode(PDO::FETCH_ASSOC);
      return $db->fetchAll();
    }

    /**
    * Obtiene información detalle de voucher para imprimir reporte
    */
    public function metConsulta2($pk_num_voucher_mast)
    {
      $db= $this->_db->query("SELECT *
                                FROM cb_c001_voucher_det
                               WHERE fk_cbb001_num_voucher_mast= '$pk_num_voucher_mast' ");
      $db->setFetchMode(PDO::FETCH_ASSOC);
      return $db->fetchAll();
    }

    public function metConsultaPersona($id_usuario)
    {
        $db= $this->_db->query("SELECT c.pk_num_persona
                                  FROM a018_seguridad_usuario a
                                       INNER JOIN rh_b001_empleado b ON (b.pk_num_empleado=a.fk_rhb001_num_empleado)
                                       INNER JOIN a003_persona c ON (c.pk_num_persona=b.fk_a003_num_persona)
                                 WHERE
                                       a.pk_num_seguridad_usuario= '$id_usuario' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        $field= $db->fetch();
        return $field['pk_num_persona'];
    }
}
