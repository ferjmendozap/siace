<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class LibroDiarioModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /*
    * permite consultar el contabilidad del voucher
    * @return pk_num_contabilidades
    */
    public function metContabilidades($data)
    {
       $db= $this->_db->query("SELECT pk_num_contabilidades FROM cb_b005_contabilidades WHERE  ind_contabilidad_acronimo='$data' ");
       $db->setFetchMode(PDO::FETCH_ASSOC);
       $field= $db->fetch();
       return $field['pk_num_contabilidades'];
    }

    /*
    * Consulta datos de voucher
    */
    public function metConsultaVoucher($ind_voucher)
    {
        $db= $this->_db->query("SELECT *
                                  FROM cb_b001_voucher
                                 WHERE ind_voucher= '$ind_voucher' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Consulta datos de voucher según parámetros de filtro
    */
    public function metLibroDiarioReportePdf($contabilidad, $filtro)
    {
        $db = $this->_db->query("SELECT a.*,
                                        b.fec_fecha_voucher,
                                        b.txt_titulo_voucher,
                                        b.num_creditos,
                                        b.ind_voucher,
                                        b.pk_num_voucher_mast
                                  FROM
                                        cb_c001_voucher_det a
                                        INNER JOIN cb_b001_voucher b ON (b.pk_num_voucher_mast=a.fk_cbb001_num_voucher_mast AND
                                                                         b.ind_mes=a.ind_mes AND
                                                                         b.ind_anio=a.fec_anio AND
                                                                         b.fk_cbb005_num_contabilidades= '$contabilidad')
                                 WHERE
                                        a.ind_estatus= 'MA' $filtro
                              ORDER BY
                                        a.fk_cbb001_num_voucher_mast, a.num_linea ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Consulta datos, obteniendo cantidad detalles del voucher
    */
    public function metLibroDiarioCantidadPdf($contabilidad, $filtro)
    {
        $db = $this->_db->query("SELECT count(pk_num_voucher_det) as cantidad
                                  FROM
                                        cb_c001_voucher_det a
                                        INNER JOIN cb_b001_voucher b ON (b.pk_num_voucher_mast=a.fk_cbb001_num_voucher_mast AND
                                                                         b.ind_mes=a.ind_mes AND
                                                                         b.ind_anio=a.fec_anio AND
                                                                         b.fk_cbb005_num_contabilidades= '$contabilidad')
                                 WHERE
                                        a.ind_estatus= 'MA' $filtro
                              ORDER BY
                                        a.fk_cbb001_num_voucher_mast, a.num_linea ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        $field= $db->fetch();

        return $field['cantidad'];
    }

    /*
    * Consulta datos de la cuenta
    */
    public function metConsulta2Pdf($pk_num_cuenta)
    {
        $db= $this->_db->query("SELECT ind_descripcion,
                                       cod_cuenta
                                  FROM cb_b004_plan_cuenta
                                 WHERE pk_num_cuenta= '$pk_num_cuenta' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Consulta datos de centro costo
    */
    public function metCentroCosto($pk_num_centro_costo)
    {
        $db= $this->_db->query("SELECT pk_num_centro_costo,
                                       ind_descripcion_centro_costo
                                  FROM a023_centro_costo
                                 WHERE pk_num_centro_costo= '$pk_num_centro_costo' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

}
