<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class LibroMayorModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /*
    * permite consultar el contabilidad del voucher
    * @return pk_num_contabilidades
    */
    public function metContabilidades($data)
    {
       $db= $this->_db->query("SELECT pk_num_contabilidades FROM cb_b005_contabilidades WHERE  ind_contabilidad_acronimo='$data' ");
       $db->setFetchMode(PDO::FETCH_ASSOC);
       $field= $db->fetch();
       return $field['pk_num_contabilidades'];
    }

    /*
    * Consulta de datos en tabla
    */
    public function metLibroMayorReportePdf($num_libro_contable, $tipo_cuenta, $fec_anio, $ind_mes, $filtro_cuenta, $num_contabilidades)
    {

        $libro_contable= $this->_db->query("SELECT *
                                              FROM cb_b003_libro_contabilidad
                                             WHERE fk_cbb002_num_libro_contable='$num_libro_contable' AND
                                                   fk_cbb005_num_contabilidades='$num_contabilidades' ");
        $libro_contable->setFetchMode(PDO::FETCH_ASSOC);
        $f_libro_contable= $libro_contable->fetch();

        $db = $this->_db->query("SELECT a.*,
                                        b.ind_descripcion,
                                        b.num_nivel,
                                        b.cod_cuenta,
                                        b.pk_num_cuenta,
                                        b.num_flag_tipo_cuenta,
                                        c.ind_tipo_contabilidad,
                                        c.pk_num_libro_contabilidad
                                  FROM
                                        cb_c004_voucher_balance a
                                        INNER JOIN cb_b004_plan_cuenta b ON (b.pk_num_cuenta=a.fk_cbb004_num_cuenta AND
                                                                             b.num_flag_tipo_cuenta='$tipo_cuenta')
                                        INNER JOIN cb_b003_libro_contabilidad c ON (c.pk_num_libro_contabilidad=a.fk_cbb003_num_libro_contabilidad)
                                 WHERE
                                        a.fec_anio= '$fec_anio' AND
                                        a.ind_mes= '$ind_mes'  AND
                                        a.fk_cbb003_num_libro_contabilidad= '".$f_libro_contable['pk_num_libro_contabilidad']."' $filtro_cuenta
                              GROUP BY
                                        b.cod_cuenta asc ");
        //var_dump($db);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();

    }

    /*
    * Consulta de datos en tabla
    */
    public function metLibroMayorReportePdf2($num_libro_contabilidad, $tipo_cuenta, $fec_anio, $ind_mes, $pk_num_cuenta)
    {
        $db = $this->_db->query("SELECT a.*,
                                        b.ind_descripcion,
                                        sum(a.num_saldo_balance),
                                        b.num_nivel,
                                        b.cod_cuenta,
                                        b.pk_num_cuenta,
                                        c.ind_tipo_contabilidad
                                  FROM
                                        cb_c004_voucher_balance a
                                        INNER JOIN cb_b004_plan_cuenta b ON (b.pk_num_cuenta=a.fk_cbb004_num_cuenta AND
                                                                             b.num_flag_tipo_cuenta='$tipo_cuenta')
                                        INNER JOIN cb_b003_libro_contabilidad c ON (c.pk_num_libro_contabilidad=a.fk_cbb003_num_libro_contabilidad)
                                 WHERE
                                        a.fec_anio= '$fec_anio' AND
                                        a.ind_mes <= '$ind_mes'  AND
                                        a.fk_cbb004_num_cuenta= '$pk_num_cuenta' AND
                                        a.fk_cbb003_num_libro_contabilidad= '$num_libro_contabilidad' ");
        //var_dump($db);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Consulta de datos obteniendo información de las cuentas
    */
    public function metConsultaSubGrupo($subGrupo, $tipo_cuenta)
    {
        $db= $this->_db->query("SELECT *
                                  FROM cb_b004_plan_cuenta
                                 WHERE cod_cuenta= '$subGrupo' AND
                                       num_flag_tipo_cuenta= '$tipo_cuenta' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Consulta de datos en tabla obteniendo montos registrados en
    * las cuentas.
    */
    public function metConsulta3Pdf($mes, $anio, $pk_num_cuenta)
    {
        $db= $this->_db->query("SELECT
                                      DISTINCT(a1.fk_cbb004_num_cuenta),
                                      sum(a1.num_debe) as Deudor,
                                      (Select
                                             sum(a2.num_haber)
                                         From
                                             cb_c001_voucher_det a2
                                        Where
                                             a1.fk_cbb004_num_cuenta=a2.fk_cbb004_num_cuenta and
                                             a1.fec_anio= a2.fec_anio and
                                             a1.ind_mes= a2.ind_mes and
                                             a2.num_haber>0 and
                                             a1.ind_estatus=a2.ind_estatus
                                     Group By
                                             a2.fk_cbb004_num_cuenta, a2.fec_anio, a2.ind_mes) as Acreedor
                                 FROM
                                     cb_c001_voucher_det a1
                                WHERE
                                      a1.fk_cbb004_num_cuenta= '$pk_num_cuenta' AND
                                      a1.fec_anio= '$anio' AND
                                      a1.ind_mes= '$mes'  AND
                                      a1.num_debe>'0' OR a1.num_haber>'0' AND
                                      a1.ind_estatus= 'MA'
                                GROUP BY
                                      a1.fk_cbb004_num_cuenta, a1.fec_anio, a1.ind_mes ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Consulta de datos en tabla
    */
    public function metDetalleReporte($mes, $anio, $num_libro_contabilidad, $pk_num_cuenta)
    {
      $db= $this->_db->query("SELECT b.ind_voucher,
                                     b.fec_fecha_voucher,
                                     b.txt_titulo_voucher,
                                     a.num_linea,
                                     a.fk_a003_num_persona as num_persona,
                                     a.num_debe,
                                     a.num_haber
                                FROM cb_c001_voucher_det a
                                     INNER JOIN cb_b001_voucher b on (b.pk_num_voucher_mast=a.fk_cbb001_num_voucher_mast and
                                                                      b.ind_mes=a.ind_mes and
                                                                      b.ind_anio=a.fec_anio)
                               WHERE a.ind_mes='$mes'  and
                                     a.fec_anio='$anio' and
                                     a.ind_estatus='MA' and
                                     a.fk_cbb004_num_cuenta='$pk_num_cuenta' and
                                     a.fk_cbb003_num_libro_contabilidad='$num_libro_contabilidad' ");
     // var_dump($db);
      $db->setFetchMode(PDO::FETCH_ASSOC);
      return $db->fetchAll();
    }
    public function metDetalleReporte2($mes, $anio, $num_libro_contabilidad, $pk_num_cuenta)
    {
        $db= $this->_db->query("SELECT
                                  sum(a.num_debe) AS num_debe,
                                  sum(a.num_haber) AS num_haber
                                FROM cb_c001_voucher_det a
                                     INNER JOIN cb_b001_voucher b on (b.pk_num_voucher_mast=a.fk_cbb001_num_voucher_mast and
                                                                      b.ind_mes=a.ind_mes and
                                                                      b.ind_anio=a.fec_anio)
                               WHERE a.ind_mes <='$mes'  and
                                     a.fec_anio='$anio' and
                                     a.ind_estatus='MA' and
                                     a.fk_cbb004_num_cuenta='$pk_num_cuenta' and
                                     a.fk_cbb003_num_libro_contabilidad='$num_libro_contabilidad' ");
        //var_dump($db);
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }


}
