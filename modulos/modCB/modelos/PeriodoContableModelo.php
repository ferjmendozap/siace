<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class PeriodoContableModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();

        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /*
    * Obtengo datos de tabla para cargar listado de registros
    */
    public function metListar()
    {
        $db = $this->_db->query("SELECT * FROM cb_c005_control_cierre_mensual");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /*
    * Ejecuta el proceso de registro del nuevo período contable
    */
    public function metNuevo($data)
    {
        $this->_db->beginTransaction();

        //consulta de previa existencia del periodo a crear
        $s_con= $this->_db->query("SELECT *
                                     FROM cb_c005_control_cierre_mensual
                                    WHERE ind_mes= '$data[ind_mes]' and
                                          fec_anio= '$data[fec_anio]' and
                                          ind_tipo_registro= '$data[ind_tipo_registro]' ");
        $s_con->setFetchMode(PDO::FETCH_ASSOC);
        $resultado= $s_con->rowCount();

        if ($resultado!=0) {
           $id=0;
        }else{

           $sql ="INSERT INTO cb_c005_control_cierre_mensual
                          SET
                              ind_tipo_registro= '$data[ind_tipo_registro]',
                              ind_mes= '$data[ind_mes]',
                              fec_anio= '$data[fec_anio]',
                              ind_estatus= '$data[ind_estatus]',
                              fk_cbb002_cod_libro_contable= '$data[pk_num_libro_contable]',
                              fk_a018_num_seguridad_usuario= '$this->idUsuario',
                              fec_ultima_modificacion= NOW() ";
           $db = $this->_db->prepare($sql);
           $db->execute();

           $id = $this->_db->lastInsertId();

        }

         ##  Consigna una transacción
         $this->_db->commit();

         return $id;
    }

    /*
    * Obtengo datos de tabla con búsqueda específica de período contable
    */
    public function metMostrar($id)
    {
        $db = $this->_db->query("SELECT * FROM cb_c005_control_cierre_mensual WHERE pk_num_control_cierre='$id'");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetch();
    }

    /*
    * Ejecuta el proceso de editado de un período contable
    */
    public function metEditar($data)
    {
        $this->_db->beginTransaction();

             $sql ="UPDATE cb_c005_control_cierre_mensual
                       SET
                           ind_tipo_registro= '$data[ind_tipo_registro]',
                           ind_mes= '$data[ind_mes]',
                           fec_anio= '$data[fec_anio]',
                           ind_estatus= '$data[ind_estatus]',
                           fk_cbb002_cod_libro_contable= '$data[pk_num_libro_contable]',
                           fk_a018_num_seguridad_usuario= '$this->idUsuario',
                           fec_ultima_modificacion= NOW()
                     WHERE
                           pk_num_control_cierre= '$data[pk_num_control_cierre]'";

         $db = $this->_db->prepare($sql);
         $db->execute();

         ##  Consigna una transacción
         $this->_db->commit();
    }

    // NO UTILIZADO
    public function metEliminar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql = "DELETE FROM cb_c005_control_cierre_mensual WHERE pk_num_control_cierre= '$data[pk_num_control_cierre]'";
        $db = $this->_db->prepare($sql);
        $db->execute();

        ##  Consigna una transacción
        $this->_db->commit();
    }

    /*
    * Ejecuta el proceso de cierre período contable
    */
    public function metCerrarPeriodo($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

         $sql ="UPDATE cb_c005_control_cierre_mensual
                   SET
                      ind_estatus= 'C',
                      fk_a018_num_seguridad_usuario= '$this->idUsuario',
                      fec_ultima_modificacion= NOW()
                WHERE
                      pk_num_control_cierre= '$data[pk_num_control_cierre]' ";
         $db = $this->_db->prepare($sql);
         $db->execute();

         ##  Consigna una transacción
         $this->_db->commit();
    }

    /*
    * Ejecuta el proceso de abrir período contable
    */
    public function metAbrirPeriodo($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

         $sql ="UPDATE cb_c005_control_cierre_mensual
                   SET
                      ind_estatus= 'A',
                      fk_a018_num_seguridad_usuario= '$this->idUsuario',
                      fec_ultima_modificacion= NOW()
                WHERE
                      pk_num_control_cierre= '$data[pk_num_control_cierre]' ";
         $db = $this->_db->prepare($sql);
         $db->execute();

         ##  Consigna una transacción
         $this->_db->commit();
    }

}
