<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: index
 * PROCESO: index del sistema
 * PROGRAMADORES:
 * | # |          NOMBRES Y APELLIDOS              |               CORREO               |          TELEFONO              |
 * | 1 |          Pablo Rodríguez                  |prodriguez@contradeltamacuro.gob.ve |         0426-7992529           |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |          PROGRAMADOR                  |          FECHA          |       VERSION      |
 * |               #1                      |        21-07-2015       |         1.0        |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
class CentroCostoModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->idUsuario = Session::metObtener('idUsuario');
    }

    /**
    * Permite obtener información a mostrar en listado
    */
    public function metListar()
    {
        $db = $this->_db->query("SELECT * FROM a023_centro_costo");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

    /**
    *Permite obtener información a mostrar al momento de editar y ver un registro
    */
    public function metMostrar($id)
    {
        $db = $this->_db->query("SELECT a.*,
                                        b.ind_nombre1 as nombre,
                                        b.ind_apellido1 as apellido,
                                        c.fk_a024_num_grupo_centro_costo as fk_grupo_centro_costo
                                   FROM a023_centro_costo a
                                        inner join a003_persona b on (b.pk_num_persona=a.fk_a003_num_persona)
                                        inner join a025_subgrupo_centro_costo c on (c.pk_num_subgrupo_centro_costo=a.fk_a025_num_subgrupo_centro_costo)
                                  WHERE a.pk_num_centro_costo= '$id' ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetch();
    }

    /**
    *Ejecuta el proceso de inserción de los datos en tabla para el nuevo registro
    */
    public function metNuevo($data)
    {
        $this->_db->beginTransaction();

          $sql= "INSERT INTO a023_centro_costo
                         SET
                             ind_descripcion_centro_costo= '$data[ind_descripcion_centro_costo]',
                             ind_abreviatura= '$data[ind_abreviatura]',
                             num_estatus= '$data[num_estatus]',
                             fk_a004_num_dependencia= '$data[fk_a004_num_dependencia]',
                             fk_a003_num_persona= '$data[pk_num_persona]',
                             fk_a025_num_subgrupo_centro_costo= '$data[fk_a025_num_subgrupo_centro_costo]',
                             num_flag_administrativo= '$data[num_flag_administrativo]',
                             num_flag_ventas= '$data[num_flag_ventas]',
                             num_flag_financiera= '$data[num_flag_financiera]',
                             num_flag_produccion= '$data[num_flag_produccion]',
                             num_flag_centro_ingresos= '$data[num_flag_centro_ingresos]',
                             fk_a018_num_seguridad_usuario= '$this->idUsuario',
                             fec_ultima_modificacion= NOW(),
                             cod_centro_costo= '$data[cod_centro_costo]' ";
          $db = $this->_db->prepare($sql);
          $db->execute();

          $id = $this->_db->lastInsertId();

            ##  Consigna una transacción
        $this->_db->commit();

        return $id;
    }

    /**
    *Ejecuta el proceso de cambio de datos en tabla de un registro
    */
    public function metEditar($data)
    {
        ##  Inicia una transacción
        $this->_db->beginTransaction();

            $modificar=""; $mensaje="";

            $sql= $this->_db->query("SELECT DISTINCT TABLE_NAME
                                       FROM information_schema.COLUMNS
                                      WHERE TABLE_SCHEMA= 'siace' AND
                                            COLUMN_NAME LIKE '%fk_a023_num_centro_costo%' ");
            $sql->setFetchMode(PDO::FETCH_ASSOC);
            $field= $sql->fetchAll();

            foreach ($field as $f) {

               $sqla= $this->_db->query("SELECT *
                                           FROM $f[TABLE_NAME]
                                          WHERE fk_a023_num_centro_costo= '$data[pk_num_centro_costo]' ");
               $sqla->setFetchMode(PDO::FETCH_ASSOC);
               $field_a= $sqla->fetchAll();

               foreach ($field_a as $f_a) {
                  if (isset($f_a['fk_a023_num_centro_costo'])) {
                      $mensaje= "Esta siendo utilizado";
                      $swal= "Registro no puede ser Modificado!";
                      $status= "nomodificar";
                  }
               }
            }

            if ($mensaje==""){
                $mensaje= "Registro actualizado exitosamente";
                $modificar= "modificar";
                $swal= "Registro Guardado!";
            }

            if ($modificar=="modificar") {

                // actualizo grupo centro costo
                $sql= "UPDATE a023_centro_costo
                          SET
                              ind_descripcion_centro_costo= '$data[ind_descripcion_centro_costo]',
                              ind_abreviatura= '$data[ind_abreviatura]',
                              num_estatus= '$data[num_estatus]',
                              fk_a004_num_dependencia= '$data[fk_a004_num_dependencia]',
                              fk_a003_num_persona= '$data[pk_num_persona]',
                              fk_a025_num_subgrupo_centro_costo= '$data[fk_a025_num_subgrupo_centro_costo]',
                              num_flag_administrativo= '$data[num_flag_administrativo]',
                              num_flag_ventas= '$data[num_flag_ventas]',
                              num_flag_financiera= '$data[num_flag_financiera]',
                              num_flag_produccion= '$data[num_flag_produccion]',
                              num_flag_centro_ingresos= '$data[num_flag_centro_ingresos]',
                              fk_a018_num_seguridad_usuario = '$this->idUsuario',
                              fec_ultima_modificacion = NOW(),
                              cod_centro_costo= '$data[cod_centro_costo]'
                        WHERE
                              pk_num_centro_costo= '$data[pk_num_centro_costo]' ";
                $db= $this->_db->prepare($sql);
                $db->execute();

                $status= "modificar";

            }

         ##  Consigna una transacción
         $this->_db->commit();

         return $mensaje.'|'.$swal.'|'.$status;
    }

    /**
    *Ejecuta la acción de eliminar un registro
    */
    public function metEliminar($data)
    {
        ## valor y mensaje: Variables utilizadas para manejo de mensajes de confirmación o no de la acción
        $valor=""; $mensaje="";

        ##  Inicia una transacción
        $this->_db->beginTransaction();

        $sql= $this->_db->query("SELECT DISTINCT TABLE_NAME
                                       FROM information_schema.COLUMNS
                                      WHERE TABLE_SCHEMA= 'siace' AND
                                            COLUMN_NAME LIKE '%fk_a023_num_centro_costo%' ");
        $sql->setFetchMode(PDO::FETCH_ASSOC);
        $field= $sql->fetchAll();

        foreach ($field as $f) {

           $sqla= $this->_db->query("SELECT *
                                       FROM $f[TABLE_NAME]
                                      WHERE fk_a023_num_centro_costo= '$data[pk_num_centro_costo]' ");
           $sqla->setFetchMode(PDO::FETCH_ASSOC);
           $field_a= $sqla->fetchAll();

           foreach ($field_a as $f_a) {
              if (isset($f_a['fk_a023_num_centro_costo'])) {
                  $mensaje= "No puede ser Eliminado";
                  $valor= '2';
              }
           }
        }

        if ($valor==""){

            $sql = "DELETE FROM a023_centro_costo WHERE pk_num_centro_costo= '$data[pk_num_centro_costo]' ";
            $db = $this->_db->prepare($sql);
            $db->execute();

            $mensaje= "Registro eliminado exitosamente";
            $valor= '1';
        }

        ##  Consigna una transacción
        $this->_db->commit();

        return $mensaje.'|'.$valor;
    }

    /**
    *Realiza consulta a Tabla para obtener información relacionada a personas
    */
    public function metMostrar_persona()
    {
        $db = $this->_db->query("SELECT a.*
                                   FROM a003_persona a
                                        INNER JOIN rh_b001_empleado b ON (b.fk_a003_num_persona=a.pk_num_persona AND b.num_estatus='1')
                              ");
        $db->setFetchMode(PDO::FETCH_ASSOC);
        return $db->fetchAll();
    }

}
