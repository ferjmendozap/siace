<?php
/*****************************************************************************************************************************************
 * DEV: CONTRALORIA DE ESTADOS
 * PROYECTO: SISTEMA INTEGRAL ADMINISTRATIVO DE CONTRALORIAS DE ESTADOS
 * MODULO: Control de Visita
 * PROCESO: Listar visitas
 * PROGRAMADORES:
 * | # |NOMBRES Y APELLIDOS                        |CORREO                              |TELEFONO                        |
 * | 1 |Sergio Zabaleta                            |zsergio01@gmail.com                 |0414-3638131                    |
 * |   |                                           |                                    |                                |
 * |___|___________________________________________|____________________________________|________________________________|
 *
 * VERSION
 *
 * |PROGRAMADOR                            |FECHA                    |VERSION             |
 * |#1                                     |08/12/2015               |1.0                 |
 * |                                       |                         |                    |
 * |_______________________________________|_________________________|____________________|
 *
 *****************************************************************************************************************************************/
require_once RUTA_MODELO . 'miscelaneoModelo.php';
class consultarArchivoModelo extends Modelo
{

    public $atMiscelaneoModelo;
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atMiscelaneoModelo=new miscelaneoModelo();
    }


    public function metListarEstante()
    {
        $estante = $this->_db->query("
                    SELECT
                      *
                    FROM
                      ad_c003_estante
                    
        ");
        $estante->setFetchMode(PDO::FETCH_ASSOC);
        return $estante->fetchAll();
    }

    public function metBuscarArchivo($documento = false, $descripcion = false, $tramo = false, $estante = false, $resumen = false, $desde = false, $hasta = false)
    {
        $filtro = '';
        if($documento){
            $filtro .= "AND carpeta.fk_a006_miscelaneo_documento = '$documento'";
        }

        if($descripcion){
            $filtro .= "AND a.ind_descripcion  LIKE '%$descripcion%'";
        }
        if($resumen){
            $filtro .= "AND a.ind_resumen  LIKE '%$resumen%'";
        }
        if($tramo){
            $filtro .= "AND carpeta.fk_a006_miscelaneo_tramo = '$tramo'";
        }
        if($estante){
            $filtro .= "AND d.pk_num_estante = '$estante'";
        }

        $verArchivo = $this->_db->query(
            "SELECT
                          a.*,
                          b.pk_num_almacen,
                          b.ind_descripcion_almacen,
                          c.pk_num_pasillo,
                          c.ind_pasillo,
                          d.pk_num_estante,
                          d.ind_descripcion_estante,
                          e.pk_num_caja,
                          e.ind_descripcion_caja,
                          g.pk_num_dependencia,
                          g.ind_dependencia,
                          carpeta.pk_num_carpeta,
                          (documento.ind_nombre_detalle) AS documento,
                          (tramo.ind_nombre_detalle) AS tramo,
                          a3.ind_nombre_archivo
                        FROM
                          ad_b001_registro_documento AS a
                        INNER JOIN
                          ad_c009_carpeta AS carpeta ON a.fk_adc009_num_carpeta = carpeta.pk_num_carpeta
                        INNER JOIN
                          ad_c003_estante AS d ON carpeta.fk_adc003_num_estante = d.pk_num_estante
                        INNER JOIN
                          ad_c002_pasillo AS c ON d.fk_adc002_num_pasillo = c.pk_num_pasillo
                        INNER JOIN
                          ad_c001_almacen AS b ON c.fk_adc001_num_almacen = b.pk_num_almacen
                        LEFT JOIN
                          ad_c004_caja AS e ON e.pk_num_caja = a.fk_adc004_num_caja
                        INNER JOIN
                          a004_dependencia AS g ON carpeta.fk_a004_num_dependencia = g.pk_num_dependencia
                        INNER JOIN
                          a006_miscelaneo_detalle AS documento ON carpeta.fk_a006_miscelaneo_documento = documento.pk_num_miscelaneo_detalle
                        INNER JOIN
                          a006_miscelaneo_detalle AS tramo ON carpeta.fk_a006_miscelaneo_tramo =  tramo.pk_num_miscelaneo_detalle
                        LEFT JOIN
                          ad_c008_archivo a3 ON a.pk_num_registro_documento = a3.fk_adb001_num_registro_documento
                        WHERE 1 $filtro
                        ");
        $verArchivo->setFetchMode(PDO::FETCH_ASSOC);

        return $verArchivo->fetchAll();

    }

    public function metListaPersona()
    {
        $menu = $this->_db->query("
            SELECT
              a003_persona.*
            FROM
              a003_persona
            INNER JOIN cv_b001_registro_visita ON a003_persona.pk_num_persona = cv_b001_registro_visita.fk_a003_num_persona
         ");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metListarEntes()
    {
        $menu = $this->_db->query("
            SELECT
            a039_ente.*
            FROM
             a039_ente
            INNER JOIN cv_b001_registro_visita ON a039_ente.pk_num_ente = cv_b001_registro_visita.fk_a039_num_ente
            ");
        $menu->setFetchMode(PDO::FETCH_ASSOC);
        return $menu->fetchAll();
    }

    public function metListarDependencias()
    {
        $organismo = $this->_db->query(
            "SELECT * FROM a004_dependencia"
        );
        $organismo->setFetchMode(PDO::FETCH_ASSOC);
        return $organismo->fetchAll();
    }

    public function metMostrarVisita($idPersona=false)
    {
        if ($idPersona) {
            $were ="WHERE cv_b001_registro_visita.fk_a003_num_persona='$idPersona' and cv_b001_registro_visita.fec_fecha_salida IS NULL";
        }
        else{
            $were='';
        }
        $visita = $this->_db->query("
          SELECT
            cv_b001_registro_visita.*,
            a018_seguridad_usuario.ind_usuario,
            a003_persona.ind_foto,
            a003_persona.ind_nombre1,
            a003_persona.ind_apellido1,
            personaAnfitrion.ind_apellido1 AS personaAnfitrionApellido,
            personaAnfitrion.ind_nombre1 AS personaAnfitrionNombre,
            motivo.ind_nombre_detalle AS ind_motivo
          FROM
            cv_b001_registro_visita
          INNER JOIN
            a006_miscelaneo_detalle AS motivo ON motivo.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_motivo_visita
          INNER JOIN
            a003_persona ON a003_persona.pk_num_persona = cv_b001_registro_visita.fk_a003_num_persona
          INNER JOIN
            rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = cv_b001_registro_visita.fk_rh_b001_empleado_anfitrion
          INNER JOIN
            a003_persona AS personaAnfitrion ON personaAnfitrion.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
          INNER JOIN
            a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cv_b001_registro_visita.fk_a018_num_seguridad_usuario
          LEFT JOIN
            a039_ente ON a039_ente.pk_num_ente = cv_b001_registro_visita.fk_a039_num_ente
          LEFT JOIN
            a006_miscelaneo_detalle AS destino ON destino.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino
           $were
        ");
       // var_dump($visita);
        $visita->setFetchMode(PDO::FETCH_ASSOC);
        if($idPersona){
            return $visita->fetch();
        }else{
            return $visita->fetchAll();
        }
    }

    public function metConsultarVisita($idVisita=false)
    {
        $visita = $this->_db->query("
          
          SELECT
              cv_b001_registro_visita.*,
              a018_seguridad_usuario.ind_usuario,
              a003_persona.ind_foto,
              a003_persona.ind_cedula_documento,
              a003_persona.ind_nombre1,
              a003_persona.ind_apellido1,
              personaAnfitrion.ind_apellido1 AS personaAnfitrionApellido,
              personaAnfitrion.ind_nombre1 AS personaAnfitrionNombre,
              motivo.ind_nombre_detalle AS ind_motivo
            FROM
              cv_b001_registro_visita
            INNER JOIN
              a006_miscelaneo_detalle AS motivo ON motivo.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_motivo_visita
            INNER JOIN
              a003_persona ON a003_persona.pk_num_persona = cv_b001_registro_visita.fk_a003_num_persona
            INNER JOIN
              a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = cv_b001_registro_visita.fk_a018_num_seguridad_usuario
            LEFT JOIN
              rh_b001_empleado ON rh_b001_empleado.pk_num_empleado = cv_b001_registro_visita.fk_rh_b001_empleado_anfitrion
            LEFT JOIN
              a003_persona AS personaAnfitrion ON personaAnfitrion.pk_num_persona = rh_b001_empleado.fk_a003_num_persona
            LEFT JOIN
              a039_ente ON a039_ente.pk_num_ente = cv_b001_registro_visita.fk_a039_num_ente
            LEFT JOIN
              a006_miscelaneo_detalle AS destino ON destino.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino
              
          WHERE
          cv_b001_registro_visita.pk_num_registro_visita='$idVisita'
        ");
        $visita->setFetchMode(PDO::FETCH_ASSOC);
        return $visita->fetch();

    }

    /**
     * @param bool|false $organismo
     * @param bool|false $dependencia
     * @param bool|false $visitante
     * @param bool|false $desde
     * @param bool|false $hasta
     * @param bool|false $motivo
     * @return array
     */
    public function metBuscarVisita($ente = false ,$dependencia = false, $visitante = false,
                                    $desde = false, $hasta = false, $motivo = false)
    {
        $filtro="";
        if ($ente) {
            $filtro .= "AND cv_b001_registro_visita.fk_a039_num_ente = '$ente' ";
        }
        if ($dependencia) {
            $pos = strpos($dependencia, 'D');
            if ($pos === false) {
                $destino = preg_replace('/[^0-9 .,]/', '', $dependencia);
                $filtro .= "AND cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino = '$destino' ";
            } else {
                $dep = preg_replace('/[^0-9 .,]/', '', $dependencia);
                $filtro .= "AND  cv_b001_registro_visita.fk_a004_num_dependencia = '$dep' ";
            }
        }
        if ($visitante) {
            $filtro .= "AND a003_persona.pk_num_persona = '$visitante' ";
        }
        if ($desde and $hasta){
            $filtro .= "AND cv_b001_registro_visita.fec_fecha_entrada >= '$desde 00:00:00'
                      AND cv_b001_registro_visita.fec_fecha_entrada <= '$hasta 23:59:59' ";
        }
        if ($motivo) {
            $filtro .= "AND cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_motivo_visita = '$motivo' ";
        }
        $visita = $this->_db->query("
          SELECT
            cv_b001_registro_visita.*,
            a018_seguridad_usuario.ind_usuario,
            a003_persona.ind_foto,
            a003_persona.ind_nombre1,
            a003_persona.ind_apellido1,
            a003_persona.ind_cedula_documento,
            motivo.ind_nombre_detalle AS ind_motivo,
            concat_ws(' ',destino.ind_nombre_detalle,a004_dependencia.ind_dependencia)
             AS ind_destino
          FROM
            cv_b001_registro_visita
          INNER JOIN
            a003_persona ON a003_persona.pk_num_persona = cv_b001_registro_visita.fk_a003_num_persona
          INNER JOIN
            a018_seguridad_usuario ON a018_seguridad_usuario.pk_num_seguridad_usuario = a003_persona.fk_a018_num_seguridad_usuario
          INNER JOIN
            a006_miscelaneo_detalle AS motivo ON motivo.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_motivo_visita
          LEFT JOIN
            a006_miscelaneo_detalle AS destino ON destino.pk_num_miscelaneo_detalle = cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino
          LEFT JOIN 
            a004_dependencia ON a004_dependencia.pk_num_dependencia = cv_b001_registro_visita.fk_a004_num_dependencia
          LEFT JOIN
            a039_ente ON a039_ente.pk_num_ente = cv_b001_registro_visita.fk_a039_num_ente
          WHERE 1 $filtro
        ");
//        var_dump($dependencia);
        $visita->setFetchMode(PDO::FETCH_ASSOC);
        return $visita->fetchAll();

    }

    public function metListarVisita()
    {
        $registro = $this->_db->query(
            "SELECT * FROM cv_b001_registro_visita"
        );
        $registro->setFetchMode(PDO::FETCH_ASSOC);
        return $registro->fetchAll();
    }

    public function metListarDependenciaVisita()
    {
        $registro = $this->_db->query(
            "SELECT * 
            FROM a004_dependencia
            INNER JOIN cv_b001_registro_visita ON cv_b001_registro_visita.fk_a004_num_dependencia = a004_dependencia.pk_num_dependencia
            GROUP BY a004_dependencia.pk_num_dependencia"
        );
        $registro->setFetchMode(PDO::FETCH_ASSOC);
        return $registro->fetchAll();
    }

    public function metListarDestinoVisita()
    {
        $registro = $this->_db->query(
            "SELECT * 
            FROM a006_miscelaneo_detalle
            INNER JOIN cv_b001_registro_visita ON cv_b001_registro_visita.fk_a006_num_miscelaneo_detalle_destino = a006_miscelaneo_detalle.pk_num_miscelaneo_detalle
            GROUP BY a006_miscelaneo_detalle.pk_num_miscelaneo_detalle"
        );
        $registro->setFetchMode(PDO::FETCH_ASSOC);
        return $registro->fetchAll();
    }



}
