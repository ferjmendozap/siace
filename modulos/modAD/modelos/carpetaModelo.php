<?php
require_once RUTA_MODELO . 'miscelaneoModelo.php';
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de cajas de archivos
class carpetaModelo extends Modelo
{
    public function __construct()
    {
        parent::__construct();
        $this->atMiscelaneoModelo = new miscelaneoModelo();
        $this->atIdUsuario = Session::metObtener('idUsuario');
        $this->atIdEmpleado = Session::metObtener('idEmpleado');
    }

    public function metListarCarpeta()
    {
        $listarCarpeta =  $this->_db->query(
            "SELECT
                          a.pk_num_carpeta,
                          a.ind_descripcion_carpeta,
                          a.fec_anio,
                          b.ind_descripcion_estante,
                          c.ind_dependencia,
                          d.ind_nombre_detalle,
                          e.ind_pasillo,
                          f.ind_descripcion_almacen
                        FROM
                          ad_c009_carpeta AS a,
                          ad_c003_estante AS b,
                          a004_dependencia AS c,
                          a006_miscelaneo_detalle AS d,
                          ad_c002_pasillo AS e,
                          ad_c001_almacen AS f
                        WHERE
                          a.fk_adc003_num_estante = b.pk_num_estante AND 
                          a.fk_a004_num_dependencia = c.pk_num_dependencia AND 
                          a.fk_a006_miscelaneo_documento = d.pk_num_miscelaneo_detalle  AND 
                          b.fk_adc002_num_pasillo = e.pk_num_pasillo AND 
                          e.fk_adc001_num_almacen = f.pk_num_almacen
"
        );
        $listarCarpeta->setFetchMode(PDO::FETCH_ASSOC);
        return $listarCarpeta->fetchAll();
    }

    public function metVerCarpeta($pkNumCarpeta, $metodo)
    {
        $verCarpeta =  $this->_db->query(
            "SELECT
                          a.pk_num_carpeta,
                          a.ind_descripcion_carpeta,
                          a.fec_anio,
                          a.fk_a006_miscelaneo_documento,
                          a.fk_a006_miscelaneo_tramo,
                          b.pk_num_estante,
                          b.ind_descripcion_estante,
                          c1.pk_num_dependencia,
                          c1.ind_dependencia,
                          d.pk_num_miscelaneo_detalle AS pk_num_miscelaneo_detalle_doc,
                          j.pk_num_miscelaneo_detalle AS pk_num_miscelaneo_detalle_tra,
                          j.ind_nombre_detalle,
                          d.ind_nombre_detalle,
                          e.pk_num_pasillo,
                          e.ind_pasillo,
                          f.ind_descripcion_almacen,
                          f.pk_num_almacen,
                          DATE_FORMAT(
                            a.fec_ultima_modificacion,
                            '%d/%m/%Y %H:%i:%S'
                          ) AS fecha,
                          i.ind_nombre1,
                          i.ind_apellido1,
                          k.pk_num_caja,
                          k.ind_descripcion_caja
                        FROM
                          ad_c009_carpeta AS a
                          INNER JOIN ad_c003_estante AS b ON a.fk_adc003_num_estante = b.pk_num_estante
                          INNER JOIN a004_dependencia AS c1 ON a.fk_a004_num_dependencia = c1.pk_num_dependencia
                          INNER JOIN a006_miscelaneo_detalle AS d ON a.fk_a006_miscelaneo_documento = d.pk_num_miscelaneo_detalle
                          INNER JOIN ad_c002_pasillo AS e ON b.fk_adc002_num_pasillo = e.pk_num_pasillo
                          INNER JOIN ad_c001_almacen AS f ON e.fk_adc001_num_almacen = f.pk_num_almacen
                          INNER JOIN a018_seguridad_usuario AS g ON a.fk_a018_num_seguridad_usuario = g.pk_num_seguridad_usuario
                          INNER JOIN rh_b001_empleado AS h ON g.fk_rhb001_num_empleado = h.pk_num_empleado
                          INNER JOIN a003_persona AS i ON h.fk_a003_num_persona = i.pk_num_persona
                          INNER JOIN a006_miscelaneo_detalle AS j ON a.fk_a006_miscelaneo_tramo = j.pk_num_miscelaneo_detalle
                          LEFT JOIN ad_c004_caja AS k ON k.pk_num_caja = a.fk_adc004_num_caja 
                        WHERE
                          a.pk_num_carpeta = '$pkNumCarpeta' 
                          "
        );

        $verCarpeta->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $verCarpeta->fetch();
        else return $verCarpeta->fetchAll();
    }

    public function metEliminarCarpeta($pkNumCarpeta)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "DELETE FROM ad_c009_carpeta WHERE pk_num_carpeta = '$pkNumCarpeta'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metListarCaja($pkNumDependencia)
    {
        $obtenerUsuario = $this->_db->query(
            "
            SELECT * FROM ad_c004_caja WHERE fk_a004_num_dependencia = '$pkNumDependencia'
            "
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetchAll();
    }

    public function metListarDependencia()
    {
        $listarDependencia =  $this->_db->query(
            "select pk_num_dependencia, ind_dependencia from a004_dependencia"
        );
        $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $listarDependencia->fetchAll();
    }

    public function metVerDependencia($pkNumCarpeta)
    {
        $verDependencia = $this->_db->query(
            "select 
                        b.ind_dependencia 
                      from 
                        ad_c009_carpeta as a, 
                        a004_dependencia as b 
                      where 
                        a.fk_a004_num_dependencia=b.pk_num_dependencia and a.pk_num_carpeta='$pkNumCarpeta'"
        );
        $verDependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $verDependencia->fetch();
    }


    public function metVisualizarMiscelaneo($pk_tipo_documento)
    {
        $visualizarDocumento = $this->_db->query(
            "select 
                  a.ind_nombre_detalle 
                  from 
                  a006_miscelaneo_detalle as a
                  where 
                  a.pk_num_miscelaneo_detalle='$pk_tipo_documento'"
        );
        $visualizarDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $visualizarDocumento->fetch();
    }
    public function metVisualizarDependencia($pk_num_dependencia)
    {
        $visualizarDependencia = $this->_db->query(
            "select b.ind_dependencia from ad_c009_carpeta as a, a004_dependencia as b where pk_num_dependencia='$pk_num_dependencia'"
        );
        $visualizarDependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $visualizarDependencia->fetch();
    }

    public function metListarDocumento()
    {
        $listarDocumento = $this->_db->query(
            "select pk_num_documento, ind_descripcion_documento from ad_c005_tipo_documento"
        );
        $listarDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $listarDocumento->fetchAll();
    }

    public function metVerDocumento($pkNumCarpeta)
    {
        $verDocumento = $this->_db->query(
            "select b.ind_descripcion_documento from ad_c009_carpeta as a, ad_c005_tipo_documento as b where a.pk_num_carpeta='$pkNumCarpeta' and a.fk_adc005_num_documento=b.pk_num_documento"
        );
        $verDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $verDocumento->fetch();
    }

    public function metVisualizarDocumento($pk_num_documento)
    {
        $visualizarDocumento = $this->_db->query(
            "select ind_descripcion_documento from ad_c005_tipo_documento where pk_num_documento='$pk_num_documento'"
        );
        $visualizarDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $visualizarDocumento->fetch();
    }

    public function metBuscarDependenciaCaja($pk_num_dependencia)
    {
        $visualizarDocumento = $this->_db->query(
            "select 
                        *
                      from 
                        ad_c004_caja
                      WHERE
                       fk_a004_num_dependencia='$pk_num_dependencia'"
        );
        $visualizarDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $visualizarDocumento->fetchAll();
    }

    public function metBuscarPasillo($pkNumAlmacen)
    {
        $buscarPasillo =  $this->_db->query(
            "SELECT pk_num_pasillo, ind_pasillo from ad_c002_pasillo where fk_adc001_num_almacen = '$pkNumAlmacen' "
        );
        $buscarPasillo->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarPasillo->fetchAll();
    }

    public function metVerPasillo($pk_num_pasillo)
    {
        $verPasillo =  $this->_db->query(
            "SELECT pk_num_pasillo, ind_pasillo from ad_c002_pasillo where pk_num_pasillo='$pk_num_pasillo' "
        );
        $verPasillo->setFetchMode(PDO::FETCH_ASSOC);
        return $verPasillo->fetch();
    }

    public function metListarAlmacen()
    {
        $listarAlmacen =  $this->_db->query(
            "SELECT pk_num_almacen, ind_descripcion_almacen from ad_c001_almacen"
        );
        $listarAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        return $listarAlmacen->fetchAll();
    }

    public function metBuscarAlmacen($pkNumAlmacen)
    {
        $listarAlmacen =  $this->_db->query(
            "SELECT pk_num_almacen, ind_descripcion_almacen from ad_c001_almacen where pk_num_almacen='$pkNumAlmacen'"
        );
        $listarAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        return $listarAlmacen->fetch();
    }

    public function metBuscarEstante($pkNumPasillo)
    {
        $buscarEstante = $this->_db->query(
            "select pk_num_estante, ind_descripcion_estante from ad_c003_estante where fk_adc002_num_pasillo='$pkNumPasillo'"
        );
        $buscarEstante->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarEstante->fetchAll();
    }

    public function metVerEstante($pk_num_estante)
    {
        $verEstante = $this->_db->query(
            "select pk_num_estante, ind_descripcion_estante from ad_c003_estante where pk_num_estante='$pk_num_estante'"
        );
        $verEstante->setFetchMode(PDO::FETCH_ASSOC);
        return $verEstante->fetch();
    }

    public function metGuardarCarpeta($pk_num_estante, $fec_anio, $ind_descripcion_carpeta, $pk_num_dependencia, $pk_num_documento, $pk_num_tramo, $pk_num_caja )
    {
        $this->_db->beginTransaction();
        $nuevaCarpeta = $this->_db->prepare("
                  INSERT INTO
                    ad_c009_carpeta
                  SET
                   ind_descripcion_carpeta=:ind_descripcion_carpeta,
                   fec_anio=:fec_anio,
                   fk_a004_num_dependencia=:fk_a004_num_dependencia,
                   fk_adc003_num_estante=:fk_adc003_num_estante,
                   fk_a006_miscelaneo_documento=:fk_a006_miscelaneo_documento,
                   fk_a006_miscelaneo_tramo=:fk_a006_miscelaneo_tramo,
                   fk_adc004_num_caja=:fk_adc004_num_caja,
                   fec_ultima_modificacion=NOW(),
                   fk_a018_num_seguridad_usuario='$this->atIdUsuario'
        ");
        $nuevaCarpeta->execute(array(
            'ind_descripcion_carpeta' => $ind_descripcion_carpeta,
            'fec_anio' => $fec_anio,
            'fk_a004_num_dependencia' => $pk_num_dependencia,
            'fk_adc003_num_estante' => $pk_num_estante,
            'fk_a006_miscelaneo_documento' => $pk_num_documento,
            'fk_a006_miscelaneo_tramo' => $pk_num_tramo,
            'fk_adc004_num_caja' => $pk_num_caja
        ));
        $idRegistro = $this->_db->lastInsertId();
        $error = $nuevaCarpeta->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $idRegistro;
        }

    }

    public function metEditarCarpeta($pk_num_estante,$fec_anio, $ind_descripcion_carpeta, $pk_num_dependencia, $pk_num_documento, $pk_num_tramo, $pk_num_caja, $pkNumCarpeta)
    {
        $this->_db->beginTransaction();
        $actualizar = $this->_db->prepare(
            "UPDATE
                       ad_c009_carpeta
                      SET
                       ind_descripcion_carpeta=:ind_descripcion_carpeta,
                       fec_anio=:fec_anio,
                       fk_a004_num_dependencia=:fk_a004_num_dependencia,
                       fk_adc003_num_estante=:fk_adc003_num_estante,
                       fk_a006_miscelaneo_documento=:fk_a006_miscelaneo_documento,
                       fk_a006_miscelaneo_tramo=:fk_a006_miscelaneo_tramo,
                       fk_adc004_num_caja=:fk_adc004_num_caja,
                       fec_ultima_modificacion=NOW(),                 
                       fk_a018_num_seguridad_usuario='$this->atIdUsuario'
                      WHERE
                       pk_num_carpeta ='$pkNumCarpeta'
        ");
        $actualizar->execute(array(
            'ind_descripcion_carpeta' => $ind_descripcion_carpeta,
            'fec_anio' => $fec_anio,
            'fk_a004_num_dependencia' => $pk_num_dependencia,
            'fk_adc003_num_estante' => $pk_num_estante,
            'fk_a006_miscelaneo_documento' => $pk_num_documento,
            'fk_a006_miscelaneo_tramo' => $pk_num_tramo,
            'fk_adc004_num_caja' => $pk_num_caja
        ));
        $error = $actualizar->errorInfo();

        if (!empty($error[1]) && !empty($error[2])) {
            $this->_db->rollBack();
            return $error;
        } else {
            $this->_db->commit();
            return $pkNumCarpeta;
        }


    }
    
}// fin de la clase
?>
