<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de estantes
class estanteModelo extends Modelo 
{
    // Constructor del modelo
    public function __construct()
    {
        parent::__construct();
    }

    public function metListarEstante()
    {
        $listarEstante =  $this->_db->query(
            "SELECT a.pk_num_estante,a.ind_descripcion_estante, b.ind_pasillo, c.ind_descripcion_almacen from ad_c003_estante as a, ad_c002_pasillo as b, ad_c001_almacen as c where a.fk_adc002_num_pasillo=b.pk_num_pasillo and b.fk_adc001_num_almacen=c.pk_num_almacen"
        );
        $listarEstante->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEstante->fetchAll();
    }

    public function metListarAlmacen()
    {
        $listarAlmacen =  $this->_db->query(
            "SELECT pk_num_almacen, ind_descripcion_almacen from ad_c001_almacen"
        );
        $listarAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        return $listarAlmacen->fetchAll();
    }

    public function metBuscarPasillo($pkNumAlmacen)
    {
        $buscarPasillo =  $this->_db->query(
            "SELECT pk_num_pasillo, ind_pasillo from ad_c002_pasillo where fk_adc001_num_almacen = '$pkNumAlmacen' "
        );
        $buscarPasillo->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarPasillo->fetchAll();
    }

    public function metGuardarEstante($pk_num_pasillo,$ind_descripcion_estante, $fecha_hora ,$usuario)
    {
        $this->_db->beginTransaction();
        $guardarEstante=$this->_db->prepare(
            "insert into ad_c003_estante values (null, :ind_descripcion_estante, :fec_ultima_modificacion, :fk_a018_num_seguridad_usuario, :fk_adc002_num_pasillo)"
        );
        $guardarEstante->execute(array(
            ':ind_descripcion_estante' => $ind_descripcion_estante,
            ':fec_ultima_modificacion' => $fecha_hora,
            ':fk_a018_num_seguridad_usuario' => $usuario,
            ':fk_adc002_num_pasillo' => $pk_num_pasillo
        ));
        $pkNumEstante= $this->_db->lastInsertId();
        $this->_db->commit();
        return $pkNumEstante;
    }

    public function metVerPasillo($pk_num_pasillo)
    {
        $verPasillo =  $this->_db->query(
            "SELECT pk_num_pasillo, ind_pasillo from ad_c002_pasillo where pk_num_pasillo='$pk_num_pasillo'"
        );
        $ind_pasillo= $verPasillo->fetch();
        return  $ind_pasillo[1];
    }

    public function metVerAlmacen($pk_num_almacen)
    {
        $verAlmacen =  $this->_db->query(
            "SELECT pk_num_almacen, ind_descripcion_almacen from ad_c001_almacen where pk_num_almacen='$pk_num_almacen'"
        );
       $ind_descripcion_almacen= $verAlmacen->fetch();
       return $ind_descripcion_almacen[1];
    }

    public function metEliminarEstante($pkNumEstante)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ad_c003_estante " .
            "where pk_num_estante = '$pkNumEstante'"
        );
        $this->_db->commit();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
           "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
            where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
            c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metListarReporte()
    {
        $listarReporte =  $this->_db->query(
            "select a.ind_descripcion_almacen, b.ind_pasillo, c.pk_num_estante, c.ind_descripcion_estante from ad_c001_almacen as a, ad_c002_pasillo as b, ad_c003_estante as c where c.fk_adc002_num_pasillo=b.pk_num_pasillo and b.fk_adc001_num_almacen=a.pk_num_almacen group by c.pk_num_estante, b.pk_num_pasillo, a.pk_num_almacen order by a.pk_num_almacen"
        );
        $listarReporte->setFetchMode(PDO::FETCH_ASSOC);
        return $listarReporte->fetchAll();
    }

    public function metListarPasillo($pkNumAlmacen)
    {
        $listarPasillo =  $this->_db->query(
            "SELECT pk_num_pasillo, ind_pasillo from ad_c002_pasillo where fk_adc001_num_almacen = '$pkNumAlmacen'"
        );
        $listarPasillo->setFetchMode(PDO::FETCH_ASSOC);
        return $listarPasillo->fetchAll();
    }

    public function metVerEstante($pkNumEstante, $metodo)
    {
        $verEstante =  $this->_db->query(
            "select a.pk_num_estante, a.ind_descripcion_estante,date_format(a.fec_ultima_modificacion,'%d/%m/%Y %H:%i:%S') as fecha, b.pk_num_pasillo, c.pk_num_almacen, b.ind_pasillo, c.ind_descripcion_almacen, f.ind_nombre1, f.ind_apellido1 from ad_c003_estante as a, ad_c002_pasillo as b, ad_c001_almacen as c, a018_seguridad_usuario as d, rh_b001_empleado as e, a003_persona as f where a.pk_num_estante='$pkNumEstante' and a.fk_adc002_num_pasillo=b.pk_num_pasillo and b.fk_adc001_num_almacen=c.pk_num_almacen and a.fk_a018_num_seguridad_usuario=d.pk_num_seguridad_usuario and d.fk_rhb001_num_empleado=e.pk_num_empleado and e.fk_a003_num_persona=f.pk_num_persona"
        );
        $verEstante->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
        return $verEstante->fetch();
        else return $verEstante->fetchAll();

    }

    public function metEditarEstante($ind_descripcion_estante, $pk_num_pasillo, $fecha_hora,$usuario, $pkNumEstante)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ad_c003_estante set ind_descripcion_estante='$ind_descripcion_estante', fec_ultima_modificacion='$fecha_hora',    fk_a018_num_seguridad_usuario='$usuario', fk_adc002_num_pasillo='$pk_num_pasillo' where pk_num_estante='$pkNumEstante'"
        );
        $this->_db->commit();
    }

    public function metBuscarAlmacen($pkNumEstante)
    {
        $buscarAlmacen =  $this->_db->query(
            "select c.pk_num_almacen from ad_c003_estante as a, ad_c002_pasillo as b, ad_c001_almacen as c where a.pk_num_estante='$pkNumEstante' and a.fk_adc002_num_pasillo=b.pk_num_pasillo and b.fk_adc001_num_almacen=c.pk_num_almacen"
        );
        $buscarAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarAlmacen->fetch();
    }

    public function metBuscarEstante($ind_descripcion_estante, $pk_num_almacen, $pk_num_pasillo, $pk_num_estante)
    {
        if($pk_num_estante == false) {
            $buscarEstante = $this->_db->query(
                "select a.pk_num_estante from ad_c003_estante as a, ad_c002_pasillo as b, ad_c001_almacen as c where a.fk_adc002_num_pasillo=b.pk_num_pasillo and b.fk_adc001_num_almacen=c.pk_num_almacen and a.ind_descripcion_estante='$ind_descripcion_estante'"
            );
            $buscarEstante->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarEstante->fetch();
            $arreglo = $buscarEstante->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        } else {
            $buscarEstante = $this->_db->query(
                "select a.pk_num_estante from ad_c003_estante as a, ad_c002_pasillo as b, ad_c001_almacen as c where a.fk_adc002_num_pasillo=b.pk_num_pasillo and b.fk_adc001_num_almacen=c.pk_num_almacen and a.ind_descripcion_estante='$ind_descripcion_estante' and a.pk_num_estante <>'$pk_num_estante' "
            );
            $buscarEstante->setFetchMode(PDO::FETCH_ASSOC);
            return $buscarEstante->fetch();
            $arreglo = $buscarEstante->fetchAll();
            if (count($arreglo) > 0) {
                return true;
            } else {
                return false;
            }
        }

    }
}// fin de la clase
?>
