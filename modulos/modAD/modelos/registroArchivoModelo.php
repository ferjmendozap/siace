<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de registros de documentos
class registroArchivoModelo extends Modelo 
{
    // Constructor del modelo
    public function __construct()
    {
        parent::__construct();
        $this->atIdUsuario = Session::metObtener('idUsuario');
    }

    public function metListarArchivo($usuario, $idAplicacion)
    {
        $listarArchivo =  $this->_db->query(
            "SELECT 
                        a.pk_num_registro_documento, 
                        a.num_registro, 
                        a.cod_memo, 
                        a.ind_documento, 
                        date_format(a.fec_documento,'%d/%m/%Y') as fecha_documento, 
                        date_format(a.fec_registro,'%d/%m/%Y') as fecha_registro, 
                        a.ind_descripcion, a.ind_resumen 
                      FROM 
                        ad_b001_registro_documento as a 
                        LEFT JOIN ad_c004_caja as b ON a.fk_adc004_num_caja=b.pk_num_caja 
                        INNER JOIN ad_c009_carpeta AS carpeta ON a.fk_adc009_num_carpeta = carpeta.pk_num_carpeta
                        INNER JOIN a019_seguridad_dependencia as c ON c.fk_a018_num_seguridad_usuario=$usuario and
                        c.fk_a015_num_seguridad_aplicacion=$idAplicacion
                      WHERE 1
                       GROUP BY a.pk_num_registro_documento
                        "
        );
        $listarArchivo->setFetchMode(PDO::FETCH_ASSOC);
        return $listarArchivo->fetchAll();
    }

    public function metListarDependencia()
    {
        $listarDependencia =  $this->_db->query(
            "select pk_num_dependencia, ind_dependencia from a004_dependencia"
        );
        $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
        return $listarDependencia->fetchAll();
    }

    public function metListarAlmacen()
    {
        $listarAlmacen =  $this->_db->query(
            "SELECT pk_num_almacen, ind_descripcion_almacen from ad_c001_almacen"
        );
        $listarAlmacen->setFetchMode(PDO::FETCH_ASSOC);
        return $listarAlmacen->fetchAll();
    }

    public function metBuscarEstante($pkNumPasillo)
    {
        $buscarEstante = $this->_db->query(
            "select pk_num_estante, ind_descripcion_estante 
                      from ad_c003_estante where fk_adc002_num_pasillo='$pkNumPasillo'"
        );
        $buscarEstante->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarEstante->fetchAll();
    }

    public function metBuscarPasillo($pkNumAlmacen)
    {
        $buscarPasillo =  $this->_db->query(
            "SELECT pk_num_pasillo, ind_pasillo, ind_descripcion_pasillo from ad_c002_pasillo where fk_adc001_num_almacen = '$pkNumAlmacen' "
        );
        $buscarPasillo->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarPasillo->fetchAll();
    }

    public function metListarCaja($pk_num_estante)
    {
        $listarCaja =  $this->_db->query(
            "select * from ad_c004_caja where fk_adc003_num_estante='$pk_num_estante'"
        );
        $listarCaja->setFetchMode(PDO::FETCH_ASSOC);
        return $listarCaja->fetchAll();
    }

    public function metListarCarpeta($pk_num_estante)
    {
        $listarCaja =  $this->_db->query(
            "select * from ad_c009_carpeta where fk_adc003_num_estante='$pk_num_estante'"
        );
        $listarCaja->setFetchMode(PDO::FETCH_ASSOC);
        return $listarCaja->fetchAll();
    }

    public function metListarDocumento($pk_num_dependencia)
    {
        $buscarDocumento =  $this->_db->query(
            "SELECT a.pk_num_documento, a.ind_descripcion_documento from ad_c005_tipo_documento as a, ad_c004_caja as b where a.pk_num_documento=b.fk_adc005_num_documento and b.fk_a004_num_dependencia='$pk_num_dependencia'"
        );
        $buscarDocumento->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarDocumento->fetchAll();
    }

    public function metListarFuncionario()
    {
        $funcionario =  $this->_db->query(
            "SELECT  b.pk_num_empleado,a.ind_nombre1, a.ind_apellido1 FROM a003_persona as a, rh_b001_empleado as b where b.fk_a003_num_persona=a.pk_num_persona"
        );
        $funcionario->setFetchMode(PDO::FETCH_ASSOC);
        return $funcionario->fetchAll();
    }

    public function metObtenerNumRegistro($anio)
    {
        if(!$anio) {
            $anio = date('Y');
        }
        $numRegistro = $this->_db->query(
            "select max(num_registro) as maximo from ad_b001_registro_documento WHERE fec_anio = '$anio'"
        );

        $numRegistro->setFetchMode(PDO::FETCH_ASSOC);
        return $numRegistro->fetch();
    }

    public function metGuardarArchivo($datos)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "insert into ad_b001_registro_documento SET
                        num_registro=:num_registro, 
                        fec_anio=:fec_anio, 
                        cod_memo=:cod_memo, 
                        ind_documento=:ind_documento, 
                        fec_documento=:fec_documento, 
                        fec_registro=NOW(), 
                        ind_descripcion=:ind_descripcion, 
                        ind_resumen=:ind_resumen, 
                        fec_ultima_modificacion=NOW(), 
                        fk_adc004_num_caja=:fk_adc004_num_caja, 
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
                        fk_rhb001_num_empleado=:fk_rhb001_num_empleado, 
                        num_flag_archivo=:num_flag_archivo,
                        fk_adc009_num_carpeta=:fk_adc009_num_carpeta
                        ");
        $NuevoRegistro->execute(array(
            ':num_registro' => $datos['num_registro'],
            ':fec_anio' => $datos['fec_anio'],
            ':cod_memo' => $datos['cod_memo'],
            ':ind_documento' => $datos['ind_documento'],
            ':fec_documento' => $datos['fec_documento'],
            ':ind_descripcion' => $datos['ind_descripcion'],
            ':ind_resumen' => $datos['ind_resumen'],
            ':fk_adc004_num_caja' => $datos['pk_num_caja'],
            ':fk_rhb001_num_empleado' => $datos['pk_num_empleado'],
            ':num_flag_archivo' => $datos['verificar'],
            ':fk_adc009_num_carpeta' => $datos['pk_num_carpeta']
        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    public function metEliminarArchivo($pkNumRegistro)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "DELETE
                        FROM 
                        ad_b001_registro_documento 
                        where 
                        pk_num_registro_documento = '$pkNumRegistro'"
        );
        $this->_db->commit();
    }

    public function metEliminarArchivoDocumento($id,$name)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ad_c008_archivo where ind_nombre_archivo = '$name' AND fk_adb001_num_registro_documento = '$id'"
        );
        $this->_db->commit();
    }

    public function metEliminarArchivoPorAnioNumRegistro($fec_anio,$numRegistro)
    {
        $this->_db->beginTransaction();
        $verRegistro = $this->_db->query(
            "SELECT
                          pk_num_registro_documento
                        FROM
                          ad_b001_registro_documento AS a
                        WHERE
                          a.fec_anio = '$fec_anio' AND a.num_registro = '$numRegistro'
                        ");
        $verRegistro->setFetchMode(PDO::FETCH_ASSOC);
        $resgistro = $verRegistro->fetch();

        $this->_db->query(
            "delete from ad_c008_archivo where fk_adb001_num_registro_documento = '".$resgistro['pk_num_registro_documento']."'"
        );
        $this->_db->commit();
    }

    public function metVisualizarRegistro($pkNumRegistro, $metodo)
    {
        $verRegistro = $this->_db->query(
            "SELECT
                          a.*,
                          b.pk_num_almacen,
                          b.ind_descripcion_almacen,
                          c.pk_num_pasillo,
                          c.ind_pasillo,
                          d.pk_num_estante,
                          d.ind_descripcion_estante,
                          e.pk_num_caja,
                          e.ind_descripcion_caja,
                          g.pk_num_dependencia,
                          g.ind_dependencia,
                          h.pk_num_empleado,
                          i.ind_nombre1,
                          i.ind_nombre2,
                          i.ind_apellido1,
                          i.ind_apellido2,
                          carpeta.pk_num_carpeta
                        FROM
                          ad_b001_registro_documento AS a
                        INNER JOIN
                          ad_c009_carpeta AS carpeta ON a.fk_adc009_num_carpeta = carpeta.pk_num_carpeta
                        INNER JOIN
                          ad_c003_estante AS d ON carpeta.fk_adc003_num_estante = d.pk_num_estante
                        INNER JOIN
                          ad_c002_pasillo AS c ON d.fk_adc002_num_pasillo = c.pk_num_pasillo
                        INNER JOIN
                          ad_c001_almacen AS b ON c.fk_adc001_num_almacen = b.pk_num_almacen
                        LEFT JOIN
                          ad_c004_caja AS e ON e.pk_num_caja = a.fk_adc004_num_caja
                        INNER JOIN
                          a004_dependencia AS g ON carpeta.fk_a004_num_dependencia = g.pk_num_dependencia
                        INNER JOIN
                          rh_b001_empleado AS h ON h.pk_num_empleado = a.fk_rhb001_num_empleado
                        INNER JOIN
                          a003_persona AS i ON h.fk_a003_num_persona = i.pk_num_persona
                        WHERE
                          a.pk_num_registro_documento = '$pkNumRegistro'
                        ");
        $verRegistro->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $verRegistro->fetch();
        else
            return $verRegistro->fetchAll();
    }

    public function metEditarArchivo($datos)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "update 
                        ad_b001_registro_documento 
                      SET
                        cod_memo=:cod_memo, 
                        ind_documento=:ind_documento, 
                        fec_documento=:fec_documento, 
                        ind_descripcion=:ind_descripcion, 
                        ind_resumen=:ind_resumen, 
                        fec_ultima_modificacion=NOW(), 
                        fk_adc004_num_caja=:fk_adc004_num_caja, 
                        fk_a018_num_seguridad_usuario='$this->atIdUsuario', 
                        fk_rhb001_num_empleado=:fk_rhb001_num_empleado, 
                        num_flag_archivo=:num_flag_archivo,
                        fk_adc009_num_carpeta=:fk_adc009_num_carpeta
                      WHERE pk_num_registro_documento=:pk_num_registro_documento
                        ");
        $NuevoRegistro->execute(array(
            ':cod_memo' => $datos['cod_memo'],
            ':ind_documento' => $datos['ind_documento'],
            ':fec_documento' => $datos['fec_documento'],
            ':ind_descripcion' => $datos['ind_descripcion'],
            ':ind_resumen' => $datos['ind_resumen'],
            ':fk_adc004_num_caja' => $datos['pk_num_caja'],
            ':fk_rhb001_num_empleado' => $datos['pk_num_empleado'],
            ':num_flag_archivo' => $datos['verificar'],
            ':fk_adc009_num_carpeta' => $datos['pk_num_carpeta'],
            ':pk_num_registro_documento' => $datos['pk_num_registro_documento']
        ));

        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $datos['pk_num_registro_documento'];
        }
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1 from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    // Método que permite obtener el identificador del documento mediante el número de registro
    public function metBuscarIdentificador($numRegistro,$fec_anio)
    {
        $consultaIndentificador = $this->_db->query(
            "select pk_num_registro_documento from ad_b001_registro_documento where num_registro='$numRegistro' AND fec_anio='$fec_anio' "
        );
        $consultaIndentificador->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaIndentificador->fetch();
    }

    // Guardo los datos del archivo
    public function metGuardarArchivoDocumento($pkNumRegistroDocumento, $nombreArchivo)
    {
        $this->_db->beginTransaction();
        $NuevoRegistro=$this->_db->prepare(
            "insert into ad_c008_archivo values (null, :fk_adb001_num_registro_documento, :ind_nombre_archivo)"
        );
        $NuevoRegistro->execute(array(
            ':fk_adb001_num_registro_documento' => $pkNumRegistroDocumento,
            ':ind_nombre_archivo' => $nombreArchivo

        ));

        $idRegistro= $this->_db->lastInsertId();
        $fallaTansaccion = $NuevoRegistro->errorInfo();

        if(!empty($fallaTansaccion[1]) && !empty($fallaTansaccion[2])){
            $this->_db->rollBack();
            return $fallaTansaccion;
        }else{
            $this->_db->commit();
            return $idRegistro;
        }
    }

    // Método que permite ubicar el nombre del archivo y el número de registro del mismo
    public function metBuscarArchivo($pkNumRegistro,$cual = false)
    {
        $where = "";
        if($cual==1) {
            $where = " AND a.ind_nombre_archivo NOT LIKE '%pdf%' ";
        }elseif($cual==2) {
            $where = " AND a.ind_nombre_archivo LIKE '%pdf%' ";
        }
        $consultaArchivo = $this->_db->query(
            "select a.*, b.num_registro from ad_c008_archivo as a, ad_b001_registro_documento as b where a.fk_adb001_num_registro_documento=b.pk_num_registro_documento and a.fk_adb001_num_registro_documento='$pkNumRegistro' $where"
        );
        $consultaArchivo->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaArchivo->fetchAll();
    }

    // Método que permite editar el nombre del archivo cargado en el sistema
    public function metEditarArchivoCargar($pkNumRegistroDocumento, $nombreArchivo)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ad_c008_archivo set ind_nombre_archivo='$nombreArchivo' where fk_adb001_num_registro_documento=$pkNumRegistroDocumento"
        );
        $this->_db->commit();
    }

    public function metListarEmpleado()
    {
        $funcionario =  $this->_db->query(
            "select a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, c.ind_dependencia from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, a004_dependencia as c where a.ind_estatus_empleado=1 and a.pk_num_empleado=b.pk_num_empleado and b.fk_a004_num_dependencia=c.pk_num_dependencia"
        );
        $funcionario->setFetchMode(PDO::FETCH_ASSOC);
        return $funcionario->fetchAll();
    }

    // Metodo que permite obtener el nombre de un empleado
    public function metConsultarEmpleado($pkNumEmpleado)
    {
        $empleado = $this->_db->query(
            "select ind_nombre1, ind_nombre2, ind_apellido1, ind_apellido2 from vl_rh_persona_empleado where pk_num_empleado=$pkNumEmpleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    public function metConsultarSeguridadAlterna($usuario, $idAplicacion)
    {
        $seguridadAlterna = $this->_db->query(
            "select b.pk_num_dependencia, b.ind_dependencia from a019_seguridad_dependencia as a, a004_dependencia as b where a.fk_a004_num_dependencia=b.pk_num_dependencia and a.fk_a018_num_seguridad_usuario=$this->atIdUsuario and a.fk_a015_num_seguridad_aplicacion=$idAplicacion"
        );
        $seguridadAlterna->setFetchMode(PDO::FETCH_ASSOC);
        return $seguridadAlterna->fetchAll();
    }
}// fin de la clase
?>