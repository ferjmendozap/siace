<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de la salida de archivos
class salidaArchivoModelo extends registroArchivoModelo
{
    // Constructor del modelo
    public function __construct()
    {
        parent::__construct();
    } 

    public function metListarSalida($metodo, $valorSalida, $usuario, $idAplicacion)
    {
        if($metodo==1) {
            $listarSalida = $this->_db->query(
                "select a.pk_num_salida, b.ind_documento, b.num_registro, b.ind_descripcion, c.ind_estado from ad_b002_salida_documento_fisico as a, ad_b001_registro_documento as b, ad_c007_operacion_salida as c, ad_c004_caja as d, a019_seguridad_dependencia as e  where a.fk_adb001_num_registro_documento=b.pk_num_registro_documento and c.num_estatus=1 and a.pk_num_salida=c.fk_adb002_num_salida and b.fk_adc004_num_caja=d.pk_num_caja and d.fk_a004_num_dependencia=e.fk_a004_num_dependencia and e.fk_a018_num_seguridad_usuario=$usuario and e.fk_a015_num_seguridad_aplicacion=$idAplicacion order by pk_num_salida desc"
            );
        } else {
            $listarSalida =  $this->_db->query(
                "select a.pk_num_salida, b.ind_documento, b.num_registro, b.ind_descripcion, c.ind_estado from ad_b002_salida_documento_fisico as a, ad_b001_registro_documento as b, ad_c007_operacion_salida as c, ad_c004_caja as d, a019_seguridad_dependencia as e where a.fk_adb001_num_registro_documento=b.pk_num_registro_documento and c.num_estatus=1 and a.pk_num_salida=c.fk_adb002_num_salida and c.ind_estado='$valorSalida' and b.fk_adc004_num_caja=d.pk_num_caja and d.fk_a004_num_dependencia=e.fk_a004_num_dependencia and e.fk_a018_num_seguridad_usuario=$usuario and e.fk_a015_num_seguridad_aplicacion=$idAplicacion order by pk_num_salida desc"
            );
        }
        $listarSalida->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSalida->fetchAll();
    }

    public function metBuscarSalida($fechaInicio, $fechaFin)
    {
        $listarSalida =  $this->_db->query(
            "select a.pk_num_salida, a.cod_memo,a.ind_motivo, date_format(a.fec_fecha_salida, '%d/%m/%Y') as fecha_salida, b.ind_descripcion, b.ind_documento, b.num_registro, c.ind_estado from ad_b002_salida_documento_fisico as a, ad_b001_registro_documento as b, ad_c007_operacion_salida as c where a.fk_adb001_num_registro_documento=b.pk_num_registro_documento and fec_fecha_salida between '$fechaInicio' and '$fechaFin' and c.num_estatus=1 and c.ind_estado<>'AN' and c.num_estatus=1 and a.pk_num_salida=c.fk_adb002_num_salida order by pk_num_salida desc"
        );
        $listarSalida->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSalida->fetchAll();
    }

    public function metVerSalida($pkNumSalida, $metodo)
    {
        $verSalida = $this->_db->query(
            "select a.pk_num_salida, a.fk_adb001_num_registro_documento, date_format(a.fec_fecha_salida, '%d/%m/%Y') as fecha_salida, a.fec_fecha_salida,  a.ind_motivo, a.cod_memo,date_format(b.fec_documento, '%d/%m/%Y') as fecha_documento , b.num_flag_archivo, b.num_registro, b.ind_documento, c.pk_num_documento,c.ind_descripcion_documento, b.ind_descripcion, h.pk_num_almacen, h.ind_descripcion_almacen, g.pk_num_pasillo, g.ind_pasillo, f.pk_num_estante, f.ind_descripcion_estante, i.pk_num_dependencia, i.ind_dependencia, d.pk_num_caja, d.ind_descripcion_caja,j.ind_estado, l.ind_nombre1, l.ind_apellido1 from ad_b002_salida_documento_fisico as a, ad_b001_registro_documento as b, ad_c005_tipo_documento as c, ad_c004_caja as d, ad_c003_estante as f, ad_c002_pasillo as g, ad_c001_almacen as h, a004_dependencia as i, ad_c007_operacion_salida as j, rh_b001_empleado as k, a003_persona as l  where a.fk_adb001_num_registro_documento=b.pk_num_registro_documento and a.pk_num_salida='$pkNumSalida' and b.fk_adc004_num_caja=d.pk_num_caja and d.fk_adc005_num_documento=c.pk_num_documento and d.fk_adc003_num_estante=f.pk_num_estante and f.fk_adc002_num_pasillo=g.pk_num_pasillo and g.fk_adc001_num_almacen=h.pk_num_almacen and d.fk_a004_num_dependencia=i.pk_num_dependencia and j.num_estatus=1 and a.pk_num_salida=j.fk_adb002_num_salida and j.fk_rhb001_num_empleado=k.pk_num_empleado and k.fk_a003_num_persona=l.pk_num_persona"
        );
        $verSalida->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $verSalida->fetch();
        else return $verSalida->fetchAll();
    }

    public function metGuardarSalida($fec_fecha_salida,$pk_num_registro_documento,$cod_memo, $ind_motivo,  $fecha_hora ,$usuario, $funcionario)
    {
        $this->_db->beginTransaction();
        $NuevaSalida=$this->_db->prepare(
            "insert into ad_b002_salida_documento_fisico values (null, :fec_fecha_salida, :ind_motivo, :cod_memo, :fec_ultima_modificacion, :fk_adb001_num_registro_documento ,:fk_a018_num_seguridad_usuario)"
        );
        $NuevaSalida->execute(array(
            ':fec_fecha_salida' => $fec_fecha_salida,
            ':ind_motivo'=> $ind_motivo,
            ':cod_memo' => $cod_memo,
            ':fec_ultima_modificacion' => $fecha_hora,
            ':fk_adb001_num_registro_documento' => $pk_num_registro_documento,
            ':fk_a018_num_seguridad_usuario' => $usuario
        ));
        $pkNumSalida= $this->_db->lastInsertId();
        $NuevaOperacion=$this->_db->prepare(
            "insert into ad_c007_operacion_salida values (null, :ind_estado, :fk_adb002_num_salida, :fk_rhb001_num_empleado, :num_estatus, :fec_operacion)"
        );
        $NuevaOperacion->execute(array(
            ':ind_estado' => 'PR',
            ':fk_adb002_num_salida' => $pkNumSalida,
            ':fk_rhb001_num_empleado' => $funcionario,
	        ':num_estatus' => 1,
            ':fec_operacion' => $fecha_hora
        ));
        $this->_db->commit();
        return $pkNumSalida;
    }

    public function metListarRegistros($pkNumCaja)
    {
        $listarRegistro = $this->_db->query(
            "select pk_num_registro_documento, num_registro, ind_documento from ad_b001_registro_documento where fk_adc004_num_caja=$pkNumCaja order by num_registro"
        );
        $listarRegistro->setFetchMode(PDO::FETCH_ASSOC);
        return $listarRegistro->fetchAll();
    }

    public function metUsuario($usuario, $metodo)
    {
         $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
        return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    public function metEliminarSalida($pkNumSalida)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ad_b002_salida_documento_fisico where pk_num_salida='$pkNumSalida'"
        );
        $this->_db->query(
            "delete from ad_c007_operacion_salida where fk_adb002_num_salida='$pkNumSalida'"
        );
        $this->_db->commit();
    }

    public function metAcceso($pkNumSalida)
    {
        $acceso = $this->_db->query(
            "select d.ind_nombre1, d.ind_apellido1, date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%i:%S') as fecha_modificacion, c.pk_num_empleado from ad_b002_salida_documento_fisico as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.pk_num_salida='$pkNumSalida' and a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona
            "
        );
        $acceso->setFetchMode(PDO::FETCH_ASSOC);
        return $acceso->fetchAll();
    }

    public function metUsuarioReporte($usuario)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c
         where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        return $obtenerUsuario->fetch();
    }

    public function metEditarSalida($cod_memo, $ind_motivo, $fec_fecha_salida, $fecha_hora,$usuario, $pkNumSalida)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ad_b002_salida_documento_fisico set cod_memo='$cod_memo', ind_motivo='$ind_motivo', fec_fecha_salida='$fec_fecha_salida', fec_ultima_modificacion='$fecha_hora', fk_a018_num_seguridad_usuario='$usuario' where pk_num_salida='$pkNumSalida'"
        );
        $this->_db->commit();
    }

    public function metBuscarFecha($fechaInicio, $fechaFin)
    {
        $buscarFecha = $this->_db->query(
            "select pk_num_salida from ad_b002_salida_documento_fisico where fec_fecha_salida between '$fechaInicio' and '$fechaFin'"
        );
        $buscarFecha->setFetchMode(PDO::FETCH_ASSOC);
        return $buscarFecha->fetchAll();
    }

    public function metCambiarEstatus($pkNumSalida, $valor, $idEmpleado, $fecha_hora)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ad_c007_operacion_salida set num_estatus=0 where fk_adb002_num_salida='$pkNumSalida'"
        );
        $cambiarEstatus=$this->_db->prepare(
            "insert into ad_c007_operacion_salida values (null, :ind_estado, :fk_adb002_num_salida, :fk_rhb001_num_empleado, :num_estatus, :fec_operacion)"
        );
        $cambiarEstatus->execute(array(
            ':ind_estado' => $valor,
            ':fk_adb002_num_salida'=> $pkNumSalida,
            ':fk_rhb001_num_empleado' => $idEmpleado,
	    ':num_estatus' => 1,
            ':fec_operacion' => $fecha_hora
        ));
        $this->_db->commit();
    }

    public function metVerificarSalida($pk_num_registro_documento) 
    {
        // Verifico que el documento se encuentre preparado, Si ya tiene una solicitud
        $salida1 = $this->_db->query(
            "select a.pk_num_salida from ad_b002_salida_documento_fisico as a, ad_c007_operacion_salida as b where a.fk_adb001_num_registro_documento='$pk_num_registro_documento' and a.pk_num_salida=b.fk_adb002_num_salida and b.ind_estado='PR' and b.num_estatus=1"
        );
        $salida1->setFetchMode(PDO::FETCH_ASSOC);
        $arreglo1 =  $salida1->fetchAll();
        if (count($arreglo1) > 0) {
            return false;
        }
        if (count($arreglo1) == 0){
            // Verifico que el documento esté anulado. Si está anulado entonces puede salir de nuevo
            $salida2 = $this->_db->query(
                "select a.pk_num_salida from ad_b002_salida_documento_fisico as a, ad_c007_operacion_salida as b where a.fk_adb001_num_registro_documento='$pk_num_registro_documento' and a.pk_num_salida=b.fk_adb002_num_salida and b.ind_estado='AN' and b.num_estatus=1"
            );
            $salida2->setFetchMode(PDO::FETCH_ASSOC);
            $arreglo2 =  $salida2->fetchAll();
            if (count($arreglo2) > 0) {
                return true;
            }
            if (count($arreglo2) == 0) {
             // Verifico si está aprobada o anulada la entrada
                $salida3 = $this->_db->query(
                    "select c.pk_num_entrada from ad_b002_salida_documento_fisico as a, ad_c007_operacion_salida as b, ad_b003_entrada_documento_fisico as c, ad_c006_operacion_entrada as d where a.fk_adb001_num_registro_documento='$pk_num_registro_documento' and a.pk_num_salida=b.fk_adb002_num_salida and a.pk_num_salida=c.fk_adb002_num_salida and c.pk_num_entrada=d.	fk_adb003_num_entrada and d.num_estatus=1 and d.ind_estado='PR' || d.ind_estado='AN'");
                $salida3->setFetchMode(PDO::FETCH_ASSOC);
                $arreglo3 =  $salida3->fetchAll();
                if (count($arreglo3) > 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    }

    public function metVisualizarRegistro($pkNumRegistro, $metodo)
    {
        $verRegistro = $this->_db->query(
            "select a.pk_num_registro_documento, a.num_registro, a.num_flag_archivo, a.cod_memo, a.ind_documento, date_format(fec_documento,'%d/%m/%Y') as fecha_documento, date_format(a.fec_registro, '%d/%m/%Y %H:%i:%S') as fecha_registro, a.ind_descripcion, a.ind_resumen, b.pk_num_almacen, b.ind_descripcion_almacen, c.pk_num_pasillo, c.ind_pasillo, d.pk_num_estante, d.ind_descripcion_estante,e.pk_num_caja, e.ind_descripcion_caja, f.pk_num_documento, f.ind_descripcion_documento, g.pk_num_dependencia, g.ind_dependencia, h.pk_num_empleado, i.ind_nombre1, i.ind_apellido1 from ad_b001_registro_documento as a, ad_c001_almacen as b, ad_c002_pasillo as c, ad_c003_estante as d, ad_c004_caja as e, ad_c005_tipo_documento as f, a004_dependencia as g, rh_b001_empleado as h, a003_persona as i where a.fk_adc004_num_caja=e.pk_num_caja and  e.fk_adc003_num_estante=d.pk_num_estante and d.fk_adc002_num_pasillo=c.pk_num_pasillo and c.fk_adc001_num_almacen=b.pk_num_almacen and e.fk_adc005_num_documento=f.pk_num_documento and a.pk_num_registro_documento='$pkNumRegistro' and e.fk_a004_num_dependencia=g.pk_num_dependencia and a.fk_rhb001_num_empleado=h.pk_num_empleado and h.fk_a003_num_persona=i.pk_num_persona"
        );
        $verRegistro->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $verRegistro->fetch();
        else return $verRegistro->fetchAll();
    }
	
	// Permite verificar si un documento aun no ha entrado al archivo
	public function metVerificarEntradaRegistro($pkNumRegistroDocumento)
	{
		 $verRegistro = $this->_db->query(
            "select a.pk_num_registro_documento from ad_b001_registro_documento as a, ad_b002_salida_documento_fisico as b, ad_b003_entrada_documento_fisico as c, ad_c006_operacion_entrada as d where a.pk_num_registro_documento=$pkNumRegistroDocumento and a.pk_num_registro_documento=b.fk_adb001_num_registro_documento and b.pk_num_salida=c.fk_adb002_num_salida and c.pk_num_entrada=d. 	fk_adb003_num_entrada and d.num_estatus=1 and d.ind_estado='PR'"
        );
        $verRegistro->setFetchMode(PDO::FETCH_ASSOC);
		return $verRegistro->fetchAll();
	}

    public function metConsultarSeguridadAlterna($usuario, $idAplicacion)
    {
        $seguridadAlterna = $this->_db->query(
            "select b.pk_num_dependencia, b.ind_dependencia from a019_seguridad_dependencia as a, a004_dependencia as b where a.fk_a004_num_dependencia=b.pk_num_dependencia and a.fk_a018_num_seguridad_usuario=$usuario and a.fk_a015_num_seguridad_aplicacion=$idAplicacion"
        );
        $seguridadAlterna->setFetchMode(PDO::FETCH_ASSOC);
        return $seguridadAlterna->fetchAll();
    }
    
}// fin de la clase
?>
