<?php
/****************************************************************************************
 * DEV: CONTRALORÍA DEL ESTADO.
 * MODULO: Archivo Digital
 * PROGRAMADORES.________________________________________________________________________
 * | # | NOMBRE.              | CORREO.                              | TELÉFONO.
 * | 1 | María B. Rondón R    | dt.ait.programador3@cgesucre.gob.ve  | 0416-3853790
 * | * |_________________________________________________________________________________
 ****************************************************************************************/
// Esta clase se encarga de gestionar el registro, búsqueda, listado, edición y eliminación de entradas de documentos de retorno al almacén
class entradaArchivoModelo extends Modelo 
{
    public function __construct()
    {
        parent::__construct();
    }

    public function metListarEntrada($metodo, $valorEntrada)
    {
        if($metodo==1) {
            $listarEntrada = $this->_db->query(
                "select a.pk_num_entrada, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, a.cod_memo, c.ind_documento, d.ind_estado from ad_b003_entrada_documento_fisico as a, ad_b002_salida_documento_fisico as b, ad_b001_registro_documento as c, ad_c006_operacion_entrada as d where a.fk_adb002_num_salida=b.pk_num_salida and b.fk_adb001_num_registro_documento=c.pk_num_registro_documento and d.num_estatus=1 and a.pk_num_entrada=d.fk_adb003_num_entrada order by a.pk_num_entrada desc"
            );
        } else {
            $listarEntrada = $this->_db->query(
                "select a.pk_num_entrada, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, a.cod_memo, c.ind_documento, d.ind_estado from ad_b003_entrada_documento_fisico as a, ad_b002_salida_documento_fisico as b, ad_b001_registro_documento as c, ad_c006_operacion_entrada as d where a.fk_adb002_num_salida=b.pk_num_salida and b.fk_adb001_num_registro_documento=c.pk_num_registro_documento and d.num_estatus=1 and a.pk_num_entrada=d.fk_adb003_num_entrada and d.ind_estado='$valorEntrada' order by a.pk_num_entrada desc"
            );
        }
        $listarEntrada->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEntrada->fetchAll();
    }

    public function metListarSalida($usuario, $idAplicacion)
    {
        $listarSalida =  $this->_db->query(
                "select a.pk_num_salida, b.ind_documento, b.num_registro, b.ind_descripcion, date_format(a.fec_fecha_salida, '%d/%m/%Y') as fecha_salida, a.cod_memo, c.ind_estado from ad_b002_salida_documento_fisico as a, ad_b001_registro_documento as b, ad_c007_operacion_salida as c, ad_c004_caja as d, a019_seguridad_dependencia as e where a.fk_adb001_num_registro_documento=b.pk_num_registro_documento and a.pk_num_salida=c.fk_adb002_num_salida and c.ind_estado='AP' and b.fk_adc004_num_caja=d.pk_num_caja and d.fk_a004_num_dependencia=e.fk_a004_num_dependencia and e.fk_a018_num_seguridad_usuario=$usuario and e.fk_a015_num_seguridad_aplicacion=$idAplicacion and a.pk_num_salida not in (select d.fk_adb002_num_salida from ad_b003_entrada_documento_fisico as d, ad_c006_operacion_entrada as e where d.pk_num_entrada=e.fk_adb003_num_entrada and e.num_estatus=1 and e.ind_estado<>'AN') order by a.pk_num_salida desc"
            );
        $listarSalida->setFetchMode(PDO::FETCH_ASSOC);
        return $listarSalida->fetchAll();
    }

    public function metVerSalida($pkNumSalida, $metodo)
    {
        $verSalida = $this->_db->query(
            "select a.pk_num_salida, a.fk_adb001_num_registro_documento, date_format(a.fec_fecha_salida, '%d/%m/%Y') as fecha_salida, a.fec_fecha_salida,  a.ind_motivo, a.cod_memo,date_format(b.fec_documento, '%d/%m/%Y') as fecha_documento ,b.num_registro, b.ind_documento, c.pk_num_documento,c.ind_descripcion_documento, b.num_flag_archivo, b.ind_descripcion, h.pk_num_almacen, h.ind_descripcion_almacen, g.pk_num_pasillo, g.ind_pasillo, f.pk_num_estante, f.ind_descripcion_estante, i.pk_num_dependencia, i.ind_dependencia, d.pk_num_caja, d.ind_descripcion_caja,j.ind_estado, l.ind_nombre1, l.ind_apellido1 from ad_b002_salida_documento_fisico as a, ad_b001_registro_documento as b, ad_c005_tipo_documento as c, ad_c004_caja as d, ad_c003_estante as f, ad_c002_pasillo as g, ad_c001_almacen as h, a004_dependencia as i, ad_c007_operacion_salida as j, rh_b001_empleado as k, a003_persona as l  where a.fk_adb001_num_registro_documento=b.pk_num_registro_documento and a.pk_num_salida='$pkNumSalida' and b.fk_adc004_num_caja=d.pk_num_caja and d.fk_adc005_num_documento=c.pk_num_documento and d.fk_adc003_num_estante=f.pk_num_estante and f.fk_adc002_num_pasillo=g.pk_num_pasillo and g.fk_adc001_num_almacen=h.pk_num_almacen and d.fk_a004_num_dependencia=i.pk_num_dependencia and j.num_estatus=1 and a.pk_num_salida=j.fk_adb002_num_salida and j.fk_rhb001_num_empleado=k.pk_num_empleado and k.fk_a003_num_persona=l.pk_num_persona"
        );
        $verSalida->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $verSalida->fetch();
        else return $verSalida->fetchAll();
    }

    public function metAcceso($valor, $operacion)
    {
        if($operacion==1) {
            $acceso = $this->_db->query(
                "select d.ind_nombre1, d.ind_apellido1, date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%i:%S') as fecha_modificacion from ad_b002_salida_documento_fisico as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.pk_num_salida='$valor' and a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona
                "
            );
        } else {
            $acceso = $this->_db->query(
                "select d.ind_nombre1, d.ind_apellido1, date_format(a.fec_ultima_modificacion, '%d/%m/%Y %H:%i:%S') as fecha_modificacion from ad_b003_entrada_documento_fisico as a, a018_seguridad_usuario as b, rh_b001_empleado as c, a003_persona as d where a.pk_num_entrada='$valor' and a.fk_a018_num_seguridad_usuario=b.pk_num_seguridad_usuario and b.fk_rhb001_num_empleado=c.pk_num_empleado and c.fk_a003_num_persona=d.pk_num_persona
            "
            );
        }
        $acceso->setFetchMode(PDO::FETCH_ASSOC);
        return $acceso->fetchAll();
    }

    public function metGuardarEntrada($pk_num_salida, $ind_observacion, $cod_memo, $fechaEntrada, $funcionario, $fecha_hora ,$usuario)
    {
        $this->_db->beginTransaction();
        $NuevaEntrada=$this->_db->prepare(
            "insert into ad_b003_entrada_documento_fisico values (null, :fec_entrada, :ind_observacion, :cod_memo, :fec_ultima_modificacion, :fk_a018_num_seguridad_usuario, :fk_adb002_num_salida)"
        );
        $NuevaEntrada->execute(array(
            ':fec_entrada' => $fechaEntrada,
            ':ind_observacion' => $ind_observacion,
            ':cod_memo' =>  $cod_memo,
            ':fec_ultima_modificacion' => $fecha_hora,
            ':fk_a018_num_seguridad_usuario' => $usuario,
            ':fk_adb002_num_salida' => $pk_num_salida
        ));
        $pkNumEntrada= $this->_db->lastInsertId();
        $NuevaOperacion=$this->_db->prepare(
            "insert into ad_c006_operacion_entrada values (null, :ind_estado, :fk_adb003_num_entrada, :fk_rhb001_num_empleado, :num_estatus, :fec_operacion)"
        );
        $NuevaOperacion->execute(array(
            ':ind_estado' => 'PR',
            ':fk_adb003_num_entrada' => $pkNumEntrada,
            ':fk_rhb001_num_empleado' => $funcionario,
            ':num_estatus' => 1,
            ':fec_operacion' => $fecha_hora
        ));
        $this->_db->commit();
        return $pkNumEntrada;
    }

    public function metVisualizarEntrada($pkNumEntrada, $metodo)
    {
         $entrada = $this->_db->query(
             "select a.pk_num_entrada, a.fk_adb002_num_salida, a.cod_memo, a.ind_observacion, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada ,b.ind_estado, date_format(c.fec_fecha_salida, '%d/%m/%Y') as fecha_salida, d.num_flag_archivo, d.ind_documento, f.ind_nombre1, f.ind_apellido1 from ad_b003_entrada_documento_fisico as a, ad_c006_operacion_entrada as b, ad_b002_salida_documento_fisico as c, ad_b001_registro_documento as d, rh_b001_empleado as e, a003_persona as f where a.pk_num_entrada='$pkNumEntrada' and a.pk_num_entrada=b.fk_adb003_num_entrada and b.num_estatus=1 and a.fk_adb002_num_salida=c.pk_num_salida and c.fk_adb001_num_registro_documento=d.pk_num_registro_documento and b.fk_rhb001_num_empleado=e.pk_num_empleado and e.fk_a003_num_persona=f.pk_num_persona"
         );
        $entrada->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
        {
            return $entrada->fetch();
        } else {
            return $entrada->fetchAll();
        }
    }

    public function metEliminarEntrada($pkNumEntrada)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "delete from ad_b003_entrada_documento_fisico where pk_num_entrada = '$pkNumEntrada'"
        );
        $this->_db->query(
            "delete from ad_c006_operacion_entrada where fk_adb003_num_entrada = '$pkNumEntrada'"
        );
        $this->_db->commit();
    }

    public function metEditarEntrada($ind_observacion, $cod_memo, $fechaEntrada, $fecha_hora, $usuario, $pkNumEntrada)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ad_b003_entrada_documento_fisico set ind_observacion='$ind_observacion', cod_memo='$cod_memo', fec_entrada='$fechaEntrada', fec_ultima_modificacion='$fecha_hora', fk_a018_num_seguridad_usuario='$usuario' where pk_num_entrada='$pkNumEntrada'"
        );
        $this->_db->commit();
    }

    public function metUsuario($usuario, $metodo)
    {
        $obtenerUsuario = $this->_db->query(
            "select c.ind_nombre1, c.ind_apellido1, b.pk_num_empleado from a018_seguridad_usuario as a, rh_b001_empleado as b, a003_persona as c where a.pk_num_seguridad_usuario='$usuario' and a.fk_rhb001_num_empleado= b.pk_num_empleado and b.fk_a003_num_persona=
         c.pk_num_persona"
        );
        $obtenerUsuario->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $obtenerUsuario->fetch();
        else return $obtenerUsuario->fetchAll();
    }

    public function metCambiarEstatus($pkNumEntrada, $valor, $idEmpleado, $fecha_hora)
    {
        $this->_db->beginTransaction();
        $this->_db->query(
            "update ad_c006_operacion_entrada set num_estatus=0 where fk_adb003_num_entrada='$pkNumEntrada'"
        );
        $cambiarEstatus=$this->_db->prepare(
            "insert into ad_c006_operacion_entrada values (null, :ind_estado, :fk_adb003_num_entrada, :fk_rhb001_num_empleado, :num_estatus, :fec_operacion)"
        );
        $cambiarEstatus->execute(array(
            ':ind_estado' => $valor,
            ':fk_adb003_num_entrada'=> $pkNumEntrada,
            ':fk_rhb001_num_empleado' => $idEmpleado,
            ':num_estatus' => 1,
            ':fec_operacion' => $fecha_hora
        ));
        $this->_db->commit();
    }

    public function metBuscarEntrada($fechaInicio, $fechaFin)
    {
        $listarEntrada = $this->_db->query(
            "select a.pk_num_entrada, a.cod_memo, a.ind_observacion, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, date_format(b.fec_fecha_salida, '%d/%m/%Y') as fecha_salida, c.ind_documento, c.ind_descripcion, c.num_registro, d.ind_estado  from ad_b003_entrada_documento_fisico as a, ad_b002_salida_documento_fisico as b, ad_b001_registro_documento as c, ad_c006_operacion_entrada as d where a.fk_adb002_num_salida=b.pk_num_salida and b.fk_adb001_num_registro_documento=c.pk_num_registro_documento and a.fec_entrada between '$fechaInicio' and '$fechaFin' and d.num_estatus=1 and d.ind_estado<>'AN' and d.fk_adb003_num_entrada=a.pk_num_entrada order by a.pk_num_entrada desc");
        $listarEntrada->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEntrada->fetchAll();
    }

    // Método que permite obtener la dependencia, fecha de ingreso y el organismo al que pertenece el empleado
    public function metEmpleado($pkNumEmpleado)
    {
        $empleado =  $this->_db->query(
            "select a.pk_num_empleado, a.ind_cedula_documento, a.ind_nombre1, a.ind_nombre2, a.ind_apellido1, a.ind_apellido2, c.pk_num_puestos, c.ind_descripcion_cargo, d.ind_descripcion_empresa, e.ind_dependencia, e.pk_num_dependencia, d.pk_num_organismo, b.fk_rhc063_num_puestos_cargo from vl_rh_persona_empleado as a, vl_rh_persona_empleado_datos_laborales as b, rh_c063_puestos as c, a001_organismo as d, a004_dependencia as e where a.pk_num_empleado=$pkNumEmpleado and a.pk_num_empleado=b.pk_num_empleado and b.fk_rhc063_num_puestos_cargo=c.pk_num_puestos and b.fk_a004_num_dependencia=e.pk_num_dependencia and e.fk_a001_num_organismo=d.pk_num_organismo"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetch();
    }

    // Método que permite listar los organismos
    public function metListarOrganismo($pk_num_organismo, $metodo)
    {
        if($metodo==1){
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetchAll();
        } else {
            $listadoOrganismo = $this->_db->query(
                "select pk_num_organismo, ind_descripcion_empresa from a001_organismo where pk_num_organismo=$pk_num_organismo"
            );
            $listadoOrganismo->setFetchMode(PDO::FETCH_ASSOC);
            return   $listadoOrganismo->fetch();
        }
    }

    // Método que permite listar las dependencias del organismo seleccionado
    public function metListarDependencia($pkNumOrganismo, $metodo, $pk_num_dependencia, $usuario, $idAplicacion)
    {
        if($metodo==1){
            $listarDependencia =  $this->_db->query(
                "select a.pk_num_dependencia, a.ind_dependencia from a004_dependencia as a, a001_organismo as b, a019_seguridad_dependencia as c  where a.fk_a001_num_organismo=$pkNumOrganismo and a.fk_a001_num_organismo=b.pk_num_organismo and a.pk_num_dependencia=c.fk_a004_num_dependencia and c.fk_a018_num_seguridad_usuario=$usuario and c.fk_a015_num_seguridad_aplicacion=$idAplicacion"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetchAll();
        } else {
            $listarDependencia =  $this->_db->query(
                "select a.pk_num_dependencia, a.ind_dependencia from a004_dependencia as a, a019_seguridad_dependencia as b where a.pk_num_dependencia=$pk_num_dependencia and a.pk_num_dependencia=b.fk_a004_num_dependencia and b.fk_a018_num_seguridad_usuario=$usuario and b.fk_a015_num_seguridad_aplicacion=$idAplicacion"
            );
            $listarDependencia->setFetchMode(PDO::FETCH_ASSOC);
            return $listarDependencia->fetch();
        }

    }

    // Método que permite listar lo empleados asociados a una dependencia
    public function metDependenciaEmpleado($pkNumDependencia) 
    {
        $empleado =  $this->_db->query(
            "select a.pk_num_empleado, b.ind_nombre1, b.ind_nombre2, b.ind_apellido1, b.ind_apellido2 from rh_b001_empleado as a, a003_persona as b, rh_c076_empleado_organizacion as c where a.fk_a003_num_persona=b.pk_num_persona and c.fk_a004_num_dependencia=$pkNumDependencia and c.fk_rhb001_num_empleado=a.pk_num_empleado"
        );
        $empleado->setFetchMode(PDO::FETCH_ASSOC);
        return $empleado->fetchAll();
    }

    // Método que permite efectuar la busqueda de una entrada
    public function metConsultarEntrada($pkNumOrganismo, $pkNumDependencia, $fecha_inicio, $fecha_fin, $txtEstatus)
    {
        if($pkNumOrganismo!= ''){
            $filtro = " and g.pk_num_organismo=$pkNumOrganismo";
        }
        if($pkNumDependencia!=''){
            $filtro .= " and f.pk_num_dependencia=$pkNumDependencia";
        }
        if($txtEstatus!=''){
            $filtro .= " and d.ind_estado='$txtEstatus'";
        }
        if(($fecha_inicio!='')&&($fecha_fin!='')){
            $explodeInicio = explode("/", $fecha_inicio);
            $fechaInicio = $explodeInicio[2].'-'.$explodeInicio[1].'-'.$explodeInicio[0];
            $explodeFin = explode("/", $fecha_fin);
            $fechaFin = $explodeFin[2].'-'.$explodeFin[1].'-'.$explodeFin[0];
            $filtro .= " and a.fec_entrada between '$fechaInicio' and '$fechaFin'";
        }
            $listarEntrada = $this->_db->query(
                "select a.pk_num_entrada, date_format(a.fec_entrada, '%d/%m/%Y') as fecha_entrada, a.cod_memo, c.ind_documento, d.ind_estado from ad_b003_entrada_documento_fisico as a, ad_b002_salida_documento_fisico as b, ad_b001_registro_documento as c, ad_c006_operacion_entrada as d, ad_c004_caja as e, a004_dependencia as f, a001_organismo as g where a.fk_adb002_num_salida=b.pk_num_salida and b.fk_adb001_num_registro_documento=c.pk_num_registro_documento and d.num_estatus=1 and a.pk_num_entrada=d.fk_adb003_num_entrada and c.fk_adc004_num_caja=e.pk_num_caja and e.	fk_a004_num_dependencia=f.pk_num_dependencia and f.fk_a001_num_organismo=g.pk_num_organismo $filtro order by a.pk_num_entrada desc"
            );

        $listarEntrada->setFetchMode(PDO::FETCH_ASSOC);
        return $listarEntrada->fetchAll();
    }

    public function metVisualizarRegistro($pkNumRegistro, $metodo)
    {
        $verRegistro = $this->_db->query(
            "select a.pk_num_registro_documento, a.num_flag_archivo, a.num_registro, a.cod_memo, a.ind_documento, date_format(fec_documento,'%d/%m/%Y') as fecha_documento, date_format(a.fec_registro, '%d/%m/%Y %H:%i:%S') as fecha_registro, a.ind_descripcion, a.ind_resumen, b.pk_num_almacen, b.ind_descripcion_almacen, c.pk_num_pasillo, c.ind_pasillo, d.pk_num_estante, d.ind_descripcion_estante,e.pk_num_caja, e.ind_descripcion_caja, f.pk_num_documento, f.ind_descripcion_documento, g.pk_num_dependencia, g.ind_dependencia, h.pk_num_empleado, i.ind_nombre1, i.ind_apellido1 from ad_b001_registro_documento as a, ad_c001_almacen as b, ad_c002_pasillo as c, ad_c003_estante as d, ad_c004_caja as e, ad_c005_tipo_documento as f, a004_dependencia as g, rh_b001_empleado as h, a003_persona as i where a.fk_adc004_num_caja=e.pk_num_caja and  e.fk_adc003_num_estante=d.pk_num_estante and d.fk_adc002_num_pasillo=c.pk_num_pasillo and c.fk_adc001_num_almacen=b.pk_num_almacen and e.fk_adc005_num_documento=f.pk_num_documento and a.pk_num_registro_documento='$pkNumRegistro' and e.fk_a004_num_dependencia=g.pk_num_dependencia and a.fk_rhb001_num_empleado=h.pk_num_empleado and h.fk_a003_num_persona=i.pk_num_persona"
        );
        $verRegistro->setFetchMode(PDO::FETCH_ASSOC);
        if($metodo==1)
            return $verRegistro->fetch();
        else return $verRegistro->fetchAll();
    }

    // Método que permite ubicar el nombre del archivo y el número de registro del mismo
    public function metBuscarArchivo($pkNumRegistro)
    {
        $consultaArchivo = $this->_db->query(
            "select a.ind_nombre_archivo, b.num_registro from ad_c008_archivo as a, ad_b001_registro_documento as b where a.fk_adb001_num_registro_documento=b.pk_num_registro_documento and a.fk_adb001_num_registro_documento=$pkNumRegistro"
        );
        $consultaArchivo->setFetchMode(PDO::FETCH_ASSOC);
        return $consultaArchivo->fetch();
    }

    public function metConsultarSeguridadAlterna($usuario, $idAplicacion, $pkNumOrganismo)
    {
        $seguridadAlterna = $this->_db->query(
            "select b.pk_num_dependencia, b.ind_dependencia from a019_seguridad_dependencia as a, a004_dependencia as b, a001_organismo as c where a.fk_a004_num_dependencia=b.pk_num_dependencia and a.fk_a018_num_seguridad_usuario=$usuario and a.fk_a015_num_seguridad_aplicacion=$idAplicacion and b.fk_a001_num_organismo=c.pk_num_organismo and c.pk_num_organismo=$pkNumOrganismo"
        );
        $seguridadAlterna->setFetchMode(PDO::FETCH_ASSOC);
        return $seguridadAlterna->fetchAll();
    }
}// fin de la clase
?>
